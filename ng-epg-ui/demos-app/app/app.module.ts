import { AppRootComponent } from './../../src/components/app/app-root'
import { IonicModule } from './../../src/index'
import { NgModule } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'
import { FormsModule } from '@angular/forms'
import { HttpClientModule } from '@angular/common/http'
import { TranslateModule } from '@ngx-translate/core'

import { NavModule } from './../../src/components/nav'
import { AppComponent } from './app.component'
import { APP_PROVIDERS } from './app.providers'
import { DemosModule } from './component-demos'
import { NotFoundComponent } from './not-found.component'
import { TimeTick } from './../../src/util/time-tick'

@NgModule({
  declarations: [
    AppComponent,
    NotFoundComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    IonicModule.forRoot(AppComponent, {}, { links: [] }),
    TranslateModule.forRoot(),

    NavModule,
    DemosModule
  ],
  entryComponents: [
    NotFoundComponent
  ],
  providers: [
    ...APP_PROVIDERS, TimeTick
  ],
  bootstrap: [AppRootComponent]
})
export class AppModule {
  constructor () {
  }
}
