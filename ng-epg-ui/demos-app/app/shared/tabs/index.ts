export { Tab } from './tab.directive'
export { Tabset } from './tabset.component'
export { TabHeading } from './tab-heading.directive'
export { TabModule } from './tabs.module'
