import { NgModule, ModuleWithProviders } from '@angular/core'
import { CommonModule } from '@angular/common'

import { Tab } from './tab.directive'
import { Tabset } from './tabset.component'
import { TabHeading } from './tab-heading.directive'
import { NgTransclude } from './common'

@NgModule({
  imports: [CommonModule],
  exports: [Tab, Tabset, TabHeading, NgTransclude],
  declarations: [Tab, Tabset, TabHeading, NgTransclude]
})

export class TabModule {
  static forRoot (): ModuleWithProviders {
      return {
          ngModule: TabModule,
          providers: []
        }
    }
}
