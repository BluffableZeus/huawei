let currentLang = localStorage.getItem('lang')
currentLang = /^(en|zh)$/.exec(currentLang)
if (currentLang) {
  currentLang = currentLang[0]
} else {
  currentLang = 'en'
}
window['lang'] = currentLang

export const LANG = currentLang
