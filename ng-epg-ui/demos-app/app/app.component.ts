import { NotFoundComponent } from './not-found.component'
import { Component, ViewEncapsulation, HostListener, NgZone } from '@angular/core'
import { TranslateService } from '@ngx-translate/core'
import * as _ from 'underscore'

import { navs } from './component-demos'
const version = require('../../src/version.json')
const languageList = ['English', '中文']
@Component({
  selector: 'ui-app-root',
  templateUrl: './app.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [`
      app-root{height:100rem;}
  `]
})
export class AppComponent {
  public version = _.last(version)
  public root = NotFoundComponent
  private componentGroups: any
  private  isShowNav = true
  private _filterName: string
  set filterName (name: string) {
    this._filterName = name
    this.componentGroups = this.getCompotentGroups()
  }
  get filterName (): string {
    return this._filterName
  }

  public language: any = window['lang'] === 'en' ? languageList[1] : languageList[0]
  private getCompotentGroups () {
    console.log(navs)
    return _.chain(navs).sortBy('name').filter((route) => {

      return new RegExp(this.filterName, 'i').test(route.name)
    }).groupBy(val => {
      return val.group.ID
    }).values().value()
  }

  constructor (translateService: TranslateService,private zone: NgZone) {
    this.componentGroups = this.getCompotentGroups()
    translateService.setDefaultLang('en')
    translateService.use('en')
    window['t'] = function (key: string, interpolateParams: Object) {
      return translateService.instant(key, interpolateParams)
    }
  }

  changeLanguge () {
    let currentLang = window['lang'] === 'en' ? 'zh' : 'en'
    localStorage.setItem('lang', currentLang)
    window.location.reload()
  }

  showOrHideNav () {
    this.isShowNav = !this.isShowNav
  }

  @HostListener('focusin', ['$event'])
  public onFocusIn (event: KeyboardEvent) {
    this.zone.runOutsideAngular(() => {
      (event.target as HTMLElement).focus()
    })
  }
}
