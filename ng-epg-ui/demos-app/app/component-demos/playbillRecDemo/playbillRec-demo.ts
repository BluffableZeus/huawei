import * as _ from 'underscore'
import { Component, OnInit } from '@angular/core'
import { PlaybillRecordComponent } from '../../../../src/webtv-components/playbillRecord'

@Component({
  selector: 'playbill-record-demo',
  templateUrl: './playbillRec-demo.html',
  styleUrls: ['./playbillRec-demo.scss']
})

export class PlaybillRecordDemo implements OnInit {

  public recordData = {}

  ngOnInit () {
      this.recordData = {
          'ID': '931',
          'channelDetail': {
              'ID': '4',
              'channelNO': '4',
              'name': 'FOX',
              'physicalChannels': [
                  {
                    'ID': '4',
                    'barkerMediaID': '4',
                    'bitrate': '6144',
                    'btvCR': {
                          'bizMediaID': '1',
                          'durationType': '1',
                          'enable': '1',
                          'isContentValid': '1',
                          'isSubscribed': '1',
                          'isValid': '1',
                          'length': '3600',
                          'trickMode': '1'
                        },
                    'channelEncrypt': {
                          'CGMSA': '0',
                          'HDCPEnable': '0',
                          'encrypt': '0',
                          'macrovision': '0'
                        },
                    'channelNamespaces': [
                          'IPTV',
                          'OTT'
                        ],
                    'code': '4',
                    'cpltvCR': {
                          'bizMediaID': '1',
                          'durationType': '1',
                          'enable': '1',
                          'isContentValid': '1',
                          'isSubscribed': '1',
                          'isValid': '1',
                          'length': '1',
                          'trickMode': '1'
                        },
                    'cpvrRecordCR': {
                          'bizMediaID': '1',
                          'durationType': '1',
                          'enable': '1',
                          'isContentValid': '1',
                          'isSubscribed': '1',
                          'isValid': '1',
                          'length': '1',
                          'trickMode': '1'
                        },
                    'cutvCR': {
                          'bizMediaID': '1',
                          'durationType': '1',
                          'enable': '1',
                          'isContentValid': '1',
                          'isSubscribed': '1',
                          'isValid': '1',
                          'length': '72000',
                          'trickMode': '1'
                        },
                    'definition': '0',
                    'dimension': '2',
                    'fccEnable': '1',
                    'fecEnable': '1',
                    'fileFormat': '1',
                    'formatOf3D': '1',
                    'fps': '256',
                    'hssStreamImportFlag': '0',
                    'isSupportPIP': '0',
                    'maxBitrate': '',
                    'mediaName': 'PhysicalChannel4SD',
                    'npvrRecordCR': {
                          'bizMediaID': '1',
                          'durationType': '1',
                          'enable': '1',
                          'isContentValid': '1',
                          'isSubscribed': '1',
                          'isValid': '1',
                          'length': '1',
                          'trickMode': '1'
                        }
                  },
                  {
                    'ID': '104',
                    'barkerMediaID': '104',
                    'bitrate': '81920',
                    'channelEncrypt': {
                          'CGMSA': '0',
                          'HDCPEnable': '0',
                          'encrypt': '0',
                          'macrovision': '0'
                        },
                    'channelNamespaces': [
                          'IPTV'
                        ],
                    'code': '104',
                    'definition': '1',
                    'dimension': '2',
                    'fccEnable': '1',
                    'fecEnable': '1',
                    'fileFormat': '1',
                    'formatOf3D': '1',
                    'fps': '256',
                    'hssStreamImportFlag': '0',
                    'isSupportPIP': '0',
                    'maxBitrate': '',
                    'mediaName': 'PhysicalChannel4HD',
                    'previewCount': '1',
                    'previewLength': '500',
                    'videoCodec': 'H.264',
                    'multiBitrates': [],
                    'blockedNetworks': [],
                    'customFields': []
                  },
                  {
                    'ID': '54',
                    'barkerMediaID': '102',
                    'bitrate': '327680',
                    'channelEncrypt': {
                          'CGMSA': '0',
                          'HDCPEnable': '0',
                          'encrypt': '0',
                          'macrovision': '0'
                        },
                    'channelNamespaces': [
                          'IPTV'
                        ],
                    'code': '54',
                    'cpltvCR': {
                          'bizMediaID': '1',
                          'durationType': '1',
                          'enable': '1',
                          'isContentValid': '1',
                          'isSubscribed': '1',
                          'isValid': '1',
                          'length': '1',
                          'trickMode': '1'
                        },
                    'definition': '2',
                    'dimension': '2',
                    'fccEnable': '1',
                    'fecEnable': '1',
                    'fileFormat': '1',
                    'formatOf3D': '1',
                    'fps': '256',
                    'hssStreamImportFlag': '0',
                    'isEncryptedRecOnHDD': '',
                    'isSupportPIP': '0',
                    'maxBitrate': '',
                    'mediaName': 'PhysicalChannel24K'
                  }
                ]
            },
          'channelID': '4',
          'contentID': '251',
          'endTime': '1497536040000',
          'hasRecordingPVR': '0',
          'isCPVR': '0',
          'isCUTV': '1',
          'isNPVR': '1',
          'name': 'The Green Mile',
          'playbillSeries': {
              'seriesID': '1'
            },
          'startTime': '1497530160000',
          'isNow': false
        }
    }
}
