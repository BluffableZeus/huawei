import { Component } from '@angular/core'
let globals = require('../../shared/global.ts')

// webpack html imports
let doc = require('!!html-loader!markdown-loader!ng-epg-ui/webtv-components/playbillRecord/readme-' + globals.LANG +  '.md')
let titleDoc = require('!!html-loader!markdown-loader!ng-epg-ui/webtv-components/playbillRecord/title-' + globals.LANG +  '.md')

let ts = require('!!prismjs-loader?lang=typescript!./playbillRec-demo.ts')
let html = require('!!prismjs-loader?lang=markup!./playbillRec-demo.html')

@Component({
  selector: 'playbill-record-section',
  template: `
   <demo-section [name]="name" [src]="src" [titleDoc]="titleDoc" [html]="html" [ts]="ts" [doc]="doc">
      <playbill-record-demo></playbill-record-demo>
  </demo-section>
  `,
  styles: [`
      playbill-record-demo{
		  position: relative;
      }
  `]
})
export class PlaybillRecordSection {
  public name = 'PlaybillRecord'
  public src = ''
  public html: string = html
  public ts: string = ts
  public titleDoc: string = titleDoc
  public doc: string = doc
}
