import { Component } from '@angular/core'
let globals = require('../../shared/global.ts')

// webpack html imports
let doc = require('!!html-loader!markdown-loader!ng-epg-ui/webtv-components/episodeTrailer/readme-' + globals.LANG +  '.md')
let titleDoc = require('!!html-loader!markdown-loader!ng-epg-ui/webtv-components/episodeTrailer/title-' + globals.LANG +  '.md')

let ts = require('!!prismjs-loader?lang=typescript!./episodeAndTrailer-demo.ts')
let html = require('!!prismjs-loader?lang=markup!./episodeAndTrailer-demo.html')

@Component({
  selector: 'episode-trailer-section',
  template: `
   <demo-section [name]="name" [src]="src" [titleDoc]="titleDoc" [html]="html" [ts]="ts" [doc]="doc">
      <episode-trailer-demo></episode-trailer-demo>
  </demo-section>
  `,
  styles: [`
      episode-trailer-demo{
      }
  `]
})
export class EpisodeSectionComponent {
  public name = 'Episode'
  public src = ''
  public html: string = html
  public ts: string = ts
  public titleDoc: string = titleDoc
  public doc: string = doc
}
