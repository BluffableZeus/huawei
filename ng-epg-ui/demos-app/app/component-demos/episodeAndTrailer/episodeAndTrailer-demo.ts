import * as _ from 'underscore'
import { Component, OnInit, ViewChild } from '@angular/core'
import { EventService } from '../../../sdk/event.service'
import { EpisodeTrailerComponent } from '../../../../src/webtv-components/episodeTrailer'

@Component({
  selector: 'episode-trailer-demo',
  templateUrl: './episodeAndTrailer-demo.html',
  styleUrls: ['./episodeAndTrailer-demo.scss']
})

export class EpisodeTrailerDemo implements OnInit {

  public vodDetail

  ngOnInit () {
      this.vodDetail = {
          'ID': '1175',
          'VODType': '1',
          'clipfiles': [
              {
                'ID': '1741',
                'elapseTime': '180',
                'picture': {
                      'posters': [
                          'http://10.162.147.137:8087/VSP/images/112/p1.jpg'
                        ]
                    }
              },
              {
                'ID': '1742',
                'elapseTime': '180',
                'picture': {
                      'posters': [
                          'http://10.162.147.137:8087/VSP/images/113/p1.jpg'
                        ]
                    }
              },
              {
                'ID': '1743',
                'elapseTime': '180',
                'picture': {
                      'posters': [
                          'http://10.162.147.137:8087/VSP/images/114/p1.jpg'
                        ]
                    }
              },
              {
                'ID': '1744',
                'elapseTime': '180',
                'picture': {
                      'posters': [
                          'http://10.162.147.137:8087/VSP/images/65/p.jpg'
                        ]
                    }
              }
            ],
          'episodeCount': '70',
          'episodes': [
              {
                'VOD': {
                      'ID': '1176'
                    },
                'sitcomNO': '1'
              },
              {
                'VOD': {
                      'ID': '1177'
                    },
                'sitcomNO': '2'
              },
              {
                'VOD': {
                      'ID': '1178'
                    },
                'sitcomNO': '3'
              },
              {
                'VOD': {
                      'ID': '1179'
                    },
                'sitcomNO': '4'
              },
              {
                'VOD': {
                      'ID': '1180'
                    },
                'sitcomNO': '5'
              }
            ]
        }
    }
}
