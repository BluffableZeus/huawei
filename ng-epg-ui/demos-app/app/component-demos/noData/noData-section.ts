import { Component } from '@angular/core'
let globals = require('../../shared/global.ts')

// webpack html imports
let doc = require('!!html-loader!markdown-loader!ng-epg-ui/webtv-components/noData/readme-' + globals.LANG +  '.md')
let titleDoc = require('!!html-loader!markdown-loader!ng-epg-ui/webtv-components/noData/title-' + globals.LANG +  '.md')

let ts = require('!!prismjs-loader?lang=typescript!./noData-demo.ts')
let html = require('!!prismjs-loader?lang=markup!./noData-demo.html')

@Component({
  selector: 'noData-demo-section',
  template: `
   <demo-section [name]="name" [src]="src" [titleDoc]="titleDoc" [html]="html" [ts]="ts" [doc]="doc">
      <noData-demo></noData-demo>
  </demo-section>
  `,
  styles: [`
      noData-demo {
      }
  `]
})
export class NoDataSectionComponent {
  public name = 'NoDataDemo'
  public src = ''
  public html: string = html
  public ts: string = ts
  public titleDoc: string = titleDoc
  public doc: string = doc
}
