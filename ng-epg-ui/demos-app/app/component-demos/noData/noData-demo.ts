import * as _ from 'underscore'
import { Component, OnInit, ViewChild } from '@angular/core'
import { WaterfallnoData } from '../../../../src/webtv-components/noData'
import { EventService } from '../../../sdk/event.service'

@Component({
  selector: 'noData-demo',
  templateUrl: './noData-demo.html',
  styleUrls: ['./noData-demo.scss']
})

export class NoDataDemo implements OnInit {
  @ViewChild(WaterfallnoData) waterfallnoData: WaterfallnoData

  ngOnInit () {

    }
}
