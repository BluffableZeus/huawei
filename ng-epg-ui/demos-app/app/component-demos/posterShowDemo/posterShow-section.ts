import { Component } from '@angular/core'
let globals = require('../../shared/global.ts')

// webpack html imports
let doc = require('!!html-loader!markdown-loader!ng-epg-ui/webtv-components/childcolumnshow/readme-' + globals.LANG +  '.md')
let titleDoc = require('!!html-loader!markdown-loader!ng-epg-ui/webtv-components/childcolumnshow/title-' + globals.LANG +  '.md')

let ts = require('!!prismjs-loader?lang=typescript!./posterShow-demo.ts')
let html = require('!!prismjs-loader?lang=markup!./posterShow-demo.html')

@Component({
  selector: 'poster-show-section',
  template: `
   <demo-section [name]="name" [src]="src" [titleDoc]="titleDoc" [html]="html" [ts]="ts" [doc]="doc">
      <poster-show-demo></poster-show-demo>
  </demo-section>
  `,
  styles: [`
      poster-show-demo{
		    position: relative;
      }
  `]
})
export class PosterShowSection {
  public name = 'PosterShow'
  public src = ''
  public html: string = html
  public ts: string = ts
  public titleDoc: string = titleDoc
  public doc: string = doc
}
