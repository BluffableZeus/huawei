import * as _ from 'underscore'
import { Component, OnInit, Input, EventEmitter, ViewChild } from '@angular/core'
import { ColumnShowComponent } from '../../../../src/webtv-components/childcolumnshow'

@Component({
  selector: 'poster-show-demo',
  templateUrl: './posterShow-demo.html',
  styleUrls: ['./posterShow-demo.scss']
})

export class PosterShowDemo implements OnInit {

  public idList = ['UIDemo', 'demoVODList', 'demoLeft', 'demoRight']
  public vodShow = [true, true, true]
  public vodListSuspension
  public vodDatas = []

  ngOnInit () {
      this.vodDatas = [
          {
            'VODList': [
                  {
                    'ID': '1005',
                    'VODType': '0',
                    'airDate': '20170607',
                    'averageScore': '0.0',
                    'castRoles': [],
                    'contentType': 'VIDEO_VOD',
                    'endTime': '20990131235959',
                    'introduce': 'YoungCarlFredricksenisashy,quietboywhoidolizesrenownedexplorerCharlesF.Muntz.Heissaddenedtolearn',
                    'isLike': 0,
                    'isSubscribed': '0',
                    'likes': 0,
                    'name': "1005The King's Speech",
                    'picture': {
                          'posters': [
                              'http://10.162.147.137:8087/VSP/images/95/p1.jpg?x=214&y=284&ar=ignore'
                            ]
                        }
                  },
                  {
                    'ID': '1006',
                    'VODType': '0',
                    'airDate': '20170607',
                    'averageScore': '8.4',
                    'castRoles': [],
                    'contentType': 'VIDEO_VOD',
                    'endTime': '20990131235959',
                    'introduce': "A 1940's period drama about about a woman who has been wronged by her husband, but must help him after he has fallen in with a family of grifters. ",
                    'isLike': 0,
                    'isSubscribed': '0',
                    'likes': 0,
                    'name': '1006Teenage Mutant Ninja Turtles',
                    'picture': {
                          'posters': [
                              'http://10.162.147.137:8087/VSP/images/96/p1.jpg?x=214&y=284&ar=ignore'
                            ]
                        }
                  },
                  {
                    'ID': '1011',
                    'VODType': '0',
                    'airDate': '20170607',
                    'averageScore': '0.0',
                    'castRoles': [],
                    'contentType': 'VIDEO_VOD',
                    'endTime': '20990131235959',
                    'introduce': "Two detectives, Jack Welles (Matt Dillon) and Eddie Hatcher (Jay Hernandez), investigate a daring heist by a group of well-organized bank robbers. The crew, led by Gordon Cozier (Idris Elba), consists of John (Paul Walker), A.J. (Hayden Christensen), and brothers Jake (Michael Ealy) and Jesse (Chris Brown) Attica. The crew is without a former member, Ghost (T.I.), who was caught during a previous robbery 5 years ago. In his absence, Jake has begun a relationship with his former girlfriend Lilly (Zoe Saldana), who has accepted his proposal.\nGhost surprises the crew after getting paroled. He insists he harbors no ill feelings toward the crew for abandoning him and draws them into a heist of an armored car for a $20 million dollar payoff. Meanwhile, Welles begins to zero in on some of the members of the crew and comes up with evidence that a second heist is in the making.\nThey create a subway and call it \"The Subway\".A protracted chase ensues during which Jesse is cornered and he is forced to shoot Detective Hatcher in his escape. Despite his partner's best efforts, Hatcher dies from his wounds.",
                    'isLike': 0,
                    'isSubscribed': '0',
                    'likes': 0,
                    'name': '1011Penguins of Madagascar',
                    'picture': {
                          'posters': [
                              'http://10.162.147.137:8087/VSP/images/101/p1.jpg?x=214&y=284&ar=ignore'
                            ]
                        }
                  },
                  {
                    'ID': '1012',
                    'VODType': '0',
                    'airDate': '20170607',
                    'averageScore': '0.0',
                    'castRoles': [],
                    'contentType': 'VIDEO_VOD',
                    'endTime': '20990131235959',
                    'introduce': "Two detectives, Jack Welles (Matt Dillon) and Eddie Hatcher (Jay Hernandez), investigate a daring heist by a group of well-organized bank robbers. The crew, led by Gordon Cozier (Idris Elba), consists of John (Paul Walker), A.J. (Hayden Christensen), and brothers Jake (Michael Ealy) and Jesse (Chris Brown) Attica. The crew is without a former member, Ghost (T.I.), who was caught during a previous robbery 5 years ago. In his absence, Jake has begun a relationship with his former girlfriend Lilly (Zoe Saldana), who has accepted his proposal.\nGhost surprises the crew after getting paroled. He insists he harbors no ill feelings toward the crew for abandoning him and draws them into a heist of an armored car for a $20 million dollar payoff. Meanwhile, Welles begins to zero in on some of the members of the crew and comes up with evidence that a second heist is in the making.\nThey create a subway and call it \"The Subway\".A protracted chase ensues during which Jesse is cornered and he is forced to shoot Detective Hatcher in his escape. Despite his partner's best efforts, Hatcher dies from his wounds.",
                    'isLike': 0,
                    'isSubscribed': '0',
                    'likes': 0,
                    'name': '1011Penguins of Madagascar',
                    'picture': {
                          'posters': [
                              'http://10.162.147.137:8087/VSP/images/102/p1.jpg?x=214&y=284&ar=ignore'
                            ]
                        }
                  },
                  {
                    'ID': '1013',
                    'VODType': '0',
                    'airDate': '20170607',
                    'averageScore': '0.0',
                    'castRoles': [],
                    'contentType': 'VIDEO_VOD',
                    'endTime': '20990131235959',
                    'introduce': "Two detectives, Jack Welles (Matt Dillon) and Eddie Hatcher (Jay Hernandez), investigate a daring heist by a group of well-organized bank robbers. The crew, led by Gordon Cozier (Idris Elba), consists of John (Paul Walker), A.J. (Hayden Christensen), and brothers Jake (Michael Ealy) and Jesse (Chris Brown) Attica. The crew is without a former member, Ghost (T.I.), who was caught during a previous robbery 5 years ago. In his absence, Jake has begun a relationship with his former girlfriend Lilly (Zoe Saldana), who has accepted his proposal.\nGhost surprises the crew after getting paroled. He insists he harbors no ill feelings toward the crew for abandoning him and draws them into a heist of an armored car for a $20 million dollar payoff. Meanwhile, Welles begins to zero in on some of the members of the crew and comes up with evidence that a second heist is in the making.\nThey create a subway and call it \"The Subway\".A protracted chase ensues during which Jesse is cornered and he is forced to shoot Detective Hatcher in his escape. Despite his partner's best efforts, Hatcher dies from his wounds.",
                    'isLike': 0,
                    'isSubscribed': '0',
                    'likes': 0,
                    'name': '1011Penguins of Madagascar',
                    'picture': {
                          'posters': [
                              'http://10.162.147.137:8087/VSP/images/103/p1.jpg?x=214&y=284&ar=ignore'
                            ]
                        }
                  },
                  {
                    'ID': '1014',
                    'VODType': '0',
                    'airDate': '20170607',
                    'averageScore': '0.0',
                    'castRoles': [],
                    'contentType': 'VIDEO_VOD',
                    'endTime': '20990131235959',
                    'introduce': "Two detectives, Jack Welles (Matt Dillon) and Eddie Hatcher (Jay Hernandez), investigate a daring heist by a group of well-organized bank robbers. The crew, led by Gordon Cozier (Idris Elba), consists of John (Paul Walker), A.J. (Hayden Christensen), and brothers Jake (Michael Ealy) and Jesse (Chris Brown) Attica. The crew is without a former member, Ghost (T.I.), who was caught during a previous robbery 5 years ago. In his absence, Jake has begun a relationship with his former girlfriend Lilly (Zoe Saldana), who has accepted his proposal.\nGhost surprises the crew after getting paroled. He insists he harbors no ill feelings toward the crew for abandoning him and draws them into a heist of an armored car for a $20 million dollar payoff. Meanwhile, Welles begins to zero in on some of the members of the crew and comes up with evidence that a second heist is in the making.\nThey create a subway and call it \"The Subway\".A protracted chase ensues during which Jesse is cornered and he is forced to shoot Detective Hatcher in his escape. Despite his partner's best efforts, Hatcher dies from his wounds.",
                    'isLike': 0,
                    'isSubscribed': '0',
                    'likes': 0,
                    'name': '1011Penguins of Madagascar',
                    'picture': {
                          'posters': [
                              'http://10.162.147.137:8087/VSP/images/97/p1.jpg?x=214&y=284&ar=ignore'
                            ]
                        }
                  },
                  {
                    'ID': '1015',
                    'VODType': '0',
                    'airDate': '20170607',
                    'averageScore': '0.0',
                    'castRoles': [],
                    'contentType': 'VIDEO_VOD',
                    'endTime': '20990131235959',
                    'introduce': "Two detectives, Jack Welles (Matt Dillon) and Eddie Hatcher (Jay Hernandez), investigate a daring heist by a group of well-organized bank robbers. The crew, led by Gordon Cozier (Idris Elba), consists of John (Paul Walker), A.J. (Hayden Christensen), and brothers Jake (Michael Ealy) and Jesse (Chris Brown) Attica. The crew is without a former member, Ghost (T.I.), who was caught during a previous robbery 5 years ago. In his absence, Jake has begun a relationship with his former girlfriend Lilly (Zoe Saldana), who has accepted his proposal.\nGhost surprises the crew after getting paroled. He insists he harbors no ill feelings toward the crew for abandoning him and draws them into a heist of an armored car for a $20 million dollar payoff. Meanwhile, Welles begins to zero in on some of the members of the crew and comes up with evidence that a second heist is in the making.\nThey create a subway and call it \"The Subway\".A protracted chase ensues during which Jesse is cornered and he is forced to shoot Detective Hatcher in his escape. Despite his partner's best efforts, Hatcher dies from his wounds.",
                    'isLike': 0,
                    'isSubscribed': '0',
                    'likes': 0,
                    'name': '1011Penguins of Madagascar',
                    'picture': {
                          'posters': [
                              'http://10.162.147.137:8087/VSP/images/98/p1.jpg?x=214&y=284&ar=ignore'
                            ]
                        }
                  },
                  {
                    'ID': '1016',
                    'VODType': '0',
                    'airDate': '20170607',
                    'averageScore': '0.0',
                    'castRoles': [],
                    'contentType': 'VIDEO_VOD',
                    'endTime': '20990131235959',
                    'introduce': "Two detectives, Jack Welles (Matt Dillon) and Eddie Hatcher (Jay Hernandez), investigate a daring heist by a group of well-organized bank robbers. The crew, led by Gordon Cozier (Idris Elba), consists of John (Paul Walker), A.J. (Hayden Christensen), and brothers Jake (Michael Ealy) and Jesse (Chris Brown) Attica. The crew is without a former member, Ghost (T.I.), who was caught during a previous robbery 5 years ago. In his absence, Jake has begun a relationship with his former girlfriend Lilly (Zoe Saldana), who has accepted his proposal.\nGhost surprises the crew after getting paroled. He insists he harbors no ill feelings toward the crew for abandoning him and draws them into a heist of an armored car for a $20 million dollar payoff. Meanwhile, Welles begins to zero in on some of the members of the crew and comes up with evidence that a second heist is in the making.\nThey create a subway and call it \"The Subway\".A protracted chase ensues during which Jesse is cornered and he is forced to shoot Detective Hatcher in his escape. Despite his partner's best efforts, Hatcher dies from his wounds.",
                    'isLike': 0,
                    'isSubscribed': '0',
                    'likes': 0,
                    'name': '1011Penguins of Madagascar',
                    'picture': {
                          'posters': [
                              'http://10.162.147.137:8087/VSP/images/99/p1.jpg?x=214&y=284&ar=ignore'
                            ]
                        }
                  }
                ],
            'subjectName': 'Oscars 2016',
            'subjectID': 'Oscars2016',
            'hasChildren': '0',
            'showSubjectName': 'OSCARS 2016'
          }
        ]
      this.vodListSuspension = [
          {
            'titleName': 'Oscars2016',
            'floatInfo': {
                  'ID': '1005',
                  'VODType': '0',
                  'airDate': '20170607',
                  'averageScore': '0.0',
                  'castRoles': [],
                  'contentType': 'VIDEO_VOD',
                  'endTime': '20990131235959',
                  'introduce': 'YoungCarlFredricksenisashy,quietboywhoidolizesrenownedexplorerCharlesF.Muntz.Heissaddenedtolearn.',
                  'isLike': 0,
                  'isSubscribed': '0',
                  'likes': 0,
                  'name': "1005The King's Speech",
                  'produceDate': '20121111',
                  'rating': {
                      'ID': '0',
                      'code': '1',
                      'name': 'G'
                    },
                  'rentPeriod': '0',
                  'series': [],
                  'startTime': '20120712200000',
                  'genres': [],
                  'audioLanguages': [],
                  'subtitleLanguages': [],
                  'advisories': [],
                  'customFields': [],
                  'locationCopyrights': [],
                  'focusRoute': 'home'
                }
          },
          {
            'titleName': 'Oscars2016',
            'floatInfo': {
                  'ID': '1005',
                  'VODType': '0',
                  'airDate': '20170607',
                  'averageScore': '0.0',
                  'castRoles': [],
                  'contentType': 'VIDEO_VOD',
                  'endTime': '20990131235959',
                  'introduce': 'YoungCarlFredricksenisashy,quietboywhoidolizesrenownedexplorerCharlesF.Muntz.Heissaddenedtolearn.',
                  'isLike': 0,
                  'isSubscribed': '0',
                  'likes': 0,
                  'name': "1005The King's Speech",
                  'produceDate': '20121111',
                  'rating': {
                      'ID': '0',
                      'code': '1',
                      'name': 'G'
                    },
                  'rentPeriod': '0',
                  'series': [],
                  'startTime': '20120712200000',
                  'genres': [],
                  'audioLanguages': [],
                  'subtitleLanguages': [],
                  'advisories': [],
                  'customFields': [],
                  'locationCopyrights': [],
                  'focusRoute': 'home'
                }
          },
          {
            'titleName': 'Oscars2016',
            'floatInfo': {
                  'ID': '1005',
                  'VODType': '0',
                  'airDate': '20170607',
                  'averageScore': '0.0',
                  'castRoles': [],
                  'contentType': 'VIDEO_VOD',
                  'endTime': '20990131235959',
                  'introduce': 'YoungCarlFredricksenisashy,quietboywhoidolizesrenownedexplorerCharlesF.Muntz.Heissaddenedtolearn.',
                  'isLike': 0,
                  'isSubscribed': '0',
                  'likes': 0,
                  'name': "1005The King's Speech",
                  'produceDate': '20121111',
                  'rating': {
                      'ID': '0',
                      'code': '1',
                      'name': 'G'
                    },
                  'rentPeriod': '0',
                  'series': [],
                  'startTime': '20120712200000',
                  'genres': [],
                  'audioLanguages': [],
                  'subtitleLanguages': [],
                  'advisories': [],
                  'customFields': [],
                  'locationCopyrights': [],
                  'focusRoute': 'home'
                }
          },
          {
            'titleName': 'Oscars2016',
            'floatInfo': {
                  'ID': '1005',
                  'VODType': '0',
                  'airDate': '20170607',
                  'averageScore': '0.0',
                  'castRoles': [],
                  'contentType': 'VIDEO_VOD',
                  'endTime': '20990131235959',
                  'introduce': 'YoungCarlFredricksenisashy,quietboywhoidolizesrenownedexplorerCharlesF.Muntz.Heissaddenedtolearn.',
                  'isLike': 0,
                  'isSubscribed': '0',
                  'likes': 0,
                  'name': "1005The King's Speech",
                  'produceDate': '20121111',
                  'rating': {
                      'ID': '0',
                      'code': '1',
                      'name': 'G'
                    },
                  'rentPeriod': '0',
                  'series': [],
                  'startTime': '20120712200000',
                  'genres': [],
                  'audioLanguages': [],
                  'subtitleLanguages': [],
                  'advisories': [],
                  'customFields': [],
                  'locationCopyrights': [],
                  'focusRoute': 'home'
                }
          },
          {
            'titleName': 'Oscars2016',
            'floatInfo': {
                  'ID': '1005',
                  'VODType': '0',
                  'airDate': '20170607',
                  'averageScore': '0.0',
                  'castRoles': [],
                  'contentType': 'VIDEO_VOD',
                  'endTime': '20990131235959',
                  'introduce': 'YoungCarlFredricksenisashy,quietboywhoidolizesrenownedexplorerCharlesF.Muntz.Heissaddenedtolearn.',
                  'isLike': 0,
                  'isSubscribed': '0',
                  'likes': 0,
                  'name': "1005The King's Speech",
                  'produceDate': '20121111',
                  'rating': {
                      'ID': '0',
                      'code': '1',
                      'name': 'G'
                    },
                  'rentPeriod': '0',
                  'series': [],
                  'startTime': '20120712200000',
                  'genres': [],
                  'audioLanguages': [],
                  'subtitleLanguages': [],
                  'advisories': [],
                  'customFields': [],
                  'locationCopyrights': [],
                  'focusRoute': 'home'
                }
          },
          {
            'titleName': 'Oscars2016',
            'floatInfo': {
                  'ID': '1005',
                  'VODType': '0',
                  'airDate': '20170607',
                  'averageScore': '0.0',
                  'castRoles': [],
                  'contentType': 'VIDEO_VOD',
                  'endTime': '20990131235959',
                  'introduce': 'YoungCarlFredricksenisashy,quietboywhoidolizesrenownedexplorerCharlesF.Muntz.Heissaddenedtolearn.',
                  'isLike': 0,
                  'isSubscribed': '0',
                  'likes': 0,
                  'name': "1005The King's Speech",
                  'produceDate': '20121111',
                  'rating': {
                      'ID': '0',
                      'code': '1',
                      'name': 'G'
                    },
                  'rentPeriod': '0',
                  'series': [],
                  'startTime': '20120712200000',
                  'genres': [],
                  'audioLanguages': [],
                  'subtitleLanguages': [],
                  'advisories': [],
                  'customFields': [],
                  'locationCopyrights': [],
                  'focusRoute': 'home'
                }
          },
          {
            'titleName': 'Oscars2016',
            'floatInfo': {
                  'ID': '1005',
                  'VODType': '0',
                  'airDate': '20170607',
                  'averageScore': '0.0',
                  'castRoles': [],
                  'contentType': 'VIDEO_VOD',
                  'endTime': '20990131235959',
                  'introduce': 'YoungCarlFredricksenisashy,quietboywhoidolizesrenownedexplorerCharlesF.Muntz.Heissaddenedtolearn.',
                  'isLike': 0,
                  'isSubscribed': '0',
                  'likes': 0,
                  'name': "1005The King's Speech",
                  'produceDate': '20121111',
                  'rating': {
                      'ID': '0',
                      'code': '1',
                      'name': 'G'
                    },
                  'rentPeriod': '0',
                  'series': [],
                  'startTime': '20120712200000',
                  'genres': [],
                  'audioLanguages': [],
                  'subtitleLanguages': [],
                  'advisories': [],
                  'customFields': [],
                  'locationCopyrights': [],
                  'focusRoute': 'home'
                }
          },
          {
            'titleName': 'Oscars2016',
            'floatInfo': {
                  'ID': '1005',
                  'VODType': '0',
                  'airDate': '20170607',
                  'averageScore': '0.0',
                  'castRoles': [],
                  'contentType': 'VIDEO_VOD',
                  'endTime': '20990131235959',
                  'introduce': 'YoungCarlFredricksenisashy,quietboywhoidolizesrenownedexplorerCharlesF.Muntz.Heissaddenedtolearn.',
                  'isLike': 0,
                  'isSubscribed': '0',
                  'likes': 0,
                  'name': "1005The King's Speech",
                  'produceDate': '20121111',
                  'rating': {
                      'ID': '0',
                      'code': '1',
                      'name': 'G'
                    },
                  'rentPeriod': '0',
                  'series': [],
                  'startTime': '20120712200000',
                  'genres': [],
                  'audioLanguages': [],
                  'subtitleLanguages': [],
                  'advisories': [],
                  'customFields': [],
                  'locationCopyrights': [],
                  'focusRoute': 'home'
                }
          }
        ]
    }

  onClickDetail (e) {

    }
}
