import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router'

@Component({
  template: `
        <ion-nav [root]="root"></ion-nav>
    `
})
export class DemosComponent implements OnInit {
  root: string

  constructor (
        private route: ActivatedRoute
    ) { }

  ngOnInit () {
      this.route.params.map(p => p['name']).subscribe((newName: string) => {
          this.root = newName || 'AvatarComponent'
        })
    }
}
