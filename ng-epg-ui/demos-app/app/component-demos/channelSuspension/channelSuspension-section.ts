import { Component } from '@angular/core'
let globals = require('../../shared/global.ts')

// webpack html imports
let doc = require('!!html-loader!markdown-loader!ng-epg-ui/webtv-components/channelSuspension/readme-' + globals.LANG +  '.md')
let titleDoc = require('!!html-loader!markdown-loader!ng-epg-ui/webtv-components/channelSuspension/title-' + globals.LANG +  '.md')

let ts = require('!!prismjs-loader?lang=typescript!./channelSuspension-demo.ts')
let html = require('!!prismjs-loader?lang=markup!./channelSuspension-demo.html')

@Component({
  selector: 'suspension-channel-section',
  template: `
   <demo-section [name]="name" [src]="src" [titleDoc]="titleDoc" [html]="html" [ts]="ts" [doc]="doc">
      <suspension-demo-channel></suspension-demo-channel>
  </demo-section>
  `,
  styles: [`
      suspension-demo-channel{
          width: 680px;
          height: 382px;
          margin-right: 20px;
          margin-bottom: 20px;
          position: relative;
          top: 0px;
          cursor: pointer;
          display: block;
      }
  `]
})
export class SuspensionSectionComponent {
  public name = 'Suspension'
  public src = ''
  public html: string = html
  public ts: string = ts
  public titleDoc: string = titleDoc
  public doc: string = doc
}
