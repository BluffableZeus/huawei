import * as _ from 'underscore'
import { Component, OnInit, ViewChild } from '@angular/core'
import { DirectionScroll } from '../../../../src/webtv-components/scroll/directionScroll.component'
import { TransverseScroll } from '../../../../src/webtv-components/scroll/transverseScroll.component'
import { EventService } from '../../../sdk/event.service'

@Component({
  selector: 'scroll-demo',
  templateUrl: './scroll-demo.html',
  styleUrls: ['./scroll-demo.scss']
})

export class ScrollDemo implements OnInit {

  constructor (
        private directionScroll: DirectionScroll,
        private transverseScroll: TransverseScroll) {
   }

  ngOnInit () {
      this.pluseOnScroll()
    }

  pluseOnScroll () {
      let self = this
      _.delay(() => {
          let oConter, oUl, oScroll, oSpan
          let oBox = document.getElementById('scrollDemo-list')
          oConter = document.getElementById('scrollDemo-content')
          oUl = document.getElementById('scrollDemoconter')
          oScroll = document.getElementById('scrollDemo')
          oSpan = oScroll.getElementsByTagName('span')[0]
          self.directionScroll.setScroll(oBox, oConter, oUl, oScroll, oSpan, false)
        }, 200)
    }

    // transverseOnScroll() {
    //     let oConter, oUl, oScroll, oSpan;
    //     let oBox = document.getElementById('conter');
    //     oConter = document.getElementById('channelRightContent');
    //     oUl = document.getElementById('channelPlaybills');
    //     oScroll = document.getElementById('transverseScroll');
    //     if (oScroll) {
    //         oSpan = oScroll.getElementsByTagName('span')[0];
    //     }
    //     let allDay = document.getElementById('allDay');
    //     this.transverseScroll.setScroll(oBox, oConter, oUl, oScroll, oSpan, allDay);
    // }
}
