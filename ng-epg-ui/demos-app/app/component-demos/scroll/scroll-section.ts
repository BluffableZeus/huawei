import { Component } from '@angular/core'
let globals = require('../../shared/global.ts')

// webpack html imports
let doc = require('!!html-loader!markdown-loader!ng-epg-ui/webtv-components/scroll/readme-' + globals.LANG +  '.md')
let titleDoc = require('!!html-loader!markdown-loader!ng-epg-ui/webtv-components/scroll/title-' + globals.LANG +  '.md')

let ts = require('!!prismjs-loader?lang=typescript!./scroll-demo.ts')
let html = require('!!prismjs-loader?lang=markup!./scroll-demo.html')

@Component({
  selector: 'scroll-section',
  template: `
   <demo-section [name]="name" [src]="src" [titleDoc]="titleDoc" [html]="html" [ts]="ts" [doc]="doc">
      <scroll-demo></scroll-demo>
  </demo-section>
  `,
  styles: [`
      scroll-demo {
          width: 630px;
          height: 380px;
          margin-right: 20px;
          margin-bottom: 20px;
          position: relative;
          top: 0px;
          cursor: pointer;
          display: block;
      }
  `]
})
export class ScrollSectionComponent {
  public name = 'ScrollDemo'
  public src = ''
  public html: string = html
  public ts: string = ts
  public titleDoc: string = titleDoc
  public doc: string = doc
}
