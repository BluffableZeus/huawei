import { Component } from '@angular/core'
let globals = require('../../shared/global.ts')

// webpack html imports
let doc = require('!!html-loader!markdown-loader!ng-epg-ui/webtv-components/liveTvMiniDetail/readme-' + globals.LANG +  '.md')
let titleDoc = require('!!html-loader!markdown-loader!ng-epg-ui/webtv-components/liveTvMiniDetail/title-' + globals.LANG +  '.md')

let ts = require('!!prismjs-loader?lang=typescript!./liveTvMiniDetail-demo.ts')
let html = require('!!prismjs-loader?lang=markup!./liveTvMiniDetail-demo.html')

@Component({
  selector: 'livetvmini-section',
  template: `
   <demo-section [name]="name" [src]="src" [titleDoc]="titleDoc" [html]="html" [ts]="ts" [doc]="doc">
      <livetvmini></livetvmini>
  </demo-section>
  `,
  styles: [`
     livetvmini {
          width: 400px;
          height: 873px;
          position: relative;
          top: 0px;
          cursor: pointer;
          display: block;
      }
  `]
})
export class LiveTvMiniDemoSectionComponent {
  public name = 'Suspension'
  public src = ''
  public html: string = html
  public ts: string = ts
  public titleDoc: string = titleDoc
  public doc: string = doc
}
