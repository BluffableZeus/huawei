import { SuspensionSectionComponent } from './channelSuspension/channelSuspension-section'
import { NoDataSectionComponent } from './noData/noData-section'
import { ScrollSectionComponent } from './scroll/scroll-section'
import { EpisodeSectionComponent } from './episodeAndTrailer/episodeAndTrailer-section'
import { LiveTvMiniDemoSectionComponent } from './liveTvMiniDetail/liveTvMiniDetail-section'
import { TimeSectionComponent } from './timeDemo/timeSelect-section'
import { PlaybillRecordSection } from './playbillRecDemo/playbillRec-section'
import { PosterShowSection } from './posterShowDemo/posterShow-section'
import { VodFilterSection } from './vodFilterDemo/vodFilter-section'
import { ReminderSection } from './reminderDemo/reminder-section'

export const groups = {
  basiclist: { ID: 2, value: 'Basic List' },
  list: { ID: 4, value: 'List' },
  playcontrol: { ID: 6, value: 'Play control' },
  poster: { ID: 3, value: 'Poster' },
  filter: { ID: 5, value: 'Filter box' },
  basic: { ID: 3, value: 'Basic' },
  message: { ID: 7, value: 'Mesage box' },
  time: { ID: 8, value: 'Time picker' },
  epg: { ID: 10, value: 'Business related' },
  others: { ID: 11, value: 'Others' },
  style: { ID: 12, value: 'CommonStyle' },
  tvguide: { ID: 9, value: 'TVGuide' },
  develop: { ID: 0, value: 'Development Guide' }
}

export const navs = [
    { path: 'SuspensionChannel', name: 'SuspensionChannel', component: SuspensionSectionComponent, group: groups.basic },
    { path: 'NoData', name: 'NoData', component: NoDataSectionComponent, group: groups.basic },
    { path: 'ScrollSection', name: 'ScrollSection', component: ScrollSectionComponent, group: groups.basic },
    { path: 'Episode', name: 'Episode', component: EpisodeSectionComponent, group: groups.basic },
    { path: 'LiveTvMini', name: 'LiveTvMini', component: LiveTvMiniDemoSectionComponent, group: groups.basic },
    { path: 'TimeSelect', name: 'TimeSelect', component: TimeSectionComponent, group: groups.basic },
    { path: 'PlaybillRecord', name: 'PlaybillRecord', component: PlaybillRecordSection, group: groups.basic },
    { path: 'PosterShow', name: 'PosterShow', component: PosterShowSection, group: groups.basic },
    { path: 'VodFilter', name: 'VodFilter', component: VodFilterSection, group: groups.basic },
    { path: 'Reminder', name: 'Reminder', component: ReminderSection, group: groups.basic }
]
