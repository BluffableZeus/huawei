import { Component } from '@angular/core'
let globals = require('../../shared/global.ts')

// webpack html imports
let doc = require('!!html-loader!markdown-loader!ng-epg-ui/webtv-components/reminderDialog/readme-' + globals.LANG +  '.md')
let titleDoc = require('!!html-loader!markdown-loader!ng-epg-ui/webtv-components/reminderDialog/title-' + globals.LANG +  '.md')

let ts = require('!!prismjs-loader?lang=typescript!./reminder-demo.ts')
let html = require('!!prismjs-loader?lang=markup!./reminder-demo.html')

@Component({
  selector: 'reminder-demo-section',
  template: `
   <demo-section [name]="name" [src]="src" [titleDoc]="titleDoc" [html]="html" [ts]="ts" [doc]="doc">
      <app-reminder-demo></app-reminder-demo>
  </demo-section>
  `,
  styles: [`
      vod-filter-demo{
		position: relative;
      }
  `]
})
export class ReminderSection {
  public name = 'Reminder'
  public src = ''
  public html: string = html
  public ts: string = ts
  public titleDoc: string = titleDoc
  public doc: string = doc
}
