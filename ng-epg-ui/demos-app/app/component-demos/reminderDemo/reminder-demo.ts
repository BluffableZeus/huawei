import * as _ from 'underscore'
import { Component, OnInit } from '@angular/core'
import { ReminderDialogComponent } from '../../../../src/webtv-components/reminderDialog'

@Component({
  selector: 'app-reminder-demo',
  templateUrl: './reminder-demo.html',
  styleUrls: ['./reminder-demo.scss']
})

export class ReminderDemo implements OnInit {
  public reminderList = []

  ngOnInit () {
      this.reminderList = [
          {
            'playbillName': 'Belle',
            'season': {
                  'num': '2'
                },
            'episode': {
                  'num': '2'
                },
            'reminderTip': {
                  'time': '15: 55: 0 0',
                  'channelname': 'WDRHD'
                },
            'channelname': 'WDRHD',
            'startTime': '1498550100000',
            'endTime': '1498556460000',
            'reminderTime': '1498550100000',
            'playbill': {
                  'ID': '9341',
                  'channel': {
                      'ID': '33',
                      'SLSType': '',
                      'averageScore': '',
                      'channelNO': '33',
                      'name': 'WDRHD',
                      'locationCopyrights': [],
                      'extensionFields': []
                    },
                  'channelID': '33',
                  'endTime': '1498556460000',
                  'genres': [],
                  'hasRecordingPVR': '0',
                  'isBlackout': '0',
                  'isCPVR': '0',
                  'isCUTV': '1',
                  'isNPVR': '1',
                  'name': 'Belle',
                  'playbillSeries': {
                      'seasonID': '2',
                      'seasonNO': '2',
                      'sitcomNO': '2',
                      'sitcomName': 'Belle'
                    },
                  'rating': {
                      'ID': '1',
                      'code': '2',
                      'name': 'PG10'
                    },
                  'reminder': {
                      'contentID': '9341',
                      'contentType': 'PROGRAM',
                      'endTime': '20170627174100',
                      'profileID': '1',
                      'reminderTime': '1498550100000',
                      'extensionFields': []
                    },
                  'startTime': '1498550100000'
                },
            'isStart': true
          },
          {
            'playbillName': 'Power',
            'season': {
                  'num': '2'
                },
            'episode': {
                  'num': '2'
                },
            'reminderTip': {
                  'time': '16:12:00',
                  'channelname': 'SYFY'
                },
            'channelname': 'SYFY',
            'startTime': '1498551120000',
            'endTime': '1498555380000',
            'reminderTime': '1498551120000',
            'playbill': {
                  'ID': '8485',
                  'channel': {
                      'ID': '30',
                      'SLSType': '',
                      'averageScore': '',
                      'channelNO': '30',
                      'name': 'SYFY',
                      'locationCopyrights': [],
                      'extensionFields': []
                    },
                  'channelID': '30',
                  'endTime': '1498555380000',
                  'genres': [],
                  'hasRecordingPVR': '0',
                  'isBlackout': '0',
                  'isCPVR': '0',
                  'isCUTV': '1',
                  'isNPVR': '1',
                  'name': 'Power',
                  'playbillSeries': {
                      'seasonID': '2',
                      'seasonNO': '2',
                      'sitcomNO': '2',
                      'sitcomName': 'Power'
                    },
                  'rating': {
                      'ID': '1',
                      'code': '2',
                      'name': 'PG10'
                    },
                  'reminder': {
                      'contentID': '8485',
                      'contentType': 'PROGRAM',
                      'endTime': '20170627172300',
                      'profileID': '1',
                      'reminderTime': '1498551120000',
                      'extensionFields': []
                    },
                  'startTime': '1498551120000'
                },
            'isStart': true
          }
        ]
    }
}
