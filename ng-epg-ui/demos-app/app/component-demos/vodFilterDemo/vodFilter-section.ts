import { Component } from '@angular/core'
let globals = require('../../shared/global.ts')

// webpack html imports
let doc = require('!!html-loader!markdown-loader!ng-epg-ui/webtv-components/vodFilter/readme-' + globals.LANG +  '.md')
let titleDoc = require('!!html-loader!markdown-loader!ng-epg-ui/webtv-components/vodFilter/title-' + globals.LANG +  '.md')

let ts = require('!!prismjs-loader?lang=typescript!./vodFilter-demo.ts')
let html = require('!!prismjs-loader?lang=markup!./vodFilter-demo.html')

@Component({
  selector: 'vod-filter-section',
  template: `
   <demo-section [name]="name" [src]="src" [titleDoc]="titleDoc" [html]="html" [ts]="ts" [doc]="doc">
      <vod-filter-demo></vod-filter-demo>
  </demo-section>
  `,
  styles: [`
      vod-filter-demo{
		position: relative;
      }
  `]
})
export class VodFilterSection {
  public name = 'VodFilter'
  public src = ''
  public html: string = html
  public ts: string = ts
  public titleDoc: string = titleDoc
  public doc: string = doc
}
