import * as _ from 'underscore'
import { Component, OnInit } from '@angular/core'
import { VodFilterComponent } from '../../../../src/webtv-components/vodFilter'

@Component({
  selector: 'vod-filter-demo',
  templateUrl: './vodFilter-demo.html',
  styleUrls: ['./vodFilter-demo.scss']
})

export class VodFilterDemo implements OnInit {
  public config = {}

  ngOnInit () {
      this.config = {
          'genres': [
              {
                'genreID': '1901',
                'genreName': 'Action',
                'genreType': '7',
                'extensionFields': []
              },
              {
                'genreID': '1907',
                'genreName': 'Thriller ',
                'genreType': '2',
                'extensionFields': []
              },
              {
                'genreID': '1908',
                'genreName': 'Animation',
                'genreType': '2',
                'extensionFields': []
              },
              {
                'genreID': '1909',
                'genreName': 'War',
                'genreType': '2',
                'extensionFields': []
              },
              {
                'genreID': '1910',
                'genreName': 'Cartoon',
                'genreType': '0',
                'extensionFields': []
              },
              {
                'genreID': '1911',
                'genreName': 'Crime',
                'genreType': '7',
                'extensionFields': []
              },
              {
                'genreID': '1912',
                'genreName': 'Family',
                'genreType': '7',
                'extensionFields': []
              },
              {
                'genreID': '1601',
                'genreName': 'Popular Music ',
                'genreType': '2',
                'extensionFields': []
              },
              {
                'genreID': '1602',
                'genreName': 'Classical Music',
                'genreType': '2',
                'extensionFields': []
              },
              {
                'genreID': '1603',
                'genreName': 'Jazz',
                'genreType': '1',
                'extensionFields': []
              },
              {
                'genreID': '1604',
                'genreName': 'Rock',
                'genreType': '1',
                'extensionFields': []
              },
              {
                'genreID': '1605',
                'genreName': 'Country Music',
                'genreType': '2',
                'extensionFields': []
              },
              {
                'genreID': '1913',
                'genreName': 'Old West',
                'genreType': '7',
                'extensionFields': []
              },
              {
                'genreID': '1914',
                'genreName': 'Sports',
                'genreType': '2',
                'extensionFields': []
              },
              {
                'genreID': '1915',
                'genreName': 'News',
                'genreType': '2',
                'extensionFields': []
              },
              {
                'genreID': '1916',
                'genreName': 'Education',
                'genreType': '0',
                'extensionFields': []
              },
              {
                'genreID': '1917',
                'genreName': 'TV',
                'genreType': '0',
                'extensionFields': []
              },
              {
                'genreID': '1918',
                'genreName': 'Tech',
                'genreType': '0',
                'extensionFields': []
              },
              {
                'genreID': '1919',
                'genreName': 'Ad',
                'genreType': '7',
                'extensionFields': []
              },
              {
                'genreID': '1922',
                'genreName': 'Carousel Card',
                'genreType': '2',
                'extensionFields': []
              },
              {
                'genreID': '1902',
                'genreName': 'Comedy',
                'genreType': '7',
                'extensionFields': []
              },
              {
                'genreID': '1903',
                'genreName': 'Romance',
                'genreType': '7',
                'extensionFields': []
              },
              {
                'genreID': '1904',
                'genreName': 'Adventure',
                'genreType': '0',
                'extensionFields': []
              },
              {
                'genreID': '1905',
                'genreName': 'Tragedy',
                'genreType': '0',
                'extensionFields': []
              },
              {
                'genreID': '1906',
                'genreName': 'Drama',
                'genreType': '2',
                'extensionFields': []
              }
            ],
          'produceZones': [
              {
                'ID': '0',
                'complete': true,
                'name': 'USA'
              },
              {
                'ID': '1',
                'complete': true,
                'name': 'Japan'
              },
              {
                'ID': '3',
                'complete': true,
                'name': 'China'
              },
              {
                'ID': '4',
                'complete': true,
                'name': 'wqeqw'
              },
              {
                'ID': '5',
                'complete': true,
                'name': 'rewr'
              },
              {
                'ID': '6',
                'complete': true,
                'name': 'rew'
              }
            ],
          'result': {
              'retCode': '000000000',
              'retMsg': 'Successfully'
            },
          'extensionFields': []
        }
    }
}
