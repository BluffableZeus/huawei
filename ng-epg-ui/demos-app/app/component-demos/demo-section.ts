import { Component, Input, ChangeDetectionStrategy, ComponentFactoryResolver, ViewChild } from '@angular/core'
import { AdDirective } from './ad.directive'
import * as _ from 'underscore'
@Component({
  selector: 'demo-section',
  templateUrl: './demo-section.html'
})
export class DemoSectionComponent {
  static editComponent: any

  @ViewChild(AdDirective) private adHost: AdDirective

  @Input() public name: string
  @Input() public titleDoc: string
  @Input() public src: string
  @Input() public html: string
  @Input() public ts: string
  @Input() public doc: string
  @Input() public testComponent: any

  public editComponentObj: any

  public keys: Array<string>

  constructor (private _componentFactoryResolver: ComponentFactoryResolver) { }

  ngAfterViewInit () {
      if (this.testComponent) {
          this.loadComponent()
          this.editComponentObj = new this.testComponent()
        }
    }

  loadComponent () {
      let componentFactory = this._componentFactoryResolver.resolveComponentFactory(this.testComponent)
      if (this.adHost) {
          let viewContainerRef = this.adHost.viewContainerRef
          viewContainerRef.clear()
          let componentRef = viewContainerRef.createComponent(componentFactory)
          DemoSectionComponent.editComponent = null
        }
    }

  refresh (val) {
      DemoSectionComponent.editComponent = this.editComponentObj
      this.loadComponent()
    }

  getKeys () {
      return _.keys(this.editComponentObj)
    }

  onChangeValue (key,value) {
      this.editComponentObj[key] = JSON.parse(value.textContent)
    }

}
