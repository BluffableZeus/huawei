import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { IonicModule } from './../../../src/index'
import { NavModule, App } from './../../../src/components/nav'
import { RemoteControllerModule } from './../../../src/components/remote-controller'
import { ChannelSuspensionModule } from './../../../src/webtv-components/channelSuspension/channel-suspension.module'
import { NoDataModule } from './../../../src/webtv-components/noData/no-data.module'
import { EpisodeTrailerModule } from './../../../src/webtv-components/episodeTrailer/episode-trailer.module'
import { liveTvMiniDetailModule } from './../../../src/webtv-components/liveTvMiniDetail/liveTvMini-Detail.module'
import { TimeSelectModule } from './../../../src/webtv-components/timeSelect/time-select.module'
import { PlaybillRecordModule } from './../../../src/webtv-components/playbillRecord/playbill-record.module'
import { ChildColumnShowModule } from './../../../src/webtv-components/childcolumnshow/child-column-show.module'
import { VodFilterModule } from './../../../src/webtv-components/vodFilter/vodFilter.module'
import { ReminderDialogModule } from './../../../src/webtv-components/reminderDialog/reminderDialog.module'

import { SuspensionDemoChannel } from './channelSuspension/channelSuspension-demo'
import { SuspensionSectionComponent } from './channelSuspension/channelSuspension-section'

import { NoDataDemo } from './noData/noData-demo'
import { NoDataSectionComponent } from './noData/noData-section'

import { ScrollDemo } from './scroll/scroll-demo'
import { ScrollSectionComponent } from './scroll/scroll-section'

import { EpisodeTrailerDemo } from './episodeAndTrailer/episodeAndTrailer-demo'
import { EpisodeSectionComponent } from './episodeAndTrailer/episodeAndTrailer-section'

import { LiveTvMiniDemo } from './liveTvMiniDetail/liveTvMiniDetail-demo'
import { LiveTvMiniDemoSectionComponent } from './liveTvMiniDetail/liveTvMiniDetail-section'

import { TimeSelectDemo } from './timeDemo/timeSelect-demo'
import { TimeSectionComponent } from './timeDemo/timeSelect-section'

import { PlaybillRecordDemo } from './playbillRecDemo/playbillRec-demo'
import { PlaybillRecordSection } from './playbillRecDemo/playbillRec-section'

import { PosterShowDemo } from './posterShowDemo/posterShow-demo'
import { PosterShowSection } from './posterShowDemo/posterShow-section'

import { VodFilterDemo } from './vodFilterDemo/vodFilter-demo'
import { VodFilterSection } from './vodFilterDemo/vodFilter-section'

import { ReminderDemo } from './reminderDemo/reminder-demo'
import { ReminderSection } from './reminderDemo/reminder-section'

import { DemosComponent } from './demos.component'
import { DemoSectionComponent } from './demo-section'

import { TabModule } from '../shared/tabs'

import * as _ from 'underscore'

@NgModule({
  imports: [
          IonicModule,
          CommonModule,
          NavModule,
          RemoteControllerModule,
          ChannelSuspensionModule,
          NoDataModule,
          TabModule,
          EpisodeTrailerModule,
          liveTvMiniDetailModule,
          TimeSelectModule,
          PlaybillRecordModule,
          ChildColumnShowModule,
          VodFilterModule,
          ReminderDialogModule
        ],
  declarations: [
          SuspensionDemoChannel,
          SuspensionSectionComponent,
          NoDataDemo,
          NoDataSectionComponent,
          ScrollDemo,
          ScrollSectionComponent,
          DemosComponent,
          DemoSectionComponent,
          EpisodeTrailerDemo,
          EpisodeSectionComponent,
          LiveTvMiniDemo,
          LiveTvMiniDemoSectionComponent,
          TimeSelectDemo,
          TimeSectionComponent,
          PlaybillRecordDemo,
          PlaybillRecordSection,
          PosterShowDemo,
          PosterShowSection,
          VodFilterDemo,
          VodFilterSection,
          ReminderDemo,
          ReminderSection
        ],
  entryComponents: [
          SuspensionDemoChannel,
          SuspensionSectionComponent,
          NoDataDemo,
          NoDataSectionComponent,
          ScrollDemo,
          ScrollSectionComponent,
          EpisodeTrailerDemo,
          EpisodeSectionComponent,
          DemoSectionComponent,
          LiveTvMiniDemo,
          LiveTvMiniDemoSectionComponent,
          TimeSelectDemo,
          TimeSectionComponent,
          PlaybillRecordDemo,
          PlaybillRecordSection,
          PosterShowDemo,
          PosterShowSection,
          VodFilterDemo,
          VodFilterSection,
          ReminderDemo,
          ReminderSection
        ]
})

export class DemosModule {
  constructor (private app: App) {
    }
}
