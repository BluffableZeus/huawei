import * as _ from 'underscore'
import { Component, OnInit } from '@angular/core'
import { TimeSelectComponent } from '../../../../src/webtv-components/timeSelect'

@Component({
  selector: 'time-select-demo',
  templateUrl: './timeSelect-demo.html',
  styleUrls: ['./timeSelect-demo.scss']
})

export class TimeSelectDemo implements OnInit {

  public startHour = ''
  public startMin = ''
  public endHour = ''
  public endMin = ''

  ngOnInit () {
      this.startHour = '10'
      this.startMin = '10'
      this.endHour = '10'
      this.endMin = '10'
    }
}
