import { Component } from '@angular/core'
let globals = require('../../shared/global.ts')

// webpack html imports
let doc = require('!!html-loader!markdown-loader!ng-epg-ui/webtv-components/timeSelect/readme-' + globals.LANG +  '.md')
let titleDoc = require('!!html-loader!markdown-loader!ng-epg-ui/webtv-components/timeSelect/title-' + globals.LANG +  '.md')

let ts = require('!!prismjs-loader?lang=typescript!./timeSelect-demo.ts')
let html = require('!!prismjs-loader?lang=markup!./timeSelect-demo.html')

@Component({
  selector: 'time-select-section',
  template: `
   <demo-section [name]="name" [src]="src" [titleDoc]="titleDoc" [html]="html" [ts]="ts" [doc]="doc">
      <time-select-demo></time-select-demo>
  </demo-section>
  `,
  styles: [`
      time-select-demo{
      }
  `]
})
export class TimeSectionComponent {
  public name = 'TimeSelect'
  public src = ''
  public html: string = html
  public ts: string = ts
  public titleDoc: string = titleDoc
  public doc: string = doc
}
