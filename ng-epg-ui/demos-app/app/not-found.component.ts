import { Component } from '@angular/core'
@Component({
  selector: 'my-not-found',
  template: `
   <div  class="ui-website">
      <h1 class="ui-website-title">UI Components</h1>
      <h2 class="ui-website-subtitle">Angular UI components for PC template</h2>
    </div>
  `
})

export class NotFoundComponent { }
