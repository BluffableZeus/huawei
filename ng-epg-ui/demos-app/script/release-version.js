const moment = require('moment');

module.exports = (type) => {
    const childProcess = require('child_process');
    const fs = require('fs');
    const versionPath = './src/version.json';
    const currentVersion = '' + fs.readFileSync(versionPath);

    try {
        console.log('start change verion....');

        childProcess.execSync('git checkout master');

        var pull = childProcess.execSync('git pull');
        console.log(pull);

        var newVersion = '' + childProcess.execSync(`npm --no-git-tag-version version ${type}`);
        newVersion = newVersion.trim();

        var versionJSON = JSON.parse(currentVersion);
         console.log('versionJSONversionJSONversionJSONversionJSON' + versionJSON);
        const versionDate = moment().format('YYYYMMDDHHmmss');
        versionJSON.push({
            version: newVersion,
            date: versionDate
        });

        if (newVersion) {
            childProcess.execSync(`git checkout -b ${newVersion}`);
        }
        console.log(JSON.stringify(versionJSON));

        fs.writeFileSync(versionPath, JSON.stringify(versionJSON));

        childProcess.execSync('git add ./src/version.json');
        childProcess.execSync('git add package.json');

        childProcess.execSync(`git commit -m "[build] ${newVersion}"`);
        childProcess.execSync(`git tag -a ${newVersion}-${versionDate} -m "${newVersion}-${versionDate}"`);
        childProcess.execSync(`git push origin ${newVersion}-${versionDate}`);
        console.log(`tag:${newVersion}-${versionDate}`);
        childProcess.execSync(`git push origin ${newVersion}`);

        console.log(newVersion);
    } catch (e) {
        childProcess.execSync('git reset --hard');
        console.error(e);
    }

}
