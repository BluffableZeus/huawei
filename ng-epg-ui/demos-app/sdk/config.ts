import * as _ from 'underscore'

export const STORE: { [key: string]: any } = {}

const parseFacadeUrlFromVspUrl = (url: string) => {
  return url.replace(/(http:\/\/.*:)(\d+)/, '$133208').replace(/(https:\/\/.*:)(\d+)/, '$133209')
}

/**
 * Configuration class
 *
 * @export
 * @class Config
 */
export const Config = {

    /**
     *
     */
  get: (key: string, fallbackValue: any = undefined): any => {
      const value = STORE[key]

      if (_.isUndefined(value) || _.isNull(value)) {
          return fallbackValue
        }

      return value
    },

    /**
     *
     */
  set: (key: string, value: any) => {
      STORE[key] = value
      return this
    },

  getBoolean: (key: string, fallbackValue: any = null): boolean => {
      let val = this.get(key)
      if (val === null) {
          return fallbackValue
        }
      if (typeof val === 'string') {
          return val === 'true'
        }
      return !!val
    },

  getNumber: (key: string, fallbackValue: number = NaN): number => {
      let val = parseFloat(this.get(key))
      return isNaN(val) ? fallbackValue : val
    },

    /**
     * vsp http address
     */
  vspUrl: '',

    /**
     * vsp https address
     */
  vspHttpsUrl: '',

    /**
     * User login status flag.
    * Return after successful authentication.
    * Ranges：
    * - 0：Normal login
    * - 1：Log on
    * If the system is configured to support the release, if the database or VSP fails,
    * the user gateway (VSP) server will allow the user to pass the authentication, then the user's login status is the log on.
     *
     * @memberOf ConfigClass
     */
  loginOccasion: false,

    /**
     *
     * Node Server http address
     */
  get facadeUrl (): string {
      return parseFacadeUrlFromVspUrl(this.vspUrl)
    },

    /**
    *
    * Node Server https address
    */
  get facadeHttpsUrl (): string {
      return parseFacadeUrlFromVspUrl(this.vspHttpsUrl)
    },

    /**
     *
     * Encryption
     *
     *
     */
  encryptionType: '0002',

    /**
     * temporary Token。
     * When the client initiates a request for authentication, it needs to generate the challenge word and generate the challenge word.
     */
  encryptToken: '',

    /**
     *
     * Equipment MAC address
     */
  physicalDeviceID: '00:00:00:00:00:00',

    /**
     * Equipment type
     */
  deviceModel: 'Q22',

    /**
     * Equipment ID
     *
     */
  deviceID: '',

  authenticatorMode: 2,

  signatureKeyAlgorithm: '',

    /**
     * Home Page Expiry Time, Unit (ms)
     */
  subjectExpireCircle: 120 * 60 * 1000,

    /**
     * Home VOD Expiry Time, Unit (ms)
     */
  vodExpireCircle: 15 * 60 * 1000,

    /**
     * Whether to support the SQM probe
     * @boolean {true}
     */
  isSupportSQMProbe: true,

    /**
     * Page jump shortcuts
     * BTV_KEY: 1108/81
     * TVOD_KEY: 1184/1110/84
     * MENU_KEY: 272/72
     * GREEN_KEY: 276/83
     * VOD_KEY: 1109/222
     */
  quickKeys: [1108, 81, 1184, 1110, 84, 272, 72, 276, 83, 1109, 222],

    /**
     * After disaster recovery, the following interfaces can not be accessed
     *
     * @type {Array<string>}
     */
  loginOccasionInterfaces: ['AddProfile', 'ModifyProfile', 'DeleteProfile', 'QueryProfile', 'QueryDeviceList', 'ReplaceDevice', 'ModifyDeviceInfo',
      'CheckPassword', 'ModifyPassword', 'ResetPassword', 'SubscribeProduct', 'CancelSubscribe', 'QueryProductsByContent', 'QueryMyContent',
      'QuerySubscription', 'AddFavoCatalog', 'DeleteFavoCatalog', 'UpdateFavoCatalog', 'QueryFavoCatalog', 'CreateFavorite', 'DeleteFavorite',
      'QueryFavorite', 'SortFavorite', 'CreateLock', 'DeleteLock', 'QueryLock', 'DeleteBookmark', 'QueryBookmark', 'CreateContentScore'],
  userFilter: ''

}
