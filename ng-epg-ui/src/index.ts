import {
    ANALYZE_FOR_ENTRY_COMPONENTS, APP_INITIALIZER,
    Inject, ModuleWithProviders, NgModule, NgZone, Optional
} from '@angular/core'
import { APP_BASE_HREF, Location, LocationStrategy, HashLocationStrategy, PathLocationStrategy, PlatformLocation } from '@angular/common'
import { DOCUMENT, BrowserModule } from '@angular/platform-browser'

import { DeepLinkConfig } from './supply/navigation/nav-util'
import { DeepLinker, setupDeepLinker } from './supply/navigation/deep-linker'
import { PlatformConfigToken, providePlatformConfigs } from './supply/platform/platform-registry'
import { DeepLinkConfigToken, UrlSerializer, setupUrlSerializer } from './supply/navigation/url-serializer'
import { Platform, setupPlatform } from './supply/platform/platform'
import { Config, setupConfig, ConfigToken } from './supply/config/config'
import { registerModeConfigs } from './supply/config/mode-registry'
import { TransitionController } from './supply/transitions/transition-controller'
import { registerTransitions } from './supply/transitions/transition-registry'
import { AppRootToken, AppRootComponent } from './components/app/app-root'
import { App } from './components/app/app'
import { NavModule } from './components/nav/nav.module'
import { DomController } from './supply/platform/dom-controller'
import { RemoteControllerModule } from './components/remote-controller'

export * from './supply/navigation'

@NgModule({
  imports: [BrowserModule, NavModule, RemoteControllerModule],
  exports: [
      BrowserModule
    ],
  declarations: [
      AppRootComponent
    ],
  entryComponents: [
      AppRootComponent
    ]
})
export class IonicModule {
    /**
     * Set the root app component for you PageStackModule
     * @param {any} appRoot The root AppComponent for this app.
     * @param {any} config Config Options for the app. Accepts any config property.
     * @param {any} deepLinkConfig Any configuration needed for the Ionic Deeplinker.
     */
  static forRoot (appRoot: any, config: any = null, deepLinkConfig: DeepLinkConfig = null): ModuleWithProviders {
      return {
          ngModule: IonicModule,
          providers: [
                // useValue: bootstrap values
                { provide: AppRootToken, useValue: appRoot },
                { provide: ConfigToken, useValue: config },
                { provide: DeepLinkConfigToken, useValue: deepLinkConfig },

                // useFactory: user values
                { provide: PlatformConfigToken, useFactory: providePlatformConfigs },

                // useFactory: ionic core providers
                { provide: Platform, useFactory: setupPlatform, deps: [DOCUMENT, PlatformConfigToken, NgZone] },
                { provide: Config, useFactory: setupConfig, deps: [ConfigToken, Platform] },

                // useFactory: ionic app initializers
                { provide: APP_INITIALIZER, useFactory: registerTransitions, deps: [Config], multi: true },
                { provide: APP_INITIALIZER, useFactory: registerModeConfigs, deps: [Config], multi: true },

                // useValue
                { provide: ANALYZE_FOR_ENTRY_COMPONENTS, useValue: appRoot, multi: true },

                // ionic providers
              App,
              DomController,

              Location,
              TransitionController,
              {
                provide: LocationStrategy,
                useFactory: provideLocationStrategy, deps: [PlatformLocation, [new Inject(APP_BASE_HREF), new Optional()], Config]
              },
                { provide: UrlSerializer, useFactory: setupUrlSerializer, deps: [DeepLinkConfigToken] },
                { provide: DeepLinker, useFactory: setupDeepLinker, deps: [App, UrlSerializer, Location] }
            ]
        }
    }
}

/**
 * @private
 */
export function provideLocationStrategy (platformLocationStrategy: PlatformLocation, baseHref: string, config: Config) {
  return config.get('locationStrategy') === 'path' ?
        new PathLocationStrategy(platformLocationStrategy, baseHref) :
        new HashLocationStrategy(platformLocationStrategy, baseHref)
}
