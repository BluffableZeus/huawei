import { TranslateModule } from '@ngx-translate/core'
import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'

import { SuspensionComponentChannel } from './channelSuspension.component'
import { CommonSuspensionService } from 'ng-epg-ui/webtv-components/channelSuspension/channelCommon.service'

@NgModule({
  imports: [CommonModule, TranslateModule],
  exports: [SuspensionComponentChannel],
  declarations: [SuspensionComponentChannel],
  providers: [CommonSuspensionService]
})
export class ChannelSuspensionModule { }
