import * as _ from 'underscore'
import { Injectable } from '@angular/core'

@Injectable()
export class CommonSuspensionService {
  constructor () {
    }

  moveEnter (currentTarget, ID, clientWidth, type) {
      let self = this
      _.delay(function () {
          if (document.getElementById(ID)) {
              let width = currentTarget.offsetWidth
              let height = currentTarget.offsetHeight
              let left = 0
              let top = 0

              switch (type) {
                  case 'waterfall':
                    if (ID === 'likemany' || ID === 'moreData' || ID === 'viewalso') {
                          left = currentTarget.offsetLeft
                        } else {
                          left = currentTarget.offsetLeft + currentTarget.offsetParent.offsetParent.offsetLeft
                        }
                    document.getElementById(ID).style.left = `${left}px`
                    document.getElementById(ID).style.top = `${currentTarget.offsetTop}px`
                    break
                  case 'waterfallChannel':
                    height = currentTarget.firstElementChild.offsetHeight
                    left = currentTarget.offsetLeft + currentTarget.offsetParent.offsetParent.offsetLeft
                    document.getElementById(ID).style.left = `${left}px`
                    document.getElementById(ID).style.top = `${currentTarget.offsetTop}px`
                    break
                  case 'LiveTvProgram':
                    left = self.getLeft(clientWidth, currentTarget.offsetParent)
                    document.getElementById(ID).style.left = `${left}px`
                    document.getElementById(ID).style.top = `${currentTarget.offsetParent.offsetParent.offsetParent.offsetTop}px`
                    break
                  case 'myChannel':
                    left = currentTarget.offsetLeft
                    document.getElementById(ID).style.left = `${left}px`
                    document.getElementById(ID).style.top = `${currentTarget.offsetTop}px`
                    break
                  default:
                    document.getElementById(ID).style.left = `${left + 60.5}px`
                    document.getElementById(ID).style.top = `${currentTarget.offsetTop + currentTarget.offsetParent.offsetTop}px`
                    break
                }
              document.getElementById(ID).style.width = 1.1 * width + 'px'
              document.getElementById(ID).style.height = 1.1 * height + 'px'
              document.getElementById(ID).children[0]['style']['height'] = 1.1 * height + 'px'
              document.getElementById(ID).children[1]['style']['height'] = 1.1 * height + 'px'
              if (document.querySelector('#' + ID + ' .title')) {
                  document.querySelector('#' + ID + ' .title')['style']['width'] = 1.1 * width - 50 + 'px'
                }
              if (document.querySelector('#' + ID + ' .produceZoneDetail')) {
                  document.querySelector('#' + ID + ' .produceZoneDetail')['style']['max-width'] = 1.1 * width - 50 + 'px'
                }
              self.showContentLine(currentTarget.firstElementChild, ID)
            }
        }, 100)
    }
    /**
     * show Content Line combined with funtion 'showContent()'
     */
  showContentLine (realCurrentTarget, ID) {
      let ua = navigator.userAgent
      if (!document.querySelector('#' + ID + ' .contents')) {
          return
        }
      let Style = document.querySelector('#' + ID + ' .contents')['style']
            // while small screen
      if (window.innerWidth <= 1440) {
              if (realCurrentTarget.offsetHeight <= 200) {
                  if (ID === 'suspensionHotTv') {
                      Style['-webkit-line-clamp'] = '1'
                      Style['display'] = '-webkit-box'
                      Style['max-height'] = '20px'
                    } else {
                      Style['display'] = 'none'
                    }

                } else if (realCurrentTarget.offsetHeight <= 240) {
                  Style['display'] = 'none'

                } else {
                  Style['-webkit-line-clamp'] = '10'
                  Style['display'] = '-webkit-box'
                  Style['max-height'] = '200px'
                }
                // while large screen
            } else {
              Style['display'] = '-webkit-box'
              if (realCurrentTarget.offsetHeight <= 243) {
                  if (ID === 'suspensionHotGenres') {
                      Style['display'] = 'none'
                    } else if (ID === 'suspensionHotTv') {
                      Style['-webkit-line-clamp'] = '1'
                      Style['max-height'] = '20px'
                    } else {
                      Style['-webkit-line-clamp'] = '2'
                      Style['max-height'] = '40px'
                    }
                } else {
                  if (ID === 'likemany' || ID === 'moreData' || ID === 'viewalso' || ID === 'filterVod' || ID === 'oscarAllContent' || ID === 'noData') {
                      Style['-webkit-line-clamp'] = '1'
                      Style['max-height'] = '20px'
                    } else {
                      Style['-webkit-line-clamp'] = '11'
                      Style['max-height'] = '220px'
                    }
                }
            }

    }

  getLeft (clientWidth, currentTarget) {
      let left = 0
      if (clientWidth <= 1440) {
          left = currentTarget.offsetLeft % 999
        } else {
          left = currentTarget.offsetLeft % 1400
        }
      return left
    }

}
