### Usage
```typescript
import { SuspensionComponentChannel } from 'ng-epg-ui/webtv-components/channelSuspension';

<suspensionchannel></suspensionchannel>

```

### Annotations
```typescript
export class SuspensionComponentChannel {
}

```

 ### Attributes
| Attribute   | Description   | Type     | Options     | Default Value    | Mandatory (M) or Optional (O)    |
|-----   |-----    |-----    |------      |------    |------    |
| null | null  | null | null | null |  0 |
