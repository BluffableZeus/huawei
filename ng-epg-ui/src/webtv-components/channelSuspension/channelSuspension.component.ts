import * as _ from 'underscore'
import { Component, OnInit } from '@angular/core'
import { TranslateService } from '@ngx-translate/core'
import { EventService } from '../../../demos-app/sdk/event.service'
import { session } from '../../../demos-app/sdk/session'
import { CommonSuspensionService } from 'ng-epg-ui/webtv-components/channelSuspension/channelCommon.service'

@Component({
  selector: 'suspensionchannel',
  templateUrl: './channelSuspension.component.html',
  styleUrls: ['./channelSuspension.component.scss']
})

export class SuspensionComponentChannel implements OnInit {
  public suspensionInfo = []
  public hotShow: string = 'false'
  public leftStyle = {}
  public leftImgStyle = {}
  public rightImgStyle = {}
  public recordsImgStyle = {}
  public leftTop = ''
  public rightTop = ''
  public leftTime = {}
  public titleName: string = ''
  public showTime: string = ''
  public channel: string = ''
  public introduce: string = ''
  public btnNamePlay: string = ''
  public btnNameRecord: string = ''
  public flagInfo: string = ''
  public definition: string = ''
  public ratingName: string = ''
  public redLen: string = ''
  public series: string = ''
  public channelNoData: boolean
  private channelID = ''
  public allowRecord: boolean = false
  private physicalChannels
  private filterCpvrChannel
  private filterNpvrChannel
  private playbillDetailInfo
  private introduceEllipsis: boolean = false
  private isSearchRecord
  private adjustTime
  private searchAll = true
  private smallest = true
  private suspensionHide = true

  constructor (
        private translate: TranslateService,
        private CommonSuspensionService: CommonSuspensionService
    ) {

    }

  ngOnInit () {
      let self = this
      EventService.on('SUSPENSION_HIDE',(data) => {
          self.suspensionHide = data
        })
    }

  setDetail (vodDetails, ID) {
      let self = this
      self.searchAll =  (ID === 'suspensionSearchChannel') ? true : false
      self.smallest = (ID === 'suspensionChannel') ? true : false
      clearTimeout(self.adjustTime)
      EventService.emit('HIDESEARCH')
        // if the current playbii is not exit in the searchAll
      if (self.searchAll) {
          if (!vodDetails.floatInfo.currentPlaybill || vodDetails.floatInfo.currentPlaybill.isFillProgram === '1' ||
            vodDetails.floatInfo.currentPlaybill.name === '') {
              self.channelNoData = true
              EventService.emit('LOADDATACHANNELSUCCESS')
              return
            } else {
              self.channelNoData = false
            }
            // while searchAll, we apply with GetPlaybillDetail
          EventService.emit('GET_CHANNELDETAIL', vodDetails.floatInfo.currentPlaybill.ID)
        } else {
            // this data was defined for transfer sceneID
          let sendData = {
              id: vodDetails.id,
              sceneID: vodDetails.sceneID
            }
            // while LOCK or FAV, we apply with QueryPlaybill
          EventService.emit('GET_QUERYPLAYBILL', sendData)
        }
      EventService.removeAllListeners(['SET_SEARCH_CHANNELDETAIL'])
      EventService.on('SET_SEARCH_CHANNELDETAIL', (detail) => {
          if (detail['playbillDetail']['ID'] === vodDetails.floatInfo.currentPlaybill.ID) {
              vodDetails['playbillDetail'] = detail['playbillDetail']
              let vodDetail = detail
              let channelDetail = vodDetails['playbillDetail']['channelDetail'] ? vodDetails['playbillDetail']['channelDetail'] : ''
              this.playbillDetailInfo = vodDetails['playbillDetail']
              if (vodDetails.playbillDetail) {
                  this.titleName = vodDetails['playbillDetail'].name
                }
              let staticChannelData = this.getStaticChannelData().channelDetails
              if (vodDetails['playbillDetail'] && vodDetails['playbillDetail']['channelDetail']) {
                  this.channelID = vodDetails['channelId']
                  let meanData = _.find(staticChannelData, (item) => {
                      return item['ID'] === vodDetails['playbillDetail'].channelDetail.ID
                    })
                  let channelDefinition = meanData['physicalChannels'][0].definition
                  this.definition = this.definitionFormat(channelDefinition)
                }
              this.showTime = vodDetail.startTime + '-' + vodDetail.endTime
              this.leftTime = { 'min': vodDetail.leftTime }
              this.channel = vodDetail.channel
              this.ratingName = vodDetail.rating
              this.introduce = vodDetail.introduce
              this.redLen = vodDetail.progress + 'px'
              this.series = vodDetail.Series ? this.translate.instant('vod_season', { num: vodDetail.Series.seasonNO }) + ', ' +
                    this.translate.instant('episode', { num: vodDetail.Series.sitcomNO }) + ' ' + ' | ' : ''
              if (document.getElementById('contentChannelBg')) {
                  document.getElementById('contentChannelBg')['innerHTML'] = vodDetail.introduce
                }
              self.showWidth()
              EventService.emit('LOADDATACHANNELSUCCESS')
              this.btnNamePlay = 'watch_now'
              let type = this.channelIsRecord(channelDetail, vodDetails['playbillDetail'])
              if (type) {
                  this.allowRecord = true
                  this.btnNameRecord = type
                  if (this.btnNameRecord === 'stop_recording') {
                      this.recordsImgStyle = {
                          background: 'url(assets/img/14/stoprecording_14.png)'
                        }
                    } else if (this.btnNameRecord === 'livetv_record') {
                      this.recordsImgStyle = {
                          background: 'url(assets/img/14/record_14.png)'
                        }
                    }
                } else {
                  this.allowRecord = false
                }
            }
        })

      EventService.removeAllListeners(['SET_CHANNELDETAIL'])
      EventService.on('SET_CHANNELDETAIL', (detail) => {
            // if the current playbii is not exit.
          if (!detail || detail['channelPlaybills'][0]['playbillLites'][0].isFillProgram === '1' ||
            detail['channelPlaybills'][0]['playbillLites'][0].name === '') {
              self.channelNoData = true
              EventService.emit('LOADDATACHANNELSUCCESS')
              return
            } else {
              self.channelNoData = false
            }

          if (detail['channelPlaybills'][0]['channelInfos']['ID'] === vodDetails.id) {
              let vodDetail = detail['channelPlaybills'][0]['playbillLites'][0]
              let channelDetail = detail['channelPlaybills'][0]['channelInfos'] ? detail['channelPlaybills'][0]['channelInfos'] : ''
              this.playbillDetailInfo = vodDetail
              this.playbillDetailInfo['channelDetail'] = detail['channelPlaybills'][0]['channelInfos']
              if (vodDetail) {
                  this.titleName = vodDetail.name
                }
              let staticChannelData = this.getStaticChannelData().channelDetails
              if (vodDetail) {
                  this.channelID = vodDetails.id
                  let meanData = _.find(staticChannelData, (item) => {
                      return item['ID'] === channelDetail.ID
                    })
                  let channelDefinition = meanData['physicalChannels'][0].definition
                  this.definition = this.definitionFormat(channelDefinition)
                }
              this.showTime = vodDetail.startTime + '-' + vodDetail.endTime
              this.leftTime = {
                  'min': vodDetail.leftTime
                }
              this.channel = vodDetail.channel
              this.ratingName = vodDetail.rating
              this.introduce = channelDetail.introduce
              this.redLen = this.formatProgress(vodDetail.startTime, vodDetail.endTime) + 'px'
              this.series = vodDetail.playbillSeries ? this.translate.instant('vod_season', { num: vodDetail.playbillSeries.seasonNO }) + ', ' +
                    this.translate.instant('episode', { num: vodDetail.playbillSeries.sitcomNO }) + ' ' + ' | ' : ''
              if (document.getElementById('contentChannelBg')) {
                  document.getElementById('contentChannelBg')['innerHTML'] = channelDetail.introduce
                }
              self.showWidth()
              EventService.emit('LOADDATACHANNELSUCCESS')

              this.btnNamePlay = 'watch_now'
              let type = this.channelIsRecord(channelDetail, vodDetail)
              if (type) {
                  this.allowRecord = true
                  this.btnNameRecord = type
                  if (this.btnNameRecord === 'stop_recording') {
                      this.recordsImgStyle = {
                          background: 'url(assets/img/14/stoprecording_14.png)'
                        }
                    } else if (this.btnNameRecord === 'livetv_record') {
                      this.recordsImgStyle = {
                          background: 'url(assets/img/14/record_14.png)'
                        }
                    }
                } else {
                  this.allowRecord = false
                }

            }
        })
    }
    // according to the attribute of physical channel to judge that the channel supports CPVR or NPVR.
  judgeRecord (recordCR, channel): any {
      let channels = _.filter(channel['physicalChannelsDynamicProperties'], function (physicalChannels) {
          let recordCRType = physicalChannels[recordCR]
          let btvCR = physicalChannels['btvCR']
          if (recordCRType && recordCRType['enable'] === '1' && recordCRType['isSubscribed'] === '1'
                && recordCRType['isContentValid'] === '1' && recordCRType['isValid'] === '1'
                && btvCR && btvCR['enable'] === '1' && btvCR['isSubscribed'] === '1'
                && btvCR['isContentValid'] === '1' && btvCR['isValid'] === '1') {
              return recordCRType
            }
        })
      return channels
    }
    // the judge conditions that the programme supports CPVR or NPVR.
  isCPVROrNPVR (playbill, type) {
      if (playbill[type] === '1') {
          if (playbill.hasRecordingPVR && playbill.hasRecordingPVR !== '0') {
              let currentTime = parseInt(Date.now()['getTime'](), 10)
              if (currentTime > playbill['startTime'] && currentTime < playbill['endTime']) {
                  return 'stop_recording'
                } else {
                  return 'cancel_record'
                }
            }
          return 'livetv_record'
        }
    }

    /**
     * the recording confitions.
     */
  channelIsRecord (channel, playbill): any {
      let userType
      if (Cookies.getJSON('IS_PROFILE_LOGIN')) {
          userType = Cookies.getJSON('USER_ID')
        } else {
          userType = session.get('Guest') || 'Guest'
        }
      let dynamicData = session.get(userType + '_Dynamic_Version_Data')
      _.each(dynamicData['channelDynamaicProp'], (list1, index) => {
          if (list1['ID'] === channel['ID']) {
              channel['physicalChannelsDynamicProperties'] = list1['physicalChannelsDynamicProperties']
            }
        })
      this.physicalChannels = channel['physicalChannelsDynamicProperties']
      if (channel['physicalChannelsDynamicProperties'] && session.get('EPGCONFIG') === 'NPVR') {
          this.filterNpvrChannel = this.judgeRecord('npvrRecordCR', channel)

          if (this.filterNpvrChannel.length > 0) {
              return this.isCPVROrNPVR(playbill, 'isNPVR')
            }
          return false
        } else if (channel['physicalChannelsDynamicProperties'] && session.get('EPGCONFIG') === 'CPVR') {
          this.filterCpvrChannel = this.judgeRecord('cpvrRecordCR', channel)

          if (this.filterCpvrChannel.length > 0) {
              return this.isCPVROrNPVR(playbill, 'isCPVR')
            }
          return false
        } else if (channel['physicalChannelsDynamicProperties'] && session.get('EPGCONFIG') === 'CPVR&NPVR') {
          this.filterNpvrChannel = this.judgeRecord('npvrRecordCR', channel)
          if ((playbill['isNPVR'] === '1') && this.filterNpvrChannel.length > 0) {
              return this.isCPVROrNPVR(playbill, 'isNPVR')
            }
          this.filterCpvrChannel = this.judgeRecord('cpvrRecordCR', channel)
          if ((playbill['isCPVR'] === '1') && this.filterCpvrChannel.length > 0) {
              return this.isCPVROrNPVR(playbill, 'isCPVR')
            }
          return false
        } else {
          return false
        }
    }
    // when the browser is IE and the length of content is too long,add ellipsis at the last line.
  showWidth () {
      let ua = navigator.userAgent
      if (ua.indexOf('Chrome') !== -1 && ua.indexOf('Edge') === -1) {
          return
        }
      if (document.getElementById('contentChannelBg')) {
          let contentBg = document.getElementById('contentChannelBg')
          if (100 < contentBg['offsetHeight']) {
              this.introduceEllipsis = true
            } else {
              this.introduceEllipsis = false
            }
        }
    }
    // when the browser is Chrome and the length of content is too long,set the style of ellipsis.
  showContent () {
      let ua = navigator.userAgent
      if (ua.indexOf('Chrome') !== -1 && ua.indexOf('Edge') === -1) {
          let styles = document.querySelector('.suspensionBig .content')['style']['-webkit-box-direction']
          let style = {
            }
          if (styles === 'reverse') {
              style['-webkit-box-direction'] = 'normal'
            } else {
              style['-webkit-box-direction'] = 'reverse'
            }
          return {
              'max-height': '220px',
              '-webkit-line-clamp': '11',
              'word-break': 'normal',
              '-webkit-box-direction': style['-webkit-box-direction']
            }
        }
    }
    // how to play
  play () {
      if (this.searchAll) {
          EventService.emit('PLAYCHANNEL', this.channelID)
        } else {
          EventService.emit('PLAY_MYCHANNEL', this.channelID)
        }
    }
  goToRecord (type) {
      let self = this
      if (Cookies.getJSON('IS_GUEST_LOGIN')) {
          EventService.emit('UILOGIN')
          return
        }
      if (type === 'livetv_record') {
          EventService.emit('LIVETV_RECORD', this.playbillDetailInfo)
          EventService.on('RECORD_ADD_SUCCESS', function (types) {
              self.btnNameRecord = 'stop_recording'
              self.recordsImgStyle = {
                  background: 'url(assets/img/14/stoprecording_14.png)'
                }
            })
        } else {
          let cancelRecordInfo = [this.playbillDetailInfo['ID'], type]
          EventService.emit('LIVETV_CENCEL', cancelRecordInfo)
          EventService.on('LIVETV_CENCELRECORD', () => {
              self.btnNameRecord = 'livetv_record'
              self.recordsImgStyle = {
                  background: 'url(assets/img/14/record_14.png)'
                }
            })
        }
    }

  definitionFormat (definition) {
      switch (definition) {
          case '0':
            return 'SD'
          case '1':
            return 'HD'
          case '2':
            return ''
          default:
            return ''
        }
    }

  mouseenter (onSuspension) {
      EventService.emit('showChannelSuspension')
    }

  mouseleave () {
      EventService.emit('closeChannelSuspension')
    }

  getStaticChannelData () {
      if (Cookies.getJSON('IS_PROFILE_LOGIN')) {
          return session.get(Cookies.getJSON('USER_ID') + '_Static_Version_Data')
        } else if (Cookies.getJSON('IS_GUEST_LOGIN')) {
          return session.get('Guest' + '_Static_Version_Data')
        }

    }
  formatProgress (start, end) {
      let date = Date.now()
      let nowTime = date['getTime']()
      let progress
      if (window.innerWidth <= 1440) {
          progress = (nowTime - start) / (end - start) * 153
        } else {
          progress = (nowTime - start) / (end - start) * 186
        }
      return Math.round(Number(progress))
    }
}
