import * as _ from 'underscore'
import { Component } from '@angular/core'
import { TranslateService } from '@ngx-translate/core'
import { ProgramSuspensionService } from './programSuspension.service'
import { EventService } from '../../../demos-app/sdk/event.service'
import { DateUtils } from '../../../demos-app/sdk/date-utils'
import { session } from '../../../demos-app/sdk/session'
import { CommonSuspensionService } from 'ng-epg-ui/webtv-components/channelSuspension/channelCommon.service'
@Component({
  selector: 'suspensionprogram',
  templateUrl: './programSuspension.component.html',
  styleUrls: ['./programSuspension.component.scss']
})

export class SuspensionComponentProgram {
  public hotShow: string = 'false'
  public leftImgStyle = {}
  public rightImgStyle = {}
  public recordsImgStyle = {}
  public remindImgStyle = {}
  public leftTop = ''
  public rightTop = ''
  public titleName: string = ''
  public weekTime: string = ''
  public showTime: string = ''
  public showClock: string = ''
  public channel: string = ''
  public introduce: string = ''
  public arrayDef: Array<Object> = []
  public btnNamePlay: string = ''
  public btnNameRecord: string = ''
  public btnNameWatch: string = ''
  public btnNameRemind: string = ''
  public isPass: boolean = false
  public isFuture: boolean = false
  public isNow: boolean = false
  public flagInfo: string = ''
  public floatDetails
  public allowRecord: boolean = false
  public cancelRecord: boolean = false
  public isSearch: boolean = false
  public isAllowReminer: boolean = false
  public introduceEllipsis: boolean = false
  public canTvod: boolean = false
  public adjustTime
  public buttonssTop
  public pocketSize: boolean = false
  public searchAll: boolean = false
  public liveProgramTop: boolean = false
  public suspensionInfo: boolean = false
  public reminderInfo: boolean = false
  public suspensionHotGenres: boolean = false
  public isFirefox: boolean = false
  public smallScreen: boolean = false

  setDetail (vodDetails, ID, realCurrentTarget) {
      let self = this
      clearTimeout(self.adjustTime)
      this.channel = ''
      this.showTime = ''
      this.showClock = ''
      this.flagInfo = ''
      this.introduce = ''
      EventService.emit('HIDESEARCH')
      this.floatDetails = vodDetails
      if (vodDetails['isSearch']) {
          this.isSearch = vodDetails['isSearch']
        }
      self.smallScreen = (window.innerWidth <= 1440) ? true : false
      self.buttonssTop = (1.1 * realCurrentTarget.offsetHeight - 80) / 2 + 'px'
      self.liveProgramTop = (ID === 'liveProgramTop') ? true : false
      self.suspensionHotGenres = (ID === 'suspensionHotGenres') ? true : false
      self.suspensionInfo = (ID === 'suspensionInfo') ? true : false
      self.reminderInfo = (ID === 'reminderInfo') ? true : false
      self.searchAll = (ID === 'suspensionSearchProgram') ? true : false
      self.pocketSize = (realCurrentTarget && realCurrentTarget.offsetHeight <= 146) ? true : false
      let ua = navigator.userAgent
      self.isFirefox = ua.indexOf('Firefox/52') === -1 ? false : true
      let vodDetail = vodDetails.floatInfo
      let sendData = {
          playbillID: vodDetail.ID
        }
      if (self.reminderInfo) {
          sendData['sceneID'] = 'playbilldetail6'
        } else {
          sendData['sceneID'] = 'playbilldetail1'
        }
      EventService.emit('GET_PLAYBILLDETAIL', sendData)
      EventService.removeAllListeners(['SET_PLAYBILLDETAIL'])
      EventService.on('SET_PLAYBILLDETAIL', (detail) => {
          if (detail['playbillDetail']['ID'] === vodDetail.ID) {
              vodDetail['introduce'] = detail['playbillDetail']['introduce']
              vodDetail['channel'] = detail['playbillDetail']['channelDetail'] ? detail['playbillDetail']['channelDetail'] : ''
              vodDetails['playbillInfo'] = detail['playbillDetail']
              vodDetail['isCUTV'] = detail['playbillDetail']['isCUTV']
              this.titleName = vodDetail.name
              this.channel = vodDetails.channelName || vodDetails.channel || ''
              this.flagInfo = vodDetails.floatflag
              this.introduce = vodDetail.introduce
              if (document.getElementById('contentPlaybillBg')) {
                  document.getElementById('contentPlaybillBg')['innerHTML'] = vodDetail.introduce
                }
              EventService.emit('LOADDATAPROGRAMSUCCESS')

              if (vodDetail.endTime < Date.now()['getTime']()) {
                  this.isPass = true
                  this.isFuture = false
                  this.isNow = false
                  let btuResult = this.getOTTMediaData(vodDetail)
                  if (btuResult === 'buy') {
                      this.btnNamePlay = 'buy'
                      this.canTvod = true
                    } else if (btuResult === 'play') {
                      this.btnNamePlay = 'play'
                      this.canTvod = true
                    } else {
                      this.canTvod = false
                    }
                  this.weekTime = DateUtils.format(vodDetail.startTime, 'ddd')
                  let month = DateUtils.format(vodDetail.startTime, 'MM')
                  let day = DateUtils.format(vodDetail.startTime, 'D')
                  let year = DateUtils.format(vodDetail.startTime, 'YYYY')
                  let lastmonth = this.suspensionFac.dealWithTime(month)
                  let lastTime = this.suspensionFac.dealWithDateJson('D05', vodDetail.startTime)
                  let nowDay = DateUtils.format(parseInt(Date.now()['getTime'](), 10), 'D')
                  let nowMonth = DateUtils.format(parseInt(Date.now()['getTime'](), 10), 'MM')
                  let nowYear = DateUtils.format(parseInt(Date.now()['getTime'](), 10), 'YYYY')
                  if (nowDay === day && nowMonth === month && nowYear === year) {
                      lastTime = this.translate.instant('today')
                      this.weekTime = ''
                    }
                  this.showTime = lastTime + ' |'
                  this.showClock = this.suspensionFac.dealWithDateJson('D10', vodDetail.startTime) +
                     '-' + this.suspensionFac.dealWithDateJson('D10', vodDetail.endTime)
                }
              if (vodDetail.endTime > Date.now()['getTime']() && vodDetail.startTime < Date.now()['getTime']()) {
                  if (this.isSearch) {
                      this.btnNameWatch = 'play'
                    } else {
                      this.btnNameWatch = 'watch_now'
                    }
                  let minLeft = this.translate.instant('playbill_format_time', { min: vodDetails.leftTime })
                  this.weekTime = ''
                  this.showTime = minLeft + ' |'
                  this.showClock = this.suspensionFac.dealWithDateJson('D10', vodDetail.startTime) +
                     '-' + this.suspensionFac.dealWithDateJson('D10', vodDetail.endTime)
                  this.isPass = false
                  this.isFuture = false
                  this.isNow = true
                  let type = this.suspensionFac.channelIsRecord(vodDetail['channel'], vodDetails['playbillInfo'])
                  if (type) {
                      this.allowRecord = true
                      this.btnNameRecord = type
                      this.showRecord(this.btnNameRecord)
                    } else {
                      this.allowRecord = false
                    }
                }
              if (vodDetail.startTime > Date.now()['getTime']()) {
                  let reminderState = this.suspensionFac.isSupportReminder(vodDetails['playbillInfo'])
                  if (reminderState) {
                      this.isAllowReminer = true
                      if (reminderState === 'ADD') {
                          this.btnNameRemind = 'set_reminder'
                          this.remindImgStyle = {
                              background: 'url(assets/img/14/reminder_14.png)'
                            }
                        } else {
                          this.btnNameRemind = 'cancel_reminder'
                          this.remindImgStyle = {
                              background: 'url(assets/img/14/cancelreminder_14.png)'
                            }
                        }
                    } else {
                      this.isAllowReminer = false
                    }
                  this.weekTime = DateUtils.format(vodDetail.startTime, 'ddd')
                  let month = DateUtils.format(vodDetail.startTime, 'MM')
                  let day = DateUtils.format(vodDetail.startTime, 'D')
                  let year = DateUtils.format(vodDetail.startTime, 'YYYY')
                  let lastmonth = this.suspensionFac.dealWithTime(month)
                  let lastTime = this.suspensionFac.dealWithDateJson('D05', vodDetail.startTime)
                  let nowDay = DateUtils.format(parseInt(Date.now()['getTime'](), 10), 'D')
                  let nowMonth = DateUtils.format(parseInt(Date.now()['getTime'](), 10), 'MM')
                  let nowYear = DateUtils.format(parseInt(Date.now()['getTime'](), 10), 'YYYY')
                  if (nowDay === day && nowMonth === month && nowYear === year) {
                      this.weekTime = ''
                      lastTime = this.translate.instant('today')
                    }
                  this.showTime = lastTime + ' |'
                  this.showClock = this.suspensionFac.dealWithDateJson('D10', vodDetail.startTime) +
                     '-' + this.suspensionFac.dealWithDateJson('D10', vodDetail.endTime)
                  this.isPass = false
                  this.isFuture = true
                  this.isNow = false
                  let type = this.suspensionFac.channelIsRecord(vodDetail['channel'], vodDetails['playbillInfo'])
                  if (type) {
                      this.allowRecord = true
                      this.btnNameRecord = type
                      this.showRecord(this.btnNameRecord)
                    } else {
                      this.allowRecord = false
                    }
                }

              _.delay(() => {
                  self.showWidth()
                }, 150)

            }
        })
    }

  constructor (
        private translate: TranslateService,
        private suspensionFac: ProgramSuspensionService,
        private CommonSuspensionService: CommonSuspensionService
    ) {
    }

  ButtonStyle () {
      if (!(this.pocketSize && this.liveProgramTop) && !(this.suspensionInfo && (this.isFuture || this.isNow)) && !this.reminderInfo) {
          return
        } else {
          return { 'margin-top': this.buttonssTop }
        }
    }
  showRecord (type) {
      if (type === 'cancel_record' || type === 'stop_recording') {
          this.recordsImgStyle = {
              background: 'url(assets/img/14/stoprecording_14.png)'
            }
          return
        }
      if (type === 'livetv_record') {
          this.recordsImgStyle = {
              background: 'url(assets/img/14/record_14.png)'
            }
          return
        }
    }
    // IE browser, the content exceeding, add ellipsis in last line
  showWidth () {
      let ua = navigator.userAgent
      if (ua.indexOf('Chrome') !== -1 && ua.indexOf('Edge') === -1) {
          return
        }
      if (document.getElementById('contentPlaybillBg')) {
          let contentBg = document.getElementById('contentPlaybillBg')
          if (100 < contentBg['offsetHeight']) {
              this.introduceEllipsis = true
            } else {
              this.introduceEllipsis = false
            }
        }
    }
    /**
     * An independent style function combined with showcontentline in commonservice
     * chrome browser, the content exceeding, add ellipsis in last line
     */
  showContent () {
      let ua = navigator.userAgent
      if (ua.indexOf('Chrome') !== -1 && ua.indexOf('Edge') === -1) {
          let styles = (document.querySelector('.suspensionProgram .contents') || document.querySelector('.suspensionProgram .contentsTvod'))['style']['-webkit-box-direction']
          let style = {}
          if (styles === 'reverse') {
              style['-webkit-box-direction'] = 'normal'
            } else {
              style['-webkit-box-direction'] = 'reverse'
            }
          return{
              'max-height': '220px',
              'word-break': 'normal',
              '-webkit-box-direction': style['-webkit-box-direction']
            }
        }
    }

  play (type) {
      let channelId = this.floatDetails['floatInfo'].channelId || this.floatDetails['channelId'] ||
                this.floatDetails['floatInfo'].channelID || this.floatDetails['floatInfo'].channel.ID
      if (type === 'watch_now') {
          if (this.floatDetails.contentType) {
              EventService.emit('PLAYCHANNEL', channelId)
            } else {
              EventService.emit('PLAYLIVETV', this.floatDetails)
            }
        } else if ((type === 'play' || type === 'buy') && !this.isSearch) {
          if (type === 'buy') {
              session.put('NEED_TO_BUY', 'buy')
            } else {
              session.put('NEED_TO_BUY', 'play')
            }
          EventService.emit('PLAYTVOD', this.floatDetails)
        } else if ((type === 'play' || type === 'buy') && this.isSearch) {
          if (type === 'buy') {
              session.put('NEED_TO_BUY', 'buy')
            } else {
              session.put('NEED_TO_BUY', 'play')
            }
          if (this.floatDetails.contentType && this.floatDetails.contentType !== '') {
              EventService.emit('PLAYCHANNEL', channelId)
            } else {
              EventService.emit('PLAYLIVETV', this.floatDetails)
            }
        }
    }
  goToRecord (type) {
      let self = this
      if (Cookies.getJSON('IS_GUEST_LOGIN')) {
          EventService.emit('UILOGIN')
          return
        }
      if (type === 'livetv_record') {
          this.floatDetails['playbillInfo']['isNow'] = this.isNow
          EventService.emit('LIVETV_RECORD', this.floatDetails['playbillInfo'])
          EventService.on('RECORD_ADD_SUCCESS', function () {
              if (self.isNow === true) {
              self.btnNameRecord = 'stop_recording'
              self.recordsImgStyle = {
                  background: 'url(assets/img/14/stoprecording_14.png)'
                }
              return
            }
              if (self.isFuture === true) {
              self.btnNameRecord = 'cancel_record'
              self.recordsImgStyle = {
                  background: 'url(assets/img/14/stoprecording_14.png)'
                }
              return
            }
            })
        } else {
          let cancelRecordInfo = [this.floatDetails['playbillInfo']['ID'], type]
          EventService.emit('LIVETV_CENCEL', cancelRecordInfo)
          EventService.on('LIVETV_CENCELRECORD', () => {
              self.btnNameRecord = 'livetv_record'
              self.recordsImgStyle = {
                  background: 'url(assets/img/14/record_14.png)'
                }
            })
        }
    }
  goToRemind (type) {
      let self = this
      if (Cookies.getJSON('IS_GUEST_LOGIN')) {
          EventService.emit('UILOGIN')
          return
        }
      if (type === 'set_reminder') {
          let req = {
              contentID: this.floatDetails['playbillInfo']['ID'],
              contentType: 'PROGRAM'
            }
          EventService.emit('ADD_SUSPENSIONREMINDER', req, this.floatDetails)
          EventService.on('ADD_REMINDER', function (data) {
              self.btnNameRemind = 'cancel_reminder'
              self.remindImgStyle = {
                  background: 'url(assets/img/14/cancelreminder_14.png)'
                }
            })
          return
        }
      if (type === 'cancel_reminder') {
          let req = {
              contentID: [this.floatDetails['playbillInfo']['ID']],
              contentType: ['PROGRAM']
            }
          EventService.removeAllListeners(['REMOVE_REMINDER_BY_SUSPENSION'])
          EventService.on('REMOVE_REMINDER_BY_SUSPENSION', function () {
              self.btnNameRemind = 'set_reminder'
              self.remindImgStyle = {
                  background: 'url(assets/img/14/reminder_14.png)'
                }
              if (self.floatDetails['dateType']) {
                  EventService.emit('CANCEL_REMINDER_BY_FLOAT', self.floatDetails)
                }
            })
          EventService.emit('CANCEL_SUSPENSIONREMINDER', req, this.floatDetails)
          return
        }
    }
  mouseenter () {
      EventService.emit('showProgramSuspension', this.flagInfo)
    }

  mouseleave () {
      EventService.emit('closeProgramSuspension')
    }

  getOTTMediaData (resp) {
      let mediaData
      let nameSpace = session.get('ott_channel_name_space')
      if (resp['channel'] && resp['channel']['physicalChannels']) {
          mediaData = _.find(resp['channel']['physicalChannels'], (property) => {
              return _.contains(property['channelNamespaces'], nameSpace)
            }) || resp['channel']['physicalChannels'][0]
        }
      let hasOutTime = this.setHasOutTime(resp, mediaData)
      let isCUTV = this.setIsCUTV(resp)
      let canPlay = mediaData && mediaData['cutvCR']['enable'] === '1' && mediaData['cutvCR']['isContentValid'] === '1' &&
            mediaData['cutvCR']['isSubscribed'] === '1' && mediaData['cutvCR']['isValid'] === '1' && isCUTV && hasOutTime
      return this.getMediaData(canPlay, mediaData, isCUTV, hasOutTime)
    }

  setHasOutTime (resp, mediaData) {
      let hasOutTime = false
      let length = Number(Date.now()['getTime']()) - this.getNumber(mediaData && mediaData['cutvCR']['length'])
      if (Number(resp['startTime']) > length) {
          hasOutTime = true
        } else {
          hasOutTime = false
        }
      return hasOutTime
    }

  setIsCUTV (resp) {
      let isCUTV = false
      if (resp['isCUTV'] && resp['isCUTV'] === '1') {
          isCUTV = true
        } else {
          isCUTV = false
        }
      return isCUTV
    }

  getMediaData (canPlay, mediaData, isCUTV, hasOutTime) {
      let canBuy = mediaData && (mediaData['cutvCR']['enable'] === '1' && mediaData['cutvCR']['isContentValid'] === '1') &&
            (mediaData['cutvCR']['isSubscribed'] !== '1' || mediaData['cutvCR']['isValid'] !== '1') && isCUTV && hasOutTime
      if (canPlay) {
          return 'play'
        } else if (canBuy) {
          return 'buy'
        } else {
          return ''
        }
    }

  getNumber (length) {
       return Number(length) * 1000
     }

}
