import { Injectable } from '@angular/core'
import { Config as SDKConfig } from '../../../demos-app/sdk/config'
import * as _ from 'underscore'
import { TranslateService } from '@ngx-translate/core'
import { session } from '../../../demos-app/sdk/session'
import { DateUtils } from '../../../demos-app/sdk/date-utils'

@Injectable()
export class ProgramSuspensionService {
  private physicalChannels
  private filterNpvrChannel
  private filterCpvrChannel

  constructor (private translate: TranslateService) { }

  formatStr (str, index) {
      let weekend
      let month
      let day
      let hour
      let minute
      let allWeekends = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']
      let arr = []
      if (str.length === 13) {
          let date = parseInt(str, 10)
          let datestr = new Date(date)
          month = (datestr.getMonth() + 1) < 10 ? '0' + (datestr.getMonth() + 1) : (datestr.getMonth() + 1)
          weekend = datestr.getDay() 
          day = datestr.getDate() < 10 ? '0' + datestr.getDate() : datestr.getDate()
          hour = datestr.getHours() < 10 ? '0' + datestr.getHours() : datestr.getHours()
          minute = datestr.getMinutes() < 10 ? '0' + datestr.getMinutes() : datestr.getMinutes()
          weekend = allWeekends[weekend]
        } else {
          month = str.slice(4, 6)
          day = str.slice(6, 8)
          hour = str.slice(8, 10)
          minute = str.slice(10, 12)
        }
      arr.push(month)
      arr.push(weekend)
      arr.push(day)
      arr.push(hour)
      arr.push(minute)
      return arr[index]

    }
    /**
     * judge whether the reminder show or hidden and judge the status
     */
  isSupportReminder (playbill): any {
      if (session.get('reminderMode')) {
          if (playbill['reminder'] && playbill['reminder'].reminderTime) {
              return 'CANCEL'
            } else {
              if (playbill['startTime'] - Date.now()['getTime']() > 0) {
                  return 'ADD'
                } else {
                  return false
                }
            }
        } else {
          return false
        }
    }
    // the condition of whether the playbill support CPVR or NPVR
  judgeRecord (recordCR, channel): any {
      let channels = _.filter(channel['physicalChannelsDynamicProperties'], function (physicalChannels) {
          let recordCRType = physicalChannels[recordCR]
          let btvCR = physicalChannels['btvCR']
          if (recordCRType && recordCRType['enable'] === '1' && recordCRType['isSubscribed'] === '1'
                && recordCRType['isContentValid'] === '1' && recordCRType['isValid'] === '1'
                && btvCR && btvCR['enable'] === '1' && btvCR['isSubscribed'] === '1'
                && btvCR['isContentValid'] === '1' && btvCR['isValid'] === '1') {
              return recordCRType
            }
        })
      return channels
    }
    // the condition of whether the playbill support CPVR or NPVR
  isCPVROrNPVR (playbill, type) {
      if (playbill[type] === '1') {
          if (playbill.hasRecordingPVR && playbill.hasRecordingPVR !== '0') {
              let currentTime = parseInt(Date.now()['getTime'](), 10)
              if (currentTime > playbill['startTime'] && currentTime < playbill['endTime']) {
                  return 'stop_recording'
                } else {
                  return 'cancel_record'
                }
            }
          return 'livetv_record'
        }
    }

    /**
     * the condition of recording
     */
  channelIsRecord (channel, playbill): any {
      let userType
      if (Cookies.getJSON('IS_PROFILE_LOGIN')) {
          userType = Cookies.getJSON('USER_ID')
        } else {
          userType = session.get('Guest') || 'Guest'
        }
      let dynamicData = session.get(userType + '_Dynamic_Version_Data')
      _.each(dynamicData['channelDynamaicProp'], (list1, index) => {
          if (list1['ID'] === channel['ID']) {
              channel['physicalChannelsDynamicProperties'] = list1['physicalChannelsDynamicProperties']
            }
        })
      this.physicalChannels = channel['physicalChannelsDynamicProperties']
      if (channel['physicalChannelsDynamicProperties'] && session.get('EPGCONFIG')  === 'NPVR') {
          this.filterNpvrChannel = this.judgeRecord('npvrRecordCR', channel)

          if (this.filterNpvrChannel.length > 0) {
              return this.isCPVROrNPVR(playbill, 'isNPVR')
            }
          return false
        } else if (channel['physicalChannelsDynamicProperties'] && session.get('EPGCONFIG')  === 'CPVR') {
          this.filterCpvrChannel = this.judgeRecord('cpvrRecordCR', channel)

          if (this.filterCpvrChannel.length > 0) {
              return this.isCPVROrNPVR(playbill, 'isCPVR')
            }
          return false
        } else if (channel['physicalChannelsDynamicProperties'] && session.get('EPGCONFIG')  === 'CPVR&NPVR') {
          this.filterNpvrChannel = this.judgeRecord('npvrRecordCR', channel)
          if ((playbill['isNPVR'] === '1') && this.filterNpvrChannel.length > 0) {
              return this.isCPVROrNPVR(playbill, 'isNPVR')
            }
          this.filterCpvrChannel = this.judgeRecord('cpvrRecordCR', channel)
          if ((playbill['isCPVR'] === '1') && this.filterCpvrChannel.length > 0) {
              return this.isCPVROrNPVR(playbill, 'isCPVR')
            }
          return false
        } else {
          return false
        }
    }
  dealWithTime (month) {
       let Month
       if (month === '01') {
          Month = 'jan_tvguide'
        } else if (month === '02') {
          Month = 'feb_tvguide'
        } else if (month === '03') {
          Month = 'mar_tvguide'
        } else if (month === '04') {
          Month = 'apr_tvguide'
        } else if (month === '05') {
          Month = 'may_tvguide'
        } else if (month === '06') {
          Month = 'jun_tvguide'
        } else if (month === '07') {
          Month = 'jul_tvguide'
        } else if (month === '08') {
          Month = 'aug_tvguide'
        } else if (month === '09') {
          Month = 'sep_tvguide'
        } else if (month === '10') {
          Month = 'oct_tvguide'
        } else if (month === '11') {
          Month = 'nov_tvguide'
        } else if (month === '12') {
          Month = 'dec_tvguide'
        }
       return this.translate.instant(Month)
     }
  dealWithDateJson (dateType, timeStr) {
      let dateStrJson = session.get('DATESTRJSON')
      let dateStr
      for (let i = 0; i < dateStrJson['date'].length; i ++) {
          if (dateStrJson['date'][i]['name'] === dateType) {
              dateStr = dateStrJson['date'][i]['value']
            }
        }

      let weekTime = DateUtils.format(parseInt(timeStr, 10), 'ddd')
      weekTime = this.translate.instant(weekTime)
      let weekTimeAll = DateUtils.format(parseInt(timeStr, 10), 'dddd')
      weekTimeAll = this.translate.instant(weekTimeAll)
      let month = DateUtils.format(parseInt(timeStr, 10), 'M')
      let monthZero = DateUtils.format(parseInt(timeStr, 10), 'MM')
      let monthSimple = DateUtils.format(parseInt(timeStr, 10), 'MMM')
      monthSimple = this.translate.instant(monthSimple)
      let monthAll = DateUtils.format(parseInt(timeStr, 10), 'MMMM')
      monthAll = this.translate.instant(monthAll)
      let day = DateUtils.format(parseInt(timeStr, 10), 'D')
      let zeroDay = DateUtils.format(parseInt(timeStr, 10), 'DD')
      let year = DateUtils.format(parseInt(timeStr, 10), 'YYYY')
      let hour12 = DateUtils.format(parseInt(timeStr, 10), 'h')
      let hour24 = DateUtils.format(parseInt(timeStr, 10), 'H')
      let hour12Two = DateUtils.format(parseInt(timeStr, 10), 'hh')
      let hour24Two = DateUtils.format(parseInt(timeStr, 10), 'HH')
      let minute = DateUtils.format(parseInt(timeStr, 10), 'm')
      let minuteZero = DateUtils.format(parseInt(timeStr, 10), 'mm')
      let second = DateUtils.format(parseInt(timeStr, 10), 's')
      let secondZero = DateUtils.format(parseInt(timeStr, 10), 'ss')
      let AmOrPm

      dateStr = dateStr.replace(/{d}/g, day) // 以1位数表示一个月中的第几天
      dateStr = dateStr.replace(/{dd}/g, zeroDay) // 以2位数表示一个月中的第几天
      dateStr = dateStr.replace(/{ddd}/g, weekTime) // 星期缩写（Sun 或者 Sat）***************国际化翻译
      dateStr = dateStr.replace(/{dddd}/g, weekTimeAll) // 星期全拼（Sunday 或者 Saturday）***************国际化翻译
      dateStr = dateStr.replace(/{m}/g, month) // 以1位数表示一年中的月份
      dateStr = dateStr.replace(/{mm}/g, monthZero) // 以2位数表示一年中的月份
      dateStr = dateStr.replace(/{mmm}/g, monthSimple) // 月份缩写，Aug ***************国际化翻译
      dateStr = dateStr.replace(/{mmmm}/g, monthAll) // 月份全称，Augest ***************国际化翻译
      dateStr = dateStr.replace(/{yyyy}/g, year) // 完整的年份
      dateStr = dateStr.replace(/{yy}}/g, year.substring(year.length - 1 , year.length)) // 年份的后两位
      dateStr = dateStr.replace(/{H12}/g, hour12) // 以一位数表示小时，12小时制
      dateStr = dateStr.replace(/{H24}/g, hour24) // 以一位数表示小时，24小时制
      dateStr = dateStr.replace(/{hh12}/g, hour12Two) // 以2位数表示小时，12小时制
      dateStr = dateStr.replace(/{hh24}/g, hour24Two) // 以2位数表示小时，12小时制
      dateStr = dateStr.replace(/{N}/g, minute) // 以一位数表示分钟
      dateStr = dateStr.replace(/{nn}/g, minuteZero) // 以2位数表示分钟
      dateStr = dateStr.replace(/{s}/g, second) // 以1位数表示秒
      dateStr = dateStr.replace(/{ss}/g, secondZero) // 以2位数表示秒
      if (Number(hour24) > 12) {
          AmOrPm = this.translate.instant('times_pm')
        } else {
          AmOrPm = this.translate.instant('times_am')
        }
      dateStr = dateStr.replace(/{AP}/g, AmOrPm) // AM, PM, 12小时制 ***************国际化翻译

      if (dateStr.indexOf(' DST') > -1) {
          dateStr = dateStr.replace(/ DST/g, '')
          dateStr = dateStr + ' DST'
        }
      return dateStr
    }
}
