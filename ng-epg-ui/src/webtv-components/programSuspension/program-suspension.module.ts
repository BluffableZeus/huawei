import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'
import { TranslateModule } from '@ngx-translate/core'
import { SuspensionComponentProgram } from './programSuspension.component'
import { ProgramSuspensionService } from './programSuspension.service'

@NgModule({
  imports: [CommonModule, TranslateModule],
  exports: [SuspensionComponentProgram],
  declarations: [SuspensionComponentProgram],
  providers: [ProgramSuspensionService]
})
export class ProgramSuspensionModule { }
