import * as _ from 'underscore'
import { Component, Input, ViewChild, EventEmitter, Output } from '@angular/core'
import { EventService } from '../../../demos-app/sdk/event.service'
import { ActivatedRoute } from '@angular/router'
import { SuspensionComponentProgram } from '../programSuspension/programSuspension.component'
import { CommonSuspensionService } from 'ng-epg-ui/webtv-components/channelSuspension/channelCommon.service'

@Component({
  selector: 'waterfall-channel',
  templateUrl: './waterfall-channel.component.html',
  styleUrls: ['./waterfall-channel.component.scss']
})
export class WaterfallChannelBox {
  @ViewChild(SuspensionComponentProgram) suspensionprogram: SuspensionComponentProgram
  @Output() child: EventEmitter<Object> = new EventEmitter()
  public hotShow: string = 'false'
  isChangeRow: number
  suspensionID: string
  dataList: Array<any>
  _data: Object
  private susTimeChannel
  private susFilter = _.debounce((fc: Function) => { fc() }, 400)
  private susUse: boolean = true

  @Input() set data (data: Object) {
      this._data = data
      if (this._data['row'] === '1') {
          if (this._data['clientW'] > 1440) {
              this.dataList = this._data['dataList']
            } else {
              this.dataList = this._data['dataList'].slice(0, 5)
            }
        } else if (this._data['row'] === '2') {
          if (this._data['clientW'] > 1440) {
              this.dataList = this._data['dataList']
            } else {
              this.dataList = this._data['dataList']
            }
        } else {
          this.dataList = this._data['dataList']
        }
      return
    }

  constructor (
        private route: ActivatedRoute,
        private CommonSuspensionService: CommonSuspensionService
    ) { }
  ngOnInit () {
      if (!_.isUndefined(this._data['suspensionID'])) {
          this.suspensionID = this._data['suspensionID']
        }
      for (let i = 0; i < this._data['dataList'].length; i++) {
          if (_.isUndefined(this._data['dataList'][i].posterUrl) || this._data['dataList'][i].posterUrl === '') {
              this._data['dataList'][i].posterUrl = 'assets/img/default/livetv_home_small.png'
            }
        }
    }

    /**
     * deal with VodDetail
     * emit events
     */
  vodDtail (list) {
      document.getElementsByClassName('play_loading')[0]['style']['display'] = 'block'
      if (list && list['isFillProgram']) {
          return
        }
      if (list.startTime > Date.now()['getTime']() || list.endTime < Date.now()['getTime']()) {
          let event
          if (list.recmActionID && list.entrance) {
              event = [list.ID, list.endTime, '0', list.recmActionID, list.entrance]
            } else {
              event = [list.ID, list.endTime, '0']
            }
          this.child.emit(event)
        } else {
          let info
          if (list.recmActionID && list.entrance) {
              info = [list['playbillInfo'], list['channelInfo'], '1', list['recmActionID'], list.entrance]
            } else {
              info = [list['playbillInfo'], list['channelInfo'], '1']
            }
          this.child.emit(info)
        }

    }

    /**
     * mouseenter events
     */
  mouseenterChannel (option, event) {
      this.susUse = true
      this.susFilter(() => {
          let self = this
          if (self.susUse === false) {
              return
            }
          if (option && option['isFillProgram']) {
              return
            }
          self.hotShow = 'false'
          let currentTarget = window.event && window.event.currentTarget['offsetParent']
                || event && event.target['offsetParent']
          let width = currentTarget.offsetWidth
          let left = currentTarget.offsetLeft
            //show the suspension detail after the suspension already get data,
          let time1 = new Date().getTime()
          EventService.removeAllListeners(['LOADDATAPROGRAMSUCCESS'])
          EventService.on('LOADDATAPROGRAMSUCCESS', function () {
              self.CommonSuspensionService.moveEnter(currentTarget, 'suspensionInfo',
                    document.body.clientWidth, 'waterfallChannel')
              let time2 = new Date().getTime()
              if ((time2 - time1) < 500) {
                  let time = 500 - (time2 - time1) + 100
                  clearTimeout(self.susTimeChannel)
                  self.susTimeChannel = setTimeout(function () {
                      self.hotShow = 'true'
                    }, time)
                } else {
                  clearTimeout(self.susTimeChannel)
                  self.susTimeChannel = setTimeout(function () {
                      self.hotShow = 'true'
                    }, 100)
                }
            })
          this.suspensionprogram.setDetail(option, 'suspensionInfo', currentTarget.firstElementChild)
        })
    }

  closeDialogChannel (option) {
      let self = this
      self.susUse = false
      if (option && option['isFillProgram']) {
          return
        }
      clearTimeout(self.susTimeChannel)
      EventService.removeAllListeners(['LOADDATAPROGRAMSUCCESS'])
      EventService.on('showProgramSuspension', function (options) {
          if (options === 'moreList') {
              self.hotShow = 'true'
            }
        })
      EventService.on('closeProgramSuspension', function () {
          self.hotShow = 'false'
        })
      self.hotShow = 'false'
    }
  nofind (img) {
      img.src = 'assets/img/default/home_poster.png'
      img.onerror = null
    }
}
