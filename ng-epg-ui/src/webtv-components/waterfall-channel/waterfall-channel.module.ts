import { NgModule } from '@angular/core'
import { TranslateModule } from '@ngx-translate/core'
import { CommonModule } from '@angular/common'

import { WaterfallChannelBox } from './waterfall-channel.component'
import { ProgramSuspensionModule } from '../programSuspension'

@NgModule({
  imports: [
      ProgramSuspensionModule,
      CommonModule,
      TranslateModule
    ],
  exports: [WaterfallChannelBox],
  declarations: [WaterfallChannelBox],
  providers: []
})
export class WaterfallChannelModule { }
