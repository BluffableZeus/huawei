import { CommonModule } from '@angular/common'
import { TranslateModule } from '@ngx-translate/core'
import { NgModule } from '@angular/core'

import { ProgramComponent } from './programComponent.component'
import { ProgramSuspensionModule } from '../programSuspension'
import { NoDataModule } from '../noData'
import { UISharedModule } from '../shared'

@NgModule({
  imports: [
      CommonModule,
      ProgramSuspensionModule,
      NoDataModule,
      UISharedModule,
      TranslateModule
    ],
  exports: [ProgramComponent],
  declarations: [ProgramComponent],
  providers: []
})
export class ProgramComponentModule { }
