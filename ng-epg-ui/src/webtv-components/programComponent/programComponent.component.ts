import * as _ from 'underscore'
import { Component, OnInit, EventEmitter, Output, Input, ViewChild, OnDestroy } from '@angular/core'
import { SuspensionComponentProgram } from '../programSuspension/programSuspension.component'
import { Router } from '@angular/router'
import { EventService } from '../../../demos-app/sdk/event.service'
import { CommonSuspensionService } from 'ng-epg-ui/webtv-components/channelSuspension/channelCommon.service'

@Component({
  selector: 'app-program-component',
  styleUrls: ['./programComponent.component.scss'],
  templateUrl: './programComponent.component.html'
})

export class ProgramComponent implements OnInit, OnDestroy {
  @Input() id   // suspention ID、list ID
  @Output() jumpTo: EventEmitter<Object> = new EventEmitter()
  @Output() jumpToData: EventEmitter<Object> = new EventEmitter()
  @ViewChild(SuspensionComponentProgram) suspensionprogram: SuspensionComponentProgram
  public suspensionInfo
  public offsetNumber = []
  public bigLen = []
  public smallLen = []
  public list
  public hotShow: string = 'false'
  public susTimeChannel
  public leftIconShow = []
  public rightIconShow = []
  public panel
  public refreshSuspension: boolean = true
  public count = []
  public programData
  public susFilter = _.debounce((fc: Function) => { fc() }, 400)
  public susUse: boolean = true
  public mouseenterList = {}

  constructor (
        private route: Router,
        private CommonSuspensionService: CommonSuspensionService
    ) {
      this.notification = this.notification.bind(this)
    }
  @Input() set datas (programData) {
      this.programData = programData
      _.each(this.programData, (list) => {
           list['titleName'] = list['name'].toUpperCase()
         })
      EventService.removeAllListeners(['CHANGESUBJECTDATA'])
      EventService.on('CHANGESUBJECTDATA', (data) => {
          this.programData = data
          _.each(this.programData, (list) => {
              list['titleName'] = list['name'].toUpperCase()
            })
          this.ngOnInit()
        })
      this.ngOnInit()
    }
  ngOnInit () {
      this.suspensionInfo = this.id[0]
      this.list = this.id[1]
      for (let i = 0; i < this.programData.length; i++) {
          this.offsetNumber.unshift(0)
          this.rightIconShow.push('true')
          this.bigLen.push('0')
          this.smallLen.push('0')
          this.count.push('0')
        }
      let self = this
      self.showIcon()
      self.hotShow = 'false'
    }
  ngOnDestroy () {
      EventService.removeAllListeners(['showProgramSuspension'])
      EventService.removeAllListeners(['closeProgramSuspension'])
    }
  moreShow (list) {
      let name = list['name']
      let id = list['id']
      if (list && list.length !== 0) {
          let data = [name, id]
          this.jumpTo.emit(data)
        }
    }
    /**
     * onClickDetail
     */
  onClickDetail (list) {
      document.getElementsByClassName('play_loading')[0]['style']['display'] = 'block'
      if (list !== undefined) {
          if (list.endTime < Date.now()['getTime']() || list.startTime > Date.now()['getTime']()) {
              if (list && list['isFillProgram']) {
                  EventService.emit('SET_FILLPROGRAM', list['channelInfo'])
                  return
                }
              let jumpData = [list.id, list.endTime]
              this.jumpToData.emit(jumpData)
            } else {
              let info = [list['playbillInfo'], list['channelInfo']]
              this.jumpToData.emit(info)
            }
        }
    }
    /**
     * when in home ,click poster which has no button, go into the detail page
     */
  specialClickDetail (list, event) {
      let currentTarget = event && event.currentTarget || (event && event.target) 
      if (currentTarget.id !== 'suspensionHotTv') {
          return
        }
      document.getElementsByClassName('play_loading')[0]['style']['display'] = 'block'
      if (list !== undefined) {
          if (list.endTime < Date.now()['getTime']() || list.startTime > Date.now()['getTime']()) {
              if (list && list['isFillProgram']) {
                  EventService.emit('SET_FILLPROGRAM', list['channelInfo'])
                  return
                }
              let jumpData = [list.id, list.endTime]
              this.jumpToData.emit(jumpData)
            } else {
              let info = [list['playbillInfo'], list['channelInfo']]
              this.jumpToData.emit(info)
            }
        }
    }

  onClickLeft (i) {
      let clientW: number = window.innerWidth
      if (clientW > 1440) {
          this.panel = 1400
        } else {
          this.panel = 999
        }
      let offsetWidth: string
      if (this.offsetNumber[i] <= -this.panel) {
          this.offsetNumber[i] += this.panel
          offsetWidth = this.offsetNumber[i] + 'px'
          this.dom(this.list + '-' + i).style.transform = 'translate(' + offsetWidth + ',0px'
          this.dom(this.list + '-' + i).style.transition = 'all 1s linear'
          this.isShowIcon(this.offsetNumber[i], i)
        } else {
          this.dom(this.list + '-' + i).style.transform = 'translate(' + 0 + 'px,0px'
          this.dom(this.list + '-' + i).style.transition = 'all 1s linear'
          this.offsetNumber[i] = 0
          this.isShowIcon(this.offsetNumber[i], i)
        }
    }

  onClickRight (i) {
      let offsetWidth: string = ''
      if (window.innerWidth > 1440) {
          const MAXSFFSETNUMBER: number = -5600
          if (this.offsetNumber[i] >= MAXSFFSETNUMBER) {
              this.offsetNumber[i] -= 1400
            }
        } else if (window.innerWidth <= 1440) {
          const MAXSFFSETNUMBER: number = -3996
          if (this.offsetNumber[i] >= MAXSFFSETNUMBER) {
              this.offsetNumber[i] -= 999
            }
        }
      offsetWidth = this.offsetNumber[i] + 'px'
      this.dom(this.list + '-' + i).style.transform = 'translate(' + offsetWidth + ',0px'
      this.dom(this.list + '-' + i).style.transition = 'all 1s linear'
      this.isShowIcon(this.offsetNumber[i], i)
    }

  isShowIcon (offsetNumber: number, i) {
      let clientW: number = window.innerWidth
      let judgeLength
      if (clientW > 1440) {
          judgeLength = (this.count[i] - 4) * (330 + 20)
        } else {
          judgeLength = (this.count[i] - 3) * (313 + 20)
        }
      if (window.innerWidth > 1440) {
          if (offsetNumber === 0) {
              this.leftIconShow[i] = 'false'
              this.rightIconShow[i] = 'true'
            } else if ((offsetNumber <= -judgeLength && this.bigLen[i] < 4) || offsetNumber === -4200) {
              this.leftIconShow[i] = 'true'
              this.rightIconShow[i] = 'false'
            } else {
              this.leftIconShow[i] = 'true'
              this.rightIconShow[i] = 'true'
            }
        }
      if (window.innerWidth <= 1440) {
          if (offsetNumber === 0) {
              this.leftIconShow[i] = 'false'
              this.rightIconShow[i] = 'true'
            } else if ((offsetNumber <= -judgeLength && this.smallLen[i] < 3) || offsetNumber === -2997) {
              this.leftIconShow[i] = 'true'
              this.rightIconShow[i] = 'false'
            } else {
              this.leftIconShow[i] = 'true'
              this.rightIconShow[i] = 'true'
            }
        }
    }

  showIcon () {
      if (window.innerWidth > 1440) {
          for (let i = 0; i < this.programData.length; i++) {
              this.bigLen[i] = Math.ceil(this.programData[i].length / 4) - 1
              this.count[i] = this.programData[i].length
              if (this.bigLen[i] === 0) {
                  this.rightIconShow[i] = 'false'
                  this.leftIconShow[i] = 'false'
                } else {
                  this.rightIconShow[i] = 'true'
                  this.leftIconShow[i] = 'false'
                }
            }
        }
      if (window.innerWidth <= 1440) {
          for (let i = 0; i < this.programData.length; i++) {
              this.smallLen[i] = Math.ceil(this.programData[i].length / 3) - 1
              this.count[i] = this.programData[i].length
              if (this.smallLen[i] === 0) {
                  this.rightIconShow[i] = 'false'
                  this.leftIconShow[i] = 'false'
                } else {
                  this.rightIconShow[i] = 'true'
                  this.leftIconShow[i] = 'false'
                }
            }
        }
    }

  dom (divName: string) {
      return document.getElementById(divName)
    }
  mouseenter (option, i, event) {
      this.mouseenterList = option
      this.susUse = true
      this.susFilter(() => {
          let self = this
          if (self.susUse === false) {
              return
            }
          if (option['isFillProgram']) {
              return
            }
          self.hotShow = 'false'
          let currentTarget = window.event && window.event.currentTarget['offsetParent'] || event && event.target['offsetParent']
          let width = currentTarget && currentTarget.offsetWidth
          let left
          let time1 = new Date().getTime()
          EventService.removeAllListeners(['LOADDATAPROGRAMSUCCESS'])
          EventService.on('LOADDATAPROGRAMSUCCESS', function () {
              self.CommonSuspensionService.moveEnter(currentTarget.firstElementChild, self.suspensionInfo, document.body.clientWidth, 'LiveTvProgram')
              let time2 = new Date().getTime()
              if ((time2 - time1) < 500) {
                  let time = 500 - (time2 - time1) + 100
                  clearTimeout(self.susTimeChannel)
                  self.susTimeChannel = setTimeout(function () {
                      self.hotShow = 'true'
                    }, time)
                } else {
                  clearTimeout(this.susTimeChannel)
                  self.susTimeChannel = setTimeout(function () {
                      self.hotShow = 'true'
                    }, 100)
                }
            })
          if (currentTarget) {
              left = (currentTarget.offsetLeft + self.offsetNumber[i])
              self.suspensionprogram.setDetail(option,self.suspensionInfo,currentTarget.firstElementChild)
            }
        })
    }

  closeDialog (option) {
      let self = this
      self.susUse = false
      if (option['isFillProgram']) {
          return
        }
      clearTimeout(self.susTimeChannel)
      EventService.removeAllListeners(['LOADDATAPROGRAMSUCCESS'])
      EventService.removeAllListeners(['showProgramSuspension'])
      EventService.on('showProgramSuspension', function (options) {
          self.hotShow = 'true'
        })
      EventService.removeAllListeners(['closeProgramSuspension'])
      EventService.on('closeProgramSuspension', function () {
          self.hotShow = 'false'
        })
      self.hotShow = 'false'
    }

  notification (eventName, data) {
      let self = this
      switch (eventName) {
          case 'ScreenSize':
            self.showIcon()
            if (window.innerWidth > 1440) {
                  for (let i = 0; i < self.programData.length; i++) {
                      let curnum = Math.ceil(self.offsetNumber[i] / (313 + 20))
                      let curLength = curnum * (330 + 20)
                      self.offsetNumber[i] = curLength
                      if (self.dom(self.list + '-' + i)) {
                          self.dom(self.list + '-' + i).style.transform = 'translate(' + curLength + 'px,0px'
                          self.dom(self.list + '-' + i).style.transition = ''
                        }
                      self.isShowIcon(self.offsetNumber[i], i)
                      self.showIcon()
                    }
                } else if (window.innerWidth <= 1440) {
                  for (let i = 0; i < self.programData.length; i++) {
                      let num = Math.ceil(self.offsetNumber[i] / 999)
                      if (num === -4) {
                          num = -3
                        }
                      let curnum = Math.ceil(self.offsetNumber[i] / (330 + 20))
                      let curLength = curnum * (313 + 20)
                      self.offsetNumber[i] = curLength
                      if (self.dom(self.list + '-' + i)) {
                          self.dom(self.list + '-' + i).style.transform = 'translate(' + curLength + 'px,0px'
                          self.dom(self.list + '-' + i).style.transition = ''
                        }
                      self.isShowIcon(self.offsetNumber[i], i)
                      self.showIcon()
                    }
                }
            break
          default:
            break
        }
    }
}
