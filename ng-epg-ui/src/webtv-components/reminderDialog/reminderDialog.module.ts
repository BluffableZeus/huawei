import { TranslateModule } from '@ngx-translate/core'
import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'

import { ReminderDialogComponent } from './reminderDialog.component'

@NgModule({
  imports: [CommonModule, TranslateModule],
  exports: [ReminderDialogComponent],
  declarations: [ReminderDialogComponent],
  providers: [ ]
})
export class ReminderDialogModule { }
