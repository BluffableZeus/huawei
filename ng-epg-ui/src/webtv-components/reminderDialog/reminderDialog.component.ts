import * as _ from 'underscore'
import { Component, OnInit, Input } from '@angular/core'

@Component({
  selector: 'app-reminder-dialog',
  templateUrl: './reminderDialog.component.html',
  styleUrls: ['./reminderDialog.component.scss']
})
export class ReminderDialogComponent implements OnInit {
  public num: any
  public reminderList = []
  public curIndex: number = 0
  public curReminder = {}

  @Input() set ReminderData (data) {
      this.reminderList = data
    }

  ngOnInit () {
      this.curReminder = this.reminderList[0]
      if (this.reminderList.length > 1) {
          this.num = this.reminderList.length
        }
    }
    // ignore one reminder
  ignore () {
        // delete one reminder, if the number > 1
      if (this.reminderList.length > 1) {
            // remove if according to the position in list
          this.reminderList.splice(this.curIndex, 1)
            // if delete reminder in the final, positioning to the first
          if (Number(this.curIndex) === Number(this.reminderList.length)) {
              this.curIndex = 0
            }
          this.curReminder = this.reminderList[this.curIndex]
            // modify the reminder number in title
          if (this.reminderList.length > 1) {
              this.num = this.reminderList.length
            }
        }
    }
    // ignore all reminder that has show already
  ignoreAll () {

    }
    // watch playbill
  watch () {

    }
    // switch next reminder
  next () {
      this.curIndex++
        // if switch the last one, change the position to first
      if (this.curIndex > this.reminderList.length - 1) {
          this.curIndex = 0
        }
      this.curReminder = this.reminderList[this.curIndex]
    }
    // switch pre reminder
  previous () {
      this.curIndex--
        // if switch the first, change the position to the last one
      if (this.curIndex < 0) {
          this.curIndex = this.reminderList.length - 1
        }
      this.curReminder = this.reminderList[this.curIndex]
    }
}
