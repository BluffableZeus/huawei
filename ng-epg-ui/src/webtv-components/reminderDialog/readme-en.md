### Usage
```typescript
import { ReminderDialogComponent } from 'ng-epg-ui/webtv-components/reminderDialog';

<app-reminder-dialog [ReminderData]="reminderList"></app-reminder-dialog>

```

### Annotations
```typescript
export class ReminderDialogComponent {
    @Input() set ReminderData(data) {
        this.reminderList = data;
    }
}

```

 ### Attributes
| Attribute   | Description   | Type     | Options     | Default Value    | Mandatory (M) or Optional (O)    |
|-----   |-----    |-----    |------      |------    |------    |
| ReminderData | A collection of programs needed to be reminded.  | Array&lt;Reminder&gt; |  |  |  M |

 ### Data Type
 #### Reminder
| Attribute   | Description   | Type    | Default Value    | Mandatory (M) or Optional (O)    |
|-----   |-----    |-----    |------    |------    |
| playbillName | 节目单名称 | string |  |  M |
| season | season信息的词条对象 | Message |  |  M |
| episode | episode信息的词条对象 | Message |  |  M |
| reminderTip | 提示内容 | array |  |  M |

 #### Message
| Attribute   | Description   | Type    | Default Value    | Mandatory (M) or Optional (O)    |
|-----   |-----    |-----    |------    |------    |
| name | 集或者季的编号 | string |  |  M |
