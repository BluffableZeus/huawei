### Usage
```typescript
import { DirectionScroll } from 'ng-epg-ui/webtv-components/scroll/directionScroll.component';
import { TransverseScroll } from 'ng-epg-ui/webtv-components/scroll/transverseScroll.component';

<div></div>

```

### Annotations
```typescript
export class DirectionScroll {
}

```

 ### Attributes
| Attribute   | Description   | Type     | Options     | Default Value    | Mandatory (M) or Optional (O)    |
|-----   |-----    |-----    |------      |------    |------    |
| null | null  | null | null | null |  null |
