import * as _ from 'underscore'
import { EventService } from '../../../demos-app/sdk/event.service'

export class TransverseScroll {

  myEvent (obj, ev, fu) {
      obj.attachEvent ? obj.attachEvent('on' + ev, fu) : obj.addEventListener(ev, fu, false)
    }

  setScroll (oBox, oConter, oUl, oScroll, oSpan, allDay?: any) {
      let currentObject = this
      let i = 0
      let iWidth = 416
      _.delay(function () {
          if (oSpan) {
              oSpan.style.width = oScroll.offsetWidth * (oConter.offsetWidth / oUl.offsetWidth) + 'px'
            }
        }, 100)

        // click scroll bar
      if (oScroll) {
          oScroll.onclick = function (e) {
              let oEvent = e || event
              let butscroll = oEvent.clientX - oSpan.offsetWidth / 2
              oscroll(butscroll, currentObject)
            }
        }

      if (oSpan) {
          oSpan.onclick = function (e) {
              let oEvent = e || event
              oEvent.cancelBubble = true
            }
        }

        // drag scroll bar
      let iX = 0
      if (oSpan) {
          oSpan.onmousedown = function (e) {
              let oEvent = e || event
              iX = oEvent.clientX - oSpan.offsetLeft
              document.onmousemove = function (e1) {
                  oEvent = e1 || event
                  let l = oEvent['clientX'] - iX
                  td(l)
                  return false
                }
              document.onmouseup = function () {
                  document.onmousemove = null
                  document.onmouseup = null
                }
              return false
            }
        }

         // scroll event
      function oscroll (l, currentObject1) {
          EventService.emit('SHOW_SKIP_TO_NOW')
          l = l - oScroll.getBoundingClientRect().left
          if (l < 0) {
              l = 0
            } else if (l > oScroll.offsetWidth - oSpan.offsetWidth) {
              l = oScroll.offsetWidth - oSpan.offsetWidth
            }
          let scrol = l / (oScroll.offsetWidth - oSpan.offsetWidth)
          currentObject1.sMove(oSpan, 'left', Math.ceil(l))
          currentObject1.sMove(oUl, 'left', '-' + Math.ceil((oUl.offsetWidth - oConter.offsetWidth) * scrol))
          currentObject1.sMove(allDay, 'left', '-' + Math.ceil((allDay.offsetWidth - oConter.offsetWidth) * scrol))
        }

      function td (l) {
          EventService.emit('SHOW_SKIP_TO_NOW')
          if (l < 0) {
              l = 0
            } else if (l > oScroll.offsetWidth - oSpan.offsetWidth) {
              l = oScroll.offsetWidth - oSpan.offsetWidth
            }
          let scrol = l / (oScroll.offsetWidth - oSpan.offsetWidth)
          oSpan.style.left = l + 'px'
          oUl.style.left = '-' + (oUl.offsetWidth - oConter.offsetWidth) * scrol + 'px'
          allDay.style.left = '-' + (allDay.offsetWidth - oConter.offsetWidth) * scrol + 'px'
        }
    }

  getStyle (obj, attr) {
      return obj.currentStyle ? obj.currentStyle[attr] : getComputedStyle(obj, null)[attr]
    }

  sMove (obj, attr, iT, onEnd) {
      let self = this
      clearInterval(obj.timer)
      obj.timer = setInterval(function () {
          self.dMove(obj, attr, iT, onEnd)
        }, 30)
    }

  dMove (obj, attr, iT, onEnd) {
      let iCur = 0
      attr === 'opacity' ? iCur = parseInt(this.getStyle(obj, attr), 10) * 100  : iCur = parseInt(this.getStyle(obj, attr), 10)
      let iS = (iT - iCur) / 5
      iS = iS > 0 ? Math.ceil(iS) : Math.floor(iS)
      if (iCur === parseInt(iT, 10)) {
          clearInterval(obj.timer)
          if (onEnd) {
              onEnd()
            }
        } else {
          if (attr === 'opacity') {
              obj.style.ficter = 'alpha(opacity:' + (iCur + iS) + ')'
              obj.style.opacity = (iCur + iS) / 100
            } else {
              obj.style[attr] = iCur + iS + 'px'
            }
        }
    }
}
