import * as _ from 'underscore'
import { EventService } from '../../../demos-app/sdk/event.service'

export class DirectionScroll {
  myEvent (obj, ev, fu) {
      if (obj) {
          obj.attachEvent ? obj.attachEvent('on' + ev, fu) : obj.addEventListener(ev, fu, false)
        }
    }

  setScroll (oBox, oConter, oUl, oScroll, oSpan, flag?: any, isToBottom?: any) {
      let isLivetvMini = false
      if (flag) {
          isLivetvMini = true
        }
      let currentObject = this
      let i = 0
      let iHeight = 50

      _.delay(function () {
          if (oSpan && oScroll && oConter && oUl) {
              oSpan.style.height = oScroll.offsetHeight * (oConter.offsetHeight / oUl.offsetHeight) + 'px'
            }
        }, 100)

        // click scroll bar
      if (oScroll) {
          oScroll.onclick = function (e) {
              let oEvent = e || event
              let butscroll = oEvent.offsetY - oSpan.offsetHeight / 2
              oscroll(butscroll, currentObject)
            }
        }
      if (oSpan) {
          oSpan.onclick = function (e) {
              let oEvent = e || event
              oEvent.cancelBubble = true
            }
        }

        // drag scroll bar
      let iY = 0
      if (oSpan) {
          oSpan.onmousedown = function (e) {
              let oEvent = e || event
              iY = oEvent.clientY - oSpan.offsetTop
              document.onmousemove = function (e1) {
                  oEvent = e1 || event
                  let l = oEvent['clientY'] - iY
                  td(l)
                  return false
                }
              document.onmouseup = function () {
                  document.onmousemove = null
                  document.onmouseup = null
                }
              return false
            }
        }

        // wheel event
      function fuScroll (e) {
          let oEvent = e || event
          let l = oSpan.offsetTop
          oEvent.wheelDelta ? (oEvent.wheelDelta > 0 ?
                l -= iHeight : l += iHeight) : (oEvent.detail > 0 ? l += iHeight : l -= iHeight)
          clickOscroll(l, currentObject)
          if (oEvent.preventDefault) {
              oEvent.preventDefault()
            }
        }

      this.myEvent(oConter, 'mousewheel', fuScroll)
      this.myEvent(oConter, 'DOMMouseScroll', fuScroll)

      function isScrollToBottom () {
          let totalHeight = oScroll.offsetHeight - oSpan.offsetHeight - 15
          let scrollHeight = parseInt(oSpan.style.top, 10)
          if (totalHeight < scrollHeight) {
              return true
            }
        }

        // scroll event
      function oscroll (l, currentObject1) {
          EventService.emit('HIDE_SEARCH_KEY')
          if (isScrollToBottom() && isToBottom === 'isToBottom') {
              EventService.emit('SHOW_ALLCHANNEL_PROGRAM')
            } else if (!isToBottom) {
              EventService.emit('SHOE_PROGRAM')
            }
          if (l < 0) {
              l = 0
            } else if (l > oScroll.offsetHeight - oSpan.offsetHeight) {
              l = oScroll.offsetHeight - oSpan.offsetHeight
            }
          let scrol = l / (oScroll.offsetHeight - oSpan.offsetHeight)
          currentObject1.sMove(oSpan, 'top', Math.ceil(l))
          currentObject1.sMove(oUl, 'top', '-' + Math.ceil((oUl.offsetHeight - oConter.offsetHeight) * scrol))
        }

      function clickOscroll (l, currentObject1) {
          EventService.emit('HIDE_SEARCH_KEY')
          if (isScrollToBottom() && isToBottom === 'isToBottom') {
              EventService.emit('SHOW_ALLCHANNEL_PROGRAM')
            } else if (!isToBottom) {
              EventService.emit('SHOE_PROGRAM')
            }
          if (l < 0) {
              l = 0
            } else if (l > oScroll.offsetHeight - oSpan.offsetHeight) {
              l = oScroll.offsetHeight - oSpan.offsetHeight
            }
          let scrol = l / (oScroll.offsetHeight - oSpan.offsetHeight)
          currentObject1.clickSMove(oSpan, 'top', Math.ceil(l))
          currentObject1.clickSMove(oUl, 'top', '-' + Math.ceil((oUl.offsetHeight - oConter.offsetHeight) * scrol))
        }

      function td (l) {
          if (isScrollToBottom() && isToBottom === 'isToBottom') {
              EventService.emit('SHOW_ALLCHANNEL_PROGRAM')
            } else if (!isToBottom) {
              EventService.emit('SHOE_PROGRAM')
            }
          if (l < 0) {
              l = 0
            } else if (l > oScroll.offsetHeight - oSpan.offsetHeight) {
              l = oScroll.offsetHeight - oSpan.offsetHeight
            }
          let scrol = l / (oScroll.offsetHeight - oSpan.offsetHeight)
          oSpan.style.top = l + 'px'
          oUl.style.top = '-' + (oUl.offsetHeight - oConter.offsetHeight) * scrol + 'px'
        }
    }

  getStyle (obj, attr) {
      return obj.currentStyle ? obj.currentStyle[attr] : getComputedStyle(obj, null)[attr]
    }

  clickSMove (obj, attr, iT, onEnd) {
      this.dMove(obj, attr, iT, onEnd)
    }

  sMove (obj, attr, iT, onEnd) {
      let self = this
      clearInterval(obj.timer)
      obj.timer = setInterval(function () {
          self.dMove(obj, attr, iT, onEnd)
        }, 30)
    }

  dMove (obj, attr, iT, onEnd) {
      let iCur = 0
      attr === 'opacity' ? iCur = parseInt(this.getStyle(obj, attr), 10) * 100
            : iCur = parseInt(this.getStyle(obj, attr), 10)
      let iS = (iT - iCur) / 5
      iS = iS > 0 ? Math.ceil(iS) : Math.floor(iS)
      if (iCur === parseInt(iT, 10)) {
          clearInterval(obj.timer)
          if (onEnd) {
              onEnd()
            }
        } else {
          if (attr === 'opacity') {
              obj.style.ficter = 'alpha(opacity:' + (iCur + iS) + ')'
              obj.style.opacity = (iCur + iS) / 100
            } else {
              obj.style[attr] = iCur + iS + 'px'
            }
        }
    }
}
