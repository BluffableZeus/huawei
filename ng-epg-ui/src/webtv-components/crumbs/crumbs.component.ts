import { Component, OnInit, Input } from '@angular/core'
import { Router } from '@angular/router'

@Component({
  selector: 'crumbs-app',
  templateUrl: './crumbs.component.html',
  styleUrls: ['./crumbs.component.scss']
})

export class CrumbsComponent implements OnInit {
  public routes: any =  []
  private currentHref = ''
  private showPointer = true

  @Input() set setCrumbsRoutes (data) {
      if (data) {
          this.routes = data
        }
    }

  constructor (private router: Router) {
    }

  ngOnInit () {
      this.currentHref = location.href
    }

  nativageCrumbs (route, dataIndex) {
      if (route && route['hash'] && route['hash'] !== '') {
          let ua = navigator.userAgent.toLowerCase()
          let isIe = !!ua.match(/msie/i) || !!ua.match(/rv:([\d.]+).*like gecko/)
          if (ua.indexOf('windows nt 6.3') !== -1 && isIe) {
              let routeHash = !!route['hash'].split('#')[1] ? '.' + route['hash'].split('#')[1] : ''
              this.router.navigate([decodeURIComponent(routeHash)])
              this.currentHref = route['hash']
            } else {
              location.href = route['hash']
              this.currentHref = location.href
            }
        }
    }

  crumbsCursor (dataIndex) {
      let self = this
      let cursor = ''
      if (self.routes.length > 0 && dataIndex === self.routes.length - 1) {
          return 'default'
        }
      for (let i = 0; i < self.routes.length; i++) {
          if (self.routes[i] && self.routes[i]['hash'] && self.routes[i]['hash'] !== '' &&
                    self.routes[i]['hash'] !== this.currentHref) {
              this.showPointer = true
              break
            } else {
              this.showPointer = false
            }
        }
      if (this.showPointer) {
          cursor = 'pointer'
        } else {
          cursor = 'default'
        }
      return cursor
    }
}
