import { TranslateModule } from '@ngx-translate/core'
import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'

import { CrumbsComponent } from './crumbs.component'

@NgModule({
  imports: [CommonModule, TranslateModule],
  exports: [CrumbsComponent],
  declarations: [CrumbsComponent],
  providers: []
})
export class CrumbsModule { }
