import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'

import { VodStill } from './vod-still.component'

@NgModule({
  imports: [CommonModule],
  exports: [VodStill],
  declarations: [VodStill],
  providers: []
})
export class VodStillModule { }
