import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'

@Component({
  selector: 'vod-still',
  styleUrls: ['./vod-still.component.scss'],
  templateUrl: './vod-still.component.html'
})

export class VodStill {
  @Output() child: EventEmitter<Object> = new EventEmitter()
  public vodStills: Array<any>
  @Input() set data (data: Array<string>) {
    this.vodStills = data
    return 
  }
  constructor (private route: Router, private _route: ActivatedRoute) { }

  ngOnInit () {
  }

  stillEnlarge (event) {
    this.child.emit(event)
  }

}
