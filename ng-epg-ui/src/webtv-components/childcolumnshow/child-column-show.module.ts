import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'
import { TranslateModule } from '@ngx-translate/core'
import { ColumnShowComponent } from './childcolumnshow.component'
import { ChildcolumnService } from './childcolumnshow.service'

import { VODSuspensionModule } from '../vodSuspension'
import { NoDataModule } from '../noData'
import { ChannelSuspensionModule } from '../channelSuspension'
import { VerticalPosterModule } from '../verticalPoster'
import { ShowComponent } from 'ng-epg-ui/webtv-components/childcolumnshow/show/show.component'

@NgModule({
  imports: [
      CommonModule,
      VODSuspensionModule,
      VerticalPosterModule,
      NoDataModule,
      TranslateModule,
      ChannelSuspensionModule
    ],
  exports: [ColumnShowComponent],
  declarations: [ColumnShowComponent, ShowComponent],
  providers: [ChildcolumnService]
})
export class ChildColumnShowModule { }
