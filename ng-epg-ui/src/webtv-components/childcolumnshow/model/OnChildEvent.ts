/**
 * Event after user click on VOD in slider
 * left it as array for backward compatibility
 */
export type OnChildEvent = [
    string, //VOD ID
    string, //Subject Name
    string  //Subject ID
]
