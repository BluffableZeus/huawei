import { VodSuspension } from '../../vodSuspension/model/VodSuspension'

export interface HoverEvent {
  event: MouseEvent
  offset: number
  suspension: VodSuspension
}
