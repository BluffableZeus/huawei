/**
 * Event after user click on VOD in slider
 * left it as array for backward compatibility
 */
export type OnJumpToEvent = [
    string, //subject Id
    number, //hasChildren
    string  //subjectName
]
