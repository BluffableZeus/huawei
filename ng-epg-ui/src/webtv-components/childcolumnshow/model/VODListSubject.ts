import { VOD } from 'ng-epg-sdk'

export interface VODListSubject {
  VODList: VOD[]
  subjectName: string
  subjectID: string
  showSubjectName?: string
  hasChildren: number
}
