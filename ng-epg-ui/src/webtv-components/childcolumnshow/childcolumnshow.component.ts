import * as _ from 'underscore'
import { Component, OnInit, Input, EventEmitter, Output, ViewChild } from '@angular/core'
import { SuspensionComponentVod } from '../vodSuspension/vodSuspension.component'
import { HoverEvent, VODListSubject, OnChildEvent } from './model'
import { EventService } from 'ng-epg-sdk'
import { OnJumpToEvent } from 'ng-epg-ui/webtv-components/childcolumnshow/model'
import { Subject } from 'rxjs'
import { VodSuspension } from 'ng-epg-ui/webtv-components/vodSuspension/model/VodSuspension'
import { VODSuspensionServices } from 'ng-epg-ui/webtv-components/vodSuspension'

@Component({
  selector: 'columnshow',
  templateUrl: './childcolumnshow.component.html',
  styleUrls: ['./childcolumnshow.component.scss']
})
export class ColumnShowComponent implements OnInit {
    // show：the right arrow,the underline,the title
  @Input() ratingShow: boolean = true
  @Input() nodatatype: string = 'ondemand'
  @Input() show: boolean[] = [true, true, true]
    // id：suspensionColumn、list、left、right
    //@TODO left it as array for backward compatibility. we use only [0] val now
  @Input() id: string[] = ['defaultSuspensionId']
  @ViewChild(SuspensionComponentVod) suspension: SuspensionComponentVod
  @Output() child: EventEmitter<OnChildEvent> = new EventEmitter<OnChildEvent>()
  @Output() jumpTo: EventEmitter<OnJumpToEvent> = new EventEmitter<OnJumpToEvent>()
  public isShow: boolean = false
  public resizer: Subject<void> = new Subject<void>()
  public datas: VODListSubject[] = []
  public susTime
  public vodListSuspension = []
  public suspensionColumn: string
  public iconShow: boolean = true
  public borderShow: boolean = true
  public titleShow: boolean = true
  public iconHover = []
  public susUse: boolean = true
  public showSuspend = false
  public susFilter = _.debounce((fc: Function) => { fc() }, 300)

  private columnList = {
    'MNovelties': 'Все новинки',
    'MRecommend': 'Все рекомендации',
    'MSeries': 'Все сериалы',
    'MKids': 'Детский мир',
    'AllMovies': 'Все фильмы',
    'MShows': 'Все шоу',
    'MReality': 'Все реалити',
    'MComedy': 'Все комедии',
    'MPremieres': 'Все примьеры',
    'MPopular': 'Больше всего смотрят',
    'counting_watching': 'История просмотров'
  }

  public customIndex = -1

  @Input() set childcolumnshow (data: VODListSubject[]) {
      _.delay(() => {
          this.isShow = false
        }, 250)
      this.customIndex = -1
      this.datas = data

      console.log(data, 'пришло');
    }

  @Input() set suspensiondata (suspensionData) {
      this.vodListSuspension = suspensionData

      _.delay(() => {
          this.isShow = false
        }, 250)
    }

  constructor (private suspensionService: VODSuspensionServices) {
      const d = _.debounce((fc: Function) => { fc() }, 400)
      EventService.on('ScreenSize', () => {
          d(() => {
              this.resizer.next()
            })
        })
    }

  ngOnInit () {
      this.suspensionColumn = this.id[0];
      [this.iconShow, this.borderShow, this.titleShow] = this.show
      this.showSuspend = this.suspensionColumn !== 'UIDemo'
    }

  getColumnName(title: string) {
    
    return this.columnList[title] || 'Перейти';
  }
  moreshow (vodLists: VODListSubject) {
      if (vodLists.VODList && vodLists.VODList.length !== 0) {
          this.jumpTo.emit([
              vodLists.subjectID,
              vodLists.hasChildren,
              vodLists.subjectName
            ])
        }
    }

    //  when the mouse enter the picture,show the suspension of the film.
  mouseEnter (event: HoverEvent) {
    // выключил поведение
    return false;
    this.susUse = true
    this.susFilter(() => {
        this.showOverlay(event)
      })
  }

  protected showOverlay ({ event, suspension, offset }: HoverEvent) {
      if (this.susUse === false || !this.showSuspend) {
          return
        }
      this.isShow = false
      clearTimeout(this.susTime)

      const currentTarget = window.event && window.event.currentTarget['offsetParent'] || event && event.target
      const timeBeforeCall = Date.now()

      EventService.removeAllListeners(['LOADDATASUCCESS'])
      EventService.on('LOADDATASUCCESS', () => {
          this.suspensionService.moveEnter(currentTarget, this.suspensionColumn)
          const timeForCall = Date.now() - timeBeforeCall
          const timeoutTime = (timeForCall < 500) ? 500 - timeForCall + 100 : 100

          this.susTime = setTimeout(() => {
              this.isShow = true
            }, timeoutTime)
        })
      if (currentTarget) {
          this.suspension.setDetail(suspension, this.suspensionColumn, currentTarget.firstElementChild)
        }
    }
  closeDialog (name: string) {
      // выключил поведение
      return false;
      this.susUse = false
      clearTimeout(this.susTime)
      EventService.removeAllListeners(['showSuspension'])
      EventService.removeAllListeners(['closeSuspension'])
      EventService.on('showSuspension', (option) => {
          if (option === name) {
              this.isShow = true
            }
        })
      EventService.on('closeSuspension', () => {
          if (document.getElementById(this.suspensionColumn)) {
              document.getElementById(this.suspensionColumn).style.left = 0 + 'px'
            }
          this.isShow = false
        })
      this.isShow = false
      clearTimeout(this.susTime)
      _.delay(() => {
          if (this.isShow === false && document.getElementById(this.suspensionColumn)) {
              document.getElementById(this.suspensionColumn).style.left = 0 + 'px'
            }
        }, 100)
      EventService.removeAllListeners(['LOADDATASUCCESS'])
      EventService.removeAllListeners(['REMOVE_SUSDATE_successful'])
      EventService.removeAllListeners(['ADD_SUSDATA_successful'])
    }

  onClickDetail (event: OnChildEvent) {
      this.child.emit(event)
    }

  onSuspensionClick (event: VodSuspension) {
      this.onClickDetail([event.floatInfo.ID, event.titleName, ''])
    }

  mouseenterTitle (vodLists: VODListSubject, i) {
      if (vodLists.VODList && vodLists.VODList.length && vodLists.VODList.length !== 0) {
          this.iconHover[i] = true
        }
    }

  closeDialogTitle (vodLists: VODListSubject, i) {
      if (vodLists.VODList && vodLists.VODList.length && vodLists.VODList.length !== 0) {
          this.iconHover[i] = false
        }
    }

  incCustomIndex() {
    this.customIndex++
    return true
  }
}
