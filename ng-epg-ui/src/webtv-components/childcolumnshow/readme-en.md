### Usage
```typescript
import { ColumnShowComponent } from 'ng-epg-ui/webtv-components/childcolumnshow';

<columnshow [childcolumnshow] = "datas" [suspensiondata] = "vodListSuspension" (jumpTo)="moreShow($event)" [show]="recomVodshow"
		[id]="recomVodId" (child)="onClickDetail($event)" ></columnshow>

```

### Annotations
```typescript
export class ColumnShowComponent {
    @Input() id;
    @Input() show: boolean;
    @Input() set childcolumnshow(data) {
        _.delay(()=>{
            this.isShow = 'false';
        }, 250)
        this.datas = data;
        _.each(this.datas, (list) => {
            list['showSubjectName'] = list['subjectName'].toUpperCase();
        });
        this.showIcon();
        this.offsetNumber = [];
        for (let i = 0; i < this.datas.length; i++) {
            this.offsetNumber.push(0);
            this.arrowShow.push(true);
            this.count.push('0');
        }
    }
    @Input() set suspensiondata(suspensionData) {
        this.vodListSuspension = suspensionData;
        _.delay(()=>{
            this.isShow = 'false';
        }, 250)
    }
    @Output() child: EventEmitter<Object> = new EventEmitter();
    @Output() jumpTo: EventEmitter<Object> = new EventEmitter();
}

```

 ### Attributes
| Attribute   | Description   | Type     | Options     | Default Value    | Mandatory (M) or Optional (O)    |
|-----   |-----    |-----    |------      |------    |------    |
| show | get flag arrays that is used to judging whether show right arrow, bottom border, column title | array | [true, true, true], [false, false, false]... |  |  M |
| id | get id for dom elements | array |  |  | M |
| childcolumnshow | get data that is used for VOD posters showing | Array&lt;ColumnData&gt; |  |  |  M |
| suspensiondata | get data that is used for VOD suspension showing | Array&lt;SuspensionDetail&gt; |  |  | O |

 ### Events
| Event   | Description   | Callback Parameter     | Type     | Remarks |
|-----   |-----    |-----    |------      |------    |
| child | click the VOD poster to navigate to VOD playing page. | click event | Event |  |
| jumpTo | click the title of column to navigate to VOD waterfall page.  | click event | Event |  |

 ### Data Type
 #### ColumnData
| Attribute   | Description   | Type    | Default Value    | Mandatory (M) or Optional (O)    |
|-----   |-----    |-----    |------    |------    |
| VODList | VOD列表 | array |  |  M |
| subjectName | 栏目名称 | string |  |  M |
| subjectID | 栏目ID | string |  |  M |
| hasChildren | 提示内容 | boolean |  |  M |

 #### SuspensionDetail
| Attribute   | Description   | Type    | Default Value    | Mandatory (M) or Optional (O)    |
|-----   |-----    |-----    |------    |------    |
| titleName | 悬浮框标题名称 | array |  |  O |
| floatInfo | 悬浮框详细信息 | object |  |  O |