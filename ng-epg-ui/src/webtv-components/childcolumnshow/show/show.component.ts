import {
    Component,
    OnInit,
    Input,
    Output,
    EventEmitter,
    OnDestroy,
    ElementRef,
    AfterViewInit
} from '@angular/core'
import { VodSuspension } from '../../vodSuspension/model'
import { ChildcolumnService } from '../childcolumnshow.service'
import { VODListSubject, OnChildEvent } from '../model'
import { HoverEvent } from '../model'
import { VOD } from 'ng-epg-sdk'
import { Subject, Subscription } from 'rxjs'

@Component({
  selector: 'single-show',
  templateUrl: './show.component.html',
  styleUrls: ['./show.component.scss'],
  providers: [ChildcolumnService]
})
export class ShowComponent implements OnInit, OnDestroy, AfterViewInit {
  @Input() ratingShow: boolean = true
  @Input() iconShow: boolean = true
  @Input() suspension: VodSuspension[]
  @Input() data: VODListSubject
  @Input() nodatatype: string = 'ondemand'
  @Input() resizer = new Subject()

  @Output() enterElement: EventEmitter<HoverEvent> = new EventEmitter<HoverEvent>()
  @Output() leaveElement: EventEmitter<HoverEvent> = new EventEmitter<HoverEvent>()
  @Output() select: EventEmitter<OnChildEvent> = new EventEmitter<OnChildEvent>()

  static BIG_SLIDE = 6
  static SMALL_SLIDE = 5
  static LARGE_SCREEN_WIDTH = 1310

  offsetNumber: number = 0
  panel: number = 0
  private posterWidth = 0
  private sliderWidth = 0
  private offsetCount = 0

  previousPageAvailable = false
  nextPageAvailable = false

  protected isLargeScreen: boolean

  private _page: number = 0
  private sub: Subscription

  get page (): number {
      return this._page
    }

  set page (value: number) {
      this._page = value
      this.offsetNumber = -value * this.panel
    }

  get totalPages () {
      if (this.data && this.data.VODList) {
          return Math.ceil(this.data.VODList.length / this.getImagesPerPage())
        }

      return 1
    }

  constructor (private el: ElementRef) {}

  public ngAfterViewInit (): void {
      const slider = this.el.nativeElement.querySelector('.slider')
      if (slider) {
          this.sliderWidth = slider.offsetWidth
        }

      this.posterWidth = this.getPosterSize()
      this.offsetNumber = -this.posterWidth * this.offsetCount
      this.isShowIcon()

      //let list = document.getElementsByClassName('list')
      //for (let i = 0; i < list.length; i++) {
          //let hammerTime = new Hammer(list[i] as HTMLElement)
          //hammerTime.get('swipe').set({ direction: Hammer.DIRECTION_HORIZONTAL })
          //hammerTime.get('swipe').set({ threshold: 0 })
        //}
    }

  public ngOnInit (): void {
      this.isLargeScreen = window.innerWidth > ShowComponent.LARGE_SCREEN_WIDTH
      this.panel = this.getPanelSize()

      this.isShowIcon()
      this.sub = this.resizer.subscribe(() => {
          const prevIsLarge = this.isLargeScreen
          this.isLargeScreen = window.innerWidth > ShowComponent.LARGE_SCREEN_WIDTH

          if (this.isLargeScreen !== prevIsLarge) {
              this.panel = this.getPanelSize()

              this.page = Math.floor(this.page * this.getImagesPerPage(prevIsLarge) / this.getImagesPerPage())
            }
          this.posterWidth = this.getPosterSize()
          this.offsetNumber = -this.posterWidth * this.offsetCount
          this.isShowIcon()
        })
    }

  public ngOnDestroy (): void {
      if (this.sub) {
          this.sub.unsubscribe()
        }
    }

    /**
     * previous page of slider via arrows
     */
  public onClickLeft (): void {
      this.offsetCount--
      this.offsetNumber += this.posterWidth
      this.isShowIcon()
    }

    /**
     * previous page of a slider swiping on a mobile device
     */
  public onSwipeLeft (): void {
      if (this.page > 0) this.page--
    }

    /**
     * next page of slider via arrows
     */
  public onClickRight (): void {
      if (this.posterWidth < 30) {
        this.posterWidth = this.getPosterSize()
      }
      this.offsetCount++
      this.offsetNumber -= this.posterWidth
      this.isShowIcon()
    }

    /**
     * next page of a slider swiping on a mobile device
     */
  public onSwipeRight (): void {
      if (this.page < this.totalPages) this.page++
    }

  public showOverlay (event: MouseEvent, index: number): void {
      this.enterElement.emit({
          event,
          offset: this.offsetNumber,
          suspension: this.getSuspension(index)
        })
    }

  public hideOverlay (event: MouseEvent, index: number): void {
      this.leaveElement.emit({
          event,
          offset: this.offsetNumber,
          suspension: this.getSuspension(index)
        })
    }

  public onClickDetail (vod: VOD): void {
      this.select.emit([vod.ID, this.data.subjectName, this.data.subjectID])
    }

  private getImagesPerPage (isLarge: boolean = this.isLargeScreen): number {
      return isLarge ? ShowComponent.BIG_SLIDE : ShowComponent.SMALL_SLIDE
    }

  private getPanelSize (isLarge: boolean = this.isLargeScreen): number {
      if (window.innerWidth < 1120) {
          return window.innerWidth
        } else {
          return isLarge ? 1296 : 1000
        }
    }

  private getPosterSize (): number {
      const slider = this.el.nativeElement.querySelector('.slider')
      if (slider) {
          this.sliderWidth = slider.offsetWidth
        }
      const poster = document.querySelector('vod-poster.item')
      if (poster) {
          const style = window.getComputedStyle ? getComputedStyle(poster, null) : (poster as any).currentStyle
          return (poster as any).offsetWidth + (parseInt(style.marginRight) || 0)
        }

      return 0
    }

  private isShowIcon () {
      this.previousPageAvailable = this.offsetNumber < 0
      this.nextPageAvailable = (this.posterWidth * (this.data.VODList.length - 1) + this.offsetNumber) > this.sliderWidth
    }

  private getSuspension (index: number): VodSuspension {
      if (this.suspension) {
          return this.suspension[index]
        } else {
          return {
              floatInfo: this.data.VODList[index],
              titleName: this.data.subjectName
            }
        }
    }
}
