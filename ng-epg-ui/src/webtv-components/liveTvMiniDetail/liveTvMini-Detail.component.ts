import * as _ from 'underscore'
import { Component, OnInit, Input, ViewChild, OnDestroy } from '@angular/core'
import { EventService } from '../../../demos-app/sdk/event.service'
import { TranslateService } from '@ngx-translate/core'

@Component({
  selector: 'app-live-mini-detail',
  styleUrls: ['./liveTvMini-Detail.component.scss'],
  templateUrl: './liveTvMini-Detail.component.html'
})

export class LiveMiniDetailComponent implements OnInit, OnDestroy {
    // @ViewChild(WeakTipDialogComponent) weakTipDialog: WeakTipDialogComponent;
    // declare variables.
    // keep the channel number.
  private channelNo: string = ''
    // keep the channel logo.
  private logo: string = ''
    // this channel logo is not display,is used to get when channel logo's src path or pic doesn't exist
  private hiddenLogo: string = ''
    // keep the programmme name.
  private name: string = ''
    // keep the channel name.
  private channel: string = ''
    // '0' is unlocked,'1' is locked.
  private isLock: string = ''
    // '0' is not favorite,'1' is favorite.
  private isFav: string = ''
    // keep the channel genre.
  private genre: string = ''
    // keep the week of the programmme.
  private weekTime: string = ''
    // keep the date of the programmme.
  private leftTime: string = ''
    // keep the start time of the programmme.
  private startTime: string = ''
    // keep the end time of the programmme.
  private endTime: string = ''
    // keep the series.
  private series: string = ''
    // keep the introduce.
  private introduce: string = ''
    // keep the rating.
  private rating: string = ''
    // keep the title of the recomend list.
  private recomendListTitle: string = ''
    // true is showing all the introduce,false is showing only 3 rows introduce.
  private showMore: boolean = false
    // true is showing the mini detail page,false is hiding the mini detail page.
  private showDetail: boolean = true
    // keep the text of play buttom.
  private btnName: string = ''
    // keep the text of remind buttom.
  private btnNameRemind: string = ''
    // keep the text of record buttom.
  private btnNameRecord: string = ''
    // keep the channel id.
  private contentID: string = ''
    // true is the programme is in future,false is the programme is in the past.
  private isFuture: boolean = false
    // true is the programme is supported to back to see,false is the programme is not supported.
  private isCUTV: boolean = false
    // 0 is the user is logined.
  private profileType
    // keep the channel detail.
  private triggerAddFavData: Object
    // keep the channel detail and the programme detail.
  private _data: Object
    // true is the information belongs to the Recmlist,false is the information belongs to the TVGuide.
  private isRecmInfo = false
    // true is show the record buttom,false is hide the record buttom.
  private isRecord: boolean = false
    // keep the icons of the record.
  private recordsImgStyle = {}
    // keep the icons of the remind.
  private remindImgStyle = {}
    // true is show the scroll bar,false is hide the scroll bar.
  private showScroll = true
    // keep the progamme id.
  private id: string = ''
    // true is show the remind buttom,false is hide the remind buttom.
  private isShowReminder: boolean = false
    // the timer for setting the arrow of introduce.
  private getIntervalTip
    //  the height of introduce content.
  private maxOffsetHeight
    // true is show the arrow of introduce,false is hide the arrow of introduce.
  private showHeight: boolean = false
    // keep the the scroll bar's height according to the window size.
  private scrollHeight
    // get playbill data
  private isFirefox = false
  private canTvod = false
  @Input() set minidata (data: Object) {
      this._data = data[0]
      this.triggerAddFavData = data[1]
      this.showDetail = true
      let type = undefined
      if (type) {
          this.isRecord = true
          this.btnNameRecord = type
          if (this.btnNameRecord === 'livetv_record') {
                // has not been recorded.
              this.recordsImgStyle = {
                  background: 'url(assets/img/14/record_14.png)'
                }
            } else {
                // has been recorded.
              this.recordsImgStyle = {
                  background: 'url(assets/img/14/stoprecording_14.png)'
                }
            }
        }
        // judge the reminder's status
      this.id = this._data['playbillDetail']['ID']
      if (this._data['playbillDetail']['reminder']) {
            // has been remindered.
          this.btnNameRemind = 'cancel_reminder'
          this.remindImgStyle = {
              background: 'url(assets/img/14/cancelreminder_14.png)'
            }
        } else {
            // has not been remindered.
          this.btnNameRemind = 'set_reminder'
          this.remindImgStyle = {
              background: 'url(assets/img/14/reminder_14.png)'
            }
        }
    }
    // private constructor
  constructor (
        private translate: TranslateService
    ) { }
  minidetailTwice (seasonNO, sitcomNO) {
      this.series = this._data['Series'] && this._data['Series'].seasonNO && this._data['Series'].seasonNO
            && seasonNO ? this.translate.instant('vod_season', { num: seasonNO }) : 'seasons 2'
      this.setSeries(sitcomNO)
      this.introduce = this._data['introduce']
      this.rating = this._data && this._data['rating'] ? this._data['rating'] : '16PG'
      this.contentID = this._data['playbillDetail'].channelDetail.ID
      let recmContents
      recmContents = this._data['recmContents'] ? this._data['recmContents'] : '0'
      this.recomendListTitle = 'Similar Content' + '(' + recmContents + ')'
    }

  setSeries (sitcomNO) {
      if (this.series !== '') {
          this.series = this._data['Series'] && this._data['Series'].sitcomNO && this._data['Series'].sitcomNO
                && sitcomNO ? this.series +
                ', ' + this.translate.instant('episode', { num: sitcomNO }) + ' ' : this.series + ''
        } else {
          this.series = this._data['Series'] && this._data['Series'].sitcomNO && this._data['Series'].sitcomNO
                && sitcomNO ? this.series +
                this.translate.instant('episode', { num: sitcomNO }) + ' ' : this.series + ''
        }
    }

  minidetail () {
        // this.profileType = Cookies.get('PROFILE_TYPE');
      this.channelNo = this._data['channelNo']
      this.logo = 'url(' + this._data['logo'] + ') 0% 0% / cover'
      this.hiddenLogo = this._data['logo']
      this.name = this._data['name']
      this.channel = this._data['channel']
      this.isLock = this._data['isLock'] ? this._data['isLock'] : '0'
      this.isFav = this._data['isFav'] ? this._data['isFav'] : '0'
      this.genre = (this._data['genre'] && this._data['genre'].length) > 0 ? ' | ' +
                    this._data['genre'][0]['genreName'] : '16PG'
      this.weekTime = this._data['weekTime']
      this.leftTime = this._data['leftTime']
      this.startTime = this._data['startTime']
      this.endTime = this._data['endTime']
      let seasonNO = ''
      let sitcomNO = ''
        //  if the seasonNO is one number,add zero ahead.
      if (this._data['Series'] && this._data['Series'].seasonNO && this._data['Series'].seasonNO < 10) {
          seasonNO = '0' + this._data['Series'].seasonNO
        }
        //  if the sitcomNO is one number,add zero ahead.
      if (this._data['Series'] && this._data['Series'].sitcomNO && this._data['Series'].sitcomNO < 10) {
          sitcomNO = '0' + this._data['Series'].sitcomNO
        }
      this.minidetailTwice(seasonNO, sitcomNO)
    }
  ngOnInit () {
        // deal with the playbill data.
      let self = this
      this.minidetail()
      this.isFuture = this._data['isFuture']
      if (this._data['isCUTV'] === '0') {
          this.isCUTV = false
        } else {
          this.isCUTV = true
        }
        // get the recomend list.
        // set the scroll hight according to the window size
      if (document.getElementById('channel-list')) {
          this.scrollHeight = document.getElementById('channel-list')['offsetHeight'] - 80 + 'px'
        }
        // reset the arrowhead of the introduce
      if (document.getElementById('introduce')) {
          let introduce = document.getElementById('introduce')
          introduce['style']['height'] = '63px'
          introduce['style']['-webkit-line-clamp'] = '3'
          this.showMore = false
          this.showScroll = false
        }
        // reset the scroll postion
      if (document.getElementById('scrollContent')) {
          document.getElementById('scrollContent')['style']['top'] = '0px'
        }
      if (document.getElementById('mini-livetv-scroll')) {
          document.getElementById('mini-livetv-scroll')['style']['top'] = '0px'
        }
      if (document.getElementById('mini-livetv-contentScroll')) {
          document.getElementById('mini-livetv-contentScroll')['style']['top'] = '0px'
        }
        // self.pluseOnScroll();
        // monitor the event of  adding record successfull and change the record's status
      EventService.on('RECORD_ADD_SUCCESS', function (type) {
          self.btnNameRecord = 'cancel_record'
          self.recordsImgStyle = {
              background: 'url(assets/img/14/stoprecording_14.png)'
            }
        })
      this.getIsReminder(this._data['playbillDetail'])
        // deal the arrowhead of the introduce
      clearInterval(this.getIntervalTip)
      this.getIntervalTip = setInterval(() => {
          if ((this.introduce.length > 0) && document.getElementById('introduce')) {
              self.showMoreHeight()
              clearInterval(this.getIntervalTip)
            }
        }, 10)
      let ua = navigator.userAgent.toLowerCase()
      this.isFirefox = !!ua.match(/firefox\/([\d.]+)/)
      this.judgePlayOrSubscribe(this._data)
    }

  judgePlayOrSubscribe (resp) {

    }

  getNumber (length) {
      return Number(length) * 1000
    }

  ngOnDestroy () {
        // clear the timer.
      clearInterval(this.getIntervalTip)
    }
    // when channel logo's src path or the pic does not exist,there show default picture
  showDefaultLogo () {
      document.getElementsByClassName('detailLogo')[0]['style']['background']
            = 'url(assets/img/default/minidetail_channel_poster.png)'
    }
  showDetailLogo () {
      return {
          'background': this.logo
        }
    }
  showMoreHeight () {
      let self = this
      if (document.getElementById('introduceShowIcon')) {
          this.maxOffsetHeight = document.getElementById('introduceShowIcon').offsetHeight
        }
      let introduce = document.getElementById('introduce')
      let intOffsetHeight
      if (introduce) {
          intOffsetHeight = introduce.offsetHeight || 0
          if (this.introduce.length > 0 && (intOffsetHeight === 0)) {
            } else {
              if (this.maxOffsetHeight <= 63) {
                    // when the row number of introduce content is less then 3,the arrowhead disappear
                  this.showHeight = false
                } else {
                    // when the row number of introduce content is more then 3,the arrowhead display
                  self.showHeight = true
                  if (self.showMore) {
                        // show all introduce content
                      introduce['style']['height'] = 'auto'
                      introduce['style']['-webkit-line-clamp'] = 'inherit'
                    } else if (!self.showMore) {
                        // show 3 rows of the introduce content
                      introduce['style']['height'] = '63px'
                      introduce['style']['-webkit-line-clamp'] = '3'
                    }
                }
            }
        }
    }
    // roll the scroll event
  pluseOnScroll () {
      let oConter, oUl, oScroll, oSpan
      let oBox = document.getElementById('miniDetail')
      oConter = document.getElementById('content-wrap')
      oUl = document.getElementById('scrollContent')
      oScroll = document.getElementById('mini-livetv-scroll')
      oSpan = oScroll.getElementsByTagName('span')[0]
        // judge the scroll show
      if (this['recomendlist'].length > 4 || oUl['offsetHeight'] > oConter['offsetHeight']) {
          this.showScroll = true
        } else if (oUl['offsetHeight'] <= oConter['offsetHeight']) {
          this.showScroll = false
        }
        // deal roll scroll
        // this.directionScroll.setScroll(oBox, oConter, oUl, oScroll, oSpan, true);
    }
    // click the arrowhead and show all introduce
  increaseMore () {
      this.showMore = true
      this.showMoreHeight()
        // this.pluseOnScroll();
    }
    // click the arrowhead and show 3 rows introduce
  discreaseLess () {
      this.showMore = false
      this.showMoreHeight()
        // initial the scroll position
      document.getElementById('scrollContent')['style']['top'] = '0px'
      document.getElementById('mini-livetv-scroll')['style']['top'] = '0px'
      document.getElementById('mini-livetv-contentScroll')['style']['top'] = '0px'
        // this.pluseOnScroll();
    }
    //  close mini detail page
  closeMiniDetail () {
      this.showDetail = false
      _.delay(function () {
          EventService.emit('CLOSE_MINIDETAIL')
        }, 1100)
    }
    // click lock icon to add or delete lock
  lockManger () {
      let self = this
      if (this.isRecmInfo) {
          if (self.isLock === '1') {
              this._data['playbillDetail']['channelDetail']['isLocked'] = '1'
            } else if (self.isLock === '0') {
              this._data['playbillDetail']['channelDetail']['isLocked'] = '0'
            }
          EventService.emit('LOCK_MANGER', this._data['playbillDetail'])
        } else {
          if (self.isLock === '1') {
              this.triggerAddFavData['channelInfos']['isLocked'] = '1'
            } else if (self.isLock === '0') {
              this.triggerAddFavData['channelInfos']['isLocked'] = '0'
            }
          EventService.emit('LOCK_MANGER', this.triggerAddFavData)
        }
      EventService.on('ADD_LOCK', function (data) {
          self.isLock = '1'
          EventService.emit('ADDLOCK_successful', {})
        })
      EventService.removeAllListeners(['REMOVE_LOCK'])
      EventService.on('REMOVE_LOCK', function (data) {
          self.isLock = '0'
          EventService.emit('UNLOCK_successful', {})
        })
    }
    // click favorite icon to add or delete favorite
  favManger () {
      let self = this
      if (this.isRecmInfo) {
          if (self.isFav === '1') {
              this._data['playbillDetail']['channelDetail']['favorite'] = true
            } else if (self.isFav === '0') {
              this._data['playbillDetail']['channelDetail']['isLocked'] = false
            }
          EventService.emit('FAVORITE_MANGER', this._data['playbillDetail'])
        } else {
          if (self.isFav === '1') {
              this.triggerAddFavData['channelInfos']['favorite'] = true
            } else if (self.isFav === '0') {
              this.triggerAddFavData['channelInfos']['favorite'] = false
            }
          EventService.emit('FAVORITE_MANGER', this.triggerAddFavData)
        }
      EventService.on('ADD_FAVORITE', function (data) {
          self.isFav = '1'
          EventService.emit('ADD_successful', {})
        })
      EventService.on('REMOVE_FAVORITE', function (data) {
          self.isFav = '0'
          EventService.emit('REMOVE_successful', {})
        })
    }
    // click the poster or playbill name and show the mini detail of the program
  getRecmDetail (list) {
      if (list.hasProcess) {
          EventService.emit('PLAYCHANNEL', list.channelID)
          return
        }
    }
  refeshStroage (channelDetail) {

    }
    // click play icon and play
  goToPlay () {

    }
    // click record button
  goToRecord (type) {

    }
    // click reminder button
  goToRemind (type) {

    }
    // reminder button status(reminder or cancel reminder)
  getIsReminder (playbill) {

    }
  showWidth () {
        // deal with the the content location change.
      if (!this.isFirefox) {
          if (document.getElementById('miniDetail') && document.getElementById('miniDetail')['offsetLeft']
                && document.getElementById('miniDetail')['offsetLeft'] === 952
                || document.getElementById('miniDetail')['offsetLeft'] === 552) {
              let styles = document.querySelector('.introduce')['style']['-webkit-box-direction']
              if (styles === 'reverse') {
                  document.querySelector('.introduce')['style']['-webkit-box-direction'] = 'normal'
                } else {
                  document.querySelector('.introduce')['style']['-webkit-box-direction'] = 'reverse'
                }
            }
        }
    }
}
