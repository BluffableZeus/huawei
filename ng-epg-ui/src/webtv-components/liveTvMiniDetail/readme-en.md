### Usage
```typescript
import { LiveMiniDetailComponent } from 'ng-epg-ui/webtv-components/episodeTrailer';

<app-live-mini-detail [minidata]="miniDetailList"></app-live-mini-detail>

```

### Annotations
```typescript
export class LiveMiniDetailComponent {
    @Input() set minidata(data: Object) {
        this._data = data[0];
        this.triggerAddFavData = data[1];
        this.showDetail = true;
        let type = undefined;
        if (type) {
            this.isRecord = true;
            this.btnNameRecord = type;
            if (this.btnNameRecord === 'livetv_record') {
                // has not been recorded.
                this.recordsImgStyle = {
                    background: 'url(assets/img/14/record_14.png)'
                };
            } else {
                // has been recorded.
                this.recordsImgStyle = {
                    background: 'url(assets/img/14/stoprecording_14.png)'
                };
            }
        }
        // judge the reminder's status
        this.id = this._data['playbillDetail']['ID'];
        if (this._data['playbillDetail']['reminder']) {
            // has been remindered.
            this.btnNameRemind = 'cancel_reminder';
            this.remindImgStyle = {
                background: 'url(assets/img/14/cancelreminder_14.png)'
            };
        } else {
            // has not been remindered.
            this.btnNameRemind = 'set_reminder';
            this.remindImgStyle = {
                background: 'url(assets/img/14/reminder_14.png)'
            };
        }
    };
}

```

 ### Attributes
| Attribute   | Description   | Type     | Options     | Default Value    | Mandatory (M) or Optional (O)    |
|-----   |-----    |-----    |------      |------    |------    |
| minidata | A list of details to show, a collection of program detailed data  | object |  |  |  M |