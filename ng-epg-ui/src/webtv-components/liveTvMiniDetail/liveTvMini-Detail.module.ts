import { TranslateModule } from '@ngx-translate/core'
import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'
import { PictureService } from 'ng-epg-ui/services/picture.service'

import { LiveMiniDetailComponent } from './liveTvMini-Detail.component'

@NgModule({
  imports: [CommonModule, TranslateModule],
  exports: [LiveMiniDetailComponent],
  declarations: [LiveMiniDetailComponent],
  providers: [PictureService]
})
export class liveTvMiniDetailModule { }
