export class PromiseWrapper {
  static completer () {
      let resolve, reject
      let promise = new Promise((res, rej) => {
          resolve = res
          reject = rej
        })
      return {
          promise,
          resolve,
          reject
        }
    }
}

export interface PromiseCompleter<T> {
  promise: Promise<T>
  resolve: Function
  reject: Function
}
