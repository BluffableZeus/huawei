import { Directive, Input, OnInit, OnDestroy } from '@angular/core'
import { EventService } from '../../../demos-app/sdk/event.service'

export interface EventAndListener {
  (event: string | string[], ...values: any[]): void
}
@Directive({
  selector: '[on-notification]'
})
export class OnNotificationDirective implements OnInit, OnDestroy {

  @Input('on-notification')
    private _onNotification: EventAndListener

  constructor () {
    }

  ngOnInit () {
      EventService.onAny(this._onNotification)
    }

  ngOnDestroy () {
      EventService.offAny(this._onNotification)
    }
}
