import { NgModule } from '@angular/core'

import { OnNotificationDirective }   from './on-notification.directive'

@NgModule({
  imports: [],
  exports: [OnNotificationDirective],
  declarations: [OnNotificationDirective],
  providers: []
})
export class UISharedModule { }
