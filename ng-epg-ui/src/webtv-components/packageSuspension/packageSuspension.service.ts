import { Injectable } from '@angular/core'
import { Config as SDKConfig } from '../../../demos-app/sdk/config'

@Injectable()
export class SuspensionFac {

  constructor () { }

  timeFormat (timeData) {
      let times = timeData || ''
      let year = times.substring(0, 4)
      let month = times.substring(4, 6)
      let data = times.substring(6, 8)
      return data + '.' + month + '.' + year
    }

  definitionFormat (definition) {
      switch (definition) {
          case '0':
            return 'SD'
          case '1':
            return 'HD'
          case '2':
            return '4K'
          default:
            return '4K'
        }
    }
}
