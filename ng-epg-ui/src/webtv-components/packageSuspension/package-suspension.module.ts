import { CommonModule } from '@angular/common'
import { TranslateModule } from '@ngx-translate/core'
import { NgModule } from '@angular/core'

import { SuspensionComponentPackage } from './packageSuspension.component'
import { DirectionScroll } from '../scroll/directionScroll.component'
import { SuspensionFac } from './packageSuspension.service'

@NgModule({
  imports: [CommonModule, TranslateModule],
  exports: [SuspensionComponentPackage],
  declarations: [SuspensionComponentPackage],
  providers: [SuspensionFac, DirectionScroll]
})
export class PackageSuspensionModule { }
