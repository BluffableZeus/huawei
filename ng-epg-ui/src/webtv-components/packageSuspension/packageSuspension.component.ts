import * as _ from 'underscore'
import { Component, OnInit } from '@angular/core'
import { TranslateService } from '@ngx-translate/core'
import { SuspensionFac } from './packageSuspension.service'
import { EventService } from '../../../demos-app/sdk/event.service'
import { DirectionScroll } from 'ng-epg-ui/webtv-components/scroll/directionScroll.component'
import { CommonSuspensionService } from 'ng-epg-ui/webtv-components/channelSuspension/channelCommon.service'
@Component({
  selector: 'packageSuspension',
  templateUrl: './packageSuspension.component.html',
  styleUrls: ['./packageSuspension.component.scss']
})

export class SuspensionComponentPackage implements OnInit {
  public suspensionInfo = []
  public isShow: string = 'false'
  public leftImgStyle = {}
  public rightImgStyle = {}
  public leftStyle = {}
  public leftTop = ''
  public rightTop = ''
  public titleName: string = ''
  public time: string = ''
  public introduce: string = ''
  public validDuration: string = ''
  public flagInfo: string = ''
  public textAreaheight = 0
  public adjustTime

  setDetail (vodDetail, id, currentTarget) {
      let self = this
      clearTimeout(self.adjustTime)
      EventService.emit('HIDESEARCH')
      this.titleName = vodDetail.productName
      this.time = vodDetail.endTimeStr
      this.introduce = vodDetail.productDesc
    }

  constructor (
        private translate: TranslateService,
        private suspensionFac: SuspensionFac,
        private directionScroll: DirectionScroll,
        private CommonSuspensionService: CommonSuspensionService
    ) {

    }

  ngOnInit () {
      this.pluseOnScroll()
    }

  mouseenter () {
      EventService.emit('showPackageSuspension', this.flagInfo)
    }

  mouseleave () {
      EventService.emit('closePackageSuspension')
    }

  pluseOnScroll () {
      let oBox, oConter, oUl, oScroll, oSpan
      oBox = this.dom('suspensionBig')
      oConter = this.dom('contents')
      oUl = this.dom('detail-content')
      oScroll = this.dom('pack-scroll')
      oSpan = oScroll.getElementsByTagName('span')[0]
      this.directionScroll.setScroll(oBox, oConter, oUl, oScroll, oSpan, true)
    }

  dom (divName: string) {
      return document.getElementById(divName)
    }
}
