import { Injectable } from '@angular/core'
import { Config as SDKConfig } from '../../../demos-app/sdk/config'

@Injectable()
export class RecordSuspensionService {

  constructor () { }

  timeFormat (timeData) {
      let times = timeData || ''
      let year = times.substring(0, 4)
      return year
    }

  definitionFormat (definition) {
      switch (definition) {
          case '0':
            return 'SD'
          case '1':
            return 'HD'
          case '2':
            return ''
          default:
            return ''
        }
    }
}
