import { TranslateModule } from '@ngx-translate/core'
import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'

import { RecordSuspensionService } from './recordSuspension.service'
import { RecordSuspensionComponent } from './recordSuspension.component'

@NgModule({
  imports: [
      CommonModule,
      TranslateModule
    ],
  exports: [RecordSuspensionComponent],
  declarations: [RecordSuspensionComponent],
  providers: [RecordSuspensionService]
})
export class RecordSuspensionModule { }
