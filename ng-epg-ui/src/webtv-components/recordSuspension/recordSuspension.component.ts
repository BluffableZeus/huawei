import * as _ from 'underscore'
import { Component } from '@angular/core'
import { TranslateService } from '@ngx-translate/core'
import { RecordSuspensionService } from './recordSuspension.service'
import { EventService } from '../../../demos-app/sdk/event.service'
import { session } from '../../../demos-app/sdk/session'
import { CommonSuspensionService } from 'ng-epg-ui/webtv-components/channelSuspension/channelCommon.service'

@Component({
  selector: 'recordSuspension',
  templateUrl: './recordSuspension.component.html',
  styleUrls: ['./recordSuspension.component.scss']
})

export class RecordSuspensionComponent {
  public titleName
  public introduce
  public definition
  public flagInfo
  public rightTop
  public leftTop
  public rightImgStyle
  public leftImgStyle
  public leftStyle
  public programGenre
  public ratingName
  public series
  public isSeriesRecord = false
  public isAuto = true
  public isCloud = true
  public isSeriesPage
  public recordDetails
  public isSTB = false
  public recordingMode
  public isHD
  public isSD
  public is4K
  public hasSetButton
  public hasPlayButton
  public hasStopButton
  public espiesds
  public data
  public repeat
  public introduceEllipsis: boolean = false
  public channelName = ''
  public policyType = ''
  public isSearchRecord
  public adjustTime
  public searchAll: boolean = false
  public onRecord: boolean = false
  public ButtonsTop
  public oneButtonTop

  constructor (
        private suspensionFac: RecordSuspensionService,
        private translate: TranslateService,
        private CommonSuspensionService: CommonSuspensionService
    ) {
    }

  setDetail (recordDetails, ID, realCurrentTarget?) {
      let self = this
      self.ButtonsTop = realCurrentTarget && ((1.1 * realCurrentTarget.offsetHeight - 80) / 2 + 'px')
      self.oneButtonTop = realCurrentTarget && ((1.1 * realCurrentTarget.offsetHeight - 30) / 2 + 'px')
      self.searchAll = (ID === 'suspensionSearchPVR') ? true : false
      this.policyType = recordDetails.policyType
      this.channelName = recordDetails.channelName
      this.hasPlayButton = false
      this.hasStopButton = false
      this.hasSetButton = false
      this.isSearchRecord = recordDetails['isSearchRecord']
      self.onRecord = (self.isSearchRecord !== 'isSearchRecord') ? true : false
      this.is4K = false
      this.isHD = false
      this.isSD = false
      this.recordDetails = recordDetails
      this.isSeriesRecord = false
      this.isAuto = true
      this.isCloud = true
      this.isSeriesPage = false
      this.isSTB = false
      this.recordingMode = recordDetails.recordingMode
      clearTimeout(self.adjustTime)
      this.manageSetButton(recordDetails)
      this.manageStopButton(recordDetails)
      this.managePlayButton(recordDetails)
      if (recordDetails.status === 'WAIT_RECORD') {
          this.isCloud = false
        }
      EventService.emit('HIDESEARCH')
      let recordDetail = recordDetails.floatInfo
      if (recordDetails.policyType === 'Series' || recordDetails.policyType === 'Period') {
          this.isSeriesRecord = true
          if (recordDetails.policyType === 'Series') {
              if (recordDetails.subtaskLength < 2) {
                  this.espiesds = recordDetails.subtaskLength + ' ' + this.translate.instant('record_episode')
                } else {
                  this.espiesds = recordDetails.subtaskLength + ' ' + this.translate.instant('record_episodes')
                }
              if (recordDetails.status === 'WAIT_RECORD') {
                  let time = new Date(Date.now()['getTime']() + Number(session.get('REC_PLAYBILL_LEN')) * 24 * 3600 * 1000)
                  let year = Date.now()['getFullYear']()
                  let month = this.addZero(Number(time['getMonth']()) + 1)
                  let day = Number(time['getDate']())
	 	                    month = this.dealWithTime(month)
                  if (session.get('languageName') === 'zh') {
                      this.data = this.translate.instant('record_fromtoday_to') + ' '
                         + year + '/' +  month + '/' + day
                    } else {
                      this.data = this.translate.instant('record_fromtoday_to') + ' '
                         + month + ' ' +  day + ', ' + year
                    }
                } else {
                  this.data = ''
                }
            } else {
              if (recordDetails.subtaskLength < 2) {
                  this.espiesds = recordDetails.subtaskLength + ' ' + this.translate.instant('record_sub-task')
                } else {
                  this.espiesds = recordDetails.subtaskLength + ' ' + this.translate.instant('record_sub-tasks')
                }
              if (recordDetails.status === 'WAIT_RECORD') {
                  let effectiveDate = ''
                  let startTime = Number(recordDetails.floatInfo.startTime)
                  if (startTime < Date.now()['getTime']()) {
                      effectiveDate = this.translate.instant('today')
                    } else {
                      effectiveDate = this.dealWithDay(recordDetails.floatInfo.effectiveDate)
                    }
                  let expireDate = recordDetails.floatInfo.expireDate
                  if (expireDate === '20991231') {
                      expireDate = this.translate.instant('unlimited')
                    } else {
                      expireDate = this.dealWithDay(recordDetails.floatInfo.expireDate)
                    }
                  this.data = this.translate.instant('record_from') + ' ' + effectiveDate + ' ' +
                        this.translate.instant('record_to') + ' ' + expireDate
                } else {
                  this.data = ''
                }
              if (!_.isUndefined(recordDetails.floatInfo['days']) && recordDetails.floatInfo['days'].length > 0) {
                  for (let i = 0; i < recordDetails.floatInfo['days'].length; i++) {
                      recordDetails.floatInfo['days'][i] = Number(recordDetails.floatInfo['days'][i])
                    }
                }
              this.repeat = this.dealwidthDay(recordDetails.floatInfo.days)
            }
        }
      if (recordDetails.policyType === 'TimeBased' || recordDetails.policyType === 'Period') {
          this.isAuto = false
        }
      self.flagInfo = recordDetails.titleName
      this.titleName = recordDetails.name
      if (recordDetail.definition) {
          for (let i = 0; i < recordDetail.definition.length; i++) {
              if (Number(recordDetail.definition[i]) === 0) {
                  this.isSD = true
                }
              if (Number(recordDetail.definition[i]) === 1) {
                  this.isHD = true
                }
              if (Number(recordDetail.definition[i]) === 2) {
                  this.is4K = true
                }
            }
        }
      this.programGenre = recordDetails.genres ? recordDetails.genres : ' /'
      this.ratingName = recordDetails.rating ? recordDetails.rating : ' /'
      this.series = recordDetails.seriesName
      this.introduce = recordDetails.introduce
      if (document.getElementById('contentRecordBg')) {
          document.getElementById('contentRecordBg')['innerHTML'] = recordDetails.introduce
        }
      self.showWidth()
    }

  dealWithTime (month) {
      let Month
      if (month === '01') {
          Month = 'jan_tvguide'
        } else if (month === '02') {
          Month = 'feb_tvguide'
        } else if (month === '03') {
          Month = 'mar_tvguide'
        } else if (month === '04') {
          Month = 'apr_tvguide'
        } else if (month === '05') {
          Month = 'may_tvguide'
        } else if (month === '06') {
          Month = 'jun_tvguide'
        } else if (month === '07') {
          Month = 'jul_tvguide'
        } else if (month === '08') {
          Month = 'aug_tvguide'
        } else if (month === '09') {
          Month = 'sep_tvguide'
        } else if (month === '10') {
          Month = 'oct_tvguide'
        } else if (month === '11') {
          Month = 'nov_tvguide'
        } else if (month === '12') {
          Month = 'dec_tvguide'
        }
      return this.translate.instant(Month)
    }

  addZero (num) {
      let temp = num + ''
      if (temp.length === 1) {
          return '0' + temp
        } else {
          return temp
        }
    }

  timeFormat (times) {
      return this.suspensionFac.timeFormat(times)
    }

  definitionFormat (definition) {
      return this.suspensionFac.definitionFormat(definition)
    }
    // IE browser, the content exceeding, add ellipsis in last line
  showWidth () {
      let ua = navigator.userAgent
      if (ua.indexOf('Chrome') !== -1 && ua.indexOf('Edge') === -1) {
          return
        }
      if (document.getElementById('contentRecordBg')) {
          let contentBg = document.getElementById('contentRecordBg')
          if (60 < contentBg['offsetHeight']) {
              this.introduceEllipsis = true
            } else {
              this.introduceEllipsis = false
            }
        }
    }
    // chrome browser, the content exceeding, add ellipsis in last line
  showContent () {
      let ua = navigator.userAgent
      if (ua.indexOf('Chrome') !== -1 && ua.indexOf('Edge') === -1) {
          let styles = document.querySelector('.recordSuspensionBig .content')['style']['-webkit-box-direction']
          let style = {
            }
          if (styles === 'reverse') {
              style['-webkit-box-direction'] = 'normal'
            } else {
              style['-webkit-box-direction'] = 'reverse'
            }
          return {
              'max-height': '80px',
              '-webkit-line-clamp': '4',
              'word-break': 'normal',
              '-webkit-box-direction': style['-webkit-box-direction']
            }
        }
    }

    // deal with time show of the period task
  dealWithDay (day) {
      let date
      if (day) {
          date = day.substr(6, 2) + '.' + day.substr(4, 2) + '.' + day.substr(0, 4)
          let lastDay = day.substr(6, 2)
          if (lastDay.substr(0, 1) === '0') {
              lastDay = day.substr(7, 1)
            }
          let lastMonth = day.substr(4, 2)
          let lastYear = day.substr(0, 4)
          lastMonth = this.dealWithTime(lastMonth)
          if (session.get('languageName') === 'zh') {
              date = lastYear + '/' +  lastMonth + '/' + lastDay
            } else {
              date = lastMonth + ' ' +  lastDay + ', ' + lastYear
            }
        } else {
          date = this.translate.instant('unlimited')
        }
      return date
    }

  dealwidthDay (day) {
      let days = []
      let repeat
      repeat = ''
      let daily = this.translate.instant('record_daily')
      let mon = this.translate.instant('Mon_suspension')
      let tue = this.translate.instant('Tues_suspension')
      let wed = this.translate.instant('Wed_suspension')
      let thu = this.translate.instant('Thur_suspension')
      let fri = this.translate.instant('Fri_suspension')
      let sat = this.translate.instant('Sat_suspension')
      let sun = this.translate.instant('Sun_suspension')
      let repeatDataDates = [daily, mon, tue, wed, thu, fri, sat, sun]
      for (let i = 0; i < day.length; i++) {
          if (day.length === 7) {
              days = [daily]
              return days
            }
          if (day[i] === 1) {
              days.push(repeatDataDates[1])
            }
          if (day[i] === 2) {
              days.push(repeatDataDates[2])
            }
          if (day[i] === 3) {
              days.push(repeatDataDates[3])
            }
          if (day[i] === 4) {
              days.push(repeatDataDates[4])
            }
          if (day[i] === 5) {
              days.push(repeatDataDates[5])
            }
          if (day[i] === 6) {
              days.push(repeatDataDates[6])
            }
          if (day[i] === 7) {
              days.push(repeatDataDates[7])
            }
        }
      for (let i = 0; i < days.length; i++) {
          repeat += days[i] + ' '
        }
      return repeat
    }

  play () {
      EventService.emit('PLAYNPVR', this.recordDetails)
    }
  stopRecording () {
      let cancelRecordInfo
      if (this.recordDetails['policyType'] === 'Period' || this.recordDetails['policyType'] === 'Series') {
          cancelRecordInfo = [this.recordDetails['floatInfo']['childPVRIDs'][0],
              'stop_recording', this.recordDetails['floatInfo']['storageType']]
        } else {
          cancelRecordInfo = [this.recordDetails['floatInfo']['ID'], 'stop_recording', this.recordDetails['floatInfo']['storageType']]
        }
      EventService.emit('RECORD_CENCEL', cancelRecordInfo)
    }
  recordSetting () {
      if (this.isAuto) {// auto
          EventService.emit('CLOSE_UPDATE_RECORD_DIALOG')
          EventService.emit('POP_UP_NPVR_RECORD_SETTING', this.recordDetails['floatInfo'])
        } else {// manual recording
          EventService.emit('POP_UP_MANUAL_RECORD_SETTING', this.recordDetails['floatInfo'])
        }
    }

  mouseenter (onSuspension) {
      EventService.emit('showRecordSuspension', this.flagInfo)
    }

  mouseleave () {
      EventService.emit('closeRecordSuspension')
    }

  manageSetButton (recordDetails) {
      if (recordDetails.policyType === 'TimeBased' || recordDetails.policyType === 'Period') {
          this.hasSetButton = true
        }
      let currentTime = Number(Date.now()['getTime']())
      if (recordDetails.policyType === 'Series' && Number(recordDetails['endTime']) > currentTime) {
          this.hasSetButton = true
        }
      if (recordDetails.isSeriesPage === 'series') {
          this.hasSetButton = false
        }
      if (recordDetails.status === 'WAIT_RECORD' || recordDetails.status === 'RECORDING') {
          if (recordDetails.isSeriesPage !== 'series') {
              if (recordDetails.policyType === 'Period') {
                  if (+recordDetails.floatInfo.isOverdue === 0) {
                      this.hasSetButton = true
                    } else {
                      this.hasSetButton = false
                    }
                } else {
                  if (_.isUndefined(recordDetails.floatInfo['parentPlanID']) || recordDetails.floatInfo['parentPlanID'] === '') {
                      this.hasSetButton = true
                    } else {
                      this.hasSetButton = false
                    }
                }
            }
        }
    }

  manageStopButton (recordDetails) {
      if (recordDetails.status === 'RECORDING') {
          if (recordDetails.policyType === 'PlaybillBased' || recordDetails.policyType === 'TimeBased') {
              this.hasStopButton = true
            } else {
              if (recordDetails.subtaskLength === 1) {
                  this.hasStopButton = true
                } else if (session.get('CURRENT_RECORD_SORT') === 'latest') {
                  this.hasStopButton = true
                }
            }
        }
    }

  managePlayButton (recordDetails) {
      if (recordDetails.status === 'SUCCESS' || recordDetails.status === 'RECORDING' || recordDetails.status === 'OTHER' || recordDetails.status === 'PART_SUCCESS') {
          if (recordDetails.policyType === 'TimeBased' || recordDetails.policyType === 'PlaybillBased') {
              this.hasPlayButton = true
            }
        }
      if (recordDetails.floatInfo.storageType === 'CPVR') {
          this.isSTB = true
          this.hasPlayButton = false
        }
    }
    /**
     *  while quantity of buttons changes, we define various styles to deal with it
     *  do sure element's margintop equals to it's marginbottom
     */
  combineStyle () {
      if (this.hasPlayButton && !this.hasStopButton && this.hasSetButton && (this.isSearchRecord !== 'isSearchRecord')) {
          return { 'margin-top': this.ButtonsTop }
        } else if (this.hasStopButton && (this.isSearchRecord !== 'isSearchRecord') && !this.hasPlayButton && !this.hasSetButton) {
          return { 'margin-top': this.oneButtonTop }
        }
    }
}
