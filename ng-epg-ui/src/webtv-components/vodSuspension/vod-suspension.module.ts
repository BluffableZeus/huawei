import { TranslateModule } from '@ngx-translate/core'
import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'

import { VODSuspensionServices } from './vodSuspension.service'
import { SuspensionComponentVod } from './vodSuspension.component'

@NgModule({
  imports: [
      CommonModule,
      TranslateModule
    ],
  exports: [SuspensionComponentVod],
  declarations: [SuspensionComponentVod],
  providers: [VODSuspensionServices]
})
export class VODSuspensionModule { }
