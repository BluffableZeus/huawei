import * as _ from 'underscore'
import { Component, ElementRef, EventEmitter, Input, OnInit, Output } from '@angular/core'
import { TranslateService } from '@ngx-translate/core'
import { VODSuspensionServices } from './vodSuspension.service'
import { session } from '../../../demos-app/sdk/session'
import { EventService } from '../../../demos-app/sdk/event.service'
import { Router } from '@angular/router'
import { VodSuspension } from 'ng-epg-ui/webtv-components/vodSuspension/model/VodSuspension'
import { GetVODDetailResponse } from 'ng-epg-sdk/vsp/api'
import { VOD, VODDetail } from 'ng-epg-sdk'
@Component({
  selector: 'suspension',
  templateUrl: './vodSuspension.component.html',
  styleUrls: ['./vodSuspension.component.scss']
})

export class SuspensionComponentVod implements OnInit {
  @Input() newRoutes = false
  @Input() profile: boolean
  @Output() select: EventEmitter<VodSuspension> = new EventEmitter<VodSuspension>()

  public suspensionInfo: VODDetail = null
  public isShow: string = 'false'
  public leftImgStyle = {}
  public rightImgStyle = {}
  public favImgBg = ''
  public playImgStyle = {}
  public leftStyle = {}
  public leftTop = ''
  public rightTop = ''
  public starArray: number[] = []
  public titleName: string = ''
  public introduce: string = ''
  public scoreTimes: string = ''
  public arrayDef: Array<Object> = []
  public averageScore
  public isFav: string = ''
  public btnNamePlay: string = ''
  public btnNameFav: string = ''
  public flagInfo: string = ''
  public definitions: string[] = []
  public price: string = ''
  public vodId: string = ''
  public routeName = 'VODAll'
  public vodInfo: VodSuspension
  public isMyFavorite: boolean
  public focusRoute
  public isDemandCast: boolean = false
  public castName = ''
  public adjustTime
  public isChildColumn: boolean = false
  public purchasedList: boolean = false
  public searchAll: boolean = false
  public pocketSize: boolean = false
  public profileSize: boolean = false
  public largeScreen: boolean = false
  public MyVideos: boolean = false
  public produceZoneString: string = ''
  protected produceZone: string[] = []

  ngOnInit (): void {
      console.log(this.profile)
      const id = (this.el.nativeElement as HTMLElement).id
      this.isChildColumn = id !== 'suspensionTopInfo' && id !== 'suspensionEveryoneInfo'
      this.purchasedList = id === 'purchasedListID'
      this.searchAll = id === 'suspensionSearchInfo'
    }

  setDetail (suspensionData: VodSuspension, ID, realTarget: HTMLElement, origin?) {
      if (!suspensionData.floatInfo) {
          return
        }
      const lastVodID = suspensionData.floatInfo.ID

      this.largeScreen = window.innerWidth > 1440
      this.profileSize = realTarget.offsetHeight === 219 || realTarget.offsetHeight === 208
      this.MyVideos = origin === 'MyVideos'
      this.pocketSize = realTarget.offsetWidth === 180

      this.vodInfo = null
      this.suspensionInfo = null
      this.flagInfo = ''
      this.titleName = ''
      clearTimeout(this.adjustTime)

      EventService.emit('HIDESEARCH')
      EventService.removeAllListeners(['SETVODDETAIL'])
      EventService.emit('GETVODDETAIL', lastVodID)
      EventService.on('SETVODDETAIL', (detail: GetVODDetailResponse) => {
          this.vodInfo = null
          this.suspensionInfo = null
          this.flagInfo = ''
          if (lastVodID === detail.VODDetail.ID) {
              const vodDetail = detail.VODDetail

              this.vodInfo = suspensionData
              this.suspensionInfo = vodDetail

              this.flagInfo = suspensionData.titleName
              this.vodId = vodDetail.ID || ''
              this.titleName = vodDetail.name

              this.isDemandCast = !_.isUndefined(suspensionData['isOnDemand'])
              this.routeName = this.flagInfo || 'VODAll'
              this.isMyFavorite = this.routeName !== 'favoriteVedios'

              if (suspensionData['isPurchased']) {
                  this.price = (parseInt(vodDetail.price, 10) / Number(session.get('CURRENCY_RATE'))).toFixed(2)
                }

              this.introduce = vodDetail.introduce
              this.processingScore(vodDetail)
              this.processingProduceZone(vodDetail)

              this.definitions = []
              if (vodDetail.mediaFiles && vodDetail.mediaFiles.length > 0) {
                  this.definitions = _.uniq(
                        vodDetail.mediaFiles.map(f => this.definitionFormat(f.definition)),
                    true
                    )
                }

              this.focusRoute = suspensionData.floatInfo['focusRoute'] ? suspensionData.floatInfo['focusRoute'] : ''

              if (Cookies.getJSON('IS_GUEST_LOGIN')) {
                  this.isFav = '0'
                  this.btnNameFav = 'add_Favorite'
                  this.favImgBg = 'url(assets/img/14/addfav_14.png)'
                  this.btnNamePlay = 'buy'
                  this.playImgStyle = {
                      background: 'url(assets/img/14/shoppingbasket_14.png)'
                    }
                } else {
                  this.isFav = vodDetail.favorite ? '1' : '0'
                  if (!vodDetail.favorite) {
                      this.btnNameFav = 'add_Favorite'
                      this.favImgBg = 'url(assets/img/14/addfav_14.png)'
                    } else {
                      this.btnNameFav = 'vod_remove_fav'
                      this.favImgBg = 'url(assets/img/14/fav2_14.png)'
                    }
                  if (vodDetail['isSubscribed'] === '0') {
                      this.btnNamePlay = 'buy'
                      this.playImgStyle = {
                          background: 'url(assets/img/14/shoppingbasket_14.png)'
                        }
                    } else {
                      this.btnNamePlay = 'play'
                      this.playImgStyle = {
                          background: 'url(assets/img/14/play_14.png)'
                        }
                    }
                }

              if (!_.isUndefined(suspensionData.castName)) {
                  this.castName = suspensionData.castName
                }

              this.refreshRatingStar()
              EventService.emit('LOADDATASUCCESS')
            }
        })

    }

  constructor (
        private translate: TranslateService,
        private suspensionFac: VODSuspensionServices,
        protected el: ElementRef,
        private route: Router
    ) {

    }
  hideDefinition (item) {
      return window.innerWidth <= 1440 && item === 2
    }

  refreshRatingStar () {
      for (let i = 0; i < 5; i++) {
          let result = Math.round(this.averageScore) - (2 * (i + 1))
          if (result >= 0) {
              this.starArray[i] = 2
            } else if (result === -1) {
              this.starArray[i] = 1
            } else {
              this.starArray[i] = 0
            }
        }
    }

  timeFormat (times) {
      return this.suspensionFac.timeFormat(times)
    }

  definitionFormat (definition) {
      return this.suspensionFac.definitionFormat(definition)
    }

  play () {
      if (!_.isUndefined(this.vodInfo['isHistory'])) {
          this.routeName = 'history'
        }
      if (this.btnNamePlay === 'buy' && !Cookies.getJSON('IS_GUEST_LOGIN')) {
          session.put('buy', true)
        }

      if (this.newRoutes) {
          this.select.emit(this.vodInfo)
        } else {
          if ((this.routeName === 'cast' && !this.isDemandCast) || this.routeName === 'viewers_also_watched' ||
                this.routeName === 'like_many_of_this_kind' || this.routeName === 'seasons') {
              if (this.routeName === 'cast') {
                  session.put('VOD_DETAIL_CRUMBS_HREF', true)
                  window.open(window.location.href.split('#')[0] + '#/video/' +
                        'on-demand' + '/' + Number(this.vodId) + '/' + this.castName + '/' + 'isDetailsCast')
                } else {
                  session.put('VOD_DETAIL_CRUMBS_HREF', true)
                  window.open(window.location.href.split('#')[0] + '#/video/' +
                        'on-demand' + '/' + Number(this.vodId) + '/' + this.routeName)
                }
            } else if (this.focusRoute === '') {
              session.put('VOD_DETAIL_CRUMBS', location.href)
              this.route.navigate(['video', Number(this.vodId)])
            } else {
              if (this.routeName === 'cast') {
                  let name = location.href.indexOf('cast') !== -1 ? this.castName :
                        this.translate.instant('cast_movies', { name: this.castName })
                  session.put('VOD_DETAIL_CRUMBS', location.href)
                  this.route.navigate(['video', this.focusRoute, Number(this.vodId), name, 'isCast'])
                } else {
                  session.put('VOD_DETAILcd _CRUMBS', location.href)
                  this.route.navigate(['video', this.focusRoute, Number(this.vodId), this.routeName])
                }
            }
        }
    }

  favEvent (event) {
      event.stopPropagation()
      if (Cookies.getJSON('IS_GUEST_LOGIN')) {
          EventService.emit('UILOGIN')
          return
        }
      if (this.isFav === '1') {
          let req = {
              contentIDs: [this.suspensionInfo['ID']],
              contentTypes: ['VOD']
            }
          EventService.emit('ADDFAVORITE', req)
          EventService.removeAllListeners(['REMOVE_SUSDATE_successful'])
          EventService.on('REMOVE_SUSDATE_successful', () => {
              this.btnNameFav = 'add_Favorite'
              this.isFav = '0'
              this.favImgBg = 'url(assets/img/14/addfav_14.png)'
              if ((this.routeName === 'cast' && !this.isDemandCast) || this.routeName === 'viewers_also_watched' ||
                    this.routeName === 'like_many_of_this_kind' || this.routeName === 'seasons') {
                  let isFavReq = {
                      isFav: this.isFav,
                      vodID: this.vodId
                    }
                  EventService.emit('CHANGE_VODDETAILFAV', isFavReq)
                }
            })
          return
        } else {
          let req = {
              favorites: [{
                  contentID: this.suspensionInfo['ID'],
                  contentType: 'VOD'
                }],
              autoCover: '0'
            }
          EventService.emit('REMOVEFAVORITE', req)
          EventService.removeAllListeners(['ADD_SUSDATA_successful'])
          EventService.on('ADD_SUSDATA_successful', () => {
              this.btnNameFav = 'vod_remove_fav'
              this.isFav = '1'
              this.favImgBg = 'url(assets/img/14/fav2_14.png)'
              if ((this.routeName === 'cast' && !this.isDemandCast) || this.routeName === 'viewers_also_watched' ||
                    this.routeName === 'like_many_of_this_kind' || this.routeName === 'seasons') {
                  let isFavReq = {
                      isFav: this.isFav,
                      vodID: this.vodId
                    }
                  EventService.emit('CHANGE_VODDETAILFAV', isFavReq)
                }
            })
          return
        }

    }

  mouseenter (onSuspension) {
      EventService.emit('showSuspension', this.flagInfo)
    }

  mouseleave () {
      EventService.emit('closeSuspension', this.flagInfo)
    }
    /**
     * set the width of title.
     * if this is in purchased page, the title width should be 200px.
     * otherwise, the title width should be 305px.
     */
  titleWidth () {
      let width = '0px'
      if (window.innerWidth <= 1400) {
          width = '294.3px'
        } else {
          width = '368px'
        }
      if (this.price !== '') {
          width = '200px'
        }
      return width
    }

  scoreClass (star) {
      if (star === 1) {
          return  'grade3-18 rating_star_half'
        } else if (star === 2) {
          return  'grade2-18 rating_star_full'
        } else {
          return  'grade-18 rating_star_empty'
        }
    }

  protected processingScore (vod: VOD) {
      if (vod.averageScore) {
          if (vod.averageScore.substring(0, 2) !== '10') {
              let averageScores = vod.averageScore
              if (averageScores.length === 1) {
                  averageScores += '.0'
                } else {
                  averageScores = averageScores.substring(0, 4)
                  averageScores = Number(averageScores).toFixed(1)
                }
              vod.averageScore = averageScores
            } else {
              vod.averageScore = '10'
            }
        } else {
          vod.averageScore = '0.0'
        }
      this.averageScore = vod.averageScore && vod.averageScore !== '' ? vod.averageScore : '0.0'

      this.scoreTimes = vod.scoreTimes && vod.scoreTimes !== '' ? vod.scoreTimes : '0'
    }

  protected processingProduceZone (vodDetail: VOD) {
      this.produceZone = []
      if (vodDetail.produceDate && vodDetail.produceDate !== '') {
          this.produceZone.push(this.timeFormat(vodDetail.produceDate))
        }

      if (vodDetail.genres && vodDetail.genres[0] && vodDetail.genres[0].genreName) {
          this.produceZone.push(vodDetail.genres.map(g => g.genreName).join(' '))
        }

      if (vodDetail.rating && vodDetail.rating.name) {
          this.produceZone.push(vodDetail.rating.name)
        }

      this.produceZoneString = this.produceZone.join(' | ')
    }
}
