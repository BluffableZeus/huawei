import { VOD } from '../../../../../ng-epg-sdk/src/vsp'

export interface VodSuspension {
  titleName: string
  floatInfo: VOD

  castName?: string
}
