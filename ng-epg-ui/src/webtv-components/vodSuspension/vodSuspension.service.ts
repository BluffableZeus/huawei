import { Injectable } from '@angular/core'
import * as _ from 'underscore'

@Injectable()
export class VODSuspensionServices {

  constructor () { }

  timeFormat (timeData) {
      let times = timeData || ''
      let year = times.substring(0, 4)
      return year
    }

  definitionFormat (definition) {
      switch (definition) {
          case '0':
            return 'SD'
          case '1':
            return 'HD'
          case '2':
            return '4K'
          default:
            return ''
        }
    }

  moveEnter (currentTarget: HTMLElement, ID) {
      const element: HTMLElement = document.getElementById(ID)
      if (element) {
          _.delay(() => {
                //will be good idea to refactor this method and move logic into component
              this.applyStyle(currentTarget, element)
            }, 100)
        }
    }

  protected applyStyle (currentTarget: HTMLElement, element: HTMLElement) {
      const bounding = currentTarget.getBoundingClientRect()
      const parent: HTMLElement = currentTarget.offsetParent as HTMLElement
      const top = parent.offsetTop + (parent.offsetParent as HTMLElement).offsetTop

      element.style.left = `${bounding.left}px`
      element.style.top = `${top}px`
    }

}
