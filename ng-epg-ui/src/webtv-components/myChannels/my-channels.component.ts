import * as _ from 'underscore'
import { Component, OnInit, Input, OnDestroy, ViewChild, Output, EventEmitter } from '@angular/core'
import { EventService } from '../../../demos-app/sdk/event.service'
import { ActivatedRoute } from '@angular/router'
import { SuspensionComponentChannel } from '../channelSuspension/channelSuspension.component'
import { CommonSuspensionService } from 'ng-epg-ui/webtv-components/channelSuspension/channelCommon.service'

@Component({
  selector: 'myChannels-channel',
  templateUrl: './my-channels.component.html',
  styleUrls: ['./my-channels.component.scss']
})
export class MyChannels implements OnInit, OnDestroy {
  @ViewChild(SuspensionComponentChannel) suspensionchannel: SuspensionComponentChannel

  @Input() set data (data: Array<any>) {
      this.dataList = data
      return
    }

  @Output() deleteChannel: EventEmitter<Object> = new EventEmitter()
  @Output() detailChannel: EventEmitter<Object> = new EventEmitter()

  isDelete: string
  dataList: Array<any>
  _data: Object
  public channelShow: string = 'false'
  public isChannelShow: boolean = true
  private susTimeChannel
  private susFilter = _.debounce((fc: Function) => { fc() }, 400)
  private susUse: boolean = true

  constructor (
        private route: ActivatedRoute,
        private CommonSuspensionService: CommonSuspensionService
    ) { }

  ngOnInit () {
      let self = this
      EventService.removeAllListeners(['DELETE'])
      EventService.on('DELETE', function (data) {
          if (data === '1') {
              self.isDelete = '1'
            }
        })
      EventService.removeAllListeners(['COMPLETE'])
      EventService.on('COMPLETE', function (data) {
          if (data === '1') {
              self.isDelete = '0'
            }
        })
      EventService.removeAllListeners(['IS_CHANNELEDIT'])
      EventService.on('IS_CHANNELEDIT', function (data) {
          if (data === false) {
              self.isChannelShow = false
            } else {
              self.isChannelShow = true
            }
        })
    }
  ngOnDestroy () {
      EventService.removeAllListeners(['showChannelSuspension'])
      EventService.removeAllListeners(['closeChannelSuspension'])
    }
  deleteData (data: Object, index) {
      let self = this
      let dataObj = {
          'data': data,
          'index': index
        }
      self.deleteChannel.emit(dataObj)
    }

  channelDetail (data) {
      if (this.isChannelShow) {
          this.detailChannel.emit(data)
        }
    }

  mouseenter (option, event) {
      this.susUse = true
      EventService.emit('SUSPENSION_HIDE',false)
      this.susFilter(() => {
          if (this.susUse === false) {
              return
            }
          if (this.isChannelShow) {
              let self = this
              self.channelShow = 'false'
              let currentTarget = window.event && window.event.currentTarget || event && event.target
              let width = currentTarget['offsetWidth']
              let left
              let time1 = new Date().getTime()
              EventService.removeAllListeners(['LOADDATACHANNELSUCCESS'])
              EventService.on('LOADDATACHANNELSUCCESS', function () {
                  self.CommonSuspensionService.moveEnter(currentTarget, 'suspensionChannel', document.body.clientWidth, 'myChannel')
                  let time2 = new Date().getTime()
                  if ((time2 - time1) < 500) {
                      let time = 500 - (time2 - time1) + 100
                      clearTimeout(self.susTimeChannel)
                      self.susTimeChannel = setTimeout(function () {
                          self.channelShow = 'true'
                        }, time)
                    } else {
                      clearTimeout(this.susTimeChannel)
                      self.susTimeChannel = setTimeout(function () {
                          self.channelShow = 'true'
                        }, 100)
                    }
                })
              self.suspensionchannel.setDetail(option,'suspensionChannel')
            }
        })
    }

  closeDialog () {
      let self = this
      self.susUse = false
      clearTimeout(self.susTimeChannel)
      EventService.removeAllListeners(['showChannelSuspension'])
      EventService.on('showChannelSuspension', function (option) {
          self.channelShow = 'true'
        })
      EventService.removeAllListeners(['closeChannelSuspension'])
      EventService.on('closeChannelSuspension', function () {
          self.channelShow = 'false'
        })
      self.channelShow = 'false'
    }
}
