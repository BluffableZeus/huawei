import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'

import { MyChannels } from './my-channels.component'
import { ChannelSuspensionModule } from 'ng-epg-ui/webtv-components/channelSuspension'

@NgModule({
  imports: [CommonModule, ChannelSuspensionModule],
  exports: [MyChannels],
  declarations: [MyChannels],
  providers: []
})
export class MyChannelsModule { }
