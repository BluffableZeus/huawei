### Usage
```typescript
import { VodFilterComponent } from 'ng-epg-ui/webtv-components/vodFilter';

<app-vod-filter [contentConfig]="config"></app-vod-filter>

```

### Annotations
```typescript
export class VodFilterComponent {
    
    @Input() set contentConfig(data) {
        this.configs = data;
    }
}

```

 ### Attributes
| Attribute   | Description   | Type     | Options     | Default Value    | Mandatory (M) or Optional (O)    |
|-----   |-----    |-----    |------      |------    |------    |
| contentConfig | set config of genres, produceZones and so on  | GetContentConfigResponse |  |  | O |

 ### Data Type
 #### GetContentConfigResponse
| Attribute   | Description   | Type    | Default Value    | Mandatory (M) or Optional (O)    |
|-----   |-----    |-----    |------    |------    |
| produceZones | 内容的出品地信息 | array |  |  O |
| genres | 内容的流派信息。 | array |  |  O |