import { TranslateModule } from '@ngx-translate/core'
import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'

import { VodFilterComponent } from './vodFilter.component'

@NgModule({
  imports: [CommonModule, TranslateModule],
  exports: [VodFilterComponent],
  declarations: [VodFilterComponent],
  providers: []
})
export class VodFilterModule { }
