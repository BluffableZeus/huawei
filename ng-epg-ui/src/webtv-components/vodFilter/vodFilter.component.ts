import * as _ from 'underscore'
import { Component, OnInit, Input, OnDestroy } from '@angular/core'
import { TranslateService } from '@ngx-translate/core'

@Component({
  selector: 'app-vod-filter',
  styleUrls: ['./vodFilter.component.scss'],
  templateUrl: './vodFilter.component.html'
})
export class VodFilterComponent implements OnInit, OnDestroy {
  vodFilterLists: {
      'dataList': Array<any>,
      'row': string,
      'clientW': number,
      'suspensionID': string
    }
    // variable definition
  public isShow: string = 'true'
  public isNodata = true
  public vodFilterList: Array<any> = []
  public showgenreMore: boolean
  public showMoreArea: boolean
  public hasMoreGenre: boolean
  public hasMoreArea: boolean
  public sortByIndex = 0
  public areaIndex = 0
  public yearIndex = 0
  public genreIndex = 0
  public sortBy = []
  public area = []
  public year = []
  public genre = []
  public genres = []
  public areas = []
  public years = []
  public chosens = []
  public chosensShow = []
  public sortBys
  public configs

  constructor (private translate: TranslateService) {

    }

  @Input() set contentConfig (data) {
      this.configs = data
    }

  ngOnInit () {
      this.sortBys = [{
          ID: 'new',
          name: 'new'
        }, {
          ID: 'top',
          name: 'top'
        }, {
          ID: 'hot',
          name: 'hot'
        }]
      let self = this
      this.getYears()
      this.getAreas(this.configs.produceZones)
      this.getGenres(this.configs.genres)
      _.delay(function () {
          let currentGenreTarget = document.getElementById('genre').offsetHeight
          if (currentGenreTarget > 53) {
              self.hasMoreGenre = true
              self.showgenreMore = true
            } else {
              self.hasMoreGenre = false
            }
          let currentGenreTargetArea = document.getElementById('area').offsetHeight
          if (currentGenreTargetArea > 55) {
              self.showMoreArea = true
              self.hasMoreArea = true
            } else {
              self.showMoreArea = true
              self.hasMoreArea = false
            }
        }, 10)
    }
  ngOnDestroy () {

    }
  getContentConfigs () {
      return {

        }
    }
    // get the year of screening conditions
  getYears () {
      let time = new Date()
      let year = time.getFullYear()
      this.years.push({ ID: 'All', name: 'searchAll' })
      for (let i = 0; i < 8; i++) {
          this.years.push({ ID: (Number(year) - i).toString(), name: (Number(year) - i).toString() })
        }
      this.years.push({ ID: 'earlier', name: 'earlier' })
    }
    // get the area of screening conditions
  getAreas (list) {
      list.splice(0, 0, { ID: 'All', name: 'searchAll' })
      this.areas = _.map(list, (data) => {
          return { IDs: data['ID'], ID: data['ID'], name: data['name'] }
        })
    }
    // get the type of screening conditions
  getGenres (list) {
      list.splice(0, 0, { genreID: 'All', genreType: 'all', genreName: 'searchAll' })
      this.genres = _.map(list, function (data) {
          return { genreIDs: data['genreID'], ID: data['genreID'], name: data['genreName'] }
        })
    }
  selectFilterTwice (type, index, ID) {
      if (type === 'sortBy') {
          this.sortByIndex = index
          this.sortBy.push(ID)
          if (this.sortBy.length > 1) {
              let i = this.getIndex(this.chosens, this.sortBy[0])
              this.chosens.splice(i, 1)
              this.sortBy.shift()
            }
        }
      if (type === 'area') {
          this.areaIndex = index
          if (ID === 'All') {
              let i = this.removeResult(this.chosens, type)
              if (i || i === 0) {
                  this.chosens.splice(i, 1)
                }
              this.area = []
            } else {
              this.area.push(ID)
              if (this.area.length > 1) {
                  let i = this.getIndex(this.chosens, this.area[0])
                  this.chosens.splice(i, 1)
                  this.area.shift()
                }
            }
        }
      if (type === 'year') {
          this.yearIndex = index
          if (ID === 'All') {
              let i = this.removeResult(this.chosens, type)
              if (i || i === 0) {
                  this.chosens.splice(i, 1)
                }
              this.year = []
            } else {
              this.year.push(ID)
              if (this.year.length > 1) {
                  let i = this.getIndex(this.chosens, this.year[0])
                  this.chosens.splice(i, 1)
                  this.year.shift()
                }
            }
        }
    }
  selectFilter (list, type, index, flag?) {
      let ID = list.ID
      let name = list.name
      if (ID === 'searchAll') {
          ID = 'All'
        }
      if (ID !== 'All') {
          this.chosens.push({ name: name, ID: ID, type: type, index: index })
        }
      this.selectFilterTwice(type, index, ID)
      if (type === 'genre') {
          this.genreIndex = index
          if (ID === 'All') {
              let i = this.removeResult(this.chosens, type)
              if (i || i === 0) {
                  this.chosens.splice(i, 1)
                }
              this.genre = []
            } else {
              this.genre.push(ID)
              if (this.genre.length > 1) {
                  let i = this.getIndex(this.chosens, this.genre[0])
                  this.chosens.splice(i, 1)
                  this.genre.shift()
                }
            }
        }
      this.chosensShow = _.extend([], this.chosens)
      let i = this.removeResult(this.chosensShow, 'sortBy')
      if (i || i === 0) {
          this.chosensShow.splice(i, 1)
        }
      let chosens1
      let chosens2
      let chosens3
      _.map(this.chosensShow, (chosens) => {
          if (chosens.type === 'genre') {
              chosens1 = chosens
            } else if (chosens.type === 'area') {
              chosens2 = chosens
            } else if (chosens.type === 'year') {
              chosens3 = chosens
            }
        })
      this.chosensShow = []
      if (chosens1) {
          this.chosensShow.push(chosens1)
        }
      if (chosens2) {
          this.chosensShow.push(chosens2)
        }
      if (chosens3) {
          this.chosensShow.push(chosens3)
        }
    }

  changeFilter (chosensShow) {
      if (this.isShow === 'true') {
          this.isShow = 'false'
        } else {
          this.isShow = 'true'
        }
    }

  removeFilter (i, list) {
      let removeName = this.chosensShow.splice(i, 1)
      let l = this.getIndex(this.chosens, list.ID)
      this.chosens.splice(l, 1)
      if (removeName[0].type === 'year') {
          this.year = []
          this.yearIndex = 0
        }
      if (removeName[0].type === 'genre') {
          this.genre = []
          this.genreIndex = 0
        }
      if (removeName[0].type === 'area') {
          this.area = []
          this.areaIndex = 0
        }
    }

  removeResult (chosens, list) {
      for (let i = 0; i < chosens.length; i++) {
          if (chosens[i].type === list) {
              return i
            }
        }
    }

  getIndex (chosens, list) {
      for (let i = 0; i < chosens.length; i++) {
          if (chosens[i].ID === list) {
              return i
            }
        }
    }

  showMoreGenreInfo () {
      if (this.showgenreMore === false) {
          this.showgenreMore = true
        } else {
          this.showgenreMore = false
        }
    }

  showMoreAreaInfo () {
      if (this.showMoreArea === false) {
          this.showMoreArea = true
        } else {
          this.showMoreArea = false
        }
    }
}
