import * as _ from 'underscore'
import { Component, OnDestroy, Input, ViewChild, EventEmitter, Output } from '@angular/core'
import { EventService } from '../../../demos-app/sdk/event.service'
import { ActivatedRoute } from '@angular/router'
import { SuspensionComponentVod } from '../vodSuspension/vodSuspension.component'
import { WaterfallVodService } from 'ng-epg-ui/webtv-components/waterfall-vod/waterfall-vod.service'

@Component({
  selector: 'waterfall-vod',
  templateUrl: './waterfall-vod.component.html',
  styleUrls: ['./waterfall-vod.component.scss']
})
export class WaterfallVodBox implements OnDestroy {
  @Input() set data (data: Object) {
      this._data = data
      if (!_.isUndefined(this._data)) {
          this.dataList = this._data['dataList']
          if (!_.isUndefined(this._data['suspensionID'])) {
              this.suspensionID = this._data['suspensionID']
            }
        }
      _.each(this.dataList, (item) => {
          if (parseInt(item['score']) === 10) {
              item['score'] = '10'
            }
        })
    }

  @ViewChild(SuspensionComponentVod) suspension: SuspensionComponentVod

  @Output() child: EventEmitter<Object> = new EventEmitter()

  suspensionID: string
  dataList: Array<any>
  _data: Object
  public isShow: boolean = false
  private susTime
  private susFilter = _.debounce((fc: Function) => { fc() }, 400)
  private susUse: boolean = true

  constructor (
        private route: ActivatedRoute,
        private service: WaterfallVodService
    ) { }

  ngOnDestroy () {
      EventService.removeAllListeners(['showSuspension'])
      EventService.removeAllListeners(['closeSuspension'])
    }

  public vodDetail (id, subjectId): void {
      this.child.emit([id, subjectId])
    }

  public onSuspensionClick (event): void {
      this.child.emit([event.vodId, event.subjectID])
    }

  trackByFn (index, data) {
      return data.vodId
    }

  mouseenter (option, event: MouseEvent) {
      this.susUse = true
      this.susFilter(() => {
          if (this.susUse === false) {
              return
            }
          this.isShow = false
          clearTimeout(this.susTime)
          let time1 = new Date().getTime()
          let currentTarget = window.event && window.event.currentTarget['offsetParent']
                || event && event.target['offsetParent']
          EventService.removeAllListeners(['LOADDATASUCCESS'])
          EventService.on('LOADDATASUCCESS', () => {
              this.service.moveEnter(currentTarget, this.suspensionID)
              let time2 = new Date().getTime()
              if ((time2 - time1) < 500) {
                  let time = 500 - (time2 - time1) + 100
                  this.susTime = setTimeout(() => {
                      this.isShow = true
                    }, time)
                } else {
                  this.susTime = setTimeout(() => {
                      this.isShow = true
                    }, 100)
                }
            })
          this.suspension.setDetail(option, this.suspensionID, currentTarget.firstElementChild)
        })
    }

  closeDialog (data) {
      this.susUse = false
      clearTimeout(this.susTime)
      EventService.removeAllListeners(['showSuspension'])
      EventService.removeAllListeners(['closeSuspension'])
      EventService.on('showSuspension', option => {
          if (option === data.titleName) {
              this.isShow = true
            }
        })
      EventService.on('closeSuspension', () => {
          this.isShow = false
          if (document.getElementById(this.suspensionID)) {
              document.getElementById(this.suspensionID).style.top = -100 + 'px'
              document.getElementById(this.suspensionID).style.left = 0 + 'px'
            }
        })
      this.isShow = false
      _.delay(() => {
          if (this.isShow === false) {
              document.getElementById(this.suspensionID).style.top = -100 + 'px'
              document.getElementById(this.suspensionID).style.left = 0 + 'px'
            }
        }, 100)
      EventService.removeAllListeners(['LOADDATASUCCESS'])
      EventService.removeAllListeners(['REMOVE_SUSDATE_successful'])
      EventService.removeAllListeners(['ADD_SUSDATA_successful'])
    }
}
