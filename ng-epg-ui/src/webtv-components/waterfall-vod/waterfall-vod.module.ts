import { WaterfallVodService } from './waterfall-vod.service'
import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'

import { WaterfallVodBox } from './waterfall-vod.component'
import { VODSuspensionModule } from '../vodSuspension/vod-suspension.module'
import { VerticalPosterModule } from 'ng-epg-ui/webtv-components/verticalPoster'

@NgModule({
  imports: [
      VODSuspensionModule,
      VerticalPosterModule,
      CommonModule
    ],
  exports: [WaterfallVodBox],
  declarations: [WaterfallVodBox],
  providers: [WaterfallVodService]
})
export class WaterfallVodModule { }
