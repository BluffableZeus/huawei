import { Injectable } from '@angular/core'
import { VODSuspensionServices } from 'ng-epg-ui/webtv-components/vodSuspension'

@Injectable()
export class WaterfallVodService extends VODSuspensionServices {
  protected applyStyle (currentTarget: HTMLElement, element: HTMLElement): void {
      element.style.left = `${currentTarget.offsetLeft}px`
      element.style.top = `${currentTarget.offsetTop}px`
    }
}
