import { Injectable } from '@angular/core'
import * as _ from 'underscore'
import { DateUtils } from '../../../demos-app/sdk/date-utils'
import { session } from '../../../demos-app/sdk/session'
import { TranslateService } from '@ngx-translate/core'

@Injectable()
export class MyRecordingsService {
  constructor (public translate: TranslateService) { }

  moveEnter (currentTarget, ID, clientWidth, type) {
      _.delay(function () {
          if (document.getElementById(ID)) {
              let width = currentTarget.offsetWidth
              let height = currentTarget.offsetHeight
              let left = 0
              let top = 0

              left = currentTarget.offsetLeft + currentTarget.offsetParent.offsetLeft
              document.getElementById(ID).style.left = `${left}px`
              document.getElementById(ID).style.top = `${currentTarget.offsetTop}px`
              document.getElementById(ID).style.width = 1.1 * width + 'px'
              document.getElementById(ID).style.height = 1.1 * height + 'px'
              document.getElementById(ID).children[0]['style']['height'] = 1.1 * height + 'px'
              document.getElementById(ID).children[1]['style']['height'] = 1.1 * height + 'px'
              if (document.querySelector('#' + ID + ' .title')) {
                  document.querySelector('#' + ID + ' .title')['style']['width'] = 1.1 * width - 50 + 'px'
                }
            }
        }, 50)
    }

  dealWithMillisecondTime (time) {
      if (time) {
          let month = DateUtils.format(time, 'MM')
          month = this.dealWithTime(month)
          let day = DateUtils.format(time, 'D')
          let year = DateUtils.format(time, 'YYYY')
          if (session.get('languageName') === 'zh') {
              return year + '/' + month + '/' + day
            } else {
              return month + ' ' + day + ', ' + year
            }
        }
    }

  dealWithTime (month) {
      let Month
      if (month === '01') {
          Month = 'jan_tvguide'
        } else if (month === '02') {
          Month = 'feb_tvguide'
        } else if (month === '03') {
          Month = 'mar_tvguide'
        } else if (month === '04') {
          Month = 'apr_tvguide'
        } else if (month === '05') {
          Month = 'may_tvguide'
        } else if (month === '06') {
          Month = 'jun_tvguide'
        } else if (month === '07') {
          Month = 'jul_tvguide'
        } else if (month === '08') {
          Month = 'aug_tvguide'
        } else if (month === '09') {
          Month = 'sep_tvguide'
        } else if (month === '10') {
          Month = 'oct_tvguide'
        } else if (month === '11') {
          Month = 'nov_tvguide'
        } else if (month === '12') {
          Month = 'dec_tvguide'
        }
      Month = this.translate.instant(Month)
      return Month
    }
}
