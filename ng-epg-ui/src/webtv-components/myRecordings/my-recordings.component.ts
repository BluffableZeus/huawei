import * as _ from 'underscore'
import { Component, OnInit, OnDestroy, Input, ViewChild, Output, EventEmitter } from '@angular/core'
import { TranslateService } from '@ngx-translate/core'
import { EventService } from '../../../demos-app/sdk/event.service'
import { ActivatedRoute } from '@angular/router'
import { MyRecordingsService } from './my-recordings.service'
import { RecordSuspensionComponent } from '../recordSuspension/recordSuspension.component'
import { DateUtils } from '../../../demos-app/sdk/date-utils'
import { session } from '../../../demos-app/sdk/session'
import { ProgramSuspensionService } from '../programSuspension/programSuspension.service'

@Component({
  selector: 'my-recording',
  templateUrl: './my-recordings.component.html',
  styleUrls: ['./my-recordings.component.scss']
})
export class MyRecordings implements OnInit, OnDestroy {
  @ViewChild(RecordSuspensionComponent) recordSuspension: RecordSuspensionComponent
  @Input() set dataRecord (data: Object) {
      this._data = data
      if (data) {
          this.dataList = this._data['dataList']
          this.suspensionID = this._data['suspensionID']
        }
      if (this.dataList && this.dataList.length > 0) {
          clearInterval(this.recordTimer)
          this.recordTimer = setInterval(() => {
              this.dataList = this.refreshPosterProgress(this.dataList)
            }, 1000 * 60)
        }
    }
  @Output() deleteRecord: EventEmitter<Object> = new EventEmitter()
  @Output() detailRecord: EventEmitter<Object> = new EventEmitter()
  isDelete: string
  dataList: Array<any>
  _data: Object
  suspensionID: string
  public isShow: string = 'false'
  public isRecordShow: boolean = true
  public susTime
  public recordTimer = null
  public susFilter = _.debounce((fc: Function) => { fc() }, 400)
  public susUse: boolean = true

  constructor (
        private route: ActivatedRoute,
        private myRecordingsService: MyRecordingsService,
        private translate: TranslateService,
        private programSuspensionService: ProgramSuspensionService
    ) {
    }

  ngOnInit () {
      let self = this
      EventService.removeAllListeners(['DELETE'])
      EventService.on('DELETE', function (data) {
          if (data === '1') {
              self.isDelete = '1'
            }
        })
      EventService.removeAllListeners(['COMPLETE'])
      EventService.on('COMPLETE', function (data) {
          if (data === '1') {
              self.isDelete = '0'
            }
        })
      EventService.removeAllListeners(['IS_RECORDEDIT'])
      EventService.on('IS_RECORDEDIT', function (data) {
          if (data === false) {
              self.isRecordShow = false
            } else {
              self.isRecordShow = true
            }
        })
      EventService.removeAllListeners(['DELETESERIES'])
      EventService.on('DELETESERIES', function (index) {
          self.dataList.splice(Number(index), 1)
        })
      EventService.removeAllListeners(['MANUAL_CHANGE_NAME'])
      EventService.on('MANUAL_CHANGE_NAME', function (data) {
          self.changeData(data, self.dataList)
        })
      EventService.removeAllListeners(['RECORD_UPDATE_SUCCESS'])
      EventService.on('RECORD_UPDATE_SUCCESS', (data) => {
          EventService.emit('PROFILE_INFO_SAVE_SUCCESSFUL')
          self.changeDataAuto(data, self.dataList)
        })
      EventService.removeAllListeners(['DELETESINGLE'])
      EventService.on('DELETESINGLE', function (data) {
          self.changeDataName(data, self.dataList)
        })
      EventService.removeAllListeners(['CHANGE_SERIES_DATA'])
      EventService.on('CHANGE_SERIES_DATA', function (data) {
          self.changeDataName(data, self.dataList)
        })
      EventService.removeAllListeners(['CHANGE_CLOUD_ICON'])
      EventService.on('CHANGE_CLOUD_ICON', function (data) {
          self.changeDataIcon(data, self.dataList)
        })
    }

  ngOnDestroy () {
      clearInterval(this.recordTimer)
    }

  deleteData (data: Object, index: string) {
      let dataObj = {
          'data': data,
          'indexDelete': index
        }
      EventService.emit('DELETEONE')
      EventService.removeAllListeners(['DELETEONECONFIRM'])
      EventService.on('DELETEONECONFIRM', () => {
          this.deleteRecord.emit(dataObj)
        })
      this.closeDialog(data)
    }

  recordDetail (data, index) {
      let recordInfo
      if (data.policyType === 'Series' || data.policyType === 'Period') {
          recordInfo = { id: data.id, isPlay: true, name: data.name, index: index }
          session.put('CHANNEL_NAME_FROM_PARENT', data.channelName)
          this.detailRecord.emit(recordInfo)
        } else {
          if (data.status === 'SUCCESS' || data.status === 'OTHER' || data.status === 'PART_SUCCESS' || data.status === 'RECORDING') {
              if (data.floatInfo.storageType !== 'CPVR') {
                  EventService.emit('PLAYNPVR', data)
                }
            } else if (data.status === 'FAIL') {
              EventService.emit('RECORDFAILED', data)
            }
        }
    }

  changeDataAuto (data, list) {
      let nameList = list
      for (let i = 0; i < nameList.length; i++) {
          if (nameList[i].floatInfo.ID === data.allMessage.ID) {
              this.dataList[i]['floatInfo']['beginOffset'] = data.allMessage.beginOffset
              this.dataList[i]['floatInfo']['endOffset'] = data.allMessage.endOffset
              this.dataList[i]['floatInfo']['storageType'] = data.allMessage.storageType
              this.dataList[i]['floatInfo']['definition'] = data.definition
            }
        }
      return nameList
    }

  changeData (data, list) {
      let nameList = list
      for (let i = 0; i < nameList.length; i++) {
          if (nameList[i].floatInfo.ID === data.ID) {
              if (data.name) {
                  nameList[i]['name'] = data.name
                  this.dataList[i]['floatInfo']['name'] = data.name
                  if (nameList[i]['policyType'] === 'Period') {
                      nameList[i]['name'] = nameList[i]['name'] + ' ( ' + nameList[i]['subtaskLength'] + ' )'
                    }
                }
              if (data.startTime) {
                  nameList[i]['startTime'] = data.startTime + ''
                  this.dataList[i]['floatInfo']['startTime'] = data.startTime + ''
                  nameList[i]['duration'] = this.programSuspensionService.dealWithDateJson('D10', data['startTime'])
                        + '-' + this.programSuspensionService.dealWithDateJson('D10', data['endTime'])
                  nameList[i]['recordweekTime'] = DateUtils.format(data['startTime'], 'ddd')
                  nameList[i]['recordTime'] =
                    this.programSuspensionService.dealWithDateJson('D01', data['startTime'])
                }
              if (data.endTime) {
                  nameList[i]['endTime'] = data.endTime + ''
                  this.dataList[i]['floatInfo']['endTime'] = data.endTime + ''
                  nameList[i]['duration'] = this.programSuspensionService.dealWithDateJson('D10',
                     nameList[i]['floatInfo']['startTime']) + '-' + this.programSuspensionService.dealWithDateJson('D10',
                      nameList[i]['floatInfo']['endTime'])
                }
              if (data.cpvrMediaID) {
                  nameList[i]['cpvrMediaID'] = data.cpvrMediaID
                  this.dataList[i]['floatInfo']['cpvrMediaID'] = data.cpvrMediaID
                }
            }
        }
      return nameList
    }

  changeDataName (data, list) {
      let nameList = list
      for (let i = 0; i < nameList.length; i++) {
          if (nameList[i].floatInfo.ID === data.ID) {
              nameList[i]['name'] = data.name
              this.dataList[i]['floatInfo']['name'] = data.name
              nameList[i]['startTime'] = data.startTime
              this.dataList[i]['floatInfo']['startTime'] = data.startTime
              this.dataList[i]['floatInfo']['endTime'] = data.endTime
              nameList[i]['duration'] = this.programSuspensionService.dealWithDateJson('D10',
                 data['childPVR']['startTime'])
                        + '-' + this.programSuspensionService.dealWithDateJson('D10',data['childPVR']['endTime'])
              nameList[i]['endTime'] = data.endTime
            }
        }
      return nameList
    }

  changeDataIcon (data, list) {
      let nameList = list
      for (let i = 0; i < nameList.length; i++) {
          if (nameList[i].floatInfo.ID === data.ID) {
              nameList[i]['storageType'] = !!data['chooseStorage'][0] ? 'NPVR' : 'CPVR'
              this.dataList[i]['floatInfo']['storageType'] = !!data['chooseStorage'][0] ? 'NPVR' : 'CPVR'
            }
        }
      return nameList
    }

    /**
     * refresh [poster] progress bar
     */
  refreshPosterProgress (list) {
      let posterList = list
      for (let i = 0; i < posterList.length; i++) {
          let length
          let curLength
          if (posterList[i]['floatInfo']['childPVR']) {
              let floatInfo = posterList[i]['floatInfo']
              length = Number(posterList[i]['length'])
              if (floatInfo['childPVR']['policyType'] === 'PlaybillBased') {
                  curLength = length -
                        (Number(floatInfo['childPVR']['endTime']) - Date.now()['getTime']() + Number(floatInfo['endOffset'] * 1000))
                } else {
                  curLength = Date.now()['getTime']() - Number(floatInfo['childPVR']['startTime'])
                }
            } else {
              let floatInfo = posterList[i]['floatInfo']
              length = Number(posterList[i]['length'])
              if (floatInfo['policyType'] === 'PlaybillBased') {
                  curLength = length -
                        (Number(floatInfo['endTime']) - Date.now()['getTime']() + Number(floatInfo['endOffset'] * 1000))
                } else {
                  curLength = Date.now()['getTime']() - Number(floatInfo['startTime'])
                }
            }
          posterList[i]['percent'] = curLength >= length ? '100%' : Number(curLength) / Number(length) * 100 + '%'
          if (posterList[i]['percent'] === '100%' && posterList[i]['status'] === 'RECORDING') {
              posterList[i]['status'] = 'SUCCESS'
              let duration = Number(posterList[i]['length']) / 1000
              posterList[i]['duration'] = Math.ceil(duration / 60) + ' ' + this.translate.instant('record_min')
              posterList[i]['recordweekTime'] = DateUtils.format(list[i]['startTime'], 'ddd')
              posterList[i]['recordTime'] =
                     this.programSuspensionService.dealWithDateJson('D01', list[i]['startTime'])
            }
        }
      return posterList
    }

  mouseenter (option, event) {
      let currentTarget = window.event && window.event.currentTarget || event && event.currentTarget
      this.susUse = true
      this.susFilter(() => {
          if (this.susUse === false) {
              return
            }
          if (this.isRecordShow) {
              let self = this
              self.isShow = 'false'
              clearTimeout(self.susTime)
              self.susTime = setTimeout(function () {
                  self.isShow = 'true'
                }, 400)
              self.myRecordingsService.moveEnter(currentTarget, self.suspensionID, document.body.clientWidth, 'record')
              self.recordSuspension.setDetail(option, self.suspensionID, currentTarget.firstElementChild)
            }
        })
    }

  closeDialog (data) {
      let self = this
      self.susUse = false
      clearTimeout(self.susTime)
      EventService.removeAllListeners(['showRecordSuspension'])
      EventService.on('showRecordSuspension', function (option) {
          if (option === data.titleName) {
              self.isShow = 'true'
            }
        })
      EventService.removeAllListeners(['closeRecordSuspension'])
      EventService.on('closeRecordSuspension', function () {
          self.isShow = 'false'
        })
      self.isShow = 'false'
    }

    /**
     * get DOM element
     */
  dom (divName: string) {
      return document.getElementById(divName)
    }
}
