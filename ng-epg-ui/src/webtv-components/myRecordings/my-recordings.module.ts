import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'

import { MyRecordings } from './my-recordings.component'
import { MyRecordingsService } from './my-recordings.service'
import { RecordSuspensionModule } from '../recordSuspension'
import { TranslateModule } from '@ngx-translate/core'

@NgModule({
  imports: [CommonModule, RecordSuspensionModule, TranslateModule],
  exports: [MyRecordings],
  declarations: [MyRecordings],
  providers: [MyRecordingsService]
})
export class MyRecordingsModule { }
