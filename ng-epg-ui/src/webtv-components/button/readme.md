### Usage
```typescript
import {Button} from 'ng-epg-ui/components/button';
```
### Annotations
```typescript
@Component({
    selector: 'btn',
    template: require('./button.component.html')
})

export class Button implements OnInit {

    @Input()
    set className(className: string) {
        className = className || '';

        _.chain((className || '').split(' ')).compact().each((str) => {
            this._el.classList.add(str.trim());
        }).value();
    };

    @Input() iconName: string;
    @Input() buttonText: string;
}
```

### Button properties
  - `className` (`?:string=''`) - the className of incoming the button component.
  - `iconName` (`?:string=''`) - the icon of incoming the button component.
  - `buttonText` (`?:string=''`) - the text of incoming the button component.

