import { Directive, Output, EventEmitter, HostListener } from '@angular/core'

@Directive({
  selector: '[once-click]',
  host: {
      '[class.clicked]': 'isClicked'
    }
})
export class OnceClickDirective {

  private isClicked = false

  @Output('once-click')
    private onceClick = new EventEmitter()

  @HostListener('xblur')
    private onXBlur () {
      this.isClicked = false
    }

  @HostListener('xfocus')
    private onXFocus () {
      this.isClicked = false
    }

  @HostListener('click', ['$event'])
    private onClick (event: Event) {
      if (this.isClicked) {
          event.stopPropagation()
          return
        }
      this.isClicked = true
      this.onceClick.emit(event)
    }
}
