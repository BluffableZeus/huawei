import * as _ from 'underscore'
import { Input, Component, OnInit, ElementRef } from '@angular/core'

@Component({
  selector: 'btn',
  template: require('./button.component.html')
})

export class Button implements OnInit {

  @Input()
    set className (className: string) {
      className = className || ''

      _.chain((className || '').split(' ')).compact().each((str) => {
          this._el.classList.add(str.trim())
        }).value()
    }

  @Input() iconName: string
  @Input() buttonText: string

  private _el: HTMLElement

  constructor (elRef: ElementRef) {
      this._el = elRef.nativeElement as HTMLElement
    }

  ngOnInit () {
    }
}
