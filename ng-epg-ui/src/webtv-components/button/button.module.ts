import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'

import { Button } from './button.component'

@NgModule({
  imports: [
      CommonModule
    ],
  exports: [Button],
  declarations: [Button],
  providers: []
})
export class ButtonModule { }
