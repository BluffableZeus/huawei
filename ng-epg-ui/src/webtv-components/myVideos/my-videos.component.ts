import * as _ from 'underscore'
import { Component, OnInit, Input, ViewChild, Output, EventEmitter } from '@angular/core'
import { EventService } from '../../../demos-app/sdk/event.service'
import { ActivatedRoute } from '@angular/router'
import { MyVideoService } from './my-videos.service'
import { SuspensionComponentVod } from '../vodSuspension'
import { TranslateService } from '@ngx-translate/core'
import { ProgramSuspensionService } from 'ng-epg-ui/webtv-components/programSuspension'

@Component({
  selector: 'myVideos-vod',
  templateUrl: './my-videos.component.html',
  styleUrls: ['./my-videos.component.scss']
})
export class MyVideos implements OnInit {
  @ViewChild(SuspensionComponentVod) suspension: SuspensionComponentVod
  @Input() set dataVod (data: Object) {
      this.dataList = data['dataList']
      this.suspensionID = data['suspensionID']
      this.handleData()
    }
  @Output() deleteVod: EventEmitter<Object> = new EventEmitter()
  @Output() detailVod: EventEmitter<Object> = new EventEmitter()
  isDelete: string
  dataList: Array<any>
  suspensionID: string
  public isShow: boolean = false
  public isVodShow: boolean = true
  public param: Array<any> = []
  private validToDate = []

  constructor (
        private route: ActivatedRoute,
        private myVideoService: MyVideoService,
        private translate: TranslateService,
        private programSuspensionService: ProgramSuspensionService
    ) { }

  ngOnInit () {
      EventService.on('DELETE', data => {
          if (data === '1') {
              this.isDelete = '1'
            }
        })
      EventService.on('COMPLETE', data => {
          if (data === '1') {
              this.isDelete = '0'
            }
        })
      EventService.on('IS_VODEDIT', data => {
          this.isVodShow = data !== false
        })
      EventService.removeAllListeners(['Refreshed'])
      EventService.on('Refreshed', () => {
          this.handleData()
        })
      this.handleData()
    }

  deleteData (data: Object, index) {
      let dataObj = {
          'data': data,
          'index': index
        }
      this.deleteVod.emit(dataObj)
    }

  vodDetail (id) {
      if (this.isDelete !== '1') {
          this.detailVod.emit([id])
        }
    }

  handleData () {
      _.map(this.dataList, (data, index) => {
          if (this.suspensionID === 'today' || this.suspensionID === 'yesterday' ||
                this.suspensionID === 'thisWeek' || this.suspensionID === 'earlier') {
              this.param[index] = {
                  'min': Math.ceil(data['vodBookmark'] / 60)
                }
            }
          if (this.suspensionID === 'purchasedListID') {
              this.myVideoService.timeClassify(data)
              if (data['leftTimeType'] === 'moreDays') {
                  this.validToDate[index] = this.programSuspensionService.dealWithDateJson('D02', data['endTime'])
                }
            }
        })
    }
}
