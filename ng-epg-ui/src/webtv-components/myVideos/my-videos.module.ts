import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'
import { TranslateModule } from '@ngx-translate/core'

import { MyVideos } from './my-videos.component'
import { MyVideoService } from './my-videos.service'
import { VODSuspensionModule } from '../vodSuspension'
import { TranslateService } from '@ngx-translate/core'

@NgModule({
  imports: [CommonModule, VODSuspensionModule, TranslateModule],
  exports: [MyVideos],
  declarations: [MyVideos],
  providers: [MyVideoService, TranslateService]
})
export class MyVideosModule { }
