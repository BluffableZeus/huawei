import { Injectable } from '@angular/core'
import * as _ from 'underscore'
import { TranslateService } from '@ngx-translate/core'

@Injectable()
export class MyVideoService {
  private monthObj: Object = {
      '01': 'jan_tvguide',
      '02': 'feb_tvguide',
      '03': 'mar_tvguide',
      '04': 'apr_tvguide',
      '05': 'may_tvguide',
      '06': 'jun_tvguide',
      '07': 'jul_tvguide',
      '08': 'aug_tvguide',
      '09': 'sep_tvguide',
      '10': 'oct_tvguide',
      '11': 'nov_tvguide',
      '12': 'dec_tvguide'
    }

  constructor (private translate: TranslateService) { }

  moveEnter (currentTarget, ID, clientWidth, type) {
      _.delay(() => {
          if (document.getElementById(ID)) {
                // let width = currentTarget.offsetWidth;
                // let height = currentTarget.offsetHeight;
              let top = 0

              const left = currentTarget.offsetLeft + currentTarget.offsetParent.offsetLeft
              document.getElementById(ID).style.left = `${left}px`
              document.getElementById(ID).style.top = `${currentTarget.offsetTop}px`
            }
        }, 100)
    }

    /**
     * classify the left time of subscribe,
     * there are more then 7 days, 2~7 days, 2hour~48hour, 0min~120min, exceed the time limit
     * list：the subscribe product message
     */
  timeClassify (list) {
      let nowTime = Date.now()['getTime']()
      let leftTime = Number(list['endTime']) - Number(nowTime)
      if (leftTime > 604800000) {  // >7days show icon
          list['leftTime'] = leftTime
          list['leftTimeType'] = 'moreDays'
        } else if (leftTime > 172800000) {  // >48hours show day
          list['leftTimeType'] = 'Days'
          list['leftTime'] = Math.ceil(leftTime / 86400000) + ''
        } else if (leftTime > 7200000) {  // >120mins show hour
          list['leftTimeType'] = 'Hours'
          list['leftTime'] = Math.ceil(leftTime / 3600000) + ''
        } else if (leftTime > 0) {  // >0 show min
          list['leftTimeType'] = 'Mins'
          list['leftTime'] = Math.ceil(leftTime / 60000) + ''
        } else if (leftTime <= 0) {  // <=0 show expired
          list['leftTimeType'] = 'Expired'
          list['leftTime'] = '0'
        }
    }

  dealWithTime (month) {
      return this.translate.instant(this.monthObj[month])
    }
}
