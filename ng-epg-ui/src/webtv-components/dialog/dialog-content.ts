import { Directive, Host, SkipSelf, ViewContainerRef } from '@angular/core'
import { DialogContainer } from './dialog-container'
/**
 * Simple decorator used only to communicate an ElementRef to the parent MdDialogContainer as the
 * location for where the dialog content will be loaded.
 */
@Directive({
  selector: 'md-dialog-content'
})
export class DialogContent {
  constructor (@Host() @SkipSelf() dialogContainer: DialogContainer, viewContainerRef: ViewContainerRef) {
      dialogContainer.contentRef = viewContainerRef
    }
}
