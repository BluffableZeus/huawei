import { DialogContent } from './dialog-content'
import { DialogContainer } from './dialog-container'
import { CommonModule } from '@angular/common'
import { Backdrop } from './backdrop'
import { DialogService } from './dialog'
import { NgModule } from '@angular/core'

@NgModule({
  imports: [
      CommonModule
    ],
  exports: [],
  declarations: [
      Backdrop,
      DialogContainer,
      DialogContent
    ],
  entryComponents: [
      Backdrop,
      DialogContainer
    ],
  providers: [
      DialogService
    ]
})
export class DialogModule { }
