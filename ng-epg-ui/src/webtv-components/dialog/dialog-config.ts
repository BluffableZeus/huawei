import { DialogRef } from './dialog-ref'

/** Configuration for a dialog to be opened. */
export class DialogConfig {
  width: string = null
  height: string = null
  container: HTMLElement = null
  sourceEvent: Event = null
  clickClose: boolean = true
  containerClass = ''

  context: any = {}

  parent (element: HTMLElement): DialogConfig {
    this.container = element
    return this
  }

  clickOutsideToClose (enabled: boolean): DialogConfig {
    this.clickClose = enabled
    return this
  }

  title (text: string): DialogConfig {
    this.context.title = text
    return this
  }

  textContent (text: string): DialogConfig {
    this.context.textContent = text
    return this
  }

  ariaLabel (text: string): DialogConfig {
    this.context.ariaLabel = text
    return this
  }

  ok (text: string): DialogConfig {
    this.context.ok = text
    return this
  }

  cancel (text: string): DialogConfig {
    this.context.cancel = text
    return this
  }

  targetEvent (ev: Event): DialogConfig {
    this.sourceEvent = ev
    return this
  }

  containerCss (cls: string) {
    this.containerClass = this.containerClass ? this.containerClass + ' ' + cls : cls
    return this
  }

  setProperty (key: string, value: any) {
    this.context[key] = value
    return this
  }

}
