import { ViewEncapsulation, Component, ViewContainerRef, forwardRef, Directive, Host, SkipSelf } from '@angular/core'
import { DialogRef } from './dialog-ref'

/**
 * Container for user-provided dialog content.
 */
@Component({
  selector: 'md-dialog-container',
  encapsulation: ViewEncapsulation.None,
  template: `
  <div>
    <md-dialog-content></md-dialog-content>
  </div>
  `,
  styles: [`
      md-dialog-container{
          position: fixed;
          top: 0px;
          z-index: 2200000000;
          left: 0px;
      }
  `],
  host: {
      'class': 'md-dialog',
      'tabindex': '-1'
    }
})
export class DialogContainer {
    // Ref to the dialog content. Used by the DynamicComponentLoader to load the dialog content.
  contentRef: ViewContainerRef

    // Ref to the open dialog. Used to close the dialog based on certain events.
  dialogRef: DialogRef

  constructor () {
      this.contentRef = null
      this.dialogRef = null
    }
}
