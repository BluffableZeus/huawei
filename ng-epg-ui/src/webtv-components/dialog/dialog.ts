require('style-loader!./dialog.scss')
import {
    ComponentRef,
    ComponentFactoryResolver,
    ElementRef,
    Injectable,
    ResolvedReflectiveProvider,
    RenderComponentType,
    ViewEncapsulation,
    ReflectiveInjector,
    Renderer2,
    RendererFactory2,
    ViewContainerRef,
    Type
} from '@angular/core'
import { isPresent } from '../../util/util'
import { Animate } from './animate'
import { DialogRef } from './dialog-ref'
import { DialogConfig } from './dialog-config'
import { DialogContainer } from './dialog-container'
import { Backdrop } from './backdrop'
import { DialogParams } from './dialog-params'

// TODO(jelbourn): Wrap focus from end of dialog back to the start. Blocked on #1251
// TODO(jelbourn): Focus the dialog element when it is opened.
// TODO(jelbourn): Pre-built `alert` and `confirm` dialogs.
// TODO(jelbourn): Animate dialog out of / into opening element.

/**
 * Service for opening modal dialogs.
 */
@Injectable()
export class DialogService {

    /**
     * Unique id counter for RenderComponentType.
     * @private
     */
  static _uniqueId: number = 0

    /**
     * Renderer for manipulating dialog and backdrop component elements.
     * @private
     */
  private _renderer: Renderer2 = null
  private _defaultContainer = document.querySelector('body')

  constructor (public componentFactoryResolver: ComponentFactoryResolver, rootRenderer: RendererFactory2) {
      let type = {
          id: `epg-dialog-${DialogService._uniqueId++}`,
          encapsulation: ViewEncapsulation.None,
          styles: [],
          data: {}
        }
      this._renderer = rootRenderer.createRenderer({}, type)
    }

    /**
     * Opens a modal dialog.
     * @param type The component to open.
     * @param elementRef The logical location into which the component will be opened.
     * @param options
     * @returns Promise for a reference to the dialog.
     */
  open (type: Type<Function>, viewContainerRef: ViewContainerRef, options: DialogConfig = new DialogConfig(),
        providers = [], params: DialogParams = new DialogParams()): Promise<DialogRef> {
        // Create the dialnogRef here so that it can be injected into the content component.
      let dialogRef = new DialogRef()
      let bindings = ReflectiveInjector.resolve([
            { provide: DialogRef, useValue: dialogRef },
            { provide: DialogParams, useValue: params },
          ...providers
        ])

        // First, load the MdDialogContainer, into which the given component will be loaded.
      const containerFactory = this.componentFactoryResolver.resolveComponentFactory(DialogContainer)
      let childInjector = ReflectiveInjector.fromResolvedProviders(bindings, viewContainerRef.parentInjector)
      let containerRef = containerFactory.create(childInjector, null, null)
      viewContainerRef.insert(containerRef.hostView)

      let dialogElement = containerRef.location.nativeElement

      if (options.containerClass.trim()) {
          options.containerClass.split(/\s+/).forEach(cls => this._renderer.addClass(dialogElement, cls))
        }

      this._renderer.addClass(dialogElement, 'md-dialog-absolute');

      (options.container || this._defaultContainer).appendChild(dialogElement)

      if (isPresent(options.width)) {
          this._renderer.setStyle(dialogElement, 'width', options.width)
        }
      if (isPresent(options.height)) {
          this._renderer.setStyle(dialogElement, 'height', options.height)
        }

      dialogRef.containerRef = containerRef

        // Now load the given component into the MdDialogContainer.
      const componentFactory = this.componentFactoryResolver.resolveComponentFactory(type)
      let contentRef = componentFactory.create(childInjector, null, null)
      containerRef.instance.contentRef.insert(contentRef.hostView)

      Object.keys(options.context).forEach((key) => {
          contentRef.instance[key] = options.context[key]
        })

        // Wrap both component refs for the container and the content so that we can return
        // the `instance` of the content but the dispose method of the container back to the
        // opener.
      dialogRef.contentRef = contentRef
      containerRef.instance.dialogRef = dialogRef

      dialogRef.whenClosed.then((_) => {
          containerRef.destroy()
          contentRef.destroy()
        })

      return Animate.enter(dialogElement, 'md-active').then(() => dialogRef)
    }

    /** Loads the dialog backdrop (transparent overlay over the rest of the page). */
  _openBackdrop (viewContainerRef: ViewContainerRef, bindings: ResolvedReflectiveProvider[], options: DialogConfig): Promise<ComponentRef<any>> {
      const backdropFactory = this.componentFactoryResolver.resolveComponentFactory(Backdrop)
      const childInjector = ReflectiveInjector.fromResolvedProviders(bindings, viewContainerRef.parentInjector)
      const backdropRef = backdropFactory.create(childInjector, null, null)
      viewContainerRef.insert(backdropRef.hostView)
      let backdrop: Backdrop = backdropRef.instance
      backdrop.clickClose = options.clickClose
      this._renderer.addClass(backdropRef.location.nativeElement, 'md-backdrop')
      this._renderer.addClass(backdropRef.location.nativeElement, 'md-opaque')
      this._renderer.addClass(backdropRef.location.nativeElement, 'md-backdrop-absolute');
      (options.container || this._defaultContainer).appendChild(backdropRef.location.nativeElement)
      return backdrop.show().then(() => backdropRef)
    }

  alert (message: string, okMessage: string): Promise<any> {
      throw new Error('Not implemented')
    }

  confirm (message: string, okMessage: string, cancelMessage: string): Promise<any> {
      throw new Error('Not implemented')
    }
}
