import { ComponentRef } from '@angular/core'
import { Animate } from './animate'
import { isPresent } from '../../util/util'
import { Subject } from 'rxjs'

import { PromiseWrapper } from '../shared'
/**
 * Reference to an opened dialog.
 */
export class DialogRef extends Subject<any> {
    // Reference to the MdDialogContainer component.
  containerRef: ComponentRef<any>

    // Reference to the MdBackdrop component.
  _backdropRef: ComponentRef<any>

    // Reference to the Component loaded as the dialog content.
  _contentRef: ComponentRef<any>

    // Whether the dialog is closed.
  isClosed: boolean

    // Deferred resolved when the dialog is closed. The promise for this deferred is publicly exposed.
  whenClosedDeferred: any

    // Deferred resolved when the content ComponentRef is set. Only used internally.
  contentRefDeferred: any

    /**
     * The backdrop hiding subscription
     * @private
     */
  private _subscription: any = null
  constructor () {
      super()
      this._contentRef = null
      this.containerRef = null
      this.isClosed = false

      this.contentRefDeferred = PromiseWrapper.completer()
      this.whenClosedDeferred = PromiseWrapper.completer()
    }

  set backdropRef (value: ComponentRef<any>) {
      this._backdropRef = value
      if (this._backdropRef) {
          this._subscription = this._backdropRef.instance.onHiding.subscribe(() => {
              this._subscription.unsubscribe()
              this.close()
            })
        }
    }

  set contentRef (value: ComponentRef<any>) {
      this._contentRef = value
      this.contentRefDeferred.resolve(value)
    }

    /**
     * Gets the component instance for the content of the dialog.
     */
  get instance () {
      if (isPresent(this._contentRef)) {
          return this._contentRef.instance
        }
    }

    /**
     * Gets a promise that is resolved when the dialog is closed.
     */
  get whenClosed (): Promise<any> {
      return this.whenClosedDeferred.promise
    }

    /**
     * Closes the dialog. This operation is asynchronous.
     */
  close (result: any = null): Promise<void> {
      this.unsubscribe()
      if (this.isClosed) {
          return this.whenClosedDeferred.promise
        }
      if (this._subscription) {
          this._subscription.unsubscribe()
        }
      this.isClosed = true
      return Animate.leave(this.containerRef.location.nativeElement, 'md-active').then(() => {
          let otherAsync = Promise.resolve()
          if (this._backdropRef) {
              otherAsync = this._backdropRef.instance.hide()
            }
          return otherAsync.then(() => {
              this.whenClosedDeferred.resolve(result)
            })
        })
    }
}
