import { TranslateModule } from '@ngx-translate/core'
import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'
import { FormsModule } from '@angular/forms'
import { HttpClientModule } from '@angular/common/http'

import { TimeSelectComponent } from './time-select.component'

@NgModule({
  imports: [CommonModule, TranslateModule, FormsModule, HttpClientModule],
  exports: [TimeSelectComponent],
  declarations: [TimeSelectComponent],
  providers: [ ]
})
export class TimeSelectModule { }
