import * as _ from 'underscore'
import { Component, OnInit, Input } from '@angular/core'

@Component({
  selector: 'app-time-select',
  templateUrl: './time-select.component.html',
  styleUrls: ['./time-select.component.scss']
})
export class TimeSelectComponent implements OnInit {

  private chooseTimeIndex = -1
  private startHour = ''
  private startMin = ''
  private endHour = ''
  private endMin = ''
  private isEnterChangeButton = false
  private showNextDayIcon = false

  constructor () {

    }

  @Input() set sHour (data) {
      this.startHour = data
    }
  @Input() set sMin (data) {
      this.startMin = data
    }
  @Input() set eHour (data) {
      this.endHour = data
    }
  @Input() set eMin (data) {
      this.endMin = data
    }

  ngOnInit () {

    }

  chooseTimes (i) {
      this.chooseTimeIndex = i
    }

  KeyEventHandle (e) {
      if (e.target.id === 'start-hour' || e.target.id === 'end-hour') {
          if (Number(e.target.value) > 23) {
              e.target.value = '23'
            }
        } else if (e.target.id === 'start-min' || e.target.id === 'end-min') {
          if (Number(e.target.value) > 59) {
              e.target.value = '59'
            }
        }
      this.KeyEventHandleTwice(e)
      this.checkIsNextDay()
    }

  checkIsNextDay () {
      if (Number(this.endHour) < Number(this.startHour)) {
          this.showNextDayIcon = true
        } else if (Number(this.endHour) === Number(this.startHour) && Number(this.endMin) <= Number(this.startMin)) {
          this.showNextDayIcon = true
        } else {
          this.showNextDayIcon = false
        }
    }

  KeyEventHandleTwice (e) {
      if (e.keyCode === 38) {
          if (e.target.id === 'start-hour' || e.target.id === 'start-min') {
              this.increaseStart()
            }
          if (e.target.id === 'end-hour' || e.target.id === 'end-min') {
              this.increaseEnd()
            }
        } else if (e.keyCode === 40) {
          if (e.target.id === 'start-hour' || e.target.id === 'start-min') {
              this.decreaseStart()
            }
          if (e.target.id === 'end-hour' || e.target.id === 'end-min') {
              this.decreaseEnd()
            }
        } else if (e.keyCode !== 37 && e.keyCode !== 39) {
          e.target.value = e.target.value.replace(/\D/g, '')
          this.assignValue(e)
        }
    }

  assignValue (e) {
      switch (e.target.id) {
          case 'start-hour':
            this.startHour = e.target.value
            break
          case 'start-min':
            this.startMin = e.target.value
            break
          case 'end-hour':
            this.endHour = e.target.value
            break
          case 'end-min':
            this.endMin = e.target.value
            break
          default:
            break
        }
    }

  timeBlur (e) {
      if (this.isEnterChangeButton) {
          return
        }
      this.chooseTimeIndex = -1
      e.target.value = this.addZero(e.target.value)
    }

  addZero (date) {
      if (date.length === 1) {
          date = '0' + date
        }
      return date
    }

  increaseStart () {
      if (this.chooseTimeIndex === 1) {
          this.startHour = this.addZero((Number(this.startHour) + 1) + '')
          if (Number(this.startHour) < 0 || Number(this.startHour) >= 24) {
              this.startHour = '00'
            }
        }
      if (this.chooseTimeIndex === 2) {
          this.startMin = this.addZero((Number(this.startMin) + 1) + '')
          if (Number(this.startMin) < 0 || Number(this.startMin) >= 60) {
              this.startMin = '00'
            }
        }
      this.checkIsNextDay()
    }
    // adjust the start time to down.
  decreaseStart () {
      if (this.chooseTimeIndex === 1) {
          this.startHour = this.addZero((Number(this.startHour) - 1) + '')
          if (Number(this.startHour) < 0 || Number(this.startHour) >= 24) {
              this.startHour = '23'
            }
        }
      if (this.chooseTimeIndex === 2) {
          this.startMin = this.addZero((Number(this.startMin) - 1) + '')
          if (Number(this.startMin) < 0 || Number(this.startMin) >= 60) {
              this.startMin = '59'
            }
        }
      this.checkIsNextDay()
    }
    // adjust the end time to up.
  increaseEnd () {
      if (this.chooseTimeIndex === 3) {
          this.endHour = this.addZero((Number(this.endHour) + 1) + '')
          if (Number(this.endHour) < 0 || Number(this.endHour) >= 24) {
              this.endHour = '00'
            }
        }
      if (this.chooseTimeIndex === 4) {
          this.endMin = this.addZero((Number(this.endMin) + 1) + '')
          if (Number(this.endMin) < 0 || Number(this.endMin) >= 60) {
              this.endMin = '00'
            }
        }
      this.checkIsNextDay()
    }
    // adjust the end time to down.
  decreaseEnd () {
      if (this.chooseTimeIndex === 3) {
          this.endHour = this.addZero((Number(this.endHour) - 1) + '')
          if (Number(this.endHour) < 0 || Number(this.endHour) >= 24) {
              this.endHour = '23'
            }
        }
      if (this.chooseTimeIndex === 4) {
          this.endMin = this.addZero((Number(this.endMin) - 1) + '')
          if (Number(this.endMin) < 0 || Number(this.endMin) >= 60) {
              this.endMin = '59'
            }
        }
      this.checkIsNextDay()
    }

  dom (divName: string) {
      return document.getElementById(divName)
    }

  enterChangeButton (e) {
      this.isEnterChangeButton = true
    }

  outChangeButton (e) {
      this.isEnterChangeButton = false
      if (this.chooseTimeIndex === 1) {
          this.dom('start-hour').focus()
        }
      if (this.chooseTimeIndex === 2) {
          this.dom('start-min').focus()
        }
      if (this.chooseTimeIndex === 3) {
          this.dom('end-hour').focus()
        }
      if (this.chooseTimeIndex === 4) {
          this.dom('end-min').focus()
        }
    }
}
