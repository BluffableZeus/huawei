### Usage
```typescript
import { TimeSelectComponent } from 'ng-epg-ui/webtv-components/timeSelect';

<app-time-select [sHour]="startHour" [sMin]="startMin" [eHour]="endHour" [eMin]="endMin"></app-time-select>

```

### Annotations
```typescript
export class TimeSelectComponent {

    @Input() set sHour(data){
        this.startHour = data;
    }
    @Input() set sMin(data){
        this.startMin = data;
    }
    @Input() set eHour(data){
        this.endHour = data;
    }
    @Input() set eMin(data){
        this.endMin = data;
    }

}

```

 ### Attributes
| Attribute   | Description   | Type     | Options     | Default Value    | Mandatory (M) or Optional (O)    |
|-----   |-----    |-----    |------      |------    |------    |
| sHour | set the value of hour of start time  | string | '0','1','2','3','4',...,'23' |  |  M |
| sMin |  set the value of minute of start time | string | '0','1','2','3','4',...,'59' |  | M |
| eHour | set the value of hour of end time  | string | '0','1','2','3','4',...,'23' |  | M |
| eMin |  set the value of minute of end time | string | '0','1','2','3','4',...,'59' |  | M |
