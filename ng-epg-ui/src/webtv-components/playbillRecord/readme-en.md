### Usage
```typescript
import { PlaybillRecordComponent } from 'ng-epg-ui/webtv-components/playbillRecord';

<app-playbill-record [recData] = "recordData"></app-playbill-record>

```

### Annotations
```typescript
export class PlaybillRecordComponent {
    @Input() set recData(data) {
        this.recordData = data;
        this.isNow = this.recordData['isNow'];
        this.manAgeRecordMode();
        this.manAgeRecordType();
        this.quality = this.configData['default_reccfg_definition'];
        switch (this.quality) {
            case 'SD':
                this.defaultQualityKey = 0;
                break;
            case 'HD':
                this.defaultQualityKey = 1;
                break;
            case '4K':
                this.defaultQualityKey = 2;
                break;
            default:
                this.defaultQualityKey = 1;
        }
        if (this.configData['BeginOffset']) {
            this.preTime = parseInt(this.configData['BeginOffset'], 10) / 60;
        }
        if (this.configData['EndOffset']) {
            this.endTime = parseInt(this.configData['EndOffset'], 10) / 60;
        }
        this.arrowPreList = [0, 5, 10, 20, 30, 45];
        this.arrowPostList = [0, 5, 10, 20, 30, 45];
    }
}

```

 ### Attributes
| Attribute   | Description   | Type     | Options     | Default Value    | Mandatory (M) or Optional (O)    |
|-----   |-----    |-----    |------      |------    |------    |
| recData | Playbill data that is used to add recording task. It contains properties that decides which type of recording task we can add | PlaybillDetail |  |  |  M |

 ### Data Type
 #### PlaybillDetail
| Attribute   | Description   | Type    | Default Value    | Mandatory (M) or Optional (O)    |
|-----   |-----    |-----    |------    |------    |
| ID | 节目的内部唯一标识 | string |  |  M |
| channelDetail | 节目单所属的频道 | ChannelDetail |  |  M |
| playbillSeries | 连续剧节目单属性 | PlaybillSeries |  |  O |
| startTime | 节目单开始时间 | string |  |  O |
| isNow | 是否是直播 | boolean |  |  O |

 #### ChannelDetail
| Attribute   | Description   | Type    | Default Value    | Mandatory (M) or Optional (O)    |
|-----   |-----    |-----    |------    |------    |
| ID | 频道ID | string |  |  M |
| name | 频道名称 | string |  |  M |
| physicalChannels | 频道的物理频道信息 | object |  |  M |
|  |  |  |  |   |

 #### PlaybillSeries
| Attribute   | Description   | Type    | Default Value    | Mandatory (M) or Optional (O)    |
|-----   |-----    |-----    |------    |------    |
| seriesID | 节目单所属系列的ID | string |  |  M |