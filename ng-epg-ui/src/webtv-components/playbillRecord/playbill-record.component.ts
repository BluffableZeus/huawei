import { Component, OnInit, Input } from '@angular/core'
import * as _ from 'underscore'
@Component({
  selector: 'app-playbill-record',
  styleUrls: ['./playbill-record.component.scss'],
  templateUrl: './playbill-record.component.html'
})

export class PlaybillRecordComponent implements OnInit {
  private hasExtended = false
  private prePaddingArrow = 'down'
  private postPaddingArrow = 'down'
  private preTime = 0
  private endTime = 0
  private arrowPreList = []
  private arrowPostList = []
  private recordType = false
  private singleRecordType = true
  private periodicRecordType = false
  private singleRecordFalse = false
  private periodicRecordFalse = false
  private isNow = true
  private canSetPrePadding = true
  private storageType = 'NPVR'
  private showQualityList = false
  private quality = 'HD'
  private qualityList = ['HD', 'SD', '4K']
  private recordingMode
  private defaultQualityKey
  private curQualityObj = {
      'quality': '',
      'qualityKey': -1,
      'cpvrMediaID': ''
    }
  private selectCloud = true
  private showQullity = true
  private canNPVRRecord = true
  private canCPVRRecord = true
  private configData = {
      'supportAllCPVR' : true,
      'PVRMode': 'NPVR',
      'default_reccfg_definition': 'SD',
      'default_reccfg_pvr_type': 'NPVR',
      'default_reccfg_single_or_series': 'Single'
    }
  private recordData
  private supportAllCPVR = false
  private PVRMode = ''

  constructor () {
    }

  @Input() set recData (data) {
      this.recordData = data
      this.isNow = this.recordData['isNow']
      this.manAgeRecordMode()
      this.manAgeRecordType()
      this.quality = this.configData['default_reccfg_definition']
      switch (this.quality) {
          case 'SD':
            this.defaultQualityKey = 0
            break
          case 'HD':
            this.defaultQualityKey = 1
            break
          case '4K':
            this.defaultQualityKey = 2
            break
          default:
            this.defaultQualityKey = 1
        }
      if (this.configData['BeginOffset']) {
          this.preTime = parseInt(this.configData['BeginOffset'], 10) / 60
        }
      if (this.configData['EndOffset']) {
          this.endTime = parseInt(this.configData['EndOffset'], 10) / 60
        }
      this.arrowPreList = [0, 5, 10, 20, 30, 45]
      this.arrowPostList = [0, 5, 10, 20, 30, 45]
    }

  ngOnInit () {
      this.recordingMode = this.configData.PVRMode
      if (this.canNPVRRecord && this.canCPVRRecord) {
          if (this.configData['default_reccfg_pvr_type'] === 'NPVR' && this.configData['PVRMode'] !== 'CPVR') {
              this.selectCloud = true
            } else {
              this.selectCloud = false
              this.showQullity = true
              this.recordType = true
            }
        } else if (this.canNPVRRecord && !this.canCPVRRecord) {
          this.selectCloud = true
          this.showQullity = false
        } else if (!this.canNPVRRecord && this.canCPVRRecord) {
          this.selectCloud = false
          this.showQullity = true
          this.recordType = true
        }
      this.qualityList = []
    }

    /**
     * manage the default values of single or series recording
     * other record status can click or not
     */
  manAgeRecordType () {
      if (this.recordData['isNPVR'] === '1' && !this.recordData['playbillSeries']['seriesID']) {
          this.singleRecordType = true
          this.periodicRecordType = false
          this.singleRecordFalse = true
          if (this.isNow) {
              this.canSetPrePadding = false
            } else {
              this.canSetPrePadding = true
            }
        }
      this.canNPVRRecord = true
      this.canCPVRRecord = true
      if (this.recordData['playbillSeries']['seriesID']) {
          if (this.configData['default_reccfg_single_or_series'] === 'Single') {
              this.singleRecordType = true
              this.periodicRecordType = false
              if (this.isNow) {
                  this.canSetPrePadding = false
                } else {
                  this.canSetPrePadding = true
                }
            } else {
              this.singleRecordType = false
              this.periodicRecordType = true
              this.canSetPrePadding = true
            }
          this.singleRecordFalse = true
          this.periodicRecordFalse = true
        }
    }

  selectStorage (data) {
      if (this.canNPVRRecord && data === 'cloud') {
          this.storageType = 'NPVR'
          this.selectCloud = true
          this.showQullity = false
        } else if (this.canCPVRRecord && data === 'stb') {
          this.storageType = 'CPVR'
          this.selectCloud = false
          this.showQullity = true
        }
      this.hideListContent([1, 2, 3])
    }

  changeFocus (type) {
      if (type === 'singleRecordType') {
          if (this.singleRecordFalse) {
              this.singleRecordType = true
              this.periodicRecordType = false
            }
          if (this.isNow) {
              this.canSetPrePadding = false
            } else {
              this.canSetPrePadding = true
            }
        } else {
          if (this.periodicRecordFalse) {
              this.singleRecordType = false
              this.periodicRecordType = true
              this.canSetPrePadding = true
            }
          if (!this.supportAllCPVR) {
              this.storageType = 'NPVR'
              this.selectCloud = true
              this.showQullity = false
            }
        }
      this.hideListContent([1, 2, 3])
    }

    // show the Quality list
  choiceQuality () {
      this.showQualityList = !this.showQualityList
      this.hideListContent([2, 3])
    }

    // the mouse leave the Quality list
  mouseOutQualityList () {
      this.showQualityList = false
    }
    // select the quality of record task
  selectQuality (qualityObj) {
      this.curQualityObj = qualityObj
      this.showQualityList = false
    }
    // click other postion then hide the Quality list
  hideListContent (key: Array<number>) {
      if (_.contains(key, 1)) {
          this.showQualityList = false
        }
      if (_.contains(key, 2)) {
          this.prePaddingArrow = 'down'
        }
      if (_.contains(key, 3)) {
          this.postPaddingArrow = 'down'
        }
    }

    /**
     *  manage NPVR and CPVR record
     */
  manAgeRecordMode () {
      if (this.configData['PVRMode'] === 'NPVR') {
          this.recordType = false
          this.storageType = 'NPVR'
        } else if (this.configData['PVRMode'] === 'CPVR') {
          this.selectCloud = false
          this.recordType = true
          this.storageType = 'CPVR'
        } else if (this.configData['PVRMode'] === 'MIXPVR' || this.configData['PVRMode'] === 'CPVR&NPVR') {
          this.recordType = true
        }
    }

    /**
     * add record:
     * single record
     * series record
     */
  addRecord (event?) {

    }

    /**
     * the prepaddingof record time, does delete the drop list manually or not
     * show or hide
     */
  extendedSet () {
      if (this.hasExtended) {
          this.hasExtended = false
        } else {
          this.hasExtended = true
          this.prePaddingArrow = 'down'
          this.postPaddingArrow = 'down'
        }
      this.hideListContent([1, 2, 3])
    }

  dealArrow (type) {
      if (this[type] === 'up') {
          this[type] = 'down'
        } else {
          if (type === 'prePaddingArrow') {
              if (this.canSetPrePadding) {
                  this[type] = 'up'
                  this.postPaddingArrow = 'down'
                }
            } else if (type === 'postPaddingArrow') {
              this[type] = 'up'
              this.prePaddingArrow = 'down'
            }
        }
      if (type === 'prePaddingArrow') {
          this.hideListContent([1, 3])
        } else {
          this.hideListContent([1, 2])
        }
    }

  changePrePadding (data) {
      this.preTime = data
      this.prePaddingArrow = 'down'
    }

  changePostPadding (data) {
      this.endTime = data
      this.postPaddingArrow = 'down'
    }

  close () {

    }
}
