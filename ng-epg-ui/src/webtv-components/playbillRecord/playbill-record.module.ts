import { TranslateModule } from '@ngx-translate/core'
import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'
import { FormsModule } from '@angular/forms'
import { HttpClientModule } from '@angular/common/http'

import { PlaybillRecordComponent } from './playbill-record.component'

@NgModule({
  imports: [CommonModule, TranslateModule, FormsModule, HttpClientModule],
  exports: [PlaybillRecordComponent],
  declarations: [PlaybillRecordComponent],
  providers: [ ]
})
export class PlaybillRecordModule { }
