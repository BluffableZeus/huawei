import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { VerticalPosterComponent } from './verticalPoster.component'

@NgModule({
  imports: [CommonModule],
  declarations: [VerticalPosterComponent],
  exports: [VerticalPosterComponent],
  providers: []
})
export class VerticalPosterModule { }
