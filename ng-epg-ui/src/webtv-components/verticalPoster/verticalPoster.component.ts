import { Component, EventEmitter, Input, Output } from '@angular/core'
import { VOD } from 'ng-epg-sdk'

@Component({
  selector: 'vod-poster',
  templateUrl: './verticalPoster.component.html',
  styleUrls: ['./verticalPoster.component.scss']
})
export class VerticalPosterComponent {
  @Input() public vod: VOD
  @Input() public ratingShow: boolean = true
  @Output() public selected: EventEmitter<VOD> = new EventEmitter<VOD>()
}
