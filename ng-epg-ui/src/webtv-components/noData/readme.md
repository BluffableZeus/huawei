### RatingStar Author
```typescript
```

### Usage
```typescript
import { RatingStar } from 'ng2-bootstrap/ng2-bootstrap';
```

### Annotations
```typescript
// class RatingStar
import { Input, Component, OnInit, Output, EventEmitter, HostListener } from '@angular/core';
import { NgIf, NgFor, NgSwitch, NgSwitchWhen, NgSwitchDefault } from '@angular/common';


@Component({
    selector: 'ratingstar',
    directives: [NgIf, NgFor, NgSwitch, NgSwitchWhen, NgSwitchDefault],
    templateUrl: './ratingstar.component.html',
    styleUrls: ['./ratingstar.component.scss']
})
export class RatingStar implements OnInit {
    @Input() public starLength: number;

    @Input() public get initialRating():number {
        return this._initialRating;
    }

    public set initialRating(newRating: number) {
        this._initialRating = newRating;
        this.refreshRatingStar();
    };

    @Input() private ratingStep: number;
    @Input() private readOnly: boolean;

    @Output() public changeRating: EventEmitter<number> = new EventEmitter();
    private _initialRating: number;

    @HostListener('keydown', ['$event'])
    private onKeydown(event: KeyboardEvent) {
        if (this.readOnly) {
            return;
        }
        if ([37, 38, 39, 40].indexOf(event.which) >= 0) {
            let sign = event.which === 38 || event.which === 39 ? 1 : -1;
            this.initialRating += sign;
            this.initialRating = Math.max(0, this.initialRating);
            this.initialRating = Math.min(this.starLength * this.ratingStep * 2, this.initialRating);
            this.refreshRatingStar();
        }
        if (event.which === 13) {
            this.changeRating.emit(this.initialRating);
        }
    }

    public starArray: number[] = new Array();

    constructor() {
        
    }

    ngOnInit() {
        this.refreshRatingStar();
    }

    refreshRatingStar() {
        for (let i = 0; i < this.starLength; i++) {
            let result = this.initialRating - ((2 / this.ratingStep) * (i + 1));
            if (result >= 0) {
                this.starArray[i] = 2;
            } else if (result === -1) {
                this.starArray[i] = 1;
            } else {
                this.starArray[i] = 0;
            }
        }
    }
}

```

### RatingStar properties
- `starLength` (`?:starLength:number=5'`) - 展示星星的个数，默认展示5颗星
- `initialRating` (`?:initialRating:number=0`) - 评价初始值，用于展示，默认值为0
- `ratingStep` (`?:ratingStep:number=1`) - 评分时修改跨度，支持半颗行及一颗星
- `readOnly` (`?readOnly:boolean=false`) - 评价是否可以编辑，用于展示和评价两种场景

### RatingStar events

