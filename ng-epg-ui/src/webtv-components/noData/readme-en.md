### Usage
```typescript
import { WaterfallnoData } from 'ng-epg-ui/webtv-components/noData';

<waterfall-noData [nodatatype]="'ondemand'"></waterfall-noData>

```

### Annotations
```typescript
export class WaterfallnoData implements OnInit {
    @Input() public nodatatype: string;
}

```

 ### Attributes
| Attribute   | Description   | Type     | Options     | Default Value    | Mandatory (M) or Optional (O)    |
|-----   |-----    |-----    |------      |------    |------    |
| nodatatype | The type 'no data' that needs to be shown  | string | home,search,ondemand,history,favoriteVod,favoritechannel,locked1,locked2,reminder..... | home|  O |
