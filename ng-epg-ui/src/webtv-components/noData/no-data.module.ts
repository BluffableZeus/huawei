import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { WaterfallnoData } from './noData.component'
import { TranslateModule } from '@ngx-translate/core'

import { TranslateService } from '@ngx-translate/core'

@NgModule({
  imports: [ CommonModule, TranslateModule ],
  exports: [WaterfallnoData],
  declarations: [WaterfallnoData],
  providers: [TranslateService]
})
export class NoDataModule { }
