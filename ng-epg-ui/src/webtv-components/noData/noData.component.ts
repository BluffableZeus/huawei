import { Component, OnInit, Input } from '@angular/core'
import { TranslateService } from '@ngx-translate/core'

@Component({
  selector: 'waterfall-noData',
  providers: [],
  templateUrl: './noData.component.html',
  styleUrls: ['./noData.component.scss']
})
export class WaterfallnoData implements OnInit {
  @Input() public nodatatype: string
  public noDataone: string = ''
  public noDatatwo: string = ''
  public noDatathree: string = ''
  public noDatafour: string = ''
  public isShowHome: string = ''
  public showImg = true

  constructor (
        private translate: TranslateService
        ) {
    }

  ngOnInit () {
      this.showImg = true
      switch (this.nodatatype) {
          case 'home':
            this.isShowHome = 'true'
            this.noDataone = 'homeNoDataone'
            this.noDatatwo = 'homeNoDatatwo'
            break
          case 'search':
            this.isShowHome = 'false'
            this.noDataone = 'searchNoDataone'
            this.noDatatwo = 'searchNoDatatwo'
            break
          case 'ondemand':
            this.isShowHome = 'true'
            this.noDataone = 'ondemandNoDataOne'
            this.noDatatwo = 'ondemandNoDataTwo'
            break
          case 'history':
            this.isShowHome = 'false'
            this.noDataone = 'there_is_no_any_watch_history_yet'
            this.noDatatwo = 'please_go_to_on_deamnd_all_contents_ to_enjoy'
            break
          case 'favoriteVod':
            this.isShowHome = 'false'
            this.noDataone = ''
            this.noDatatwo = 'go_on_demand_all'
            break
          case 'favoritechannel':
            this.isShowHome = 'false'
            this.noDataone = 'no_favorite_channel'
            this.noDatatwo = 'go_to_add_interested'
            break
          case 'locked1':
            this.isShowHome = 'false'
            this.noDataone = 'there_is_no_any_locked_channel'
            this.noDatatwo = 'please_go_to_tv_guide_add_what_you_want_to_lock'
            break
          case 'locked2':
            this.isShowHome = 'false'
            this.noDataone = 'there_is_no_any_locked_channel'
            break
          case 'reminder':
            this.isShowHome = 'false'
            this.noDataone = 'no_reminder_program'
            this.noDatatwo = 'go_to_tv_guide'
            break
          case 'purchased':
            this.isShowHome = 'false'
            this.noDataone = 'no_purchased_string'
            break
          case 'recordCloud':
            this.isShowHome = 'false'
            this.showImg = false
            this.noDataone = 'no_cloud_recording'
            this.noDatatwo = 'to_record_a_program_via_these_ways'
            this.noDatathree = 'click_record_button_in_the_program_detail_view'
            this.noDatafour = 'click_manual_recording_tab_on_the above_area'
            break
          case 'recordScheduled':
            this.isShowHome = 'false'
            this.showImg = false
            this.noDataone = 'no_scheduled_recording'
            this.noDatatwo = 'record_a_program_via_these_ways'
            this.noDatathree = 'click_record_button_in_the_program_detail_view'
            this.noDatafour = 'click_manual_recording_tab_on_the_above_area'
            break
          case 'recordStb':
            this.isShowHome = 'false'
            this.showImg = false
            this.noDataone = 'no_STB_recording'
            this.noDatatwo = 'record_a_program_via_these_ways'
            this.noDatathree = 'click_record_button_in_the_program_detail_view'
            this.noDatafour = 'click_manual_recording_tab_on_the_above_area'
            break
          case 'noFilter':
            this.isShowHome = 'false'
            this.showImg = false
            this.noDataone = 'no_any_result_related_to_your filter'
            break
          case 'miniEPG':
            this.isShowHome = 'true'
            this.noDataone = 'no_favorite_channel'
            break
          case 'noManualChannels':
            this.isShowHome = 'false'
            this.noDataone = 'no_avaiable_channel_to_record'
            break
          default:
            this.isShowHome = 'true'
            this.noDataone = 'homeNoDataone'
            this.noDatatwo = 'homeNoDatatwo'
            break
        }
    }
}
