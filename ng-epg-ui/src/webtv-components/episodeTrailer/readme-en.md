### Usage
```typescript
import { EpisodeTrailerComponent } from 'ng-epg-ui/webtv-components/episodeTrailer';

<app-episode-trailer></app-episode-trailer>

```

### Annotations
```typescript
export class EpisodeTrailerComponent {
    @Input() set setEpisode(details) {
        this.details = details;
    }
}

```

 ### Attributes
| Attribute   | Description   | Type     | Options     | Default Value    | Mandatory (M) or Optional (O)    |
|-----   |-----    |-----    |------      |------    |------    |
| setEpisode | VOD detail information that is used to showing Episodes and Trailers | VODDetail |  |  |  M |

 ### Data Type
 #### VODDetail
| Attribute   | Description   | Type    | Default Value    | Mandatory (M) or Optional (O)    |
|-----   |-----    |-----    |------    |------    |
| ID | 点播内容ID | string |  |  M |
| VODType | VOD类型, 0：非电视剧, 1：普通连续剧, 2：季播剧父集, 3：季播剧  | string |  |  M |
| clipfiles | 片花媒体文件列表 | array |  |  O |
| episodeCount | 子集总集数 | string |  |  O |
| episodes | 子集列表 | array |  |  O |
