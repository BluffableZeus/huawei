import { TranslateModule } from '@ngx-translate/core'
import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'
import { PictureService } from 'ng-epg-ui/services/picture.service'
import { DirectionScroll } from 'ng-epg-ui/webtv-components/scroll/directionScroll.component'
import { TransverseScroll } from 'ng-epg-ui/webtv-components/scroll/transverseScroll.component'

import { EpisodeTrailerComponent } from './episode-trailer.component'

@NgModule({
  imports: [CommonModule, TranslateModule],
  exports: [EpisodeTrailerComponent],
  declarations: [EpisodeTrailerComponent],
  providers: [PictureService, DirectionScroll, TransverseScroll]
})
export class EpisodeTrailerModule { }
