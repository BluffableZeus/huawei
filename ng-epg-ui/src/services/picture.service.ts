import * as _ from 'underscore'
/**
 * the kind of dealing with picture.
 */
export class PictureService {

    /**
     * @param picture
     * @param size:string
     * - xl
     * - l
     * - m
     * - s
     * - xs
     * @param type
     *
     *
     */
  convertToSizeUrl (pictureUrls: Array<string> | string, size): string {
      if (_.isEmpty(pictureUrls)) {
          return ''
        }

      let url = pictureUrls as string
      if (_.isArray(pictureUrls)) {
          url = _.first(pictureUrls)
        }
      if (url.startsWith('http://')) {
          // DISABLE FOR C50
          //url = url.replace(/http:\/\/([a-zA-Z0-9._-]+)(?::[0-9]+)?/, 'https://$1')
        }

      size = size || {}
      if (url) {
          let clientW: number = window.innerWidth
          if (clientW > 1440 && clientW <= 1920) {
              return url = url + '?x=' + size.maxwidth + '&y=' + size.maxheight + '&ar=ignore'
            } else {
              return url = url + '?x=' + size.minwidth + '&y=' + size.minheight + '&ar=ignore'
            }
        } else {
          return ''
        }
    }
}
