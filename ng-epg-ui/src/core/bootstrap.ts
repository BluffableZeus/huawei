import { bootstrap } from '@angular/platform-browser-dynamic'
import { Directive, ReflectiveInjector, RootRenderer, Renderer, enableProdMode, ViewContainerRef, provide, PLATFORM_DIRECTIVES, ComponentRef, NgZone, DynamicComponentLoader } from '@angular/core'
import { ROUTER_PROVIDERS } from '@angular/router'
import { LocationStrategy, HashLocationStrategy } from '@angular/common'

import { App } from './app'
import { Config } from './config'
import { Events } from '../util/events'
import { IONIC_DIRECTIVES } from './directives'
import { isPresent } from '../util/util'
import { NavRegistry } from './nav/nav-registry'
const _reflect: any = Reflect

export function epgBootstrap (appRootComponent: any, customProviders?: Array<any>, config?: any): Promise<ComponentRef<any>> {
  // get all Ionic Providers
  let providers = epgProviders(customProviders, config)

  // automatically set "ion-app" selector to users root component
  addSelector(appRootComponent, 'ion-app')

  // call angular bootstrap
  return bootstrap(appRootComponent, providers).then(ngComponentRef => {
    // epg app has finished bootstrapping
    return epgPostBootstrap(ngComponentRef)
  })
}

export function epgPostBootstrap (ngComponentRef: ComponentRef<any>): ComponentRef<any> {
  let app: App = ngComponentRef.injector.get(App)
  app.setAppInjector(ngComponentRef.injector)

  return ngComponentRef
}

export function epgProviders (customProviders?: Array<any>, config?: any): any[] {
  let directives = IONIC_DIRECTIVES

  // add custom providers to Ionic's app
  customProviders = isPresent(customProviders) ? customProviders : []

  // create an instance of Config
  if (!(config instanceof Config)) {
    config = new Config(config)
  }

  let events = new Events()
  let navRegistry = new NavRegistry()

  return [
    App,
    provide(Config, { useValue: config }),
    provide(Events, { useValue: events }),
    provide(NavRegistry, { useValue: navRegistry }),
    provide(PLATFORM_DIRECTIVES, { useValue: [directives], multi: true }),
    ROUTER_PROVIDERS,
    provide(LocationStrategy, { useClass: HashLocationStrategy }),
    customProviders
  ]
}

/**
 * @private
 */
export function addSelector (type: any, selector: string) {
  if (type) {
    let annotations = _reflect.getMetadata('annotations', type)
    if (annotations && !annotations[0].selector) {
      annotations[0].selector = selector
      _reflect.defineMetadata('annotations', annotations, type)
    }
  }
}
