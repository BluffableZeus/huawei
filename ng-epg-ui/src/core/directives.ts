import { forwardRef, Type } from '@angular/core'
import { CORE_DIRECTIVES, FORM_DIRECTIVES } from '@angular/common'

import { Nav } from './nav'
import { NavPush, NavPop } from './nav'
import { NavRouter } from './nav'

/**
 * @name IONIC_DIRECTIVES
 * @description
 * The core Ionic directives as well as Angular's `CORE_DIRECTIVES` and `FORM_DIRECTIVES` are
 * available automatically when you bootstrap your app with the `ionicBootstrap`. This means
 * if you are using custom components you do not need to import `IONIC_DIRECTIVES` as they
 * are part of the app's default directives.
 *
 *
 * #### Angular
 * - CORE_DIRECTIVES
 * - FORM_DIRECTIVES
 *
 */
export const IONIC_DIRECTIVES: any[] = [
    // Angular
  CORE_DIRECTIVES,
  FORM_DIRECTIVES,

    //Nav
  Nav,
  NavPush,
  NavPop,
  NavRouter
]
