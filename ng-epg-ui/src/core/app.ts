import { Injectable, Injector, Type } from '@angular/core'

import { Config } from './config'
import { NavController } from './nav'
import { session } from '../../../demos-app/sdk/session'

/**
 * Ionic App utility service.
 */
@Injectable()
export class App {
  private _disTime: number = 0
  private _rootNav: any = null
  private _appInjector: Injector
  private routers: Array<RouterCfg> = []
  private defaultRouter: RouterCfg

  constructor (
        private _config: Config
    ) {}

    /**
     * @private
     * Sets if the app is currently enabled or not, meaning if it's
     * available to accept new user commands. For example, this is set to `false`
     * while views transition, a modal slides up, an action-sheet
     * slides up, etc. After the transition completes it is set back to `true`.
     * @param {boolean} isEnabled
     * @param {boolean} fallback  When `isEnabled` is set to `false`, this argument
     * is used to set the maximum number of milliseconds that app will wait until
     * it will automatically enable the app again. It's basically a fallback incase
     * something goes wrong during a transition and the app wasn't re-enabled correctly.
     */
  setEnabled (isEnabled: boolean, duration: number = 700) {
      this._disTime = (isEnabled ? 0 : Date.now() + duration)
    }

    /**
     * @private
     * Boolean if the app is actively enabled or not.
     * @return {boolean}
     */
  isEnabled (): boolean {
      return (this._disTime < Date.now())
    }

    /**
     * @private
     */
  getActiveNav (): any {
      let nav = this._rootNav || null
      let activeChildNav: any

      while (nav) {
          activeChildNav = nav.getActiveChildNav()
          if (!activeChildNav) {
              break
            }
          nav = activeChildNav
        }

      return nav
    }

    /**
     * @private
     */
  getRootNav (): NavController {
      return this._rootNav
    }

    /**
     * @private
     */
  setRootNav (nav: NavController) {
      this._rootNav = nav
    }

    /**
     * Set the global app injector that contains references to all of the instantiated providers
     * @param injector
     */
  setAppInjector (injector: Injector) {
      this._appInjector = injector
    }

    /**
     * Get an instance of the global app injector that contains references to all of the instantiated providers
     * @returns {Injector}
     */
  getAppInjector (): Injector {
      return this._appInjector
    }

  setRouters (routers: Array<RouterCfg>) {
      this.routers = routers
      this.defaultRouter = routers.find(r => r.useAsDefault)
    }

  getRouter (nameOrpath: string = location.hash) {

      let routerStack: Array<any> = session.get('ROUTER_STACK') || []
      let stack = routerStack.reverse().find(s => s.path === nameOrpath)

      let router = this.routers.find(r => {
          if (nameOrpath === r.path || nameOrpath === r.name) {
              return true
            } else if (stack) {
              let relPath = stack.param ? r.path.replace(/\:(\w+)/, (_, key) => stack.param[key]) : r.path
              return relPath === nameOrpath
            }
        })

      if (router && stack) {
          router.params = stack.params
        }

      return router || this.defaultRouter
    }
}

export interface RouterCfg {
  path: string
  name: string
  component: Type
  useAsDefault?: boolean
  params?: any
}
