
export class UIConfig {
  private store: { [key: string]: any } = {}

  constructor () {
    }

    /**
     *
     */
  get (key: string, fallbackValue: any = null): any {
      const value = this.store[key]

      if (_.isUndefined(value) || _.isNull(value)) {
          return fallbackValue
        }

      return value
    }

    /**
     *
     */
  set (key: string, value: any) {
      this.store[key] = value
      return this
    }

  getBoolean (key: string, fallbackValue: any = null): boolean {
      let val = this.get(key)
      if (val === null) {
          return fallbackValue
        }
      if (typeof val === 'string') {
          return val === 'true'
        }
      return !!val
    }

  getNumber (key: string, fallbackValue: number = NaN): number {
      let val = parseFloat(this.get(key))
      return isNaN(val) ? fallbackValue : val
    }
}
