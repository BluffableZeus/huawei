
/**
 * @name NavParams
 * @description
 * NavParams are an object that exists on a page and can contain data for that particular view.
 * Similar to how data was pass to a view in V1 with `$stateParams`, NavParams offer a much more flexible
 * option with a simple `get` method.
 *
 * @demo /docs/v2/demos/nav-params/
 * @see {@link /docs/v2/components#navigation Navigation Component Docs}
 * @see {@link ../NavController/ NavController API Docs}
 * @see {@link ../Nav/ Nav API Docs}
 * @see {@link ../NavPush/ NavPush API Docs}
 */
export class NavParams {

  /**
   * @private
   * @param {TODO} data  TODO
   */
  constructor (public data: any = {}) {}

  /**
   * Get the value of a nav-parameter for the current view
   *
   * @param {string} parameter Which param you want to look up
   */
  get (param: string): any {
    return this.data[param]
  }
}
