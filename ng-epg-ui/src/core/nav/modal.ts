require('style!./modal.scss')

import { Component, ComponentResolver, HostListener, Renderer, ViewChild, ViewContainerRef, ElementRef } from '@angular/core'

import { addSelector } from '../bootstrap'
import { Animation } from '../../animations/animation'
import { isPresent, pascalCaseToDashCase } from '../../util/util'
import { NavParams } from './nav-params'
import { ViewController } from './view-controller'
import { windowDimensions } from '../../util/dom'

/**
 * @name Modal
 * @description
 * A Modal is a content pane that goes over the user's current page.
 * Usually it is used for making a choice or editing an item. A modal uses the
 * `NavController` to
 * {@link /docs/v2/api/components/nav/NavController/#present present}
 * itself in the root nav stack. It is added to the stack similar to how
 * {@link /docs/v2/api/components/nav/NavController/#push NavController.push}
 * works.
 *
 * When a modal (or any other overlay such as an alert or actionsheet) is
 * "presented" to a nav controller, the overlay is added to the app's root nav.
 * After the modal has been presented, from within the component instance The
 * modal can later be closed or "dismissed" by using the ViewController's
 * `dismiss` method. Additionally, you can dismiss any overlay by using `pop`
 * on the root nav controller.
 *
 * Data can be passed to a new modal through `Modal.create()` as the second
 * argument. The data can then be accessed from the opened page by injecting
 * `NavParams`. Note that the page, which opened as a modal, has no special
 * "modal" logic within it, but uses `NavParams` no differently than a
 * standard page.
 *
 * @demo /docs/v2/demos/modal/
 * @see {@link /docs/v2/components#modals Modal Component Docs}
 */
export class Modal extends ViewController {

  constructor (componentType: any, data: any = {}, opts: ModalOptions = {}) {
    data.componentType = componentType
    opts.showBackdrop = isPresent(opts.showBackdrop) ? !!opts.showBackdrop : true
    opts.enableBackdropDismiss = isPresent(opts.enableBackdropDismiss) ? !!opts.enableBackdropDismiss : true
    data.opts = opts

    super(ModalCmp, data)
    this.isOverlay = true
    this.usePortal = true
  }

  /**
   * @private
   */
  getTransitionName (direction: string) {
    let key = (direction === 'back' ? 'modalLeave' : 'modalEnter')
    return this._nav && this._nav.config.get(key)
  }

  /**
   * Create a modal with the following options
   *
   * | Option                | Type       | Description                                                                                                      |
   * |-----------------------|------------|------------------------------------------------------------------------------------------------------------------|
   * | showBackdrop          |`boolean`   | Whether to show the backdrop. Default true.                                                                      |
   * | enableBackdropDismiss |`boolean`   | Whether the popover should be dismissed by tapping the backdrop. Default true.                                   |
   *
   *
   * @param {object} componentType The Modal view
   * @param {object} data Any data to pass to the Modal view
   * @param {object} opts Modal options
   */
  static create (componentType: any, data: any = {}, opts: ModalOptions = {}) {
    return new Modal(componentType, data, opts)
  }

  // Override the load method and load our child component
  loaded (done: Function) {
    // grab the instance, and proxy the ngAfterViewInit method
    let originalNgAfterViewInit = this.instance.ngAfterViewInit

    this.instance.ngAfterViewInit = () => {
      if (originalNgAfterViewInit) {
        originalNgAfterViewInit()
      }
      this.instance.loadComponent(done)
    }
  }
}

export interface ModalOptions {
  showBackdrop?: boolean
  enableBackdropDismiss?: boolean
}

@Component({
  selector: 'ion-modal',
  template:
    '<ion-backdrop disableScroll="false" (click)="bdClick($event)"></ion-backdrop>' +
    '<div class="modal-wrapper">' +
  '<div #viewport nav-viewport></div>' +
    '</div>'
})
export class ModalCmp {

  @ViewChild('viewport', { read: ViewContainerRef }) viewport: ViewContainerRef

  private d: any
  private enabled: boolean

  constructor (
    private _compiler: ComponentResolver,
    private _renderer: Renderer,
    private _navParams: NavParams,
    private _viewCtrl: ViewController,
    private elementRef: ElementRef) {

    this.d = _navParams.data.opts
  }

  loadComponent (done: Function) {
    let componentType = this._navParams.data.componentType
    addSelector(componentType, 'ion-page')

    this._compiler.resolveComponent(componentType).then((componentFactory) => {
      let componentRef = this.viewport.createComponent(componentFactory, this.viewport.length, this.viewport.parentInjector)
      const viewController = this._viewCtrl
      Object.keys(viewController.data.context).forEach((key) => {
        componentRef.instance[key] = viewController.data.context[key]
      })
      this._renderer.setElementClass(componentRef.location.nativeElement, 'show-page', true)
      // auto-add page css className created from component JS class name
      let cssClassName = pascalCaseToDashCase(componentType.name)
      this._renderer.setElementClass(componentRef.location.nativeElement, cssClassName, true)
      if (viewController.data.containerClass.trim()) {
        viewController.data.containerClass.split(/\s+/).forEach(cls => this._renderer.setElementClass(this.elementRef.nativeElement, cls, true))
      }
      this._viewCtrl.setInstance(componentRef.instance)
      this.enabled = true
      done()
    })
  }

  ngAfterViewInit () {
    // intentionally kept empty
  }

  dismiss (role: any): Promise<any> {
    return this._viewCtrl.dismiss(null, role)
  }

  bdClick () {
    if (this.enabled && this.d.enableBackdropDismiss) {
      this.dismiss('backdrop')
    }
  }
}
