import { Directive, ElementRef, Optional, NgZone, Renderer, DynamicComponentLoader, ViewContainerRef } from '@angular/core'

import { App } from '../app'
import { Config } from '../config'
import { NavController } from './nav-controller'
import { ViewController } from './view-controller'

/**
 * @private
 */
@Directive({
  selector: '[nav-portal]'
})
export class NavPortal extends NavController {
  constructor (
    @Optional() viewCtrl: ViewController,
    @Optional() parent: NavController,
    app: App,
    config: Config,
    elementRef: ElementRef,
    zone: NgZone,
    renderer: Renderer,
    loader: DynamicComponentLoader,
    viewPort: ViewContainerRef
  ) {
    super(parent, app, config, elementRef, zone, renderer, loader)
    this.isPortal = true
    this.setViewport(viewPort)
  }
}
