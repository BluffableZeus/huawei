export class ModalConfig {
  containerClass = ''

  context: any = {}

  title (text: string): ModalConfig {
    this.context.title = text
    return this
  }

  textContent (text: string): ModalConfig {
    this.context.textContent = text
    return this
  }

  ariaLabel (text: string): ModalConfig {
    this.context.ariaLabel = text
    return this
  }

  ok (text: string): ModalConfig {
    this.context.ok = text
    return this
  }

  cancel (text: string): ModalConfig {
    this.context.cancel = text
    return this
  }

  css (cls: string) {
    this.containerClass = this.containerClass ? this.containerClass + ' ' + cls : cls
    return this
  }

  setProperty (key: string, value: any) {
    this.context[key] = value
    return this
  }

  setContext (context: any) {
    this.context = context
    return this
  }
}
