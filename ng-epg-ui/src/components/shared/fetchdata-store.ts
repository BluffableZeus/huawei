import { FetchDataCacheOptions } from './fetchdata-cache'
import { utils } from './utils'
import { NgForList } from './ngfor-list'
import { FetchDataWrap, FetchDataRequest, FetchDataFunction, FetchDataResponse } from './fetchdata-wrap'
import { FetchDataCache } from './fetchdata-cache'
import { BehaviorSubject } from 'rxjs'
import * as _ from 'underscore'

export class FetchDataStore {

  public fetchDataWrap: FetchDataWrap

  private fetchDataCache: FetchDataCache

  private ngforList: NgForList

    /**
     * wrap static data as fetchdata callback
     */
  public static createFetchDataFromData (resp: FetchDataResponse): FetchDataFunction {
      return ({ offset, count }: FetchDataRequest): Promise<FetchDataResponse> => {
          return Promise.resolve({
              total: resp.total || resp.list.length,
              list: resp.list.slice(offset, offset + count)
            })
        }
    }

  constructor (private options: FetchDataStoreOptions) {
      if (options.supportLongPress) {
          this.fetchDataList = utils.createLongPressCallback(this.fetchDataList.bind(this))
        }
    }

    /**
     * clear cache by index
     */
  public removeCache (index: number): void {
      this.fetchDataWrap.removeCache(index)
    }

  public clearCache (): void {
      this.fetchDataWrap.clearCache()
    }

    /**
     * is query total value
     */
  public isInitializing () {
      return this.fetchDataWrap.initializing
    }

  public getLoopList () {
      if (this.ngforList) {
          return this.ngforList.getList()
        }
    }

  public getDataByIndex (index: number): any {
      if (this.options.isLoop) {
          index = this.positiveOffset(index, this.getTotal())
        }
      return this.fetchDataWrap.getDataByIndex(index)
    }

  public getTotal () {
      return this.fetchDataWrap.total
    }

    /**
     * set data query callback
     */
  public setFetchData (fetchDataFunction: FetchDataFunction) {
      const o = this.options
      this.fetchDataWrap = new FetchDataWrap(fetchDataFunction, {
          batchSize: o.batchSize
        })

      this.fetchDataCache = undefined
      if (this.options.cache && this.options.cache.id) {
          this.fetchDataCache = new FetchDataCache(this.fetchDataWrap, this.options.cache)
        }

      this.ngforList = new NgForList(o.ngforSize)
    }

  public queryData ({ offset, count }: FetchDataRequest): Promise<FetchDataResponse> {
      const total = this.getTotal()
      const o = this.options

        // if not loop, so count can't greater then total
        // offset can't less then 0
      if (total) {
          if (!o.isLoop) {
              offset = Math.max(0, offset)
              count = Math.min(count, total - offset)
            }
        } else {
          offset = Math.max(0, offset)
        }

      let reqList = []
      if (this.getTotal()) {
          reqList = this.getRequestListByLoop({ offset, count }, total, o.isLoop)
        } else {
          reqList = [{ offset, count }]
        }

        // has uncached data, so fill empty data first
      if (reqList.length && total) {
          if (total) {
              this.ngforList.fillEmptyData({ offset, count })
            }
        }

      return this.fetchDataList(reqList).then((resp: FetchDataResponse) => {
          this.ngforList.fillActualData(offset, resp.list)
          return resp
        })
    }

  private getRequestListByLoop ({ offset, count }: FetchDataRequest,
        total: number, isLoop = false): Array<FetchDataRequest> {
      let reqList: Array<FetchDataRequest> = []

      if (isLoop) {

          let begin = this.positiveOffset(offset, total)
          if ((begin + count - 1) > total - 1) {
              reqList.push({ offset: begin, count: total - begin })
              reqList.push({ offset: 0, count: count - (total - begin) })
            } else {
              reqList.push({ offset: begin, count: count })
            }

        } else {
          reqList.push({ offset, count })
        }

      return reqList
    }

  private positiveOffset (offset: number, total: number): number {
        // normalize offset
      return ((offset % total) + total) % total
    }

  private fetchDataList (reqList: Array<FetchDataRequest>): Promise<FetchDataResponse> {

      const promises = reqList.map((req: FetchDataRequest) => {
          return this.fetchDataWrap.queryData(req)
        })

      return Promise.all(promises).then((respList: Array<FetchDataResponse>) => {
          const dataList = respList.reduce((list: Array<any>, resp: FetchDataResponse) => {
              return list.concat(resp.list)
            }, [])

          if (this.fetchDataCache) {
              this.fetchDataCache.updateCache()
            }

          return {
              total: respList[0].total,
              list: dataList
            }
        })
    }
}

export interface FetchDataStoreOptions {
  ngforSize: number
  isLoop?: boolean
  batchSize?: number
  supportLongPress?: boolean
  cache?: FetchDataCacheOptions
}
