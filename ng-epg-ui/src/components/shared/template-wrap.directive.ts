import { BehaviorSubject } from 'rxjs'
import { Directive, TemplateRef, ViewContainerRef, Input } from '@angular/core'

@Directive({
  selector: '[tpl]'
})
export class TemplateWrapDirective {

  @Input()
    public item: Object

  @Input()
    public data: { item: any, index: number }

  @Input()
    public asyncItem: BehaviorSubject<any>

  @Input()
    public asyncIndex: BehaviorSubject<number>

  @Input()
    public index: number

  constructor (private _viewContainer: ViewContainerRef) {
    }

  @Input()
    public set tpl (templateRef: TemplateRef<any>) {
      this._viewContainer.createEmbeddedView(templateRef, {
          item: this.item,
          data: this.data,
          index: this.index,
          asyncItem: this.asyncItem,
          asyncIndex: this.asyncIndex
        })
    }
}
