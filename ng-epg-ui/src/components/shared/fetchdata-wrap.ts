import * as _ from 'underscore'

export class FetchDataWrap {

  public total: number

  private cachedList: Array<any> = []

  public initializing = false

  private preLoadTimeoutId: any

  private requesting: { [key: string]: Promise<FetchDataResponse> } = {}

  private isActive = false;

  constructor (private fetchDataFunction: FetchDataFunction,
        private options: FetchDataWrapOptions = {}) {
    }

  public getDataByIndex (index: number): any {
      return this.cachedList[index]
    }

  public clearCache (): void {
      this.cachedList = []
    }

  public setActive (isActive: boolean) {
      this.isActive = isActive
    }

    /**
     * remove by index
     */
  public removeCache (index: number, count: number = 1): void {
      _.times(count, (i: number) => {
          delete this.cachedList[index + i]
        })
    }

  public queryData ({ offset, count }: FetchDataRequest): Promise<FetchDataResponse> {

      this.clearPreLoadTimeoutId()

      return this._queryData({ offset, count }).then((resp) => {
          if (this.isActive) {
              this.preLoadTimeoutId = window['__zone_symbol__setTimeout'](() => {
                  this._queryData({ offset: offset + count, count })
                  this.clearPreLoadTimeoutId()
                }, 50)
            }
          return resp
        })
    }

  private clearPreLoadTimeoutId () {
      if (this.preLoadTimeoutId) {
          clearTimeout(this.preLoadTimeoutId)
          this.preLoadTimeoutId = null
        }
    }

    /**
     * query data by offset & count
     */
  private _queryData ({ offset, count }: FetchDataRequest): Promise<FetchDataResponse> {

      let reqList = []
      if (this.total) {
          if (offset >= this.total) {
              return Promise.resolve({
                  total: this.total,
                  list: []
                })
            }

          reqList = this.buildUnCachedRequestList({ offset, count })
        } else {
          this.initializing = true
          reqList = [{ offset, count: count }]
        }

      reqList = _.chain(reqList)
            .map(r => this.convertToBatchRequest(r))
            .flatten()
            .uniq(r => r.offset)
            .value()

      const promise = Promise.all(reqList.map((req: FetchDataRequest) => {
          return this.fetchData(req)
        }))

      return promise.then(() => {
          this.initializing = false
          let list = _.times(count, (i) => {
              return this.getDataByIndex(offset + i)
            })

          list = _.compact(list)

          return {
              total: this.total,
              list: list
            }
        }, () => {
          this.initializing = false
        })
    }

  private convertToBatchRequest ({ offset, count }: FetchDataRequest): Array<FetchDataRequest> {
      const { batchSize } = this.options
      if (batchSize > 0) {
          const min = Math.floor(offset / batchSize)
          const max = Math.floor((offset + count - 1) / batchSize)
          return _.times(max - min + 1, (i: number) => {
              return { offset: (min + i) * batchSize, count: batchSize }
            })
        }
      return [{ offset, count }]
    }

    /**
     * build request for uncached data
     */
  private buildUnCachedRequestList ({ offset, count }: FetchDataRequest): Array<FetchDataRequest> {

      let reqList = []
      let req: FetchDataRequest

      _.times(count, (i: number) => {
          const index = offset + i
          if (this.getDataByIndex(index)) {
              req = null
            } else {
              if (!req) {
                  req = { offset: index, count: 0 }
                  reqList.push(req)
                }
              req.count++
            }
        })

      return _.chain(reqList)
            .uniq(r => r.offset)
            .value()
    }

  private fetchData ({ offset, count }: FetchDataRequest): Promise<FetchDataResponse> {

      const key = `${offset}_${count}`
      if (this.requesting[key]) {
          return this.requesting[key]
        }

      if (offset >= this.total) {
          return Promise.resolve({ list: [], total: this.total })
        }

      this.requesting[key] = this.fetchDataFunction({ offset, count }).then((resp: FetchDataResponse) => {
          delete this.requesting[key]
          this.onFetchData(offset, resp)
          return resp
        }, () => {
          delete this.requesting[key]
        })

      return this.requesting[key]
    }

  public onFetchData (offset, resp: FetchDataResponse) {
      if (resp) {
          this.total = resp.total || resp.list.length
          resp.list.forEach((data, i: number) => {
              this.cachedList[offset + i] = data
            })
        }
    }
}

export interface FetchDataWrapOptions {
  batchSize?: number
}

export interface FetchDataRequest {
  offset: number
  count: number
}

export interface FetchDataResponse {
  total: number
  list: Array<any>
}

export type FetchDataFunction = (req: FetchDataRequest) => Promise<FetchDataResponse>
