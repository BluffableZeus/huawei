import { Directive, NgZone, Output, Input, HostListener, EventEmitter, OnInit, OnDestroy } from '@angular/core'
import * as _ from 'underscore'

@Directive({
  selector: '[keypress-status]',
  exportAs: 'keypressStatus'
})
export class KeypressStatusDirective implements OnDestroy, OnInit {

  @Input()
    public enterLongPressDelay: number = 800

    /**
     * emit event, after pressed enterLongPressDelay milliseconds
     */
  @Output()
    public longPressStart = new EventEmitter()

    /**
     * emit event when keyup
     */
  @Output()
    public longPressStop = new EventEmitter()

  @Output()
    public keypressedChange = new EventEmitter<boolean>()

    /**
     * key pressed status
     */
  public isKeypressed = false

  private timeoutId

  constructor (private ngZone: NgZone) {
    }

  public ngOnInit () {
      this.longPressStart.subscribe(() => {
          this.keypressedChange.emit(this.isKeypressed)
        })

      this.longPressStop.subscribe(() => {
          this.keypressedChange.emit(this.isKeypressed)
        })
    }

  public ngOnDestroy () {
      clearTimeout(this.timeoutId)
    }

  @HostListener('document:keyup', ['$event'])
    public onKeyup (event: KeyboardEvent) {
      if (768 !== event.keyCode) {
          clearTimeout(this.timeoutId)

          if (this.isKeypressed === true) {
              this.isKeypressed = false
              this.longPressStop.emit()
            }
        }
    }

  @HostListener('document:keydown', ['$event'])
    public onKeydown (event: KeyboardEvent) {
      if (768 !== event.keyCode) {
          clearTimeout(this.timeoutId)

          this.ngZone.runOutsideAngular(() => {
              this.timeoutId = setTimeout(() => {
                  this.enterLongKeyPress()
                }, this.enterLongPressDelay)
            })
        }
    }

  private enterLongKeyPress () {
      if (this.isKeypressed === false) {
          this.isKeypressed = true
          this.longPressStart.emit()
        }
    }
}
