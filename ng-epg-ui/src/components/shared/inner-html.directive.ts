import { Directive, ElementRef, Input } from '@angular/core'

@Directive({
  selector: '[inner-html]'
})
export class InnerHtmlDirective {

  @Input('inner-html')
    set innerHTML (htmlStr: string) {
      this.getElement().innerHTML = htmlStr
    }

  constructor (private _elementRef: ElementRef) {
    }

  getElement (): HTMLElement {
      return this._elementRef.nativeElement
    }

}
