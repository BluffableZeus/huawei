import { Directive, Output, Input, HostListener, EventEmitter, OnInit, OnDestroy } from '@angular/core'
import * as _ from 'underscore'

export interface LongPressEvent {
  type: string
  step?: number
}

@Directive({
  selector: '[longPress]'
})
export class LongPressDirective implements OnInit, OnDestroy {

  @Input()
    public set eventType (eventTypes: Array<string> | string) {

      if ('string' === typeof eventTypes) {
          this.eventTypes = [eventTypes]
        } else {
          this.eventTypes = eventTypes
        }
    }

  @Input()
    public intervalDruingLongPress: number

  @Input()
    public enterLongPressDelay: number

    /**
     * emit keydown event per keypress
     */
  @Output()
    public shortPress = new EventEmitter<CustomEvent>()

    /**
     * emit event every intervalDruingPressed millisecond during press
     */
  @Output()
    public longPress = new EventEmitter<LongPressEvent>()

    /**
     * emit event, after pressed enterLongPressDelay milliseconds
     */
  @Output()
    public longPressStart = new EventEmitter<LongPressEvent>()

    /**
     * emit event when keyup
     */
  @Output()
    public longPressStop = new EventEmitter<LongPressEvent>()

  private isPressed = false

  private onKeyupEmitter = new EventEmitter()

  private eventTypes: Array<string>

  private onKeyup = () => {
      this.onKeyupEmitter.emit()
    }

  constructor () {
    }

  ngOnInit () {
      this.addEventListener('keyup', this.onKeyup)

      this.onKeyupEmitter.subscribe(() => {
          this.isPressed = false
        })
    }

  ngOnDestroy () {
      this.onKeyupEmitter.emit()
      this.removeEventListener('keyup', this.onKeyup)
    }

  @HostListener('xkeydown', ['$event'])
    public onKeydown (event: CustomEvent) {
      if (_.contains(this.eventTypes, event.detail.type)) {
          if (!this.isPressed) {
              this.isPressed = true
              this.shortPress.emit(event)
              this.delayStartLongPress(event.detail.type)
            } else {
              event.stopPropagation()
            }
        }
    }

  private delayStartLongPress (type: string) {

      if (this.enterLongPressDelay > 0) {
          let timeoutId = setTimeout(() => {
              timeoutId = null
              this.startLongPress(type)
            }, this.enterLongPressDelay)

          this.onKeyupEmitter.take(1).subscribe(() => {
              if (timeoutId) {
                  clearTimeout(timeoutId)
                }
            })
        }
    }

  private startLongPress (type: string) {
      if (!this.isPressed) {
          return
        }

      this.longPressStart.emit({ type })
      let step = 0
      let intervalId

      if (this.intervalDruingLongPress > 0) {
          intervalId = this.interval(() => {
              console.log(`step:${step}`)
              this.longPress.emit({ type, step })
              step++
            }, this.intervalDruingLongPress)

        }

      this.onKeyupEmitter.take(1).subscribe(() => {
          this.cancelInterval(intervalId)
          this.stopLongPress({ type, step })
        })
    }

  private stopLongPress (event: LongPressEvent) {
      this.longPressStop.emit(event)
    }

  private addEventListener (type: string, callback: Function) {
      return window['__zone_symbol__addEventListener'](type, callback)
    }

  private removeEventListener (type: string, callback: Function) {
      return window['__zone_symbol__removeEventListener'](type, callback)
    }

  private interval (callback: Function, timeout?: number): number {
      return window['__zone_symbol__setInterval'](callback, timeout)
    }

    /**
     * @private
     * This setTimeout will NOT be wrapped by zone.
     */
  private cancelInterval (timeoutId: number) {
      window['__zone_symbol__clearInterval'](timeoutId)
    }
}
