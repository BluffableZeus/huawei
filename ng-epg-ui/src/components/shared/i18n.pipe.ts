import { Pipe, PipeTransform, ChangeDetectorRef } from '@angular/core'
import { TranslatePipe, TranslateService } from '@ngx-translate/core'
import * as _ from 'underscore'

@Pipe({
  name: 'i18n',
  pure: false
})
export class I18nPipe extends TranslatePipe implements PipeTransform {

  constructor (translate: TranslateService, _ref: ChangeDetectorRef) {
      super(translate, _ref)
    }

  transform (_query: any, ...args: any[]): any {
      if (_.isArray(_query) && !args.length) {
          const [query, ..._args] = _query
          return super.transform(query, ..._args)
        }

      return super.transform(_query, ...args)
    }
}
