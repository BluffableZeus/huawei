import { BehaviorSubject } from 'rxjs'
import * as _ from 'underscore'

export class NgForList {

  private list: Array<FetchDataStoreRecord>

  private indexOfList: { [key: string]: number } = {}

  private head = -1

  constructor (size: number) {
      this.list = _.times(size, (i: number) => {
          this.indexOfList[i] = -1
          const data = { item: {}, index: undefined }
          return {
              data: data,
              isNull: true,
              asyncData: new BehaviorSubject<any>(data),
              asyncIndex: new BehaviorSubject<any>(undefined)
            }
        })
    }

  public fillEmptyData ({ offset, count }: { offset: number, count: number }): void {

      _.times(count, (i: number) => {
          const index = offset + i
          if (this.indexOfList[index] >= 0) {
              return
            }

          const record = this.moveHead({ offset, count, index })
          for (let name of record.data.item) {
              record.data.item[name] = undefined
            }
          record.asyncData.next({})
        })
    }

  public fillActualData (offset: number, list: Array<any>) {
      const count = list.length
      _.times(count, (i: number) => {
          const index = offset + i
          let record = this.list[this.indexOfList[index]]

          if (!record) {
              record = this.moveHead({ offset, count, index })
            }

          record.isNull = false
          _.extend(record.data.item, list[i])
          record.asyncData.next(list[i])
        })
    }

  public getList () {
      return this.list
    }

  private moveHead ({ offset, count, index }: { offset: number, count: number, index: number }): FetchDataStoreRecord {
      const length = this.list.length

      while (true) {
            // loop increase head index
          this.head++
          this.head = this.head % length

            // is in current page
          const record = this.list[this.head]

          const inCurrentPage = offset <= record.data.index && record.data.index < (offset + count)
          if (inCurrentPage) {
              continue
            }
          break
        }

      const record = this.list[this.head]

      delete this.indexOfList[record.data.index]
      this.indexOfList[index] = this.head
      record.data.index = index
      record.asyncIndex.next(index)
      return record
    }
}

export interface FetchDataStoreRecord {
  isNull: boolean
  data: { item: any, index: number }
  asyncData: BehaviorSubject<any>
  asyncIndex: BehaviorSubject<number>
}
