import { KeypressStatusDirective } from './keypress-status.directive'
import { LongPressDirective } from './longkeypress.directive'
import { TemplateWrapDirective } from './template-wrap.directive'
import { NgModule } from '@angular/core'
import { TranslateModule } from '@ngx-translate/core'

import { I18nPipe } from './i18n.pipe'
import { DateFormatPipe } from './date-format'
import { InnerHtmlDirective } from './inner-html.directive'

@NgModule({
  imports: [TranslateModule],
  exports: [I18nPipe, InnerHtmlDirective, DateFormatPipe, TranslateModule, TemplateWrapDirective, LongPressDirective, KeypressStatusDirective],
  declarations: [I18nPipe, InnerHtmlDirective, DateFormatPipe, TemplateWrapDirective, LongPressDirective, KeypressStatusDirective],
  providers: []
})
export class UISharedModule { }
