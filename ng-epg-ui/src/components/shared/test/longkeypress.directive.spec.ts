import { LongPressDirective } from '../longkeypress.directive'

describe('LongKeypressDirective', () => {

  it('isPressed', () => {
    let longKeypressDirective = new LongPressDirective()
    longKeypressDirective.eventType = 'up'
    longKeypressDirective.onKeydown({ detail: { type: 'up' } } as any)

    expect(longKeypressDirective['isPressed']).toBeTruthy()
  })

  it('delayStartLongPress 1', (done) => {
    let longPress = new LongPressDirective()
    longPress.enterLongPressDelay = 10

    spyOn(longPress, 'startLongPress')
    longPress['delayStartLongPress']('up')

    setTimeout(() => {
      expect(longPress['startLongPress']).toHaveBeenCalled()
      done()
    }, 11)
  })

  it('delayStartLongPress 2', (done) => {
    let longPress = new LongPressDirective()
    longPress.enterLongPressDelay = 10

    spyOn(longPress, 'startLongPress')
    spyOn(longPress, 'removeEventListener')
    longPress['delayStartLongPress']('up')
    longPress['ngOnDestroy']()

    setTimeout(() => {
      expect(longPress['startLongPress']).not.toHaveBeenCalled()
      done()
    }, 11)
  })
})
