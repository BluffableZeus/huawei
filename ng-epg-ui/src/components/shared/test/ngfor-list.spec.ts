import { NgForList } from './../ngfor-list'
import * as _ from 'underscore'

describe('NgForList', () => {

  it('preFetchData', () => {
      const loopList = new NgForList(10)

      expect(loopList['indexOfList']['3']).toEqual(-1)

      loopList.fillEmptyData({ offset: 0, count: 5 })

      const record = loopList['list'][3]
      expect(record.data.index).toEqual(3)
      expect(record.isNull).toBeTruthy()
    })

  it('afterFetchData', () => {
      const loopList = new NgForList(10)

      expect(loopList['indexOfList']['3']).toEqual(-1)

      loopList.fillActualData(0, _.times(5, r => {
          return { id: r }
        }))

      const record = loopList['list'][3]
      expect(record.data.index).toEqual(3)
      expect(record.isNull).not.toBeTruthy()
      expect(record.data.item.id).toEqual(3)
    })

  it('afterFetchData, exceed', () => {
      const loopList = new NgForList(10)

      loopList.fillActualData(0, _.times(10, r => {
          return { id: r }
        }))

      loopList.fillActualData(10, _.times(5, r => {
          return { id: r }
        }))

      let record = loopList.getList()[9]
      expect(record.data.index).toEqual(9)

      record = loopList.getList()[0]
      expect(record.data.index).toEqual(10)

      record = loopList.getList()[4]
      expect(record.data.index).toEqual(14)
    })

  it('moveHead 1', () => {
      const ngforList = new NgForList(10)

      const record = ngforList['moveHead']({
          offset: 0,
          count: 5,
          index: 0
        })

      expect(record.data.index).toEqual(0)
    })

  it('moveHead 2', () => {
      const ngforList = new NgForList(10)

      ngforList['list'][0].data.index = 0

      const item = ngforList['moveHead']({
          offset: 0,
          count: 5,
          index: 1
        })

      expect(ngforList['list'][ngforList['indexOfList'][1]].data.index).toEqual(1)
    })

  it('moveHead 3', () => {
      const ngforList = new NgForList(10)

      ngforList['head'] = -1
      _.times(10, (i) => {
          ngforList['moveHead']({
              offset: 0,
              count: 10,
              index: i
            })
        })

      ngforList['head'] = -1
      _.times(10, (i) => {
          ngforList['moveHead']({
              offset: 10,
              count: 10,
              index: 10 + i
            })
        })

      expect(ngforList['indexOfList'][8]).not.toBeDefined()
    })
})
