import { FetchDataCache } from './../fetchdata-cache'
import * as _ from 'underscore'

describe('FetchDataCache', () => {
  let fetchDataCache: FetchDataCache
  beforeEach(() => {
      fetchDataCache = new FetchDataCache({ getDataByIndex: () => { } } as any,
            { id: _.identity(''), duration: 100, count: 10 })
    })

  it('buildCachedList', () => {
      spyOn(fetchDataCache['fetchDataWrap'], 'getDataByIndex').and.returnValue(1)

      const list = fetchDataCache['buildCachedList']()
      const cacheItem = fetchDataCache['getCacheItem']()
      expect(list).toEqual(_.times(10, () => 1))

    })

  it('buildCachedList 1', () => {
      spyOn(fetchDataCache['fetchDataWrap'], 'getDataByIndex').and.callFake((i) => {
          if (i < 5) {
              return 1
            }
        })

      const list = fetchDataCache['buildCachedList']()
      expect(list).toBeDefined()
      expect(list).toEqual(_.times(5, () => 1))

    })

  it('buildCachedList 2', () => {
      spyOn(fetchDataCache['fetchDataWrap'], 'getDataByIndex').and.returnValue(1)

      const list = fetchDataCache['buildCachedList']()
      expect(list).toBeDefined()
      expect(list).toEqual(_.times(10, () => 1))
    })

  it('checkExpire, expired', (done) => {
      spyOn(fetchDataCache, 'refreshCache')
      fetchDataCache['checkExpire']({ timestamp: Date.now() - 110, resp: undefined })
      setTimeout(() => {
          expect(fetchDataCache['refreshCache']).toHaveBeenCalled()
          done()
        })
    })

  it('checkExpire, not expired', (done) => {
      spyOn(fetchDataCache, 'refreshCache')
      fetchDataCache['checkExpire']({ timestamp: Date.now() - 90, resp: undefined })

      setTimeout(() => {
          expect(fetchDataCache['refreshCache']).not.toHaveBeenCalled()
          done()
        })
    })
})
