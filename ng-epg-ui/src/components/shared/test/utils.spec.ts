import { utils } from './../utils'

describe('utils.createLongPressCallback', () => {

  let count: number = 0
  let flag: string
  let cb: Function

  beforeEach(() => {
      count = 0
      flag = ''

      cb = utils.createLongPressCallback((arg) => {
          count++
          flag = arg + count
          return Promise.resolve(count)
        }, 200)
    })

  it('createLongPressCallback 1', () => {
      expect(count).toEqual(0)
      cb()
      expect(count).toEqual(1)
    })

  it('createLongPressCallback 2', (done) => {
      expect(count).toEqual(0)
      cb()

      expect(count).toEqual(1)

      setTimeout(function () {
          cb()
          expect(count).toEqual(1)
        }, 100)

      setTimeout(function () {
          expect(count).toEqual(1)
          utils.keyupObservable.next()
          expect(count).toEqual(2)
          done()
        }, 210)
    })

  it('createLongPressCallback 3', (done) => {
      expect(count).toEqual(0)

      cb('test')
      expect(count).toEqual(1)
      expect(flag).toEqual('test1')

      cb()
      setTimeout(function () {
          utils.keyupObservable.next()
          expect(count).toEqual(1)

          done()
        }, 100)
    })

  it('createLongPressCallback 4', (done) => {
      cb = utils.createLongPressCallback((arg) => {
          count++
          return Promise.resolve(count)
        }, 200, false)

      cb()
      expect(count).toEqual(0)

      setTimeout(function () {
          utils.keyupObservable.next()
          expect(count).toEqual(1)
          done()
        }, 210)
    })

})
