import { FetchDataWrap } from './../fetchdata-wrap'

describe('FetchDataWrap', () => {
  it('convertToBatchRequest, normal', () => {
      const fetchDataWrap = new FetchDataWrap(null, { batchSize: 4 })
      const reqList = fetchDataWrap['convertToBatchRequest']({ offset: 0, count: 4 })

      expect(reqList).toEqual([{ offset: 0, count: 4 }])
    })

  it('convertToBatchRequest, normal three', () => {
      const fetchDataWrap = new FetchDataWrap(null, { batchSize: 4 })
      const reqList = fetchDataWrap['convertToBatchRequest']({ offset: 0, count: 12 })

      expect(reqList).toEqual([{ offset: 0, count: 4 }, { offset: 4, count: 4 },
        { offset: 8, count: 4 }])
    })

  it('convertToBatchRequest, range two', () => {
      const fetchDataWrap = new FetchDataWrap(null, { batchSize: 4 })
      const reqList = fetchDataWrap['convertToBatchRequest']({ offset: 2, count: 4 })

      expect(reqList).toEqual([{ offset: 0, count: 4 }, { offset: 4, count: 4 }])
    })

  it('convertToBatchRequest, range four', () => {
      const fetchDataWrap = new FetchDataWrap(null, { batchSize: 4 })
      const reqList = fetchDataWrap['convertToBatchRequest']({ offset: 2, count: 16 })

      expect(reqList).toEqual([{ offset: 0, count: 4 }, { offset: 4, count: 4 },
        { offset: 8, count: 4 }, { offset: 12, count: 4 }, { offset: 16, count: 4 }])
    })

  it('fetchData cache', (done) => {
      const fetchDataWrap = new FetchDataWrap((() => { }) as any, { batchSize: 4 })
      fetchDataWrap.setActive(true)
      const promise = new Promise((resolve, reject) => {
          setTimeout(function () {
              reject()
            }, 100)
        })

      spyOn(fetchDataWrap, 'fetchDataFunction').and.returnValue(promise)

        // first invoke
      fetchDataWrap['fetchData']({ offset: 0, count: 5 })
        // second invoke, use cache
      fetchDataWrap['fetchData']({ offset: 0, count: 5 })

      expect(fetchDataWrap['fetchDataFunction']).toHaveBeenCalledTimes(1)

      setTimeout(function () {
            // third invoke, use cache
          fetchDataWrap['fetchData']({ offset: 0, count: 5 })
          expect(fetchDataWrap['fetchDataFunction']).toHaveBeenCalledTimes(2)
          done()
        }, 110)
    })
})
