import { FetchDataStore } from './../fetchdata-store'
import * as _ from 'underscore'

describe('FetchDataStore', () => {

  it('getRequestListByLoop, middle', () => {
      const fetchDataStore = new FetchDataStore({ ngforSize: 24 })

      const reqList = fetchDataStore['getRequestListByLoop']({ offset: 0, count: 5 }, 10)

      expect(reqList).toEqual([{ offset: 0, count: 5 }])
    })

  it('getRequestListByLoop, pre', () => {
      const fetchDataStore = new FetchDataStore({ ngforSize: 24 })

      let reqList = fetchDataStore['getRequestListByLoop']({ offset: -4, count: 2 }, 10, true)
      expect(reqList).toEqual([{ offset: 6, count: 2 }])

      reqList = fetchDataStore['getRequestListByLoop']({ offset: -4, count: 4 }, 10, true)
      expect(reqList).toEqual([{ offset: 6, count: 4 }])
    })

  it('getRequestListByLoop, after', () => {
      const fetchDataStore = new FetchDataStore({ ngforSize: 24 })

      let reqList = fetchDataStore['getRequestListByLoop']({ offset: 12, count: 2 }, 10, true)
      expect(reqList).toEqual([{ offset: 2, count: 2 }])

      reqList = fetchDataStore['getRequestListByLoop']({ offset: 22, count: 2 }, 10, true)
      expect(reqList).toEqual([{ offset: 2, count: 2 }])

      reqList = fetchDataStore['getRequestListByLoop']({ offset: 9, count: 2 }, 10, true)
      expect(reqList).toEqual([{ offset: 9, count: 1 }, { offset: 0, count: 1 }])
    })

  it('positiveOffset nomal', () => {
      const fetchDataStore = new FetchDataStore({ ngforSize: 24 })

      expect(fetchDataStore['positiveOffset'](1, 5)).toEqual(1)
      expect(fetchDataStore['positiveOffset'](0, 5)).toEqual(0)
      expect(fetchDataStore['positiveOffset'](4, 5)).toEqual(4)
      expect(fetchDataStore['positiveOffset'](3, 5)).toEqual(3)
    })

  it('positiveOffset positive', () => {
      const fetchDataStore = new FetchDataStore({ ngforSize: 24 })

      expect(fetchDataStore['positiveOffset'](5, 5)).toEqual(0)
      expect(fetchDataStore['positiveOffset'](9, 5)).toEqual(4)
      expect(fetchDataStore['positiveOffset'](11, 5)).toEqual(1)
      expect(fetchDataStore['positiveOffset'](20, 5)).toEqual(0)
    })

  it('positiveOffset negative', () => {
      const fetchDataStore = new FetchDataStore({ ngforSize: 24 })

      expect(fetchDataStore['positiveOffset'](-1, 5)).toEqual(4)
      expect(fetchDataStore['positiveOffset'](-4, 5)).toEqual(1)
      expect(fetchDataStore['positiveOffset'](-10, 5)).toEqual(0)
      expect(fetchDataStore['positiveOffset'](-11, 5)).toEqual(4)
    })

  it('getRequestListByLoop, loop false', () => {
      const fetchDataStore = new FetchDataStore({ ngforSize: 24 })

      const reqList = fetchDataStore['getRequestListByLoop']({ offset: 0, count: 4 }, 5)

      expect(reqList).toEqual([{ offset: 0, count: 4 }])
    })

  it('getRequestListByLoop, loop true', () => {
      const fetchDataStore = new FetchDataStore({ ngforSize: 24 })

      const reqList = fetchDataStore['getRequestListByLoop']({ offset: -2, count: 4 }, 5, true)

      expect(reqList).toEqual([{ offset: 3, count: 2 }, { offset: 0, count: 2 }])
    })
})
