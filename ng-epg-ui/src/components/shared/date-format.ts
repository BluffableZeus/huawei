import { Pipe, PipeTransform } from '@angular/core'
import { DateUtils } from '../../util'

@Pipe({
  name: 'fmt',
  pure: false
})
export class DateFormatPipe implements PipeTransform {

    /**
     * @param {(Date | number | Array<Date | number>)} value
     * @param {string} format
     * @param {string} [joinChar]
     * @returns {string}
     * @memberOf DateFormatPipe
     */
  transform (value: Date | number | [Date | number, Date | number], format: string, joinChar?: string): string {
      if (value instanceof Array) {
          return value.map((valueItem) => {
              return DateUtils.format(valueItem, format)
            }).join(joinChar)
        } else {
          return DateUtils.format(value, format)
        }
    }
}
