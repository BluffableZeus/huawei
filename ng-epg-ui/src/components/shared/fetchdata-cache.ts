import { FetchDataWrap, FetchDataRequest, FetchDataResponse } from './fetchdata-wrap'
import * as _ from 'underscore'

interface CacheItem {
  timestamp: number
  resp: FetchDataResponse
}

export class FetchDataCache {

  private static cache: { [id: string]: CacheItem } = {}

  constructor (private fetchDataWrap: FetchDataWrap,
        private options: FetchDataCacheOptions) {

      _.defaults(options, {
          duration: 15 * 60 * 1000,
          count: 12
        })

      this.onInit()
    }

  public updateCache (): void {
      const count = this.getCacheCount()
      const cacheItem = this.getCacheItem()
      if (!cacheItem || cacheItem.resp.list.length < count) {
          const list = this.buildCachedList()
          if (list) {
              this.saveRespToCache({ total: this.fetchDataWrap.total, list: list })
            }
        }
    }

  private getCacheItem (): CacheItem {
      return FetchDataCache.cache[this.options.id]
    }

  private buildCachedList (): Array<any> {
      const o = this.options
      const count = this.getCacheCount()

      const list = []

        // get head data list
      _.chain(count).range().find((i) => {
          const data = this.fetchDataWrap.getDataByIndex(i)
          if (data) {
              list.push(data)
            }
          return !data
        }).value()

      const compactedList = _.compact(list)
      if (list.length === compactedList.length) {
          return list
        }
    }

  private getCacheCount () {
      if (undefined !== this.fetchDataWrap.total) {
          return Math.min(this.fetchDataWrap.total, this.options.count)
        }
      return this.options.count
    }

  private onInit (): void {
      const cacheItem = this.getCacheItem()
      if (cacheItem) {
          this.fillCache(cacheItem)
          this.checkExpire(cacheItem)
        }
    }

  private fillCache (cacheItem: CacheItem): void {
      this.fetchDataWrap.onFetchData(0, cacheItem.resp)
    }

  private checkExpire (cacheItem: CacheItem): void {
      const o = this.options
      const isExpired = (Date.now() - cacheItem.timestamp) > o.duration

      if (isExpired) {
          setTimeout(() => {
              this.refreshCache()
            })
        }
    }

  private refreshCache (): void {
      const o = this.options
      this.fetchDataWrap.removeCache(0, o.count)
      this.fetchDataWrap.queryData({
          offset: 0,
          count: o.count
        }).then((resp) => {
          this.saveRespToCache(resp)
        })
    }

  private saveRespToCache (resp: FetchDataResponse): void {
      const cacheItem = this.getCacheItem() || {
          timestamp: Date.now(),
          resp: resp
        }

      cacheItem.resp = resp

      FetchDataCache.cache[this.options.id] = cacheItem
    }
}

export interface FetchDataCacheOptions {
  id: string
  duration?: number
  count: number
}
