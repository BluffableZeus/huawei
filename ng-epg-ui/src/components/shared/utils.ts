import { Observable, Subject, Subscription } from 'rxjs'
import 'rxjs/add/operator/delay'

import * as _ from 'underscore'

const addEventListener = window['__zone_symbol__addEventListener']

class Utils {

  public keyupObservable: Subject<KeyboardEvent>

  constructor () {
      this.keyupObservable = new Subject<KeyboardEvent>()
      addEventListener('keyup', (event: KeyboardEvent) => {
          this.keyupObservable.next(event)
        })
    }

  public createLongPressCallback (cb: Function, delay = 0, immediate = true): any {

      let lastTimestamp = Date.now()
      let subscription: Subscription
      let firstCall = true

      const func = (args: Array<any>, resolve, reject) => {
          lastTimestamp = Date.now()

          Promise.all([cb(...args)]).then(([resp]) => {
              resolve(resp)
            }, ([resp]) => {
              reject(resp)
            })
        }

      return (...args) => {
          return new Promise((resolve, reject) => {
              if (subscription) {
                  subscription.unsubscribe()
                }

              if (firstCall && immediate) {
                  func(args, resolve, reject)
                } else {
                  subscription = this.keyupObservable.take(1).subscribe(() => {
                      if ((Date.now() - lastTimestamp) > delay) {
                          func(args, resolve, reject)
                        }
                      firstCall = true
                    })
                }
              firstCall = false
            })
        }
    }
}

export const utils = new Utils()
