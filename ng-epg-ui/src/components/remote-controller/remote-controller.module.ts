import { NgModule, ModuleWithProviders } from '@angular/core'
import { KeyMap } from './keymap/keymap'
import { KeyboardKeyMap } from './keymap/keyboard-keymap'
import { RemoteControlKeyMap } from './keymap/remote-controller-keymap'
import { FocusScrollDirective } from './focus-scroll.directive'

const win: any = window
const isSTB = win.Utility && win.Utility.getValueByName.toString().indexOf('native code') >= 0

export function keyMapProvide () {
  return isSTB ? new RemoteControlKeyMap() : new KeyboardKeyMap()
}

@NgModule({
  exports: [FocusScrollDirective],
  declarations: [FocusScrollDirective],
  providers: [{ provide: KeyMap, useFactory: keyMapProvide }]
})

export class RemoteControllerModule {
  static forRoot (): ModuleWithProviders {
      return {
          ngModule: RemoteControllerModule,
          providers: []
        }
    }
}
