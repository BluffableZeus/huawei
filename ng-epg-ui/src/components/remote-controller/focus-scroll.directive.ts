import { HostListener, Directive, Input, ElementRef } from '@angular/core'

import { InputConverter, NumberConverter } from '../shared/input-converter.decorator'

@Directive({
  selector: '[focus-scroll],launchergroup'
})
export class FocusScrollDirective {

  @InputConverter(NumberConverter)
    @Input()
    speed = 150

  animating = false

  private clientRect: ClientRect

  el: HTMLElement

  constructor (elRef: ElementRef) {
      this.el = elRef.nativeElement as HTMLElement
    }

  @HostListener('move', ['$event'])
    onMove (event: CustomEvent) {
      if (this.animating) {
          event.stopPropagation()
        }
    }

  getClientRect () {
      if (!this.clientRect) {
          this.clientRect = this.el.getClientRects()[0]
        }
      return this.clientRect
    }

  @HostListener('xfocus', ['$event'])
    onXFocus (event: CustomEvent) {
      const focusEl: HTMLElement = event.target as HTMLElement
      const focusElRect = focusEl.getClientRects()[0]
      const containerRect = this.getClientRect()

      let xOffset: number

      if (focusElRect.right > containerRect.right) {
          xOffset = focusElRect.right - containerRect.right
        } else if (focusElRect.left < containerRect.left) {
          xOffset = focusElRect.left - containerRect.left
        }

      if (xOffset) {
          const wrapEl = this.el.firstElementChild as HTMLElement
          const left = parseInt(wrapEl.style.left, 10) || 0
          wrapEl.style.left = (left - xOffset) + 'px'
          wrapEl.style.transform = 'translate3d(-' + (left - xOffset) + 'px, 0px, 0px)'
        }
    }
}
