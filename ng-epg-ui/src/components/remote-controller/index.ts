
export * from './keymap/keymap'
export * from './keymap/keyboard-keymap'
export * from './keymap/remote-controller-keymap'
export * from './focus-scroll.directive'
export * from './remote-controller.module'

import { FocusScrollDirective } from './focus-scroll.directive'

export const REMOTECONTROLLER_DIRECTIVES = [FocusScrollDirective]
