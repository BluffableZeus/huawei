### Usage
```typescript
import { CAROUSEL_DIRECTIVES } from 'ng-epg-sdk';
```

### remote control属性
    nav-group
        当前元素是否为焦点组
  
    nav/nav-left/nav-right/nav-up/nav-down:
        当为false时，表明元素不支持移动或某方向不支持移动
        非false时，应该为css选择器，直接指定下一组或下一个焦点

    import {AutoFocus} from '../../../components/remote-controller';
    auto-focus="true"：
        默认初始焦点

    nav-history:
        false/true
        此焦点组是否支持记录历史焦点，默认为true

    nav-dialog:
        modal窗口
        焦点只在此区域移动

    nav-init
        group初始焦点选择器



### remote control事件

####1.取消系统的焦点控制，采用组件自定义控制焦点
    如果需要自定义处理xkeydown事件，则需要调用event.stopPropagation()方法，取消自动处理

####4.焦点事件
    xfocus
    xblur
    xleft
    xright
    xup
    xdown
    group-xfocus
    group-xblur
    move-cancel
    move-left-cancel
    move-right-cancel
    move-up-cancel
    move-right-cancel


