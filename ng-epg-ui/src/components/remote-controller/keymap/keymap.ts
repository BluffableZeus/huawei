import * as _ from 'underscore'

export abstract class KeyMap {
  POWER_KEY: number
  MUTE_KEY: number
  MENU_KEY: number
  VOD_KEY: number
  OPTION_KEY: number
  MYTV_KEY: number
  INFO_KEY: number
  BTV_KEY: number
  TVOD_KEY: Array<number> | number
  UP_KEY: number
  LEFT_KEY: number
  RIGHT_KEY: number
  DOWN_KEY: number
  OK_KEY: number
  SET_KEY: number
  PVR_KEY: number
  LONG_PVR_KEY: number
  BACK_KEY: number

  SEEKDOWN_KEY: number
  SEEKUP_KEY: number
  PAGEDOWN_KEY: number
  PAGEUP_KEY: number
  FBWD_KEY: number
  FFWD_KEY: number
  PAUSEPLAY_KEY: number
  LAST_KEY: number

  RED_KEY: number
  GREEN_KEY: number
  YELLOW_KEY: number
  BLUE_KEY: number
  VOLUME_UP_KEY: number
  VOLUME_DOWN_KEY: number
  CHANNEL_UP_KEY: number
  CHANNEL_DOWN_KEY: number
  ZERO_KEY: number
  ONE_KEY: number
  TWO_KEY: number
  THREE_KEY: number
  FOUR_KEY: number
  FIVE_KEY: number
  SIX_KEY: number
  SEVEN_KEY: number
  EIGHT_KEY: number
  NINE_KEY: number
  STAR_KEY: number
  POUND_KEY: number
  TRACK_KEY: number
  STB_EVENT_KEY: number
  STOP_KEY: number
  FAVOR_KEY: number
  TVGUIDE_ALIGN_KEY: number
  CHANNEL_SWITCH_KEY: number
  LAST_CHANNEL_KEY: number
  VOICE_KEY: number
  NUMBER_KEY: Array<number> = _.times(10, (i) => { return 48 + i })

  keyCodeToNameMapping: any

    /**
     * Create the key index
     */
  private buildIndex () {
      if (this.keyCodeToNameMapping) {
          return
        }

      this.keyCodeToNameMapping = {}
      const keys: string[] = _.keys(this)
      _.each(keys, (name) => {
          _.each(_.flatten([this[name]]), (value) => {
              if (_.isNumber(value)) {
                  if (this.keyCodeToNameMapping[value]) {
                      this.keyCodeToNameMapping[value].push(name)
                    } else {
                      this.keyCodeToNameMapping[value] = [name]
                    }
                }
            })
        })
    }

    /**
     * get key name
     * @param  {number} keyCode [description]
     */
  public getEvents (event: KeyboardEvent): Array<{ keyCode: number, type: string }> {
      if (768 === event.keyCode) {
        } else {
          this.buildIndex()
          return _.map(this.keyCodeToNameMapping[event.keyCode], (type: string) => {
              return {
                  keyCode: event.keyCode,
                  type: type,
                  metaKey: event.metaKey
                }
            })
        }
    }
}
