import { KeyMap } from './keymap'

export class KeyboardKeyMap extends KeyMap {
  UP_KEY = 38
  LEFT_KEY = 37
  RIGHT_KEY = 39
  DOWN_KEY = 40
  OK_KEY = 13
  TT_KEY = 24
  RESTART_KEY = 25
  H_KEY = 88
  TV_KEY = 89
  LIVE_KEY = 85
  VAS_KEY = 87
  MENU_KEY = 72
  INFO_KEY = 73
  BTV_KEY = 81
  EXIT_KEY = 27
  AUDIO_KEY = 79
  SUBTITLE_KEY = 80
  MUTE_KEY = 75
  BACK_KEY = 66
  SEEKUP_KEY = 188
  SEEKDOWN_KEY = 190
  PAGEUP_KEY = 104
  PAGEDOWN_KEY = 98
  VOLUME_UP_KEY = 187
  VOLUME_DOWN_KEY = 189
  PAUSEPLAY_KEY = 192
  RED_KEY = 65
  GREEN_KEY = 83
  YELLOW_KEY = 68
  BLUE_KEY = 70
  STAR_KEY = 219
  LIVETV_KEY = 186
  VOD_KEY = 222
  GUIDE_KEY = 220
  POUND_KEY = 105
  TEST_KEY = 71
  OPTION_KEY = 74
  FBWD_KEY = 78
  FFWD_KEY = 77
  LOCAL_MEDIA_KEY = 67
  PVR_KEY = 112
  LONG_PVR_KEY = 113
  RATE_KEY = 82
  POWER_SUPPLY_KEY = 191
  CHANNEL_UP_KEY = 104
  CHANNEL_DOWN_KEY = 98
  RECENT_KEY = 86
  RECORD_KEY = 90
  OFF_KEY = 34
  SEARCH_KEY = 32
  FAVOR_KEY = 35
  POWER_KEY = 114
  ENTER_KEY = 110
  DVB_TXT_KEY = 36
  RADIO_KEY = 121
  LONG_PAGEUP_KEY = 10188
  LONG_PAGEDOWN_KEY = 10190
  STB_EXIT_KEY = 119
  LONG_UP_KEY = 10038
  LONG_DOWN_KEY = 10040
  LONG_CHANNEL_UP_KEY = 10187
  LONG_CHANNEL_DOWN_KE = 10189
  LONG_OK_KEY = 10013
  LONG_DELETE_KEY = 10046
  LONG_CHANNEL_DOWN_KEY = 10258
  ZERO_KEY = 48
  ONE_KEY = 49
  TWO_KEY = 50
  THREE_KEY = 51
  FOUR_KEY = 52
  FIVE_KEY = 53
  SIX_KEY = 54
  SEVEN_KEY = 55
  EIGHT_KEY = 56
  NINE_KEY = 57
  DELETE_KEY = 46
  STOP_KEY = 270
  RECORDINGS_KEY = 1119
  STB_EVENT_KEY = 768
  RENDER_COMPLETE_KEY = 769
  ON_KEY = 1152
  DVB_KEY = 1124
  TVOD_KEY = 84
  TVGUIDE_ALIGN_KEY = 76
  CHANNEL_SWITCH_KEY = 69
  LAST_CHANNEL_KEY = 269
  TRACK_KEY = 221
  VOICE_KEY = 45
}
