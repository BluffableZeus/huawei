﻿### Usage
```typescript
import { CAROUSEL_DIRECTIVES } from 'ng-epg-sdk';
```

### Remote control attribute.
    nav-group
        Indicates whether the current element is the focus group. 
  
    nav/nav-left/nav-right/nav-up/nav-down:
        If the value is false, the element or a certain direction does not support moving. 
        If the value is not false, the value is a CSS selector that specifies the next group or focus. 

    import {AutoFocus} from '../../../components/remote-controller';
    auto-focus="true":
        Default initial focus. 

    nav-history:
        false/true
        Indicates whether the focus group supports the function of recording historical focuses. The default value is true.

    nav-dialog:
        Modal window.
        The focus moves only within the area. 

    nav-init
        Initial focus selector of the group.



### Remote control event.

####1. Focus control of the system is canceled. The control focus self-defined by the component is used. 
    If you want to self-define the processing of the xkeydown event, invoke the event.stopPropagation() method to cancel automatic processing. 

####4. Focus events
    xfocus
    xblur
    xleft
    xright
    xup
    xdown
    group-xfocus
    group-xblur
    move-cancel
    move-left-cancel
    move-right-cancel
    move-up-cancel
    move-right-cancel


