import { DeepLinkConfig, NavLink } from './../../supply/navigation/nav-util'
import { DeepLinkConfigToken } from './../../supply/navigation/url-serializer'

import { EventEmitter, Injectable, Inject } from '@angular/core'

import { AppPortal, AppRootComponent } from './app-root'
import { Config } from '../../supply/config/config'
import { NavController } from '../../supply/navigation/nav-controller'
import { Platform } from '../../supply/platform/platform'
import { ViewController } from '../../supply/navigation/view-controller'

import * as _ from 'underscore'

@Injectable()
export class App {

  public viewDidEnter = new EventEmitter<ViewController>()
  public viewDidLeave = new EventEmitter<ViewController>()
  public viewDidLoad = new EventEmitter<ViewController>()
  public viewWillEnter = new EventEmitter<ViewController>()
  public viewWillLeave = new EventEmitter<ViewController>()
  public viewWillUnload = new EventEmitter<ViewController>()
  public viewDidChanged = new EventEmitter<{ enteringView: ViewController, leavingView: ViewController }>()
  public viewWillChanged = new EventEmitter<{ enteringView: ViewController, leavingView: ViewController }>()

  private rootNav: NavController = null
  private appRoot: AppRootComponent

  constructor (private config: Config,
    private plt: Platform,
    @Inject(DeepLinkConfigToken) private deepLinkConfig: DeepLinkConfig
  ) {
    this.viewDidEnter.subscribe(() => {
      this.addTopPageClass()
    })
    this.viewDidLeave.subscribe((view: ViewController) => {
      this.addTopPageClass(view)
    })
  }

  public addRoutes (routes: Array<{ segment?: string, path?: string, name: string, component: any, tags?: Array<string> }>) {
    routes.forEach((route) => {

      const navLink: any = _.clone(route)
      navLink.segment = route.path
      delete navLink.path

      let links = this.deepLinkConfig.links
      let found = _.find(links, (item, index) => {
        if (item.name === navLink.name) {
          links[index] = navLink
          return true
        }

        return false
      })

      if (!found) {
        this.deepLinkConfig.links.push(navLink)
      }
    })
  }

  public getPortal (portal: 'modal' | 'toast' | 'top'): NavController {
    switch (portal) {
      case 'modal':
        return this.getAppRoot()._getPortal(AppPortal.MODAL)

      case 'toast':
        return this.getAppRoot()._getPortal(AppPortal.TOAST)

      case 'top':
        return this.getAppRoot()._getPortal(AppPortal.TOP)
    }
  }

  public getRouter (name: string): NavLink {
    return _.findWhere(this.deepLinkConfig.links, { name: name })
  }

  public setAppRoot (appRoot: AppRootComponent) {
    this.appRoot = appRoot
  }

  public getAppRoot (): AppRootComponent {
    return this.appRoot
  }

  public setElementClass (className: string, isAdd: boolean) {
    this.appRoot.setElementClass(className, isAdd)
  }

  public getActiveNav (): NavController {
    const portal = this.appRoot._getPortal(AppPortal.MODAL)
    if (portal.length() > 0) {
      return findTopNav(portal)
    }
    return findTopNav(this.rootNav || null)
  }

  public getRootNav (): NavController {
    return this.rootNav
  }

  public setRootNav (nav: any) {
    this.rootNav = nav
  }

  private addTopPageClass (leaveView?: ViewController) {
    const rootNav = this.getRootNav()
    const modalNav = this.getPortal('modal')

    const allViews = _.without([...rootNav.getViews(), ...modalNav.getViews()], leaveView)

    const reversedAllViews = allViews.reverse()

    reversedAllViews.forEach((view: ViewController, i: number) => {
      const viewEl: HTMLElement = view.cmpRef.location.nativeElement
      const isTopPage = 0 === i
      if (isTopPage) {
        viewEl.classList.add('top-page')
      } else {
        viewEl.classList.remove('top-page')
      }
    })
  }
}

function findTopNav (nav: NavController) {
  let activeChildNav: any

  while (nav) {
    activeChildNav = nav.getActiveChildNav()
    if (!activeChildNav) {
      break
    }
    nav = activeChildNav
  }

  return nav
}
