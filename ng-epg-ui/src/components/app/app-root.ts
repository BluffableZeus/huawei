import { ViewController } from './../../supply/navigation/view-controller'
import {
  Component, ComponentFactoryResolver, ElementRef, Inject, OnInit, InjectionToken, Renderer, ViewChild, ViewContainerRef, ViewEncapsulation
} from '@angular/core'

import { App } from './app'
import { NavComponent } from '../nav/nav'
import { assert } from '../../supply/util/util'
import { Config } from '../../supply/config/config'
import { Ion } from '../ion'
import { OverlayPortalDirective } from '../nav/overlay-portal'
import { Platform } from '../../supply/platform/platform'

export const AppRootToken = new InjectionToken('USERROOT')

/**
 * @private
 */
@Component({
  selector: 'app-root',
  styleUrls: ['./app.scss'],
  encapsulation: ViewEncapsulation.None,
  template:
  '<div #viewport app-viewport></div>' +
  '<div #overlayPortal overlay-portal></div>' +
  '<div #loadingPortal class="loading-portal" overlay-portal></div>' +
  '<div #toastPortal class="toast-portal" [overlay-portal]="10000"></div>' +
  '<div class="click-block"></div>'
})
export class AppRootComponent extends Ion implements OnInit {

  @ViewChild('viewport', { read: ViewContainerRef }) _viewport: ViewContainerRef

  @ViewChild('overlayPortal', { read: OverlayPortalDirective }) _overlayPortal: OverlayPortalDirective

  @ViewChild('loadingPortal', { read: OverlayPortalDirective }) _loadingPortal: OverlayPortalDirective

  @ViewChild('toastPortal', { read: OverlayPortalDirective }) _toastPortal: OverlayPortalDirective

  constructor (
    @Inject(AppRootToken) private _userCmp: any,
    private _cfr: ComponentFactoryResolver,
    elementRef: ElementRef,
    renderer: Renderer,
    config: Config,
    private _plt: Platform,
    private app: App
  ) {
    super(config, elementRef, renderer)
    // register with App that this is Ionic's appRoot component. tada!
    app.setAppRoot(this)
  }

  ngOnInit () {
    // load the user root component
    // into Ionic's root component
    const factory = this._cfr.resolveComponentFactory(this._userCmp)
    const componentRef = this._viewport.createComponent(factory)
    this._renderer.setElementClass(componentRef.location.nativeElement, 'app-root', true)
    componentRef.changeDetectorRef.detectChanges()

    // set the mode class name
    // ios/md/wp
    this.setElementClass(this.config.get('mode'), true)

    const versions = this._plt.versions()
    this._plt.platforms().forEach(platformName => {
      // platform-ios
      let platformClass = 'platform-' + platformName
      this.setElementClass(platformClass, true)

      let platformVersion = versions[platformName]
      if (platformVersion) {
        // platform-ios9
        platformClass += platformVersion.major
        this.setElementClass(platformClass, true)

        // platform-ios9_3
        this.setElementClass(platformClass + '_' + platformVersion.minor, true)
      }
    })

    // touch devices should not use :hover CSS pseudo
    // enable :hover CSS when the "hoverCSS" setting is not false
    if (this.config.getBoolean('hoverCSS', true)) {
      this.setElementClass('enable-hover', true)
    }

    // sweet, the app root has loaded!
    // which means angular and ionic has fully loaded!
    // fire off the platform prepare ready, which could
    // have been switched out by any of the platform engines
    this._plt.prepareReady()
  }

  /**
   * @private
   */
  _getPortal (portal?: AppPortal): OverlayPortalDirective {

    if (portal === AppPortal.TOAST) {
      return this._toastPortal
    }
    // Modals need their own overlay becuase we don't want an ActionSheet
    // or Alert to trigger lifecycle events inside a modal
    if (portal === AppPortal.MODAL) {
      return this._modalPortal
    }

    if (portal === AppPortal.TOP) {
      return this._topPortal
    }
    return this._overlayPortal
  }

  /**
   * @private
   */
  _getActivePortal (): OverlayPortalDirective {
    const defaultPortal = this._overlayPortal
    const modalPortal = this._modalPortal
    const hasModal = modalPortal.length() > 0
    const hasDefault = defaultPortal.length() > 0

    if (!hasModal && !hasDefault) {
      return null

    } else if (hasModal && hasDefault) {
      let defaultIndex = defaultPortal.getActive().getZIndex()
      let modalIndex = modalPortal.getActive().getZIndex()

      if (defaultIndex > modalIndex) {
        return defaultPortal
      } else {
        assert(modalIndex > defaultIndex, 'modal and default zIndex can not be equal')
        return modalPortal
      }

    } if (hasModal) {
      return modalPortal

    } else if (hasDefault) {
      return defaultPortal
    }

  }

  get _modalPortal (): OverlayPortalDirective {
    const navCmp = this.app.getRootNav() as NavComponent
    return navCmp.modalPortal
  }

  get _topPortal (): OverlayPortalDirective {
    const navCmp = this.app.getRootNav() as NavComponent
    return navCmp.topPortal
  }

}

export const enum AppPortal {
  MODAL,
  TOAST,
  TOP
}
