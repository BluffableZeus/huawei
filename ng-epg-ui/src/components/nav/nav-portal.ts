import { DomController } from './../../supply/platform/dom-controller'
import { DeepLinker } from './../../supply/navigation/deep-linker'
import { TransitionController } from './../../supply/transitions/transition-controller'
import { Platform } from './../../supply/platform/platform'
import { Directive, ElementRef, Optional, NgZone, Renderer, ComponentFactoryResolver, ViewContainerRef } from '@angular/core'

import { App } from './app'
import { Config } from '../../supply/config/config'
import { NavController } from './nav-controller'
import { ViewController } from './view-controller'

/**
 * @private
 */
@Directive({
  selector: '[nav-portal]'
})
export class NavPortalDirective extends NavController {
  constructor (
    @Optional() viewCtrl: ViewController,
    @Optional() parent: NavController,
    app: App,
    config: Config,
    plt: Platform,
    elementRef: ElementRef,
    zone: NgZone,
    renderer: Renderer,
    _cfr: ComponentFactoryResolver,
    _trnsCtrl: TransitionController,
    _linker: DeepLinker,
    _domCtrl: DomController,
    viewPort: ViewContainerRef
  ) {
    super(parent, app, config, plt, elementRef, zone, renderer, _cfr, _trnsCtrl, _linker, _domCtrl)
    this.isPortal = true
    this.setViewport(viewPort)
  }
}
