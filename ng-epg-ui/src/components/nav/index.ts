export * from './nav'
export * from './nav-controller'
export * from './nav-params'
export * from './nav-portal'
export * from './view-controller'
export * from './route-tag.service'
export * from './resolve'
export * from './app'
export * from './nav.module'
export * from './nav-utils'

import { NavComponent } from './nav'
import { RouteTagService } from './route-tag.service'

export const NAV_DIRECTIVES = [
  NavComponent
]
export const NAV_PROVIDERS = [RouteTagService]

export enum PageType {
    Normal = 0,
    Home = 1,
    Video = 2,
    Background = 3
}
