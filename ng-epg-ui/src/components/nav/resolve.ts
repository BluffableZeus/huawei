import { NavParams } from './nav-params'

export interface Resolve {
  resolve (params: NavParams<any>)
}
