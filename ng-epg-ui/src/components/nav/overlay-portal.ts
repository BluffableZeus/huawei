import { ComponentFactoryResolver,
  Directive, ElementRef, forwardRef, Inject, Input, NgZone, Optional, Renderer, ViewContainerRef } from '@angular/core'

import { App } from '../app/app'
import { Config } from '../../supply/config/config'
import { DeepLinker } from '../../supply/navigation/deep-linker'
import { DomController } from '../../supply/platform/dom-controller'
import { NavController } from '../../supply/navigation/nav-controller'
import { Platform } from '../../supply/platform/platform'
import { TransitionController } from '../../supply/transitions/transition-controller'

/**
 * @private
 */
@Directive({
  selector: '[overlay-portal]'
})
export class OverlayPortalDirective extends NavController {
  constructor (
    @Inject(forwardRef(() => App)) app: App,
    config: Config,
    plt: Platform,
    elementRef: ElementRef,
    zone: NgZone,
    renderer: Renderer,
    cfr: ComponentFactoryResolver,
    transCtrl: TransitionController,
    @Optional() linker: DeepLinker,
    viewPort: ViewContainerRef,
    domCtrl: DomController
  ) {

    super(null, app, config, plt, elementRef, zone, renderer, cfr, transCtrl, linker, domCtrl)
    this.isPortal = true
    this.init = true
    this.setViewport(viewPort)

    // on every page change make sure the portal has
    // dismissed any views that should be auto dismissed on page change
    app.viewDidLeave.subscribe((ev: any) => {
      if (!ev.isOverlay) {
        this.dismissPageChangeViews()
      }
    })
  }

  @Input('overlay-portal')
  set _overlayPortal (val: number) {
    this.zIndexOffset = (val || 0)
  }

}
