### Usage
```typescript
import { NavModule } from 'ng-epg-ui/components/nav';
```
### Annotations
```typescript

export class nav implements OnInit {
    constructor() {
    }

    ngOnInit() {

    }
}
```

### nav properties
  - `nav-group`  - the focus moves within the specified range.
  - `nav-init` (`?:string=''`) - Set the initial focus position within the specified range
  - `nav-left` (`?:number=''`) - Set the focus position left shift from the specified range to the other range
  - `nav-right` (`?:number=''`) - Set the focus position right shift from the specified range to the other range
  - `nav-up` (`?:number=''`) - Set the focus position up shift from the specified range to the other range
  - `nav-down` (`?:number=''`) - Set the focus position down shift from the specified range to the other range
  - `nav-history` (`?:number=''`) - Set the history focus position in the specified range
  - `nav-tab` (`?:number=''`) - Set the focus position from the right side back to the left side with the Back key


