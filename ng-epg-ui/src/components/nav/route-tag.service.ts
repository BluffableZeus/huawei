import { NavLink } from './../../supply/navigation/nav-util'
import { Injectable } from '@angular/core'
import * as _ from 'underscore'

import { App } from './app'
import { ViewController } from './view-controller'
import { Logger } from '../../supply/util/logger'

const log = new Logger('route-tag.service.ts')

@Injectable()
export class RouteTagService {

  lastCheckTagCount = {}

  tagCount: { [key: string]: number } = {}

  lastRouter: NavLink
  checkTagChanged = _.debounce(() => {

      const lastCheckTagCount = this.lastCheckTagCount
      this.lastCheckTagCount = _.clone(this.tagCount)
      let isChanged = false

      _.each(this.tagCount as any, (count: number, tag: string) => {
          if (!lastCheckTagCount[tag] && this.tagCount[tag]) {
              isChanged = true
            }

          if (lastCheckTagCount[tag] && !this.tagCount[tag]) {
              isChanged = true
            }
        })

      if (isChanged) {
          log.debug('ROUTE_TAG changed, current:%s, last:%s', this.tagCount, lastCheckTagCount)
        }

    }, 1000)

  constructor (private app: App) {
    }

  onNotification (eventName: string, view: ViewController) {
      let router
      switch (eventName) {
          case 'ONPAGE_WILLENTER':
            router = this.app.getRouter(view.routeName)
            this.checkRouterActive(router)
            break

          case 'ONPAGE_DIDLOAD':
            router = this.app.getRouter(view.routeName)
            this.changeTagCount(router, 1)

            break

          case 'ONPAGE_DIDUNLOAD':
            router = this.app.getRouter(view.routeName)
            this.changeTagCount(router, -1)
            break
        }
    }

  checkRouterActive (router: NavLink) {
      if (!router) {
          return
        }
      let lastTags: Array<string> = []
      if (this.lastRouter) {
          lastTags = this.lastRouter.tags
        }
      this.lastRouter = router
    }

  changeTagCount (router: any, offset) {
      if (router && !_.isEmpty(router.tags)) {
          _.each(router.tags, (tag: string) => {
              this.tagCount[tag] = this.tagCount[tag] || 0
              this.tagCount[tag] = this.tagCount[tag] + offset
              this.tagCount[tag] = Math.max(this.tagCount[tag], 0)
            })

          this.checkTagChanged()
        }
    }

}
