import { OverlayPortalDirective } from './overlay-portal'
import { RouteTagService } from './route-tag.service'
import {
  AfterViewInit, Component, ComponentFactoryResolver, ElementRef, HostListener,
  Input, Optional, NgZone, Renderer, ViewChild, ViewContainerRef, ViewEncapsulation
} from '@angular/core'

import { App } from '../app/app'
import { AppPortal } from '../app/app-root'
import { Config } from '../../supply/config/config'
import { DeepLinker } from '../../supply/navigation/deep-linker'
import { DomController } from '../../supply/platform/dom-controller'
import { NavController } from '../../supply/navigation/nav-controller'
import { NavOptions } from '../../supply/navigation/nav-util'
import { Platform } from '../../supply/platform/platform'
import { TransitionController } from '../../supply/transitions/transition-controller'
import { ViewController } from '../../supply/navigation/view-controller'
import { KeyMap } from '../remote-controller'
import { EventService } from '../../../demos-app/sdk/event.service'
import { Logger } from '../../supply/util/logger'
import { FocusController } from '../../focus-controller'

import * as _ from 'underscore'

const log = new Logger('nav.ts')

const oldStopPropagation = CustomEvent.prototype.stopPropagation
CustomEvent.prototype.stopPropagation = function () {
  this._isStopPropagation = true
  return oldStopPropagation.apply(this)
}

CustomEvent.prototype['isStopPropagation'] = function () {
  return this._isStopPropagation
}

@Component({
  selector: 'ion-nav',
  template:
  '<div #viewport nav-viewport></div>' +
  '<div class="nav-decor"></div>' +
  '<div #modalPortal overlay-portal></div>' +
  '<div #topPortal overlay-portal></div>',
  encapsulation: ViewEncapsulation.None
})
export class NavComponent extends NavController implements AfterViewInit {

  @ViewChild('modalPortal', { read: OverlayPortalDirective })
  modalPortal: OverlayPortalDirective

  @ViewChild('topPortal', { read: OverlayPortalDirective })
  topPortal: OverlayPortalDirective
  /**
   * @input {object} Any nav-params to pass to the root page of this nav.
   */
  @Input() public rootParams: any
  private _root: any
  private _hasInit = false
  private noIneractionTimeout: any

  private routeTagService: RouteTagService
  private _logZoneStable = 'onkeydown'

  /**
   * no 768 keydown time
   */
  private lastUserKeydownTime = Number.MAX_VALUE

  constructor (
    @Optional() viewCtrl: ViewController,
    @Optional() parent: NavController,
    app: App,
    config: Config,
    plt: Platform,
    elementRef: ElementRef,
    zone: NgZone,
    renderer: Renderer,
    cfr: ComponentFactoryResolver,
    transCtrl: TransitionController,
    @Optional() linker: DeepLinker,
    domCtrl: DomController,
    private keyMap: KeyMap
  ) {
    super(parent, app, config, plt, elementRef, zone, renderer, cfr, transCtrl, linker, domCtrl)
    this.routeTagService = new RouteTagService(app)

    if (viewCtrl) {
      // an ion-nav can also act as an ion-page within a parent ion-nav
      // this would happen when an ion-nav nests a child ion-nav.
      viewCtrl.setContent(this)
    }

    if (parent) {
      // this Nav has a parent Nav
      parent.registerChildNav(this)

    } else if (viewCtrl && viewCtrl.getNav()) {
      // this Nav was opened from a modal
      this.parent = viewCtrl.getNav()
      this.parent.registerChildNav(this)

    } else if (app && !app.getRootNav()) {
      // a root nav has not been registered yet with the app
      // this is the root navcontroller for the entire app
      app.setRootNav(this)
      this.bindEvent(this)
    }
  }

  dispatchEvent (targetEl: HTMLElement, eventName: string, eventDetail: any = {}, options = {
    bubbles: true,
    cancelable: true
  }): { isStopPropagation: Function } {

    const customEvent = new CustomEvent(eventName, _.extend({
      detail: _.extend({ target: targetEl }, eventDetail)
    }, options))

    targetEl.dispatchEvent(customEvent)

    return customEvent as any
  }

  private bindEvent (rootNav: NavController) {
    this.zone.onStable.subscribe(this.onZoneStable.bind(this))
  }

  onZoneStable () {
    if (this._logZoneStable) {
      if (log.isKpi()) {
        log.kpi('HybirdVideoKPI_EPG_ONSTABLE|%s|%s|pass', this._logZoneStable, Date.now())
      } else {
        console.log('zone onstable, ' + this._logZoneStable)
      }
      this._logZoneStable = ''
    }
  }

  @HostListener('focusin', ['$event'])
  public onFocusIn (event: KeyboardEvent) {
    this.zone.runOutsideAngular(() => {
      (event.target as HTMLElement).blur()
    })
  }

  @HostListener('document:keydown', ['$event'])
  public onKeydown (event: KeyboardEvent) {
    const targetEl = event.target as HTMLElement
    if (event.keyCode !== 768) {
      this.lastUserKeydownTime = Date.now()
    }

    if ('INPUT' !== targetEl.nodeName && !targetEl.isContentEditable && 'TEXTAREA' !== targetEl.nodeName && _.has(this.keyMap.keyCodeToNameMapping, String(event.keyCode))) {
      return false
    }
  }

  public _onKeydown = (event: KeyboardEvent) => {
    let beginTime

    const keydownId = _.uniqueId('keydown_')
    const events = this.keyMap.getEvents(event)
    const keyCode = event.keyCode

    if (log.isKpi()) {
      beginTime = Date.now()
      log.kpi('HybirdVideoKPI_EPG_onkeydown|begin(%s) keyCode:%s events:%s|%s|pass, %s', keydownId, keyCode, events, Date.now(), this.config.get('gitInfo'))
    } else {
      console.log(`begin onkeydown(${keydownId}),keyCode:${keyCode}, ${this.config.get('gitInfo')}, events:${_.pluck(events, 'type')}`)
    }

    if (768 === event.keyCode) {
      const stbEvent = _.first(events)
      if (!stbEvent) {
        return
      }
      EventService.emit(stbEvent.type, stbEvent)
    } else {
      EventService.emit('USER_KEYDOWN', event)
      _.each(events as any, (_event: any) => {
        this.dispatchXKeydown(_event)
      })

      this._logZoneStable = 'onkeydown'
    }

    if (log.isKpi()) {
      log.kpi('HybirdVideoKPI_EPG_onkeydown|end(%s) keyCode:%s|%s|pass', keydownId, keyCode, Date.now())
    } else {
      console.log(`end onkeydown(${keydownId}),keyCode:${keyCode}`)
    }
  }

  dispatchXKeydown (event: { type: string, keyCode: number, data?: any, isStopPropagation?: any }) {
    const mainView = this.getActive()
    const topModalView = this.app.getPortal('top').getActive()
    const modalView = this.app.getPortal('modal').getActive()
    const toastView = this.app.getPortal('toast').getActive()

    let customEvent
    let focusController: FocusController

    if (toastView) {
      focusController = toastView.getFocusController()
      log.log(`dispatchXKeydown to toast view: %s`, toastView.name)

      if (focusController) {
        customEvent = focusController.onKeyDown(event as any)
        if (customEvent.isStop()) {
          return customEvent
        }
      }
    }

    if (topModalView) {
      focusController = topModalView.getFocusController()
      log.log(`dispatchXKeydown to modal view: %s`, topModalView.name)

      if (focusController) {
        customEvent = focusController.onKeyDown(event as any)
        if (customEvent.isStop() || !topModalView.getNavParams().get('transparentEvent')) {
          return customEvent
        }
      }
    }

    if (modalView) {
      focusController = modalView.getFocusController()
      log.log(`dispatchXKeydown to modal view: %s`, modalView.name)

      if (focusController) {
        customEvent = focusController.onKeyDown(event as any)
        if (customEvent.isStop() || !modalView.getNavParams().get('transparentEvent')) {
          return customEvent
        }
      }
    }

    if (mainView) {
      focusController = mainView.getFocusController()
      log.log(`dispatchXKeydown to main view: %s`, mainView.name)

      return focusController.onKeyDown(event as any)
    }
  }

  /**
   * @private
   */
  @ViewChild('viewport', { read: ViewContainerRef })
  set _vp (val: ViewContainerRef) {
    this.setViewport(val)
  }

  ngAfterViewInit () {
    this._hasInit = true

    let navSegment = this._linker.initNav(this)
    if (navSegment && navSegment.component) {
      // there is a segment match in the linker
      this.setPages(this._linker.initViews(navSegment), null, null)

    } else if (this._root) {
      // no segment match, so use the root property
      this.push(this._root, this.rootParams, {
        isNavRoot: (this.app.getRootNav() as any === this)
      }, null)
    }
  }

  goToRoot (opts: NavOptions) {
    this.setRoot(this._root, this.rootParams, opts, null)
  }

  /**
   * @input {Page} The Page component to load as the root page within this nav.
   */
  @Input()
  get root (): any {
    return this._root
  }
  set root (page: any) {
    this._root = page

    if (this._hasInit) {
      this.setRoot(page)
    }
  }

  public destroy () {
    super.destroy()
  }
}

class NoInteraction {

  onKeydown () {

  }
}
