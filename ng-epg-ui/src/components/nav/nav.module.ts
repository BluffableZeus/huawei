import { NgModule, ModuleWithProviders, ApplicationModule } from '@angular/core'
import { NavComponent } from './nav'
import { OverlayPortalDirective } from './overlay-portal'
import { RouteTagService } from './route-tag.service'
import { NavPortalDirective } from './nav-portal'
import { App } from './app'
import { Config } from '../../supply/config/config'
import { UISharedModule } from '../shared/shared.module'

@NgModule({
  imports: [ApplicationModule, UISharedModule],
  declarations: [
      NavComponent,
      OverlayPortalDirective,
      NavPortalDirective
    ],
  exports: [
      NavComponent,
      OverlayPortalDirective,
      NavPortalDirective
    ],
  providers: [RouteTagService, App, Config],
  entryComponents: []
})

export class NavModule {
  static forRoot (): ModuleWithProviders {
      return {
          ngModule: NavModule,
          providers: []
        }
    }
}
