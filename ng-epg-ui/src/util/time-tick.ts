import { BehaviorSubject } from 'rxjs'
export class TimeTick {
  public currentTime = null
  private timer = null

  start () {
      this.currentTime = new BehaviorSubject(new Date().getTime())
      this.startTimeTick()
    }
  startTimeTick () {
      this.stopTimeTick()
      this.currentTime.next(new Date().getTime())
      this.timer = setTimeout(() => {
          this.startTimeTick()
        }, 60 * 1000)
    }

  stopTimeTick () {
      if (this.timer) {
          clearTimeout(this.timer)
        }
    }
}
