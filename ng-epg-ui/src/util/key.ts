export enum Key {
  ENTER = 13 as any,
  ESCAPE = 27 as any,
  TAB = 9 as any
}
