import { Animation } from '../animations/animation'
import { PageTransition } from './page-transition'
import { TransitionOptions } from './transition'
import { ViewController } from '../components/nav/view-controller'

const TRANSLATEY = 'translateY'
const OFF_BOTTOM = '40px'
const CENTER = '0px'
const SHOW_BACK_BTN_CSS = 'show-back-button'

class ScaleTransition extends PageTransition {

  constructor (enteringView: ViewController, leavingView: ViewController, opts: TransitionOptions) {
    super(enteringView, leavingView, opts)

    // what direction is the transition going
    let backDirection = (opts.direction === 'back')

    // do they have navbars?
    let enteringHasNavbar = enteringView.hasNavbar()
    let leavingHasNavbar = leavingView && leavingView.hasNavbar()

    if (backDirection) {
      this.duration(opts.duration || 200).easing('cubic-bezier(0.47,0,0.745,0.715)')
      this.enteringPage.before.clearStyles(['transform'])

    } else {
      this.duration(opts.duration || 280).easing('cubic-bezier(0.36,0.66,0.04,1)')
      this.enteringPage
        .fromTo('transform', 'translate3d(-50%, -50%, 0) scale(0.2)', 'translate3d(-50%, -50%, 0) scale(1.0)', true)
        .fromTo('opacity', 0.01, 1, true)
    }

    // setup leaving view
    if (leavingView && backDirection) {
      // leaving content
      this.duration(opts.duration || 200).easing('cubic-bezier(0.47,0,0.745,0.715)')
      let leavingPage = new Animation(leavingView.pageRef())
      this.add(leavingPage.fromTo('transform', 'translate3d(-50%, -50%, 0) scale(1.0)', 'translate3d(-50%, -50%, 0) scale(0.2)').fromTo('opacity', 0.99, 0))
    }

  }

}

PageTransition.register('scale-transition', ScaleTransition)
