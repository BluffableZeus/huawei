import { Animation } from '../animations/animation'
import { PageTransition } from './page-transition'
import { TransitionOptions } from './transition'
import { ViewController } from '../components/nav/view-controller'

const DURATION = 500
const EASING = 'cubic-bezier(0.36,0.66,0.04,1)'
const OPACITY = 'opacity'
const TRANSLATEX = 'translateX'
const OFF_RIGHT = '99.5%'
const OFF_LEFT = '-33%'
const CENTER = '0%'
const OFF_OPACITY = 0.8
const SHOW_BACK_BTN_CSS = 'show-back-button'

class IOSTransition extends PageTransition {

  constructor (enteringView: ViewController, leavingView: ViewController, opts: TransitionOptions) {
    super(enteringView, leavingView, opts)

  }

}

PageTransition.register('ios-transition', IOSTransition)
