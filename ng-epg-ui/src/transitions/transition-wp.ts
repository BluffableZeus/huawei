import { Animation } from '../animations/animation'
import { PageTransition } from './page-transition'
import { TransitionOptions } from './transition'
import { ViewController } from '../components/nav/view-controller'

const SHOW_BACK_BTN_CSS = 'show-back-button'
const SCALE_SMALL = .95

class WPTransition extends PageTransition {

  constructor (enteringView: ViewController, leavingView: ViewController, opts: TransitionOptions) {
    super(enteringView, leavingView, opts)

    // what direction is the transition going
    let backDirection = (opts.direction === 'back')

    // setup leaving view
    if (leavingView && backDirection) {
      // leaving content
      this.duration(opts.duration || 200).easing('cubic-bezier(0.47,0,0.745,0.715)')
      let leavingPage = new Animation(leavingView.pageRef())
      this.add(leavingPage.fromTo('scale', 1, SCALE_SMALL).fromTo('opacity', 0.99, 0))
    }

  }

}

PageTransition.register('wp-transition', WPTransition)
