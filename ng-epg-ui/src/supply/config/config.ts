import { InjectionToken } from '@angular/core'

/**
* @ngdoc service
* @name Config
* @module ionic
* @description
* Config allows you to set the modes of your components
*/
import { Platform } from '../platform/platform'
import { isObject, isArray } from '../util/util'

export class Config {
  private convertSize = 1

  private store: Object = {}

  private trns: Object = {}

  private _modes = {}

  public plt: Platform

  public init (config: any, plt: Platform) {
    this.store = config && isObject(config) && !isArray(config) ? config : {}
    this.plt = plt
  }

  /**
   * @name get
   * @description
   * Returns a single config value, given a key.
   *
   * @param {string} [key] - the key for the config value
   * @param {any} [fallbackValue] - a fallback value to use when the config
   * value was not found, or is config value is `null`. Fallback value
   *  defaults to `null`.
   */
  public get (key: string, fallbackValue: any = null): any {
    const value = this.store[key]
    if (null === value || undefined === value) {
      return fallbackValue
    }

    return value
  }

  public set (key: string, value: any) {
    this.store[key] = value
  }

  /**
   * @private
   */
  public setModeConfig (modeName: string, modeConfig: any) {
    this._modes[modeName] = modeConfig
  }

  /**
   * @private
   */
  getModeConfig (modeName: string): any {
    return this._modes[modeName] || null
  }

  /**
   * @name getBoolean
   * @description
   * Same as `get()`, however always returns a boolean value. If the
   * value from `get()` is `null`, then it'll return the `fallbackValue`
   * which defaults to `false`. Otherwise, `getBoolean()` will return
   * if the config value is truthy or not. It also returns `true` if
   * the config value was the string value `"true"`.
   * @param {string} [key] - the key for the config value
   * @param {boolean} [fallbackValue] - a fallback value to use when the config
   * value was `null`. Fallback value defaults to `false`.
   */
  public getBoolean (key: string, fallbackValue = false): boolean {
    const val = this.get(key)
    if (val === null) {
      return fallbackValue
    }
    if (typeof val === 'string') {
      return val === 'true'
    }
    return !!val
  }

  /**
   * @name getNumber
   * @description
   * Same as `get()`, however always returns a number value. Uses `parseFloat()`
   * on the value received from `get()`. If the result from the parse is `NaN`,
   * then it will return the value passed to `fallbackValue`. If no fallback
   * value was provided then it'll default to returning `NaN` when the result
   * is not a valid number.
   * @param {string} [key] - the key for the config value
   * @param {number} [fallbackValue] - a fallback value to use when the config
   * value turned out to be `NaN`. Fallback value defaults to `NaN`.
   */
  public getNumber (key: string, fallbackValue: number = NaN): number {
    const val = parseFloat(this.get(key))
    return isNaN(val) ? fallbackValue : val
  }

  /**
 * @private
 */
  setTransition (trnsName: string, trnsClass: any) {
    this.trns[trnsName] = trnsClass
  }

  /**
   * @private
   */
  getTransition (trnsName: string): any {
    return this.trns[trnsName] || null
  }

  /**
   * @private
   */
  setConvertSize (convertSize: number) {
    this.convertSize = convertSize
  }

  /**
   * get convert ratio of font size based on 720P
   */
  getConvertSize (): number {
    return this.convertSize || 1
  }
}

/**
 * @private
 */
export const ConfigToken = new InjectionToken('USERCONFIG')

/**
 * @private
 */
export function setupConfig (userConfig: any, plt: Platform): Config {
  const config = new Config()
  config.init(userConfig, plt)

  // add the config obj to the window
  const win: any = plt.win()
  win['Ionic'] = win['Ionic'] || {}
  win['Ionic']['config'] = config

  return config
}
