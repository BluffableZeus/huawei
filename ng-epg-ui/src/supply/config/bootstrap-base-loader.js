/* eslint func-names: 0 */

const path = require('path');
const fs = require('fs');
const yaml = require('js-yaml');
const stripComments = require('strip-json-comments');

const CONFIG_FILE = '.bootstraprc';
const userConfigPath = path.resolve(__dirname, `../${CONFIG_FILE}`);
var componentsCustomizationsPath;
if (fileExists(userConfigPath)) {
    const userConfig = parseConfig(userConfigPath);
    if (userConfig.componentsCustomizations) {
        componentsCustomizationsPath = path.resolve(__dirname, '../', userConfig.componentsCustomizations);
    }
}
/**
 * Bootstrap SASS styles loader
 *
 * @returns {string}
 */
module.exports = function(source) {
    if (this.cacheable) this.cacheable();

    const config = global.__BOOTSTRAP_CONFIG__||'3';
    const bootstrapVersion = parseInt(config.bootstrapVersion, 10);
    const styles = config.styles;
    const bootstrapPath = config.bootstrapPath;
    const useFlexbox = config.useFlexbox;
    const useCustomIconFontPath = config.useCustomIconFontPath;
    const preBootstrapCustomizations = config.preBootstrapCustomizations;
    const bootstrapCustomizations = config.bootstrapCustomizations;

    const processedStyles = [];

    if (bootstrapVersion === 4 && useFlexbox) {
        processedStyles.push('$enable-flex: true;');
    }

    if (styles.indexOf('mixins') > -1) {
        processedStyles.push(
            createBootstrapImport('mixins', bootstrapVersion, bootstrapPath, this)
        );
    }

    if (preBootstrapCustomizations) {
        processedStyles.push(
            createUserImport(preBootstrapCustomizations, this)
        );
    }

    processedStyles.push(
        createBootstrapImport('variables', bootstrapVersion, bootstrapPath, this)
    );

    if (bootstrapVersion === 3 && !useCustomIconFontPath) {
        processedStyles.push(
            `$icon-font-path: "${getFontsPath(bootstrapPath, this)}";`
        );
    }

    if (bootstrapCustomizations) {
        processedStyles.push(
            createUserImport(bootstrapCustomizations, this)
        );
    }

    processedStyles.push(source);

    const componentsStrIndex = this.resource.indexOf('components' + path.sep);
    var componentCustomizeStyle = '';

    if (componentsStrIndex >= 0) {
        const componentsBasePath = this.resource.substring(0, componentsStrIndex + 'components/'.length);
        const customizePath = path.resolve(componentsCustomizationsPath, path.relative(componentsBasePath, this.resource));
        if (fileExists(customizePath)) {
            componentCustomizeStyle = createUserImport(customizePath, this);
            processedStyles.push(componentCustomizeStyle);
        }
    }

    const stylesOutput = (
        processedStyles
        .map(style => style.replace(/\\/g, '/') + '\n')
        .join('')
    );

    return stylesOutput;
};

function getFontsPath(bootstrapPath, webpack) {
    return path.join(path.relative(webpack.context, bootstrapPath), 'assets', 'fonts', 'bootstrap/');
}

function createUserImport(module, webpack) {
    const userModule = path.relative(webpack.context, module);
    webpack.addDependency(userModule);
    return `@import "${userModule}";`;
}


function createBootstrapImport(module, bootstrapVersion, bootstrapPath, webpack) {
    const stylesPath = (
        parseInt(bootstrapVersion, 10) === 3 ? ['assets', 'stylesheets', 'bootstrap'] : ['scss']
    );
    const bootstrapModule = path.join(path.relative(webpack.context, bootstrapPath), ...stylesPath, `_${module}`);
    return `@import "${bootstrapModule}";`;
}

/**
 * Parses config in YAML or JSON formats
 *
 * @param {string} configPath
 * @returns {Object}
 */
function parseConfig(configPath) {
    const configContent = stripComments(fs.readFileSync(configPath, 'utf8'));
    return yaml.safeLoad(configContent);
}

/**
 * Checks if file exists
 *
 * @param {string} file
 * @returns {boolean}
 */
function fileExists(file) {
    try {
        return fs.statSync(file).isFile();
    } catch (error) {
        return false;
    }
}
