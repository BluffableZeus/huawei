import { Logger } from './logger'
export const STORE: { [key: string]: any } = {}
import * as _ from 'underscore'
const keys = {
  loggerInjector: 'loggerInjector'
}
/**
 * suppor KEY:loggerInjector
 */
export const UIInjector = {
  get: (key: string, fallbackValue: any = undefined): any => {
    const value = STORE[key]

    if (_.isUndefined(value) || _.isNull(value)) {
      return fallbackValue
    }

    return value
  },

  /**
   *
   */
  set: (key: string, value: any) => {
    STORE[key] = value
    if (key === keys.loggerInjector) {
      Logger.injectorLogger = value
    }
    return this
  }
}
