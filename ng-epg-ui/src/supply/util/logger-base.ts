import { Optional } from '@angular/core'

export abstract class BaseLogger {
  public static setLevel (levelName: string) { }
  constructor (@Optional() public logName: string) { }

  public log (...optionalParams: any[]) { }

  public debug (...optionalParams: any[]) { }

  public info (...optionalParams: any[]) { }

  public kpi (...optionalParams: any[]) { }

  public warn (...optionalParams: any[]) { }

  public error (...optionalParams: any[]) { }
  public isKpi () {}
  public returnDebugLogers (): string[] { return [] }
}
