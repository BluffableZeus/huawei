import { Injectable } from '@angular/core'
import { BaseLogger } from './logger-base'
import * as _ from 'underscore'

@Injectable()
export class Logger extends BaseLogger {

  /**
   * set the log classes injected from the EPG project
   */
  public static set injectorLogger (logger: any) {
    if (logger) {
      let loggerInstance = new logger('')
      _.extend(Logger.prototype, loggerInstance)
      _.extend(Logger, logger)
    }
  }

}
