import { Animation } from '../animations/animation'
import { isPresent } from '../util/util'
import { PageTransition } from './page-transition'

const TRANSLATEY = 'translateY'
const OFF_BOTTOM = '40px'
const CENTER = '0px'

export class MDTransition extends PageTransition {

  init () {
    super.init()

    const plt = this.plt
    const enteringView = this.enteringView
    const leavingView = this.leavingView
    const opts = this.opts

    // what direction is the transition going
    const backDirection = (opts.direction === 'back')

    if (enteringView) {
      if (backDirection) {
        this.duration(isPresent(opts.duration) ? opts.duration : 200).easing('cubic-bezier(0.47,0,0.745,0.715)')

      } else {
        this.duration(isPresent(opts.duration) ? opts.duration : 280).easing('cubic-bezier(0.36,0.66,0.04,1)')
        this.enteringPage
          .fromTo(TRANSLATEY, OFF_BOTTOM, CENTER, true)
          .fromTo('opacity', 0.01, 1, true)
      }
    }

    // setup leaving view
    if (leavingView && backDirection) {
      // leaving content
      this.duration(opts.duration || 200).easing('cubic-bezier(0.47,0,0.745,0.715)')
      const leavingPage = new Animation(plt, leavingView.pageRef())
      this.add(leavingPage.fromTo(TRANSLATEY, CENTER, OFF_BOTTOM).fromTo('opacity', 1, 0))
    }

  }

}
