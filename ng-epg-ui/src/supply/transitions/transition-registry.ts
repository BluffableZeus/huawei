import { Config } from '../config/config'
import { Platform } from '../platform/platform'
import { Transition } from './transition'
import { MDTransition } from './transition-md'
import { WPTransition } from './transition-wp'

export function registerTransitions (config: Config) {
  return function () {
    config.setTransition('ios-transition', MDTransition)
    config.setTransition('md-transition', MDTransition)
    config.setTransition('wp-transition', WPTransition)

  }
}

export function createTransition (plt: Platform,
config: Config, transitionName: string, enteringView: any, leavingView: any, opts: any): Transition {
  let TransitionClass: any = config.getTransition(transitionName)
  if (!TransitionClass) {
    // didn't find a transition animation, default to ios-transition
    TransitionClass = config.getTransition('ios-transition')
  }

  return new TransitionClass(plt, enteringView, leavingView, opts)
}
