import { Injectable } from '@angular/core'

import { AnimationOptions } from '../animations/animation'
import { Config } from '../config/config'
import { createTransition } from './transition-registry'
import { isPresent } from '../util/util'
import { NavController } from '../navigation/nav-controller'
import { Platform } from '../platform/platform'
import { Transition } from './transition'
import { ViewController } from '../navigation/view-controller'

/**
 * @private
 */
@Injectable()
export class TransitionController {
  private _ids = 0
  private _trns: {[key: number]: Transition} = {}

  constructor (public plt: Platform, private _config: Config) {}

  getRootTrnsId (nav: NavController): number {
    let parent = nav.parent as NavController
    while (parent) {
      if (isPresent(parent.trnsId)) {
        return parent.trnsId
      }
      parent = parent.parent
    }
    return null
  }

  nextId () {
    return this._ids++
  }

  get (trnsId: number, enteringView: ViewController, leavingView: ViewController, opts: AnimationOptions): Transition {
    const trns = createTransition(this.plt, this._config, opts.animation, enteringView, leavingView, opts)
    trns.trnsId = trnsId

    if (!this._trns[trnsId]) {
      // we haven't created the root transition yet
      this._trns[trnsId] = trns

    } else {
      // we already have a root transition created
      // add this new transition as a child to the root
      this._trns[trnsId].add(trns)
    }

    return trns
  }

  destroy (trnsId: number) {
    if (this._trns[trnsId]) {
      this._trns[trnsId].destroy()
      delete this._trns[trnsId]
    }
  }

}
