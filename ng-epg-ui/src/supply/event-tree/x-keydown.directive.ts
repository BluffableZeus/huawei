import {
    Directive, Input, Output, EventEmitter, Optional, AfterViewInit, OnDestroy,
    SkipSelf, ViewContainerRef, ElementRef
} from '@angular/core'

import { CustomEvent } from './custom-event'
import { EventTreeBase } from './event-tree-base'
import { FocusController } from '../focus-controller/focus-controller'
import { FocusDirective } from '../focus-controller/focus.directive'

let FocusDirectiveClass: typeof FocusDirective
@Directive({
  selector: '[xkeydown], [navSkip]'
})
export class XKeydownDirective extends EventTreeBase implements AfterViewInit, OnDestroy {

  @Input()
    isInTemplate = false

  @Input()
    navSkip

  @Output('xkeydown')
    xkeydown = new EventEmitter<CustomEvent>()

  el: HTMLElement
  children: Array<XKeydownDirective> = []

  static setFocusDirectiveClass (_FocusDirectiveClass: typeof FocusDirective) {
      FocusDirectiveClass = _FocusDirectiveClass
    }

  constructor (
        @Optional() @SkipSelf() protected parent: XKeydownDirective,
        @Optional() protected focusController: FocusController,
        protected viewContainerRef: ViewContainerRef,
        private elementRef: ElementRef
    ) {
      super()
      this.el = elementRef.nativeElement
      if (this.focusController) {
          this.focusController.addXKeydown(this)
        }
    }

  ngAfterViewInit () {
      if (this.isInTemplate && this.focusController) {
          this.parent = this.focusController.findNearestXKeydown(this.el, false) || this.parent
        }
      const focus: FocusDirective = this.viewContainerRef.parentInjector.get(FocusDirectiveClass, null)
      if (focus) {
          focus.xKeydown = this
        }
      super.ngAfterViewInit()
    }

  dispatchEvent (event: CustomEvent) {
      if (this.navSkip === event.type) {
          event.stop('nav skip')
          return this.focusController.moveSkip(this.el, event.type)
        }
      return super.dispatchEvent(event, 'xkeydown')
    }

  ngOnDestroy () {
      if (this.focusController) {
          this.focusController.removeXKeydown(this)
        }
      super.ngOnDestroy()
    }
}
