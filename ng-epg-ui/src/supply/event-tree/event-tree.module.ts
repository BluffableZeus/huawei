import { NgModule } from '@angular/core'

import { XKeydownDirective } from './x-keydown.directive'
import { XFocusDirective } from './x-focus.directive'

@NgModule({
  imports: [],
  exports: [XKeydownDirective, XFocusDirective],
  declarations: [XKeydownDirective, XFocusDirective]
})
export class EventTreeModule { }
