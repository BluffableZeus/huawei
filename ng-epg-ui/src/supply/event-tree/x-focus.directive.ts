import {
    EventEmitter, Directive, AfterViewInit, OnDestroy, Input, Output,
    Optional, SkipSelf, ViewContainerRef, ElementRef
} from '@angular/core'

import { CustomEvent } from './custom-event'
import { EventTreeBase } from './event-tree-base'
import { FocusController } from '../focus-controller/focus-controller'
import { FocusDirective } from '../focus-controller/focus.directive'

let FocusDirectiveClass: typeof FocusDirective
@Directive({
  selector: '[xwillfocus], [xfocus], [xwillblur], [xblur], [xmovefailed]'
})
export class XFocusDirective extends EventTreeBase implements AfterViewInit, OnDestroy {

  @Input()
    isInTemplate = false

  @Output('xwillfocus')
    xwillfocus = new EventEmitter<CustomEvent>()

  @Output('xfocus')
    xfocus = new EventEmitter<CustomEvent>()

  @Output('xwillblur')
    xwillblur = new EventEmitter<CustomEvent>()

  @Output('xblur')
    xblur = new EventEmitter<CustomEvent>()

  @Output('xmovefailed')
    xmovefailed = new EventEmitter<CustomEvent>()

  @Output('xfocusdisappear')
    xfocusdisappear = new EventEmitter<CustomEvent>()

  children: Array<EventTreeBase> = []
  el: HTMLElement

  static setFocusDirectiveClass (_FocusDirectiveClass: typeof FocusDirective) {
      FocusDirectiveClass = _FocusDirectiveClass
    }

  constructor (
        @Optional() @SkipSelf() protected parent: XFocusDirective,
        protected viewContainerRef: ViewContainerRef,
        @Optional() protected focusController: FocusController,
        private elementRef: ElementRef
    ) {
      super()
      this.el = elementRef.nativeElement
      this.focusController.addXFocus(this)
    }

  ngAfterViewInit () {
      if (this.isInTemplate) {
          this.parent = this.focusController.findNearestXFocus(this.el, false) || this.parent
        }
      const focus: FocusDirective = this.viewContainerRef.parentInjector.get(FocusDirectiveClass, null)
      if (focus) {
          focus.xFocus = this
        }
      super.ngAfterViewInit()
    }

  ngOnDestroy () {
      this.focusController.removeXFocus(this)
      super.ngOnDestroy()
    }
}
