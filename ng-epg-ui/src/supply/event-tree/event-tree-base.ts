import * as _ from 'underscore'
import { AfterViewInit, OnDestroy, ViewContainerRef, EventEmitter } from '@angular/core'

import { CustomEvent } from './custom-event'
import { FocusController } from '../focus-controller'

export abstract class EventTreeBase implements AfterViewInit, OnDestroy {

    /**
     * emit before focus
     *
     * @type {EventEmitter<CustomEvent>}
     * @memberOf EventTreeBase
     */
  xwillfocus: EventEmitter<CustomEvent>

    /**
     * emit when focused
     *
     * @type {EventEmitter<CustomEvent>}
     * @memberOf EventTreeBase
     */
  xfocus: EventEmitter<CustomEvent>

    /**
     * emit before focus will unfocus
     *
     * @type {EventEmitter<CustomEvent>}
     * @memberOf EventTreeBase
     */
  xwillblur: EventEmitter<CustomEvent>

    /**
     * emit when focus has unfocused
     *
     * @type {EventEmitter<CustomEvent>}
     * @memberOf EventTreeBase
     */
  xblur: EventEmitter<CustomEvent>

    /**
     * emit when focus move failed
     *
     * @type {EventEmitter<CustomEvent>}
     * @memberOf EventTreeBase
     */
  xmovefailed: EventEmitter<CustomEvent>

    /**
     * emit when focus disappear
     *
     * @type {EventEmitter<CustomEvent>}
     * @memberOf EventTreeBase
     */
  xfocusdisappear: EventEmitter<CustomEvent>

    /**
     * emit when when remote controller key is pressed
     *
     * @type {EventEmitter<CustomEvent>}
     * @memberOf EventTreeBase
     */
  xKeydown: EventEmitter<CustomEvent>

  el: HTMLElement

  protected parent: EventTreeBase

  public children: Array<EventTreeBase> = []

  protected focusController: FocusController

  protected viewContainerRef: ViewContainerRef

  ngAfterViewInit () {
      if (this.parent) {
          this.parent.add(this)
        }
    }

  add (eventTree: EventTreeBase) {
      this.children.push(eventTree)
    }

  remove (eventTree: EventTreeBase) {
      this.children = _.without(this.children, eventTree)
    }

  dispatchEvent (event: CustomEvent, emitterName?: string) {
      event.detail.currentTarget = this.el
      this[emitterName].emit(event)
      if (!event.isStop() && this.parent) {
          return this.parent.dispatchEvent(event, emitterName)
        }

      return event.isStop()
    }

  ngOnDestroy () {
      if (this.parent) {
          this.parent.remove(this)
        }
    }
}
