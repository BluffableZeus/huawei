import { Logger } from '../util/logger'

const log = new Logger('ui:CustomEvent')
export class CustomEvent {

  type: string
  keyCode: number
  metaKey: boolean
  detail: any = {}

  private _isStop = false
  private canStop: boolean

  constructor (type: string, { keyCode = -1, metaKey = false, canStop = true, target = null } = {}) {
      this.type = type
      this.keyCode = keyCode
      this.metaKey = metaKey
      this.canStop = canStop
      this.detail = { type, keyCode, metaKey, target }
    }

  stop (reason = 'unknown') {
      if (!this.canStop) {
          throw new Error(`${this.type} event can not be stoped!`)
        }
      log.debug('%s is stoped by %s', this.type, reason)
      this._isStop = true
    }

  stopPropagation (reason?: string) {
      this.stop(reason)
    }

  isStop () {
      return this._isStop
    }
}
