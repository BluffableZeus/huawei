FocusControllerModule - 主要用来自动控制焦点的移动， 当EPG页面收到遥控器的按键事件时，会从当前焦点所在位置按照angular构造的组件树一层一层的向上（外）传递，直到遇到组件处理事件后停止了事件的传播或者到达整个应用的根组件；当事件在整个组件树传递完之后，如果没有被主动停止掉，并且事件是上（'UP_KEY'）、下（'DOWN_KEY'）、左（'LEFT_KEY'）、右（'RIGHT_KEY'）时，底层焦点机制会根据配置（FocusConfig）进行自动焦点移动。

关于事件的传播 - 目前事件传播，是通过angular的指令（XKeydownDirective、XFocusDirective等）构造的事件树进行传播的。
