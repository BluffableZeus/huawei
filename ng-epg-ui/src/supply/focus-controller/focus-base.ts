import * as _ from 'underscore'
import { EventEmitter, Renderer2 } from '@angular/core'

import { EventTreeBase, CustomEvent } from '../event-tree'
import { FocusController } from './focus-controller'
import { FocusDirective } from './focus.directive'
import { FocusGroupDirective } from './focus-group.directive'

export const DIRECTIONS = {
  init: 'init',
  left: 'left',
  down: 'down',
  right: 'right',
  top: 'top'
}

export const DefaultFocusConfig: FocusConfig = {
  straightOnly: false,
  straightLtr: null,
  straightUtd: null,
  straightOverlapThreshold: 0.5,
  rememberSource: false,
  disabled: false,
    // '', 'last-focused', 'first', 'last', '$n', 'last-focused|first', 'last-focused|last', 'last-focused|$n'
  enterTo: '',
  left: '',
  right: '',
  up: '',
  down: '',
  restrict: 'self-first', // 'self-first', 'self-only', 'none'
  autoFocus: false,
  focusedClass: 'focused',
  groupFocusedClass: 'group-focused',
  cycle: false,
  cache: false,
  order: 0,
  autoFocusAfterDisappear: true
}
export abstract class FocusBase {

    /**
     * emit before focus will unfocus
     *
     * @type {EventEmitter<CustomEvent>}
     * @memberOf FocusBase
     */
  _willUnfocus: EventEmitter<CustomEvent>

    /**
     * emit when focus has unfocused
     *
     * @type {EventEmitter<CustomEvent>}
     * @memberOf FocusBase
     */
  _unfocused: EventEmitter<CustomEvent>

    /**
     * emit before focus
     *
     * @type {EventEmitter<CustomEvent>}
     * @memberOf FocusBase
     */
  _willFocus: EventEmitter<CustomEvent>

    /**
     * emit when focused
     *
     * @type {EventEmitter<CustomEvent>}
     * @memberOf FocusBase
     */
  _focused: EventEmitter<CustomEvent>

    /**
     * emit when focus move failed
     *
     * @type {EventEmitter<CustomEvent>}
     * @memberOf FocusBase
     */
  _moveFailed: EventEmitter<CustomEvent>

  parent: FocusGroupDirective
  config: FocusConfig

  focusController: FocusController
  focusList: Array<FocusDirective>
  renderer2: Renderer2
  xKeydownDirective: EventTreeBase
  xFocusDirective: EventTreeBase
  currentFocus: FocusDirective
  lastFocus: FocusDirective

  private clientRect: ClientRect

  constructor (protected el: HTMLElement) {
    }

  getBoundingClientRect (): ClientRect {
      if (this.config.cache) {
          this.clientRect = this.clientRect || this.el.getBoundingClientRect()
        } else {
          return this.el.getBoundingClientRect()
        }

      return this.clientRect
    }

  clearBoundingClientRect () {
      this.clientRect = null
    }

  getElement (): HTMLElement {
      return this.el
    }

  protected mergeConfig (parentConfig: FocusConfig, selfConfig: FocusConfig) {
      const config: FocusConfig = _.extend({}, parentConfig)
      const canNotInheritConfig = ['autoFocus', 'up', 'down', 'left', 'right', 'cycle', 'order', 'restrict']
      _.each(canNotInheritConfig, key => {
          delete config[key]
        })
      return _.extend({}, DefaultFocusConfig, config, selfConfig)
    }

  abstract isFocus (): boolean

  abstract isFocused (): boolean

  abstract add (focusable: FocusBase)

  abstract remove (focusable: FocusBase)

  abstract willFocus (direction: string, oldFocus?: FocusDirective): boolean

  abstract focus (direction: string, oldFocus?: FocusBase)

  abstract focused (direction: string, oldFocus?: FocusDirective): void

  abstract willBlur (direction: string, newFocus?: FocusBase): boolean

  abstract blur (direction: string, newFocus?: FocusBase)

  abstract blured (direction: string, newFocus?: FocusBase): void

  abstract moveFailed (direction: string): void

  addClass (className: string) {
      this.renderer2.addClass(this.el, className)
    }

  removeClass (className: string) {
      this.renderer2.removeClass(this.el, className)
    }
}

export interface FocusConfig {
  straightOnly?: boolean
  straightLtr?: boolean
  straightUtd?: boolean
  straightOverlapThreshold?: number
  rememberSource?: boolean
  disabled?: boolean
  enterTo?: string
    // leaveFor
  left?: string
  right?: string
  up?: string
  down?: string
  restrict?: string
  autoFocus?: boolean
  previous?: RememberSource
  focusedClass?: string
  groupFocusedClass?: string
  cycle?: boolean
  cache?: boolean
  order?: number
  autoFocusAfterDisappear?: boolean
  focusTag?: string
}

export const DIRECTIONS_REVERSE = {
  'left': 'right',
  'up': 'down',
  'right': 'left',
  'down': 'up'
}

export interface RememberSource {
  target: FocusBase
  destination: FocusBase
  reverse: string
}
