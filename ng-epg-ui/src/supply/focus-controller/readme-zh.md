### 需要焦点控制和按键监听的Module需要引入FocusControllerModule
```typescript
import { FocusControllerModule } from 'ng-epg-ui/focus-controller';
......
@NgModule({
    imports: [..., FocusControllerModule, ...],
})
```

## FocusControllerModule 中包含的主要指令
### FocusDirective：
需要落焦点的dom元素，需要指定此指令
```typescript
@Directive({
    selector: '[snFocus],ui-button,[focusable]'
})
export class FocusDirective extends FocusBase implements AfterViewInit, OnDestroy {
    @Input('snFocus')
    set config(_cfg: FocusConfig) {
        this.originConfig = _cfg;
        this._config = this.mergeConfig(this.parent && this.parent.config, _cfg);
    }

    @Input()
    isInTemplate = false;
```
```html
<div snFocus>focusable element</div>
```
#### 属性说明
| 属性   | 说明    | 类型     | 可选值     | 默认值    | 必传     |
|-----   |-----    |-----    |------      |------    |------    |
| snFocus | 焦点的配置对象，详见下面 FocusConfig 属性说明 | FocusConfig | / | / |  否 |
| isInTemplate | 当焦点存在于嵌入模板（ng-content、ng-template）时，为了查找实际生成的dom层次结构中的父group时，需要指定此参数为true | boolean | true/false | false |  否 |

### FocusGroupDirective：
代表某个区域的一组焦点，group支持嵌套
```typescript
@Directive({
    selector: '[snSection], [snGroup], [nav-group], [groupwillblur], [groupblur], [groupwillfocus], [groupfocus], [groupmovefailed]'
})
export class FocusGroupDirective extends FocusBase implements AfterViewInit, OnDestroy, OnInit {

    @Input('snGroup')
    set config(_cfg: FocusConfig) {
        this.originConfig = _cfg;
        this._config = this.mergeConfig(this.parent && this.parent.config, _cfg);
    }

    get config() {
        return this._config;
    }

    @Output('groupwillblur')
    groupWillBlur = new EventEmitter<CustomEvent>();
    @Output('groupblur')
    groupBlur = new EventEmitter<CustomEvent>();
    @Output('groupwillfocus')
    groupWillFocus = new EventEmitter<CustomEvent>();
    @Output('groupfocus')
    groupFocus = new EventEmitter<CustomEvent>();
    @Output('groupmovefailed')
    groupMoveFailed = new EventEmitter<CustomEvent>();
```
```html
<div snGroup>
    <div snFocus>focusable element 1</div>
    <div snFocus>focusable element 2</div>
    <div snFocus>focusable element 3</div>
</div>
```
#### 属性说明
| 属性   | 说明    | 类型     | 可选值     | 默认值    | 必传     |
|-----   |-----    |-----    |------      |------    |------    |
| snGroup | Group的配置对象，详见下面 FocusConfig 属性说明 | FocusConfig | / | / |  否 |

#### 事件说明
| 事件名   | 说明    | 回调参数     | 类型     |
|-----   |-----    |-----    |------      |
| groupwillblur | 当焦点即将离开此group之前触发 | event | CustomEvent  | 
| groupblur | 当焦点已经离开此group时触发 | event | CustomEvent  | 
| groupwillfocus | 当焦点即将进入此group之前触发 | event | CustomEvent  | 
| groupfocus | 当焦点已经进入此group时触发 | event | CustomEvent  | 
| groupmovefailed | 当焦点在此group里移动失败时触发 | event | CustomEvent  | 

#### FocusDirective（[snFocus]） 和 FocusGroupDirective（[snGroup]）接收一个配置对象（FocusConfig），并且在dom层次结构上相互继承
##### FocusConfig 属性说明
| 属性   | 说明    | 类型     | 可选值     | 默认值    | 必传     |
|-----   |-----    |-----    |------      |------    |------    |
| straightOnly | 只寻找正对的方向（将当前焦点所在盒子的4条边延长，除焦点所在盒子自身外，有8个方向：左上方、左方、左下等，当此选项为true时，如果按LEFT_KEY，将只考虑中心落在正左方的候选焦点，否则，中心落在左上、左、左下的焦点都会成为候选焦点；其他方向同理） | boolean | trur/false | false |  否 |
| straightLtr | 同上，只针对左右 | boolean | true/false | false |  否 |
| straightUtd | 同上，只针对上下 | boolean | true/false | false |  否 |
| straightOverlapThreshold | 当straightOnly为true时，左上、左下与当前焦点在水平方向上的交集占当前焦点的比率大于等于此参数，则认为是落在正左方，从而成为候选焦点 | number | [0-1] | 0.5 |  否 |
| rememberSource | 是否记住前一个焦点，比如，从焦点A按RIGHT_KEY到达焦点B，若rememberSource为true，则从焦点B按LEFT_KEY回到焦点A | boolean | true/false | false |  否 |
| disabled | 焦点是否被禁止 | boolean | true/false | false |  否 |
| enterTo | 进入一个不同的group时，优先选择落在哪个焦点上, 配置成'last-focused&#124;first'， 将首先找历史焦点，如果没有，则落到第一个上 | string | last-focused, first, last, $n, last-focused&#124;first, last-focused&#124;last, last-focused&#124;$n 等| '' |  否 |
| up/down/left/right | 指示对应方向的移动，若为false，则对应方向上不可移动，若为字符串（选择符），则与选择符匹配的焦点作为候选焦点 | boolean&#124;string | flase/selector | '' |  否 |
| restrict | 限制焦点的移动范围 | string | self-first, self-only | self-first |  否 |
| autoFocus | 页面初始化后，是否自动落焦, 目前只适用于焦点的配置 | boolean | true/false | false |  否 |
| focusedClass | 落焦的元素所添加的css类 | string | / | focused |  否 |
| groupFocusedClass | 落焦的group所添加的css类 | string | / | group-focused |  否 |
| cycle | 一个group里面的焦点是否支持循环（上下与左右） | boolean | true/false | false |  否 |
| cache | 焦点的位置与尺寸信息是否要缓存 | boolean | true/false | true |  否 |
| order | 适用于同级的group，指定焦点在group之间的移动顺序 | number | / | 0 |  否 |
| autoFocusAfterDisappear | 当前焦点销毁会是否需要自动落焦 | boolean | true/false | true |  否 |

### XKeydownDirective：
需要响应按键事件的组件需要指定(xkeydown)指令, 一般注册在组件的根节点上
```typescript
@Directive({
    selector: '[xkeydown], [navSkip]'
})
export class XKeydownDirective extends EventTreeBase implements AfterViewInit, OnDestroy {

    @Input()
    isInTemplate = false;

    @Input()
    navSkip;

    @Output('xkeydown')
    xkeydown = new EventEmitter<CustomEvent>();
```
```html
<div class="some-component" (xkeydown)="onXKeydown($event)">
    <div snGroup>
        <div snFocus>focusable element 1</div>
        <div snFocus>focusable element 2</div>
        <div snFocus>focusable element 3</div>
    </div>
</div>
```
#### 属性说明
| 属性   | 说明    | 类型     | 可选值     | 默认值    | 必传     |
|-----   |-----    |-----    |------      |------    |------    |
| isInTemplate | 当焦点存在于嵌入模板（ng-content、ng-template）时，为了查找实际生成的dom层次结构中的父xkeydown时，需要指定此参数为true | boolean | true/false | false |  否 |
| navSkip | 当接收到方向键（上、下、左、右）移动焦点时，跳过此指令包含的焦点 | string | UP_KEY/DOWN_KEY/LEFT_KEY/RIGHT_KEY | / |  否 |

#### 事件说明
| 事件名   | 说明    | 回调参数     | 类型     |
|-----   |-----    |-----    |------      |
| xkeydown | 当有按键事件（遥控器或者键盘）时触发，将事件传递给组件注册的回调 | event | CustomEvent  |

### XFocusDirective：
负责焦点相关事件的传递
```typescript
@Directive({
    selector: '[xwillfocus], [xfocus], [xwillblur], [xblur], [xmovefailed]'
})
export class XFocusDirective extends EventTreeBase implements AfterViewInit, OnDestroy {

    @Input()
    isInTemplate = false;

    @Output('xwillfocus')
    xwillfocus = new EventEmitter<CustomEvent>();

    @Output('xfocus')
    xfocus = new EventEmitter<CustomEvent>();

    @Output('xwillblur')
    xwillblur = new EventEmitter<CustomEvent>();

    @Output('xblur')
    xblur = new EventEmitter<CustomEvent>();

    @Output('xmovefailed')
    xmovefailed = new EventEmitter<CustomEvent>();

    @Output('xfocusdisappear')
    xfocusdisappear = new EventEmitter<CustomEvent>();
```
```html
<div class="some-component" (xkeydown)="onXKeydown($event)">
    <div snGroup>
        <div (xfocus)="onXFocus($event)">
            <div snFocus>focusable element 1</div>
            <div snFocus>focusable element 2</div>
            <div snFocus>focusable element 3</div>
        </div>
    </div>
</div>
```
#### 属性说明
| 属性   | 说明    | 类型     | 可选值     | 默认值    | 必传     |
|-----   |-----    |-----    |------      |------    |------    |
| isInTemplate | 当焦点存在于嵌入模板（ng-content、ng-template）时，为了查找实际生成的dom层次结构中的父XFocusDirective时，需要指定此参数为true | boolean | true/false | false |  否 |

#### 事件说明
| 事件名   | 说明    | 回调参数     | 类型     |
|-----   |-----    |-----    |------      |
| xwillfocus | 当焦点即将进入此区域时触发，停止事件将取消焦点的切换 | event | CustomEvent  |
| xfocus | 当焦点已经进入此区域时触发 | event | CustomEvent  |
| xwillblur | 当焦点即将离开此区域时触发， 停止事件将取消焦点的切换 | event | CustomEvent  |
| xblur | 当焦点已经离开此区域时触发 | event | CustomEvent  |
| xmovefailed | 当焦点在此区域移动失败时触发 | event | CustomEvent  |
| xfocusdisappear | 当焦点在此区域消失时触发 | event | CustomEvent  |
