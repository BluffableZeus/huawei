﻿### Usage
```typescript
import {HWInputComponent} from 'ng-epg-ui/components/input';
```

```html
<ui-hw-input isActive="true" [inputName]="'search-inut'" maxLength="1024" maxWidth="1000" (change)="onChange($event)"></ui-hw-input>
```

### Attributes
| Attribute   | Description   | Type     | Options     | Default Value    | Mandatory (M) or Optional (O)    |
|-----   |-----    |-----    |------      |------    |------    |
| placeHolder | Placeholder of the input box | string |  | '' |  O |
| maxLength | Maximum number of characters that can be entered | number |  | 2048  |  O |
| visibleCount | Number of visible characters | number |  | 30 |  O |
| type | Input box type | string | text and password | text |  O |
| isActive | Indicates whether the input box is activated | boolean |  | false |  O |
| value | Has inputed chars | string |  |  |  O |
| inputName | Input box name | string |  |  |  O |
| caretPosition | Cursor position | number |  | 0 |  O |
| iconName | Icon name | string |  | '' |  M |
| isIconRight | Icon position | boolean |  | false |  O |
| hideCaret | Whether hide cart | boolean |  | true |  O |
| canMoveCaretPosition | Whether can move cart position | boolean |  | false |  O |
| isTextInput | Whether is text input | boolean |  | false |  O |
| maxWidth | Input component's max width | number |  | 260 |  O |

### Events
| Event   | Description    | Callback Parameter     | Type     |
|-----   |-----    |-----    |------      |
| change | event triggered when the input box changes | entered characters | string |
| onCaretPositionChange |  event triggered when input char or move cart position | cart position | number  | 
| isToastExceedLength | whether toast when input characters exceed maxlength | current value | boolean | 
