import * as _ from 'underscore'
import {
    Directive, Optional, ElementRef, Input, Output, EventEmitter, NgZone, Renderer2,
    SkipSelf, AfterViewInit, OnDestroy, OnInit
} from '@angular/core'

import { FocusBase, FocusConfig, DefaultFocusConfig } from './focus-base'
import { FocusController } from './focus-controller'
import { CustomEvent } from '../event-tree'
import { FocusDirective } from './focus.directive'

@Directive({
  selector: '[snSection], [snGroup], [nav-group], [groupwillblur], [groupblur], [groupwillfocus], [groupfocus], [groupmovefailed]'
})
export class FocusGroupDirective extends FocusBase implements AfterViewInit, OnDestroy, OnInit {

  @Input('snGroup')
    set config (_cfg: FocusConfig) {
      this.originConfig = _cfg
      this._config = this.mergeConfig(this.parent && this.parent.config, _cfg)
    }

  get config () {
      return this._config
    }

  @Output('groupwillblur')
    groupWillBlur = new EventEmitter<CustomEvent>()
  @Output('groupblur')
    groupBlur = new EventEmitter<CustomEvent>()
  @Output('groupwillfocus')
    groupWillFocus = new EventEmitter<CustomEvent>()
  @Output('groupfocus')
    groupFocus = new EventEmitter<CustomEvent>()
  @Output('groupmovefailed')
    groupMoveFailed = new EventEmitter<CustomEvent>()

  private _config = DefaultFocusConfig
  private originConfig: FocusConfig
  private _isFocused = false

  focusList: Array<FocusDirective> = []

  children: Array<FocusGroupDirective> = []
  el: HTMLElement

  currentFocus: FocusDirective
  lastFocus: FocusDirective

  constructor (
        @Optional() @SkipSelf() public parent: FocusGroupDirective,
        public focusController: FocusController,
        private eleRef: ElementRef,
        private ngZone: NgZone,
        public renderer2: Renderer2
    ) {
      super(eleRef.nativeElement)
      this.el = eleRef.nativeElement
      this.focusController.addGroup(this)
    }

  ngOnInit () {
      if (this.parent) {
          this._config = this.mergeConfig(this.parent.config, this.originConfig)
        }
    }

  ngAfterViewInit () {
      if (this.parent) {
          this.parent.addGroup(this)
        }
    }

  add (focus: FocusDirective) {
      this.focusList.push(focus)
      if (this.parent) {
          this.parent.add(focus)
        }
    }

  addGroup (group: FocusGroupDirective) {
      this.children.push(group)
      if (group.config.order) {
          this.children.sort((focus1, focus2) => focus1.config.order - focus2.config.order)
        }
    }

  removeGroup (group: FocusGroupDirective) {
      this.children = _.without(this.children, group)
    }

  remove (focus: FocusDirective) {
      const index = this.focusList.indexOf(focus)
      if (index >= 0) {
          this.focusList.splice(index, 1)
        }
      if (this.parent) {
          this.parent.remove(focus)
        }
    }

  willFocus (direction: string, oldFocus?: FocusDirective): boolean {
      const willFocusEvent = new CustomEvent(direction)
      this.groupWillFocus.emit(willFocusEvent)
      if (willFocusEvent.isStop()) {
          return false
        } else if (this.parent && (!oldFocus || this.parent.focusList.indexOf(oldFocus) === -1)) {
          return this.parent.willFocus(direction, oldFocus)
        }

      return true
    }

  focus (direction: string, focus: FocusDirective) {
      this._isFocused = true
      this.currentFocus = focus
      this.addClass(this.config.groupFocusedClass)
      if (this.parent) {
          this.parent.focus(direction, focus)
        }
    }

  focused (direction: string, oldFocus?: FocusDirective): void {
      const focusedEvent = new CustomEvent(direction, { canStop: false })
      this.groupFocus.emit(focusedEvent)
      if (this.parent && (!oldFocus || this.parent.focusList.indexOf(oldFocus) === -1)) {
          this.parent.focused(direction, oldFocus)
        }
    }

  willBlur (direction: string, newFocus?: FocusDirective): boolean {
      const willBlur = new CustomEvent(direction)
      this.groupWillBlur.emit(willBlur)

      if (willBlur.isStop()) {
          return false
        } else if (this.parent && (!newFocus || this.parent.focusList.indexOf(newFocus) === -1)) {
          return this.parent.willBlur(direction, newFocus)
        }

      return true
    }

  blur (direction: string, focus: FocusDirective) {
      this._isFocused = false
      this.lastFocus = focus
      this.removeClass(this.config.groupFocusedClass)
      if (this.parent) {
          this.parent.blur(direction, focus)
        }
    }

  blured (direction: string, newFocus?: FocusDirective): void {

      const blurEvent = new CustomEvent(direction, { canStop: false })
      this.groupBlur.emit(blurEvent)

      if (this.parent && (!newFocus || this.parent.focusList.indexOf(newFocus) === -1)) {
          this.parent.blured(direction, newFocus)
        }
    }

  moveFailed (direction: string): void {
      const moveFailedEvent = new CustomEvent(direction, { canStop: false })
      this.groupMoveFailed.emit(moveFailedEvent)
      if (this.parent) {
          this.parent.moveFailed(direction)
        }
    }

  isFocus () {
      return false
    }

  isFocused () {
      return this._isFocused
    }

  ngOnDestroy () {
      this.focusController.removeGroup(this)
      if (this.parent) {
          this.parent.removeGroup(this)
        }
    }
}
