import * as _ from 'underscore'
import {
    Directive, AfterViewInit, ElementRef, HostBinding, Input, OnDestroy, Renderer2, Optional
} from '@angular/core'

import { FocusBase, FocusConfig, DefaultFocusConfig } from './focus-base'
import { FocusGroupDirective } from './focus-group.directive'
import { FocusController } from './focus-controller'
import { XKeydownDirective, XFocusDirective, CustomEvent } from '../event-tree'

@Directive({
  selector: '[snFocus],ui-button,[focusable]'
})
export class FocusDirective extends FocusBase implements AfterViewInit, OnDestroy {

  @Input('snFocus')
    set config (_cfg: FocusConfig) {
      this.originConfig = _cfg
      this._config = this.mergeConfig(this.parent && this.parent.config, _cfg)
    }

  @Input()
    isInTemplate = false

  @Input()
    focusID = _.uniqueId('focus')

  @HostBinding('attr.tabindex')
    tabindex = '-1'

  get config (): FocusConfig {
      return this._config
    }

  private _config = DefaultFocusConfig
  private originConfig: FocusConfig
  private _isFocused = false

  constructor (
        public focusController: FocusController,
        public eleRef: ElementRef,
        public renderer2: Renderer2,
        @Optional() public parent: FocusGroupDirective,
        @Optional() public xKeydown: XKeydownDirective,
        @Optional() public xFocus: XFocusDirective
    ) {
      super(eleRef.nativeElement)
    }

  ngAfterViewInit () {
      this.focusController.addFocus(this)
      if (this.isInTemplate) {
          const focusGroup = this.focusController.findNearestGroup(this.el)
          if (focusGroup) {
              this.parent = focusGroup
              this._config = this.mergeConfig(this.parent && this.parent.config, this.originConfig)
            }

          this.xKeydown = this.focusController.findNearestXKeydown(this.el) || this.xKeydown
          this.xFocus = this.focusController.findNearestXFocus(this.el) || this.xFocus
        }
      if (this.parent) {
          this.parent.add(this)
        }

      if (this._config.autoFocus) {
          this.focusController.setFocus('init', this)
        }
    }

  isFocus () {
      return true
    }

  willFocus (direction: string, oldFocus?: FocusDirective): boolean {
      return this.emitEvent(direction, 'xwillfocus', true, 'willFocus', oldFocus)
    }

  focus (direction: string, oldFocus?: FocusDirective) {

      if (!this.willFocus(direction, oldFocus)) {
          return false
        }
      this.addClass(this.config.focusedClass)
      if (this.parent) {
          this.parent.focus(direction, this)
        }
      this.focused(direction, oldFocus)
      this._isFocused = true

      return true
    }

  focused (direction: string, oldFocus?: FocusDirective): void {
      this.emitEvent(direction, 'xfocus', false, 'focused', oldFocus)
    }

  willBlur (direction: string, newFocus?: FocusDirective): boolean {
      return this.emitEvent(direction, 'xwillblur', true, 'willBlur', newFocus)
    }

  blur (direction: string, newFocus?: FocusDirective) {
      if (!this.willBlur(direction, newFocus)) {
          return false
        }

      this.removeClass(this.config.focusedClass)

      if (this.parent) {
          this.parent.blur(direction, this)
        }

      this.blured(direction, newFocus)
      this._isFocused = false
      return true
    }

  blured (direction: string, newFocus?: FocusDirective): void {
      this.emitEvent(direction, 'xblur', false, 'blured', newFocus)
    }

  moveFailed (direction: string): void {
      const moveFailedEvent = new CustomEvent(direction, { canStop: false })
      if (this.xFocus) {
          this.xFocus.dispatchEvent(moveFailedEvent, 'xmovefailed')
        }
      if (this.parent) {
          this.parent.moveFailed(direction)
        }
    }

  isFocused () {
      return this._isFocused
    }

  add () { throw new Error('not implement') }

  remove () { throw new Error('not implement') }

  isValid () {
      return !this.config.disabled
    }

  ngOnDestroy () {
      if (this.parent) {
          this.parent.remove(this)
        }
      this.focusController.removeFocus(this)
    }

  private emitEvent (direction: string, eventName: string, canStop: boolean, parentMethod: string, focus?: FocusDirective): boolean {
      const customEvent = new CustomEvent(direction, { canStop, target: this.el })
      if (this.xFocus) {
          this.xFocus.dispatchEvent(customEvent, eventName)
        }
      if (customEvent.isStop()) {
          return false
        } else {
          if (this.parent && (!focus || this.parent.focusList.indexOf(focus) === -1)) {
              return this.parent[parentMethod](direction, focus)
            }
        }

      return true
    }
}

XKeydownDirective.setFocusDirectiveClass(FocusDirective)
XFocusDirective.setFocusDirectiveClass(FocusDirective)
