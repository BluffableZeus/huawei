import { NgModule } from '@angular/core'

import { FocusDirective } from './focus.directive'
import { FocusGroupDirective } from './focus-group.directive'
import { EventTreeModule } from '../event-tree'

@NgModule({
  imports: [
      EventTreeModule
    ],
  exports: [
      FocusDirective,
      FocusGroupDirective,
      EventTreeModule
    ],
  declarations: [
      FocusDirective,
      FocusGroupDirective
    ]
})
export class FocusControllerModule { }
