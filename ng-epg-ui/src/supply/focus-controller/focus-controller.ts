import * as _ from 'underscore'
import { NgZone } from '@angular/core'

import { DIRECTIONS_REVERSE, DefaultFocusConfig } from './focus-base'
import { FocusDirective } from './focus.directive'
import { FocusGroupDirective } from './focus-group.directive'
import { SpatialNavigation } from './spatial-navigation'
import { CustomEvent, EventTreeBase, XKeydownDirective, XFocusDirective } from '../event-tree'
import { once } from '../util/util'

const keyMap = {
  'UP_KEY': 'up',
  'DOWN_KEY': 'down',
  'LEFT_KEY': 'left',
  'BACK_KEY': 'left',
  'RIGHT_KEY': 'right'
}

const cycleMap = {
  'up': 'utd',
  'down': 'utd',
  'left': 'ltr',
  'right': 'ltr'
}

export class FocusController {

  focusList: Array<FocusDirective> = []
  groupList: Array<FocusGroupDirective> = []
  config = DefaultFocusConfig
  parent = null

  currentFocus: FocusDirective
  lastFocus: FocusDirective

  private xKeydownList: Array<XKeydownDirective> = []
  private xFocusList: Array<XFocusDirective> = []
  private isScheduleAutoFocus = false
  private isRefocusAfterDisappear = false
  private disappearFocusInfo: {
      center: { left: number, top: number },
      parent: FocusGroupDirective,
      focus: FocusDirective
    }
  private destroyed = false

  constructor (private ngZone: NgZone) { }

  addFocus (focus: FocusDirective) {
      this.focusList.push(focus)

      if (!this.currentFocus) {
          if (!this.isScheduleAutoFocus) {
              this.isScheduleAutoFocus = true
              this.scheduleToFocus()
            } else if (this.isRefocusAfterDisappear &&
                this.disappearFocusInfo.focus.config.autoFocusAfterDisappear) {
              once(this.ngZone.onStable, () => {
                  this.refocusAfterDisappear(this.disappearFocusInfo.focus.config.focusTag)
                })
              this.isRefocusAfterDisappear = false
            }
        }
    }

  removeFocus (focus: FocusDirective) {

      let index = this.focusList.indexOf(focus)
      if (index >= 0) {
          this.focusList.splice(index, 1)
        }

      if (this.currentFocus === focus) {
          if (focus.xFocusDirective) {
              focus.xFocusDirective.dispatchEvent(new CustomEvent('xfocusdisappear'), 'xfocusdisappear')
            }
          this.currentFocus = null
          this.isRefocusAfterDisappear = true
        }
    }

  addGroup (group: FocusGroupDirective) {
      this.groupList.push(group)
    }

  removeGroup (group: FocusGroupDirective) {
      this.groupList = _.without(this.groupList, group)
    }

  setFocus (direction: string, newFocus: FocusDirective, oldFocus = this.currentFocus, isForce = false) {
      if (this.currentFocus === newFocus || (newFocus && newFocus.config.disabled && !isForce)) {
          return false
        }
      if (oldFocus) {
          if (oldFocus.blur(direction, newFocus)) {
              if (newFocus && newFocus.focus(direction, oldFocus)) {
                  this.lastFocus = this.currentFocus
                  this.setNewFocus(newFocus)
                  return true
                }
            }
        } else {
          if (newFocus && newFocus.focus(direction, oldFocus)) {
              this.lastFocus = this.currentFocus
              this.setNewFocus(newFocus)
              return true
            }
        }

      return false
    }

  addFocusClass () {
      if (this.currentFocus) {
          this.currentFocus.addClass(this.config.focusedClass)
        }
    }

  removeFocusClass () {
      if (this.currentFocus) {
          this.currentFocus.removeClass(this.config.focusedClass)
        }
    }

  private setNewFocus (focus: FocusDirective) {
      this.currentFocus = focus
      const clientRect = focus.getBoundingClientRect()
      this.disappearFocusInfo = {
          center: {
              left: clientRect.left + clientRect.width / 2,
              top: clientRect.top + clientRect.height / 2
            },
          parent: focus.parent,
          focus: focus
        }
    }

  initFocus (selector?: string) {
      let focusable: FocusDirective
      if (selector) {
          focusable = _.find(this.focusList, focus => focus.getElement().matches(selector))
        }
      if (!focusable) {
          focusable = _.find(this.focusList, (focus: FocusDirective) => !focus.config.disabled)
        }
      if (focusable) {
          this.setFocus('init', focusable)
        }
    }

  getFocus (): FocusDirective {
      return this.currentFocus
    }

  getFocusEl () {
      if (this.currentFocus) {
          return this.currentFocus.getElement()
        }
    }

  forceFocus (selector: HTMLElement | string) {
      if (!selector) {
          return false
        }
      const target = _.find(this.focusList, (focus: FocusDirective) => {
          if (_.isString(selector)) {
              return focus.getElement().matches(selector as string)
            }
          return focus.getElement() === selector
        })
      return this.setFocus('init', target, this.currentFocus, true)
    }

  onKeyDown (event: { type: string, keyCode: number, metaKey?: boolean }): CustomEvent {
      const { type, keyCode, metaKey } = event
      const customEvent = new CustomEvent(type, {
          keyCode, metaKey,
          target: this.currentFocus && this.currentFocus.getElement()
        })

      let startXKeydown

      if (this.currentFocus) {
          startXKeydown = this.currentFocus.xKeydown
        } else if (this.focusList.length === 0) {
          startXKeydown = _.last(this.xKeydownList)
        }

      if (startXKeydown) {
          startXKeydown.dispatchEvent(customEvent)
        }

      if (!customEvent.isStop() && _.contains(['LEFT_KEY', 'UP_KEY', 'RIGHT_KEY', 'DOWN_KEY'], type)) {
          const moveResult = this.move(keyMap[type])
          if (moveResult) {
              customEvent.stop('auto focus move!')
              return customEvent
            }
        }

      if (!customEvent.isStop() && 'OK_KEY' === type && this.currentFocus) {
          this.currentFocus.getElement().click()
        }

      return customEvent
    }

  move (direction: string) {
      if (!this.currentFocus) {
          return false
        }

      let next = this.findNext(direction)
      if (!next) {
          this.currentFocus.moveFailed(direction)
        } else {
          this.setFocus(direction, next)
        }
      return true
    }

  moveSkip (excludeEl: HTMLElement, direction = 'LEFT_KEY') {
      const focusCandidates = _.reject(this.focusList,
            (focus: FocusDirective) => excludeEl.contains(focus.getElement()))
      direction = keyMap[direction]
      const nextFocus = SpatialNavigation.navigate(this.currentFocus, direction,
            focusCandidates, this.config)
      if (nextFocus) {
          this.setFocus(direction, this.getEnterTo(nextFocus, direction))
        }
    }

  moveByDirect (direction: string) {
      return this.move(direction)
    }

  addXKeydown (xKeydown: XKeydownDirective) {
      this.xKeydownList.push(xKeydown)
    }

  removeXKeydown (xKeydown: XKeydownDirective) {
      this.xKeydownList = _.without(this.xKeydownList, xKeydown)
    }

  addXFocus (xKeydown: XFocusDirective) {
      this.xFocusList.push(xKeydown)
    }

  removeXFocus (xKeydown: XFocusDirective) {
      this.xFocusList = _.without(this.xFocusList, xKeydown)
    }

  clearCache () {
      _.each(this.focusList, (focus: FocusDirective) => {
          focus.clearBoundingClientRect()
          let parent = focus.parent
          while (parent) {
              parent.clearBoundingClientRect()
              parent = parent.parent
            }
        })
    }

  findNearestGroup (el: HTMLElement): FocusGroupDirective {
      return this.findNearestDirective(this.groupList, el) as FocusGroupDirective
    }

  findNearestXFocus (el: HTMLElement, containSelf = true): XFocusDirective {
      return this.findNearestDirective(this.xFocusList, el, containSelf) as XFocusDirective
    }

  findNearestXKeydown (el: HTMLElement, containSelf = true): XKeydownDirective {
      return this.findNearestDirective(this.xKeydownList, el, containSelf) as XKeydownDirective
    }

  private findNext (direction: string): FocusDirective {
      let next: FocusDirective
      let currentSection = this.getParent(this.currentFocus)
      const config = this.currentFocus.config
      let selector = config[direction]
      if (selector && _.isString(selector)) {
          const focusCandidates = _.filter(this.exclude(this.focusList, this.currentFocus),
                (focus: FocusDirective) => focus.getElement().matches(selector))
          if (focusCandidates.length === 1) {
              return focusCandidates[0]
            } else {
              return SpatialNavigation.navigate(
                    this.currentFocus,
                    direction,
                    focusCandidates,
                    _.extend({}, config, { previous: currentSection.config.previous })
                )
            }
        } else if (selector === false) {
          return null
        }

      if (config.restrict === 'self-only' || config.restrict === 'self-first') {

          next = SpatialNavigation.navigate(
                this.currentFocus,
                direction,
                this.exclude(currentSection.focusList, this.currentFocus),
                _.extend({}, config, { previous: currentSection.config.previous })
            )

          if (!next) {
              next = this.getCycleNext(direction)
            }

          let parent = this.getParent(currentSection)
          if (!next && config.restrict === 'self-first' && parent) {
              next = this.findNearestRecursive(direction, parent, currentSection)
            }
        } else {
          next = SpatialNavigation.navigate(
                this.currentFocus,
                direction,
                this.exclude(this.focusList, this.currentFocus),
                _.extend({}, config, this.currentFocus.parent && this.currentFocus.parent.config.previous)
            )
        }

      if (next) {
          let nextSection = (next as FocusDirective).parent

          currentSection.config.previous = {
              target: this.currentFocus,
              destination: next,
              reverse: DIRECTIONS_REVERSE[direction]
            }

          next = this.getEnterTo(next, direction)
        }

      return next
    }

  private getCycleNext (direction: string) {
      const innerSection = this.getParent(this.currentFocus)
      if (innerSection && innerSection.config.cycle &&
            innerSection.focusList.length > 1 && innerSection.config.cycle === cycleMap[direction]) {
          if (direction === 'up' || direction === 'left') {
              return _.last(innerSection.focusList)
            } else {
              return innerSection.focusList[0]
            }
        }
      return null
    }

  public getParent (section: FocusDirective | FocusGroupDirective | FocusController)
        : FocusGroupDirective | FocusController {
      if (section as any === this) {
          return null
        }

      if ((section as FocusDirective | FocusGroupDirective).parent) {
          return (section as FocusDirective | FocusGroupDirective).parent
        } else {
          return this
        }
    }

  moveFocusInto (containEl: HTMLElement, direct?: string) {
      if (this.currentFocus && containEl.contains(this.currentFocus.getElement())) {
          return
        }

      let nextFocus
      const targetList = _.filter(this.focusList, (focus: FocusDirective) => containEl.contains(focus.getElement()))
      if (this.currentFocus && direct) {
          nextFocus = SpatialNavigation.navigate(
                this.currentFocus,
                direct,
                targetList,
                _.extend(this.currentFocus.config)
            )
        }

      if (!nextFocus) {
          nextFocus = _.first(targetList)
        }

      if (nextFocus) {
          this.setFocus(direct, nextFocus, this.currentFocus)
        }
    }

  destroy () {
      this.destroyed = true
    }

  private getEnterTo (toFocus: FocusDirective, direction: string) {
      const nextSection = this.getParent(toFocus)
      const currentSection = this.getParent(this.currentFocus)
      let enterToFocus
      let enterTo

      if (nextSection && nextSection.config.enterTo && currentSection as any !== nextSection &&
            nextSection.focusList.indexOf(this.currentFocus) === -1) {
          let enterToConfig = nextSection.config.enterTo
          const index = enterToConfig.indexOf(':')
          if (index >= 0) {
              const enterDirection = enterToConfig.split(':')[0]
              if (enterDirection && enterDirection !== direction) {
                  return toFocus
                }
              enterToConfig = enterToConfig.split(':')[1]
            }
          const enterToArray = enterToConfig.split('|')
          for (let i = 0; i < enterToArray.length; i++) {
              enterTo = enterToArray[i]
              if (enterTo.indexOf('$') === 0) {
                  const index = parseInt(enterTo.slice(1), 10)
                  enterToFocus = nextSection.focusList[index]
                } else {
                  switch (enterTo) {
                      case 'last-focused':
                        enterToFocus = this.getLastFocus(nextSection, nextSection.lastFocus)
                        break
                      case 'first':
                        enterToFocus =
                                _.find(nextSection.focusList, (focus: FocusDirective) => !focus.config.disabled)
                        break
                      case 'last':
                        let lastIndex =
                                _.findLastIndex(nextSection.focusList,
                                    (focus: FocusDirective) => !focus.config.disabled)
                        enterToFocus = nextSection.focusList[lastIndex]
                        break
                      default:
                        break
                    }
                }
              if (enterToFocus) {
                  break
                }
            }
        }

      return enterToFocus || toFocus
    }

  private getLastFocus (section: FocusGroupDirective | FocusController = this, lastFocus = this.lastFocus) {
      if (lastFocus) {
          return _.find(section.focusList, focus => focus.focusID === lastFocus.focusID)
        }
    }

  private findNearestRecursive (
        direction: string,
        focusSection: FocusGroupDirective | FocusController,
        innerSection: FocusGroupDirective | FocusController
    ) {

      let next = null
      while (focusSection && focusSection.focusList.length && innerSection.config[direction] !== false) {
          if (focusSection.focusList.length === innerSection.focusList.length) {
              innerSection = focusSection
              focusSection = this.getParent(focusSection as any)
              continue
            }
          let focusCandidates = []
          let selector = innerSection.config[direction]
          if (innerSection.config.order) {
              let children = (focusSection as FocusGroupDirective).children ||
                    (focusSection as FocusController).groupList
              let index = children.indexOf(innerSection as FocusGroupDirective)
              if (direction === 'down' || direction === 'right') {
                  index++
                } else {
                  index--
                }
              if (index >= 0 && index < children.length) {
                  focusCandidates = children[index].focusList
                }
            } else if (selector && _.isString(selector)) {
              focusCandidates = _.filter(this.exclude(focusSection.focusList, innerSection.focusList),
                    (focus: FocusDirective) => focus.getElement().matches(selector))
            } else {
              focusCandidates = this.exclude(focusSection.focusList, innerSection.focusList)
            }

          if (focusCandidates.length === 0) {
              focusCandidates = this.exclude(focusSection.focusList, innerSection.focusList)
            }

          next = SpatialNavigation.navigate(
                this.currentFocus,
                direction,
                focusCandidates,
                _.extend({}, focusSection.config, focusSection.config.previous)
            )
          if (next || focusSection.config.restrict === 'self-only') {
              break
            }
          innerSection = focusSection
          focusSection = this.getParent(focusSection as any)
        }

      return next
    }

  private exclude (sourceList: Array<FocusDirective>,
        excludedList: FocusDirective | Array<FocusDirective>): Array<FocusDirective> {
      if (!_.isArray(excludedList)) {
          excludedList = [excludedList as FocusDirective]
        }
      let result: Array<FocusDirective> = []
      let index, length = sourceList.length
      for (let i = 0; i < length; i++) {
          if (sourceList[i].config.disabled) {
              continue
            }
          index = (excludedList as Array<FocusDirective>).indexOf(sourceList[i])
          if (index === -1) {
              result.push(sourceList[i])
            }
        }
      return result
    }

  private scheduleToFocus () {

      once(this.ngZone.onStable, () => {
          if (!this.currentFocus && !this.isRefocusAfterDisappear) {
              const toFocus = _.find(this.focusList, (focus: FocusDirective) => !focus.config.disabled)
              if (toFocus) {
                  this.setFocus('init', toFocus)
                }
            }
        })
    }

  private refocusAfterDisappear (refocusConfig: string) {
      if (this.currentFocus) {
          return
        }
      let focusList: Array<FocusDirective>
      if (refocusConfig) {
          const group = _.find(this.groupList,
                (group: FocusGroupDirective) => group.config.focusTag === refocusConfig)

          if (group) {
              focusList = group.focusList
            }
        } else if (this.disappearFocusInfo.parent && this.disappearFocusInfo.parent.focusList.length) {
          focusList = this.disappearFocusInfo.parent.focusList
        } else {
          focusList = this.focusList
        }

      const center = this.disappearFocusInfo.center
      let toFocus: FocusDirective = _.find(focusList, (focus: FocusDirective) => {
          const rect = focus.getBoundingClientRect()
          return rect.left < center.left && rect.right > center.left &&
                rect.top < center.top && rect.bottom > center.top
        })

      if (!toFocus) {
          toFocus = _.last(focusList)
        }

      if (toFocus) {
          this.setFocus('init', toFocus)
        } else if (refocusConfig && !this.destroyed) {
          once(this.ngZone.onStable, () => {
              this.refocusAfterDisappear(refocusConfig)
            })
        }
    }

  private findNearestDirective (
        list: Array<EventTreeBase | FocusGroupDirective>,
        el: HTMLElement, isSameNode = false
    ): EventTreeBase | FocusGroupDirective {

      let result: EventTreeBase | FocusGroupDirective

      let children
      while (list.length) {
          for (let i = list.length - 1; i >= 0; i--) {
              if ((isSameNode || list[i].el !== el) && list[i].el.contains(el)) {
                  result = list[i]
                  children = result.children
                  break
                }
            }
          if (children) {
              list = children
              children = null
            } else {
              list = []
            }
        }
      return result
    }

}
