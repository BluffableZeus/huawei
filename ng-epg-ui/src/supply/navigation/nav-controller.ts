import { Logger } from '../util/logger'
import { EventService } from '../../../demos-app/sdk/event.service'
import {
  ComponentRef, ComponentFactoryResolver, ElementRef, EventEmitter, NgZone,
  ReflectiveInjector, Renderer, ViewContainerRef
} from '@angular/core'

import { AnimationOptions } from '../animations/animation'
import { App } from '../../components/app/app'
import { Config } from '../config/config'
import {
  convertToView, convertToViews, NavOptions, DIRECTION_BACK, DIRECTION_FORWARD, INIT_ZINDEX,
  TransitionResolveFn, TransitionInstruction, ViewState
} from './nav-util'
import { setZIndex } from './nav-util'
import { DeepLinker } from './deep-linker'
import { DomController } from '../platform/dom-controller'
import { isBlank, isNumber, isPresent, assert, removeArrayItem } from '../util/util'
import { ViewController } from './view-controller'
import { Ion } from '../../components/ion'
import { NavParams } from './nav-params'
import { Platform } from '../platform/platform'
import { Transition } from '../transitions/transition'
import { TransitionController } from '../transitions/transition-controller'
import * as _ from 'underscore'
import { FocusController } from '../focus-controller'

const log = new Logger('nav-controller')

let ctrlIds = -1

/**
 * @private
 * This class is for internal use only. It is not exported publicly.
 */
export class NavController extends Ion {

  public viewDidLoad: EventEmitter<any> = new EventEmitter()
  public viewWillEnter: EventEmitter<any> = new EventEmitter()
  public viewDidEnter: EventEmitter<any> = new EventEmitter()
  public viewWillLeave: EventEmitter<any> = new EventEmitter()
  public viewDidLeave: EventEmitter<any> = new EventEmitter()
  public viewWillUnload: EventEmitter<any> = new EventEmitter()
  public viewDidChanged = new EventEmitter<{ enteringView: ViewController, leavingView: ViewController }>()
  public viewWillChanged = new EventEmitter<{ enteringView: ViewController, leavingView: ViewController }>()

  public zIndexOffset = 0
  public trnsId: number = null
  public isPortal: boolean

  protected init = false

  private children: any[] = []
  private ids: number = -1
  private queue: TransitionInstruction[] = []
  private trnsTm = false
  private viewport: ViewContainerRef
  private views: ViewController[] = []

  private id: string

  public constructor (
    public parent: any,
    public app: App,
    public config: Config,
    public plt: Platform,
    elementRef: ElementRef,
    public zone: NgZone,
    renderer: Renderer,
    public _cfr: ComponentFactoryResolver,
    public _trnsCtrl: TransitionController,
    public _linker: DeepLinker,
    private _domCtrl: DomController
  ) {
    super(config, elementRef, renderer)

    this.id = 'n' + (++ctrlIds)

    EventService.onAny((event: string, ...values: any[]) => {
      const promises = this.getViews().map((view: ViewController) => {
        if (view.instance && view.instance.onNotification) {
          return view.instance.onNotification(event, ...values)
        }
      })

      return Promise.all(promises)
    })
  }

  public push (page: any, params?: any, opts?: NavOptions, done?: Function): Promise<any> {
    const view = convertToView(this._linker, page, params)
    const promise = this._queueTrns({
      insertStart: -1,
      insertViews: [view],
      opts: opts
    }, done)

    if (promise) {
      return promise.then(() => {
        return view
      })
    }
  }

  public insert (insertIndex: number, page: any, params?: any, opts?: NavOptions, done?: Function): Promise<any> {
    const view = convertToView(this._linker, page, params)
    const promise = this._queueTrns({
      insertStart: insertIndex,
      insertViews: [convertToView(this._linker, page, params)],
      opts: opts
    }, done)

    if (promise) {
      return promise.then(() => {
        return view
      })
    }
  }

  public insertPages (insertIndex: number, insertPages: any[], opts?: NavOptions, done?: Function): Promise<any> {
    return this._queueTrns({
      insertStart: insertIndex,
      insertViews: convertToViews(this._linker, insertPages),
      opts: opts
    }, done)
  }

  public pop (opts?: NavOptions, done?: Function): Promise<any> {
    const promise = this._queueTrns({
      removeStart: -1,
      removeCount: 1,
      opts: opts
    }, done)

    if (promise) {
      promise.catch(() => {
        log.debug('pop cancelled')
      })
    }
    return promise
  }

  public popTo (indexOrViewCtrl: any, opts?: NavOptions, done?: Function): Promise<any> {
    let config: TransitionInstruction = {
      removeStart: -1,
      removeCount: -1,
      opts: opts
    }

    config.removeStart = this.indexOf(indexOrViewCtrl) + 1
    return this._queueTrns(config, done)
  }

  public popToAndPush (indexOrViewCtrls: Array<any>, page: any, params?: any, opts?: NavOptions): Promise<any> {
    return this.push(page, params, opts).then((pushResp) => {
      let removeStart = -1

      _.find(indexOrViewCtrls, (indexOrViewCtrl) => {
        if (isNumber(indexOrViewCtrl)) {
          removeStart = indexOrViewCtrl + 1
        } else {
          const foundIndex = this.indexOf(indexOrViewCtrl)
          if (foundIndex >= 0) {
            removeStart = foundIndex + 1
          }
        }

        return -1 !== removeStart
      })

      if (removeStart >= 0) {
        return this.remove(removeStart, this.length() - removeStart - 1).then(() => {
          return pushResp
        })
      }
      return pushResp
    })
  }

  public popToRoot (opts?: NavOptions, done?: Function): Promise<any> {
    return this._queueTrns({
      removeStart: 1,
      removeCount: -1,
      opts: opts
    }, done)
  }

  public popAll (): Promise<any[]> {

    const views = _.reject(this.views, (view: ViewController) => {
      return false === view.data.dismissOnPageChange
    })

    const promises = views.map((view: ViewController) => {
      view.dismiss('popAll')
    })

    return Promise.all(promises)
  }

  public remove (startIndex: number, removeCount = 1, opts?: NavOptions, done?: Function): Promise<any> {
    return this._queueTrns({
      removeStart: startIndex,
      removeCount: removeCount,
      opts: opts
    }, done)
  }

  public removeView (viewController: ViewController, opts?: NavOptions, done?: Function): Promise<any> {
    return this._queueTrns({
      removeView: viewController,
      removeStart: 0,
      removeCount: 1,
      opts: opts
    }, done)
  }

  public setRoot (pageOrViewCtrl: any, params?: any, opts?: NavOptions, done?: Function): Promise<any> {
    const view = convertToView(this._linker, pageOrViewCtrl, params)
    const promise = this._setPages([view], opts, done)

    if (promise) {
      return promise.then(() => {
        return view
      })
    }
  }

  public setPages (pages: any[], opts?: NavOptions, done?: Function): Promise<any> {
    const viewControllers = convertToViews(this._linker, pages)
    return this._setPages(viewControllers, opts, done)
  }

  public _setPages (viewControllers: ViewController[], opts?: NavOptions, done?: Function): Promise<any> {
    if (isBlank(opts)) {
      opts = {}
    }
    // if animation wasn't set to true then default it to NOT animate
    if (opts.animate !== true) {
      opts.animate = false
    }
    return this._queueTrns({
      insertStart: 0,
      insertViews: viewControllers,
      removeStart: 0,
      removeCount: -1,
      opts: opts
    }, done)
  }

  public _queueTrns (ti: TransitionInstruction, done: Function): Promise<any> {
    let promise: Promise<any>
    let resolve: Function = done
    let reject: Function = done

    ti.opts = ti.opts || {}
    ti.opts.updateUrl = false

    if (done === undefined) {
      // only create a promise if a done callback wasn't provided
      // done can be a null, which avoids any functions
      promise = new Promise((res, rej) => {
        resolve = res
        reject = rej
      })
    }

    ti.resolve = (hasCompleted: boolean, isAsync: boolean, enteringName: string, leavingName: string, direction: string) => {
      // transition has successfully resolved
      this.trnsId = null
      this.init = true
      if (resolve) {
        resolve(hasCompleted, isAsync, enteringName, leavingName, direction)
      }

      // let's see if there's another to kick off
      this.setTransitioning(false)
      this._nextTrns()
    }

    ti.reject = (rejectReason: any, trns: Transition) => {
      log.error(rejectReason)
      // rut row raggy, something rejected this transition
      this.trnsId = null
      this.queue.length = 0

      while (trns) {
        if (trns.enteringView && (trns.enteringView.state !== ViewState.LOADED)) {
          // destroy the entering views and all of their hopes and dreams
          this._destroyView(trns.enteringView)
        }
        if (!trns.parent) {
          break
        }
      }

      if (trns) {
        this._trnsCtrl.destroy(trns.trnsId)
      }

      if (reject) {
        reject(false, false, rejectReason)
      }
      // let's see if there's another to kick off
      this.setTransitioning(false)
      this._nextTrns()
    }

    if (ti.insertViews) {
      // ensure we've got good views to insert
      ti.insertViews = ti.insertViews.filter(v => v !== null)
      if (ti.insertViews.length === 0) {
        ti.reject('invalid views to insert')
        return promise
      }

    } else if (isPresent(ti.removeStart) && this.views.length === 0 && !this.isPortal) {
      ti.reject('no views in the stack to be removed')
      return promise
    }

    this.queue.push(ti)

    // if there isn't a transition already happening
    // then this will kick off this transition
    this._nextTrns()

    // promise is undefined if a done callbacks was provided
    return promise
  }

  public _nextTrns (): boolean {
    // this is the framework's bread 'n butta function
    // only one transition is allowed at any given time
    if (this.isTransitioning()) {
      log.warn('current is transitioning, queue length:%s', this.queue.length)
      if (this.queue.length >= 2) {
        log.error('reset transitioning to false')
        this.setTransitioning(false)
      }
      return false
    }

    // there is no transition happening right now
    // get the next instruction
    const ti = this._nextTI()
    if (!ti) {
      return false
    }

    // Get entering and leaving views
    const leavingView = this.getActive()
    const enteringView = this._getEnteringView(ti, leavingView)

    if (!leavingView && !enteringView) {
      ti.reject('leavingView and enteringView are null. stack is already empty')
      return false
    }

    // set that this nav is actively transitioning
    this.setTransitioning(true)

    // Initialize enteringView
    if (enteringView && isBlank(enteringView.state)) {
      // render the entering view, and all child navs and views
      // ******** DOM WRITE ****************
      try {
        this._viewInit(enteringView)
      } catch (e) {
        ti.reject(`_nextTrns _viewInit() error: ${e.message}`)
        return false
      }
    }

    // Only test canLeave/canEnter if there is transition
    const requiresTransition = (ti.enteringRequiresTransition || ti.leavingRequiresTransition) && enteringView !== leavingView
    if (requiresTransition) {
      // views have been initialized, now let's test
      // to see if the transition is even allowed or not
      return this._viewTest(enteringView, leavingView, ti)

    } else {
      return this._postViewInit(enteringView, leavingView, ti)
    }
  }

  public _nextTI (): TransitionInstruction {
    const ti = this.queue.shift()
    if (!ti) {
      return null
    }
    const viewsLength = this.views.length

    if (isPresent(ti.removeView)) {
      assert(isPresent(ti.removeStart), 'removeView needs removeStart')
      assert(isPresent(ti.removeCount), 'removeView needs removeCount')

      const index = this.views.indexOf(ti.removeView)
      if (index >= 0) {
        ti.removeStart += index
      }
    }
    if (isPresent(ti.removeStart)) {
      if (ti.removeStart < 0) {
        ti.removeStart = (viewsLength - 1)
      }
      if (ti.removeCount < 0) {
        ti.removeCount = (viewsLength - ti.removeStart)
      }
      ti.leavingRequiresTransition = ((ti.removeStart + ti.removeCount) === viewsLength)
    }

    if (ti.insertViews) {
      // allow -1 to be passed in to auto push it on the end
      // and clean up the index if it's larger then the size of the stack
      if (ti.insertStart < 0 || ti.insertStart > viewsLength) {
        ti.insertStart = viewsLength
      }
      ti.enteringRequiresTransition = (ti.insertStart === viewsLength)
    }
    return ti
  }

  public _getEnteringView (ti: TransitionInstruction, leavingView: ViewController): ViewController {
    const insertViews = ti.insertViews
    if (insertViews) {
      // grab the very last view of the views to be inserted
      // and initialize it as the new entering view
      return insertViews[insertViews.length - 1]
    }

    const removeStart = ti.removeStart
    if (isPresent(removeStart)) {
      const views = this.views
      const removeEnd = removeStart + ti.removeCount
      for (let i = views.length - 1; i >= 0; i--) {
        const view = views[i]
        if ((i < removeStart || i >= removeEnd) && view !== leavingView) {
          return view
        }
      }
    }
    return null
  }

  // tslint:disable:cyclomatic-complexity
  public _postViewInit (enteringView: ViewController, leavingView: ViewController, ti: TransitionInstruction) {
    assert(leavingView || enteringView, 'Both leavingView and enteringView are null')
    assert(ti.resolve, 'resolve must be valid')
    assert(ti.reject, 'reject must be valid')

    const opts = ti.opts || {}
    const insertViews = ti.insertViews
    const removeStart = ti.removeStart
    const removeCount = ti.removeCount
    let view: ViewController
    let i: number
    let destroyQueue: ViewController[]

    // there are views to remove
    if (isPresent(removeStart)) {
      assert(removeStart >= 0, 'removeStart can not be negative')
      assert(removeCount >= 0, 'removeCount can not be negative')

      destroyQueue = []
      for (i = 0; i < removeCount; i++) {
        view = this.views[i + removeStart]
        if (view && view !== enteringView && view !== leavingView) {
          destroyQueue.push(view)
        }
      }
      // default the direction to "back"
      opts.direction = opts.direction || DIRECTION_BACK
    }

    const finalBalance = this.views.length + (insertViews ? insertViews.length : 0) - (removeCount ? removeCount : 0)
    assert(finalBalance >= 0, 'final balance can not be negative')
    if (finalBalance === 0 && !this.isPortal) {
      log.warn(`You can't remove all the pages in the navigation stack. nav.pop() is probably called too many times.`)
      ti.reject('navigation stack needs at least one root page')
      return false
    }

    // there are views to insert
    if (insertViews) {
      // manually set the new view's id if an id was passed in the options
      if (isPresent(opts.id)) {
        enteringView.id = opts.id
      }

      // add the views to the
      for (i = 0; i < insertViews.length; i++) {
        view = insertViews[i]
        assert(view, 'view must be non null')
        this._insertViewAt(view, ti.insertStart + i)
      }

      if (ti.enteringRequiresTransition) {
        // default to forward if not already set
        opts.direction = opts.direction || DIRECTION_FORWARD
      }
    }

    // if the views to be removed are in the beginning or middle
    // and there is not a view that needs to visually transition out
    // then just destroy them and don't transition anything
    // batch all of lifecycles together
    // let's make sure, callbacks are zoned
    if (destroyQueue && destroyQueue.length > 0) {
      this.zone.run(() => {
        for (i = 0; i < destroyQueue.length; i++) {
          view = destroyQueue[i]
          this._willLeave(view, true)
          this._didLeave(view)
          this._willUnload(view)
        }
      })

      // once all lifecycle events has been delivered, we can safely detroy the views
      for (i = 0; i < destroyQueue.length; i++) {
        this._destroyView(destroyQueue[i])
      }
    }

    if (ti.enteringRequiresTransition || ti.leavingRequiresTransition && enteringView !== leavingView) {
      // set which animation it should use if it wasn't set yet
      if (!opts.animation) {
        if (isPresent(ti.removeStart)) {
          opts.animation = (leavingView || enteringView).getTransitionName(opts.direction)
        } else {
          opts.animation = (enteringView || leavingView).getTransitionName(opts.direction)
        }
      }

      // huzzah! let us transition these views
      this._transition(enteringView, leavingView, opts, ti.resolve)

    } else {
      // they're inserting/removing the views somewhere in the middle or
      // beginning, so visually nothing needs to animate/transition
      // resolve immediately because there's no animation that's happening
      ti.resolve(true, false)
    }
    return true
  }

  /**
   * DOM WRITE
   */
  public _viewInit (enteringView: ViewController) {
    // entering view has not been initialized yet

    const focusController = new FocusController(this.zone)
    const componentProviders = ReflectiveInjector.resolve([
      { provide: NavController, useValue: this },
      { provide: ViewController, useValue: enteringView },
      { provide: NavParams, useValue: enteringView.getNavParams() },
      { provide: FocusController, useValue: focusController }
    ])
    const componentFactory = this._cfr.resolveComponentFactory(enteringView.component)
    const childInjector = ReflectiveInjector.fromResolvedProviders(componentProviders, this.viewport.parentInjector)
    const componentRef: ComponentRef<any> = componentFactory.create(childInjector, [])

    // focusControllerDirective.setComponentRef(componentRef);

    // create ComponentRef and set it to the entering view
    enteringView.setFocusController(focusController)
    enteringView.init(componentRef)
    enteringView.state = ViewState.INITIALIZED
    this.preLoad(enteringView)
  }

  public clearPortal () {
    this.app.getPortal('modal').popAll()
    this.app.getPortal('toast').popAll()
  }

  public clearDialog () {
    return this.clearPortal()
  }

  public getLeafView (isExcludeDialog = false): ViewController {
    let leafView

    if (!isExcludeDialog) {
      const portalNav = this.app.getPortal('modal')
      leafView = portalNav.getActive()
    }

    return leafView || this.getActive()
  }

  public _viewAttachToDOM (view: ViewController, componentRef: ComponentRef<any>, viewport: ViewContainerRef) {
    assert(view.state === ViewState.INITIALIZED, 'view state must be INITIALIZED')

    // fire willLoad before change detection runs
    this.willLoad(view)

    // render the component ref instance to the DOM
    // ******** DOM WRITE ****************
    viewport.insert(componentRef.hostView, viewport.length)
    view.state = ViewState.PRE_RENDERED

    if (view.cssClass) {
      // the ElementRef of the actual ion-page created
      const pageElement = componentRef.location.nativeElement

      // ******** DOM WRITE ****************
      this._renderer.setElementClass(pageElement, view.cssClass, true)
    }

    componentRef.changeDetectorRef.detectChanges()

    // successfully finished loading the entering view
    // fire off the "didLoad" lifecycle events
    this.zone.run(this.didLoad.bind(this, view))
  }

  public _viewTest (enteringView: ViewController, leavingView: ViewController, ti: TransitionInstruction): boolean {
    const promises: Promise<any>[] = []

    if (leavingView) {
      const leavingTestResult = leavingView.lifecycleTest('Leave')

      if (leavingTestResult === false) {
        // synchronous reject
        ti.reject((leavingTestResult !== false ? leavingTestResult : `onPageCanLeave rejected`))
        return false
      } else if (leavingTestResult instanceof Promise) {
        // async promise
        promises.push(leavingTestResult)
      }
    }

    if (enteringView) {
      const enteringTestResult = enteringView.lifecycleTest('Enter')

      if (enteringTestResult === false) {
        // synchronous reject
        ti.reject((enteringTestResult !== false ? enteringTestResult : `onPageCanEnter rejected`))
        return false
      } else if (enteringTestResult instanceof Promise) {
        // async promise
        promises.push(enteringTestResult)
      }
    }

    if (promises.length) {
      // darn, async promises, gotta wait for them to resolve
      Promise.all(promises).then((values: any[]) => {
        if (values.some(result => result === false)) {
          ti.reject(`onPageCanEnter rejected`)
        } else {
          this._postViewInit(enteringView, leavingView, ti)
        }
      }).catch(ti.reject)
      return true
    } else {
      // synchronous and all tests passed! let's move on already
      return this._postViewInit(enteringView, leavingView, ti)
    }
  }

  public _transition (enteringView: ViewController, leavingView: ViewController, opts: NavOptions, resolve: TransitionResolveFn) {
    // figure out if this transition is the root one or a
    // child of a parent nav that has the root transition
    this.trnsId = this._trnsCtrl.getRootTrnsId(this)
    if (this.trnsId === null) {
      // this is the root transition, meaning all child navs and their views
      // should be added as a child transition to this one
      this.trnsId = this._trnsCtrl.nextId()
    }

    // default animate
    if (!isPresent(opts.animate)) {
      opts.animate = this.config.getBoolean('animate', false)
    }

    // create the transition options
    const animationOpts: AnimationOptions = {
      animation: opts.animation,
      direction: opts.direction,
      duration: opts.animate ? opts.duration : 0,
      easing: opts.easing,
      isRTL: this.config.plt.isRTL(),
      ev: opts.ev
    }

    // create the transition animation from the TransitionController
    // this will either create the root transition, or add it as a child transition
    const transition = this._trnsCtrl.get(this.trnsId, enteringView, leavingView, animationOpts)

    // transition start has to be registered before attaching the view to the DOM!
    transition.registerStart(() => {
      this._trnsStart(transition, enteringView, leavingView, opts, resolve)
      if (transition.parent) {
        transition.parent.start()
      }
    })

    if (enteringView && enteringView.state === ViewState.INITIALIZED) {
      // render the entering component in the DOM
      // this would also render new child navs/views
      // which may have their very own async canEnter/Leave tests
      // ******** DOM WRITE ****************
      this._viewAttachToDOM(enteringView, enteringView.cmpRef, this.viewport)
    } else {
      log.debug('enteringView state is not INITIALIZED', enteringView && enteringView.name)
    }

    if (!transition.hasChildren) {
      // lowest level transition, so kick it off and let it bubble up to start all of them
      transition.start()
    }
  }

  public _trnsStart (transition: Transition,
    enteringView: ViewController,
    leavingView: ViewController,
    opts: NavOptions,
    resolve: TransitionResolveFn) {
    assert(this.isTransitioning(), 'isTransitioning() has to be true')

    this.trnsId = null

    // set the correct zIndex for the entering and leaving views
    // ******** DOM WRITE ****************
    setZIndex(this, enteringView, leavingView, opts.direction, this._renderer)

    // always ensure the entering view is viewable
    // ******** DOM WRITE ****************
    if (enteringView) {
      enteringView.domShow(true, this._renderer)
    }

    // always ensure the leaving view is viewable
    // ******** DOM WRITE ****************
    if (leavingView) {
      leavingView.domShow(true, this._renderer)
    }

    // initialize the transition
    transition.init()

    // we should animate (duration > 0) if the pushed page is not the first one (startup)
    // or if it is a portal (modal, actionsheet, etc.)
    const isFirstPage = !this.init && this.views.length === 1
    const shouldNotAnimate = isFirstPage && !this.isPortal
    const canNotAnimate = this.config.get('animate') === false
    if (shouldNotAnimate || canNotAnimate) {
      opts.animate = false
    }

    if (opts.animate === false) {
      // if it was somehow set to not animation, then make the duration zero
      transition.duration(0)
    }

    // create a callback that needs to run within zone
    // that will fire off the willEnter/Leave lifecycle events at the right time
    transition.beforeAddRead(this._viewsWillLifecycles.bind(this, enteringView, leavingView))

    // create a callback for when the animation is done
    transition.onFinish(() => {
      // transition animation has ended
      this.zone.run(this._trnsFinish.bind(this, transition, opts, resolve))
    })

    if (transition.isRoot()) {
      // this is the top most, or only active transition, so disable the app
      // add XXms to the duration the app is disabled when the keyboard is open

      // cool, let's do this, start the transition
      if (opts.progressAnimation) {
        // this is a swipe to go back, just get the transition progress ready
        // kick off the swipe animation start
        transition.progressStart()

      } else {
        // only the top level transition should actually start "play"
        // kick it off and let it play through
        // ******** DOM WRITE ****************
        transition.play()
      }
    }
  }

  public _viewsWillLifecycles (enteringView: ViewController, leavingView: ViewController) {
    if (enteringView || leavingView) {
      this.zone.run(() => {
        // Here, the order is important. WillLeave must be called before WillEnter.
        if (leavingView) {
          this._willLeave(leavingView, !enteringView)
        }
        if (enteringView) {
          this._willEnter(enteringView)
        }

        this._willChanged({ enteringView, leavingView })
      })
    }
  }

  public _trnsFinish (transition: Transition, opts: NavOptions, resolve: TransitionResolveFn) {
    const hasCompleted = transition.hasCompleted
    const enteringView = transition.enteringView
    const leavingView = transition.leavingView

    // mainly for testing
    let enteringName: string
    let leavingName: string

    if (hasCompleted) {
      // transition has completed (went from 0 to 1)
      if (leavingView) {
        leavingName = leavingView.name
        this._didLeave(leavingView)
      }

      this._didChanged({ enteringView, leavingView })

      this._cleanup(enteringView)

      if (enteringView) {
        enteringName = enteringView.name
        this._didEnter(enteringView)
      }
    } else {
      // If transition does not complete, we have to cleanup anyway, because
      // previous pages in the stack are not hidden probably.
      this._cleanup(leavingView)
    }

    if (transition.isRoot()) {
      // this is the root transition
      // it's save to destroy this transition
      this._trnsCtrl.destroy(transition.trnsId)

      if (opts.updateUrl !== false) {
        // notify deep linker of the nav change
        // if a direction was provided and should update url
        this._linker.navChange(opts.direction)
      }
    }

    // congrats, we did it!
    resolve(hasCompleted, true, enteringName, leavingName, opts.direction)
  }

  public _insertViewAt (view: ViewController, index: number) {
    const existingIndex = this.views.indexOf(view)
    if (existingIndex > -1) {
      // this view is already in the stack!!
      // move it to its new location
      this.views.splice(index, 0, this.views.splice(existingIndex, 1)[0])

    } else {
      // this is a new view to add to the stack
      // create the new entering view
      view.setNav(this)

      // give this inserted view an ID
      this.ids++
      if (!view.id) {
        view.id = `${this.id}-${this.ids}`
      }

      // insert the entering view into the correct index in the stack
      this.views.splice(index, 0, view)
    }
  }

  public _removeView (view: ViewController) {
    const views = this.views
    const index = views.indexOf(view)
    assert(index > -1, 'view must be part of the stack')
    if (index >= 0) {
      views.splice(index, 1)
    }
  }

  public _destroyView (view: ViewController) {
    view.destroy(this._renderer)
    this._removeView(view)
  }

  /**
   * DOM WRITE
   */
  public _cleanup (activeView: ViewController) {
    // ok, cleanup time!! Destroy all of the views that are
    // INACTIVE and come after the active view
    const activeViewIndex = this.indexOf(activeView)
    const views = this.views
    let reorderZIndexes = false
    let view: ViewController
    let i: number

    for (i = views.length - 1; i >= 0; i--) {

      view = views[i]
      if (i > activeViewIndex) {
        // this view comes after the active view
        // let's unload it
        this._willUnload(view)
        this._destroyView(view)

      } else if (i < activeViewIndex) {
        // this view comes before the active view
        // and it is not a portal then ensure it is hidden
        view.domShow(false, this._renderer)
      }
      if (view.getZIndex() <= 0) {
        reorderZIndexes = true
      }
    }

    if (!this.isPortal && reorderZIndexes) {
      for (i = 0; i < views.length; i++) {
        view = views[i]
        // ******** DOM WRITE ****************
        view.setZIndex(view.getZIndex() + INIT_ZINDEX + 1, this._renderer)
      }
    }
  }

  public preLoad (view: ViewController) {
    assert(this.isTransitioning(), 'nav controller should be transitioning')

    view.emitPreLoad()
  }

  public willLoad (view: ViewController) {
    assert(this.isTransitioning(), 'nav controller should be transitioning')

    view.emitWillLoad()
  }

  public didLoad (view: ViewController) {
    assert(this.isTransitioning(), 'nav controller should be transitioning')
    assert(NgZone.isInAngularZone(), 'callback should be zoned')

    view.emitDidLoad()
    this.viewDidLoad.emit(view)
    this.app.viewDidLoad.emit(view)
  }

  public _willEnter (view: ViewController) {
    assert(this.isTransitioning(), 'nav controller should be transitioning')
    assert(NgZone.isInAngularZone(), 'callback should be zoned')

    view.emitWillEnter()
    this.viewWillEnter.emit(view)
    this.app.viewWillEnter.emit(view)
  }

  public _didEnter (view: ViewController) {
    assert(this.isTransitioning(), 'nav controller should be transitioning')
    assert(NgZone.isInAngularZone(), 'callback should be zoned')

    view.emitDidEnter()
    this.viewDidEnter.emit(view)
    this.app.viewDidEnter.emit(view)
  }

  public _willLeave (view: ViewController, willUnload: boolean) {
    assert(this.isTransitioning(), 'nav controller should be transitioning')
    assert(NgZone.isInAngularZone(), 'callback should be zoned')

    view.emitWillLeave(willUnload)
    this.viewWillLeave.emit(view)
    this.app.viewWillLeave.emit(view)
  }

  public _didLeave (view: ViewController) {
    assert(this.isTransitioning(), 'nav controller should be transitioning')
    assert(NgZone.isInAngularZone(), 'callback should be zoned')

    view.emitDidLeave()
    this.viewDidLeave.emit(view)
    this.app.viewDidLeave.emit(view)
  }

  public _willUnload (view: ViewController) {
    assert(this.isTransitioning(), 'nav controller should be transitioning')
    assert(NgZone.isInAngularZone(), 'callback should be zoned')

    view.emitWillUnload()
    this.viewWillUnload.emit(view)
    this.app.viewWillUnload.emit(view)
  }

  public _willChanged (data: { enteringView: ViewController, leavingView: ViewController }) {
    if (!this.isPortal) {
      this.viewWillChanged.emit(data)
      this.app.viewWillChanged.emit(data)
    }
  }

  public _didChanged (data: { enteringView: ViewController, leavingView: ViewController }) {
    if (!this.isPortal) {
      this.viewDidChanged.emit(data)
      this.app.viewDidChanged.emit(data)
    }
  }

  public getActiveChildNav (): any {
    return this.children[this.children.length - 1]
  }

  public registerChildNav (nav: any) {
    this.children.push(nav)
  }

  public unregisterChildNav (nav: any) {
    removeArrayItem(this.children, nav)
  }

  public destroy () {
    const views = this.views
    let view: ViewController
    for (let i = 0; i < views.length; i++) {
      view = views[i]
      view.emitWillUnload()
      view.destroy(this._renderer)
    }

    // purge stack
    this.views.length = 0

    // Unregister navcontroller
    if (this.parent && this.parent.unregisterChildNav) {
      this.parent.unregisterChildNav(this)
    }
  }

  public canGoBack (): boolean {
    const activeView = this.getActive()
    return !!(activeView && activeView.enableBack())
  }

  public isTransitioning (): boolean {
    return this.trnsTm
  }

  public setTransitioning (isTransitioning: boolean) {
    this.trnsTm = isTransitioning
  }

  public getActive (): ViewController {
    return this.views[this.views.length - 1]
  }

  public isActive (view: ViewController): boolean {
    return (view === this.getActive())
  }

  public getByIndex (index: number): ViewController {
    return this.views[index]
  }

  public getPrevious (view?: ViewController): ViewController {
    // returns the view controller which is before the given view controller.
    if (!view) {
      view = this.getActive()
    }
    return this.views[this.indexOf(view) - 1]
  }

  public first (): ViewController {
    // returns the first view controller in this nav controller's stack.
    return this.views[0]
  }

  public last (): ViewController {
    // returns the last page in this nav controller's stack.
    return this.views[this.views.length - 1]
  }

  public indexOf (view: ViewController | string | number): number {
    // returns the index number of the given view controller.
    if (view instanceof Number) {
      return view
    } else if (typeof view === 'string') {
      return _.findIndex(this.views, (_view: ViewController) => {
        return _view.routeName === view
      })
    } else {
      return this.views.indexOf(view)
    }
  }

  public containsView (view: ViewController | string): boolean {
    return this.indexOf(view) >= 0
  }

  public length (): number {
    return this.views.length
  }

  /**
   * Return the stack of views in this NavController.
   */
  public getViews (): Array<ViewController> {
    return this.views
  }

  public dismissPageChangeViews () {
    this.dismissViewsBySetting('dismissOnPageChange', false)
  }

  public setViewport (val: ViewContainerRef) {
    this.viewport = val
  }

  public get rootNav (): NavController {
    return this.app.getRootNav()
  }

  private dismissViewsBySetting (settingName: string, defaultValue: any) {
    this.views.reduce((promise: any, view: ViewController) => {
      return Promise.all([promise]).then(() => {
        if (view.data) {
          let val = view.data[settingName]
          if (null === val || undefined === val) {
            val = defaultValue
          }

          if (val) {
            log.warn(`dismiss view[${view.routeName}] for ${settingName}`)
            return view.dismiss(null, settingName)
          }
        }
      })
    }, Promise.resolve())
  }
}
