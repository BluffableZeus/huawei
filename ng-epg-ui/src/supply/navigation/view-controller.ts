import { Logger } from '../util/logger'
import { ComponentRef, ElementRef, EventEmitter, Output, Renderer } from '@angular/core'
import { EventEmitter2 } from 'eventemitter2'
import { isPresent } from '../util/util'
import { NavController } from './nav-controller'
import { NavOptions, ViewState, NavLink } from './nav-util'
import { NavParams } from './nav-params'
import { FocusController } from '../focus-controller'

const DEFAULT_CSS_CLASS = 'ion-page'
const log = new Logger('view-controller')
/**
 * @name ViewController
 * @description
 * Access various features and information about the current view.
 * @usage
 *  ```ts
 * import { Component } from '@angular/core';
 * import { ViewController } from 'ionic-angular';
 *
 * @Component({...})
 * export class MyPage{
 *
 *   constructor(public viewCtrl: ViewController) {}
 *
 * }
 * ```
 */
export class ViewController {

  private _isHidden = false
  private _leavingOpts: NavOptions
  private _dismissData: any
  private _dismissRole: any
  private _detached: boolean
  private _cntDir: any

  protected _nav: NavController
  private _zIndex: number

  /** @private */
  @Output() private _emitter: EventEmitter<any> = new EventEmitter<any>()

  public cssClass: string

  public cmpRef: ComponentRef<any>

  public state: ViewState

  /**
   * Observable to be subscribed to when the current component will become active
   * @returns {Observable} Returns an observable
   */
  public willEnter: EventEmitter<any> = new EventEmitter<any>()

  /**
   * Observable to be subscribed to when the current component has become active
   * @returns {Observable} Returns an observable
   */
  public didEnter: EventEmitter<any> = new EventEmitter<any>()

  /**
   * Observable to be subscribed to when the current component will no longer be active
   * @returns {Observable} Returns an observable
   */
  public willLeave: EventEmitter<any> = new EventEmitter<any>()

  /**
   * Observable to be subscribed to when the current component is no long active
   * @returns {Observable} Returns an observable
   */
  public didLeave: EventEmitter<any> = new EventEmitter<any>()

  /**
   * Observable to be subscribed to when the current component has been destroyed
   * @returns {Observable} Returns an observable
   */
  public willUnload: EventEmitter<any> = new EventEmitter<any>()

  /**
   * @private
   */
  public readReady: EventEmitter<any> = new EventEmitter<any>()

  /**
   * @private
   */
  public writeReady: EventEmitter<any> = new EventEmitter<any>()

  /** @private */
  public data: any

  /** @private */
  public instance: any

  /** @private */
  public id: string

  /** @private */
  public isOverlay = false

  private navLink: NavLink

  private focusController: FocusController

  private eventEmitter2 = new EventEmitter2()

  public whenClosed: Promise<any>

  private whenClosedResolve

  constructor (public component?: any, data?: any, rootCssClass = DEFAULT_CSS_CLASS) {
    // passed in data could be NavParams, but all we care about is its data object
    this.data = (data instanceof NavParams ? data.data : (isPresent(data) ? data : {}))

    this.cssClass = rootCssClass
    this.whenClosed = new Promise((resolve) => {
      this.whenClosedResolve = resolve
    })
    this.willUnload.subscribe(() => {
      console.log(`destory view ${this.name}`)
    })
  }

  /**
   * @private
   */
  init (componentRef: ComponentRef<any>) {
    console.log(`create view ${this.name}`)
    this.cmpRef = componentRef
    this.instance = this.instance || componentRef.instance
    this._detached = false
  }

  public on (eventName: string, listener: Function) {
    return this.eventEmitter2.on(eventName, listener as any)
  }

  public setFocusController (focusController: FocusController) {
    this.focusController = focusController
  }

  public getFocusController (): FocusController {
    return this.focusController
  }

  get routeName () {
    if (this.navLink) {
      return this.navLink.name
    }
    return this.name
  }

  public setNav (navCtrl: NavController) {
    this._nav = navCtrl
  }

  public setInstance (instance: any) {
    this.instance = instance
  }

  public setNavLink (navLink: NavLink) {
    this.navLink = navLink
  }

  /**
   * @private
   */
  subscribe (generatorOrNext?: any): any {
    return this._emitter.subscribe(generatorOrNext)
  }

  setContent (directive: any) {
    this._cntDir = directive
  }

  /**
   * @private
   */
  emit (event: string, ...values: any[]) {
    this._emitter.emit(event)
    return this.eventEmitter2.emitAsync(event, ...values)
  }

  /**
   * Called when the current viewController has be successfully dismissed
   */
  onDidDismiss (callback: Function) {
    this.willUnload.subscribe(() => {
      callback(this._dismissData, this._dismissRole)
    })
  }

  /**
   * Called when the current viewController will be dismissed
   */
  onWillDismiss (callback: Function) {
    this.willLeave.subscribe((willUnload) => {
      if (willUnload) {
        callback(this._dismissData, this._dismissRole)
      }
    })
  }

  /**
   * Dismiss the current viewController
   * @param {any} [data] Data that you want to return when the viewController is dismissed.
   * @param {any} [role ]
   * @param {NavOptions} NavOptions Options for the dismiss navigation.
   * @returns {any} data Returns the data passed in, if any.
   */
  dismiss (data?: any, role?: any, navOptions: NavOptions = {}): Promise<any> {
    if (!this._nav) {
      return Promise.resolve(false)
    }
    if (this.isOverlay && !navOptions.minClickBlockDuration) {
      // This is a Modal being dismissed so we need
      // to add the minClickBlockDuration option
      // for UIWebView
      navOptions.minClickBlockDuration = 400
    }
    this._dismissData = data
    this._dismissRole = role

    const options = Object.assign({}, this._leavingOpts, navOptions)
    return this._nav.removeView(this, options).then(() => data)
  }

  /**
   * @private
   */
  getNav (): NavController {
    return this._nav
  }

  /**
   * @private
   */
  getTransitionName (direction: string): string {
    return this._nav && this._nav.config.get('pageTransition')
  }

  /**
   * @private
   */
  getNavParams<T> (): NavParams<T> {
    return new NavParams<T>(this.data)
  }

  /**
   * @private
   */
  setLeavingOpts (opts: NavOptions) {
    this._leavingOpts = opts
  }

  /**
   * Check to see if you can go back in the navigation stack.
   * @returns {boolean} Returns if it's possible to go back from this Page.
   */
  enableBack (): boolean {
    // update if it's possible to go back from this nav item
    if (!this._nav) {
      return false
    }
    // the previous view may exist, but if it's about to be destroyed
    // it shouldn't be able to go back to
    const previousItem = this._nav.getPrevious(this)
    return !!(previousItem)
  }

  /**
   * @private
   */
  get name (): string {
    return this.component ? this.component.name : ''
  }

  /**
   * Get the index of the current component in the current navigation stack.
   * @returns {number} Returns the index of this page within its `NavController`.
   */
  get index (): number {
    return (this._nav ? this._nav.indexOf(this) : -1)
  }

  /**
   * @returns {boolean} Returns if this Page is the first in the stack of pages within its NavController.
   */
  isFirst (): boolean {
    return (this._nav ? this._nav.first() === this : false)
  }

  /**
   * @returns {boolean} Returns if this Page is the last in the stack of pages within its NavController.
   */
  isLast (): boolean {
    return (this._nav ? this._nav.last() === this : false)
  }

  /**
   * @private
   * DOM WRITE
   */
  public domShow (shouldShow: boolean, renderer: Renderer) {
    // using hidden element attribute to display:none and not render views
    // _hidden value of '' means the hidden attribute will be added
    // _hidden value of null means the hidden attribute will be removed
    // doing checks to make sure we only update the DOM when actually needed
    if (this.cmpRef) {
      // if it should render, then the hidden attribute should not be on the element
      if (shouldShow === this._isHidden) {
        this._isHidden = !shouldShow
        let value = (shouldShow ? null : '')
        // ******** DOM WRITE ****************
        renderer.setElementAttribute(this.pageRef().nativeElement, 'hidden', value)
      }
    }
  }

  /**
   * @private
   */
  public getZIndex (): number {
    return this._zIndex
  }

  /**
   * @private
   * DOM WRITE
   */
  public setZIndex (zIndex: number, renderer: Renderer) {
    if (zIndex !== this._zIndex) {
      this._zIndex = zIndex
      const pageRef = this.pageRef()
      if (pageRef) {
        // ******** DOM WRITE ****************
        renderer.setElementStyle(pageRef.nativeElement, 'z-index', (zIndex as any))
      }
    }
  }

  /**
   * @returns {ElementRef} Returns the Page's ElementRef.
   */
  public pageRef (): ElementRef {
    return this.cmpRef && this.cmpRef.location
  }

  public emitPreLoad () {
    this.lifecycle('PreLoad')
  }

  /**
   * @private
   * The view has loaded. This event only happens once per view will be created.
   * This event is fired before the component and his children have been initialized.
   */
  public emitWillLoad () {
    this.lifecycle('WillLoad')
  }

  /**
   * @private
   * The view has loaded. This event only happens once per view being
   * created. If a view leaves but is cached, then this will not
   * fire again on a subsequent viewing. This method is a good place
   * to put your setup code for the view; however, it is not the
   * recommended method to use when a view becomes active.
   */
  public emitDidLoad () {
    this.lifecycle('DidLoad')
  }

  /**
   * @private
   * The view is about to enter and become the active view.
   */
  public emitWillEnter () {
    if (this._detached && this.cmpRef) {
      // ensure this has been re-attached to the change detector
      this.cmpRef.changeDetectorRef.reattach()
      this._detached = false
    }

    this.willEnter.emit(null)
    this.lifecycle('WillEnter')
  }

  /**
   * @private
   * The view has fully entered and is now the active view. This
   * will fire, whether it was the first load or loaded from the cache.
   */
  public emitDidEnter () {
    this.didEnter.emit(null)
    this.lifecycle('DidEnter')
  }

  /**
   * @private
   * The view is about to leave and no longer be the active view.
   */
  public emitWillLeave (willUnload: boolean) {
    this.willLeave.emit(willUnload)
    this.lifecycle('WillLeave')
  }

  /**
   * @private
   * The view has finished leaving and is no longer the active view. This
   * will fire, whether it is cached or unloaded.
   */
  public emitDidLeave () {
    this.didLeave.emit(null)
    this.lifecycle('DidLeave')

    // when this is not the active page
    // we no longer need to detect changes
    if (!this._detached && this.cmpRef) {
      this.cmpRef.changeDetectorRef.detach()
      this._detached = true
    }
  }

  /**
   * @private
   */
  public emitWillUnload () {
    this.willUnload.emit(this._dismissData)
    this.lifecycle('WillUnload')

    this.whenClosedResolve(this._dismissData)

    this._dismissData = null
    this._dismissRole = null
  }

  public isActive () {
    return this._nav.isActive(this)
  }

  /**
   * @private
   * DOM WRITE
   */
  public destroy (renderer: Renderer) {
    if (this.cmpRef) {
      if (renderer) {
        // ensure the element is cleaned up for when the view pool reuses this element
        // ******** DOM WRITE ****************
        let cmpEle = this.cmpRef.location.nativeElement
        renderer.setElementAttribute(cmpEle, 'class', null)
        renderer.setElementAttribute(cmpEle, 'style', null)
      }

      // completely destroy this component. boom.
      this.cmpRef.destroy()
    }

    if (this.focusController) {
      this.focusController.destroy()
    }

    this._nav = this.cmpRef = this.instance = null
  }

  /**
   * @private
   */
  public lifecycleTest (lifecycle: string): boolean | Promise<any> {
    const instance = this.instance
    const methodName = 'onPageCan' + lifecycle

    if (this._detached === true) {
      return true
    }
    if (instance && instance[methodName]) {
      try {
        let result = instance[methodName]()
        if (result === false) {
          return false
        } else if (result instanceof Promise) {
          return result
        } else {
          return true
        }

      } catch (e) {
        log.error(`${this.name} ${methodName} error: ${e.message}`)
        return false
      }
    }
    return true
  }

  public getElement (): HTMLElement {
    return this.cmpRef.location.nativeElement
  }

  private lifecycle (lifecycle: string) {
    const instance = this.instance
    const methodName = 'onPage' + lifecycle
    log.debug('page[%s] lifecycle[%s]', this.name, methodName)

    if (instance && instance[methodName]) {
      try {
        instance[methodName]()
      } catch (e) {
        log.error(`${this.name} ${methodName} error: ${e.message}`)
      }
    }
  }

}

export function isViewController (viewCtrl: any): boolean {
  return !!(viewCtrl && (viewCtrl as ViewController).emitDidLoad && (viewCtrl as ViewController).emitWillLoad)
}
