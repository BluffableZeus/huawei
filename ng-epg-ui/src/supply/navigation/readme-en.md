# NavController
NavController是用于按理页面栈的类，当前共有三个页面栈，主页面栈、Modal页面栈、Toast页面栈

```typescript
import { App, NavController } from 'ng-epg-ui/components/nav';
import { ToastController } from 'ng-epg-ui/components/toast/toast';
import { ModalController } from 'ng-epg-ui/components/modal/modal';
```

```typescript
@Component({
    ...
})
export class CustomComponent implements OnInit {

    constructor(private app:App, private modalCtrl:ModalController, private toastCtrl:ToastController){
    }

    ngOnInit(){
        // 在主页面栈打开新页面
        this.app.getRootNav().push(<组件类引用>);

        // 在Modal打开新窗口
        this.modalCtrl.push(<组件类引用>);

        // 在Toast打开新窗口
        this.toastCtrl.push(<组件类引用>);
    }
```

### API说明
| 方法名称 | 入参 | 返回| 描述 |
| --- | --- | --- |---|
| push | (page: any, params?: any, opts?: NavOptions) | Promise<ViewController> |打开新页面|
| getActive|()|ViewController|返回栈顶页面|
|getViews|()|Array<ViewController>|返回栈内所有页面|
|pop|(opts?: NavOptions)|Promise<void>|关闭栈顶页面|
|popTo|(indexOrViewCtrl: any, opts?: NavOptions)|Promise<void>|从栈顶关闭到指定页面|
|removeView|(viewController: ViewController, opts?: NavOptions)|Promise<void>|关闭指定页面|

### 属性说明
| 方法名称 | 返回| 描述 |
| --- | --- | --- |
| viewDidEnter | Observable&lt;ViewController&gt; | Observable to be subscribed to when a component has fully become the active component.|
| viewDidLeave | Observable&lt;ViewController&gt; | Observable to be subscribed to when a component has fully left and is no longer active.|
| viewDidLoad | Observable&lt;ViewController&gt; | Observable to be subscribed to when a component is loaded.|
| viewWillEnter | Observable&lt;ViewController&gt; | Observable to be subscribed to when a component is about to be loaded.|
| viewWillLeave | Observable&lt;ViewController&gt; |Observable to be subscribed to when a component is about to leave, and no longer active.|
| viewWillUnload | Observable&lt;ViewController&gt; |Observable to be subscribed to when a component is about to be unloaded and destroyed.|



### 页面回调说明
| Page Event | Returns | Description |
| --- | --- | --- |
| `onPageDidLoad` | void | Runs when the page has loaded. This event only happens once per page being created. If a page leaves but is cached, then this event will not fire again on a subsequent viewing. The `onPageDidLoad` event is good place to put your setup code for the page. |
| `onPageWillEnter` | void | Runs when the page is about to enter and become the active page. |
| `onPageDidEnter` | void | Runs when the page has fully entered and is now the active page. This event will fire, whether it was the first load or a cached page. |
| `onPageWillLeave` | void | Runs when the page is about to leave and no longer be the active page. |
| `onPageDidLeave` | void | Runs when the page has finished leaving and is no longer the active page. |
| `onPageWillUnload` | void | Runs when the page is about to be destroyed and have its elements removed. |
| `onPageCanEnter` | boolean/Promise<void> | Runs before the view can enter. This can be used as a sort of "guard" in authenticated views where you need to check permissions before the view can enter |
| `onPageCanLeave` | boolean/Promise<void> | Runs before the view can leave. This can be used as a sort of "guard" in authenticated views where you need to check permissions before the view can leave |
