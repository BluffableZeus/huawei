var gulp = require('gulp');
var releaseVersion = require('./demos-app/script/release-version');

gulp.task('version:patch', function () {
    return releaseVersion('patch');
});

gulp.task('version:minor', function () {
    return releaseVersion('minor');
});

gulp.task('version:major', function () {
    return releaseVersion('major');
});

gulp.task('default', ['version:patch', 'version:minor', 'version:major']);