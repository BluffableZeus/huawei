import { NgWebtvPage } from './app.po';

describe('ng-webtv App', function() {
  let page: NgWebtvPage;

  beforeEach(() => {
    page = new NgWebtvPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
