export const Methods = {
    post: 'POST',
    get: 'GET',
    delete: 'DELETE',
    put: 'PUT',
    head: 'HEAD',
    patch: 'PATCH'
};

export const EVENTS = {
    READY_STATE_CHANGE: 'readystatechange',
    LOAD_START: 'loadstart',
    PROGRESS: 'progress',
    ABORT: 'abort',
    ERROR: 'error',
    LOAD: 'load',
    TIMEOUT: 'timeout',
    LOAD_END: 'loadend'
};

export class OCData {
    public subscriberId: string;
    public sessionId: string;
    public host: string;
}

class XR {
    static apiUrl = '';

    static default = {
        method: 'POST',
        // tslint:disable-next-line:no-empty
        success: () => {},
        // tslint:disable-next-line
        error: ( msg ) => console.error( msg ),
        withCredentials: false,
        isStringAnswer: false
    };

    static configure ( operCenterApiUrl ) {
        XR.apiUrl = operCenterApiUrl;
    }

    static request ( args ) {
        return new Promise((resolve, reject) => {
            args = XR.setDefaults( args );

            if ( !args.url ) {
                args.error( 'No URL provided' );
            }

            const xhr = new XMLHttpRequest();

            xhr.open( args.method, XR.apiUrl + args.url + XR.formParams( args.params ), true);
            xhr.withCredentials = XR.default.withCredentials;
            xhr.setRequestHeader('Content-Type', 'application/json');

            xhr.addEventListener(EVENTS.LOAD, () => {
                let data;
                let error;
                if (xhr.status >= 200 && xhr.status < 300) {
                    if (xhr.responseText) {
                        try {
                            if (args.isStringAnswer) {
                                data = xhr.responseText;
                            }
                            else {
                                data = JSON.parse( xhr.responseText );
                            }
                        }
                        catch (e) {
                            error = e;
                        }
                    }
                }
                else {
                    error = true;
                }
                if (error) {
                    if (args.error) {
                        args.error(xhr);
                    }
                    reject(xhr);
                }
                else {
                    if (args.success) {
                        args.success(data);
                    }
                    resolve(data);
                }
            });

            xhr.addEventListener(EVENTS.ABORT, () => reject(xhr));
            xhr.addEventListener(EVENTS.ERROR, () => reject(xhr));
            xhr.addEventListener(EVENTS.TIMEOUT, () => reject(xhr));

            xhr.send( args.data );
        });
    }

    static formParams ( params ) {
        if (Object.keys(params).length < 1) {
            return '';
        }

        return '?' + Object.keys( params ).map(key => {
            return key + '=' + params[key];
        }).join('&');
    }

    static setDefaults ( args ) {
        return {
            url: args.url,
            params: args.params || {},
            data: JSON.stringify(args.data),
            withCredentials: args.withCredentials,
            isStringAnswer: args.isStringAnswer,
            method: args.method || XR.default.method,
            success: args.success || XR.default.success,
            error: args.error || XR.default.error,
        };
    }

    static post = ( args ) => {
        args.method = Methods.post;
        return XR.request( args );
    }

    static get = ( args ) => {
        args.method = Methods.get;
        return XR.request( args );
    }

    static delete = ( args ) => {
        args.method = Methods.delete;
        return XR.request( args );
    }
}

export const xr = XR;
