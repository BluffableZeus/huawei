import { Injectable } from '@angular/core';
import { idempotence } from '../util/idempotence';
import { RetryResult, RetrySettings, retry } from '../util/retry';
import { xr, OCData } from './requests';
import { StatusResponse } from './type';

export const enum PaymentSystem {
    apple = 'APPLE',
    google = 'GOOGLE',
    yandex = 'YANDEX',
}

export const enum OperationType {
    empty = '',
    initialBuy = 'SUBSCRIPTION_INITIAL_BUY',
    renewal = 'SUBSCRIPTION_RENEWAL',
    interactiveRenewal = 'SUBSCRIPTION_INTERACTIVE_RENEWAL',
    cancel = 'SUBSCRIPTION_CANCEL',
}

export const enum SubscriptionState {
    /** Product is unsubscribed. */
    unsubscribed,
    /** Product is subscribed. */
    subscribed,
    /** Product will be unsubscribed in some time (is waiting unsubscription). */
    end,
    /** Product subscription cannot be managed. */
    unmanaged,
}

export interface BankCard {
    card_type: string;
    expiry_month: string;
    expiry_year: string;
    last4: string;
}

export interface PaymentMethod {
    paymentId: string;
    createDate: number;
    active: boolean;
    card?: BankCard;
}

export interface SubscriptionInfo {
    subscriberId: string;
    productId: string;
    processStatus: string;
    paymentSystem: PaymentSystem;
    resultStatus: string;
    productType: string;
    operationType: OperationType;
    isTrialPeriod: boolean;
    createdDate: number;
    startDate: number;
    expiresDate: number;
    price: number;
    currency: string;
    state?: SubscriptionState;
}

export interface SubscriptionInfoSet {
    [productId: string]: SubscriptionInfo;
}

export interface Product {
    id: string;
    price: string;
}

export interface PayForProductSettings {
    product: Product;
    paymentId: string;
    ocData: OCData;
    retrySettings: RetrySettings;
}

export interface VspProduct {
    isSubscribed: '0' | '1';
}

export interface VspQueryProductResponse {
    productList: VspProduct[];
}

/**
 * Attempts that should be made to retry getting data about product's subscription status after product was paid.
 * Array items are timeouts in milliseconds specifying delay before an attempt.
 */
export let payForProductRetryAttempts = [5000, 15000];

/**
 * Check whether subscription can be managed.
 *
 * @param {SubscriptionInfo} subscription
 *      Subscription that should be checked.
 * @return {boolean}
 *      `true` when subscription can be managed, otherwise `false`.
 */
export function isManagedSubscription(subscription: SubscriptionInfo): boolean {
    return subscription.paymentSystem === PaymentSystem.yandex;
}

/**
 * Get subscription state.
 *
 * @param {SubscriptionInfo} subscription
 *      Subscription for which state should be returned.
 * @return {SubscriptionState}
 *      Subscription state.
 */
export function getSubscriptionState(subscription: SubscriptionInfo): SubscriptionState {
    let state: SubscriptionState;
    if (isManagedSubscription(subscription)) {
        const { expiresDate } = subscription;
        if (expiresDate && expiresDate > new Date().getTime()) {
            state = subscription.operationType === OperationType.cancel
                ? SubscriptionState.end
                : SubscriptionState.subscribed;
        }
        else {
            state = SubscriptionState.unsubscribed;
        }
    }
    else {
        state = SubscriptionState.unmanaged;
    }

    return state;
}

/**
 * Set state for given subscription.
 *
 * @param {SubscriptionInfo} subscription
 *      Subscription for which state should be set.
 * @return {SubscriptionState}
 *      Original subscription object with modified `state` field.
 * @see .getSubscriptionState
 */
export function setSubscriptionState(subscription: SubscriptionInfo): SubscriptionInfo {
    subscription.state = getSubscriptionState(subscription);

    return subscription;
}

/**
 * Check whether VSP product from response is not subscribed.
 *
 * @param {VspQueryProductResponse} response
 *      Response data.
 * @return {boolean}
 *      `true` when product is not subscribed, otherwise `false`.
 */
function isNotSubscribedVspProduct(response: VspQueryProductResponse): boolean {
    const { productList } = response;
    return ! productList || ! productList.length || productList[0].isSubscribed === '0';
}


// tslint:disable
@Injectable()
export class subscriptionAPI {
  endpoints = {
    getInfo: 'v1/payment_operations/last',
    getInfoList: 'v1/payment_operations/lasts'
  }

  public getInfo(productId, ocData: OCData): Promise<SubscriptionInfo> {
    return xr.get({
      url: this.endpoints['getInfo'],
      params: {
        productId: productId,
        subscriberId: ocData.subscriberId,
        sessionId: ocData.sessionId,
        host: ocData.host
      }
    }) as Promise<SubscriptionInfo>;
  }

  public getInfoList(productIdList: String[], ocData: OCData): Promise<SubscriptionInfoSet> {
    return xr.get({
      url: this.endpoints.getInfoList,
      params: {
        productIds: productIdList.join(','),
        subscriberId: ocData.subscriberId,
        sessionId: ocData.sessionId,
        host: ocData.host
      }
    }) as Promise<SubscriptionInfoSet>;
  }

  public getPaymentMethods(ocData: OCData): Promise<PaymentMethod[]> {
    let url = `yandex/v1/subscriber/${ocData.subscriberId}/saved_payments`
    return xr.get({
      url,
      params: {
        sessionId: ocData.sessionId,
        host: ocData.host
      }
    }) as Promise<PaymentMethod[]>;
  }

  public createPaymentMethod(productId, productAmount, ocData: OCData, paymentMethodId = undefined) {
    let url = 'yandex/proxy/api/payments';
    let data: any = {
      amount: {
        value: productAmount,
        currency: 'RUB'
      },
      metadata: {
        idempotenceKey: this.getIdempotenceKey(ocData, 'create_payment'),
        subscriberId: ocData.subscriberId,
        productId: productId
      }
    };

    if (paymentMethodId) {
      data.payment_method_id = paymentMethodId;
      data.save_payment_method = false;
    } else {
        data.payment_method_data = { type: 'bank_card' };
        data.save_payment_method = true;
        data.confirmation = {
            type: 'redirect',
            return_url: window.location.href
        };
    }

    return xr.post({
      url, data,
      params: {
        sessionId: ocData.sessionId,
        host: ocData.host
      },
      withCredentials: true
    });
  }

  public payForProduct(settings: PayForProductSettings): Promise<RetryResult> {
    const retrySettings = Object.assign({}, settings.retrySettings);
    const { product } = settings;
    const productId = product.id;
    if (! retrySettings.actionParams) {
        // Suppose that action is request to /VSP/QueryProduct
        retrySettings.actionParams = [
            {
                queryType: 'BY_IDLIST',
                productIds: [productId]
            },
            {
                isIgnoreError: true
            }
        ];
    }
    if (! retrySettings.retryTest) {
        retrySettings.retryTest = isNotSubscribedVspProduct;
    }
    if (! ('retryOnError' in retrySettings)) {
        retrySettings.retryOnError = true;
    }
    if (! retrySettings.retryAttempts && ! retrySettings.retryQty) {
        retrySettings.retryAttempts = payForProductRetryAttempts;
    }

    return this.createPaymentMethod(productId, product.price, settings.ocData, settings.paymentId)
        .then(function onPaymentEnd() {
            return retry(retrySettings);
        });
  }

  public removeSubscription(productId, ocData: OCData): Promise<StatusResponse> {
    return xr.delete({
      url: 'yandex/v1/subscription',
      params: {
        sessionId: ocData.sessionId,
        host: ocData.host
      },
      data: {
        idempotenceKey: this.getIdempotenceKey(ocData, 'unsubscribe'),
        subscriberId: ocData.subscriberId,
        productId: productId
      }
    }) as Promise<StatusResponse>;
  }

  public renewSubscription(productId, ocData: OCData): Promise<StatusResponse> {
    return xr.post({
      url: 'yandex/v1/subscription',
      params: {
        sessionId: ocData.sessionId,
        host: ocData.host
      },
      data: {
        idempotenceKey: this.getIdempotenceKey(ocData, 'unsubscribe'),
        subscriberId: ocData.subscriberId,
        productId: productId
      }
    }) as Promise<StatusResponse>;
  }

  // // public register = ( userinfo ) =>
  // // {
  // //   xr.post(
  // //     endpoint(this.endpoints['getInfo'],
  // //     this.wrapData( userinfo ),
  // //     {
  // //       withCredentials: true
  // //     }
  // //   )
  // // }

  // prepareInfo ( userinfo )
  // {
  //   return {
  //     ...userinfo,
  //     idempotenceKey: this.getIdempotenceKey( userinfo, 'register' ),
  //     client: 'WEB'
  //   }
  // }

  getIdempotenceKey ( ocData: OCData, type )
  {
    return idempotence.generate( type )
  }

  debug ( data )
  {
      console.log('API:', data)
  }
}
