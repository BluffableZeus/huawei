import { Injectable } from '@angular/core';
import { xr, OCData } from './requests';

// tslint:disable-next-line:class-name
export interface socialAccountInfo {
    code: number;
    data: {
      vk: boolean
    };
    msg: string;
}

export interface VKData {
    code: string;
    error: string;
    error_reason: string;
    error_description: string;
    state: string;
    redirect_uri?: string;
}

@Injectable()
export class socialAPI {   // tslint:disable-line:class-name

    endpoints =
    {
      vk:
        {
          test:     '/users/register',
          link:     '/users/login/link/vk',

          add:      '/users/register/link/vk',
          login:    '/users/login/vk',
          remove:   '/users/unregister/link/vk',

          m_add:    '/users/register/mobile/vk',
          m_login:  '/users/login/mobile/vk',
          m_remove: '/users/unregister/mobile/vk'
        }
    };

    public configure = ( operCenterApiUrl ) => {
        xr.configure( operCenterApiUrl );
    }

    public test ( networkID, ocData: OCData ): Promise<socialAccountInfo> {
        return xr.get({
            // tslint:disable-next-line:no-string-literal
            url: this.endpoints[networkID]['test'],
            params: {
                username: ocData.subscriberId,
                sessionId: ocData.sessionId,
                host: ocData.host
            }
        }) as Promise<socialAccountInfo>;
    }

    public auth ( networkID, vkData: VKData ): Promise<socialAccountInfo> {
        return xr.get({
            // tslint:disable-next-line:no-string-literal
            url: this.endpoints[networkID]['login'],
            params: vkData
        }) as Promise<socialAccountInfo>;
    }

    public login ( networkID, redirect? ): Promise<string> {
        let params = (redirect) ? { redirect_uri: redirect } : {};

        return xr.get({
            // tslint:disable-next-line:no-string-literal
            url: this.endpoints[networkID]['link'],
            params: params,
            isStringAnswer: true
        }) as Promise<string>;
    }

    public add ( networkID, ocData: OCData, redirect? ): Promise<string> {
        return xr.get({
            // tslint:disable-next-line:no-string-literal
            url: this.endpoints[networkID]['add'],
            params: {
                username: ocData.subscriberId,
                sessionId: ocData.sessionId,
                host: ocData.host,
                redirect_uri: redirect ? redirect : ''
            },
            isStringAnswer: true
        }) as Promise<string>;
    }

    public remove ( networkID, ocData: OCData, redirect? ): Promise<string> {
        return xr.get({
            // tslint:disable-next-line:no-string-literal
            url: this.endpoints[networkID]['remove'],
            params: {
                username: ocData.subscriberId,
                sessionId: ocData.sessionId,
                host: ocData.host,
                redirect_uri: redirect ? redirect : ''
            },
            isStringAnswer: true
        }) as Promise<string>;
    }

    public extractVKData(): VKData {
        if (window.location.search.indexOf('code') < 0 && window.location.search.indexOf('error') < 0) {
            return;
        }

        let str = window.location.search.replace(/\?/g, '').split('&'),
            params = {};

        str.forEach(item => {
            let s = item.split('=');
            params[s[0]] = s[1];
        });

        return params as VKData;
    }

    public testVKCallback( url, onSuccess?, onError? ) {
        let data = this.extractVKData();

        if (! data) {
            return;
        }

        data.redirect_uri = window.location.origin + url;

        this.auth('vk', data).then(resp => {
            onSuccess && onSuccess(resp);
            window.history.replaceState( {} , document.title, url );
        }, resp => {
            onError && onError(resp);
            window.history.replaceState( {} , document.title, url );
        });
    }
}
