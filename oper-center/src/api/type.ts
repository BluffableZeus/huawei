export interface StatusResponse {
    code: number;
    data?: any;
    msg: string;
}
