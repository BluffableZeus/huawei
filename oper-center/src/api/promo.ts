import { Injectable } from '@angular/core';
import { xr } from './requests';


@Injectable()
export class PromoAPI {
    endpoint = '/v1/codes/';

    constructor() {
        //   Default constructor
    }

    public configure = (operCenterApiUrl) => {
        xr.configure(operCenterApiUrl);
    }

    public promocodeRegister = (data) => {
        xr.post({
            url: this.endpoint + data.promocode,
            data: {
                id: data.promocode,
                login: data.login,
                host: data.userData.host,
                sessionId: data.userData.sessionId,
                clientType: 'WEB'
            },
            success: this.success(data.success, data.error),
            error: this.error(data.error)
        });
    }

    success(success, error) {
        return (res) => {
            if (res.code === 0) {
                success(res);
            } else {
                error(res.msg);
            }
        };
    }

    error(callback) {
        return (msg) => {
            callback(msg);
        };
    }
}
