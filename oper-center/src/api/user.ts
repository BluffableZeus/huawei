import { Injectable } from '@angular/core';
import { idempotence } from '../util/idempotence';
import { xr } from './requests';

@Injectable()
export class userAPI {   // tslint:disable-line:class-name

    endpoints =
    {
        register: '/v1/add_subscriber',
        reset: '/v1/reset_password'
    };

    public configure = ( operCenterApiUrl ) => {
        xr.configure( operCenterApiUrl );
    }

    public register = ( userinfo ) => {
        xr.post({
            // tslint:disable-next-line:no-string-literal
            url: this.endpoints['register'],
            data: this.prepareInfo( userinfo ),
            withCredentials: true,
            success: this.check( userinfo.success, userinfo.error ),
            error: this.error( userinfo.error )
        });
    }

    public resetPassword = ( userinfo ) => {
        xr.post({
            // tslint:disable-next-line:no-string-literal
            url: this.endpoints['reset'],
            data: this.prepareInfo( userinfo ),
            withCredentials: true,
            success: this.check( userinfo.success, userinfo.error ),
            error: this.error( userinfo.error )
        });
    }

    prepareInfo ( userinfo ) {
        return {
            ...userinfo,
            success: undefined,
            error: undefined,
            idempotenceKey: this.getIdempotenceKey( 'register' ),
            client: 'WEB'
        };
    }

    check ( success, error ) {
        return ( res ) => {
            if ( res.code === 0 ) {
                success();
            } else {
                error( res.msg );
            }
        };
    }

    error ( callback ) {
        return ( msg ) => {
            // tslint:disable-next-line:no-console
            console.error('Error API:', msg);
            callback( msg );
        };
    }

    getIdempotenceKey ( type ) {
        return idempotence.generate( type );
    }
}
