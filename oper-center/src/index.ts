export * from './api';

import {
    userAPI,
    subscriptionAPI,
    PromoAPI,
    socialAPI
} from './api';

export const OC_PROVIDERS = [
    userAPI,
    subscriptionAPI,
    PromoAPI,
    socialAPI
];
