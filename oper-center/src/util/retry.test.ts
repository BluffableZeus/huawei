import { retry } from './retry';

describe('retry', () => {
    function getAction(value) {
        return function action() {
            return Promise.resolve(value++);
        }
    }

    function getLessTest(max) {
        return function lessTest(value) {
            return value < max;
        }
    }

    it('should call action only once', () => {
        const num = 3;
        const result = retry({
            action: getAction(num),
            retryTest: getLessTest(num),
            retryQty: 200
        });

        return result.promise.then((value) => {
            expect( result.attempt )
                .toBe( 1 );
            expect( value )
                .toBe( num );
        });
    });

    it('should call action specified number of times', () => {
        const retryQty = 7;
        const result = retry({
            action: getAction(1),
            retryTest: getLessTest(1000),
            retryQty
        });

        return result.promise.then((value) => {
            expect( result.attempt )
                .toBe( retryQty + 1 );
            expect( value )
                .toBe( retryQty + 1 );
        });
    });

    it('should call action first time after delay', () => {
        const num = 10;
        const delay = 200;
        const retryQty = 2;
        const startTime = new Date().getTime();
        const result = retry({
            action: getAction(num),
            retryTest: getLessTest(num + 100),
            retryQty,
            delay
        });

        return result.promise.then((value) => {
            expect( new Date().getTime() - startTime )
                .toBeGreaterThanOrEqual( delay );
            expect( result.attempt )
                .toBe( retryQty + 1 );
            expect( value )
                .toBe( num + retryQty );
        });
    });

    it('should retry action calls after delay', () => {
        const num = 4;
        const delay = 100;
        const retryTimeout = 200;
        const retryQty = 2;
        const startTime = new Date().getTime();
        const result = retry({
            action: getAction(num),
            retryTest: getLessTest(num + 100),
            retryQty,
            retryTimeout,
            delay
        });

        return result.promise.then((value) => {
            expect( new Date().getTime() - startTime )
                .toBeGreaterThanOrEqual( delay + (retryQty * retryTimeout) );
            expect( result.attempt )
                .toBe( retryQty + 1 );
            expect( value )
                .toBe( num + retryQty );
        });
    });

    it('should retry action calls after specified timeouts', () => {
        const num = 1;
        const delay = 100;
        const retryAttempts = [100, 200, 300];
        const retryQty = retryAttempts.length;
        const startTime = new Date().getTime();
        const result = retry({
            action: getAction(num),
            retryTest: getLessTest(num + 100),
            retryAttempts,
            delay
        });

        return result.promise.then((value) => {
            expect( new Date().getTime() - startTime )
                .toBeGreaterThanOrEqual( delay + retryAttempts.reduce((sum, value) => sum + value, 0) );
            expect( result.attempt )
                .toBe( retryQty + 1 );
            expect( value )
                .toBe( num + retryQty );
        });
    });

    it('should call action with context and parameters', () => {
        const obj = {
            value: 0,
            change(getChange) {
                return Promise.resolve( this.value += getChange() );
            }
        };
        const retryQty = 3;
        const result = retry({
            action: obj.change,
            actionContext: obj,
            actionParams: [
                function getChange() {
                    return result.attempt * 2;
                }
            ],
            retryTest: getLessTest(1000),
            retryQty,
            delay: 0
        });

        return result.promise.then((value) => {
            expect( result.attempt )
                .toBe( retryQty + 1 );
            expect( value )
                .toBe( obj.value );
            expect( value )
                .toBe( 2 + 4 + 6 + 8 );
        });
    });

    it('should not retry action calls on promise rejection', () => {
        const reason = 'Test rejection';
        const result = retry({
            action: () => Promise.reject(reason),
            retryTest: () => true,
            retryQty: 100
        });

        return result.promise.catch((value) => {
            expect( result.attempt )
                .toBe( 1 );
            expect( value )
                .toBe( reason );
        });
    });

    it('should retry action calls on promise rejection', () => {
        const reason = 'Test rejection';
        const retryQty = 2;
        const result = retry({
            action: () => Promise.reject(reason),
            retryQty,
            retryOnError: true
        });

        return result.promise.catch((value) => {
            expect( result.attempt )
                .toBe( retryQty + 1 );
            expect( value )
                .toBe( reason );
        });
    });

    it('should retry action calls on promise rejection depending on condition', () => {
        const maxLen = 3;
        let reason = '';
        const result = retry({
            action: () => Promise.reject(reason += '-'),
            retryQty: 1000,
            retryOnError: (reason) => reason.length < maxLen
        });

        return result.promise.catch((value) => {
            expect( result.attempt )
                .toBe( maxLen );
            expect( value )
                .toBe( reason );
            expect( value )
                .toBe( '-'.repeat(maxLen) );
        });
    });

    it('should stop repetition of action calls', (done) => {
        const num = 7;
        const result = retry({
            action: getAction(num),
            retryTest: getLessTest(1000),
            retryQty: 100,
            retryTimeout: 100
        });
        let qty;

        expect( result.stopped )
            .toBe( false );

        setTimeout(() => {
            qty = result.attempt;
            expect( result.stop() )
                .toBe( result.promise );
        }, 250);

        setTimeout(() => {
            expect( result.attempt )
                .toBe( qty );
            expect( result.stopped )
                .toBe( true );
            result.promise.then((value) => {
                expect( result.stop() )
                    .toBe( result.promise );
                expect( value )
                    .toBe( num + qty - 1 );

                done();
            });
        }, 450);
    });
});
