export type RetryTest = (value: any) => boolean;

export interface RetrySettings {
    action: (...args: any[]) => Promise<any>;
    actionContext?: any;
    actionParams?: any[];
    delay?: number;
    retryAttempts?: number[];
    retryQty?: number;
    retryTimeout?: number;
    retryOnError?: boolean | RetryTest;
    retryTest?: RetryTest;
}

export interface WithPromiseField {
    promise: Promise<any>;
}

export interface RetryResult extends WithPromiseField {
    attempt: number;
    stop: () => Promise<any>;
    stopped: boolean;
}

export function retry(settings: RetrySettings): RetryResult {
    let actionResult, resultReject, resultResolve, timeoutId;
    const resultPromise = new Promise(function(resolve, reject) {
        resultResolve = resolve;
        resultReject = reject;
    });

    let index = 0;
    let stopped = false;

    let attempts = [settings.delay];
    const { retryAttempts } = settings;
    if (! retryAttempts || ! retryAttempts.length) {
        const { retryQty } = settings;
        if (retryQty && retryQty > 0) {
            attempts.push(...Array(retryQty).fill(settings.retryTimeout));
        }
    }
    else {
        attempts.push(...retryAttempts);
    }

    function retryAction() {
        retryResult.attempt = ++index;
        actionResult = settings.action.apply(settings.actionContext || null, settings.actionParams || []);
        actionResult.then(onActionEnd, onActionError);
    }

    function repeat() {
        const timeout = attempts.shift();
        if (typeof timeout !== 'number' || timeout < 0) {
            retryAction();
        }
        else {
            timeoutId = setTimeout(retryAction, timeout);
        }
    }

    function onActionEnd(value: any) {
        if (stopped) {
            return;
        }
        let retryTest: any = settings.retryTest;
        if (attempts.length && retryTest) {
            retryTest = retryTest(value);
        }
        else {
            retryTest = false;
        }
        if (retryTest) {
            repeat();
        }
        else {
            resultResolve(value);
        }
    }

    function onActionError(reason: any) {
        if (stopped) {
            return;
        }
        let { retryOnError } = settings;
        if (! attempts.length) {
            retryOnError = false;
        }
        else if (typeof retryOnError === 'function') {
            retryOnError = retryOnError(reason);
        }
        if (retryOnError) {
            repeat();
        }
        else {
            resultReject(reason);
        }
    }

    function stopRetry() {
        if (! stopped) {
            if (timeoutId) {
                clearTimeout(timeoutId);
            }
            stopped = retryResult.stopped = true;
            if (actionResult) {
                actionResult.then(resultResolve, resultReject);
            }
            else {
                resultResolve();
            }
        }

        return resultPromise;
    }

    const retryResult: RetryResult = {
        attempt: index,
        promise: resultPromise,
        stop: stopRetry,
        stopped: false
    };

    repeat();

    return retryResult;
}

export function getPromiseField(obj: WithPromiseField): Promise<any> {
    return obj.promise;
}
