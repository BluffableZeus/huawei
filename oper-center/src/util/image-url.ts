export interface ImageUrlMap {
    [key: string]: string;
}

export class ImageUrl {
    constructor(private urlMap: ImageUrlMap, private urlPrefix?: string) {

    }

    getUrl(imageKey: string): string {
        let url = this.urlMap[imageKey];
        if (url && this.urlPrefix) {
            url = `${this.urlPrefix}${url}`;
        }

        return url || imageKey;
    }

    getUrlWrap(imageKey: string): string {
        return `url(${this.getUrl(imageKey)})`;
    }
}
