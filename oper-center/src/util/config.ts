import URLSearchParams from 'url-search-params';

export { URLSearchParams };

export type UpdateKey = boolean | string | string[];

export interface UpdateData {
    key: UpdateKey;
    convert?: (value: string, key?: string, config?: object) => any;
}

export interface ReconfigData {
    config: object;
    updateMap: {
        [propName: string]: UpdateKey | UpdateData;
    };
}

export function reconfigFromQuery(query: string, update: ReconfigData | ReconfigData[]) {
    if (query) {
        const updateSet = Array.isArray(update)
            ? update
            : [update];
        const searchParams = new URLSearchParams(query);
        for (let i = 0, q = updateSet.length; i < q; i++) {
            const data = updateSet[i];
            const { config, updateMap: map } = data;
            for (const key in map) {
                let paramList: any = map[key];
                if (paramList) {
                    let convert;
                    if (! Array.isArray(paramList)) {
                        let type = typeof paramList;
                        if (type === 'object') {
                            convert = paramList.convert;
                            paramList = paramList.key;
                            type = typeof paramList;
                        }
                        if (type === 'boolean') {
                            paramList = [key];
                        }
                        else if (type === 'string') {
                            paramList = [paramList];
                        }
                    }
                    for (let k = 0, len = paramList.length; k < len; k++) {
                        const param = paramList[k];
                        if (searchParams.has(param)) {
                            const value = searchParams.get(param);
                            config[key] = convert
                                ? convert(value, key, config)
                                : value;
                            break;
                        }
                    }
                }
            }
        }
    }
}
