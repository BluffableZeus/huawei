export class IdempotenceService {

    tsLength = 5;
    csLength = 8;
    opLength = 7;

    public generate = ( operation ) => {
        return this.fingerprint() + this.checksum( operation, this.opLength ) + this.timestamp();
    }

    timestamp () {
        const chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

        let ts = Math.round( new Date().getTime() / 10000 ), // round to ten seconds to prevent any issues
            s = '';

        while (ts > 0) {
            s = chars[ts % 62] + s;
            ts = Math.floor(ts / 62);
        }

        return this.testLength( s, this.tsLength );
    }

    checksum ( str, slen = this.csLength ) {
        // fletcher32
        let s1 = 0xffff,
            s2 = 0xffff,
            buffer = str.split('').map( c => c.charCodeAt( 0 ) ),
            len = buffer.length,
            index = 0;

        while ( len ) {
            let tlen = len > 359 ? 359 : len;
            len -= tlen;

            do {
            s2 += s1 += buffer[ index++ ];
            } while ( --tlen );

            /* tslint:disable:no-bitwise */
            s1 = ( ( s1 & 0xffff ) >>> 0 ) + ( s1 >>> 16 );
            s2 = ( ( s2 & 0xffff ) >>> 0 ) + ( s2 >>> 16 );
        }

        s1 = ( ( s1 & 0xffff ) >>> 0 ) + ( s1 >>> 16 );
        s2 = ( ( s2 & 0xffff ) >>> 0 ) + ( s2 >>> 16 );


        let res = ( ( ( s2 << 16 ) >>> 0 | s1 ) >>> 0 ).toString(16);
        /* tslint:enable:no-bitwise */

        return this.testLength( res, slen );
    }

    testLength ( str, len ) {
        if ( str.length === len ) {
            return str;
        }
        if ( str.length > len ) {
            return str.slice( 0, len );
        }

        return Array( len - str.length ).fill( '0' ).join( '' ) + str;
    }

    fingerprint () {
        return this.checksum( this.browserData().join('###') );
    }

    browserData () {
        return [
            navigator.userAgent,
            [ screen.height, screen.width, screen.colorDepth ].join('x'),
            new Date().getTimezoneOffset(),
            !!window.sessionStorage,
            !!window.localStorage
        ];
    }
}

export const idempotence = new IdempotenceService();
