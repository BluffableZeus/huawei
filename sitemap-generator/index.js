const ApiService = require('./utils/api-service');
const DataService = require('./utils/data-service');
const SitemapGenerator = require('./utils/sitemap-generator');
const config = require('./config');
const path = require('path');

let args = {};
const requiredArgs = ['api', 'target', 'origin'];

process.argv.slice(2).forEach(param => {
    const [name, val] = param.split('=');

    args[name.replace('--', '')] = val;
});

const missedArgs =  requiredArgs.filter(arg => !args.hasOwnProperty(arg));

if(missedArgs.length > 0) {
    console.error(`Missed required arguments: [${missedArgs.join(', ')}]`);
    process.exit(128);
}

const target = path.resolve(__dirname, args.target);

const dataService = new DataService(new ApiService(args.api));
const generator = new SitemapGenerator(dataService, args.origin, config);

generator.run(target);
