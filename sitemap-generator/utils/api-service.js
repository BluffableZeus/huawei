const rx = require("rxjs");
const url = require('url');
const { URL } = require('url');
const https = require('https');

class ApiService {
    static get PATH() {
        return '/VSP/V3/';
    }

    constructor(host) {
        process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
        this.request_counter = 0;
        this.path = url.resolve(host, ApiService.PATH);
        this.uuid = `${Math.floor(Math.random()*10000000000)}`;
        this._cookies = {};

        this.resetInit();
    }
    get cookies() {
        return Object.entries(this._cookies).map(pair => pair.join('=')).join('; ')
    }

    set cookies(stringArray) {
        stringArray.map(c => c.split(';')[0].split('=')).forEach(data => {
            this._cookies[data[0]] = data[1];

        });
        console.info('cookies updated: ', this._cookies);
    }

    init(firstTry = false) {
        return new Promise((resolve, reject) => {
            firstTry || console.info('--------------------<INIT API>-------------------------');
            this.login().flatMap(data => {
                this.path = url.resolve(data.vspHttpsURL, ApiService.PATH);

                return this.authenticate();
            }).delay(2500).subscribe(res => {
                this.token = res.userToken;
                this.deviceID = res.deviceID;
                this.request_counter = 0;
                firstTry ||console.info('--------------</INIT API>----------------------');
                resolve(true);
            }, error => {
                reject(error);
                return console.error('authenticate failed', error);
            });
        });
    }

    resetInit() {
        this.ready && console.info('--------------------RELOGIN-------------------------');
        this.ready = this.init(this.ready);

    }

    login() {
        return this.request('Login', {"subscriberID":"","deviceModel":"PC"}, true);
    }

    authenticate() {
        const params = {
            "authenticateBasic": {
                "userType": "3",
                "isSupportWebpImgFormat": "0",
                "needPosterTypes": ["1","2","3","4","5","6","7"],
                "lang": "ru"
            },
            "authenticateDevice": {
                "physicalDeviceID": this.uuid,
                "terminalID": this.uuid,
                "deviceModel": "PC",
                "CADeviceInfos": [{"CADeviceType": "7","CADeviceID": this.uuid}]
            },
            "authenticateTolerant": {
                "areaCode": "",
                "templateName": "",
                "subnetID": "",
                "bossID": "",
                "userGroup": ""
            }
        };

        return this.request('Authenticate', params, true);
    }

    safe_request(method_name, data, silent = false) {
        const response = new rx.Subject();

        this.ready.then(() => {
            if (this.request_counter === 400) {
                this.resetInit();
            }

            const subsc = this.request(method_name, data, silent)
                .retryWhen(errors => {
                    console.error("error while request", errors);
                    console.log('Try to wain and retry once more');
                    return errors.delay(5000);
                })
                .subscribe(result => {
                    response.next(result);
                    response.complete();
                    subsc.unsubscribe();
                });
        });

        return response.asObservable();
    }

    request(method_name, data, silent = false) {
        this.request_counter++;

        return rx.Observable.create((observer) => {
            const params = new URL(url.resolve(this.path, method_name));
            const options = {
                hostname: params.hostname,
                path: params.pathname,
                protocol: params.protocol,
                port: params.port,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Cookie': this.cookies
                }
            };

            !silent && console.log(`Request to ${options.path} with `, data);

            const post_req = https.request(options, (res) => {
                    res.setEncoding('utf8');
                    if(res.headers['set-cookie']) {
                        this.cookies = res.headers['set-cookie'];
                    }

                    let raw = '';
                    res.on('data', data => raw += data);
                    res.on('end', () => {
                        observer.next(JSON.parse(raw));
                        observer.complete();
                    });
                })
                .on('error', error => {
                    console.log();
                    console.error(`Request to ${options.path} FAILED: `, error.message);
                    observer.error(error);
                });
            // post the data
            post_req.write(JSON.stringify(data));
            post_req.end();
        });
    }
}

module.exports = ApiService;
