const sm = require('sitemap');
const fs = require('fs');
const rx = require('rxjs');

class SitemapGenerator {

    /**
     *
     * @param {DataService} dataService
     * @param {string} basePath
     * @param config
     */
    constructor(dataService, basePath, config) {
        this.service = dataService;
        this.config = config;
        this.defaultAsset = `${basePath}/assets/img/default/my_poster.png`;

        this.sitemap = sm.createSitemap({
            hostname: basePath,
            urls: config.static
        });
    }

    run(targetDir) {
        this.categories()
            .flatMap(() => this.vods())
            .subscribe(() => {
                console.log('save sitemap =======> ', targetDir);
                fs.writeFileSync(targetDir, this.sitemap.toString());
            },err => console.log(err));
    }

    categories() {
        const resp = new rx.Subject();
        console.log('-------------------<CATEGORIES ROUTES>-------------');
        this.service.getSubjects().subscribe(
            data => {
                data.forEach(id => {
                    this.sitemap.add(Object.assign({url: `/category/${id}`}, this.config.category));
                });
            },
            err => console.error(err),
            () => {
                resp.next(true);
                resp.complete();
            });

        return resp.asObservable();
    }

    vods() {
        const resp = new rx.Subject();
        console.log('------------------------<VODs ROUTES>--------------------------');
        this.service.getVods().subscribe(
            page => {
                page.forEach(vod => {
                    let thumbnail_loc;
                    if (vod.picture && vod.picture.drafts && vod.picture.drafts[0]) {
                       thumbnail_loc = vod.picture.drafts[0];
                    } else if(vod.picture && vod.picture.posters && vod.picture.posters[0]) {
                        thumbnail_loc = vod.picture.posters[0];
                    } else {
                        thumbnail_loc = this.defaultAsset
                    }

                    this.sitemap.add(Object.assign({
                        url: this.vodRoute(vod),
                        video: [{
                            title: vod.name || 'empty',
                            description: vod.introduce || 'empty',
                            thumbnail_loc
                        }]
                    }, this.config.video));
                });
            },
            err => console.error(err),
            () => {
                resp.next(true);
                resp.complete();
            }
        );

        return resp.asObservable();
    }


    vodRoute(vod) {
        switch (Number(vod.VODType)) {
            case 1:
            case 2:
                return `/series/${vod.ID}`;
            case 3:
                const seriesId = vod.series && vod.series[0] && vod.series[0].VODID;

                if (seriesId) {
                    return `/series/${seriesId}/season/${this.getSeasonNumber(vod)}`;
                } else {
                    console.error('Could not find series for season', vod.ID);
                }
                break;
            case 0:
                if (vod.series && vod.series.length > 0) {
                    return `/video/${vod.ID}`;
                } else {
                    return `/movie/${vod.ID}`;
                }
            default:
                console.error(`Unknown VODType ${vod.VODType}, VODID: ${vod.ID}`);
        }
    }

    getSeasonNumber(vod) {
        const season = vod.brotherSeasonVODs && vod.brotherSeasonVODs.find(season => season.VOD.ID === vod.ID);
        return season ? Number(season.sitcomNO) : 0;
    }
}

module.exports = SitemapGenerator;
