const rx = require("rxjs");

class DataService {
    static get FRAME_SIZE() {
        return 50;
    }

    /**
     *
     * @param {ApiService} api
     */
    constructor(api) {
        this.api = api;
        this.categories = [];
    }

    getSubjects() {
        return this.getRecursze(page => {
            return this.api.safe_request('QueryVODSubjectList', {count: DataService.FRAME_SIZE, offset: page*DataService.FRAME_SIZE })
                .map(data => {
                    this.logWatterfall(page, data.total);
                    return data.subjects.map(subj => subj.ID);
                });
        });
    }

    getRecursze(action) {
        let page = 0;
        const response = new rx.BehaviorSubject(page);
        const obs = response.asObservable();

        return obs.flatMap(action).map(data => {
            if(data.length < DataService.FRAME_SIZE) {
                console.log(`getVods complete`);
                response.complete();
            } else {
                page++;
                response.next(page);
            }

            return data;
        });
    }

    getVods() {
        return this.getRecursze(value => this.getVodPage(value));
    }

    /**
     *
     * @param {Number} page
     * @returns {*}
     */
    getVodPage(page) {
        return this.api.safe_request('QueryVODListBySubject', { count: DataService.FRAME_SIZE, offset:page*DataService.FRAME_SIZE}, true).flatMap(resp => {
            this.logWatterfall(page, resp.total);
            return rx.Observable.zip(...resp.VODs.map(vod => this.getVodDetails(vod.ID))).delay(2500);
        });
    }

    getVodDetails(id) {
        return this.api.safe_request('GetVODDetail', {VODID: id}, true)
            .map(resp => resp.VODDetail)
            .delay(90 + Math.floor(Math.random() * 150));
    }

    logWatterfall(page, total) {
        const totalPages = Math.ceil(total/DataService.FRAME_SIZE);
        if (page === 0) {
            console.log(`found records: ${total} on ${totalPages}`);
        }

        console.log(`fetching ${page+1}/${totalPages} page`);
    }
}

module.exports = DataService;
