#Sitemap generator


##Example:
`
node sitemap-generator/index.js --target=../build/dist/sitemap.xml --origin=https://try-huawei.mediastage.tv:34443/test/huawei/portal/demo --api=https://try-huawei.mediastage.tv:34443
`
##Required args:

- api - api URL without "/VSP/V3/".
- origin - full base path of the project(origin + baseHref )
- target - target file to write result
