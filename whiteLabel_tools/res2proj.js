const argv = require('yargs').argv;
const admZip = require('adm-zip');
const path = require('path');
const fsJetpack = require('fs-jetpack');
const rimraf = require('rimraf');

const jsonToSass = require('./base/jsonToSass');
const image = require('./base/image');
const copy = require('./base/copy');
const fontJsonToSass = require('./base/fontJsonToSass');
const fontJsonToCss = require('./base/fontJsonToCss');

const resourcesInfo = fsJetpack.cwd(__dirname).read('wl_config.json', 'json').resourcesInfo;
const whiteLabelJson = fsJetpack.cwd(__dirname).read('whitelabel.json', 'json').whiteLabelJson;

const projectFSJetpack = fsJetpack.cwd(path.join(__dirname, '../../'));

const imageType = ['*.json'];
// console.log("++++++++++++++++++++++++++++++++++");
// console.log(whiteLabelJson);


const resZip = new admZip(argv._[0]);

resZip.extractAllTo(path.join(__dirname, './temp'), /*overwrite*/ true);
// console.log("++++++++++++++++++++++++++++++++++");

const colorObj = JSON.parse(fsJetpack.read(path.join(__dirname, `./temp${whiteLabelJson.color}/color.json`), {
  encoding: 'utf-8'
}));

jsonToSass(colorObj, resourcesInfo.colorPath);

// deal with image
image.res2proj(`whiteLabel_tools/temp${whiteLabelJson.image}`, resourcesInfo.imagePath);

// leading-out data json
copy.res2projDate(`whiteLabel_tools/temp${whiteLabelJson.date}`, 'src/assets/i18n/date/', 'date_');

// leading-out entry json
copy.res2proj(`whiteLabel_tools/temp${whiteLabelJson.string}`, 'src/assets/i18n', 'string_');

// leading-out error code json
copy.res2proj(`whiteLabel_tools/temp${whiteLabelJson.message}`, 'src/assets/i18n');

// export font ttf
fontJsonToSass.overWriteTtf(`whiteLabel_tools/temp${whiteLabelJson.font}`, '../ng-epg-ui/src/webtv-components/assets/font', 'ng-epg-ui/src/webtv-components/assets/font');
// modify CSS file in project accordding to the whitelabel

const enObj = JSON.parse(fsJetpack.read(path.join(__dirname, `./temp${whiteLabelJson.font}/pc_en.json`), {
  encoding: 'utf-8'
}));
const esObj = JSON.parse(fsJetpack.read(path.join(__dirname, `./temp${whiteLabelJson.font}/pc_es.json`), {
  encoding: 'utf-8'
}));
const ruObj = JSON.parse(fsJetpack.read(path.join(__dirname, `./temp${whiteLabelJson.font}/pc_ru.json`), {
  encoding: 'utf-8'
}));
const zhObj = JSON.parse(fsJetpack.read(path.join(__dirname, `./temp${whiteLabelJson.font}/pc_zh.json`), {
  encoding: 'utf-8'
}));

// console.log(enObj);

fontJsonToCss.overWriteCss(enObj, esObj, ruObj, zhObj, 'src/assets', 'ng-epg-ui/src/webtv-components/assets/css');



let pcConfig = fsJetpack.read(path.join(__dirname, './../src/app/shared/services/config.ts'));
let newVersion = fsJetpack.read(path.join(__dirname, './temp/build/build.json'), 'json').version.app_version.value;	
pcConfig = pcConfig.replace(/\'Video\s*MSA\s*.*\'/, `'${newVersion}'`);		
fsJetpack.write(path.join(__dirname, './../src/app/shared/services/config.ts'), pcConfig);

rimraf.sync('whiteLabel_tools/temp');
