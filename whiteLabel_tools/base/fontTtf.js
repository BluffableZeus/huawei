const fsJetpack = require('fs-jetpack');
const path = require('path');
const _ = require('underscore');
const os = require('os');

const projectFSJetpack = fsJetpack.cwd(path.join(__dirname, '../../'));
const fontType = ['*.ttf'];
exports.proj2res = function (paths, destPath, fontFamilyPath) {
    const fonts = {
        fontFamily: []
    };

    const allPromises = paths.map(path => {
        return copy(path, destPath, fontFamilyPath).then(font => {
            fonts.fontFamily.push.apply(fonts.fontFamily, font);
        });
    });

    return Promise.all(allPromises).then(() => fonts);
};

function copy(sourcesPath, destPath, fontFamilyPath) {
  // console.log(fontFamilyPath);
  const fontFamily = [];
  const finallyFontFamily = [];
  return projectFSJetpack.findAsync(sourcesPath, {
    matching: fontType
  }).then(files => {
    const fileCopyPromises = files.map(file => {
      const basename = path.basename(file);
      let relativePath = path.relative(sourcesPath, file);
      return projectFSJetpack.copyAsync(file, path.join(destPath, relativePath), {
        overwrite: true
      }).then(() => {
        fontFamily.push({
            name: basename,
            value: basename,
            path: relativePath
        });
        
      });

    });
    return Promise.all(fileCopyPromises).then(() => fontFamily);
  });
}
