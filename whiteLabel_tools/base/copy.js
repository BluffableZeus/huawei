const fsJetpack = require('fs-jetpack');
const path = require('path');
const _ = require('underscore');

const projectFSJetpack = fsJetpack.cwd(path.join(__dirname, '../../'));
exports.proj2res = function (msgPaths, destPath, destPrefix = '') {
  const allPromises = msgPaths.map(p => {
    const basename = path.basename(p);
    return projectFSJetpack.copyAsync(p, `${destPath}/${destPrefix}${basename}`);
  });
  return Promise.all(allPromises);
};

exports.res2proj = function (resPath, projPath, destPrefix = '') {
  const files = projectFSJetpack.find(resPath, {
    matching: '*.json'
  });
  _.each(files, file => {
    const baseName = path.basename(file);
    projectFSJetpack.copy(file, path.join(projPath, baseName.match(/\w*_(\w*)\./)[1], baseName.slice(destPrefix.length)), {
      overwrite: true
    });
  });
};

exports.res2projDate = function (resPath, projPath, destPrefix = '') {
  const files = projectFSJetpack.find(resPath, {
    matching: '*.json'
  });
  _.each(files, file => {
    const baseName = path.basename(file);
    
    projectFSJetpack.copy(file, path.join(projPath, baseName), {
      overwrite: true
    });
  });
};
