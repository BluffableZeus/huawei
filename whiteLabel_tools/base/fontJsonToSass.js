const fsJetpack = require('fs-jetpack');
const fs = require('fs');
const path = require('path');
const _ = require('underscore');
const rimraf = require('rimraf');

var fsCopy = require('fs-extra');

const projectFSJetpack = fsJetpack.cwd(path.join(__dirname, '../../'));

const imageType = ['*.ttf', '*.TTF'];
exports.overWriteTtf = function (resPath, projPaths, finPath) {
    // 清空项目字体文件夹
    emptyDir(projPaths, resPath, finPath);
    // 将白标更换的字体放入项目字体文件夹
    // copyFromRes(projPaths, resPath, finPath);

};

function copyFromRes(dirPath, resPath, finPath) {
  
  const files = projectFSJetpack.find(resPath, {
    matching: imageType
  });
  // console.log(files);
   _.each(files, file => {
      const baseName = path.basename(file);
      projectFSJetpack.copy(file, path.join(finPath, baseName), {
        overwrite: true
      });
   });
}

function emptyDir(fileUrl, resPath, finPath){
    rimraf.sync(fileUrl);
    copyFromRes(fileUrl, resPath, finPath);
}

