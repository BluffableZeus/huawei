const fsJetpack = require('fs-jetpack');
const fs = require('fs');
const path = require('path');
const _ = require('underscore');

var fsCopy = require('fs-extra');

const projectFSJetpack = fsJetpack.cwd(path.join(__dirname, '../../'));

const imageType = ['*.json'];
exports.overWriteCss = function (enObj, esObj, ruObj, zhObj, projAppPath, proUiPath) {
    let fontFamilyEn = enObj['font-family'];
    let fontFamilyEs = esObj['font-family'];
    let fontFamilyRu = ruObj['font-family'];
    let fontFamilyZh = zhObj['font-family'];
    let fontSizeEn = enObj['font-size'];
    let fontSizeEs = esObj['font-size'];
    let fontSizeRu = ruObj['font-size'];
    let fontSizeZh = zhObj['font-size'];
    // console.log(fontFamilyEn);
    writeMixinCss(fontSizeEn, fontFamilyEn, proUiPath, 'en');
    writeMixinCss(fontSizeEs, fontFamilyEs, proUiPath, 'es');
    writeMixinCss(fontSizeRu, fontFamilyRu, proUiPath, 'ru');
    writeMixinCss(fontSizeZh, fontFamilyZh, proUiPath, 'zh');
    writei18JSON(fontSizeEn, fontFamilyEn, projAppPath, 'en');
    writei18JSON(fontSizeEs, fontFamilyEs, projAppPath, 'es');
    writei18JSON(fontSizeRu, fontFamilyRu, projAppPath, 'ru');
    writei18JSON(fontSizeZh, fontFamilyZh, projAppPath, 'zh');
};

function writeMixinCss(fontSize, fontFamily, proUiPath, type){
    // console.log(fontFamily[0]);
    let mixin_enFont = '@font-face {font-family:' + fontFamily[0].value + ';src: url(../font/' + fontFamily[0].path + ')}'
        + '@font-face {font-family:' + fontFamily[1].value + ';src: url(../font/' + fontFamily[1].path + ')}';
    let mixin_enCss = [];
    mixin_enCss.push(mixin_enFont);
    let mixin_enClass = '';
    for (let i = 0; i < 8; i ++ ) {
        let family;
        if (fontSize[i].family === fontFamily[0].name) {
            family = fontFamily[0].value;
        }
        if (fontSize[i].family === fontFamily[1].name) {
            family = fontFamily[1].value;
        }
        mixin_enClass = '.title-t1' + (i + 1) + '{font-size:' + fontSize[i].size + ';font-family: ' + family + ';}';
        mixin_enCss.push(mixin_enClass);
    }
    for (let i = 8; i < fontSize.length; i ++ ) {
        let family;
        if (fontSize[i].family === fontFamily[0].name) {
            family = fontFamily[0].value;
        }
        if (fontSize[i].family === fontFamily[1].name) {
            family = fontFamily[1].value;
        }
        mixin_enClass = '.body-t' + (20 + i - 8) + '{font-size:' + fontSize[i].size + ';font-family: ' + family + ';}';
        mixin_enCss.push(mixin_enClass);
    }
    mixin_enCss = mixin_enCss.join(" ");

    if (type === 'en') {
        projectFSJetpack.write(proUiPath + '/mixin_en.css', mixin_enCss)
    }else if (type === 'zh') {
        projectFSJetpack.write(proUiPath + '/mixin_zh.css', mixin_enCss);
    }else if (type === 'ru') {
        projectFSJetpack.write(proUiPath + '/mixin_ru.css', mixin_enCss);
    }else {
        projectFSJetpack.write(proUiPath + '/mixin_es.css', mixin_enCss);
    }
}

function writei18JSON(fontSize, fontFamily, projAppPath, type){
    let finallyJson = {
        "F01": fontFamily[0],
        "F02": fontFamily[1]
    }
    // console.log(finallyJson);
    if (type === 'en') {
        fsJetpack.write('../' + projAppPath + '/font_en.json', finallyJson);
    }else if (type === 'zh') {
        fsJetpack.write('../' + projAppPath + '/font_zh.json', finallyJson);
    }else if (type === 'ru') {
        fsJetpack.write('../' + projAppPath + '/font_ru.json', finallyJson);
    }else {
        fsJetpack.write('../' + projAppPath + '/font_es.json', finallyJson);
    }
}


