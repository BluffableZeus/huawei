const fsJetpack = require('fs-jetpack');
const path = require('path');

const projectFSJetpack = fsJetpack.cwd(path.join(__dirname, '../../'));
exports.parseFont = function (filePath) {
  const result = [];
  const urlResult = [];

  return projectFSJetpack.readAsync(filePath).then(content => {
    // console.log(content);
    const lines = content.split(".");
    // console.log(lines);

    lines.forEach((line, index) => {
      // console.log(line);
      const lineDeleteleft = line.replace(/{/g,',');
      const lineDeleteMiddle = lineDeleteleft.replace(/;/g,',');
      const lineDeleteRight = lineDeleteMiddle.replace(/}/g,'');
      const lineDeleteSpace = lineDeleteRight.replace(/\r?\n/g,'');

      const lastLine = lineDeleteSpace.split(" ");
    //  console.log(lastLine);
      let finallyLine = [];
      for(let i = 0; i < lastLine.length; i++){
          if (lastLine[i] != '' && lastLine[i] != ',') {
            finallyLine.push(lastLine[i]);
          }
      }
      // console.log(finallyLine);
      if (finallyLine[0] && finallyLine[2] && finallyLine[4]) {
        const name = finallyLine[0];
        let size = finallyLine[2].replace(/,/g,'');
        const family = finallyLine[4].replace(/,/g,'');
        const description = finallyLine[0];
        const fontResult = {
          name: name,
          size: size,
          family: family,
          description: description
        }
        result.push(fontResult);
      }else if (finallyLine.length === 1 && finallyLine !== []) {
        urlResult.push(finallyLine);
      }
      
    });
    // console.log(urlResult[0][5]);
    return {result, urlResult};
  });
};

