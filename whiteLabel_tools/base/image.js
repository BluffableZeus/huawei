const fsJetpack = require('fs-jetpack');
const path = require('path');
const _ = require('underscore');
const os = require('os');

const projectFSJetpack = fsJetpack.cwd(path.join(__dirname, '../../'));
const imageType = ['*.png', '*.jpg', '*.jpeg', '*.gif', '*.ico', '!profile*'];
exports.proj2res = function (paths, destPath) {
  const images = {
    image: []
  };

  const allPromises = paths.map(path => {
    return copy(path, destPath).then(image => {
      images.image.push.apply(images.image, image);
    });
  });

  return Promise.all(allPromises).then(() => images);
};

exports.res2proj = function (resPath, projPaths) {
  _.each(projPaths, dirPath => {
    copyFromRes(dirPath, resPath);
  });
};

function copyFromRes(dirPath, resPath) {
  const files = projectFSJetpack.find(dirPath, {
    matching: imageType
  });
  let filterFiles = filter(files);
  _.each(filterFiles, file => {
    let relativePath = path.relative(dirPath, file);
    projectFSJetpack.copy(path.join(resPath, relativePath), file, {
      overwrite: true
    });
  });
}

function copy(sourcesPath, destPath) {
  const image = [];
  return projectFSJetpack.findAsync(sourcesPath, {
    matching: imageType
  }).then(files => {
    let filterFiles = filter(files);
    const fileCopyPromises = filterFiles.map(file => {
      const basename = path.basename(file);
      let relativePath = path.relative(sourcesPath, file);
      return projectFSJetpack.copyAsync(file, path.join(destPath, relativePath), {
        overwrite: true
      }).then(() => {
        let value = relativePath.split(path.sep);
        value.unshift('/');
        value.pop();
        image.push({
          value: value.join('/').replace('//', '/'),
          name: basename,
          description: `description of ${basename}`
        });
      });
    });
    return Promise.all(fileCopyPromises).then(() => image);
  });
}

function filter(files) {
  let filterImgString = fsJetpack.cwd(__dirname).read('pc_matched_file_list.txt', '');
  let result = os.platform();
  let filterImgMap;
  if (result === 'win32') {
    filterImgMap = filterImgString.split('\r\n');
  }else {
    filterImgMap = filterImgString.split('\n');
  }
  let maps = _.filter(files, function(file) {
      return _.filter(filterImgMap, function(filterImg) {
          if (file.toString().indexOf(filterImg.toString()) !== -1) {
              return true;
          }
      })[0];
  });

  return maps;
}
