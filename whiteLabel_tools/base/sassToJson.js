const fsJetpack = require('fs-jetpack');
const path = require('path');

const projectFSJetpack = fsJetpack.cwd(path.join(__dirname, '../../'));
exports.parseColor = function (filePath) {
  const result = {
    color: []
  };
  const extraInfo = {
    color: []
  };

  return projectFSJetpack.readAsync(filePath).then(content => {
    const lines = content.split(/\r?\n/);

    lines.forEach((line, index) => {
      const color = parseLine(line, result, extraInfo);
      if (color) {
        color.description = lines[index - 1].replace(/\s*\/\/\s*/, '');
        result.color.push(color);
        extraInfo.color.push({
          line,
          lineNumber: index + 1
        });
      }
    });
    return {
      result,
      extraInfo,
      filePath: path.relative(process.cwd(), filePath)
    };
  });
};

function parseLine(line, result) {
  const sassDeclarationReg = /\$(.*)\s*:\s*(.*);/;
  const valueVarReg = /\$(\S*)(?=,)/;
  const hexReg = /^#[0-9a-f]{3,8}/i;
  const rgbaReg = /^rgba\s*\((.*)\)/i;

  const matchs = sassDeclarationReg.exec(line);
  if (matchs) {

    const name = matchs[1].trim();
    let value = matchs[2].trim();

    const isColorValue = hexReg.test(value) || rgbaReg.test(value);

    // Handle only color dependent variables
    if (isColorValue) {
      value = value.replace(/\s*!default\s*/, '');

      value = value.replace(valueVarReg, (all, varName) => {
        varName = varName.trim();
        const c = result.color.find(item => item.name === varName);
        return c.value.slice(3);
      });
      value = value.replace(rgbaReg, (all, list) => {
        list = list.split(/\s*,\s*/);

        const opacity = list[list.length - 1];
        const vs = [];

        // Modify the RGBA value to #AARRGGBB form, the first two are transparent, consistent with the Ott color format
        vs[0] = prependZero(parseInt(255 * opacity, 10).toString(16));
        if (list.length === 4) {
          for (let i = 0; i < 3; i++) {
            vs[i + 1] = prependZero(parseInt(list[i], 10).toString(16));
          }
        } else if (list.length === 2) {
          for (let i = 0; i < 3; i++) {
            vs[i + 1] = list[0].length === 4 ? list[0][i + 1] + list[0][i + 1] : list[0][2 * i] + list[0][2 * i + 1];
          }
        }
        return `#${vs.join('')}`;
      });

      // Converts the final color value to #AARRGGBB 8 digit form
      value = value.replace(hexReg, all => {
        switch (all.length) {
          case 4:
            return `#ff${all[1]}${all[1]}${all[2]}${all[2]}${all[3]}${all[3]}`;
          case 7:
            return `#ff${all.slice(1)}`;
          default:
            return all;
        }
      });

      return {
        name,
        value
      };
    }
  }
}

function prependZero(str) {
  return str.length === 1 ? `0${str}` : str;
}
