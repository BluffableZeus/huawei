const nodePath = require('path');
const fsJetpack = require('fs-jetpack');
const _ = require('underscore');

const projectFSJetpack = fsJetpack.cwd(nodePath.join(__dirname, '../../'));
module.exports = function (colorObj, sassFilePaths) {
  const sassContents = {};
  let colorList = [];
  colorObj.basicColor.color.forEach(color => {
      colorList.push(color);
  });
  colorObj.textColor.color.forEach(color => {
      colorList.push(color);
  });
  colorObj = { color: colorList };
  _.each(colorObj.color, item => {
    _.each(sassFilePaths, path => {
      if (!sassContents[path]) {
        sassContents[path] = projectFSJetpack.read(path).split(/\r?\n/);
      }
      _.each(sassContents, lines => {
        _.each(lines, (text, index, list) => {
          if (text.indexOf(`$${item.name}:`) === 0 || text.indexOf(`$${item.name} :`) === 0) {
            const opacity = parseInt(item.value.slice(1, 3), 16) / 255;
            const redValue = parseInt(item.value.slice(3, 5), 16);
            const greenValue = parseInt(item.value.slice(5, 7), 16);
            const blueValue = parseInt(item.value.slice(7, 9), 16);
            list[index] = `$${item.name}: rgba(${redValue}, ${greenValue}, ${blueValue}, ${opacity}) !default;`;
          }
        });
      });
    });
  });

  _.each(sassFilePaths, path => {
    projectFSJetpack.write(path, sassContents[path].join('\r\n'));
  });
};
