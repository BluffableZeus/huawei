const path = require('path');
const _ = require('underscore');
const fsJetpack = require('fs-jetpack');
const rimraf = require('rimraf');
const gulp = require('gulp');
const zip = require('gulp-zip');
const moment = require('moment');

const versionDate = moment().format('YYYYMMDD');
const config = fsJetpack.cwd(__dirname).read('../src/app/shared/services/config.ts');
const MATCH_VERSION_REGEXP = new RegExp(/\'Version\'\:\s*\'Video\s*MSA\s*.*\'/g);
let tempVersion = config.match(MATCH_VERSION_REGEXP);
let crpVersion = tempVersion[0].replace(/\'Version\'\:\s*\'Video\s*MSA\s*/, '');
crpVersion = crpVersion.replace(/\'/g, '');


const MATCH_LAN_REGEXP = new RegExp(/\'SupportLanguageList\'\:\s*\[.*\]/g);
let tempData = config.match(MATCH_LAN_REGEXP);
let lan = tempData[0].replace(/\'SupportLanguageList\'\:\s*/, '');
lan = lan.replace(/\'/g, '');
let firstLanguage = lan.replace(/\[/g, '').replace(/\,.*/g, '');
let secondLanguage = lan.substring(5, 7);
let thirdLanguage = lan.substring(9, 11);
let fourthLanguage = lan.replace(/.*\,\s*/g, '').replace(/\]/g, '');

const sassToJson = require('./base/sassToJson');
const fontSassToJson = require('./base/fontSassToJson');
const image = require('./base/image');
const fontTtf = require('./base/fontTtf');
const copy = require('./base/copy');

const resourcesInfo = fsJetpack.cwd(__dirname).read('wl_config.json', 'json').resourcesInfo;
const whiteLabelJson = fsJetpack.cwd(__dirname).read('whitelabel.json', 'json').whiteLabelJson;
let buildJson = fsJetpack.cwd(__dirname).read('build.json', 'json');

// temporary catalogue
const tempPath = 'temp';


// delete temporary catalogue
rimraf.sync(tempPath);

// derive color variable
const colorParsePromises = resourcesInfo.colorPath.map(path => sassToJson.parseColor(path));

const colorPromise = Promise.all(colorParsePromises).then(colorInfos => {
  // console.log(colorInfos);
  let colorResult = {};
  // Classify colors
  colorInfos.forEach(info => {
    let basicColorList = [];
    let textColorList = [];
    info.result.color.forEach(color => {
      if (color.name.indexOf('basic') > -1) {
        basicColorList.push(color);
      } else if (color.name.indexOf('text') > -1) {
        textColorList.push(color);
      }
    });
    colorResult = {
      basicColor: { color: basicColorList },
      textColor: { color: textColorList }
    }
  });

  return fsJetpack.writeAsync(`${tempPath}/color/color.json`, colorResult);
});

// derive font json, font ttf
const fontTtfPromises = fontTtf.proj2res(resourcesInfo.fontFamilayPath, `${tempPath}/font`, resourcesInfo.fontFamillyStrPath).then(fontTtf => {

  const fontParsePromises = resourcesInfo.fontSizePath.map(path => fontSassToJson.parseFont(path));

    const colorPromise = Promise.all(fontParsePromises).then(fontInfos => {

      const famillyEn = fontInfos[0]['result'][0];
      const famillyZh = fontInfos[1]['result'][0];
      const famillyRu = fontInfos[2]['result'][0];
      const famillyEs = fontInfos[3]['result'][0];

      const famillyEn2 = fontInfos[0]['result'][1];
      const famillyZh2 = fontInfos[1]['result'][1];
      const famillyRu2 = fontInfos[2]['result'][1];
      const famillyEs2 = fontInfos[3]['result'][1];

      famillyEn['size'] = famillyEn['size'].split("'")[1];
      famillyZh['size'] = famillyZh['size'].split("'")[1];
      famillyRu['size'] = famillyRu['size'].split("'")[1];
      famillyEs['size'] = famillyEs['size'].split("'")[1];

      famillyEn2['size'] = famillyEn2['size'].split("'")[1];
      famillyZh2['size'] = famillyZh2['size'].split("'")[1];
      famillyRu2['size'] = famillyRu2['size'].split("'")[1];
      famillyEs2['size'] = famillyEs2['size'].split("'")[1];

      const urlEn = fontInfos[0]['urlResult'][0][0].replace("\/font\/", "") + '.ttf';
      const urlZh = fontInfos[1]['urlResult'][0][0].replace("\/font\/", "") + '.ttf';
      const urlRu = fontInfos[2]['urlResult'][0][0].replace("\/font\/", "") + '.ttf';
      const urlEs = fontInfos[3]['urlResult'][0][0].replace("\/font\/", "") + '.ttf';

      const urlEn2 = fontInfos[0]['urlResult'][1][0].replace("\/font\/", "") + '.ttf';
      const urlZh2 = fontInfos[1]['urlResult'][1][0].replace("\/font\/", "") + '.ttf';
      const urlRu2 = fontInfos[2]['urlResult'][1][0].replace("\/font\/", "") + '.ttf';
      const urlEs2 = fontInfos[3]['urlResult'][1][0].replace("\/font\/", "") + '.ttf';


      fontInfos[0]['result'].splice(0, 2);
      fontInfos[1]['result'].splice(0, 2);
      fontInfos[2]['result'].splice(0, 2);
      fontInfos[3]['result'].splice(0, 2);

      const fontEn = {fontSize: fontInfos[0]['result']};
      const fontZh = {fontSize: fontInfos[1]['result']};
      const fontRu = {fontSize: fontInfos[2]['result']};
      const fontEs = {fontSize: fontInfos[3]['result']};

      const fontFamillyEn = {
        fontFamily: []
      }
      const fontFamillyZh = {
        fontFamily: []
      }
      const fontFamillyRu = {
        fontFamily: []
      }
      const fontFamillyEs = {
        fontFamily: []
      }

      fontFamillyEn.fontFamily.push({name: 'F01', value: famillyEn.size, path: urlEn}, {name: 'F02', value: famillyEn2.size, path: urlEn2});
      fontFamillyZh.fontFamily.push({name: 'F01', value: famillyZh.size, path: urlZh}, {name: 'F02', value: famillyEn2.size, path: urlEn2});
      fontFamillyRu.fontFamily.push({name: 'F01', value: famillyRu.size, path: urlRu}, {name: 'F02', value: famillyEn2.size, path: urlEn2});
      fontFamillyEs.fontFamily.push({name: 'F01', value: famillyEs.size, path: urlEs}, {name: 'F02', value: famillyEn2.size, path: urlEn2});
      _.each(fontFamillyEn['fontFamily'], (list, index) => {
          _.each(fontEn['fontSize'], (list1, index1) => {
              if (list['value'] === list1['family']) {
                list1['family'] = list['name'];
              }
          });
      });
      _.each(fontFamillyZh['fontFamily'], (list, index) => {
          _.each(fontZh['fontSize'], (list1, index1) => {
              if (list['value'] === list1['family']) {
                list1['family'] = list['name'];
              }
          });
      });
      _.each(fontFamillyRu['fontFamily'], (list, index) => {
          _.each(fontRu['fontSize'], (list1, index1) => {
              if (list['value'] === list1['family']) {
                list1['family'] = list['name'];
              }
          });
      });
      _.each(fontFamillyEs['fontFamily'], (list, index) => {
          _.each(fontEs['fontSize'], (list1, index1) => {
              if (list['value'] === list1['family']) {
                list1['family'] = list['name'];
              }
          });
      });

      let finallyFontEn = Object.assign(fontFamillyEn, fontEn);
      let finallyFontZh = Object.assign(fontFamillyZh, fontZh);
      let finallyFontRu = Object.assign(fontFamillyRu, fontRu);
      let finallyFontEs = Object.assign(fontFamillyEs, fontEs);

      finallyFontEn['font-family'] = finallyFontEn.fontFamily;
      finallyFontEn['font-size'] = finallyFontEn.fontSize;
      delete finallyFontEn.fontFamily;
      delete finallyFontEn.fontSize;
      finallyFontZh['font-family'] = finallyFontZh.fontFamily;
      finallyFontZh['font-size'] = finallyFontZh.fontSize;
      delete finallyFontZh.fontFamily;
      delete finallyFontZh.fontSize;
      finallyFontRu['font-family'] = finallyFontRu.fontFamily;
      finallyFontRu['font-size'] = finallyFontRu.fontSize;
      delete finallyFontRu.fontFamily;
      delete finallyFontRu.fontSize;
      finallyFontEs['font-family'] = finallyFontEs.fontFamily;
      finallyFontEs['font-size'] = finallyFontEs.fontSize;
      delete finallyFontEs.fontFamily;
      delete finallyFontEs.fontSize;

      fsJetpack.writeAsync(`${tempPath}/font/pc_en.json`, finallyFontEn);
      fsJetpack.writeAsync(`${tempPath}/font/pc_zh.json`, finallyFontZh);
      fsJetpack.writeAsync(`${tempPath}/font/pc_ru.json`, finallyFontRu);
      fsJetpack.writeAsync(`${tempPath}/font/pc_es.json`, finallyFontEs);
    })
});

// derive date json

let datePath = [`${resourcesInfo.datePath[0]}/date_${firstLanguage}.json`, `${resourcesInfo.datePath[0]}/date_${secondLanguage}.json`, `${resourcesInfo.datePath[0]}/date_${thirdLanguage}.json`, `${resourcesInfo.datePath[0]}/date_${fourthLanguage}.json`];
const datePromise = copy.proj2res(datePath, `${tempPath}/date`, '');

// derive image
const imagePromise = image.proj2res(resourcesInfo.imagePath, `${tempPath}/image`).then(images => {
  return fsJetpack.writeAsync(`${tempPath}/image/image.json`, images);
});


// Export entry file's path
let stringPath = [`${resourcesInfo.stringPath[0]}/${firstLanguage}/${firstLanguage}.json`, `${resourcesInfo.stringPath[0]}/${secondLanguage}/${secondLanguage}.json`, `${resourcesInfo.stringPath[0]}/${thirdLanguage}/${thirdLanguage}.json`, `${resourcesInfo.stringPath[0]}/${fourthLanguage}/${fourthLanguage}.json`];
// Export entry file
// console.log(stringPath);
const stringPromise = copy.proj2res(stringPath, `${tempPath}/string`, 'string_');


// Export error code file's path
let messagePath = [`${resourcesInfo.messagePath[0]}/${firstLanguage}/message_${firstLanguage}.json`, `${resourcesInfo.stringPath[0]}/${secondLanguage}/message_${secondLanguage}.json`, `${resourcesInfo.stringPath[0]}/${thirdLanguage}/message_${thirdLanguage}.json`, `${resourcesInfo.stringPath[0]}/${fourthLanguage}/message_${fourthLanguage}.json`];
// Export error code file
const messagePromise = copy.proj2res(messagePath, `${tempPath}/message`);


let version = tempVersion[0].replace(/\'Version\'\:\s*/, '').replace(/\'/g, '');
buildJson.version.app_version.value = version;
const buildPromise = fsJetpack.writeAsync(`${tempPath}/build/build.json`, buildJson);
Promise.all([colorPromise, imagePromise, stringPromise, messagePromise, buildPromise, datePromise]).then(() => {
  return fsJetpack.writeAsync(`${tempPath}/whiteLabel.json`, whiteLabelJson);
}).then(() => {
  genZipFile();
}).catch(e => {
  console.log('Promise.all rejected', e);
});

function genZipFile() {
  let destPath = resourcesInfo.destPath;
  let name = `WhiteLabel_PC_${crpVersion}_${versionDate}.crp`;
  destPath = destPath.split('/');
  destPath.pop();
  destPath = destPath.join('/');

  gulp.task('zip', function () {
    return gulp.src(`${tempPath}/**`)
      .pipe(zip(name, {
        compress: true
      }))
      .pipe(gulp.dest(destPath));
  });

  gulp.start('zip', function () {
    rimraf.sync(tempPath);
  });
}
