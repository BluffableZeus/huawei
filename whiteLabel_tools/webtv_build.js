const exec = require('child_process').exec;
const spawn = require('child_process').spawn;
const argv = require('yargs').argv;
const path = require('path');
const jetpack = require('fs-jetpack');

const cmd = 'npm run build';

exec('find ' + path.join(__dirname, '../node_modules/.bin') + ' -type f | grep  -v \'cmd$\' | xargs chmod u+x', function () {
  console.log('cli path: ' + cmd);
  const buildCmd = spawn('npm', ['run', 'build'], {
    cwd: path.join(__dirname, '../')
  });

  buildCmd.stdout.on('data', function (data) {
    console.log(`stdout: ${data}`);
  });
  buildCmd.stderr.on('data', (data) => {
    console.log(`stderr: ${data}`);
  });
  buildCmd.on('close', (code) => {
    console.log(`build result: ${code}`);
    const files = jetpack.find(path.join(__dirname, '../build'), {
      matching: ['V6WEBTV.zip']
    });
    if (files && files.length) {
      jetpack.copy(files[files.length - 1], argv._[0]);
    }
  });
});


