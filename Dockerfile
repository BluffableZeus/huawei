FROM node:alpine as builder
RUN apk add --no-cache paxctl && paxctl -cm `which node`
RUN apk add --update --no-cache git python make g++
WORKDIR /app
COPY package.json /app
COPY package-lock.json /app
RUN npm install
COPY . /app
RUN npm run build:prod

FROM rhrn/docker-nginx-spa
COPY --from=builder /app/build/dist /usr/share/nginx/html
