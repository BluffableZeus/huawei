require('style-loader!./styles.scss')
// require('style-loader!../ng-epg-ui/src/webtv-components/assets/css/common-variables.scss');

import { platformBrowser } from '@angular/platform-browser'
import { enableProdMode } from '@angular/core'
import { environment } from './environments/environment'
import { AppModule } from './app/'
import { init } from './init'
/**
 * Disable Angular's development mode, which turns off assertions and other
 * checks within the framework.
 *
 * One important assertion this disables verifies that a change detection pass
 * does not result in additional changes to any bindings (also known as
 * unidirectional data flow).
 */
if (environment.production) {
  enableProdMode()
}
/**
 * initialize the page when enter the webtv
 */
init()
/**
 * Generated bundle index
 */
platformBrowser().bootstrapModule(AppModule)
