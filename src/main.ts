import { platformBrowserDynamic } from '@angular/platform-browser-dynamic'
import { enableProdMode } from '@angular/core'
import { environment } from './environments/environment'
import { AppModule } from './app/'
import { init } from './init'
/**
 * Disable Angular's development mode, which turns off assertions and other
 * checks within the framework.
 *
 * One important assertion this disables verifies that a change detection pass
 * does not result in additional changes to any bindings (also known as
 * unidirectional data flow).
 */
if (environment.production) {
  enableProdMode()
}
/**
 * initialize the page when enter the webtv
 */
init()
/**
 * Entry point for all public APIs of the platform-browser-dynamic package.
 */
platformBrowserDynamic().bootstrapModule(AppModule)
