(function () {
    var root = this;
    var ShareChain = function () {
    };
    ShareChain.prototype = {
        vspUrl: '',
        vspHttpsUrl: '',
        csrfToken: '',
        /**
         * Before and after the data exchange, through Ajax implementation
         */
        ajax: function(req) {
            var retDfd = $.Deferred();
            var url = req.url;
            var type = (req.type || 'POST').toUpperCase();
            var settings = {
                contentType: 'application/json;charset=utf-8',
                url: url,
                dataType: 'json',
                type: type,
                success: function(response) {
                    if (typeof response == "string") {
                        try {
                            response = JSON.parse(response);
                        } catch (exception) {
                            retDfd.resolve({});
                        }
                    }
                    if(response && response.result && response.result.retCode === '000000000') {
                        retDfd.resolve(response);
                    } else {
                        retDfd.reject(response);
                    }
                    retDfd.resolve(response);
                },
                headers: req.headers || {},
                error: function(jqXHR, textStatus) {
                    retDfd.reject({
                        resultCode: 'http' + jqXHR.status,
                        description: textStatus || jqXHR.statusText
                    });
                }
            };
            if(!!this.csrfToken){
                settings.headers.X_CSRFToken = this.csrfToken;
            }
            settings.data = JSON.stringify(req.data);
            if (false === req.async) {
                settings.async = false;
            }
            $.ajax(settings);
            return retDfd;
        },
        /**
         * Computer, mobile phone models can not load the device type
         */
        IsPC: function()  {
            var userAgentInfo = navigator.userAgent;
            var ua = userAgentInfo.toLowerCase();
            var Agents = ["Android", "iPhone", "iPad"];
            for (var v = 0; v < Agents.length; v++) {
                if (userAgentInfo.indexOf('Windows') > 0) {
                    var VODID = location.href.split('?id=')[1];
                    var href = location.href.split('share/index.html')[0] + 'index.html#/voddetail/' + VODID;
                    location.href = href;
                } else {
                    document.write("<link rel='stylesheet' type='text/css' href='./css/"+ 'phone' +".css'/>");
                    if(userAgentInfo.indexOf('iPhone') > 0) {
                        if (navigator.userAgent.indexOf('Mac') > -1 && (ua.match(/MicroMessenger/i) != "micromessenger") &&
                            (ua.match(/WeiBo/i) != "weibo") ) {
                            document.write("<link rel='stylesheet' type='text/css' href='./css/"+ 'iosqq' +".css'/>");
                        }
                    }
                }
            }
        },
        /**
         * Device ID field generation in authentication interfaces
         */
        setCookie: function(name, value, iDay) {
            sessionStorage.setItem('share_chain_uuid_cookie', value);
        },
        getCookie: function(name) {
            return sessionStorage.getItem('share_chain_uuid_cookie') || '';
        },
        securityRandomString: function(maxLength) {
            var maxLength = maxLength;
            if (maxLength === 0) { maxLength = 128; }
            var randomIntArray = new Uint32Array(Math.ceil(maxLength / 7));
            var crypto = window.msCrypto || window.crypto;
            crypto.getRandomValues(randomIntArray);
            var randomStringArray = _.map(randomIntArray, function(i) {
                return i.toString(36);
            });
            return randomStringArray.join('').substring(0, maxLength);
        },
        setDeviceInfo: function() {
            var uuid = this.getCookie('share_chain_uuid');
            if (!uuid) {
                uuid = this.securityRandomString(8) + '-' + this.securityRandomString(4) + '-' +
                        this.securityRandomString(4) + '-' + this.securityRandomString(4) + '-' + this.securityRandomString(12);
                this.setCookie('share_chain_uuid', uuid.toUpperCase(), 36500);
            }
            var authenticateDevice = {};
            authenticateDevice['physicalDeviceID'] = uuid;
            authenticateDevice['terminalID'] = uuid;
            authenticateDevice['deviceModel'] = 'PC';
            return authenticateDevice;
        },
        createAuthenticateTolerant: function() {
            var authenticateTolerant = {};
            var areaCode = sessionStorage.getItem('areaid');
            var templateName = sessionStorage.getItem('templateName');
            var subnetID = sessionStorage.getItem('subnetId');
            var bossID = sessionStorage.getItem('bossId');
            var userGroup = sessionStorage.getItem('UserGroupNMB');

            _.extend(authenticateTolerant, {
                areaCode: areaCode && areaCode !== '' ? areaCode : '',
                templateName: templateName && templateName !== '' ? templateName : '',
                subnetID: subnetID && subnetID !== '' ? subnetID : '',
                bossID: bossID && bossID !== '' ? bossID : '',
                userGroup: userGroup && userGroup !== '' ? userGroup : ''
            });
            return authenticateTolerant;
        },
        /**
         * Visitor login and authentication
         */
        guestLogin: function() {
            var guestLoginReq = {
                'url' : '/VSP/V3/Login',
                'type': 'POST',
                'data': { subscriberID: '' }
            }

            var guestAuthReq = {
                'url' : '/VSP/V3/Authenticate',
                'type': 'POST',
                'data': {
                    authenticateBasic: {
                        userType: '3',
                        isSupportWebpImgFormat: '0',
                        needPosterTypes: ['1', '2', '3', '6']
                    },
                    authenticateDevice: this.setDeviceInfo()
                 }
            }
            var authenticateTolerant = this.createAuthenticateTolerant();
            if (!_.isEmpty(authenticateTolerant)) {
                _.extend(guestAuthReq, {
                    authenticateTolerant: authenticateTolerant
                });
            }
            var self = this;
            return this.ajax(guestLoginReq).then(function(loginresp){

                // Dirty hack
                loginresp.vspURL = 'http://tv-test.rhrn.ru:80';
                loginresp.vspHttpsURL = 'https://tv-test.rhrn.ru:443';
                self.vspUrl = loginresp.vspURL;
                self.vspHttpsUrl = loginresp.vspHttpsURL;
                return self.ajax(guestAuthReq).then(function(resp){
                    self.csrfToken = resp.csrfToken;
                    return resp;
                });
            });
        },
        /**
         * Visitors get details of the current film
         */
        getVodDetail: function(req){
            var vodDetailReq = {
                'url' : '/VSP/V3/GetVODDetail',
                'type': 'POST',
                'data': req
            }
            return this.ajax(vodDetailReq).then(function(detailResp) {
                 return detailResp;
            });
        },
        vodPlayCheckLock: function(data, VODID, mediaID, checkLock, isReturnProduct) {
            var self = this;
            var retDfd = $.Deferred();
            var req = {
                'url' : '/VSP/V3/PlayVOD',
                'type': 'POST',
                'data': {
                    VODID: VODID,
                    mediaID: mediaID || '',
                    checkLock: checkLock || {
                        checkType: '0'
                    },
                    isReturnProduct: isReturnProduct
                }
            };
            return this.ajax(req).then(function(resp) {// geographical position, authentication, subscribe ,all above meet the condition
                return retDfd.resolve(resp);
            }, function(resp) {// geographical position, authentication, subscribe ,one of them not meet the condition
                return retDfd.reject(resp);
            });
        },

        searchContent: function(req) {
            var searchReq = {
                'url' : '/VSP/V3/SearchContent',
                'type': 'POST',
                'data': req
            }
            return this.ajax(searchReq).then(function(searchResp) {
                 return searchResp;
            });
        },

        queryCustomizeConfig: function() {
            var req = {
                'url' : '/VSP/V3/QueryCustomizeConfig',
                'type': 'POST',
                'data': {
                    queryType: '0,2,5,7'
                }
            }
            return this.ajax(req).then(function(resp) {
                var data ;
                if (resp['configurations']) {
                    data = _.find(resp['configurations'], function(item) {
                        return item['cfgType'] === '0';
                    });
                }
                return data;
            });
        }
    }
    shareChain = new ShareChain();

    if (typeof exports !== 'undefined') {
        if (typeof module !== 'undefined' && module.exports) {
            exports = module.exports = shareChain;
        }
        exports.shareChain = shareChain;
    } else {
        root.shareChain = shareChain;
    }
}.call(jQuery, this));
