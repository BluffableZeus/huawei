define(function(require) {
    var ko = require('lib/knockout-3.1.0');
    return {
        start: function() {
            var self = this;
            this.ko = ko;
            this.isShowIntroduceMore = false;
            this.titlename = ko.observable('');
            this.averageScore = ko.observable('');
            this.averageScoreA = ko.observable('');
            this.averageScoreB = ko.observable('');
            this.produceDate = ko.observable('');
            this.ratingName = ko.observable('');
            this.playTime = ko.observable('');
            this.produceZoneName = ko.observable('');
            this.introduce = ko.observable('');
            this.showMoreIntroduce = ko.observable(false);
            this.videoSrc = ko.observable('');
            this.cast = ko.observable('CAST');
            this.castShowData = ko.observableArray([]);
            this.getVodDetail();
            
            this.nowName = ko.observable('');
            this.nowHead = ko.observable('');
            this.nowID = ko.observable('');
            this.castTime = ko.observable('');
            
            this.dataObj = ko.observable({});
            this.vodlist = ko.observableArray([]);
            this.datas = ko.observableArray([]);
            this.isEnter = ko.observable(0);
            this.offsetNumber = ko.observable(0);
            this.count = ko.observable('0');
            this.contentDatas =  ko.observable({})
            this.panel =  ko.observable(1400);
            
            this.moreStillsLists = ko.observableArray([]);

            this.dataList = ko.observableArray([]);
            this.dataViewList = ko.observableArray([]);
            this.seasonslist = ko.observableArray([]);
            this.clipfilesVODs = ko.observableArray([]); 
            this.offsetSeasonNumber = ko.observable(0);
            this.seasonCount = ko.observable('0');

            this.appCastVod = ko.observable(true);
            this.appSeasons = ko.observable(false);
            this.ifclipfilesVODsLength = ko.observable(false);

            this.offsetTrailsNumber = ko.observable(0);
            this.offsetEpiNumber = ko.observable(0);
            this.offsetEpiLeft = ko.observable(false);
            this.offsetEpiRight = ko.observable(true);
            this.trailsCount = ko.observable('0');
            this.isPlayTrail = ko.observable(false);
            this.isShowPlayFilmButton = ko.observable(false);
            this.flag = ko.observable(true);

            this.errorPlayContentMessage = ko.observable('Failed to play.');

            this.pages = ko.observableArray([]); 
            this.ifEpisodesLength = ko.observable(false);
            this.episodes = ko.observableArray([]);
            this.pagesNum = ko.observable('');
            this.episodeNum = ko.observable('1');
            this.page = ko.observable(1);

            this.details = ko.observable({});
            this.curEpisodeIndex = ko.observable(0);
            this.episodeData = ko.observable([]);
            this.curEpisode = ko.observable({});

            this.isPlaying = ko.observable(false);
            this.chipFileIndex = ko.observable();
            this.download_android_app_url = ko.observable();
            this.download_ios_app_url = ko.observable();
            this.showCtlButton = ko.observable('');
            this.episodeCounts = ko.observable('');

            this.mouseenter = function(data, e) {
                var currentTarget = (window.event && window.event.currentTarget) || e.target;
                self.nowName(currentTarget['parentNode'].childNodes[3].innerHTML);
                self.nowHead(currentTarget['parentNode'].childNodes[1].getAttribute('src'));
                self.nowID(data['castIDs']);
                clearTimeout(self.castTime);
                self.castTime = setTimeout(function() {
                    self.clearData();
                    self.dataHandle(self.nowID());
                    self.isEnter(data['castIDs']);
                }, 200);
            };

            this.mouseleave= function(i, e) {
            };

            this.onclikshowmoreintroduce = function() {
                if (self.isShowIntroduceMore === true) {
                    self.isShowIntroduceMore = false;
                    self.dom('introduceCtrl').style.height = 68 + 'px';
                    self.dom('introducemore').style.background = "url(../assets/img/32/down_32.png) center center no-repeat";
                }else {
                    self.isShowIntroduceMore = true;
                    self.dom('introduceCtrl').style.height = self.dom('introduce').offsetHeight + 'px';
                    self.dom('introducemore').style.background = "url(../assets/img/32/up_32.png) center center no-repeat";
                }
            }
            this.onclikPlayFilm = function() {
                self.isPlayTrail(false);
                self.chipFileIndex('');
                if (self.details['mediaFiles'] && self.details['mediaFiles'][0]) {
                    self.playVod(self.details, self.details['ID'], self.details['mediaFiles'][0]['ID'], '', false);
                } else {
                    self.playVod(self.details, self.details['ID'], '', '', false);
                }
            }

            this.onClickLeft = function(data) {
                var i = data['castID'];
                var clientW= window.innerWidth;
                if (clientW > 1440) {
                    self.panel(1400);
                } else {
                    self.panel(800);
                }
                var offsetWidth;
                if (self.offsetNumber <= -(self.panel() + 50)) {
                    self.offsetNumber += self.panel();
                    offsetWidth = self.offsetNumber + 'px';
                    self.dom('list' + i).style.transform = 'translate(' + offsetWidth + ', 0px)';
                    self.dom('list' + i).style.transition = 'all 1s linear';
                    self.isShowIcon(self.offsetNumber, i);
                }else {
                    self.offsetNumber = 0;
                    self.dom('list' + i).style.transform = 'translate(' + 0 + 'px, 0px)';
                    self.dom('list' + i).style.transition = 'all 1s linear';
                    self.isShowIcon(self.offsetNumber, i);
                }
            }
            //  click the right arrow to move.
            this.onClickRight = function(data) {
                var i = data['castID'];
                var clientW = window.innerWidth;
                if (clientW > 1440) {
                    self.panel(1400);
                } else {
                    self.panel(800);
                }
                var offsetWidth;
                self.offsetNumber -= self.panel();
                offsetWidth = self.offsetNumber + 'px';
                self.dom('list' + i).style.transform = 'translate(' + offsetWidth + ', 0px)';
                self.dom('list' + i).style.transition = 'all 1s linear';
                self.isShowIcon(self.offsetNumber, i);
            }
            this.onClickSeasonLeft = function(data) {
                var clientW= window.innerWidth;
                if (clientW > 1440) {
                    self.panel(1400);
                } else {
                    self.panel(1000);
                }
                var offsetWidth;
                if (self.offsetSeasonNumber <= -(self.panel() + 50)) {
                    self.offsetSeasonNumber += self.panel();
                    offsetWidth = self.offsetSeasonNumber + 'px';
                    self.dom('seasonsList').style.transform = 'translate(' + offsetWidth + ', 0px)';
                    self.dom('seasonsList').style.transition = 'all 1s linear';
                    self.isShowSeasonIcon(self.offsetSeasonNumber);
                }else {
                    self.offsetSeasonNumber = 0;
                    self.dom('seasonsList').style.transform = 'translate(' + 0 + 'px, 0px)';
                    self.dom('seasonsList').style.transition = 'all 1s linear';
                    self.isShowSeasonIcon(self.offsetSeasonNumber);
                }
            }
            //  click the right arrow to move.
            this.onClickSeasonRight = function(data) {
                var clientW = window.innerWidth;
                if (clientW > 1440) {
                    self.panel(1400);
                } else {
                    self.panel(1000);
                }
                var offsetWidth;
                self.offsetSeasonNumber -= self.panel();
                offsetWidth = self.offsetSeasonNumber + 'px';
                self.dom('seasonsList').style.transform = 'translate(' + offsetWidth + ', 0px)';
                self.dom('seasonsList').style.transition = 'all 1s linear';
                self.isShowSeasonIcon(self.offsetSeasonNumber);
            }

            this.onClickTrailsLeft = function(data) {
                self.panel(399);
                var offsetWidth;
                if (self.offsetTrailsNumber <= -(self.panel() + 50)) {
                    self.offsetTrailsNumber += self.panel();
                    offsetWidth = self.offsetTrailsNumber + 'px';
                    self.dom('trailslist').style.transform = 'translate(' + offsetWidth + ', 0px)';
                    self.dom('trailslist').style.transition = 'all 1s linear';
                    self.isShowTrailsIcon(self.offsetTrailsNumber);
                }else {
                    self.offsetTrailsNumber = 0;
                    self.dom('trailslist').style.transform = 'translate(' + 0 + 'px, 0px)';
                    self.dom('trailslist').style.transition = 'all 1s linear';
                    self.isShowTrailsIcon(self.offsetTrailsNumber);
                }
            }
            //  click the right arrow to move.
            this.onClickTrailsRight = function(data) {
                self.panel(399);
                var offsetWidth;
                self.offsetTrailsNumber -= self.panel();
                offsetWidth = self.offsetTrailsNumber + 'px';
                self.dom('trailslist').style.transform = 'translate(' + offsetWidth + ', 0px)';
                self.dom('trailslist').style.transition = 'all 1s linear';
                self.isShowTrailsIcon(self.offsetTrailsNumber);
            }
            
            this.playTrail = function(data) {
                self.VodCheckLock(data['clip'], data['clip']['ID'], data['clip']['ID'], '', true);
                self.chipFileIndex(data['clipIndex']);
                self.episodeNum('');
                self.isPlayTrail(true);
                if (parseInt(self.details['VODType'], 10) === 0) {
                    self.isShowPlayFilmButton(true);
                }
            }

            this.clickEpi = function(data) {
                self.isPlayTrail(false);
                self.chipFileIndex('');
                self.episodeNum(data.VOD.ID);
                self.curEpisodeIndex = _.indexOf(self.episodeData, data);
                self.VodCheckLock(data['VOD'], data['VOD']['ID'], data['VOD']['ID'], '', true);
            }

            this.playVODContent = function(data) {
                var id = data['ID'] || data['vodId'];
                location.href = location.href.split('id=')[0]+ 'id=' + id;
            }

            this.onClickEpiLeft = function() {
                self.offsetEpiRight(true);
                var offsetWidth;
                var panel = 310;
                const MINSFFSETNUMBER = -1;
                if (self.offsetEpiNumber <= MINSFFSETNUMBER) {
                    self.offsetEpiNumber += panel;
                    if(self.offsetEpiNumber >= 0) {
                        self.offsetEpiLeft(false);
                    } else {
                        self.offsetEpiLeft(true);
                    }
                    offsetWidth = self.offsetEpiNumber + 'px';
                    self.dom('pages-list')['style'].transform = 'translate(' + offsetWidth + ')';
                    self.dom('pages-list')['style'].transition = 'all 1s linear';
                }
            }
            this.onClickEpiRight = function() {
                self.offsetEpiLeft(true);
                var offsetWidth;
                var panel = 310;
                var maxWidth = -(panel * (self.page - 1));
                if (self.offsetEpiNumber > maxWidth) {
                    self.offsetEpiNumber -= panel;
                    if(self.offsetEpiNumber <= 0) {
                        self.offsetEpiRight(false);
                    } else {
                        self.offsetEpiRight(true);
                    }
                    offsetWidth = self.offsetEpiNumber + 'px';
                    self.dom('pages-list')['style'].transform = 'translate(' + offsetWidth + ')';
                    self.dom('pages-list')['style'].transition = 'all 1s linear';
                } else {
                    self.offsetEpiRight(false);
                }
            }

            this.changePages = function(data) {
                self.episodes(data['episodes']);
                self.pagesNum(data['index']);
            }

            ko.applyBindings(this);
        },

        getVodDetail: function(){
            var self = this;
            var VODID = location.href.split('?id=')[1];
            if( !Number(location.href.split('?id=')[1]) ){
                VODID = VODID.split('&')[0];
            }
            shareChain.guestLogin().then(function(resp){
                var req = {
                    VODID: VODID || '',
                    queryDynamicRecmContent: {
                        recmScenarios: [{ contentType: 'VOD', entrance: 'VOD_Like_Many_of_its_Kind', contentID: VODID, count: '12', offset: '0' },
                        { contentType: 'VOD', entrance: 'VOD_Viewers_also_Watched', contentID: VODID, count: '12', offset: '0' }]
                    }
                }
                shareChain.getVodDetail(req).then(function(details){
                    var episodeCounts = parseInt(details.VODDetail.episodeCount);
                    var episodeCounts = "Total " + episodeCounts + " episodes";
                    if (episodeCounts === "Total 0 episodes") {
                        episodeCounts = null;
                    }
                    var recmContents = details && details['recmContents'];
                    var details = details && details.VODDetail || {};
                    self.details = details;
                    var playTime = details['mediaFiles'] && details['mediaFiles'][0] &&
                                    (Math.ceil(details['mediaFiles'][0]['elapseTime'] / 60, 10) + 'min');
                    if (episodeCounts) {
                        playTime = null;
                    }
                    self.titlename(details.name.toUpperCase());
                    self.averageScore(Number(details.averageScore).toFixed(1));
                    self.averageScoreA(details.averageScore.split('.')[0]);
                    self.averageScoreB(Number(details.averageScore).toFixed(1).split('.')[1]);
                    self.produceDate(details.produceDate && details.produceDate.substring(0, 4) + ' | ');
                    if(details['rating'] && details['rating'].name && details['rating'].name !== '') {
                        self.ratingName(details['rating'] && details['rating'].name + ' | '); 
                    }
                    self.playTime(playTime);
                    self.produceZoneName(details['produceZone'] && details['produceZone'].name + ' | ');
                    self.introduce(details['introduce']);
                    self.episodeCounts(episodeCounts);
                    if (parseInt(details['VODType'], 10) === 0) {
                        if (details['mediaFiles'] && details['mediaFiles'][0]) {
                            self.playVod(details, details['ID'], details['mediaFiles'][0]['ID'], '', false);
                        } else {
                            self.playVod(details, details['ID'], '', '', false);
                        }
                    }else {
                        if (details['episodes'] && details['episodes'].length > 0) {
                            self.curEpisodeIndex = 0;
                            self.episodeBeforePlay(details);
                        }else {
                            if (details['mediaFiles'] && details['mediaFiles'][0]) {
                                self.playVod(details, details['ID'],  details['mediaFiles'][0]['ID'], '', false);
                            } else {
                                self.playVod(details, details['ID'],  '', '', false);
                            }
                        }
                    }

                    if (self.dom('leftTrails')) {
                        self.dom('leftTrails').style.visibility = 'hidden';
                    }
                    if (self.dom('rightSeasons')) {
                        if (self.seasonslist().length > 4) {
                            self.dom('rightSeasons').style.visibility = 'visible';
                        }
                    }
                    if (parseInt(self.details.averageScore) !== 10) {
                        document.getElementsByClassName('hideA')[0]['style']['display'] = 'inlineBlock';
                        document.getElementsByClassName('hideA')[1]['style']['display'] = 'inlineBlock';
                    } else {
                        document.getElementsByClassName('hideA')[0]['style']['display'] = 'none';
                        document.getElementsByClassName('hideA')[1]['style']['display'] = 'none';
                    }
                    if (self.details.episodes.length > 0) {
                        if (self.details.episodes.length === 1) {
                            setTimeout(function() {
                                self.dom('changePages').style.visibility = "hidden";
                            }, 10);
                        }
                        self.episodeNum(details.episodes[0].VOD.ID);
                    }
                    
                    self.dealEpisodes(details);
                    self.dealClip(details);
                    self.isShowMoreIntroduceIcon();
                    self.isShowCtlButton();

                    self.stillsResult([details.picture && details.picture.stills, details]);
                    
                    self.getVodCast(details.castRoles);
                    if (self.castShowData() && self.castShowData()[0] && self.castShowData()[0]['castIDs']) {
                        self.dataHandle(self.castShowData() && self.castShowData()[0] && self.castShowData()[0]['castIDs']);
                    }

                    if (details['VODType'] === '2' || details['VODType'] === '3') {
                        self.showSeasons(details);
                    }

                    self.showLikeVOD(recmContents);
                    self.showViewVOD(recmContents);
                    shareChain.queryCustomizeConfig().then(function(resp){
                        if (resp && resp['values']) {
                            var values = resp['values'];
                            var download_android_app_url = _.find(values, function(item) {
                                return self.trim(item['key']) === 'download_android_app_url';
                            });
                            var download_ios_app_url = _.find(values, function(item) {
                                return self.trim(item['key']) === 'download_ios_app_url';
                            });
                            self.download_android_app_url(download_android_app_url);
                            self.download_ios_app_url(download_ios_app_url);
                        }
                    });
                })
            });
        },

        episodeBeforePlay: function(details) {
            var self = this;
            self.episodeData = details['episodes'];
            self.curEpisode = self.episodeData[0];
            var esDataID = self.curEpisode && self.curEpisode['VOD'] && self.curEpisode['VOD']['ID'];
            var esDataMediaFiles = self.curEpisode && self.curEpisode['VOD'] && self.curEpisode['VOD']['mediaFiles'];
            if (esDataMediaFiles && !_.isUndefined(esDataMediaFiles[0])) {
                self.playVod(details, esDataID,  esDataMediaFiles[0]['ID'] || '', details['ID'], false);
            }else {
                self.playVod(details, esDataID,  '', details['ID'], false);
            }
        },

        videoEventBind: function() {
            var self = this;
            var video = document.getElementById('videoPlayer');
            video.removeEventListener('ended', function() {}, true);
            video.addEventListener('ended', function() {
            if(self.flag){
                self.flag = false;
                self.checkPlayNextVideo();
            }
            });
            video.addEventListener('playing', function() {
                self.isPlaying = true;
            });
            video.addEventListener('pause', function() {
                self.isPlaying = false;
            });
        },

        checkPlayNextVideo: function() {
            var self = this;
            if (self.curEpisodeIndex === self.episodeData.length - 1) {
                return;
            }
            self.curEpisodeIndex += 1;
            self.curEpisode = self.episodeData[self.curEpisodeIndex];
            self.episodeNum(self.curEpisode['VOD']['ID']);
            var esDataID = self.curEpisode && self.curEpisode['VOD'] && self.curEpisode['VOD']['ID'];
            var esDataMediaFiles = self.curEpisode && self.curEpisode['VOD'] && self.curEpisode['VOD']['mediaFiles'];
            if (esDataMediaFiles && !_.isUndefined(esDataMediaFiles[0])) {
                self.autoPlayNextVod(self.details, esDataID,  esDataMediaFiles[0]['ID'] || '', self.details['ID'], false);
            }else {
                self.autoPlayNextVod(self.details, esDataID,  '', self.details['ID'], false);
            }
        },

        playVod: function(data, VODID, mediaID, seriesID, isTra) { 
            this.VodCheckLock(data, VODID, mediaID, seriesID, false);
        },

        autoPlayNextVod: function(data, VODID, mediaID, seriesID, isTra) { 
            this.VodCheckLock(data, VODID, mediaID, seriesID, true);
        },

        iosQQplay: function() {
            var u = navigator.userAgent;
            var isiOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); //ios terminal
            if (isiOS) {
                if (navigator.userAgent.indexOf('Mac') > -1) {
                    document.getElementById('videoPlayer').play();
                }
            }
        },

        reset: function() {
            document.getElementById('videoPlayer')['style']['display'] = 'block';
            this.videoEventBind();            
        },
        /**
         * play authentication, get URL
         */
        VodCheckLock: function(data, VODID, mediaID, seriesID, isAuto) {
            var self = this;
            shareChain.vodPlayCheckLock(data, VODID, mediaID, null, '1').then(function(resp) {
                if (resp.result && resp.result.retCode === '000000000') {
                    document.getElementById('videoPlayer').removeEventListener('ended', function() {}, true);
                    self.reset();
                    document.getElementById('videoPlayer').src = resp.playURL;
                        if (isAuto) {
                            document.getElementById('videoPlayer').play();
                            if(!self.flag){
                                self.flag = true;
                        }
                    }
                    document.getElementById('videoFailPlay')['style']['display'] = 'none';
                } else {
                    if (parseInt(resp['result']['retCode'], 10) === 144020008) {
                        self.errorPlayContentMessage('You have not subscribed to this vod. Please download the application login to continue viewing')
                    } else if(resp['authorizeResult'] && (resp['authorizeResult']['isLocked'] === '1' ||
                        resp['authorizeResult']['isParentControl'] === '1')) {
                        self.errorPlayContentMessage('The age rating of the movie is higher than your parental control level. Please download the application login to continue viewing')
                    } else {
                        self.errorPlayContentMessage('The movie format cannot be played. Please download the application login to continue viewing');
                    }
                    document.getElementById('videoPlayer').removeAttribute("src");
                    document.getElementById('videoPlayer').load();
                    document.getElementById('videoPlayer')['style']['display'] = 'none';
                    document.getElementById('videoFailPlay')['style']['display'] = 'block';
                    document.getElementById('iosPlayIcon')['style']['display'] = 'none';
                }
            }, function(resp) {
                if (parseInt(resp['result']['retCode'], 10) === 144020008) {
                    self.errorPlayContentMessage('You have not subscribed to this vod. Please download the application login to continue viewing')
                } else if(resp['authorizeResult'] && (resp['authorizeResult']['isLocked'] === '1' ||
                    resp['authorizeResult']['isParentControl'] === '1')) {
                    self.errorPlayContentMessage('The age rating of the movie is higher than your parental control level. Please download the application login to continue viewing')
                } else {
                    self.errorPlayContentMessage('The movie format cannot be played. Please download the application login to continue viewing');
                }
                document.getElementById('videoPlayer').removeAttribute("src");
                document.getElementById('videoPlayer').load();
                document.getElementById('videoPlayer')['style']['display'] = 'none';
                document.getElementById('videoFailPlay')['style']['display'] = 'block';
                document.getElementById('iosPlayIcon')['style']['display'] = 'none';
            });
        },

        /**
         * app-vod-episode
         */
        dealEpisodes: function(details) {
            var self = this;
            var episodes = details.episodes || [];
            var totalEpisodes = episodes;
            if (details && details.episodes && details.episodes.length > 0) {
                var pages = self.getPages(episodes, 20);
                self.pages(pages);
                self.episodes(pages[0]['episodes']);
                self.pagesNum(pages[0]['index']);
                self.page = Math.ceil(pages.length/ 3);
                self.ifEpisodesLength(true);
                self.offsetEpiNumber = 0;
            } else {
                self.ifEpisodesLength(false);
            }
        },

        getPages: function(episodes, pagesNum) {
            var pages = [];
            var num = Math.floor(episodes.length / pagesNum);
            if (num > 0) {
                for (var i = 0 ; i < num; i++) {
                    var episodesList = episodes.slice( i * pagesNum, (i + 1) * pagesNum);
                    var index = (i * pagesNum + 1) + ' - ' + ((i + 1) * pagesNum);
                    pages.push({'episodes': episodesList, 'index': index});
                }
                if (episodes.length % pagesNum !== 0) {
                    var episodesList = episodes.slice(num * pagesNum, episodes.length);
                    var index = (num * pagesNum + 1) + ' - ' + episodes.length;
                    pages.push({'episodes': episodesList, 'index': index});
                }
            } else {
                var episodesList = episodes.slice(0, episodes.length);
                var index = 1 + ' - ' + episodes.length;
                pages.push({'episodes': episodesList, 'index': index});
            }
            return pages;
        },

        /**
         * app-trails
         */
        dealClip: function(details) {
            var self = this;
            if (details && details.clipfiles && details.clipfiles.length > 0) {
                var elapseTime = '';
                var time = '';
                var second = '';
                var clipfilesVODs = _.map(details.clipfiles, function(clip, index) {
                    if ( Number(clip['elapseTime']) / 60 < 10 ) {
                        time = '0' + Math.floor(Number(clip['elapseTime']) / 60);
                    }else {
                        time = '' + Math.floor(Number(clip['elapseTime']) / 60);
                    }
                    if ( Number(clip['elapseTime']) % 60 < 10) {
                        second = '0' + Number(clip['elapseTime']) % 60;
                        elapseTime = time + ':' + second;
                    }else {
                        elapseTime = time + ':' + Math.floor(Number(clip['elapseTime']) % 60);
                    }
                    var url;
                    if (clip['picture']) {
                        url = self.convertToSizeUrl(clip['picture'].posters,
                        {minwidth: 117, minheight: 63, maxwidth: 155 , maxheight: 84});
                    }
                    return {
                        'clipIndex': index,
                        'id': clip['ID'],
                        'picture': url ? url : '',
                        'elapseTime': elapseTime,
                        'clip': clip
                    };
                });
                this.ifclipfilesVODsLength(true);
                this.clipfilesVODs(clipfilesVODs);
                if (this.clipfilesVODs().length > this.minScrollIndex) {
                    this.clipfilesVisible = 'visible';
                }else {
                    this.clipfilesVisible = 'hidden';
                }
            }else if (details && details.clipfiles && details.clipfiles.length === 0) {
                this.ifclipfilesVODsLength(false);
            }
            this.offsetTrailsNumber = 0;
            this.showTrailsIcon();
        },
        isShowTrailsIcon: function(offsetTrailsNumber) {
            var self = this;
            var len ;
            var clientW = window.innerWidth;
            var judgeLength = (this.trailsCount() - 2) * (384 + 15);
            if (self.clipfilesVODs() && self.clipfilesVODs().length) {
                len = Math.ceil(self.clipfilesVODs().length / 5) - 1;
            }else {
                len = 0;
            }
            if (offsetTrailsNumber === 0) {
                if (self.dom('leftTrails')) {
                    self.dom('leftTrails').style.visibility = 'hidden';
                }
                if (self.dom('rightTrails')) {
                    self.dom('rightTrails').style.visibility = 'visible';
                }
            }else if ( offsetTrailsNumber <= -judgeLength || offsetTrailsNumber === -(len * 1000) ) {
                if (self.dom('leftTrails')) {
                    self.dom('leftTrails').style.visibility = 'visible';
                }
                if (self.dom('rightTrails')) {
                    self.dom('rightTrails').style.visibility = 'hidden';
                }
            }else {
                if (self.dom('leftTrails')) {
                    self.dom('leftTrails').style.visibility = 'visible';
                }
                if (self.dom('rightTrails')) {
                    self.dom('rightTrails').style.visibility = 'visible';
                }
            }
        },
        isShowMoreIntroduceIcon: function() {
            var self = this;
            if (self.dom('introduce').innerHTML == "") {
                self.dom('storylineContent').style.display = "none";
            }
            if (self.dom('introduce').offsetHeight >= 69) {
                self.showMoreIntroduce(true);
            }else {
                self.showMoreIntroduce(false);
            }
        },
        isShowCtlButton: function(){
            var self = this;
            if (self.details.episodes.length > 60){
                self.showCtlButton(true);
            }else {
                self.showCtlButton(false);
            }
        },
        showTrailsIcon: function() {
            var self = this;
            if (self.clipfilesVODs() && self.clipfilesVODs().length ) {
                self.trailsCount(self.clipfilesVODs().length);
            }

        },
        /**
         * app-cast-vod  show data
         */
        getVodCast: function(castRoles) {
            if (castRoles) {
                castRoles.sort(function(a, b){
                    return Number(b.roleType === '1') - Number(a.roleType === '1');
                });
                this.handleData(castRoles, 1);
                this.handleData(castRoles, 0);
                this.handleData(castRoles, 2);
                this.handleData(castRoles, 3);
                this.handleData(castRoles, 4);
                this.handleData(castRoles, 5);
                this.handleData(castRoles, 6);
                this.handleData(castRoles, 7);
                this.handleData(castRoles, 8);
                this.handleData(castRoles, 9);
                this.handleData(castRoles, 100);
                this.castShowData(this.filterSameCast(this.castShowData.splice(0, 4)));
                var castShowData = this.castShowData() && this.castShowData()[0];
                if(castShowData) {
                    this.isEnter(this.castShowData()[0]['castIDs']);
                }
            }
            if (this.castShowData() && this.castShowData().length > 0) {
                this.appCastVod(true);
            } else {
                this.appCastVod(false);
            }
        },
        filterSameCast: function(castArray) {
            var existIDs = [];
            var filterCast = [];
            for (var i = 0; i < castArray.length; i++) {
                var isAlreadyExist = _.find(existIDs, function(item) {
                    return item === castArray[i]['castIDs'];
                });
                if (!isAlreadyExist) {
                    filterCast.push(castArray[i]);
                    existIDs.push(castArray[i]['castIDs']);
                }
            }
            return filterCast;
        },
        handleData: function(data, type) {
            var self = this;
            var number = this.castShowData && this.castShowData.length;
            _.map(data, function(castInfo, index){
                if (parseInt(castInfo['roleType'], 10) === type) {
                    var casts =  _.map(castInfo['casts'], function(cast, i) {
                       var pictures = cast['picture'] && cast['picture']['icons'] ?
                                cast['picture'].icons[0] : 'assets/img/default/ondemand_casts.png';
                       return {
                            castName: cast['castName'],
                            pictures: pictures,
                            id: cast['castID'],
                            avatar: self.convertToSizeUrl(pictures,{minwidth: 88, minheight: 88, maxwidth: 88 , maxheight: 88}),
                            castIDs: cast['castID']
                       }
                    });
                    self.castShowData(self.castShowData().concat(casts));
                }
            });
        },
        convertToSizeUrl: function(pictureUrls, size){
            if (_.isEmpty(pictureUrls)) {
                return '';
            }

            var url = pictureUrls;
            if (_.isArray(pictureUrls)) {
                url = _.first(pictureUrls);
            }

            size = size || {};
            if (url) {
                var clientW = window.innerWidth;
                if (clientW > 1440 && clientW <= 1920) {
                    return url = url + '?x=' + size.maxwidth + '&y=' + size.maxheight + '&ar=ignore';
                } else {
                    return url = url + '?x=' + size.minwidth + '&y=' + size.minheight + '&ar=ignore';
                }
            } else {
                return '';
            }
        },
        clearData: function() {
            this.vodlist([]);
            this.dataObj({
                VODList: [],
                subjectName: ''
            });
            this.datas([]);
        },
        dataHandle: function(id) {
            var self = this;
            var castDataId = id;
            shareChain.searchContent({
                searchKey: id,
                contentTypes: ['VOD'],
                searchScopes: ['CAST_ID'],
                count: '24',
                offset: '0',
                sortType: ['RELEVANCE']
            }).then(function(resp){
                self.contentDatas(resp.contents);
                self.clearData();
                for (var i = 0; i < self.contentDatas().length; i++) {
                    if (self.contentDatas()[i].VOD.averageScore !== '10') {
                        var averageScore = self.contentDatas()[i].VOD.averageScore;
                        if (averageScore.length === 1) {
                            averageScore += '.0';
                        } else {
                            averageScore = averageScore.substring(0, 4);
                            averageScore = Number(averageScore);
                            averageScore = averageScore.toFixed(1);
                            if (averageScore === '10.0') {
                                averageScore = '10';
                            }
                        }
                        self.contentDatas()[i].VOD.averageScore = averageScore || '0.0';
                    }
                    self.vodlist().push(self.contentDatas()[i].VOD);
                }
                self.dataObj({
                    VODList: self.vodlist(),
                    subjectName: self.nowName(),
                    castID: castDataId
                });
                self.datas().push(self.dataObj());
                _.map(self.datas(), function(lists, index) {
                    _.map(lists['VODList'], function(list, nextIndex) {
                        if (list['picture'] && list['picture'].posters && list['picture'].posters[0]) {
                            var url = self.convertToSizeUrl(list['picture'].posters[0],
                            {minwidth: 180, minheight: 240, maxwidth: 214 , maxheight: 284});
                            list['picture'].posters = [];
                            list['picture'].posters[0] = url || '../../assets/img/default/home_poster.png';
                        }
                    });
                });
                self.datas(self.datas());
                self.offsetNumber = 0;
                self.showIcon();
                document.getElementById('left' + castDataId).style.visibility = 'hidden';
            });
        },
        dom:function(divName) {
            return document.getElementById(divName);
        },
        isShowIcon: function(offsetNumber, i) {
            var self = this;
            var len ;
            var clientW = window.innerWidth;
            var judgeLength;
            if (clientW > 1440) {
                judgeLength = (this.count() - 6) * (214 + 19);
            } else {
                judgeLength = (this.count() - 5) * (180 + 20);
            }
            if (self.datas()[0].VODList && self.datas()[0].VODList.length) {
                if (window.innerWidth > 1440 && window.innerWidth <= 1920) {
                    len = Math.ceil(self.datas()[0].VODList.length / 6) - 1;
                }else if (window.innerWidth <= 1440) {
                    len = Math.ceil(self.datas()[0].VODList.length / 5) - 1;
                }
            }else {
                len = 0;
            }
            if (offsetNumber === 0) {
                if (self.dom('left' + i)) {
                    self.dom('left' + i).style.visibility = 'hidden';
                }
                if (self.dom('right' + i)) {
                    self.dom('right' + i).style.visibility = 'visible';
                }
            }else if ( offsetNumber <= -judgeLength || offsetNumber === -(len * 1000) ) {
                if (self.dom('left' + i)) {
                    self.dom('left' + i).style.visibility = 'visible';
                }
                if (self.dom('right' + i)) {
                    self.dom('right' + i).style.visibility = 'hidden';
                }
            }else {
                if (self.dom('left' + i)) {
                    self.dom('left' + i).style.visibility = 'visible';
                }
                if (self.dom('right' + i)) {
                    self.dom('right' + i).style.visibility = 'visible';
                }
            }
        },
        showIcon: function() {
            var self = this;
            if (window.innerWidth > 1440 && window.innerWidth <= 1920) {
                for (var i = 0; i < self.datas().length; i++) {
                    if (self.datas()[i] && self.datas()[i].VODList ) {
                        self.count(self.datas()[i].VODList.length);
                    }
                }
            }
            if (window.innerWidth <= 1440) {
                for (var i = 0; i < self.datas().length; i++) {
                    if (self.datas()[i] && self.datas()[i].VODList ) {
                        self.count(self.datas()[i].VODList.length);
                    }

                }
            }
        },

        /**
         * app-stills-vod show data
         */
        stillsResult: function(stillsVod) {
            var self = this;
            var stillsLists = stillsVod[0];
            var name = stillsVod[1];
            var subjectID = stillsVod[2];
            if (stillsLists && stillsLists.length > 0 && stillsLists[0] !== '') {
                for (var i = 0; i < stillsLists.length; i++) {
                    stillsLists[i] = self.convertToSizeUrl(stillsLists[i],
                    {minwidth: 180, minheight: 180, maxwidth: 180 , maxheight: 180});
                }
            }
            this.moreStillsLists(stillsLists && stillsLists.slice(0, 7));
        },

        /**
         * app-seasons show data
         */
        showSeasons: function(details){
            var seasonsInfo = [];
            if (details.brotherSeasonVODs && details.brotherSeasonVODs.length > 0) {
                seasonsInfo = details.brotherSeasonVODs.slice(0, 24);
            }
            this.seasonsData(seasonsInfo);
            this.offsetSeasonNumber = 0;
            this.showSeasonIcon();
        },
        seasonsData: function(receiveData) {
            var dataList = [];
            var self = this;
            for (var i = 0; i < receiveData.length; i++) {
                if (receiveData && receiveData[i] && receiveData[i].VOD) {
                    if (!receiveData[i].VOD.averageScore) {
                        receiveData[i].VOD.averageScore = '0.0';
                    }else {
                        if (receiveData[i].VOD.averageScore !== '10') {
                            var averageScore = receiveData[i].VOD.averageScore;
                            if (averageScore.length === 1) {
                                averageScore += '.0';
                            } else {
                                averageScore = averageScore.substring(0, 4);
                                averageScore = Number(averageScore);
                                averageScore = averageScore.toFixed(1);
                                if (averageScore === '10.0') {
                                    averageScore = '10';
                                }
                            }
                            receiveData[i].VOD.averageScore = averageScore;
                        }
                    }
                }
                dataList.push(receiveData[i].VOD);
            }
            var dataObj = {
                VODList: dataList,
                subjectName: 'Seasons'
            };
            this.seasonslist().push(dataObj);
            _.map(this.seasonslist(), function(lists, index) {
                _.map(lists['VODList'], function(list, nextIndex) {
                    var url = list['picture'] && list['picture'].posters && self.
                        convertToSizeUrl(list['picture'].posters[0], {minwidth: 180, minheight: 240, maxwidth: 214 , maxheight: 284});
                    list['picture'].posters = [];
                    list['picture'].posters[0] = url || 'assets/img/default/home_poster.png';
                });
            });
            this.seasonslist(this.seasonslist());
            if(this.seasonslist() && this.seasonslist().length > 0 ) {
                this.appSeasons(true);
            }else {
                this.appSeasons(false);
            }
        },
        isShowSeasonIcon: function(offsetSeasonNumber) {
            var self = this;
            var len ;
            var clientW = window.innerWidth;
            var judgeLength;
            if (clientW > 1440) {
                judgeLength = (this.seasonCount() - 6) * (214 + 19);
            } else {
                judgeLength = (this.seasonCount() - 5) * (180 + 20);
            }
            if (self.seasonslist()[0].VODList && self.seasonslist()[0].VODList.length) {
                if (window.innerWidth > 1440 && window.innerWidth <= 1920) {
                    len = Math.ceil(self.seasonslist()[0].VODList.length / 6) - 1;
                }else if (window.innerWidth <= 1440) {
                    len = Math.ceil(self.seasonslist()[0].VODList.length / 5) - 1;
                }
            }else {
                len = 0;
            }
            if (offsetSeasonNumber === 0) {
                if (self.dom('leftSeasons')) {
                    self.dom('leftSeasons').style.visibility = 'hidden';
                }
                if (self.dom('rightSeasons')) {
                    self.dom('rightSeasons').style.visibility = 'visible';
                }
            }else if ( offsetSeasonNumber <= -judgeLength || offsetSeasonNumber === -(len * 1000) ) {
                if (self.dom('leftSeasons')) {
                    self.dom('leftSeasons').style.visibility = 'visible';
                }
                if (self.dom('rightSeasons')) {
                    self.dom('rightSeasons').style.visibility = 'hidden';
                }
            }else {
                if (self.dom('leftSeasons')) {
                    self.dom('leftSeasons').style.visibility = 'visible';
                }
                if (self.dom('rightSeasons')) {
                    self.dom('rightSeasons').style.visibility = 'visible';
                }
            }
        },
        showSeasonIcon: function() {
            var self = this;
            if (window.innerWidth > 1440 && window.innerWidth <= 1920) {
                for (var i = 0; i < self.seasonslist().length; i++) {
                    if (self.seasonslist()[i] && self.seasonslist()[i].VODList ) {
                        self.seasonCount(self.seasonslist()[i].VODList.length);
                    }
                }
            }
            if (window.innerWidth <= 1440) {
                for (var i = 0; i < self.seasonslist().length; i++) {
                    if (self.seasonslist()[i] && self.seasonslist()[i].VODList ) {
                        self.seasonCount(self.seasonslist()[i].VODList.length);
                    }

                }
            }
        },

        /**
         * app-like-many  show data
         */
        showLikeVOD: function(recmContents) {
            var likeManyDate = this.getlikeVod(recmContents && recmContents[0] && recmContents[0].recmVODs).slice(0, 5);
            var moreVodLists = {
                'dataList': likeManyDate,
                'row': '2',
                'clientW': document.body.clientWidth,
                'suspensionID': 'likemany'
            };
            
            if (!_.isUndefined(moreVodLists)) {
                if (moreVodLists['row'] === '1') {
                    if (moreVodLists['clientW'] > 1440) {
                        this.dataList(moreVodLists['dataList']);
                    }else {
                        this.dataList(moreVodLists['dataList']);
                    }
                }else if (moreVodLists['row'] === '2') {
                    if (moreVodLists['clientW'] > 1440) {
                        this.dataList(moreVodLists['dataList']);
                    }else {
                        this.dataList(moreVodLists['dataList']);
                    }
                }else {
                    this.dataList(moreVodLists['dataList']);
                }
            }
        },
        getlikeVod: function(likeManyData) {
            var self = this;
            var list = _.map(likeManyData, function(list1, index) {
                    self.score(list1);
                    var curSubjectName = 'like_many_of_this_kind';
                    var url = list1['picture'] && list1['picture'].posters &&
                    self.convertToSizeUrl(list1['picture'].posters[0],
                    {minwidth: 180, minheight: 240, maxwidth: 214 , maxheight: 284});
                    return {
                        vodId: list1['ID'],
                        name:  list1['name'],
                        posterUrl: url,
                        score: list1['averageScore'] || '0.0',
                        floatInfo: list1,
                        titleName: curSubjectName,
                        subjectID: '-1'
                    };
                });
            return list;
        },
        getviewVod: function(viewalsoData) {
            var self = this;
            var list = _.map(viewalsoData, function(list1, index) {
                    self.score(list1);
                    var curSubjectName = 'viewers_also_watched';
                    var url = list1['picture'] && list1['picture'].posters &&
                    self.convertToSizeUrl(list1['picture'].posters[0],
                    {minwidth: 180, minheight: 240, maxwidth: 214 , maxheight: 284});
                    return {
                        vodId: list1['ID'],
                        name:  list1['name'],
                        posterUrl:  url || '../assets/img/default/home_poster.png',
                        score: list1['averageScore'],
                        floatInfo: list1,
                        titleName: curSubjectName,
                        subjectID: '-1'
                    };
                });
            return list;
        },
        score: function(list) {
            if (list.averageScore) {
                if (list.averageScore.length === 1) {
                    list.averageScore += '.0';
                }else {
                    var score = list.averageScore;
                    list.averageScore = score.substring(0, 4);
                    list.averageScore = Number(list.averageScore);
                    list.averageScore = list.averageScore.toFixed(1);
                    if (list.averageScore === '10.0') {
                        list.averageScore = '10';
                    }
                }
            }
            return list;
        },
        showViewVOD: function(recmContents) {
            var clientW = document.body.clientWidth;
            var viewAlsoDate = this.getviewVod(recmContents && recmContents[1] && recmContents[1].recmVODs).slice(0, 5);
            var moreViewLists = {
                'dataList': viewAlsoDate,
                'row': '2',
                'clientW': clientW,
                'suspensionID': 'viewalso'
            };

            if (!_.isUndefined(moreViewLists)) {
                if (moreViewLists['row'] === '1') {
                    if (moreViewLists['clientW'] > 1440) {
                        this.dataViewList(moreViewLists['dataList']);
                    }else {
                        this.dataViewList(moreViewLists['dataList']);
                    }
                }else if (moreViewLists['row'] === '2') {
                    if (moreViewLists['clientW'] > 1440) {
                        this.dataViewList(moreViewLists['dataList']);
                    }else {
                        this.dataViewList(moreViewLists['dataList']);
                    }
                }else {
                    this.dataViewList(moreViewLists['dataList']);
                }
            }
        },

        getCurrentdatas: function() {
            if(this.datas() && this.datas()[0] && this.datas()[0]['VODList'] && this.datas()[0]['VODList'].length) {
                return this.datas()[0]['VODList'].length;
            }
        },
        
        trim: function(str) {
            str = str.replace(/(^\s)|(\s*$)/g, '').replace(/(^\s*)/g, '').replace(/(\s*$)/g, '');
            return str;
        },

        downLoadApk: function() {
            if(navigator.userAgent.match(/(iPhone|iPod|iPad);?/i)) {
                var ua = navigator.userAgent.toLowerCase();
                if(ua.match(/MicroMessenger/i) == "micromessenger") {
                    document.getElementById("popweixin").style.display = "block";
                } else {
                    window.location.href = this.download_ios_app_url()['values'][0] ||
                        'itms-apps://itunes.apple.com/cn/app/he-shi-pin/id787130974?mt=8';
                }
            } 

            if(navigator.userAgent.match(/android/i)) {
                window.location.href = this.download_android_app_url()['values'][0] || 
                    'http://appstore.huawei.com/app/C10188539';
            }
        },

        hidePopweixin: function() {
            document.getElementById("popweixin").style.display = "none";
        }
    }
});