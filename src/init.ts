// import 'hammerjs'
import '/src/assets/lib/PageTracker'

import { config } from './app/shared/services/config'
import { Config as SDKConfig } from 'ng-epg-sdk/core'
import { Logger } from 'ng-epg-sdk/utils'
import { xr } from 'ng-epg-sdk/utils/xr'
import { xhr } from 'ng-epg-sdk/sdk/http/backends/xhr-backend/xhr'
import { reconfigFromQuery } from 'oper-center/util/config'

import { session } from 'src/app/shared/services/session'
import { sdkpolicy } from './sdkpolicy'

export const init = () => {
  const paramSet = location.search
  if (paramSet) {
    reconfigFromQuery(
      paramSet,
      {
        config,
        updateMap: {
          vspHttpsUrl: ['vsp', 'vspHttpsUrl']
        }
      }
    )
  }
  // set withCredentials for sending cookies from services in requerst header
  xr.configure({
    withCredentials: true
  })
  xhr.configure({
    withCredentials: true
  })
  // initialize the log level and perhaps create another config for production
  Logger.setLevel(config.logLevel)
  // save the vsp url when refresh the page
  SDKConfig.vspUrl = config.vspUrl || session.get('config_vspURL')
  SDKConfig.vspHttpsUrl = config.vspHttpsUrl || session.get('config_vspHttpsURL')
  SDKConfig.set('enableSDKCache', true)
  SDKConfig.playHttps = 1
  sdkpolicy.setSdkPolicy()
}
