// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `angular-cli.json`.

export const environment = {

  production: process.env.NODE_ENV === 'production',

  // TV HOUSE

  TV_HOUSE_API_LOGIN_BASE: 'https://mts-tvhouse-stable.smart-itech.ru/tvh-login/public/rest',
  TV_HOUSE_API_OFFERS_BASE: 'https://mts-tvhouse-stable.smart-itech.ru/tvh-public-offers/public/rest',
  TV_HOUSE_API_OFFERS_ACCEPT: 'https://mts-tvhouse-stable.smart-itech.ru/tvh-offers/rest',

  // C50 (NEW)

  TV_PROXY_HTTP: 'http://mts-web-staging-api.smart-itech.ru',
  TV_PROXY_HTTPS: 'https://mts-web-staging-api.smart-itech.ru',
  TV_PROXY_REPLACE_FROM_PROTOCOL: 'https',
  TV_PROXY_REPLACE_TO_PROTOCOL: 'https',
  TV_PROXY_REPLACE_FROM_ADDRESS: '195.34.49.38:443',
  TV_PROXY_REPLACE_TO_ADDRESS: 'mts-web-staging-api.smart-itech.ru',

  // C50 (DEPRECATED)

  //TV_PROXY_HTTP: 'http://tv-proxy-c50.rhrn.ru',
  //TV_PROXY_HTTPS: 'https://tv-proxy-c50.rhrn.ru',
  //TV_PROXY_REPLACE_FROM_PROTOCOL: 'https',
  //TV_PROXY_REPLACE_TO_PROTOCOL: 'https',
  //TV_PROXY_REPLACE_FROM_ADDRESS: '195.34.49.38:443',
  //TV_PROXY_REPLACE_TO_ADDRESS: 'tv-proxy-c50.rhrn.ru:33200',

  // C40

  //TV_PROXY_HTTP: 'http://tv-test.rhrn.ru:80',
  //TV_PROXY_HTTPS: 'https://tv-test.rhrn.ru:443',
  //TV_PROXY_REPLACE_FROM_IP: '37.16.83.206',
  //TV_PROXY_REPLACE_FROM_PROTOCOL: 'https',
  //TV_PROXY_REPLACE_TO_PROTOCOL: 'https',
  //TV_PROXY_REPLACE_FROM_ADDRESS: '37.16.83.206:443',
  //TV_PROXY_REPLACE_TO_ADDRESS: 'tv-proxy.rhrn.ru:443',

}
