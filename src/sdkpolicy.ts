import * as _ from 'underscore'
import { session } from 'src/app/shared'
import { SdkCacheConfig } from './app/shared/services/sdk-cache'
import { http } from 'ng-epg-sdk/sdk'
import { Logger } from 'ng-epg-sdk/utils'

const log = new Logger('sdkpolicy.ts')

export class SdkPolicyHandler {
  constructor () {}
  // set sdk cache policy.
  public setSdkPolicy () {
    http.setCachePolicy(SdkCacheConfig)
    let customizeConfig = session.get('CUSTOM_CONFIG_DATAS')
    let policyFromSession = null
    if (customizeConfig) {
      let config
      if (customizeConfig['configurations']) {
        config = _.find(customizeConfig['configurations'], item => {
          return item['cfgType'] === '0'
        })
      }
      if (config && config['values']) {
        let networkSdkCache = _.find(config['values'], item => {
          return item['key'] === 'network_interface_policy'
        })
        if (networkSdkCache && networkSdkCache['values'] && networkSdkCache['values'][0] && networkSdkCache['values'][0] !== '') {
          try {
            policyFromSession = JSON.parse(networkSdkCache['values'][0])
          } catch (e) {
            log.error(e)
          }
        }
      }
    }
    if (policyFromSession) {
      http.setCachePolicy(policyFromSession)
    }
  }
}

export const sdkpolicy = new SdkPolicyHandler()
