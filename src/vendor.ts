import 'ng-epg-sdk/vsp/api'

import 'rxjs'
import 'underscore'

import '@angular/common'
import '@angular/common/http'
import '@angular/forms'
import '@angular/router'
import '@angular/platform-browser'
import 'style-loader/addStyles'

import 'moment'
import 'moment-timezone'
import 'moment-timezone/moment-timezone-utils'

import 'localforage'
import 'fingerprintjs'
// import 'qrious'
import 'eventemitter2'
// import 'angular2-qrcode'
import 'md5'

import '@ngx-translate/core'
import '@ngx-translate/http-loader'
