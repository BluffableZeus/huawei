var GlobaleConfig = {
    ssh_enable: 0,
    //the mapping definition of https port and http port
    protocolPortMap: {
        "443": "80",
        "33207": "33200",
        "8441": "8081",
        "8442": "8082",
        "8443": "8083",
        "8444": "8084",
        "8445": "8085",
        "8446": "8086",
        "8447": "8087",
        "8448": "8088",
        "8449": "8089",
        "8450": "8090",
        "8451": "8091",
        "8452": "8092",
        "8453": "8093",
        "8454": "8094",
        "8455": "8095",
        "8456": "8096",
        "8457": "8097",
        "8458": "8098",
        "8459": "8099",
        "8460": "8100"
    }
};

if (/windows|win32/i.test(navigator.userAgent)) {
    GlobaleConfig.macPlugType = 'HW';
}

GlobaleConfig.getHttpsPort = function(){
    if (location.protocol == 'https') {
        return location.port;
    }

    for (prop in GlobaleConfig.protocolPortMap) {
        if (GlobaleConfig.protocolPortMap[prop] == location.port) {
            return prop;
        }
    }

    return location.port;
}

GlobaleConfig.getHttpPort = function(){
    if (location.protocol == 'http') {
        return location.port;
    }

    for (prop in GlobaleConfig.protocolPortMap) {
        if (prop == location.port) {
            return GlobaleConfig.protocolPortMap[prop];
        }
    }
    return location.port;
}
