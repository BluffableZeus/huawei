'use strict';
define(function(require) {
    require('moment-timezone');
    var moment = require('moment-timezone/moment-timezone-utils');
    var _ = require('underscore');

    /**
     * [findDstSwitchTime description]
     * @param  {number} timeObject
     * @return {number}
     */
    var findDstSwitchTime = function(timeObject) {
        var year = timeObject.year,
            month = timeObject.month,
            day = timeObject.day,
            changeMinute = timeObject.changeMinute,
            baseOffset = timeObject.baseOffset;
        var minutes = 0;
        // Back-up one day and grap the offset
        var tmpDate = new Date(Date.UTC(year, month, day - 1, 0, 0, 0, 0));
        var tmpOffset = -1 * tmpDate.getTimezoneOffset() / 60;

        // Count the minutes until a timezone chnage occurs
        while (changeMinute === -1) {
            tmpDate = new Date(Date.UTC(year, month, day - 1, 0, minutes, 0, 0));
            tmpOffset = -1 * tmpDate.getTimezoneOffset() / 60;

            // Determine the exact minute a timezone change
            // occurs
            if (tmpOffset !== baseOffset) {
                // Back-up a minute to get the date/time just
                // before a timezone change occurs
                tmpOffset = new Date(Date.UTC(year, month, day - 1, 0, minutes - 1, 0, 0));
                changeMinute = minutes;
                break;
            } else {
                minutes++;
            }
        }

        // Add a month (for display) since JavaScript counts
        // months from 0 to 11
        var dstDate = tmpOffset.getMonth() + 1;

        // Pad the month as needed
        if (dstDate < 10) {
            dstDate = '0' + dstDate;
        }

        // Add the day and year
        dstDate += '/' + tmpOffset.getDate() + '/' + year + ' ';

        // Capture the time stamp
        tmpDate = new Date(Date.UTC(year, month, day - 1, 0, minutes - 1, 0, 0));
        dstDate += tmpDate.toTimeString().split(' ')[0];
        return tmpDate;
    };

    /**
     * [findDstSwitchDate description]
     * @param  {number} year
     * @param  {number} month
     * @return {Date}
     */
    function findDstSwitchDate(year, month) {
        // Set the starting date
        var baseDate = new Date(Date.UTC(year, month, 0, 0, 0, 0, 0));
        var changeMinute = -1;
        var baseOffset = -1 * baseDate.getTimezoneOffset() / 60;

        // Loop to find the exact day a timezone adjust occurs
        for (var day = 0; day < 50; day++) {
            var tmpDate = new Date(Date.UTC(year, month, day, 0, 0, 0, 0));
            var tmpOffset = -1 * tmpDate.getTimezoneOffset() / 60;

            // Check if the timezone changed from one day to the next
            if (tmpOffset !== baseOffset) {
                var timeObject = {
                    year: year,
                    month: month,
                    day: day,
                    changeMinute: changeMinute,
                    baseOffset: baseOffset
                };
                return findDstSwitchTime(timeObject);
            }
        }
    }

    //DisplayDstSwitchDates
    return function(dateTime) {
        dateTime = dateTime || new Date();
        var year = dateTime.getYear();
        if (year < 1000) {
            year += 1900;
        }

        var firstSwitch = 0;
        var secondSwitch = 0;
        var lastOffset = 99;

        // Loop through every month of the current year
        for (var i = 0; i < 12; i++) {
            // Fetch the timezone value for the month
            var newDate = new Date(Date.UTC(year, i, 0, 0, 0, 0, 0));
            var tz = -1 * newDate.getTimezoneOffset() / 60;

            // Capture when a timzezone change occurs
            if (tz > lastOffset) {
                firstSwitch = i - 1;
            } else if (tz < lastOffset) {
                secondSwitch = i - 1;
            }

            lastOffset = tz;
        }

        // Go figure out date/time occurences a minute before
        // a DST adjustment occurs
        var secondDstDate = findDstSwitchDate(year, secondSwitch);
        var firstDstDate = findDstSwitchDate(year, firstSwitch);
        var sameZoneNames = _.filter(moment.tz.names(), function(name) {
            return moment().format('HH:mm:ss') === moment().tz(name).format('HH:mm:ss');
        });
        var zoneName;

        if (firstDstDate && secondDstDate) {
            zoneName = _.find(sameZoneNames, function(name) {
                var filterYear = moment.tz.filterYears(moment.tz.zone(name), year);
                var firstTime = moment(firstDstDate).add(1, 'minutes').toDate().getTime() === filterYear.untils[0],
                    secondTime = moment(secondDstDate).add(1, 'minutes').toDate().getTime() === filterYear.untils[1];
                if (filterYear) {
                    return firstTime && secondTime;
                }
            });
        } else {
            zoneName = sameZoneNames[0];
        }
        return zoneName ? zoneName : sameZoneNames[0];
    };

});
