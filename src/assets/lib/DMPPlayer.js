DMPPlayer = function(videoElement) {

    "use strict";

    var DMPVERSION = "20.0.19.2",
        audioTracks = null,
        videoTracks = null,
        subtitleTracks = null,
        videoBitrateList = null,
        currentSubtitleTrack = null,
        currentAudioTrack = null,
        registrations = null,
        libPlayer = null,
        video = null,
        firstTimeStamp = null,
        onStreamInitialized = function() {
            audioTracks = libPlayer.getTracksFor("audio");
            videoTracks = libPlayer.getTracksFor("video");
            videoBitrateList = libPlayer.getBitrateInfoListFor("video");
            currentAudioTrack = libPlayer.getCurrentTrackFor("audio");
            dispatchEvent({ type: DMPPlayer.events.STREAM_INITIALIZED });
        },

        onSubtitlesLoaded = function(evt) {
            subtitleTracks = evt.tracks;

            if (!subtitleTracks || subtitleTracks.length === 0) {
                return;
            }

            currentSubtitleTrack = libPlayer.getCurrentTrackFor("text") || libPlayer.getCurrentTrackFor("fragmentedText");

            var subtitleLanguages = subtitleTracks.map(function(subtitleTrack) {
                return subtitleTrack.lang;
            });
            dispatchEvent({ type: DMPPlayer.events.SUBTILES_INITIALIZED, subtitles: subtitleLanguages });
        },
        onError = function(evt) {
            dispatchEvent({ type: DMPPlayer.events.PLAYBACK_ERROR, errorCode: evt.errorCode, errorDescription: evt.errorDescription, errorInfo: evt.errorInfo });
        },
        onPlaybackStart = function() {
            dispatchEvent({ type: DMPPlayer.events.PLAYBACK_START });
        },
        onPlaybackSeeking = function() {
            dispatchEvent({ type: DMPPlayer.events.PLAYBACK_SEEKING });
        },
        onPlaybackSeeked = function() {
            dispatchEvent({ type: DMPPlayer.events.PLAYBACK_SEEKED });
        },
        onPlaybackTimeUpdated = function() {
            var isNPVR = libPlayer.getIsNPVR();
            if (isNPVR === true) {
                if (firstTimeStamp === null) {
                    firstTimeStamp = Math.floor(libPlayer.time() - libPlayer.getRealBookMark());
                }
            }
            dispatchEvent({ type: DMPPlayer.events.PLAYBACK_TIMEUPDATED });
        },
        onStreamDurationUpdated = function(evt) {
            dispatchEvent({ type: DMPPlayer.events.STREAM_DURATION_UPDATED, duration: evt.duration});
        },
        onPlaybackEnded = function() {
            dispatchEvent({ type: DMPPlayer.events.PLAYBACK_END });
        },
        onPlaybackWaiting = function() {
            dispatchEvent({ type: DMPPlayer.events.BUFFER_STARTED });
        },
        onPlaybackPlaying = function() {
            dispatchEvent({ type: DMPPlayer.events.PLAYBACK_PLAYING });
        },
        convertTrackToLanguage = function(subtitleTrack) {
            var lang = subtitleTrack.lang;
            if (!lang) {
                return "undefined";
            }
            return lang;
        },
        switchVideo = function(requirements) {
            if (!libPlayer) return;
            var bitrate = requirements.bitrate;
            if (!bitrate) return;
            var desBitrateInfo = videoBitrateList.find(function(bitrateInfo) {
                return bitrateInfo.bitrate === bitrate;
            });

            if (!desBitrateInfo) return;
            libPlayer.setQualityFor("video", desBitrateInfo.qualityIndex);
        },
        switchAudio = function(requirements) {
            if (!libPlayer) return;
            var lang = requirements.lang;
            if (!audioTracks || audioTracks.length === 0 || !lang) return;
            var desTrack = audioTracks.find(function(audioTrack) {
                return audioTrack.lang === lang;
            });

            if (!desTrack) return;
            libPlayer.setCurrentTrack(desTrack);
            currentAudioTrack = desTrack;
        },

        findSubtitleTrackIndex = function(subtitleLang) {
            var subtitleIndex = Number.NaN;
            if (!subtitleTracks || subtitleTracks.length === 0) {
                return subtitleIndex;
            }

            for (var index = 0; index < subtitleTracks.length; index++) {
                if (subtitleTracks[index].lang === subtitleLang) {
                    subtitleIndex = index;
                    break;
                }
            }
            return subtitleIndex;
        },

        switchSubtitle = function(requirements) {
            if (!libPlayer) return;
            var lang = requirements.lang;
            if (!subtitleTracks || subtitleTracks.length === 0 || !lang) return;

            if (lang === "off") {
                libPlayer.setTextTrack(-1);
            } else {
                var subtitleIndex = findSubtitleTrackIndex(lang);
                if (!isNaN(subtitleIndex)) {
                    libPlayer.setTextTrack(subtitleIndex);
                    currentSubtitleTrack = subtitleTracks[subtitleIndex];
                }
            }
        },

        switchStream = function(type, requirements) {
            log("switchStream invoked(" + type + ")");
            if (type === "audio") {
                switchAudio(requirements);
            } else if (type === "video") {
                switchVideo(requirements);
            } else if (type === "subtitle") {
                switchSubtitle(requirements);
            }
        },
        getListeners = function(type) {
            registrations = registrations || {};
            if (!(type in registrations)) {
                registrations[type] = [];
            }

            return registrations[type];
        },

        addEventListener = function(type, listener) {
            var listeners = getListeners(type);
            var idx = listeners.indexOf(listener);
            if (idx === -1) {
                listeners.push(listener);
            }
        },

        removeEventListener = function(type, listener) {
            var listeners = getListeners(type);
            var idx = listeners.indexOf(listener);
            if (idx !== -1) {
                listeners.splice(idx, 1);
            }
        },

        dispatchEvent = function(evt) {
            var listeners = getListeners(evt.type);
            for (var i = 0; i < listeners.length; i++) {
                listeners[i].call(this, evt);
            }
        },

        log = function(message) {
            console.log(message);
        },

        initParams = function() {
            audioTracks = null;
            videoTracks = null;
            subtitleTracks = null;
            videoBitrateList = null;
            currentSubtitleTrack = null;
            currentAudioTrack = null;
            firstTimeStamp = null;
            initVideoElment();
        },

        initializeLibPlayer = function() {
            initVideoElment();
            libPlayer = LibDMPPlayer.MediaPlayer().create();
            libPlayer.initialize(video, null, true);
            libPlayer.on(LibDMPPlayer.MediaPlayer.events.STREAM_INITIALIZED, onStreamInitialized, this);
            libPlayer.on(LibDMPPlayer.MediaPlayer.events.TEXT_TRACKS_ADDED, onSubtitlesLoaded, this);
            libPlayer.on(LibDMPPlayer.MediaPlayer.events.ERROR, onError, this);
            libPlayer.on(LibDMPPlayer.MediaPlayer.events.PLAYBACK_ENDED, onPlaybackEnded, this);
            libPlayer.on(LibDMPPlayer.MediaPlayer.events.PLAYBACK_PLAYING, onPlaybackPlaying, this);
            libPlayer.on(LibDMPPlayer.MediaPlayer.events.PLAYBACK_STARTED, onPlaybackStart, this);
            libPlayer.on(LibDMPPlayer.MediaPlayer.events.PLAYBACK_SEEKED, onPlaybackSeeked, this);
            libPlayer.on(LibDMPPlayer.MediaPlayer.events.PLAYBACK_SEEKING, onPlaybackSeeking, this);
            libPlayer.on(LibDMPPlayer.MediaPlayer.events.PLAYBACK_WAITING, onPlaybackWaiting, this);
            libPlayer.on(LibDMPPlayer.MediaPlayer.events.PLAYBACK_TIME_UPDATED, onPlaybackTimeUpdated, this);
            libPlayer.on(LibDMPPlayer.MediaPlayer.events.STREAM_DURATION_UPDATED, onStreamDurationUpdated, this);
        },

        destoryLibPlayer = function() {
            if (libPlayer) {
                libPlayer.off(LibDMPPlayer.MediaPlayer.events.STREAM_INITIALIZED, onStreamInitialized, this);
                libPlayer.off(LibDMPPlayer.MediaPlayer.events.TEXT_TRACKS_ADDED, onSubtitlesLoaded, this);
                libPlayer.off(LibDMPPlayer.MediaPlayer.events.ERROR, onError, this);
                libPlayer.off(LibDMPPlayer.MediaPlayer.events.PLAYBACK_ENDED, onPlaybackEnded, this);
                libPlayer.off(LibDMPPlayer.MediaPlayer.events.PLAYBACK_PLAYING, onPlaybackPlaying, this);
                libPlayer.off(LibDMPPlayer.MediaPlayer.events.PLAYBACK_STARTED, onPlaybackStart, this);
                libPlayer.off(LibDMPPlayer.MediaPlayer.events.PLAYBACK_SEEKED, onPlaybackSeeked, this);
                libPlayer.off(LibDMPPlayer.MediaPlayer.events.PLAYBACK_SEEKING, onPlaybackSeeking, this);
                libPlayer.off(LibDMPPlayer.MediaPlayer.events.PLAYBACK_WAITING, onPlaybackWaiting, this);
                libPlayer.off(LibDMPPlayer.MediaPlayer.events.PLAYBACK_TIME_UPDATED, onPlaybackTimeUpdated, this);
                libPlayer.off(LibDMPPlayer.MediaPlayer.events.STREAM_DURATION_UPDATED, onStreamDurationUpdated, this);
                libPlayer.reset();
                libPlayer = null;
            }
        },

        initVideoElment = function() {

            if (video) {
                video.removeAttribute('src');
                video.load();
            }
        },

        destoryVideoElement = function() {
            initVideoElment();
            video = null;
        };

    video = videoElement;
    initializeLibPlayer();

    return {

        addEventListener: function(type, listener) {
            if (!type || !listener) {
                log("params passed is invalid");
                return;
            }
            addEventListener.call(this, type, listener);
        },

        removeEventListener: function(type, listener) {
            if (!type || !listener) {
                log("params passed is invalid");
                return;
            }
            removeEventListener.call(this, type, listener);
        },

        open: function(url, prodata) {
            log("open invoked !");
            if (!libPlayer) {
                log("player is null, please create player instance");
                return;
            }

            initParams();

            if (prodata) {
                libPlayer.setProtectionData(prodata);
            }

            if (url) {
                libPlayer.attachSource(url);
            }
        },

        play: function() {
            log("play invoked !");

            if (!libPlayer) {
                log("player is null, please create player instance");
                return;
            }
            libPlayer.play();
        },

        pause: function() {
            log("pause invoked !");

            if (!libPlayer) {
                log("player is null, please create player instance");
                return;
            }
            libPlayer.pause();
        },

        isPaused: function() {
            log("isPaused invoked !");
            if (!libPlayer) {
                log("player is null, please create player instance");
                return;
            }
            return libPlayer.isPaused();
        },

        setIsNPVR: function(isNPVRFlag) {
            log("setIsNPVR invoked !");
            if (!libPlayer) {
                log("player is null, please create player instance");
                return;
            }
            libPlayer.setIsNPVR(isNPVRFlag);
        },

        getIsNPVR: function() {
            log("getIsNPVR invoked !");
            if (!libPlayer) {
                log("player is null, please create player instance");
                return;
            }
            
            return libPlayer.getIsNPVR();
        },
        setBookMark: function(bookmark) {
            log("setBookMark invoked !");
            if (!libPlayer) {
                log("player is null, please create player instance");
                return;
            }
            libPlayer.setBookMark(bookmark);
        },
        getBookMark: function() {
            log("getBookMark invoked !");
            if (!libPlayer) {
                log("player is null, please create player instance");
                return;
            }
            libPlayer.getBookMark();
        },

        setMute: function(muteFlag) {
            log("setMute invoked !");
            if (!libPlayer) {
                log("player is null, please create player instance");
                return;
            }

            if ((typeof muteFlag) !== 'boolean') {
                log("param type is invalid");
                return;
            }
            libPlayer.setMute(muteFlag);
        },

        isMute: function() {
            log("isMute invoked !");
            if (!libPlayer) {
                log("player is null, please create player instance");
                return;
            }
            return libPlayer.isMuted();
        },

        setPosition: function(position) {
            log("setPosition invoked !");
            if (!libPlayer) {
                log("player is null, please create player instance");
                return;
            }

            if ((typeof position) !== 'number') {
                log("param type is invalid");
                return;
            }

            var isNPVR = libPlayer.getIsNPVR();
            var seekTarget;
            if (isNPVR === true) {
                if (firstTimeStamp === null) {
                    return;
                }
                seekTarget = position + firstTimeStamp;
            } else {
                seekTarget = position;
            }

            libPlayer.seek(seekTarget);
        },

        getPosition: function() {
            if (!libPlayer) {
                log("player is null, please create player instance");
                return;
            }

            if (libPlayer.isDynamic()) {
                return Math.floor(libPlayer.time());
            }

            var isNPVR = libPlayer.getIsNPVR();
            if (isNPVR === false) {
                return Math.floor(libPlayer.time());
            }

            if (firstTimeStamp === null) {
                return 0;
            }

            var tempTimeStamp = Math.floor(libPlayer.time());
            if ((tempTimeStamp - firstTimeStamp) < 0) {
                return 0
            }

            tempTimeStamp = tempTimeStamp - firstTimeStamp;
            return tempTimeStamp;
        },

        setVolume: function(volume) {
            log("setVolume invoked !");
            if (!libPlayer) {
                log("player is null, please create player instance");
                return;
            }

            if ((typeof volume) !== 'number') {
                log("param type is invalid");
                return;
            }

            libPlayer.setVolume(volume);
        },

        getVolume: function() {
            log("getVolume invoked !");
            if (!libPlayer) {
                log("player is null, please create player instance");
                return;
            }

            libPlayer.getVolume();
        },

        getDuration: function() {
            if (!libPlayer) {
                log("player is null, please create player instance");
                return;
            }
            return libPlayer.duration();
        },

        getVersion: function() {
            return DMPVERSION;
        },

        switchStream: switchStream,

        getCurrentBitrate: function() {
            log("getCurrentBitrate invoked !");
            if (!libPlayer) {
                log("player is null, please create player instance");
                return;
            }
            var currentQuality = libPlayer.getQualityFor("video");
            if (!videoBitrateList) {
                return Number.NaN;
            }
            return videoBitrateList.find(function(bitrateInfo) {
                return bitrateInfo.qualityIndex === currentQuality;
            }).bitrate;
        },

        getAllBitRates: function() {
            log("getAllBitRates invoked !");
            if (!libPlayer) {
                log("player is null, please create player instance");
                return;
            }
            if (!videoBitrateList || videoBitrateList.length === 0) {
                return [];
            }
            return videoBitrateList.map(function(bitrateInfo) {
                return bitrateInfo.bitrate;
            });
        },

        getAudioLanguages: function() {
            log("getAudioLanguages invoked !");
            if (!libPlayer) {
                log("player is null, please create player instance");
                return;
            }
            if (!audioTracks || audioTracks.length === 0) {
                return [];
            }
            return audioTracks.map(convertTrackToLanguage);
        },

        getCurrentAudioLanguage: function() {
            log("getCurrentAudioLanguage invoked !");
            if (!libPlayer) {
                log("player is null, please create player instance");
                return;
            }
            var currentAudioTrack = libPlayer.getCurrentTrackFor("audio");
            if (!currentAudioTrack) {
                return "";
            }
            return libPlayer.getCurrentTrackFor("audio").lang;
        },

        getCurrentSubtitleLanguage: function() {
            log("getCurrentSubtitleLanguage invoked !");
            if (!libPlayer) {
                log("player is null, please create player instance");
                return;
            }
            if (!currentSubtitleTrack) {
                return "";
            }
            return currentSubtitleTrack.lang;
        },

        getSubtitleLanguages: function() {
            log("getSubtitleLanguages invoked !");
            if (!libPlayer) {
                log("player is null, please create player instance");
                return;
            }
            if (!subtitleTracks || subtitleTracks.length === 0) {
                return [];
            }
            return subtitleTracks.map(convertTrackToLanguage);
        },

        setSubtitleContainer: function(container) {
            if (!libPlayer) {
                log("player is null, please create player instance");
                return;
            }
            if(!container) {
                log("container is null, please check it");
                return;
            }
            libPlayer.attachTTMLRenderingDiv(container);
        },

        setAutoSwitchQuality: function(autoSwitch) {
            log("setAutoSwitchQuality invoked !");
            if ((typeof autoSwitch) !== 'boolean') {
                log("param type is invalid");
                return;
            }
            libPlayer.setAutoSwitchQuality(autoSwitch);
        },

        setPerferedLang: function(type, value) {
            log("setPerferedLang invoked !");
            if (type != "audio" && type != "subtitle") {
                log("type setting is error, type must be audio or subtitle");
                return;
            }

            if ((typeof value) !== 'string' || value === "") {
                log("param type is invalid or empty");
                return;
            }

            libPlayer.setPerferedLang(type, value);
        },

        getPerferedLang: function(type) {
            log("getPerferedLang invoked !");
            if (!libPlayer) {
                log("player is null, please create player instance");
                return;
            }

            return libPlayer.getPerferedLang(type);
        },

        getAutoSwitchQuality: function() {
            log("getAutoSwitchQuality invoked !");
            if (!libPlayer) {
                log("player is null, please create player instance");
                return;
            }
            return libPlayer.getAutoSwitchQualityFor('video');
        },

        close: function() {
            log("close invoked !");
            if (!libPlayer) {
                log("player is null, please create player instance");
                return;
            }
            audioTracks = null;
            videoTracks = null;
            subtitleTracks = null;
            videoBitrateList = null;
            currentSubtitleTrack = null;
            currentAudioTrack = null;
            registrations = null;
            destoryLibPlayer();
            destoryVideoElement();
        }
    };

};

DMPPlayer.prototype = {
    constructor: DMPPlayer
};

DMPPlayer.events = {
    STREAM_INITIALIZED: "streaminitialized",
    SUBTILES_INITIALIZED: "subtitlesInitialized",
    BUFFER_STARTED: "bufferStarted",
    PLAYBACK_START: "playbackPlayStarted",
    PLAYBACK_SEEKING: "playbackSeeking",
    PLAYBACK_SEEKED: "playbackSeeked",
    PLAYBACK_TIMEUPDATED: "playbackTimeUpdated",
    PLAYBACK_END: "playbackEnd",
    PLAYBACK_ERROR: "playbackError",
    PLAYBACK_QUALITY_CHANGED: "playbackQualitychanged",
    PLAYBACK_PLAYING: "playbackPlaying",
    STREAM_DURATION_UPDATED: "streamDurationUpdated"
};
