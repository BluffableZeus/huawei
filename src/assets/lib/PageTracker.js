﻿(function () {

    var root = this;
    var sendCount = 5, //the number of times of the cache request
        sendTime = 10000, //the default for the interval is 10 seconds for send the request 
        timeOut = 200000; //the time for the request timeout

    var browseContentLog = [],
        browsePageLog = [],
        userClickLog = [],
        commonEventLog = [];
    var sendParams, requestInterval;


    var PageTracker = function () {
        this.__initPageTracker();
    };

    PageTracker.prototype = {
        actionUrl: '', //browse the trajectory collect the server address
        subscriberID: '', //subscriber's ID
        deviceID: '', //the ID of terminal equipment
        epgTemplateID: '', //template number of EPG page
        deviceType: '', //terminal equipment type
        needSend: true, //whether send switch
        appid: '', //the only tag for EPG page
        apppwd: '', //application page of EPG access authentication password
        sendCount: 5, //add up the number of every one
        sendTime: 10000, //the interval time
        __initPageTracker: function () {
            var self = this;
            sendCount = self.sendCount;
            sendTime = self.sendTime;
            clearInterval(requestInterval);
            requestInterval = setInterval(function () {
                if (self.__checkSend(true)) {
                    self.__sendAjaxRequest(sendParams);
                    browseContentLog.splice(0, browseContentLog.length);
                    browsePageLog.splice(0, browsePageLog.length);
                    commonEventLog.splice(0, commonEventLog.length);
                }
            }, sendTime);
        },
        /* [__checkParmsContent inside method，check the parameters of legitimacy when upload parameters]
         * @param  {[string]} logType      [log type：browseContentLog,browsePageLog,commonEventLog]
         * @param  {[object]} parmscontent [content object]
         * @return {[boolean]}   [true:parameters are legal；false:parameters are not legal]
         */
        __checkParmsContent: function (logType, parmscontent) {
            var check = this.__checkParmsError;
            //all events must be send parameters
            var checkParms = {
                'appid': this.appid,
                'apppwd': this.apppwd,
                'subscriberID': this.subscriberID,
                'deviceID': this.deviceID
            };
            // different events different parameters
            switch (logType) {
                case 'browseContentLog':
                    checkParms.actionType = parmscontent.actionType;

                    if (parmscontent.actionType === 'searchContent') {
                        checkParms.keyword = parmscontent.keyword;
                    }
                    if (parmscontent.actionType === 'viewContentDetail') {
                        checkParms.source = parmscontent.source;
                    }
                    break;
                case 'commonEventLog':
                    checkParms.actionType = parmscontent.actionType;
                    //search content by the keyword, this keyword field must be choose
                    if (parmscontent.actionType === 'searchContent') {
                        checkParms.keyword = parmscontent.keyword;
                    }
                    break;
                case 'browsePageLog':
                    checkParms.pageFrom = parmscontent.pageFrom;
                    checkParms.pageTo = parmscontent.pageTo;
                    break;
                case 'userClickLog':
                    checkParms = {};
                    checkParms.EpgPageName = parmscontent.EpgPageName;
                    checkParms.EpgPagePath = parmscontent.EpgPagePath;
                    break;
                default:
                    break;
            }

            for (var key in checkParms) {
                if (checkParms.hasOwnProperty(key) && !check(key, checkParms[key])) {
                    return false;
                }
            }
            return true;
        },
        /* [__checkParmsError judge is empty or undefined，print the error log]
         * @param  {[type]} name [description]
         * @param  {[type]} val  [description]
         * @return {[type]}      [description]
         */
        __checkParmsError: function (name, val) {
            if (val === '') {
                return false;
            }
            if (typeof (val) === 'undefined') {
                return false;
            }
            return true;
        },
        __sendAjaxRequest: function (params) {
            if (this.needSend && this.userLogCollectStatus) {
                //not upload the page jump event when browsing

                var ajaxRequest = new XMLHttpRequest();
                ajaxRequest.open("POST", this.actionUrl);
                ajaxRequest.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                ajaxRequest.send(JSON.stringify(params));
                ajaxRequest.addEventListener('readystatechange', function () {
                    if ((ajaxRequest.readyState == 4) && (ajaxRequest.status == 200)) {

                    } else {
                        var error = ajaxRequest.responseText;
                        browseContentLog = [];
                        browsePageLog = [];
                        commonEventLog = [];
                    }
                });

            }
        },
        __checkSend: function (isTimeout) {
            var returnVal = false;
            if (isTimeout) {
                if ((parseInt(browseContentLog.length) + parseInt(browsePageLog.length) + parseInt(commonEventLog.length)) > 0) {
                    sendParams = {
                        "appid": this.appid,
                        "apppwd": this.apppwd,
                        "browseContentLog": browseContentLog,
                        "browsePageLog": browsePageLog,
                        "commonEventLog": commonEventLog
                    };
                    returnVal = true;
                }
            } else {
                if ((browseContentLog.length === sendCount || browsePageLog.length === sendCount || commonEventLog.length === sendCount) || (parseInt(browseContentLog.length) + parseInt(browsePageLog.length) + parseInt(commonEventLog.length) > sendCount)) {
                    returnVal = true;
                }
            }
            return returnVal;
        },
        /* get the parameters which user set that need to be uoload
         * @param  {[type]} browseParamsMap [default parameters list]
         * @param  {[type]} paramsArray     [specific parameters list]
         * @return {[type]}                 [description]
         */
        __getSendParamsObj: function (browseParamsMap, paramsArray) {
            var contentObj = {};
            for (var i = 0; i < paramsArray.length; i++) {
                if (paramsArray[i]) {
                    //if the parameters has been set up and need to be upload
                    contentObj[browseParamsMap[i]] = paramsArray[i];
                }
            }
            return contentObj;
        },
        /*[start description]start upload data, compatible with the old version
         * @param  {[type]} events [description]
         * @param  {[type]} parms0 [description]
         * @param  {[type]} parms1 [description]
         * @param  {[type]} parms2 [description]
         * @param  {[type]} parms3 [description]
         * @param  {[type]} parms4 [description]
         * @param  {[type]} parms5 [description]
         * @param  {[type]} parms6 [description]
         * @param  {[type]} parms7 [description]
         * @return {[type]}        [description]
         */
        start: function (events, parms0, parms1, parms2, parms3, parms4, parms5, parms6, parms7) {
            if (events === "browseContentLog") { //browse the content
                var contentObj = {};
                contentObj.subscriberID = this.subscriberID;
                contentObj.deviceID = this.deviceID;
                contentObj.deviceType = this.deviceType;
                parms0 && (contentObj.actionType = parms0);
                parms1 && (contentObj.contentCode = parms1);
                parms2 && (contentObj.businessType = parms2);
                parms3 && (contentObj.keyword = parms3);
                parms4 && (contentObj.genre = parms4);
                parms5 && (contentObj.castID = parms5);
                if (parms0 === 'viewContentDetail') {
                    parms6 && (contentObj.source = parms6);
                }
                parms7 && (contentObj.timestamp = parms7);

                browseContentLog.push(contentObj);

            } else if (events === "browsePageLog") { //browse the page
                var pageObj = {};
                pageObj.subscriberID = this.subscriberID;
                pageObj.deviceID = this.deviceID;
                pageObj.epgTemplateID = this.epgTemplateID;
                pageObj.deviceType = this.deviceType;

                parms0 && (pageObj.pageFrom = parms0);
                parms1 && (pageObj.pageDiv = parms1);
                parms2 && (pageObj.pageTo = parms2);
                parms3 && (pageObj.stayTime = parms3);
                parms4 && (pageObj.timestamp = parms4);

                browsePageLog.push(pageObj);

            } else if (events === "commonEventLog") { //expand
                var eventObj = {};
                eventObj.subscriberID = this.subscriberID;
                eventObj.deviceID = this.deviceID;
                eventObj.deviceType = this.deviceType;

                parms0 && (eventObj.actionType = parms0);
                parms1 && (eventObj.label = parms1);
                parms2 && (eventObj.timestamp = parms2);

                commonEventLog.push(eventObj);

            }
            if (this.__checkSend(false)) {
                sendParams = {
                    "appid": this.appid,
                    "apppwd": this.apppwd,
                    "browseContentLog": browseContentLog,
                    "browsePageLog": browsePageLog,
                    "commonEventLog": commonEventLog
                };
                this.__sendAjaxRequest(sendParams);

                browseContentLog.splice(0, sendCount);
                browsePageLog.splice(0, sendCount);
                commonEventLog.splice(0, sendCount);
            }
        },
        /* start [push] to different list and upload data
         * @param  {[type]} events      [event type]
         * @param  {[type]} paramsArray [input parameters array]
         * @return {[type]}             [description]
         */
        __start: function (events, paramsArray) {
            switch (events) {
                case 'browseContentLog':
                    var contentObj = {},
                        contentParamsMap = {
                            0: 'actionType',
                            1: 'contentCode',
                            2: 'businessType',
                            3: 'keyword',
                            4: 'genre',
                            5: 'castID',
                            6: 'source',
                            7: 'timestamp',
                            8: 'entrance',
                            9: 'recmActionID',
                            10: 'number',
                            11: 'year',
                            12: 'productZone',
                            13: 'recmActionType',
                            14: 'productID',
                            15: 'extensionFields'
                        };
                    //set up input parameters
                    contentObj = this.__getSendParamsObj(contentParamsMap, paramsArray);

                    contentObj.subscriberID = this.subscriberID;
                    contentObj.deviceID = this.deviceID;
                    contentObj.deviceType = this.deviceType;
                    browseContentLog.push(contentObj);
                    break;
                case 'browsePageLog':
                    var pageObj = {},
                        pageParamsMap = {
                            0: 'pageFrom',
                            1: 'pageDiv',
                            2: 'pageTo',
                            3: 'stayTime',
                            4: 'timestamp',
                            5: 'extensionFields'
                        };
                    pageObj = this.__getSendParamsObj(pageParamsMap, paramsArray);

                    pageObj.subscriberID = this.subscriberID;
                    pageObj.deviceID = this.deviceID;

                    pageObj.deviceType = this.deviceType;

                    browsePageLog.push(pageObj);
                    break;
                case 'commonEventLog':
                    var commonObj = {},
                        commonParamsMap = {
                            0: 'actionType',
                            1: 'label',
                            2: 'timestamp'
                        };
                    commonObj = this.__getSendParamsObj(commonParamsMap, paramsArray);
                    commonObj.subscriberID = this.subscriberID;
                    commonObj.deviceID = this.deviceID;
                    commonObj.deviceType = this.deviceType;
                    commonEventLog.push(commonObj);
                    break;
                case 'userClickLog':
                    var pageObj = {},
                        pageParamsMap = {
                            0: 'EpgPageName',
                            1: 'EpgPagePath',
                            2: 'OperType',
                            3: 'OperObjectCode',
                            4: 'LinkInTime',
                            5: 'LinkOutTime',
                            6: 'extensionFields'
                        };
                    pageObj = this.__getSendParamsObj(pageParamsMap, paramsArray);
                    userClickLog.push(pageObj);
                    break;
                default:
                    break;
            }

            if (this.__checkSend(false)) {
                sendParams = {
                    "appid": this.appid,
                    "apppwd": this.apppwd,
                    "browseContentLog": browseContentLog,
                    "browsePageLog": browsePageLog,
                    "commonEventLog": commonEventLog
                };
                this.__sendAjaxRequest(sendParams);

                browseContentLog.splice(0, sendCount);
                browsePageLog.splice(0, sendCount);
                commonEventLog.splice(0, sendCount);
            }
        },
        /* [pushBrowseContentList description]upload the browsing content data including list,detail and search
         * @param  {[type]} contentObj [description] [actionType] must be upload
         * @return {[type]}            [description]
         */
        __pushBrowseContent: function (contentObj) {

            var actionType = contentObj.actionType,
                contentCode = contentObj.contentCode ? contentObj.contentCode : false,
                businessType = contentObj.businessType ? contentObj.businessType : false,
                keyword = contentObj.keyword ? contentObj.keyword : false,
                genre = contentObj.genre ? contentObj.genre : false,
                castID = contentObj.castID ? contentObj.castID : false,
                source = contentObj.source ? contentObj.source : false,
                timestamp = contentObj.timestamp ? contentObj.timestamp : false,
                //interface to enhance============================================================
                entrance = contentObj.entrance || '',
                //the request of the corresponding serial number which the user to select,
                //the user to view content detail, from search content,  the corresponding serial number which is from search
                recmActionID = contentObj.recmActionID || '',
                number = contentObj.number || '',
                //the filter conditions of the user browse the content list additional when operation type is [browseContentList]  
                //corresponding field must be upload when filter by [year、productZone、genre、castID]
                year = contentObj.year || '',
                productZone = contentObj.productZone || '',
                //the operation type of the user to access recommend the results，value including：0：watch, 1；subscribe, 2：favorite, 3：reminder
                recmActionType = contentObj.recmActionType || '',
                //when operation type [actionType] is [writeRecmBehavior] that is effective what the behavior records of the user to access and
                // the operation type of the user to access is subscriber
                productID = contentObj.productID || '',
                extensionFields = contentObj.extensionFields || '';
            //=========================================================================
            if (this.__checkParmsContent('browseContentLog', contentObj)) {
                var contentParamsArray = [actionType, contentCode,
                    businessType, keyword, genre, castID, source, timestamp, entrance, recmActionID, number, year, productZone, recmActionType, productID, extensionFields
                ];
                top.pageTracker.__start('browseContentLog', contentParamsArray);
            }

        },

        /* [pushBrowseContentList description]browse the content list
         * @param  {[type]} contentObj [description]
         * @return {[type]}            [description]
         */
        pushBrowseContentList: function (contentObj) {
            contentObj.actionType = 'browseContentList';
            this.__pushBrowseContent(contentObj);
        },
        /* [pushBrowseContentDetail description]browse the content detail
         * @param  {[type]} contentObj [description]
         * @return {[type]}            [description]
         */
        pushBrowseContentDetail: function (contentObj) {
            contentObj.actionType = 'viewContentDetail';
            this.__pushBrowseContent(contentObj);
        },
        /* [pushBrowseSearchContent description]keyword search
         * @param  {[type]} contentObj [description]
         * @return {[type]}            [description]
         */
        pushBrowseSearchContent: function (contentObj) {
            contentObj.actionType = 'searchContent';
            this.__pushBrowseContent(contentObj);
        },
        /* [pushBrowseSearchContent description]the behavior records of the user to access result
         * @param  {[type]} contentObj [description]
         * @return {[type]}            [description]
         */
        pushBrowseRecmBehavior: function (contentObj) {
            contentObj.actionType = 'writeRecmBehavior';
            this.__pushBrowseContent(contentObj);
        },
        /* [pushBrowsePage description]page jump event be upload
         * @param  {[type]} pageObj [description] [pageFrom,pageTo] must be upload
         * @return {[type]}         [description]
         */
        pushBrowsePage: function (pageObj) {
            var pageFrom = pageObj.pageFrom,
                pageDiv = pageObj.pageDiv ? pageObj.pageDiv : false,
                pageTo = pageObj.pageTo,
                stayTime = pageObj.stayTime ? pageObj.stayTime : false,
                timestamp = pageObj.timestamp ? pageObj.timestamp : '',
                extensionFields = pageObj.extensionFields || '';
            if (this.__checkParmsContent('browsePageLog', pageObj)) {
                var pageParamsArray = [pageFrom, pageDiv, pageTo, stayTime, timestamp, extensionFields];
                top.pageTracker.__start('browsePageLog', pageParamsArray);
            }
        },
        /*[pushBrowsePage description]page jump event be upload
         * @param  {[type]} pageObj [description] [pageFrom,pageTo] must be upload
         * @return {[type]}         [description]
         */
        //TODO only provide ability
        userClickLog: function (pageObj) {
            var EpgPageName = pageObj.EpgPageName || false,
                EpgPagePath = pageObj.EpgPagePath || false,
                OperType = pageObj.OperType || false,
                OperObjectCode = pageObj.OperObjectCode || false,
                LinkInTime = pageObj.LinkInTime || false,
                LinkOutTime = pageObj.LinkOutTime || false,
                extensionFields = pageObj.extensionFields || false;
            if (this.__checkParmsContent('userClickLog', pageObj)) {
                var pageParamsArray = [EpgPageName, EpgPagePath, OperType, OperObjectCode, LinkInTime, LinkOutTime, extensionFields];
                top.pageTracker.__start('userClickLog', pageParamsArray);
            }
        },
        /* [pushCommonEvent description]to upload the event is expand and general 
         * @param  {[type]} commonObj [description] [actionType] must be upload
         * @return {[type]}           [description]
         */
        pushCommonEvent: function (commonObj) {
            var actionType = commonObj.actionType,
                label = commonObj.label ? commonObj.label : false,
                timestamp = commonObj.timestamp ? commonObj.timestamp : false;
            if (this.__checkParmsContent('commonEventLog', commonObj)) {
                var commonParamsArray = [actionType, label, timestamp];
                top.pageTracker.__start("commonEventLog", commonParamsArray);
            }
        },
        /* [setTrackerServerUrl set user collection server address of browse trajectory]
         * @param {[type]} url [description]
         */
        setTrackerServerUrl: function (url) {
            //the URL from emulate authentication interface has including address
            this.actionUrl = url;
        },
        //initialize the trajectory collection parameters
        setTrackerParams: function (params) {
            //set user browsing trajectory collection server address
            this.setTrackerServerUrl(params.actionUrl);
            //subscribe ID
            this.subscriberID = params.subscriberID || '';
            //terminal physical device ID
            this.deviceID = params.deviceID || '';
            //template number of EPG page
            this.epgTemplateID = params.epgTemplateID || '';
            //the only tag for EPG page application
            this.appid = params.appid || '';
            //EPG page application access authentication password
            this.apppwd = params.apppwd || '';
            //whether report log switch
            this.needSend = params.needSend || false;
            //the type of terminal physical device
            this.deviceType = params.deviceType || '';
            //optional, send it when add up 5 default
            this.sendCount = params.sendCount || 5;
            //optional, send it when ten seconds default
            this.sendTime = params.sendTime || 10000;
        },
        setStatus: function (status) {
            this.userLogCollectStatus = (status === undefined) ? true : (status + '' === '1');
        }
    };
    pageTracker = new PageTracker();

    if (typeof exports !== 'undefined') {
        if (typeof module !== 'undefined' && module.exports) {
            exports = module.exports = pageTracker;
        }
        exports.pageTracker = pageTracker;
    } else {
        root.pageTracker = pageTracker;
    }
}.call(this));
