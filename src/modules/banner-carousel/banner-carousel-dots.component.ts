import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core'
import { Observable, Subscription } from 'rxjs'

@Component({
  selector: 'app-banner-carousel-dots',
  templateUrl: './banner-carousel-dots.component.html',
  styleUrls: ['./banner-carousel-dots.component.scss']
})
export class BannerCarouselDotsComponent implements OnInit, OnDestroy {

  dots: number[] = []
  middle: number = -1
  activeDot: number = -1

  dotsClasses: any = {
    next: false,
    prev: false
  }

  animationState: boolean = true
  animationTimeout: number = 0
  directionSubscription!: Subscription

  @Input() count: number = 0
  @Input() currentIndex: number = -1
  @Input() direction!: Observable<string>
  @Input('speed') animationSpeed: number = 1000

  @Output('on-click') onClick: EventEmitter<number> = new EventEmitter<number>()
  @Output() directionChanged: EventEmitter<string> = new EventEmitter<string>()

  constructor() { }

  ngOnInit() {
    this.dots = Array(this.count).fill('').map((x: any, i: number) => i)
    this.middle = Math.floor(this.count / 2)
    this.directionSubscription = this.direction.subscribe(value => this.onDirection(value))
  }

  click(index: number) {
    this.directionChanged.emit(index < this.middle ? 'prev': 'next')
  }

  getTransition() {
    return this.animationState ? `all ${ this.animationSpeed / 1000 }s ease` : 'none'
  }
  
  onDirection(dir: string) {
    if (this.animationTimeout) {
      return
    }
    this.dotsClasses[dir] = true
    this.animationTimeout = window.setTimeout(() => {
      this.animationState = false
      this.dotsClasses[dir] = false
      setTimeout(() => {
        this.animationState = true
        this.animationTimeout = 0
      }, 30)
    }, this.animationSpeed)
  }

  ngOnDestroy() {
    this.directionSubscription.unsubscribe()
  }

}
