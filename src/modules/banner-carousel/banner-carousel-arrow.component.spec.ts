import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BannerCarouselArrowComponent } from './banner-carousel-arrow.component';

describe('BannerCarouselArrowComponent', () => {
  let component: BannerCarouselArrowComponent;
  let fixture: ComponentFixture<BannerCarouselArrowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BannerCarouselArrowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BannerCarouselArrowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
