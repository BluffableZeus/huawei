import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { RouterModule } from '@angular/router'

import { BannerCarouselComponent } from './banner-carousel.component'
import { BannerCarouselItemComponent } from './banner-carousel-item.component'
import { BannerCarouselArrowComponent } from './banner-carousel-arrow.component'
import { BannerCarouselDotsComponent } from './banner-carousel-dots.component'
import { TranslateModule } from "@ngx-translate/core"
import { NavigationButtonComponent } from '../navigation-button/navigation-button.component';

@NgModule({
    imports: [
      RouterModule,
      CommonModule,
      TranslateModule
    ],
    exports: [
      BannerCarouselComponent,
      BannerCarouselItemComponent,
      BannerCarouselArrowComponent,
      BannerCarouselDotsComponent
    ],
    declarations: [
      NavigationButtonComponent,
      BannerCarouselComponent,
      BannerCarouselItemComponent,
      BannerCarouselArrowComponent,
      BannerCarouselDotsComponent
    ],
    providers: [],
})
export class BannerCarouselModule { };
