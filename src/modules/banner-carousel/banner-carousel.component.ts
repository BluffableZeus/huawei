import { Component, OnInit, OnDestroy, Input, Output, EventEmitter, HostListener } from '@angular/core';
import { Router } from '@angular/router'
import { Subject } from 'rxjs'

@Component({
  selector: 'app-banner-carousel',
  templateUrl: './banner-carousel.component.html',
  styleUrls: ['./banner-carousel.component.scss']
})
export class BannerCarouselComponent implements OnInit, OnDestroy {

  @Input('data') data: any = []
  @Input() width: number = 900
  @Input() height: number = 375
  @Input('speed') animationSpeed: number = 1000
  @Input('autoScroll') autoScroll: boolean = false
  @Input('autoSpeed') autoSpeed: number = 10000

  private direction: Subject<string> = new Subject<string>();

  itemsLength: number = 0
  currentIndex: number = 0;
  baseTranslateX: number = -1
  animationState: boolean = true
  animationTimeout: number = 0
  autoScrollTimeout: number = 0

  constructor(
    private router: Router
  ) { }

  ngOnInit() {
    this.itemsLength = this.data.length
    this.currentIndex = Math.floor(this.itemsLength / 2)
    this.baseTranslateX = this.getBaseTranslateX()
    this.startAutoScroll()
  }

  startAutoScroll() {
    this.autoScrollTimeout = window.setTimeout(() => {
      this.next()
      this.startAutoScroll()
    }, this.autoSpeed)
  }

  clearAutoScroll() {
    if (this.autoScrollTimeout) {
      clearTimeout(this.autoScrollTimeout)
      this.autoScrollTimeout = 0
    }
  }

  getBaseTranslateX() {
    return this.width
  }

  @HostListener('window:resize', ['$event'])
  onResize(event: any) {
    this.baseTranslateX = this.getBaseTranslateX()
  }

  animation(state: boolean): void {
    this.animationState = state
    if (state) {
      this.animationTimeout = 0
    }
  }

  getTransition(): string {
    return this.animationState ? `all ${ this.animationSpeed / 1000 }s` : 'none'
  }

  onItemClick(item) {
    if (item && item.ID) {
      this.router.navigate(['video', 'movie', item.ID])
    }
  }

  next() {
    
    if (this.animationTimeout) {
      return
    }

    this.direction.next('next')

    this.currentIndex = this.currentIndex + 1
    this.animationTimeout = window.setTimeout(() => {
      this.animation(false)
      this.data.push(this.data.shift())
      this.currentIndex = this.currentIndex - 1
      setTimeout(() => this.animation(true), 30)
    }, this.animationSpeed)
  }

  prev() {

    if (this.animationTimeout) {
      return
    }

    this.direction.next('prev')

    this.currentIndex = this.currentIndex - 1
    this.animationTimeout = window.setTimeout(() => {
      this.animation(false)
      this.currentIndex = this.currentIndex + 1
      this.data.unshift(this.data.pop())
      setTimeout(() => this.animation(true), 30)
    }, this.animationSpeed)
  }

  getTranslateX(): number {
    const position = this.baseTranslateX + ((this.currentIndex - 1) * this.width)
    return position * -1
  }

  getContainerTransform(): string {
    return `translateX(${ this.getTranslateX() }px)`
  }

  directionChanged(dir: string) {
    if (dir === 'prev') {
      return this.prev()
    }
    if (dir === 'next') {
      return this.next()
    }
  }

  controlMouseEnter() {
    this.clearAutoScroll()
  }

  controlMouseLeave() {
    this.startAutoScroll()
  }

  ngOnDestroy() {
    this.clearAutoScroll()
  }

}
