import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BannerCarouselItemComponent } from './banner-carousel-item.component';

describe('BannerCarouselItemComponent', () => {
  let component: BannerCarouselItemComponent;
  let fixture: ComponentFixture<BannerCarouselItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BannerCarouselItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BannerCarouselItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
