import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core'

@Component({
  selector: 'app-banner-carousel-arrow',
  templateUrl: './banner-carousel-arrow.component.html',
  styleUrls: ['./banner-carousel-arrow.component.scss']
})
export class BannerCarouselArrowComponent implements OnInit {

  @Input() dir: string = ''

  @Output('on-click') click: EventEmitter<any> = new EventEmitter<any>()

  filterAttr = ''
  fillAttr = ''

  constructor() { }

  ngOnInit() {
    this.filterAttr = `url(${ location.pathname }#filter0_d)`
    this.fillAttr = `url(${ location.pathname }#paint0_linear)`
  }

  onClick() {
    this.click.emit(this.dir)
  }

}
