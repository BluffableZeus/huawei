import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BannerCarouselDotsComponent } from './banner-carousel-dots.component';

describe('BannerCarouselDotsComponent', () => {
  let component: BannerCarouselDotsComponent;
  let fixture: ComponentFixture<BannerCarouselDotsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BannerCarouselDotsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BannerCarouselDotsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
