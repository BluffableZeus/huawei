import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'home'
  },
  {
    path: 'home',
    data: { preload: true },
    loadChildren: './home/home.module#HomeModule'
  },
  {
    path: 'live-tv',
    data: { preload: true },
    loadChildren: './live-tv/live-tv.module#LiveTVModule'
  },
  {
    path: 'video',
    data: { preload: true },
    loadChildren: './on-demand/on-demand.module#OnDemandModule'
  },
  {
    path: 'profile',
    data: { preload: true },
    loadChildren: './my-tv/my-tv.module#MyTVModule'
  },
  {
    path: 'page',
    loadChildren: './page/page.module#PageModule'
  },
  {
    path: 'storybook',
    loadChildren: './storybook/storybook.module#StorybookModule'
  }
]

/**
 *  Type of the NgModule metadata.
 */
@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
