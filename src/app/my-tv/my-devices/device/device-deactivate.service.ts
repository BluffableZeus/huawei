import { Injectable } from '@angular/core'
import { CanDeactivate } from '@angular/router'
import { Observable } from 'rxjs'
import { session } from 'src/app/shared/services/session'
import { EventService } from 'ng-epg-sdk'

export interface CanComponentDeactivate {
  canDeactivate: () => Observable<boolean> | Promise<boolean> | boolean
}

@Injectable()
export class DeviceDeactivate implements CanDeactivate<CanComponentDeactivate> {
  // to judge whether the message of current page is changed.
  public isInfoChanged = false
  // to save route url message.
  public currentUrl = ''

  /**
   * function used to get route information.
   */
  getRouteMessage (e) {
    if (e['srcElement']['hash']) {
      let hash = e['srcElement']['hash'] + ''
      this.currentUrl = hash.split('#')[1] || hash
      session.put('RouteMessage', this.currentUrl)
    }
  }

  /**
   * function used to decide whether routing hop is allowable.
   */
  canDeactivate (component: CanComponentDeactivate) {
    if (this.isInfoChanged && document.getElementById('changeDeviceName') &&
      document.getElementById('changeDeviceName')['value'] !== '') {
      document.onclick = this.getRouteMessage
      EventService.emit('POP_UP_DEVICE_SUBMIT')
      return false
    } else {
      return true
    }
  }
}
