import * as _ from 'underscore'
import { Component, OnInit, OnDestroy } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { DeviceInfoService } from '../device-name.service'
import { session } from 'src/app/shared/services/session'
import { MyTvService } from '../../my-tv.service'
import { DeviceDeactivate } from './device-deactivate.service'
import { Device, EventService } from 'ng-epg-sdk'

@Component({
  selector: 'app-device-name',
  styleUrls: ['./device-name.component.scss'],
  templateUrl: './device-name.component.html'
})

export class DeviceNameComponent implements OnInit, OnDestroy {
    // to save the device name.
  public deviceName: string

  public device: Device
    // to judge whether device name is changed.
  public changedDeviceName = false
    // is save button disabled
  public btnDisabled = true
    // to judge whether show delete button.
  public isShowNameDelete = false
    // keep the message about entering error device name.
  public deviceNameErrorMsg: string
    // if true, the error message is visible.if false, the error message is hidden.
  public isShowDeviceNameError = false
    // to judge whether show submit tips dialog.
  public showSubmitDialog = false
    // to show tips message about inputing.
  public deviceTipMsgRule2
    // to save the black list of inputing.
  public blackListCache = ''
    // to save the max length of inputing.
  public maxLengthCache = 128

    // private constructor
  constructor (
        private service: DeviceInfoService,
        private router: Router,
        private route: ActivatedRoute,
        private myTvService: MyTvService,
        private deviceDeactivate: DeviceDeactivate
    ) { }
    /**
     * process init data and register event listeners.
     */
  ngOnInit () {
    this.blackListCache = session.get('COMMON_INPUT_BLACKLIST_CACHE')
    this.maxLengthCache = session.get('COMMON_INPUT_MAXLENGTH_CACHE')
    EventService.removeAllListeners(['POP_UP_DEVICE_SUBMIT'])

    this.route.params.subscribe(params => {
      this.service.getDevice(params.deviceID).subscribe(value => {
        this.device = value
        this.deviceName = value.name
      })
    })

    EventService.on('POP_UP_DEVICE_SUBMIT', () => {
      this.showSubmitDialog = true
    })

    if (this.blackListCache !== '') {
      this.deviceTipMsgRule2 = { char: this.blackListCache }
    }
  }
    /**
     * lifecycle function 'ngOnDestroy', which is used to clear timer and cancel event listeners.
     */
  ngOnDestroy () {
    EventService.removeAllListeners(['POP_UP_DEVICE_SUBMIT'])
  }

    /**
     * save the setting after changing device name.
     */
  saveDeviceName () {
    this.callModifyInterface().then(() => {
      this.router.navigate(['/profile', 'devices'])
    })
  }

    /**
     * call ModifyDeviceInfo interface to modify the device name.
     */
  callModifyInterface () {
      // if the device name is unchanged,you can not to save.
    if (!this.changedDeviceName || this.deviceNameErrorMsg || this.deviceName === this.device.name) {
      return
    }
    session.put('IS_MANUAL_SET_DEVICE_NAME', true)

      // if the device name is changed correctly,keep the changed device name.
    this.device.name = this.service.toOneSpace(this.deviceName)

    return this.service.modifyDeviceInfo({ device: this.device }).then(resp => {
      if (resp.result.retCode === '000000000') {
        this.deviceName = this.device.name
        if (('' + session.get('deviceID')) === this.device.ID) {
            session.put('deviceName', this.device.name)
          }
        this.isDeviceNameChanged(this.deviceName)
        EventService.emit('PROFILE_INFO_SAVE_SUCCESSFUL')
      }
    })
  }

    /**
     * Listening to the keyboard events of device name box.
     */
  deviceNameChanged (deviceName: string) {
    this.isDeviceNameChanged(deviceName)
      // judge the delete button whether is visible.
    this.isShowNameDelete = this.isDeleteVisible(deviceName)
  }

    /**
     * when the device name input get focus.
     */
  nameInput (value: string) {
    this.isShowDeviceNameError = false
    this.isShowNameDelete = this.isDeleteVisible(value)
  }

    /**
     * when the device name input lose focus.
     */
  nameBlur (value: string) {
    _.delay(() => {
      this.isShowNameDelete = false
    }, 300)

      // initialize the error message.
    this.isShowDeviceNameError = true
    this.deviceNameErrorMsg = ''
    if (value.length > this.maxLengthCache) {
      this.deviceNameErrorMsg = 'input_does_not_conform_rules'
      return
    }

      // if the device name is empty or undefined,the error message about that the device name is empty is visible.
    if (value.trim() === '' || value === undefined) {
      this.deviceNameErrorMsg = 'device_name_empty'
      return
    }

    if (this.blackListCache !== '') {
      if (!(/^[A-Z|a-z|0-9]+$/.test(value))) {
        let numLetters = value.replace(/[A-Z|a-z|0-9]/ig, '')
        let numLettersArr = numLetters.split('')
        if (numLettersArr.length !== 0) {
            let hasLettersOfBlackList = false
            nameOuter: for (let i = 0; i < numLettersArr.length; i++) {
              for (let j = 0; j < this.blackListCache.length; j++) {
                if (numLettersArr[i] === this.blackListCache.charAt(j) || (/[\u4e00-\u9fa5]/.test(numLettersArr[i]))) {
                  hasLettersOfBlackList = true
                  break nameOuter
                }
              }
            }
            if (hasLettersOfBlackList) {
              this.deviceNameErrorMsg = 'input_does_not_conform_rules'
            }
          }
      }
    }
  }

    /**
     * click the delete button.
     */
  deviceNameDelete () {
    this.deviceName = ''
      // the input get focus.
    document.getElementById('changeDeviceName').focus()
  }

    /**
     * judge the device name whether is changed.
     */
  isDeviceNameChanged (value: string) {
    if (value === this.device.name) {
      this.changedDeviceName = false
      this.btnDisabled = true
      this.deviceDeactivate.isInfoChanged = false
    } else {
      this.changedDeviceName = true
      this.btnDisabled = false
      this.deviceDeactivate.isInfoChanged = true
    }
  }

  isDeleteVisible (inputValue) {
    return !(inputValue === '' || inputValue === undefined)
  }

    /**
     * close the submit dialog.
     */
  closeSubDialog ($event) {
    this.showSubmitDialog = false
    EventService.emit('DEVICE_FOCUS')
  }

    /**
     * click the cancel button of the submit dialog.
     */
  cancelOperation ($event) {
    this.showSubmitDialog = false
    this.deviceDeactivate.isInfoChanged = false
      // continue jump to where you want.
    this.sessionRouterAction()
  }

    /**
     * click the submit button of the submit dialog.
     */
  submitData ($event) {
    this.showSubmitDialog = false
    this.callModifyInterface().then(() => {
      this.sessionRouterAction()
    })
  }

  sessionRouterAction () {
    if (session.get('PROFILE_NOTSAVE_LOGOUT_TYPE')) {
      _.delay(() => {
        EventService.emit('PROFILE_NOTSAVE_LOGOUT')
        this.router.navigate(['' + session.get('RouteMessage')])
      }, 1000)
    } else {
      this.router.navigate(['' + session.get('RouteMessage')])
    }
  }
}
