import { Injectable } from '@angular/core'
import { modifyDeviceInfo } from 'ng-epg-sdk/vsp'
import { queryDeviceList } from 'ng-epg-sdk'
import { Device } from 'ng-epg-sdk/vsp/api'
import { session } from 'src/app/shared/services/session'
import { Observable, Subject } from 'rxjs'
import 'rxjs/add/operator/filter'
import 'rxjs/add/operator/switchMap'
import 'rxjs/add/observable/from'

@Injectable()
export class DeviceInfoService {
  responseDevices: Subject<Device[]> = new Subject<Device[]>()
  devices: Device[] = []
  loading = false

  constructor () { }
    /**
     * function used to call interface to modify device information.
     */
  modifyDeviceInfo (req: any) {
    return modifyDeviceInfo(req).then(resp => {
      return resp
    })
  }

  getDevices (): Observable<Device[]> {
    if (!this.loading) {
      this.loading = true

        // 0: STB
        // 2: OTT device, including the PC plug-in, iOS, and Android devices
        // 3: mobile device for the MTV solution
        // 4: all (somehow, it doesn't work, that is why we send all three types)
      queryDeviceList({
        subscriberID: session.get('SUBSCRIBER_ID'),
        profileID: session.get('PROFILE_ID'),
        deviceType: '0;2;3'
      }).then(value => {
          this.devices = value.devices
          this.responseDevices.next(this.devices)
          this.loading = false
        }).catch(reason => {
          this.responseDevices.error(reason)
          this.loading = false
        })
    }

    return this.responseDevices.asObservable()
  }

  getDevice (deviceId: string): Observable<Device> {
    return (this.loading ? this.responseDevices.asObservable() : this.getDevices())
        .switchMap((l: Device[]) => Observable.from(l))
        .filter((device: Device) => {
          return device.ID === deviceId
        })
  }

    /**
     * function used to make there is only one space between two words.
     */
  toOneSpace (str: string) {
    return str.trim().replace(/\s+/g, ' ')
  }
}
