import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { DeviceInfoService } from './device-name.service'
import { Device } from 'ng-epg-sdk/vsp/api'

@Component({
  templateUrl: './my-devices.component.html',
  styleUrls: ['./my-devices.component.scss']
})
export class MyDevicesComponent implements OnInit {
  devices: Device[] = []

  constructor (
        protected router: Router,
        protected service: DeviceInfoService
    ) {
  }

  ngOnInit (): void {
    this.service.getDevices().subscribe(devices => {
      this.devices = devices
    })
  }
}
