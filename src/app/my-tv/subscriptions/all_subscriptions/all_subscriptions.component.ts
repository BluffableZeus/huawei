import { Component, OnInit } from '@angular/core'
import { SubscriptionsService } from '../subscriptions.service'
import { WeakMessageDialogComponent } from '../../../component/weakMessageDialog/weakMessageDialog.component'
import { CommonService } from '../../../core/common.service'

@Component({
  selector: 'app-all-subscriptions',
  templateUrl: './all_subscriptions.component.html',
  styleUrls: ['../subscriptions.common.scss']
})
export class AllSubscriptionsComponent implements OnInit {
  public dataList = []
  public processing = false

  constructor (
        private service: SubscriptionsService,
        private commonService: CommonService
    ) { }

  ngOnInit () {
    this.fetchData()
  }

  setProcessing () {
    this.processing = true
  }

  stopProcessing () {
    this.processing = false
  }

  fetchData () {
    this.service.getAllSubscriptions({ 'queryType': 'ALL' }).then((subscriptions) => {
      this.dataList = subscriptions
      this.stopProcessing()
    })
  }
}
