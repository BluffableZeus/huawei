import { Injectable } from '@angular/core'
import * as _ from 'underscore'
import * as moment from 'moment'
import { queryProduct } from 'ng-epg-sdk/vsp'
import { subscriptionAPI } from 'oper-center/api/subscription'
import { session } from 'src/app/shared/services/session'
import { Product } from 'ng-epg-sdk'

@Injectable()
export class SubscriptionsService {
  constructor (
        private subscriptionAPI: subscriptionAPI
  ) { }

  ocData () {
    return JSON.parse(localStorage.getItem('oper_center'))
  }

  getAllSubscriptions (req: any) {
    return queryProduct(req).then(resp => {
      if (resp.productList && resp.productList.length !== 0) {
        return this.getProductList(resp.productList)
      } else {
        return []
      }
    })
  }

  getPaymentMethods () {
    return this.subscriptionAPI.getPaymentMethods(this.ocData())
  }

  createPaymentMethod (product, paymentMethodId = undefined) {
    return this.subscriptionAPI.createPaymentMethod(product.productID, product.price, this.ocData(), paymentMethodId)
  }

  removeSubscription (product) {
    return this.subscriptionAPI.removeSubscription(product.productID, this.ocData())
  }

  renewSubscription (product) {
    return this.subscriptionAPI.renewSubscription(product.productID, this.ocData())
  }

  wrapSubscription (vspItem: Product) {
    return new Promise(resolve => {
      let result: any = {
        productID: vspItem['ID'] || vspItem['productID'],
        name: vspItem['name'] || vspItem['productName'],
        price: ' ' + (Number(vspItem['price']) / session.get('CURRENCY_RATE')).toFixed(2),
        productType: vspItem['productType'],
        subscribedVSP: vspItem['isSubscribed'],
        introduce: vspItem['introduce'] && vspItem['introduce'] || 'N.A.',
        endTime: vspItem['endTime'],
        ocError: true,
        expiresDate: undefined,
        paymentSystem: 'NONE',
        operationType: 'NONE',
        cancelPending: false,
        subscribed: vspItem['isSubscribed'] === '1',
        externalSubscription: false
      }

      if (vspItem.picture && vspItem.picture.posters && vspItem.picture.posters.length > 0) {
        result.poster = vspItem.picture.posters[0]
      } else {
        result.poster = '/assets/img/56/purchase2_56.png'
      }

      this.subscriptionAPI.getInfo(result.productID, this.ocData()).then(resp => {
        let ocData = resp || {},
          expiresDate = ocData['expiresDate'] ? moment(ocData['expiresDate']) : undefined,
          paymentSystem = ocData['paymentSystem'] || 'NONE',
          operationType = ocData['operationType'] || 'SUBSCRIPTION_INITIAL_BUY',
          cancelPending = operationType === 'SUBSCRIPTION_CANCEL',
          externalSubscription = paymentSystem !== 'NONE' && paymentSystem !== 'YANDEX'
        result = {
          ...result,
          ocError: false,
          expiresDate,
          paymentSystem,
          operationType,
          externalSubscription,
          cancelPending
        }
        resolve(result)
      }).catch(err => {
        console.log('[OC Subscription] Error: ', err)
        console.log(result)
        resolve(result)
      })
    })
  }

  getProductList (list: Product[]) {
    let products = _.map(list, (item) => this.wrapSubscription(item))
    return Promise.all(products)
  }
}
