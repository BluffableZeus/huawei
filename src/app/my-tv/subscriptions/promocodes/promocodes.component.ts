import { Component, OnInit, ViewContainerRef } from '@angular/core'
import { CommonService } from '../../../core/common.service'
import * as _ from 'underscore'
import { PromoAPI } from 'oper-center/api/promo'
import { session } from 'src/app/shared/services/session'
import { ErrorMessageService } from 'src/app/shared/services/error-message.service'
import { SubscriptionsService } from '../subscriptions.service'

@Component({
  selector: 'app-promocodes',
  templateUrl: './promocodes.component.html',
  styleUrls: ['./promocodes.component.scss', '../subscriptions.common.scss']
})
export class PromocodesComponent implements OnInit {
  public promo = ''
  public subscribedPackage = null

  constructor (private service: SubscriptionsService,
                private commonService: CommonService,
                private promoApi: PromoAPI,
                private viewContainerRef: ViewContainerRef,
                private errorMessageService: ErrorMessageService) {
  }

  ngOnInit () {
  }

  promoApply () {
    const userData = this.service.ocData()
    this.promoApi.promocodeRegister({
      login: session.get('SUBSCRIBER_ID'),
      userData: userData,
      promocode: this.promo,
      success: this.promoSucces.bind(this),
      error: this.promoError.bind(this)
    })
  }

  promoSucces (response) {
    this.service.getAllSubscriptions({ 'queryType': 'ALL' }).then((subscriptions) => {
      this.subscribedPackage = _.filter(subscriptions, subscription => {
        return subscription['productID'] === response.data.productId
      })
    }).then(() => {
      this.errorMessageService.openErrorDialog(this.viewContainerRef, this.dialogTextForming(response))
      this.clearPromocode()
    }
      )
  }

  promoError (response) {
    this.errorMessageService.openErrorDialog(this.viewContainerRef, this.dialogTextForming(response))
    this.clearPromocode()
  }

  clearPromocode () {
    this.promo = ''
  }

  dialogTextForming (data) {
    let message = ''
    if (data.data) {
      let date = new Date(data.data.expiresDate)
      if (+data.additionalCode === 2) {
        message = data.msg
      } else {
        message = `Пакет будет доступен до ${date.toLocaleDateString('ru')}`
      }
      return {
        m: `Активирована подписка на пакет \"${this.subscribedPackage[0].name}\"`,
        s: message,
        t: 'Подписка успешно оформлена'
      }
    } else {
      return {
        m: 'Произошла ошибка',
        s: data,
        errorMessageCode: 1,
        t: 'Произошла ошибка'
      }
    }
  }
}
