import { Component, OnInit, ViewContainerRef } from '@angular/core'
import { SubscriptionsService } from '../subscriptions.service'
import { CommonService } from '../../../core/common.service'
import * as _ from 'underscore'
import { PromoAPI } from 'oper-center/api/promo'
import { ErrorMessageService } from 'src/app/shared/services/error-message.service'

@Component({
  selector: 'app-my-subscriptions',
  templateUrl: './my_subscriptions.component.html',
  styleUrls: ['../subscriptions.common.scss']
})
export class MySubscriptionsComponent implements OnInit {
  public dataList = []
  public processing = false
  public noSubscriptions = false
  public promo = ''

  constructor (private service: SubscriptionsService,
                private commonService: CommonService,
                private promoapi: PromoAPI,
                private viewContainerRef: ViewContainerRef,
                private errorMessageService: ErrorMessageService) {
  }

  ngOnInit () {
    this.fetchData()
  }

  setProcessing () {
    this.processing = true
  }

  stopProcessing () {
    this.processing = false
  }

  fetchData () {
    this.service.getAllSubscriptions({ 'queryType': 'ALL' }).then((subscriptions) => {
      this.dataList = _.filter(subscriptions, subscription => subscription['subscribed'])
      if (this.dataList.length == 0) {
        this.noSubscriptions = true
      }
      this.stopProcessing()
    })
  }
}
