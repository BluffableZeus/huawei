import { Input, Component, Output, EventEmitter } from '@angular/core'
import { SubscriptionsService } from '../../subscriptions.service'
import { TranslateService } from '@ngx-translate/core'
import { WeakMessageDialogComponent } from '../../../../component/weakMessageDialog/weakMessageDialog.component'
import { CommonService } from '../../../../core/common.service'
import { PictureService } from 'ng-epg-ui/services'

@Component({
  selector: 'subscriptions-list',
  templateUrl: './subscriptions_list.component.html',
  styleUrls: ['../../subscriptions.common.scss']
})

export class SubscriptionsListComponent {
  @Input() public dataList = []
  @Input() public processing = false
  @Output() callFetch: EventEmitter<void> = new EventEmitter<void>()
  @Output() callStartProcessing: EventEmitter<void> = new EventEmitter<void>()
  @Output() callStopProcessing: EventEmitter<void> = new EventEmitter<void>()
  public currentProduct = null

  public showMethodDialog = false
  public paymentMethods = null

  constructor (
        private service: SubscriptionsService,
        private translate: TranslateService,
        private commonService: CommonService,
        protected picture: PictureService
    ) { }

  fetchData () {
    this.callFetch.emit()
  }

  setProcessing () {
    this.callStartProcessing.emit()
  }

  stopProcessing () {
    this.callStopProcessing.emit()
  }

  subscriptionStatus (product) {
    if (product.subscribed) {
      if (product.expiresDate) {
        return this.translate.instant('subscribed_until') + product.expiresDate.format('DD.MM.YYYY')
      } else {
        return this.translate.instant('subscribed')
      }
    } else {
      return this.translate.instant('not_subscribed')
    }
  }

  buttonTextCode (product) {
    if (product.subscribed) {
      if (product.cancelPending) {
        return 'renew_subscription'
      } else {
        return 'unsubscribe'
      }
    } else {
      return 'subscribe'
    }
  }

  selectPayment (product, payment) {
    this.service.createPaymentMethod(product, payment.paymentId).then(resp => {
      this.showMethodDialog = false
      this.fetchData()
    }).catch(err => {
      console.log(err)
      this.showOperCenterError()
    })
  }

  createPayment (product) {
    this.service.createPaymentMethod(product).then(resp => {
      if (resp['confirmation'] && resp['confirmation']['confirmation_url']) {
        window.location.href = resp['confirmation']['confirmation_url']
      } else {
        this.showOperCenterError()
      }
    }).catch(err => {
      console.log(err)
      this.showOperCenterError()
    })
  }

  closePopup () {
    this.stopProcessing()
    this.showMethodDialog = false
  }

  showOperCenterError () {
    this.showMethodDialog = false
    this.stopProcessing()
    this.commonService.openDialog(WeakMessageDialogComponent, null, this.translate.instant('oper_center_error'))
  }

  process (product) {
    this.setProcessing()
    this.currentProduct = product
    if (product.subscribed) {
      if (product.cancelPending) {
        this.service.renewSubscription(product).then(() => {
            this.fetchData()
          }).catch(err => {
            console.log(err)
            this.showOperCenterError()
          })
      } else {
        this.service.removeSubscription(product).then(() => {
            this.fetchData()
          }).catch(err => {
            console.log(err)
            this.showOperCenterError()
          })
      }
    } else {
      this.service.getPaymentMethods().then((paymentMethods) => {
        this.paymentMethods = paymentMethods
        this.showMethodDialog = true
      }).catch(err => {
          console.log(err)
          this.showOperCenterError()
        })
    }
  }

  prepareImg (src: string) {
    return this.picture.convertToSizeUrl(src, { minwidth: 110, minheight: 150, maxwidth: 110, maxheight: 150 })
  }
}
