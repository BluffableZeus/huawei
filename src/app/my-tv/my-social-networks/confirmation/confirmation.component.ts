import { Component, OnInit } from '@angular/core'
import { DialogRef, DialogParams } from 'ng-epg-ui/webtv-components/dialog'

@Component({
  templateUrl: './confirmation.component.html',
  styleUrls: ['./confirmation.component.scss']
})

export class ConfirmationComponent implements OnInit {
    /**
     * declare variables
     */
  public network: any = {}

    /**
     * name for modules that would be used in this module
     */
  constructor (
        private config: DialogParams,
        private dialog: DialogRef
    ) {
  }

    /**
     * initialization
     */
  ngOnInit () {
    this.network = this.config.data.network
  }

    /*
     *   method for close box
     */
  close () {
    if (this.dialog) {
      this.dialog.close()
    }
  }

    /*
     *   method for click the confirm buttom on box
     */
  confirm () {
    if (this.config.data.onAccept) {
      this.config.data.onAccept()
    }
    this.close()
  }
}
