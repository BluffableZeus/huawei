import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { socialAPI, VKData } from 'oper-center/api/social'
import { CommonService } from '../../core/common.service'
import { DialogConfig, DialogParams } from 'ng-epg-ui/webtv-components/dialog'
import { ConfirmationComponent } from './confirmation/confirmation.component'

export interface SocialNetwork {
  id: string,
  icon: string,
  availableIcon: string,
  name: string,
  status: boolean
}

@Component({
  templateUrl: './my-networks.component.html',
  styleUrls: ['./my-networks.component.scss']
})

export class MySocialNetworksComponent implements OnInit {
  private networkToEdit

  public networks: SocialNetwork[] = []

  constructor (
        protected router: Router,
        private api: socialAPI,
        private commonService: CommonService
    ) {
  }

  ngOnInit (): void {
      // Available networks
      // TODO: Fetch networks list from API
    this.networks = [{
      id: 'vk',
      icon: 'assets/img/24/vk.png',
      availableIcon: 'assets/img/20/vk-without-bg.png',
      name: 'vk',
      status: null
    }]

    this.api.testVKCallback('/profile/social', () => {
      this.networks[0].status = true
    })

    setTimeout(() => {
        // Added accounts
      this.networks.forEach(n => {
        this.api.test(n.id, this.ocData()).then(data => {
            n.status = !!data.data[n.id]
          })
      })
    }, 500)
  }

  ocData () {
    return JSON.parse(localStorage.getItem('oper_center'))
  }

  editAccount (network) {
    this.networkToEdit = network
    this.dlgShow()
  }

  dlgShow () {
    const data = {
      network: this.networkToEdit,
      onAccept: () => this.removeAccount(this.networkToEdit)
    }

    this.commonService.openDialog(ConfirmationComponent, null, { data } as DialogParams)
  }

  addAccount (network) {
    this.api.add(
        network.id,
        this.ocData(),
        window.location.origin + '/profile/social'
      ).then(url => {
        window.location.href = url
      })
  }

  removeAccount (network) {
    this.api.remove(
        network.id,
        this.ocData(),
        window.location.origin + '/profile/social'
      ).then(url => {
        window.location.href = url
      })
  }

  loading () {
    return this.networks.map(item => item.status).indexOf(null) > -1
  }
}
