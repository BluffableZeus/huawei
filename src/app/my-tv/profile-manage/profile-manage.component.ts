import * as _ from 'underscore'
import { Component, OnInit } from '@angular/core'
import { MyTvService } from '../my-tv.service'
import { ProfileManageService } from './profile-manage.service'
import { EventService } from 'ng-epg-sdk/services'
import { session } from 'src/app/shared/services/session'
import { TranslateService } from '@ngx-translate/core'
import { ProfileDeactivate } from './profile-deactivate.service'
import { Router } from '@angular/router'

@Component({
  selector: 'app-profile-manage',
  styleUrls: ['./profile-manage.component.scss'],
  templateUrl: './profile-manage.component.html'
})

export class ProfileManageComponent implements OnInit {
    // keys for showing prompt content on the Submit confirmation dialog.
  infoKey = 'Do_you_submit_setting'
    // to save information of current profile.
  public profileInfo: any = {}
    // to save list of avatars.
  public avatarList = []
    // to save page ammouts of avatar.
  public page: number
    // save the data for clicking.
  public count = 0
    // save the max value of offset of VOD posters.
  public maxWidth: number
    // save the value of offset of VOD posters.
  public offsetNumber = 0
    // save the initial avartar.
  public originalAvatar: string
    // save the initial profile name.
  public originalName: string
    // save the initial login ID.
  public originalLoginName: string
    // save the initial value of level ID.
  public originalLevelID: string
    // save the initial value of credit limit.
  public originalLimit: string
    // save the list of profile credit limit.
  public limitList: Array<any>
    // to judge whether show the avartar list.
  public isShowAvatarList = false
    // to judge whether show the drop list of credit limit.
  public isShowLimitList = false
    // to judge whether the profile information is changed.
  public isChanged = false
    // to judge show which part, basic information or modify password.
  public titleFlag = true
    // error message of profile name.
  public nameErrorMsg: string
    // to judge whether show error message of profile name.
  public isShowNameError = false
    // error message of login ID.
  public loginNameErrorMsg: string
    // to judge whether show error message of login ID.
  public isShowLoginNameError = false
    // to save the old password.
  public oldPwd: string
    // to save the new password.
  public newPwd: string
    // to save the confirmation password.
  public confirmPwd: string
    // to judge whether the new password is valid.
  public isNewPwdRight = false
    // to judge whether the confirmation password is valid.
  public isConfirmPwdRight = false
    // to judge whether show the delete icon of old password.
  public isShowOldDelete = false
    // to judge whether show the delete icon of new password.
  public isShowNewDelete = false
    // to judge whether show the delete icon of confirmation password.
  public isShowConfirmDelete = false
    // to judge whether show the error message of old password.
  public isShowOldVerify = false
    // to judge whether show the error message of new password.
  public isShowNewVerify = false
    // to judge whether show the error message of confirmation password.
  public isShowConfirmVerify = false
    // error message of old password.
  public oldPwdErrorMsg = ''
    // error message of new password.
  public newPwdErrorMsg = ''
    // error message of confirmation password.
  public confirmPwdErrorMsg = ''
    // to judge whether show the tips message of new password.
  public showPwdTip = false
    // save the rules of password input.
  public pwdConfig: any
    // to judge whether the password information is changed.
  public isPwdChanged = false
    // save the width of one page of avatar.
  public panel
    // to judge whether show the delete icon of profile name.
  public isShowNameDelete = false
    // to judge whether show the delete icon of login ID.
  public isShowLoginNameDelete = false
    // to judge whether show the Submit confirmation dialog.
  public showSubmitDialog = false
    // save the avatar before edit operation.
  public AvatarBeforeEdit
    // to judge whether the avatar is changed.
  public avatarHasChanged = false
    // to judge whether show the wrong message of profile name.
  public wrongMessage = false
    // save the content of error message of profile name.
  public wrongMessageTip
    // save the content of error message of login ID.
  public wrongLogIdMessageTip
    // to judge whether error type of profile name is exceeding maximum length.
  public wrongMessageMaxNumber = true
    // to judge whether show the wrong message of login id.
  public wrongLoginIdMessage = false
    // to judge whether error type of login id is exceeding maximum length.
  public wrongLogIdMessageMaxNumber = true
    // to save the black list of inputing.
  public blackListCache = ''
    // to save the maximum length of inputing.
  public maxLengthCache = 128
    // to save the input tips for password.
  public passwordInputTips = ''
    // to judge whether show the left icon of avatar list.
  public vodLeftVisible = 'hidden'
    // to judge whether show the right icon of avatar list.
  public vodRightVisible = 'visible'

    // private constructor
  constructor (
        private myTvService: MyTvService,
        private profileManageService: ProfileManageService,
        private translate: TranslateService,
        private profileDeactivate: ProfileDeactivate,
        private router: Router
    ) {
    this.getProfileRule()
  }

    /**
     * init function.
     * get profile information and register event listener.
     */
  ngOnInit () {
    if (!Cookies.getJSON('IS_PROFILE_LOGIN')) {
      return
    }
    this.getProfileInfo()
    this.getLimitList()
    this.profileManageService.getAllProfile()
    _.delay(() => {
      this.getPasswordTips()
    }, 500)
      // listening to the change of screen size.
    EventService.on('ScreenSize', () => {
      if (this.offsetNumber === -810) {
        this.offsetNumber = -1160
      } else if (this.offsetNumber === -1620) {
          this.offsetNumber = -1740
        } else if (this.offsetNumber === -1740) {
          this.offsetNumber = -1620
        } else if (this.offsetNumber === 0) {
          this.offsetNumber = 0
        } else if (this.offsetNumber === -580 || -1160) {
          this.offsetNumber = -810
        }
      if (!_.isUndefined(document.getElementById('avatarList'))) {
        if (document.getElementById('avatarList')) {
            document.getElementById('avatarList').style.transform = 'translateX(' + this.offsetNumber + 'px)'
            document.getElementById('avatarList').style.transition = 'all 0s linear'
          }
      }
    })
    EventService.removeAllListeners(['POP_UP_SUBMIT'])
    EventService.on('POP_UP_SUBMIT', () => {
      this.showSubmitDialog = true
    })
  }

    /**
     * get the rules of profile name,user id and user password.
     */
  getProfileRule () {
    this.pwdConfig = session.get('USER_PWD_RULER_CACHE')
    if (_.isUndefined(this.pwdConfig['userPwdMinLength']) || this.pwdConfig['userPwdMinLength'] === null) {
      this.pwdConfig['UserPwdMinLength'] = 1
    }
    this.blackListCache = session.get('COMMON_INPUT_BLACKLIST_CACHE')
    this.maxLengthCache = session.get('COMMON_INPUT_MAXLENGTH_CACHE')
  }

    /**
     * switch the title
     * @param {Object} data [click event]
     */
  changeTitle (data) {
    if (data.currentTarget.className === 'unSelected1' || data.currentTarget.className === 'selected1') {
      this.titleFlag = false
      this.isShowAvatarList = false
      this.checkAvatarChange()
    } else {
      this.titleFlag = true
    }
  }

    /**
     * cancel the setting after changing the profile.
     */
  undo () {
    if (this.isChanged) {
      this.isChanged = false
      this.profileDeactivate.isBasicInfoChanged = false
      this.getProfileInfo()
    }
    if (this.isShowAvatarList) {
      this.isShowAvatarList = false
      this.checkAvatarChange()
    }
    this.isShowNameError = false
    this.isShowLoginNameError = false
  }

    /**
     * save the setting after changing the profile.
     */
  save () {
    _.delay(() => {
      if (!this.isChanged) {
        return
      }
      if (this.isShowAvatarList) {
        this.isShowAvatarList = false
        this.checkAvatarChange()
      }
        // the profile name should not be empty.
      if (this.profileManageService.trim(this.profileInfo['name']) === '') {
        this.profileInfo['name'] = ''
        this.wrongMessage = false
        this.nameErrorMsg = 'profile_name_empty'
        this.isShowNameError = true
        return
      }
        // the login id should not be empty.
      if (this.profileManageService.trim(this.profileInfo['loginName']) === '') {
        this.profileInfo['loginName'] = ''
        this.wrongLoginIdMessage = false
        this.loginNameErrorMsg = 'login_id_empty'
        this.isShowLoginNameError = true
      }
      if (this.isShowNameError || this.isShowLoginNameError) {
        return
      }
        // package the request.
      let req = {
        ID: this.profileInfo['profileId'],
        name: this.profileManageService.toOneSpace(this.profileInfo['name']),
        loginName: this.profileManageService.toOneSpace(this.profileInfo['loginName']),
        logoURL: this.profileInfo['avatar'].slice((this.profileInfo['avatar'].lastIndexOf('/') + 1),
            this.profileInfo['avatar'].lastIndexOf('.')),
        ratingName: this.profileInfo['level'],
        ratingID: this.profileInfo['levelId'],
        profileType: Cookies.get('PROFILE_TYPE')
      }
      this.profileManageService.modifyProfile({ profile: req }).then(resp => {
        if (resp.result.retCode === '0' || resp.result.retCode === '000000000') {
            this.isChanged = false
            if (this.dom('info-button')) {
              this.dom('info-button')['disabled'] = true
            }
            this.profileDeactivate.isBasicInfoChanged = false
            session.put('PROFILERATING_ID', this.profileInfo['levelId'])
            let profile = JSON.parse(localStorage.getItem('curProfile'))
            profile['name'] = req.name
            profile['avatar'] = this.profileInfo['avatar']
            profile['quota'] = this.profileInfo['quota']
            profile['loginName'] = req.loginName
            profile['level'] = this.profileInfo['level']
            profile['levelId'] = this.profileInfo['levelId']
            localStorage.setItem('curProfile', JSON.stringify(profile))
            session.put('LOGIN_NAME', profile['loginName'], true)
            EventService.emit('MODIFY_PROFILE')
            if (profile['name'] !== this.originalName) {
              Cookies.set('PROFILE_NAME', profile['name'])
              EventService.emit('CHANGE_PROFILE_NAME')
            }
            if (profile['avatar'] !== this.originalAvatar) {
              Cookies.set('LOGIN_PROFILE_LOGOURL', req.logoURL)
              EventService.emit('CHANGE_PROFILE_AVATAR')
            }
            this.getProfileInfo()
            EventService.emit('PROFILE_INFO_SAVE_SUCCESSFUL')
          }
      })
    }, 150)
  }

    /**
     * get the list of expense limit.
     */
  getLimitList () {
    if (!this.limitList) {
      this.profileManageService.getLimitList().then(resp => {
        this.limitList = resp
        if (this.limitList.length === 0) {
            if (this.dom('credit-limit')) {
              this.dom('credit-limit')['style']['backgroundImage'] = 'url(assets/img/20/angledown2_20.png)'
              this.dom('credit-limit')['style']['cursor'] = 'default'
            }
          }
      })
    }
  }

    /**
     * get current profile information.
     */
  getProfileInfo () {
    let profile = JSON.parse(localStorage.getItem('curProfile'))
    this.profileInfo = _.extend({}, profile)
    this.originalAvatar = profile['avatar']
    this.AvatarBeforeEdit = profile['avatar']
    this.originalName = profile['name']
    this.originalLoginName = profile['loginName']
    this.originalLevelID = profile['levelId']
    this.originalLimit = profile['quota']
  }

    /**
     * edit avatar.
     */
  editAvatar () {
    this.avatarHasChanged = false
    this.getAvatarList()
    this.offsetNumber = 0
    this.isShowIcon(this.offsetNumber)
    document.getElementById('avatarList').style.transform = 'translateX(' + 0 + 'px)'
    this.isShowAvatarList = true
    this.AvatarBeforeEdit = this.profileInfo['avatar']
  }

    /**
     * get the avatar's list.
     */
  getAvatarList () {
    this.avatarList = this.profileManageService.getAvatarList()
  }

    /**
     * choose one avatar
     * @param {string} avatar [url of avartar selected]
     */
  selectAvatar (avatar) {
    this.profileInfo['avatar'] = avatar
    this.avatarHasChanged = true
    this.isShowAvatarList = false
    this.isInfoChanged()
  }

    /**
     * preview the avatars.
     * @param {string} avatar [url of avartar selected]
     */
  mouseOn (avatar) {
    this.profileInfo['avatar'] = avatar
  }

    /**
     * check whether the avatar has been changed.
     */
  checkAvatarChange () {
    if (!this.avatarHasChanged) {
      this.profileInfo['avatar'] = this.AvatarBeforeEdit
    }
  }

    /**
     * switch the avatars towards the left.
     */
  leftArrow () {
    let offsetWidth: string
    let clientW: number = document.body.clientWidth
    if (clientW > 1440) {
      this.panel = 810
    } else {
      this.panel = 580
    }
    const MINSFFSETNUMBER = 0
    if (this.offsetNumber < MINSFFSETNUMBER) {
      this.offsetNumber += this.panel
      offsetWidth = this.offsetNumber + 'px'
      document.getElementById('avatarList').style.transform = 'translateX(' + offsetWidth + ')'
      document.getElementById('avatarList').style.transition = 'all 1s linear'
      this.count--
      this.isShowIcon(this.offsetNumber)
    }
  }

    /**
     * switch the avatars towards the right.
     */
  rightArrow () {
    let offsetWidth: string
    let clientW: number = document.body.clientWidth
    if (clientW > 1440) {
      this.panel = 810
      this.page = Math.ceil(this.avatarList.length / 7)
    } else {
      this.panel = 580
      this.page = Math.ceil(this.avatarList.length / 5)
    }
    this.maxWidth = -(this.panel * (this.page - 1))
    if (this.offsetNumber > this.maxWidth) {
      this.offsetNumber -= this.panel
      offsetWidth = this.offsetNumber + 'px'
      document.getElementById('avatarList').style.transform = 'translateX(' + offsetWidth + ')'
      document.getElementById('avatarList').style.transition = 'all 1s linear'
      this.count++
      this.isShowIcon(this.offsetNumber)
    }
  }

    /**
     * judge the show state of arrowhead.
     * @param {number} offsetNumber [offset of avatar list]
     */
  isShowIcon (offsetNumber: number) {
    if (offsetNumber === 0) {
      this.vodLeftVisible = 'hidden'
      this.vodRightVisible = 'visible'
    } else if (offsetNumber === -1620) {
      this.vodLeftVisible = 'visible'
      this.vodRightVisible = 'hidden'
    } else if (offsetNumber === -1740) {
        this.vodLeftVisible = 'visible'
        this.vodRightVisible = 'hidden'
      } else {
        this.vodLeftVisible = 'visible'
        this.vodRightVisible = 'visible'
      }
  }

    /**
     * key press event handler.
     * @param {Object} event [keypress event]
     */
  onPressChange (event) {
    let caretPos = 0
    let ctrl = event.currentTarget
    if (document['selection']) {
      ctrl.focus()
      let sel = document['selection'].createRange()
      sel.moveStart('character', -ctrl['value'].length)
      caretPos = sel.text.length
    } else if (ctrl['selectionStart'] || ctrl['selectionStart'] === '0') {
      caretPos = ctrl['selectionStart']
    }
  }

    /**
     * profile name get focus.
     */
  nameFocus () {
    if (this.profileInfo['name'] === '' || this.profileInfo['name'] === undefined) {
      this.isShowNameDelete = false
      this.isShowNameError = false
    } else {
      this.isShowNameDelete = true
    }
    this.isShowAvatarList = false
    this.checkAvatarChange()
  }

    /**
     * profile name lose focus.
     */
  nameBlur () {
    _.delay(() => {
      this.checkNameRules()
    }, 100)
  }

    /**
     * click delete icon of profile name.
     */
  nameDelete () {
    this.profileInfo['name'] = ''
    this.wrongMessage = false
    this.nameErrorMsg = ''
    this.isShowNameError = false
    this.isShowNameDelete = false
    this.isInfoChanged()
    document.getElementById('profileName-input').focus()
  }

    /**
     * click delete icon of login ID.
     */
  loginNameDelete () {
    this.profileInfo['loginName'] = ''
    this.wrongLoginIdMessage = false
    this.loginNameErrorMsg = ''
    this.isShowLoginNameError = false
    this.isShowLoginNameDelete = false
    this.isInfoChanged()
    document.getElementById('loginID-input').focus()
  }

    /**
     * check whether the profile name comply with the rules.
     */
  checkNameRules () {
    if (this.profileInfo['name'] === '') {
      return
    }
    _.delay(() => {
      this.isShowNameDelete = false
      if (this.profileInfo['name'].length > this.maxLengthCache) {
        this.wrongMessageMaxNumber = true
        this.wrongMessage = true
        this.wrongMessageTip = { 'common': this.maxLengthCache }
        this.isShowNameError = true
        return
      }
      if (this.blackListCache !== '') {
        this.wrongMessageMaxNumber = false
        this.wrongMessage = true
        if (!(/^[A-Z|a-z|0-9]+$/.test(this.profileInfo['name']))) {
            let numLetters = this.profileInfo['name'].replace(/[A-Z|a-z|0-9]/ig, '')
            let numLettersArr = numLetters.split('')
            if (numLettersArr.length !== 0) {
              let hasLettersOfBlackList = false
              nameOuter: for (let i = 0; i < numLettersArr.length; i++) {
                for (let j = 0; j < this.blackListCache.length; j++) {
                  if (numLettersArr[i] === this.blackListCache.charAt(j)) {
                    hasLettersOfBlackList = true
                    break nameOuter
                  }
                }
              }
              if (hasLettersOfBlackList) {
                this.wrongMessageTip = { 'common': this.blackListCache }
                this.isShowNameError = true
                return
              }
            }
          }
      }
      let name = _.find(this.profileManageService.names, item => {
        return item === this.profileInfo['name']
      })
      if (name) {
        this.wrongMessage = false
        this.nameErrorMsg = 'profile_name_repeat'
        this.isShowNameError = true
        return
      }
      this.isShowNameError = false
    }, 100)
  }

    /**
     * judge whether the profile information is changed.
     */
  isInfoChanged () {
    if (this.profileInfo['name'] === this.originalName &&
            this.profileInfo['loginName'] === this.originalLoginName &&
            this.profileInfo['levelId'] === this.originalLevelID &&
            this.profileInfo['avatar'] === this.originalAvatar &&
            this.profileInfo['quota'] === this.originalLimit) {
      this.isChanged = false
      if (this.dom('info-button')) {
        this.dom('info-button')['disabled'] = true
      }
      this.profileDeactivate.isBasicInfoChanged = false
    } else {
      this.isChanged = true
      if (this.dom('info-button')) {
        this.dom('info-button')['disabled'] = false
      }
      this.profileDeactivate.isBasicInfoChanged = true
    }
  }

    /**
     * judge whether the profile name is changed.
     */
  isNameChanged (event) {
    if (this.profileInfo['name'] === '' || this.profileInfo['name'] === undefined) {
      this.isShowNameDelete = false
    } else {
      this.isShowNameError = false
      this.wrongMessage = false
      this.nameErrorMsg = ''
      this.isShowNameDelete = true
    }
    this.profileInfo['name'] = event.target.value
    this.isInfoChanged()
  }

    /**
     * modify the password
     */
  modifyPwd () {
    if (!this.isPwdChanged) {
      return
    }
    if (this.oldPwd === '' || this.oldPwd === undefined) {
      this.oldPwdErrorMsg = 'old_pwd_empty'
      this.isShowOldVerify = true
      return
    }
    if (this.newPwd === '' || this.newPwd === undefined) {
      this.newPwdErrorMsg = 'new_pwd_empty'
      this.isNewPwdRight = false
      this.isShowNewVerify = true
      return
    }
    if (!this.isNewPwdRight) {
      return
    }
    if (this.isNewPwdRight && (this.confirmPwd === '' || this.confirmPwd === undefined)) {
      this.confirmPwdErrorMsg = 'confirm_pwd_empty'
      this.isConfirmPwdRight = false
      this.isShowConfirmVerify = true
      return
    }
    this.modifyPwdSuccess()
  }

    /**
     * call interface to modify the password.
     */
  modifyPwdSuccess () {
    if (this.isNewPwdRight && this.isConfirmPwdRight) {
      this.profileManageService.modifyPassword({
        oldPwd: this.oldPwd,
        newPwd: this.newPwd,
        confirmPwd: this.confirmPwd
      }).then(resp => {
          if (resp['result'].retCode === '000000000') {
            this.undoPwd()
            EventService.emit('PROFILE_INFO_SAVE_SUCCESSFUL')
          }
        })
    }
  }

    /**
     * cancel changing password.
     */
  undoPwd () {
    if (this.isPwdChanged) {
      this.isShowOldVerify = false
      this.isShowNewVerify = false
      this.isShowConfirmVerify = false
      this.isPwdChanged = false
      if (this.dom('password-button')) {
        this.dom('password-button')['disabled'] = true
      }
      this.profileDeactivate.isPwdInfoChanged = false
      this.oldPwd = ''
      this.newPwd = ''
      this.confirmPwd = ''
    }
  }

    /**
     * old password get focus.
     */
  oldPwdFocus () {
    if (this.oldPwd === '' || this.oldPwd === undefined) {
      this.isShowOldDelete = false
      this.isShowOldVerify = false
    } else {
      this.isShowOldDelete = true
    }
  }

    /**
     * new password get focus.
     */
  newPwdFocus () {
    this.showPwdTip = !this.isShowNewVerify
    if (this.newPwd === '' || this.newPwd === undefined) {
      this.isShowNewVerify = false
      this.isShowNewDelete = false
      this.showPwdTip = true
    } else {
      this.isShowNewDelete = true
    }
  }

    /**
     * confirmation password get focus.
     */
  confirmPwdFocus () {
    if (this.confirmPwd === '' || this.confirmPwd === undefined) {
      this.isShowConfirmDelete = false
      this.isShowConfirmVerify = false
    } else {
      this.isShowConfirmDelete = true
    }
  }

    /**
     * old password lose focus.
     */
  oldPwdBlur () {
    _.delay(() => {
      this.isShowOldDelete = false
    }, 300)
    if (this.oldPwd === '' || this.oldPwd === undefined) {
      if ((this.newPwd === undefined || this.newPwd === '') &&
                (this.confirmPwd === undefined || this.confirmPwd === '')) {
        this.isShowOldVerify = false
      } else {
        this.isShowOldVerify = true
        this.oldPwdErrorMsg = 'old_pwd_empty'
      }
    } else {
      this.isShowOldVerify = false
    }
  }

    /**
     * new password lose focus handler.
     */
  newPwdBlurTwice () {
      //  the limit of password length (if has)
    if (this.pwdConfig['userPwdMinLength'] && this.newPwd.length < Number(this.pwdConfig['userPwdMinLength'])) {
      this.newPwdErrorMsg = 'password_does_not_conform_rules'
      return
    }
      // including the capital letters (if has)
    if (this.pwdConfig['userPwdUpperCaseLetters']) {
      if (!/[A-Z]+/.test(this.newPwd)) {
        this.newPwdErrorMsg = 'password_does_not_conform_rules'
        return
      }
    }
      // including the small letters (if has)
    if (this.pwdConfig['userPwdLowerCaseLetters']) {
      if (!/[a-z]+/.test(this.newPwd)) {
        this.newPwdErrorMsg = 'password_does_not_conform_rules'
        return
      }
    }
      //  including digit (if has)
    if (this.pwdConfig['userPwdNumbers']) {
      if (!/[0-9]+/.test(this.newPwd)) {
        this.newPwdErrorMsg = 'password_does_not_conform_rules'
        return
      }
    }
      // including special character (if has)
    if (this.pwdConfig['userPwdOthersLetters'] !== '' && this.pwdConfig['userPwdOthersLetters'] !== undefined) {
      let exp = new RegExp('[' + this.pwdConfig['userPwdOthersLetters'] + ']', 'g')
      if (!exp.test(this.newPwd)) {
        this.newPwdErrorMsg = 'password_does_not_conform_rules'
        return
      }
    }
    return true
  }

    /**
     * new password lose focus.
     */
  newPwdBlur () {
    this.showPwdTip = false
    this.isNewPwdRight = false
    _.delay(() => {
      this.isShowNewDelete = false
    }, 300)
    this.isShowNewVerify = true
    this.newPwdErrorMsg = ''
    if (this.newPwd === undefined || this.newPwd === '') {
      this.isShowNewVerify = false
      if (this.confirmPwd !== '' && this.confirmPwd !== undefined) {
        this.isConfirmPwdRight = false
        this.confirmPwdErrorMsg = 'password_not_same'
      }
      return
    }
      // the new password should not be same as login ID.
    if (this.newPwd === this.profileInfo['loginName']) {
      this.newPwdErrorMsg = 'password_does_not_conform_rules'
      return
    }
      // the new password should not be same as login ID by reverse.
    let reverseID = this.profileInfo['loginName'].split('').reverse().join('')
    if (this.newPwd === reverseID) {
      this.newPwdErrorMsg = 'password_does_not_conform_rules'
      return
    }
      // the new password should not be same as the old password.
    if (this.newPwd === this.oldPwd) {
      this.isConfirmPwdRight = false
      this.confirmPwdErrorMsg = ''
      this.newPwdErrorMsg = 'password_does_not_conform_rules'
      return
    } else {
      if (this.newPwd === this.confirmPwd) {
        this.isConfirmPwdRight = true
      } else {
        this.isConfirmPwdRight = false
        this.confirmPwdErrorMsg = 'password_not_same'
      }
    }
    if (!this.newPwdBlurTwice()) {
      return
    }
      // can not have space (if has)
    if (this.pwdConfig['userPwdSupportSpace']) {
      if (this.newPwd.indexOf(' ') === -1) {
        this.newPwdErrorMsg = 'password_does_not_conform_rules'
        return
      }
    } else {
      if (this.newPwd.indexOf(' ') !== -1) {
        this.newPwdErrorMsg = 'password_does_not_conform_rules'
        return
      }
    }
    this.isNewPwdRight = true
  }

    /**
     * confirmation password lose focus.
     */
  confirmPwdBlur () {
    this.isShowConfirmVerify = !(this.confirmPwd === undefined || this.confirmPwd === '')
    _.delay(() => {
      this.isShowConfirmDelete = false
    }, 300)
    if (this.newPwd === this.oldPwd) {
      this.isConfirmPwdRight = false
      this.confirmPwdErrorMsg = ''
    } else {
      if (this.newPwd === this.confirmPwd && this.newPwd !== '' && this.newPwd !== undefined) {
        this.isConfirmPwdRight = true
      } else {
        this.isConfirmPwdRight = false
        this.confirmPwdErrorMsg = 'password_not_same'
      }
    }
  }

    /**
     * click the delete icon of old password.
     */
  oldPwdDelete () {
    this.oldPwd = ''
    this.isShowOldDelete = false
    this.isShowOldVerify = false
    this.checkPwdChanged()
    document.getElementById('oldPassword-input').focus()
  }

    /**
     * click the delete icon of new password.
     */
  newPwdDelete () {
    this.newPwd = ''
    this.isShowNewDelete = false
    this.isShowNewVerify = false
    if (this.newPwd === this.confirmPwd) {
      this.isConfirmPwdRight = true
    } else {
      this.isConfirmPwdRight = false
      this.confirmPwdErrorMsg = 'password_not_same'
    }
    this.checkPwdChanged()
    document.getElementById('newPassword-input').focus()
  }

    /**
     * click the delete icon of confirmation password.
     */
  confirmPwdDelete () {
    this.confirmPwd = ''
    this.isShowConfirmDelete = false
    this.isShowConfirmVerify = false
    this.checkPwdChanged()
    document.getElementById('confirmPassword-input').focus()
  }

    /**
     * check whether the old password is changed.
     */
  isOldPwdChanged () {
    if (this.oldPwd === undefined || this.oldPwd === '') {
      this.isShowOldDelete = false
      if ((this.newPwd === undefined || this.newPwd === '') && (this.confirmPwd === undefined || this.confirmPwd === '')) {
        this.isPwdChanged = false
        if (this.dom('password-button')) {
            this.dom('password-button')['disabled'] = true
          }
        this.profileDeactivate.isPwdInfoChanged = false
      }
    } else {
      this.isPwdChanged = true
      if (this.dom('password-button')) {
        this.dom('password-button')['disabled'] = false
      }
      this.profileDeactivate.isPwdInfoChanged = true
      this.isShowOldDelete = true
    }
  }

    /**
     * check whether the new password is changed.
     */
  isNewPwdChanged () {
    if (!this.newPwd) {
      this.isShowNewDelete = false
      if ((this.oldPwd === undefined || this.oldPwd === '') && (this.confirmPwd === undefined || this.confirmPwd === '')) {
        this.isPwdChanged = false
        if (this.dom('password-button')) {
            this.dom('password-button')['disabled'] = true
          }
        this.profileDeactivate.isPwdInfoChanged = false
      }
    } else {
      this.isPwdChanged = true
      if (this.dom('password-button')) {
        this.dom('password-button')['disabled'] = false
      }
      this.profileDeactivate.isPwdInfoChanged = true
      this.isShowNewDelete = true
    }
  }

    /**
     * check whether the confirmation password is changed.
     */
  isConfirmPwdChanged () {
    if (this.confirmPwd === undefined || this.confirmPwd === '') {
      this.isShowConfirmDelete = false
      if ((this.newPwd === undefined || this.newPwd === '') && (this.oldPwd === undefined || this.oldPwd === '')) {
        this.isPwdChanged = false
        if (this.dom('password-button')) {
            this.dom('password-button')['disabled'] = true
          }
        this.profileDeactivate.isPwdInfoChanged = false
      }
    } else {
      this.isPwdChanged = true
      if (this.dom('password-button')) {
        this.dom('password-button')['disabled'] = false
      }
      this.profileDeactivate.isPwdInfoChanged = true
      this.isShowConfirmDelete = true
    }
  }

    /**
     * check whether password is changed.
     */
  checkPwdChanged () {
    if ((this.oldPwd === undefined || this.oldPwd === '') && (this.newPwd === undefined || this.newPwd === '') &&
            (this.confirmPwd === undefined || this.confirmPwd === '')
      ) {
      this.isPwdChanged = false
      if (this.dom('password-button')) {
        this.dom('password-button')['disabled'] = true
      }
      this.profileDeactivate.isPwdInfoChanged = false
    } else {
      this.isPwdChanged = true
      if (this.dom('password-button')) {
        this.dom('password-button')['disabled'] = false
      }
      this.profileDeactivate.isPwdInfoChanged = true
    }
  }

    /**
     * close Submit confirmation dialog.
     */
  closeSubDialog () {
    this.showSubmitDialog = false
    EventService.emit('PROFILE_FOCUS')
  }

    /**
     * cancel the Submit operation.
     */
  cancelOperation () {
    this.showSubmitDialog = false
    this.profileDeactivate.isBasicInfoChanged = false
    this.profileDeactivate.isPwdInfoChanged = false
    if (session.get('PROFILE_NOTSAVE_LOGOUT_TYPE')) {
      _.delay(() => {
        EventService.emit('PROFILE_NOTSAVE_LOGOUT')
        this.router.navigate(['' + session.get('RouteMessage')])
      }, 1000)
    } else {
      this.router.navigate(['' + session.get('RouteMessage')])
    }
  }

    /**
     * do the Submit operation.
     */
  submitData () {
    this.showSubmitDialog = false
    this.save()
    this.modifyPwd()
    this.profileDeactivate.isBasicInfoChanged = false
    this.profileDeactivate.isPwdInfoChanged = false
    if (session.get('PROFILE_NOTSAVE_LOGOUT_TYPE')) {
      _.delay(() => {
        EventService.emit('PROFILE_NOTSAVE_LOGOUT')
        this.router.navigate(['' + session.get('RouteMessage')])
      }, 1000)
    } else {
      this.router.navigate(['' + session.get('RouteMessage')])
    }
  }

    /**
     * get dom element by element id.
     */
  dom (divName: string) {
    return document.getElementById(divName)
  }

    /**
     * get content of tips of password.
     */
  getPasswordTips () {
    let tips = this.translate.instant('password_require') + this.translate.instant('new_password_simple')
    tips += ' ' + this.translate.instant('new_pasaword_tips')
    if (this.pwdConfig['userPwdMinLength'] >= 1) {
      tips += ' ' + this.translate.instant('password_min_length', { minLength: this.pwdConfig['userPwdMinLength'] })
    }
    if (this.pwdConfig['userPwdUpperCaseLetters']) {
      tips += ' ' + this.translate.instant('up_letter')
    }
    if (this.pwdConfig['userPwdLowerCaseLetters']) {
      tips += ' ' + this.translate.instant('low_letter')
    }
    if (this.pwdConfig['userPwdNumbers']) {
      tips += ' ' + this.translate.instant('digit')
    }
    if (this.pwdConfig['userPwdOthersLetters']) {
      tips += ' ' + this.translate.instant('special_char', { special_char: this.pwdConfig['userPwdOthersLetters'] })
    }
    if (!this.pwdConfig['userPwdSupportSpace']) {
      tips += ' ' + this.translate.instant('Space_not_allowed')
    }
    this.passwordInputTips = tips
  }
}
