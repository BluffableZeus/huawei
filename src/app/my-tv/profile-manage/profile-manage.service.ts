import * as _ from 'underscore'
import { Injectable } from '@angular/core'
import { modifyPassword, modifyProfile } from 'ng-epg-sdk/vsp'
import { TranslateService } from '@ngx-translate/core'
import { session } from 'src/app/shared/services/session'
import { MyTvService } from '../my-tv.service'
import { TimerService } from '../../shared/services/timer.service'
import { CommonService } from '../../core/common.service'
import { config } from '../../shared/services/config'

@Injectable()
export class ProfileManageService {
    // to save list of profile names.
  public names
    // to save list of profile login ID.
  public loginNames
    // to save value of currenrySymbol.
  private currenrySymbol

    // constructor of class ProfileManageService.
  constructor (
        private translate: TranslateService,
        private myTvService: MyTvService,
        private timerService: TimerService,
        private commonService: CommonService
    ) {
  }

    /**
     * get the list of limit.
     */
  getLimitList () {
    return this.timerService.getCaheCustomConfig().then(resp => {
      this.currenrySymbol = session.get('CURRENCY_SYMBOL')
      let creditLimitData
      let configuration = _.find(resp['configurations'], (data) => {
        return data['cfgType'] === '0'
      })
      if (configuration && configuration['values']) {
        creditLimitData = _.filter(configuration['values'], (limitData) => {
            return limitData['key'] === 'credit_limit_list'
          })
      }
      let list = creditLimitData && creditLimitData[0] && creditLimitData[0].values && creditLimitData[0].values[0].split(',')
      let key = creditLimitData && creditLimitData[0] && creditLimitData[0].key
      if (list && key === 'credit_limit_list') {
        list = this.getCreditLimitList(list, resp)
      } else {
        list = []
      }
      return list
    })
  }

    /**
     * get the list of credit limit.
     * @param {Array} list [credit_limit_list].
     * @param {Object} resp [response of QueryCustomizeConfig].
     */
  getCreditLimitList (list, resp) {
    if (list.length === 1 && (list[0] === '' || list[0] === null)) {
      this.commonService.MSAErrorLog(38068, 'QueryCustomizeConfig')
      return
    }
    for (let i = 0; i < list.length; i++) {
      if (Number(list[i]) < 0 && Number(list[i]) !== -1) {
        this.commonService.MSAErrorLog(38068, 'QueryCustomizeConfig')
      }
      list[i] = list[i] && list[i] === '-1' ? 'unlimited'
          : session.get('CURRENCY_SYMBOL') + String(Number(list[i]) / Number(resp['currencyRate']))
    }

    return list
  }

    /**
     * get the content's level
     */
  getLevelList () {
    return session.get('TERMINAL_RATING_LIST')
  }

    /**
     * get the avatar list.
     */
  getAvatarList () {
    let list = []
    let avatarLength = config['avatarLength']
    for (let i = 0; i < avatarLength; i++) {
      list[i] = 'assets/img/avatars/avatar' + (i + 1) + '.png'
    }
    return list
  }

    /**
     * modify the password.
     * @param {any} req [request parameters of interface ModifyPassword].
     */
  modifyPassword (req: any) {
    let modifyPasswordReq = {
      oldPwd: req.oldPwd,
      newPwd: req.newPwd,
      confirmPwd: req.confirmPwd
    }
    return modifyPassword(modifyPasswordReq).then(resp => {
      return resp
    })
  }

    /**
     * modify the profile information.
     * @param {any} req [request parameters of interface ModifyProfile].
     */
  modifyProfile (req: any) {
    return modifyProfile(req).then(resp => {
      return resp
    })
  }

    /**
     * remove the blank space of the head and the tail.
     * @param {any} str [string to be processed].
     */
  trim (str) {
    if (typeof str === 'string') {
      str = str.replace(/(^\s)|(\s*$)/g, '').replace(/(^\s*)/g, '').replace(/(\s*$)/g, '')
      return str
    }
  }

    /**
     * function used to make there is only one space between two words.
     * @param {any} str [string to be processed].
     */
  toOneSpace (str) {
    return this.trim(str).replace(/\s+/g, ' ')
  }

    /**
     * get all profiles of subscriber.
     */
  getAllProfile () {
    this.myTvService.queryAllProfile({ type: '1', count: '50', offset: '0' }).then(resp => {
      for (let i = 0; i < resp['profiles'].length; i++) {
        if (resp['profiles'][i]['loginName'] === JSON.parse(localStorage.getItem('curProfile'))['loginName']) {
            resp['profiles'].splice(i, 1)
            break
          }
      }
      this.names = _.pluck(resp['profiles'], 'name')
      this.loginNames = _.pluck(resp['profiles'], 'loginName')
    })
  }
}
