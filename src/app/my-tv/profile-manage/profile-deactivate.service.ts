import { Injectable } from '@angular/core'
import { CanDeactivate } from '@angular/router'
import { Observable } from 'rxjs'
import { EventService } from 'ng-epg-sdk/services'
import { session } from 'src/app/shared/services/session'

export interface CanComponentDeactivate {
  canDeactivate: () => Observable<boolean> | Promise<boolean> | boolean
}

@Injectable()
export class ProfileDeactivate implements CanDeactivate<CanComponentDeactivate> {
    // to judge whether the basic information is changed.
  public isBasicInfoChanged = false
    // to judge whether the password information is changed.
  public isPwdInfoChanged = false
    // to save route url message.
  public currentUrl = ''

    /**
     * function used to get route information.
     * @param {Object} e [window event(type: click)]
     */
  getRouteMessage (e) {
    if (e['srcElement']['hash']) {
      let hash = e['srcElement']['hash'] + ''
      this.currentUrl = hash.split('#')[1] || hash
      session.put('RouteMessage', this.currentUrl)
    }
  }

    /**
     * function used to decide whether routing hop is allowable.
     * @param {CanComponentDeactivate} component [CanComponentDeactivate]
     */
  canDeactivate (component: CanComponentDeactivate) {
    if (this.isBasicInfoChanged || this.isPwdInfoChanged) {
      document.onclick = this.getRouteMessage
      EventService.emit('POP_UP_SUBMIT')
      return false
    } else {
      return true
    }
  }
}
