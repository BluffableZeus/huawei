import * as _ from 'underscore'
import { Injectable } from '@angular/core'
import { queryFavorite, deleteFavorite } from 'ng-epg-sdk/vsp'

@Injectable()
export class FavoriteVodService {
  /**
     * get favorite video on demand.
     * @param {any} req [request parameters of interface QueryFavorite]
     */
  showFavoriteVod (req: any) {
    return queryFavorite(req).then(resp => {
      let totalAll = resp.total
      return _.map(resp.favorites, (favorite) => {
        let series
        let episodeCount
        let favoriteInfo = favorite['VOD']
        if (favoriteInfo.averageScore.length === 1) {
          favoriteInfo.averageScore += '.0'
        } else {
          favoriteInfo.averageScore = favoriteInfo.averageScore.substring(0, 4)
        }
        if (favoriteInfo.VODType === '3' && !_.isUndefined(favoriteInfo.series) && !_.isUndefined(favoriteInfo.series[0]) &&
                    !_.isUndefined(favoriteInfo.series[0].sitcomNO)) {
          series = Number(favoriteInfo.series[0].sitcomNO)
        } else {
          series = 0
        }
        episodeCount = this.getEpisodeCount(favoriteInfo)

        return {
          id: favoriteInfo.ID,
          contentType: favorite.contentType,
          vodName: favoriteInfo.name,
          isFavorite: '1',
          totalAll: totalAll,
          floatInfo: favoriteInfo,
          titleName: 'favoriteVedios',
          series: {
            'num': series
          },
          episodeCount: {
            'num': episodeCount
          }
        }
      })
    })
  }

  /**
     * get episode count of favorite VOD.
     * @param {Object} favoriteInfo [favorite VOD information]
     */
  getEpisodeCount (favoriteInfo) {
    if (favoriteInfo.VODType !== '0' && !_.isUndefined(favoriteInfo.episodeCount) && Number(favoriteInfo.episodeCount) > 0) {
      return Number(favoriteInfo.episodeCount)
    }

    return 0
  }

  /**
     * delete favorite VOD.
     * @param {any} req [request parameters of interface DeleteFavorite]
     */
  deleteFavoriteVod (req: any) {
    return deleteFavorite(req).then(resp => resp)
  }
}
