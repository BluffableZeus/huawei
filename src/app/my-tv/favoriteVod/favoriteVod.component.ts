import { Component, ViewChild, OnInit, OnDestroy } from '@angular/core'
import { FavoriteVodService } from './favoriteVod.service'
import { EventService } from 'ng-epg-sdk/services'
import { Router, ActivatedRoute } from '@angular/router'
import { VODRouterService } from '../../shared/services/vod-router.service'
import { MyVideos } from 'ng-epg-ui/webtv-components/myVideos/my-videos.component'

@Component({
  selector: 'app-mytv-favoritevod',
  styleUrls: ['./favoriteVod.component.scss'],
  templateUrl: './favoriteVod.component.html'
})

export class MyTVVodFavoriteComponent implements OnInit, OnDestroy {
    // declare child component MyVideos.
  @ViewChild(MyVideos) myVideos: MyVideos
    // to judge whether it is editing state. true means normal state, false means editing state.
  show = true
    // to judge whether show clear confirmation dialog.
  isClearAll = false
    // to save all favorite VOD data.
  favoriteLists: Array<any> = []
    // encapsulate favorite VOD data for child component.
  favoriteList: {
    'dataList': Array<any>,
    'suspensionID': string
  }
    // ID list of VOD to be deleted.
  vodIDs: Array<string> = []
    // amount of all favorite VOD datas.
  totalAll = 0
    // amount of favorite VOD loaded.
  nowDataLength = 0
    // to judge whether there is no favorite VOD.
  noDataList = '1'
    // to show amount of all favorite VOD datas on the page.
  totalStr = ''
    // check whether interface call is failed.'1' means failed.
  failed = '0'
    // timer for showing failed information.
  failTimer: any
    // keys for showing title and prompt content on the delete confirmation dialog.
  confirmInfoKey = ['deleteConfirmInfo', 'deleteConfirmTitle']
    // to judge whether show the loading icon.
  showLoadIcon = false
    // to judge loading type, true means loading content when you first enter page, false means rolling load.
  firstLoad = true
    // to judge whether the delete operation is finished.
  private deleteFlag = true
    // to save the last ID deleted.
  private sigleDeleteID
    // private constructor
  constructor (
        private showFavorite: FavoriteVodService,
        private route: ActivatedRoute,
        private router: Router,
        private vodRouter: VODRouterService
    ) { }

    /**
     * init function.
     * get favorite VOD data and register event listener.
     */
  ngOnInit () {
    if (!Cookies.getJSON('IS_PROFILE_LOGIN')) {
      return
    }
    this.showLoadIcon = true
    this.showData()
      // listening to the event that the scrollbar is scrolled to the bottom.
    EventService.removeAllListeners(['LOAD_DATA'])
    EventService.on('LOAD_DATA', () => {
      if (this.favoriteLists && this.favoriteLists.length > 0 && this.show) {
        if (this.totalAll !== this.favoriteLists.length) {
            this.showLoadIcon = true
            this.firstLoad = false
            this.showFavorite.showFavoriteVod({
              'offset': this.nowDataLength + '',
              'count': '36',
              'contentTypes': ['VOD'],
              'sortType': 'FAVO_TIME:DESC'
            }).then((resp) => {
              let data = this.favoriteLists
              if (resp) {
                let moredata = data.concat(resp)
                this.favoriteLists = moredata
                this.favoriteList = {
                  'dataList': this.favoriteLists,
                  'suspensionID': 'favoriteListID'
                }
                this.nowDataLength = moredata.length
                this.showLoadIcon = false
              }
            })
          }
      }
    })
  }

    /**
     * destory function.
     * clear the timer,and remove the eventservice.
     */
  ngOnDestroy () {
    clearTimeout(this.failTimer)
    EventService.removeAllListeners(['LOAD_DATA'])
  }

    /**
     * get the favorite datas of video on demand.
     */
  showData () {
    let req = {
      'count': '36',
      'offset': '0',
      'contentTypes': ['VOD'],
      'sortType': 'FAVO_TIME:DESC'
    }
    this.showFavorite.showFavoriteVod(req).then((resp) => {
      this.showLoadIcon = false
      if (resp.length === 0) {
        this.noDataList = '1'
      } else {
        this.noDataList = '0'
        this.favoriteLists = resp
        this.favoriteList = {
            'dataList': this.favoriteLists,
            'suspensionID': 'favoriteListID'
          }
        this.nowDataLength = resp.length
        this.totalAll = Number(resp[0].totalAll)
        if (this.totalAll === 0) {
            this.noDataList = '1'
          } else {
            this.totalStr = '(' + this.totalAll + ')'
          }
      }
    })
  }

    /**
     * click the edit button.
     */
  editData () {
    this.show = false
    EventService.emit('DELETE', '1')
    EventService.emit('IS_VODEDIT', false) // if false,you can not to click the picture to play or show the suspension.
  }

    /**
     * click the 'clear all' button.
     */
  clearAllData () {
    this.isClearAll = true // if true, show the confirm dialog.
  }

    /**
     * click the 'complete' button.
     */
  completeData () {
    this.show = true
    EventService.emit('COMPLETE', '1')
    EventService.emit('IS_VODEDIT', true) // if true,you can click the picture to play and show the suspension.
  }

    /**
     * click the delete button of the picture.
     * @param {Object} $event [data from child component]
     */
  deleteVod ($event) {
    this.vodIDs = []
    if (this.deleteFlag || $event['data'].id !== this.sigleDeleteID) {
      this.sigleDeleteID = $event['data'].id
      this.deleteFlag = false
      this.vodIDs.push($event['data'].id)
      if (this.vodIDs.length !== 0) {
        let req = {
            'contentTypes': ['VOD'],
            'contentIDs': this.vodIDs
          }
        this.showFavorite.deleteFavoriteVod(req).then((resp) => {
            if (resp['result']['retCode'] === '000000000') {
              this.myVideos.dataList.splice($event['index'], 1)
              // this.myVideos.closeDialog($event['data']);
              this.nowDataLength = this.nowDataLength - this.vodIDs.length
              this.totalAll = this.totalAll - this.vodIDs.length
              if (this.totalAll === 0) {
                this.noDataList = '1'
                this.totalStr = ''
                this.completeData()
              } else {
                this.noDataList = '0'
                this.totalStr = '(' + this.totalAll + ')'
              }
              this.vodIDs = []
              this.deleteFlag = true
            }
          }, () => {
            this.deleteFlag = true
          })
      }
    }
  }

    /**
     * click the picture to jump to detail.
     * @param {Object} $event [data from child component]
     */
  detailVod ($event) {
    this.vodRouter.navigate($event[0])
  }

    /**
     * click the cancel button of the 'cannel all' dialog.
     * @param {Object} $event [data from child component]
     */
  cancelData ($event) {
      // if false, close the 'cannel all' dialog.
    this.isClearAll = false
  }

    /**
     * click the cancel button of the 'cannel all' dialog.
     * @param {Object} $event [data from child component]
     */
  confirmData ($event) {
    let req = {
      'contentTypes': ['VOD']
    }
    this.showFavorite.deleteFavoriteVod(req).then(() => {
      this.isClearAll = false
      this.show = true
      this.totalStr = ''
      this.noDataList = '1'
      this.favoriteLists = []
    }).catch(() => {
      this.failed = '1'
      this.failTimer = setTimeout(() => this.failed = '0', 2000)
      this.showData()
    })
  }

    /**
     * if there are no favorite vod,change the style of the edit button.
     */
  noDataStyle () {
    let styles
    if (this.noDataList === '1') {
      styles = {
        'color': '#5f5f5f',
        'cursor': 'default'
      }
    } else {
      styles = {}
    }
    return styles
  }
}
