import * as _ from 'underscore'
import { Component, ViewChild, OnInit, OnDestroy } from '@angular/core'
import { RecordingService } from '../recording.service'
import { EventService } from 'ng-epg-sdk/services'
import { RecordDataService } from '../../../component/recordDialog/recordData.service'
import { CloudService } from './cloud-record.service'
import { session } from 'src/app/shared/services/session'
import { NPVRAppService } from '../../../component/playPopUpDialog/nPvrApp.service'
import { MyRecordings } from 'ng-epg-ui/webtv-components/myRecordings/my-recordings.component'

const QUERYPVR_OPTIONS = {
  params: {
    SID: 'querypvr1'
  }
}

const QUERYPVR_OPTIONS_FOR_CHILD = {
  params: {
    SID: 'querypvr8'
  }
}

@Component({
  selector: 'app-cloud-recording',
  styleUrls: ['./cloud-record.component.scss'],
  templateUrl: './cloud-record.component.html'
})

export class CloudRecordingComponent implements OnInit, OnDestroy {
  @ViewChild(MyRecordings) myRecordings: MyRecordings
  recordFilterLists: {
    'dataList': Array<any>,
    'row': string,
    'clientW': number,
    'suspensionID': string
  }
  recordFilterListss: {
    'dataList': Array<any>,
    'row': string,
    'clientW': number,
    'suspensionID': string
  }

    /**
     * filter condition of sort
     */
  public sort = [{
    ID: 'STARTTIME:DESC',
    name: 'latest_first'
  }, {
    ID: 'STARTTIME:ASC',
    name: 'oldest_first'
  }]

    /**
     * filter condition of type
     */
  public type = [{
    ID: 'all',
    name: 'searchAll'
  }, {
    ID: 'single',
    name: 'single'
  }, {
    ID: 'series',
    name: 'series'
  }]

    /**
     * filter condition of availabilty
     */
  public availabilty = [{
    ID: 'all',
    name: 'searchAll'
  }, {
    ID: 'available',
    name: 'available'
  }, {
    ID: 'unavailable',
    name: 'unavailable'
  }]
  public filterDataList = []
    /**
     * index of current selected sort filter
     */
  public sortIndex = 0
    /**
     * index of current selected type filter
     */
  public typeIndex = 0
    /**
     * index of current selected availability filter
     */
  public availabiltyIndex = 0
  public recordFilterList: Array<any> = []
  public recordFilterListSeries: Array<any> = []
    /**
     * has record type filter
     */
  public hasType = true
    /**
     * name of series record
     */
  public seriesName = ''
  public scrollTop = 0
    /**
     * save the current filter data.
     */
  public filter
  public hasData = true
  public isEdit = false
  public filterData = true
  public seriesId
  public index
  public deleteSpace
  public total
  public totalSeries
    /**
     * new information of recording after change recording settings
     */
  public changeData
  public showNO = false
  public isSeriesPage = true
  public isDelete = false
    /**
     * show load circle
     */
  public showLoadCycle = false

  constructor (
        private recordingService: RecordingService,
        private cloudService: CloudService,
        private recordDataService: RecordDataService,
        private nPVRAppService: NPVRAppService
    ) { }
  ngOnInit () {
    this.sort = [{
      ID: 'STARTTIME:DESC',
      name: 'latest_first'
    }, {
      ID: 'STARTTIME:ASC',
      name: 'oldest_first'
    }]
    this.type = [{
      ID: 'all',
      name: 'searchAll'
    }, {
      ID: 'single',
      name: 'single'
    }, {
        ID: 'series',
        name: 'series'
      }]
    this.availabilty = [{
      ID: 'all',
      name: 'searchAll'
    }, {
      ID: 'available',
      name: 'available'
    }, {
        ID: 'unavailable',
        name: 'unavailable'
      }]
    let self = this
    this.hasType = true
    this.deleteSpace = 0

      // init the selected filter condition
    this.selectFilter('STARTTIME:DESC', 'sort', '0', 'init')

      // listen to delete all recording tasks event
    EventService.removeAllListeners(['DELETEALL'])
    EventService.on('DELETEALL', function () {
      self.deleteAll()
    })

      // listen to click the edit button event
    EventService.removeAllListeners(['DELETES'])
    EventService.on('DELETES', function () {
      self.isEdit = true
    })

      // listen to click the complete button event;
      // listen to click series name and return back to the first page event.
    EventService.removeAllListeners(['COMPLETES'])
    EventService.on('COMPLETES', function () {
      self.isEdit = false
    })

      // listen to play NPVR event
    EventService.removeAllListeners(['PLAYNPVR'])
    EventService.on('PLAYNPVR', (data) => {
      if (this.isEdit) {
        return
      }
      this.nPVRAppService.playNPVR(data)
        // send open the player in  full screen event
      EventService.emit('OPEN_FULLSCREEN_LIVETVVIDEO', {})

      let video = document.querySelector('#videoContainer video')
      let ua = navigator.userAgent.toLowerCase()
        // judge browser
      let isIe = !!ua.match(/msie/i) || !!ua.match(/rv:([\d.]+).*like gecko/)
      let isFirefox = !!ua.match(/firefox\/([\d.]+)/)
      let isEdge = !!ua.match(/edge\/([\d.]+)/)
      if (isIe) {
        video = document.querySelector('#videoContainer')
        video['msRequestFullscreen']()
      } else if (isEdge) {
          video = document.querySelector('#videoContainer')
          video.webkitRequestFullScreen()
        } else if (isFirefox) {
          video = document.querySelector('#videoContainer')
          video['mozRequestFullScreen']()
        } else if (ua.match(/version\/([\d.]+).*safari/)) {
          video.requestFullscreen()
        } else {
          video.webkitRequestFullScreen()
        }
    })

      // listen to cancel record event
    EventService.removeAllListeners(['LIVETV_CENCELRECORD'])
    EventService.on('LIVETV_CENCELRECORD', () => {
        // send query pvr space event
      EventService.emit('DELETESPACE', 'querypvrspace4')
      self.refresh()
    })

      // listen to change single recording to series recording, add series recording successfully event
    EventService.removeAllListeners(['RECORD_UPDATE_ADD_SUCCESS'])
    EventService.on('RECORD_UPDATE_ADD_SUCCESS', () => {
        // send show operation succeeded event
      EventService.emit('PROFILE_INFO_SAVE_SUCCESSFUL')
      self.refresh()
    })

      // listen to click the confirm button and update recording task event in recording settings page
    EventService.removeAllListeners(['MANUAL_SET_SUCCESS'])
    EventService.on('MANUAL_SET_SUCCESS', function (data) {
        // send show operation succeeded event
      EventService.emit('PROFILE_INFO_SAVE_SUCCESSFUL')
        // send change recording name event
      EventService.emit('MANUAL_CHANGE_NAME', data)
      self.changeData = data
    })

      // listen to update the period recording task event.
    EventService.removeAllListeners(['MANUAL_PERIOD_SET_SUCCESS'])
    EventService.on('MANUAL_PERIOD_SET_SUCCESS', function (data) {
        // send show operation succeeded event
      EventService.emit('PROFILE_INFO_SAVE_SUCCESSFUL')
      if (data['effectiveDate'] || data['expireDate'] || data['beginDayTime'] || data['endDayTime'] || data['days']) {
        self.refresh()
      } else {
          // send change recording name event
        EventService.emit('MANUAL_CHANGE_NAME', data)
      }
    })

      // listen to init cloud record page event
    EventService.removeAllListeners(['CLICK_TITLE_CLOUD'])
    EventService.on('CLICK_TITLE_CLOUD', () => {
        // send query pvr space event
      EventService.emit('DELETESPACE', 'querypvrspace1')
      self.hasType = true
      self.isSeriesPage = true
      self.filterData = true
      self.selectFilter('STARTTIME:DESC', 'sort', '0', 'init')
    })
  }

    /**
     * remove listeners on destroy components
     */
  ngOnDestroy () {
    session.pop('hasData')
    EventService.removeAllListeners(['LOAD_DATA'])
    EventService.removeAllListeners(['PLAYNPVR'])
  }

    /**
     * back to the home page of cloud recording
     */
  returnFirstPage () {
    if (this.showLoadCycle) {
      return
    }
    this.isSeriesPage = true
    this.hasType = true
    this.filterData = true
    _.delay(() => {
      document.body.scrollTop = this.scrollTop
    }, 0)
    let filterDataList = session.get('seriesFilterDataList')
    for (let i = 0; i < filterDataList.length; i++) {
      if (filterDataList[i].type === 'sort') {
        this.sortIndex = Number(filterDataList[i].index)
      }
      if (filterDataList[i].type === 'type') {
        this.typeIndex = Number(filterDataList[i].index)
      }
      if (filterDataList[i].type === 'availabilty') {
        this.availabiltyIndex = Number(filterDataList[i].index)
      }
    }
    if (this.isDelete) {
      this.refresh()
    }
    this.isDelete = false
    if (this.sortIndex === 0) {
      session.put('CURRENT_RECORD_SORT', 'latest')
    } else {
      session.put('CURRENT_RECORD_SORT', 'earliest')
    }
    EventService.emit('EXIT_EDIT')
    EventService.emit('COMPLETES', '1')
  }

    /**
     * refresh data of the page
     */
  refresh () {
    if (this.hasType) {
      this.filter = session.get('filterFlag')
      this.getDataCloud()
    } else {
      this.showData()
      this.filter = session.get('filterFlag')
      this.getDataCloud()
    }
  }

    /**
     * delect one pvr task.
     */
  deleteRecord ($event) {
    let self = this
    let flag
    let req
    if ($event.data.policyType === 'Series' || $event.data.policyType === 'Period') {
      req = $event.data.floatInfo.childPVRIDs
      flag = true
    } else {
      req = [$event.data.floatInfo.ID]
      flag = false
      this.deleteSpace = this.deleteSpace + $event.data.floatInfo.duration
    }
    this.recordDataService.deletePVRByID('NPVR', req).then((resp) => {
      if (resp['result']['retCode'] === '000000000') {
        self.isDelete = true
        if ($event.data.isSeriesPage === 'series') {
            let originalName = self.seriesName
            let lastIndex = originalName.lastIndexOf('(')
            let nameHead = originalName.substring(0, lastIndex)
            let nameTail = originalName.substring(lastIndex)
            let taskLength = Number(nameTail.replace(/[^0-9]/ig, '')) - 1
            self.seriesName = nameHead + '( ' + taskLength + ' )'
          }
        if (flag) {
            let options = {
              params: {
                SID: 'querypvrspacebyid1'
              }
            }
            this.recordDataService.queryPVRSpaceByID(req, 'NPVR', options).then((resps) => {
              this.deleteSpace = this.deleteSpace + resps
            })
          }
        self.myRecordings.dataList.splice($event['indexDelete'], 1)
        if (self.myRecordings.dataList.length === 0) {
            self.filterData = false
            self.showNO = true
            self.hasData = false
            EventService.emit('NO_RECORD_EDIT')
          }
        EventService.emit('DELETESPACE', 'querypvrspace5')
      }
    })
  }

    /**
     * delete all recording tasks
     */
  deleteAll () {
    let self = this
    let req
    this.isEdit = false
    req = this.filter.PVRCondition
    this.recordDataService.deletePVRByCondition('NPVR', req).then((resp) => {
      if (resp['result']['retCode'] === '000000000') {
        self.isDelete = true
        if (!this.isSeriesPage) {
            let originalName = self.seriesName
            let lastIndex = originalName.lastIndexOf('(')
            let nameHead = originalName.substring(0, lastIndex)
            let nameTail = '( 0 )'
            self.seriesName = nameHead + nameTail
          }
        EventService.emit('DELETESPACE', 'querypvrspace6')
        if (this.hasType) {
            this.filterData = false
            this.showNO = true
            this.hasData = false
          } else {
            EventService.emit('DELETESERIES', this.index)
            this.filterData = false
            this.showNO = true
          }
      }
    })
  }

    /**
     * get record detail
     */
  detailRecord ($event) {
    if (this.isEdit) {
      return
    }
    this.isSeriesPage = false
    session.pop('seriesFilterDataList')
    this.hasType = false
    this.seriesName = $event.name
    this.seriesId = $event.id
    this.filter = this.cloudService.createPVRConditions(session.get('seriesPageFilterDataList'), this.seriesId, 'cloud').then(resp => {
      this.filter = resp
      this.showData()
    })
    session.put('seriesFilterDataList', _.extend([], session.get('seriesPageFilterDataList')))
    this.index = $event.index
    this.scrollTop = document.body.scrollTop
    document.body.scrollTop = 0
    document.documentElement.scrollTop = 0
  }

    /**
     * click filter condition to filter
     * @param {Object} list [list of filter]
     * @param {Object} type [type of filter]
     * @param {Object} index [index of selected filter in filter]
     * @param {Object} initFlag? [carry on init]
     */
  selectFilter (list, type, index, initFlag?) {
    if (this.showLoadCycle) {
      return
    }
    if (initFlag) {
      this.filterDataList = []
      this.typeIndex = 0
      this.sortIndex = 0
      this.availabiltyIndex = 0
    }
    if (!this.isEdit) {
      let lastChoice
      switch (type) {
        case 'sort':
          lastChoice = _.find(this.filterDataList, item => {
              return item['type'] === 'sort'
            })
          if (lastChoice) {
              let choiceIndex = _.indexOf(this.filterDataList, lastChoice)
              this.filterDataList.splice(choiceIndex, 1)
            }
          this.sortIndex = index
          break
        case 'type':
          lastChoice = _.find(this.filterDataList, item => {
              return item['type'] === 'type'
            })
          if (lastChoice) {
              let choiceIndex = _.indexOf(this.filterDataList, lastChoice)
              this.filterDataList.splice(choiceIndex, 1)
            }
          this.typeIndex = index
          break
        case 'availabilty':
          lastChoice = _.find(this.filterDataList, item => {
              return item['type'] === 'availabilty'
            })
          if (lastChoice) {
              let choiceIndex = _.indexOf(this.filterDataList, lastChoice)
              this.filterDataList.splice(choiceIndex, 1)
            }
          this.availabiltyIndex = index
          break
      }
      this.filterDataList.push({ name: list, type: type, index: index })
      if (this.isSeriesPage) {
        session.put('seriesPageFilterDataList', _.extend([], this.filterDataList))
      }
      if (this.hasType) {
        this.cloudService.createPVRConditions(this.filterDataList, '', 'cloud').then(resp => {
            this.filter = resp
            if (initFlag) {
              this.getDataCloud(initFlag)
            } else {
              this.getDataCloud()
            }
          })
      } else {
        this.cloudService.createPVRConditions(this.filterDataList, this.seriesId, 'cloud').then(resp => {
            this.filter = resp
            this.showData()
          })
      }
    }
  }

    /**
     * show data of record tasks
     */
  showData (option?) {
    let self = this
    self.showNO = false
    self.filterData = false
    let queryPVRRequest = {
      count: '12',
      offset: '0',
      sortType: this.filter.sortType,
      PVRCondition: this.filter.PVRCondition
    }
    self.secondPageQuery(queryPVRRequest)
    EventService.removeAllListeners(['LOAD_DATA'])
    EventService.on('LOAD_DATA', function () {
      if (self.recordFilterListSeries && self.recordFilterListSeries.length &&
                self.recordFilterListSeries.length < self.totalSeries) {
        let offset = self.recordFilterListSeries.length
        self.showLoadCycle = true
        let request = {
            count: '12',
            offset: offset + '',
            sortType: self.filter.sortType,
            PVRCondition: self.filter.PVRCondition
          }
        self.recordDataService.queryPVR(request, QUERYPVR_OPTIONS_FOR_CHILD).then((resp) => {
            let filterList = self.recordingService.getFilterRecord(resp.PVRList, offset, 'cloud', 'series', null, self.totalSeries)
            if (filterList.length > 0) {
              let molist = self.recordingService.getFilterRecord(resp.PVRList, offset, 'cloud', 'series', null, self.totalSeries)
              let moredata = self.recordFilterListSeries.concat(molist)
              self.recordFilterListSeries = moredata
              self.recordFilterListss = {
                'dataList': self.recordFilterListSeries,
                'row': 'All',
                'clientW': document.body.clientWidth,
                'suspensionID': 'filterRecord'
              }
            }
            self.showLoadCycle = false
          })
      }
    })
  }

  secondPageQuery (queryPVRRequest) {
    let self = this
    this.showLoadCycle = true
    this.recordDataService.queryPVR(queryPVRRequest, QUERYPVR_OPTIONS_FOR_CHILD).then(resp => {
      self.totalSeries = resp.total
      if (resp.PVRList && resp.PVRList.length > 0) {
        self.filterData = true
        let filterList = self.recordingService.getFilterRecord(resp.PVRList, 0, 'cloud', 'series', null, self.totalSeries)
        self.recordFilterListSeries = filterList
        self.recordFilterListss = {
            'dataList': filterList,
            'row': 'All',
            'clientW': document.body.clientWidth,
            'suspensionID': 'filterVod'
          }
        EventService.emit('CAN_RECORD_EDIT')
      } else {
        self.filterData = false
        self.showNO = true
        EventService.emit('NO_RECORD_EDIT')
      }
      self.showLoadCycle = false
    })
  }

    /**
     * get data of cloud recording
     */
  getDataCloud (flag?) {
    let self = this
    session.put('filterFlag', this.filter)
    self.recordFilterList.length = 0
    self.recordFilterLists = {
      'dataList': self.recordFilterList,
      'row': 'All',
      'clientW': document.body.clientWidth,
      'suspensionID': 'filterRecord'
    }
    let queryPVRRequest = {
      count: '12',
      offset: '0',
      sortType: this.filter.sortType,
      PVRCondition: this.filter.PVRCondition
    }
    if (flag) {
      self.firstPageQuery(queryPVRRequest, flag)
    } else {
      self.firstPageQuery(queryPVRRequest)
    }
    EventService.removeAllListeners(['LOAD_DATA'])
    EventService.on('LOAD_DATA', function () {
      if (self.recordFilterList && self.recordFilterList.length && self.recordFilterList.length < self.total) {
        let offset = self.recordFilterList.length
        self.showLoadCycle = true
        let request = {
            count: '12',
            offset: offset + '',
            sortType: self.filter.sortType,
            PVRCondition: self.filter.PVRCondition
          }
        self.recordDataService.queryPVR(request, QUERYPVR_OPTIONS).then(resp => {
            let filterList = self.recordingService.getFilterRecord(resp.PVRList, offset, 'cloud')
            if (filterList.length > 0) {
              let molist = self.recordingService.getFilterRecord(resp.PVRList, offset, 'cloud')
              let moredata = self.recordFilterList.concat(molist)
              self.recordFilterList = moredata
              self.recordFilterLists = {
                'dataList': self.recordFilterList,
                'row': 'All',
                'clientW': document.body.clientWidth,
                'suspensionID': 'filterRecord'
              }
            }
            self.showLoadCycle = false
          })
      }
    })
  }

  firstPageQuery (queryPVRRequest, flag?) {
    let self = this
    this.showLoadCycle = true
    this.recordDataService.queryPVR(queryPVRRequest, QUERYPVR_OPTIONS).then(resp => {
      self.total = resp.total
      if (resp.PVRList && resp.PVRList.length > 0) {
        EventService.emit('CAN_RECORD_EDIT')
        let filterList = this.recordingService.getFilterRecord(resp.PVRList, 0, 'cloud')
        self.recordFilterList = filterList
        self.filterData = true
        self.recordFilterLists = {
            'dataList': filterList,
            'row': 'All',
            'clientW': document.body.clientWidth,
            'suspensionID': 'filterRecord'
          }
      } else {
        EventService.emit('NO_RECORD_EDIT')
        if (flag) {
            self.filterData = false
            self.hasData = false
            self.showNO = true
          } else {
            if (self.typeIndex === 0 && self.availabiltyIndex === 0) {
              self.hasData = false
            } else {
              self.hasData = true
            }
            self.filterData = false
            self.showNO = true
          }
      }
      self.showLoadCycle = false
    })
  }
}
