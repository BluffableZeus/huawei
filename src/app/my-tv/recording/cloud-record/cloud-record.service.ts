import * as _ from 'underscore'
import { TranslateService } from '@ngx-translate/core'
import { Injectable } from '@angular/core'
import { session } from 'src/app/shared/services/session'

@Injectable()
export class CloudService {
  constructor (
        private translate: TranslateService
  ) { }

  createPVRConditions (filterDataList, flag, pvrType) {
    let filter = {}
    let PVRCondition = {}
    let singlePVRFilter = {}
    let layout = {}
    let sortType = ''
    let status = []
    let scope = ''
    let policyType
    let storageType
    if (pvrType === 'stb') {
      storageType = 'CPVR'
    } else {
      storageType = 'NPVR'
    }
    let sortCondition = _.find(filterDataList, item => {
      return item['type'] === 'sort'
    })
    if (!_.isUndefined(sortCondition)) {
      sortType = sortCondition['name']
    } else {
      sortType = 'STARTTIME:DESC'
    }
    if (sortType === 'STARTTIME:DESC') {
      session.put('CURRENT_RECORD_SORT', 'latest')
    } else {
      session.put('CURRENT_RECORD_SORT', 'earliest')
    }
    let typeCondition = _.find(filterDataList, item => {
      return item['type'] === 'type'
    })
    scope = this.getScope(typeCondition)
    let availabilty = _.find(filterDataList, item => {
      return item['type'] === 'availabilty'
    })
    status = this.getStatus(availabilty)
    if (flag !== '') {
      singlePVRFilter = {
        scope: scope,
        storageType: storageType,
        status: status,
        isFilterByDevice: pvrType === 'cloud' ? '1' : '0',
        periodicPVRID: flag
      }
      layout = { childPlanLayout: '0' }
    } else {
      singlePVRFilter = {
        scope: scope,
        storageType: storageType,
        isFilterByDevice: pvrType === 'cloud' ? '1' : '0',
        status: status
      }
      layout = { childPlanLayout: '1' }
    }
    policyType = ['PlaybillBased', 'TimeBased']
    PVRCondition = { policyType: policyType, singlePVRFilter: singlePVRFilter, layout: layout }
    filter = { sortType: sortType, PVRCondition: PVRCondition }
    return new Promise((resolve, reject) => {
      resolve(filter)
    })
  }

  /**
     * get scope by record type
     * @param {Object} typeCondition [object of type filter]
     */
  getScope (typeCondition) {
    let scope = ''
    if (!_.isUndefined(typeCondition)) {
      if (typeCondition['name'] === 'all') {
        scope = 'ALL'
      }
      if (typeCondition['name'] === 'single') {
        scope = 'NotIncludeChild'
      }
      if (typeCondition['name'] === 'series') {
        scope = 'OnlyChild'
      }
    } else {
      scope = 'ALL'
    }
    return scope
  }

  /**
     * get status by record availability
     * @param {Object} availabilty [object of availabilty filter]
     */
  getStatus (availabilty) {
    let status = []
    if (!_.isUndefined(availabilty)) {
      if (availabilty['name'] === 'all') {
        status = ['RECORDING', 'OTHER', 'SUCCESS', 'PART_SUCCESS', 'FAIL']
      }
      if (availabilty['name'] === 'available') {
        status = ['RECORDING', 'OTHER', 'SUCCESS', 'PART_SUCCESS']
      }
      if (availabilty['name'] === 'unavailable') {
        status = ['FAIL']
      }
    } else {
      status = ['RECORDING', 'OTHER', 'SUCCESS', 'PART_SUCCESS', 'FAIL']
    }
    return status
  }
}
