import * as _ from 'underscore'
import { Injectable } from '@angular/core'
import { TranslateService } from '@ngx-translate/core'
import { DateUtils } from 'ng-epg-sdk/utils'
import { config } from '../../shared/services/config'
import { CommonService } from '../../core/common.service'
import { session } from 'src/app/shared/services/session'

/**
 * export class RecordingService.
 */
@Injectable()
export class RecordingService {
    // to save the current recording mode.
  private recordingMode = config.PVRMode
    // to save the default status of recording task.
  private defaultStatus = ''

    /**
     * constructor of class RecordingService.
     */
  constructor (
        private translate: TranslateService,
        private commonService: CommonService
    ) { }

    /**
     * get the filter record data.
     * @param {object} recordData [data of the recording task]
     * @param {number} offset [offset of data list]
     * @param {string} source [caller of this function]
     * @param {string} series [to judge whether the is in the first page]
     * @param {string} notEnoughStartTime [value of notEnoughStartTime that is for calculating PVR space]
     * @param {string} total [ammout of all PVR tasks]
     */
  getFilterRecord (recordData, offset, source, series?, notEnoughStartTime?, total?) {
    if (source === 'sche') {
      this.defaultStatus = 'WAIT_RECORD'
    } else {
      this.defaultStatus = 'RECORDING'
    }
    let recordingMode = this.recordingMode
    let list = _.map(recordData, (list1, index) => {
      let channelName
      let subtaskLength
      let fileID = this.getFileID(list1)
      if (list1['policyType'] === 'Period' || list1['policyType'] === 'Series') {
        subtaskLength = list1['childPVRIDs'] && list1['childPVRIDs'].length
      }
      channelName = this.getChannelName(list1)
      let basicInfo = {}
      basicInfo = this.getBasicInfo(list1, basicInfo)
      return {
        id: list1['ID'],
        name: basicInfo['name'],
        posterUrl: basicInfo['posterUrl'] || 'assets/img/default/livetv_program.png',
        log: basicInfo['log'] || 'assets/img/defaultchannel00x80.png',
        seriesName: basicInfo['seriesName'],
        genres: basicInfo['genres'],
        rating: basicInfo['rating'],
        introduce: basicInfo['introduce'],
        floatInfo: list1,
        status: basicInfo['status'],
        policyType: list1['policyType'],
        recordweekTime: basicInfo['recordweekTime'],
        recordTime: basicInfo['recordTime'],
        duration: basicInfo['duration'],
        endTime: list1['endTime'],
        startTime: list1['startTime'],
        spaceTime: Number(basicInfo['spaceTime']),
        recordingMode: recordingMode,
        notEnoughStartTime: Number(notEnoughStartTime),
        isSeriesPage: series,
        percent: basicInfo['curLength'] >= basicInfo['length'] ? '100%'
            : Number(basicInfo['curLength']) / Number(basicInfo['length']) * 100 + '%',
        channelID: list1['channelID'],
        subtaskLength: subtaskLength,
        channelName: channelName || session.get('CHANNEL_NAME_FROM_PARENT'),
        index: index + Number(offset),
        total: total,
        parentPlanID: list1['parentPlanID'],
        length: basicInfo['length'],
        fileID: fileID,
        beginOffsetTime: Number(list1['startTime']) - Number(list1['beginOffset'] || 0) * 1000,
        endOffsetTime: Number(list1['endTime']) + Number(list1['endOffset'] || 0) * 1000
      }
    })
    return list
  }

    /**
     * get file ID of the recording task.
     * @param {object} list [data of recording task]
     */
  getFileID (list) {
    let id = list['files'] && list['files'][0] && list['files'][0]['fileID']
    return id
  }

    /**
     * get channel name of the recording task.
     * @param {object} list1 [data of recording task]
     */
  getChannelName (list1) {
    let channelName = list1['extensionFields'] && list1['extensionFields'][0] && list1['extensionFields'][0].values &&
            list1['extensionFields'][0].values[0] || list1['playbill'] && list1['playbill'].channel &&
            list1['playbill'].channel.name
    return channelName
  }

    /**
     * encapsulate the basic information of the recording task.
     * @param {object} list1 [data of recording task]
     * @param {object} basicInfo [object to save the basic information]
     */
  getBasicInfo (list1, basicInfo) {
    if (list1['policyType'] === 'Series') {
      basicInfo = this.getSeriesBasicInfo(list1, basicInfo)
    } else if (list1['policyType'] === 'PlaybillBased') {
      basicInfo = this.getPlaybillBasicInfo(list1, basicInfo)
    } else {
      basicInfo['introduce'] = ''
    }
    this.getRecordStatus(list1, basicInfo)
    if (basicInfo['status'] === 'RECORDING') {
      basicInfo = this.recordingTask(list1, basicInfo)
    } else if (basicInfo['status'] === 'WAIT_RECORD') {
      basicInfo = this.waitRecordTask(list1, basicInfo)
    } else {
      basicInfo = this.successTask(list1, basicInfo)
    }
    return basicInfo
  }

    /**
     * get status of the recording task.
     * @param {object} list1 [data of recording task]
     * @param {object} basicInfo [object to save the basic information]
     */
  getRecordStatus (list1, basicInfo) {
    if (list1['policyType'] === 'Series' || list1['policyType'] === 'Period') {
      basicInfo['status'] = list1['childPVR'] && list1['childPVR']['status'] || this.defaultStatus
      if (list1['childPVRIDs'] && list1['childPVRIDs'].length === 0) {
        basicInfo['status'] = 'WAIT_RECORD' || this.defaultStatus
      }
    } else {
      basicInfo['status'] = list1['status'] || this.defaultStatus
    }
  }

    /**
     * encapsulate the basic information of series PVR task.
     * @param {object} list1 [data of recording task]
     * @param {object} basicInfo [object to save the basic information]
     */
  getSeriesBasicInfo (list1, basicInfo) {
    basicInfo['introduce'] = list1['childPVR'] && list1['childPVR']['playbill'] ? list1['childPVR']['playbill'].introduce : ''
    basicInfo = this.getSeriesPosterUrlAndLog(list1, basicInfo)
    basicInfo = this.getSeriesSeasonAndSitcom(list1, basicInfo)
    basicInfo = this.getSeriesName(basicInfo)
    basicInfo['genres'] = list1['childPVR'] && list1['childPVR'].playbill && list1['childPVR'].playbill.genres &&
            list1['childPVR'].playbill.genres[0] && list1['childPVR'].playbill.genres[0].genreName
    basicInfo['rating'] = list1['childPVR'] && list1['childPVR'].playbill && list1['childPVR'].playbill.rating &&
            list1['childPVR'].playbill.rating.name
    return basicInfo
  }

    /**
     * encapsulate the basic information of playbill-based PVR task.
     * @param {object} list1 [data of recording task]
     * @param {object} basicInfo [object to save the basic information]
     */
  getPlaybillBasicInfo (list1, basicInfo) {
    basicInfo['introduce'] = list1['playbill'] && list1['playbill'].introduce
    basicInfo['posterUrl'] = this.getPosterUrl(list1)
    basicInfo['log'] = list1['playbill'] && list1['playbill'].channel &&
            list1['playbill'].channel.logo && list1['playbill'].channel.logo['url']
    basicInfo = this.getPlaybilSeasonAndSitcom(list1, basicInfo)
    basicInfo = this.getSeriesName(basicInfo)
    basicInfo['genres'] = list1['playbill'] && list1['playbill'].genres &&
            list1['playbill'].genres[0] && list1['playbill'].genres[0].genreName
    basicInfo['rating'] = list1['playbill'] && list1['playbill'].rating && list1['playbill'].rating.name
    return basicInfo
  }

    /**
     * get status of the recording task.
     * @param {object} list1 [data of recording task]
     */
  getPosterUrl (list1) {
    let url = list1['playbill'] && list1['playbill'].picture &&
            list1['playbill'].picture.posters && list1['playbill'].picture.posters[0]
    return url
  }

    /**
     * get url of poster of the recording task.
     * @param {object} list1 [data of recording task]
     * @param {object} basicInfo [object to save the basic information]
     */
  getSeriesPosterUrlAndLog (list1, basicInfo) {
    basicInfo['posterUrl'] = list1['childPVR'] && list1['childPVR'].playbill && list1['childPVR'].playbill.picture &&
            list1['childPVR'].playbill.picture.posters && list1['childPVR'].playbill.picture.posters[0]
    basicInfo['log'] = list1['childPVR'] && list1['childPVR'].playbill && list1['childPVR'].playbill.channel &&
            list1['childPVR'].playbill.channel.logo && list1['childPVR'].playbill.channel.logo['url']
    return basicInfo
  }

    /**
     * get season number and sitcom number of series task.
     * @param {object} list1 [data of recording task]
     * @param {object} basicInfo [object to save the basic information]
     */
  getSeriesSeasonAndSitcom (list1, basicInfo) {
    basicInfo['seasonNO'] = list1['childPVR'] && list1['childPVR'].playbill && list1['childPVR'].playbill.playbillSeries &&
            list1['childPVR'].playbill.playbillSeries.seasonNO
    basicInfo['sitcomNO'] = list1['childPVR'] && list1['childPVR'].playbill && list1['childPVR'].playbill.playbillSeries &&
            list1['childPVR'].playbill.playbillSeries.sitcomNO
    return basicInfo
  }

    /**
     * get season number and sitcom number of playbill-based task.
     * @param {object} list1 [data of recording task]
     * @param {object} basicInfo [object to save the basic information]
     */
  getPlaybilSeasonAndSitcom (list1, basicInfo) {
    basicInfo['seasonNO'] = list1['playbill'] && list1['playbill'].playbillSeries && list1['playbill'].playbillSeries.seasonNO
    basicInfo['sitcomNO'] = list1['playbill'] && list1['playbill'].playbillSeries && list1['playbill'].playbillSeries.sitcomNO
    if (basicInfo['sitcomNO'] < 10) {
      basicInfo['sitcomNO'] = '0' + basicInfo['sitcomNO']
    }
    return basicInfo
  }

    /**
     * get series name by the related information of series and episodes.
     * @param {object} basicInfo [object to save the basic information]
     */
  getSeriesName (basicInfo) {
    if (basicInfo['seasonNO'] && basicInfo['sitcomNO']) {
      basicInfo['seriesName'] = this.translate.instant('vod_season', { num: basicInfo['seasonNO'] }) +
                ', ' + this.translate.instant('episode', { num: basicInfo['sitcomNO'] })
    } else if (basicInfo['seasonNO'] && !basicInfo['sitcomNO']) {
      basicInfo['seriesName'] = this.translate.instant('vod_season', { num: basicInfo['seasonNO'] })
    } else if (!basicInfo['seasonNO'] && basicInfo['sitcomNO']) {
        basicInfo['seriesName'] = this.translate.instant('episode', { num: basicInfo['sitcomNO'] })
      } else {
        basicInfo['seriesName'] = ''
      }
    return basicInfo
  }

    /**
     * handle data of the recording task.
     * @param {object} list1 [data of recording task]
     * @param {object} basicInfo [object to save the basic information]
     */
  recordingTask (list1, basicInfo) {
    basicInfo['recordweekTime'] = ''
    basicInfo['recordTime'] = 'now'
    if (list1['policyType'] === 'Period' || list1['policyType'] === 'Series') {
      basicInfo['duration'] = this.getDuration(list1)
      let subtast = list1['childPVRIDs'] && list1['childPVRIDs'].length
      basicInfo['name'] = list1['name'] + ' ( ' + subtast + ' )'
      basicInfo['length'] = Number(list1['childPVR'] && list1['childPVR']['duration']) * 1000
      if (list1['policyType'] === 'Series') {
        basicInfo['curLength'] = basicInfo['length'] -
                    (Number(list1['childPVR'] && list1['childPVR']['endTime']) -
                        Date.now()['getTime']() + Number(list1['endOffset'] * 1000))
      } else {
        basicInfo['curLength'] = Date.now()['getTime']() - Number(list1['childPVR'] && list1['childPVR']['startTime'])
      }
    } else {
      basicInfo['name'] = list1['name']
      basicInfo['duration'] = this.commonService.dealWithDateJson('D10', list1['startTime']) +
             '-' + this.commonService.dealWithDateJson('D10', list1['endTime'])
      basicInfo['length'] = Number(list1['duration']) * 1000
      if (list1['policyType'] === 'PlaybillBased') {
        basicInfo['curLength'] = basicInfo['length'] -
                    (Number(list1['endTime']) - Date.now()['getTime']() + Number(list1['endOffset'] * 1000))
      } else {
        basicInfo['curLength'] = Date.now()['getTime']() - Number(list1['startTime'])
      }
    }
    return basicInfo
  }

    /**
     * get duration of the recording task.
     * @param {object} list1 [data of recording task]
     */
  getDuration (list1) {
    let duration = this.commonService.dealWithDateJson('D10', list1['childPVR'] && list1['childPVR']['startTime']) +
            '-' + this.commonService.dealWithDateJson('D10', list1['childPVR'] && list1['childPVR']['endTime'])
    return duration
  }

    /**
     * handle data of the scheduled task.
     * @param {object} list1 [data of recording task]
     * @param {object} basicInfo [object to save the basic information]
     */
  waitRecordTask (list1, basicInfo) {
    let subtast = list1['childPVRIDs'] && list1['childPVRIDs'].length
    if (list1['policyType'] === 'Period' || list1['policyType'] === 'Series') {
      basicInfo['recordweekTime'] = DateUtils.format(list1['childPVR'] && list1['childPVR']['startTime'], 'ddd')
      basicInfo['recordTime'] = this.commonService.dealWithDateJson('D01', list1['childPVR'] && list1['childPVR']['startTime'])
      basicInfo['duration'] = this.commonService.dealWithDateJson('D10', list1['childPVR'] && list1['childPVR']['startTime']) +
            '-' + this.commonService.dealWithDateJson('D10', list1['childPVR'] && list1['childPVR']['endTime'])
      basicInfo['name'] = list1['name'] + ' ( ' + subtast + ' )'
      basicInfo['spaceTime'] = list1['customizedProperty'] && list1['customizedProperty'].childPVRMaxStartTime
    } else {
      basicInfo['recordweekTime'] = DateUtils.format(list1['startTime'], 'ddd')
      basicInfo['recordTime'] = this.commonService.dealWithDateJson('D01', list1['startTime'])
      basicInfo['duration'] = DateUtils.format(list1['startTime'], 'HH:mm') + '-' + DateUtils.format(list1['endTime'], 'HH:mm')
      basicInfo['duration'] = this.commonService.dealWithDateJson('D10', list1['startTime']) +
            '-' + this.commonService.dealWithDateJson('D10', list1['endTime'])
      basicInfo['name'] = list1['name']
      basicInfo['spaceTime'] = list1['startTime']
    }
    basicInfo['length'] = 1
    basicInfo['curLength'] = 1
    return basicInfo
  }

    /**
     * handle data of the successfully recorded task.
     * @param {object} list1 [data of recording task]
     * @param {object} basicInfo [object to save the basic information]
     */
  successTask (list1, basicInfo) {
    let subtast = list1['childPVRIDs'] && list1['childPVRIDs'].length
    if (list1['policyType'] === 'Period' || list1['policyType'] === 'Series') {
      basicInfo['recordweekTime'] = DateUtils.format(list1['childPVR'] && list1['childPVR']['startTime'], 'ddd')
      basicInfo['recordTime'] = this.commonService.dealWithDateJson('D01', list1['childPVR'] && list1['childPVR']['startTime'])
      if (list1['childPVR'] && list1['childPVR']['duration']) {
        basicInfo['duration'] = Math.ceil(list1['childPVR']['duration'] / 60) + ' ' + this.translate.instant('record_min')
      } else {
        basicInfo['duration'] = '0' + ' ' + this.translate.instant('record_min')
      }
      basicInfo['name'] = list1['name'] + ' ( ' + subtast + ' )'
    } else {
      basicInfo['recordweekTime'] = DateUtils.format(list1['startTime'], 'ddd')
      basicInfo['recordTime'] = this.commonService.dealWithDateJson('D01', list1['startTime'])
      basicInfo['name'] = list1['name']
      if (list1['duration']) {
        basicInfo['duration'] = Math.ceil(list1['duration'] / 60) + ' ' + this.translate.instant('record_min')
      } else {
        basicInfo['duration'] = '0' + ' ' + this.translate.instant('record_min')
      }
    }
    basicInfo['length'] = 1
    basicInfo['curLength'] = 1
    return basicInfo
  }
}
