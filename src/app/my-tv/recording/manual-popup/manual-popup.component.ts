import * as _ from 'underscore'
import { Component, ViewChild } from '@angular/core'
import { TranslateService } from '@ngx-translate/core'
import { EventService } from 'ng-epg-sdk/services'
import { session } from 'src/app/shared/services/session'
import { ManualService } from '../manual-record/manual-record.service'
import { DateUtils } from 'ng-epg-sdk/utils'
import { RecordRepeatDataComponent } from '../manual-record/record-repeat-data/recordrepeatdata.component'
import { RecordStartTimeComponent } from '../manual-record/recordStartTime/recordStartTime.component'
import { RecordDataNameComponent } from '../manual-record/record-manual-data/recordDataName.component'
import { config } from '../../../shared/services/config'
import { RecordDataService } from '../../../component/recordDialog/recordData.service'
import { CommonService } from '../../../core/common.service'
import { CacheConfigService } from '../../../shared/services/cache-config.service'

const SPACE_KEY = 32, LEFT_KEY = 37, UP_KEY = 38, RIGHT_KEY = 39, DOWN_KEY = 40
const ONE = 1, TWO = 2, THREE = 3, FOUR = 4, FIVE = 5, SIX = 6, SEVEN = 7

@Component({
  selector: 'app-manualrecord-popup',
  styleUrls: ['./manual-popup.component.scss'],
  templateUrl: './manual-popup.component.html'
})

export class ManualRecordPopUpComponent {
  @ViewChild(RecordRepeatDataComponent) recordRepeatData: RecordRepeatDataComponent
  @ViewChild(RecordStartTimeComponent) recordStartTime: RecordStartTimeComponent
  @ViewChild(RecordDataNameComponent) recordDataName: RecordDataNameComponent

    /**
     * show recording settings
     */
  public isShowManualPopUp = false
    /**
     * record type is single
     */
  public isSingle = false
    /**
     * show next day icon
     */
  public showNextDayIcon = false
    /**
     * recording settings have been changed
     */
  public ContentHasChanged = false
    /**
     * show quality list
     */
  public showQualityList = false
  public hasQuality = false
  public recordDetails = {}
    /**
     * configuration data
     */
  public configData = {}
  public repeatIndex = []
  public initRepeatIndex = []
  public qualityList = []
    /**
     * a array of repeat day
     */
  public repeatArray = []
    /**
     * channel id
     */
  public channelID = ''
    /**
     * name of the record
     */
  public recordName = ''
    /**
     * the day of repeat
     */
  public repeat = ''
    /**
     * start date of record
     */
  public startDate = ''
  public initStartDate = ''
  public endDate = ''
  public whiteEndDate = ''
  public initEndDate = ''
  public showStartDate = ''
  public showEndDate = ''
    /**
     * start hour of record
     */
  public startHour = ''
  public initStartHour = ''
    /**
     * start minute of record
     */
  public startMin = ''
  public initStartMin = ''
    /**
     * start second of record
     */
  public startSec = ''
    /**
     * start millisecond of record
     */
  public startMill = ''
    /**
     * end hour of record
     */
  public endHour = ''
  public initEndHour = ''
    /**
     * end minute of record
     */
  public endMin = ''
  public initEndMin = ''
    /**
     * end second of record
     */
  public endSec = ''
    /**
     * end milliseconds of record
     */
  public endMill = ''
    /**
     * key of record date
     */
  public recordDateKey = ''
    /**
     * start date of single record
     */
  public singleDate = ''
  public initSingleDate = ''
    /**
     * type of storage
     */
  public storageType = ''
    /**
     * the status of record
     */
  public status = ''
  public pvrId = ''
    /**
     * current quality
     */
  public curQuality = ''
  public chooseTimeIndex = -1
  public curYear: number
  public curMonth: number
  public curDay: number
  public curHour: number
  public curMin: number
  public curSec: number
    /**
     * current date
     */
  public currentDate
    /**
     * the name of record is empty
     */
  public isNameEmpty = false
    /**
     * all channels information
     */
  public allChannelCache = []
  public cPVRChannelCache = []
  public cpvrMediaID = ''
  public initQuality = ''
    /**
     * PVR mode, includes CPVR，NPVR，CPVR&NPVR, MIXPVR
     */
  public PVRMode = ''
    /**
     * the selected storage is Cloud or STB
     */
  public chooseStorage = [false, false]
    /**
     *  the task support multiple storage
     */
  public canChooseStorage = false
    /**
     * current channel
     */
  public curChannel
    /**
     * type of storage
     */
  public initStorageType
    /**
     * default quality
     */
  public defaultQuality
    /**
     * the key of default quality
     */
  public defaultQualityKey
    /**
     * blacklist of input
     */
  public blackListCache = ''
    /**
     * when the name is modified, the maximum length the input box can enter
     */
  public maxLengthCache = 128
    /**
     * variable to check the system whether will auto change the Recording name by time setting.
     */
  public canAutoChangeName = false
  public channelName = ''
  public defaultRecName = ''
  public focusRecName = ''
    /**
     * check whether support all types of CPVR.
     * true: means support all types of CPVR.
     * false: means only support playbill-based CPVR.
     */
  public supportAllCPVR = false
  public isEnterChangeButton = false
  public saveSingleDate
  public saveStartDate
  public saveEndDate
  public hasLogicChannel = true
  public orginalDefinition = ''
  constructor (private manualService: ManualService,
        private translate: TranslateService,
        private recordDataService: RecordDataService,
        private cacheConfigService: CacheConfigService,
        private commonService: CommonService) {
  }

    /**
     * when open the dialog, it will load some related data.
     * @param {Object} data [record details]
     */
  loadData (data) {
    this.blackListCache = session.get('COMMON_INPUT_BLACKLIST_CACHE')
    this.maxLengthCache = session.get('COMMON_INPUT_MAXLENGTH_CACHE')
    this.configData = config
    this.supportAllCPVR = this.configData['supportAllCPVR']
    this.PVRMode = this.configData['PVRMode']
    this.storageType = data['storageType']
    this.initStorageType = this.storageType
    this.getAllChannelCache()
    this.getDefaultQuality()
    this.getCurrentTimeInfo()
    this.pvrId = data['ID']
    this.recordName = data['name']
    this.channelID = data['channelID']
    if (!_.isUndefined(data['days']) && data['days'].length > 0) {
      for (let i = 0; i < data['days'].length; i++) {
        data['days'][i] = Number(data['days'][i])
      }
    }
    if (this.storageType === 'NPVR') {
      data['cpvrMediaID'] = ''
    }
    this.recordDetails = _.clone(data)
    this.judgePolicyType(data)
    if (this.PVRMode === 'CPVR&NPVR') {
      this.isSupportChooseStorage(data)
    }
    if (this.hasLogicChannel) {
      this.getQualityData(data)
      this.qualityList = []
      this.getQualityList(data)
    }
    this.initStartHour = this.startHour
    this.initStartMin = this.startMin
    this.initEndHour = this.endHour
    this.initEndMin = this.endMin
    this.checkRevisable()
    this.checkIsNextDay()
    this.checkNameIsDefault(data)
  }

    /**
     * get quality data
     * @param {Object} data [record details]
     */
  getQualityData (data) {
    if (this.storageType === 'CPVR') {
      this.cpvrMediaID = data['cpvrMediaID'] || ''
      if ((this.isSingle && this.status === 'WAIT_RECORD') ||
                (!this.isSingle && (this.status === 'WAIT_RECORD' || this.status === 'RECORDING'))) {
        this.hasQuality = true
        this.getCurrQuality(data)
      } else {
        this.hasQuality = false
      }
    } else {
      this.cpvrMediaID = ''
      this.hasQuality = false
    }
  }

    /**
     * judge the type of recording task
     * @param {Object} data [record details]
     */
  judgePolicyType (data) {
    if (data['policyType'] === 'TimeBased') {
      this.handleTimeBasedData(data)
    } else if (data['policyType'] === 'Period') {
      this.handlePeriodData(data)
      this.checkPeriodStatus()
    }
  }

    /**
     * get all channels from static and dynamic datas.
     */
  getAllChannelCache () {
    let userType
    if (Cookies.getJSON('IS_PROFILE_LOGIN')) {
      userType = Cookies.getJSON('USER_ID')
    } else {
      userType = session.get('Guest')
    }
    let staticData = session.get(userType + '_Static_Version_Data')
    let dynamicData = session.get(userType + '_Dynamic_Version_Data')
    _.map(dynamicData['channelDynamaicProp'], (list1, index) => {
      _.each(staticData['channelDetails'], (list) => {
        if (list1['ID'] === list['ID']) {
            list['physicalChannelsDynamicProperties'] = list1['physicalChannelsDynamicProperties']
          }
      })
    })
    if (!this.configData['supportAllCPVR']) {
      this.allChannelCache = this.cacheConfigService.getNPVRChannelManual(staticData['channelDetails'])
    } else {
      let npvrChannel = this.cacheConfigService.getNPVRChannelManual(staticData['channelDetails'])
      this.cPVRChannelCache = this.cacheConfigService.getCPVRChannelManual(staticData['channelDetails'])
      this.allChannelCache = _.union(npvrChannel, this.cPVRChannelCache)
    }
  }

    /**
     * get channel name.
     * @param {Object} data [record details]
     */
  getChannelName (data) {
    if (!_.isUndefined(data['extensionFields'])) {
      let channelNameObj = _.find(data['extensionFields'], item => {
        return item['key'] === 'channelName'
      })
      if (channelNameObj && channelNameObj['values'] && channelNameObj['values'][0]) {
        this.channelName = channelNameObj['values'][0]
      }
    }
  }

    /**
     * function to handle the time-based recording task.
     * @param {Object} data [record details]
     */
  handleTimeBasedData (data) {
    this.isSingle = true
    this.recordDateKey = 'recored_date'
    this.saveSingleDate = DateUtils.format(data['startTime'], 'DD.MM.YYYY')
    this.singleDate = this.commonService.dealWithOrgTime(this.saveSingleDate)
    this.initSingleDate = this.saveSingleDate
    let startDateObj = new Date(Number(data['startTime']))
    this.startHour = this.addZero(startDateObj.getHours() + '')
    this.startMin = this.addZero(startDateObj.getMinutes() + '')
    this.startSec = this.addZero(startDateObj.getSeconds() + '')
    this.startMill = this.addZero(startDateObj.getMilliseconds() + '')
    let endDateObj = new Date(Number(data['endTime']))
    this.endHour = this.addZero(endDateObj.getHours() + '')
    this.endMin = this.addZero(endDateObj.getMinutes() + '')
    this.endSec = this.addZero(endDateObj.getSeconds() + '')
    this.endMill = this.addZero(endDateObj.getMilliseconds() + '')
    this.status = data['status']
  }

    /**
     * function to handle the period recording task.
     * @param {Object} data [record details]
     */
  handlePeriodData (data) {
    this.isSingle = false
    this.recordDateKey = 'record_dates'
    let day = data['days']
    let days = []
    let daily = this.translate.instant('record_daily')
    let mon = this.translate.instant('Mon_suspension')
    let tue = this.translate.instant('Tues_suspension')
    let wed = this.translate.instant('Wed_suspension')
    let thu = this.translate.instant('Thur_suspension')
    let fri = this.translate.instant('Fri_suspension')
    let sat = this.translate.instant('Sat_suspension')
    let sun = this.translate.instant('Sun_suspension')
    this.repeatArray = [daily, mon, tue, wed, thu, fri, sat, sun]
    for (let i = 0; i < day.length; i++) {
      if (day.length === SEVEN) {
        days = [daily]
        this.repeat = days[0]
        break
      }
      if (day[i] === ONE) {
        days.push(this.repeatArray[1])
      }
      if (day[i] === TWO) {
        days.push(this.repeatArray[2])
      }
      if (day[i] === THREE) {
        days.push(this.repeatArray[3])
      }
      if (day[i] === FOUR) {
        days.push(this.repeatArray[4])
      }
      if (day[i] === FIVE) {
        days.push(this.repeatArray[5])
      }
      if (day[i] === SIX) {
        days.push(this.repeatArray[6])
      }
      if (day[i] === SEVEN) {
        days.push(this.repeatArray[7])
      }
    }
    if (this.repeat !== this.translate.instant('record_daily')) {
      for (let i = 0; i < days.length; i++) {
        this.repeat += days[i] + ' '
      }
    }
    this.repeatIndex = data['days']
    this.initRepeatIndex = this.repeatIndex
    this.saveStartDate = this.getDateStr(data['effectiveDate'])
    this.startDate = this.commonService.dealWithOrgTime(this.saveStartDate)
    this.getShowStartDate()
    this.initStartDate = this.saveStartDate
    if (data['expireDate'] === '20991231' || _.isUndefined(data['expireDate'])) {
      this.endDate = 'unlimited'
      this.showEndDate = 'unlimited'
    } else {
      this.saveEndDate = this.getDateStr(data['expireDate'])
      this.endDate = this.commonService.dealWithOrgTime(this.saveEndDate)
      this.getShowEndDate()
        // this.whiteEndDate = this.commonService.dealWithDateJson();
    }
    this.initEndDate = this.endDate
    this.startHour = data['beginDayTime'].substring(0, 2)
    this.startMin = data['beginDayTime'].substring(2, 4)
    this.endHour = data['endDayTime'].substring(0, 2)
    this.endMin = data['endDayTime'].substring(2, 4)
  }

  getShowStartDate () {
    let dateStr = this.startDate.split('/')
    if (session.get('languageName') === 'zh' && dateStr[2].length === ONE) {
      this.showStartDate = dateStr[0] + '/' + dateStr[1] + '/0' + dateStr[2]
    } else {
      this.showStartDate = this.startDate
    }
  }

  getShowEndDate () {
    if (this.endDate !== 'unlimited' && this.endDate !== 'seven_days_later' &&
        this.endDate !== 'fourteen_days_later' && this.endDate !== 'thirty_days_later' && session.get('languageName') === 'zh') {
      let dateStr = this.endDate.split('/')
      if (dateStr[2].length === ONE) {
        this.showEndDate = dateStr[0] + '/' + dateStr[1] + '/0' + dateStr[2]
      } else {
        this.showEndDate = this.endDate
      }
    } else {
      this.showEndDate = this.endDate
    }
  }

    /**
     * Get the default name by the time setting of the recording task.
     * And we should check whether the task name is the same as the default name.
     * @param {Object} data [record details]
     */
  checkNameIsDefault (data) {
    this.getChannelName(data)
    if (data['policyType'] === 'TimeBased') {
      let dateArray = this.saveSingleDate.split('.')
      this.defaultRecName = this.channelName + '_' + this.addZero(dateArray[0]) + '.' +
                this.addZero(dateArray[1]) + '_' + this.addZero(this.startHour) + ':' + this.addZero(this.startMin)
    } else {
      this.defaultRecName = this.channelName + '_' + this.addZero(this.startHour) + ':' + this.addZero(this.startMin) +
                '－' + this.addZero(this.endHour) + ':' + this.addZero(this.endMin)
    }
    if (this.recordName === this.defaultRecName) {
      this.canAutoChangeName = true
    } else {
      this.canAutoChangeName = false
    }
  }

    /**
     * set the recording name auto according to the value of date and time.
     */
  autoSetNameByTime () {
    if (this.canAutoChangeName) {
      if (this.isSingle) {
        let dateArray = this.saveSingleDate.split('.')
        this.recordName = this.channelName + '_' + this.addZero(dateArray[0]) + '.' +
                    this.addZero(dateArray[1]) + '_' + this.addZero(this.startHour) + ':' + this.addZero(this.startMin)
      } else {
        this.recordName = this.channelName + '_' + this.addZero(this.startHour) + ':' + this.addZero(this.startMin) +
                    '－' + this.addZero(this.endHour) + ':' + this.addZero(this.endMin)
      }
    }
  }

    /**
     * check the task whether support multiple storage.
     * @param {Object} data [record details]
     */
  isSupportChooseStorage (data) {
    if (this.storageType === 'NPVR') {
      this.chooseStorage = [true, false]
    } else if (this.storageType === 'CPVR') {
      this.chooseStorage = [false, true]
    }
    this.curChannel = _.find(this.allChannelCache, item => {
      return item['ID'] === data['channelID']
    })
    if (this.curChannel) {
      let supportNPVR = this.setSupportNPVR()
      let supportCPVR = this.setSupportCPVR()
      if (supportNPVR === true && supportCPVR === true &&
                ((this.isSingle && this.status === 'WAIT_RECORD') || (!this.isSingle && this.status !== 'SUCCESS'))
        ) {
        this.canChooseStorage = true
      } else {
        this.canChooseStorage = false
      }
      this.hasLogicChannel = true
    } else {
      this.canChooseStorage = false
      this.hasLogicChannel = false
    }
  }

    /**
     * check the current channel whether support NPVR
     */
  setSupportNPVR () {
    let supportNPVR = false
    let npvrPhyChannels = this.commonService.judgeRecordDynamic('npvrRecordCR', this.curChannel)
    if (npvrPhyChannels.length > 0) {
      supportNPVR = true
    }
    return supportNPVR
  }

    /**
     * check the current channel whether support CPVR
     */
  setSupportCPVR () {
    let supportCPVR = false
    let cpvrPhyChannels = this.commonService.judgeRecordDynamic('cpvrRecordCR', this.curChannel)
    if (cpvrPhyChannels.length > 0) {
      supportCPVR = true
    }
    return supportCPVR
  }

    /**
     * user select the Cloud storage.
     */
  selectCloud () {
    this.chooseStorage = [true, false]
    this.storageType = 'NPVR'
    this.hasQuality = false
    this.ContentHasChanged = this.checkContentChange()
    this.hideQualityList()
  }

    /**
     * user select the STB storage.
     */
  selectSTB () {
    this.chooseStorage = [false, true]
    this.storageType = 'CPVR'
    this.hasQuality = true
    if (this.curQuality === '') {
      let curObj = this.findUpNearQuality(this.qualityList, this.defaultQualityKey)
      this.curQuality = curObj['quality']
      this.cpvrMediaID = curObj['cpvrMediaID']
    }
    this.ContentHasChanged = this.checkContentChange()
    this.hideQualityList()
  }

    /**
     * check the task status of the period task.
     */
  checkPeriodStatus () {
    let effectiveDate = Number(this.recordDetails['effectiveDate'])
    let expireDate
    if (_.isUndefined(this.recordDetails['expireDate'])) {
      expireDate = 20991231
    } else {
      expireDate = Number(this.recordDetails['expireDate'])
    }
    let nowDate = Number(DateUtils.format(Date.now()['getTime'](), 'YYYYMMDD'))
    let endTime = Number(this.recordDetails['endDayTime'])
    let nowTime = Number(DateUtils.format(Date.now()['getTime'](), 'HHmmss'))
    if ((nowDate > expireDate) || (nowDate === expireDate && nowTime > endTime)) {
      this.status = 'SUCCESS'
    } else if (nowDate >= effectiveDate) {
      this.status = 'RECORDING'
    } else {
      this.status = 'WAIT_RECORD'
    }
  }

    /**
     * check whether can set start time.
     */
  checkRevisable () {
    if (this.dom('start-hours') && this.dom('start-mins')) {
      if (this.status === 'RECORDING' && this.isSingle === true) {
        this.dom('start-hours').setAttribute('readOnly', 'true')
        this.dom('start-mins').setAttribute('readOnly', 'true')
      } else {
        this.dom('start-hours').removeAttribute('readOnly')
        this.dom('start-mins').removeAttribute('readOnly')
      }
    }
  }

    /**
     * close the dialog and do something related.
     */
  close () {
    this.showNextDayIcon = false
    this.repeat = ''
    this.repeatIndex = []
    this.isShowManualPopUp = false
    this.chooseTimeIndex = -1
    this.ContentHasChanged = false
    this.curQuality = ''
    this.qualityList = []
  }

    /**
     * get the current date and time.
     */
  getCurrentTimeInfo () {
    this.currentDate = Date.now()
    this.curYear = Date.now()['getFullYear']()
    this.curMonth = Date.now()['getMonth']() + 1
    this.curDay = Date.now()['getDate']()
    this.curHour = Date.now()['getHours']()
    this.curMin = Date.now()['getMinutes']()
    this.curSec = Date.now()['getSeconds']()
  }

    /**
     * check whether user has changed recording settings.
     */
  checkContentChange () {
    if (this.recordName !== this.recordDetails['name']) {
      return true
    }
    if (this.isSingle === true) {
      if (this.saveSingleDate !== this.initSingleDate) {
        return true
      }
    } else {
      if (this.repeatIndex.toString() !== this.initRepeatIndex.toString()) {
        return true
      }
      if (this.saveStartDate !== this.initStartDate) {
        return true
      }
      if (this.endDate !== this.initEndDate) {
        return true
      }
    }
    if (this.startHour !== this.initStartHour) {
      return true
    }
    if (this.startMin !== this.initStartMin) {
      return true
    }
    if (this.endHour !== this.initEndHour) {
      return true
    }
    if (this.endMin !== this.initEndMin) {
      return true
    }
    if (this.storageType === 'CPVR' && this.curQuality !== this.initQuality) {
      return true
    }
    if (this.canChooseStorage && this.storageType !== this.initStorageType) {
      return true
    }
  }

    /**
     * get the formarted date string.
     * @param {String} data [date]
     */
  getDateStr (data: string) {
    return data.substring(6, 8) + '.' + data.substring(4, 6) + '.' + data.substring(0, 4)
  }

    /**
     * if the string has only one letter, we should add '0' in the front of the string.
     * @param {String} date [date]
     */
  addZero (date) {
    if (date.length === ONE) {
      date = '0' + date
    }
    return date
  }

    /**
     * when the mouse focuses on the recording name, we need do something.
     * for example:
     * 1. we need to save the recording name temporarily.
     * 2. we need to hide the quality list.
     */
  nameFocus (e) {
    this.focusRecName = e.target.value
    this.hideQualityList()
  }

    /**
     * when the mouse lose focus on the recording name, we also need do something.
     * for example:
     * 1. we need to check user whether has changed the recording name.
     */
  nameBlur (e) {
    if (e.target.value !== this.focusRecName) {
      this.canAutoChangeName = false
    }
  }

    /**
     * when the user is inputing recording name, we need do something related.
     */
  nameKeyup (e) {
    let value = e.target.value
    if (value.trim() === '') {
      value = ''
    }
    this.recordName = value
    if (this.recordName === '') {
      this.isNameEmpty = true
    } else {
      this.isNameEmpty = false
    }
    this.ContentHasChanged = this.checkContentChange()
  }

    /**
     * blank space is not allowed in the head and the tail of input.
     */
  onPressChange (e) {
    let caretPos = 0
    let ctrl = e.currentTarget
    if (document['selection']) {
      ctrl.focus()
      let sel = document['selection'].createRange()
      sel.moveStart('character', -ctrl['value'].length)
      caretPos = sel.text.length
    } else if (ctrl['selectionStart'] || ctrl['selectionStart'] === '0') {
      caretPos = ctrl['selectionStart']
    }
    if (e.keyCode === SPACE_KEY) {
      if (caretPos === 0) {
        e.returnValue = false
      }
    }
  }

    /**
     * when the user press the button, he can input name right now.
     */
  editRecName () {
    if (this.dom('nameRefs')) {
      this.dom('nameRefs').focus()
    }
  }

    /**
     * get the current quality.
     * @param {Object} data [record details]
     */
  getCurrQuality (data) {
    let userType
    if (Cookies.getJSON('IS_PROFILE_LOGIN')) {
      userType = Cookies.getJSON('USER_ID')
    } else {
      userType = session.get('Guest')
    }
    let staticData = session.get(userType + '_Static_Version_Data')
    let channel = _.find(staticData['channelDetails'], item => {
      return item['ID'] === data['channelID']
    })
    let physical = _.find(channel['physicalChannels'], item => {
      return item['ID'] === data['cpvrMediaID']
    })
    this.orginalDefinition = physical && physical['definition']
    switch (this.orginalDefinition) {
      case '0':
        this.curQuality = 'SD'
        break
      case '1':
        this.curQuality = 'HD'
        break
      case '2':
        this.curQuality = '4K'
        break
      default:
        this.curQuality = 'HD'
    }
    this.initQuality = this.curQuality
  }

    /**
     * get the supported quality list.
     * @param {Object} data [record details]
     */
  getQualityList (data) {
    let self = this
    let channel = _.find(self.cPVRChannelCache, item => {
      return item['ID'] === data['channelID']
    })
    let qualityList = []
    let qualityObj = {}
    let dynamicProperty = this.commonService.judgeRecordDynamic('cpvrRecordCR', channel)
    let physicalChannels = _.filter(channel['physicalChannels'], item => {
      let findPhysical = _.find(dynamicProperty, itemDynamic => {
        return itemDynamic['ID'] === item['ID']
      })
      if (findPhysical) {
        return true
      }
    })
    if (physicalChannels.length > 0) {
      _.map(physicalChannels, item => {
        if (!_.isUndefined(item['definition'])) {
            if (Number(item['definition']) === 0) {
              qualityObj = {
                'quality': 'SD',
                'qualityKey': 0,
                'cpvrMediaID': item['ID']
              }
            } else if (Number(item['definition']) === ONE) {
              qualityObj = {
                'quality': 'HD',
                'qualityKey': 1,
                'cpvrMediaID': item['ID']
              }
            } else if (Number(item['definition']) === TWO) {
              qualityObj = {
                'quality': '4K',
                'qualityKey': 2,
                'cpvrMediaID': item['ID']
              }
            }
          }
        if (qualityList.length > 0) {
            let existed = _.find(qualityList, function (ql) {
              return ql['quality'] === qualityObj['quality']
            })
            if (_.isUndefined(existed)) {
              qualityList.push(qualityObj)
            }
          } else {
            qualityList.push(qualityObj)
          }
      })
      if (this.storageType === 'CPVR') {
        let supportCurQuality = _.find(qualityList, item => {
            return item['cpvrMediaID'] === self.recordDetails['cpvrMediaID']
          })
        if (!supportCurQuality) {
            let curQualityObj
            if (self.recordDetails['policyType'] === 'Period') {
              curQualityObj = {
                'quality': self.curQuality,
                'qualityKey': Number(self.orginalDefinition),
                'cpvrMediaID': self.recordDetails['cpvrMediaID'] || ''
              }
            } else {
              curQualityObj = {
                'quality': self.curQuality,
                'qualityKey': Number(self.recordDetails['definition'][0]),
                'cpvrMediaID': self.recordDetails['cpvrMediaID'] || ''
              }
            }
            qualityList.push(curQualityObj)
          }
      }
    }
    qualityList = _.sortBy(qualityList, ql => {
      return ql.qualityKey
    })
    this.qualityList = qualityList
  }

    /**
     * get the configuration of the default quality.
     */
  getDefaultQuality () {
    this.defaultQuality = this.configData['default_reccfg_definition']
    switch (this.defaultQuality) {
      case 'SD':
        this.defaultQualityKey = 0
        break
      case 'HD':
        this.defaultQualityKey = 1
        break
      case '4K':
        this.defaultQualityKey = 2
        break
    }
  }

    /**
     * the principle of the quality up close to.
     * @param {Object} qualityList [list of quality]
     * @param {Number} defQualityKey [key of default quality]
     */
  findUpNearQuality (qualityList, defQualityKey) {
    let curQualityObj = {}
    let findQualityObj = _.find(qualityList, item => {
      return item['qualityKey'] === defQualityKey
    })
    if (!_.isUndefined(findQualityObj)) {
      curQualityObj = findQualityObj
    } else {
      let higherQualityObj = _.find(qualityList, item => {
        return item['qualityKey'] > defQualityKey
      })
      if (!_.isUndefined(higherQualityObj)) {
        curQualityObj = higherQualityObj
      } else {
        curQualityObj = _.max(qualityList, item => {
            return item['qualityKey']
          })
      }
    }
    return curQualityObj
  }

    /**
     * show the choice list of record quality.
     */
  choiceQuality () {
    this.showQualityList = !this.showQualityList
  }

    /**
     * when the mouse leave the quality list, the list disappeared.
     */
  mouseOutQualityList () {
    this.showQualityList = false
  }

    /**
     * select the record quality.
     * @param {Object} qualityObj [object of quality]
     */
  selectQuality (qualityObj) {
    this.curQuality = qualityObj['quality']
    this.cpvrMediaID = qualityObj['cpvrMediaID']
    this.showQualityList = false
    this.ContentHasChanged = this.checkContentChange()
  }

    /**
     * if click the other location,hidden the list of record quality.
     */
  hideQualityList () {
    if (this.showQualityList === true) {
      this.showQualityList = false
    }
  }

    /**
     * choose the repeat date of period record.
     */
  choiceRepeatDate () {
    this.recordRepeatData.isShowRepeatData = true
    this.recordRepeatData.loadDate(this.repeat, this.repeatIndex)
  }

    /**
     * change the repeat setting.
     */
  changeRepeat (data) {
    this.repeatIndex = data['indexArray']
    this.repeat = ''
    if (data['indexArray'].length === SEVEN) {
      this.repeat = this.translate.instant('record_daily')
    } else {
      _.map(data['choseDate'], (item, index) => {
        if (index === data['choseDate'].length - 1) {
            this.repeat = this.repeat + item
          } else {
            this.repeat = this.repeat + item + ' '
          }
      })
    }
    this.ContentHasChanged = this.checkContentChange()
  }

    /**
     * choose the single record date.
     */
  choiceManualDate () {
    this.recordDataName.isShowDataName = true
    this.saveSingleDate = this.commonService.dealDateToNumber(this.singleDate)
    this.recordDataName.setChooseDate(this.saveSingleDate)
  }

    /**
     * choose the start date of single task.
     */
  changeSingleDate (data) {
    this.singleDate = data
    this.saveSingleDate = this.commonService.dealDateToNumber(this.singleDate)
    this.ContentHasChanged = this.checkContentChange()
    this.autoSetNameByTime()
  }

    /**
     * chooset the start date of period task.
     */
  chooseStartTime () {
    this.saveStartDate = this.commonService.dealDateToNumber(this.startDate)
    if (this.endDate !== 'unlimited' && this.endDate !== 'seven_days_later' &&
            this.endDate !== 'fourteen_days_later' && this.endDate !== 'thirty_days_later') {
      this.saveEndDate = this.commonService.dealDateToNumber(this.endDate)
    } else {
      this.saveEndDate = this.endDate
    }
    if (this.status === 'WAIT_RECORD') {
      this.recordStartTime.isShowStartTime = true
      this.recordStartTime.loadDateData('start', this.saveStartDate, this.saveEndDate)
      this.hideQualityList()
    }
  }

    /**
     * chooset the end date of period task.
     */
  chooseEndTime () {
    this.saveStartDate = this.commonService.dealDateToNumber(this.startDate)
    if (this.endDate !== 'unlimited' && this.endDate !== 'seven_days_later' &&
            this.endDate !== 'fourteen_days_later' && this.endDate !== 'thirty_days_later') {
      this.saveEndDate = this.commonService.dealDateToNumber(this.endDate)
    } else {
      this.saveEndDate = this.endDate
    }
    this.recordStartTime.isShowEndTime = true
    this.recordStartTime.loadDateData('end', this.saveStartDate, this.saveEndDate)
    this.hideQualityList()
  }

    /**
     * change the period date setting.
     */
  changePeriodDate (data) {
    if (data['types'] === 'start') {
      this.startDate = data['chooseTimeDate']
      this.getShowStartDate()
    } else {
      this.endDate = data['chooseTimeDate']
      this.getShowEndDate()
    }
    this.saveStartDate = this.commonService.dealDateToNumber(this.startDate)
    if (this.endDate !== 'unlimited' && this.endDate !== 'seven_days_later' &&
            this.endDate !== 'fourteen_days_later' && this.endDate !== 'thirty_days_later') {
      this.saveEndDate = this.commonService.dealDateToNumber(this.endDate)
    } else {
      this.saveEndDate = this.endDate
    }
    this.ContentHasChanged = this.checkContentChange()
    let dateIllegalFlag = this.manualService.checkDateInvalidType(this.saveStartDate, this.saveEndDate)
    if (dateIllegalFlag !== '') {
      if (dateIllegalFlag === 'Invalid') {
        EventService.emit('END_DATA_MUST_LATER')
      }
      this.autoChangeNextDay()
    }
  }

    /**
     * when the end-time is earlier than the start-time, we should set the end-date as the next day of the start-date.
     */
  autoChangeNextDay () {
    this.saveStartDate = this.commonService.dealDateToNumber(this.startDate)
    this.saveEndDate = this.manualService.getExpireDateByInterval(this.saveStartDate, 1, 'DD.MM.YYYY')
    this.endDate = this.commonService.dealWithOrgTime(this.saveEndDate)
    this.getShowEndDate()
  }

    /**
     * focus on the time input.
     * @param {Number} i [index of choose time]
     */
  chooseUpdateTimes (i) {
    if (!(this.isSingle && this.status === 'RECORDING' && (i === ONE || i === TWO))) {
      this.chooseTimeIndex = i
      this.hideQualityList()
    }
  }

    /**
     * handle the key-up event.
     */
  KeyEventHandle (e) {
    if (e.target.id === 'start-hours' || e.target.id === 'end-hours') {
      if (e.target.value > 23) {
        e.target.value = 23
      }
    } else if (e.target.id === 'start-mins' || e.target.id === 'end-mins') {
      if (e.target.value > 59) {
          e.target.value = 59
        }
    }
    if (e.keyCode === UP_KEY) {
      this.upKeyHandle(e)
    } else if (e.keyCode === DOWN_KEY) {
      this.downKeyHandle(e)
    } else if (e.keyCode !== LEFT_KEY && e.keyCode !== RIGHT_KEY) {
        e.target.value = e.target.value.replace(/\D/g, '')
      }
    if (e.keyCode !== UP_KEY && e.keyCode !== DOWN_KEY) {
      this.assignValue(e)
    }
    this.autoSetNameByTime()
    this.checkIsNextDay()
    this.ContentHasChanged = this.checkContentChange()
  }

    /**
     * if we pressd the up key, we should make the time increase automatically.
     */
  upKeyHandle (e) {
    if (e.target.id === 'start-hours' || e.target.id === 'start-mins') {
      this.increaseStart()
    }
    if (e.target.id === 'end-hours' || e.target.id === 'end-mins') {
      this.increaseEnd()
    }
  }

    /**
     * if we pressd the down key, we should make the time decrease automatically.
     */
  downKeyHandle (e) {
    if (e.target.id === 'start-hours' || e.target.id === 'start-mins') {
      this.decreaseStart()
    }
    if (e.target.id === 'end-hours' || e.target.id === 'end-mins') {
      this.decreaseEnd()
    }
  }

    /**
     * we should assign value to one of the time according to our select.
     */
  assignValue (e) {
    switch (e.target.id) {
      case 'start-hours':
        this.startHour = e.target.value
        break
      case 'start-mins':
        this.startMin = e.target.value
        break
      case 'end-hours':
        this.endHour = e.target.value
        break
      case 'end-mins':
        this.endMin = e.target.value
        break
      default:
        break
    }
  }

    /**
     * increase the start time.
     */
  increaseStart () {
    if (this.chooseTimeIndex === -1) {
      return
    }
    if (this.chooseTimeIndex === ONE) {
      this.startHour = this.addZero((Number(this.startHour) + 1) + '')
      if (Number(this.startHour) < 0 || Number(this.startHour) >= 24) {
        this.startHour = '00'
      }
    }
    if (this.chooseTimeIndex === TWO) {
      this.startMin = this.addZero((Number(this.startMin) + 1) + '')
      if (Number(this.startMin) < 0 || Number(this.startMin) >= 60) {
        this.startMin = '00'
      }
    }
    this.autoSetNameByTime()
    this.checkIsNextDay()
    this.ContentHasChanged = this.checkContentChange()
  }

    /**
     * decrease the start time.
     */
  decreaseStart () {
    if (this.chooseTimeIndex === -1) {
      return
    }
    if (this.chooseTimeIndex === ONE) {
      this.startHour = this.addZero((Number(this.startHour) - 1) + '')
      if (Number(this.startHour) < 0 || Number(this.startHour) >= 24) {
        this.startHour = '23'
      }
    }
    if (this.chooseTimeIndex === TWO) {
      this.startMin = this.addZero((Number(this.startMin) - 1) + '')
      if (Number(this.startMin) < 0 || Number(this.startMin) >= 60) {
        this.startMin = '59'
      }
    }
    this.autoSetNameByTime()
    this.checkIsNextDay()
    this.ContentHasChanged = this.checkContentChange()
  }

    /**
     * increase the end time.
     */
  increaseEnd () {
    if (this.chooseTimeIndex === -1) {
      return
    }
    if (this.chooseTimeIndex === THREE) {
      this.endHour = this.addZero((Number(this.endHour) + 1) + '')
      if (Number(this.endHour) < 0 || Number(this.endHour) >= 24) {
        this.endHour = '00'
      }
    }
    if (this.chooseTimeIndex === FOUR) {
      this.endMin = this.addZero((Number(this.endMin) + 1) + '')
      if (Number(this.endMin) < 0 || Number(this.endMin) >= 60) {
        this.endMin = '00'
      }
    }
    this.autoSetNameByTime()
    this.checkIsNextDay()
    this.ContentHasChanged = this.checkContentChange()
  }

    /**
     * decrease the end time.
     */
  decreaseEnd () {
    if (this.chooseTimeIndex === -1) {
      return
    }
    if (this.chooseTimeIndex === THREE) {
      this.endHour = this.addZero((Number(this.endHour) - 1) + '')
      if (Number(this.endHour) < 0 || Number(this.endHour) >= 24) {
        this.endHour = '23'
      }
    }
    if (this.chooseTimeIndex === FOUR) {
      this.endMin = this.addZero((Number(this.endMin) - 1) + '')
      if (Number(this.endMin) < 0 || Number(this.endMin) >= 60) {
        this.endMin = '59'
      }
    }
    this.autoSetNameByTime()
    this.checkIsNextDay()
    this.ContentHasChanged = this.checkContentChange()
  }

    /**
     * check whether the next-day icon should be visible.
     */
  checkIsNextDay () {
    if (Number(this.endHour) < Number(this.startHour)) {
      this.showNextDayIcon = true
    } else if (Number(this.endHour) === Number(this.startHour) && Number(this.endMin) <= Number(this.startMin)) {
      this.showNextDayIcon = true
    } else {
      this.showNextDayIcon = false
    }
    this.hideQualityList()
  }

    /**
     * when the time input loses the focus, we should format the input automatically.
     */
  timeBlur (e) {
    if (this.isEnterChangeButton) {
      return
    }
    this.chooseTimeIndex = -1
    e.target.value = this.addZero(e.target.value)
  }

    /**
     * check whether the setting of single recording task is changed.
     * @param {Object} checkData [object of check data]
     * @param {Object} inputData [object of input data]
     */
  checkSingleChange (checkData, inputData) {
    let outputData = {}
    if (checkData['name'] !== inputData['name']) {
      outputData['name'] = checkData['name']
    }
    if (Number(checkData['startTime']) !== Number(inputData['startTime'])) {
      outputData['startTime'] = checkData['startTime']
    }
    if (Number(checkData['endTime']) !== Number(inputData['endTime'])) {
      outputData['endTime'] = checkData['endTime']
    }
    if (checkData['storageType'] !== inputData['storageType']) {
      outputData['storageType'] = checkData['storageType']
    }
    if (checkData['storageType'] === 'CPVR') {
      if (inputData['storageType'] === 'NPVR') {
        outputData['cpvrMediaID'] = checkData['cpvrMediaID']
      } else if (checkData['cpvrMediaID'] !== inputData['cpvrMediaID']) {
          outputData['cpvrMediaID'] = checkData['cpvrMediaID']
        }
    }
    return outputData
  }

    /**
     * check whether the setting of period recording task is changed.
     * @param {Object} checkData [object of check data]
     * @param {Object} inputData [object of input data]
     */
  checkPeriodChange (checkData, inputData) {
    let outputData = {}
    if (checkData['name'] !== inputData['name']) {
      outputData['name'] = checkData['name']
    }
    if (!_.isEqual(checkData['days'], inputData['days'])) {
      outputData['days'] = checkData['days']
    }
    if (checkData['beginDayTime'] !== inputData['beginDayTime']) {
      outputData['beginDayTime'] = checkData['beginDayTime']
    }
    if (checkData['endDayTime'] !== inputData['endDayTime']) {
      outputData['endDayTime'] = checkData['endDayTime']
    }
    if (checkData['effectiveDate'] !== inputData['effectiveDate']) {
      outputData['effectiveDate'] = checkData['effectiveDate']
    }
    if (checkData['expireDate'] !== inputData['expireDate']) {
      outputData['expireDate'] = checkData['expireDate']
    }
    if (checkData['storageType'] !== inputData['storageType']) {
      outputData['storageType'] = checkData['storageType']
    }
    this.getCPVRMediaID(checkData, inputData, outputData)
    return outputData
  }

    /**
     * get media id of CPVR
     * @param {Object} checkData [object of check data]
     * @param {Object} inputData [object of input data]
     * @param {Object} outputData [object of output data]
     */
  getCPVRMediaID (checkData, inputData, outputData) {
    if (checkData['storageType'] === 'CPVR') {
      if (inputData['storageType'] === 'NPVR') {
        outputData['cpvrMediaID'] = checkData['cpvrMediaID']
      } else if (checkData['cpvrMediaID'] !== inputData['cpvrMediaID']) {
          outputData['cpvrMediaID'] = checkData['cpvrMediaID']
        }
    }
  }

    /**
     * check whether the recording name conform the rules.
     */
  checkNameInputRule () {
    let returnValue = ''
    if (this.recordName.length > this.maxLengthCache) {
      returnValue = this.translate.instant('record_name_maxLength', { common: this.maxLengthCache })
      return returnValue
    }
    if (this.blackListCache !== '') {
      if (!(/^[A-Z|a-z|0-9]+$/.test(this.recordName))) {
        let numLetters = this.recordName.replace(/[A-Z|a-z|0-9]/ig, '')
        let numLettersArr = numLetters.split('')
        if (numLettersArr.length !== 0) {
            let hasLettersOfBlackList = false
            nameOuter: for (let i = 0; i < numLettersArr.length; i++) {
              for (let j = 0; j < this.blackListCache.length; j++) {
                if (numLettersArr[i] === this.blackListCache.charAt(j)) {
                  hasLettersOfBlackList = true
                  break nameOuter
                }
              }
            }
            if (hasLettersOfBlackList) {
              returnValue = this.translate.instant('record_name_blacklist', { special_char: this.blackListCache })
              return returnValue
            }
          }
      }
    }
    return returnValue
  }

    /**
     * press the button, we will update the recording task.
     */
  confirm (event?) {
    if (this.isNameEmpty) {
      return
    }
    let checkMsg = this.checkNameInputRule()
    if (checkMsg !== '') {
      EventService.emit('RECORD_NAME_INPUT_INCORRECT', checkMsg)
      return
    }
    if (this.startHour === '' || this.startMin === '' || this.endHour === '' || this.endMin === '') {
      EventService.emit('TIME_INPUT_EMPTY')
      return
    }
    EventService.emit('CHANGE_CLOUD_ICON', { ID: this.pvrId, chooseStorage: this.chooseStorage })
    if (this.ContentHasChanged === true) {
      if (this.isSingle === true) {
        this.timebasedRecord()
      } else {
        if (event) {
            this.periodRecord(event)
          } else {
            this.periodRecord()
          }
      }
    }
  }

    /**
     * check whether the start time is five minutes earlier than the current time.
     */
  checkTimeIsCorrect () {
    this.getCurrentTimeInfo()
    let nowTime = this.currentDate['getTime']()
    let startTime = new Date(this.curYear, this.curMonth - 1, this.curDay,
        Number(this.startHour), Number(this.startMin), this.curSec).getTime()
    if (nowTime - startTime > 5 * 60 * 1000) {
      return true
    }
  }

    /**
     * update the time-based recording task.
     */
  timebasedRecord () {
    let self = this
    let isDSTSpecialScene = false
    let inputData, totalData, parameter, type, checkData, outputData
    inputData = _.pick(self.recordDetails, 'name', 'startTime', 'endTime', 'cpvrMediaID', 'storageType')
    let timeInt = this.manualService.dateStrToArray(this.saveSingleDate)
    let startDateObj = new Date(timeInt[2], timeInt[1] - 1, timeInt[0],
        Number(this.startHour), Number(this.startMin), Number(this.startSec), Number(this.startMill))
    let startTime = startDateObj.getTime()
    let startResp = this.manualService.judgeDSTDate(startDateObj, 'start', this.addZero(this.startHour))
    if (startResp['type'] === '2') {
      EventService.emit('DST_JUMPING_TOAST')
      return
    } else if (startResp['type'] === '3') {
      startTime = startResp['correctMSeconds']
      let currDate = new Date()
      let currDateType = this.manualService.judgeDSTDate(currDate, 'start')
      if (startResp['correctMSeconds'] > currDate.getTime() && currDateType === '3') {
          isDSTSpecialScene = true
        }
    }
    let illegalFlag = this.recordStartTimeIllegal(isDSTSpecialScene)
    if (illegalFlag) {
      return
    }
    let endDateObj = self.getEndDateObj(timeInt)
    let endTime = endDateObj.getTime()
    let endResp = this.manualService.judgeDSTDate(endDateObj, 'end', this.addZero(this.endHour))
    if (endResp['type'] === '2') {
      EventService.emit('DST_JUMPING_TOAST')
      return
    } else if (endResp['type'] === '3') {
      endTime = endResp['correctMSeconds']
    }
    totalData = {
      ID: this.pvrId,
      name: this.recordName,
      channelID: this.channelID,
      storageType: this.storageType,
      policyType: 'TimeBased',
      startTime: startTime + '',
      endTime: endTime + ''
    }
    self.getCPVRediaID(totalData)
    checkData = _.pick(totalData, 'name', 'startTime', 'endTime', 'cpvrMediaID', 'storageType')
    outputData = self.checkSingleChange(checkData, inputData)
    outputData['ID'] = self.recordDetails['ID']
    parameter = outputData
    type = 'TimeBasedPVR'
    this.recordDataService.updatePVR(parameter, type).then(resp => {
      if (resp['isNPVRSpaceEnough'] && resp['isNPVRSpaceEnough'] === '0') {
        EventService.emit('RECORD_SPACE_NOT_ENOUGH', { type: 'successful' })
      }
      outputData['channelID'] = self.recordDetails['channelID']
      EventService.emit('MANUAL_SET_SUCCESS', outputData)
      if (_.isUndefined(parameter['cpvrMediaID'])) {
        EventService.emit('DELETESPACE', 'querypvrspace7')
      }
      self.close()
    }, resp => {
      self.close()
      let conflictData = {}
      if (resp['result']['retCode'] === '147020005') {
        EventService.emit('RECORD_SPACE_NOT_ENOUGH', { type: 'failed', source: 'manualUpdate' })
      } else if (resp.conflictGroups && resp.conflictGroups !== '') {
          conflictData = { resp: resp, data: parameter, type: type, update: true }
          EventService.emit('RECORD_CONFLICT', conflictData)
        }
    })
  }

    /**
     * get end date of record
     * @param {Array} timeInt [a array of record start time]
     */
  getEndDateObj (timeInt) {
    let endDateObj
    if (this.showNextDayIcon === true && (this.status === 'RECORDING' || this.status === 'WAIT_RECORD')) {
      let startDay = new Date(timeInt[2], timeInt[1] - 1, timeInt[0])
      let nextDay = new Date(startDay.setDate(startDay.getDate() + 1))
      endDateObj = new Date(nextDay.getFullYear(), nextDay.getMonth(), nextDay.getDate(), Number(this.endHour), Number(this.endMin))
    } else {
      endDateObj = new Date(timeInt[2], timeInt[1] - 1, timeInt[0],
          Number(this.endHour), Number(this.endMin), Number(this.endSec), Number(this.endMill))
    }
    return endDateObj
  }

    /**
     * get media id of CPVR
     * @param {Object} totalData [information of record]
     */
  getCPVRediaID (totalData) {
    if (this.storageType === 'CPVR' && (this.status === 'RECORDING' || this.status === 'WAIT_RECORD')) {
      totalData['cpvrMediaID'] = this.cpvrMediaID
    }
  }

    /**
     * check whether the start time is illegal.
     * @param {Boolean} isDSTSpecialScene
     */
  recordStartTimeIllegal (isDSTSpecialScene) {
    let flag = false
    if (!isDSTSpecialScene && this.status === 'WAIT_RECORD') {
      let currDateStr = DateUtils.format(Date.now()['getTime'](), 'DD.MM.YYYY')
      if (this.singleDate === currDateStr) {
        let timeIllegalFlag = this.checkTimeIsCorrect()
        if (timeIllegalFlag === true) {
            EventService.emit('START_TIME_SHOULD_BE_LATER')
            flag = true
          }
      }
    }
    return flag
  }

    /**
     * update the period recording task.
     */
  periodRecord (event?) {
    let self = this
    this.saveStartDate = this.commonService.dealDateToNumber(this.startDate)
    let dateLegal = this.manualService.isRepeatAndDateLegal(this.repeat, this.repeatIndex, this.saveStartDate, this.saveEndDate)
    if (dateLegal === false) {
      EventService.emit('REPEAT_AND_DATE_ILLEGAL')
      return
    }
    let inputData, totalData, parameter, type, checkData, outputData
    inputData = _.pick(self.recordDetails, 'name', 'days', 'beginDayTime', 'endDayTime',
        'effectiveDate', 'expireDate', 'cpvrMediaID', 'storageType')
    let effectArr = this.saveStartDate.split('.')
    let effectiveDate = effectArr[2] + effectArr[1] + effectArr[0]
    let expireDate
    if (this.endDate === 'unlimited') {
      expireDate = '20991231'
    } else if (this.endDate === 'seven_days_later') {
      expireDate = this.manualService.getExpireDateByInterval(this.saveStartDate, 7, 'YYYYMMDD')
    } else if (this.endDate === 'fourteen_days_later') {
        expireDate = this.manualService.getExpireDateByInterval(this.saveStartDate, 14, 'YYYYMMDD')
      } else if (this.endDate === 'thirty_days_later') {
        expireDate = this.manualService.getExpireDateByInterval(this.saveStartDate, 30, 'YYYYMMDD')
      } else {
        let expireArr = this.saveEndDate.split('.')
        expireDate = expireArr[2] + expireArr[1] + expireArr[0]
      }
    let stringDays = _.map(this.repeatIndex, day => {
      return day + ''
    })
    totalData = {
      ID: this.pvrId,
      name: this.recordName,
      days: stringDays,
      beginDayTime: this.startHour + this.startMin + this.recordDetails['beginDayTime'].substring(4, 6),
      endDayTime: this.endHour + this.endMin + this.recordDetails['endDayTime'].substring(4, 6),
      effectiveDate: effectiveDate,
      expireDate: expireDate,
      channelID: this.channelID,
      storageType: this.storageType,
      policyType: 'Period'
    }
    if (this.storageType === 'CPVR') {
      totalData['cpvrMediaID'] = this.cpvrMediaID
    }
    checkData = _.pick(totalData, 'name', 'days', 'beginDayTime', 'endDayTime',
        'effectiveDate', 'expireDate', 'cpvrMediaID', 'storageType')
    outputData = self.checkPeriodChange(checkData, inputData)
    outputData['ID'] = self.recordDetails['ID']
    parameter = outputData
    type = 'PeriodPVR'
    self.recordDataService.updatePeriodicPVR(parameter, type, event).then(resp => {
      if (resp['isNPVRSpaceEnough'] && resp['isNPVRSpaceEnough'] === '0') {
        EventService.emit('RECORD_SPACE_NOT_ENOUGH', { type: 'successful' })
      }
      outputData['channelID'] = self.recordDetails['channelID']
      EventService.emit('MANUAL_PERIOD_SET_SUCCESS', outputData)
      if (_.isUndefined(parameter['cpvrMediaID'])) {
        EventService.emit('DELETESPACE', 'querypvrspace8')
      }
      self.close()
    }, resp => {
      self.close()
      let conflictData = { resp: resp, data: parameter, type: type, update: true }
      if (resp['result']['retCode'] === '147020005') {
        EventService.emit('RECORD_SPACE_NOT_ENOUGH', { type: 'failed', source: 'manualUpdate' })
      } else if (resp.conflictGroups && resp.conflictGroups !== '') {
          if (resp['conflictGroups'][0]['PVRList'][0]['policyType'] === 'PlaybillBased' ||
                    resp['conflictGroups'][0]['PVRList'][0]['policyType'] === 'TimeBased') {
            EventService.emit('MIXED_CONFLICT', conflictData)
          } else {
            EventService.emit('RECORD_CONFLICT', conflictData)
          }
        }
    })
  }

    /**
     * return the dom element.
     * @param {String} divName [id of div]
     */
  dom (divName: string) {
    return document.getElementById(divName)
  }

    /**
     * mouseenter event about time-change button.
     */
  enterChangeButton (e) {
    this.isEnterChangeButton = true
  }

    /**
     * mouseleave event about time-change button.
     */
  outChangeButton (e) {
    this.isEnterChangeButton = false
    if (this.chooseTimeIndex === ONE) {
      this.dom('start-hours').focus()
    }
    if (this.chooseTimeIndex === TWO) {
      this.dom('start-mins').focus()
    }
    if (this.chooseTimeIndex === THREE) {
      this.dom('end-hours').focus()
    }
    if (this.chooseTimeIndex === FOUR) {
      this.dom('end-mins').focus()
    }
  }
}
