import * as _ from 'underscore'
import { Component, Output, EventEmitter } from '@angular/core'
import { session } from 'src/app/shared/services/session'
import { DirectionScroll } from 'ng-epg-ui/webtv-components/scroll/directionScroll.component'
import { CommonService } from '../../../../core/common.service'

const ONE = 1

@Component({
  selector: 'app-record-data-name',
  styleUrls: ['./recordDataName.component.scss'],
  templateUrl: './recordDataName.component.html'
})

export class RecordDataNameComponent {
  @Output() selectSingleDate: EventEmitter<Object> = new EventEmitter()
  public dataDates = []
  public dataStrs = []
  public isChooseIndex = '0'
  public isShowDataName = false
  private onePageNum = 8
  constructor (
        private directionScroll: DirectionScroll,
        private commonService: CommonService
    ) { }
    /**
     * get current time
     */
  getCurrentTime () {
    let curTime = Date.now()['getTime']()
    let dstExtraTime = 0
    let nowDate = new Date(curTime).getDate()
    let nextDate = new Date(curTime + 86400000).getDate()
    if (nowDate === nextDate) {
      dstExtraTime = 60 * 60 * 1000
    }
    let length
    let playbillLen = session.get('PLAYBILL_LEN')
    if (!_.isUndefined(playbillLen)) {
      length = Number(playbillLen)
    } else {
      length = 7
    }
      // set time format
    for (let i = 0; i <= length; i++) {
      let dateObj = new Date(curTime + 86400000 * i + dstExtraTime)
      let month = dateObj.getMonth() + 1 + ''
      if (month.length === ONE) {
        month = '0' + month
      }
      let day = dateObj.getDate() + ''
      let year = dateObj.getFullYear() + ''
      let lastTime
      month = this.commonService.dealWithTime(month)
      if (session.get('languageName') === 'zh') {
        lastTime = year + '/' + month + '/' + day
      } else {
        lastTime = month + ' ' + day + ', ' + year
      }
      let curDateStr = dateObj.getTime()
      curDateStr = this.commonService.dealWithDateJson('D02', curDateStr)
      let whiteDateStr = [curDateStr]
      let dataDate = [lastTime]

      this.dataStrs[i] = [whiteDateStr, dataDate]
      this.dataDates.push(lastTime)
    }
  }

    /**
     * close the page
     */
  close () {
    this.isShowDataName = false
  }
    /**
     * choose date
     */
  chooseData (dataDate, i) {
    this.selectSingleDate.emit(dataDate)
    this.isChooseIndex = i
    this.close()
  }
    /**
     * set the selected date
     */
  setChooseDate (date) {
    this.dataDates = []
    this.getCurrentTime()
    date = date.split('.')
    date[1] = this.commonService.dealWithTime(date[1])
    if (date[0].charAt(0) === '0') {
      date[0] = date[0].charAt(1)
    }
    let dateFirst
    let dateSecond
    if (session.get('languageName') === 'zh') {
      dateFirst = date[2] + '/' + date[1] + '/' + date[0]
    } else {
      dateSecond = date[1] + ' ' + date[0] + ', ' + date[2]
    }
    this.isChooseIndex = '-1'
    _.map(this.dataDates, (item, index) => {
      if (dateFirst === item || dateSecond === item) {
        this.isChooseIndex = index + ''
      }
    })
    if (this.dataDates.length > this.onePageNum) {
      _.delay(() => {
        this.pluseOnScroll()
      }, 100)
    }
  }
    /**
     * Scroll
     */
  pluseOnScroll () {
    let oBox, oConter, oUl, oScroll, oSpan
    oBox = this.dom('dataName')
    oConter = this.dom('datawrap')
    oUl = this.dom('dataNamePanel')
    oScroll = this.dom('singleTime-scroll')
    if (oScroll && oScroll.getElementsByTagName('span')) {
      oSpan = oScroll.getElementsByTagName('span')[0]
    }
    this.directionScroll.setScroll(oBox, oConter, oUl, oScroll, oSpan)
  }

    /**
     * return the dom element.
     */
  dom (divName: string) {
    return document.getElementById(divName)
  }
}
