import * as _ from 'underscore'
import { TranslateService } from '@ngx-translate/core'
import { DateUtils } from 'ng-epg-sdk/utils'
import { Injectable } from '@angular/core'
import { CommonService } from '../../../core/common.service'

const ONE = 1, TWO = 2

@Injectable()
export class ManualService {
  constructor (
        private translate: TranslateService,
        private commonService: CommonService
  ) { }

  /**
     * the date is divided to array.
     * @param {String} dateStr [date]
     * @return {Object}
     */
  dateStrToArray (dateStr: string) {
    let start = dateStr.replace(',', '').split('.')
    let startInt = []
    _.map(start, (item, index) => {
      startInt[index] = Number(item)
    })
    return startInt
  }

  /**
     * check the date whether is comply with the rules.
     * @param {String} startDate [start date]
     * @param {String} endDate [end date]
     * @return {String}
     */
  checkDateInvalidType (startDate, endDate) {
    let invalidType = ''
    if (endDate !== 'unlimited' && endDate !== 'seven_days_later' && endDate !== 'thirty_days_later') {
      let startArray = this.dateStrToArray(startDate)
      let endArray = this.dateStrToArray(endDate)
      let DateA = new Date(startArray[2], startArray[1] - 1, startArray[0])
      let DateB = new Date(endArray[2], endArray[1] - 1, endArray[0])
      if (DateA.getTime() >= DateB.getTime()) {
        invalidType = 'Invalid'
      }
      if ((DateB.getTime() - DateA.getTime()) > 30 * 86400000) {
        invalidType = 'Inexistent'
      }
    }
    return invalidType
  }

  /**
     * deal with the channel data.
     * @param {Object} channelDetails [channel details]
     * @param {String} recordingMode [recording mode]
     * @return {Object}
     */
  channelDataHandle (channelDetails, recordingMode) {
    let self = this
    let channelNameDate
    channelNameDate = _.map(channelDetails, (channnelDetail, index) => {
      let channelData = {
        ID: channnelDetail['ID'],
        channelNO: channnelDetail['channelNO'] || (index + 1 + ''),
        logoUrl: channnelDetail['picture'] && channnelDetail['picture']['icons'] && channnelDetail['picture']['icons'][0],
        name: channnelDetail['name']
      }
      if (recordingMode !== 'NPVR') {
        let npvrPhyChannels = self.commonService.judgeRecordDynamic('npvrRecordCR', channnelDetail)
        if (npvrPhyChannels.length > 0) {
          channelData['supportNPVR'] = true
        } else {
          channelData['supportNPVR'] = false
        }
        let cpvrPhyChannels = self.commonService.judgeRecordDynamic('cpvrRecordCR', channnelDetail)
        if (cpvrPhyChannels.length > 0) {
          channelData['supportCPVR'] = true
          let qualityList = []
          let qualityObj = {}
          let physicalChannels = channnelDetail['physicalChannels']
          _.map(physicalChannels, item => {
            let dynamicProperty = _.find(channnelDetail['physicalChannelsDynamicProperties'], items => {
              return items['ID'] === item['ID']
            })
            let supportFlag = self.commonService.isQualitySupport(dynamicProperty)
            if (supportFlag === false) {
              return
            }
            if (!_.isUndefined(item['definition'])) {
              if (Number(item['definition']) === 0) {
                qualityObj = {
                  'quality': 'SD',
                  'qualityKey': 0,
                  'cpvrMediaID': item['ID']
                }
              } else if (Number(item['definition']) === ONE) {
                qualityObj = {
                  'quality': 'HD',
                  'qualityKey': 1,
                  'cpvrMediaID': item['ID']
                }
              } else if (Number(item['definition']) === TWO) {
                qualityObj = {
                  'quality': '4K',
                  'qualityKey': 2,
                  'cpvrMediaID': item['ID']
                }
              }
            }
            if (qualityList.length > 0) {
              let existed = _.find(qualityList, function (data) {
                return data['quality'] === qualityObj['quality']
              })
              if (_.isUndefined(existed)) {
                qualityList.push(qualityObj)
              }
            } else {
              qualityList.push(qualityObj)
            }
          })
          qualityList = _.sortBy(qualityList, ql => {
            return ql.qualityKey
          })
          channelData['qualityList'] = qualityList
        } else {
          channelData['supportCPVR'] = false
        }
      }
      return channelData
    })
    return channelNameDate
  }

  /**
     * check the date and repeat whether is comply with the rules.
     * @param {String} repeat [current value of Repeat]
     * @param {Array} repeatIndex [a array of repeat day]
     * @param {String} startDate [current value of Start Date]
     * @param {String} endDate [current value of End Date]
     *
     */
  isRepeatAndDateLegal (repeat, repeatIndex, startDate, endDate) {
    if (repeat === this.translate.instant('record_daily')) {
      // if repeat is daily,judgment is comply with the rules.
      return true
    } else if (endDate === 'seven_days_later' || endDate === 'fourteen_days_later' ||
            endDate === 'thirty_days_later' || endDate === 'unlimited') {
      // if the end time is seven days later ,thirty days later or unlimited,judgement is comply with the rules.
      return true
    } else if (this.hasIntersection(repeatIndex, startDate, endDate)) {
      // if the repeat and the date have an intersection,judgement is comply with the rules.
      return true
    } else {
      return false
    }
  }

  /**
     * remove some special cases,check that the repeat and the date have an intersection.
     * @param {Array} repeatIndex [a array of repeat day]
     * @param {String} startDate [current value of start date]
     * @param {String} endDate [current value of end date]
     */
  hasIntersection (repeatIndex, startDate, endDate) {
    let sDate = startDate.split('.')
    let eDate = endDate.split('.')
    let sDateObj = new Date(Number(sDate[2]), Number(sDate[1]) - 1, Number(sDate[0]))
    let eDateObj = new Date(Number(eDate[2]), Number(eDate[1]) - 1, Number(eDate[0]))
    if (eDateObj.getTime() - sDateObj.getTime() >= 86400000 * 6) {
      return true
    }

    // get day according to the date,if the value is 0, it is Sunday.
    let sWeek = sDateObj.getDay()
    let eWeek = eDateObj.getDay()
    if (sWeek === 0) {
      sWeek = 7
    }
    if (eWeek === 0) {
      eWeek = 7
    }
    return this.isValidRepeat(sWeek, eWeek, repeatIndex)
  }

  /**
     * judge whether the repeat recording is conform to the rules.
     * @param {Number} sWeek [the day of start date]
     * @param {Number} eWeek [the day of end date]
     * @param {Array} repeatIndex [a array of repeat day]
     */
  isValidRepeat (sWeek, eWeek, repeatIndex) {
    let legal = false
    if (sWeek < eWeek) {
      for (let i = 0; i < repeatIndex.length; i++) {
        let weekValue = repeatIndex[i]
        if (weekValue >= sWeek && weekValue <= eWeek) {
          legal = true
          break
        }
      }
      return legal
    } else {
      for (let i = 0; i < repeatIndex.length; i++) {
        let weekValue = repeatIndex[i]
        if ((weekValue >= sWeek && weekValue <= 7) || (weekValue >= 1 && weekValue <= eWeek)) {
          legal = true
          break
        }
      }
      return legal
    }
  }

  /**
     * @param {Date} dateObj current local date.
     * @param {String} timeType 'start' means it is start time, 'end' means it is end time.
     * @param {String} selectHour the value of hour user selected.
     * @return {Object}
     */
  judgeDSTDate (dateObj: Date, timeType: string, selectHour?: string) {
    let localString = dateObj.toTimeString()
    let ONE_HOUR_MILLSECONDS = 60 * 60 * 1000
    let response = {}
    let dateMillseconds = dateObj.getTime()
    let dateHour = dateObj.getHours()
    let postMSeconds = dateMillseconds - ONE_HOUR_MILLSECONDS
    let prevMSeconds = dateMillseconds + ONE_HOUR_MILLSECONDS
    let postHour = new Date(postMSeconds).getHours()
    let prevHour = new Date(prevMSeconds).getHours()
    if (postHour === dateHour || prevHour === dateHour) {
      // is repeat time.
      response['type'] = '3'
      if (timeType === 'start') {
        response['correctMSeconds'] = dateObj.getTime()
      } else if (timeType === 'end') {
        response['correctMSeconds'] = new Date(prevMSeconds).getTime()
      }
    } else {
      let localHour = localString.split(':')[0]
      if (!_.isUndefined(selectHour) && localHour !== selectHour) {
        // is missing time.
        response['type'] = '2'
      } else {
        // is common time.
        response['type'] = '1'
      }
    }
    return response
  }

  /**
     * get UTC property of local date.
     * @param {Date} dateObj
     * @return {Object}
     */
  getUTCProperty (dateObj: Date) {
    let date = {}
    date['utcYear'] = dateObj.getUTCFullYear()
    date['utcMonth'] = dateObj.getUTCMonth()
    date['utcDay'] = dateObj.getUTCDate()
    date['utcHour'] = dateObj.getUTCHours()
    date['utcMin'] = dateObj.getUTCMinutes()
    date['utcSec'] = dateObj.getUTCSeconds()
    return date
  }

  /**
     * get UTC millseconds.
     * @param {Object} utcDate
     * @return {Number}
     */
  getUTCMillseconds (utcDate: Object) {
    let millSeconds = Date.UTC(utcDate['utcYear'], utcDate['utcMonth'], utcDate['utcDay'],
      utcDate['utcHour'], utcDate['utcMin'], utcDate['utcSec'])
    return millSeconds
  }

  /**
     * get the expire date by days interval, and it will be formated according to the format specified.
     * @param {String} startDate [current value of start date]
     * @param {Number} interval [end date]
     * @param {String} dateFormat [time format]
     * @return {String}
     */
  getExpireDateByInterval (startDate: string, interval: number, dateFormat: string) {
    let dateArray = startDate.split('.')
    let startDateObj = new Date(Number(dateArray[2]), Number(dateArray[1]) - 1, Number(dateArray[0]))
    let millSeconds = startDateObj.getTime() + 86400000 * interval
    let expireDate = DateUtils.format(millSeconds, dateFormat)
    return expireDate
  }
}
