import * as _ from 'underscore'
import { Component, Output, OnInit, EventEmitter } from '@angular/core'
import { DirectionScroll } from 'ng-epg-ui/webtv-components/scroll/directionScroll.component'
import { TranslateService } from '@ngx-translate/core'
import { CommonService } from '../../../../core/common.service'

const ONE = 1, TWO = 2, THREE = 3, FOUR = 4, FIVE = 5, SIX = 6, SEVEN = 7

@Component({
  selector: 'app-record-repeat-data',
  styleUrls: ['./recordrepeatdata.component.scss'],
  templateUrl: './recordrepeatdata.component.html'
})

export class RecordRepeatDataComponent implements OnInit {
  @Output() selectRepeat: EventEmitter<Object> = new EventEmitter()
  public repeatDataDates = []
  public whiteRepeatDataDates = []
  public whiteDate = []
  public isShowRepeatData = false
  public isChoosen = [true, false, false, false, false, false, false, false]
  public isConfirm = true
  public choseDate = []
  private isChooseAllDate = false
  constructor (
        private directionScroll: DirectionScroll,
        private translate: TranslateService,
        private commonService: CommonService
    ) {}

  ngOnInit () {
  }
    /**
     * close the current page.
     */
  close () {
    this.isShowRepeatData = false
  }
    /**
     * choose data to record
     * @param i
     */
  chooseData (i) {
    if (this.isChoosen[i] === true) {
      this.isChoosen[i] = false
    } else {
      if (i !== 0) {
        this.isChoosen[0] = false
        this.isChoosen[i] = true
      } else {
        this.isChoosen = _.map(this.isChoosen, (item) => {
            item = false
            return item
          })
        this.isChoosen[0] = true
      }
    }
    let curState = _.find(this.isChoosen, item => {
      return item === true
    })
    if (!curState) {
      this.isConfirm = false
    } else {
      this.isConfirm = true
    }
  }
    /**
     * click  to comfirm you select this current state.
     */
  confirm () {
    let curState = _.find(this.isChoosen, item => {
      return item === true
    })
    if (curState) {
      let indexArray = []
      this.choseDate = []
      this.whiteDate = []
      if (this.isChoosen[0] === true) {
        this.choseDate = this.repeatDataDates[0]
        this.whiteDate = this.whiteRepeatDataDates[0]
        indexArray = [1, 2, 3, 4, 5, 6, 7]
        this.isChooseAllDate = true
      } else {
        for (let i = 0; i < this.isChoosen.length; i++) {
            if (this.isChoosen[i] === true && i !== 0) {
              indexArray.push(i)
            }
          }
        if (indexArray.length === SEVEN) {
            this.choseDate.push(this.repeatDataDates[0])
            this.whiteDate.push(this.whiteRepeatDataDates[0])
            this.isChooseAllDate = true
          } else {
            for (let i = 0; i < this.isChoosen.length; i++) {
              if (this.isChoosen[i] === true) {
                this.choseDate.push(this.repeatDataDates[i])
                this.whiteDate.push(this.whiteRepeatDataDates[i])
              }
            }
            this.isChooseAllDate = false
          }
      }
      let repeatData = {
        choseDate: this.choseDate,
        indexArray: indexArray,
        whiteDate: this.whiteDate
      }
      this.selectRepeat.emit(repeatData)
      this.isShowRepeatData = false
    }
  }
    /**
     * load date.
     * @param date
     * @param repeatIndex
     */
  loadDate (date, repeatIndex) {
    let daily = this.translate.instant('record_daily')
    let mon = this.translate.instant('Mon_suspension')
    let tue = this.translate.instant('Tues_suspension')
    let wed = this.translate.instant('Wed_suspension')
    let thu = this.translate.instant('Thur_suspension')
    let fri = this.translate.instant('Fri_suspension')
    let sat = this.translate.instant('Sat_suspension')
    let sun = this.translate.instant('Sun_suspension')
    this.whiteRepeatDataDates = []
    this.getAllWeekSec()
    this.repeatDataDates = [daily, mon, tue, wed, thu, fri, sat, sun]
    this.isChoosen = [false, false, false, false, false, false, false, false]
    if (repeatIndex.length === SEVEN) {
      for (let i = 0; i < this.isChoosen.length; i++) {
        if (i === 0) {
            this.isChoosen[i] = true
          } else {
            this.isChoosen[i] = false
          }
      }
    } else {
      this.isChoosen[0] = false
      for (let i = 0; i < repeatIndex.length; i++) {
        this.isChoosen[repeatIndex[i]] = true
      }
    }
  }
  getAllWeekSec () {
    let curTime = new Date().getTime()
    let curWeek = new Date().getDay()
    this.whiteRepeatDataDates.push(this.translate.instant('record_daily'))
    if (curWeek === ONE) {
      this.whiteRepeatDataDates.push(curTime)
      for (let i = 1; i < 7; i++) {
        let nextDay = curTime + i * 86400000
        this.whiteRepeatDataDates.push(nextDay)
      }
    } else if (curWeek === TWO) {
      this.whiteRepeatDataDates.push(curTime - 86400000)
      this.whiteRepeatDataDates.push(curTime)
      for (let i = 1; i < 6; i++) {
          let nextDay = curTime + i * 86400000
          this.whiteRepeatDataDates.push(nextDay)
        }
    } else if (curWeek === THREE) {
        this.whiteRepeatDataDates.push(curTime - 2 * 86400000)
        this.whiteRepeatDataDates.push(curTime - 86400000)
        this.whiteRepeatDataDates.push(curTime)
        for (let i = 1; i < 5; i++) {
          let nextDay = curTime + i * 86400000
          this.whiteRepeatDataDates.push(nextDay)
        }
      } else if (curWeek === FOUR) {
        this.whiteRepeatDataDates.push(curTime - 3 * 86400000)
        this.whiteRepeatDataDates.push(curTime - 2 * 86400000)
        this.whiteRepeatDataDates.push(curTime - 86400000)
        this.whiteRepeatDataDates.push(curTime)
        for (let i = 1; i < 4; i++) {
          let nextDay = curTime + i * 86400000
          this.whiteRepeatDataDates.push(nextDay)
        }
      } else if (curWeek === FIVE) {
        this.whiteRepeatDataDates.push(curTime - 4 * 86400000)
        this.whiteRepeatDataDates.push(curTime - 3 * 86400000)
        this.whiteRepeatDataDates.push(curTime - 2 * 86400000)
        this.whiteRepeatDataDates.push(curTime - 86400000)
        this.whiteRepeatDataDates.push(curTime)
        for (let i = 1; i < 3; i++) {
          let nextDay = curTime + i * 86400000
          this.whiteRepeatDataDates.push(nextDay)
        }
      } else if (curWeek === SIX) {
        this.whiteRepeatDataDates.push(curTime - 5 * 86400000)
        this.whiteRepeatDataDates.push(curTime - 4 * 86400000)
        this.whiteRepeatDataDates.push(curTime - 3 * 86400000)
        this.whiteRepeatDataDates.push(curTime - 2 * 86400000)
        this.whiteRepeatDataDates.push(curTime - 86400000)
        this.whiteRepeatDataDates.push(curTime)
        this.whiteRepeatDataDates.push(curTime + 86400000)
      } else if (curWeek === SEVEN) {
        this.whiteRepeatDataDates.push(curTime - 6 * 86400000)
        this.whiteRepeatDataDates.push(curTime - 5 * 86400000)
        this.whiteRepeatDataDates.push(curTime - 4 * 86400000)
        this.whiteRepeatDataDates.push(curTime - 3 * 86400000)
        this.whiteRepeatDataDates.push(curTime - 2 * 86400000)
        this.whiteRepeatDataDates.push(curTime - 86400000)
        this.whiteRepeatDataDates.push(curTime)
      }
    for (let i = 1; i < this.whiteRepeatDataDates.length; i++) {
      this.whiteRepeatDataDates[i] = this.commonService.dealWithDateJson('D07', this.whiteRepeatDataDates[i])
    }
  }
    /**
     *return the dom element.
     */
  dom (divName: string) {
    return document.getElementById(divName)
  }
}
