import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core'
import { DirectionScroll } from 'ng-epg-ui/webtv-components/scroll/directionScroll.component'

@Component({
  selector: 'app-record-channel-name',
  styleUrls: ['./recordChannelName.component.scss'],
  templateUrl: './recordChannelName.component.html'
})

export class RecordChannelNameComponent implements OnInit {
  @Input() set setChannelDates (channelData) {
    if (channelData) {
      this.channelDates = channelData
    }
  }
  @Output() selectChannel: EventEmitter<Object> = new EventEmitter()

  public channelDates = []
  public isShowScrollList = true
  public isShowChannelName = false
  public isChooseIndex = 0
  public showChannelLoad = false

  constructor (
        private directionScroll: DirectionScroll
    ) {}

  ngOnInit () {

  }
    /**
     * close the page
     */
  close () {
    this.isShowChannelName = false
  }
    /**
     * choose data by i
     * @param data
     * @param i
     */
  chooseData (data, i) {
    this.selectChannel.emit(data)
    this.isChooseIndex = i
    this.isShowChannelName = false
  }
    /**
     * scoll
     */
  pluseOnScroll () {
    if (this.channelDates.length > 12) {
      let oBox, oConter, oUl, oScroll, oSpan
      oBox = this.dom('channelName')
      oConter = this.dom('wrap')
      oUl = this.dom('channelNamePanel')
      oScroll = this.dom('channelScroll')
      oSpan = oScroll.getElementsByTagName('span')[0]
      this.directionScroll.setScroll(oBox, oConter, oUl, oScroll, oSpan, false, 'isToBottom')
    }
  }

  dom (divName: string) {
    return document.getElementById(divName)
  }
}
