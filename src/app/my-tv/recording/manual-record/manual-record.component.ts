import * as _ from 'underscore'
import { Component, ViewChild, OnInit, Input } from '@angular/core'
import { TranslateService } from '@ngx-translate/core'
import { session } from 'src/app/shared/services/session'
import { ManualService } from './manual-record.service'
import { RecordChannelNameComponent } from './record-manual-channelname/recordChannelName.component'
import { RecordRepeatDataComponent } from './record-repeat-data/recordrepeatdata.component'
import { RecordDataNameComponent } from './record-manual-data/recordDataName.component'
import { RecordStartTimeComponent } from './recordStartTime/recordStartTime.component'
import { EventService } from 'ng-epg-sdk/services'
import { CacheConfigService } from '../../../shared/services/cache-config.service'
import { RecordDataService } from '../../../component/recordDialog/recordData.service'
import { DateUtils } from 'ng-epg-sdk/utils'
import { RecordDeactivate } from '../record-deactivate.service'
import { ChannelCacheService } from '../../../shared/services/channel-cache.service'
import { config } from '../../../shared/services/config'
import { CommonService } from '../../../core/common.service'

const SPACE_KEY = 32, LEFT_KEY = 37, UP_KEY = 38, RIGHT_KEY = 39, DOWN_KEY = 40
const ONE = 1, TWO = 2, THREE = 3, FOUR = 4, SEVEN = 7, TWEETY_THREE = 23

@Component({
  selector: 'app-manual-recording',
  styleUrls: ['./manual-record.component.scss'],
  templateUrl: './manual-record.component.html'
})
export class ManualRecordingComponent implements OnInit {
  @ViewChild(RecordChannelNameComponent) recordChannelName: RecordChannelNameComponent
  @ViewChild(RecordRepeatDataComponent) recordRepeatData: RecordRepeatDataComponent
  @ViewChild(RecordDataNameComponent) recordDataName: RecordDataNameComponent
  @ViewChild(RecordStartTimeComponent) recordStartTime: RecordStartTimeComponent
  public channelNameDate // array of channel data that was encapsulated.
  public channelDetails = [] // array of channel data that was got by interface.
  public channelItem = {} // variable to save the current channel information.
  public curQualityObj = {} // variable to save the current quality information.
  public singleRecName = '' // variable to save the single recording name.
  public periodRecName = '' // variable to save the period recording name.
  public recordName = '' // variable to save the current recording name.
  public repeat = '' // variable to save the current value of Repeat.
  public startDate = '' // variable to save the current value of Start Date.
  public whiteStartDate = ''
  public whiteEndDate = ''
  public endDate = '' // variable to save the current value of End Date.
  public singleStartDate = ''
  public startHour = ''
  public startMin = ''
  public endHour = ''
  public endMin = ''
  public recordingMode = ''
  public recoredDate = 'recored_date'
  public focusRecName = ''
  public defaultQuality = ''
  public isSingle = true
  public showNextDayIcon = false // variable to check the next-day icon whether should be visbile.
  public canAutoChangeName = true // variable to check the system whether will auto change the Recording name by time setting.
  public showQualityList = false
  public canChooseStorage = false
  public currentDate
  public curYear: number
  public curMonth: number
  public curDay: number
  public curHour: number
  public curMin: number
  public curSec: number
    /**
     * keep the storage of current channel supporting,
     * 0 represent that all are not supported,
     * 1 represent that NPVR is supported,
     * 2 represent that CPVR is supported,
     * 3 represent that all are supported.
     */
  public defaultQualityKey: number
    /**
     * keep the storage of default channel supporting,
     * 1 represent that NPVR is selected by default,
     * 2 represent that CPVR is selected by default,
     * 3 represent that all are selected by default,
     */
  public supportStorage: number
    /**
     * the default channel storage location,
     * 1 represents the default check NPVR,
     * 2 represents the default check CPVR,
     * 3 represents the default check all,
     */
  public defaultStorage: number
  public chooseTimeIndex = -1
  public repeatArray = []
  public repeatIndex = []
  public configData
  public allChannelData = []
  public NPVRData = []
  public CPVRData = []
  public chooseStorage = [false, false]
  public isNameEmpty = false
  public noChannelsExist = false
  public totalLength
  public nowLength
  public isToBottom = true
  public userChangeStorage = false
  public blackListCache = ''
  public maxLengthCache = 128
  public supportAllCPVR = false
  public allChannelPVRData
  public hasCommitRecord = false
  public channelItemName
  public focusTimeValue = ''
  public isEnterChangeButton = false
  public saveSingleStartDate
  public saveStartDate
  public saveEndDate
  public curDateStr
  delayFilter = _.debounce((fc: Function) => {
    fc()
  }, 500, true)
  loadFilter = _.debounce((fc: Function) => {
    fc()
  }, 500, true)

  constructor (
        private manualService: ManualService,
        private translate: TranslateService,
        private recordDataService: RecordDataService,
        private recordDeactivate: RecordDeactivate,
        private cacheConfigService: CacheConfigService,
        private channelCacheService: ChannelCacheService,
        private commonService: CommonService
    ) { }

  @Input() set setConfig (data) {
    this.configData = data
    this.recordingMode = config['PVRMode']
    this.supportAllCPVR = config['supportAllCPVR']
    if (this.recordingMode === 'NPVR') {
      this.chooseStorage = [true, false]
    }
    if (this.recordingMode === 'CPVR') {
      this.chooseStorage = [false, true]
      this.getDefaultQuality()
    }
    if (this.recordingMode === 'CPVR&NPVR') {
      this.getDefaultStorage()
      this.getDefaultQuality()
    }
  }

  ngOnInit () {
    let self = this
    this.getCurrentTimeInfo()
    this.initDatePeriod()
    this.initTimePeriod()
    this.getPVRChannelCache()
    this.setDefaultRepeat()
    this.blackListCache = session.get('COMMON_INPUT_BLACKLIST_CACHE')
    this.maxLengthCache = session.get('COMMON_INPUT_MAXLENGTH_CACHE')
      /**
         * when the scrollbar of the channel list scroll to the bottom, load the next page of the channel list.
         */
    EventService.removeAllListeners(['SHOW_ALLCHANNEL_PROGRAM'])
    EventService.on('SHOW_ALLCHANNEL_PROGRAM', (event) => {
      self.loadFilter(() => {
        self.getMoreChannel()
      })
    })
      /**
         * after the recording conflict was solved, we need to restore related settings.
         */
    EventService.on('MANUAL_SET_AFTER_SOLVE_CONFLICT', () => {
      self.backToInitialSet()
    })
    if (this.channelItem['name'] && this.channelItem['name'].length >= 20) {
      this.channelItemName = this.channelItem['name'].slice(0, 20) + '...'
    } else {
      this.channelItemName = this.channelItem['name']
    }
  }

    /**
     * get the attribute value of the current time.
     */
  getCurrentTimeInfo () {
    this.currentDate = Date.now()
    this.curYear = Date.now()['getFullYear']()
    this.curMonth = Date.now()['getMonth']() + 1
    this.curDay = Date.now()['getDate']()
    this.curHour = Date.now()['getHours']()
    this.curMin = Date.now()['getMinutes']()
    this.curSec = Date.now()['getSeconds']()
      // 2017/08/30 11:17:27
    this.curDateStr = this.currentDate.getTime()
  }

    /**
     * set the initial date.
     */
  initDatePeriod () {
    this.saveStartDate = this.addZero(this.curDay + '') + '.' + this.addZero(this.curMonth + '') + '.' + this.curYear
    this.saveEndDate = 'unlimited'
    this.whiteStartDate = this.commonService.dealWithDateJson('D02', this.curDateStr)
    this.startDate = this.commonService.dealWithOrgTime(this.saveStartDate)
    this.endDate = 'unlimited'
    this.whiteEndDate = 'unlimited'
    this.singleStartDate = this.startDate
    this.singleStartDate = this.commonService.dealWithOrgTime(this.saveStartDate)
  }

    /**
     * set the initial time.
     */
  initTimePeriod () {
    this.startHour = this.addZero(this.curHour + '')
    this.startMin = this.addZero(this.curMin + '')
    if (this.curHour === TWEETY_THREE) {
      this.endHour = this.addZero(0 + '')
    } else {
      this.endHour = this.addZero((this.curHour + 1) + '')
    }
    this.endMin = this.addZero(this.curMin + '')
  }

    // get channel list data according to the configuration of the PVRMode.
  getChannelLists () {
    if (this.channelDetails.length > 0) {
      this.channelNameDate = this.manualService.channelDataHandle(this.channelDetails, this.recordingMode)
      this.channelItem = this.channelNameDate[0]
      this.getStorage(this.channelItem)
      if (this.recordingMode !== 'NPVR' && this.supportStorage !== 1) {
        this.curQualityObj = this.findUpNearQuality(this.channelItem['qualityList'], this.defaultQualityKey, true)
      }
      this.getInitRecordName()
    } else {
      this.noChannelsExist = true
    }
  }

    /**
     * get the cache data of the channel supporting PVR.
     */
  getPVRChannelCache () {
    let userType
    if (Cookies.getJSON('IS_PROFILE_LOGIN')) {
      userType = Cookies.getJSON('USER_ID')
    } else {
      userType = session.get('Guest')
    }
    let staticData = session.get(userType + '_Static_Version_Data')
    let dynamicData = session.get(userType + '_Dynamic_Version_Data')
    _.map(dynamicData['channelDynamaicProp'], (list1, index) => {
      _.each(staticData['channelDetails'], (list) => {
        if (list1['ID'] === list['ID']) {
            list['physicalChannelsDynamicProperties'] = list1['physicalChannelsDynamicProperties']
          }
      })
    })
    let allChannel = staticData['channelDetails']
    if (!config['supportAllCPVR']) {
      this.allChannelPVRData = this.cacheConfigService.getNPVRChannelManual(allChannel)
      this.NPVRData = this.allChannelData
    } else {
      let allNPVRChannel = this.cacheConfigService.getNPVRChannelManual(allChannel)
      let allCPVRChannel = this.cacheConfigService.getCPVRChannelManual(allChannel)
      this.allChannelPVRData = _.union(allNPVRChannel, allCPVRChannel)
      this.allChannelPVRData.sort((a, b) => {
        return Number(a['ID']) - Number(b['ID'])
      })
      this.NPVRData = allNPVRChannel.slice(0, 13)
      this.CPVRData = allCPVRChannel.slice(0, 13)
    }
    this.totalLength = this.allChannelPVRData.length
    this.allChannelData = this.allChannelPVRData.slice(0, 13)
    if (this.recordingMode === 'NPVR') {
      this.channelDetails = this.NPVRData
    } else if (this.recordingMode === 'CPVR') {
      if (!this.supportAllCPVR) {
          this.channelDetails = []
        } else {
          this.channelDetails = this.CPVRData
        }
    } else {
      this.channelDetails = this.allChannelData
    }
    this.nowLength = this.channelDetails.length
    this.getChannelLists()
  }

    /**
     * when scroll to the bottom, it will get more channel to show.
     */
  getMoreChannel () {
    let self = this
    if (self.isToBottom && this.nowLength < this.totalLength) {
      self.isToBottom = false
      self.recordChannelName.showChannelLoad = true
      let newChannelData = this.allChannelPVRData.slice(Number(self.nowLength), Number(self.nowLength) + 13)
      self.allChannelData = self.allChannelData.concat(newChannelData)
      self.nowLength = self.allChannelData.length
      self.NPVRData = self.cacheConfigService.getNPVRChannelManual(self.allChannelData)
      self.CPVRData = self.cacheConfigService.getCPVRChannelManual(self.allChannelData)
      if (self.recordingMode === 'NPVR') {
        self.channelDetails = self.NPVRData
      } else if (self.recordingMode === 'CPVR') {
          self.channelDetails = self.CPVRData
        } else {
          self.channelDetails = self.allChannelData
        }
      self.channelNameDate = self.manualService.channelDataHandle(self.channelDetails, self.recordingMode)
      self.isToBottom = true
      self.recordChannelName.showChannelLoad = false
      self.recordChannelName.pluseOnScroll()
    }
  }

    // get the configuration of the default storgage.
  getDefaultStorage () {
    let storage = this.configData['default_reccfg_pvr_type']
    switch (storage) {
      case 'NPVR':
        this.defaultStorage = 1
        break
      case 'CPVR':
        this.defaultStorage = 2
        break
      case 'MIXPVR':
        this.defaultStorage = 3
        break
    }
  }

    // get the storage judged by channel and the principle of up close to.
  getStorage (channelData) {
    if (channelData['supportNPVR'] === true && channelData['supportCPVR'] === true) {
      this.supportStorage = 3
      this.canChooseStorage = true
      if (this.userChangeStorage) {
        return
      }
      if (this.supportAllCPVR) {
        if (this.defaultStorage === ONE) {
            this.chooseStorage = [true, false]
          } else if (this.defaultStorage === TWO) {
            this.chooseStorage = [false, true]
          }
      } else {
        this.chooseStorage = [true, false]
      }
    } else if (channelData['supportNPVR'] === true || this.recordingMode === 'NPVR') {
      this.supportStorage = 1
      this.chooseStorage = [true, false]
      this.canChooseStorage = false
    } else if (channelData['supportCPVR'] === true) {
        this.supportStorage = 2
        this.chooseStorage = [false, true]
        this.canChooseStorage = false
      } else {
        this.supportStorage = 0
        this.chooseStorage = [false, false]
        this.canChooseStorage = false
      }
  }

    // get  the configuration of the default quality.
  getDefaultQuality () {
    this.defaultQuality = this.configData['default_reccfg_definition']
    switch (this.defaultQuality) {
      case 'SD':
        this.defaultQualityKey = 0
        break
      case 'HD':
        this.defaultQualityKey = 1
        break
      case '4K':
        this.defaultQualityKey = 2
        break
    }
  }

    // the principle of the quality up close to.
  findUpNearQuality (qualityList, defQualityKey, firstFlag?: boolean) {
    let curQualityObj = {}
    let findQualityObj
    if (!_.isUndefined(firstFlag) && firstFlag === true) {
        // traverse the list of support quality,find the quality that is equal to default quality.
      findQualityObj = _.find(qualityList, item => {
        return item['qualityKey'] === defQualityKey
      })
    }
    if (!_.isUndefined(findQualityObj)) {
      curQualityObj = findQualityObj
    } else {
        // if the default quality is not exit,use the up near principle.
      let higherQualityObj = _.find(qualityList, item => {
        return item['qualityKey'] > defQualityKey
      })
      if (!_.isUndefined(higherQualityObj)) {
        curQualityObj = higherQualityObj
      } else {
          // if the quality of up near is not exit,use the up down principle.
        curQualityObj = _.max(qualityList, item => {
            return item['qualityKey']
          })
      }
    }
    return curQualityObj
  }

    // set the initial repeat.
  setDefaultRepeat () {
    this.repeat = this.translate.instant('record_daily')
    this.repeatIndex = [1, 2, 3, 4, 5, 6, 7]
  }

    // set the record name.
  setRecordName () {
    if (this.canAutoChangeName) {
      this.setSingleName()
      this.setPeriodName()
      if (this.isSingle) {
        this.recordName = this.singleRecName.replace(',', '')
      } else {
        this.recordName = this.periodRecName
      }
    }
  }

    // set the initial record name.
  getInitRecordName () {
    this.saveSingleStartDate = this.commonService.dealDateToNumber(this.singleStartDate)
    let dateArray = this.saveSingleStartDate.split('.')
    this.singleRecName = this.channelItem['name'] + '_' + this.addZero(dateArray[0]) + '.' + this.addZero(dateArray[1]) + '_' +
            this.addZero(this.startHour) + ':' + this.addZero(this.startMin)
    this.periodRecName = this.channelItem['name'] + '_' + this.addZero(this.startHour) + ':' + this.addZero(this.startMin) +
            '－' + this.addZero(this.endHour) + ':' + this.addZero(this.endMin)
    if (this.isSingle) {
      this.recordName = this.singleRecName.replace(',', '')
    } else {
      this.recordName = this.periodRecName
    }
  }

    // set the single record name.
  setSingleName () {
    this.saveSingleStartDate = this.commonService.dealDateToNumber(this.singleStartDate)
    let dateArray = this.saveSingleStartDate.split('.')
    this.singleRecName = this.channelItem['name'] + '_' + this.addZero(dateArray[0]) + '.' + this.addZero(dateArray[1]) + '_' +
            this.addZero(this.startHour) + ':' + this.addZero(this.startMin)
  }

    // set the period record name.
  setPeriodName () {
    this.periodRecName = this.channelItem['name'] + '_' + this.addZero(this.startHour) + ':' + this.addZero(this.startMin) +
            '－' + this.addZero(this.endHour) + ':' + this.addZero(this.endMin)
  }

    // choose the channel.
  changeChannel (data) {
    if (!_.isEqual(this.channelItem, data)) {
      this.recordDeactivate.conditionChanged = true
    }
    this.channelItem = data
    if (this.recordingMode !== 'NPVR') {
      this.getStorage(this.channelItem)
      if (this.channelItem['supportCPVR'] === true) {
        let existSameQuality = false
        for (let i = 0; i < this.channelItem['qualityList'].length; i++) {
            if (this.channelItem['qualityList'][i]['quality'] === this.curQualityObj['quality']) {
              existSameQuality = true
              let curQuality = this.curQualityObj['quality']
              this.curQualityObj = _.find(this.channelItem['qualityList'], item => {
                return item['quality'] === curQuality
              })
              break
            }
          }
        if (existSameQuality === false) {
            this.curQualityObj = this.findUpNearQuality(this.channelItem['qualityList'], this.defaultQualityKey)
          }
      }
    }
    this.setRecordName()
    if (this.channelItem['name'].length >= 20) {
      this.channelItemName = this.channelItem['name'].slice(0, 20) + '...'
    } else {
      this.channelItemName = this.channelItem['name']
    }
  }

    // clicke the edit buttom of the record name.
  editRecName () {
    if (this.dom('nameRef')) {
      this.dom('nameRef').focus()
    }
    this.hideQualityList()
  }

    // change the record name by manual.
  inputRecName (e) {
    let value = e.target.value
    if (value.trim() === '') {
      value = ''
    }
    if (this.isSingle) {
      this.singleRecName = value
    } else {
      this.periodRecName = value
    }
    this.recordName = value
    if (this.recordName === '') {
      this.isNameEmpty = true
    } else {
      this.isNameEmpty = false
      if (this.recordName !== this.focusRecName) {
        this.recordDeactivate.conditionChanged = true
      }
    }
  }

    // get the focus of record name.
  RecNameFocus (e) {
    this.focusRecName = e.target.value
    this.hideQualityList()
  }

    // lose the focus of record name.
  RecNameBlur (e) {
    if (e.target.value !== this.focusRecName) {
      this.canAutoChangeName = false
    }
  }

    // the first character of input does not allow to input spaces.
  onPressChange (e) {
    let caretPos = 0
    let ctrl = e.currentTarget
    if (document['selection']) {
      ctrl.focus()
      let sel = document['selection'].createRange()
      sel.moveStart('character', -ctrl['value'].length)
      caretPos = sel.text.length
    } else if (ctrl['selectionStart'] || ctrl['selectionStart'] === '0') {
      caretPos = ctrl['selectionStart']
    }
    if (e.keyCode === SPACE_KEY) {
      if (caretPos === 0) {
        e.returnValue = false
      }
    }
  }

    // select single record.
  selectSingle () {
    if (!this.isSingle) {
      this.recordDeactivate.conditionChanged = true
    }
    this.isSingle = true
    this.recoredDate = 'recored_date'
    if (this.canAutoChangeName) {
      this.recordName = this.singleRecName
    }
    this.hideQualityList()
  }

    // select period record.
  selectPeriodic () {
    if (this.isSingle) {
      this.recordDeactivate.conditionChanged = true
    }
    this.isSingle = false
    this.recoredDate = 'record_dates'
    if (this.canAutoChangeName) {
      this.recordName = this.periodRecName
    }
    this.hideQualityList()
  }

    // select the storage of cloud.
  selectCloud () {
    if (this.canChooseStorage === true) {
      if (!this.chooseStorage[0]) {
        this.recordDeactivate.conditionChanged = true
      }
      this.userChangeStorage = true
      this.chooseStorage = [true, false]
      this.hideQualityList()
    }
  }

    // select the storage of stb.
  selectSTB () {
    if (this.canChooseStorage === true) {
      if (!this.chooseStorage[1]) {
        this.recordDeactivate.conditionChanged = true
      }
      this.userChangeStorage = true
      this.chooseStorage = [false, true]
      this.hideQualityList()
    }
  }

    // select repeat.
  changeRepeat (data) {
    if (!_.isEqual(this.repeatIndex, data['indexArray'])) {
      this.recordDeactivate.conditionChanged = true
    }
    this.repeatArray = data['choseDate']
    this.repeatIndex = data['indexArray']
    this.repeat = ''
    if (data['indexArray'].length === SEVEN) {
      this.repeat = this.translate.instant('record_daily')
    } else {
      _.map(data['whiteDate'], (item, index) => {
        if (index === data['whiteDate'].length - 1) {
            this.repeat = this.repeat + item
          } else {
            this.repeat = this.repeat + item + ' '
          }
      })
    }
  }

    // select the single record data.
  changeSingleDate (data) {
    this.saveSingleStartDate = this.commonService.dealDateToNumber(this.singleStartDate)
    if (this.saveSingleStartDate !== data) {
      this.recordDeactivate.conditionChanged = true
    }
    this.singleStartDate = data
    this.setRecordName()
  }

    // select the period record data.
  changePeriodDate (data) {
    if (data['types'] === 'start') {
      if (this.startDate !== data['chooseTimeDate']) {
        this.recordDeactivate.conditionChanged = true
      }
      this.startDate = data['chooseTimeDate']
      this.whiteStartDate = data['whiteChooseTimeDate']
    } else {
      if (this.endDate !== data['chooseTimeDate']) {
        this.recordDeactivate.conditionChanged = true
      }
      this.endDate = data['chooseTimeDate']
      this.whiteEndDate = data['whiteChooseTimeDate']
    }
    this.saveStartDate = this.commonService.dealDateToNumber(this.startDate)
    if (this.endDate !== 'unlimited' && this.endDate !== 'seven_days_later' &&
            this.endDate !== 'fourteen_days_later' && this.endDate !== 'thirty_days_later') {
      this.saveEndDate = this.commonService.dealDateToNumber(this.endDate)
    } else {
      this.saveEndDate = this.endDate
    }
    let dateIllegalFlag = this.manualService.checkDateInvalidType(this.saveStartDate, this.saveEndDate)
    if (dateIllegalFlag !== '') {
      if (dateIllegalFlag === 'Invalid') {
        EventService.emit('END_DATA_MUST_LATER')
      }
      this.autoChangeNextDay()
    }
  }

    // if the data do not conform to the rule,the end time changes to the next day of the start time automatically.
  autoChangeNextDay () {
    this.saveStartDate = this.commonService.dealDateToNumber(this.startDate)
    this.saveEndDate = this.manualService.getExpireDateByInterval(this.saveStartDate, 1, 'DD.MM.YYYY')
    this.endDate = this.commonService.dealWithOrgTime(this.saveEndDate)
    let dateArray = this.saveEndDate.split('.')
    let millSec = new Date(dateArray[2], dateArray[1] - 1, dateArray[0]).getTime()
    this.whiteEndDate = this.commonService.dealWithDateJson('D02', millSec)
  }

    // add the zero before the time.
  addZero (date) {
    if (date.length === ONE) {
      date = '0' + date
    }
    return date
  }

    // choose the hour or minute of the start time or the end time,to sign the assignment.
  chooseTimes (i) {
    this.chooseTimeIndex = i
    this.hideQualityList()
  }

    // choose the channel dialog.
  choiceChannel () {
    this.recordChannelName.isShowChannelName = true
    this.recordChannelName.pluseOnScroll()
    this.hideQualityList()
  }

    // show the choice llist of record quality.
  choiceQuality () {
    this.showQualityList = !this.showQualityList
  }

    // the mouse leave the quality list.
  mouseOutQualityList () {
    this.showQualityList = false
  }

    // select the record quality.
  selectQuality (qualityObj) {
    if (!_.isEqual(this.curQualityObj, qualityObj)) {
      this.recordDeactivate.conditionChanged = true
    }
    this.curQualityObj = qualityObj
    this.showQualityList = false
  }

    // if click the other location,hidden the list of record quality.
  hideQualityList () {
    if (this.showQualityList === true) {
      this.showQualityList = false
    }
  }

    // select the repeat date of period record.
  choiceRepeatDate () {
    this.recordRepeatData.isShowRepeatData = true
    this.recordRepeatData.loadDate(this.repeat, this.repeatIndex)
    this.hideQualityList()
  }

    // select the single record date.
  choiceManualDate () {
    this.recordDataName.isShowDataName = true
    this.saveSingleStartDate = this.commonService.dealDateToNumber(this.singleStartDate)
    this.recordDataName.setChooseDate(this.saveSingleStartDate)
    this.hideQualityList()
  }

    // select the start time.
  chooseStartTime () {
    this.saveStartDate = this.commonService.dealDateToNumber(this.startDate)
    if (this.endDate !== 'unlimited' && this.endDate !== 'seven_days_later' &&
            this.endDate !== 'fourteen_days_later' && this.endDate !== 'thirty_days_later') {
      this.saveEndDate = this.commonService.dealDateToNumber(this.endDate)
    } else {
      this.saveEndDate = this.endDate
    }
    this.recordStartTime.isShowStartTime = true
    this.recordStartTime.loadDateData('start', this.saveStartDate, this.saveEndDate)
    this.hideQualityList()
  }

    // select the end time.
  chooseEndTime () {
    this.saveStartDate = this.commonService.dealDateToNumber(this.startDate)
    if (this.endDate !== 'unlimited' && this.endDate !== 'seven_days_later' &&
            this.endDate !== 'fourteen_days_later' && this.endDate !== 'thirty_days_later') {
      this.saveEndDate = this.commonService.dealDateToNumber(this.endDate)
    } else {
      this.saveEndDate = this.endDate
    }
    this.recordStartTime.isShowEndTime = true
    this.recordStartTime.loadDateData('end', this.saveStartDate, this.saveEndDate)
    this.hideQualityList()
  }

    // input the time by manual.
  KeyEventHandle (e) {
    if (e.target.id === 'start-hour' || e.target.id === 'end-hour') {
      if (Number(e.target.value) > 23) {
        e.target.value = '23'
      }
    } else if (e.target.id === 'start-min' || e.target.id === 'end-min') {
      if (Number(e.target.value) > 59) {
          e.target.value = '59'
        }
    }
    this.KeyEventHandleTwice(e)
    this.setRecordName()
    this.checkIsNextDay()
  }

  KeyEventHandleTwice (e) {
    if (e.keyCode === UP_KEY) {
      if (e.target.id === 'start-hour' || e.target.id === 'start-min') {
        this.increaseStart()
      }
      if (e.target.id === 'end-hour' || e.target.id === 'end-min') {
        this.increaseEnd()
      }
    } else if (e.keyCode === DOWN_KEY) {
      if (e.target.id === 'start-hour' || e.target.id === 'start-min') {
          this.decreaseStart()
        }
      if (e.target.id === 'end-hour' || e.target.id === 'end-min') {
          this.decreaseEnd()
        }
    } else if (e.keyCode !== LEFT_KEY && e.keyCode !== RIGHT_KEY) {
        e.target.value = e.target.value.replace(/\D/g, '')
        this.assignValue(e)
      }
  }

  assignValue (e) {
    switch (e.target.id) {
      case 'start-hour':
        this.startHour = e.target.value
        break
      case 'start-min':
        this.startMin = e.target.value
        break
      case 'end-hour':
        this.endHour = e.target.value
        break
      case 'end-min':
        this.endMin = e.target.value
        break
      default:
        break
    }
  }

    /**
     * adjust the start time to up.
     */
  increaseStart () {
    if (this.chooseTimeIndex === ONE) {
      this.startHour = this.addZero((Number(this.startHour) + 1) + '')
      if (Number(this.startHour) < 0 || Number(this.startHour) >= 24) {
        this.startHour = '00'
      }
      if (this.startHour !== this.focusTimeValue) {
        this.recordDeactivate.conditionChanged = true
      }
    }
    if (this.chooseTimeIndex === TWO) {
      this.startMin = this.addZero((Number(this.startMin) + 1) + '')
      if (Number(this.startMin) < 0 || Number(this.startMin) >= 60) {
        this.startMin = '00'
      }
      if (this.startMin !== this.focusTimeValue) {
        this.recordDeactivate.conditionChanged = true
      }
    }
    this.setRecordName()
    this.checkIsNextDay()
    this.hideQualityList()
  }

    /**
     * adjust the start time to down.
     */
  decreaseStart () {
    if (this.chooseTimeIndex === ONE) {
      this.startHour = this.addZero((Number(this.startHour) - 1) + '')
      if (Number(this.startHour) < 0 || Number(this.startHour) >= 24) {
        this.startHour = '23'
      }
      if (this.startHour !== this.focusTimeValue) {
        this.recordDeactivate.conditionChanged = true
      }
    }
    if (this.chooseTimeIndex === TWO) {
      this.startMin = this.addZero((Number(this.startMin) - 1) + '')
      if (Number(this.startMin) < 0 || Number(this.startMin) >= 60) {
        this.startMin = '59'
      }
      if (this.startMin !== this.focusTimeValue) {
        this.recordDeactivate.conditionChanged = true
      }
    }
    this.setRecordName()
    this.checkIsNextDay()
    this.hideQualityList()
  }

    /**
     * adjust the end time to up.
     */
  increaseEnd () {
    if (this.chooseTimeIndex === THREE) {
      this.endHour = this.addZero((Number(this.endHour) + 1) + '')
      if (Number(this.endHour) < 0 || Number(this.endHour) >= 24) {
        this.endHour = '00'
      }
      if (this.endHour !== this.focusTimeValue) {
        this.recordDeactivate.conditionChanged = true
      }
    }
    if (this.chooseTimeIndex === FOUR) {
      this.endMin = this.addZero((Number(this.endMin) + 1) + '')
      if (Number(this.endMin) < 0 || Number(this.endMin) >= 60) {
        this.endMin = '00'
      }
      if (this.endMin !== this.focusTimeValue) {
        this.recordDeactivate.conditionChanged = true
      }
    }
    this.setRecordName()
    this.checkIsNextDay()
    this.hideQualityList()
  }

    /**
     * adjust the end time to down.
     */
  decreaseEnd () {
    if (this.chooseTimeIndex === THREE) {
      this.endHour = this.addZero((Number(this.endHour) - 1) + '')
      if (Number(this.endHour) < 0 || Number(this.endHour) >= 24) {
        this.endHour = '23'
      }
      if (this.endHour !== this.focusTimeValue) {
        this.recordDeactivate.conditionChanged = true
      }
    }
    if (this.chooseTimeIndex === FOUR) {
      this.endMin = this.addZero((Number(this.endMin) - 1) + '')
      if (Number(this.endMin) < 0 || Number(this.endMin) >= 60) {
        this.endMin = '59'
      }
      if (this.endMin !== this.focusTimeValue) {
        this.recordDeactivate.conditionChanged = true
      }
    }
    this.setRecordName()
    this.checkIsNextDay()
    this.hideQualityList()
  }

    /**
     * focus on the time.
     */
  timeFocus (e) {
    this.focusTimeValue = e.target.value
  }

    /**
     * the time input loset the focus,check that it is need to add zero.
     */
  timeBlur (e) {
    if (this.isEnterChangeButton) {
      return
    }
    this.chooseTimeIndex = -1
    e.target.value = this.addZero(e.target.value)
    if (e.target.value !== this.focusTimeValue) {
      this.recordDeactivate.conditionChanged = true
    }
  }

    /**
     * check whether the end time is belong to the next day.
     */
  checkIsNextDay () {
    if (Number(this.endHour) < Number(this.startHour)) {
      this.showNextDayIcon = true
    } else if (Number(this.endHour) === Number(this.startHour) && Number(this.endMin) <= Number(this.startMin)) {
      this.showNextDayIcon = true
    } else {
      this.showNextDayIcon = false
    }
  }

    /**
     * check whether the start time is five minutes earlier than the current time.
     */
  checkTimeIsCorrect () {
    this.getCurrentTimeInfo()
    let nowTime = this.currentDate['getTime']()
    if (new Date(nowTime).getHours() === new Date(nowTime - 60 * 60 * 1000).getHours()) {
      nowTime = nowTime - 60 * 60 * 1000
    }
    let startTime = new Date(this.curYear, this.curMonth - 1, this.curDay,
        Number(this.startHour), Number(this.startMin), this.curSec).getTime()
    if (nowTime - startTime > 5 * 60 * 1000) {
      return true
    }
  }

    /**
     * check whether the task name input is legal.
     */
  checkNameInputRule () {
    let returnValue = ''
    if (this.recordName.length > this.maxLengthCache) {
      returnValue = this.translate.instant('record_name_maxLength', { common: this.maxLengthCache })
      return returnValue
    }
    if (this.blackListCache !== '') {
      if (!(/^[A-Z|a-z|0-9]+$/.test(this.recordName))) {
        let numLetters = this.recordName.replace(/[A-Z|a-z|0-9]/ig, '')
        let numLettersArr = numLetters.split('')
        if (numLettersArr.length !== 0) {
            let hasLettersOfBlackList = false
            nameOuter: for (let i = 0; i < numLettersArr.length; i++) {
              for (let j = 0; j < this.blackListCache.length; j++) {
                if (numLettersArr[i] === this.blackListCache.charAt(j)) {
                  hasLettersOfBlackList = true
                  break nameOuter
                }
              }
            }
            if (hasLettersOfBlackList) {
              returnValue = this.translate.instant('record_name_blacklist', { special_char: this.blackListCache })
              return returnValue
            }
          }
      }
    }
    return returnValue
  }

    /**
     * add the recording task.
     */
  record (event?) {
    if (event) {
      this.delayFilter(() => {
        this.handleTask(event)
      })
    } else {
      this.delayFilter(() => {
        this.handleTask()
      })
    }
  }

    /**
     * after filtering the repeat requests, it will continue handling the recording task.
     */
  handleTask (event?) {
    if (this.isNameEmpty) {
      return
    }
    let checkMsg = this.checkNameInputRule()
    if (checkMsg !== '') {
      EventService.emit('RECORD_NAME_INPUT_INCORRECT', checkMsg)
      return
    }
    if (this.startHour === '' || this.startMin === '' || this.endHour === '' || this.endMin === '') {
      EventService.emit('TIME_INPUT_EMPTY')
      return
    }
    if (this.hasCommitRecord && !this.recordDeactivate.conditionChanged) {
      return
    }
    if (this.isSingle) {
      this.addTimebasedRecord()
    } else {
      if (event) {
        this.addPeriodRecord(event)
      } else {
        this.addPeriodRecord()
      }
    }
    this.setScrollZero(this.channelDetails.length)
  }

    /**
     * make the scroll bar initial.
     * @param {Number} length [length of channels]
     */
  setScrollZero (length) {
    if (length > 12) {
      if (this.dom('channelNamePanel') && this.dom('channelNamePanel')['style'] &&
                this.dom('channelScrollSpan') && this.dom('channelScrollSpan')['style']) {
        this.dom('channelNamePanel')['style']['top'] = '0px'
        this.dom('channelScrollSpan')['style']['top'] = '0px'
      }
    }
  }

    /**
     * add time-based recording task.
     */
  addTimebasedRecord () {
    let self = this
    let data, type
    let isDSTSpecialScene = false
    this.saveSingleStartDate = this.commonService.dealDateToNumber(this.singleStartDate)
    let startInt = this.manualService.dateStrToArray(this.saveSingleStartDate)
    let startDateObj = new Date(startInt[2], startInt[1] - 1, startInt[0], Number(this.startHour), Number(this.startMin))
    let startTime = startDateObj.getTime()
    let startResp = this.manualService.judgeDSTDate(startDateObj, 'start', this.addZero(this.startHour))
    if (startResp['type'] === '2') {
      EventService.emit('DST_JUMPING_TOAST')
      return
    } else if (startResp['type'] === '3') {
      startTime = startResp['correctMSeconds']
      let currDate = new Date()
      let currDateType = this.manualService.judgeDSTDate(currDate, 'start')
      if (startResp['correctMSeconds'] > currDate.getTime() && currDateType === '3') {
          isDSTSpecialScene = true
        }
    }
    if (!isDSTSpecialScene) {
      let currDateStr = DateUtils.format(Date.now()['getTime'](), 'DD.MM.YYYY')
      if (this.saveSingleStartDate === currDateStr) {
        let timeIllegalFlag = this.checkTimeIsCorrect()
        if (timeIllegalFlag === true) {
            EventService.emit('START_TIME_SHOULD_BE_LATER')
            return
          }
      }
    }
    this.hideQualityList()
    let endDateObj = self.getEndDateObj(startInt)
    let endTime = endDateObj.getTime()
    let endResp = this.manualService.judgeDSTDate(endDateObj, 'end', this.addZero(this.endHour))
    if (endResp['type'] === '2') {
      EventService.emit('DST_JUMPING_TOAST')
      return
    } else if (endResp['type'] === '3') {
      endTime = endResp['correctMSeconds']
    }
    data = {
      name: this.recordName,
      channelID: this.channelItem['ID'],
      policyType: 'TimeBased',
      startTime: startTime + '',
      endTime: endTime + '',
      extensionFields: [
        {
          key: 'channelName',
          values: [this.channelItem['name']]
        }
      ]
    }
    self.getStorageType(data)
    self.getCPVRMediaID(data)
    type = 'TimeBasedPVR'
    this.recordDataService.addPVR(data, type).then(resp => {
      if (resp['isNPVRSpaceEnough'] && resp['isNPVRSpaceEnough'] === '0') {
        EventService.emit('RECORD_SPACE_NOT_ENOUGH', { type: 'successful' })
      } else {
        EventService.emit('ADD_RECORDED_SUCCESS')
      }
      self.backToInitialSet()
      if (_.isUndefined(data['cpvrMediaID'])) {
        EventService.emit('DELETESPACE', 'querypvrspace9')
      }
    }, resp => {
      let conflictData = {}
      if (resp['result']['retCode'] === '126014701') {
        EventService.emit('ADD_RECORDED_SUCCESS')
        self.backToInitialSet()
        if (_.isUndefined(data['cpvrMediaID'])) {
            EventService.emit('DELETESPACE', 'querypvrspace9')
          }
      } else if (resp['result']['retCode'] === '147020005') {
          EventService.emit('RECORD_SPACE_NOT_ENOUGH', { type: 'failed' })
        } else if (resp.conflictGroups && resp.conflictGroups !== '') {
          conflictData = { resp: resp, data: data, type: type }
          EventService.emit('RECORD_CONFLICT', conflictData)
        }
    })
  }

    /**
     * get end date of PVR.
     * @param {Object} startInt [message about start date]
     */
  getEndDateObj (startInt) {
    let endDateObj
    if (this.showNextDayIcon === true) {
      let startDay = new Date(startInt[2], startInt[1] - 1, startInt[0])
      let nextDay = new Date(startDay.setDate(startDay.getDate() + 1))
      endDateObj = new Date(nextDay.getFullYear(), nextDay.getMonth(), nextDay.getDate(), Number(this.endHour), Number(this.endMin))
    } else {
      endDateObj = new Date(startInt[2], startInt[1] - 1, startInt[0], Number(this.endHour), Number(this.endMin))
    }
    return endDateObj
  }

    /**
     * get storage type of CPVR.
     * @param {Object} data [message about PVR]
     */
  getStorageType (data) {
    if (_.isEqual(this.chooseStorage, [true, false])) {
      data['storageType'] = 'NPVR'
    } else if (_.isEqual(this.chooseStorage, [false, true])) {
      data['storageType'] = 'CPVR'
    } else if (_.isEqual(this.chooseStorage, [true, true])) {
        data['storageType'] = 'MIXPVR'
      }
  }

    /**
     * get media ID of CPVR.
     * @param {Object} data [message about PVR]
     */
  getCPVRMediaID (data) {
    if (this.chooseStorage[1] === true) {
      data['cpvrMediaID'] = this.curQualityObj['cpvrMediaID']
    }
  }

    /**
     * add period recording task.
     */
  addPeriodRecord (event?) {
    let self = this
    let data, type
    this.saveStartDate = this.commonService.dealDateToNumber(this.startDate)
    let dateLegal = this.manualService.isRepeatAndDateLegal(this.repeat, this.repeatIndex, this.saveStartDate, this.saveEndDate)
    if (dateLegal === false) {
      EventService.emit('REPEAT_AND_DATE_ILLEGAL')
      return
    }
    this.hideQualityList()
    let effectArr = this.saveStartDate.split('.')
    if (session.get('languageName') !== 'zh') {
      effectArr[0] = effectArr[0].slice(0, effectArr[0].length)
    }
    effectArr[0] = this.addZero(effectArr[0])
    let effectiveDate = effectArr[2] + effectArr[1] + effectArr[0]
    let expireDate = self.getExpireDate()
    let stringDays = _.map(this.repeatIndex, day => {
      return day + ''
    })
    data = {
      name: this.recordName,
      days: stringDays,
      beginDayTime: this.addZero(this.startHour) + this.addZero(this.startMin) + '00',
      endDayTime: this.addZero(this.endHour) + this.addZero(this.endMin) + '00',
      effectiveDate: effectiveDate,
      expireDate: expireDate,
      channelID: this.channelItem['ID'],
      policyType: 'Period',
      extensionFields: [
        {
          key: 'channelName',
          values: [this.channelItem['name']]
        }
      ]
    }
    if (_.isEqual(this.chooseStorage, [true, false])) {
      data['storageType'] = 'NPVR'
    } else if (_.isEqual(this.chooseStorage, [false, true])) {
      data['storageType'] = 'CPVR'
    } else if (_.isEqual(this.chooseStorage, [true, true])) {
        data['storageType'] = 'MIXPVR'
      }
    if (this.chooseStorage[1] === true) {
      data['cpvrMediaID'] = this.curQualityObj['cpvrMediaID']
    }
    type = 'PeriodPVR'
    self.recordDataService.addPeriodicPVR(data, type, event).then(resp => {
      if (resp['isNPVRSpaceEnough'] && resp['isNPVRSpaceEnough'] === '0') {
        EventService.emit('RECORD_SPACE_NOT_ENOUGH', { type: 'successful' })
      } else {
        EventService.emit('ADD_RECORDED_SUCCESS')
      }
      self.backToInitialSet()
      if (_.isUndefined(data['cpvrMediaID'])) {
        EventService.emit('DELETESPACE', 'querypvrspace10')
      }
    }, resp => {
      let conflictData = {}
      if (resp['result']['retCode'] === '126014701') {
        EventService.emit('ADD_RECORDED_SUCCESS')
        self.backToInitialSet()
        if (_.isUndefined(data['cpvrMediaID'])) {
            EventService.emit('DELETESPACE', 'querypvrspace10')
          }
      } else if (resp['result']['retCode'] === '147020005') {
          EventService.emit('RECORD_SPACE_NOT_ENOUGH', { type: 'failed' })
        } else if (resp.conflictGroups && resp.conflictGroups !== '') {
          conflictData = { resp: resp, data: data, type: type }
          if (resp['conflictGroups'][0]['PVRList'][0]['policyType'] === 'PlaybillBased' ||
                    resp['conflictGroups'][0]['PVRList'][0]['policyType'] === 'TimeBased') {
            EventService.emit('MIXED_CONFLICT', conflictData)
          } else {
            EventService.emit('RECORD_CONFLICT', conflictData)
          }
        }
    })
  }

    /**
     * get expire date.
     */
  getExpireDate () {
    let expireDate
    if (this.endDate === 'unlimited') {
      expireDate = '20991231'
    } else if (this.endDate === 'seven_days_later') {
      expireDate = this.manualService.getExpireDateByInterval(this.saveStartDate, 7, 'YYYYMMDD')
    } else if (this.endDate === 'fourteen_days_later') {
        expireDate = this.manualService.getExpireDateByInterval(this.saveStartDate, 14, 'YYYYMMDD')
      } else if (this.endDate === 'thirty_days_later') {
        expireDate = this.manualService.getExpireDateByInterval(this.saveStartDate, 30, 'YYYYMMDD')
      } else {
        let expireArr = this.saveEndDate.split('.')
        if (session.get('languageName') !== 'zh') {
          expireArr[0] = expireArr[0].slice(0, expireArr[0].length)
        }
        expireArr[0] = this.addZero(expireArr[0])
        expireDate = expireArr[2] + expireArr[1] + expireArr[0]
      }
    return expireDate
  }

    /**
     * show the buttom style according to status.
     */
  getRecordClass () {
    let className = ''
    if (this.isNameEmpty) {
      className = 'disable-button'
    } else {
      if (!this.hasCommitRecord) {
        className = 'able-button'
      } else {
        if (this.recordDeactivate.conditionChanged) {
            className = 'able-button'
          } else {
            className = 'disable-button'
          }
      }
    }
    return className
  }

    /**
     * after click the record,return the various of status.
     */
  backToInitialSet () {
    this.chooseTimeIndex = -1
    this.showNextDayIcon = false
    this.recordDeactivate.conditionChanged = false
    this.hasCommitRecord = true
    this.canAutoChangeName = true
    this.isSingle = true
    this.channelItem = this.channelNameDate[0]
    this.curQualityObj = this.findUpNearQuality(this.channelItem['qualityList'], this.defaultQualityKey, true)
    this.recoredDate = 'recored_date'
    this.getCurrentTimeInfo()
    this.initDatePeriod()
    this.initTimePeriod()
    this.userChangeStorage = false
    this.getStorage(this.channelItem)
    this.getInitRecordName()
    this.setDefaultRepeat()
    this.recordStartTime.startTimestamp = 0
    this.recordChannelName.isChooseIndex = 0
  }

    /**
     * get dom element by element id.
     * @param {string} divName [element id]
     */
  dom (divName: string) {
    return document.getElementById(divName)
  }

    /**
     * mouse enter the arrow button.
     * @param {Object} e [mouseenter event]
     */
  enterChangeButton (e) {
    this.isEnterChangeButton = true
  }

    /**
     * mouse leave the arrow button.
     * @param {Object} e [mouseleave event]
     */
  outChangeButton (e) {
    this.isEnterChangeButton = false
    if (this.chooseTimeIndex === ONE) {
      this.dom('start-hour').focus()
    }
    if (this.chooseTimeIndex === TWO) {
      this.dom('start-min').focus()
    }
    if (this.chooseTimeIndex === THREE) {
      this.dom('end-hour').focus()
    }
    if (this.chooseTimeIndex === FOUR) {
      this.dom('end-min').focus()
    }
  }
}
