import * as _ from 'underscore'
import { Component, OnInit, Output, EventEmitter } from '@angular/core'
import { DirectionScroll } from 'ng-epg-ui/webtv-components/scroll/directionScroll.component'
import { DateUtils } from 'ng-epg-sdk/utils'
import { session } from 'src/app/shared/services/session'
import { CommonService } from '../../../../core/common.service'

const ONE = 1

@Component({
  selector: 'app-record-start-time',
  styleUrls: ['./recordStartTime.component.scss'],
  templateUrl: './recordStartTime.component.html'
})

export class RecordStartTimeComponent implements OnInit {
  @Output() selectPeriodDate: EventEmitter<Object> = new EventEmitter()
  public recordTimeDates = []
  public recordEndTimeDates = []
  public isShowStartTime = false
  public isShowEndTime = false
  public isChooseIndex = 0
  public startdateDatas
  public title
  public chooseTimeDate
  public types
  public startTimestamp
  public dateObjStr = []
  private minLength = 8
  constructor (
        private directionScroll: DirectionScroll,
        private commonService: CommonService
    ) { }

  ngOnInit () {
    this.recordEndTimeDates = ['unlimited', 'thirty_days_later', 'fourteen_days_later', 'seven_days_later']
    this.startdateDatas = DateUtils.format(Date.now(), 'DD.MM.YYYY')
  }

    /**
     * close the dialog.
     */
  close () {
    this.isShowStartTime = false
    this.isShowEndTime = false
  }

    /**
     * get current time.
     */
  getCurrentTime (startDate, type) {
    if (!startDate && type === 'start') {
      startDate = Date.now()['getTime']()
    }
    let dstExtraTime = 0
    let nowDate = new Date(startDate).getDate()
    let nextDate = new Date(startDate + 86400000).getDate()
    if (nowDate === nextDate) {
      dstExtraTime = 60 * 60 * 1000
    }
    let length
    let playbillLen = session.get('PLAYBILL_LEN')
    if (!_.isUndefined(playbillLen)) {
      length = Number(playbillLen)
    } else {
      length = 7
    }
    if (type === 'end') {
      length = 29
    }
      /**
         * set time format
         */
    for (let i = 0; i <= length; i++) {
      let dateObj
      if (type === 'start') {
        dateObj = new Date(startDate + 86400000 * i + dstExtraTime)
      } else {
        dateObj = new Date(startDate + 86400000 * (i + 1) + dstExtraTime)
      }
      let month = dateObj.getMonth() + 1 + ''
      if (month.length === ONE) {
        month = '0' + month
      }
      let day = dateObj.getDate() + ''
      let year = dateObj.getFullYear() + ''
      let lastTime
      month = this.commonService.dealWithTime(month)
      if (session.get('languageName') === 'zh') {
        lastTime = year + '/' + month + '/' + day
      } else {
        lastTime = month + ' ' + day + ', ' + year
      }
      let curDateStr = dateObj.getTime()
      curDateStr = this.commonService.dealWithDateJson('D02', curDateStr)
      this.dateObjStr.push(curDateStr)
      let whiteDateStr = [curDateStr]
      let dataDate = [lastTime]
      if (type === 'start') {
        this.dateObjStr[i] = [whiteDateStr, dataDate]
      } else {
        this.dateObjStr[i + 4] = [whiteDateStr, dataDate]
      }
      this.recordTimeDates.push(lastTime)
    }
  }

    /**
     * show the date to choose according to the get date to get.
     */
  loadDateData (type, startDate, endDate) {
    this.isChooseIndex = -1
    this.types = type
    let curTime = Date.now()['getTime']()
    let startDateArray = startDate.replace(',', '').split('.')
    if (type === 'start') {
      startDateArray[1] = this.commonService.dealWithTime(startDateArray[1])
      if (startDateArray[0].charAt(0) === '0') {
        startDateArray[0] = startDateArray[0].charAt(1)
      }
      let dateFirst
      let dateSecond
      if (session.get('languageName') === 'zh') {
        dateFirst = startDateArray[2] + '/' + startDateArray[1] + '/' + startDateArray[0]
      } else {
        dateSecond = startDateArray[1] + ' ' + startDateArray[0] + ', ' + startDateArray[2]
      }
      this.minLength = 8
      this.recordTimeDates = []
      this.dateObjStr = []
      this.getCurrentTime(curTime, 'start')
      this.title = 'record_start_date'
      document.querySelector('.recordTimeSelect')['style']['height'] = '449px'
      _.each(this.recordTimeDates, (item, index) => {
        if (dateFirst === item || dateSecond === item) {
            this.isChooseIndex = index
          }
      })
      if (this.dom('recordStartTimePanel') && this.dom('recordStartTimePanel')['style']) {
        this.dom('recordStartTimePanel')['style']['top'] = '0px'
      }
    } else {
      let dateFirst
      let dateSecond
      if (endDate !== 'unlimited' && endDate !== 'thirty_days_later' &&
                endDate !== 'fourteen_days_later' && endDate !== 'seven_days_later') {
        let endDateArray = endDate.split('.')
        endDateArray[1] = this.commonService.dealWithTime(endDateArray[1])
        if (endDateArray[0].charAt(0) === '0') {
            endDateArray[0] = endDateArray[0].charAt(1)
          }
        if (session.get('languageName') === 'zh') {
            dateFirst = endDateArray[2] + '/' + endDateArray[1] + '/' + endDateArray[0]
          } else {
            dateSecond = endDateArray[1] + ' ' + endDateArray[0] + ', ' + endDateArray[2]
          }
      } else {
        dateFirst = endDate
      }
      this.minLength = 11
      this.recordTimeDates = ['unlimited', 'thirty_days_later', 'fourteen_days_later', 'seven_days_later']
      this.dateObjStr = []
      this.dateObjStr = [[['unlimited'], ['unlimited']], [['thirty_days_later'], ['thirty_days_later']],
          [['fourteen_days_later'], ['fourteen_days_later']], [['seven_days_later'], ['seven_days_later']]]
      let startDateObj = new Date(Number(startDateArray[2]), Number(startDateArray[1]) - 1, Number(startDateArray[0]))
      this.startTimestamp = startDateObj.getTime()
      this.getCurrentTime(this.startTimestamp, 'end')
      this.title = 'record_end_date'
      document.querySelector('.recordTimeSelect')['style']['height'] = '587px'
      _.each(this.recordTimeDates, (item, index) => {
        if (dateFirst === item || dateSecond === item) {
            this.isChooseIndex = index
          }
      })
      if (this.dom('recordEndTimePanel') && this.dom('recordEndTimePanel')['style']) {
        this.dom('recordEndTimePanel')['style']['top'] = '0px'
      }
    }
    _.delay(() => {
      this.pluseOnScroll(this.recordTimeDates.length)
    }, 100)
  }

    /**
     * choose the date of record start or end
     */
  chooseData (i) {
    this.isChooseIndex = i
    this.chooseTimeDate = this.dateObjStr[i][1][0]
    let whiteChooseTimeDate = this.dateObjStr[i][0][0]
    let outPutData = {
      'chooseTimeDate': this.chooseTimeDate,
      'types': this.types,
      'whiteChooseTimeDate': whiteChooseTimeDate
    }
    if (this.types === 'start') {
      this.startTimestamp = Date.now()['getTime']() + 86400000 * i
    }
    this.selectPeriodDate.emit(outPutData)
    this.close()
  }

    /**
     * get dom element by element id.
     * @param {string} divName [element id]
     */
  dom (divName: string) {
    return document.getElementById(divName)
  }

    /**
     * set scroll bar.
     * @param {Number} length [length of date list]
     */
  pluseOnScroll (length) {
    if (length > this.minLength) {
      let oBox = this.dom('recordStartContent')
      let oConter, oUl, oScroll, oSpan
      if (this.types === 'start') {
        oConter = this.dom('recordStartTimewrap')
        oUl = this.dom('recordStartTimePanel')
      } else {
        oConter = this.dom('recordEndTimewrap')
        oUl = this.dom('recordEndTimePanel')
      }
      oScroll = this.dom('record-scroll')
      if (oScroll && oScroll.getElementsByTagName('span')) {
        oSpan = oScroll.getElementsByTagName('span')[0]
      }
      this.directionScroll.setScroll(oBox, oConter, oUl, oScroll, oSpan)
    }
  }
}
