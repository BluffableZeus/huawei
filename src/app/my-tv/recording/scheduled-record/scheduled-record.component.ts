import * as _ from 'underscore'
import { Component, ViewChild, OnInit, OnDestroy } from '@angular/core'
import { RecordingService } from '../recording.service'
import { EventService } from 'ng-epg-sdk/services'
import { ScheduledService } from './scheduled-record.service'
import { RecordDataService } from '../../../component/recordDialog/recordData.service'
import { config } from '../../../shared/services/config'
import { session } from 'src/app/shared/services/session'
import { MyRecordings } from 'ng-epg-ui/webtv-components/myRecordings/my-recordings.component'

const QUERYPVR_OPTIONS = {
  params: {
    SID: 'querypvr3'
  }
}

const QUERYPVR_OPTIONS_FOR_CHILD = {
  params: {
    SID: 'querypvr6'
  }
}

const QUERYPVRSPACE_OPTIONS = {
  params: {
    SID: 'querypvrspace2'
  }
}

const QUERYPVRSPACE_OPTIONS_FOR_CHILD = {
  params: {
    SID: 'querypvrspace13'
  }
}

@Component({
  selector: 'app-scheduled-recording',
  styleUrls: ['./scheduled-record.component.scss'],
  templateUrl: './scheduled-record.component.html'
})

export class ScheduledRecordingComponent implements OnInit, OnDestroy {
  @ViewChild(MyRecordings) myRecordings: MyRecordings
  recordFilterLists: {
    'dataList': Array<any>,
    'notEnoughStartTime': string,
    'clientW': number,
    'suspensionID': string
  }
  recordFilterListss: {
    'dataList': Array<any>,
    'notEnoughStartTime': string,
    'clientW': number,
    'suspensionID': string
  }
  public hasType = true
  public filterData = true
  public showNO = false
    /**
     * current page has child recording task
     */
  public isSeriesPage = true
    /**
     * init sort filter
     */
  public sort = [{
    ID: 'STARTTIME:ASC',
    name: 'first_distance'
  }, {
    ID: 'STARTTIME:DESC',
    name: 'older_distance'
  }]
    /**
     * init record type filter
     */
  public type = [{
    ID: 'all',
    name: 'searchAll'
  }, {
    ID: 'single',
    name: 'single'
  }, {
    ID: 'series',
    name: 'series'
  }]
    /**
     * init storage filter
     */
  public storage = [{
    ID: 'all',
    name: 'searchAll'
  }, {
    ID: 'cloud',
    name: 'recored_cloud'
  }, {
    ID: 'stb',
    name: 'record_STB'
  }]

    /**
     * record mode,including CPVR，NPVR，CPVR&NPVR, MIXPVR
     */
  public recordingMode = config.PVRMode
  public filterDataList = []
    /**
     * index of current sort filter, the default sort filter is STARTTIME:ASC
     */
  public sortIndex = 0
    /**
     * index of current record type filter, the default sort filter is all
     */
  public typeIndex = 0
    /**
     * index of current storage filter, the default sort filter is all
     */
  public storageIndex = 0
  public recordFilterList: Array<any> = []
  public recordFilterListSeries: Array<any> = []
    /**
     * the name of series record
     */
  public seriesName = ''
  public scrollTop = 0
  public filter

    /**
     * whether has recording
     */
  public hasData = true
    /**
     * edit status
     */
  public isEdit = false
  public deleteSpaceFuture
    /**
     * new information of recording after change recording settings
     */
  public changeData
    /**
    * the number of milliseconds in the range from January 1, 1970：
    * if usedSize + bookingSize > totalSize, this field is the start time of a space deficit PVR task nearest the current time,
    * representing all the recording tasks after this time, and the space is expected to be insufficient.
    * if the space is enough, return 0.
    */
  public notEnoughStartTime
    /**
     * id of series record task
     */
  public seriesId
  public total
  public totalSeries
  public isDelete = false
  public PVRSpaceReq = 'NPVR'

    /**
     * show load circle before show data
     */
  public showLoadCycle = false

  constructor (private recordingService: RecordingService,
        private scheduledService: ScheduledService,
        private recordDataService: RecordDataService) { }

  ngOnInit () {
    this.sort = [{
      ID: 'STARTTIME:ASC',
      name: 'first_distance'
    }, {
      ID: 'STARTTIME:DESC',
      name: 'older_distance'
    }]
    this.type = [{
      ID: 'all',
      name: 'searchAll'
    }, {
      ID: 'single',
      name: 'single'
    }, {
        ID: 'series',
        name: 'series'
      }]
    this.storage = [{
      ID: 'all',
      name: 'searchAll'
    }, {
      ID: 'cloud',
      name: 'recored_cloud'
    }, {
        ID: 'stb',
        name: 'record_STB'
      }]
    let self = this
    this.recordingMode = config.PVRMode

      // init the selected filter condition
    self.selectFilter('STARTTIME:ASC', 'sort', '0', 'init')
    this.hasType = true
    this.deleteSpaceFuture = 0

      // listen to delete all recording tasks event
    EventService.removeAllListeners(['DELETEALL'])
    EventService.on('DELETEALL', function () {
      self.deleteAll()
    })

      // listen to click the edit button event
    EventService.removeAllListeners(['DELETES'])
    EventService.on('DELETES', function () {
      self.isEdit = true
    })

      // listen to click the complete button event;
      // listen to click series name and return back to the first page event.
    EventService.removeAllListeners(['COMPLETES'])
    EventService.on('COMPLETES', function () {
      self.isEdit = false
    })

      // listen to change single recording to series recording, add series recording successfully event.
    EventService.removeAllListeners(['RECORD_UPDATE_ADD_SUCCESS'])
    EventService.on('RECORD_UPDATE_ADD_SUCCESS', () => {
        // send show operation succeeded event.
      EventService.emit('PROFILE_INFO_SAVE_SUCCESSFUL')
      self.refresh()
    })

      // listen to click the confirm button and update recording task event in recording settings page
    EventService.removeAllListeners(['MANUAL_SET_SUCCESS'])
    EventService.on('MANUAL_SET_SUCCESS', function (data) {
        // send show operation succeeded event.
      EventService.emit('PROFILE_INFO_SAVE_SUCCESSFUL')
        // send change recording name event.
      EventService.emit('MANUAL_CHANGE_NAME', data)
      self.changeData = data
    })

      // listen to update the period recording task event.
    EventService.removeAllListeners(['MANUAL_PERIOD_SET_SUCCESS'])
    EventService.on('MANUAL_PERIOD_SET_SUCCESS', function (data) {
        // send show operation succeeded event.
      EventService.emit('PROFILE_INFO_SAVE_SUCCESSFUL')
      if (data['effectiveDate'] || data['expireDate'] || data['beginDayTime'] || data['endDayTime'] || data['days']) {
        self.refresh()
      } else {
          // send change recording name event.
        EventService.emit('MANUAL_CHANGE_NAME', data)
      }
    })
  }

    /**
     * remove listeners on destroy components
     */
  ngOnDestroy () {
    EventService.removeAllListeners(['LOAD_DATA'])
  }

    /**
     * query whether has enough space, if not, get the start time of a space deficit PVR task nearest the current time
     */
  queryNotEnoughStartTime () {
    let self = this
    if (self.recordingMode !== 'CPVR') {
      this.recordDataService.queryPVRSpace(self.PVRSpaceReq, QUERYPVRSPACE_OPTIONS).then((resp) => {
        self.notEnoughStartTime = resp['NPVRStorage'] && resp['NPVRStorage'].notEnoughStartTime
      })
    }
  }

    /**
     * refresh recording data
     */
  refresh () {
    let self = this
    this.filter = session.get('filterFlag')
    if (this.filter.PVRCondition.singlePVRFilter.storageType !== 'CPVR') {
      if (self.recordingMode !== 'CPVR') {
        this.recordDataService.queryPVRSpace(self.PVRSpaceReq, QUERYPVRSPACE_OPTIONS).then((resp) => {
            self.notEnoughStartTime = resp['NPVRStorage'] && resp['NPVRStorage'].notEnoughStartTime
            self.getDataScheduled()
          })
      }
    } else {
      self.getDataScheduled()
    }
  }

    /**
     * refresh recording data
     */
  refreshData (data) {
    let PVRCondition = _.extend([], this.filter.PVRCondition)
    let PVRConditions
    let layout = { childPlanLayout: '1' }
    let singlePVRFilter
    singlePVRFilter = {
      scope: PVRCondition.singlePVRFilter.scope,
      status: ['WAIT_RECORD'],
      storageType: PVRCondition.singlePVRFilter.storageType,
      isFilterByDevice: '0',
      periodicPVRID: data
    }
    PVRConditions = {
      policyType: PVRCondition.policyType,
      singlePVRFilter: singlePVRFilter,
      periodicPVRFilter: PVRCondition.periodicPVRFilter,
      layout: layout
    }
    let queryPVRRequest = {
      count: '12',
      offset: '0',
      sortType: this.filter.sortType,
      PVRCondition: this.filter.PVRCondition
    }
    this.recordDataService.queryPVR(queryPVRRequest, QUERYPVR_OPTIONS).then((resp) => {
      if (resp.PVRList && resp.PVRList.length > 0) {
        let filterList = this.recordingService.getFilterRecord(resp.PVRList, 0, 'sche', 'series', this.notEnoughStartTime)
        EventService.emit('CHANGE_SERIES_DATA', filterList[0])
      }
    })
  }

    /**
     * click the series name in detail records page of series record, it will return to the first page
     */
  returnFirstPage () {
    if (this.showLoadCycle) {
      return
    }
    this.hasType = true
    this.isSeriesPage = true
    this.showNO = false
    this.filterData = true
    _.delay(() => {
      document.body.scrollTop = this.scrollTop
    }, 0)
      // back to the first page, get original filter data list to show them
    let filterDataList = session.get('seriesFilterDataList')
    for (let i = 0; i < filterDataList.length; i++) {
      if (filterDataList[i].type === 'sort') {
        this.sortIndex = filterDataList[i].index
      }
      if (filterDataList[i].type === 'type') {
        this.typeIndex = filterDataList[i].index
      }
      if (filterDataList[i].type === 'storage') {
        this.storageIndex = filterDataList[i].index
      }
    }
    session.pop('seriesFilterDataList')
    if (this.isDelete) {
      this.refresh()
    }
    this.isDelete = false
    EventService.emit('EXIT_EDIT')
    EventService.emit('COMPLETES', '1')
  }

    /**
     * delete a recording task.
     */
  deleteRecord ($event) {
    let self = this
    let req
    if ($event.data.policyType === 'Series' || $event.data.policyType === 'Period') {
        // the record to be deleted is series record or periodic record
      req = [$event.data.floatInfo.ID]
      this.recordDataService.deletePVRByIDWait(this.filter.PVRCondition.singlePVRFilter.storageType, req).then((resp) => {
        if (resp['result']['retCode'] === '000000000') {
            if ($event.data.floatInfo.storageType !== 'CPVR') {
              let options = {
                params: {
                  SID: 'querypvrspacebyid2'
                }
              }
              self.recordDataService.queryPVRSpaceByID($event.data.floatInfo.childPVRIDs,
                $event.data.floatInfo.storageType, options).then((resps) => {
                  self.deleteSpaceFuture = self.deleteSpaceFuture + resps
                })
            }
            self.myRecordings.dataList.splice($event['indexDelete'], 1)
            if (self.myRecordings.dataList.length === 0) {
              self.filterData = false
              self.showNO = true
              self.hasData = false
              EventService.emit('NO_RECORD_EDIT')
            }
            if ($event.data.floatInfo.storageType === 'NPVR') {
              EventService.emit('DELETESPACE', 'querypvrspace11')
            }
          }
      })
    } else {
        // the record to be deleted is timebased record
      req = [$event.data.floatInfo.ID]
      this.recordDataService.deletePVRByID(this.filter.PVRCondition.singlePVRFilter.storageType, req).then((resp) => {
        if (resp['result']['retCode'] === '000000000') {
            if ($event.data.isSeriesPage === 'series') {
              self.isDelete = true
              let originalName = self.seriesName
              let lastIndex = originalName.lastIndexOf('(')
              let nameHead = originalName.substring(0, lastIndex)
              let nameTail = originalName.substring(lastIndex)
              let taskLength = Number(nameTail.replace(/[^0-9]/ig, '')) - 1
              self.seriesName = nameHead + '( ' + taskLength + ' )'
            }
            self.myRecordings.dataList.splice($event['indexDelete'], 1)
            if (self.myRecordings.dataList.length === 0) {
              self.filterData = false
              self.showNO = true
              self.hasData = false
              EventService.emit('NO_RECORD_EDIT')
            }
            if ($event.data.floatInfo.storageType === 'NPVR') {
              EventService.emit('DELETESPACE', 'querypvrspace11')
            }
          }
      })
      this.deleteSpaceFuture = this.deleteSpaceFuture + $event.data.floatInfo.duration
    }
  }

    /**
     * delete all recording task.
     */
  deleteAll () {
    let self = this
    let req
    this.isEdit = false
    req = this.filter.PVRCondition
    this.recordDataService.deletePVRByCondition(req.singlePVRFilter.storageType, req).then((resp) => {
      if (resp['result']['retCode'] === '000000000') {
        if (!this.isSeriesPage) {
            let originalName = self.seriesName
            let lastIndex = originalName.lastIndexOf('(')
            let nameHead = originalName.substring(0, lastIndex)
            let nameTail = '( 0 )'
            self.seriesName = nameHead + nameTail
          }
        if (self.hasType) {
            self.hasData = false
            self.filterData = false
            self.showNO = true
          } else {
            self.filterData = false
            self.showNO = true
          }
        self.isDelete = true
        EventService.emit('DELETESPACE', 'querypvrspace12')
      }
    })
  }

    /**
     * click series record in the schedule to get detail records.
     */
  detailRecord ($event) {
    let self = this
      // if click the series record to delete, it will not show detail records.
    if (this.isEdit) {
      return
    }
    this.isSeriesPage = false
    this.seriesName = $event.name
    this.scheduledService.createPVRConditions(session.get('seriesPageFilterDataList'), this.recordingMode, $event.id).then(resps => {
      self.filter = resps
      if (this.filter.PVRCondition.singlePVRFilter.storageType !== 'CPVR') {
        if (self.recordingMode !== 'CPVR') {
            this.recordDataService.queryPVRSpace(self.PVRSpaceReq, QUERYPVRSPACE_OPTIONS_FOR_CHILD).then((resp) => {
              self.notEnoughStartTime = resp['NPVRStorage'] && resp['NPVRStorage'].notEnoughStartTime
              self.showData()
            })
          } else {
            self.showData()
          }
      } else {
        self.showData()
      }
    })
      // get current filter data list and save them in new session
    session.put('seriesFilterDataList', _.extend([], session.get('seriesPageFilterDataList')))
    this.seriesId = $event.id
  }

    /**
     * click filter to filter
     * @param {Object} list [list of filter]
     * @param {Object} type [type of filter]
     * @param {Object} index [index of selected filter in filter]
     * @param {Object} flag? [carry on init]
     */
  selectFilter (list, type, index, flag?) {
    if (this.showLoadCycle) {
      return
    }
    let self = this
    if (flag) {
        // init variable
      self.filterDataList = []
      self.sortIndex = 0
      self.typeIndex = 0
      self.storageIndex = 0
    }
    if (!self.isEdit) {
      let lastChoice
      switch (type) {
        case 'sort':
          lastChoice = _.find(this.filterDataList, item => {
              return item['type'] === 'sort'
            })
          self.getFilterDataList(lastChoice)
          this.sortIndex = index
          break
        case 'type':
          lastChoice = _.find(this.filterDataList, item => {
              return item['type'] === 'type'
            })
          self.getFilterDataList(lastChoice)
          this.typeIndex = index
          break
        case 'storage':
          lastChoice = _.find(this.filterDataList, item => {
              return item['type'] === 'storage'
            })
          self.getFilterDataList(lastChoice)
          this.storageIndex = index
          break
      }
        // add the filter to filterDataList
      self.filterDataList.push({ name: list, type: type, index: index })
        // if current page has series record, save filter data list in session
      if (self.isSeriesPage) {
        session.put('seriesPageFilterDataList', _.extend([], self.filterDataList))
      }
      if (self.hasType) {
        self.scheduledService.createPVRConditions(self.filterDataList, self.recordingMode).then(resp => {
            self.filter = resp
            if (flag) {
              self.judgeBeforeQuery(flag)
            } else {
              self.judgeBeforeQuery()
            }
          })
      } else {
        self.scheduledService.createPVRConditions(self.filterDataList, self.recordingMode, self.seriesId).then(resp => {
            self.filter = resp
            if (flag) {
              self.judgeBeforeQuery(flag)
            } else {
              self.judgeBeforeQuery()
            }
          })
      }
    }
  }

    /**
     * get filterDataList
     * @param {Object} lastChoice [last selected filterData]
     */
  getFilterDataList (lastChoice) {
    if (lastChoice) {
      let choiceIndex = _.indexOf(this.filterDataList, lastChoice)
      this.filterDataList.splice(choiceIndex, 1)
    }
  }

    /**
     * query pvr space before query pvr task.
     */
  judgeBeforeQuery (flag?) {
    let self = this
    if (self.filter.PVRCondition.singlePVRFilter.storageType !== 'CPVR') {
      if (self.recordingMode !== 'CPVR') {
        self.recordDataService.queryPVRSpace(self.PVRSpaceReq, QUERYPVRSPACE_OPTIONS).then((resp) => {
            EventService.emit('SPACE_RESPONSE_FROM_SCHED', resp)
            self.notEnoughStartTime = resp['NPVRStorage'] && resp['NPVRStorage'].notEnoughStartTime
            if (self.hasType) {
              if (flag) {
                self.getDataScheduled(flag)
                self.hasData = true
              } else {
                self.getDataScheduled()
                self.hasData = true
              }
            } else {
              self.showData()
            }
          })
      } else {
        if (this.hasType) {
            if (flag) {
              self.getDataScheduled(flag)
              self.hasData = true
            } else {
              self.getDataScheduled()
              self.hasData = true
            }
          } else {
            self.showData()
          }
      }
    } else {
      if (this.hasType) {
        if (flag) {
            self.getDataScheduled(flag)
            self.hasData = true
          } else {
            self.getDataScheduled()
            self.hasData = true
          }
      } else {
        self.showData()
      }
    }
  }

    /**
     * query pvr tasks of child page.
     */
  showData (option?) {
    let self = this
    this.recordFilterListSeries.length = 0
    this.filterData = false
    this.showNO = false
    this.recordFilterListss = {
      'dataList': this.recordFilterList,
      'notEnoughStartTime': 'All',
      'clientW': document.body.clientWidth,
      'suspensionID': 'filterRecord'
    }
    let queryPVRRequest = {
      count: '12',
      offset: '0',
      sortType: this.filter.sortType,
      PVRCondition: this.filter.PVRCondition
    }
    this.secondPageQuerySched(queryPVRRequest)
    EventService.removeAllListeners(['LOAD_DATA'])
    EventService.on('LOAD_DATA', function () {
      if (self.recordFilterListSeries && self.recordFilterListSeries.length &&
                self.recordFilterListSeries.length < self.totalSeries) {
        let offset = self.recordFilterListSeries.length.toString()
        self.showLoadCycle = true
        if (self.filter.PVRCondition.singlePVRFilter.storageType !== 'CPVR') {
            if (self.recordingMode !== 'CPVR') {
              self.recordDataService.queryPVRSpace(self.PVRSpaceReq, QUERYPVRSPACE_OPTIONS_FOR_CHILD).then((resps) => {
                self.notEnoughStartTime = resps['NPVRStorage'] && resps['NPVRStorage'].notEnoughStartTime
                self.getMoreData(offset)
              })
            }
          } else {
            self.getMoreData(offset)
          }
      }
    })
  }

    /**
     * load more pvr tasks of child page.
     */
  getMoreData (offset) {
    let self = this
    let queryPVRRequest = {
      count: '12',
      offset: offset + '',
      sortType: this.filter.sortType,
      PVRCondition: this.filter.PVRCondition
    }
    this.recordDataService.queryPVR(queryPVRRequest, QUERYPVR_OPTIONS_FOR_CHILD).then((resp) => {
      let filterList = self.recordingService.getFilterRecord(resp.PVRList, offset, 'sche', 'series', self.notEnoughStartTime)
      if (filterList.length > 0) {
        let molist = self.recordingService.getFilterRecord(resp.PVRList, offset, 'sche', 'series', self.notEnoughStartTime)
        let moredata = self.recordFilterListSeries.concat(molist)
        self.recordFilterListSeries = moredata
        self.recordFilterListss = {
            'dataList': self.recordFilterListSeries,
            'notEnoughStartTime': 'All',
            'clientW': document.body.clientWidth,
            'suspensionID': 'filterRecord'
          }
      }
      self.showLoadCycle = false
    })
  }

    /**
     * get scheduled record data
     */
  getDataScheduled (flag?) {
    let self = this
    this.showNO = false
    this.recordFilterList.length = 0
    this.recordFilterLists = {
      'dataList': this.recordFilterList,
      'notEnoughStartTime': 'All',
      'clientW': document.body.clientWidth,
      'suspensionID': 'filterRecord'
    }
    session.put('filterFlag', this.filter)
    let queryPVRRequest = {
      count: '12',
      offset: '0',
      sortType: this.filter.sortType,
      PVRCondition: this.filter.PVRCondition
    }
    if (flag) {
      this.firstPageQuerySched(queryPVRRequest, flag)
    } else {
      this.firstPageQuerySched(queryPVRRequest)
    }
    EventService.removeAllListeners(['LOAD_DATA'])
    EventService.on('LOAD_DATA', function () {
      if (self.recordFilterList && self.recordFilterList.length && self.recordFilterList.length < self.total) {
        let offset = self.recordFilterList.length
        self.showLoadCycle = true
          // the cpvr is not need to query the case whether the space is enough.
        if (self.filter.PVRCondition.singlePVRFilter.storageType !== 'CPVR') {
            if (self.recordingMode !== 'CPVR') {
              self.recordDataService.queryPVRSpace(self.PVRSpaceReq, QUERYPVRSPACE_OPTIONS).then((resps) => {
                self.notEnoughStartTime = resps['NPVRStorage'] && resps['NPVRStorage'].notEnoughStartTime
                self.getMoreDataSeries(offset)
              })
            }
          } else {
            self.getMoreDataSeries(offset)
          }
      }
    })
  }

    /**
     * load more pvr tasks of parent page.
     */
  getMoreDataSeries (offset) {
    let self = this
    let queryPVRRequest = {
      count: '12',
      offset: offset + '',
      sortType: this.filter.sortType,
      PVRCondition: this.filter.PVRCondition
    }
    this.recordDataService.queryPVR(queryPVRRequest, QUERYPVR_OPTIONS).then((resp) => {
      let filterList = self.recordingService.getFilterRecord(resp.PVRList, offset, 'sche', '', this.notEnoughStartTime)
      if (filterList.length > 0) {
        let molist = self.recordingService.getFilterRecord(resp.PVRList, offset, 'sche', '', this.notEnoughStartTime)
        let moredata = self.recordFilterList.concat(molist)
        self.recordFilterList = moredata
        self.recordFilterLists = {
            'dataList': self.recordFilterList,
            'notEnoughStartTime': 'All',
            'clientW': document.body.clientWidth,
            'suspensionID': 'filterRecord'
          }
      }
      self.showLoadCycle = false
    })
  }

    /**
     * query pvr tasks at the parent page.
     */
  firstPageQuerySched (queryPVRRequest, flag?) {
    let self = this
    this.showLoadCycle = true
    this.recordDataService.queryPVR(queryPVRRequest, QUERYPVR_OPTIONS).then((resp) => {
      self.total = resp.total
      if (resp.PVRList && resp.PVRList.length > 0) {
        EventService.emit('CAN_RECORD_EDIT')
        self.filterData = true
        let filterList = self.recordingService.getFilterRecord(resp.PVRList, 0, 'sche', '', this.notEnoughStartTime)
        self.recordFilterList = filterList
        self.recordFilterLists = {
            'dataList': filterList,
            'notEnoughStartTime': 'All',
            'clientW': document.body.clientWidth,
            'suspensionID': 'filterVod'
          }
      } else {
        EventService.emit('NO_RECORD_EDIT')
        if (flag) {
            self.filterData = false
            self.hasData = false
            self.showNO = true
          } else {
            if (self.typeIndex === 0 && self.storageIndex === 0) {
              self.hasData = false
            }
            self.filterData = false
            self.showNO = true
          }
      }
      self.showLoadCycle = false
    })
  }

    /**
     * query pvr tasks at the child page.
     */
  secondPageQuerySched (queryPVRRequest) {
    let self = this
    this.showLoadCycle = true
    this.recordDataService.queryPVR(queryPVRRequest, QUERYPVR_OPTIONS_FOR_CHILD).then((resp) => {
      if (!(queryPVRRequest.sortType === self.filter['sortType'] &&
                _.isEqual(queryPVRRequest.PVRCondition, self.filter['PVRCondition']))) {
        return
      }
      self.hasType = false
      self.scrollTop = document.body.scrollTop
      document.body.scrollTop = 0
      document.documentElement.scrollTop = 0
      self.totalSeries = resp.total
      if (resp.PVRList && resp.PVRList.length > 0) {
        EventService.emit('CAN_RECORD_EDIT')
        self.filterData = true
        let filterList = this.recordingService.getFilterRecord(resp.PVRList, 0, 'sche', 'series', this.notEnoughStartTime)
        self.recordFilterListSeries = filterList
        self.recordFilterListss = {
            'dataList': self.recordFilterListSeries,
            'notEnoughStartTime': 'All',
            'clientW': document.body.clientWidth,
            'suspensionID': 'filterVod'
          }
      } else {
        EventService.emit('NO_RECORD_EDIT')
        self.filterData = false
        self.showNO = true
      }
      self.showLoadCycle = false
    })
  }

    /**
     * get element by element id
     * @param {String} divName [value of element id]
     */
  dom (divName: string) {
    return document.getElementById(divName)
  }
}
