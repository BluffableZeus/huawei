import * as _ from 'underscore'
import { TranslateService } from '@ngx-translate/core'
import { Injectable } from '@angular/core'

@Injectable()
export class ScheduledService {
  constructor (
        private translate: TranslateService
  ) { }

  /**
     * create the conditions of PVR filter
     * @param {Object} filterDataList [the list of filter information]
     * @param {String} recordingMode [recording business configuration values：CPVR，NPVR，CPVR&NPVR, MIXPVR]
     * @param {Number} flag? [id of series record task]
     */
  createPVRConditions (filterDataList, recordingMode, flag?) {
    let filter = {}
    let PVRCondition = {}
    let periodicPVRFilter = {}
    let singlePVRFilter = {}
    let layout = {}
    let sortType = ''
    let storageType
    let scope = ''
    let policyType
    storageType = this.getStorage(filterDataList, recordingMode)
    sortType = this.getSortType(filterDataList)
    // get record type filter
    let typeCondition = _.find(filterDataList, item => {
      return item['type'] === 'type'
    })
    let infoArray = this.getInfoByTypeCondition(typeCondition, scope, policyType, periodicPVRFilter)
    scope = infoArray[0]
    policyType = infoArray[1]
    periodicPVRFilter = infoArray[2]

    if (flag) {
      if (storageType) {
        singlePVRFilter = {
          scope: scope,
          storageType: storageType,
          status: ['WAIT_RECORD'],
          isFilterByDevice: '0',
          periodicPVRID: flag
        }
      } else {
        singlePVRFilter = {
          scope: scope,
          status: ['WAIT_RECORD'],
          isFilterByDevice: '0',
          periodicPVRID: flag
        }
      }
      periodicPVRFilter = undefined
      layout = { childPlanLayout: '0' }
      policyType = ['PlaybillBased', 'TimeBased']
    } else {
      if (storageType) {
        singlePVRFilter = {
          scope: scope,
          storageType: storageType,
          isFilterByDevice: '0',
          status: ['WAIT_RECORD']
        }
      } else {
        singlePVRFilter = {
          scope: scope,
          isFilterByDevice: '0',
          status: ['WAIT_RECORD']
        }
      }
      layout = { childPlanLayout: '1' }
    }
    if (periodicPVRFilter) {
      PVRCondition = {
        policyType: policyType,
        singlePVRFilter: singlePVRFilter,
        periodicPVRFilter: periodicPVRFilter,
        layout: layout
      }
    } else {
      PVRCondition = { policyType: policyType, singlePVRFilter: singlePVRFilter, layout: layout }
    }
    if (!_.isUndefined(PVRCondition['periodicPVRFilter']) && storageType !== undefined) {
      PVRCondition['periodicPVRFilter']['storageType'] = storageType
    }
    filter = { sortType: sortType, PVRCondition: PVRCondition }
    return new Promise((resolve, reject) => {
      resolve(filter)
    })
  }

  /**
     * get value of scope, policyType and periodicPVRFilter by typeCondition
     */
  getInfoByTypeCondition (typeCondition, scope, policyType, periodicPVRFilter) {
    let infoArray = []
    if (!_.isUndefined(typeCondition)) {
      if (typeCondition['name'] === 'all') {
        scope = 'ALL'
        periodicPVRFilter = { isOverdue: '0' }
        policyType = ['PlaybillBased', 'TimeBased', 'Series', 'Period']
      }
      if (typeCondition['name'] === 'single') {
        scope = 'NotIncludeChild'
        policyType = ['PlaybillBased', 'TimeBased']
        periodicPVRFilter = undefined
      }
      if (typeCondition['name'] === 'series') {
        scope = 'OnlyChild'
        policyType = ['PlaybillBased', 'TimeBased', 'Series', 'Period']
        periodicPVRFilter = { isOverdue: '0' }
      }
    } else {
      scope = 'ALL'
      policyType = ['PlaybillBased', 'TimeBased', 'Series', 'Period']
      periodicPVRFilter = { isOverdue: '0' }
    }
    infoArray[0] = scope
    infoArray[1] = policyType
    infoArray[2] = periodicPVRFilter
    return infoArray
  }

  /**
     * get the storage filter.
     * @param {Object} filterDataList [the list of filter information]
     * @param {String} recordingMode [recording business configuration values：CPVR，NPVR，CPVR&NPVR, MIXPVR]
     */
  getStorage (filterDataList, recordingMode) {
    let storageType
    if (recordingMode === 'NPVR') {
      storageType = 'NPVR'
    } else if (recordingMode === 'CPVR') {
      storageType = 'CPVR'
    } else {
      let storageCondition = _.find(filterDataList, item => {
        return item['type'] === 'storage'
      })
      if (!_.isUndefined(storageCondition)) {
        if (storageCondition['name'] === 'all') {
          storageType = undefined
        }
        if (storageCondition['name'] === 'cloud') {
          storageType = 'NPVR'
        }
        if (storageCondition['name'] === 'stb') {
          storageType = 'CPVR'
        }
      } else {
        storageType = undefined
      }
    }
    return storageType
  }

  /**
     * get the sort-type filter.
     * @param {Object} filterDataList [the list of filter information]
     */
  getSortType (filterDataList) {
    let sortType
    let sortCondition = _.find(filterDataList, item => {
      return item['type'] === 'sort'
    })
    if (!_.isUndefined(sortCondition)) {
      sortType = sortCondition['name']
    } else {
      sortType = 'STARTTIME:ASC'
    }
    return sortType
  }
}
