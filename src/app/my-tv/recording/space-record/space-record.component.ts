import { Component } from '@angular/core'
import { queryPVRSpace } from 'ng-epg-sdk/vsp'
import { TranslateService } from '@ngx-translate/core'
import { EventService } from 'ng-epg-sdk/services'
import { session } from 'src/app/shared/services/session'

@Component({
  selector: 'app-space-record',
  styleUrls: ['./space-record.component.scss'],
  templateUrl: './space-record.component.html'
})

export class SpaceRecordComponent {
    /**
     * unit of record space
     */
  public unit = ''
    /**
     * total space
     */
  public totalSize = ''
    /**
     * used space
     */
  public usedSize = ''
    /**
     * reservation space
     */
  public bookingSize = ''
  public canSize = ''
    /**
     * width of used space on the progress bar
     */
  public usedPercent = ''
    /**
     * width of reservation space on the progress bar
     */
  public schedPercent = ''
    /**
     * width of available space on the progress bar
     */
  public availPercent = ''
    /**
     * have no enough space
     */
  public spaceEnough = false

  public totalUnit
  public usedUnit
  public bookingUnit
  public canUnit

  constructor (
        private translate: TranslateService) {
    this.queryPVRSpace('querypvrspace1')
    EventService.removeAllListeners(['DELETESPACE'])
    EventService.on('DELETESPACE', (sceneID) => {
      this.queryPVRSpace(sceneID)
    })
    EventService.removeAllListeners(['SPACE_RESPONSE_FROM_SCHED'])
    EventService.on('SPACE_RESPONSE_FROM_SCHED', (resp) => {
      this.handleSpaceData(resp)
    })
  }

    /**
     * handle PVRSpace data
     * @param {Number} usedSize [used space]
     * @param {Number} bookingSize [reservation space]
     * @param {Number} totalSize [total space]
     *
     */
  queryPVRSpaceTwice (usedSize, bookingSize, totalSize) {
    let self = this
    if (self.unit === 'gb') {
      self.usedUnit = Math.ceil(usedSize / 1024 * 10) / 10
      self.bookingUnit = Math.ceil(bookingSize / 1024 * 10) / 10
      self.totalUnit = Math.ceil(totalSize / 1024 * 10) / 10
      self.canUnit = (self.totalUnit - self.usedUnit - self.bookingUnit).toFixed(1)
      self.changeToNumber()
      self.usedSize = self.usedUnit + ''
      self.bookingSize = self.bookingUnit + ''
      self.totalSize = self.totalUnit + ''
      self.canSize = self.canUnit + ''
    } else if (self.unit === 'record_hours') {
      self.usedUnit = Math.ceil(usedSize / 3600 * 10) / 10
      self.bookingUnit = Math.ceil(bookingSize / 3600 * 10) / 10
      self.totalUnit = Math.ceil(totalSize / 3600 * 10) / 10
      self.canUnit = (self.totalUnit - self.usedUnit - self.bookingUnit).toFixed(1)
      if (Number(self.canUnit) * 10 % 10 === 0) {
          self.canUnit = (self.totalUnit - self.usedUnit - self.bookingUnit).toFixed(0)
        }
        // if it has no enough space, show warning icon
      if (totalSize - usedSize - bookingSize < 0) {
          self.spaceEnough = true
          self.canUnit = '0'
        } else {
          self.spaceEnough = false
        }
      self.changeToNumber()
      self.usedSize = self.usedUnit + ''
      self.bookingSize = self.bookingUnit + ''
      self.totalSize = self.totalUnit + ''
      self.canSize = self.canUnit + ''
    }
  }

    /**
     * convert data types into Number.
     */
  changeToNumber () {
    this.totalUnit = Number(this.totalUnit)
    this.usedUnit = Number(this.usedUnit)
    this.bookingUnit = Number(this.bookingUnit)
    this.canUnit = Number(this.canUnit)
  }
    /**
     * query the space of pvr.
     */
  queryPVRSpace (sceneID?: string) {
    let self = this
    let PVR = {
      storageType: 'NPVR'
    }
    let options = {
      params: {
        SID: sceneID,
        DEVICE: 'PC',
        DID: session.get('uuid_cookie')
      }
    }
    queryPVRSpace(PVR, options).then(function (resp) {
      self.handleSpaceData(resp)
    })
  }

    /**
     * deal with PVRSpace data
     * @param {Object} resp [response of interface queryPVRSpace]
     */
  handleSpaceData (resp) {
    let self = this
    if ((resp['NPVRStorage']['spaceMode'] + '') === '1') {
        // space is calculated according to network capacity, unit is GB.
      self.unit = 'gb'
    } else if ((resp['NPVRStorage']['spaceMode'] + '') === '2') {
        // space is calculated according to record duration, unit is hour.
      self.unit = 'record_hours'
    }
    let usedSize = Number(resp['NPVRStorage']['usedSize']) || 0
    let totalSize = Number(resp['NPVRStorage']['totalSize']) || 0
    let bookingSize = Number(resp['NPVRStorage']['bookingSize']) || 0
    let availSize = totalSize - usedSize - bookingSize
      // get width of used space, reservation space and available space on the progress bar
    EventService.on('ScreenSize', function () {
      let clientW = document.body.clientWidth
      if (clientW > 1440) {
        self.usedPercent = usedSize / totalSize * 875 + 'px'
        self.schedPercent = bookingSize / totalSize * 875 + 'px'
        self.availPercent = availSize / totalSize * 875 + 'px'
      } else {
        self.usedPercent = usedSize / totalSize * 476 + 'px'
        self.schedPercent = bookingSize / totalSize * 476 + 'px'
        self.availPercent = availSize / totalSize * 476 + 'px'
      }
    })
    let clientW = document.body.clientWidth
    if (clientW > 1440) {
      self.usedPercent = usedSize / totalSize * 875 + 'px'
      self.schedPercent = bookingSize / totalSize * 875 + 'px'
      self.availPercent = availSize / totalSize * 875 + 'px'
    } else {
      self.usedPercent = usedSize / totalSize * 476 + 'px'
      self.schedPercent = bookingSize / totalSize * 476 + 'px'
      self.availPercent = availSize / totalSize * 476 + 'px'
    }

    self.queryPVRSpaceTwice(usedSize, bookingSize, totalSize)
  }

    /**
     * set position of scheduled line
     */
  schedPosition () {
    let position = 'relative'
    let clientW = document.body.clientWidth
    let maxLen
    if (clientW > 1440) {
      maxLen = 875
    } else {
      maxLen = 476
    }
    if (this.usedPercent !== '' && this.schedPercent !== '') {
      let used = Number(this.usedPercent.split('p')[0])
      let sched = Number(this.schedPercent.split('p')[0])
      if ((used + sched) > maxLen) {
        position = 'absolute'
      } else {
        position = 'relative'
      }
    }
    return position
  }
}
