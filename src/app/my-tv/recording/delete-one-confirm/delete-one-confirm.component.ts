import { Component, OnInit, Input } from '@angular/core'
import { EventService } from 'ng-epg-sdk/services'

@Component({
  selector: 'app-delete-one-confirm',
  templateUrl: './delete-one-confirm.component.html',
  styleUrls: ['./delete-one-confirm.component.scss']
})

export class DeleteOneConfirmComponent implements OnInit {
  tipsContent = ''

  constructor (
    ) { }

  @Input() set uiString (data) {
    this.tipsContent = data
  }

  ngOnInit () {
  }
    /**
     * click :close the dialog and cancel delete
     */
  close () {
      // just close the dialog
    EventService.emit('CLOSEDELETEONE')
  }
    /**
     * confirm to delete
     */
  confirm () {
      // close the dialog
    EventService.emit('CLOSEDELETEONE')
      // delete
    EventService.emit('DELETEONECONFIRM')
  }
}
