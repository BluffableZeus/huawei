import { Injectable } from '@angular/core'
import { CanDeactivate } from '@angular/router'
import { Observable } from 'rxjs'
import { EventService } from 'ng-epg-sdk/services'
import { session } from 'src/app/shared/services/session'

/**
 * export interface CanComponentDeactivate.
 */
export interface CanComponentDeactivate {
  canDeactivate: () => Observable<boolean> | Promise<boolean> | boolean
}

/**
 * export class RecordDeactivate.
 */
@Injectable()
export class RecordDeactivate implements CanDeactivate<CanComponentDeactivate> {
    // to judge whether the settings of manual recording page are changed.
  public conditionChanged = false
    // to judge whether is in the manual recording page.
  public showManulRecord = false
    // to save the routing url.
  public currentUrl = ''

    /**
     * to get route information.
     * @param {object} e [click event]
     */
  getRouteMessage (e) {
    if (e['srcElement']['hash']) {
      let hash = e['srcElement']['hash'] + ''
      this.currentUrl = hash.split('#')[1] || hash
      session.put('RouteMessage', this.currentUrl)
    }
  }

    /**
     * to to decide whether routing hop is allowable.
     * @param {CanComponentDeactivate} component [CanComponentDeactivate]
     */
  canDeactivate (component: CanComponentDeactivate) {
    if (this.showManulRecord && this.conditionChanged === true) {
      document.onclick = this.getRouteMessage
      EventService.emit('POP_UP_RECORD_SUBMIT')
      return false
    } else {
      return true
    }
  }
}
