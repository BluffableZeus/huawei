import * as _ from 'underscore'
import { Component, ViewChild, OnInit, OnDestroy } from '@angular/core'
import { EventService } from 'ng-epg-sdk/services'
import { session } from 'src/app/shared/services/session'
import { CommonService } from '../../core/common.service'
import { config } from '../../shared/services/config'
import { RecordingService } from './recording.service'
import { RecordDeactivate } from './record-deactivate.service'
import { ManualRecordingComponent } from './manual-record/manual-record.component'
import { Router } from '@angular/router'
import { ScheduledRecordingComponent } from './scheduled-record/scheduled-record.component'

@Component({
  selector: 'app-mytv-recording',
  styleUrls: ['./recording.component.scss'],
  templateUrl: './recording.component.html'
})

/**
 * export class MyRecordingComponent.
 */
export class MyRecordingComponent implements OnInit, OnDestroy {
    // declare child component ManualRecordingComponent.
  @ViewChild(ManualRecordingComponent) manualRecording: ManualRecordingComponent
    // declare child component ScheduledRecordingComponent.
  @ViewChild(ScheduledRecordingComponent) scheduledRecording: ScheduledRecordingComponent
    // key of prompt of delete-confirm-dialog.
  deleteUIString = 'delete_one_recording_confirm'
    // flag to keep the title that user select.
  public selectTitleFlag = 'cloud'
    // flag to check whether the state is editting.
  public isEditing = false
    // flag to keep the recording mode.
  public recordingMode = config.PVRMode
    // flag to check whether show the Delete All Confirmation dialog.
  public isClearAll = false
    // flag to check whether show the edit button.
  public showEdit = true
    // flag to check whether show the Submit Confirmation dialog.
  public showSubmitDialog = false
    // flag to check whether the content has data.
  public hasData = false
    // save the key of the title of the Submit Confirmation dialog.
  public infoKey = 'record_submit_new_recording'
    // save the key of the title and main body of the Delete All Confirmation dialog.
  public confirmInfoKey = ['deleteConfirmInfo', 'deleteConfirmTitle']
    // save the data of config.
  public configData
    // save the value of title that user select.
  public clickData = ''
    // save the value of title that user last selected.
  public lastSelect = 'cloud'
    // whether show the Delete One Confirmation dialog.
  public deleteOneConfirm = false

    /**
     * constructor of class MyRecordingComponent.
     * to get config of PVR.
     */
  constructor (
        private recordingService: RecordingService,
        private recordDeactivate: RecordDeactivate,
        private router: Router,
        private commonService: CommonService
    ) {
    let sessionConfig = session.get('CONFIG_DATAS')
    if (!_.isUndefined(sessionConfig)) {
      this.configData = sessionConfig
    } else {
      this.configData = config
    }
  }

    /**
     * init function.
     * get cloud PVR data and register event listener.
     */
  ngOnInit () {
    this.infoKey = 'record_submit_new_recording'
    this.confirmInfoKey = ['deleteConfirmInfo', 'deleteConfirmTitle']
    if (!Cookies.getJSON('IS_PROFILE_LOGIN')) {
      return
    }
    if (this.recordingMode === 'CPVR') {
      this.selectTitleFlag = 'stb'
      this.lastSelect = 'stb'
    }
    let self = this
      // enter my recording page.
    EventService.removeAllListeners(['MYTVJUMP'])
    EventService.on('MYTVJUMP', function () {
      self.showEdit = true
      self.selectTitleFlag = 'cloud'
      self.recordDeactivate.showManulRecord = false
      self.hasData = true
      self.isEditing = false
      EventService.emit('CLICK_TITLE_CLOUD', 'cloud')
    })
      // show the Submit Confirmation dialog.
    EventService.removeAllListeners(['POP_UP_RECORD_SUBMIT'])
    EventService.on('POP_UP_RECORD_SUBMIT', function () {
      self.showSubmitDialog = true
    })
      // there is no data now.
    EventService.removeAllListeners(['NODATA'])
    EventService.on('NODATA', function () {
      self.hasData = false
    })
      // when user deletes the last recording task, we should make the edit button gray and not clickable.
    EventService.removeAllListeners(['NO_RECORD_EDIT'])
    EventService.on('NO_RECORD_EDIT', () => {
      this.isEditing = false
      this.hasData = false
    })
      // when the content is in editing mode, we should make the edit button clickable.
    EventService.removeAllListeners(['CAN_RECORD_EDIT'])
    EventService.on('CAN_RECORD_EDIT', () => {
      this.isEditing = false
      this.hasData = true
    })
      // user exits the editing mode.
    EventService.removeAllListeners(['EXIT_EDIT'])
    EventService.on('EXIT_EDIT', () => {
      this.isEditing = false
      this.showEdit = true
    })
    if (session.get('hasData') === 'true') {
      this.hasData = true
    }
      // show the Delete One Confirm dialog.
    EventService.removeAllListeners(['DELETEONE'])
    EventService.on('DELETEONE', () => {
      this.deleteOneConfirm = true
    })
      // close the Delete One Confirm dialog.
    EventService.removeAllListeners(['CLOSEDELETEONE'])
    EventService.on('CLOSEDELETEONE', () => {
      this.deleteOneConfirm = false
    })
  }

    /**
     * destory function.
     * clear the timer,and remove the eventservice.
     */
  ngOnDestroy () {
  }

    /**
     * function executed after user clicks the edit button.
     */
  editData () {
    if (this.hasData) {
      this.isEditing = true
      EventService.emit('DELETE', '1')
      EventService.emit('DELETES', '1')
      EventService.emit('IS_RECORDEDIT', false)
    }
  }

    /**
     * function executed after user clicks the Clear All button.
     */
  clearAllData () {
    this.isClearAll = true
  }

    /**
     * function executed after user clicks the Complete button.
     */
  completeData () {
    this.isEditing = false
    EventService.emit('COMPLETE', '1')
    EventService.emit('COMPLETES', '1')
    EventService.emit('IS_RECORDEDIT', true)
  }

    /**
     * user canceled the delete operation.
     * @param {object} $event [event of cancel-operation]
     */
  cancelData ($event) {
    this.isClearAll = false
  }

    /**
     * user confirmed the delete operation.
     * @param {object} $event [event of confirm-operation]
     */
  confirmData ($event) {
    this.isClearAll = false
    this.isEditing = false
    EventService.emit('DELETEALL')
    this.hasData = false
  }

    /**
     * function executed after user selects the title.
     * @param {string} data [save value of title you clicked just now]
     */
  changeTitle (data) {
    this.completeData()
    if (data === 'manual') {
      this.handleManual()
    }
    if (data === 'cloud') {
      this.handleCloud(data)
    }
    if (data === 'stb') {
      if (this.recordDeactivate.showManulRecord && this.recordDeactivate.conditionChanged) {
        this.clickData = data
        this.showSubmitDialog = true
        return
      }
      this.showEdit = true
      this.selectTitleFlag = 'stb'
      this.recordDeactivate.showManulRecord = false
      this.hasData = false
      this.isEditing = false
      if (this.lastSelect === data) {
        EventService.emit('CLICK_TITLE_STB', 'stb')
      }
    }
    if (data === 'sched') {
      if (this.recordDeactivate.showManulRecord && this.recordDeactivate.conditionChanged) {
        this.clickData = data
        this.showSubmitDialog = true
        return
      }
      this.showEdit = true
      this.selectTitleFlag = 'sched'
      this.recordDeactivate.showManulRecord = false
      this.hasData = false
      this.isEditing = false
      EventService.emit('COMPLETES', '1')
      if (this.scheduledRecording) {
        this.scheduledRecording.hasType = true
        this.scheduledRecording.isSeriesPage = true
        this.scheduledRecording.showNO = false
        this.scheduledRecording.filterData = true
        this.scheduledRecording.selectFilter('STARTTIME:ASC', 'sort', '0', 'init')
      }
    }
    this.lastSelect = data
  }

    /**
     * handle the related data about cloud recording.
     * @param {string} data [save value of title you clicked just now]
     */
  handleCloud (data) {
    if (this.recordDeactivate.showManulRecord && this.recordDeactivate.conditionChanged) {
      this.clickData = data
      this.showSubmitDialog = true
      return
    }
    this.showEdit = true
    this.selectTitleFlag = 'cloud'
    this.recordDeactivate.showManulRecord = false
    this.hasData = false
    this.isEditing = false
    if (this.lastSelect === data) {
      EventService.emit('CLICK_TITLE_CLOUD', 'cloud')
    } else {
      EventService.emit('DELETESPACE', 'querypvrspace1')
    }
  }

    /**
     * handle the related data about manual recording.
     */
  handleManual () {
    this.showEdit = false
    this.selectTitleFlag = 'manual'
    this.recordDeactivate.showManulRecord = true
    this.isEditing = true
  }

    /**
     * user closed the Submit Confirm Dialog.
     * @param {object} $event [event of close-operation]
     */
  closeSubDialog ($event) {
    this.showSubmitDialog = false
    EventService.emit('RECORD_FOCUS')
  }

    /**
     * user canceled the submit operation.
     * @param {object} $event [event of cancel-operation]
     */
  cancelOperation ($event) {
    let self = this
    this.showSubmitDialog = false
    this.recordDeactivate.conditionChanged = false
    if (self.clickData !== '') {
      let temp = self.clickData
      self.clickData = ''
      self.changeTitle(temp)
    } else {
      if (session.get('PROFILE_NOTSAVE_LOGOUT_TYPE')) {
        _.delay(() => {
            EventService.emit('PROFILE_NOTSAVE_LOGOUT')
            self.router.navigate(['' + session.get('RouteMessage')])
          }, 1000)
      } else {
        this.router.navigate(['' + session.get('RouteMessage')])
      }
    }
  }

    /**
     * user confirmed the submit operation.
     * @param {object} $event [event of submit-operation]
     */
  submitData ($event) {
    let self = this
    this.showSubmitDialog = false
    this.manualRecording.record()
    this.recordDeactivate.conditionChanged = false
    if (self.clickData !== '') {
      let temp = self.clickData
      self.clickData = ''
      self.changeTitle(temp)
    } else {
      if (session.get('PROFILE_NOTSAVE_LOGOUT_TYPE')) {
        _.delay(() => {
            EventService.emit('PROFILE_NOTSAVE_LOGOUT')
            self.router.navigate(['' + session.get('RouteMessage')])
          }, 1000)
      } else {
        this.router.navigate(['' + session.get('RouteMessage')])
      }
    }
  }

    /**
     * get the DOM element by id.
     * @param {string} divName [ID of element]
     */
  dom (divName: string) {
    return document.getElementById(divName)
  }
}
