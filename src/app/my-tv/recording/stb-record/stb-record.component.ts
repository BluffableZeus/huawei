import * as _ from 'underscore'
import { Component, ViewChild, OnInit, OnDestroy } from '@angular/core'
import { RecordingService } from '../recording.service'
import { EventService } from 'ng-epg-sdk/services'
import { RecordDataService } from '../../../component/recordDialog/recordData.service'
import { session } from 'src/app/shared/services/session'
import { NPVRAppService } from '../../../component/playPopUpDialog/nPvrApp.service'
import { MyRecordings } from 'ng-epg-ui/webtv-components/myRecordings/my-recordings.component'
import { CloudService } from '../cloud-record/cloud-record.service'

const QUERYPVR_OPTIONS = {
  params: {
    SID: 'querypvr2'
  }
}

const QUERYPVR_OPTIONS_FOR_CHILD = {
  params: {
    SID: 'querypvr7'
  }
}

@Component({
  selector: 'app-stb-recording',
  styleUrls: ['./stb-record.component.scss'],
  templateUrl: './stb-record.component.html'
})

export class StbRecordingComponent implements OnInit, OnDestroy {
  @ViewChild(MyRecordings) myRecordings: MyRecordings
  public type = [{
    ID: 'all',
    name: 'searchAll'
  }, {
    ID: 'single',
    name: 'single'
  }, {
    ID: 'series',
    name: 'series'
  }]
  recordFilterLists: {
    'dataList': Array<any>,
    'row': string,
    'clientW': number,
    'suspensionID': string
  }
  recordFilterListss: {
    'dataList': Array<any>,
    'row': string,
    'clientW': number,
    'suspensionID': string
  }
  public availabilty = [{
    ID: 'all',
    name: 'searchAll'
  }, {
    ID: 'available',
    name: 'available'
  }, {
    ID: 'unavailable',
    name: 'unavailable'
  }]
  public sort = [{
    ID: 'STARTTIME:DESC',
    name: 'latest_first'
  }, {
    ID: 'STARTTIME:ASC',
    name: 'oldest_first'
  }]
  public filterDataList = []
  public sortIndex = 0
  public typeIndex = 0
  public availabiltyIndex = 0
    // save the current filter data.
  public filter
  public hasData = true
  public isEdit = false
  public changeData
  public recordFilterList: Array<any> = []
  public recordFilterListSeries: Array<any> = []
  public hasType = true
  public seriesName = ''
  public filterData = true
  public seriesId
  public index
  public deleteSpace
  public total
  public totalSeries
  public scrollTop = 0
  public isSeriesPage = true
  public showLoadCycle = false
  public isDelete = false

  constructor (
        private recordingService: RecordingService,
        private cloudService: CloudService,
        private recordDataService: RecordDataService,
        private nPVRAppService: NPVRAppService
    ) { }
  ngOnInit () {
    this.availabilty = [{
      ID: 'all',
      name: 'searchAll'
    }, {
      ID: 'available',
      name: 'available'
    }, {
        ID: 'unavailable',
        name: 'unavailable'
      }]
    this.type = [{
      ID: 'all',
      name: 'searchAll'
    }, {
      ID: 'single',
      name: 'single'
    }, {
        ID: 'series',
        name: 'series'
      }]
    this.sort = [{
      ID: 'STARTTIME:DESC',
      name: 'latest_first'
    }, {
      ID: 'STARTTIME:ASC',
      name: 'oldest_first'
    }]
    let self = this
    this.hasType = true
    this.deleteSpace = 0
      // if it is the first that you enter this page,the select order by default.
    this.selectFilter('STARTTIME:DESC', 'sort', '0', 'init')
    EventService.removeAllListeners(['DELETES'])
    EventService.on('DELETES', function () {
      self.isEdit = true
    })
    EventService.removeAllListeners(['DELETEALL'])
    EventService.on('DELETEALL', function () {
      self.deleteAll()
    })
    EventService.removeAllListeners(['PLAYNPVR'])
    EventService.on('PLAYNPVR', (data) => {
      if (this.isEdit) {
        return
      }
      this.nPVRAppService.playNPVR(data)
      self.playNpvr()
    })
    EventService.removeAllListeners(['COMPLETES'])
    EventService.on('COMPLETES', function () {
      self.isEdit = false
    })
    EventService.removeAllListeners(['MANUAL_PERIOD_SET_SUCCESS'])
    EventService.on('MANUAL_PERIOD_SET_SUCCESS', function (data) {
      EventService.emit('PROFILE_INFO_SAVE_SUCCESSFUL')
      if (data['effectiveDate'] || data['expireDate'] || data['beginDayTime'] || data['endDayTime'] || data['days']) {
        self.refresh()
      } else {
        EventService.emit('MANUAL_CHANGE_NAME', data)
      }
    })
    EventService.removeAllListeners(['LIVETV_CENCELRECORD'])
    EventService.on('LIVETV_CENCELRECORD', () => {
      self.refresh()
    })
    EventService.removeAllListeners(['MANUAL_SET_SUCCESS'])
    EventService.on('MANUAL_SET_SUCCESS', function (data) {
      EventService.emit('PROFILE_INFO_SAVE_SUCCESSFUL')
      EventService.emit('MANUAL_CHANGE_NAME', data)
      self.changeData = data
    })
    EventService.removeAllListeners(['RECORD_UPDATE_ADD_SUCCESS'])
    EventService.on('RECORD_UPDATE_ADD_SUCCESS', () => {
      EventService.emit('PROFILE_INFO_SAVE_SUCCESSFUL')
      self.refresh()
    })
    EventService.removeAllListeners(['CLICK_TITLE_STB'])
    EventService.on('CLICK_TITLE_STB', () => {
      self.hasType = true
      self.isSeriesPage = true
      self.filterData = true
      self.selectFilter('STARTTIME:DESC', 'sort', '0', 'init')
    })
  }

    /**
     * clear the content
     */
  ngOnDestroy () {
    session.pop('hasData')
    EventService.removeAllListeners(['LOAD_DATA'])
    EventService.removeAllListeners(['PLAYNPVR'])
  }

    /**
     * play NPVR.
     */
  playNpvr () {
    EventService.emit('OPEN_FULLSCREEN_LIVETVVIDEO', {})
    let video = document.querySelector('#videoContainer video')
    let ua = navigator.userAgent.toLowerCase()
    let isIe = !!ua.match(/msie/i) || !!ua.match(/rv:([\d.]+).*like gecko/)
    let isFirefox = !!ua.match(/firefox\/([\d.]+)/)
    let isEdge = !!ua.match(/edge\/([\d.]+)/)
    if (isIe) {
      video = document.querySelector('#videoContainer')
      video['msRequestFullscreen']()
    } else if (isEdge) {
      video = document.querySelector('#videoContainer')
      video.webkitRequestFullScreen()
    } else if (isFirefox) {
        video = document.querySelector('#videoContainer')
        video['mozRequestFullScreen']()
      } else if (ua.match(/version\/([\d.]+).*safari/)) {
        video.requestFullscreen()
      } else {
        video.webkitRequestFullScreen()
      }
  }

    // get the first page content
  returnFirstPage () {
    if (this.showLoadCycle) {
      return
    }
    this.isSeriesPage = true
    this.hasType = true
    this.filterData = true
    _.delay(() => {
      document.body.scrollTop = this.scrollTop
    }, 0)
    let filterDataListStb = session.get('seriesFilterDataList')
    for (let i = 0; i < filterDataListStb.length; i++) {
      if (filterDataListStb[i].type === 'sort') {
        this.sortIndex = Number(filterDataListStb[i].index)
      }
      if (filterDataListStb[i].type === 'type') {
        this.typeIndex = Number(filterDataListStb[i].index)
      }
      if (filterDataListStb[i].type === 'availabilty') {
        this.availabiltyIndex = Number(filterDataListStb[i].index)
      }
    }
    if (this.isDelete) {
      this.refresh()
    }
    this.isDelete = false
    if (this.sortIndex === 0) {
      session.put('CURRENT_RECORD_SORT', 'latest')
    } else {
      session.put('CURRENT_RECORD_SORT', 'earliest')
    }
  }

    /**
     * refresh : get stb data again
     */
  refresh () {
    this.filter = session.get('STB_FIRST_PAGE_FILTER')
    this.getDataStb()
  }

    /**
     * delete record.
     */
  deleteRecord ($event) {
    let self = this
    let req
    if ($event.data.policyType === 'Series' || $event.data.policyType === 'Period') {
      req = $event.data.floatInfo.childPVRIDs
    } else {
      req = [$event.data.floatInfo.ID]
    }
    this.recordDataService.deletePVRByID('CPVR', req).then((resp) => {
      if (resp['result']['retCode'] === '000000000') {
        self.isDelete = true
        self.myRecordings.dataList.splice($event['indexDelete'], 1)
        if (self.myRecordings.dataList.length === 0) {
            self.filterData = false
            self.hasData = false
            EventService.emit('NO_RECORD_EDIT')
          }
        if ($event.data.isSeriesPage === 'series') {
            let originalName = self.seriesName
            let lastIndex = originalName.lastIndexOf('(')
            let nameHead = originalName.substring(0, lastIndex)
            let nameTail = originalName.substring(lastIndex)
            let taskLength = Number(nameTail.replace(/[^0-9]/ig, '')) - 1
            self.seriesName = nameHead + '( ' + taskLength + ' )'
          }
      }
    })
  }

    /**
     * turn to record detail page
     */
  detailRecord ($event) {
    if (this.isEdit) {
      return
    }
    document.body.scrollTop = 0
    document.documentElement.scrollTop = 0
    this.isSeriesPage = false
    this.index = $event.index
    this.scrollTop = document.body.scrollTop
    session.pop('seriesFilterDataList')
    this.hasType = false
    this.seriesName = $event.name
    this.seriesId = $event.id
    this.cloudService.createPVRConditions(session.get('seriesPageFilterDataList'), this.seriesId, 'stb').then(resp => {
      this.filter = resp
      this.showData()
    })
    session.put('seriesFilterDataList', _.extend([], session.get('seriesPageFilterDataList')))
  }

    /**
     * delete all data
     */
  deleteAll () {
    let req
    this.isEdit = false
    req = this.filter.PVRCondition
    this.recordDataService.deletePVRByCondition('CPVR', req).then((resp) => {
      if (resp['result']['retCode'] === '000000000') {
        this.isDelete = true
        if (this.hasType) {
            this.hasData = false
            this.filterData = false
          } else {
            this.filterData = false
            EventService.emit('DELETESERIES', this.index)
          }
          // To determine whether it is a series.
        if (!this.isSeriesPage) {
            let originalName = this.seriesName
            let lastIndex = originalName.lastIndexOf('(')
            let nameHead = originalName.substring(0, lastIndex)
            let nameTail = '( 0 )'
            this.seriesName = nameHead + nameTail
          }
      }
    })
  }

    /** select filter
     *
     * @param list
     * @param type
     * @param index
     * @param flag
     */
  selectFilter (list, type, index, flag?) {
    if (this.showLoadCycle) {
      return
    }
      // initialization
    if (flag) {
      this.filterDataList = []
      this.sortIndex = 0
      this.typeIndex = 0
      this.availabiltyIndex = 0
    }
    if (!this.isEdit) {
      let lastChoice
        // judge type
      switch (type) {
        case 'sort':
          lastChoice = _.find(this.filterDataList, item => {
              return item['type'] === 'sort'
            })
          this.getFilterDataList(lastChoice)
          this.sortIndex = index
          break
        case 'type':
          lastChoice = _.find(this.filterDataList, item => {
              return item['type'] === 'type'
            })
          this.getFilterDataList(lastChoice)
          this.typeIndex = index
          break
        case 'availabilty':
          lastChoice = _.find(this.filterDataList, item => {
              return item['type'] === 'availabilty'
            })
          this.getFilterDataList(lastChoice)
          this.availabiltyIndex = index
          break
      }
      this.filterDataList.push({ name: list, type: type, index: index })
      if (this.isSeriesPage) {
        session.put('seriesPageFilterDataList', _.extend([], this.filterDataList))
      }
      if (this.hasType) {
        this.cloudService.createPVRConditions(this.filterDataList, '', 'stb').then(resp => {
            this.filter = resp
            // the 'flag' is judged the case whether it is the first that you enter the page.
            // the show that it is no data
            // after filter is different from that show that it is no data when you enter the page firstly.
            if (flag) {
              this.getDataStb(flag)
            } else {
              this.getDataStb()
            }
          })
      } else {
        this.cloudService.createPVRConditions(this.filterDataList, this.seriesId, 'stb').then(resp => {
            this.filter = resp
            this.showData()
          })
      }
    }
  }

  getFilterDataList (lastChoice) {
    if (lastChoice) {
      let choiceIndex = _.indexOf(this.filterDataList, lastChoice)
      this.filterDataList.splice(choiceIndex, 1)
    }
  }

    // show data
  showData (option?) {
    let self = this
    let queryPVRRequest = {
      count: '12',
      offset: '0',
      sortType: this.filter.sortType,
      PVRCondition: this.filter.PVRCondition
    }
    self.secondPageQuerySTB(queryPVRRequest)
    EventService.removeAllListeners(['LOAD_DATA'])
    EventService.on('LOAD_DATA', function () {
      if (self.recordFilterListSeries && self.recordFilterListSeries.length &&
                self.recordFilterListSeries.length < self.totalSeries) {
        let offset = self.recordFilterListSeries.length
        self.showLoadCycle = true
        let request = {
            count: '12',
            offset: '0',
            sortType: self.filter.sortType,
            PVRCondition: self.filter.PVRCondition
          }
        self.recordDataService.queryPVR(request, QUERYPVR_OPTIONS_FOR_CHILD).then((resp) => {
            let filterList = self.recordingService.getFilterRecord(resp.PVRList, offset, 'stb', 'series', null, self.totalSeries)
            if (filterList.length > 0) {
              let molist = self.recordingService.getFilterRecord(resp.PVRList, offset, 'stb', 'series', null, self.totalSeries)
              let moredata = self.recordFilterListSeries.concat(molist)
              self.recordFilterListSeries = moredata
              self.recordFilterListss = {
                'dataList': self.recordFilterListSeries,
                'row': 'All',
                'clientW': document.body.clientWidth,
                'suspensionID': 'filterRecord'
              }
            }
            self.showLoadCycle = false
          })
      }
    })
  }

    /**
     * get stb content
     * @param flag
     */
  getDataStb (flag?) {
    let self = this
    session.put('STB_FIRST_PAGE_FILTER', this.filter)
    this.recordFilterList.length = 0
    this.recordFilterLists = {
      'dataList': this.recordFilterList,
      'row': 'All',
      'clientW': document.body.clientWidth,
      'suspensionID': 'filterRecord'
    }
    let queryPVRRequest = {
      count: '12',
      offset: '0',
      sortType: this.filter.sortType,
      PVRCondition: this.filter.PVRCondition
    }
    if (flag) {
      this.firstPageQuerySTB(queryPVRRequest, flag)
    } else {
      this.firstPageQuerySTB(queryPVRRequest)
    }
    EventService.removeAllListeners(['LOAD_DATA'])
    EventService.on('LOAD_DATA', function () {
      if (self.recordFilterList && self.recordFilterList.length && self.recordFilterList.length < self.total) {
        let offset = self.recordFilterList.length
        self.showLoadCycle = true
        let request = {
            count: '12',
            offset: offset + '',
            sortType: self.filter.sortType,
            PVRCondition: self.filter.PVRCondition
          }
        self.recordDataService.queryPVR(request, QUERYPVR_OPTIONS).then((resp) => {
            let filterList = self.recordingService.getFilterRecord(resp.PVRList, offset, 'stb')
            if (filterList.length > 0) {
              let molist = self.recordingService.getFilterRecord(resp.PVRList, offset, 'stb')
              let moredata = self.recordFilterList.concat(molist)
              self.recordFilterList = moredata
              self.recordFilterLists = {
                'dataList': self.recordFilterList,
                'row': 'All',
                'clientW': document.body.clientWidth,
                'suspensionID': 'filterRecord'
              }
            }
            self.showLoadCycle = false
          })
      }
    })
  }

    /**
     * query stb content.
     * @param queryPVRRequest
     * @param flag
     */
  firstPageQuerySTB (queryPVRRequest, flag?) {
    let self = this
    this.showLoadCycle = true
    this.recordDataService.queryPVR(queryPVRRequest, QUERYPVR_OPTIONS).then((resp) => {
      self.total = resp.total
      if (resp.PVRList && resp.PVRList.length > 0) {
        EventService.emit('CAN_RECORD_EDIT')
        let filterList = this.recordingService.getFilterRecord(resp.PVRList, 0, 'stb', '')
        self.recordFilterList = filterList
        self.filterData = true
        self.recordFilterLists = {
            'dataList': filterList,
            'row': 'All',
            'clientW': document.body.clientWidth,
            'suspensionID': 'filterRecord'
          }
      } else {
        EventService.emit('NO_RECORD_EDIT')
        if (flag) {
            self.filterData = false
            self.hasData = false
          } else {
            if (self.typeIndex === 0 && self.availabiltyIndex === 0) {
              self.hasData = false
            } else {
              self.hasData = true
            }
            self.filterData = false
          }
      }
      self.showLoadCycle = false
    })
  }

    /**
     * query stb content.
     * @param queryPVRRequest
     */
  secondPageQuerySTB (queryPVRRequest, option?) {
    let self = this
    this.showLoadCycle = true
    this.recordDataService.queryPVR(queryPVRRequest, QUERYPVR_OPTIONS_FOR_CHILD).then((resp) => {
      self.totalSeries = resp.total
      if (resp.PVRList && resp.PVRList.length > 0) {
        EventService.emit('CAN_RECORD_EDIT')
        let filterList = self.recordingService.getFilterRecord(resp.PVRList, 0, 'stb', 'series', null, self.totalSeries)
        self.recordFilterListSeries = filterList
        self.recordFilterListss = {
            'dataList': filterList,
            'row': 'All',
            'clientW': document.body.clientWidth,
            'suspensionID': 'filterVod'
          }
      } else {
        EventService.emit('NO_RECORD_EDIT')
        self.filterData = false
      }
      self.showLoadCycle = false
    })
  }
}
