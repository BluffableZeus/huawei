import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core'

@Component({
  selector: 'app-mytv-submit',
  templateUrl: './submitOrLeave.component.html',
  styleUrls: ['./submitOrLeave.component.scss']
})

export class SubmitComponent implements OnInit {
  /**
     * Angular Input function.
     * to get data from parent component.
     * @param {string} data [key of prompt message]
     */
  @Input() set infoKey (data) {
    this.contentInfo = data
  }
    /**
     * Output(EventEmitter).
     * emit event to communicate with parent component.
     */
    // when clicking the close icon, emit event [closeSubDialog].
  @Output() closeSubDialog: EventEmitter<Object> = new EventEmitter()
    // when clicking the submit button, emit event [submitData].
  @Output() submitData: EventEmitter<Object> = new EventEmitter()
    // when clicking the cancel button, emit event [cancelOperation].
  @Output() cancelOperation: EventEmitter<Object> = new EventEmitter()
    // to save key of prompt message.
  public contentInfo
    // to prevent repeated submit.
  public enableSubmit = true

    /**
     * constructor of class SubmitComponent.
     */
  constructor (
    ) {}

    /**
     * init function of class SubmitComponent.
     */
  ngOnInit () {
  }

    /**
     * click the close icon.
     * @param {string} data [close flag]
     */
  close (data) {
    this.closeSubDialog.emit(data)
  }

    /**
     * click the cancel button.
     * @param {string} data [cancel flag]
     */
  cancel (data) {
    this.cancelOperation.emit(data)
  }

    /**
     * click the submit button.
     * @param {string} data [submit flag]
     */
  submit (data) {
    if (this.enableSubmit) {
      this.submitData.emit(data)
      this.enableSubmit = false
    }
  }
}
