import * as _ from 'underscore'
import { Injectable } from '@angular/core'
import { queryFavorite, deleteFavorite, queryPlaybillList } from 'ng-epg-sdk/vsp'
import { PictureService } from 'ng-epg-ui/services/picture.service'
import { session } from 'src/app/shared/services/session'

@Injectable()
export class ShowFavorite {
  constructor (private pictureService: PictureService) { }

  /**
     * get favorite channels
     */
  showFavorite (req: any) {
    let self = this
    return queryFavorite(req).then(resp => {
      let totalAll = resp.total
      let favoriteChannel = _.map(resp.favorites, (favorite) => {
        let favoriteInfo = favorite.channel
        let url = favoriteInfo.picture && favoriteInfo.picture.icons &&
                self.pictureService.convertToSizeUrl(favoriteInfo.picture.icons[0],
                  { minwidth: 90, minheight: 90, maxwidth: 100, maxheight: 100 })
        return {
          id: favoriteInfo.ID,
          contentType: favorite.contentType,
          ChannelName: favoriteInfo.name,
          posterUrl: url || 'assets/img/default/myTV_channel_poster.png',
          totalAll: totalAll,
          floatInfo: favoriteInfo
        }
      })
      return favoriteChannel
    })
  }

  /**
     * call interface to delete favorite channels
     */
  deleteFavorite (req: any) {
    return deleteFavorite(req).then(resp => {
      return resp
    })
  }

  /**
     * call interface to query playbill list
     */
  getQueryPlaybill (req: any) {
    let options = {
      params: {
        SID: 'queryplaybilllist7',
        DEVICE: 'PC',
        DID: session.get('uuid_cookie')
      }
    }
    return queryPlaybillList(req, options).then(resp => {
      return resp
    })
  }
}
