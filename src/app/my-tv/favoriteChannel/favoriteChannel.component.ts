import * as _ from 'underscore'
import { Component, ViewChild, OnInit, OnDestroy } from '@angular/core'
import { ShowFavorite } from './favoriteChannel.service'
import { Router, ActivatedRoute } from '@angular/router'
import { EventService } from 'ng-epg-sdk/services'
import { PlaybillAppService } from '../../component/playPopUpDialog/playbillApp.service'
import { MyChannels } from 'ng-epg-ui/webtv-components/myChannels/my-channels.component'
import { session } from 'src/app/shared/services/session'
import { ChannelCacheService } from '../../shared/services/channel-cache.service'
import { PictureService } from 'ng-epg-ui/services'
@Component({
  selector: 'app-mytv-favorite-channel',
  styleUrls: ['./favoriteChannel.component.scss'],
  templateUrl: './favoriteChannel.component.html'
})

export class MyTVChannelFavoriteComponent implements OnInit, OnDestroy {
  @ViewChild(MyChannels) myChannels: MyChannels
    // declare variables.
  show = true
  isClearAll = false
  favoriteLists: Array<any>
  channelIDs: Array<string> = []
  totalAll = 0
  nowDataLength = 0
  totalStr = ''
  noDataList = '1'
  failed = '0'
  failTimer: any
  private channelPlayFlag = true
  private confirmInfoKey = ['deleteConfirmInfo', 'deleteConfirmTitle']
  showLoadIcon = false
  firstLoad = true
  channelFavoriteIDs: string
  public finishDelete = true

    // private constructor
  constructor (
        private playbillAppService: PlaybillAppService,
        private showFavorite: ShowFavorite,
        private route: ActivatedRoute,
        protected picture: PictureService,
        private channelCacheService: ChannelCacheService,
        private router: Router) { }
    /**
     * initialization
     */
  ngOnInit () {
    this.confirmInfoKey = ['deleteConfirmInfo', 'deleteConfirmTitle']
    let self = this
    if (!Cookies.getJSON('IS_PROFILE_LOGIN')) {
      return
    }
    self.showLoadIcon = true
    self.showData()
  }
    /**
     * clear the timer,and remove the eventservice.
     */
  ngOnDestroy () {
    clearTimeout(this.failTimer)
    EventService.removeAllListeners(['LOAD_DATA'])
  }
    /**
     * get the favorite datas of channel.
     */
  showData () {
    let self = this
      // get staticChannelData
    let staticChannelData = this.channelCacheService.getStaticChannelData().channelDetails
      // get dynamicChannelData
    let dynamicChannelData = this.channelCacheService.getDynamicChannelData().channelDynamaicProp
    let lastChannel = _.filter(dynamicChannelData, function (channelDetails) {
      return channelDetails['favorite'] !== undefined
    })
      // get request data
    let resp = _.map(lastChannel, (item) => {
        // get staticData
      let channel = _.find(staticChannelData, items => {
        return item['ID'] === items['ID']
      })
        // return favorite data
      let picture: string
      if (channel['picture'] && channel['picture']['icons']) {
        picture = this.picture.convertToSizeUrl(
            channel['picture']['icons'][0],
            { minwidth: 132, minheight: 132, maxwidth: 132, maxheight: 132 }
          )
      } else {
        picture = 'assets/img/default/myTV_channel_poster.png'
      }

      return {
        id: channel['ID'],
        ChannelName: channel['name'],
        posterUrl: picture,
        totalAll: lastChannel.length,
        floatInfo: {
            currentPlaybill: {}
          },
        collectTime: item['favorite']['collectTime'],
        sceneID: 'queryplaybilllist9'
      }
    })
    resp = _.sortBy(resp, respItem => {
      return -respItem['collectTime']
    })
    self.showLoadIcon = false
    if (resp.length === 0) {
      self.noDataList = '1'
    } else {
      self.noDataList = '0'
      self.favoriteLists = resp
      self.totalAll = Number(resp[0]['totalAll'])
      self.nowDataLength = resp.length
      if (self.totalAll === 0) {
        self.noDataList = '1'
      } else {
        self.totalStr = '(' + self.totalAll + ')'
      }
        // refresh storage
      _.each(resp, (list, index) => {
        self.refreshStroagr(list['id'], list['collectTime'])
      })
    }
  }
    /**
     * put data into sessionStaticData and sessionDynamicData
     */
  refreshStroagr (ID, collectTime) {
    let self = this
    let sessionStaticData = self.channelCacheService.getStaticChannelData()
    let sessionDynamicData = self.channelCacheService.getDynamicChannelData()
    _.each(sessionStaticData['channelDetails'], (list, index) => {
      if (list['ID'] === ID) {
        list['favorite'] = {}
      }
    })
    _.each(sessionDynamicData['channelDynamaicProp'], (list, index) => {
      if (list['ID'] === ID) {
        list['favorite'] = {}
        list['favorite']['collectTime'] = collectTime
      }
    })
    let userType
    if (Cookies.getJSON('IS_PROFILE_LOGIN')) {
      userType = Cookies.getJSON('USER_ID')
    } else {
      userType = session.get('Guest')
    }
    session.put(userType + '_Static_Version_Data', sessionStaticData, true)
    session.put(userType + '_Dynamic_Version_Data', sessionDynamicData, true)
  }
    /**
     * click the edit button
     */
  editData () {
    this.channelPlayFlag = false // if false,you can not to click the picture to play or show the suspension.
    this.show = false
    EventService.emit('DELETE', '1')
    EventService.emit('IS_CHANNELEDIT', this.channelPlayFlag)
  }
    /**
     * click the 'clear all' button
     */
  clearAllData () {
    this.channelPlayFlag = true
    this.isClearAll = true // if true, show the confirm dialog.
  }
    /**
     * click the 'complete' button
     */
  completeData () {
    this.channelPlayFlag = true // if true,you can click the picture to play and show the suspension.
    this.show = true
    EventService.emit('COMPLETE', '1')
    EventService.emit('IS_CHANNELEDIT', this.channelPlayFlag)
  }

    /**
     * click the delete button of the picture
     */
  deleteChannel ($event) {
    let self = this
    if (self.finishDelete) {
      self.finishDelete = false
      self.channelIDs.push($event['data'].id)
      if (self.channelIDs.length !== 0) {
        let req = {
            'contentTypes': ['CHANNEL'],
            'contentIDs': self.channelIDs
          }
        self.channelIDs = []
        self.showFavorite.deleteFavorite(req).then((resp) => {
            if (resp['result']['retCode'] === '000000000') {
              let num = 1
              if (self.channelFavoriteIDs === $event['data'].id) {
                num = 0
              }
              self.channelFavoriteIDs = $event['data'].id
              self.myChannels.dataList.splice($event['index'], num)
              self.myChannels.closeDialog()
              self.nowDataLength = self.nowDataLength - num
              self.totalAll = self.totalAll - num > 0 ? self.totalAll - num : 0
              if (self.totalAll === 0) {
                self.noDataList = '1'
                self.totalStr = ''
                self.completeData()
              } else {
                self.noDataList = '0'
                self.totalStr = '(' + self.totalAll + ')'
              }
              self.deleteOneFavoriteRefresh($event['data'].id)
            }
            self.finishDelete = true
          }, respFail => {
            self.finishDelete = true
          })
      }
    }
  }
    /**
     *  delete one favorite, refresh storage
     */
  deleteOneFavoriteRefresh (ID) {
    let self = this
    let sessionDynamicData = self.channelCacheService.getDynamicChannelData()
    _.each(sessionDynamicData['channelDynamaicProp'], (list, index) => {
      if (list['ID'] === ID) {
        list['favorite'] = undefined
      }
    })
    let userType
    if (Cookies.getJSON('IS_PROFILE_LOGIN')) {
      userType = Cookies.getJSON('USER_ID')
    } else {
      userType = session.get('Guest')
    }
    session.put(userType + '_Dynamic_Version_Data', sessionDynamicData, true)
  }
    /**
     * click the picture to play the playbill.
     */
  detailChannel ($event) {
    if (document.getElementsByClassName('live_load_bg') && document.querySelector('.channel_login_load_content')) {
      document.getElementsByClassName('live_load_bg')[0]['style']['display'] = 'block'
      document.querySelector('.channel_login_load_content')['style']['display'] = 'block'
    }
    let self = this
    if (self.channelPlayFlag) {
      let channelInfo
      self.showFavorite.getQueryPlaybill({
        needChannel: '0',
        queryPlaybill: {
            type: '1',
            count: '1',
            offset: '0'
          },
        queryChannel: {
            channelIDs: [$event.id],
            isReturnAllMedia: '1'
          }
      }).then(resp => {
          channelInfo = resp.channelPlaybills[0]
          let dynamicChannelData = this.channelCacheService.getDynamicChannelData().channelDynamaicProp
          let channelDtails = this.channelCacheService.getStaticChannelData().channelDetails
          channelInfo['channelInfos'] = _.find(channelDtails, (detail) => {
            return detail['ID'] === channelInfo['playbillLites'][0]['channelID']
          })
          _.find(dynamicChannelData, (detail) => {
            if (detail && channelInfo['channelInfos'] && channelInfo['channelInfos']['ID'] === detail['ID']) {
              channelInfo['channelInfos']['channelNO'] = detail['channelNO']
              let physicalChannelsDynamicProperty = _.find(detail['physicalChannelsDynamicProperties'], (property) => {
                return _.contains(property['channelNamespaces'], session.get('ott_channel_name_space'))
              })
              channelInfo['channelInfos']['physicalChannelsDynamicProperty'] =
                            physicalChannelsDynamicProperty || detail['physicalChannelsDynamicProperties'][0]
            }
          })
          self.playbillAppService.playProgram(channelInfo['playbillLites'][0], channelInfo['channelInfos'])
        })
      EventService.emit('OPEN_FULLSCREEN_LIVETVVIDEO', {})
      let video = document.querySelector('#videoContainer video')
      let ua = navigator.userAgent.toLowerCase()
      let isIe = !!ua.match(/msie/i) || !!ua.match(/rv:([\d.]+).*like gecko/)
      let isFirefox = !!ua.match(/firefox\/([\d.]+)/)
      let isEdge = !!ua.match(/edge\/([\d.]+)/)
      if (isIe) {
        video = document.querySelector('#videoContainer')
        video['msRequestFullscreen']()
      } else if (isEdge) {
          video = document.querySelector('#videoContainer')
          video.webkitRequestFullScreen()
        } else if (isFirefox) {
          video = document.querySelector('#videoContainer')
          video['mozRequestFullScreen']()
        } else if (ua.match(/version\/([\d.]+).*safari/)) {
          video.requestFullscreen()
        } else {
          video.webkitRequestFullScreen()
        }
    }
  }
    /**
     * click the cancel button of the 'cannel all' dialog.
     */
  cancelData ($event) {
      // if false,close the 'cannel all' dialog.
    this.isClearAll = false
  }
    /**
     * click the confirm button of the 'cannel all' dialog.
     */
  confirmData ($event) {
    let self = this
    let req = {
      'contentTypes': ['CHANNEL']
    }
    self.showFavorite.deleteFavorite(req).then((resp) => {
      self.noDataList = '1'
      self.favoriteLists = []
      self.isClearAll = false
      self.show = true
      self.totalStr = ''
      self.deleteAllFavoriteRefresh()
    })
        .catch(() => {
          self.failed = '1'
          self.failTimer = setTimeout(() => self.failed = '0', 2000)
          self.showData()
        })
  }
    /**
     * delete all favorite, refresh storage
     */
  deleteAllFavoriteRefresh () {
    let self = this
    let sessionDynamicData = self.channelCacheService.getDynamicChannelData()
    _.each(sessionDynamicData['channelDynamaicProp'], (list, index) => {
      list['favorite'] = undefined
    })
    let userType
    if (Cookies.getJSON('IS_PROFILE_LOGIN')) {
      userType = Cookies.getJSON('USER_ID')
    } else {
      userType = session.get('Guest')
    }
    session.put(userType + '_Dynamic_Version_Data', sessionDynamicData, true)
  }
    /**
     * if there are no favorite channels,change the style of the edit button.
     */
  noDataStyle () {
    let styles
    if (this.noDataList === '1') {
      styles = {
        'color': '#5f5f5f',
        'cursor': 'default'
      }
    } else {
      styles = {}
    }
    return styles
  }
    /**
     * get the dom
     */
  dom (divName: string) {
    return document.getElementById(divName)
  }
}
