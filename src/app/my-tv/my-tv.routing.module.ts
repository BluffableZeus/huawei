import { NgModule } from '@angular/core'
import { Route, RouterModule } from '@angular/router'
import { AuthGuard } from '../shared/services/auth.guard'

import { MyTvComponent } from './my-tv.component'
import { MyTVHistoryVodComponent } from './historyVod/historyVod.component'
import { MyTVVodFavoriteComponent } from './favoriteVod/favoriteVod.component'
import { MyTVChannelFavoriteComponent } from './favoriteChannel/favoriteChannel.component'
import { MyTVChannelLockedComponent } from './lockedChannel/lockedChannel.component'
import { ProfileManageComponent } from './profile-manage/profile-manage.component'
import { ReminderManageComponent } from './reminder-manage/reminder-manage.component'
import { DeviceNameComponent } from './my-devices/device/device-name.component'
import { PurchasedComponent } from './purchased/purchased.component'
// import { MyRecordingComponent } from './recording/recording.component'
import { ProfileDeactivate } from './profile-manage/profile-deactivate.service'
import { DeviceDeactivate } from './my-devices/device/device-deactivate.service'
import { RecordDeactivate } from './recording/record-deactivate.service'
import { StorageRemindComponent } from '../component/storageRemind/storageRemind.component'
// import { PreferencesHomeComponent } from '../login/preferences/preferences.component'
import { PreferenceDeactivate } from '../login/preferences/preferences-deactivate.service'
// import { SubProfileComponent } from './sub-profile/sub-profile.component'
import { SubProfileActivate } from './sub-profile/subProfile-activate.service'
import { AllSubscriptionsComponent } from './subscriptions/all_subscriptions/all_subscriptions.component'
import { MySubscriptionsComponent } from './subscriptions/my_subscriptions/my_subscriptions.component'

import { MyDevicesComponent } from './my-devices/my-devices.component'
// import { PromocodesComponent } from './subscriptions/promocodes/promocodes.component'
// import { MySocialNetworksComponent } from './my-social-networks/my-networks.component'

const myTVRoutes: Array<Route> = [
  {
    path: '',
    component: MyTvComponent,
    canActivate: [AuthGuard],
    children: [
      { path: 'historyVod', component: MyTVHistoryVodComponent },
      { path: 'favoriteVod', component: MyTVVodFavoriteComponent },
      { path: 'favoriteChannels', component: MyTVChannelFavoriteComponent },
      { path: 'lockedChannels', component: MyTVChannelLockedComponent },
      { path: 'recording/storageRemind', component: StorageRemindComponent },
      { path: 'profile', component: ProfileManageComponent, canDeactivate: [ ProfileDeactivate ] },
      { path: 'my_subscriptions', component: MySubscriptionsComponent },
      // { path: 'promocodes', component: PromocodesComponent },
      //  DEPRECATED DWD-32
      // { path: 'preferences', component: PreferencesHomeComponent, canDeactivate: [ PreferenceDeactivate ]},
      // { path: 'recording', component: MyRecordingComponent, canDeactivate: [ RecordDeactivate ] },
      { path: 'reminder', component: ReminderManageComponent },
      //  DEPRECATED DWD-32
      // { path: 'sub-profile', component: SubProfileComponent},
      { path: 'devices', component: MyDevicesComponent },
      // { path: 'social', component: MySocialNetworksComponent },
      { path: 'devices/:deviceID', component: DeviceNameComponent, canDeactivate: [ DeviceDeactivate ] },
      { path: 'all_subscriptions', component: AllSubscriptionsComponent, canActivate: [ SubProfileActivate ] }
    ]
  }
]

@NgModule({
  imports: [
    RouterModule.forChild(myTVRoutes)
  ],
  exports: [
    RouterModule
  ],
  providers: [ProfileDeactivate, DeviceDeactivate, RecordDeactivate, PreferenceDeactivate, SubProfileActivate]
})
export class MyTVRoutingModule { }
