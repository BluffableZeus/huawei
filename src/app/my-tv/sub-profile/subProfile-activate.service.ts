import { Injectable } from '@angular/core'
import { CanActivate } from '@angular/router'

@Injectable()
export class SubProfileActivate implements CanActivate {
  /**
     * function used to decide whether the manage-page of sub profile is accessible.
     */
  canActivate () {
    let profile = JSON.parse(localStorage.getItem('curProfile'))
    if (profile['profileType'] === '0') {
      return true
    } else {
      return false
    }
  }
}
