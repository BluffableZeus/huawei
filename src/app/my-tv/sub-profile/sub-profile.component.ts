import * as _ from 'underscore'
import { Component, OnInit, OnDestroy } from '@angular/core'
import { SubProfileService } from './sub-profile.service'
import { ProfileManageService } from '../profile-manage/profile-manage.service'
import { EventService } from 'ng-epg-sdk/services'

@Component({
  selector: 'app-sub-profile',
  styleUrls: ['./sub-profile.component.scss'],
  templateUrl: './sub-profile.component.html'
})

export class SubProfileComponent implements OnInit, OnDestroy {
    // keys for showing prompt content on the delete confirmation dialog.
  deleteUIString = 'delete_sub_profile_tips'
    // judge the type of verifying.
  verifyType = 'deleteProfile'
    // save the confirm information.
  confirmInfoKey = ['delete_sub_profile_prompt', 'deleteConfirmTitle']
    // check whether there is no sub profiles, 1 means no sub profiles.
  public noDataList = false
    // check whether show the edit button.
  public showEdit = true
    // check whether show the dialog that is used to confirm the delete-all-profiles operation.
  public showConfirmDialog = false
    // save all the sub profiles data.
  public subProfileData = []
    // check whether show the password-verify dialog.
  public showPasswordDialog = false
    // check whether it is in editting status.
  public isEditting = false
    // save all profile ID that will be deleted.
  public allDeleteIDs = []
    // check whether show the modify-basic-information dialog.
  public showBasicDialog = false
    // save the current sub profile data.
  public currentSubInfo
    // save the data that is used to add or modify sub profile information.
  public sendBasicInfo
    // save the all avatar data.
  public avatarList = []
    // check whether show the modify-password dialog.
  public showModifyPassword = false
    // save the information about profile password.
  public sendPasswordInfo
    // save the information of new profile.
  public newProfile = {}
    // save the list of level.
  public levelList = []
    // save the list of limit.
  public limitList = []
    // check which one profile the mouse is in.
  public isEnter = []
    // check whether show the delete-confirm dialog.
  public showDeleteConfirm = false
    // save the current ID List.
  public deleteIDs = []
    // save the index of profile that will be deleted.
  public deleteIndex = 0

    /**
     * constructor of class SubProfileComponent.
     */
  constructor (
        private subProfileService: SubProfileService,
        private profileManageService: ProfileManageService
    ) {}

    /**
     * init function.
     * get sub profile data and register event listener.
     */
  ngOnInit () {
    this.getSubProfiles()
    this.avatarList = this.subProfileService.getAvatarList()
    this.levelList = this.profileManageService.getLevelList()
    this.profileManageService.getLimitList().then(resp => {
      this.limitList = resp
    })
    EventService.removeAllListeners(['CLOSEDELETEONE'])
    EventService.on('CLOSEDELETEONE', () => {
      this.closeDelOneDialog()
    })
    EventService.removeAllListeners(['DELETEONECONFIRM'])
    EventService.on('DELETEONECONFIRM', () => {
      this.deleteOneProfile()
    })
  }

    /**
     * get data of all profile.
     */
  getSubProfiles () {
    this.subProfileService.getAllSubProfiles().then(resp => {
      this.subProfileData = resp
      for (let i = 0; i < resp.length; i++) {
        this.isEnter[i] = false
      }
      this.getAllProfileIDs(resp)
    })
  }

    /**
     * get ID List of all profile.
     * @param {Array} resp [length of profile array]
     */
  getAllProfileIDs (resp) {
    this.allDeleteIDs = []
    if (resp.length > 0) {
      for (let i = 0; i < resp.length; i++) {
        this.allDeleteIDs.push(resp[i]['ID'])
      }
      this.noDataList = false
    } else {
      this.noDataList = true
    }
  }

    /**
     * destory function.
     * clear the timer,and remove the eventservice.
     */
  ngOnDestroy () {

  }

    /**
     * click the 'Edit' button
     */
  editData () {
    this.showPasswordDialog = true
  }

    /**
     * click the 'Clear All' button
     */
  clearAllData () {
    this.showConfirmDialog = true
  }

    /**
     * click the 'Complete' button
     */
  completeData () {
    this.showEdit = true
    this.isEditting = false
  }

    /**
     * click the 'Add' button, show the Add-Profile dialog.
     */
  addSubProfile () {
    this.showBasicDialog = true
    this.sendBasicInfo = {
      avatarList: this.avatarList,
      type: 'add',
      levelList: this.levelList,
      limitList: this.limitList
    }
  }

    /**
     * when you has finished the basic information, you need to set password.
     * @param {Object} profile [get profile information after finish basic information]
     */
  toSetPassword (profile) {
    let sendProfile = _.clone(profile)
    this.newProfile = profile
    this.showModifyPassword = true
    this.sendPasswordInfo = {
      subInfo: sendProfile,
      type: 'add'
    }
  }

    /**
     * click the close button of checking password dialog.
     * @param {Object} $event [close event from check password component]
     */
  closePassword ($event) {
    this.showPasswordDialog = false
  }

    /**
     * click the confirm button of checking password dialog.
     * @param {Object} $event [confirmation event from check password component]
     */
  confirmPass ($event) {
    this.showPasswordDialog = false
    this.isEditting = true
    this.showEdit = false
  }

    /**
     * delete all the sub profiles.
     * @param {String} id [profile ID to be deleted]
     * @param {index} index [index of profile to be deleted]
     */
  deleteSubProfile (id, index) {
    this.showDeleteConfirm = true
    this.deleteIDs = []
    this.deleteIDs.push(id)
    this.deleteIndex = index
  }

    /**
     * delete one of the sub profiles.
     */
  deleteOneProfile () {
    let deleteReq = {
      profileIDs: this.deleteIDs
    }
    this.subProfileService.deleteSubProfile(deleteReq).then(resp => {
      if (resp['result'] && resp['result']['retCode'] && resp['result']['retCode'] === '000000000') {
        this.showDeleteConfirm = false
        this.subProfileData.splice(this.deleteIndex, 1)
        this.allDeleteIDs.splice(this.deleteIndex, 1)
        if (this.subProfileData.length === 0) {
            this.noDataList = true
            this.showEdit = true
            this.isEditting = false
          }
      }
    })
  }

    /**
     * close the delete-confirm dialog.
     */
  closeDelOneDialog () {
    this.showDeleteConfirm = false
  }

    /**
     * click the cancel button of the 'cancel all' dialog.
     * @param {Object} $event [event of cancel operation]
     */
  closeDeleteAll ($event) {
    this.showConfirmDialog = false
  }

    /**
     * click the confirm button of the 'cancel all' dialog.
     * @param {Object} $event [event of confirmation operation]
     */
  confirmDelete ($event) {
    let deleteReq = {
      profileIDs: this.allDeleteIDs
    }
    this.subProfileService.deleteSubProfile(deleteReq).then(resp => {
      if (resp['result'] && resp['result']['retCode'] && resp['result']['retCode'] === '000000000') {
        this.subProfileData = []
        this.showEdit = true
        this.isEditting = false
        this.showConfirmDialog = false
        this.noDataList = true
      }
    })
  }

    /**
     * show basic information modifying dialog.
     * @param {Object} subInfo [data of sub profile]
     */
  showModifyBasicDialog (subInfo) {
    this.showBasicDialog = true
    this.currentSubInfo = _.clone(subInfo)
    this.sendBasicInfo = {
      subInfo: this.currentSubInfo,
      avatarList: this.avatarList,
      type: 'modify',
      levelList: this.levelList,
      limitList: this.limitList
    }
  }

    /**
     * refresh the page information after updating the sub profile.
     * @param {Object} subInfo [data of sub profile]
     */
  updateSubInfo (profile) {
    for (let i = 0; i < this.subProfileData.length; i++) {
      if (this.subProfileData[i]['ID'] === profile['ID']) {
        this.subProfileData[i]['name'] = profile['name']
        this.subProfileData[i]['loginName'] = profile['loginName']
        this.subProfileData[i]['logoURL'] = profile['logoURL']
        this.subProfileData[i]['avatar'] = 'assets/img/avatars/' + this.subProfileData[i]['logoURL'] + '.png'
        this.subProfileData[i]['ratingID'] = profile['ratingID']
        this.subProfileData[i]['ratingName'] = profile['ratingName']
        this.subProfileData[i]['quota'] = profile['quota']
        if (this.subProfileData[i]['quota'] === '-1') {
            this.subProfileData[i]['quota'] = 'unlimited'
          } else {
            this.subProfileData[i]['quota'] = this.subProfileService.currencySymbol +
                        Number(this.subProfileData[i]['quota']) / this.subProfileService.currencyRate
          }
        break
      }
    }
  }

    /**
     * close basic information modifying dialog.
     */
  closeBasicDialog () {
    this.showBasicDialog = false
  }

    /**
     * show password modifying dialog.
     * @param {Object} subInfo [data of sub profile]
     */
  showModifyPasswordDialog (subInfo) {
    let sendProfile = _.clone(subInfo)
    this.showModifyPassword = true
    this.sendPasswordInfo = {
      subInfo: sendProfile,
      type: 'modify'
    }
  }

    /**
     * close password modifying dialog.
     */
  closeModifyPwd () {
    this.showModifyPassword = false
  }

    /**
     * add profile successfully.
     */
  finishAddProfile () {
    this.getSubProfiles()
  }

    /**
     * mouseenter one profile.
     * @param {Number} i [index of sub profile]
     */
  enterProfile (i) {
    this.isEnter[i] = true
  }

    /**
     * mouseleave one profile.
     * @param {Number} i [index of sub profile]
     */
  leaveProfile (i) {
    this.isEnter[i] = false
  }
}
