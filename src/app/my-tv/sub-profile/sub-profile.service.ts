import * as _ from 'underscore'
import { Injectable } from '@angular/core'
import { queryProfile, QueryProfileRequest, deleteProfile } from 'ng-epg-sdk/vsp'
import { TranslateService } from '@ngx-translate/core'
import { session } from 'src/app/shared/services/session'
import { MyTvService } from '../my-tv.service'
import { TimerService } from '../../shared/services/timer.service'
import { CommonService } from '../../core/common.service'
import { config } from '../../shared/services/config'

@Injectable()
export class SubProfileService {
    // to save the value of currencySymbol.
  public currencySymbol
    // to save the value of currencyRate.
  public currencyRate
    // to save list of profile names.
  private names
    // to save list of profile login ID.
  private loginNames

    /**
     * constructor of class SubProfileService.
     */
  constructor (
        private translate: TranslateService,
        private myTvService: MyTvService,
        private timerService: TimerService,
        private commonService: CommonService
    ) {
    this.currencySymbol = JSON.parse(localStorage.getItem('CURRENCY_SYMBOL'))
    this.currencyRate = JSON.parse(localStorage.getItem('CURRENCY_RATE'))
  }

    /**
     * get all sub profiles by interface QueryProfile.
     */
  getAllSubProfiles () {
    let req: QueryProfileRequest = {
      type: '1',
      count: '50',
      offset: '0'
    }
    return queryProfile(req).then(resp => {
      let subProfiles = []
      if (!_.isUndefined(resp['profiles'])) {
        subProfiles = _.filter(resp['profiles'], profile => {
            return profile['profileType'] !== '0'
          })
        let ratingList = session.get('TERMINAL_RATING_LIST')
        for (let i = 0; i < subProfiles.length; i++) {
            if (subProfiles[i]['quota'] && subProfiles[i]['quota'] === '-1') {
              subProfiles[i]['quota'] = 'unlimited'
            } else {
              subProfiles[i]['quota'] = this.currencySymbol + Number(subProfiles[i]['quota']) / this.currencyRate
            }
            if (subProfiles[i]['logoURL']) {
              subProfiles[i]['avatar'] = 'assets/img/avatars/' + subProfiles[i]['logoURL'] + '.png'
            } else {
              subProfiles[i]['avatar'] = 'assets/img/default/ondemand_casts.png'
            }
            let rating = _.find(ratingList, item => {
              return item['ID'] === subProfiles[i]['ratingID']
            })
            if (rating) {
              subProfiles[i]['ratingName'] = rating['name']
            }
          }
        this.getAllNameAndID(resp)
      }
      return subProfiles
    })
  }

    /**
     * get profile names and login IDs of all profiles.
     * @param {Object} resp [responce of interface QueryProfile]
     */
  getAllNameAndID (resp) {
    this.names = _.pluck(resp['profiles'], 'name')
    this.loginNames = _.pluck(resp['profiles'], 'loginName')
    session.put('ALL_PROFILE_NAME', this.names)
    session.put('ALL_LOGIN_ID', this.loginNames)
  }

    /**
     * delete sub profile.
     * @param {Object} req [request of interface DeleteProfile]
     */
  deleteSubProfile (req) {
    return deleteProfile(req).then(resp => {
      return resp
    })
  }

    /**
     * get the all avatar list.
     */
  getAvatarList () {
    let list = []
    let avatarLength = config['avatarLength']
    for (let i = 0; i < avatarLength; i++) {
      list[i] = 'assets/img/avatars/avatar' + (i + 1) + '.png'
    }
    return list
  }
}
