import { FormsModule } from '@angular/forms'
import { CommonModule } from '@angular/common'
import { PackageSuspensionModule } from 'ng-epg-ui/webtv-components/packageSuspension/package-suspension.module'
import { PurchasedService } from './purchased/purchased.service'
import { PurchasedComponent } from './purchased/purchased.component'
import { MyTvService } from './my-tv.service'
import { DeviceInfoService } from './my-devices/device-name.service'
import { DeviceNameComponent } from './my-devices/device/device-name.component'
import { ProfileManageComponent } from './profile-manage/profile-manage.component'
import { ShowLocked } from './lockedChannel/lockedChannel.service'
import { MyTVChannelLockedComponent } from './lockedChannel/lockedChannel.component'
import { ComponentModule } from '../component/component.module'
import { ShowFavorite } from './favoriteChannel/favoriteChannel.service'
import { MyChannelsModule } from 'ng-epg-ui/webtv-components/myChannels/my-channels.module'
import { MyTVChannelFavoriteComponent } from './favoriteChannel/favoriteChannel.component'
import { FavoriteVodService } from './favoriteVod/favoriteVod.service'
import { MyTVVodFavoriteComponent } from './favoriteVod/favoriteVod.component'
import { ShowHistory } from './historyVod/historyVod.service'
import { NoDataModule } from 'ng-epg-ui/webtv-components/noData/no-data.module'
import { MyVideosModule } from 'ng-epg-ui/webtv-components/myVideos/my-videos.module'
import { MyTVHistoryVodComponent } from './historyVod/historyVod.component'
// import { MyRecordingComponent } from './recording/recording.component'
// import { RecordingService } from './recording/recording.service'
import { MyTVRoutingModule } from './my-tv.routing.module'
import { NgModule } from '@angular/core'
import { ProfileManageService } from './profile-manage/profile-manage.service'
import { SubmitComponent } from './submitOrLeave/submitOrLeave.component'
// import { ManualRecordingComponent } from './recording/manual-record/manual-record.component'
// import { RecordChannelNameComponent } from './recording/manual-record/record-manual-channelname/recordChannelName.component'
// import { RecordDataNameComponent } from './recording/manual-record/record-manual-data/recordDataName.component'
// import { RecordRepeatDataComponent } from './recording/manual-record/record-repeat-data/recordrepeatdata.component'
// import { RecordStartTimeComponent } from './recording/manual-record/recordStartTime/recordStartTime.component'
// import { CloudRecordingComponent } from './recording/cloud-record/cloud-record.component'
// import { StbRecordingComponent } from './recording/stb-record/stb-record.component'
// import { ScheduledRecordingComponent } from './recording/scheduled-record/scheduled-record.component'
// import { MyRecordingsModule } from 'ng-epg-ui/webtv-components/myRecordings/my-recordings.module'
// import { RecordSuspensionModule } from 'ng-epg-ui/webtv-components/recordSuspension/record-suspension.module'
// import { ManualService } from './recording/manual-record/manual-record.service'
// import { CloudService } from './recording/cloud-record/cloud-record.service'
// import { StbService } from './recording/stb-record/stb-record.service'
// import { ScheduledService } from './recording/scheduled-record/scheduled-record.service'
import { WaterfallVodModule } from 'ng-epg-ui/webtv-components/waterfall-vod/waterfall-vod.module'
// import { ManualRecordPopUpComponent } from './recording/manual-popup/manual-popup.component'
// import { SpaceRecordComponent } from './recording/space-record/space-record.component'
import { ReminderManageComponent } from './reminder-manage/reminder-manage.component'
import { ReminderManageService } from './reminder-manage/reminder-manage.service'
import { TranslateService } from '@ngx-translate/core'
import { DialogModule } from 'ng-epg-ui/webtv-components/dialog/dialog.module'
import { DialogRef } from 'ng-epg-ui/webtv-components/dialog'
import { UISharedModule } from 'ng-epg-ui/webtv-components/shared/shared.module'
import { MyTvComponent } from './my-tv.component'
import { ProgramSuspensionModule } from 'ng-epg-ui/webtv-components/programSuspension/program-suspension.module'
// import { DeleteOneConfirmComponent } from './recording/delete-one-confirm/delete-one-confirm.component'
import { SubProfileComponent } from './sub-profile/sub-profile.component'
import { SubProfileService } from './sub-profile/sub-profile.service'
import { SubscriptionsService } from './subscriptions/subscriptions.service'
import { SubscriptionsListComponent } from './subscriptions/shared/subscriptions_list/subscriptions_list.component'
import { AllSubscriptionsComponent } from 'src/app/my-tv/subscriptions/all_subscriptions/all_subscriptions.component'
import { MySubscriptionsComponent } from 'src/app/my-tv/subscriptions/my_subscriptions/my_subscriptions.component'
import { PipesModule } from 'src/app/shared/pipes/pipes.module'
import { MyDevicesComponent } from './my-devices/my-devices.component'
// import { MySocialNetworksComponent } from './my-social-networks/my-networks.component'
import { ConfirmationComponent } from './my-social-networks/confirmation/confirmation.component'
// import { PromocodesComponent } from './subscriptions/promocodes/promocodes.component'
// import { PromocodesService } from './subscriptions/promocodes/promocodes.service'
import { SharedModule } from '../shared/shared.module'

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    UISharedModule,

    DialogModule,
    NoDataModule,
    MyVideosModule,
    MyChannelsModule,
    ComponentModule,
    MyTVRoutingModule,
    PackageSuspensionModule,
    // MyRecordingsModule,
    WaterfallVodModule,
    // RecordSuspensionModule,
    ProgramSuspensionModule,
    PipesModule,
    SharedModule
  ],
  exports: [
    SubmitComponent, 
    // ManualRecordPopUpComponent
  ],
  declarations: [
    MyTvComponent,
    MyTVHistoryVodComponent,
    // MyRecordingComponent,
    MyTVVodFavoriteComponent,
    MyTVChannelFavoriteComponent,
    MyTVChannelLockedComponent,
    ProfileManageComponent,
    DeviceNameComponent,
    PurchasedComponent,
    SubmitComponent,
    // ManualRecordingComponent,
    // RecordChannelNameComponent,
    // RecordDataNameComponent,
    // RecordRepeatDataComponent,
    // RecordStartTimeComponent,
    // CloudRecordingComponent,
    // StbRecordingComponent,
    // ScheduledRecordingComponent,
    // ManualRecordPopUpComponent,
    // SpaceRecordComponent,
    ReminderManageComponent,
    // DeleteOneConfirmComponent,
    MyDevicesComponent,
    // MySocialNetworksComponent,
    ConfirmationComponent,
    SubProfileComponent,
    SubscriptionsListComponent,
    AllSubscriptionsComponent,
    MySubscriptionsComponent,
    // PromocodesComponent
  ],
  entryComponents: [
    // ManualRecordPopUpComponent,
    ConfirmationComponent
  ],
  providers: [
    ShowHistory,
    FavoriteVodService,
    ShowFavorite,
    ShowLocked,
    DeviceInfoService,
    MyTvService,
    PurchasedService,
    ProfileManageService,
    // RecordingService,
    // ManualService,
    // CloudService,
    // StbService,
    // ScheduledService,
    ReminderManageService,
    TranslateService,
    DialogRef,
    SubProfileService,
    SubscriptionsService,
    // PromocodesService
  ]
})
export class MyTVModule { }
