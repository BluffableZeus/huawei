import * as _ from 'underscore'
import { Component, OnInit } from '@angular/core'
import { EventService } from 'ng-epg-sdk/services'
import { session } from 'src/app/shared/services/session'
import { Router, ActivatedRoute } from '@angular/router'
import { MyTvService } from './my-tv.service'
import { TranslateService } from '@ngx-translate/core'
import { config } from '../shared/services/config'
@Component({
  selector: 'app-my-tv',
  styleUrls: ['./my-tv.component.scss'],
  templateUrl: './my-tv.component.html'
})

export class MyTvComponent implements OnInit {
    // declare variables.
  public curProfile: any = {}
  public curTitle: any = {}
  public myVideos: Array<any> = [
    {
      logo: 'assets/img/16/history_16.png',
      focusLogo: 'assets/img/16/history2_16.png',
      title: 'history',
      id: 'historyVod'
    },
    {
      logo: 'assets/img/16/favorite_videos_16.png',
      focusLogo: 'assets/img/16/favorite_videos2_16.png',
      title: 'Fav_Video',
      id: 'favoriteVod'
    }
  ]
  public personals: Array<any> = [
    {
      logo: 'assets/img/16/purchased_16.png',
      focusLogo: 'assets/img/16/purchased2_16.png',
      title: 'my_subscriptions',
      id: 'my_subscriptions'
    },
    {
      logo: 'assets/img/16/purchased_16.png',
      focusLogo: 'assets/img/16/purchased2_16.png',
      title: 'all_subscriptions',
      id: 'all_subscriptions'
    },
    /** FOR REMOVE
    {
      logo: 'assets/img/16/promocode_normal.png',
      focusLogo: 'assets/img/16/promocode_hover.png',
      title: 'promocodes',
      id: 'promocodes'
    }
    */
  ]
  public others: Array<any> = [
    {
      logo: 'assets/img/16/device_name_16.png',
      focusLogo: 'assets/img/16/device_name2_16.png',
      title: 'device_name',
      id: 'devices'
    },
    /** FOR REMOVE
    {
      logo: 'assets/img/16/vk_16.png',
      focusLogo: 'assets/img/16/vk2_16.png',
      title: 'social_networks',
      id: 'social'
    }
    */
      // DEPRECATED DWD-32
      // ,
      // {
      //     logo: 'assets/img/16/setting_16.png',
      //     focusLogo: 'assets/img/16/setting2_16.png',
      //     title: 'preference_setting',
      //     id: 'preferences'
      // }
  ]
  public recordingMode = config.PVRMode
  public isMainProfile = true
    /*
     * private constructor
     */
  constructor (
        private router: Router,
        private route: ActivatedRoute,
        private myTvService: MyTvService,
        private translate: TranslateService) {
    this.curProfile['level'] = ' '
    this.curProfile['avatar'] = (Cookies.getJSON('LOGIN_PROFILE_LOGOURL')
        ? 'assets/img/avatars/' + Cookies.getJSON('LOGIN_PROFILE_LOGOURL') + '.png'
        : 'assets/img/default/ondemand_casts.png')
      // DEPRECATED DWD-32
      // let profileType = Cookies.get('PROFILE_TYPE');
      // if (profileType === '0') {
      //     this.isMainProfile = true;
      //     let subProfile = {
      //         logo: 'assets/img/16/commmonprofiles_16.png',
      //         focusLogo: 'assets/img/16/commmonprofiles2_16.png',
      //         title: 'sub_profile_navigate',
      //         id: 'sub-profile'
      //     };
      //     this.others.splice(0, 0, subProfile);
      // } else {
      //     this.isMainProfile = false;
      // }
  }
    /*
     * initiclization
      */
  ngOnInit () {
    this.isMytvIn()
    if (!Cookies.getJSON('IS_PROFILE_LOGIN')) {
      return
    }
    this.getProfileInfo()
    this.setCurTitle()
      // listening to the events that the user click the profile avatar and the profile name on the navigation bar.;
      // get the current profile information.
    EventService.removeAllListeners(['MODIFY_PROFILE'])
    EventService.on('MODIFY_PROFILE', () => {
      this.curProfile = JSON.parse(localStorage.getItem('curProfile')) || {}
    })
      // listening to the events that the user click the history button on the navigation bar or the pull-down list.;
      // get the history information.
    EventService.removeAllListeners(['HISTORYVOD'])
    EventService.on('HISTORYVOD', data => {
      this.curTitle = this.searchTitle(this.myVideos, data) || {}
    })
      // listening to the events that the user click the favoriter vod button on the pull-down list.;
      // get the favoriter vod information.
    EventService.removeAllListeners(['FAVORITEVOD'])
    EventService.on('FAVORITEVOD', data => {
      this.curTitle = this.searchTitle(this.myVideos, data) || {}
    })
      // listening to the events that the user click the purchased button on the navigation bar.;
      // get the purchased information.
    EventService.removeAllListeners(['PURCHASED'])
    EventService.on('PURCHASED', data => {
      this.curTitle = this.searchTitle(this.personals, data) || {}
    })
      // listening to the events that the user click the purchased button on the navigation bar.;
      // get the purchased information.
    EventService.removeAllListeners(['SUBPROFILE'])
    EventService.on('SUBPROFILE', data => {
      this.curTitle = this.searchTitle(this.others, data) || {}
    })
      // listening to the events that the user click the device name button on the navigation bar.;
      // get the device name information.
    EventService.removeAllListeners(['DEVICENAME'])
    EventService.on('DEVICENAME', data => {
      this.curTitle = this.searchTitle(this.others, data) || {}
    })
    EventService.removeAllListeners(['PROFILE_FOCUS'])
    EventService.on('PROFILE_FOCUS', () => {
      this.selectTitle(this.curProfile)
    })
    EventService.removeAllListeners(['DEVICE_FOCUS'])
    EventService.on('DEVICE_FOCUS', () => {
      let device = _.find(this.others, item => {
        return item['id'] === 'device-name'
      })
      this.selectTitle(device)
    })
    EventService.removeAllListeners(['RECORD_FOCUS'])
    EventService.on('RECORD_FOCUS', () => {
      this.selectTitle(this.personals[1])
    })
    EventService.removeAllListeners(['PREFERENCES_FOCUS'])
    EventService.on('PREFERENCES_FOCUS', () => {
      let preferences = _.find(this.others, item => item['id'] === 'preferences')
      this.selectTitle(preferences)
    })
      // remove  that default events effects my TV page after playing.
    if (window.addEventListener) {
      document.addEventListener('keydown', e => { e.returnValue = true }, false)
    } else if (window['attachEvent']) {
      document['attachEvent']('onkeydown', e => { e.returnValue = true }, false)
    }
  }
    /**
   * if the guest enter the application from the my TV page,jump to the home page.
   */
  isMytvIn () {
    if (location.pathname.indexOf('profile') !== -1 && !Cookies.getJSON('IS_PROFILE_LOGIN')) {
      this.router.navigate(['home'])
    }
  }
    /*
     * emulate the hover effect.
     */
  mouseInTitle (title, event?: any) {
    let currentTarget = window.event && window.event.currentTarget || event && event.target
    if (currentTarget['className'] !== 'profileInfo') {
      if (title.id !== this.curTitle.id) {
        currentTarget['className'] = 'itemEnter'
        currentTarget['childNodes'][3]['src'] = title.focusLogo
      }
    } else {
      if (title.id !== this.curTitle.id) {
        currentTarget['id'] = 'focus'
      }
    }
  }
  mouseOutTitle (title, event?: any) {
    if (title.id !== this.curTitle.id) {
      let currentTarget = window.event && window.event.currentTarget || event && event.target
      if (currentTarget['className'] !== 'profileInfo') {
        currentTarget['className'] = ''
        currentTarget['childNodes'][3]['src'] = title.logo
      } else {
        currentTarget['id'] = ''
      }
    }
  }

  searchTitle (a: Array<Object>, s: string) {
    return _.find(a, item => item['id'] === s)
  }

  setCurTitle () {
    let hash = window.location.pathname
    this.curTitle.id = hash.slice(hash.lastIndexOf('/') + 1)
  }

    /*
     * choose a column
     */
  selectTitle (title) {
    if (this.recordingMode === '' && title.id === 'recording') {
      return
    }
    if (!config.reminderMode && title.id === 'reminder') {
      return
    }
    this.curTitle = title || {}
    this.router.navigate(['profile', title.id])
    session.put('RouteMessage', 'profile/' + title.id)
  }

    /**
     * get the information of current profile.
     */
  getProfileInfo () {
    this.myTvService.queryProfile({ type: '0', count: '1', offset: '0' }).then(resp => {
      this.curProfile = resp
    })
  }
}
