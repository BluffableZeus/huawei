import * as _ from 'underscore'
import { Injectable } from '@angular/core'
import { querySubscription } from 'ng-epg-sdk/vsp'
import { TranslateService } from '@ngx-translate/core'
import { CommonService } from '../../core/common.service'
import { session } from 'src/app/shared/services/session'

const Jan = 1, Feb = 2, Mar = 3, May = 5, Jul = 7, Aug = 8, Oct = 10, Dec = 12
const Four = 4

@Injectable()
export class PurchasedService {
  constructor (private translate: TranslateService,
        private commonService: CommonService) { }
  /**
     * get subscription products by interface QuerySubscription.
     * @param {any} req [request parameters of interface QuerySubscription].
     */
  getSubScription (req: any) {
    return querySubscription(req).then(resp => {
      let singleList = []
      let packageList = []
      let allTotal = 0
      if (resp['products'] && resp['products'].length !== 0) {
        allTotal = Number(resp.total)
        singleList = _.filter(resp['products'], product => {
          return product['productType'] === '1'
        })
        packageList = _.filter(resp['products'], product => {
          return product['productType'] === '0'
        })
        singleList = this.getSingleList(singleList)
        packageList = this.getPackageList(packageList)
      }
      return {
        singleList: singleList,
        packageList: packageList,
        allTotal: allTotal
      }
    })
  }

  /**
     * single products handler.
     * @param {Object} list [list of single products].
     */
  getSingleList (list) {
    let products = _.map(list, (item) => {
      let priceObjectDetail = item['priceObjectDetail']
      if (priceObjectDetail && priceObjectDetail['contentType'] === 'VOD' || priceObjectDetail['contentType'] === 'VIDEO_VOD') {
        let vod = priceObjectDetail['VOD']
        if (vod['averageScore'] !== '10') {
          if (vod['averageScore'].length === 1) {
            vod['averageScore'] += '.0'
          } else {
            vod['averageScore'] = vod['averageScore'].substring(0, 3)
          }
        }
        return {
          isPurchased: '1',
          price: item['price'] / 100,
          endTime: item['endTime'],
          id: vod['ID'],
          contentType: vod['contentType'],
          vodName: vod['name'],
          posterUrl: vod.picture && vod.picture.posters && vod.picture.posters[0] || 'assets/img/default/my_poster.png',
          score: vod['averageScore'] || '0.0',
          floatInfo: vod,
          titleName: 'Purchased'
        }
      } else {
        return {
          isPurchased: '1',
          price: item['price'] / 100,
          endTime: item['endTime'],
          id: '',
          contentType: '',
          vodName: item['productName'],
          posterUrl: 'assets/img/default/my_poster.png',
          score: '0.0',
          floatInfo: null,
          titleName: 'Purchased'
        }
      }
    })
    return products
  }

  /**
     * package products handler.
     * @param {Object} list [list of package products].
     */
  getPackageList (list) {
    let products = _.map(list, (item) => {
      item['leftTimeType'] = ''
      this.commonService.timeClassify(item)
      let dateStr = this.commonService.dealWithDateJson('D02', Number(item['endTime']))
      let timeStr = this.commonService.dealWithDateJson('D10', Number(item['endTime']))
      if (session.get('languageName') === 'zh') {
        item['endTimeStr'] = dateStr + ' ' + timeStr
      } else {
        item['endTimeStr'] = timeStr + ' ' + dateStr
      }
      let hintHead
      let hintTail
      let timeHint = this.getTimeHint(item['leftTimeType'], item['leftTime'])
      if (item['leftTimeType'] !== 'Expired') {
        let time = timeHint.replace(/\D/g, '')
        hintHead = timeHint.split(time)[0]
        hintTail = timeHint.split(time)[1]
      }
      return {
        productName: item['productName'],
        price: item['price'] / 100,
        productDesc: item['productDesc'],
        endTime: item['endTime'],
        endTimeStr: item['endTimeStr'],
        leftTimeType: item['leftTimeType'],
        leftTime: item['leftTime'],
        timeHint: timeHint,
        hintHead: hintHead,
        hintTail: hintTail
      }
    })
    return products
  }

  /**
     * get hint of product expiration time.
     */
  getTimeHint (type, time) {
    let hint = ''
    if (type === 'Expired') {
      hint = this.translate.instant('time_expired')
    } else if (type === 'Days') {
      hint = this.translate.instant('days_left', { 'daysLength': time })
    } else if (type === 'Hours') {
      hint = this.translate.instant('hours_left', { 'hoursLength': time })
    } else if (type === 'Mins') {
      hint = this.translate.instant('mins_left', { 'minsLength': time })
    } else if (type === 'moreDays') {
      hint = this.translate.instant('days_left', { 'daysLength': time })
    }
    return hint
  }

  /**
     * add prefix '0'.
     * @param {number} num [value of month].
     */
  checkNumber (num) {
    if (num < 10) {
      num = '0' + num
    }
    return num
  }

  addZero (num) {
    if (num.length < 2) {
      num = '0' + num
    }
    return num
  }

  /**
     * get number of days of every month.
     * @param {number} year [value of year].
     * @param {number} month [value of month].
     */
  getDays (year, month) {
    if (month === Feb) {
      if ((year % Four === 0 && year % 100 !== 0) || (year % 400 === 0)) {
        return 29
      } else {
        return 28
      }
    } else if (month === Jan || month === Mar || month === May || month === Jul || month === Aug || month === Oct || month === Dec) {
      return 31
    } else {
      return 30
    }
  }

  /**
     * get the latest 6 months.
     * @param {Date} nowDate [current date].
     * @param {Array} timeList [list of date].
     */
  getAvailMonth (nowDate, timeList) {
    let currentHour = this.addZero(nowDate.getHours())
    let currentMinute = this.addZero(nowDate.getMinutes())
    let currentHourSecond = this.addZero(nowDate.getSeconds())
    // 2017/08/30 11:17:27
    let lastTime = currentHour + ':' + currentMinute + ':' + currentHourSecond
    let currentDay = this.addZero(nowDate.getDay())

    let currentYear = nowDate.getFullYear()
    let currentMonth = nowDate.getMonth() + 1
    if (currentMonth > 5) {
      let firstMonth = currentMonth - 5
      for (let i = 0; i < 6; i++) {
        let dateObj = {}
        dateObj['orginalDate'] = this.checkNumber(firstMonth) + '.' + currentYear
        dateObj['whiteDate'] = new Date(currentYear + '/' + this.checkNumber(firstMonth) + '/' + currentDay + ' ' + lastTime)
        dateObj['whiteDate'] = dateObj['whiteDate'].getTime()
        timeList.push(dateObj)
        firstMonth++
      }
    } else if (currentMonth >= 1) {
      let lastYearAmount = 6 - currentMonth
      for (let i = lastYearAmount; i > 0; i--) {
        let dateObj = {}
        dateObj['orginalDate'] = this.checkNumber(12 - i + 1) + '.' + (currentYear - 1)
        dateObj['whiteDate'] = new Date((currentYear - 1) + '/' + this.checkNumber(12 - i + 1) + '/' + currentDay + ' ' + lastTime)
        dateObj['whiteDate'] = dateObj['whiteDate'].getTime()
        timeList.push(dateObj)
      }
      for (let i = 0; i < currentMonth; i++) {
        let dateObj = {}
        dateObj['orginalDate'] = this.checkNumber(i + 1) + '.' + currentYear
        dateObj['whiteDate'] = new Date(currentYear + '/' + this.checkNumber(i + 1) + '/' + currentDay + ' ' + lastTime)
        dateObj['whiteDate'] = dateObj['whiteDate'].getTime()
        timeList.push(dateObj)
      }
    }
    for (let i = 0; i < timeList.length; i++) {
      timeList[i]['translateDates'] = this.commonService.dealWithDateJson('D03', timeList[i]['whiteDate'])
    }
    let allAvailable = {
      orginalDate: 'all_available',
      translateDates: this.translate.instant('all_available')
    }
    timeList.push(allAvailable)
    timeList.reverse()
  }
}
