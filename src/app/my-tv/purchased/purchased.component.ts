import * as _ from 'underscore'
import { Component, ViewChild, OnInit, OnDestroy } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { EventService } from 'ng-epg-sdk/services'
import { Router } from '@angular/router'
import { VODRouterService } from '../../shared/services/vod-router.service'
import { PurchasedService } from './purchased.service'
import { SuspensionComponentPackage } from 'ng-epg-ui/webtv-components/packageSuspension/packageSuspension.component'
import { TranslateService } from '@ngx-translate/core'
import { CommonService } from '../../core/common.service'

@Component({
  selector: 'app-purchased',
  templateUrl: './purchased.component.html',
  styleUrls: ['./purchased.component.scss']
})

export class PurchasedComponent implements OnInit, OnDestroy {
    // declare child component SuspensionComponentPackage.
  @ViewChild(SuspensionComponentPackage) suspensionpackage: SuspensionComponentPackage
    // declare variables.
    // to save the amount of all single products.
  public singleTotalAll = 0
    // to show amount of all single products on the page.
  public singleTotalStr = ''
    // to save list of single products.
  public dataList = []
    // to encapsulate data of single products.
  public singleLists: {
    'dataList': Array<any>,
    'suspensionID': string
  }
    // to save the amount of all package products.
  public packTotalAll = 0
    // to show amount of all package products on the page.
  public packTotalStr = ''
    // to save list of package products.
  public packageProducts: Array<any>
    // to save the current Date.
  public nowDate
    // to save the picture url of long-term product.
  public moreDaysUrl = ''
    // to judge whether show the suspension of package product.
  public isShow = false
    // to judge show single product list or package product list.
  public showSingle = false
    // to judge whether show the drop list of date.
  public isShowTimeList = false
    // to judge whether the browser is Chrome.
  public isChrome = true
    // to judge whether the browser is IE.
  public isIE = false
    // to save the list of date available.
  public timeList = []
    // to save the value of date selected.
  public selectedDate: string
    // to save the start time of querying.
  public fromDate: number
    // to save the end time of querying.
  public toDate: number
    // timer for showing package suspension.
  public susTimeChannel
    // to judge whether there is no single product.
  public singleNoData = true
    // to judge whether there is no package product.
  public packageNoData = true
    // to save the value of date selected temporarily.
  public chooseTime = ''
    // debounce function from underscore.
  public susFilter = _.debounce((fc: Function) => { fc() }, 400)
    // to judge whether the mouse is in package product.
  public susUse = true
    // to save length of all products.
  public allProductsLength = 0
  public showLoadIcon = true
  public firstLoad = true
    /**
     * constructor of class PurchasedComponent.
     * instantiate services.
     */
  constructor (
        private purchasedService: PurchasedService,
        private route: ActivatedRoute,
        private router: Router,
        private vodRouter: VODRouterService,
        private translate: TranslateService,
        private commonService: CommonService
    ) { }

    /**
     * init function.
     * get product datas and register event listener.
     */
  ngOnInit () {
    if (!Cookies.getJSON('IS_PROFILE_LOGIN')) {
      return
    }
    _.delay(() => {
      this.moreDaysUrl = 'assets/img/56/purchase2_56.png'
        // this.nowDate = Date.now();
        // this.purchasedService.getAvailMonth(this.nowDate, this.timeList);
        // this.selectedDate = this.timeList[0]['translateDates'];
        // this.selectTime(this.timeList[0]);
        // this.checkBrowser();
      this.getProducts(true)
    }, 500)
    EventService.on('ScreenSize', () => {
      if (document.body.clientWidth > 1440 && document.body.clientWidth <= 1920) {
        document.body.scrollTop = 0
        document.documentElement.scrollTop = 0
      } else if (document.body.clientWidth <= 1440) {
          document.body.scrollTop = 0
          document.documentElement.scrollTop = 0
        }
    })
    EventService.removeAllListeners(['LOAD_DATA'])
    EventService.on('LOAD_DATA', () => {
      if (this.allProductsLength !== 0 && (this.dataList.length + this.packageProducts.length) < this.allProductsLength) {
        let req = this.createLoadRequest()
        this.firstLoad = false
        this.showLoadIcon = true
        this.purchasedService.getSubScription(req).then(resp => {
            if (resp['singleList'].length > 0) {
              this.dataList = this.dataList.concat(resp['singleList'])
              this.singleTotalAll = this.dataList.length
              this.singleTotalStr = '(' + this.singleTotalAll + ')'
              this.singleLists = {
                'dataList': this.dataList,
                'suspensionID': 'purchasedListID'
              }
            }
            if (resp['packageList'].length > 0) {
              this.packageProducts = this.packageProducts.concat(resp['packageList'])
              this.packTotalAll = this.packageProducts.length
              this.singleTotalStr = '(' + this.packTotalAll + ')'
            }
            this.showLoadIcon = false
          })
      }
    })
  }

    /**
     * create request parameter for interface QuerySubscription.
     */
  createLoadRequest () {
    let req = {}
    if (this.chooseTime === 'all_available') {
      req = {
        count: '30',
        offset: (this.dataList.length + this.packageProducts.length) + '',
        sortType: 'ORDERTIME:DESC'
      }
    } else {
      req = {
        fromDate: this.fromDate + '',
        toDate: this.toDate + '',
        count: '30',
        offset: (this.dataList.length + this.packageProducts.length) + '',
        sortType: 'ORDERTIME:DESC'
      }
    }
    return req
  }

    /**
     * destroy function.
     * clear timer and event listener.
     */
  ngOnDestroy () {
    clearTimeout(this.susTimeChannel)
    EventService.removeAllListeners(['LOAD_DATA'])
  }

    /**
     * get the type of current browser.
     */
  checkBrowser () {
    let explorer = navigator.userAgent.toLowerCase()
      // Chrome
    if (explorer.indexOf('chrome') >= 0) {
      this.isChrome = true
    } else {
      this.isChrome = false
    }
      // IE
    this.isIE = !!explorer.match(/msie/i) || !!explorer.match(/rv:([\d.]+).*like gecko/)
  }

    /**
     * process product datas.
     * @param {boolean} selectAll [true: query all products. false: query products of selected month]
     */
  dataHandle (selectAll: boolean) {
    this.singleLists = {
      'dataList': [],
      'suspensionID': ''
    }
    this.packageProducts = []
    if (selectAll) {
      this.getProducts(true, this.fromDate, this.toDate)
    } else {
      this.getProducts(false, this.fromDate, this.toDate)
    }
  }

    /**
     * product datas handler.
     * @param {boolean} selectAll [true: query all products. false: query products of selected month]
     */
  getProducts (selectAll: boolean, fromDate?, toDate?) {
    let req
    if (selectAll) {
      req = {
        count: '30',
        offset: '0',
        sortType: 'ORDERTIME:DESC'
      }
    } else {
      req = {
        fromDate: fromDate + '',
        toDate: toDate + '',
        count: '30',
        offset: '0',
        sortType: 'ORDERTIME:DESC'
      }
    }
    this.showLoadIcon = true
    this.purchasedService.getSubScription(req).then((resp) => {
      if (resp['allTotal'] === 0) {
        this.singleNoData = true
        this.singleTotalAll = 0
        this.singleTotalStr = ''
        this.packageNoData = true
        this.packTotalAll = 0
        this.packTotalStr = ''
      } else {
        this.allProductsLength = resp['allTotal']
        this.dataList = resp['singleList']
        this.packageProducts = resp['packageList']
        this.handleSingleInfo()
        this.handlePackageInfo()
      }
      this.showLoadIcon = false
    })
  }

    /**
     * process single products.
     */
  handleSingleInfo () {
    if (this.dataList.length > 0) {
      this.singleNoData = false
      this.singleTotalAll = this.dataList.length
      this.singleLists = {
        'dataList': this.dataList,
        'suspensionID': 'purchasedListID'
      }
      this.singleTotalStr = '(' + this.singleTotalAll + ')'
    } else {
      this.singleNoData = true
      this.singleTotalAll = 0
      this.singleTotalStr = ''
    }
  }

    /**
     * process package products.
     */
  handlePackageInfo () {
    if (this.packageProducts.length > 0) {
      this.packageNoData = false
      this.packTotalAll = this.packageProducts.length
      this.packTotalStr = '(' + this.packTotalAll + ')'
    } else {
      this.packageNoData = true
      this.packTotalAll = 0
      this.packTotalStr = ''
    }
  }

    /**
     * toggle to show or hide list of date.
     */
  showTimeList () {
    if (this.isShowTimeList) {
      this.isShowTimeList = false
    } else {
      this.isShowTimeList = true
    }
  }

    /**
     * mouse leave the list of date.
     */
  leaveTimeList () {
    this.isShowTimeList = false
  }

    /**
     * select a month and get the relevant product datas.
     * @param {Object} time [month selected]
     */
  selectTime (time) {
    this.chooseTime = time['orginalDate']
    this.selectedDate = time['translateDates']
    this.isShowTimeList = false
    if (time['orginalDate'] === 'all_available') {
      this.dataHandle(true)
    } else {
      let year = Number(this.chooseTime.substring(3, 7))
      let month = Number(this.chooseTime.substring(0, 2))
      let daysOfMonth = this.purchasedService.getDays(year, month)
      this.fromDate = new Date(year, month - 1, 1, 0, 0, 0).getTime()
      this.toDate = new Date(year, month - 1, daysOfMonth, 23, 59, 59).getTime()
      this.dataHandle(false)
    }
  }

    /**
     * when user click the package content, it will show toast to notify user.
     */
  clickPackage () {
    EventService.emit('PACKAGE_CANT_OPEN')
  }

    /**
     * click single title and show single products.
     */
  selectSingle () {
    this.showSingle = true
    this.dom('single-title').style.color = '#f5f5f5'
    this.dom('package-title').style.color = '#8f8f8f'
  }

    /**
     * click package title and show package products.
     */
  selectPackage () {
    this.showSingle = false
    this.dom('single-title').style.color = '#8f8f8f'
    this.dom('package-title').style.color = '#f5f5f5'
  }

    /**
     * click VOD poster to go to the VOD playing page.
     * @param {Object} $event [click event]
     */
  detailVod ($event) {
    if ($event) {
      this.vodRouter.navigate($event)
    }
  }

    /**
     * mouse enter a package product.
     * @param {Object} option [data of the product]
     * @param {Object} event [mouseenter event]
     */
  mouseenter (option, event) {
    let currentTarget = window.event && window.event.currentTarget || event && event.currentTarget
    this.susUse = true
    this.susFilter(() => {
      let self = this
      if (self.susUse === false) {
        return
      }
      self.isShow = false
      clearTimeout(self.susTimeChannel)
      self.susTimeChannel = setTimeout(function () {
        self.isShow = true
      }, 500)
      self.suspensionpackage.setDetail(option, 'suspensionInfo', currentTarget)
      self.commonService.moveEnter(currentTarget, 'suspensionInfo', document.body.clientWidth, 'purchased')
    })
  }

    /**
     * mouse leave a package product.
     */
  mouseleave () {
    let self = this
    self.susUse = false
    clearTimeout(self.susTimeChannel)
    EventService.removeAllListeners(['showPackageSuspension'])
    EventService.on('showPackageSuspension', function (option) {
      self.isShow = true
    })
    EventService.removeAllListeners(['closePackageSuspension'])
    EventService.on('closePackageSuspension', function () {
      self.isShow = false
    })
    self.isShow = false
  }

    /**
     * to prevent shake phenomenon of dot.
     */
  showWidth () {
    let direction = document.querySelector('.package-description')['style']['-webkit-box-direction']
    let style = {}
    if (direction === 'reverse') {
      style['-webkit-box-direction'] = 'normal'
    } else {
      style['-webkit-box-direction'] = 'reverse'
    }
    return style
  }

    /**
     * to satisfy the compatibility of IE browser.
     */
  showDot () {
    for (let i = 0; i < this.packageProducts.length; i++) {
      if (this.dom('description-' + i) && this.dom('dot-' + i) && this.dom('dot-' + i)['style']) {
        if (this.dom('description-' + i)['clientHeight'] > 40) {
            this.dom('dot-' + i)['style']['display'] = 'block'
          } else {
            this.dom('dot-' + i)['style']['display'] = 'none'
          }
      } else {
        return
      }
    }
  }

    /**
     * get dom element by element id.
     * @param {string} divName [element id]
     */
  dom (divName: string) {
    return document.getElementById(divName)
  }
}
