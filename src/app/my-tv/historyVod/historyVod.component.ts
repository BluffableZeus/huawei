import * as _ from 'underscore'
import { Component, ViewChild, OnInit, OnDestroy } from '@angular/core'
import { ShowHistory } from './historyVod.service'
import { EventService } from 'ng-epg-sdk/services'
import { Router, ActivatedRoute } from '@angular/router'
import { VODRouterService } from '../../shared/services/vod-router.service'
import { MyVideos } from 'ng-epg-ui/webtv-components/myVideos/my-videos.component'
import { session } from 'src/app/shared/services/session'

@Component({
  selector: 'app-mytv-historyvod',
  styleUrls: ['./historyVod.component.scss'],
  templateUrl: './historyVod.component.html'
})

export class MyTVHistoryVodComponent implements OnInit, OnDestroy {
  @ViewChild(MyVideos) myVideos: MyVideos
    // check whether interface call is failed.'1' means failed.
  failed = '0'
    // to judge whether it is editing state. true means normal state, false means editing state.
  show = true
    // to judge whether show clear confirmation dialog.
  isClearAll = false
    // save all history data.
  historyLists: Array<any> = []
    // save history data of today.
  todayData: Array<any> = []
    // save history data of yesterday.
  yesterdayData: Array<any> = []
    // save history data of this week.
  thisWeekData: Array<any> = []
    // save history data earlier.
  earlierData: Array<any> = []
    // encapsulate history data of today for child component.
  historyListsToday: {
    'dataList': Array<any>,
    'suspensionID': string
  }
    // encapsulate history data of yesterday for child component.
  historyListsYesterday: {
    'dataList': Array<any>,
    'suspensionID': string
  }
    // encapsulate history data of this week for child component.
  historyListsThisWeek: {
    'dataList': Array<any>,
    'suspensionID': string
  }
    // encapsulate history data earlier for child component.
  historyListsEarlier: {
    'dataList': Array<any>,
    'suspensionID': string
  }
    // ID list of VOD to be deleted.
  vodIDs: Array<string> = []
    // amount of all history datas.
  totalAll = 0
    // amount of history datas loaded.
  nowDataLength = 0
    // show amount of all history datas on the page.
  totalStr = ''
    // to judge whether there is no history.
  noDataList = '1'
    // timer for showing failed information.
  failTimer: any
    // keys for showing title and prompt content on the delete confirmation dialog.
  confirmInfoKey = ['deleteConfirmInfo', 'deleteConfirmTitle']
    // delay time of querying history.
  delayTime = 0
    // to judge whether show the loading icon.
  showLoadIcon = false
    // to judge loading type, true means loading content when you first enter page, false means rolling load.
  firstLoad = true
    // to judge whether the delete operation is finished.
  public finishDelete = true
    // to save the last ID deleted.
  public lastId
    // private constructor
  constructor (
        private showHistory: ShowHistory,
        private route: ActivatedRoute,
        private router: Router,
        private vodRouter: VODRouterService
    ) { }

    /**
     * init function.
     * get history data and register event listener.
     */
  ngOnInit () {
    if (!Cookies.getJSON('IS_PROFILE_LOGIN')) {
      return
    }
    this.showLoadIcon = true
    this.showData()
      // listening to the event that the scrollbar is scrolled to the bottom.
    EventService.removeAllListeners(['LOAD_DATA'])
    EventService.on('LOAD_DATA', () => {
      if (this.historyLists && this.historyLists.length > 0 && this.show) {
        if (this.totalAll !== this.historyLists.length) {
            this.showLoadIcon = true
            this.firstLoad = false
            this.showHistory.showHistoryVod({
              'offset': this.nowDataLength + '',
              'count': '36',
              'bookmarkTypes': ['VOD'],
              'sortType': 'UPDATE_TIME:DESC'
            }).then((resp) => {
              let data = this.historyLists
              if (resp) {
                let moredata = data.concat(resp)
                this.dateType(moredata)
                this.historyLists = moredata
                this.nowDataLength = moredata.length
                this.showLoadIcon = false
              }
            })
          }
      }
    })
    EventService.removeAllListeners(['DELETE_HISTORY'])
    EventService.on('DELETE_HISTORY', (data) => {
      if (!data) {
        return
      }
      this.vodIDs = []
      let dateType
      let nowTime = this.showHistory.dateToString(Date.now(), 'yyyy-MM-dd')
      let updateTime = this.showHistory.dateToString(new Date(Number(data['time'])), 'yyyy-MM-dd')
      let dateDiff = this.showHistory.daysBetween(nowTime, updateTime)
      if (dateDiff === 0) {
        dateType = 'today'
      } else if (dateDiff === 1) {
          dateType = 'yesterday'
        } else if (dateDiff >= 2 && dateDiff <= 7) {
          dateType = 'thisWeek'
        } else {
          dateType = 'earlier'
        }
      if (dateType === 'today') {
        let datalist = _.extend({}, this.historyListsToday['dataList'])
        _.each(datalist, (item, index) => {
            if (item['id'] === data.id) {
              this.historyListsToday['dataList'].splice(index, 1)
            }
          })
      } else if (dateType === 'yesterday') {
          let datalist = _.extend({}, this.historyListsYesterday['dataList'])
          _.each(datalist, (item, index) => {
            if (item['id'] === data.id) {
              this.historyListsYesterday['dataList'].splice(index, 1)
            }
          })
        } else if (dateType === 'thisWeek') {
          let datalist = _.extend({}, this.historyListsThisWeek['dataList'])
          _.each(datalist, (item, index) => {
            if (item['id'] === data.id) {
              this.historyListsThisWeek['dataList'].splice(index, 1)
            }
          })
        } else if (dateType === 'earlier') {
          let datalist = _.extend({}, this.historyListsEarlier['dataList'])
          _.each(datalist, (item, index) => {
            if (item['id'] === data.id) {
              this.historyListsEarlier['dataList'].splice(index, 1)
            }
          })
        }
      this.vodIDs.push(data.id)
      this.dealDeleteVOD(data)
    })
  }
    /**
     * clear the timer,and remove the eventservice.
     */
  ngOnDestroy () {
    EventService.removeAllListeners(['LOAD_DATA'])
    clearTimeout(this.failTimer)
  }
    /**
     * get the history of video on demand.
     */
  showData () {
    let delayTime = session.pop('QUERY_HISTORY_DELAY')
    if (_.isUndefined(delayTime)) {
      delayTime = 0
    }
    let req = {
      'count': '36',
      'offset': '0',
      'bookmarkTypes': ['VOD'],
      'sortType': 'UPDATE_TIME:DESC'
    }
    _.delay(() => {
      this.showHistory.showHistoryVod(req).then((resp) => {
        this.showLoadIcon = false
        if (resp.length === 0) {
            this.noDataList = '1'
          } else {
            this.noDataList = '0'
            this.historyLists = resp
            this.nowDataLength = resp.length
            this.totalAll = Number(resp[0].totalAll)
            if (this.totalAll === 0) {
              this.noDataList = '1'
            } else {
              this.totalStr = '(' + this.totalAll + ')'
            }
            this.dateType(resp)
          }
      })
    }, delayTime)
  }

    /**
     * the play record is divided into four types.
     */
  dateType (resp) {
    for (const item of resp) {
      if (item['dateType'] === 'today') {
        this.todayData.push(item)
      } else if (item['dateType'] === 'yesterday') {
          this.yesterdayData.push(item)
        } else if (item['dateType'] === 'thisWeek') {
          this.thisWeekData.push(item)
        } else if (item['dateType'] === 'earlier') {
          this.earlierData.push(item)
        }
    }

    this.historyListsToday = {
      'dataList': this.todayData,
      'suspensionID': 'today'
    } // keep the today's record
    this.historyListsYesterday = {
      'dataList': this.yesterdayData,
      'suspensionID': 'yesterday'
    } // keep the yesterday's record
    this.historyListsThisWeek = {
      'dataList': this.thisWeekData,
      'suspensionID': 'thisWeek'
    } // keep this week's record
    this.historyListsEarlier = {
      'dataList': this.earlierData,
      'suspensionID': 'earlier'
    } // keep earlier record
  }
    /**
     * click the edit button.
     */
  editData () {
    this.show = false
    EventService.emit('DELETE', '1')
    EventService.emit('IS_VODEDIT', false) // if false,you can not to click the picture to play or show the suspension.
  }
    /**
     * click the 'clear all' button.
     */
  clearAllData () {
    this.isClearAll = true
  }
    /**
     * click the 'complete' button.
     */
  completeData () {
    this.show = true
    EventService.emit('COMPLETE', '1')
    EventService.emit('IS_VODEDIT', true) // if true,you can click the picture to play and show the suspension.
  }
    /**
     * click the delete button of the picture.
     */
  deleteVod ($event) {
    this.vodIDs = []
    if (this.finishDelete || $event['data'].id !== this.lastId) {
      this.finishDelete = false
      this.lastId = $event['data'].id
      this.vodIDs.push($event['data'].id)
      if (this.vodIDs.length !== 0) {
        let req = {
            'bookmarkTypes': ['VOD'],
            'itemIDs': this.vodIDs
          }
        this.showHistory.deleteHistoryVod(req).then((resp) => {
            if (resp['result']['retCode'] === '000000000') {
              if ($event['data']['dateType'] === 'today') {
                this.historyListsToday['dataList'].splice($event['index'], 1)
              }
              if ($event['data']['dateType'] === 'yesterday') {
                this.historyListsYesterday['dataList'].splice($event['index'], 1)
              }
              if ($event['data']['dateType'] === 'thisWeek') {
                this.historyListsThisWeek['dataList'].splice($event['index'], 1)
              }
              if ($event['data']['dateType'] === 'earlier') {
                this.historyListsEarlier['dataList'].splice($event['index'], 1)
              }
              this.dealDeleteVOD($event['data'])
            }
            this.finishDelete = true
          }, () => {
            this.finishDelete = true
          })
      }
    }
  }
    /**
     * process relevant history information after delete operation.
     */
  dealDeleteVOD (data) {
    let vodData = this.historyLists.find(item => item['id'] === data.id)
    let index = _.indexOf(this.historyLists, vodData)
    this.historyLists.splice(index, 1)
    this.nowDataLength = this.nowDataLength - this.vodIDs.length
    this.totalAll = this.totalAll - this.vodIDs.length
    if (this.nowDataLength <= 0) {
      this.noDataList = '1'
      this.totalStr = ''
      this.completeData()
    } else {
      this.noDataList = '0'
      this.totalStr = '(' + this.totalAll + ')'
    }
    this.vodIDs = []
    EventService.emit('Refreshed')
  }
    /**
     * click the picture to jump to detail.
     */
  detailVod ($event) {
    this.vodRouter.navigate($event[0])
  }
    /**
     * click the cancel button of the 'cannel all' dialog.
     */
  cancelData ($event) {
    this.isClearAll = false
  }
    /**
     * click the confirm button of the 'cannel all' dialog.
     */
  confirmData ($event) {
    let req = {
      'bookmarkTypes': ['VOD']
    }
    this.showHistory.deleteHistoryVod(req)
        .then(() => {
          this.isClearAll = false
          this.show = true
          this.totalStr = ''
          this.noDataList = '1'
          this.todayData = []
          this.yesterdayData = []
          this.thisWeekData = []
          this.earlierData = []
        })
        .catch(() => {
          this.failed = '1'
          this.failTimer = setTimeout(() => this.failed = '0', 2000)
          this.showData()
        })
  }
    /**
     * if there are no play's record,change the style of the edit button.
     */
  noDataStyle () {
    let styles
    if (this.noDataList === '1') {
      styles = {
        'color': '#5f5f5f',
        'cursor': 'default'
      }
    } else {
      styles = {}
    }
    return styles
  }
}
