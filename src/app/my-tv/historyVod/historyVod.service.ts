import * as _ from 'underscore'
import { Injectable } from '@angular/core'
import { queryBookmark, deleteBookmark } from 'ng-epg-sdk/vsp'

@Injectable()
export class ShowHistory {
  /**
     * function used to convert time to string.
     */
  dateToString (historyDate, fmt) {
    let o = {
      'M+': historyDate.getMonth() + 1,
      'd+': historyDate.getDate(),
      'H+': historyDate.getHours(),
      'm+': historyDate.getMinutes(),
      's+': historyDate.getSeconds(),
      'S': historyDate.getMilliseconds()
    }
    if (/(y+)/.test(fmt)) {
      fmt = fmt.replace(RegExp.$1, (historyDate.getFullYear() + '').substr(4 - RegExp.$1.length))
    }
    for (let k in o) {
      if (new RegExp('(' + k + ')').test(fmt)) {
        fmt = fmt.replace(RegExp.$1, (RegExp.$1.length === 1) ? (o[k]) : (('00' + o[k]).substr(('' + o[k]).length)))
      }
    }
    return fmt
  }
  /**
     * function used to deal with the time on now and the time after update.
     */
  daysBetween (dateOne, dateTwo) {
    let oneMonth = dateOne.substring(5, dateOne.lastIndexOf('-'))
    let oneDay = dateOne.substring(dateOne.length, dateOne.lastIndexOf('-') + 1)
    let oneYear = dateOne.substring(0, dateOne.indexOf('-'))

    let twoMonth = dateTwo.substring(5, dateTwo.lastIndexOf('-'))
    let twoDay = dateTwo.substring(dateTwo.length, dateTwo.lastIndexOf('-') + 1)
    let twoYear = dateTwo.substring(0, dateTwo.indexOf('-'))

    let cha = ((Date.parse(oneMonth + '/' + oneDay + '/' + oneYear) - Date.parse(twoMonth + '/' + twoDay + '/' + twoYear)) / 86400000)
    return Math.abs(cha)
  }
  /**
     * function used to process series information of VOD.
     */
  historySeries (historyInfo) {
    let historySeriesCount = {}
    let series
    let episodeCount
    if (!_.isUndefined(historyInfo.series) && !_.isUndefined(historyInfo.series[0]) &&
            !_.isUndefined(historyInfo.series[0].sitcomNO)) {
      series = Number(historyInfo.series[0].sitcomNO)
    } else {
      series = 0
    }
    if (!_.isUndefined(historyInfo.bookmark) && !_.isUndefined(historyInfo.bookmark.sitcomNO)) {
      episodeCount = Number(historyInfo.bookmark.sitcomNO)
    } else {
      episodeCount = 0
    }
    historySeriesCount['series'] = series
    historySeriesCount['episodeCount'] = episodeCount
    return historySeriesCount
  }
  /**
     * function used to get the play's record of video on demand.
     */
  showHistoryVod (req: any) {
    return queryBookmark(req).then(resp => {
      let totalAll = resp.total
      return resp.bookmarks.map(bookmark => {
        let series
        let episodeCount
        let dateType
        let historyInfo = bookmark['VOD']
        let nowTime = this.dateToString(Date.now(), 'yyyy-MM-dd')
        let updateTime = this.dateToString(new Date(Number(historyInfo.bookmark.updateTime)), 'yyyy-MM-dd')
        let dateDiff = this.daysBetween(nowTime, updateTime)
        let vodBookmark = Number(historyInfo.bookmark.rangeTime)
        let vodElapseTime = 0
        if (!_.isUndefined(historyInfo.mediaFiles) && !_.isUndefined(historyInfo.mediaFiles[0]) &&
                    !_.isUndefined(historyInfo.mediaFiles[0].elapseTime)) {
          vodElapseTime = Number(historyInfo.mediaFiles[0].elapseTime)
        }
        dateType = this.getDateType(dateDiff)
        series = this.historySeries(historyInfo)['series']
        episodeCount = this.historySeries(historyInfo)['episodeCount']

        return {
          dateType: dateType,
          id: historyInfo.ID,
          vodName: historyInfo.name,
          isHistory: '1',
          totalAll: totalAll,
          floatInfo: historyInfo,
          titleName: dateType,
          vodBookmark: vodBookmark,
          vodElapseTime: vodElapseTime,
          series: {
            'num': series
          },
          episodeCount: {
            'num': episodeCount
          }
        }
      })
    })
  }

  /**
     * function used to classify history by date.
     */
  getDateType (dateDiff) {
    let dateType
    if (dateDiff === 0) {
      dateType = 'today'
    } else if (dateDiff === 1) {
      dateType = 'yesterday'
    } else if (dateDiff >= 2 && dateDiff <= 7) {
      dateType = 'thisWeek'
    } else {
      dateType = 'earlier'
    }
    return dateType
  }

  /**
     * function used to call interface to delete history.
     */
  deleteHistoryVod (req: any) {
    return deleteBookmark(req).then(resp => resp)
  }
}
