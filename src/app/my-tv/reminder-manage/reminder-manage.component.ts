import * as _ from 'underscore'
import { Component, ViewChild, OnInit, OnDestroy } from '@angular/core'
import { MyTvService } from '../my-tv.service'
import { ReminderManageService } from './reminder-manage.service'
import { TranslateService } from '@ngx-translate/core'
import { Router } from '@angular/router'
import { EventService } from 'ng-epg-sdk/services'
import { SuspensionComponentProgram } from 'ng-epg-ui/webtv-components/programSuspension/programSuspension.component'
import { ReminderService } from '../../component/reminder/reminder.service'
import { PlaybillAppService } from '../../component/playPopUpDialog/playbillApp.service'
import { CommonService } from '../../core/common.service'

@Component({
  selector: 'app-reminder-manage',
  styleUrls: ['./reminder-manage.component.scss'],
  templateUrl: './reminder-manage.component.html'
})

export class ReminderManageComponent implements OnInit, OnDestroy {
    // declare child component SuspensionComponentProgram.
  @ViewChild(SuspensionComponentProgram) reminderFloat: SuspensionComponentProgram
    // declare variables.
    // to judge whether it is editing state. true means normal state, false means editing state.
  public isEdit = false
    // to judge whether there is no reminder.
  public isNoData = true
    // to save reminders data today.
  public todayReminders: Array<any> = []
    // to save reminders data in future.
  public futherReminders: Array<any> = []
    // to judge whether show the suspension of reminder.
  public isShowFloat = false
    // timer for showing suspension of reminder.
  public floatShowTimer: any
    // to save amount of all reminders.
  public total: any
    // to show amount of all reminders on the page.
  public count = ''
    // to judge whether the new reminder data is loaded.
  public isLoadData = true
    // to judge whether show the loading icon.
  public isShowLoad = false
    // to judge whether show clear confirmation dialog.
  public isClearAll = false
    // to judge whether the delete operation is finished.
  public finishDelete = true
    // to save the last ID deleted.
  public lastReminderID = ''

  public bufferReminderData

  public alwaysShowData

    // keys for showing title and prompt content on the delete confirmation dialog.
  confirmInfoKey = ['deleteConfirmInfo', 'deleteConfirmTitle']

    // private constructor.
  constructor (
        private myTvService: MyTvService,
        private reminderManageService: ReminderManageService,
        private translate: TranslateService,
        private router: Router,
        private reminderService: ReminderService,
        private playbillAppService: PlaybillAppService,
        private commonService: CommonService
    ) { }

    /**
     * init function.
     * get reminder data and register event listener.
     */
  ngOnInit () {
    if (!Cookies.getJSON('IS_PROFILE_LOGIN')) {
      return
    }
    this.alwaysShowData = 0
    this.getReminder()
      // listening to the event that the scrollbar is scrolled to the bottom.
    EventService.removeAllListeners(['LOAD_DATA'])
    EventService.on('LOAD_DATA', () => {
        // if the number of reminder on show less than the total,the scrollbar is scrolled to load the data.
      if (this.todayReminders.length + this.futherReminders.length < this.total && this.isLoadData) {
        this.isLoadData = false
        this.bufferReminderData = []
        this.getReminder()
      }
    })
      // cancel the reminder by suspension.
    EventService.removeAllListeners(['CANCEL_REMINDER_BY_FLOAT'])
    EventService.on('CANCEL_REMINDER_BY_FLOAT', (data) => {
        // remove this reminder and close the suspension.
      this.removeReminderFormList(data)
      this.isShowFloat = false
    })
      // refresh the reminder liset when delete one from dialog
    EventService.removeAllListeners(['REMOVE_REMINDER_FROM_DIALOG'])
    EventService.on('REMOVE_REMINDER_FROM_DIALOG', (data) => {
      this.removeReminderFormList(data, true)
    })
      // refresh the reminder liset when delete more from dialog
    EventService.removeAllListeners(['REMOVE_ALL_REMINDER_FROM_DIALOG'])
    EventService.on('REMOVE_ALL_REMINDER_FROM_DIALOG', (data) => {
      _.each(data, (playbillId) => {
        this.removeReminderFormList({ playbillId: playbillId }, true)
      })
    })
  }

    /**
     * destory function.
     * clear the timer, and remove the eventservice.
     */
  ngOnDestroy () {
    clearTimeout(this.floatShowTimer)
  }

    /**
     * get the reminder data.
     */
  getReminder (offset?) {
      // show the loading icon.
    this.isShowLoad = true
      // let offset = this.todayReminders.length + this.futherReminders.length;
    let offsetNum
    if (offset) {
      offsetNum = 0
    } else {
      offsetNum = this.alwaysShowData
    }
    this.reminderManageService.queryReminder(offsetNum).then((resp) => {
      if (resp.list.length > 12) {
        this.bufferReminderData = resp.list.slice(12, 20)
      }

      this.isShowLoad = false
      this.total = Number(resp.total)
      this.count = '(' + this.total + ')'
        /**
             * if there are no reminders,show the tips about that the reminders is empty
             * the count should not be display.
             */
      if (this.total === 0) {
        this.isNoData = true
        this.count = ''
      } else {
        this.isNoData = false
      }
        // judge that the reminder belong to today or belong to future.
      if (offset) {
        this.todayReminders = []
        this.futherReminders = []
      }
      _.each(resp.list.slice(0, 12), (item) => {
        if (this.reminderManageService.isToday(item['startTime'])) {
            item['dateType'] = 'today'
            this.todayReminders.push(item)
          } else {
            item['dateType'] = 'future'
            this.futherReminders.push(item)
          }
      })
      this.alwaysShowData = this.todayReminders.length + this.futherReminders.length
      this.isLoadData = true
      if (offset) {
        this.finishDelete = true
      }
    })
  }

    /**
     * the mouse enter the picture,then show the suspension.
     * @param {Object} reminder [reminder data]
     * @param {Object} event [mouseenter event]
     */
  mouseenter (reminder, event) {
      // if stay editting,the suspension should not be display.
    if (this.isEdit) {
      return
    }
    this.isShowFloat = false
    clearTimeout(this.floatShowTimer)
    this.floatShowTimer = setTimeout(() => {
      this.isShowFloat = true
    }, 500)
    let currentTarget = window.event && window.event.currentTarget || event && event.target
    this.setFloatPostion(reminder, currentTarget)
  }

    /**
     * set the location of suspension.
     * @param {Object} reminder [reminder data]
     * @param {Object} currentTarget [current dom target]
     */
  setFloatPostion (reminder, currentTarget) {
    let self = this
    self.reminderFloat.setDetail(reminder, 'reminderInfo', currentTarget.firstElementChild)
    self.commonService.moveEnter(currentTarget, 'reminderInfo', document.body.clientWidth, 'reminder')
  }

    /**
     * when the mouse leave the picture,the suspension should be hidden.
     */
  closeFloat () {
    clearTimeout(this.floatShowTimer)
    EventService.removeAllListeners(['showProgramSuspension'])
    EventService.on('showProgramSuspension', (data) => {
        // if the mouse leave the picture to the suspension,the suspension should be visible.
      if (data === 'reminder') {
        this.isShowFloat = true
      }
    })
    EventService.removeAllListeners(['closeProgramSuspension'])
    EventService.on('closeProgramSuspension', () => {
      this.isShowFloat = false
    })
    this.isShowFloat = false
  }

    /**
     * click the edit button.
     */
  edit () {
    if (!this.isNoData) {
      this.isEdit = true
    }
  }

    /**
     * click the complete button.
     */
  complete () {
    this.isEdit = false
  }

    /**
     * delete a reminder.
     * @param {Object} reminder [reminder data]
     */
  delete (reminder, index) {
    let self = this
    if (self.finishDelete) {
      EventService.removeAllListeners(['REMOVE_REMINDER_BY_SUSPENSION'])
      self.finishDelete = false

      self.lastReminderID = reminder['playbillId']
      self.reminderService.removeReminder({
        contentIDs: [reminder.playbillId],
        contentTypes: ['PROGRAM']
      }).then(resp => {
          if (resp['result']['retCode'] === '000000000') {
            this.alwaysShowData = this.alwaysShowData - 1
            EventService.emit('REMOVE_REMINDER', reminder)
            self.removeReminderFormList(reminder, false, index)
          } else {
            self.finishDelete = true
          }
        }, respFail => {
          self.finishDelete = true
        })
    }
  }

    /**
     * click 'clear all' button,the  'cannel all' dialog is show.
     */
  deleteAll () {
    this.isClearAll = true
  }

    /**
     * click the cancel button of the 'cannel all' dialog.
     */
  cancelData () {
    this.isClearAll = false
  }

    /**
     * click the confirm button of the 'cannel all' dialog.
     */
  confirmData () {
    this.reminderService.removeReminder({
      contentTypes: ['PROGRAM']
    }).then((resp) => {
      EventService.emit('REMOVE_REMINDER', 'clearAllReminderManage')
      this.todayReminders = []
      this.futherReminders = []
      this.total = 0
      this.count = ''
      this.isNoData = true
      this.isClearAll = false
      this.isEdit = false
    })
  }

    /**
     * remove the reminder that has been delete for the reminder list.
     * @param {Object} reminder [reminder data]
     */
  removeReminderFormList (data, deleteCur?, index?) {
    if (data['dateType'] === 'today' || deleteCur) {
      if (index === 0 || index) {
        this.todayReminders.splice(index, 1)
      } else {
        for (let i = 0; i < this.todayReminders.length; i++) {
            if (this.todayReminders[i]['playbillId'] === data['playbillId']) {
              this.todayReminders.splice(i, 1)
              break
            }
          }
      }
    } else {
      if (index === 0 || index) {
        this.futherReminders.splice(index, 1)
      } else {
        for (let i = 0; i < this.futherReminders.length; i++) {
            if (this.futherReminders[i]['playbillId'] === data['playbillId']) {
              this.futherReminders.splice(i, 1)
              break
            }
          }
      }
    }
    this.total = this.total - 1
    this.count = '(' + this.total + ')'
    if (this.total <= 0) {
      this.count = ''
      this.isNoData = true
      this.isEdit = false
    } else {
      this.isNoData = false
    }
    this.addTo12AndCheckAllTatol()
  }

  addTo12AndCheckAllTatol () {
    if ((this.alwaysShowData < 12) && this.bufferReminderData && (this.bufferReminderData.length > 0)) {
      let data = this.bufferReminderData.slice(0, 1)[0]
      this.bufferReminderData.splice(0, 1)
      if (this.reminderManageService.isToday(data['startTime'])) {
        data['dateType'] = 'today'
        this.todayReminders.push(data)
      } else {
        data['dateType'] = 'future'
        this.futherReminders.push(data)
      }
      this.alwaysShowData = this.alwaysShowData + 1
      this.finishDelete = true
    } else if ((this.todayReminders.length + this.futherReminders.length < this.total) && this.isLoadData &&
         (this.bufferReminderData.length < 1)) {
      this.isLoadData = false
      this.getReminder(true)
    } else {
      this.finishDelete = true
    }
  }

    /**
     * click the picture.
     * if the program is past or future,jump to detail page.
     * if the program is now,enter full screen to play.
     * @param {Object} reminder [reminder data]
     */
  clickReminder (reminder) {
    if (this.isEdit) {
      return
    }
    if (reminder['startTime'] < Date.now()['getTime']() && reminder['endTime'] > Date.now()['getTime']()) {
      if (document.getElementsByClassName('live_load_bg') && document.querySelector('.channel_login_load_content')) {
        document.getElementsByClassName('live_load_bg')[0]['style']['display'] = 'block'
        document.querySelector('.channel_login_load_content')['style']['display'] = 'block'
      }
      EventService.emit('OPEN_FULLSCREEN_LIVETVVIDEO', {})
      let video = document.querySelector('#videoContainer video')
      let ua = navigator.userAgent.toLowerCase()
      let isIe = !!ua.match(/msie/i) || !!ua.match(/rv:([\d.]+).*like gecko/)
      let isFirefox = !!ua.match(/firefox\/([\d.]+)/)
      let isEdge = !!ua.match(/edge\/([\d.]+)/)
      if (isIe) {
        video = document.querySelector('#videoContainer')
        video['msRequestFullscreen']()
      } else if (isEdge) {
          video = document.querySelector('#videoContainer')
          video.webkitRequestFullScreen()
        } else if (isFirefox) {
          video = document.querySelector('#videoContainer')
          video['mozRequestFullScreen']()
        } else if (ua.match(/version\/([\d.]+).*safari/)) {
          video.requestFullscreen()
        } else {
          video.webkitRequestFullScreen()
        }
      this.reminderService.getChannelDetail(reminder['playbillId']).then((resp) => {
        let playbill = resp['playbillDetail']
        this.playbillAppService.playProgram(playbill, playbill['channelDetail'])
      })
    } else {
      this.router.navigate(['live-tv', 'live-tv-detail', Number(reminder['playbillId']), reminder['endTime']])
    }
  }
}
