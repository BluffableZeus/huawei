import * as _ from 'underscore'
import { Injectable } from '@angular/core'
import { queryReminder } from 'ng-epg-sdk/vsp'
import { TranslateService } from '@ngx-translate/core'
import { DateUtils } from 'ng-epg-sdk/utils'
import { PictureService } from 'ng-epg-ui/services/picture.service'
import { CommonService } from '../../core/common.service'
import { session } from 'src/app/shared/services/session'
@Injectable()
export class ReminderManageService {
  /**
     * constructor of class ReminderManageService.
     */
  constructor (
        private translate: TranslateService,
        private pictureService: PictureService,
        private commonService: CommonService
  ) { }

  /**
     * get the reminder datas in bulk.
     * @param {string} offset [offset of querying position]
     */
  queryReminder (offset) {
    return queryReminder({ count: '20', offset: offset + '' }).then((resp) => {
      if (Number(resp.total) > 0) {
        let list = _.map(resp.reminders, (item) => {
          let playbill = item.playbill
          let poster = playbill['picture'] && playbill['picture'].posters &&
                        this.pictureService.convertToSizeUrl(playbill['picture'].posters[0],
                          { minwidth: 216, minheight: 119, maxwidth: 256, maxheight: 142 })
          let logo = playbill['channel'] && playbill['channel'].logo &&
                    this.pictureService.convertToSizeUrl(playbill['channel'].logo.url,
                      { minwidth: 40, minheight: 40, maxwidth: 40, maxheight: 40 })
          let leftTime = Math.ceil((new Date(Number(playbill['endTime'])).getTime() -
                    new Date(Number(Date.now()['getTime']())).getTime()) / 60000)
          let leftTimes
          if (leftTime < 0) {
            leftTime = 0
          }
          leftTimes = { min: leftTime }
          let currentDuration = Date.now()['getTime']() - Number(playbill['startTime'])
          let playbilllTime = Number(playbill['endTime']) - Number(playbill['startTime'])
          let month = DateUtils.format(parseInt(playbill['startTime'], 10), 'MM')
          let day = DateUtils.format(parseInt(playbill['startTime'], 10), 'D')
          month = this.commonService.dealWithTime(month)
          if (session.get('languageName') === 'zh') {
            month = month + '/' + day
          } else {
            month = month + ' ' + day
          }

          return {
            playbillId: playbill.ID,
            channelId: playbill['channelID'],
            name: playbill['name'],
            reminderTime: playbill['reminder'].reminderTime,
            startTime: playbill['startTime'],
            endTime: playbill['endTime'],
            channelName: playbill['channel'].name,
            weekTime: this.commonService.dealWithDateJson('D05', Number(playbill['startTime'])),
            time: this.commonService.dealWithDateJson('D10', Number(playbill['startTime'])) + '-' +
                        this.commonService.dealWithDateJson('D10', Number(playbill['endTime'])),
            poster: poster || 'assets/img/default/livetv_program.png',
            channelLogo: logo || 'assets/img/defaultchannel00x80.png',
            floatInfo: playbill,
            floatflag: 'reminder',
            contentType: true,
            len: currentDuration > playbilllTime ? '100%' : Number(currentDuration / playbilllTime * 100) + '%',
            leftTimes: leftTimes,
            leftTime: leftTime
          }
        })
        return { list: list, total: resp.total }
      } else {
        return { list: [], total: 0 }
      }
    })
  }

  /**
     * judge whether the program's start time belongs to to today or future.
     * @param {string} startTime [start time of reminder]
     */
  isToday (startTime) {
    if (new Date(Number(startTime)).toDateString() === new Date(Date.now()).toDateString()) {
      return true
    } else {
      return false
    }
  }
}
