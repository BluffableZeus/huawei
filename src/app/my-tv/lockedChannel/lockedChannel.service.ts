import * as _ from 'underscore'
import { Injectable } from '@angular/core'
import { queryLock, deleteLock, queryPlaybillList } from 'ng-epg-sdk/vsp'
import { PictureService } from 'ng-epg-ui/services/picture.service'
import { session } from 'src/app/shared/services/session'

@Injectable()
export class ShowLocked {
  constructor (private pictureService: PictureService) { }

  /**
     * get locked channels by interface QueryLock.
     * @param {any} req [request parameter of QueryLock]
     */
  showLock (req: any) {
    let self = this
    return queryLock(req).then(resp => {
      let totalAll = resp.locks && resp.locks.length || 0
      let lockedChannel = _.map(resp.locks, (lock) => {
        let lockedInfo = lock.channel
        let url = lockedInfo.picture && lockedInfo.picture.icons &&
                self.pictureService.convertToSizeUrl(lockedInfo.picture.icons[0],
                  { minwidth: 90, minheight: 90, maxwidth: 100, maxheight: 100 })
        return {
          id: lockedInfo.ID,
          ChannelName: lockedInfo.name,
          posterUrl: url || 'assets/img/default/myTV_channel_poster.png',
          totalAll: totalAll,
          floatInfo: lockedInfo
        }
      })
      return lockedChannel
    })
  }

  /**
     * delete locked channels by interface DeleteLock.
     * @param {any} req [request parameter of DeleteLock]
     */
  deleteLock (req: any) {
    return deleteLock(req).then(resp => {
      return resp
    })
  }

  /**
     * get playbill list by interface QueryPlaybillList.
     * @param {any} req [request parameter of QueryPlaybillList]
     */
  getQueryPlaybill (req: any) {
    let options = {
      params: {
        SID: 'queryplaybilllist8',
        DEVICE: 'PC',
        DID: session.get('uuid_cookie')
      }
    }
    return queryPlaybillList(req, options).then(resp => {
      return resp
    })
  }
}
