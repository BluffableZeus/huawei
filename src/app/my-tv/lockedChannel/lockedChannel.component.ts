import * as _ from 'underscore'
import { Component, ViewChild, OnInit, OnDestroy } from '@angular/core'
import { ShowLocked } from './lockedChannel.service'
import { Router, ActivatedRoute } from '@angular/router'
import { EventService } from 'ng-epg-sdk/services'
import { session } from 'src/app/shared/services/session'
import { CommonService } from '../../core/common.service'
import { PlaybillAppService } from '../../component/playPopUpDialog/playbillApp.service'
import { MyChannels } from 'ng-epg-ui/webtv-components/myChannels/my-channels.component'
import { ChannelCacheService } from '../../shared/services/channel-cache.service'
@Component({
  selector: 'app-mytv-lockedchannel',
  styleUrls: ['./lockedChannel.component.scss'],
  templateUrl: './lockedChannel.component.html'
})

export class MyTVChannelLockedComponent implements OnInit, OnDestroy {
  @ViewChild(MyChannels) myChannels: MyChannels
    // declare variables.
    // to save the type of verifying.
  verifyType = 'lockChannel'
    // to save the value of profile ID.
  profileID: string
    // to judge whether it is editing state. true means normal state, false means editing state.
  show = true
    // to judge whether show the password verifying dialog.
  isCheckpass = false
    // to judge whether show the confirmation dialog of clear all operation.
  isClearAll = false
    // to save all locked channel datas.
  lockedLists: Array<any>
    // ID list of locked channels to be deleted.
  channelIDs: Array<string> = []
    // amount of all locked channel datas.
  totalAll = 0
    // amount of locked channel datas loaded.
  nowDataLength = 0
    // show amount of all locked channel datas on the page.
  totalStr = ''
    // to judge whether there is no locked channel.
  noDataList = '1'
    // check whether interface call is failed.'1' means failed.
  failed = '0'
    // to decide the type of prompt information according to the profile type.
  dataType: string
    // timer for showing failed information.
  failTimer: any
    // to judge whether the current profile is main profile.
  isAdmin = false
    // keys for showing title and prompt content on the delete confirmation dialog.
  confirmInfoKey = ['contentConfirmInfo', 'deleteConfirmTitle']
    // to judge whether show the loading icon.
  showLoadIcon = false
    // to judge loading type, true means loading content when you first enter page, false means rolling load.
  firstLoad = true
    // to save the last ID deleted.
  channelLockedIDs: string
  public finishDelete = true

    // private constructor
  constructor (
        private playbillAppService: PlaybillAppService,
        private showLocked: ShowLocked,
        private commonService: CommonService,
        private channelCacheService: ChannelCacheService,
        private route: ActivatedRoute,
        private router: Router) { }

    /**
     * init function.
     * get locked channel datas and register event listener.
     */
  ngOnInit () {
    let self = this
    if (!Cookies.getJSON('IS_PROFILE_LOGIN')) {
      return
    }
    let profileType = Cookies.get('PROFILE_TYPE')
    if (profileType === '0') {
      self.isAdmin = true
      self.dataType = 'locked1'
    } else {
      self.isAdmin = false
      self.dataType = 'locked2'
    }
    self.profileID = session.get('PROFILE_ID')
    self.showLoadIcon = true
    self.showData()
  }

    /**
     * destroy function.
     * clear the timer,and remove the eventservice.
     */
  ngOnDestroy () {
    clearTimeout(this.failTimer)
    EventService.removeAllListeners(['LOAD_DATA'])
  }

    /**
     * get the locked datas of channel.
     */
  showData () {
    let self = this
      // get staticChannelData
    let staticChannelData = this.channelCacheService.getStaticChannelData().channelDetails
      // get dynamicChannelData
    let dynamicChannelData = this.channelCacheService.getDynamicChannelData().channelDynamaicProp
    let lastChannel = _.filter(dynamicChannelData, function (channelDetails) {
      return channelDetails['isLocked'] === '1'
    })
      //  get request data
    let resp = _.map(lastChannel, (item) => {
        // get staticData
      let channel = _.find(staticChannelData, items => {
        return item['ID'] === items['ID']
      })
        // return lock data
      return {
        id: item['ID'],
        ChannelName: channel['name'],
        posterUrl: channel['picture'] && channel['picture']['icons'] || 'assets/img/default/myTV_channel_poster.png',
        totalAll: lastChannel.length,
        floatInfo: {
            currentPlaybill: {}
          },
        sceneID: 'queryplaybilllist10'
      }
    })
    self.showLoadIcon = false
    if (resp.length === 0) {
      self.noDataList = '1'
    } else {
      self.noDataList = '0'
      self.lockedLists = resp
      self.totalAll = Number(resp[0].totalAll)
      self.nowDataLength = resp.length
      if (self.totalAll === 0) {
        self.noDataList = '1'
      } else {
        self.totalStr = '(' + self.totalAll + ')'
      }
      _.each(resp, (list, index) => {
        self.refreshStorage(list['id'])
      })
    }
  }

    /**
     * refresh static and dynamic data of localStorage.
     * @param {string} ID [channel ID]
     */
  refreshStorage (ID) {
    let self = this
    let sessionStaticData = self.channelCacheService.getStaticChannelData()
    let sessionDynamicData = self.channelCacheService.getDynamicChannelData()
    _.each(sessionStaticData['channelDetails'], (list, index) => {
      if (list['ID'] === ID) {
        list['isLocked'] = '1'
      }
    })
    _.each(sessionDynamicData['channelDynamaicProp'], (list, index) => {
      if (list['ID'] === ID) {
        list['isLocked'] = '1'
      }
    })
    let userType
    if (Cookies.getJSON('IS_PROFILE_LOGIN')) {
      userType = Cookies.getJSON('USER_ID')
    } else {
      userType = session.get('Guest')
    }
    session.put(userType + '_Static_Version_Data', sessionStaticData, true)
    session.put(userType + '_Dynamic_Version_Data', sessionDynamicData, true)
  }

    /**
     * operation after you click the edit button.
     */
  editData () {
    this.isCheckpass = true
    EventService.emit('IS_CHANNELEDIT', false) // if false,you can not to click the picture to play or show the suspension.
  }

    /**
     * operation after you click the 'clear all' button.
     */
  clearAllData () {
    this.isClearAll = true
  }

    /**
     * operation after you click the 'complete' button.
     */
  completeData () {
    this.show = true
    EventService.emit('COMPLETE', '1')
    EventService.emit('IS_CHANNELEDIT', true)
  }

    /**
     * operation after you click the close button of checking password dialog.
     * @param {Object} $event [click event]
     */
  closeDiv ($event) {
    this.isCheckpass = false // if false,close the checking password dialog.
  }

    /**
     * operation after you click the confirm button of checking password dialog.
     * @param {Object} $event [click event]
     */
  confirmDiv ($event) {
    this.isCheckpass = false
    this.show = false
    EventService.emit('DELETE', '1')
  }

    /**
     * operation after you click the delete button of the picture.
     * @param {Object} $event [click event]
     */
  deleteChannel ($event) {
    let self = this
    if (self.finishDelete) {
      self.finishDelete = false
      self.channelIDs.push($event['data']['id'])
      if (self.channelIDs.length !== 0) {
        let req = {
            'profileID': self.profileID,
            'lockTypes': ['CHANNEL'],
            'itemIDs': self.channelIDs
          }
        self.channelIDs = []
        self.showLocked.deleteLock(req).then((resp) => {
            if (resp['result']['retCode'] === '000000000') {
              let num = 1
              if (self.channelLockedIDs === $event['data'].id) {
                num = 0
              }
              self.channelLockedIDs = $event['data'].id
              self.myChannels.dataList.splice($event['index'], num)
              self.myChannels.closeDialog()
              self.nowDataLength = self.nowDataLength - num
              self.totalAll = self.totalAll - num > 0 ? self.totalAll - num : 0
              if (self.nowDataLength === 0) {
                self.noDataList = '1'
                self.totalStr = ''
                self.completeData()
              } else {
                self.noDataList = '0'
                self.totalStr = '(' + self.totalAll + ')'
              }
              // refresh storage
              self.deleteOneLockInStorage($event['data']['id'])
            }
            self.finishDelete = true
          }, respFail => {
            self.finishDelete = true
          })
      }
    }
  }

    /**
     * refresh dynamic data of localStorage when delete one lock channel.
     * @param {string} ID [channel ID]
     */
  deleteOneLockInStorage (ID) {
    let self = this
    let sessionDynamicData = self.channelCacheService.getDynamicChannelData()
    _.each(sessionDynamicData['channelDynamaicProp'], (list, index) => {
      if (list['ID'] === ID) {
        list['isLocked'] = '0'
      }
    })
    let userType
    if (Cookies.getJSON('IS_PROFILE_LOGIN')) {
      userType = Cookies.getJSON('USER_ID')
    } else {
      userType = session.get('Guest')
    }
    session.put(userType + '_Dynamic_Version_Data', sessionDynamicData, true)
  }

    /**
     * click the picture to play the playbill.
     * @param {Object} $event [click event]
     */
  detailChannel ($event) {
    if (document.getElementsByClassName('live_load_bg') && document.querySelector('.channel_login_load_content')) {
      document.getElementsByClassName('live_load_bg')[0]['style']['display'] = 'block'
      document.querySelector('.channel_login_load_content')['style']['display'] = 'block'
    }
    let self = this
    let channelInfo
    self.showLocked.getQueryPlaybill({
      needChannel: '0',
      queryPlaybill: {
        type: '1',
        count: '1',
        offset: '0'
      },
      queryChannel: {
        channelIDs: [$event.id],
        isReturnAllMedia: '1'
      }
    }).then(resp => {
      channelInfo = resp.channelPlaybills[0]
      let dynamicChannelData = this.channelCacheService.getDynamicChannelData().channelDynamaicProp
      let channelDtails = this.channelCacheService.getStaticChannelData().channelDetails
      channelInfo['channelInfos'] = _.find(channelDtails, (detail) => {
          return detail['ID'] === channelInfo.playbillLites[0]['channelID']
        })
      _.find(dynamicChannelData, (detail) => {
          if (detail && channelInfo['channelInfos'] && channelInfo['channelInfos']['ID'] === detail['ID']) {
            channelInfo['channelInfos']['channelNO'] = detail['channelNO']
            let nameSpace = session.get('ott_channel_name_space')
            let physicalChannelsDynamicProperty = _.find(detail['physicalChannelsDynamicProperties'], (property) => {
              return _.contains(property['channelNamespaces'], nameSpace)
            })
            channelInfo['channelInfos']['physicalChannelsDynamicProperty'] =
                        physicalChannelsDynamicProperty || detail['physicalChannelsDynamicProperties'][0]
          }
        })
      self.playbillAppService.playProgram(channelInfo['playbillLites'][0], channelInfo['channelInfos'])
    })
    EventService.emit('OPEN_FULLSCREEN_LIVETVVIDEO', {})
    let video = document.querySelector('#videoContainer video')
    let ua = navigator.userAgent.toLowerCase()
    let isIe = !!ua.match(/msie/i) || !!ua.match(/rv:([\d.]+).*like gecko/)
    let isFirefox = !!ua.match(/firefox\/([\d.]+)/)
    let isEdge = !!ua.match(/edge\/([\d.]+)/)
    if (isIe) {
      video = document.querySelector('#videoContainer')
      video['msRequestFullscreen']()
    } else if (isEdge) {
      video = document.querySelector('#videoContainer')
      video.webkitRequestFullScreen()
    } else if (isFirefox) {
        video = document.querySelector('#videoContainer')
        video['mozRequestFullScreen']()
      } else if (ua.match(/version\/([\d.]+).*safari/)) {
        video.requestFullscreen()
      } else {
        video.webkitRequestFullScreen()
      }
  }

    /**
     * operation after you click the cancel button of the 'cannel all' dialog.
     * @param {Object} $event [click event]
     */
  cancelData ($event) {
    this.isClearAll = false // if false,close the 'cannel all' dialog.
  }

    /**
     * operation after you click the confirm button of the 'cannel all' dialog.
     * @param {Object} $event [click event]
     */
  confirmData ($event) {
    let self = this
    self.noDataList = '1'
    self.lockedLists = []
    let req = {
      'lockTypes': ['CHANNEL']
    }
    self.showLocked.deleteLock(req)
        .then((resp) => {
          self.isClearAll = false
          self.show = true
          self.totalStr = ''
          // refresh storage
          self.deleteAllLockInStorage()
        })
        .catch(() => {
          self.failed = '1'
          self.failTimer = setTimeout(() => self.failed = '0', 2000)
          self.showData()
        })
  }

    /**
     * delete dynamic data of localStorage after you delete all locked channels.
     */
  deleteAllLockInStorage () {
    let self = this
    let sessionDynamicData = self.channelCacheService.getDynamicChannelData()
    _.each(sessionDynamicData['channelDynamaicProp'], (list, index) => {
      list['isLocked'] = '0'
    })
    let userType
    if (Cookies.getJSON('IS_PROFILE_LOGIN')) {
      userType = Cookies.getJSON('USER_ID')
    } else {
      userType = session.get('Guest')
    }
    session.put(userType + '_Dynamic_Version_Data', sessionDynamicData, true)
  }

    /**
     * if there are no locked channels,change the style of the edit button.
     */
  noDataStyle () {
    let styles
    if (!this.isAdmin || this.noDataList === '1') {
      styles = {
        'color': '#5f5f5f',
        'cursor': 'default'
      }
    } else {
      styles = {}
    }
    return styles
  }

    /**
     * get dom element by element id.
     */
  dom (divName: string) {
    return document.getElementById(divName)
  }
}
