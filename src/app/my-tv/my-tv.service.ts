import * as _ from 'underscore'
import { Injectable } from '@angular/core'
import { queryProfile } from 'ng-epg-sdk/vsp'
import { session } from 'src/app/shared/services/session'
import { TranslateService } from '@ngx-translate/core'
import { TimerService } from '../shared/services/timer.service'
import { CommonService } from '../core/common.service'

@Injectable()
export class MyTvService {
  public profileData: any
  public currencyRate: any
  constructor (
        private translate: TranslateService,
        private timerService: TimerService,
        private commonService: CommonService
    ) { }

    /**
     * get the user message.
     */
  queryProfile (req: any): Promise<any> {
    let self = this
    if (req.type === '0' && JSON.parse(localStorage.getItem('curProfile'))) {
      return Promise.resolve(JSON.parse(localStorage.getItem('curProfile')))
    }
    return this.timerService.getCaheCustomConfig().then(resps => {
      if (resps && resps['result']) {
        self.currencyRate = resps['currencyRate']
        if (_.isUndefined(resps['currencyRate']) || resps['currencyRate'] === '' || resps['currencyRate'] === null ||
                    Number(resps['currencyRate']) < 0) {
            self.commonService.MSAErrorLog(38007, 'QueryCustomizeConfig')
          }
        return queryProfile(req).then(resp => {
            if (req.type === '0') {
              let profile = resp.profiles[0]
              let curProfile = {
                id: 'profile',
                profileId: profile['ID'],
                isAdmin: profile['profileType'] === '0',
                loginName: profile['loginName'],
                name: profile['name'],
                avatar: profile['logoURL'] ? 'assets/img/avatars/' + profile['logoURL'] + '.png'
                  : 'assets/img/default/ondemand_casts.png',
                level: profile['ratingName'],
                levelId: profile['ratingID'],
                quota: profile.quota === '-1' ? 'unlimited'
                  : session.get('CURRENCY_SYMBOL') + String(Number(profile['quota']) / Number(resps['currencyRate'])),
                profileType: profile['profileType']
              }
              let ratingList = session.get('TERMINAL_RATING_LIST')
              curProfile['level'] = _.find(ratingList, item => {
                return item['ID'] === curProfile['levelId']
              })['name']
              self.profileData = curProfile
              localStorage.setItem('curProfile', JSON.stringify(curProfile))
              return Promise.resolve(curProfile)
            }
          })
      } else {
        return Promise.reject('fail')
      }
    })
  }

    /**
     * call interface to get all profiles
     */
  queryAllProfile (req) {
    return queryProfile(req).then(resp => {
      return resp
    })
  }
}
