import { TimerService } from './shared/services/timer.service'
import { ErrorMessageService } from './shared/services/error-message.service'
import { GuestPlayAppService } from './component/live-tv-guest/guestPlayApp.service'
import { TrackService } from './shared/services/track.service'
import { TranslateService } from '@ngx-translate/core'
import { LiveTvService } from './shared/services/livetv.service'
import { PlaybillAppService } from './component/playPopUpDialog/playbillApp.service'
import { TVODAppService } from './component/playPopUpDialog/tVodApp.service'
import { HookService, HeartbeatService, SessionService, ChannelService } from 'ng-epg-sdk/services'
import { ProfileManageService } from './my-tv/profile-manage/profile-manage.service'
import { PlayVodService } from './on-demand/voddetail/vod-player/vod-player.service'
import { CheckResponseHook, AbortAjaxHook, LoginOccationHook } from 'ng-epg-sdk/hooks'
import { HookConfig } from './hook-config'
import { MediaPlayService } from './component/mediaPlay/mediaPlay.service'
import { MyTvService } from './my-tv/my-tv.service'
import { SubscriptionsService } from './my-tv/subscriptions/subscriptions.service'
import { DialogService, DialogConfig, DialogRef } from 'ng-epg-ui/webtv-components/dialog'
import { VODPlayerDrop } from './on-demand/voddetail/vod-player/vod-player-drop.service'
import { ReminderTask } from './component/reminder/reminder-task.service'
import { ReminderService } from './component/reminder/reminder.service'
import { HomeService } from './home/home.service'
import { CommonSuspensionService } from 'ng-epg-ui/webtv-components/channelSuspension/channelCommon.service'

/**
 * the provides map
 */
export const APP_PROVIDERS = [
  CommonSuspensionService,
  SubscriptionsService,
  HomeService,
  ReminderService,
  TimerService,
  ErrorMessageService,
  GuestPlayAppService,
  TrackService,
  TranslateService,
  LiveTvService,
  PlaybillAppService,
  TVODAppService,
  HeartbeatService,
  SessionService,
  ChannelService,
  ProfileManageService,
  PlayVodService,
  HookService,
  CheckResponseHook,
  AbortAjaxHook,
  LoginOccationHook,
  HookConfig,
  MediaPlayService,
  MyTvService,
  DialogService,
  DialogConfig,
  DialogRef,
  VODPlayerDrop,
  ReminderTask
]
