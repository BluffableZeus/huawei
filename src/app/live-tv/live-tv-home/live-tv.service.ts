import * as _ from 'underscore'
import { session } from 'src/app/shared/services/session'
import { Injectable } from '@angular/core'
import { queryPlaybillList, queryOTTLiveTVHomeData } from 'ng-epg-sdk/vsp'
import { ChannelCacheService } from '../../shared/services/channel-cache.service'

@Injectable()
export class LiveTvService {
  constructor (private channelCacheService: ChannelCacheService) { }
  /**
    * call a interface
    * deal with the data come from the interface
    * get the data from queryOTTLiveTVHomeData
    */
  getPCLiveTvHome (req: any) {
    return queryOTTLiveTVHomeData(req).then(resp => {
      return this.channelCacheService.asynGetDynamicData().then(dynamicData => {
        let dynamicChannelData = dynamicData['channelDynamaicProp']
        _.each(resp.liveTVHomeSubjectList, (item) => {
          _.each(item.channelPlaybillList, (program) => {
            _.find(dynamicChannelData, (detail) => {
              if (program['channelDetail']['ID'] === detail['ID']) {
                program['channelDetail']['isLocked'] = detail['isLocked']
                program['channelDetail']['favorite'] = detail['favorite']
                program['channelDetail']['channelNO'] = detail['channelNO']
                let nameSpace = session.get('ott_channel_name_space')
                let physicalChannelsDynamicProperty = _.find(detail['physicalChannelsDynamicProperties'], (property) => {
                  return _.contains(property['channelNamespaces'], nameSpace)
                })
                program['channelDetail']['physicalChannelsDynamicProperty'] =
                                physicalChannelsDynamicProperty || detail['physicalChannelsDynamicProperties'][0]
              }
            })
          })
        })
        return resp
      })
    })
  }
  /**
    * call a interface
    * deal with the data come from the interface
    * get the data from queryPlaybillList
    */
  getQueryPlaybill (req: any, sceneID?: string) {
    let options = {
      params: {
        SID: sceneID,
        DEVICE: 'PC',
        DID: session.get('uuid_cookie')
      }
    }
    return queryPlaybillList(req, options).then(resp => {
      return resp
    })
  }
}
