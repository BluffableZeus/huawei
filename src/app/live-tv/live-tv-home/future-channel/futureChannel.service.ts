import * as _ from 'underscore'
import { Injectable } from '@angular/core'
import { PictureService } from 'ng-epg-ui/services/picture.service'
import { CommonService } from '../../../core/common.service'

@Injectable()
export class FutureChannelService {
  /**
     * name for modules that would be used in this module
     */
  constructor (
        private pictureService: PictureService,
        private commonService: CommonService
  ) { }
  /**
     * function to add 'zero' in front of the target number
     * @param str target number
     * @param len the minimum length of str, if not enough, add 'zero'
     */
  prefixZero (str, len) {
    // change the data type from Number to String
    str = '' + (str || '')
    if (str.length >= len) {
      return str
    }
    // number of 'zero' that need to add
    let gapLen = len - str.length, i, arr = []
    for (i = 0; i < gapLen; i++) {
      arr[i] = '0'
    }
    return arr.join('') + str
  }
  /**
     * get url and resize the poster
     * @param index index of the target
     * @param list playbill object
     */
  futureUrl (index, list) {
    let url
    let rowNum = 6
    if (index % rowNum === 0 || index % rowNum === 1) {
      url = this.pictureService.convertToSizeUrl(list['posterUrl'], { minwidth: 480, minheight: 269, maxwidth: 680, maxheight: 382 })
    } else if (index % rowNum === 2 || index % rowNum === 3) {
      url = this.pictureService.convertToSizeUrl(list['posterUrl'], { minwidth: 383, minheight: 212, maxwidth: 540, maxheight: 311 })
    } else {
      url = this.pictureService.convertToSizeUrl(list['posterUrl'], { minwidth: 230, minheight: 174, maxwidth: 96, maxheight: 146 })
    }
    return url
  }
  /**
     * deal data
     * @param data array of channels
     */
  queryFutureChannel (data) {
    let channelList = _.map(data, (list, index) => {
      let channelDetail = list['channelDetail']
      let playbill = list['playbillLites'] && list['playbillLites'][0] || {}
      let isFillProgram = false
      list['channelID'] = channelDetail && channelDetail['ID']
      list['channelName'] = channelDetail && channelDetail['name']
      list['playbillID'] = playbill && playbill['ID']
      list['playbillName'] = this.getPlaybillName(playbill)
      this.setPlaybillName(list, playbill)
      isFillProgram = this.isFillProgramOrNot(playbill, isFillProgram)
      list['playbillTime'] = this.commonService.dealWithDateJson('D10', playbill['startTime'])
      list['playbillEndTime'] = playbill && playbill['endTime']
      list['posterUrl'] = this.getPosterUrl(playbill)
      let url = this.futureUrl(index, list)
      return {
        channelInfo: channelDetail,
        playbillInfo: list['playbillLites'] && list['playbillLites'][0] || {},
        channelID: list['channelID'],
        channelName: '| ' + list['channelName'],
        playbillID: list['playbillID'],
        playbillName: list['playbillName'],
        playbillTime: list['playbillTime'],
        playbillEndTime: list['playbillEndTime'],
        posterUrl: url,
        floatInfo: playbill,
        floatflag: 'happyNext',
        leftTime: Math.ceil((new Date(Number(playbill['endTime'])).getTime() -
                    new Date(Number(Date.now()['getTime']())).getTime()) / 60000),
        isFillProgram: isFillProgram
      }
    })
    return channelList.slice(0, 24)
  }
  /**
     * use the name you get or the default
     * @param playbill target playbill
     */
  getPlaybillName (playbill) {
    let name = playbill && playbill['name'] ? playbill['name'] : ''
    return name
  }
  /**
     * the address of playbill poster
     * @param playbill target playbill
     */
  getPosterUrl (playbill) {
    let url = playbill && playbill['picture'] && playbill['picture'].posters && playbill['picture'].posters[0]
    return url
  }
  /**
     * another condition that change the value of playbillName
     * @param list channel which includes the playbill
     * @param playbill target playbill
     */
  setPlaybillName (list, playbill) {
    if (list['playbillName'] && playbill && playbill['isFillProgram'] && (playbill && playbill['isFillProgram'] === '1') &&
            (list['playbillName'] === '')) {
      list['playbillName'] = 'not_program'
    }
  }
  /**
     * the value of isFillProgram
     */
  isFillProgramOrNot (playbill, isFillProgram) {
    if (playbill && playbill['isFillProgram'] && (playbill && playbill['isFillProgram'] === '1')) {
      isFillProgram = true
    }
    return isFillProgram
  }
}
