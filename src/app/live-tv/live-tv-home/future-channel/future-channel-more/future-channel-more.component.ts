import { Component, OnInit } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { FutureChannelMoreService } from './future-channel-more.service'

@Component({
  selector: 'app-future-channel-more',
  templateUrl: './future-channel-more.component.html',
  styleUrls: ['./future-channel-more.component.scss']
})

export class FutureChannelMoreComponent implements OnInit {
  moreFutureChannelList: {
    'dataList': Array<any>,
    'row': string,
    'suspensionID': string
  }

  constructor (
        private route: ActivatedRoute,
        private router: Router,
        private futureChannelService: FutureChannelMoreService) {
  }
  ngOnInit () {
    this.showFutureChannelMore()
  }
    /**
     * call QueryRecmContent interface to get RecmContent playbill
     */
  showFutureChannelMore () {
    this.futureChannelService.getQueryRecmContentDatas().then((resp) => {
      this.moreFutureChannelList = {
        'dataList': resp,
        'row': 'All',
        'suspensionID': 'moreFutureChannelData'
      }
    })
  }
    /**
     * turn to detail page
     */
  onClickDetail (data) {
    if (data[2] === '0') {
      this.router.navigate(['live-tv', 'live-tv-detail', data[0], data[1], data[3], data[4]])
    }
  }
}
