import * as _ from 'underscore'
import { Injectable } from '@angular/core'
import { queryRecmContent, getPlaybillDetail } from 'ng-epg-sdk/vsp'
import { PictureService } from 'ng-epg-ui/services/picture.service'
import { CommonService } from '../../../../core/common.service'

@Injectable()
export class FutureChannelMoreService {
  constructor (
        private pictureService: PictureService,
        private commonService: CommonService
  ) { }
  /**
     * get playbill by playbillID
     * @param id
     */
  getQueryPlaybill (id) {
    return getPlaybillDetail({
      playbillID: id,
      isReturnAllMedia: '1'
    }).then(resp => {
      return resp
    })
  }
  /**
     * get recommended content datas
     */
  getQueryRecmContentDatas () {
    return queryRecmContent({
      queryDynamicRecmContent: {
        'recmScenarios': [{
          'contentType': 'PROGRAM',
          'businessType': 'BTV',
          'entrance': 'Program_Happy_Next',
          'count': '30',
          'offset': '0'
        }]
      }
    }).then(resp => {
      let recmContents = resp['recmContents'] && resp['recmContents'][0] && resp['recmContents'][0]['recmPrograms']
      let lists = _.map(recmContents, (list, index) => {
        let channelDetail
        let playbill = {}
        if (!_.isUndefined(list['channelDetail'])) {
          channelDetail = list['channelDetail']
        }
        if (!_.isUndefined(list['playbillLites'])) {
          playbill = list['playbillLites'][0]
        }
        let url = playbill && playbill['picture'] && playbill['picture'].posters &&
                    this.pictureService.convertToSizeUrl(playbill['picture'].posters[0],
                      { minwidth: 230, minheight: 129, maxwidth: 260, maxheight: 146 })
        return {
          channelID: channelDetail['ID'],
          channnelName: channelDetail['name'],
          playbillID: playbill['ID'],
          ID: playbill['ID'],
          name: playbill['name'],
          posterUrl: url,
          floatInfo: playbill,
          morePlaybillflag: 'morePlaybillList',
          floatflag: 'moreList',
          startTime: playbill['startTime'],
          endTime: playbill['endTime'],
          recmActionID: resp['recmActionID'],
          entrance: 'Program_Happy_Next',
          startTimes: this.commonService.dealWithDateJson('D10', playbill['startTime']),
          channelName: ' | ' + channelDetail['name'],
          isLen: '0',
          leftTime: Math.ceil((new Date(Number(playbill['endTime'])).getTime() -
                        new Date(Number(Date.now()['getTime']())).getTime()) / 60000)
        }
      })
      return lists
    })
  }
}
