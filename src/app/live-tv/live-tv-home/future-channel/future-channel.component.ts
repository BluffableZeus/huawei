import * as _ from 'underscore'
import { Component, OnInit, Input, EventEmitter, Output, ElementRef, ViewChild, OnDestroy } from '@angular/core'
import { SuspensionComponentProgram } from 'ng-epg-ui/webtv-components/programSuspension/programSuspension.component'
import { EventService } from 'ng-epg-sdk/services'
import { Router } from '@angular/router'
import { FutureChannelService } from './futureChannel.service'
import { CommonService } from '../../../core/common.service'

@Component({
  selector: 'app-future-channel',
  templateUrl: './future-channel.component.html',
  styleUrls: ['./future-channel.component.scss']
})

export class FutureChannelComponent implements OnInit, OnDestroy {
  @Input() set futureChannel (data) {
    this.futureChannels = data
    this.channelLists = this.futureChannelService.queryFutureChannel(data['channelPlaybillList'])
    if (this.channelLists.length > 0) {
      // if return  future playbill data,judge the show state of arrowhead
      let rownum = 6
      this.page = Math.floor(this.channelLists.length / rownum)
      if (this.channelLists.length % rownum === 0) {
        this.page -= 1
      }
      if (this.channelLists.length <= rownum) {
        this.channelLeftVisible = 'hidden'
        this.channelRightVisible = 'hidden'
      }
      this.hasFutureChannel = true
    } else {
      // if there is no data returned，show no data
      this.hasFutureChannel = false
    }
  }
  @ViewChild(SuspensionComponentProgram) suspensionprogram: SuspensionComponentProgram
  @Output() data: EventEmitter<Object> = new EventEmitter()
  @ViewChild('futureChannelDom') futureChannelDom: ElementRef
  public hasFutureChannel: boolean
  private channelLists: Array<any>
  private channelLeftVisible = 'hidden'
  private channelRightVisible = 'visible'
  private panel: number
  private offsetNumber = 0
  private count = 1
  private page: number
  public isShow = 'false'
  public suspensionInfo
  private susTimeChannel
  private susFilter = _.debounce((fc: Function) => { fc() }, 400)
  private susUse = true
  private futureChannels

  constructor (
        private route: Router,
        private futureChannelService: FutureChannelService,
        private commonService: CommonService
    ) {

  }

  ngOnInit () {
    this.channelListResult()
      // monitor the screen size changed
    let self = this
    let clientW: number = document.body.clientWidth
    if (clientW > 1440) {
      self.panel = 1280
    } else {
      self.panel = 980
    }
    EventService.on('ScreenSize', function () {
      clientW = document.body.clientWidth
        // computate the length of slippage according to the window size
      if (clientW > 1440) {
        self.panel = 1280
      } else {
        self.panel = 980
      }
      self.offsetNumber = self.panel * -(self.count - 1)
      if (self.futureChannelDom) {
        self.futureChannelDom.nativeElement.style.transform = 'translate(' + self.offsetNumber + 'px,0px)'
        self.futureChannelDom.nativeElement.style.transition = ''
      }
    })
  }

  imgError (i) {
      // get default img according to error event
    let img = event.srcElement
      // prevent img flash
    img['onerror'] = null
    let itemNum = 6
      // set default img according to ScreenSize
    if (i % itemNum === 0 || i % itemNum === 1) {
      img['src'] = 'assets/img/default/livetv_home_landscape_program2.png'
    } else if (i % itemNum === 2 || i % itemNum === 3) {
      img['src'] = 'assets/img/default/livetv_home_landscape_program3.png'
    } else {
      img['src'] = 'assets/img/default/livetv_home_small.png'
    }
  }

  ngOnDestroy () {
    clearTimeout(this.susTimeChannel)
  }
    // deal the future playbill data
  channelListResult () {
    this.channelLists = this.futureChannelService.queryFutureChannel(this.futureChannels['channelPlaybillList'])
    if (this.channelLists.length > 0) {
      let itemMum = 6
        // if return  future playbill data,judge the show state of arrowhead
      this.page = Math.floor(this.channelLists.length / itemMum)
      if (this.channelLists.length % itemMum === 0) {
        this.page -= 1
      }
      if (this.channelLists.length <= itemMum) {
        this.channelLeftVisible = 'hidden'
        this.channelRightVisible = 'hidden'
      }
      this.hasFutureChannel = true
    } else {
        // if there is no data returned，show no data
      this.hasFutureChannel = false
    }
  }
    // click left arrowhead
  onClickLeft () {
    this.count--
    let offsetWidth: string
    let clientW: number = document.body.clientWidth
      //  computate the length of slippage according to the window size
    if (clientW > 1440) {
      this.panel = 1280
    } else {
      this.panel = 980
    }
    const MINSFFSETNUMBER = 0
    if (this.offsetNumber <= MINSFFSETNUMBER) {
      this.offsetNumber += this.panel
      offsetWidth = this.offsetNumber + 'px'
      this.futureChannelDom.nativeElement.style.transform = 'translate(' + offsetWidth + ',0px)'
      this.futureChannelDom.nativeElement.style.transition = 'all 1s linear'
      this.isShowIcon(this.offsetNumber)
    }
  }
    // click right arrowhead
  onClickRight () {
    this.count++
    let offsetWidth: string
    let clientW: number = document.body.clientWidth
      // computate the length of slippage according to the window size
    if (clientW > 1440) {
      this.panel = 1280
    } else {
      this.panel = 980
    }
    let maxWidth: number = -(this.panel * (this.page - 1))
    if (this.offsetNumber >= maxWidth) {
      this.offsetNumber -= this.panel
      offsetWidth = this.offsetNumber + 'px'
      this.futureChannelDom.nativeElement.style.transform = 'translate(' + offsetWidth + ',0px)'
      this.futureChannelDom.nativeElement.style.transition = 'all 1s linear'
      this.isShowIcon(this.offsetNumber)
    }
  }
    // judge the show state of arrowhead
  isShowIcon (offsetNumber: number) {
    if (offsetNumber === 0) {
        // if first,left arrowhead disappeared
      this.channelLeftVisible = 'hidden'
      this.channelRightVisible = 'visible'
    } else if (offsetNumber === -(this.panel * this.page)) {
        // if last,right arrowhead disappeared
      this.channelLeftVisible = 'visible'
      this.channelRightVisible = 'hidden'
    } else {
      this.channelLeftVisible = 'visible'
      this.channelRightVisible = 'visible'
    }
  }
    // turn to more future playbill page
  moreShow () {
      // only have the future playbill can transfer
    if (this.hasFutureChannel) {
      this.route.navigate(['live-tv/moreProgram'])
      document.body.scrollTop = 0
      document.documentElement.scrollTop = 0
    }
  }
    // turn to detail page
  channelDtail (data) {
      // FillProgram can't transfer
    if (data['isFillProgram']) {
      return
    }
    this.route.navigate(['live-tv/live-tv-detail', data.playbillID, data.playbillEndTime, this.futureChannels['recmActionID'] || '',
      'Program_Happy_Next'])
    document.body.scrollTop = 0
    document.documentElement.scrollTop = 0
  }
  showDetail () {

  }
    // show suspension box
  mouseenter (option, event) {
    this.susUse = true
    this.susFilter(() => {
      let self = this
      if (self.susUse === false) {
        return
      }
        // FillProgram can't show suspension box
      if (option['isFillProgram']) {
        return
      }
      self.isShow = 'false'
        // when get data,the suspension box display
      let currentTarget = window.event && window.event.currentTarget || event && event.target
      let time1 = new Date().getTime()
      this.suspensionInfo = 'liveProgramTop'
      EventService.removeAllListeners(['LOADDATAPROGRAMSUCCESS'])
      EventService.on('LOADDATAPROGRAMSUCCESS', function () {
        self.commonService.moveEnter(currentTarget, self.suspensionInfo, document.body.clientWidth, 'future')
        let time2 = new Date().getTime()
        if ((time2 - time1) < 500) {
            let time = 500 - (time2 - time1) + 100
            clearTimeout(self.susTimeChannel)
            self.susTimeChannel = setTimeout(function () {
              self.isShow = 'true'
            }, time)
          } else {
            clearTimeout(self.susTimeChannel)
            self.susTimeChannel = setTimeout(function () {
              self.isShow = 'true'
            }, 100)
          }
      })
      this.suspensionprogram.setDetail(option, self.suspensionInfo, currentTarget.firstElementChild)
    })
  }
    // suspension box disappear
  closeDialog (option) {
    let self = this
    self.susUse = false
    if (option['isFillProgram']) {
      return
    }
    clearTimeout(self.susTimeChannel)
    EventService.removeAllListeners(['LOADDATAPROGRAMSUCCESS'])
    EventService.removeAllListeners(['showProgramSuspension'])
    EventService.on('showProgramSuspension', function (options) {
      if (options === 'happyNext') {
        self.isShow = 'true'
      }
    })
    EventService.removeAllListeners(['closeProgramSuspension'])
    EventService.on('closeProgramSuspension', function () {
      self.isShow = 'false'
    })
    self.isShow = 'false'
  }

  pName (i) {
    let itemMum = 6
    if ((i + 1) % itemMum === 1 || (i + 1) % itemMum === 2) {
      return 'channelList-name body-t22'
    } else if ((i + 1) % itemMum === 3 || (i + 1) % itemMum === 4) {
      return 'channelList-name body-t23'
    } else {
      return 'channelList-name body-t25'
    }
  }

  pTime (i) {
    let itemMum = 6
    if ((i + 1) % itemMum === 5 || (i + 1) % itemMum === 0) {
      return 'channelList-time body-t29'
    } else {
      return 'channelList-time body-t26'
    }
  }

  pChannel (i) {
    let itemMum = 6
    if ((i + 1) % itemMum === 5 || (i + 1) % itemMum === 0) {
      return 'channelList-channel body-t29'
    } else {
      return 'channelList-channel body-t26'
    }
  }
}
