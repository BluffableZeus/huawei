import * as _ from 'underscore'
import { Injectable } from '@angular/core'
import { queryPlaybillList } from 'ng-epg-sdk/vsp'
import { PictureService } from 'ng-epg-ui/services/picture.service'
import { TranslateService } from '@ngx-translate/core'
import { session } from 'src/app/shared/services/session'
import { ChannelCacheService } from '../../../shared/services/channel-cache.service'
import { CommonService } from '../../../core/common.service'

@Injectable()
export class WhatOnService {
  private programsList: Array<any> = []
  private posterList: Array<any> = []
    /**
     * name for modules that would be used in this module
     */
  constructor (
        private pictureService: PictureService,
        private translate: TranslateService,
        private channelCacheService: ChannelCacheService,
        private commonService: CommonService
    ) { }
    /**
     * deal with the play times
     * @param playbill the exact program you get
     */
  getProgramsTime (playbill) {
    let playTime
    if (playbill && playbill['playTimes']) {
      let num = Number(playbill['playTimes'])
      if (num >= 1000 && num <= 1000000) {
        playTime = (num || 0).toString().replace(/(\d)(?=(?:\d{3})+$)/g, '$1,')
      }
      if (num > 1000000) {
        playTime = 1
      }
      if (num < 1000) {
        playTime = Number(playbill['playTimes'])
      }
    }
    return playTime
  }
    /**
     * deal the playbill data
     */
  getProgramsTwice (item, playbill) {
    let playbillName = item['playbillLites'] && item['playbillLites'][0] && playbill['name']
    let isFillProgram
      // if the playbill is fill program, its name showes no program
    if (playbill['isFillProgram'] && (playbill['isFillProgram'] === '1') && (playbillName === '') || !playbillName) {
      playbillName = 'not_program'
    }
    isFillProgram = this.allChannelFillProgram(item, playbill)
    let playTime = this.getProgramsTime(playbill)
    return { 'playbillName': playbillName, 'playTime': playTime, 'isFillProgram': isFillProgram }
  }
    /**
     * deal the data of banner
     */
  getPrograms (list) {
      // if the list is no data, return
    if (!list) {
      return
    }
    let programList: Array<Object> = list
      // if the count of whaton larger then 3, the count is 3
    if (programList.length > 3) {
      programList.length = 3
    }
    programList = _.map(programList, (item, index) => {
      let channelInfo = item['channelDetail'] || item['channelInfos']
      let playbill = item['playbillLites'] && item['playbillLites'][0] || {}
        // the length of the program
      let length = Number(playbill['endTime']) - Number(playbill['startTime'])
        // the surplus length of the program
      let curLength = Date.now()['getTime']() - Number(playbill['startTime'])
      let url = this.getUrl(index, playbill)
      let playbillNameAndTime = this.getProgramsTwice(item, playbill)
      return {
        channelId: channelInfo['ID'],
        channelName: channelInfo['name'],
        channelNO: channelInfo['channelNO'],
        channelDetail: channelInfo,
        playbillDetail: playbill,
        playbillId: playbill['ID'],
        playbillName: playbillNameAndTime['playbillName'],
        endTime: playbill['endTime'],
        startTime: playbill['startTime'],
        isMillion: !!(playbill && playbill['playTimes'] && (Number(playbill['playTimes']) > 1000000)),
        playTime: playbill && playbill['playTimes'] ? playbillNameAndTime['playTime'] : '0',
        picture: url,
        percent: curLength >= length ? '100%' : Number(curLength) / Number(length) * 100 + '%',
        isFillProgram: playbillNameAndTime['isFillProgram']
      }
    })
    return programList
  }

  getUrl (index, playbill) {
    //return playbill.picture && playbill.picture.posters[0]
    /** DISALBE FOR C50 */
    let url
      // when it is the first poster, put it as the larger poster
    let count = 3
    if (index % count === 0) {
        // use the packaged function in pictureService
        // resize the big poster
      url = playbill['picture'] && playbill['picture'].posters &&
                this.pictureService.convertToSizeUrl(playbill['picture'].posters[0],
                  { minwidth: 650, minheight: 364, maxwidth: 914, maxheight: 514 })
    } else {
        // others put them as the small poster
        // resize the small poster
      url = playbill['picture'] && playbill['picture'].posters &&
                this.pictureService.convertToSizeUrl(playbill['picture'].posters[0],
                  { minwidth: 315, minheight: 176, maxwidth: 446, maxheight: 250 })
    }
    return url
    /** DISALBE FOR C50 -- end */
  }
    /**
     *  refresh the progress bar of the poster
     */
  refreshPosterProgress (list) {
    if (!list) {
      return
    }
    this.posterList = list
    for (let i = 0; i < this.posterList.length; i++) {
      let length = Number(this.posterList[i]['endTime']) - Number(this.posterList[i]['startTime'])
      let curLength = Date.now()['getTime']() - Number(this.posterList[i]['startTime'])
        // if the program is end and not refresh the next one,the progress show full
      this.posterList[i]['percent'] = curLength >= length ? '100%' : Number(curLength) / Number(length) * 100 + '%'
    }
    return this.posterList
  }
    /**
     * judge the progam is fill progam or not
     */
  allChannelFillProgram (item, playbill) {
    let isFillProgram
    if (item['playbillLites'] && item['playbillLites'][0] && playbill['isFillProgram'] && (playbill['isFillProgram'] === '1')) {
      isFillProgram = true
    }
    return isFillProgram
  }
    /**
     * deal with the channel info for channellist
     */
  allChannelList (playbill, channelInfo, item) {
      // use the packaged function in commonService to calculate the left time of the program
    let leftTime = this.commonService.dealWithPlaybillTimeReduce(new Date(Number(Date.now()['getTime']())).getTime(),
        new Date(Number(playbill['endTime'])).getTime())
      // if the leftTime less then 0,leftTime is 0
    if (leftTime < 0) {
      leftTime = 0
    }
    let leftTimes = { min: leftTime }
    let ispltvCR
      // does the progam support time-shift or not
    if (channelInfo && channelInfo.physicalChannelsDynamicProperty &&
            channelInfo.physicalChannelsDynamicProperty.pltvCR) {
      let pltvCR = channelInfo.physicalChannelsDynamicProperty.pltvCR
      let ispltvCRlength = pltvCR.length > 0
      let isEnable = pltvCR.enable === '1'
      let isContentValid = pltvCR.isContentValid === '1'
      if (ispltvCRlength && isEnable && isContentValid && Cookies.getJSON('IS_PROFILE_LOGIN')) {
        ispltvCR = true
      }
    }
    let playbillName = this.getPlaybillName(item, playbill)
    let isFillProgram = this.allChannelFillProgram(item, playbill)
    return { 'leftTimes': leftTimes, 'playbillName': playbillName, 'isFillProgram': isFillProgram, 'ispltvCR': ispltvCR }
  }
    /**
     * function to get name of the target program
     */
  getPlaybillName (item, playbill) {
    let playbillName = item['playbillLites'] && item['playbillLites'][0] && playbill['name']
    if (playbill['isFillProgram'] && (playbill['isFillProgram'] === '1') && (playbillName === '')) {
      playbillName = 'not_program'
    }
    return playbillName
  }
    /**
     * deal all channel data
     */
  getAllChannel (list) {
      // if the list is no data,return the empty array
    if (!list) {
      return []
    }
    let channelList = _.map(list, (item, index) => {
      let channelInfo = item['channelDetail'] || item['channelInfos']
      let playbill = item['playbillLites'][0]
      let length = playbill['endTime'] - playbill['startTime']
      let curLength = Date.now()['getTime']() - Number(playbill['startTime'])
        // the adress of channel logo
      let url = this.getChannelLogo(channelInfo)
      let ChannelListData = this.allChannelList(playbill, channelInfo, item)
      return {
        index: index,
        channelId: channelInfo['ID'],
        channelName: ' | ' + channelInfo['name'],
        channelNO: channelInfo['channelNO'],
        channelRatingID: this.getChannelRatingID(channelInfo),
        channelDetail: channelInfo,
        playbillDetail: playbill,
        isLocked: (channelInfo['isLocked'] === '1'),
        isFav: channelInfo['favorite'] !== undefined,
        isRecording: !!((playbill.hasRecordingPVR && playbill.hasRecordingPVR !== '0' &&
                    Cookies.getJSON('IS_PROFILE_LOGIN'))),
        hasRecordingPVR: (playbill.hasRecordingPVR && playbill.hasRecordingPVR !== '0' &&
                    (playbill.hasRecordingPVR === '1')) ? '1' : '2',
        logo: url,
        endTime: playbill['endTime'],
        startTime: playbill['startTime'],
        playbillId: playbill['ID'],
        playbillName: ChannelListData['playbillName'],
        percent: curLength >= length ? '100%' : Number(curLength) / Number(length) * 100 + '%',
        leftTimes: ChannelListData['leftTimes'],
        ispltvCR: ChannelListData['ispltvCR'],
        isFillProgram: ChannelListData['isFillProgram']
      }
    })
    return channelList
  }
    /**
     * function to get the address of the channel logo
     * @param channelInfo the target channel
     */
  getChannelLogo (channelInfo) {
    return channelInfo.logo && channelInfo.logo.url &&
            this.pictureService.convertToSizeUrl(channelInfo.logo.url, { minwidth: 42, minheight: 42, maxwidth: 42, maxheight: 42 })
    /** DISALBE FOR C50
    let url = channelInfo['picture'] && channelInfo['picture'].icons &&
            this.pictureService.convertToSizeUrl(channelInfo['picture'].icons[0],
              { minwidth: 42, minheight: 42, maxwidth: 42, maxheight: 42 })
    return url
    */
  }
    /**
     * function to get rating id of the channel
     * @param channelInfo the target channel
     */
  getChannelRatingID (channelInfo) {
    let ratingID = channelInfo['rating'] && channelInfo['rating']['ID'] ? channelInfo['rating']['ID'] : '18'
    return ratingID
  }
    /**
     * refresh all channel's progress bar and playbill data
     * @param list the program list
     */
  refreshProgress (list) {
    this.programsList = list
    for (let i = 0; i < this.programsList.length; i++) {
      if (!this.programsList[i]['endTime']) {
        continue
      }
        // the whole time of the program
      let length = this.programsList[i]['endTime'] - this.programsList[i]['startTime']
        // time that has passed
      let curLength = Date.now()['getTime']() - this.programsList[i]['startTime']
        // if the program is end and not refresh the next one,the  progress show full
      this.programsList[i]['percent'] = curLength >= length ? '100%' : Number(curLength) / Number(length) * 100 + '%'
      let leftTime = Math.ceil((new Date(Number(this.programsList[i]['endTime'])).getTime() -
                new Date(Number(Date.now()['getTime']())).getTime()) / 60000)
        // if the leftTime less then 0,leftTime is 0
      if (leftTime < 0) {
        leftTime = 0
      }
      this.programsList[i]['leftTimes'] = { min: leftTime }
    }
    return this.programsList
  }
    /**
     *  get next playbill when the current playing program is end
     */
  getPlaybillContext (info) {
      // call a interface
    return queryPlaybillList({
      needChannel: '0',
      queryPlaybill: {
        type: '1',
        count: '1',
        offset: '0',
        startTime: String(info['time']),
        isFillProgram: '1'
      },
      queryChannel: {
        channelIDs: [info['channelId']],
        isReturnAllMedia: '1'
      }
    }).then(resp => {
        // deal with the data come from the interface
      let playbill = resp['channelPlaybills'][0]['playbillLites'][0]
      return playbill
    })
  }
    /**
     * roll the scroll and load data for all channellist
     */
  queryAllChannels (req) {
      // get id of all the channels in session storage
    let IDList = this.channelCacheService.getStorageChannelIDList()
    let count = req.count || '11'
    let offset = req.offset || '0'
    IDList = IDList.slice(Number(offset), Number(offset) + Number(count))
    let curTime = Date.now()['getTime']()
      // call a interface
    return queryPlaybillList({
      needChannel: '0',
      queryPlaybill: {
        type: '1',
        count: '1',
        offset: '0',
        startTime: String(curTime),
        isFillProgram: '1'
      },
      queryChannel: {
        channelIDs: IDList,
        subjectID: '-1',
        isReturnAllMedia: '1'
      }
    }).then(resp => {
        // get the channel detail info from the cache
      let channelDtails
      let dynamicChannelData
        // get the static version data asynchronously
      return this.channelCacheService.asynGetStaticData().then(staticData => {
          channelDtails = staticData['channelDetails']
          _.each(resp.channelPlaybills, (item) => {
            item['channelInfos'] = _.find(channelDtails, (detail) => {
              return detail['ID'] === item['playbillLites'][0].channelID
            })
          })
          // get the dynamic version data asynchronously
          return this.channelCacheService.asynGetDynamicData().then(dynamicData => {
            dynamicChannelData = dynamicData['channelDynamaicProp']
            _.each(resp.channelPlaybills, (item) => {
              _.find(dynamicChannelData, (detail) => {
                if (item['channelInfos']['ID'] === detail['ID']) {
                  item['channelInfos']['isLocked'] = detail['isLocked']
                  item['channelInfos']['favorite'] = detail['favorite']
                  item['channelInfos']['channelNO'] = detail['channelNO']
                  let nameSpace = session.get('ott_channel_name_space')
                  let physicalChannelsDynamicProperty = _.find(detail['physicalChannelsDynamicProperties'], (property) => {
                    return _.contains(property['channelNamespaces'], nameSpace)
                  })
                  item['channelInfos']['physicalChannelsDynamicProperty'] =
                                physicalChannelsDynamicProperty || detail['physicalChannelsDynamicProperties'][0]
                }
              })
            })
            return resp
          })
        })
    })
  }
}
