import * as _ from 'underscore'
import { Component, OnDestroy, OnInit, Input } from '@angular/core'
import { Router } from '@angular/router'
import { WhatOnService } from './what-on.service'
import { DirectionScroll } from 'ng-epg-ui/webtv-components/scroll/directionScroll.component'
import { session } from 'src/app/shared/services/session'
import { EventService } from 'ng-epg-sdk/services'
import { PlaybillAppService } from '../../../component/playPopUpDialog/playbillApp.service'
import { MediaPlayService } from '../../../component/mediaPlay/mediaPlay.service'

@Component({
  selector: 'app-what-on',
  styleUrls: ['./what-on.component.scss'],
  templateUrl: './what-on.component.html'
})

export class WhatOnComponent implements OnInit, OnDestroy {
  /**
     * when the data changed
     */
  @Input() set whatOnData (data) {
    this.whatOnDatas = data
    this.programList = this.whatOnService.getPrograms(data['channelPlaybillList'])
    // large poster in the background
    this.backPoster = this.programList && this.programList[0] && this.programList[0]['picture']
  }
    /**
     * declare variables
     */
  public programList: Array<Object>
  public allChannelList: Array<any>
  public channelData: Array<any>
  public backPoster: string
  public bannerCover = 'assets/img/vodbannercover.png'
  public channelTimer: any
  public nextAllPlaybillTimer: any
  public isToBottom = true
  public totalLength: number
  public showCurrenList
  public getWeakTip: any
  public channleListNodata = false
  public judgeLogInBg = false
  private whatOnDatas
    // private judgeWeak: boolean = false;

    /**
     * name for modules that would be used in this module
     */
  constructor (
        private router: Router,
        private whatOnService: WhatOnService,
        private directionScroll: DirectionScroll,
        private playbillAppService: PlaybillAppService,
        protected playService: MediaPlayService
    ) {
    EventService.on('LOGININTRUE', state => {
      this.judgeLogInBg = state
    })
  }
    /**
     * get channel list data
     */
  @Input() set allChannelData (allChannel) {
    this.channelData = allChannel[0]
    this.allChannelList = this.whatOnService.getAllChannel(allChannel[0])
    this.totalLength = Number(allChannel[1])
    this.channleListNodata = allChannel[0].length === 0
    if (this.allChannelList.length > 0) {
        // load scroll
      this.pluseOnScroll()
        // refresh next playbill
      this.getNextPlaybill()
    }
  }

    /**
     * when exit the component, clear all the timer and events
     */
  ngOnDestroy () {
    clearInterval(this.channelTimer)
    clearTimeout(this.nextAllPlaybillTimer)
    clearTimeout(this.getWeakTip)
    EventService.removeAllListeners(['SHOW_ALLCHANNEL_PROGRAM'])
  }

    /**
     * initialization
     */
  ngOnInit () {
    this.bannerCover = 'assets/img/vodbannercover.png'
    this.programList = this.whatOnService.getPrograms(this.whatOnDatas['channelPlaybillList'])
      // large poster in the background
    this.backPoster = this.programList && this.programList[0] && this.programList[0]['picture']
      // when the size of screen changed, the scroll turn to initial state
    EventService.on('ScreenSize', () => {
      this.pluseOnScroll()
      let oScroll = document.getElementById('scroll')
      let oUl = document.getElementById('channleUl')
      if (oScroll && oUl) {
        let oSpan = oScroll.getElementsByTagName('span')[0]
        oSpan.style.top = 0 + 'px'
        oUl.style.top = 0 + 'px'
      }
    })
      // roll the scroll and load data
    EventService.removeAllListeners(['SHOW_ALLCHANNEL_PROGRAM'])
    EventService.on('SHOW_ALLCHANNEL_PROGRAM', (event) => {
      this.showChannelList()
    })
      // when user add favorite in the mini detail page, the channel list refresh the favorite icon
    EventService.on('ADD_FAVORITE', (data) => {
      if (data['channelInfos']) {
        for (let i = 0; i < this.allChannelList.length; i++) {
            if (this.allChannelList[i]['channelId'] === data['channelInfos']['ID']) {
              this.allChannelList[i]['isFav'] = true
              break
            }
          }
      } else {
        for (let i = 0; i < this.allChannelList.length; i++) {
            if (this.allChannelList[i]['channelId'] === data['channelDetail']['ID']) {
              this.allChannelList[i]['isFav'] = true
              break
            }
          }
      }
    })
      // when user delete favorite in the mini detail page, the channel list refresh the favorite icon
    EventService.on('REMOVE_FAVORITE', (data) => {
      if (data['channelInfos']) {
        for (let i = 0; i < this.allChannelList.length; i++) {
            if (this.allChannelList[i]['channelId'] === data['channelInfos']['ID']) {
              this.allChannelList[i]['isFav'] = false
              break
            }
          }
      } else {
        for (let i = 0; i < this.allChannelList.length; i++) {
            if (this.allChannelList[i]['channelId'] === data['channelDetail']['ID']) {
              this.allChannelList[i]['isFav'] = false
              break
            }
          }
      }
    })

    EventService.removeAllListeners(['RECORDLIVE_ADD_SUCCESS'])
      // when user add recording, the channel list refresh the recording icon
    EventService.on('RECORDLIVE_ADD_SUCCESS', (data) => {
      for (let i = 0; i < this.allChannelList.length; i++) {
          // single recording
        if (data[0] === 'single' && data[1] === this.allChannelList[i]['playbillId']) {
            this.allChannelList[i]['isRecording'] = true
            this.allChannelList[i]['hasRecordingPVR'] = '1'
            this.allChannelList[i]['playbillDetail']['hasRecordingPVR'] = '1'
            break
            // series recording
          } else if (data[0] === 'periodic' && data[1] === this.allChannelList[i]['playbillId']) {
            this.allChannelList[i]['isRecording'] = true
            this.allChannelList[i]['hasRecordingPVR'] = '2'
            this.allChannelList[i]['playbillDetail']['hasRecordingPVR'] = '2'
            break
          }
      }
    })
    EventService.removeAllListeners(['RECORDLIVE_CANCEL_SUCCESS'])
      // when user delete recording, the channel list refresh the recording icon
    EventService.on('RECORDLIVE_CANCEL_SUCCESS', (data) => {
      for (let i = 0; i < this.allChannelList.length; i++) {
        if (data === this.allChannelList[i]['playbillId']) {
            this.allChannelList[i]['playbillDetail']['hasRecordingPVR'] = '0'
            this.allChannelList[i]['isRecording'] = false
            break
          }
      }
    })
    EventService.on('PLAYBILL_TASK_SOLVE_CONFLICT', (playbillID) => {
      for (let i = 0; i < this.allChannelList.length; i++) {
        if (playbillID === this.allChannelList[i]['playbillId']) {
            this.allChannelList[i]['hasRecordingPVR'] = '1'
            this.allChannelList[i]['playbillDetail']['hasRecordingPVR'] = '1'
            this.allChannelList[i]['isRecording'] = true
            break
          }
      }
    })
    EventService.on('SLOVE_SERIES_CONFLICT', (data) => {
      for (let i = 0; i < this.allChannelList.length; i++) {
        if (data['playbillID'] === this.allChannelList[i]['playbillId']) {
            this.allChannelList[i]['hasRecordingPVR'] = '2'
            this.allChannelList[i]['playbillDetail']['hasRecordingPVR'] = '2'
            this.allChannelList[i]['isRecording'] = true
            break
          }
      }
    })
    EventService.removeAllListeners(['SOLVE_CONFLICT_BY_DELETE'])
    EventService.on('SOLVE_CONFLICT_BY_DELETE', (playbillIDArray) => {
      for (let i = 0; i < playbillIDArray.length; i++) {
        for (let j = 0; j < this.allChannelList.length; j++) {
            if (playbillIDArray[i] === this.allChannelList[j]['playbillId']) {
              this.allChannelList[j]['hasRecordingPVR'] = '0'
              this.allChannelList[j]['playbillDetail']['hasRecordingPVR'] = '0'
              this.allChannelList[j]['isRecording'] = false
              break
            }
          }
      }
    })
  }

    /**
     * judge channel img isn't exit
     */
  isChannelLogo (channelPlaybill) {
    channelPlaybill.showChannelLogo = true
  }

    /**
     * monitor the scroll event and load data in current page
     */
  showChannelList () {
      // if the count of current data less then the total
    if (this.isToBottom && this.totalLength > this.allChannelList.length) {
        // when call the interface, the scroll roll without calling the interface
      this.isToBottom = false
        // show the load circle
      document.querySelector('.livetv-banner-load')['style']['display'] = 'block'
      this.whatOnService.queryAllChannels({ offset: this.allChannelList.length }).then(resp => {
        this.allChannelList = this.allChannelList.concat(this.whatOnService.getAllChannel(resp.channelPlaybills))
        this.pluseOnScroll()
          // when finish call interface, the scroll roll and load data
        this.isToBottom = true
          // refresh next playbill
        this.getNextPlaybill()
        document.querySelector('.livetv-banner-load')['style']['display'] = 'none'
      })
    }
  }

    /**
     * refresh the progress bar
     */
  refreshBanner () {
      // resfresh whatOn's data
    this.programList = this.whatOnService.refreshPosterProgress(this.programList)
      // refresh channel list data
    if (this.channelData && this.channelData.length > 0) {
      this.allChannelList = this.whatOnService.refreshProgress(this.allChannelList)
    }
  }

    /**
     * resfresh the next playbill
     */
  getNextPlaybill () {
    let timeList = []
    for (let i = 0; i < this.allChannelList['length']; i++) {
      if (this.allChannelList[i]['channelId']) {
        timeList.push({
            index: i,
            time: this.allChannelList[i]['endTime'],
            channelId: this.allChannelList[i]['channelId']
          })
      }
    }
      // find the earliest end program
    let minTime = _.min(timeList, item => {
      if (item && item['channelId']) {
        return Number(item['time'])
      }
    })
      // set timer
      // when current palybill is end,refresh next palybill
    clearTimeout(this.nextAllPlaybillTimer)
    this.nextAllPlaybillTimer = setTimeout(() => {
      this.whatOnService.getPlaybillContext({
          channelId: minTime.channelId,
          time: Number(minTime['time']) + 1000
        }).then(playbill => {
          let length = Number(playbill['endTime']) - Number(playbill['startTime'])
          let curLength = Date.now()['getTime']() - Number(playbill['startTime'])
          let i = minTime['index']
          this.allChannelList[i]['endTime'] = playbill['endTime']
          this.allChannelList[i]['startTime'] = playbill['startTime']
          this.allChannelList[i]['playbillId'] = playbill['ID']
          this.allChannelList[i]['playbillDetail'] = playbill
          this.allChannelList[i]['playbillName'] = this.getPlaybillName(playbill)
          let leftTimes = Math.ceil((new Date(Number(playbill['endTime'])).getTime() -
                    new Date(Number(Date.now()['getTime']())).getTime()) / 60000)
          this.allChannelList[i]['leftTimes'] = { min: leftTimes }
          this.allChannelList[i]['isLocked'] = this.judgeIsLocked(playbill, i)
          this.allChannelList[i]['percent'] = curLength >= length ? '100%' : Number(curLength) / Number(length) * 100 + '%'
          this.allChannelList[i]['isRecording'] = this.judgeIsRecording(playbill)
          this.allChannelList[i]['hasRecordingPVR'] = (playbill.hasRecordingPVR &&
                    playbill.hasRecordingPVR !== '0' && (playbill.hasRecordingPVR === '1')) ? '1' : '2'
          this.allChannelList[i]['isFillProgram'] = playbill && playbill['isFillProgram'] &&
                    (playbill['isFillProgram'] === '1')
          // refresh finished and start a new timer
          this.getNextPlaybill()
        })
    }, minTime['time'] - Date.now()['getTime']())
  }

    /**
     * function to get the name of the program
     * @param playbill the target program
     */
  getPlaybillName (playbill) {
    return playbill['isFillProgram'] && (playbill['isFillProgram'] === '1') &&
        (playbill['name'] === '') ? 'not_program' : playbill['name']
  }

    /**
     * judge the channel is locked or not
     * @param playbill target channel
     * @param i index of all channels list
     */
  judgeIsLocked (playbill, i) {
    return Number(playbill['rating']['ID']) > Number(session.get('PROFILERATING_ID')) ||
            Number(this.allChannelList[i]['channelRatingID']) > Number(session.get('PROFILERATING_ID'))
  }

    /**
     * judge whether the channel is recording
     */
  judgeIsRecording (playbill) {
    return !!(playbill.hasRecordingPVR && playbill.hasRecordingPVR !==
            '0' && Cookies.getJSON('IS_PROFILE_LOGIN'))
  }

    /**
     * scroll event
     */
  pluseOnScroll () {
    let oConter, oUl, oScroll, oSpan
    let oBox = document.getElementById('playBill')
    oConter = document.getElementById('channleList')
    oUl = document.getElementById('channleUl')
    oScroll = document.getElementById('scroll')
      // when the count of data is less,the scroll disappear according to the screen size
    if (document.body.clientWidth > 1440) {
      if (this.allChannelList.length < 12) {
        return
      }
    } else {
      if (this.allChannelList.length < 8) {
        return
      }
    }
    if (oScroll) {
      oSpan = oScroll.getElementsByTagName('span')[0]
    }
    this.directionScroll.setScroll(oBox, oConter, oUl, oScroll, oSpan, false, 'isToBottom')
  }

    /**
     * play
     */
  public play (channelPlaybill): void {
    if (Cookies.getJSON('IS_GUEST_LOGIN')) {
      this.judgeLogInBg = true
      return
    }
      // show the loading circle first
    document.getElementsByClassName('play_loading')[0]['style']['display'] = 'block'
    this.showCurrenList = channelPlaybill
      // if the playbill have been end ,click the poster and turn to detail page
    if (channelPlaybill['playbillDetail']['endTime'] > Date.now()['getTime']()) {
      EventService.emit('OPEN_FULLSCREEN_LIVETVVIDEO', {})

      this.playService.fullScreen(document.querySelector('#videoContainer'), true)
      this.playbillAppService.playProgram(channelPlaybill['playbillDetail'], channelPlaybill['channelDetail'])
    } else {
        // if the playbill is fill program, show the tips
      if (channelPlaybill['isFillProgram']) {
        EventService.emit('NO_PROGRAM_TOAST')
        return
      }

      this.router.navigate(['live-tv/live-tv-detail', Number(channelPlaybill['playbillId']),
        channelPlaybill['endTime'], this.whatOnDatas['recmActionID'],
        'Program_Whats_On'])
    }
  }

    /**
     * turn to TVGuide page
     */
  gotoGuide () {
    session.put('LIVE_TV_BACKPOSTER', this.backPoster, true)
      // jump to a new page according to the route
    this.router.navigate(['./live-tv/guide'])
  }

    /**
     *  the Chrome browser show blur, but IE browser not
     */
  showBlur (): any {
    let ua = navigator.userAgent
    if (ua.indexOf('Chrome') !== -1 && ua.indexOf('Edge') === -1) {
      return {
        '-webkit-filter': 'blur(20px)'
      }
    } else if (ua.indexOf('Firefox') !== -1 && ua.indexOf('Edge') === -1) {
      return {
          'filter': 'blur(20px)'
        }
    }
  }

  public showVODPlayOtherCom (): void {
    EventService.emit('SHOW_PLAYER_DROP', 'SECOND_SHOW')
  }

  public hideVODPlayOtherCom (): void {
    EventService.emit('HIDE_PLAYER_DROP')
  }
}
