import * as _ from 'underscore'
import { Component, OnInit, Input, OnDestroy, ViewChild } from '@angular/core'
import { SuspensionComponentProgram } from 'ng-epg-ui/webtv-components/programSuspension/programSuspension.component'
import { EventService } from 'ng-epg-sdk/services'
import { HotGenresService } from './hot-genres.service'
import { Router } from '@angular/router'
import { ProgramComponent } from 'ng-epg-ui/webtv-components/programComponent/programComponent.component'
import { PlaybillAppService } from '../../../component/playPopUpDialog/playbillApp.service'
import { TranslateService } from '@ngx-translate/core'
import { CommonService } from '../../../core/common.service'

@Component({
  selector: 'app-hot-genres',
  styleUrls: ['./hot-genres.component.scss'],
  templateUrl: './hot-genres.component.html'
})

export class HotGenresComponent implements OnInit, OnDestroy {
  @ViewChild(SuspensionComponentProgram) suspensionComponentProgram: SuspensionComponentProgram
  @ViewChild(ProgramComponent) programComponent: ProgramComponent
    /**
     * declare variables
     */
  public hotGenresLists = []
  public data = []
  public hotGenresId = []
  public nextPlaybillTimer: any
    /**
     * when the data changed, get current playbill of each genre
     */
  @Input() set hotGenresData (lists) {
    this.data = lists
    for (let i = 0; i < lists.length; i++) {
      this.hotGenresLists[i] = this.hotGenresService.queryHotGenres(this.data[i]['channelPlaybillList'])
    }
    _.map(this.hotGenresLists, (list, index) => {
      list.name = this.data[index].name
      list.id = this.data[index].ID
    })
    EventService.emit('CHANGESUBJECTDATA', this.hotGenresLists)
    _.delay(() => {
        // refresh next playbill
      this.getNextPlaybill()
    }, 1000 * 8)
  }
    /**
     * when exit the component, clear all the timer and events
     */
  ngOnDestroy () {
    clearInterval(this.nextPlaybillTimer)
  }
    /**
     * initialization
     */
  ngOnInit () {
    this.hotGenresId = ['suspensionHotGenres', 'hotGenresData']
  }
    /**
     * name for modules that would be used in this module
     */
  constructor (
        private route: Router,
        private hotGenresService: HotGenresService,
        private playbillAppService: PlaybillAppService,
        private translate: TranslateService,
        private commonService: CommonService
    ) { }
    /**
     * turn to more program page
     */
  moreShow (data) {
    this.route.navigate(['live-tv', data[0], data[1]])
  }
    /**
     * play the playbill
     */
  onClickDetail (info) {
      // enter full screen
    EventService.emit('OPEN_FULLSCREEN_LIVETVVIDEO', {})
    let video = document.querySelector('#videoContainer video')
      // judge different browsers
    let ua = navigator.userAgent.toLowerCase()
    let isIe = !!ua.match(/msie/i) || !!ua.match(/rv:([\d.]+).*like gecko/)
    let isFirefox = !!ua.match(/firefox\/([\d.]+)/)
    let isEdge = !!ua.match(/edge\/([\d.]+)/)
    if (isIe) {
      video = document.querySelector('#videoContainer')
      video['msRequestFullscreen']()
    } else if (isEdge) {
      video = document.querySelector('#videoContainer')
      video.webkitRequestFullScreen()
    } else if (isFirefox) {
        video = document.querySelector('#videoContainer')
        video['mozRequestFullScreen']()
      } else if (ua.match(/version\/([\d.]+).*safari/)) {
        video.requestFullscreen()
      } else {
        video.webkitRequestFullScreen()
      }
    this.playbillAppService.playProgram(info[0], info[1])
  }
    /**
     * refresh data
     */
  refreshHotGenres () {
    if (this.programComponent['programData'] && this.programComponent['programData'].length > 0) {
      this.programComponent['programData'] = this.hotGenresService.refreshRecm(this.programComponent['programData'])
    }
  }
    /**
     * refresh next playbill
     */
  getNextPlaybill () {
    if (this.programComponent && this.programComponent['programData'] && this.programComponent['programData'].length > 0) {
        // array to store end time and channel id
      let timeList = []
      for (let i = 0; i < this.programComponent['programData']['length']; i++) {
        for (let j = 0; j < this.programComponent['programData'][i]['length']; j++) {
            timeList.push({
              i: i,
              j: j,
              time: this.programComponent['programData'][i][j]['endTime'],
              channelId: this.programComponent['programData'][i][j]['channelID']
            })
          }
      }
        // find the earliest end program
      let minTime = _.min(timeList, item => {
        return Number(item['time'])
      })
        // set timer
        // when current palybill is end,refresh next palybill
      clearTimeout(this.nextPlaybillTimer)
      this.nextPlaybillTimer = setTimeout(() => {
        this.hotGenresService.getPlaybill({
            channelId: minTime['channelId'],
            time: minTime['time']
          }).then(data => {
            let leftTime = this.commonService.dealWithPlaybillTimeReduce(Date.now()['getTime'](), Number(data['endTime']))
            let leftTimes
            leftTimes = { min: leftTime }
            let playbilllTime = (Number(data['endTime'])) - (Number(data['startTime']))
            let currentDuration = Date.now()['getTime']() - (Number(data['startTime']))
            let i = minTime['i']
            let j = minTime['j']
            this.programComponent['programData'][i][j]['playbillDetail'] = data
            this.programComponent['programData'][i][j]['endTime'] = data['endTime']
            this.programComponent['programData'][i][j]['startTime'] = data['startTime']
            this.programComponent['programData'][i][j]['playbillID'] = data['ID']
            this.programComponent['programData'][i][j]['name'] = data['isFillProgram'] && (data['isFillProgram'] === '1') &&
                     (data['name'] === '') ? 'not_program' : data['name']
            this.programComponent['programData'][i][j]['posterUrl'] = data['picture'] && data['picture'].posters &&
                    data['picture'].posters[0]
            this.programComponent['programData'][i][j]['floatInfo'] = data
            this.programComponent['programData'][i][j]['leftTime'] = leftTime
            this.programComponent['programData'][i][j]['leftTimes'] = leftTimes
            this.programComponent['programData'][i][j]['len'] = currentDuration > playbilllTime
              ? '100%' : Number(currentDuration / playbilllTime * 100) + '%'
            this.programComponent['programData'][i][j]['isFillProgram'] = !!(data && data['isFillProgram'] &&
                     (data['isFillProgram'] === '1'))
            // refresh finished, start a new timer
            this.getNextPlaybill()
          })
      }, minTime['time'] - Date.now()['getTime']())
    }
  }
}
