import * as _ from 'underscore'
import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core'
import { WaterfallChannelBox } from 'ng-epg-ui/webtv-components/waterfall-channel/waterfall-channel.component'
import { ActivatedRoute, Router } from '@angular/router'
import { HotGenresMoreService } from './hot-genres-more.service'
import { PictureService } from 'ng-epg-ui/services/picture.service'
import { EventService } from 'ng-epg-sdk/services'
import { PlaybillAppService } from '../../../../component/playPopUpDialog/playbillApp.service'
import { DateUtils } from 'ng-epg-sdk/utils'
import { session } from 'src/app/shared/services/session'
import { ChannelCacheService } from '../../../../shared/services/channel-cache.service'

@Component({
  selector: 'app-hot-genres-more',
  templateUrl: './hot-genres-more.component.html',
  styleUrls: ['./hot-genres-more.component.scss'],
  providers: [HotGenresMoreService, PictureService, PlaybillAppService]
})

export class HotGenresMoreComponent implements OnInit, OnDestroy {
  @ViewChild(WaterfallChannelBox) waterfallChannelBox: WaterfallChannelBox
    // declare variables.
  private liveTvMoreTimer: any
  public moreChannelList: {
    'dataList': Array<any>,
    'row': string,
    'suspensionID': string
  }
  private title: string
  private subjectId: string
  private refreshTimer: any
  private showDataList
  private nextAllPlaybillTimer: any
  private theCurrentData
  private theFutureData
  private theCurrentAllData = []
  private theFutureAllData = []
  private theFuturOffset
  private getFutureData = false
    // private constructor
  constructor (
        private route: ActivatedRoute,
        private router: Router,
        private hotGenresMoreService: HotGenresMoreService,
        private pictureService: PictureService,
        private channelCacheService: ChannelCacheService,
        private playbillAppService: PlaybillAppService) {
  }
  ngOnDestroy () {
    if (this.liveTvMoreTimer) {
      clearInterval(this.liveTvMoreTimer)
    }
    if (this.refreshTimer) {
      clearTimeout(this.refreshTimer)
    }
  }
  ngOnInit () {
    let self = this
    document.body.scrollTop = 0
    document.documentElement.scrollTop = 0
      // get the subject id and the subject name from the route.
    this.route
        .params
        .subscribe(params => {
          self.title = params['name']
          self.subjectId = params['subjectID']
        })
    this.showHotGenresMore(0, true)
    EventService.removeAllListeners(['LOAD_DATA'])
    EventService.on('LOAD_DATA', function () {
      if (document.getElementsByClassName('live-filter-load')) {
        document.getElementsByClassName('live-filter-load')[0]['style']['display'] = 'block'
      }
      if (self.getFutureData) {
        let futureSliceData = self.theFutureAllData.slice(self.theFuturOffset, self.theFuturOffset + 30)
        self.showDataList = self.showDataList.concat(futureSliceData)
        self.moreChannelList = {
            'dataList': self.showDataList,
            'row': 'All',
            'suspensionID': 'moreData'
          }
        self.theFuturOffset = self.theFuturOffset + futureSliceData.length
        if (document.getElementsByClassName('live-filter-load')) {
            document.getElementsByClassName('live-filter-load')[0]['style']['display'] = 'none'
          }
      } else {
        self.showHotGenresMore(self.showDataList.length + '', false)
      }
    })
  }

    /**
     * deal with all datas.
     */
  showHotGenresMore (offset, ifFirst) {
      // get the subject datas according to the subject id.
    this.hotGenresMoreService.getQueryPlaybillDatas(this.subjectId, offset, ifFirst).then((resp) => {
      this.theCurrentData = resp[0]
      this.theFutureData = resp[1]
      this.theCurrentAllData = this.theCurrentAllData.concat(resp[0])
      this.theFutureAllData = this.theFutureAllData.concat(resp[1])
      if (resp[0].length >= 30) {
        if (ifFirst) {
            this.showDataList = resp[0]
          } else {
            this.showDataList = this.showDataList.concat(resp[0])
          }
        this.getFutureData = false
      } else {
        if (ifFirst) {
            this.showDataList = resp[0].concat(resp[1]).slice(0, 30)
            this.getFutureData = true
          } else {
            let lastShowDataListLength = this.showDataList.length
            let stayData = resp[0].concat(this.theFutureAllData).slice(0, 30)
            this.showDataList = this.showDataList.concat(stayData)
            this.theFuturOffset = this.showDataList.length - lastShowDataListLength - resp[0].length
            this.getFutureData = true
          }
      }
      this.moreChannelList = {
        'dataList': this.showDataList,
        'row': 'All',
        'suspensionID': 'moreData'
      }
        // refresh next playbill
      this.getNextPlaybill()
      this.refreshData()
    })
  }

    /**
     * refresh next playbill
     */
  getNextPlaybill () {
    let timeList = []
    for (let i = 0; i < this.showDataList['length']; i++) {
      if (this.showDataList[i]['channelId']) {
        timeList.push({
            index: i,
            time: this.showDataList[i]['endTime'],
            channelId: this.showDataList[i]['channelId']
          })
      }
    }
      // find the earliest end program
    let minTime = _.min(timeList, item => {
      if (item && item['channelId']) {
        return Number(item['time'])
      }
    })
      // set timer
      // when current palybill is end,refresh next palybill
    clearTimeout(this.nextAllPlaybillTimer)
    this.nextAllPlaybillTimer = setTimeout(() => {
      this.hotGenresMoreService.getPlaybillContext({
          channelId: minTime.channelId,
          subjectId: this.subjectId
        }).then(playbill => {
          let channelDtails = this.channelCacheService.getStaticChannelData().channelDetails
          let dynamicChannelData = this.channelCacheService.getDynamicChannelData().channelDynamaicProp
          let channelDetail = _.find(channelDtails, (item) => {
            return item['ID'] = playbill['currentPlaybillLite'].channelID ||
                        (item['ID'] = playbill['nextPlaybillLites'][0].channelID)
          })
          _.find(dynamicChannelData, (detail) => {
            if (detail && channelDetail && channelDetail['ID'] === detail['ID']) {
              channelDetail['channelNO'] = detail['channelNO']
              let nameSpace = session.get('ott_channel_name_space')
              let physicalChannelsDynamicProperty = _.find(detail['physicalChannelsDynamicProperties'], (property) => {
                return _.contains(property['channelNamespaces'], nameSpace)
              })
              channelDetail['physicalChannelsDynamicProperty'] = physicalChannelsDynamicProperty ||
                            detail['physicalChannelsDynamicProperties'][0]
            }
          })
          let currentPlaybill = playbill['currentPlaybillLite']
          let futurePlaybill = playbill['nextPlaybillLites'][0]
          let leftTimes = Math.ceil((new Date(Number(currentPlaybill['endTime'])).getTime() -
                    new Date(Number(Date.now()['getTime']())).getTime()) / 60000)
          if (leftTimes < 0) {
            leftTimes = 0
          }
          let isFillProgram
          if (currentPlaybill['isFillProgram'] && (currentPlaybill['isFillProgram'] === '1')) {
            isFillProgram = true
          }
          _.each(this.showDataList, (list, index) => {
            this.getList(currentPlaybill, list, futurePlaybill, channelDetail, isFillProgram, leftTimes)
          })
          // refresh finished and start a new timer
          this.getNextPlaybill()
        })
    }, minTime['time'] - Date.now()['getTime']())
  }
    /**
     * get current page content
     * @param currentPlaybill
     * @param list
     * @param futurePlaybill
     * @param channelDetail
     * @param isFillProgram
     */
  getList (currentPlaybill, list, futurePlaybill, channelDetail, isFillProgram, leftTimes) {
    if ((currentPlaybill['channelID'] === list['channelId']) && list['isFuture'] === 'current') {
      list['playbillInfo'] = futurePlaybill
      list['channelInfo'] = channelDetail
      list['channelId'] = currentPlaybill['channelID']
      list['ID'] = currentPlaybill['ID']
      list['startTime'] = currentPlaybill['startTime']
      list['endTime'] = currentPlaybill['endTime']
      list['name'] = this.getPlaybillName(currentPlaybill)
      this.setPosterUrl(list, currentPlaybill)
      list['isLen'] = Number(currentPlaybill['startTime']) > Date.now()['getTime']() ? '0' : '1'
      list['floatflag'] = 'moreList'
      list['morePlaybillflag'] = 'morePlaybillList'
      list['floatInfo'] = currentPlaybill
      list['len'] = Date.now()['getTime']() < Number(currentPlaybill['startTime']) ? undefined
          : (Date.now()['getTime']() - Number(currentPlaybill['startTime'])) / (Number(currentPlaybill['endTime']) -
                    Number(currentPlaybill['startTime'])) * 100 + '%'
      list['leftTime'] = leftTimes
      list['leftTimes'] = { min: list['leftTime'] }
      list['startTimes'] = DateUtils.format(parseInt(currentPlaybill['startTime'], 10), 'HH:mm')
      list['channelName'] = ' | ' + channelDetail['name']
      list['isFillProgram'] = isFillProgram
      list['isFuture'] = 'current'
    } else if ((futurePlaybill['channelID'] === list['channelId']) && list['isFuture'] === 'future') {
      list['playbillInfo'] = futurePlaybill
      list['channelInfo'] = channelDetail
      list['channelId'] = futurePlaybill['channelID']
      list['ID'] = futurePlaybill['ID']
      list['startTime'] = futurePlaybill['startTime']
      list['endTime'] = futurePlaybill['endTime']
      list['name'] = this.getPlaybillName(futurePlaybill)
      this.setPosterUrl(list, futurePlaybill)
      list['isLen'] = Number(futurePlaybill['startTime']) > Date.now()['getTime']() ? '0' : '1'
      list['floatflag'] = 'moreList'
      list['morePlaybillflag'] = 'morePlaybillList'
      list['floatInfo'] = futurePlaybill
      list['len'] = Date.now()['getTime']() < Number(futurePlaybill['startTime']) ? undefined
          : (Date.now()['getTime']() - Number(futurePlaybill['startTime'])) / (Number(futurePlaybill['endTime']) -
                    Number(futurePlaybill['startTime'])) * 100 + '%'
      list['leftTime'] = leftTimes
      list['leftTimes'] = { min: list['leftTime'] }
      list['startTimes'] = DateUtils.format(parseInt(futurePlaybill['startTime'], 10), 'HH:mm')
      list['channelName'] = ' | ' + channelDetail['name'] || ''
      list['isFillProgram'] = isFillProgram
      list['isFuture'] = 'future'
    }
  }
    /**
     * refresh playbill name
     * @param playbill
     */
  getPlaybillName (playbill) {
    let name = playbill && playbill['name'] ? playbill['name'] : ''
    return name
  }
    /**
     * set this posterUrl style
     * @param list
     * @param playbill
     */
  setPosterUrl (list, playbill) {
    list['posterUrl'] = playbill['picture'] && playbill['picture'].posters &&
            this.pictureService.convertToSizeUrl(playbill['picture'].posters[0],
              { minwidth: 230, minheight: 129, maxwidth: 260, maxheight: 146 })
  }

    /**
     * set the timer.
     */
  refreshData () {
    this.liveTvMoreTimer = setInterval(() => {
      this.refreshHotGenresMore()
    }, 1000 * 60)
  }
    /**
     * refresh the playbill.
     */
  refreshHotGenresMore () {
    this.waterfallChannelBox.dataList = this.hotGenresMoreService.refreshhotGenreMore(this.waterfallChannelBox.dataList)
  }
    /**
     * click this picture or the playbill name to play or the detail page.
     */
  onClickDetail (data) {
      // if the playbill belong to the future,click to the detail page.
    if (data[2] === '0') {
      this.router.navigate(['live-tv', 'live-tv-detail', data[0], data[1]])
    }
      // if the playbill belong to the now,click to play.
    if (data[2] === '1') {
      EventService.emit('OPEN_FULLSCREEN_LIVETVVIDEO', {})
      let video = document.querySelector('#videoContainer video')
      let ua = navigator.userAgent.toLowerCase()
      let isIe = !!ua.match(/msie/i) || !!ua.match(/rv:([\d.]+).*like gecko/)
      let isFirefox = !!ua.match(/firefox\/([\d.]+)/)
      let isEdge = !!ua.match(/edge\/([\d.]+)/)
        // judge  different browsers
      if (isIe) {
        video = document.querySelector('#videoContainer')
        video['msRequestFullscreen']()
      } else if (isEdge) {
          video = document.querySelector('#videoContainer')
          video.webkitRequestFullScreen()
        } else if (isFirefox) {
          video = document.querySelector('#videoContainer')
          video['mozRequestFullScreen']()
        } else if (ua.match(/version\/([\d.]+).*safari/)) {
          video.requestFullscreen()
        } else {
          video.webkitRequestFullScreen()
        }
      this.playbillAppService.playProgram(data[0], data[1])
    }
  }
}
