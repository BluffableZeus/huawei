import * as _ from 'underscore'
import { Injectable } from '@angular/core'
import { queryPlaybillContext } from 'ng-epg-sdk/vsp'
import { PictureService } from 'ng-epg-ui/services/picture.service'
import { session } from 'src/app/shared/services/session'
import { ChannelCacheService } from '../../../../shared/services/channel-cache.service'
import { CommonService } from '../../../../core/common.service'

@Injectable()
export class HotGenresMoreService {
  public hotGenreMoreRefresh: Array<any> = []
  constructor (
        private pictureService: PictureService,
        private channelCacheService: ChannelCacheService,
        private commonService: CommonService
    ) { }
  getQueryPlaybillDatas (subjectId, offset, ifFirst) {
    let IDList = this.channelCacheService.getStroageChannelIDListBySubjectID(subjectId)
    IDList = IDList.slice(Number(offset), Number(offset) + 30)

    return queryPlaybillContext({
      needChannel: '0',
      queryPlaybillContext: {
        type: '1',
        date: String(Date.now()['getTime']()),
        preNumber: '0',
        nextNumber: '1',
        isFillProgram: '1'
      },
      queryChannel: {
        subjectID: subjectId,
        channelIDs: IDList,
        isReturnAllMedia: '1'
      }
    }).then(resp => {
      let channelDtails = this.channelCacheService.getStaticChannelData().channelDetails
      let dynamicChannelData = this.channelCacheService.getDynamicChannelData().channelDynamaicProp
      _.each(resp.channelPlaybillContexts, (item) => {
          item['channelInfos'] = _.find(channelDtails, (detail) => {
            return detail['ID'] === item['currentPlaybillLite'].channelID ||
                        detail['ID'] === item['nextPlaybillLites'][0].channelID
          })
          _.find(dynamicChannelData, (detail) => {
            if (detail && item['channelInfos'] && item['channelInfos']['ID'] === detail['ID']) {
              item['channelInfos']['channelNO'] = detail['channelNO']
              let nameSpace = session.get('ott_channel_name_space')
              let physicalChannelsDynamicProperty = _.find(detail['physicalChannelsDynamicProperties'], (property) => {
                return _.contains(property['channelNamespaces'], nameSpace)
              })
              item['channelInfos']['physicalChannelsDynamicProperty'] =
                            physicalChannelsDynamicProperty || detail['physicalChannelsDynamicProperties'][0]
            }
          })
        })
        // define futPlaybill
      let futPlaybill = _.map(resp['channelPlaybillContexts'], (list) => {
          let channelDetail = list['channelDetail'] || list['channelInfos']
          let futItem = list['nextPlaybillLites'] && list['nextPlaybillLites'][0]
          let leftTimes = this.commonService.dealWithPlaybillTimeReduce(new Date(Number(Date.now()['getTime']())).getTime(),
            new Date(Number(futItem['endTime'])).getTime())
          if (leftTimes < 0) {
            leftTimes = 0
          }
          let playbillName = futItem && futItem['name'] ? futItem['name'] : ''
          let isFillProgram
          playbillName = this.setPlaybillName(futItem, playbillName)
          if (futItem['isFillProgram'] && (futItem['isFillProgram'] === '1')) {
            isFillProgram = true
          }
          // define futPlaybillData
          let futPlaybillData = {
            playbillInfo: list['nextPlaybillLites'][0],
            channelInfo: channelDetail,
            channelId: futItem['channelID'],
            ID: futItem['ID'],
            startTime: futItem['startTime'],
            endTime: futItem['endTime'],
            name: playbillName,
            posterUrl: this.getPosterUrl(futItem),
            isLen: Number(futItem['startTime']) > Date.now()['getTime']() ? '0' : '1',
            floatflag: 'moreList',
            morePlaybillflag: 'morePlaybillList',
            floatInfo: futItem,
            len: Date.now()['getTime']() < Number(futItem['startTime']) ? undefined
              : (Date.now()['getTime']() - Number(futItem['startTime'])) / (Number(futItem['endTime']) -
                            Number(futItem['startTime'])) * 100 + '%',
            leftTime: leftTimes,
            leftTimes: { min: leftTimes },
            startTimes: this.commonService.dealWithDateJson('D10', futItem['startTime']),
            channelName: ' | ' + channelDetail['name'],
            isFillProgram: isFillProgram,
            isFuture: 'future'
          }
          return futPlaybillData
        })
        // define curPlaybill
      let curPlaybill = _.map(resp['channelPlaybillContexts'], (list) => {
          let channelDetail = list['channelDetail'] || list['channelDetail'] || list['channelInfos']
          let curItem = list['currentPlaybillLite']
          let leftTimes = Math.ceil((new Date(Number(curItem['endTime'])).getTime() -
                    new Date(Number(Date.now()['getTime']())).getTime()) / 60000)
          if (leftTimes < 0) {
            leftTimes = 0
          }
          let playbillName = curItem && curItem['name'] ? curItem['name'] : ''
          let isFillProgram
          playbillName = this.setPlaybillName(curItem, playbillName)
          if (curItem['isFillProgram'] && (curItem['isFillProgram'] === '1')) {
            isFillProgram = true
          }
          // define curPlaybillData
          let curPlaybillData = {
            playbillInfo: list['nextPlaybillLites'][0],
            channelInfo: channelDetail,
            channelId: curItem['channelID'],
            ID: curItem['ID'],
            startTime: curItem['startTime'],
            endTime: curItem['endTime'],
            name: playbillName,
            posterUrl: this.getPosterUrl(curItem),
            isLen: Number(curItem['startTime']) > Date.now()['getTime']() ? '0' : '1',
            floatflag: 'moreList',
            morePlaybillflag: 'morePlaybillList',
            floatInfo: curItem,
            len: Date.now()['getTime']() < Number(curItem['startTime']) ? undefined
              : (Date.now()['getTime']() - Number(curItem['startTime'])) / (Number(curItem['endTime']) -
                            Number(curItem['startTime'])) * 100 + '%',
            leftTime: leftTimes,
            leftTimes: { min: leftTimes },
            startTimes: this.commonService.dealWithDateJson('D10', curItem['startTime']),
            channelName: ' | ' + channelDetail['name'],
            isFillProgram: isFillProgram,
            isFuture: 'current'
          }
          _.delay(() => {
            document.getElementsByClassName('live-filter-load')[0]['style']['display'] = 'none'
          }, 300)
          return curPlaybillData
        })
      return [curPlaybill, futPlaybill]
    })
  }
    /**
     * get posterUrl and set style of posterUrl
     * @param Item
     */
  getPosterUrl (Item) {
    let url = Item['picture'] && Item['picture'].posters && this.pictureService.convertToSizeUrl(Item['picture'].posters[0],
        { minwidth: 230, minheight: 129, maxwidth: 260, maxheight: 146 })
    return url
  }
    /**
     * set playbill name
     * @param item
     * @param playbillName
     */
  setPlaybillName (item, playbillName) {
    if (item && item['isFillProgram'] && (item['isFillProgram'] === '1') && (item['name'] === '')) {
      playbillName = 'not_program'
    }
    return playbillName
  }

    /**
     * refresh the progress bar
     */
  refreshhotGenreMore (list) {
    for (let i = 0; i < list.length / 2; i++) {
      list[i]['len'] = Date.now()['getTime']() < Number(list[i]['endTime'])
          ? (Date.now()['getTime']() - Number(list[i]['startTime'])) / (Number(list[i]['endTime']) -
                    Number(list[i]['startTime'])) * 100 + '%' : '100%'
      list[i]['isLen'] = Number(list[i]['startTime']) > Date.now()['getTime']() ? '0' : '1'
      list[i]['leftTime'] = Math.ceil((new Date(Number(list[i]['endTime'])).getTime() -
                new Date(Number(Date.now()['getTime']())).getTime()) / 60000)
      if (list[i]['leftTime'] < 0) {
        list[i]['leftTime'] = 0
      }
      list[i]['leftTimes'] = { min: list[i]['leftTime'] }
    }
    return list
  }
    /**
     * get playbill context
     * @param info
     */
  getPlaybillContext (info) {
    return queryPlaybillContext({
      needChannel: '0',
      queryPlaybillContext: {
        type: '1',
        date: String(Date.now()['getTime']()),
        preNumber: '0',
        nextNumber: '1',
        isFillProgram: '1'
      },
      queryChannel: {
        subjectID: [info['subjectId']] + '',
        channelIDs: [info['channelId']],
        isReturnAllMedia: '1'
      }
    }).then(resp => {
      let playbill = resp['channelPlaybillContexts'][0]
      return playbill
    })
  }
}
