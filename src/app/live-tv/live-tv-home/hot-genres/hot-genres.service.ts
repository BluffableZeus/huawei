import * as _ from 'underscore'
import { Injectable } from '@angular/core'
import { queryPlaybillList, getPlaybillDetail } from 'ng-epg-sdk/vsp'
import { PictureService } from 'ng-epg-ui/services/picture.service'
import { TranslateService } from '@ngx-translate/core'
import { CommonService } from '../../../core/common.service'

@Injectable()
export class HotGenresService {
    /**
     * declare variables
     */
  public needRefresh: Array<any> = []
  private recmProgramList: Array<any> = []
    /**
     * name for modules that would be used in this module
     */
  constructor (
        private pictureService: PictureService,
        private translate: TranslateService,
        private commonService: CommonService
    ) { }
    /**
     * function to add 'zero' in front of the target number
     * @param str target number
     * @param len the minimum length of str, if not enough, add 'zero'
     */
  prefixZero (str, len) {
      // change the data type from Number to String
    str = '' + (str || '')
    if (str.length >= len) {
      return str
    }
    let gapLen = len - str.length, i, arr = []
    for (i = 0; i < gapLen; i++) {
      arr[i] = '0'
    }
    return arr.join('') + str
  }

  getQueryPlaybill (id) {
      // call a interface
    return getPlaybillDetail({
      playbillID: id,
      isReturnAllMedia: '1'
    }).then(resp => {
        // return data come from the interface
      return resp
    })
  }
    /**
     * deal the playbill data
     */
  queryHotGenresTwice (list, playbill) {
    let playbillGenres = {}
    let playbillName = list['playbillLites'] && list['playbillLites'][0] && playbill['name']
    let isFillProgram
      // if the playbill is fill program, its name showes no program
    if (playbill['isFillProgram'] && (playbill['isFillProgram'] === '1') && (playbillName === '')) {
      playbillName = 'not_program'
    }
    if (list['playbillLites'] && list['playbillLites'][0] && playbill['isFillProgram'] && (playbill['isFillProgram'] === '1')) {
      isFillProgram = true
    }
    playbillGenres['playbillName'] = playbillName
    playbillGenres['isFillProgram'] = isFillProgram
    return playbillGenres
  }

  queryHotGenres (lists) {
      // if the length of lists more than 16, it would be 16
    if (lists.length > 16) {
      lists.length = 16
    }
    let channelList = _.map(lists, (list, index) => {
      let channelDetail = list['channelDetail']
      let playbill = list['playbillLites'] && list['playbillLites'][0] || {}
        // use the packaged function in commonService to calculate the left time
      let leftTime = this.commonService.dealWithPlaybillTimeReduce(new Date(Number(Date.now()['getTime']())).getTime(),
          new Date(Number(playbill['endTime'])).getTime())
      let leftTimes
        // if the leftTime less then 0, leftTime is 0
      if (leftTime < 0) {
        leftTime = 0
      }
      leftTimes = { min: leftTime }
        // time that the program has played
      let currentDuration = Date.now()['getTime']() - playbill['startTime']
      let playbilllTime = playbill['endTime'] - playbill['startTime']
        /**
             * the address of playbill poster
             * use the packaged function in pictureService
             * resize the poster
             */
      let url = playbill['picture'] && playbill['picture'].posters &&
                        this.pictureService.convertToSizeUrl(playbill['picture'].posters[0],
                          { minwidth: 313, minheight: 177, maxwidth: 330, maxheight: 186 })
      let playbillName = this.queryHotGenresTwice(list, playbill)['playbillName']
      let isFillProgram = this.queryHotGenresTwice(list, playbill)['isFillProgram']
      return {
        channelInfo: channelDetail,
        playbillInfo: list['playbillLites'] && list['playbillLites'][0] || {},
        channelID: channelDetail['ID'],
        playbillID: playbill['ID'],
        name: playbillName,
        startTime: playbill['startTime'],
        endTime: playbill['endTime'],
        posterUrl: url,
        channelDetail: channelDetail,
        playbillDetail: playbill,
        floatInfo: playbill,
        len: currentDuration > playbilllTime ? '100%' : Number(currentDuration / playbilllTime * 100) + '%',
        leftTimes: leftTimes,
        leftTime: leftTime,
        channel: ' | ' + channelDetail['name'],
        isFillProgram: isFillProgram
      }
    })
    return channelList
  }
    /**
     * refresh the progress bar
     */
  refreshRecm (list) {
    this.recmProgramList = list
    for (let j = 0; j < this.recmProgramList.length; j++) {
      for (let i = 0; i < this.recmProgramList[j].length; i++) {
          // the time that the program remains
        let leftTime = Math.ceil((new Date(Number(this.recmProgramList[j][i]['endTime'])).getTime() -
                Date.now()['getTime']()) / 60000)
        let leftTimes
        if (leftTime < 0) {
            leftTime = 0
          }
        leftTimes = { min: leftTime }
        let currentDuration = Date.now()['getTime']() - this.recmProgramList[j][i]['startTime']
        let playbilllTime = this.recmProgramList[j][i]['endTime'] - this.recmProgramList[j][i]['startTime']
        this.recmProgramList[j][i]['leftTime'] = Math.ceil((Number(this.recmProgramList[j][i]['endTime']) -
                Date.now()['getTime']()) / 60000)
        this.recmProgramList[j][i]['leftTimes'] = leftTimes
        this.recmProgramList[j][i]['len'] = currentDuration > playbilllTime
            ? '100%' : Number(currentDuration / playbilllTime * 100) + '%'
      }
    }
    return this.recmProgramList
  }
    /**
     *  the interface which to refresh next playbill
     */
  getPlaybill (info) {
      // call a interface
    return queryPlaybillList({needChannel: '0',
      queryPlaybill: {
        type: '1',
        count: '1',
        offset: '0',
        startTime: String(info['time']),
        isFillProgram: '1'
      },
      queryChannel: {
        channelIDs: [info['channelId']],
        isReturnAllMedia: '1'
      }
    }).then(resp => {
        // deal with the data come from the interface
      let playbill = resp['channelPlaybills'][0]['playbillLites'][0]
      return playbill
    })
  }
}
