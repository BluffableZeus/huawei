import * as _ from 'underscore'
import { Component, ViewChild, OnInit, OnDestroy } from '@angular/core'
import { LiveTvService } from './live-tv.service'
import { WhatOnComponent } from './what-on/what-on.component'
import { HotGenresComponent } from './hot-genres/hot-genres.component'
import { EventService } from 'ng-epg-sdk/services'
import { CustomConfigService } from '../../shared/services/custom-config.service'
import { CommonService } from '../../core/common.service'
import { session } from 'src/app/shared/services/session'
import { ChannelCacheService } from '../../shared/services/channel-cache.service'

@Component({
  selector: 'app-live-tv',
  providers: [
    LiveTvService, CustomConfigService, CommonService
  ],
  styleUrls: ['./live-tv.component.scss'],
  templateUrl: './live-tv.component.html'
})

export class LiveTVComponent implements OnInit, OnDestroy {
  @ViewChild(WhatOnComponent) whatOnModel: WhatOnComponent
  @ViewChild(HotGenresComponent) hotGenresModel: HotGenresComponent
    /**
    * variable definition
    */
  public whatOn: {}
  public happyNext: {}
  public hotGenres: Array<any>
  public allChannel: Array<any>
  public liveTvHomeTimer: any
    /**
     * name for modules that would be used in this module
     */
  constructor (
        private liveTvService: LiveTvService,
        private customConfigService: CustomConfigService,
        private commonService: CommonService,
        private channelCacheService: ChannelCacheService
    ) {
    this.liveTvHomeNotification = this.liveTvHomeNotification.bind(this)
  }
    /**
    * initialization
    */
  ngOnInit () {
    if (!session.get('CUSTOM_CONFIG_DATAS')) {
      EventService.on('CUSTOM_CACHE_CONFIG_DATAS', () => {
        this.getLiveTvHomeData()
      })
    } else {
      this.getLiveTvHomeData()
    }
    if (Cookies.getJSON('IS_GUEST_LOGIN')) {
      EventService.on('CUSTOM_CACHE_CONFIG_DATAS', () => {
        this.getLiveTvHomeData()
      })
    }
    this.getProgramData()
      // when user log in at live tv home page, refresh channel list
    EventService.removeAllListeners(['FIRST_TIME_LOGIN'])
    EventService.on('FIRST_TIME_LOGIN', () => {
      this.getProgramData()
        // the scroll resetting
      let oScroll = document.getElementById('scroll')
      let oUl = document.getElementById('channleUl')
        // data protection
      if (oScroll && oUl) {
        let oSpan = oScroll.getElementsByTagName('span')[0]
        oSpan.style.top = 0 + 'px'
        oUl.style.top = 0 + 'px'
      }
    })
  }
    /**
     * when exit the component, clear all the timer and events
     */
  ngOnDestroy () {
      // clear timer
    if (this.liveTvHomeTimer) {
      clearInterval(this.liveTvHomeTimer)
    }
    EventService.removeAllListeners(['CUSTOM_CACHE_CONFIG_DATAS'])
  }
    /**
     *  refresh the data of livetv home and the progress bar every 1 minute
     */
  refreshData () {
    clearInterval(this.liveTvHomeTimer)
    this.liveTvHomeTimer = setInterval(() => {
        // whatOn page
      if (this.whatOnModel) {
        this.whatOnModel.refreshBanner()
      }
        // hot genre page
      if (this.hotGenresModel) {
        this.hotGenresModel.refreshHotGenres()
      }
    }, 1000 * 60)
  }
    /**
     *  get the data of livetv home
     */
  getLiveTvHomeData () {
      // get custom config parameter
    if (document.querySelector('.livetv-filter-load')) {
      document.querySelector('.livetv-filter-load')['style']['display'] = 'block'
    }
    this.customConfigService.getCustomizeConfig({ queryType: '0' }).then((config) => {
      let req = {
        count: '24',
        subjectCount: '24',
        date: Date.now()['getTime']() + ''
      }
      let customizeConfig = config['list']
      let livetv_home_subject_ids = customizeConfig['livetv_home_subject_ids']
        // show MSAErrorLog
      if (_.isUndefined(livetv_home_subject_ids) || livetv_home_subject_ids === '') {
        this.commonService.MSAErrorLog(38013, 'QueryCustomizeConfig')
      } else {
        req['channelSubjectID'] = customizeConfig['livetv_home_subject_ids']
      }
      this.liveTvService.getPCLiveTvHome(req).then(resp => {
        this.livetvMSALog(resp)
        let resultData = resp['liveTVHomeSubjectList']
          // -1 mean whatOn data.
        this.whatOn = _.find(resultData, item => {
            return item['ID'] === '-1'
          }) || []
          // -2 mean happyNext data
        this.happyNext = _.find(resultData, item => {
            return item['ID'] === '-2'
          })
          // the others mean recommand program
        this.hotGenres = _.filter(resultData, item => {
            return item['ID'] !== '-1' && item['ID'] !== '-2'
          })
          // data protection
        if (document.querySelector('.livetv-filter-load')) {
            document.querySelector('.livetv-filter-load')['style']['display'] = 'none'
          }
      })
    })
  }
    /**
     * get channel list data
     */
  getProgramData () {
    this.channelCacheService.getStaticData().then(staticData => {
        // Array of total channel
      let channelDetails = staticData['channelDetails']
        // list of channel
      let IDList = _.pluck(channelDetails, 'ID')
        // sum of channel
      let IDListLength = IDList.length
        // quantities of channel to show initialization
      IDList = IDList.slice(0, 12)
      if (IDList.length === 0) {
        this.allChannel = [[], '0']
        return
      }
        // current time
      let curTime = Date.now()['getTime']() + ''
      this.liveTvService.getQueryPlaybill({
        needChannel: '0',
        queryPlaybill: { // only query current palybill and allow fill program
            type: '0',
            count: '1',
            offset: '0',
            startTime: curTime,
            endTime: curTime,
            isFillProgram: '1'
          },
        queryChannel: {
            channelIDs: IDList,
            subjectID: '-1',
            isReturnAllMedia: '1'
          }
      }, 'queryplaybilllist1').then(resp => {
          // show MSAErrorLog
          if (_.isUndefined(resp['channelPlaybills']) || resp['channelPlaybills'].length === 0) {
            this.commonService.MSAErrorLog(38031, 'QueryPlaybillList', 'liveTVHomeSubjectList')
          }
          this.refreshData()
          // get the channel detail info from the cache
          let channelDtails
          let dynamicChannelData
          channelDtails = staticData['channelDetails']
          _.each(resp.channelPlaybills, (item) => {
            item['channelInfos'] = _.find(channelDtails, (detail) => {
              return detail['ID'] === item['playbillLites'][0].channelID
            })
          })
          this.channelCacheService.asynGetDynamicData().then(dynamicData => {
            dynamicChannelData = dynamicData['channelDynamaicProp']
            _.each(resp.channelPlaybills, (item) => {
              _.find(dynamicChannelData, (detail) => {
                if (item['channelInfos']['ID'] === detail['ID']) {
                  item['channelInfos']['isLocked'] = detail['isLocked']
                  item['channelInfos']['favorite'] = detail['favorite']
                  item['channelInfos']['channelNO'] = detail['channelNO']
                  let nameSpace = session.get('ott_channel_name_space')
                  let physicalChannelsDynamicProperty = _.find(detail['physicalChannelsDynamicProperties'], (property) => {
                    return _.contains(property['channelNamespaces'], nameSpace)
                  })
                  item['channelInfos']['physicalChannelsDynamicProperty'] =
                                    physicalChannelsDynamicProperty || detail['physicalChannelsDynamicProperties'][0]
                }
              })
            })
            this.allChannel = [resp.channelPlaybills.slice(0, 12), IDListLength] || [[], '0']
          })
        })
    }, respFail => {
      _.delay(() => {
        this.getProgramData()
      }, 50)
    })
  }
    /**
     * listening dynamicChannelData
     */
  liveTvHomeNotification (eventName, data) {
    switch (eventName) {
      case 'CHANNEL_VERSION_CHANGED':
        this.getProgramData()
        break
      default:
        break
    }
  }
    /**
     * show log message
     */
  livetvMSALog (resp) {
    if (_.isUndefined(resp['liveTVHomeSubjectList']) || resp['liveTVHomeSubjectList'].length === 0) {
      this.commonService.commonLog('QueryOTTLiveTVHomeData', 'liveTVHomeSubjectList')
    } else {
      let livetvLengthZero = _.find(resp['liveTVHomeSubjectList'], item => {
        return item['channelPlaybillList'] && item['channelPlaybillList'].length === 0
      })
        // show MSAErrorLog
      if (!_.isUndefined(livetvLengthZero)) {
        this.commonService.MSAErrorLog(38031, 'QueryOTTLiveTVHomeData', 'liveTVHomeSubjectList')
      }
    }
  }
}
