import * as _ from 'underscore'
import { Injectable } from '@angular/core'
import { getPlaybillDetail } from 'ng-epg-sdk/vsp'
import { PictureService } from 'ng-epg-ui/services/picture.service'
import { DateUtils } from 'ng-epg-sdk/utils'
import { session } from 'src/app/shared/services/session'
import { CommonService } from '../../core/common.service'
import { CustomConfigService } from '../../shared/services/custom-config.service'
import { ChannelCacheService } from '../../shared/services/channel-cache.service'

@Injectable()
export class ChannelDetailService {
  /**
     * name for modules that would be used in this module
     */
  constructor (
        private pictureService: PictureService,
        private commonService: CommonService,
        private customConfigService: CustomConfigService,
        private channelCacheService: ChannelCacheService
  ) { }
  /**
    * call a interface
    * deal with the data come from the interface
    * use getPlaybillDetail to get data
    */
  getQueryPlaybill (id) {
    // call a interface
    return getPlaybillDetail({
      playbillID: id,
      isReturnAllMedia: '1'
    }).then(resp => {
      return resp
    })
  }
  /**
     * extend data of list
     */
  getDetailListTwice (resp) {
    // define list
    let list = {
      channelDetailrating: this.getChannelDetailrating(resp),
      rating: resp['playbillDetail'].rating && resp['playbillDetail'].rating.ID ? resp['playbillDetail'].rating.ID
        : session.get('PLAYBILLMAXRATING'),
      playbillDetail: resp.playbillDetail,
      channel: this.getChannel(resp),
      recomendList: this.getRecomendList(resp),
      channelDetail: resp['playbillDetail'].channelDetail ? resp['playbillDetail'].channelDetail : '',
      recmActionID: resp['recmActionID'],
      isCUTV: this.judgeIsCUTV(resp),
      isNPVR: resp['playbillDetail'].isNPVR,
      npvrRecordCR: resp['playbillDetail'].channelDetail && resp['playbillDetail'].channelDetail.physicalChannels[0] &&
            resp['playbillDetail'].channelDetail.physicalChannels[0]['npvrRecordCR'],
      param: {
        'num': this.prefixZero(resp['playbillDetail'] && resp['playbillDetail'].playbillSeries &&
                    resp['playbillDetail'].playbillSeries.seasonNO, 2)
      },
      param1: {
        'num': this.prefixZero(resp['playbillDetail'] && resp['playbillDetail'].playbillSeries &&
                    resp['playbillDetail'].playbillSeries.sitcomNO, 2)
      }
    }
    return list
  }
  /**
     * provide ratingID for key of channelDetailrating
     */
  getChannelDetailrating (resp) {
    let ratingID = resp['playbillDetail'].channelDetail && resp['playbillDetail'].channelDetail['rating']
      ? resp['playbillDetail'].channelDetail.rating.ID : '18'
    return ratingID
  }
  /**
     * provide channel for key of channel
     */
  getChannel (resp) {
    let channel = resp['playbillDetail'] ? resp['playbillDetail'].name : ''
    return channel
  }
  /**
     * provide list for key of recomendList
     */
  getRecomendList (resp) {
    let list = resp['recmContents'] && resp['recmContents'][0] && resp['recmContents'][0]['recmPrograms']
    return list
  }
  /**
     * provide isCUTV for key of isCUTV
     */
  judgeIsCUTV (resp) {
    let isCUTV = resp['playbillDetail'].channelDetail && resp['playbillDetail'].channelDetail.physicalChannels[0] &&
            resp['playbillDetail'].channelDetail.physicalChannels[0].cutvCR.enable
    return isCUTV
  }
  /**
     * get data from a method of getDetailList
     */
  getDetailList (resp, url) {
    // define time
    let weekTime = DateUtils.format(parseInt(resp['playbillDetail'].startTime, 10), 'ddd')
    let month = DateUtils.format(parseInt(resp['playbillDetail'].startTime, 10), 'MM')
    let day = DateUtils.format(parseInt(resp['playbillDetail'].startTime, 10), 'D')
    let year = DateUtils.format(parseInt(resp['playbillDetail'].startTime, 10), 'YYYY')
    // deal with month
    let leftTime = this.commonService.dealWithDateJson('D05', resp['playbillDetail'].startTime)
    let todayTime
    // current time
    let nowDay = DateUtils.format(parseInt(Date.now()['getTime'](), 10), 'D')
    let nowMonth = DateUtils.format(parseInt(Date.now()['getTime'](), 10), 'MM')
    let nowYear = DateUtils.format(parseInt(Date.now()['getTime'](), 10), 'YYYY')
    if (nowDay === day && nowMonth === month && nowYear === year) {
      leftTime = ''
      weekTime = ''
      todayTime = 'today'
    }
    // define list
    let list = {
      channelNo: resp.playbillDetail.channelDetail ? resp.playbillDetail.channelDetail.channelNO : '',
      logo: url,
      name: resp['playbillDetail'].name,
      isLock: resp['playbillDetail'].channelDetail ? resp['playbillDetail'].channelDetail.isLocked : '',
      isFav: resp['playbillDetail'].channelDetail ? resp['playbillDetail'].channelDetail['isFavorited'] : '',
      genre: resp.playbillDetail.genres,
      posterUrl: url,
      weekTime: weekTime,
      leftTime: leftTime,
      startTime: this.commonService.dealWithDateJson('D10', resp['playbillDetail'].startTime),
      startTimeCompare: resp['playbillDetail'].startTime,
      endTime: this.commonService.dealWithDateJson('D10', resp['playbillDetail'].endTime),
      Series: resp['playbillDetail'].playbillSeries,
      introduce: resp['playbillDetail'].introduce,
      ratingName: this.getRatingName(resp),
      todayTime: todayTime || ''
    }
    // extend list in a method of getDetailListTwice
    return _.extend(list, this.getDetailListTwice(resp))
  }
  /**
     * provide name for key of ratingName
     */
  getRatingName (resp) {
    let name = resp['playbillDetail'] && resp['playbillDetail'].rating &&
            resp['playbillDetail'].rating.name ? resp['playbillDetail'].rating.name : ''
    return name
  }
  /**
    * call a interface
    * deal with the data come from the interface
    */
  getDetail (playbillID, endtimes) {
    let recmscenarios
    if (endtimes > Date.now()['getTime']()) {
      recmscenarios = [{
        count: '12',
        offset: '0',
        contentType: 'PROGRAM',
        contentID: playbillID + '',
        entrance: 'Program_Similar_Recommendations',
        businessType: 'BTV'
      }]
    } else if (endtimes < Date.now()['getTime']()) {
      recmscenarios = [{
        count: '12',
        offset: '0',
        contentType: 'PROGRAM',
        contentID: playbillID + '',
        entrance: 'CatchupTV_Similar_Recommendations',
        businessType: 'CUTV'
      }]
    }
    // get data from QueryCustomizeConfigRequest
    return this.customConfigService.getCustomizeConfig({ queryType: '0' }).then(config => {
      let customizeConfig = config['list']
      let nameSpace = customizeConfig['ott_channel_name_space']
      let options = {
        params: {
          SID: 'playbilldetail7',
          DEVICE: 'PC',
          DID: session.get('uuid_cookie')
        }
      }
      // get data from interface
      return getPlaybillDetail({
        playbillID: playbillID,
        isReturnAllMedia: '1',
        channelNamespace: nameSpace,
        queryDynamicRecmContent: {
          recmScenarios: recmscenarios
        }
      }, options).then(resp => {
        let url = resp['playbillDetail'] && resp['playbillDetail'].picture && resp['playbillDetail'].picture.posters &&
                    this.pictureService.convertToSizeUrl(resp['playbillDetail'].picture.posters[0],
                      { minwidth: 443, minheight: 244, maxwidth: 610, maxheight: 344 })
        // define list of extend
        let list = this.getDetailList(resp, url)
        if (resp['playbillDetail'].playbillSeries !== undefined) {
          list['seasonNO'] = this.prefixZero(resp['playbillDetail'].playbillSeries.seasonNO, 2)
          list['sitcomNO'] = this.prefixZero(resp['playbillDetail'].playbillSeries.sitcomNO, 2)
          list['exist'] = true
        } else {
          list['exist'] = false
        }
        list['entrance'] = recmscenarios[0]['entrance']
        if (resp['playbillDetail']['reminder']) {
          list['reminder'] = resp['playbillDetail']['reminder']
        } else {
          list['reminder'] = '0'
        }
        return list
      })
    })
  }
  /**
     * get data of definition
     */
  getRecomend (recomendList, recmActionID, entrance) {
    // define data
    let data = _.map(recomendList, (list, index) => {
      let channelDtails = this.channelCacheService.getStaticChannelData().channelDetails
      let dynamicChannelData = this.channelCacheService.getDynamicChannelData().channelDynamaicProp
      list['channelInfos'] = _.find(channelDtails, (detail) => {
        return detail['ID'] === list['channelDetail']['ID']
      })
      _.find(dynamicChannelData, (detail) => {
        if (list['channelInfos']['ID'] === detail['ID']) {
          list['channelInfos']['channelNO'] = detail['channelNO']
          let nameSpace = session.get('ott_channel_name_space')
          let physicalChannelsDynamicProperty = _.find(detail['physicalChannelsDynamicProperties'], (property) => {
            return _.contains(property['channelNamespaces'], nameSpace)
          })
          list['channelInfos']['physicalChannelsDynamicProperty'] =
                        physicalChannelsDynamicProperty || detail['physicalChannelsDynamicProperties'][0]
        }
      })

      // get url
      let url = list['playbillLites'] && list['playbillLites'][0] &&
                list['playbillLites'][0].picture && list['playbillLites'][0].picture.posters &&
                this.pictureService.convertToSizeUrl(list['playbillLites'][0].picture.posters[0],
                  { minwidth: 230, minheight: 129, maxwidth: 260, maxheight: 146 })
      // get leftTimes
      let leftTimes = Math.ceil((new Date(Number(list['playbillLites'][0].endTime)).getTime() -
                new Date(Number(Date.now()['getTime']())).getTime()) / 60000)
      return {
        name: list['playbillLites'][0].name,
        channelId: list['playbillLites'][0].channelID,
        posterUrl: url,
        id: list['playbillLites'][0].ID,
        ID: list['playbillLites'][0].ID,
        endTime: list['playbillLites'][0].endTime,
        startTime: list['playbillLites'][0].startTime,
        idType: 'isChannel',
        floatflag: 'moreList',
        floatInfo: list['playbillLites'][0],
        playbillLites: list['playbillLites'][0],
        channelDetail: list['channelInfos'],
        channelInfo: list['channelInfos'],
        playbillInfo: list['playbillLites'][0],
        morePlaybillflag: 'morePlaybillList',
        recmActionID: recmActionID,
        entrance: entrance,
        leftTime: leftTimes,
        leftTimes: { min: leftTimes },
        len: Date.now()['getTime']() > Number(list['playbillLites'][0].startTime) &&
                    (Date.now()['getTime']() < Number(list['playbillLites'][0].endTime))
          ? (Date.now()['getTime']() - Number(list['playbillLites'][0].startTime)) / (Number(list['playbillLites'][0].endTime) -
                        Number(list['playbillLites'][0].startTime)) * 100 + '%' : '',
        isLen: (Date.now()['getTime']() > Number(list['playbillLites'][0].startTime)) &&
                    (Date.now()['getTime']() < Number(list['playbillLites'][0].endTime)) ? '1' : '0',
        startTimes: this.commonService.dealWithDateJson('D10', list['playbillLites'][0].startTime),
        channelName: list['channelDetail'] ? ' | ' + list['channelInfos']['name'] : ''
      }
    })
    // provide data to other component
    return data
  }
  /**
     * get date of jionting
     */
  prefixZero (str, len) {
    str = '' + (str || '')
    if (str.length >= len) {
      return str
    }
    let gapLen = len - str.length, i, arr = []
    for (i = 0; i < gapLen; i++) {
      arr[i] = '0'
    }
    return arr.join('') + str
  }
  /**
     * set time format
     */
  formatStr (str, index) {
    // variable definition
    let weekend
    let month
    let day
    let hour
    let minute
    let allWeekends = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']
    let arr = []
    // define time format
    if (str.length === 13) {
      let date = parseInt(str, 10)
      let datestr = new Date(date)
      month = (datestr.getMonth() + 1) < 10 ? '0' + (datestr.getMonth() + 1) : (datestr.getMonth() + 1)
      weekend = datestr.getDay()
      day = datestr.getDate() < 10 ? '0' + datestr.getDate() : datestr.getDate()
      hour = datestr.getHours() < 10 ? '0' + datestr.getHours() : datestr.getHours()
      minute = datestr.getMinutes() < 10 ? '0' + datestr.getMinutes() : datestr.getMinutes()
      weekend = allWeekends[weekend]
    } else {
      month = str.slice(4, 6)
      day = str.slice(6, 8)
      hour = str.slice(8, 10)
      minute = str.slice(10, 12)
    }
    // keep data in arr
    arr.push(month)
    arr.push(weekend)
    arr.push(day)
    arr.push(hour)
    arr.push(minute)
    return arr[index]
  }
}
