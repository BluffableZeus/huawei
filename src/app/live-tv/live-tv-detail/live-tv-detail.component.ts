import * as _ from 'underscore'
import { Component, OnInit, OnDestroy } from '@angular/core'
import { ChannelDetailService } from './live-tv-detail.service'
import { EventService } from 'ng-epg-sdk/services'
import { Router } from '@angular/router'
import { ActivatedRoute } from '@angular/router'
import { session } from 'src/app/shared/services/session'
import { TVODAppService } from '../../component/playPopUpDialog/tVodApp.service'
import { PlaybillAppService } from '../../component/playPopUpDialog/playbillApp.service'
import { CommonService } from '../../core/common.service'
import { RecordDataService } from '../../component/recordDialog/recordData.service'
import { ReminderService } from '../../component/reminder/reminder.service'
import { TranslateService } from '@ngx-translate/core'
import { AccountLoginComponent } from '../../login/account-login'
import { ChannelCacheService } from '../../shared/services/channel-cache.service'
import { CustomConfigService } from '../../shared/services/custom-config.service'

@Component({
  selector: 'app-channel-detail',
  styleUrls: ['./live-tv-detail.component.scss'],
  templateUrl: './live-tv-detail.component.html'
})
export class ChannelDetailComponent implements OnInit, OnDestroy {
  public count: number
    /**
     * list of detail information
     */
  public detailList
  private recomendListTitle = ''
  private recomendlist
  private forword: boolean
  private showMore = false
  private id: string
  private endtime: any
  private locked: boolean
  private showSeason: boolean
  private playdata
  private recmActionID
  private entrance
  private hasChannel = true
  private isReminderCancel: boolean
  private canTvod: boolean
  private isCanRecord = false
  private isCancelRecord = false
  private recordDate
  private btnRecord
  private isShowReminder = false
  private maxOffsetHeight = 0
  private showHeight = false
  private getIntervalTip
  private playBtn
  private isCUTV = false
  private recProgramTimer: any
  private moreChannelList: {
    'dataList': Array<any>,
    'row': string,
    'clientW': number,
    'suspensionID': string
  }

  private moreChannelLists: {
    'dataList': Array<any>,
    'row': string,
    'clientW': number,
    'suspensionID': string
  }
    /**
     * name for modules that would be used in this module
     */
  constructor (
        private route: ActivatedRoute,
        private router: Router,
        private channelDetailService: ChannelDetailService,
        private tVODAppService: TVODAppService,
        private playbillAppService: PlaybillAppService,
        private commonService: CommonService,
        private translate: TranslateService,
        private recordDataService: RecordDataService,
        private reminderService: ReminderService,
        private channelCacheService: ChannelCacheService,
        private customConfigService: CustomConfigService
    ) { }
    /**
    * initialization
    */
  ngOnInit () {
    let self = this
    this.route.params.subscribe(params => {
      document.body.scrollTop = 0
      document.documentElement.scrollTop = 0
      this.id = params['id']
      this.endtime = params['endtime']
      if (params['recmActionID'] && params['entrance']) {
        this.recmActionID = params['recmActionID']
        this.entrance = params['entrance']
      }
      this.getDetailDate(this.id, this.endtime)
        // define the part of introduce
      this.showMore = false
      this.showHeight = false
    })
      // when initialization accept the events
    EventService.on('RECORD_ADD_SUCCESS', (data) => {
      if (data.id === this.id) {
        this.isCanRecord = false
        this.isCancelRecord = true
      }
    })
    EventService.on('FIRST_TIME_LOGIN', () => {
      this.getDetailDate(this.id, this.endtime)
    })
    EventService.on('ScreenSize', function () {
      self.showMoreHeight()
    })
    EventService.on('CHANGELANGUAGE', function () {
      self.getDetailDate(self.id, self.endtime)
    })
  }
    /**
     * when exit the component, clear all the timer and events
     */
  ngOnDestroy () {
    clearInterval(this.recProgramTimer)
    if (this.getIntervalTip) {
      clearInterval(this.getIntervalTip)
    }
  }
    /**
     * set the style of introduce
     */
  clampIntroduce (introduce, maxheight, clamp) {
    let self = this
    if (this.maxOffsetHeight <= maxheight) {
      this.showHeight = false
      this.isChangeArrow(this.showMore, this.showHeight)
      introduce['style']['height'] = 'auto'
      introduce['style']['-webkit-line-clamp'] = 'inherit'
    } else {
      self.showHeight = true
      this.isChangeArrow(this.showMore, this.showHeight)
      if (self.showMore) {
        introduce['style']['height'] = 'auto'
        introduce['style']['-webkit-line-clamp'] = 'inherit'
      } else if (!self.showMore) {
          introduce['style']['height'] = maxheight + 'px'
          introduce['style']['-webkit-line-clamp'] = clamp
        }
    }
  }
    /**
     * control the height of the introduce
     */
  showMoreHeight () {
    let self = this
    if (document.getElementById('showiconchannel')) {
      this.maxOffsetHeight = document.getElementById('showiconchannel').offsetHeight
    }
    let introduce
    let intOffsetHeight
    if (document.getElementById('introduce')) {
      introduce = document.getElementById('introduce')
    }
    if (introduce) {
      intOffsetHeight = introduce.offsetHeight || 0
      if (this.detailList['introduce'].length > 0 && (intOffsetHeight === 0)) {
      } else {
          // set the maxheight according to ScreenSize
        if (window.innerWidth > 1440 && window.innerWidth <= 1920) {
            self.clampIntroduce(introduce, 168, '7')
          } else if (window.innerWidth <= 1440) {
            self.clampIntroduce(introduce, 72, '3')
          }
      }
    }
  }
    /**
     * control the style of arrow
     */
  isChangeArrow (showMore, showHeight) {
    if (!showMore && showHeight) {
      if (document.getElementById('introduceMore')) {
        document.getElementById('introduceMore')['style']['display'] = 'block'
      }
    } else {
      if (document.getElementById('introduceMore')) {
        document.getElementById('introduceMore')['style']['display'] = 'none'
      }
    }
    if (showMore && showHeight) {
      if (document.getElementById('introduceLess')) {
        document.getElementById('introduceLess')['style']['display'] = 'block'
      }
    } else {
      if (document.getElementById('introduceLess')) {
        document.getElementById('introduceLess')['style']['display'] = 'none'
      }
    }
  }
    /**
    * call a interface
    * deal with the data come from the interface
    */
  getDetailDate (id, endtime) {
    this.channelDetailService.getDetail(id, endtime).then((resp) => {
      this.refeshStroage(resp['channelDetail'])
      this.detailList = resp
      let self = this
      clearInterval(this.getIntervalTip)
      this.getIntervalTip = setInterval(() => {
        if ((this.detailList['introduce'].length > 0) && document.getElementById('introduce')) {
            self.showMoreHeight()
            clearInterval(this.getIntervalTip)
          }
      }, 10)
        // get the type of channel
      let type = this.recordDataService.channelIsRecord(resp['channelDetail'], resp['playbillDetail'])
      if (type && type === 'livetv_record') {
        this.isCanRecord = true
        this.isCancelRecord = false
        this.btnRecord = 'livetv_record'
      } else if (type && type === 'cancel_record' || type && type === 'stop_recording') {
          this.isCanRecord = false
          this.isCancelRecord = true
        }
        // transmit record data
      this.recordDate = resp['playbillDetail']
        // is show reminder or cancel reminder
      this.getIsReminder(resp['playbillDetail'])
        // player's data
      this.getdata(resp)
        // is show lock
      self.showLock(resp)
        // is show play button or record button
      self.showButton()
      this.showSeason = resp['exist']
        // Similar recommendations
      if (resp['recomendList'] && resp['recomendList'].length !== 0) {
        this.recomendlist = this.channelDetailService
            .getRecomend(resp['recomendList'], resp['recmActionID'], resp['entrance'])
        this.moreChannelLists = {
            'dataList': this.recomendlist,
            'row': '2',
            'clientW': document.body.clientWidth,
            'suspensionID': 'moreData'
          }
        this.moreChannelList = this.moreChannelLists
        this.count = this.moreChannelList['dataList'].length
        this.recomendListTitle = this.translate.instant('the_viewers_also_watched') + ' ' + '(' + this.count + ')'
        this.hasChannel = true
        this.refreshRecProgram()
      } else {
        this.hasChannel = false
        this.count = 0
        this.recomendListTitle = this.translate.instant('the_viewers_also_watched')
      }
      this.judgePlayOrSubscribe(resp)
    })
  }

    /**
     * show lock icon
     */
  showLock (resp) {
    if ((Number(session.get('PROFILERATING_ID')) < Number(resp['rating']) ||
            Number(session.get('PROFILERATING_ID')) < Number(resp['channelDetailrating']))) {
      this.locked = true
    } else if (session.get('PROFILERATING_ID') === undefined) {
      this.locked = false
    } else {
      this.locked = false
    }
  }

    /**
     * show play button or record button
     */
  showButton () {
    if (Date.now()['getTime']() < this.endtime) {
      this.forword = false
    } else if (Date.now()['getTime']() > this.endtime) {
      this.forword = true
    }
  }

    /**
     * judge the type of video
     */
  judgePlayOrSubscribe (resp) {
      // film detail
    let mediaData = resp['playbillDetail']['channelDetail']['physicalChannels'][0] ||
                        resp['playbillDetail']['channelDetail']['physicalChannelsDynamicProperties'][0]
    let hasOutTime = false
      // get film length
    let length = Number(Date.now()['getTime']()) - this.getNumber(mediaData['cutvCR']['length'])
    if (Number(resp['startTimeCompare']) > length) {
      hasOutTime = true
    } else {
      hasOutTime = false
    }
      // is TVOD
    this.getIscutv(resp['isCUTV'])
      // define canPlay
    let canPlay = mediaData['cutvCR']['enable'] === '1' && mediaData['cutvCR']['isContentValid'] === '1' &&
            mediaData['cutvCR']['isSubscribed'] === '1' && mediaData['cutvCR']['isValid'] === '1' && this.isCUTV && hasOutTime
    this.setBtnName(canPlay, mediaData, hasOutTime)
  }
    /**
     * according variable to set button
     */
  setBtnName (canPlay, mediaData, hasOutTime) {
    let canBuy = (mediaData['cutvCR']['enable'] === '1' && mediaData['cutvCR']['isContentValid'] === '1') &&
            (mediaData['cutvCR']['isSubscribed'] !== '1' || mediaData['cutvCR']['isValid'] !== '1') && this.isCUTV && hasOutTime
    if (canPlay) {
      this.playBtn = 'play'
      this.canTvod = true
    } else if (canBuy) {
      this.playBtn = 'buy'
      this.canTvod = true
    } else {
      this.canTvod = false
    }
  }
    /**
     * get time length of the film
     */
  getNumber (length) {
    return Number(length) * 1000
  }

    /**
     * refresh storage
     */
  refeshStroage (channelDetail) {
    let self = this
      // get the sessionData
    let sessionStaticData = self.channelCacheService.getStaticChannelData()
    let sessionDynamicData = self.channelCacheService.getDynamicChannelData()
      /**
         * traverse the staticData
         */
    _.each(sessionStaticData['channelDetails'], (list, index) => {
      if (list['ID'] === channelDetail['ID']) {
        list['isLocked'] = channelDetail['isLocked']
        if (channelDetail['favorite']) {
            list['favorite'] = {}
          } else {
            list['favorite'] = undefined
          }
      }
    })
      /**
         * traverse the dynamicData
         */
    _.each(sessionDynamicData['channelDynamaicProp'], (list, index) => {
      if (list['ID'] === channelDetail['ID']) {
        list['isLocked'] = channelDetail['isLocked']
        if (channelDetail['favorite']) {
            list['favorite'] = {}
          } else {
            list['favorite'] = undefined
          }
      }
    })
    let userType
    if (Cookies.getJSON('IS_PROFILE_LOGIN')) {
      userType = Cookies.getJSON('USER_ID')
    } else {
      userType = session.get('Guest')
    }
    session.put(userType + '_Static_Version_Data', sessionStaticData, true)
    session.put(userType + '_Dynamic_Version_Data', sessionDynamicData, true)
  }
    /**
     * click to show more introduce
     */
  increaseMore () {
    this.showMore = true
    this.showMoreHeight()
  }
    /**
     * click to show less introduce
     */
  discreaseLess () {
    this.showMore = false
    this.showMoreHeight()
  }
    /**
     * click poster to skip
     */
  vodDtail (event) {
    if (event[2] === '0') {
        // skip to vodDtail
      this.router.navigate(['live-tv', 'live-tv-detail', event[0], event[1], event[3], event[4]])
    }
    if (event[2] === '1') {
      EventService.emit('OPEN_FULLSCREEN_LIVETVVIDEO', {})
      let video = document.querySelector('#videoContainer video')
        // judge the type of browser
      let ua = navigator.userAgent.toLowerCase()
      let isIe = !!ua.match(/msie/i) || !!ua.match(/rv:([\d.]+).*like gecko/)
      let isFirefox = !!ua.match(/firefox\/([\d.]+)/)
      let isEdge = !!ua.match(/edge\/([\d.]+)/)
        // use video according to browser
      if (isIe) {
        video = document.querySelector('#videoContainer')
        video['msRequestFullscreen']()
      } else if (isEdge) {
          video = document.querySelector('#videoContainer')
          video.webkitRequestFullScreen()
        } else if (isFirefox) {
          video = document.querySelector('#videoContainer')
          video['mozRequestFullScreen']()
        } else if (ua.match(/version\/([\d.]+).*safari/)) {
          video.requestFullscreen()
        } else {
          video.webkitRequestFullScreen()
        }
      event[1]['isFromDetail'] = true
      this.playbillAppService.playProgram(event[0], event[1])
      if (event[3] && event[4]) {
        EventService.emit('sendParameter', event[3], event[4])
      }
    }
  }
    /**
    * provide the function to get data from the interface
    */
  getdata (data) {
      // set variable to save data
    this.playdata = data
  }
    /**
     * play the video according to browser
     */
  goplay () {
    this.saveBtnValue()
    if (this.playdata['playbillDetail'].channelDetail.contentType === 'VIDEO_CHANNEL' || 'AUDIO_CHANNEL') {
      this.tVODAppService.playTvod(this.playdata['playbillDetail'], this.playdata['playbillDetail'].channelDetail)
      EventService.emit('OPEN_FULLSCREEN_LIVETVVIDEO', this.playdata)
        // get the video
      let video = document.querySelector('#videoContainer video')
      let ua = navigator.userAgent.toLowerCase()
      let isIe = !!ua.match(/msie/i) || !!ua.match(/rv:([\d.]+).*like gecko/)
      let isFirefox = !!ua.match(/firefox\/([\d.]+)/)
      let isEdge = !!ua.match(/edge\/([\d.]+)/)
      if (isIe) {
        video = document.querySelector('#videoContainer')
        video['msRequestFullscreen']()
      } else if (isEdge) {
          video = document.querySelector('#videoContainer')
          video.webkitRequestFullScreen()
        } else if (isFirefox) {
          video = document.querySelector('#videoContainer')
          video['mozRequestFullScreen']()
        } else if (ua.match(/version\/([\d.]+).*safari/)) {
          video.requestFullscreen()
        } else {
          video.webkitRequestFullScreen()
        }
      if (this.recmActionID && this.entrance) {
        EventService.emit('sendParameter', this.recmActionID, this.entrance)
      }
    }
  }
    /**
     * get the value of the button
     */
  saveBtnValue () {
    if (this.playBtn === 'buy') {
      session.put('NEED_TO_BUY', 'buy')
    } else {
      session.put('NEED_TO_BUY', 'play')
    }
  }
    /**
     * judge the tvod can play
     */
  getIscutv (isCUTV) {
    if (isCUTV === '0') {
      this.isCUTV = false
    } else {
      this.isCUTV = true
    }
  }
    /**
     * is show reminder or cancel reminder
     */
  getIsReminder (playbill) {
    let state = this.reminderService.isSupportReminder(playbill)
    if (state) {
      this.isShowReminder = true
      if (state === 'ADD') {
        this.isReminderCancel = false
      } else {
        this.isReminderCancel = true
      }
    } else {
      this.isShowReminder = false
    }
  }
    /**
     * set reminder
     */
  addReminder (data) {
    if (Cookies.getJSON('IS_GUEST_LOGIN')) {
        // popup weaktip
      this.commonService.openDialog(AccountLoginComponent)
      return
    }
    let req = {
      contentID: this.id,
      contentType: 'PROGRAM'
    }
    if (this.entrance) {
      req['entrance'] = this.entrance
    }
    if (this.recmActionID) {
      req['recmActionID'] = this.recmActionID
    }
      /**
        * call a interface
        * deal with the data come from the createReminder
        */
    this.reminderService.addReminder({ reminders: [req] }).then((resp) => {
        // control reminder button
      this.isReminderCancel = true
      EventService.emit('ADD_REMINDER', data)
    })
  }
    /**
     * cancel reminder
     */
  removeReminder (data) {
    if (Cookies.getJSON('IS_GUEST_LOGIN')) {
      this.commonService.openDialog(AccountLoginComponent)
      return
    }
      /**
        * call a interface
        * deal with the data come from the deleteReminder
        */
    this.reminderService.removeReminder({
      contentIDs: [this.id],
      contentTypes: ['PROGRAM']
    }).then((resp) => {
      this.isReminderCancel = false
      EventService.emit('REMOVE_REMINDER', data)
    })
  }
    /**
     * create record
     */
  goToRecord (type) {
    if (Cookies.getJSON('IS_GUEST_LOGIN')) {
      EventService.emit('CLOSED_LOGIN_DIALOG')
      this.commonService.openDialog(AccountLoginComponent)
      return
    }
      // get the recordData
    if (type === 'livetv_record') {
      let recordData = this.recordDate
      recordData['isNow'] = false
      EventService.emit('LIVETV_RECORD', recordData)
    }
  }
    /**
     * cancel record
     */
  goToCancelRecord () {
      /**
        * call a interface
        * deal with the data come from the cancelPVRByPlaybillID
        */
    this.recordDataService.cancelRecord([this.recordDate['ID']]).then(function (resp) {
      this.isCanRecord = true
      this.btnRecord = 'livetv_record'
      this.isCancelRecord = false
    }.bind(this))
  }
    /**
     * control the height of introduce
     */
  showWidth () {
    let styles = document.querySelector('.introduce')['style']['-webkit-box-direction']
    if (styles === 'reverse') {
      document.querySelector('.introduce')['style']['-webkit-box-direction'] = 'normal'
    } else {
      document.querySelector('.introduce')['style']['-webkit-box-direction'] = 'reverse'
    }
  }
    /**
     * refresh the progress of the recommand programs
     */
  refreshRecProgram () {
    this.recProgramTimer = setInterval(() => {
      this.recomendlist = this.refreshProgress(this.recomendlist)
    }, 1000 * 60)
  }

    /**
     * refresh the progress of programs
     */
  refreshProgress (list) {
      // let programsList = list;
    for (let i = 0; i < list.length; i++) {
      if (!list[i]['endTime']) {
        continue
      }
      let length = list[i]['endTime'] - list[i]['startTime']
      let curLength = Date.now()['getTime']() - list[i]['startTime']
        // if the program is end and not refresh the next one,the  progress show full
      let leftTime = Math.ceil((new Date(Number(list[i]['endTime'])).getTime() -
                new Date(Number(Date.now()['getTime']())).getTime()) / 60000)
        // if the leftTime less then 0,leftTime is 0
      if (leftTime < 0) {
        leftTime = 0
      }
      if (curLength > length) {
        list[i]['len'] = '100%'
        list[i]['hasProcess'] = false
        list[i]['isLen'] = this.getIsLen(list, i)
      } else if (curLength < 0) {
          list[i]['len'] = '0%'
          list[i]['hasProcess'] = false
          list[i]['isLen'] = this.getIsLen(list, i)
        } else {
          list[i]['len'] = curLength >= length ? '100%' : Number(curLength) / Number(length) * 100 + '%'
          list[i]['hasProcess'] = true
          list[i]['isLen'] = this.getIsLen(list, i)
        }
      list[i]['leftTimes'] = { min: leftTime }
    }
    return list
  }

    /**
     * get data of isLen
     */
  getIsLen (list, i) {
    let isLen = (Date.now()['getTime']() > Number(list[i].startTime)) &&
            (Date.now()['getTime']() < Number(list[i].endTime)) ? '1' : '0'
    return isLen
  }
}
