import { NgModule } from '@angular/core'
import { RouterModule, Route } from '@angular/router'
import { AuthGuard } from '../shared/services/auth.guard'

import { LiveTVComponent } from './live-tv-home/live-tv.component'
import { TvGuideComponent } from './live-tv-guide/tv-guide.component'
import { ChannelDetailComponent } from './live-tv-detail/live-tv-detail.component'
import { LiveTvPlayerComponent } from './live-tv-play/livetv-player.component'
import { HotGenresMoreComponent } from './live-tv-home/hot-genres/hot-genres-more/hot-genres-more.component'
import { FutureChannelMoreComponent } from './live-tv-home/future-channel/future-channel-more/future-channel-more.component'

export const livetvRoutes: Array<Route> = [
  { path: '', component: LiveTVComponent, canActivate: [AuthGuard] },
  { path: 'guide', component: TvGuideComponent, canActivate: [AuthGuard] },
  { path: 'live-tv-detail/:id/:endtime', component: ChannelDetailComponent, canActivate: [AuthGuard] },
  { path: 'live-tv-detail/:id/:endtime/:recmActionID/:entrance', component: ChannelDetailComponent, canActivate: [AuthGuard] },
  { path: 'live-tv-player', component: LiveTvPlayerComponent, canActivate: [AuthGuard] },
  { path: ':name/:subjectID', component: HotGenresMoreComponent, canActivate: [AuthGuard] },
  { path: 'moreProgram', component: FutureChannelMoreComponent, canActivate: [AuthGuard] }
]

@NgModule({
  imports: [
    RouterModule.forChild(livetvRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class LiveTVRoutingModule { }
