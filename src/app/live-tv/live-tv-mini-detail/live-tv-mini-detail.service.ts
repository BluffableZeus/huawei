import * as _ from 'underscore'
import { Injectable } from '@angular/core'
import { PictureService } from 'ng-epg-ui/services/picture.service'
import { getPlaybillDetail } from 'ng-epg-sdk/vsp'
import { CommonService } from '../../core/common.service'
import { DateUtils } from 'ng-epg-sdk/utils'
import { ChannelCacheService } from '../../shared/services/channel-cache.service'

@Injectable()
export class MiniDetailService {
  /**
     * name for modules that would be used in this module
     */
  constructor (
        private pictureService: PictureService,
        private commonService: CommonService,
        private channelCacheService: ChannelCacheService
  ) { }
  /**
     * get data of miniDetail
     */
  getRecomend (recomendList, recmActionID?, entrance?) {
    return _.map(recomendList, (list, index) => {
      // a variable to control stylesheet
      let len
      // a variable to control element
      let hasProcess
      // get url of posters
      let url = list['playbillLites'] && list['playbillLites'][0] &&
                list['playbillLites'][0].picture && list['playbillLites'][0].picture.posters &&
                this.pictureService.convertToSizeUrl(list['playbillLites'][0].picture.posters[0],
                  { minwidth: 184, minheight: 104, maxwidth: 184, maxheight: 104 })
      // get playbillDetail
      let playbill = list['playbillLites'][0]
      // get length of playbill time
      let playbilllTime = playbill['endTime'] - playbill['startTime']
      // get currentDuration
      let currentDuration = Date.now()['getTime']() - playbill['startTime']
      // get left time
      let leftTimes = Math.ceil((new Date(Number(playbill['endTime'])).getTime() -
                new Date(Number(Date.now()['getTime']())).getTime()) / 60000)
      if (currentDuration > playbilllTime) {
        len = '100%'
        hasProcess = false
      } else if (currentDuration < 0) {
        len = '0%'
        hasProcess = false
      } else {
        len = Number(currentDuration / playbilllTime * 100) + '%'
        hasProcess = true
      }
      let staticChannelData = this.channelCacheService.getStaticChannelData().channelDetails
      let channelNames = _.filter(staticChannelData, function (channelItem) {
        return channelItem['ID'] === list['channelDetail'].ID
      })
      // set object to save variables
      return {
        name: list['playbillLites'][0].name,
        posterUrl: url || 'assets/img/default/livetv_mini.png',
        ID: list['playbillLites'][0].ID,
        endTime: list['playbillLites'][0].endTime,
        len: len,
        channelID: playbill['channelID'],
        hasProcess: hasProcess,
        recmActionID: recmActionID,
        entrance: entrance,
        leftTime: leftTimes,
        leftTimes: { min: leftTimes },
        startTimes: DateUtils.format(parseInt(list['playbillLites'][0]['startTime'], 10), 'HH:mm'),
        channelName: list['channelDetail']['name'] ? ' | ' + list['channelDetail']['name'] : ' | ' +
                    (channelNames && channelNames[0] && channelNames[0]['name']),
        startTime: playbill['startTime']
      }
    })
  }
  /**
     * refresh the playbill data
     */
  refreshProgress (list) {
    let programsList = list
    for (let i = 0; i < programsList.length; i++) {
      if (!programsList[i]['endTime']) {
        continue
      }
      // get total length of playbill time
      let length = programsList[i]['endTime'] - programsList[i]['startTime']
      // get broadcast time
      let curLength = Date.now()['getTime']() - programsList[i]['startTime']
      // if the program is end and not refresh the next one,the  progress show full
      let leftTime = Math.ceil((new Date(Number(programsList[i]['endTime'])).getTime() -
                new Date(Number(Date.now()['getTime']())).getTime()) / 60000)
      // if the leftTime less then 0,leftTime is 0
      if (leftTime < 0) {
        leftTime = 0
      }
      // if the curLength equal to length,leftTime is 0
      if (curLength > length) {
        programsList[i]['len'] = '100%'
        programsList[i]['hasProcess'] = false
        // if the curLength equal to 0,leftTime is 100%
      } else if (curLength < 0) {
        programsList[i]['len'] = '0%'
        programsList[i]['hasProcess'] = false
      } else {
        // normal length
        programsList[i]['len'] = curLength >= length ? '100%' : Number(curLength) / Number(length) * 100 + '%'
        programsList[i]['hasProcess'] = true
      }
      programsList[i]['leftTimes'] = { min: leftTime }
    }
    return programsList
  }
  /**
    * call a interface
    * deal with the data come from the interface
    * get the data from getPlaybillDetail
    */
  getDetail (playbillID, endtimes) {
    let recmscenarios
    // a variable to control button
    let isFuture
    if (endtimes > Date.now()['getTime']()) {
      // the number is count to show
      recmscenarios = [{
        count: '12',
        offset: '0',
        contentType: 'PROGRAM',
        contentID: playbillID,
        entrance: 'Program_Similar_Recommendations',
        businessType: 'BTV'
      }]
      isFuture = true
    } else if (endtimes < Date.now()['getTime']()) {
      recmscenarios = [{
        count: '12',
        offset: '0',
        contentType: 'PROGRAM',
        contentID: playbillID,
        entrance: 'CatchupTV_Similar_Recommendations',
        businessType: 'CUTV'
      }]
      isFuture = false
    }
    let dynamicData = this.channelCacheService.getDynamicChannelData().channelDynamaicProp
    let staticData = this.channelCacheService.getStaticChannelData().channelDetails
    // call a interface
    return getPlaybillDetail({
      playbillID: playbillID,
      queryDynamicRecmContent: {
        recmScenarios: recmscenarios
      },
      isReturnAllMedia: '1'
    }).then(resp => {
      // receive posterUrl
      let dynamicDetail = _.find(dynamicData, (detail) => {
        return detail['ID'] === resp['playbillDetail']['channelDetail']['ID']
      })
      let staticDetail = _.find(staticData, (detail) => {
        return detail['ID'] === resp['playbillDetail']['channelDetail']['ID']
      })
      let url = this.pictureService.convertToSizeUrl(staticDetail && staticDetail['picture'] &&
             staticDetail['picture']['icons'] && staticDetail['picture']['icons'][0],
      { minwidth: 40, minheight: 40, maxwidth: 40, maxheight: 40 })
      // save a object
      return {
        channelNo: dynamicDetail && dynamicDetail['channelNO'],
        logo: url,
        name: resp.playbillDetail.name,
        channel: staticDetail['name'],
        isLock: dynamicDetail && dynamicDetail['isLocked'],
        isFav: dynamicDetail && dynamicDetail['favorite'],
        genre: resp.playbillDetail.genres,
        weekTime: DateUtils.format(Number(resp.playbillDetail.startTime), 'ddd'),
        leftTime: this.commonService.dealWithDateJson('D05', Number(resp.playbillDetail.startTime)),
        startTimeorg: resp.playbillDetail.startTime,
        startTime: this.commonService.dealWithDateJson('D10', resp.playbillDetail.startTime),
        startTimeCompare: resp['playbillDetail'].startTime,
        endTime: this.commonService.dealWithDateJson('D10', resp.playbillDetail.endTime),
        Series: resp.playbillDetail.playbillSeries,
        introduce: resp.playbillDetail.introduce,
        rating: resp.playbillDetail.rating && resp.playbillDetail.rating.name,
        playbillDetail: resp.playbillDetail,
        recomendList: resp['recmContents'] && resp['recmContents'][0] && resp['recmContents'][0]['recmPrograms'],
        isCUTV: dynamicDetail && dynamicDetail['physicalChannelsDynamicProperties'][0].cutvCR.enable,
        recmContents: resp['recmContents'] && resp['recmContents'][0] && resp.recmContents[0].recmPrograms &&
                resp.recmContents[0].recmPrograms.length,
        isFuture: isFuture
      }
    })
  }

  prefixZero (str, len) {
    str = '' + (str || '')
    if (str.length >= len) {
      return str
    }
    let gapLen = len - str.length, i, arr = []
    for (i = 0; i < gapLen; i++) {
      arr[i] = '0'
    }
    return arr.join('') + str
  }
}
