import * as _ from 'underscore'
import { Component, OnInit, Input, OnDestroy } from '@angular/core'
import { EventService } from 'ng-epg-sdk/services'
import { Router } from '@angular/router'
import { ActivatedRoute } from '@angular/router'
import { MiniDetailService } from './live-tv-mini-detail.service'
import { TranslateService } from '@ngx-translate/core'
import { DirectionScroll } from 'ng-epg-ui/webtv-components/scroll/directionScroll.component'
import { CommonService } from '../../core/common.service'
import { session } from 'src/app/shared/services/session'
import { TVODAppService } from '../../component/playPopUpDialog/tVodApp.service'
import { RecordDataService } from '../../component/recordDialog/recordData.service'
import { AccountLoginComponent } from '../../login/account-login'
import { ReminderService } from '../../component/reminder/reminder.service'
import { ChannelCacheService } from '../../shared/services/channel-cache.service'

@Component({
  selector: 'app-channel-mini-detail',
  styleUrls: ['./live-tv-mini-detail.component.scss'],
  templateUrl: './live-tv-mini-detail.component.html'
})

export class MiniDetailComponent implements OnInit, OnDestroy {
    // declare variables.
    // keep the channel number.
  public channelNo = ''
    // keep the channel logo.
  public logo = ''
    // this channel logo is not display,is used to get when channel logo's src path or pic doesn't exist
  public hiddenLogo = ''
    // keep the programmme name.
  public name = ''
    // keep the channel name.
  public channel = ''
    // '0' is unlocked,'1' is locked.
  public isLock = ''
    // '0' is not favorite,'1' is favorite.
  public isFav = ''
    // keep the channel genre.
  public genre = ''
    // keep the week of the programmme.
  public weekTime = ''
    // keep the date of the programmme.
  public leftTime = ''
    // keep the start time of the programmme.
  public startTime = ''
    // keep the end time of the programmme.
  public endTime = ''
    // keep the series.
  public series = ''
    // keep the introduce.
  public introduce = ''
    // keep the rating.
  public rating = ''
    // keep the title of the recomend list.
  public recomendListTitle = ''
    // true is showing all the introduce,false is showing only 3 rows introduce.
  public showMore = false
    // true is showing the mini detail page,false is hiding the mini detail page.
  public showDetail = true
    // keep the text of play buttom.
  public btnName = ''
    // keep the text of remind buttom.
  public btnNameRemind = ''
    // keep the text of record buttom.
  public btnNameRecord = ''
    // keep the channel id.
  public contentID = ''
    // true is the programme is in future,false is the programme is in the past.
  public isFuture = false
    // true is the programme is supported to back to see,false is the programme is not supported.
  public isCUTV = false
    // 0 is the user is logined.
  public profileType
    // keep the channel detail.
  public triggerAddFavData: Object
    // keep the channel detail and the programme detail.
  public _data: Object
    // true is the information belongs to the Recmlist,false is the information belongs to the TVGuide.
  public isRecmInfo = false
    // true is show the record buttom,false is hide the record buttom.
  public isRecord = false
    // keep the icons of the record.
  public recordsImgStyle = {}
    // keep the icons of the remind.
  public remindImgStyle = {}
    // true is show the scroll bar,false is hide the scroll bar.
  public showScroll = true
    // keep the progamme id.
  public id = ''
    // true is show the remind buttom,false is hide the remind buttom.
  public isShowReminder = false
    // the timer for setting the arrow of introduce.
  public getIntervalTip
    //  the height of introduce content.
  public maxOffsetHeight
    // true is show the arrow of introduce,false is hide the arrow of introduce.
  public showHeight = false
    // keep the the scroll bar's height according to the window size.
  public scrollHeight
    // get playbill data
  public isFirefox = false
  public canTvod = false
  public onerror = null
  public recomendlist: any = {}
  private recProgramTimer: any
    // control miniDetail
  public ctrlHomeMini
    /**
     * get dynamic data of minidata
     */
  @Input() set minidata (data: Object) {
      // emit data of reception
    EventService.emit('showPointdata', data)
    this._data = data[0]
    this.triggerAddFavData = data[1]
    this.showDetail = true
      // judge the record's status
    let type = this.recordDataService.channelIsRecord(this._data['playbillDetail']['channelDetail'] ||
            this.triggerAddFavData['channelDetail'] || this.triggerAddFavData['channelInfos'],
      this._data['playbillDetail'])
    if (type) {
      this.isRecord = true
      this.btnNameRecord = type
      if (this.btnNameRecord === 'livetv_record') {
          // has not been recorded.
        this.recordsImgStyle = {
            background: 'url(assets/img/14/record_14.png)'
          }
      } else {
          // has been recorded.
        this.recordsImgStyle = {
            background: 'url(assets/img/14/stoprecording_14.png)'
          }
      }
    }
      // judge the reminder's status
    this.id = this._data['playbillDetail']['ID']
    if (this._data['playbillDetail']['reminder']) {
        // has been remindered.
      this.btnNameRemind = 'cancel_reminder'
      this.remindImgStyle = {
        background: 'url(assets/img/14/cancelreminder_14.png)'
      }
    } else {
        // has not been remindered.
      this.btnNameRemind = 'set_reminder'
      this.remindImgStyle = {
        background: 'url(assets/img/14/reminder_14.png)'
      }
    }
  }
    /**
     * name for modules that would be used in this module
     */
  constructor (
        // private constructor
        private directionScroll: DirectionScroll,
        private route: ActivatedRoute,
        private router: Router,
        private miniDetailService: MiniDetailService,
        private translate: TranslateService,
        private commonService: CommonService,
        private tVODAppService: TVODAppService,
        private recordDataService: RecordDataService,
        private reminderService: ReminderService,
        private channelCacheService: ChannelCacheService
    ) { }
    /**
     * deal with variables
     */
  minidetailTwice (seasonNO, sitcomNO) {
    this.series = this._data['Series'] && this._data['Series'].seasonNO && this._data['Series'].seasonNO && seasonNO
        ? this.translate.instant('vod_season', { num: seasonNO }) : ''
    this.setSeries(sitcomNO)
    this.introduce = this._data['introduce']
    this.rating = this._data && this._data['rating'] ? this._data['rating'] : ''
    this.contentID = this._data['playbillDetail'].channelDetail.ID
    let recmContents
    recmContents = this._data['recmContents'] ? this._data['recmContents'] : '0'
    if (recmContents !== '0') {
      this.recomendListTitle = this.translate.instant('the_viewers_also_watched') + ' ' + '(' + recmContents + ')'
    } else {
      this.recomendListTitle = this.translate.instant('the_viewers_also_watched')
    }
  }
    /**
     * get variables of series
     */
  setSeries (sitcomNO) {
    if (this.series !== '') {
      this.series = this._data['Series'] && this._data['Series'].sitcomNO && this._data['Series'].sitcomNO && sitcomNO ? this.series +
                ', ' + this.translate.instant('episode', { num: sitcomNO }) + ' ' : this.series + ''
    } else {
      this.series = this._data['Series'] && this._data['Series'].sitcomNO && this._data['Series'].sitcomNO && sitcomNO ? this.series +
                this.translate.instant('episode', { num: sitcomNO }) + ' ' : this.series + ''
    }
  }
    /**
     * set value of variables
     */
  minidetail () {
      // get type of login
    this.profileType = Cookies.get('PROFILE_TYPE')
    this.channelNo = this._data['channelNo'] || this._data['playbillDetail']['channelDetail']['ID']
      // get stylesheet of logo
    this.logo = 'url(' + this._data['logo'] + ') 0% 0% / cover'
    this.hiddenLogo = this._data['logo']
    this.name = this._data['name']
    this.channel = this._data['channel']
    this.isLock = this._data['isLock'] ? this._data['isLock'] : '0'
    this.isFav = this._data['isFav'] ? this._data['isFav'] : '0'
    this.genre = this.getGenre()
    this.weekTime = this._data['weekTime']
    this.leftTime = this._data['leftTime']
    this.startTime = this._data['startTime']
    this.endTime = this._data['endTime']
    let seasonNO = ''
    let sitcomNO = ''
      //  if the seasonNO is one number,add zero ahead.
    if (this._data['Series'] && this._data['Series'].seasonNO && this._data['Series'].seasonNO < 10) {
      seasonNO = '0' + this._data['Series'].seasonNO
    }
      //  if the sitcomNO is one number,add zero ahead.
    if (this._data['Series'] && this._data['Series'].sitcomNO && this._data['Series'].sitcomNO < 10) {
      sitcomNO = '0' + this._data['Series'].sitcomNO
    }
    this.minidetailTwice(seasonNO, sitcomNO)
  }
    /**
     * get genre
     */
  getGenre () {
    let genre = (this._data['genre'] && this._data['genre'].length) > 0 ? ' | ' + this._data['genre'][0]['genreName'] : ''
    return genre
  }
    /**
    * initialization
    */
  ngOnInit () {
      // deal with the playbill data.
    let self = this
    this.minidetail()
    this.isFuture = this._data['isFuture']
    if (this._data['isCUTV'] === '0') {
      this.isCUTV = false
    } else {
      this.isCUTV = true
    }
      // get the recomend list.
    this['recomendlist'] = this.miniDetailService.getRecomend(this._data['recomendList'])
    this.refreshRecProgram()
      // set the scroll hight according to the window size
    if (document.getElementById('channel-list')) {
      this.scrollHeight = document.getElementById('channel-list')['offsetHeight'] - 80 + 'px'
    }
      // reset the arrowhead of the introduce
    if (document.getElementById('introduce')) {
      let introduce = document.getElementById('introduce')
      introduce['style']['height'] = '63px'
      introduce['style']['-webkit-line-clamp'] = '3'
      this.showMore = false
      this.showScroll = false
    }
      // reset the scroll postion
    if (document.getElementById('scrollContent')) {
      document.getElementById('scrollContent')['style']['top'] = '0px'
    }
    if (document.getElementById('mini-livetv-scroll')) {
      document.getElementById('mini-livetv-scroll')['style']['top'] = '0px'
    }
    if (document.getElementById('mini-livetv-contentScroll')) {
      document.getElementById('mini-livetv-contentScroll')['style']['top'] = '0px'
    }
    self.pluseOnScroll()
      // monitor the event of  adding record successfull and change the record's status
    EventService.on('RECORD_ADD_SUCCESS', function (type) {
      self.btnNameRecord = 'cancel_record'
      self.recordsImgStyle = {
        background: 'url(assets/img/14/stoprecording_14.png)'
      }
    })
    this.getIsReminder(this._data['playbillDetail'])
      // deal the arrowhead of the introduce
    clearInterval(this.getIntervalTip)
    this.getIntervalTip = setInterval(() => {
      if ((this.introduce.length > 0) && document.getElementById('introduce')) {
        self.showMoreHeight()
        clearInterval(this.getIntervalTip)
      }
    }, 10)
    let ua = navigator.userAgent.toLowerCase()
    this.isFirefox = !!ua.match(/firefox\/([\d.]+)/)
    this.judgePlayOrSubscribe(this._data)

    EventService.on('REMOVE_REMINDER_FOR_TVGUIDE', (data) => {
      this._data['playbillDetail']['reminder'] = undefined
      this.btnNameRemind = 'set_reminder'
      this.remindImgStyle = {
        background: 'url(assets/img/14/reminder_14.png)'
      }
    })

    EventService.on('REMOVE_REMINDER_FOR_TVGUIDE_ALL', (data) => {
      if (data.length === 0) {
        return
      }
      for (let f = 0; f < data.length; f++) {
        if (this.id === data[f]) {
            this._data['playbillDetail']['reminder'] = undefined
            this.btnNameRemind = 'set_reminder'
            this.remindImgStyle = {
              background: 'url(assets/img/14/reminder_14.png)'
            }
          }
      }
    })
  }
    /**
     * deal with data to judge type
     */
  judgePlayOrSubscribe (resp) {
    let mediaData = resp['playbillDetail']['channelDetail']['physicalChannels'][0] ||
                        resp['playbillDetail']['channelDetail']['physicalChannelsDynamicProperties'][0]
    let hasOutTime = false
    let length = Number(Date.now()['getTime']()) - this.getNumber(mediaData['cutvCR']['length'])
    if (Number(resp['startTimeorg']) > length) {
      hasOutTime = true
    } else {
      hasOutTime = false
    }
    let canPlay = mediaData['cutvCR']['enable'] === '1' && mediaData['cutvCR']['isContentValid'] === '1' &&
            mediaData['cutvCR']['isSubscribed'] === '1' && mediaData['cutvCR']['isValid'] === '1' && this.isCUTV && hasOutTime
    this.setBtnName(canPlay, mediaData, hasOutTime)
  }
    /**
     * a method to set name of button
     */
  setBtnName (canPlay, mediaData, hasOutTime) {
    let canBuy = (mediaData['cutvCR']['enable'] === '1' && mediaData['cutvCR']['isContentValid'] === '1') &&
            (mediaData['cutvCR']['isSubscribed'] !== '1' || mediaData['cutvCR']['isValid'] !== '1') && this.isCUTV && hasOutTime
    if (canPlay) {
      this.btnName = 'play'
      this.canTvod = true
    } else if (canBuy) {
      this.btnName = 'buy'
      this.canTvod = true
    } else {
      this.canTvod = false
    }
  }
    /**
     * transform time into millisecond.
     */
  getNumber (length) {
    return Number(length) * 1000
  }
    /**
     * when exit the component, clear all the timer.
     */
  ngOnDestroy () {
      // clear the timer.
    clearInterval(this.getIntervalTip)
  }
    /**
     * when channel logo's src path or the pic does not exist,there show default picture
     */
  showDefaultLogo () {
    document.getElementsByClassName('detailLogo')[0]['style']['background'] =
            'url(assets/img/default/minidetail_channel_poster.png)'
  }
    /**
     * load logo
     */
  showDetailLogo () {
    return {
      'background': this.logo
    }
  }
    /**
     * control introduce height.
     */
  showMoreHeight () {
    let self = this
    if (document.getElementById('introduceShowIcon')) {
      this.maxOffsetHeight = document.getElementById('introduceShowIcon').offsetHeight
    }
    let introduce = document.getElementById('introduce')
    let intOffsetHeight
      // data protection
    if (introduce) {
      intOffsetHeight = introduce.offsetHeight || 0
      if (this.introduce.length > 0 && (intOffsetHeight === 0)) {
      } else {
        if (this.maxOffsetHeight <= 63) {
            // when the row number of introduce content is less then 3,the arrowhead disappear
            this.showHeight = false
          } else {
            // when the row number of introduce content is more then 3,the arrowhead display
            self.showHeight = true
            if (self.showMore) {
              // show all introduce content
              introduce['style']['height'] = 'auto'
              introduce['style']['-webkit-line-clamp'] = 'inherit'
            } else if (!self.showMore) {
              // show 3 rows of the introduce content
              introduce['style']['height'] = '63px'
              introduce['style']['-webkit-line-clamp'] = '3'
            }
          }
      }
    }
  }
    /**
     * roll the scroll event
     */
  pluseOnScroll () {
    let oConter, oUl, oScroll, oSpan
    let oBox = document.getElementById('miniDetail')
    oConter = document.getElementById('content-wrap')
    oUl = document.getElementById('scrollContent')
    oScroll = document.getElementById('mini-livetv-scroll')
    oSpan = oScroll.getElementsByTagName('span')[0]
      // judge the scroll show
    if (this['recomendlist'].length > 4 || oUl['offsetHeight'] > oConter['offsetHeight']) {
      this.showScroll = true
    } else if (oUl['offsetHeight'] <= oConter['offsetHeight']) {
      this.showScroll = false
    }
      // deal roll scroll
    this.directionScroll.setScroll(oBox, oConter, oUl, oScroll, oSpan, true)
  }
    /**
     * click the arrowhead and show all introduce
     */
  increaseMore () {
    this.showMore = true
    this.showMoreHeight()
    this.pluseOnScroll()
  }
    /**
     * click the arrowhead and show 3 rows introduce
     */
  discreaseLess () {
    this.showMore = false
    this.showMoreHeight()
      // initial the scroll position
    document.getElementById('scrollContent')['style']['top'] = '0px'
    document.getElementById('mini-livetv-scroll')['style']['top'] = '0px'
    document.getElementById('mini-livetv-contentScroll')['style']['top'] = '0px'
    this.pluseOnScroll()
  }
    /**
     * close mini detail page
     */
  closeMiniDetail () {
    this.showDetail = false
    _.delay(function () {
      EventService.emit('CLOSE_MINIDETAIL')
    }, 1100)
    clearInterval(this.recProgramTimer)
  }
    /**
     * click lock icon to add or delete lock
     */
  lockManger () {
    let self = this
    if (this.isRecmInfo) {
      if (self.isLock === '1') {
        this._data['playbillDetail']['channelDetail']['isLocked'] = '1'
      } else if (self.isLock === '0') {
          this._data['playbillDetail']['channelDetail']['isLocked'] = '0'
        }
      EventService.emit('LOCK_MANGER', this._data['playbillDetail'])
    } else {
      if (self.isLock === '1') {
        this.triggerAddFavData['channelInfos']['isLocked'] = '1'
      } else if (self.isLock === '0') {
          this.triggerAddFavData['channelInfos']['isLocked'] = '0'
        }
      EventService.emit('LOCK_MANGER', this.triggerAddFavData)
    }
    EventService.on('ADD_LOCK', function (data) {
      self.isLock = '1'
      EventService.emit('ADDLOCK_successful', {})
    })
    EventService.removeAllListeners(['REMOVE_LOCK'])
    EventService.on('REMOVE_LOCK', function (data) {
      self.isLock = '0'
      EventService.emit('UNLOCK_successful', {})
    })
  }
    /**
     * click favorite icon to add or delete favorite
     */
  favManger () {
    let self = this
    if (this.isRecmInfo) {
      if (self.isFav === '1') {
        this._data['playbillDetail']['channelDetail']['favorite'] = true
      } else if (self.isFav === '0') {
          this._data['playbillDetail']['channelDetail']['favorite'] = false
        }
      EventService.emit('FAVORITE_MANGER', this._data['playbillDetail'])
    } else {
      if (self.isFav === '1') {
        this.triggerAddFavData['channelInfos']['favorite'] = true
      } else if (self.isFav === '0') {
          this.triggerAddFavData['channelInfos']['favorite'] = false
        }
      EventService.emit('FAVORITE_MANGER', this.triggerAddFavData)
    }
    EventService.on('ADD_FAVORITE', function (data) {
      self.isFav = '1'
      EventService.emit('ADD_successful', {})
    })
    EventService.on('REMOVE_FAVORITE', function (data) {
      self.isFav = '0'
      EventService.emit('REMOVE_successful', {})
    })
  }
    /**
     * click the poster or playbill name and show the mini detail of the program
     */
  getRecmDetail (list) {
    if (list.hasProcess) {
      EventService.emit('PLAYCHANNEL', list.channelID)
    }
    this.miniDetailService.getDetail(list.ID, list.endTime).then((resp) => {
      this._data = resp
      this.ngOnInit()
      this.isFav = this._data['isFav'] ? this._data['isFav'] : '0'
      this.isRecmInfo = true
        // refresh storage
      this.refeshStroage(resp['channelDetail'] || resp['playbillDetail']['channelDetail'])
    })
  }
  refeshStroage (channelDetail) {
    let self = this
    let sessionStaticData = self.channelCacheService.getStaticChannelData()
    let sessionDynamicData = self.channelCacheService.getDynamicChannelData()
    _.each(sessionStaticData['channelDetails'], (list, index) => {
      if (list['ID'] === channelDetail['ID']) {
        list['isLocked'] = channelDetail['isLocked']
        if (channelDetail['favorite']) {
            list['favorite'] = {}
          } else {
            list['favorite'] = undefined
          }
      }
    })
    _.each(sessionDynamicData['channelDynamaicProp'], (list, index) => {
      if (list['ID'] === channelDetail['ID']) {
        list['isLocked'] = channelDetail['isLocked']
        if (channelDetail['favorite']) {
            list['favorite'] = {}
          } else {
            list['favorite'] = undefined
          }
      }
    })
    let userType
    if (Cookies.getJSON('IS_PROFILE_LOGIN')) {
      userType = Cookies.getJSON('USER_ID')
    } else {
      userType = session.get('Guest')
    }
    session.put(userType + '_Static_Version_Data', sessionStaticData, true)
    session.put(userType + '_Dynamic_Version_Data', sessionDynamicData, true)
  }
    /**
     * click play icon and play
     */
  goToPlay () {
    session.put('TVGuide', 'true')
    this.showDetail = false
    EventService.emit('CLOSE_MINIDETAIL')
    EventService.emit('OPEN_FULLSCREEN_LIVETVVIDEO', this._data)
    if (this.btnName === 'buy') {
      session.put('NEED_TO_BUY', 'buy')
      EventService.emit('openLiveDialog')
    } else {
      session.put('NEED_TO_BUY', 'play')
    }
    let video = document.querySelector('#videoContainer video')
    let ua = navigator.userAgent.toLowerCase()
    let isIe = !!ua.match(/msie/i) || !!ua.match(/rv:([\d.]+).*like gecko/)
    let isFirefox = !!ua.match(/firefox\/([\d.]+)/)
    let isEdge = !!ua.match(/edge\/([\d.]+)/)
    if (isIe) {
      video = document.querySelector('#videoContainer')
      video['msRequestFullscreen']()
    } else if (isEdge) {
      video = document.querySelector('#videoContainer')
      video.webkitRequestFullScreen()
    } else if (isFirefox) {
        video = document.querySelector('#videoContainer')
        video['mozRequestFullScreen']()
      } else if (ua.match(/version\/([\d.]+).*safari/)) {
        video.requestFullscreen()
      } else {
        video.webkitRequestFullScreen()
      }
    this.tVODAppService.playTvod(this._data['playbillDetail'], this._data['playbillDetail'].channelDetail)
  }
    /**
     * click record button
     */
  goToRecord (type) {
    if (Cookies.getJSON('IS_GUEST_LOGIN')) {
      EventService.emit('CLOSED_LOGIN_DIALOG')
      this.commonService.openDialog(AccountLoginComponent)
      return
    }
    if (type === 'livetv_record') {
        // open  record dialog
      let recordData = this._data['playbillDetail']
      recordData['isNow'] = false
      EventService.emit('LIVETV_RECORD', recordData)
    } else {
        // cancel record
      this.recordDataService.cancelRecord([this._data['playbillDetail']['ID']]).then(function (resp) {
        this.btnNameRecord = 'livetv_record'
        this.recordsImgStyle = {
            background: 'url(assets/img/14/record_14.png)'
          }
        EventService.emit('RECORD_CANCEL_SUCCESS')
      }.bind(this))
    }
  }
    /**
     * click reminder button
     */
  goToRemind (type) {
    if (Cookies.getJSON('IS_GUEST_LOGIN')) {
      this.commonService.openDialog(AccountLoginComponent)
      return
    }
    if (type === 'set_reminder') {
        // add reminder
      let req = {
        contentID: this.id,
        contentType: 'PROGRAM'
      }
      this.reminderService.addReminder({ reminders: [req] }).then((resp) => {
        this.btnNameRemind = 'cancel_reminder'
        this.remindImgStyle = {
            background: 'url(assets/img/14/cancelreminder_14.png)'
          }
        EventService.emit('ADD_REMINDER', this._data)
        EventService.emit('ADD_REMINDER_TVGUIDE')
      })
      return
    }
    if (type === 'cancel_reminder') {
        // cancel reminder
      this.reminderService.removeReminder({
        contentIDs: [this.id],
        contentTypes: ['PROGRAM']
      }).then((resp) => {
          this.btnNameRemind = 'set_reminder'
          this.remindImgStyle = {
            background: 'url(assets/img/14/reminder_14.png)'
          }
          EventService.emit('REMOVE_REMINDER', this._data)
          EventService.emit('REMOVE_REMINDER_TVGUIDE')
        })
    }
  }
    /**
     * reminder button status(reminder or cancel reminder)
     */
  getIsReminder (playbill) {
    let state = this.reminderService.isSupportReminder(playbill)
    if (state) {
      this.isShowReminder = true
      if (state === 'ADD') {
        this.btnNameRemind = 'set_reminder'
      } else {
        this.btnNameRemind = 'cancel_reminder'
      }
    } else {
      this.isShowReminder = false
    }
  }
    /**
     * deal with the the content location change.
     */
  showWidth () {
    if (!this.isFirefox) {
      let maxOffsetLeft = 952
      let minOffsetLeft = 552
      if (document.getElementById('miniDetail') && document.getElementById('miniDetail')['offsetLeft'] &&
                document.getElementById('miniDetail')['offsetLeft'] === maxOffsetLeft ||
                document.getElementById('miniDetail')['offsetLeft'] === minOffsetLeft) {
        let styles = document.querySelector('.introduce')['style']['-webkit-box-direction']
        if (styles === 'reverse') {
            document.querySelector('.introduce')['style']['-webkit-box-direction'] = 'normal'
          } else {
            document.querySelector('.introduce')['style']['-webkit-box-direction'] = 'reverse'
          }
      }
    }
  }
    /**
     * refresh the progress of the recommand programs
     */
  refreshRecProgram () {
    this.recProgramTimer = setInterval(() => {
      this.recomendlist = this.miniDetailService.refreshProgress(this.recomendlist)
    }, 1000 * 60)
    if (this.recomendlist.length <= 0) {
      this.ctrlHomeMini = true
    } else {
      this.ctrlHomeMini = false
    }
  }
}
