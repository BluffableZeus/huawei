import { Component } from '@angular/core'
import { DateStingComponent } from '../../component/dateString/dateString.component'

@Component({
  selector: 'app-tv-guide',
  styleUrls: ['./tv-guide.component.scss'],
  templateUrl: './tv-guide.component.html',
  providers: [DateStingComponent]
})

export class TvGuideComponent {
  public newDate = {}
  public nowToToday = {}

    /**
     * name for modules that would be used in this module
     */
  constructor (private dateStingComponent: DateStingComponent) {
  }

    /**
     * change the date
     */
  changeChannelList (event) {
    this.newDate = event
  }

    /**
     * change the date to today
     */
  changeDateToToday (event) {
    this.nowToToday = event
  }
}
