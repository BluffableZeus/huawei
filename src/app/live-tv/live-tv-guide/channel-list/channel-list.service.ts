import * as _ from 'underscore'
import { Injectable } from '@angular/core'
import { queryChannelSubjectList } from 'ng-epg-sdk/vsp'
import { DirectionScroll } from 'ng-epg-ui/webtv-components/scroll/directionScroll.component'
import { TransverseScroll } from 'ng-epg-ui/webtv-components/scroll/transverseScroll.component'
import { PictureService } from 'ng-epg-ui/services/picture.service'
import { EventService } from 'ng-epg-sdk/services'
import { DateUtils } from 'ng-epg-sdk/utils'
import { CommonService } from '../../../core/common.service'

@Injectable()
export class ChannelListService {
    /**
     * declare variables
     */
  private genresList = []

  private torrentDate: Number
    /**
     * name for modules that would be used in this module
     */
  constructor (
        private directionScroll: DirectionScroll,
        private transverseScroll: TransverseScroll,
        private commonService: CommonService,
        private pictureService: PictureService) {
  }
    /**
     * query the one-level genre of the channel
     */
  queryChanSubject (): Promise<any[]> {
    let allChannel = {
      ID: '-1',
      name: 'all_channel',
      type: 'VIDEO_CHANNEL'
    }
      // DWD-54 changes. If other points needed — uncomment/review code below
    return new Promise(resolve => resolve([allChannel]))

      // let self = this;
      // // call a interface
      // return queryChannelSubjectList({
      //     subjectID: '',
      //     contentTypes: ['CHANNEL'],
      //     count: '50',
      //     offset: '0'
      // }).then(resp => {
      //     // deal with the data come from the interface
      //     this.genresList = resp.subjects;
      //     let purchansed = {
      //         name : 'purchased',
      //         type : 'VIDEO_CHANNEL'
      //     };
      //     // judge whether login or not
      //     if (Cookies.getJSON('IS_PROFILE_LOGIN')) {
      //         this.genresList.unshift(purchansed);
      //         this.genresList.unshift(favorites);
      //     } else {
      //         this.genresList.unshift(favorites);
      //         EventService.on('LOGIN', function () {
      //             if (!_.contains(self.genresList, favorites) || !_.contains(self.genresList, purchansed)) {
      //                 self.genresList.splice(1, 0, favorites);
      //                 self.genresList.splice(2, 0, purchansed);
      //             }
      //         });
      //     }
      //     this.genresList.unshift(allChannel);
      //     return this.genresList;
      // });
  }

    /**
     * show 24 hours in one day
     */
  showAllTime (timeNewDate?: any) {
      // initialize an array to save time
    let tempTime = []
      // declare the time object
    let timeObj
      /**
         * i represents the index of half an hour
         * i from 0 to 48 means the time from 00:00 to 24:00
         */
    this.torrentDate = 0
    let dateCount = 23
    let evenFlag = 2
    let time = timeNewDate ? timeNewDate.getTime() : new Date(new Date()['toLocaleDateString']() + ' 00:00:00').getTime()
    for (let i = 0; i < 50; i++) {
        /**
             * satisfy the condition, circularly modify the time format
             * ensure that the number at hour position is double-digit
             * i is times of 2 means the time is on the hour
             */
      let timeCount = DateUtils.format(time + Math.floor(i / 2) * 60 * 60 * 1000, 'HH:mm')
      let timeCountStr = this.commonService.dealWithDateJson('D10', time + Math.floor(i / 2) * 60 * 60 * 1000)
      if (this.torrentDate === dateCount && parseInt(timeCount, 10) === 0) {
        break
      }
      this.torrentDate = parseInt(timeCount, 10)

      if (i < 20) {
        if (i % evenFlag === 0) {
            timeObj = {
              name: timeCountStr,
              id: i
            }
          } else {
            timeObj = {
              name: this.commonService.dealWithDateJson('D10', time + Math.floor(i / 2) * 60 * 60 * 1000 + 30 * 60000),
              id: i
            }
          }
      } else {
        if (i % evenFlag === 0) {
            timeObj = {
              name: timeCountStr,
              id: i
            }
          } else {
            timeObj = {
              name: this.commonService.dealWithDateJson('D10', time + Math.floor(i / 2) * 60 * 60 * 1000 + 30 * 60000),
              id: i
            }
          }
      }
      tempTime.push(timeObj)
    }
    return tempTime
  }

    /**
     * monitor mouse scroll event
     */
  pluseOnScroll () {
    let oConter, oUl, oScroll, oSpan
    let oBox = document.getElementById('channel-list')
    oConter = document.getElementById('channel-content')
    oUl = document.getElementById('conter')
    oScroll = document.getElementById('scroll')
    oSpan = oScroll.getElementsByTagName('span')[0]
      // use the packaged function from DirectionScroll module
    this.directionScroll.setScroll(oBox, oConter, oUl, oScroll, oSpan, false, 'isToBottom')
  }

  transverseOnScroll () {
    let oConter, oUl, oScroll, oSpan
    let oBox = document.getElementById('conter')
    oConter = document.getElementById('channelRightContent')
    oUl = document.getElementById('channelPlaybills')
    oScroll = document.getElementById('transverseScroll')
      // protect the data
    if (oScroll) {
      oSpan = oScroll.getElementsByTagName('span')[0]
    }
    let allDay = document.getElementById('allDay')
      // use the packaged function from TransverseScroll module
    this.transverseScroll.setScroll(oBox, oConter, oUl, oScroll, oSpan, allDay)
  }

  genreOnScroll () {
    let oConter, oUl, oScroll, oSpan
    let oBox = document.getElementById('channel-list')
    oConter = document.getElementById('genresConter')
    oUl = document.getElementById('genresList')
    oScroll = document.getElementById('genreScroll')
      // protect the data
    if (oScroll) {
      oSpan = oScroll.getElementsByTagName('span')[0]
    }
      // use the packaged function from DirectionScroll module
    this.directionScroll.setScroll(oBox, oConter, oUl, oScroll, oSpan)
  }
}
