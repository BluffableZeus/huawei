import * as _ from 'underscore'
import { Component, Input, Output, EventEmitter, ElementRef, ViewChild, OnInit, OnDestroy, enableProdMode } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { queryPlaybillList } from 'ng-epg-sdk/vsp'
import { EventService, ChannelService } from 'ng-epg-sdk/services'
import { session } from 'src/app/shared/services/session'
import { DateStingComponent } from '../../../component/dateString/dateString.component'
import { DirectionScroll } from 'ng-epg-ui/webtv-components/scroll/directionScroll.component'
import { TransverseScroll } from 'ng-epg-ui/webtv-components/scroll/transverseScroll.component'
import { ChannelListDetailService } from './channel-list-detail.service'
import { ChannelListService } from './channel-list.service'
import { CommonService } from '../../../core/common.service'
import { AccountLoginComponent } from '../../../login/account-login'
import { PictureService } from 'ng-epg-ui/services/picture.service'
import { PassInputDialogComponent } from '../../../component/passInput/pass-input-dialog.component'
import { PlaybillAppService } from '../../../component/playPopUpDialog/playbillApp.service'
import { WeakTipDialogComponent } from '../../../component/weakTip/weakTip.component'
import { TimerService } from '../../../shared/services/timer.service'
import { ChannelCacheService } from '../../../shared/services/channel-cache.service'
import { CacheConfigService } from '../../../shared/services/cache-config.service'
import { AbortAjaxHook } from 'ng-epg-sdk/hooks'

// FIXME: For some reason this call allows application to function properly. But it's WRONG!
// enableProdMode();

@Component({
  selector: 'app-channel-list',
  styleUrls: ['./channel-list.component.scss'],
  templateUrl: './channel-list.component.html',
  providers: [DirectionScroll, TransverseScroll, DateStingComponent, ChannelListDetailService, ChannelListService,
    PictureService]
})

export class ChannelListComponent implements OnInit, OnDestroy {
  @ViewChild(WeakTipDialogComponent) weakTipDialog: WeakTipDialogComponent
  @ViewChild('myVertical') myVerticalDom: ElementRef
  @ViewChild('transVertical') transVerticalDOM: ElementRef
  @ViewChild('genreContentTop') genreContentTopDOM: ElementRef
  @ViewChild('genreScrolltTop') genreScrolltTopDOM: ElementRef
  @Output() changeDateToNow: EventEmitter<Object> = new EventEmitter()
  public livetvVideoWidth = window.screen.width + 'px'
  public livetvVideoHeight = window.screen.height + 'px'
  public getWeakTip
  public profileType = ''
  public currentGenre: any = {}
    /**
     * list of genre
     */
  public genresList = []
  public channelPlaybillsList = []
  public miniDetailList
  public allTime = []
  public haveClick = false
  public currentTime = ''
  public isFavChannel = false
  public programListTimeout
  public dateToSetNewChannelList
    /**
     * the length of channel content
     */
  public contentHeight = '0px'
  public newDate: any = {}
  public judgeVertical = false
  public contentWidth = 1177
  public contentID = ''
  public contentType = ''
  public favInfo = ''
  public lockInfo = ''
  public profileRatingID
    /**
     * show list of genre
     */
  public isShowGenreList = false
  public isShowScrollList = false
  public currentPlaybillIcon = ''
  public currentToNow = 0
  public verticalFirstTimeOut
  public verticalTimer
  public isToBottom = true
  public channelTotalLength = 0
  public isScrollLoading = false
  public isFirstChanged = true
  public currentNOWTime = Number(Date.now()['getTime']())
  public showCurrenList
  public firstTime
  public miniDetailCurrentList
  public channelPlaybillsCurrentList
  public isLoginIn = false
  public isLeaveOrder = true
  public isLeaveTitle = true
  public isLeaveScroll = true
  public orderTip
    /**
     * keep the playbill that have been click
     */
  public clickPlaybill
  public isShowAlignScroll = false
  public runChannelName = []
  public startChanneName
  public defaultImgA
  public defaultImgB
  public channeNameLength = 0
  public isRecm = false
  public favJudgeI
  public favJudgeIndex
  public lockJudgeI
  public lockJudgeIndex
  public profileLogin
    /**
     * name for modules that would be used in this module
     */
  constructor (
        private directionScroll: DirectionScroll,
        private transverseScroll: TransverseScroll,
        private dateStingComponent: DateStingComponent,
        private channelListService: ChannelListService,
        private channelListDetailService: ChannelListDetailService,
        private commonService: CommonService,
        private pictureService: PictureService,
        private playbillAppService: PlaybillAppService,
        private channelService: ChannelService,
        private timerService: TimerService,
        private channelCacheService: ChannelCacheService,
        private cacheConfigService: CacheConfigService,
        private http: HttpClient,
        private _abortAjaxHook: AbortAjaxHook
    ) {
    this.notification = this.notification.bind(this)
  }
    /**
     * client time
     */
  synClientTime () {
    this.firstTime = 60 * 1000 - Date.now()['getSeconds']() * 1000 - Date.now()['getMilliseconds']()
    clearTimeout(this.verticalFirstTimeOut)
    this.verticalFirstTimeOut = setTimeout(() => {
      this.myVerticalDom.nativeElement.style.left = this.vertical()
      this.currentNOWTime = Number(Date.now()['getTime']())
      clearInterval(this.verticalTimer)
      this.verticalTimer = setInterval(() => {
        this.myVerticalDom.nativeElement.style.left = this.vertical()
        this.currentNOWTime = Number(Date.now()['getTime']())
      }, 60 * 1000)
    }, this.firstTime)
  }
    /**
     *  if data changed,current genre and channel remain unchanged widthout the playbill
     */
  @Input() set setNewChannelList (newDate) {
    this.isScrollLoading = true
    document.querySelector('.tvguide-load')['style']['display'] = 'block'
    if (newDate && newDate.time) {
      this.newDate = newDate
      let time = newDate.time.slice(0, 8) + '000000'
      this.allTime = this.channelListService['showAllTime'](this.dateStingComponent.stringToDate(time, 'yyyyMMddHHmmss'))
      document.querySelector('.epg-title .epg-right .allDay')['style']['width'] = this.allTime.length * 228 + 19 + 'px'
      document.querySelector('.channel-Playbills')['style']['width'] = this.allTime.length * 228 + 19 + 'px'
      newDate['startTime'] = this.dateStingComponent.stringToDate(
          newDate.time.replace(
            newDate.time.substring(8, 14), '000000'), 'yyyyMMddHHmmss')
      newDate['endTime'] = this.dateStingComponent.stringToDate(
          newDate.time.replace(
            newDate.time.substring(8, 14), '235959'), 'yyyyMMddHHmmss')
      let options = {}
      if (!session.get('FIRST_LOAD_VERTICAL')) {
        options = { isIgnoreError: true }
        this._abortAjaxHook.abortAjax('QueryPlaybillList')
      }
      this.queryChannels(this.currentGenre, 0, newDate, '13', options).then(resp => {
        let totalLength = parseInt(resp['total'], 10)
        if (totalLength === 0) {
            document.querySelector('.channel-right-content .no_channel_playbill')['style']['display'] = 'block'
          } else {
            document.querySelector('.channel-right-content .no_channel_playbill')['style']['display'] = 'none'
          }
        document.querySelector('.tvguide-load')['style']['display'] = 'none'
        let channelDtails = this.channelCacheService.getStaticChannelData().channelDetails
        let dynamicChannelData = this.channelCacheService.getDynamicChannelData().channelDynamaicProp
        _.each(resp.channelPlaybills, (item) => {
            item['channelInfos'] = _.find(channelDtails, (detail) => {
              return detail['ID'] === item['playbillLites'][0].channelID
            })
          })

        _.each(resp.channelPlaybills, (item) => {
            _.find(dynamicChannelData, (detail) => {
              if (item['channelInfos']['ID'] === detail['ID']) {
                item['channelInfos']['isLocked'] = detail['isLocked']
                item['channelInfos']['favorite'] = detail['favorite']
                item['channelInfos']['channelNO'] = detail['channelNO']
                let nameSpace = session.get('ott_channel_name_space')
                let physicalChannelsDynamicProperty = _.find(detail['physicalChannelsDynamicProperties'], (property) => {
                  return _.contains(property['channelNamespaces'], nameSpace)
                })
                item['channelInfos']['physicalChannelsDynamicProperty'] =
                                physicalChannelsDynamicProperty || detail['physicalChannelsDynamicProperties'][0]
              }
            })
          })

        for (let i = 0; i < resp.channelPlaybills.length; i++) {
            resp.channelPlaybills[i]['defaultImgID'] = i
            this.runChannelName[i] = false
          }
        this.channelPlaybillsList = resp.channelPlaybills
        this.contentHeight = this.channelPlaybillsList.length * 71 + 'px'
        this.channelListService['pluseOnScroll']()
        this.genreContentTopDOM.nativeElement.style.top = '0px'
        this.genreScrolltTopDOM.nativeElement.style.top = '0px'
        this.addOptions(newDate)
        this.currentToNow = 0
        this.isScrollLoading = false
      })
    }
  }
    /**
     * query the genre,chanenl and playbill
     * register the scroll roll event in order to roll the srcoll and load data
     * when the screen size change,the content and scroll's length change
     * initialization
     */
  ngOnInit () {
    this.defaultImgA = document.getElementById('defaultImgA')
    this.defaultImgB = document.getElementById('defaultImgB')

    document.addEventListener('click', e => {
      if (!(e.target['className'] === 'genresList-all' ||
                e.target['className'] === 'genresList_top' ||
                e.target['id'] === 'genresConter' ||
                e.target['className'] === 'genresList' ||
                e.target['className'] === 'currentGenre' ||
                e.target['className'] === 'currentGenre-text body-t29' ||
                e.target['id'] === 'currentGenre-down' ||
                e.target['id'] === 'content'
        )) {
        this.isShowGenreList = false
      }
    })
    if (!Cookies.getJSON('IS_GUEST_LOGIN')) {
      this.isLoginIn = true
    }
    let clientW: number = document.body.clientWidth
    if (clientW > 1440) {
      this.contentWidth = 1177
    } else {
      this.contentWidth = 777
    }
    this.profileRatingID = Number(session.get('PROFILERATING_ID'))
    this.profileType = Cookies.get('PROFILE_TYPE')
    this.queryChanSubject()
    EventService.removeAllListeners(['FIRST_TIME_LOGIN'])
    EventService.on('FIRST_TIME_LOGIN', (event) => {
      this.isLoginIn = true
      this.queryChanSubject('isLogin')
      if (this.haveClick) {
        this.showDetail(this.miniDetailCurrentList, this.channelPlaybillsCurrentList)
      }
      this.profileType = Cookies.get('PROFILE_TYPE')
    })
    EventService.removeAllListeners(['FAVORITE_MANGER'])
    EventService.on('FAVORITE_MANGER', (event) => {
      this.isRecm = true
      this.favManger(event)
      this.isRecm = false
    })
    EventService.removeAllListeners(['LOCK_MANGER'])
    EventService.on('LOCK_MANGER', (event) => {
      this.lockManger(event)
    })
    EventService.removeAllListeners(['CLOSE_MINIDETAIL'])
    EventService.on('CLOSE_MINIDETAIL', (event) => {
      this.haveClick = false
      this.clickPlaybill.haveclick = false
    })
    this.allTime = this.channelListService['showAllTime']()
    document.querySelector('.epg-title .epg-right .allDay')['style']['width'] = this.allTime.length * 228 + 19 + 'px'
    document.querySelector('.channel-Playbills')['style']['width'] = this.allTime.length * 228 + 19 + 'px'
    EventService.on('SHOW_ALLCHANNEL_PROGRAM', () => {
      this.showProgramList()
    })
    EventService.on('SHOW_SKIP_TO_NOW', () => {
      let vertical = Date.now()
      let hours = vertical['getHours']()
      let minutes = vertical['getMinutes']()
      let time = hours * 60 + minutes
      this.judgeToNow(time * (228 / 30))
    })
    EventService.on('ScreenSize', () => {
      const clientY = document.body.scrollWidth
      const clientZ = document.body.clientWidth
      if (clientZ > 1440) {
        this.contentWidth = 1177
      } else {
        this.contentWidth = 777
      }
      this.channelListService['transverseOnScroll']()
      if (clientY > 1440) {
        if (document.getElementById('transverseScroll')) {
            document.getElementById('transverseScroll')['style']['left'] = (clientY - 1280) / 2 + 203 + 'px'
            let channelPlaybillsWidth = document.getElementById('channelPlaybills').offsetWidth
            let channelPlaybillsLeft = document.getElementById('channelPlaybills')['style']['left']
            let spanWidth = 1158 * (1177 / channelPlaybillsWidth)
            let offsetLeft = document.getElementById('transverseSpan').offsetLeft / 758 * 1158
            document.getElementById('transverseSpan')['style']['left'] = spanWidth + offsetLeft > 1158
              ? 1158 - spanWidth + 'px' : offsetLeft + 'px'
            document.getElementById('channelPlaybills')['style']['left'] = spanWidth + offsetLeft > 1158
              ? (1177 - channelPlaybillsWidth) + 'px' : channelPlaybillsLeft
            document.getElementById('allDay')['style']['left'] = document.getElementById('channelPlaybills')['style']['left']
          }
      } else {
        if (document.getElementById('transverseScroll')) {
            document.getElementById('transverseScroll')['style']['left'] = (clientY - 980) / 2 + 203 + 'px'
            let spanWidth = 758 * (777 / document.getElementById('channelPlaybills').offsetWidth)
            let offsetLeft = document.getElementById('transverseSpan').offsetLeft / 1158 * 758
            document.getElementById('transverseSpan')['style']['left'] = spanWidth + offsetLeft > 758
              ? 758 - spanWidth + 'px' : offsetLeft + 'px'
          }
      }
    })
    EventService.on('LIVE_SCREEN_SIZE', () => {
      let clientY: number = document.body.scrollWidth
      if (document.getElementById('transverseScroll')) {
        document.getElementById('transverseScroll')['style']['left'] = (clientY - 980) / 2 + 203 - document.body.scrollLeft + 'px'
      }
    })
    this.isScrollLoading = false
    EventService.removeAllListeners(['RECORD_ADD_SUCCESS'])
    EventService.on('RECORD_ADD_SUCCESS', (data) => {
      if (data.type === 'single' && this.showCurrenList && this.showCurrenList['hasRecordingPVR']) {
        this.showCurrenList['hasRecordingPVR'] = '1'
        this.addWidthInt()
      } else if (data.type === 'periodic' && this.showCurrenList && this.showCurrenList['hasRecordingPVR']) {
          let seriesID = this.showCurrenList['playbillSeries']['seriesID']
          let channelID = this.showCurrenList['channelID']
          this.seriesIDPlaybill(seriesID, channelID)
        }
    })
    EventService.removeAllListeners(['RECORD_CANCEL_SUCCESS'])
    EventService.on('RECORD_CANCEL_SUCCESS', () => {
      if (this.showCurrenList && this.showCurrenList['hasRecordingPVR']) {
        this.showCurrenList['hasRecordingPVR'] = '0'
        this.removeWidthInt()
      }
    })
    EventService.removeAllListeners(['ADD_REMINDER_TVGUIDE'])
    EventService.on('ADD_REMINDER_TVGUIDE', () => {
      if (this.showCurrenList && this.showCurrenList['reminderStatus']) {
        this.showCurrenList['reminderStatus'] = '1'
        this.addWidthInt()
      }
    })
    EventService.removeAllListeners(['REMOVE_REMINDER_TVGUIDE'])
    EventService.on('REMOVE_REMINDER_TVGUIDE', () => {
      if (this.showCurrenList && this.showCurrenList['reminderStatus']) {
        this.showCurrenList['reminderStatus'] = '0'
        this.removeWidthInt()
      }
    })

    EventService.on('REMOVE_REMINDER_FOR_TVGUIDE', (data) => {
      for (let i = 0; i < this.channelPlaybillsList.length; i++) {
        let playbills = this.channelPlaybillsList[i]['playbillLites']
        for (let j = 0; j < playbills.length; j++) {
            if (playbills[j]['ID'] === data['playbillId']) {
              playbills[j]['reminderStatus'] = '0'
            }
          }
      }
    })

    EventService.on('REMOVE_REMINDER_FOR_TVGUIDE_ALL', data => {
      if (data.length === 0) {
        return
      }
      for (let f = 0; f < data.length; f++) {
        for (let i = 0; i < this.channelPlaybillsList.length; i++) {
            let playbills = this.channelPlaybillsList[i]['playbillLites']
            for (let j = 0; j < playbills.length; j++) {
              if (playbills[j]['ID'] === data[f]) {
                playbills[j]['reminderStatus'] = '0'
              }
            }
          }
      }
    })
  }

    /**
     * if add the series record.
     */
  seriesIDPlaybill (seriesID, channelID) {
    for (let i = 0; i < this.channelPlaybillsList.length; i++) {
      let playbills = this.channelPlaybillsList[i]['playbillLites']
      for (let j = 0; j < playbills.length; j++) {
        if (playbills[j]['playbillSeries'] && playbills[j]['playbillSeries']['seriesID'] &&
                    playbills[j]['playbillSeries']['seriesID'] === seriesID && playbills[j]['channelID'] === channelID) {
            playbills[j]['hasRecordingPVR'] = '2'
            playbills[j]['ImgWidthInt'] = playbills[j]['ImgWidthInt'] + 26
            playbills[j]['ImgWidth'] = playbills[j]['ImgWidthInt'] + 'px'
            playbills[j]['titleWidthInt'] = playbills[j]['titleWidthInt'] - 26
            playbills[j]['titleWidth'] = playbills[j]['titleWidthInt'] + 'px'
          }
      }
    }
  }
  addWidthInt () {
    this.showCurrenList['ImgWidthInt'] = this.showCurrenList['ImgWidthInt'] + 26
    this.showCurrenList['ImgWidth'] = this.showCurrenList['ImgWidthInt'] + 'px'
    this.showCurrenList['titleWidthInt'] = this.showCurrenList['titleWidthInt'] - 26
    this.showCurrenList['titleWidth'] = this.showCurrenList['titleWidthInt'] + 'px'
  }

  removeWidthInt () {
    this.showCurrenList['ImgWidthInt'] = this.showCurrenList['ImgWidthInt'] - 26
    this.showCurrenList['ImgWidth'] = this.showCurrenList['ImgWidthInt'] + 'px'
    this.showCurrenList['titleWidthInt'] = this.showCurrenList['titleWidthInt'] + 26
    this.showCurrenList['titleWidth'] = this.showCurrenList['titleWidthInt'] + 'px'
  }
    /**
     * open left suspension
     */
  openLeftSuspension (channelPlaybill, i) {
    clearInterval(this.startChanneName)
    if (Cookies.getJSON('IS_PROFILE_LOGIN')) {
      this.profileLogin = 1
    } else if (Cookies.getJSON('IS_GUEST_LOGIN')) {
      this.profileLogin = 0
    }
    this.favJudgeI = i
    this.lockJudgeI = i
    if (channelPlaybill.isLeftSuspensionShow = true) {
      channelPlaybill.isLeftSuspensionShow = false
    }
    if (channelPlaybill['channelDetail'] || channelPlaybill['channelInfos']) {
      let channeldetail = this.getchanneldetail(channelPlaybill)
      this.contentID = channeldetail['ID']
      this.contentType = channeldetail['type']
      if (channeldetail.favorite || (this.currentGenre && this.currentGenre['name'] === 'livetv_favorite')) {
        this.favInfo = 'cance_Favorite'
      } else {
        this.favInfo = 'add_Favorite'
      }
      if (channeldetail.isLocked !== '1') {
        this.lockInfo = 'lock'
      } else {
        this.lockInfo = 'unlock'
      }
      channelPlaybill.isLeftSuspensionShow = true
      this.startTurn(channelPlaybill, i)
    }
  }
    /**
     * get channel detail
     */
  getchanneldetail (channelPlaybill) {
    return channelPlaybill && channelPlaybill['channelDetail'] || channelPlaybill['channelInfos'] || {}
  }
    /**
     * close the left suspension
     */
  closeLeftSuspension (channelPlaybill, i) {
    clearInterval(this.startChanneName)
    let k = document.getElementsByClassName('channelLeftSuspension')
    for (let j = 0; j < k.length; j++) {
      k[j].remove()
    }
    if (channelPlaybill) {
      channelPlaybill.isLeftSuspensionShow = false
      this.endTurn(channelPlaybill, i)
    }
  }
    /**
     * get the left distance of the transverse scroll
     */
  getScrollLeft () {
    let vertical = Date.now()
    let hours = vertical['getHours']()
    let minutes = vertical['getMinutes']()
    let time = hours * 60 + minutes
    if (this.currentToNow !== this.transVerticalDOM.nativeElement.style.left && !session.get('FIRST_LOAD_VERTICAL')) {
      this.currentToNow = this.transVerticalDOM.nativeElement.style.left
      this.judgeToNow(time * (228 / 30))
    }

    let clientW: number = document.body.scrollWidth
    if (clientW > 1440) {
      return (clientW - 1280) / 2 + 203 + 'px'
    } else {
      return (clientW - 980) / 2 + 203 + 'px'
    }
  }
    /**
     * compute the width of the transverse scroll
     */
  getScrollWidth () {
      // get the width of the screen
    let clientW: number = document.body.scrollWidth
      // if the width of the screen is larger than 1440 and show the transverse scroll
    if (clientW > 1440) {
      if (this.isShowScrollList) {
        return '1158px'
      } else {
        return '1177px'
      }
    } else {
        // if the width of the screen is not larger than 1440 and show the transverse scroll
      if (this.isShowScrollList) {
        return '758px'
      } else {
        return '777px'
      }
    }
  }
    /**
     * deal channel,images of the playbill and the time show
     * current channel style
     * the width of channel
     */
  addOptions (newDate?: any) {
    this.profileRatingID = Number(session.get('PROFILERATING_ID'))
    return this.timerService.getCaheCustomConfig().then(resp => {
      _.map(this.channelPlaybillsList, (channelPlaybill, index) => {
        channelPlaybill.isLeftSuspensionShow = false
        let channelDtail = channelPlaybill && channelPlaybill['channelDetail'] || channelPlaybill['channelInfos']
        channelPlaybill.channelPics = channelDtail &&
                    channelDtail['picture'] && channelDtail['picture']['channelPics']
        if (channelDtail) {
            channelPlaybill.channelPics = this.pictureService.convertToSizeUrl(channelPlaybill['channelPics'],
              { minwidth: 54, minheight: 54, maxwidth: 54, maxheight: 54 })
          }
        channelPlaybill.playbillLites = this.dateStingComponent.compleChannelPlaybillLites(channelPlaybill.playbillLites, newDate,
            channelDtail && channelDtail.rating && channelDtail.rating.ID)
      })
    })
  }

  isChannelLogo (channelPlaybill) {
    channelPlaybill.showChannelLogo = true
  }

  startTurn (channelPlaybill, i) {
    clearInterval(this.startChanneName)
    this.channeNameLength = 0
    let copyID = 'defaultImgFCopy' + channelPlaybill['defaultImgID']
    if (document.getElementById(copyID) && document.getElementById(copyID).offsetWidth > 54) {
      this.runChannelName[i] = true
      let allID = 'defaultImgAll' + channelPlaybill['defaultImgID']
      let aID = 'defaultImgA' + channelPlaybill['defaultImgID']
      clearInterval(this.startChanneName)
      this.startChanneName = setInterval(() => {
        if (document.getElementById(allID) && document.getElementById(aID)) {
            document.getElementById(allID).style.width = Math.ceil(document.getElementById(aID).offsetWidth) * 2 + 21 + 'px'
            let tranlateDis = document.getElementById(aID).offsetWidth
            this.channeNameLength--
            this.moveChannelname(tranlateDis, this.channeNameLength, allID)
          }
      }, 35)
    }
  }
    /**
     * move channel name
     */
  moveChannelname (tranlateDis, channeNameLength, allID) {
    document.getElementById(allID).style.transform = 'translate(' + channeNameLength + 'px, 0px)'
    if (channeNameLength === -tranlateDis - 20) {
      document.getElementById(allID).style.left = 0 + 'px'
      this.channeNameLength = 0
    }
  }

  endTurn (channelPlaybills, i) {
    clearInterval(this.startChanneName)
    let allID = 'defaultImgAll' + channelPlaybills['defaultImgID']
    if (document.getElementById(allID)) {
      document.getElementById(allID).style.transform = 'translate(0px, 0px)'
    }
    this.runChannelName[i] = false
  }
    /**
     * query the one-level genre
     */
  queryChanSubject (isLogin?: any) {
    session.put('FIRST_LOAD_VERTICAL', true)
    this.channelListService['queryChanSubject']().then(resp => {
      this.genresList = resp
      this.currentGenre = this.genresList[0]
      if (isLogin) {
        this.dealPlaybillDetails(this.currentGenre)
      } else {
        this.dealPlaybillDetails(this.currentGenre, 'firstLogin')
      }
    })
  }
    /**
     * TVGUIDE page, the scroll bar below and the scroll bar on the left coincide
     */
  changeGenreByLeft () {
    let clientW: number = document.body.scrollWidth
    let transVerticalLeft = parseInt(this.transVerticalDOM.nativeElement.style.left, 10)
    let scrollWidth, offsetWidth = document.getElementById('transverseSpan').offsetWidth
    if (clientW > 1440) {
      scrollWidth = 1158
    } else {
      scrollWidth = 758
    }
    if ((scrollWidth - offsetWidth) < transVerticalLeft) {
      document.getElementById('transverseSpan').style.left = scrollWidth - offsetWidth + 'px'
    }
  }
  dealWithChannelIDSEmpty () {
    document.querySelector('.tvguide-load')['style']['display'] = 'none'
    document.querySelector('.channel-right-content .no_channel_playbill')['style']['display'] = 'block'
    this.channelPlaybillsList = []
    this.contentHeight = this.channelPlaybillsList.length * 71 + 'px'
    this.isShowScrollList = false
    this.genreContentTopDOM.nativeElement.style.top = '0px'
    this.genreScrolltTopDOM.nativeElement.style.top = '0px'
    if (this.newDate && this.newDate['time']) {
      this.addOptions(this.newDate)
    } else {
      this.addOptions()
    }
    if (this.currentGenre['name'] === 'livetv_favorite') {
      this.isFavChannel = true
    } else {
      this.isFavChannel = false
    }
    this.currentToNow = 0
    this.isScrollLoading = false
  }

  judgeWhetherCallInterface (genre) {
    if (this.currentGenre && this.currentGenre['name'] === 'livetv_favorite') {
      let dynamicChannelData = this.channelCacheService.getDynamicChannelData().channelDynamaicProp
      let lastChannel = _.filter(dynamicChannelData, channelDetails => channelDetails['favorite'] !== undefined)

      return _.pluck(lastChannel, 'ID')
    } else if (this.currentGenre && this.currentGenre['name'] === 'purchased') {
      return this.cacheConfigService.getSubChannel().then((resp) => {
          let staticChannelData = this.channelCacheService.getStaticChannelData().channelDetails
          let allChannels = resp
          let IDList1 = _.pluck(staticChannelData, 'ID')
          let IDList2 = _.pluck(allChannels['channelDetails'], 'ID')

          return _.intersection(IDList1, IDList2)
        })
    } else {
      let IDList
      IDList = this.getChannelIDsBySubjectID(genre, IDList, 0, 13)
      return IDList
    }
  }
    /**
     * change genre by dates
     */
  changeGenreByDates (genre, options?: any) {
    document.querySelector('.channel-right-content .no_channel_playbill')['style']['display'] = 'none'
    this.isScrollLoading = true
    document.querySelector('.tvguide-load')['style']['display'] = 'block'
      // if IDList is empty, not call interface
    let IDList = this.judgeWhetherCallInterface(genre)
    if (IDList.length === 0) {
      this.dealWithChannelIDSEmpty()
      return
    }
    this.queryChannels(genre, 0, this.newDate, '13', options).then(resp => {
      let totalLength = parseInt(resp['total'], 10)
      if (totalLength === 0) {
        document.querySelector('.channel-right-content .no_channel_playbill')['style']['display'] = 'block'
      } else {
        document.querySelector('.channel-right-content .no_channel_playbill')['style']['display'] = 'none'
      }
      document.querySelector('.tvguide-load')['style']['display'] = 'none'
      let channelDtails = this.channelCacheService.getStaticChannelData().channelDetails
      let dynamicChannelData = this.channelCacheService.getDynamicChannelData().channelDynamaicProp
      _.each(resp.channelPlaybills, (item) => {
        item['channelInfos'] = _.find(channelDtails, (detail) => {
            return detail['ID'] === item['playbillLites'][0].channelID
          })
      })
      _.each(resp.channelPlaybills, (item) => {
        _.find(dynamicChannelData, (detail) => {
            if (item['channelInfos']['ID'] === detail['ID']) {
              item['channelInfos']['isLocked'] = detail['isLocked']
              item['channelInfos']['favorite'] = detail['favorite']
              item['channelInfos']['channelNO'] = detail['channelNO']
              let nameSpace = session.get('ott_channel_name_space')
              let physicalChannelsDynamicProperty = _.find(detail['physicalChannelsDynamicProperties'], (property) => {
                return _.contains(property['channelNamespaces'], nameSpace)
              })
              item['channelInfos']['physicalChannelsDynamicProperty'] =
                            physicalChannelsDynamicProperty || detail['physicalChannelsDynamicProperties'][0]
            }
          })
      })
      for (let i = 0; i < resp.channelPlaybills.length; i++) {
        resp.channelPlaybills[i]['defaultImgID'] = i
        this.runChannelName[i] = false
      }
      this.channelPlaybillsList = resp.channelPlaybills
      this.contentHeight = this.channelPlaybillsList.length * 71 + 'px'
      if (totalLength > 12) {
        this.isShowScrollList = true
        this.channelListService['pluseOnScroll']()
        this.changeGenreByLeft()
      } else {
        this.isShowScrollList = false
      }
      this.genreContentTopDOM.nativeElement.style.top = '0px'
      this.genreScrolltTopDOM.nativeElement.style.top = '0px'
      if (this.newDate && this.newDate['time']) {
        this.addOptions(this.newDate)
      } else {
        this.addOptions()
      }
      if (this.currentGenre['name'] === 'livetv_favorite') {
        this.isFavChannel = true
      } else {
        this.isFavChannel = false
      }
      this.currentToNow = 0
      this.isScrollLoading = false
    })
  }
  dealWithFirstLogin (isFirstLogin) {
    document.querySelector('.channel-right-content .no_channel_playbill')['style']['display'] = 'block'
    this.channelPlaybillsList = []
    let channelLength = this.channelPlaybillsList.length
    this.contentHeight = channelLength * 71 + 'px'
    this.genreContentTopDOM.nativeElement.style.top = '0px'
    this.genreScrolltTopDOM.nativeElement.style.top = '0px'
    this.addOptions()
    this.isShowScrollList = false
    this.channelListService['transverseOnScroll']()
    session.put('FIRST_LOAD_VERTICAL', false)
    this.toNow(isFirstLogin)
    document.querySelector('.tvguide-load')['style']['display'] = 'none'
    if (this.currentGenre['name'] === 'livetv_favorite') {
      this.isFavChannel = true
    } else {
      this.isFavChannel = false
    }
    this.synClientTime()
    this.isShowAlignScroll = true
  }
    /**
     *  query current palybill and replenish all playbill (temporary empty)
     * initialize the longitudinal and  transverse scroll
     *  turn to the postion of now
     */
  dealPlaybillDetails (currentGenre?: any, isFirstLogin?: any) {
    let currentGenres = currentGenre || this.currentGenre
    document.querySelector('.tvguide-load')['style']['display'] = 'block'
    this.isShowAlignScroll = false
    let IDList = this.judgeWhetherCallInterface(currentGenre)
    if (IDList.length === 0) {
      this.dealWithFirstLogin(isFirstLogin)
      return
    }
    this.queryChannels(currentGenres).then(resp => {
      let totalLength = parseInt(resp['total'], 10)
      if (document.querySelector('.channel-right-content .no_channel_playbill')) {
        if (totalLength === 0) {
            document.querySelector('.channel-right-content .no_channel_playbill')['style']['display'] = 'block'
          } else {
            document.querySelector('.channel-right-content .no_channel_playbill')['style']['display'] = 'none'
          }
      }
      let channelDtails = this.channelCacheService.getStaticChannelData().channelDetails
      let dynamicChannelData = this.channelCacheService.getDynamicChannelData().channelDynamaicProp
      _.each(resp.channelPlaybills, (item) => {
        item['channelInfos'] = _.find(channelDtails, (detail) => {
            return detail['ID'] === item['playbillLites'][0].channelID
          })
      })
      _.each(resp.channelPlaybills, (item) => {
        _.find(dynamicChannelData, (detail) => {
            if (item['channelInfos']['ID'] === detail['ID']) {
              item['channelInfos']['isLocked'] = detail['isLocked']
              item['channelInfos']['favorite'] = detail['favorite']
              item['channelInfos']['channelNO'] = detail['channelNO']
              let nameSpace = session.get('ott_channel_name_space')
              let physicalChannelsDynamicProperty = _.find(detail['physicalChannelsDynamicProperties'], (property) => {
                return _.contains(property['channelNamespaces'], nameSpace)
              })
              item['channelInfos']['physicalChannelsDynamicProperty'] =
                            physicalChannelsDynamicProperty || detail['physicalChannelsDynamicProperties'][0]
            }
          })
      })
      for (let i = 0; i < resp.channelPlaybills.length; i++) {
        resp.channelPlaybills[i]['defaultImgID'] = i
        this.runChannelName[i] = false
      }
      this.channelPlaybillsList = resp.channelPlaybills
      let channelLength = this.channelPlaybillsList.length
      this.contentHeight = channelLength * 71 + 'px'
      this.genreContentTopDOM.nativeElement.style.top = '0px'
      this.genreScrolltTopDOM.nativeElement.style.top = '0px'
      this.addOptions()
      if (totalLength > 12) {
        this.isShowScrollList = true
        this.channelListService['pluseOnScroll']()
      } else {
        this.isShowScrollList = false
      }
      this.channelListService['transverseOnScroll']()
      session.put('FIRST_LOAD_VERTICAL', false)
      this.toNow(isFirstLogin)
      document.querySelector('.tvguide-load')['style']['display'] = 'none'
      if (this.currentGenre['name'] === 'livetv_favorite') {
        this.isFavChannel = true
      } else {
        this.isFavChannel = false
      }
      this.synClientTime()
      this.isShowAlignScroll = true
    })
  }
    /**
     * start or end date
     */
  startOrEndDate (isIe, isEdge, newDate) {
    let date = Date.now()['getDate']()
    let month = Date.now()['getMonth']()
    let year = Date.now()['getFullYear']()
    let startDateTime = newDate && newDate['startTime'] && newDate['startTime'].getTime() ||
            new Date(year, month, date, 0, 0, 0).getTime() + ''
    let endDateTime = newDate && newDate['endTime'] && newDate['endTime'].getTime() ||
            new Date(year, month, date, 23, 59, 59).getTime() + ''

    return { 'startDateTime': startDateTime, 'endDateTime': endDateTime }
  }
    /**
     * query playbill information
     */
  queryChannels (subject, offset?: number, newDate?: any, count?: string, option?: any) {
    let channelFilter = {}
    let IDList
    let options = option || {}
    options.params = {
      SID: 'queryplaybilllist2',
      DEVICE: 'PC',
      DID: session.get('uuid_cookie')
    }
    if (this.currentGenre && this.currentGenre['name'] === 'livetv_favorite') {
      channelFilter = { isFavorite: '1' }
      let dynamicChannelData = this.channelCacheService.getDynamicChannelData().channelDynamaicProp
      let lastChannel = _.filter(dynamicChannelData, channelDetails => channelDetails['favorite'] !== undefined)
      IDList = _.pluck(lastChannel, 'ID')
      this.channelTotalLength = IDList.length
      return this.returnQueryChannel(IDList, offset, count, subject, channelFilter, options, newDate)
    } else if (this.currentGenre && this.currentGenre['name'] === 'purchased') {
      channelFilter = { subscription: ['0', '1'] }
      return this.cacheConfigService.getSubChannel().then((resp) => {
          let staticChannelData = this.channelCacheService.getStaticChannelData().channelDetails
          let allChannels = resp
          let IDList1 = _.pluck(staticChannelData, 'ID')
          let IDList2 = _.pluck(allChannels['channelDetails'], 'ID')
          IDList = _.intersection(IDList1, IDList2)
          this.channelTotalLength = IDList.length
          return this.returnQueryChannel(IDList, offset, count, subject, channelFilter, options, newDate)
        })
    } else {
      IDList = this.getChannelIDsBySubjectID(subject, IDList, offset, count)
      return this.returnQueryChannel(IDList, offset, count, subject, channelFilter, options, newDate, true)
    }
  }

  returnQueryChannel (IDList, offset, count, subject, channelFilter, options, newDate, other?) {
    if (!offset) {
      offset = 0
    }
    if (!count) {
      count = '13'
    }
    if (!other) {
      IDList = IDList.slice(offset, offset + Number(count))
    }
    subject = subject || this.currentGenre
    let ua = navigator.userAgent.toLowerCase()
    let isIe = !!ua.match(/msie/i) || !!ua.match(/rv:([\d.]+).*like gecko/)
    let isEdge = !!ua.match(/edge\/([\d.]+)/)
    let startOrEndDates = this.startOrEndDate(isIe, isEdge, newDate)

    return queryPlaybillList({
      queryChannel: {
        subjectID: subject.ID,
        contentType: 'CHANNEL',
        channelIDs: IDList,
        isReturnAllMedia: '1',
        channelFilter: channelFilter
      },
      needChannel: '0',
      queryPlaybill: {
        startTime: startOrEndDates['startDateTime'] + '',
        endTime: startOrEndDates['endDateTime'] + '',
        count: '100',
        offset: '0',
        type: '0',
        isFillProgram: '1'
      }
    }, options)
  }

    /**
     * get channels' id
     */
  getChannelIDsBySubjectID (subject, IDList, offset, count) {
    if (!offset) {
      offset = 0
    }
    if (!count) {
      count = '13'
    }
    let subjectID = subject['ID'] || subject['name']
    if (Number(subjectID) === -1) {
      IDList = this.channelCacheService.getStorageChannelIDList()
      this.channelTotalLength = IDList.length
      IDList = IDList.slice(offset, offset + Number(count))
    } else {
      IDList = this.channelCacheService.getStroageChannelIDListBySubjectID(subjectID)
      this.channelTotalLength = IDList.length
      IDList = IDList.slice(offset, offset + Number(count))
    }
    return IDList
  }
    /**
     * get queryplaybill current count
     */
  getQueryPlaybillCount (startIndex, channelPlaybillsList) {
    let listRight = channelPlaybillsList.splice(startIndex)
    let dealList = listRight.splice(0, 12)
    let count = 0
    let firstIndex = 0
    _.map(dealList, (channelPlaybill, index) => {
      if (!channelPlaybill || channelPlaybill && !channelPlaybill['playbillCount']) {
        count += 1
        if (firstIndex === 0) {
            firstIndex = index
          }
      }
    })
    return {
      count: count + '',
      firstIndex: startIndex + firstIndex
    }
  }
    /**
     * monitor the srcoll rolling event and load data which had not shown
     */
  showProgramList () {
    if (this.isToBottom && this.channelTotalLength > this.channelPlaybillsList.length) {
      this.isToBottom = false
      document.querySelector('.tvguide-load')['style']['display'] = 'block'
      let count = (this.channelTotalLength - this.channelPlaybillsList.length) > 12
          ? '12' : this.channelTotalLength - this.channelPlaybillsList.length + ''
      this.queryChannels(this.currentGenre, this.channelPlaybillsList.length, this.newDate, count).then(resp => {
        if (this.isScrollLoading) {
            this.isScrollLoading = false
            this.isToBottom = true
            document.querySelector('.tvguide-load')['style']['display'] = 'none'
            return
          }
        document.querySelector('.tvguide-load')['style']['display'] = 'none'
        let channelDtails = this.channelCacheService.getStaticChannelData().channelDetails
        let dynamicChannelData = this.channelCacheService.getDynamicChannelData().channelDynamaicProp
        _.each(resp.channelPlaybills, (item) => {
            item['channelInfos'] = _.find(channelDtails, (detail) => {
              return detail['ID'] === item['playbillLites'][0].channelID
            })
          })
        _.each(resp.channelPlaybills, (item) => {
            _.find(dynamicChannelData, (detail) => {
              if (item['channelInfos']['ID'] === detail['ID']) {
                item['channelInfos']['isLocked'] = detail['isLocked']
                item['channelInfos']['favorite'] = detail['favorite']
                item['channelInfos']['channelNO'] = detail['channelNO']
                let nameSpace = session.get('ott_channel_name_space')
                let physicalChannelsDynamicProperty = _.find(detail['physicalChannelsDynamicProperties'], (property) => {
                  return _.contains(property['channelNamespaces'], nameSpace)
                })
                item['channelInfos']['physicalChannelsDynamicProperty'] =
                                physicalChannelsDynamicProperty || detail['physicalChannelsDynamicProperties'][0]
              }
            })
          })
        for (let i = 0; i < resp.channelPlaybills.length; i++) {
            resp.channelPlaybills[i]['defaultImgID'] = i
            this.runChannelName[i] = false
          }
        this.channelPlaybillsList = this.channelPlaybillsList.concat(resp.channelPlaybills)
        if (this.newDate && this.newDate['time']) {
            this.addOptions(this.newDate)
          } else {
            this.addOptions()
          }
        this.contentHeight = this.channelPlaybillsList.length * 71 + 'px'
        this.channelListService['pluseOnScroll']()
        this.isToBottom = true
      })
    }
  }
    /**
     * get the position of the time line
     */
  vertical () {
    let vertical = Date.now()
    let hours
    hours = vertical['getHours']()
    let minutes
    minutes = vertical['getMinutes']()
    let time = hours * 60 + minutes

    if (Math.floor(hours) < 10) {
      hours = '0' + hours
    }
    if (Math.floor(minutes) < 10) {
      minutes = '0' + minutes
    }
    this.currentTime = hours + ':' + minutes
    if (this.currentPlaybillIcon !== time) {
      this.currentPlaybillIcon = time
      this.verCurrentPlaybill()
    }
    this.judgeToNow(time * (228 / 30))
    return time * (228 / 30) + 'px'
  }
    /**
     * get current playbill
     */
  verCurrentPlaybill () {
    let isCurrentIDIndex = 0
    _.map(this.channelPlaybillsList, channelPlaybill => {
      _.map(channelPlaybill.playbillLites, (playbill, index) => {
        if (playbill) {
            playbill[playbill['ID'] + 'isCurrent'] =
                        this.dateStingComponent.judgeCurrentTime(playbill['startTime'], playbill['endTime'])
            if (playbill[playbill['ID'] + 'isCurrent']) {
              isCurrentIDIndex = index
            }
            if (this.haveClick && this.miniDetailList[0].playbillDetail.startTime <
                        Date.now()['getTime']() && this.miniDetailList[0].playbillDetail.endTime > Date.now()['getTime']()) {
              this.haveClick = false
            }
          }
      })
      if (isCurrentIDIndex > 2) {
        channelPlaybill.playbillLites[isCurrentIDIndex - 1]['isTVODID'] = true
        channelPlaybill.playbillLites[isCurrentIDIndex - 2]['isTVODID'] = true
        channelPlaybill.playbillLites[isCurrentIDIndex - 1]['autoType'] = 'tvodPlaybill'
        channelPlaybill.playbillLites[isCurrentIDIndex - 2]['autoType'] = 'tvodPlaybill'
      } else if (isCurrentIDIndex > 1) {
          channelPlaybill.playbillLites[isCurrentIDIndex - 1]['isTVODID'] = true
          channelPlaybill.playbillLites[isCurrentIDIndex - 1]['autoType'] = 'tvodPlaybill'
        }
      if (channelPlaybill.playbillLites[isCurrentIDIndex + 1]) {
        channelPlaybill.playbillLites[isCurrentIDIndex + 1]['isTVODID'] = true
        channelPlaybill.playbillLites[isCurrentIDIndex + 1]['autoType'] = 'futherPlaybill'
      }
    })
  }
    /**
     * After roll the transverse scroll, deal with  the now icon display
     */
  judgeToNow (nowleft) {
    let result: boolean
    let newDate
    let CurrentTime
    let ClickedTime
    if (this.newDate['time']) {
      newDate = this.dateStingComponent.stringToDate(this.newDate['time'], 'yyyyMMddHHmmss')
      CurrentTime = new Date(Date.now()['getFullYear'](), Date.now()['getMonth'](), Date.now()['getDate'](), 0, 0, 0).getTime()
      ClickedTime = new Date(newDate['getFullYear'](), newDate['getMonth'](), newDate['getDate'](), 0, 0, 0).getTime()
    }

    if (!this.newDate['time'] || ClickedTime === CurrentTime) {
      let nowLeft = nowleft
      let miniLeft = 0
      let miniRight = this.contentWidth
      if (document.getElementById('allDay')) {
        miniLeft = this.getMiniLeft()
        miniRight = -parseInt(document.getElementById('allDay').style.left, 10) + this.contentWidth || this.contentWidth
      }
      if (nowLeft < miniLeft) {
        this.isLeftOrRightToNow('leftNow', 'visible')
        this.isLeftOrRightToNow('rightNow', 'hidden')
      } else if (nowLeft > miniRight) {
          this.isLeftOrRightToNow('leftNow', 'hidden')
          this.isLeftOrRightToNow('rightNow', 'visible')
        } else {
          this.isLeftOrRightToNow('leftNow', 'hidden')
          this.isLeftOrRightToNow('rightNow', 'hidden')
        }
      result = true
    } else if (ClickedTime < CurrentTime) {
      this.isLeftOrRightToNow('leftNow', 'hidden')
      this.isLeftOrRightToNow('rightNow', 'visible')
      result = false
    } else if (ClickedTime > CurrentTime) {
        this.isLeftOrRightToNow('leftNow', 'visible')
        this.isLeftOrRightToNow('rightNow', 'hidden')
        result = false
      }

    setTimeout(() => {
      this.judgeVertical = result
    }, 0)
  }
    /**
     * get minileft
     */
  getMiniLeft () {
    return -parseInt(document.getElementById('allDay').style.left, 10) || 0
  }
    /**
     * judge is left or right to now
     */
  isLeftOrRightToNow (type, isshow) {
    if (document.getElementById(type)) {
      document.getElementById(type)['style']['visibility'] = isshow
    }
  }
    /**
     * click the now icon
     */
  toNow (isFirstLogin?: any) {
    if (!this.newDate['time'] || this.dateStingComponent.stringToDate(this.newDate['time'], 'yyyyMMddHHmmss').getDate() ===
            Date.now()['getDate']()) {
    } else {
      if (isFirstLogin) {
        return
      }
      this.changeDateToNow.emit(Date.now())
    }
      // when the mini detail is visible,first close the mini detail.
    if (document.getElementById('miniDetail') && !document.getElementById('miniDetail')['hidden']) {
      this.haveClick = false
      this.clickPlaybill.haveclick = false
    }
    let contentLeft, oUl, oScroll
    this.myVerticalDom.nativeElement.style.left = this.vertical()
    contentLeft = parseInt(this.myVerticalDom.nativeElement.style.left, 10) - this.contentWidth / 2
    oUl = document.getElementById('channelPlaybills')
    oScroll = document.getElementById('transverseScroll')
    if (oUl && oScroll) {
      if (parseInt(this.myVerticalDom.nativeElement.style.left, 10) > (oUl.offsetWidth - this.contentWidth)) {
        contentLeft = oUl.offsetWidth - this.contentWidth
      } else if (parseInt(this.myVerticalDom.nativeElement.style.left, 10) < this.contentWidth) {
          contentLeft = 0
        }
      document.getElementById('allDay').style.left = -contentLeft + 'px'
      document.getElementById('channelPlaybills').style.left = -contentLeft + 'px'
      document.getElementById('transverseSpan').style.left =
                oScroll.offsetWidth * (contentLeft / oUl.offsetWidth) + 'px'
    }
    this.isLeftOrRightToNow('leftNow', 'hidden')
    this.isLeftOrRightToNow('rightNow', 'hidden')
  }
    /**
     *  judge the FillProgram
     */
  isFillAllProgram (program) {
    if (program['isFillProgram'] === '1') {
      return true
    } else {

    }
  }
    /**
     * solve current page remain when play channel in full screen
     */
  showDetail (list, channelPlaybills) {
    document.getElementsByClassName('live_load_bg')[0]['style']['display'] = 'block'
    document.getElementsByClassName('play_loading')[0]['style']['display'] = 'block'

    this.miniDetailCurrentList = list
    this.channelPlaybillsCurrentList = channelPlaybills
    this.showCurrenList = list
    if (list[list['ID'] + 'isCurrent']) {
      session.put('ISCURRENT', true)
      session.put('TVGuide', 'true')
      EventService.emit('OPEN_FULLSCREEN_LIVETVVIDEO', {})
      this.playbillAppService.playProgram(list, channelPlaybills['channelInfos'])
      for (let i = 0; i <= 50000000; i++) {
        if (i === 50000000) {
            let video = document.querySelector('#videoContainer video')
            let ua = navigator.userAgent.toLowerCase()
            let isIe = !!ua.match(/msie/i) || !!ua.match(/rv:([\d.]+).*like gecko/)
            let isFirefox = !!ua.match(/firefox\/([\d.]+)/)
            let isEdge = !!ua.match(/edge\/([\d.]+)/)
            if (isIe) {
              video = document.querySelector('#videoContainer')
              video['msRequestFullscreen']()
            } else if (isEdge) {
              video = document.querySelector('#videoContainer')
              video.webkitRequestFullScreen()
            } else if (isFirefox) {
              video = document.querySelector('#videoContainer')
              video['mozRequestFullScreen']()
            } else if (ua.match(/version\/([\d.]+).*safari/)) {
              video.requestFullscreen()
            } else {
              video.webkitRequestFullScreen()
            }
          }
      }
    } else {
      session.put('ISCURRENT', false)
    }
    if (this.isFillAllProgram(list) && list[list['ID'] + 'isCurrent']) {
      return
    } else if (this.isFillAllProgram(list)) {
      EventService.emit('NO_PROGRAM_TOAST')
      return
    }
    this.channelListDetailService.getDetail(list.ID, list.endTime).then((resp) => {
      if (!list[list['ID'] + 'isCurrent']) {
        this.miniDetailList = [resp, channelPlaybills]
        this.haveClick = true
        list.haveclick = true
        this.clickPlaybill = list
      }
    })
  }
    /**
     * manage the state of favourite or not,
     * judge in different situations like minibar, tvguide, livetv-favorite genre and so on,
     * put the favourite data into session if need
    */
  favManger (channelPlaybill, indexs?) {
    this.favJudgeIndex = indexs
    let channelDetail = channelPlaybill.channelDetail || channelPlaybill['channelInfos']
    if (channelDetail) {
      for (let i = 0; i < this.channelPlaybillsList.length; i++) {
        if (channelDetail.ID === this.channelPlaybillsList[i]['channelInfos'].ID) {
            channelPlaybill = this.channelPlaybillsList[i]
            break
          }
      }
    }
    if (Cookies.getJSON('IS_GUEST_LOGIN')) {
      this.haveClick = false
      if (this.clickPlaybill) {
        this.clickPlaybill.haveclick = false
      }
      EventService.emit('CLOSED_LOGIN_DIALOG')
      EventService.on('MD_DIALOG_CONTAINER', () => {
        document.getElementsByTagName('md-dialog-container')[0]['style'].position = 'fixed'
      })
      this.commonService.openDialog(AccountLoginComponent)
      return
    }
    if (channelDetail.favorite || (!this.isRecm && this.currentGenre && this.currentGenre['name'] === 'livetv_favorite')) {
      let req = {
        contentIDs: [channelPlaybill['channelInfos'] && channelPlaybill['channelInfos']['ID'] ||
                    channelPlaybill['channelDetail']['ID']],
        contentTypes: ['CHANNEL']
      }

      this.commonService.removeFavorite(req).then(resp => {
        if (resp['result']['retCode'] === '000000000') {
            // refresh the localStroge
            let sessionDynamicData = this.channelCacheService.getDynamicChannelData()
            _.each(sessionDynamicData['channelDynamaicProp'], (list, index) => {
              if (list['ID'] === (channelPlaybill['channelInfos'] &&
                            channelPlaybill['channelInfos']['ID'] || channelPlaybill['channelDetail']['ID'])) {
                list['favorite'] = undefined
              }
            })
            let userType
            if (Cookies.getJSON('IS_PROFILE_LOGIN')) {
              userType = Cookies.getJSON('USER_ID')
            } else {
              userType = session.get('Guest')
            }
            session.put(userType + '_Dynamic_Version_Data', sessionDynamicData, true)
            this.channelCacheService.favRefresh(resp['version'], resp['preVersion'])
            // if cancel favorite from the mini detail, the favorite channel list should be refresh.
            if (this.currentGenre['name'] === 'livetv_favorite') {
              channelPlaybill.isLeftSuspensionShow = false
              if (this.haveClick) {
                this.haveClick = false
                this.clickPlaybill.haveclick = false
              }
              this.queryChannels(this.currentGenre, 0, this.newDate).then(resps => {
                let totalLength = parseInt(resps['total'], 10)
                if (totalLength === 0) {
                  document.querySelector('.channel-right-content .no_channel_playbill')['style']['display'] = 'block'
                } else {
                  document.querySelector('.channel-right-content .no_channel_playbill')['style']['display'] = 'none'
                }
                document.querySelector('.tvguide-load')['style']['display'] = 'none'
                let channelDtails = this.channelCacheService.getStaticChannelData().channelDetails
                let dynamicChannelData = this.channelCacheService.getDynamicChannelData().channelDynamaicProp
                _.each(resps.channelPlaybills, (item) => {
                  item['channelInfos'] = _.find(channelDtails, (detail) => {
                    return detail['ID'] === item['playbillLites'][0].channelID
                  })
                })
                _.each(resps.channelPlaybills, (item) => {
                  _.find(dynamicChannelData, (detail) => {
                    if (item['channelInfos']['ID'] === detail['ID']) {
                      item['channelInfos']['isLocked'] = detail['isLocked']
                      item['channelInfos']['favorite'] = detail['favorite']
                      item['channelInfos']['channelNO'] = detail['channelNO']
                      let nameSpace = session.get('ott_channel_name_space')
                      let physicalChannelsDynamicProperty =
                                            _.find(detail['physicalChannelsDynamicProperties'], (property) => {
                                              return _.contains(property['channelNamespaces'], nameSpace)
                                            })
                      item['channelInfos']['physicalChannelsDynamicProperty'] =
                                            physicalChannelsDynamicProperty || detail['physicalChannelsDynamicProperties'][0]
                    }
                  })
                })
                for (let i = 0; i < resps.channelPlaybills.length; i++) {
                  resps.channelPlaybills[i]['defaultImgID'] = i
                  this.runChannelName[i] = false
                }
                this.channelPlaybillsList = resps.channelPlaybills
                this.contentHeight = this.channelPlaybillsList.length * 71 + 'px'
                if (totalLength > 12) {
                  this.isShowScrollList = true
                  this.channelListService['pluseOnScroll']()
                } else {
                  this.isShowScrollList = false
                }
                this.genreContentTopDOM.nativeElement.style.top = '0px'
                this.genreScrolltTopDOM.nativeElement.style.top = '0px'
                if (this.newDate && this.newDate['time']) {
                  this.addOptions(this.newDate)
                } else {
                  this.addOptions()
                }
                this.currentToNow = 0
                this.isScrollLoading = false
              })
            }
            if (channelPlaybill.channelInfos) {
              channelPlaybill.channelInfos.favorite = undefined
            } else {
              channelPlaybill.channelDetail.favorite = undefined
            }
            EventService.emit('REMOVE_FAVORITE', channelPlaybill)
            EventService.emit('MINI_REMOVE_FAVORITE', channelPlaybill)
            EventService.emit('REMOVE_successful', {})
          }
      })
      EventService.on('REMOVE_successful', () => {
        if (this.favJudgeI === this.favJudgeIndex) {
            this.favInfo = 'add_Favorite'
          }
      })
    } else {
      let req = {
        favorites: [{
            contentID: (channelPlaybill['channelInfos'] && channelPlaybill['channelInfos']['ID']) ||
                    channelPlaybill['channelDetail']['ID'],
            contentType: 'CHANNEL'
          }],
        autoCover: '0'
      }

      this.commonService.addFavorite(req).then(resp => {
        if (resp['result']['retCode'] === '000000000') {
            if (channelPlaybill.channelInfos) {
              channelPlaybill.channelInfos.favorite = '1'
            } else {
              channelPlaybill.channelDetail.favorite = '1'
            }
            EventService.emit('ADD_FAVORITE', channelPlaybill)
            EventService.emit('MINI_ADD_FAVORITE', channelPlaybill)
            EventService.emit('ADD_successful', {})
            let sessionDynamicData = this.channelCacheService.getDynamicChannelData()
            _.each(sessionDynamicData['channelDynamaicProp'], (list, index) => {
              if (list['ID'] === (channelPlaybill['channelInfos'] &&
                            channelPlaybill['channelInfos']['ID'] || channelPlaybill['channelDetail']['ID'])) {
                list['favorite'] = {}
                list['favorite']['collectTime'] = Number(Date.now()['getTime']())
              }
            })
            let userType
            if (Cookies.getJSON('IS_PROFILE_LOGIN')) {
              userType = Cookies.getJSON('USER_ID')
            } else {
              userType = session.get('Guest')
            }
            session.put(userType + '_Dynamic_Version_Data', sessionDynamicData, true)
            this.channelCacheService.favRefresh(resp['version'], resp['preVersion'])
            if (this.currentGenre && this.currentGenre['name'] === 'livetv_favorite') {
              this.changeGenreByDates(this.currentGenre)
            }
          }
      })
      EventService.on('ADD_successful', () => {
        if (this.favJudgeI === this.favJudgeIndex) {
            this.favInfo = 'cance_Favorite'
          }
      })
    }
  }
    /**
     * manage the state of locked or not,
     * judge in different situations like minibar, tvguide, livetv-favorite genre and so on,
    */
  lockManger (channelPlaybill, indexs?) {
    this.lockJudgeIndex = indexs
    if (channelPlaybill.channelId) {
      for (let i = 0; i < this.channelPlaybillsList.length; i++) {
        if (channelPlaybill.channelId === this.channelPlaybillsList[i]['channelInfos'].ID) {
            channelPlaybill = this.channelPlaybillsList[i]
            break
          }
      }
    }
    if (Cookies.getJSON('IS_GUEST_LOGIN')) {
      EventService.emit('CLOSED_LOGIN_DIALOG')
      EventService.on('MD_DIALOG_CONTAINER', () => {
        document.getElementsByTagName('md-dialog-container')[0]['style'].position = 'fixed'
      })
      this.commonService.openDialog(AccountLoginComponent).then(resp => {
        if (Cookies.getJSON('IS_PROFILE_LOGIN')) {
            this.ngOnInit()
          }
      })
      return
    }
    let channelDtail = channelPlaybill.channelDetail || channelPlaybill['channelInfos']
    if (channelDtail.isLocked !== '1') {
      if (this.profileType === '0') {
        this.commonService.createLock([{ lockType: 'CHANNEL', itemID: channelDtail['ID'] }]).then(resp => {
            if (resp && resp['result'] && resp['result']['retCode'] === '000000000') {
              if (channelPlaybill['channelInfos']) {
                channelPlaybill['channelInfos'].isLocked = '1'
              } else {
                channelPlaybill['channelDetail'].isLocked = '1'
              }
              if (this.lockJudgeI === this.lockJudgeIndex) {
                this.lockInfo = 'unlock'
              }
              EventService.emit('ADD_LOCK', channelPlaybill)
              EventService.emit('ADDLOCK_successful', {})
              let sessionDynamicData = this.channelCacheService.getDynamicChannelData()
              _.each(sessionDynamicData['channelDynamaicProp'], (list, index) => {
                if (list['ID'] === channelPlaybill.channelInfos['ID']) {
                  list['isLocked'] = '1'
                }
              })
              let userType
              if (Cookies.getJSON('IS_PROFILE_LOGIN')) {
                userType = Cookies.getJSON('USER_ID')
              } else {
                userType = session.get('Guest')
              }
              session.put(userType + '_Dynamic_Version_Data', sessionDynamicData, true)
              this.channelCacheService.lockedRefresh(resp['version'], resp['preVersion'])
            }
          })
      } else {
        EventService.emit('CHANNEL_LOCK_TOAST')
      }
    } else {
      let passObject
      passObject = { type: 'lock', name: channelDtail.name }
      this.commonService.openDialog(PassInputDialogComponent, null, passObject)
      EventService.removeAllListeners(['CHECK_PASSWORD'])
      EventService.on('CHECK_PASSWORD', data => {
        if (data.result.retCode === '000000000') {
            this.commonService.unLockChannel([channelDtail['ID']], ['CHANNEL'], true).then(resp => {
              if (resp && resp['result'] && resp['result']['retCode'] === '000000000') {
                if (channelPlaybill['channelInfos']) {
                  channelPlaybill['channelInfos'].isLocked = '0'
                } else {
                  channelPlaybill['channelDetail'].isLocked = '0'
                }
                if (this.lockJudgeI === this.lockJudgeIndex) {
                  this.lockInfo = 'lock'
                }
                EventService.emit('REMOVE_LOCK', channelPlaybill)
                let sessionDynamicData = this.channelCacheService.getDynamicChannelData()
                _.each(sessionDynamicData['channelDynamaicProp'], (list, index) => {
                  if (list['ID'] === channelPlaybill.channelInfos['ID']) {
                    list['isLocked'] = '0'
                  }
                })
                let userType
                if (Cookies.getJSON('IS_PROFILE_LOGIN')) {
                  userType = Cookies.getJSON('USER_ID')
                } else {
                  userType = session.get('Guest')
                }
                session.put(userType + '_Dynamic_Version_Data', sessionDynamicData, true)
                this.channelCacheService.lockedRefresh(resp['version'], resp['preVersion'])
              }
            })
          }
      })
      EventService.removeAllListeners(['CHECK_PASSWORDS'])
      EventService.on('CHECK_PASSWORDS', data => {
        if (data.result.retCode === '000000000') {
            this.commonService.unLockChannel([channelDtail['ID']], ['CHANNEL'], true).then(resp => {
              if (resp && resp['result'] && resp['result']['retCode'] === '000000000') {
                if (channelPlaybill['channelInfos']) {
                  channelPlaybill['channelInfos'].isLocked = '0'
                } else {
                  channelPlaybill['channelDetail'].isLocked = '0'
                }
                if (this.lockJudgeI === this.lockJudgeIndex) {
                  this.lockInfo = 'lock'
                }
                EventService.emit('REMOVE_LOCK', channelPlaybill)
                EventService.emit('UNLOCK_successful', {})
                channelPlaybill['channelInfos'].isLocked = '0'
                if (this.lockJudgeI === this.lockJudgeIndex) {
                  this.lockInfo = 'lock'
                }
                EventService.emit('REMOVE_LOCK', channelPlaybill)
                let sessionDynamicData = this.channelCacheService.getDynamicChannelData()
                _.each(sessionDynamicData['channelDynamaicProp'], (list, index) => {
                  if (list['ID'] === channelPlaybill.channelInfos['ID']) {
                    list['isLocked'] = '0'
                  }
                })
                let userType
                if (Cookies.getJSON('IS_PROFILE_LOGIN')) {
                  userType = Cookies.getJSON('USER_ID')
                } else {
                  userType = session.get('Guest')
                }
                session.put(userType + '_Dynamic_Version_Data', sessionDynamicData, true)
                this.channelCacheService.lockedRefresh(resp['version'], resp['preVersion'])
              }
            })
          }
      })
    }
  }
    /**
     * when the mini detail is visible,first close the mini detail.
     */
  judgeGenre () {
    if (this.haveClick) {
      this.haveClick = false
      this.clickPlaybill.haveclick = false
      return
    }
    if (this.isShowGenreList) {
      this.isShowGenreList = false
      this.isLeaveTitle = true
    } else {
      this.isShowGenreList = true
      this.isLeaveTitle = false
      if (this.genresList.length > 9) {
        this.channelListService['genreOnScroll']()
      }
    }
  }

    /**
     * the mouse enters the scroll
     */
  mouseEnterScroll () {
    this.isLeaveScroll = false
    clearTimeout(this.orderTip)
  }

    /**
     * the mouse enters the list of genres
     */
  mouseEnter () {
    this.isLeaveOrder = false
    clearTimeout(this.orderTip)
  }

    /**
     * the mouse enters the title of current genre
     */
  mouseEnterTitle () {
    this.isLeaveTitle = false
    clearTimeout(this.orderTip)
    this.mouseLeave()
  }

    /**
     * the mouse moves the title of current genre
     */
  mouseLeaveTitle () {
    this.isLeaveTitle = true
    this.mouseLeave()
  }

    /**
     * the mouse moves the list of genres
     */
  mouseLeaveOrder () {
    this.isLeaveOrder = true
    this.mouseLeave()
  }

    /**
     * the mouse moves the scroll
     */
  mouseLeaveScroll () {
    this.isLeaveScroll = true
    this.mouseLeave()
  }

    /**
     * if the genre list is expanded, the mouse moves the title of the genre list, and the list closes after 3 seconds.
     */
  mouseLeave () {
    if (this.isLeaveTitle && this.isLeaveOrder && this.isLeaveScroll) {
      clearTimeout(this.orderTip)
      this.orderTip = setTimeout(() => {
        this.isShowGenreList = false
      }, 3000)
    }
  }
    /**
     * change genre
     */
  changeGenre (genre) {
    if (genre['name'] === 'livetv_favorite' && Cookies.getJSON('IS_GUEST_LOGIN')) {
      EventService.emit('UILOGIN')
      return
    }
    let options = { isIgnoreError: true }
    this._abortAjaxHook.abortAjax('QueryPlaybillList')
    this.currentGenre = genre
    this.changeGenreByDates(this.currentGenre, options)
  }
    /**
     * request fullscreen and play program
     */
  goToPlay (channelPlaybill) {
    let curPlaybill = _.find(channelPlaybill['playbillLites'], item => {
      return item[item['ID'] + 'isCurrent'] === true
    })
    document.querySelector('.channel_login_load_content')['style']['display'] = 'block'
    EventService.emit('OPEN_FULLSCREEN_LIVETVVIDEO', {})
    let video = document.querySelector('#videoContainer video')
      // judge type of browser
    let ua = navigator.userAgent.toLowerCase()
    let isIe = !!ua.match(/msie/i) || !!ua.match(/rv:([\d.]+).*like gecko/)
    let isFirefox = !!ua.match(/firefox\/([\d.]+)/)
    let isEdge = !!ua.match(/edge\/([\d.]+)/)
    if (isIe) {
      video = document.querySelector('#videoContainer')
      video['msRequestFullscreen']()
    } else if (isEdge) {
      video = document.querySelector('#videoContainer')
      video.webkitRequestFullScreen()
    } else if (isFirefox) {
        video = document.querySelector('#videoContainer')
        video['mozRequestFullScreen']()
      } else if (ua.match(/version\/([\d.]+).*safari/)) {
        video.requestFullscreen()
      } else {
        video.webkitRequestFullScreen()
      }
    this.playbillAppService.playProgram(curPlaybill, channelPlaybill['channelInfos'])
  }

    /**
     * notification
     */
  notification (eventName, data) {
    switch (eventName) {
      case 'CHANNEL_VERSION_CHANGED':
          // while first change
        if (this.isFirstChanged) {
          this.isFirstChanged = false
          let isNeedToChangedList = this.channelPlaybillsList
          let IDList
          IDList = this.getChannelIDsBySubjectID(this.currentGenre, IDList, 0, isNeedToChangedList.length + '')
          if (IDList.length === 0) {
              return
            }
          this.queryChannels(this.currentGenre, 0, this.newDate, isNeedToChangedList.length + '').then(resp => {
              let totalLength = parseInt(resp['total'], 10)
              if (totalLength === 0) {
                document.querySelector('.channel-right-content .no_channel_playbill')['style']['display'] = 'block'
              } else {
                document.querySelector('.channel-right-content .no_channel_playbill')['style']['display'] = 'none'
              }
              document.querySelector('.tvguide-load')['style']['display'] = 'none'
              let channelDtails = this.channelCacheService.getStaticChannelData().channelDetails
              let dynamicChannelData = this.channelCacheService.getDynamicChannelData().channelDynamaicProp
              _.each(resp.channelPlaybills, (item) => {
                item['channelInfos'] = _.find(channelDtails, (detail) => {
                  return detail['ID'] === item['playbillLites'][0].channelID
                })
              })
              _.each(resp.channelPlaybills, (item) => {
                _.find(dynamicChannelData, (detail) => {
                  if (item['channelInfos']['ID'] === detail['ID']) {
                    item['channelInfos']['isLocked'] = detail['isLocked']
                    item['channelInfos']['favorite'] = detail['favorite']
                    item['channelInfos']['channelNO'] = detail['channelNO']
                    let nameSpace = session.get('ott_channel_name_space')
                    let physicalChannelsDynamicProperty =
                                        _.find(detail['physicalChannelsDynamicProperties'], (property) => {
                                          return _.contains(property['channelNamespaces'], nameSpace)
                                        })
                    item['channelInfos']['physicalChannelsDynamicProperty'] =
                                        physicalChannelsDynamicProperty || detail['physicalChannelsDynamicProperties'][0]
                  }
                })
              })
              for (let i = 0; i < resp.channelPlaybills.length; i++) {
                resp.channelPlaybills[i]['defaultImgID'] = i
                this.runChannelName[i] = false
              }
              this.channelPlaybillsList = resp.channelPlaybills
              this.addOptions()
              this.isFirstChanged = true
            }, resp => {
              this.isFirstChanged = true
            })
        }
        break
      default:
        break
    }
  }
    /**
    * when exit the component, clear all the timer and events
    */
  ngOnDestroy () {
    if (this.verticalFirstTimeOut) {
      clearTimeout(this.verticalFirstTimeOut)
    }
    clearTimeout(this.orderTip)
    clearTimeout(this.getWeakTip)
    clearTimeout(this.dateToSetNewChannelList)
    clearTimeout(this.programListTimeout)
    clearInterval(this.verticalTimer)
    EventService.removeAllListeners(['SHOW_ALLCHANNEL_PROGRAM'])
    EventService.removeAllListeners(['SHOW_SKIP_TO_NOW'])
    EventService.emit('CLOSED_WEAKTIP')
  }
}
