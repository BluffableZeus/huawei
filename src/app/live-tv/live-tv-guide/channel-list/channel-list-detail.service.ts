import * as _ from 'underscore'
import { Injectable } from '@angular/core'
import { getPlaybillDetail } from 'ng-epg-sdk/vsp'
import { PictureService } from 'ng-epg-ui/services/picture.service'
import { DateUtils } from 'ng-epg-sdk/utils'
import { CommonService } from '../../../core/common.service'
import { session } from 'src/app/shared/services/session'
import { TranslateService } from '@ngx-translate/core'
import { CustomConfigService } from '../../../shared/services/custom-config.service'
import { ChannelCacheService } from '../../../shared/services/channel-cache.service'

@Injectable()
export class ChannelListDetailService {
  constructor (
        private pictureService: PictureService,
        private commonService: CommonService,
        private translate: TranslateService,
        private customConfigService: CustomConfigService,
        private channelCacheService: ChannelCacheService
  ) { }
  /**
     * begin getPlaybillDetail
     */
  getDetail (playbillID, endtimes) {
    let recmscenarios
    let isFuture
    if (endtimes > Date.now()['getTime']()) {
      recmscenarios = [{
        count: '12',
        offset: '0',
        contentType: 'PROGRAM',
        contentID: playbillID,
        entrance: 'Program_Similar_Recommendations',
        businessType: 'BTV'
      }]
      isFuture = true
    } else if (endtimes < Date.now()['getTime']()) {
      recmscenarios = [{
        count: '12',
        offset: '0',
        contentType: 'PROGRAM',
        contentID: playbillID,
        entrance: 'CatchupTV_Similar_Recommendations',
        businessType: 'CUTV'
      }]
      isFuture = false
    }
    return this.customConfigService.getCustomizeConfig({ queryType: '0' }).then(config => {
      let customizeConfig = config['list']
      let nameSpace = customizeConfig['ott_channel_name_space']
      let dynamicData = this.channelCacheService.getDynamicChannelData().channelDynamaicProp
      let staticData = this.channelCacheService.getStaticChannelData().channelDetails
      let options = {
        params: {
          SID: 'playbilldetail3',
          DEVICE: 'PC',
          DID: session.get('uuid_cookie')
        }
      }
      return getPlaybillDetail({
        playbillID: playbillID,
        channelNamespace: nameSpace,
        isReturnAllMedia: '1',
        queryDynamicRecmContent: {
          recmScenarios: recmscenarios
        }
      }, options).then(resp => {
        let weekTime = DateUtils.format(parseInt(resp.playbillDetail.startTime, 10), 'ddd')
        let Month = DateUtils.format(parseInt(resp.playbillDetail.startTime, 10), 'MM')
        let Day = DateUtils.format(parseInt(resp.playbillDetail.startTime, 10), 'D')
        let year = DateUtils.format(parseInt(resp.playbillDetail.startTime, 10), 'YYYY')
        let leftTime = this.commonService.dealWithDateJson('D05', resp.playbillDetail.startTime)
        let nowDay = DateUtils.format(parseInt(Date.now()['getTime'](), 10), 'D')
        let nowMonth = DateUtils.format(parseInt(Date.now()['getTime'](), 10), 'MM')
        let nowYear = DateUtils.format(parseInt(Date.now()['getTime'](), 10), 'YYYY')
        if (nowDay === Day && nowMonth === Month && nowYear === year) {
          leftTime = this.translate.instant('today')
          weekTime = ''
        }
        let dynamicDetail = _.find(dynamicData, (detail) => {
          return detail['ID'] === resp['playbillDetail']['channelDetail']['ID']
        })
        let staticDetail = _.find(staticData, (detail) => {
          return detail['ID'] === resp['playbillDetail']['channelDetail']['ID']
        })

        let url = this.getPictureURL(staticDetail)
        let list = {
          channelNo: dynamicDetail && dynamicDetail['channelNO'] || '',
          logo: url,
          name: resp.playbillDetail.name,
          channel: staticDetail['name'],
          isLock: dynamicDetail && dynamicDetail['isLocked'],
          isFav: dynamicDetail && dynamicDetail['favorite'],
          genre: resp.playbillDetail.genres,
          weekTime: weekTime,
          leftTime: leftTime,
          startTimeorg: resp.playbillDetail.startTime,
          startTime: this.commonService.dealWithDateJson('D10', resp.playbillDetail.startTime),
          endTime: this.commonService.dealWithDateJson('D10', resp.playbillDetail.endTime),
          Series: resp.playbillDetail.playbillSeries,
          introduce: resp.playbillDetail.introduce,
          rating: resp.playbillDetail.rating && resp.playbillDetail.rating.name,
          playbillDetail: resp.playbillDetail,
          recomendList: resp['recmContents'] && resp['recmContents'][0] && resp['recmContents'][0]['recmPrograms'],
          isCUTV: dynamicDetail && dynamicDetail['physicalChannelsDynamicProperties'][0].cutvCR.enable,
          recmContents: resp['recmContents'] && resp['recmContents'][0] && resp.recmContents[0].recmPrograms &&
                    resp.recmContents[0].recmPrograms.length,
          isFuture: isFuture,
          channelDetail: resp.playbillDetail['channelDetail']
        }
        return list
      })
    })
  }

  getPictureURL (staticDetail) {
    return this.pictureService.convertToSizeUrl(staticDetail && staticDetail['picture'] &&
         staticDetail['picture']['icons'] && staticDetail['picture']['icons'][0],
    { minwidth: 40, minheight: 40, maxwidth: 40, maxheight: 40 })
  }

  getLeftTime (Month, Day) {
    let lastMonth = this.commonService.dealWithTime(Month)
    let leftTime
    if (session.get('languageName') === 'zh') {
      leftTime = lastMonth + '/' + Day
    } else {
      leftTime = lastMonth + ' ' + Day
    }
    return leftTime
  }

  getRecomend (recomendList) {
    let list = _.map(recomendList, (data, index) => {
      let url = data['playbillLites'] && data['playbillLites'][0] && data['playbillLites'][0].picture &&
                data['playbillLites'][0].picture.posters && data['playbillLites'][0].picture.posters[0]
      return {
        name: data['playbillLites'][0].name,
        posterUrl: url,
        idType: 'isChannel',
        floatflag: 'moreList',
        floatInfo: data['playbillLites'][0]
      }
    })
    return list = list.slice(0, 10)
  }

  prefixZero (str, len) {
    str = '' + (str || '')
    if (str.length >= len) {
      return str
    }

    let gapLen = len - str.length, i, arr = []
    for (i = 0; i < gapLen; i++) {
      arr[i] = '0'
    }

    return arr.join('') + str
  }

  formatStr (str, index) {
    let weekend
    let day
    let hour
    let minute
    let month
    let allWeekends = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
    let arr = []
    if (str.length === 13) {
      let date = parseInt(str, 10)
      let datestr = new Date(date)
      month = (datestr.getMonth() + 1) < 10 ? '0' + (datestr.getMonth() + 1) : (datestr.getMonth() + 1)
      weekend = datestr.getDay()
      day = datestr.getDate() < 10 ? '0' + datestr.getDate() : datestr.getDate()
      hour = datestr.getHours() < 10 ? '0' + datestr.getHours() : datestr.getHours()
      minute = datestr.getMinutes() < 10 ? '0' + datestr.getMinutes() : datestr.getMinutes()
      weekend = allWeekends[weekend]
    } else {
      month = str.slice(4, 6)
      day = str.slice(6, 8)
      hour = str.slice(8, 10)
      minute = str.slice(10, 12)
    }
    arr.push(month)
    arr.push(weekend)
    arr.push(day)
    arr.push(hour)
    arr.push(minute)
    return arr[index]
  }
}
