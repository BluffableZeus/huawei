import * as _ from 'underscore'
import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core'
import { EventService } from 'ng-epg-sdk/services'
import { session } from 'src/app/shared/services/session'
import { TranslateService } from '@ngx-translate/core'
import { CommonService } from '../../../core/common.service'

@Component({
  selector: 'app-date-list',
  styleUrls: ['./date-list.component.scss'],
  templateUrl: './date-list.component.html'
})

export class DateListComponent implements OnInit {
  @Output() changeDate: EventEmitter<Object> = new EventEmitter()
    /**
     * declare variables
     */

    /**
     * dates to show before 'today'
     */
  public preNum = 0
    /**
     * dates to show after 'today'
     */
  public nextNum = 0
    /**
     * all the dates to show
     */
  public totalNum = 0
  public shownum = 0

  public indexnum = 0
  public nowDateList = []
  public datalist = []

  public allDate = []
  public curDate = ''

    /**
     * the state of left-move arrow
     */
  public timeLeftVisible = 'hidden'
    /**
     * the state of right-move arrow
     */
  public timeRightVisible = 'visible'
    /**
     * variable to judge whether hide or show the left-move and right-move arrows
     */
  public offsetNumber = 0
  public page: number
  public panel: number
  public count = 1
    /**
     * name for modules that would be used in this module
     */
  constructor (
        private translate: TranslateService,
        private commonService: CommonService
    ) {
  }

    /**
     * when the data changed, what to do
     */
  @Input() set setNowToToday (newDate) {
    let self = this
    _.map(this.datalist, function (date) {
      if (date.timeName === 'today') {
        self.curDate = date
        self.changeDate.emit(self.curDate)
      }
    })
  }

    /**
     * initialization
     */
  ngOnInit () {
    EventService.on('CUSTOM_REFRESH_REC_PLAYBILLLENS', () => {
      this.getShowNum()
        // call the translate method, when language setted over.
      _.delay(() => {
        this.showDate()
      }, 1000)
    })
    if (Cookies.getJSON('CUSTOM_REC_PLAYBILLLENS')) {
      this.getShowNum()
      _.delay(() => {
        this.showDate()
      }, 1000)
    }
    this.dateListResult()
      // when the screensize changed
    EventService.on('ScreenSize', () => {
      this.dateListResult()
    })
    this.showButton()
  }

  showButton () {
      // get current web page's width
    let clientW: number = document.body.clientWidth
      // according to different conditions to show or hide button
    if (clientW > 1440) {
      if (this.totalNum <= 14) {
        this.timeLeftVisible = 'hidden'
        this.timeRightVisible = 'hidden'
      }
    } else {
      if (this.totalNum <= 10) {
        this.timeLeftVisible = 'hidden'
        this.timeRightVisible = 'hidden'
      }
    }
  }

  dateListResult () {
    this.offsetNumber = 0
      // the variable to ensure that the offset is integer times of panel
    let clientW: number = document.body.clientWidth
    let windowNum = 14
      // according to clientW to initialize values of panel and page
    if (clientW > 1440) {
      this.panel = 94.42
      windowNum = 14
    } else {
      this.panel = 92
      windowNum = 10
    }

    let prePage = this.preNum >= windowNum / 2 ? this.preNum - windowNum / 2 : 0
    if (this.totalNum <= windowNum) {
      this.page = 0
    } else {
      if (this.nextNum >= (windowNum / 2 - 1)) {
        this.page = prePage
      } else {
        if (this.preNum > windowNum / 2) {
            this.page = this.totalNum - windowNum
          }
      }
    }

    if (this.dom('center-list')) {
      let offset = this.page * this.panel
        // initialize the position to ensure that 'today' is nearly at center of the screen
      offset = Math.round(offset * 100) * 0.01
      this.dom('center-list').style.transform = 'translate(' + '-' + offset + 'px' + ',0px)'
      this.dom('center-list').style.transition = 'all 0s linear'
      this.offsetNumber = -offset
    }
      // hide both arrows when the data of date is not enough to fill the container
    if (this.totalNum <= windowNum) {
      this.timeLeftVisible = 'hidden'
      this.timeRightVisible = 'hidden'
      return
    }
    this.isShowIcon(this.offsetNumber)
  }

  getShowNum () {
    this.indexnum = 0
      /**
         * whether the date scope is platform configuration data
         * the values of preNum and nextNum are firstly got from session, else 7 is default
         */
    this.preNum = parseInt(session.get('REC_PLAYBILL_LEN'), 10) || parseInt('7', 10)
    this.nextNum = parseInt(session.get('PLAYBILL_LEN'), 10) || parseInt('7', 10)
    if (parseInt(session.get('REC_PLAYBILL_LEN'), 10) === 0) {
      this.preNum = 0
    }
    if (parseInt(session.get('PLAYBILL_LEN'), 10) === 0) {
      this.nextNum = 0
    }
    this.totalNum = this.preNum + this.nextNum + 1
    this.shownum = this.totalNum
  }

  addDays (date, days) {
    return this.addMilliseconds(date, days * 864e5)
  }

  addMilliseconds (date, value) {
    let d = new Date(date.getTime())
    d.setMilliseconds(d.getMilliseconds() + value)
    return d
  }

  dateToString (date, fmt) {
      // fmt is the time format that you give
    let o = {
      'M+': date.getMonth() + 1,
      'd+': date.getDate(),
      'H+': date.getHours(),
      'm+': date.getMinutes(),
      's+': date.getSeconds(),
      'S': date.getMilliseconds()
    }
    if (/(y+)/.test(fmt)) {
      fmt = fmt.replace(RegExp.$1, (date.getFullYear() + '').substr(4 - RegExp.$1.length))
    }
    for (let k in o) {
        // satisfy the condition, circularly replace data
      if (new RegExp('(' + k + ')').test(fmt)) {
        fmt = fmt.replace(RegExp.$1, (RegExp.$1.length === 1) ? (o[k]) : (('00' + o[k]).substr(('' + o[k]).length)))
      }
    }
    return fmt
  }

  formatWeek (data) {
    let weekList = ['Sun_tvguide', 'Mon_tvguide', 'Tues_tvguide', 'Wed_tvguide', 'Thur_tvguide', 'Fri_tvguide', 'Sat_tvguide']
    return weekList[data]
  }

  prefixZero (str, len) {
      // change the data type from Number to String
    str = '' + (str || '')
    if (str.length >= len) {
      return str
    }
    let gapLen = len - str.length, i, arr = []
    for (i = 0; i < gapLen; i++) {
      arr[i] = '0'
    }
    return arr.join('') + str
  }

  formatDate (datas) {
    let dateStr = datas.getTime()
    let date = datas.getDate()
    let nowDate = Date.now()['getDate']()
    let datasmonth = datas.getMonth()
    let nowMonth = Date.now()['getMonth']()
    if (datasmonth === nowMonth && date - nowDate === 0) {
      let times = []
      times[0] = 'today'
      times[1] = this.commonService.dealWithDateJson('D07', dateStr)
      return times
    } else {
      let times = []
        // ensure that month and date are double-digit
      let month = this.prefixZero(datas.getMonth() + 1, 2)
      date = this.prefixZero(datas.getDate(), 1)
      let month1 = this.getMonthSimple(month)
      let month2 = this.translate.instant(month1)
        // according to language, the format of time will change
      if (session.get('languageName') === 'zh') {
        times[0] = month2 + '/' + date
      } else {
        times[0] = month2 + ' ' + date
      }
      times[0] = this.commonService.dealWithDateJson('D04', dateStr)
        // times[1] = this.formatWeek(datas.getDay());
      times[1] = this.commonService.dealWithDateJson('D07', dateStr)
      return times
    }
  }

    /**
     * prepare to use TranslateService module for months
     */
  getMonthSimple (month) {
    let monthArray = [
      {
        'mon': '01',
        'monSimple': 'jan_tvguide'
      }, {
        'mon': '02',
        'monSimple': 'feb_tvguide'
      }, {
          'mon': '03',
          'monSimple': 'mar_tvguide'
        }, {
          'mon': '04',
          'monSimple': 'apr_tvguide'
        }, {
          'mon': '05',
          'monSimple': 'may_tvguide'
        }, {
          'mon': '06',
          'monSimple': 'jun_tvguide'
        }, {
          'mon': '07',
          'monSimple': 'jul_tvguide'
        }, {
          'mon': '08',
          'monSimple': 'aug_tvguide'
        }, {
          'mon': '09',
          'monSimple': 'sep_tvguide'
        }, {
          'mon': '10',
          'monSimple': 'oct_tvguide'
        }, {
          'mon': '11',
          'monSimple': 'nov_tvguide'
        }, {
          'mon': '12',
          'monSimple': 'dec_tvguide'
        }
    ]
    for (let i = 0; i < monthArray.length; i++) {
      if (month === monthArray[i].mon) {
        return monthArray[i].monSimple
      }
    }
  }

  getDateList () {
    let datelisttmp = []
    for (let i = 0; i < this.totalNum; i++) {
      let dt = this.addDays(Date.now(), i - this.preNum)
      datelisttmp.push(dt)
    }
    return datelisttmp
  }

  showDate () {
      // get and format current time
    let nowTime = this.dateToString(Date.now(), 'yyyyMMddHHmmss')
    let dateParam = nowTime
    this.nowDateList = []
    let tempDates = this.getDateList()
    let targetDate
    let self = this
    _.each(tempDates, function (list, index) {
      let temp = {
        'timeName': self.formatDate(list)[0],
        'name': self.formatDate(list)[1],
        'dateId': index,
        'time': self.dateToString(list, 'yyyyMMddHHmmss')
      }
      self.allDate.push(temp)
      if (temp.time.substring(0, 8) === nowTime.substring(0, 8)) {
      }
      if (temp.time.substring(0, 8) === dateParam.substring(0, 8)) {
        targetDate = temp
      }
    })
    for (let i = 0; i < this.shownum; i++) {
      this.nowDateList.push(this.allDate[i])
    }
    this.curDate = targetDate
    this.datalist = this.nowDateList
  }

    /**
     * when click the left arrow button
     */
  onClickLeft () {
    this.count--
    let offsetWidth: string
    let clientW: number = document.body.clientWidth
    if (clientW > 1440) {
      this.panel = 94.42
    } else {
      this.panel = 92
    }
    const MINSFFSETNUMBER = 0
      // only when there has hidden date at left, you can click the button
    if (this.offsetNumber <= MINSFFSETNUMBER) {
      this.offsetNumber += this.panel
        // the distance for moving
      offsetWidth = Math.round(this.offsetNumber * 100) * 0.01 + 'px'
        // animate for moving to right
      this.dom('center-list').style.transform = 'translate(' + offsetWidth + ',0px)'
      this.dom('center-list').style.transition = 'all 1s linear'
      this.isShowIcon(this.offsetNumber)
    }
  }

    /**
     * when click the right arrow button
     */
  onClickRight () {
    this.count++
    let offsetWidth: string
    let clientW: number = document.body.clientWidth
    let windowNum = 14
    if (clientW > 1440) {
      windowNum = 14
      this.panel = 94.42
    } else {
      windowNum = 10
      this.panel = 92
    }
    let maxWidth: number = -(this.panel * (this.totalNum - windowNum))
      // only when there has hidden date at right, you can click the button
    if (this.offsetNumber >= maxWidth) {
      this.offsetNumber -= this.panel
        // the distance for moving
      offsetWidth = Math.round(this.offsetNumber * 100) * 0.01 + 'px'
        // animate for moving to left
      this.dom('center-list').style.transform = 'translate(' + offsetWidth + ',0px)'
      this.dom('center-list').style.transition = 'all 1s linear'
      this.isShowIcon(this.offsetNumber)
    }
  }

    /**
     * judge whether show or hide the left and right icons
     * @param offsetNumber the translate distance
     */
  isShowIcon (offsetNumber: number) {
    let offsetNum = Math.round(offsetNumber * 100) * 0.01
    let clientW: number = document.body.clientWidth
    let windowNum = 14
      // according to clientW to initialize values of panel and page
    if (clientW > 1440) {
      windowNum = 14
    } else {
      windowNum = 10
    }
      // when there has no hidden date at left, show the right and hide the left icon
    if (offsetNum === 0) {
      this.timeLeftVisible = 'hidden'
      this.timeRightVisible = 'visible'
        // when there has no hidden date at right, show the left and hide the right icon
    } else if (offsetNum === -(this.panel * (this.totalNum - windowNum))) {
      this.timeLeftVisible = 'visible'
      this.timeRightVisible = 'hidden'
        // when both sides have hidden date, show the left and right icon at same time
    } else {
      this.timeLeftVisible = 'visible'
      this.timeRightVisible = 'visible'
    }
  }

    /**
     * package a function to get element by id
     * @param divName the id of the target element
     */
  dom (divName: string) {
    return document.getElementById(divName)
  }

    /**
     * function to show or hide the element you give
     * @param type id of the element
     * @param isshow the value of visibility attribute
     */
  isLeftOrRightToNow (type, isshow) {
    if (document.getElementById(type)) {
      document.getElementById(type)['style']['visibility'] = isshow
    }
  }

    /**
     * when click date in center-list
     * @param curdate the date you choose
     */
  changeCurDate (curdate) {
      // when the mini detail is visible,first close the mini detail.
    if (document.getElementById('miniDetail') && !document.getElementById('miniDetail')['hidden']) {
        // emit an event to tell the browser to close the mini datail
      EventService.emit('CLOSE_MINIDETAIL')
    }
      // get today's date format first eight number
    let nowTime = this.dateToString(Date.now(), 'yyyyMMddHHmmss').substring(0, 8)
      // if the date you choose is before today
    if (curdate['time'].substring(0, 8) < nowTime) {
      this.isLeftOrRightToNow('leftNow', 'hidden')
      this.isLeftOrRightToNow('rightNow', 'visible')
        // if the date you choose is today
    } else if (curdate['time'].substring(0, 8) === nowTime) {
      this.isLeftOrRightToNow('leftNow', 'hidden')
      this.isLeftOrRightToNow('rightNow', 'hidden')
        // if the date you choose is after today
    } else if (curdate['time'].substring(0, 8) > nowTime) {
        this.isLeftOrRightToNow('leftNow', 'visible')
        this.isLeftOrRightToNow('rightNow', 'hidden')
      }
    this.curDate = curdate
    this.changeDate.emit(curdate)
  }
}
