import { HotGenresService } from './live-tv-home/hot-genres/hot-genres.service'
import { FutureChannelService } from './live-tv-home/future-channel/futureChannel.service'
import { MiniDetailService } from './live-tv-mini-detail/live-tv-mini-detail.service'
import { MiniDetailComponent } from './live-tv-mini-detail/live-tv-mini-detail.component'
import { UISharedModule } from 'ng-epg-ui/webtv-components/shared'
import { ProgramComponentModule } from 'ng-epg-ui/webtv-components/programComponent/program-component.module'
import { WhatOnService } from './live-tv-home/what-on/what-on.service'
import { ChannelDetailService } from './live-tv-detail/live-tv-detail.service'
import { ProgramSuspensionModule } from 'ng-epg-ui/webtv-components/programSuspension/program-suspension.module'
import { FutureChannelMoreService } from './live-tv-home/future-channel/future-channel-more/future-channel-more.service'
import { FutureChannelMoreComponent } from './live-tv-home/future-channel/future-channel-more/future-channel-more.component'
import { HotGenresMoreComponent } from './live-tv-home/hot-genres/hot-genres-more/hot-genres-more.component'
import { ChannelDetailComponent } from './live-tv-detail'
import { NoDataModule } from 'ng-epg-ui/webtv-components/noData/no-data.module'
import { WaterfallChannelModule } from 'ng-epg-ui/webtv-components/waterfall-channel'
import { TvGuideComponent } from './live-tv-guide/tv-guide.component'
import { ChannelListComponent } from './live-tv-guide/channel-list/channel-list.component'
import { DateListComponent } from './live-tv-guide/date-list/date-list.component'
import { LiveTVComponent } from './live-tv-home'
import { HotGenresComponent } from './live-tv-home/hot-genres/hot-genres.component'
import { FutureChannelComponent } from './live-tv-home/future-channel/future-channel.component'
import { WhatOnComponent } from './live-tv-home/what-on/what-on.component'
import { ComponentModule } from '../component/component.module'
import { SharedModule } from '../shared/shared.module'
import { NgModule } from '@angular/core'

import { LiveTvPlayerComponent } from './live-tv-play/livetv-player.component'
import { LiveTVRoutingModule } from './live-tv.routing.module'

import '/src/assets/lib/LibDMPPlayer'
import '/src/assets/lib/DMPPlayer'

@NgModule({
  imports: [
    UISharedModule,
    WaterfallChannelModule,
    NoDataModule,
    ProgramSuspensionModule,
    ProgramComponentModule,
    SharedModule,
    ComponentModule,
    LiveTVRoutingModule
  ],
  exports: [
    LiveTvPlayerComponent
  ],
  declarations: [
    LiveTvPlayerComponent,
    WhatOnComponent,
    FutureChannelComponent,
    FutureChannelMoreComponent,
    HotGenresComponent,
    HotGenresMoreComponent,
    LiveTVComponent,
    DateListComponent,
    ChannelListComponent,
    TvGuideComponent,
    ChannelDetailComponent,
    MiniDetailComponent
  ],
  providers: [
    FutureChannelMoreService,
    ChannelDetailService,
    WhatOnService,
    MiniDetailService,
    FutureChannelService,
    HotGenresService
  ]
})
export class LiveTVModule { }
