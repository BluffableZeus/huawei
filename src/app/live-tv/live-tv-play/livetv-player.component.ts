import * as _ from 'underscore'
import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core'
import { LiveTVPlayBarComponent } from '../../component/liveTVPlayBar/liveTVPlayBar.component'
import { CommonService } from '../../core/common.service'
import { EventService } from 'ng-epg-sdk/services'
import { session } from 'src/app/shared/services/session'
import { PlaybillAppService } from '../../component/playPopUpDialog/playbillApp.service'
import { NPVRAppService } from '../../component/playPopUpDialog/nPvrApp.service'
import { TVODAppService } from '../../component/playPopUpDialog/tVodApp.service'
import { MediaPlayService } from '../../component/mediaPlay/mediaPlay.service'
import { RecordDataService } from '../../component/recordDialog/recordData.service'

@Component({
  selector: 'app-livetv-player',
  styleUrls: ['./livetv-player.component.scss'],
  templateUrl: './livetv-player.component.html'
})

export class LiveTvPlayerComponent implements OnInit, OnDestroy {
  @ViewChild(LiveTVPlayBarComponent) liveTVPlayBar: LiveTVPlayBarComponent
    /**
    * variable definition
    */
  public judgeLivetvPlayer = false
  private getNextProgramTimer: any
  private getNextProgramifNoInputTimer: any
  private oneHourTimer: any
  private oneHourPlaybill: Array<any>
  private pltvCRPlaybill: Array<any>
  private curChannel: Object
  private playURL = ''
  private nextPlayProgram: Object
  private pltvLength: any
  private recordList: Array<any> = []
  private recordCount: any
  private isQueryRecord = false
  private recordOffset = 0
  private rePlaydata
  private timeDiffer
  private channelDetail
  private beginTime
  private overTime
  private nextPlaybills
  private iscoming
    /**
     * name for modules that would be used in this module
     */
  constructor (
        private commonService: CommonService,
        private playbillAppService: PlaybillAppService,
        private tVODAppService: TVODAppService,
        private mediaPlayService: MediaPlayService,
        private recordDataService: RecordDataService,
        private nPVRAppService: NPVRAppService
    ) {
  }
    /**
    * initialization
    */
  ngOnInit () {
      // open the player in  full screen
    EventService.on('OPEN_FULLSCREEN_LIVETVVIDEO', miniDetail => {
      this.judgeLivetvPlayer = true
      this.getMiniDetail(miniDetail)
    })
      // close the player and exit full screen
    EventService.on('CLOSED_FULLSCREEN_LIVETVVIDEO', () => {
      this.resetPlaybar()
      this.judgeLivetvPlayer = false
      document.querySelector('.channel_login_load_content')['style']['display'] = 'none'
        // clear the unlocked program
      session.remove('unlocked_playbill_id')
      this.liveTVPlayBar.closePlayer()
        // set these data empty
      this.recordList = []
      this.recordCount = null
      this.recordOffset = 0
        // clear all the timer
      clearInterval(this.oneHourTimer)
      clearInterval(this.getNextProgramifNoInputTimer)
      clearInterval(this.liveTVPlayBar.fullChannel.seekTimer)
    })
      // load the player's infomation
    EventService.on('LOAD_FULLSCREEN_LIVETVVIDEO', liveVideoData => {
      this.doLoad(liveVideoData)
      this.rePlaydata = liveVideoData
      EventService.emit('closeLiveDialog')
    })
      // when user change the channel from miniEPG or miniDetail, get the next program of the new channel right now
    EventService.on('CHANGE_PLAYING_CHANNEL', liveVideoData => {
      this.playURL = liveVideoData['url']
      this.getNextProgram(liveVideoData)
    })
      // when playing the next program which is under control, check passowrd correctly and play it
    EventService.on('LOCAL_AUTHORIZATE', () => {
      clearInterval(this.getNextProgramifNoInputTimer)
      let liveVideoData = {}
      liveVideoData['url'] = this.playURL
      liveVideoData['playbill'] = this.nextPlayProgram
      liveVideoData['channel'] = this.curChannel
      liveVideoData['type'] = 'LiveTV'
      liveVideoData['isLock'] = false
      liveVideoData['isParentControl'] = false
      this.liveTVPlayBar.playPopUpDialog.enterProfilePwd(liveVideoData)
    })
      // when the guest login successfully in the full screen,refresh the user's infomation and restart to play the channel
    EventService.on('GUEST_TO_LOGIN_LIVETV_PLAYING', liveVideoData => {
      this.liveTVPlayBar.playPopUpDialog.closeDialog('GuestLogin')
      this.liveTVPlayBar.isprofileLogin = Cookies.getJSON('IS_PROFILE_LOGIN')
        // play catch-up tv
      if (liveVideoData['type'] === 'TVOD') {
        this.tVODAppService.playTvod(liveVideoData['playbill'], liveVideoData['channel'])
      } else {
          // play live tv
        this.liveTVPlayBar.isPreview = false
        this.playbillAppService.playProgram(liveVideoData['playbill'], liveVideoData['channel'])
      }
    })
      // get next catch-up tv data, when the user clicks the next button or the previous one have been end
    EventService.on('nextPlaybill', data => {
      this.nextPlaybill(data)
    })
      // get next record task data, when the user clicks the next button or the previous one one have been end
    EventService.on('nextNPVR', data => {
      this.playNextPVR(data)
    })
      //  query the program in the pltvLength if the channel supports seek.
    EventService.on('PLTVCR_CHANNEL', data => {
      this.getPltvCRProgram(data)
    })
      // play the pltv according to the time user selected
    EventService.on('SEEK_PLAYBILL_TIME', data => {
      this.seekplay(data)
    })
      // program detail page send report parameter
      // EventService.on('sendParameter', (recmActionID, entrance) => {
      //
      // });
    EventService.removeAllListeners(['PLAY_LiveTv_FAIL_AND_REFRESH'])
    EventService.on('PLAY_LiveTv_FAIL_AND_REFRESH', () => {
      this.liveTVPlayBar.playPopUpDialog.judgePlayFailLiveTv = false
      this.nPVRAppService.liveTvReplay(this.rePlaydata)
    })
    EventService.removeAllListeners(['forJudgeNextbill'])
    EventService.on('forJudgeNextbill', judgeInfo => {
      this.timeDiffer = Number(judgeInfo['overTime'] - judgeInfo['beginTime'])
      this.channelDetail = judgeInfo['channelDetail']
      this.beginTime = judgeInfo['beginTime']
      this.overTime = judgeInfo['overTime']
      this.nextPlaybills = judgeInfo['nextPlaybill']
      this.iscoming = 1
    })
  }
    /**
     * leave the page clear all the timer
     */
  ngOnDestroy () {
    clearInterval(this.oneHourTimer)
    clearInterval(this.getNextProgramifNoInputTimer)
    clearTimeout(this.getNextProgramTimer)
  }
    /**
     * load the player data
     */
  doLoad (liveVideoData) {
    this.iscoming = 0
    this.curChannel = liveVideoData['channel']
    this.liveTVPlayBar.doLoad(liveVideoData)
  }
    // send the full screen info to mini detail page
  getMiniDetail (miniDetail) {
    miniDetail.isFullScrean = true
  }

    /**
     * Get one hour playbills from now on
     */
  getNextProgram (data) {
    clearInterval(this.oneHourTimer)
    let req = {
      queryPlaybill: {
        type: '0',
        count: '20',
        offset: '0',
        startTime: data['startTime'] || Date.now()['getTime']() + '',
        endTime: Number(Date.now()['getTime']() + 1000 * 3600) + '',
        isFillProgram: '1'
      },
      queryChannel: {
        channelIDs: [data['channelId']],
        isReturnAllMedia: '1'
      },
      needChannel: '0'
    }
    this.playbillAppService.getNextPlayBill(req).then(resp => {
      this.oneHourPlaybill = resp['playbill']
      this.curChannel = resp['channel']
      this.nextPlayProgram = this.oneHourPlaybill[1]
        // if the channel has next playbill in one hour, judging the next playbill being lock or not
      if (this.oneHourPlaybill.length > 1) {
        this.nextProgramIsLocked(this.oneHourPlaybill)
      }
        // set a timer width 59 minutes
      this.oneHourTimer = setInterval(() => {
        if (Number(this.oneHourPlaybill[this.oneHourPlaybill.length - 1]['endTime']) - Date.now()['getTime']() <= 1000 * 3600) {
            this.getNextProgram(data)
          }
      }, 1000 * 60 * 59)
    })
  }

    /**
     * judging the next playbill being lock or not
     */
  nextProgramIsLocked (resp) {
    clearTimeout(this.getNextProgramTimer)
    this.getNextProgramTimer = setTimeout(() => {
        // query the unlocked list to jadge the next program have unlocked
      let isUnlock = _.find(session.get('unlocked_playbill_id'), item => {
        return item === resp[1]['ID']
      })
        // if the next program is not unlocked and under parent control,show the tips of parent control
      if (!isUnlock && resp[1] && Number(resp[1]['rating']['ID']) > Number(session.get('PROFILERATING_ID'))) {
          // close the player
        this.liveTVPlayBar.player['close']()
        this.liveTVPlayBar.isPlaying = false
          // change the program name
        this.liveTVPlayBar.setNameData({ channel: this.curChannel, playbill: resp[1] })
          // show the tips of parent control
        this.liveTVPlayBar.playPopUpDialog.setPopUpAuthorizeEmp({ type: 'NextBTV', playbill: resp[1], isParentControl: true })
          // if the user doesnot input passowrd util the program end,judge the next playbill being lock or not
        clearInterval(this.getNextProgramifNoInputTimer)
        this.getNextProgramifNoInput(resp[1]['endTime'])
          // delete the previous program from the one hour playill list
        this.oneHourPlaybill.shift()
      } else {
        if (this.liveTVPlayBar.playPopUpDialog.judgeAuthorizeEmp || this.liveTVPlayBar.playPopUpDialog.judgeFullScreenPwd) {
            this.liveTVPlayBar.playPopUpDialog.judgeHideDialog('')
            this.playbillAppService.playProgram(this.nextPlayProgram, this.curChannel)
            return
          }
          // change the program name
        this.liveTVPlayBar.setNameData({ channel: this.curChannel, playbill: this.nextPlayProgram })
          // delete the previous program from the one hour playill list
        this.oneHourPlaybill.shift()
          // if one hour list length large than 1,check the next program locked or not
        if (this.oneHourPlaybill.length > 1) {
            this.nextPlayProgram = this.oneHourPlaybill[1]
            this.nextProgramIsLocked(this.oneHourPlaybill)
          }
      }
    }, Number(resp[0]['endTime']) - Date.now()['getTime']())
  }

    /**
     *  if the program is locked and the user doesnot input passowrd all the time,
     *  judge the next playbill being lock or not
     */
  getNextProgramifNoInput (time) {
    this.getNextProgramifNoInputTimer = setInterval(() => {
        // if the user doesnot input passowrd util the program end,judge the next playbill being lock or not
      if (Number(time) >= Date.now()['getTime']() &&
                (this.liveTVPlayBar.playPopUpDialog.judgeFullScreenPwd || this.liveTVPlayBar.playPopUpDialog.isJudgeSubscrEmp)) {
        this.nextProgramIsLocked(this.oneHourPlaybill)
      }
    }, 1000)
  }

    /**
     *  click next button and get the infomation of the next Tvod
     */
  nextPlaybill (pillsDetail) {
    const hurryTime = new Date().getTime()

    if (this.iscoming === 1) {
        // if the next program  does not exist,show the tips of not next one
      if (hurryTime < this.beginTime || (hurryTime - this.beginTime) < this.timeDiffer) {
        this.liveTVPlayBar.fullChannel.noNextTVOD = true
        EventService.emit('NoNextTVOD')
        document.getElementsByClassName('play_loading')[0]['style']['display'] = 'none'
        document.getElementsByClassName('live_load_bg')[0]['style']['display'] = 'none'

          // if the next program exist,play it
      } else {
        this.resetPlaybar()
        this.nextPlaybills[0]['isTvodPlayNext'] = true
        this.tVODAppService.playTvod(this.nextPlaybills[0], this.channelDetail)
      }
    }
  }

  resetPlaybar () {
    this.liveTVPlayBar.closePlayer()
    this.liveTVPlayBar.fullChannel.billDuration = '00:00:00'
    this.liveTVPlayBar.fullChannel.billTotalTime = ' / 00:00:00'
    this.liveTVPlayBar.fullChannel.isBarValid = false
    this.liveTVPlayBar.fullChannel.seekbar['value'] = 0
    this.liveTVPlayBar.fullChannel.seekbar['max'] = 0
    this.liveTVPlayBar.fullChannel.seekbar['style'].background = 'linear-gradient(to right, #FFCD7E 0%, #979797 0%)'
  }
    /**
    *  play next NPVR when it is child of the series or period recording
    */
  playNextPVR (npvr, count?) {
      // set the offset param
    this.recordOffset = count || this.recordOffset
      // if the list is not empty and not loading the data, deal width the record info
    if (this.recordList.length > 0 && !this.isQueryRecord) {
      this.dealNextPVR(this.recordList, npvr, this.recordOffset)
    } else {
        // otherwise,load the data
      let PVRCondition = {
        policyType: ['PlaybillBased', 'TimeBased'],
        layout: { childPlanLayout: '0' },
        singlePVRFilter: {
            scope: 'ALL',
            storageType: 'NPVR',
            status: ['OTHER', 'SUCCESS', 'PART_SUCCESS'],
            isFilterByDevice: '1',
            periodicPVRID: npvr['playbill'].parentPlanID
          }
      }
      let queryPVRRequest = {
        count: '50',
        offset: this.recordOffset + '',
        sortType: 'STARTTIME:ASC',
        PVRCondition: PVRCondition
      }
      let options = {
        params: {
            SID: 'querypvr5'
          }
      }
      this.recordDataService.queryPVR(queryPVRRequest, options).then((resp) => {
        this.recordList = this.recordList.concat(resp.PVRList || [])
        this.recordCount = resp.total
        this.isQueryRecord = false
        this.dealNextPVR(this.recordList, npvr, this.recordOffset)
      })
    }
  }
  dealNextPVR (list, npvr, offset) {
    let npvrData = _.find(list, item => item['startTime'] > npvr['playbill'].startTime)
      // if the next record exist,play it
    if (npvrData) {
      npvrData['id'] = npvrData['ID']
      npvrData['isSeriesPage'] = 'series'
      npvrData['channelName'] = this.getChannelName(npvrData)
      npvrData['fileID'] = npvrData['files'] && npvrData['files'][0] && npvrData['files'][0]['fileID']
      npvrData['isNpvrNext'] = true
      this.resetPlaybar()
      this.nPVRAppService.playNPVR(npvrData)
        // if the record episode list is open, change the current playing record
      if (this.liveTVPlayBar.isEpisodesShow) {
        EventService.emit('CHANGE_PLAYING_NPVR', npvrData)
      }
        // if the record list is not all,laod again
    } else if (this.recordCount > this.recordList.length) {
      this.isQueryRecord = true
      this.playNextPVR(npvr, offset + 50)
    } else {
        // if the next record does not exist, show the tips
      this.liveTVPlayBar.fullChannel.noNextTVOD = true
      EventService.emit('NoNextTVOD')
      if (_.isUndefined(npvr['clickFlag'])) {
          _.delay(() => {
            this.liveTVPlayBar.exitFullScreen()
          }, 800)
        }
    }
  }

  getChannelName (npvrData) {
    return npvrData['extensionFields'] && npvrData['extensionFields'][0] &&
            npvrData['extensionFields'][0].values && npvrData['extensionFields'][0].values[0] ||
            npvrData['playbill'] && npvrData['playbill'].channel && npvrData['playbill'].channel.name
  }

    /**
     * get the program  for the time length of time-shift
     */
  getPltvCRProgram (data) {
    this.pltvLength = Number(data['length'])
    let req = {
      queryPlaybill: {

        type: '0',
        count: '20',
        offset: '0',
        startTime: Number(Date.now()['getTime']() - 1000 * Number(data['length'])) + '',
        endTime: data['endTime'] + '',
        isFillProgram: '1'
      },
      queryChannel: {
        channelIDs: [data['channelId']],
        isReturnAllMedia: '1'
      },
      needChannel: '0'
    }
    this.playbillAppService.getNextPlayBill(req).then(resp => {
      this.pltvCRPlaybill = resp['playbill']
    })
  }
    /**
     * judge the PLTV being lock or not
     */
  isPlaybillLocked (playbill, time, seekTime) {
      // query the unlocked list to jadge the seeked program have unlocked
    let isUnlock = _.find(session.get('unlocked_playbill_id'), item => {
      return item === playbill['ID']
    })
      // change the program's name
    this.liveTVPlayBar.setNameData({ channel: this.curChannel, playbill: playbill })
      // if the seeked program is not unlocked and under parent control,show the tips of parent control
    if (!isUnlock && Number(playbill['rating']['ID']) > Number(session.get('PROFILERATING_ID'))) {
        // show the tips of parent control
      this.liveTVPlayBar.playPopUpDialog.judgeAuthorizeEmp = true
      this.liveTVPlayBar.playPopUpDialog.setPopUpAuthorizeEmp(
          { type: 'PLTV', playbill: playbill, isParentControl: true })
        // when user input the right passowrd, play the seeked program
      EventService.removeAllListeners(['CHECK_PLTV_PLAY_PASSWORD'])
      EventService.on('CHECK_PLTV_PLAY_PASSWORD', () => {
          // if current time exceed seek time , play the playbill from the left side
        if (Date.now()['getTime']() - time > this.pltvLength * 1000) {
            EventService.emit('SEEK_PLAYBILL_TIME',
              { playTime: Date.now()['getTime']() - length * 1000, isLiveTV: false, seekTime: 0, isFirst: false })
            // close all the dialog
            EventService.emit('HIDE_POPUP_DIALOG')
          } else {
            // play the seeked program from seek time after unlocked
            let videoData = {}
            videoData['type'] = 'PLTV'
            videoData['PltvTime'] = seekTime
            videoData['url'] = this.liveTVPlayBar.playURL
            videoData['channel'] = this.curChannel
            videoData['playbill'] = playbill
            EventService.emit('LOAD_FULLSCREEN_LIVETVVIDEO', videoData)
            EventService.emit('HIDE_POPUP_DIALOG')
          }
      })
    } else {
        // play the seeked program from seek time,if not locked
      let videoData = {}
      videoData['type'] = 'PLTV'
      videoData['PltvTime'] = seekTime
      videoData['url'] = this.liveTVPlayBar.playURL
      videoData['channel'] = this.curChannel
      videoData['playbill'] = playbill
      EventService.emit('LOAD_FULLSCREEN_LIVETVVIDEO', videoData)
    }
  }
    /**
     *  get the program of seek time point and play
     */
  seekplay (data) {
      // when seeking, clear all the timers of live tv
    clearTimeout(this.getNextProgramTimer)
    clearInterval(this.oneHourTimer)
    let time = Number(data['playTime'])
    let isLive = data['isLiveTV']
      // query the seeked program is in the pltvCRPlaybill which is laoded at first or not
    let playbill = _.find(this.pltvCRPlaybill, item => {
      return item['startTime'] < time && item['endTime'] > time
    })
      // if the seeked program exist
    if (playbill) {
        // if turn to live tv,play the current playbill
      if (isLive) {
        playbill['isLive'] = isLive
        this.playbillAppService.playProgram(playbill, this.curChannel)
        EventService.emit('CHANGE_PLAYING_CHANNEL',
            { channelId: this.curChannel['ID'], url: this.playURL, startTime: Date.now()['getTime']() + '' })
      } else {
          // if play the next seek
        if (data['isNextPlaybill']) {
            this.playNextPLTVPlaybill(playbill, time, data['seekTime'])
          } else {
            // if the user play time-shift first time, call the playChannel interface to authenticate
            if (data['isFirst']) {
              this.playbillAppService.playPLTVCRpromgram(playbill, this.curChannel, time, this.pltvLength, data['seekTime'])
            } else {
              // if the user play time-shift not first time,check the lock in local
              this.isPlaybillLocked(playbill, time, data['seekTime'])
            }
          }
      }
    } else {
        // query the seeked playbill by calling interface
      let req = {
        queryPlaybill: {
            type: '0',
            count: '1',
            offset: '0',
            startTime: isLive ? Date.now()['getTime']() + '' : Math.floor(time) + '',
            endTime: Date.now()['getTime']() + '',
            isFillProgram: '1'
          },
        queryChannel: {
            channelIDs: [this.curChannel['ID']],
            isReturnAllMedia: '1'
          },
        needChannel: '0'
      }

      this.playbillAppService.getNextPlayBill(req).then(resp => {
        if (isLive) {
            resp['playbill'][0]['isLive'] = isLive
            this.playbillAppService.playProgram(resp['playbill'][0], this.curChannel)
            EventService.emit('CHANGE_PLAYING_CHANNEL',
              { channelId: this.curChannel['ID'], url: this.playURL, startTime: Date.now()['getTime']() + '' })
          } else {
            if (data['isNextPlaybill']) {
              this.playNextPLTVPlaybill(resp['playbill'][0], time, data['seekTime'])
            } else {
              // if the user play time-shift first time, call the playChannel interface to authenticate
              if (data['isFirst']) {
                this.playbillAppService.playPLTVCRpromgram(resp['playbill'][0], this.curChannel,
                  time, this.pltvLength, data['seekTime'])
              } else {
                // if the user play time-shift not first time,check the lock in local
                _.delay(() => {
                  this.isPlaybillLocked(resp['playbill'][0], time, data['seekTime'])
                }, 500)
              }
            }
          }
      })
    }
  }
    /**
     * play next time-shift program
     */
  playNextPLTVPlaybill (playbill, time, seekTime) {
    let isUnlock = _.find(session.get('unlocked_playbill_id'), item => {
      return item === playbill['ID']
    })
    this.liveTVPlayBar.setNameData({ channel: this.curChannel, playbill: playbill })
    if (!isUnlock && playbill && Number(playbill['rating']['ID']) > Number(session.get('PROFILERATING_ID'))) {
      this.liveTVPlayBar.fullChannel.playchannelControl()
        // set the authenticate true,when click the unlocked botton,otherwise the page will disappeared
      this.liveTVPlayBar.playPopUpDialog.judgeAuthorizeEmp = true
        // show the unlocked tips page
      this.liveTVPlayBar.playPopUpDialog.setPopUpAuthorizeEmp({ type: 'PLTV', playbill: playbill, isParentControl: true })
      EventService.removeAllListeners(['CHECK_PLTV_PLAY_PASSWORD'])
      EventService.on('CHECK_PLTV_PLAY_PASSWORD', () => {
          // if current time exceed seek time , play the playbill from the left side
        if (Date.now()['getTime']() - time > this.pltvLength * 1000) {
            EventService.emit('SEEK_PLAYBILL_TIME',
              { playTime: Date.now()['getTime']() - length * 1000, isLiveTV: false, seekTime: 0, isFirst: false })
            EventService.emit('HIDE_POPUP_DIALOG')
          } else {
            let videoData = {}
            videoData['type'] = 'PLTV'
            videoData['PltvTime'] = seekTime
            videoData['url'] = this.liveTVPlayBar.playURL
            videoData['channel'] = this.curChannel
            videoData['playbill'] = playbill
            videoData['playNextpltv'] = true
            EventService.emit('LOAD_FULLSCREEN_LIVETVVIDEO', videoData)
            EventService.emit('HIDE_POPUP_DIALOG')
          }
      })
    }
  }
}
