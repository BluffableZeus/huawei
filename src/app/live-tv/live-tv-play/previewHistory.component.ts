export interface PreviewHistory {
    /**
     * subscriber ID
     */
  subscriberID: string
    /**
     * channel ID
     */
  channelID: string
    /**
     * channel Number
     */
  channelNo: string
    /**
     * channel preview number
     */
  previewTotalCount: number
}
