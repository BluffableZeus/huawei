import * as _ from 'underscore'
import { Injectable } from '@angular/core'
import { searchHotKey } from 'ng-epg-sdk/vsp'

/**
 * filter options in the filter window
 */
export interface FilterSelect {
  isSelected?: boolean
  name: string
  type: string
}

@Injectable()
export class SearchHotKey {
  constructor () { }
  /**
     * the  interface of search the hotkey
     */
  searchHotKey (req: any) {
    return searchHotKey(req).then(resp => {
      return resp
    })
  }

  /**
     * traverse the recommended hot words
     */
  mapHotKeyWords (hotKeys) {
    return _.map(hotKeys, (hotKey) => {
      return {
        name: hotKey
      }
    })
  }

  /**
    * traverse the history record
    */
  mapHistoryKeyWords (hotKeys) {
    return _.map(hotKeys, (hotKey) => {
      return {
        name: hotKey['VOD']['name'],
        id: hotKey['VOD']['ID'],
        time: hotKey['VOD'].bookmark.updateTime
      }
    })
  }
}
