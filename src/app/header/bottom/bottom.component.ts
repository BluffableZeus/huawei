
import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { config } from '../../shared/services/config'

@Component({
  selector: 'app-bottom-routes',
  styleUrls: ['./bottom.component.scss'],
  templateUrl: './bottom.component.html'
})

export class BottomRoutesComponent implements OnInit {
  public title = ''
  public version

  constructor (public route: ActivatedRoute) {
  }

    /**
     * init the content by titile
     */
  ngOnInit () {
    this.route.params.subscribe(params => {
      this.title = params['title']
    })
    this.version = { 'num': config.Version }
  }
}
