import * as _ from 'underscore'
import { Component, EventEmitter, Output, OnInit } from '@angular/core'
import { queryBookmark } from 'ng-epg-sdk/vsp'
import { EventService } from 'ng-epg-sdk/services'
import { session } from 'src/app/shared/services/session'
import { Router } from '@angular/router'
import { VODRouterService } from '../../shared/services/vod-router.service'
import { TranslateService } from '@ngx-translate/core'
import { SearchHotKey } from '../hotkey.service'
import { SearchHeaderComponent } from '../search/search.component'
import { deleteBookmark } from 'ng-epg-sdk/vsp'

@Component({
  selector: 'app-history-header',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss']
})

export class HistoryHeaderComponent implements OnInit {
  @Output() hideFromHis: EventEmitter<Object> = new EventEmitter()
  public historyVisible = 'none'
  public historyKeys = []
  public dataDisplay: boolean
  public noDataDisplay: boolean
  public moreDisplay: boolean
  public historyVOD: any = {}
  public historyVODs = []
  public hasLeave = false
  public finishDelete = true

  ngOnInit () {
    let self = this
    EventService.on('LOGIN', function () {
      if (Cookies.getJSON('IS_PROFILE_LOGIN')) {
          // if the user logins,show the history buttom.
        self.historyVisible = 'block'
      } else {
          // if the user doesn't login,hide the history buttom.
        self.historyVisible = 'none'
      }
    })
  }

  constructor (
        private searchHotKey: SearchHotKey,
        private router: Router,
        private vodRouter: VODRouterService,
        private translate: TranslateService,
        private searchHeader: SearchHeaderComponent
    ) {
  }

    /**
     * deal with the cast roles.
     */
  dealRoles (bookmark) {
    let castRoles = bookmark['castRoles']
    let bookmarkRole = {
      actors: '',
      directors: ''
    }
      // get all actors.
    _.map(castRoles, function (role, index) {
      if (parseInt(role['roleType'], 10) === 0) {
          // the role type of actor is 0.
        for (let i = 0; i < role['casts'].length; i++) {
            // separate all actors by a comma.
            bookmarkRole['actors'] = bookmarkRole['actors'] + role['casts'][i].castName + ' , '
          }
      }
    })
      // remove the last comma.
    bookmarkRole['actors'] = bookmarkRole['actors'].slice(0, bookmarkRole['actors'].lastIndexOf(','))
      // get all directors.
    _.map(castRoles, function (role, index) {
      if (parseInt(role['roleType'], 10) === 1) {
          // the role type of director is 1.
        for (let i = 0; i < role['casts'].length; i++) {
            // separate all directors by a comma.
            bookmarkRole['directors'] = bookmarkRole['directors'] + role['casts'][i].castName + ' , '
          }
      }
    })
      // remove the last comma.
    bookmarkRole['directors'] = bookmarkRole['directors'].slice(0, bookmarkRole['directors'].lastIndexOf(','))

    return bookmarkRole
  }

    /*
    * deal with history record detail
    */
  dealWithHistory (bookmarks?) {
    let self = this
    if (bookmarks) {
      let marks = []
      marks.push(bookmarks)
      let state = ''
      let play = ''
      let historyVOD = _.map(marks, (bookmark) => {
        let vodBookmark = Number(bookmark.bookmark.rangeTime)
        let vodElapseTime = bookmark.mediaFiles && bookmark.mediaFiles[0] && bookmark.mediaFiles[0].elapseTime
            ? Number(bookmark.mediaFiles[0].elapseTime) : 0
        if (vodBookmark >= vodElapseTime) {
            // if the rangeTime is more than the elapseTime,it is finished.
            state = this.translate.instant('finished')
            play = this.translate.instant('replay')
          } else {
            // if the rangeTime is more than the elapseTime,it is not finished.
            let timeDiff = Math.ceil((vodElapseTime - vodBookmark) / 60)
            state = this.translate.instant('min_left', { min: timeDiff })
            play = this.translate.instant('play')
          }
        let bookmarkRole = self.dealRoles(bookmark)
        let url = bookmark.picture && bookmark.picture.posters && bookmark.picture.posters[0]
        return {
            title: bookmark.name,
            posters: url,
            director: bookmarkRole.directors || '/',
            actor: bookmarkRole.actors || '/',
            id: bookmark.ID,
            state: state,
            play: play,
            time: bookmark.bookmark.updateTime
          }
      })
        // the first history VOD.
      this.historyVOD = historyVOD[0]
    }
  }

    /*
    * when the mouse put in the history buttom,get history record detail
    */
  showHistorys () {
    this.searchHeader.inputBlur()
    this.hasLeave = false
    queryBookmark({
      bookmarkTypes: ['VOD'],
      count: '50',
      offset: '0'
    }).then(resp => {
      if (!this.hasLeave) {
          // only when there returns values and the mouse does't leave,show the list.
          if (resp && resp['bookmarks'].length > 0) {
            this.dataDisplay = true
            this.noDataDisplay = false
            this.dealWithHistory(resp['bookmarks'][0]['VOD'])
            this.historyVODs = resp.bookmarks
            let list = resp.bookmarks.slice(1, 10)
            this.historyKeys = this.searchHotKey.mapHistoryKeyWords(list)
            if (resp.bookmarks.length > 10) {
              // when the number of bookmarks is more than 10,show the more buttom.
              this.moreDisplay = true
            } else {
              // when the number of bookmarks is less than 10,hide the more buttom.
              this.moreDisplay = false
            }
          } else {
            this.historyVODs = []
            this.dataDisplay = false
            this.noDataDisplay = true
            this.moreDisplay = false
          }
        }
    })
  }

    /**
     * click the history buttom and jump to the history page of MYTV.
     */
  toHistoryVod () {
    EventService.emit('HISTORYVOD', 'historyVod')
    this.router.navigate(['./profile/historyVod'])
    session.put('RouteMessage', './profile/historyVod')
  }

    /**
     * when the mouse leave the list or the history buttom, hide the list.
     */
  hideHistory () {
    this.hasLeave = true
    this.dataDisplay = false
    this.noDataDisplay = false
    this.moreDisplay = false
  }

    /**
     * when the mouse put in the list,show the list.
     */
  showHistoryList () {
    if (this.historyVODs.length > 0) {
      this.dataDisplay = true
      this.noDataDisplay = false
    } else {
      this.dataDisplay = false
      this.noDataDisplay = true
    }
    if (this.historyVODs.length > 10) {
        // when the number of bookmarks is more than 10,show the more buttom.
      this.moreDisplay = true
    } else {
        // when the number of bookmarks is less than 10,hide the more buttom.
      this.moreDisplay = false
    }
  }

    /**
     * click the play buttom to play this VOD.
     */
  detail (data) {
    this.hideHistory()
    this.vodRouter.navigate(data.id)
  }

    /**
     * detele history record
     */
  deleteData (data, indexs?) {
    let self = this
    if (self.historyKeys.length !== 0 || self.historyVOD) {
      let req = {
        'bookmarkTypes': ['VOD'],
        'itemIDs': [data.id]
      }
      if (self.finishDelete) {
        self.finishDelete = false
        self.deleteHistoryVod(req).then(resp => {
            if (resp['result']['retCode'] === '000000000') {
              EventService.emit('DELETE_HISTORY', data)
              // find the VOD deleted.
              let vodData = _.find(self.historyVODs, item => {
                return item['VOD']['ID'] === data.id
              })
              // get the index of this VOD deleted.
              let index = _.indexOf(self.historyVODs, vodData)
              // delete this VOD from the all history VODs.
              self.historyVODs.splice(index, 1)
              if (index > 0) {
                // delete historys except the first.
                self.historyKeys = self.searchHotKey.mapHistoryKeyWords(self.historyVODs.slice(1, 10))
              } else {
                // delete the first VOD.
                if (self.historyKeys && self.historyKeys.length > 0) {
                  // find the next VOD
                  let vodDatas = _.find(self.historyVODs, item => {
                    return item['VOD']['ID'] === self.historyKeys[0]['id']
                  })
                  // show the next VOD as the first.
                  self.dealWithHistory(vodDatas['VOD'])
                  self.historyKeys = self.searchHotKey.mapHistoryKeyWords(self.historyVODs.slice(1, 10))
                } else {
                  // only one.
                  self.historyVOD = {}
                  self.noDataDisplay = true
                  self.dataDisplay = false
                }
              }
              if (self.historyVODs.length > 10) {
                // when the number of bookmarks is more than 10,show the more buttom.
                self.moreDisplay = true
              } else {
                // when the number of bookmarks is less than 10,hide the more buttom.
                self.moreDisplay = false
              }
            }
            self.finishDelete = true
          }, respFail => {
            self.finishDelete = true
          })
      }
    }
  }

    /**
     * query the deleteBookmark.
     */
  deleteHistoryVod (req: any) {
    return deleteBookmark(req).then(resp => {
      return resp
    })
  }
}
