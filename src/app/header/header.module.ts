import { BottomRoutesComponent } from './bottom/bottom.component'
import { CommonModule } from '@angular/common'
import { FormsModule } from '@angular/forms'
import { ProfileHeaderComponent } from './profile/profile.component'
import { LanguageHeaderComponent } from './language/language.component'
import { HistoryHeaderComponent } from './history/history.component'
import { SearchHeaderComponent } from './search/search.component'
import { HeaderComponent } from './header.component'
import { NgModule } from '@angular/core'
import { HeaderRoutingModule } from './header-routing.module'
import { SearchHotKey } from './hotkey.service'
import { SharedModule } from '../shared/shared.module'

@NgModule({
  imports: [
    FormsModule,
    CommonModule,
    SharedModule,
    HeaderRoutingModule
  ],
  exports: [
    HeaderComponent
  ],
  declarations: [
    HeaderComponent,
    SearchHeaderComponent,
    HistoryHeaderComponent,
    LanguageHeaderComponent,
    ProfileHeaderComponent,
    BottomRoutesComponent
  ],
  providers: [
    SearchHotKey,
    SearchHeaderComponent
  ]
})
export class HeaderModule { }
