import { Component, EventEmitter, Output, OnInit, OnDestroy } from '@angular/core'
import { Router } from '@angular/router'
import { session } from 'src/app/shared/services/session'
import { EventService } from 'ng-epg-sdk/services'
import { config } from '../../shared/services/config'
import { SearchHeaderComponent } from '../search/search.component'
import { Store } from '@ngrx/store'

import { APP_LOGOUT } from '../../store/app'

@Component({
  selector: 'app-profile-header',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})

export class ProfileHeaderComponent implements OnInit, OnDestroy {
  @Output() hideFromPro: EventEmitter<Object> = new EventEmitter()
    /**
     * display user navigation list or not
     */
  public profileDisplay: boolean
    /**
     * url of profile logo
     */
  readonly urlToIconProfile = 'assets/img/ui/login.svg';
  public profilelogoURL;
  public loginProfile = {}
    /**
     * name of login profile
     */
  public loginName = ''
  public hideProfileTimeout
  public recordingMode = config.PVRMode
    /**
     * the login profile is main profile or not
     */
  public isMainProfile = true

  constructor (
        private router: Router,
        private searchHeader: SearchHeaderComponent,
        private store: Store<any>
    ) {
      this.profilelogoURL = this.urlToIconProfile;
  }

    /**
     * init the profile header
     */
  ngOnInit () {
    this.loginProfile = {}
    this.profileDisplay = false

    EventService.on('LOGIN', () => this.getProfileLogoURL())
    EventService.on('UILOGOUT', () => this.onClickLogout())
    EventService.removeAllListeners(['CHANGE_PROFILE_AVATAR'])
    EventService.on('CHANGE_PROFILE_AVATAR', () => this.getProfileLogoURL())
  }

  ngOnDestroy () {
    if (this.hideProfileTimeout) {
      clearTimeout(this.hideProfileTimeout)
    }
  }

    /**
     * click on profile logo
     *
     * 1. if the profile is logged in, the page will jump to My TV.
     *
     * 2. if the profile is not logged in, it will display login box.
     */
  loginOrGoToMy () {
    if (Cookies.getJSON('IS_PROFILE_LOGIN')) {
      this.toHistoryVod()
    } else {
      EventService.emit('UILOGIN')
    }
  }

    /**
     * get the url of profile logo
     */
  getProfileLogoURL () {
    this.profilelogoURL = (Cookies.getJSON('LOGIN_PROFILE_LOGOURL')
        ? 'assets/img/avatars/' + Cookies.getJSON('LOGIN_PROFILE_LOGOURL') + '.png'
        : 'assets/img/default/ondemand_casts.png')
    if (Cookies.getJSON('IS_GUEST_LOGIN')) {
      this.profilelogoURL = this.urlToIconProfile;
    }
  }

    /**
     * show user navigation list
     */
  showProfile () {
    if (Cookies.getJSON('IS_PROFILE_LOGIN')) {
      let profileType = Cookies.get('PROFILE_TYPE')
      if (profileType === '0') {
        this.isMainProfile = true
      } else {
        this.isMainProfile = false
      }
      clearTimeout(this.hideProfileTimeout)
      this.searchHeader.inputBlur()
      this.profileDisplay = true
      this.loginName = session.get('LOGIN_NAME')
      EventService.emit('HIGH_LIGHT_NAME', '1')
    }
  }

    /**
     * hide user navigation list
     */
  hideProfile () {
    clearTimeout(this.hideProfileTimeout)
    this.hideProfileTimeout = setTimeout(function () {
        this.profileDisplay = false
        EventService.emit('HIGH_LIGHT_NAME', '0')
      }.bind(this), 400)
  }

    /**
     * remove profile Cookies
     * when logout or open new chrome page
     */
  removeSession () {
    Cookies.remove('IS_PROFILE_LOGIN')
    Cookies.remove('IS_GUEST_LOGIN')
    Cookies.remove('PROFILE_NAME')
    session.remove('CUSTOM_CONFIG_DATAS')
    Cookies.remove('LOGIN_PROFILE_LOGOURL')
    Cookies.remove('USER_ID')
    Cookies.remove('CUSTOM_REC_PLAYBILLLENS')
    Cookies.remove('PROFILE_TYPE')
  }

    /**
     * when click on 'Log Out' item, the user will exit the login.
     */
  onClickLogout () {
    EventService.emit('AUTHENTICATE_LOGOUT')
    this.store.dispatch({ type: APP_LOGOUT })
    this.hideProfile()
    this.router.navigate(['/home'])
    config['isLogout'] = true
//    let self = this
//      // the type send 1 is wrong in the document
//    this.router.navigate(['/home'])
//    config['isLogout'] = true
//    if (this.profileDeactivate.isBasicInfoChanged || this.profileDeactivate.isPwdInfoChanged ||
//            this.deviceDeactivate.isInfoChanged || this.preferenceDeactivate.isPreferenceChanged ||
//            this.recordDeactivate.conditionChanged) {
//      session.put('RouteMessage', './home')
//      session.put('PROFILE_NOTSAVE_LOGOUT_TYPE', true)
//
//      EventService.on('PROFILE_NOTSAVE_LOGOUT', function () {
//        session.remove('PROFILE_NOTSAVE_LOGOUT_TYPE')
//        const logoutReq = { type: '1' }
//        logout(logoutReq).then(resp => {
//            if (resp) {
//              self.removeSession()
//              session.clear(false)
//              location.reload()
//            }
//          }, resp => {
//            if (resp && resp.result && ['157021002', '125023001'].includes(resp.result.retCode)) {
//              self.removeSession()
//              session.clear(false)
//              location.reload()
//            }
//          })
//      })
//    } else {
//      const logoutReq = { type: '1' }
//      logout(logoutReq).then(resp => {
//        if (resp) {
//            self.removeSession()
//            session.clear(false)
//            location.reload()
//          }
//      }, resp => {
//        if (resp && resp.result && ['157021002', '125023001'].includes(resp.result.retCode)) {
//            self.removeSession()
//            session.clear(false)
//            location.reload()
//          }
//      })
//    }
  }

    /**
     * when click on history item, the page jump to history page of my TV.
     */
  toHistoryVod () {
    EventService.emit('HISTORYVOD', 'historyVod')
    this.router.navigate(['./profile/historyVod'])
    session.put('RouteMessage', './profile/historyVod')
  }

    /**
     * when click on 'Favorite Videos' item, the page jump to favorite videos page of my TV.
     */
  toFavoriteVod () {
    EventService.emit('FAVORITEVOD', 'favoriteVod')
    this.router.navigate(['./profile/favoriteVod'])
    session.put('RouteMessage', './profile/favoriteVod')
  }

    /**
     * when click on 'Favorite Channels' item, the page jump to favorite channels page of my TV.
     */
  toFavoriteChannels () {
    EventService.emit('FAVORITECHANNELS', 'favoriteChannels')
    this.router.navigate(['./profile/favoriteChannels'])
    session.put('RouteMessage', './profile/favoriteChannels')
  }

    /**
     * when click on 'Locked Channels' item, the page jump to locked channels page of my TV.
     */
  toLockedChannels () {
    EventService.emit('LOCKEDCHANNELS', 'lockedChannels')
    this.router.navigate(['./profile/lockedChannels'])
    session.put('RouteMessage', './profile/lockedChannels')
  }

    /**
     * when click on 'Reminders' item, the page jump to reminders page of my TV.
     */
  toReminder () {
    if (config.reminderMode) {
      EventService.emit('REMINDER', 'reminder')
      this.router.navigate(['./profile/reminder'])
      session.put('RouteMessage', './profile/reminder')
    }
  }

    /**
     * when click on 'Recordings' item, the page jump to recordings page of my TV.
     */
  toRecording () {
    if (this.recordingMode !== '') {
      EventService.emit('RECORDING', 'recording')
      this.router.navigate(['./profile/recording'])
      session.put('RouteMessage', './profile/recording')
    }
  }

    /**
     * when click on 'Purchased' item, the page jump to purchased page of my TV.
     */
  toMySubscribed () {
    EventService.emit('PURCHASED', 'purchased')
    this.router.navigate(['./profile/my_subscriptions'])
    session.put('RouteMessage', './profile/my_subscriptions')
  }

    /**
     * when click on 'All Subscribed' item, the page jump to purchased page of my TV.
     */
  toAllSubscriptions () {
      // EventService.emit('PURCHASED', 'purchased');
    this.router.navigate(['./profile/all_subscriptions'])
    session.put('RouteMessage', './profile/all_subscriptions')
  }

    /**
     * navigate to promocodes menu.
     */
  toPromocodes () {
      // EventService.emit('PURCHASED', 'purchased');
    this.router.navigate(['./profile/promocodes'])
    session.put('RouteMessage', './profile/promocodes')
  }

    /**
     * navigate to social accounts menu.
     */
  toSocial () {
      // EventService.emit('PURCHASED', 'purchased');
    this.router.navigate(['./profile/social'])
    session.put('RouteMessage', './profile/social')
  }

    /**
     * navigate to sub-profile page.
     */
  toSubProfile () {
    EventService.emit('SUBPROFILE', 'sub-profile')
    this.router.navigate(['./profile/sub-profile'])
    session.put('RouteMessage', './profile/sub-profile')
  }

    /**
     * when click on 'Device Name' item, the page jump to device name page of my TV.
     */
  toDeviceName () {
    EventService.emit('DEVICENAME', 'device-name')
    this.router.navigate(['./profile/devices'])
    session.put('RouteMessage', './profile/devices')
  }

    /**
     * when click on 'Preference Settings' item, the page jump to preference settings page of my TV.
     */
  toPreference () {
    EventService.emit('DEVICENAME', 'preferences')
    this.router.navigate(['./profile/preferences'])
    session.put('RouteMessage', './profile/preferences')
  }
  nofind (img) {
    img.src = 'assets/img/default/ondemand_casts.png'
    img.onerror = null
  }

    /**
     * when click on the user id, the page jump to history page of my TV.
     */
  jumptomy () {
    this.router.navigate(['./profile/historyVod'])
    session.put('RouteMessage', './profile/historyVod')
    EventService.emit('HISTORYVOD', 'historyVod')
  }
}
