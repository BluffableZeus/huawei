import * as _ from 'underscore'
import { Component, OnDestroy, OnInit, ElementRef, ViewChild } from '@angular/core'
import { SearchHotKey } from '../hotkey.service'
import { queryRecmVODList } from 'ng-epg-sdk/vsp'
import { session } from 'src/app/shared/services/session'
import { Router } from '@angular/router'
import { VODRouterService } from 'src/app/shared/services/vod-router.service'
import { EventService } from 'ng-epg-sdk/services'

@Component({
  selector: 'app-search-header',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})

export class SearchHeaderComponent implements OnDestroy, OnInit {
    /*
    * hot key word for search
    */
  public searchKey = ''
  public keyWordsVisible = 'hidden'
    /**
     * array of hot keys
     */
  public hotKeys
  public currentKey: string
  public hotVOD: any = {}
  public hotfilter = false
  public hotVodfilter = false
  public filterlist = false
  public getSuggestKeyword
  public lostBlur = true
  public mobile = false
  public onchange = 0
  public blackListCache = ''
  public searchHotkey = false

  @ViewChild('search') searchElement: ElementRef

  /**
   * @property {boolean} classActive Show/Hide search field
   */
  public classActive: boolean = false

  constructor (
        private searchHotKey: SearchHotKey,
        private route: Router,
        private vodRouter: VODRouterService
    ) {
  }

  ngOnInit () {
    let self = this
      // hide the drop down list when another drop down list show
    EventService.removeAllListeners(['HIDE_SEARCH_KEY'])
    EventService.on('HIDE_SEARCH_KEY', function () {
      self.hideSearch()
      self.inputBlur()
    })
    EventService.removeAllListeners(['SEARCH_LOCATION_HASH_KEY'])
    EventService.on('SEARCH_LOCATION_HASH_KEY', function () {
      self.isSearchIn()
    })
      // get the hotkey
    EventService.removeAllListeners(['HOTKEY_WORD'])
    EventService.on('HOTKEY_WORD', (hotKeyword) => {
      if (self.searchKey !== session.get('HOTKEYWORD')) {
        session.put('HOTKEYWORD', hotKeyword)
        self.searchKey = hotKeyword
      } else {
        session.put('HOTKEYWORD', hotKeyword)
        if (self.onchange === 0 && !self.lostBlur) {
          self.searchKey = ''
        } else {
          self.searchKey = session.get('HOTKEYWORD')
        }
      }
    })
    this.blackListCache = session.get('COMMON_INPUT_BLACKLIST_CACHE')
    this.searchKey = session.get('HOTKEYWORD')
      // to prevent the hotkey do not confirm to the search result when refresh page
    EventService.on('searchAllId', function (id) {
      if (id !== '') {
        self.searchKey = id
      }
    })
    this.isSearchIn()
  }
  dealWithVisible (ifVisible) {
    if (ifVisible === 'visible' && this.keyWordsVisible !== 'visible') {
      this.keyWordsVisible = 'visible'
    } else if (ifVisible === 'hidden' && this.keyWordsVisible !== 'hidden') {
      this.keyWordsVisible = 'hidden'
    }
  }
  dealRoles (recmVOD) {
    let castRoles = recmVOD['castRoles']
    let recmVODRole = {
      actors: '',
      directors: ''
    }
    _.map(castRoles, function (role, index) {
      if (parseInt(role['roleType'], 10) === 0) {
        recmVODRole['actors'] = recmVODRole['actors'] + role['casts'][0].castName + ' , '
      }
    })
    recmVODRole['actors'] = recmVODRole['actors'].slice(0, recmVODRole.actors.length - 3)

    _.map(castRoles, function (role, index) {
      if (parseInt(role['roleType'], 10) === 1) {
        recmVODRole['directors'] = recmVODRole['directors'] + role['casts'][0].castName + ' , '
      }
    })
    recmVODRole['directors'] = recmVODRole['directors'].slice(0, recmVODRole.actors.length - 3)

    return recmVODRole
  }

    /**
     * deal with VOD data that is show in drop down list
     */
  dealWithRecmVOD (recmVOD) {
    let hotVOD = _.map(recmVOD, (VOD) => {
      let role = this.dealRoles(VOD)
      return {
        title: VOD['name'],
        posters: VOD['picture'] && VOD['picture'].posters && VOD['picture'].posters[0],
        director: role['directors'] || '/',
        actor: role['actors'] || '/',
        id: VOD['ID']
      }
    })
    this.hotVOD = hotVOD[0]
  }

    /**
     *  show search box when input get focus
     */
  showSearch () {
    this.blackListCache = session.get('COMMON_INPUT_BLACKLIST_CACHE')
    EventService.emit('onBlur', true)
    this.lostBlur = false
      // if the word in search box equal to the hotkey empty the search box after click search box
    if (this.searchKey === session.get('HOTKEYWORD') && !this.searchHotkey) {
      this.searchKey = ''
      this.hotKeys = []
      session.put('IS_DEFAULT_HOT_KEYWORD', true)
      queryRecmVODList({
        count: '1',
        offset: '0',
        action: '4',
        sortType: 'PLAYTIMES:DESC'
      }).then(resp => {
        session.remove('IS_DEFAULT_HOT_KEYWORD')
        if (resp && resp.VODs && resp.VODs[0]) {
          this.dealWithRecmVOD(resp.VODs)
          this.hotVodfilter = true
        } else {
          this.hotVodfilter = false
        }
        this.searchHotKey.searchHotKey({
          count: '9',
          offset: '0'
        }).then(resp1 => {
          if (resp1 && resp1.hotKeys) {
            this.dealWithVisible('visible')
            this.filterlist = true
            this.hotKeys = this.mapSuggestWords(resp1.hotKeys)
          } else {
            this.filterlist = false
            this.dealWithVisible('hidden')
          }
          this.gethotfilterBoolean()
            // to prevent when mouse click search box and move to history fast,
            // the search box will blur but the search recommended is show due to the call interface slowly
          if (document.getElementById('input-text-overflow') !== document.activeElement) {
            this.hotfilter = false
          }
        })
      }, respFail => {
        session.remove('IS_DEFAULT_HOT_KEYWORD')
      })
    } else {
        // the word in search box not equal to hotkey
      clearTimeout(this.getSuggestKeyword)
      this.getSuggestKeyword = setTimeout(() => {
        queryRecmVODList({
            count: '1',
            offset: '0',
            action: '4',
            sortType: 'PLAYTIMES:DESC'
          }).then(resp => {
            if (resp && resp.VODs && resp.VODs[0]) {
              this.dealWithRecmVOD(resp.VODs)
              this.hotVodfilter = true
            } else {
              this.hotVodfilter = false
            }
            this.searchHotKey.searchHotKey({
              count: '9',
              offset: '0'
            }).then(resp1 => {
              if (resp1 && resp1.hotKeys) {
                this.dealWithVisible('visible')
                this.filterlist = true
                this.hotKeys = this.mapSuggestWords(resp1.hotKeys)
              } else {
                this.filterlist = false
                this.dealWithVisible('hidden')
              }
              this.gethotfilterBoolean()
            })
          })
      }, 1000)
    }
  }
  gethotfilterBoolean () {
    if (this.hotVodfilter || this.filterlist) {
      this.hotfilter = true
    }
  }

    /**
     * when the input blur, the method will be run, and the drop down list will pack up
     */
  hideSearch () {
    let self = this
    this.classActive = false
    EventService.emit('onBlur', false)
    this.searchElement.nativeElement.value = ''
    this.lostBlur = true
    _.delay(function () {
      self.hotfilter = false
      if (self.searchKey === '') {
        self.searchKey = session.get('HOTKEYWORD')
      }
        // to prevent mouse move to search box fast and click it due to time delay lead to the word in search box brighten
        // when mouse on profile or history
      if (document.getElementById('input-text-overflow') === document.activeElement && self.searchKey === session.get('HOTKEYWORD')) {
        self.searchKey = ''
        self.hotfilter = true
      }
    }, 300)
      // to prevent mouse move to search box fast and click it due to time delay lead to the word in search box brighten
      // when mouse on profile or history
    if (document.getElementById('input-text-overflow') === document.activeElement && self.searchKey === session.get('HOTKEYWORD')) {
      self.searchKey = ''
      self.hotfilter = true
    }
  }

  inputBlur () {
    document.getElementById('input-text-overflow').blur()
  }

    /**
     * traverse search retation word
     */
  mapSuggestWords (suggests: Array<string>) {
    let self = this
    return _.map(suggests, (suggest) => {
      let lowerHotkey = suggest.toLowerCase()
      let lowerCurrentKey = self.searchKey.toLowerCase()
      let highlightStr
      let nameStart
      let nameEnd
      if (lowerHotkey.indexOf(lowerCurrentKey) >= 0) {
        highlightStr = suggest.slice(lowerHotkey.indexOf(lowerCurrentKey),
            lowerCurrentKey.split('').length + lowerHotkey.indexOf(lowerCurrentKey))
        nameStart = suggest.slice(0, lowerHotkey.indexOf(lowerCurrentKey))
        nameEnd = suggest.slice(lowerCurrentKey.split('').length + lowerHotkey.indexOf(lowerCurrentKey), lowerHotkey.length)
      } else {
        highlightStr = ''
        nameStart = suggest
        nameEnd = ''
      }
      return {
        name: suggest,
        namestart: nameStart,
        nameheightlight: highlightStr,
        nameend: nameEnd
      }
    })
  }

    /**
     * monitor the keyboard event change
     */
  onChange (e) {
    let self = this
    this.onchange++
    self.currentKey = self.searchKey
    clearTimeout(self.getSuggestKeyword)
    self.getSuggestKeyword = setTimeout(() => {
      if (!this.lostBlur) {
        self.showSearch()
      }
    }, 1000)
    if (e.keyCode === 13) {
      // document.getElementById('searchIcon').click()
      if (this.trim(e.target.value).length > 0) {
        this.searchAll()
        self.hideSearch()
      }
    }
    if (e.keyCode === 8 && self.searchKey === '') {
      this.searchHotkey = false
    }
  }

    /**
     * the first word is space be not allowed in search box
     */
  onPressChange (event) {
    let caretPos = 0
    let ctrl = document.getElementById('input-text-overflow')
    if (document['selection']) {
      ctrl.focus()
      let sel = document['selection'].createRange()
      sel.moveStart('character', -ctrl['value'].length)
      caretPos = sel.text.length
    } else if (ctrl['selectionStart'] || ctrl['selectionStart'] === '0') {
      caretPos = ctrl['selectionStart']
    }
  }

    /**
     * test and verify of input
     */
  verificateSearchKey () {
    this.blackListCache = session.get('COMMON_INPUT_BLACKLIST_CACHE')
    if (this.trim(this.searchKey) === '') {
      return false
    }
    if (this.searchKey.length > 128) {
      return false
    }
    if (this.blackListCache !== '') {
      if (!(/^[A-Z|a-z|0-9]+$/.test(this.searchKey))) {
        let numLetters = this.searchKey.replace(/[A-Z|a-z|0-9]/ig, '')
        let numLettersArr = numLetters.split('')
        if (numLettersArr.length !== 0) {
          let hasLettersOfBlackList = false
          nameOuter: for (let i = 0; i < numLettersArr.length; i++) {
            for (let j = 0; j < this.blackListCache.length; j++) {
              if (numLettersArr[i] === this.blackListCache.charAt(j)) {
                hasLettersOfBlackList = true
                break nameOuter
              }
            }
          }
          if (hasLettersOfBlackList) {
            return false
          }
        }
      }
    }
    return true
  }

    /**
     * when click hot keys, it will search all content about the keys.
     */
  searchAll () {
    this.searchHotkey = true
    session.put('isSearch', 'isSearch')
    this.hotfilter = false
      // this.searchKey = id;
    if (this.searchKey === '') {
      this.searchKey = session.get('HOTKEYWORD')
    }
    if (!this.verificateSearchKey()) {
      EventService.emit('VERIFICATE_SEARCHKEY')
      return
    }
    if (this.searchKey === '') {
      this.route.navigate(['home', 'search', session.get('HOTKEYWORD')])
      session.put('RouteMessage', 'home/search/' + session.get('HOTKEYWORD'))
    } else {
      this.route.navigate(['home', 'search', this.searchKey])
      session.put('RouteMessage', 'home/search/' + this.searchKey)
    }
  }

    /**
     * it will jump to vod detail page, when click the play button.
     */
  gotoVoddetail (data) {
    this.vodRouter.navigate(data.id)
  }

    /**
     * leave this page, destroy timer
     */
  ngOnDestroy () {
    if (this.getSuggestKeyword) {
      clearTimeout(this.getSuggestKeyword)
    }
  }

  isSearchIn () {
    if (!this.searchKey) {
      return
    }
    if (location.pathname.indexOf('search') !== -1 && !this.verificateSearchKey()) {
      EventService.emit('VERIFICATE_SEARCHKEY')
      this.route.navigate(['home'])
    }
  }

  onShowSearchField () {
    this.classActive = true
    if (this.classActive) {
      this.searchElement.nativeElement.focus()
    }
  }
    /**
     * remove the blank space of the head and the tail.
     */
  trim (str): String {
    if (str) {
      str = str.replace(/(^\s)|(\s*$)/g, '').replace(/(^\s*)/g, '').replace(/(\s*$)/g, '')
      return str
    } else return ''
  }
}
