import * as _ from 'underscore'
import { Component, OnInit } from '@angular/core'
import { modifyProfile } from 'ng-epg-sdk/vsp'
import { session } from 'src/app/shared/services/session'
import { TranslateService } from '@ngx-translate/core'
import { EventService, HeartbeatService } from 'ng-epg-sdk/services'
import { TimerService } from '../../shared/services/timer.service'
import { CommonService } from '../../core/common.service'
import { ChannelCacheService } from '../../shared/services/channel-cache.service'

@Component({
  selector: 'app-language-header',
  templateUrl: './language.component.html',
  styleUrls: ['./language.component.scss']
})

export class LanguageHeaderComponent implements OnInit {
  public languageList: Array<any>
  public languageNameList: Array<string>

  public afterLanguage
    /**
     * current language
     */
  public diffAfterLanguage
  public selectedLanguage: string
  public lastLanguageList
  public diffLastLanguageList
  public clickLanguage = false
    /**
     * show language list
     */
  public showLanguage = true
    /**
     * array of language list
     */
  public showLanguageList = []

  constructor (private translate: TranslateService,
        private timerService: TimerService,
        private channelCacheService: ChannelCacheService,
        private commonService: CommonService,
        private heartbeatService: HeartbeatService) {
  }

  ngOnInit () {
    let self = this
      // binding function to close language list when click other not including the language list
    if (window.addEventListener) {
      document.addEventListener('click', function (e) {
        if (!(e.target['className'] === 'language-list' ||
                    e.target['className'] === 'language-list-content' ||
                    e.target['id'] === 'language-item' ||
                    e.target['className'] === 'language-item-content' ||
                    e.target['className'] === 'list-arrow' ||
                    e.target['className'] === 'language-button-content body-t29' ||
                    e.target['className'] === 'language-header' ||
                    e.target['className'] === 'arrow'
          )) {
            self.clickLanguage = false
            self.showLanguage = true
          }
      })
    } else {
      document['attachEvent']('click', function (e) {
        if (!(e.target['className'] === 'language-list' ||
                    e.target['className'] === 'language-list-content' ||
                    e.target['id'] === 'language-item' ||
                    e.target['className'] === 'language-item-content' ||
                    e.target['className'] === 'list-arrow' ||
                    e.target['className'] === 'language-button-content body-t29' ||
                    e.target['className'] === 'language-header' ||
                    e.target['className'] === 'arrow'
          )) {
            self.clickLanguage = false
            self.showLanguage = true
          }
      })
    }
    this.lastLanguageList = session.get('LASTLANGUAGELIST')
      // judge the user wherther open the application for the first time
    let firstOpen = session.get('firstOpen')
    if (firstOpen) {
      this.diffLastLanguageList = JSON.parse(JSON.stringify(this.lastLanguageList))
      _.each(this.diffLastLanguageList, (list, index) => {
        list['name'] = this.translate.instant('ISO639_1')[list['name']].split(',')[0]
        if (list['name'] === 'English') {
            list['name'] = 'EN'
          }
      })
    }
    this.afterLanguage = session.get('languageName')
    this.selectedLanguage = session.get('language')
    this.languageNameList = []
    EventService.removeAllListeners(['LANGUAGE_QUERYCUSTOMIZE'])
    EventService.on('LANGUAGE_QUERYCUSTOMIZE', function () {
      self.getCustomizeConfig()
    })
  }

    /**
     * judge the user wherther open the application for the first time
     * if not, mark it
     */
  checkFirstOpen () {
    let firstOpen = session.get('firstOpen')
    if (!firstOpen) {
      session.put('firstOpen', true, true)
    }
  }

    /**
     * initialize language
     */
  initLanguage () {
    let CustomConfiglanguageNameList = session.get('CustomConfiglanguageNameList')
    let firstOpen = session.get('firstOpen')
    if (!firstOpen) {
      this.lastLanguageList = _.filter(CustomConfiglanguageNameList, (list, index) => {
        return list['name'] === 'en' || list['name'] === 'es' || list['name'] === 'zh' || list['name'] === 'ru'
      })
      this.diffLastLanguageList = JSON.parse(JSON.stringify(this.lastLanguageList))
      _.each(this.diffLastLanguageList, (list, index) => {
        list['name'] = this.translate.instant('ISO639_1')[list['name']].split(',')[0]
        if (list['name'] === 'English') {
            list['name'] = 'EN'
          }
      })
      session.put('LASTLANGUAGELIST', this.lastLanguageList, true)
      let nameList = _.pluck(this.lastLanguageList, 'name')
      nameList = _.uniq(nameList)
      if (nameList.length < 4 && nameList.length !== 0) {
        this.commonService.MSAErrorLog(38010, 'QueryCustomizeConfig')
      } else if (nameList.length === 0) {
          this.commonService.MSAErrorLog(38010, 'QueryCustomizeConfig')
          EventService.emit('NO_CHANGELANGUAGE')
        }
      let lastLanguage = session.get('language')
      this.setLanguage(lastLanguage, this.lastLanguageList)
      session.put('languageName', this.afterLanguage, true)
      session.put('language', this.afterLanguage, true)
    } else {
      this.afterLanguage = JSON.parse(localStorage.getItem('language'))
      this.setLanguage(this.afterLanguage)
      session.put('languageName', this.afterLanguage, true)
      session.put('language', this.afterLanguage, true)
    }
  }

  setLanguage (languageType, lastLanguageList?) {
    let setLanguage
    EventService.emit('SETWHITECSSSTYLE', languageType)
    _.delay(() => {
      if (languageType === 'zh') {
        setLanguage = 'zh'
        this.transLangulage(setLanguage)
      } else if (languageType === 'en') {
          setLanguage = 'en'
          this.transLangulage(setLanguage)
        } else if (languageType === 'ru') {
          setLanguage = 'ru'
          this.transLangulage(setLanguage)
        } else if (languageType === 'es') {
          setLanguage = 'es'
          this.transLangulage(setLanguage)
        } else {
          this.transLangulage(setLanguage)
        }
      this.showLanguageList = _.clone(this.diffLastLanguageList)
      let findObj = _.find(this.diffLastLanguageList, item => {
        return item['name'] === this.diffAfterLanguage
      })
      let index = _.indexOf(this.diffLastLanguageList, findObj)
      if (index !== -1) {
        this.showLanguageList.splice(index, 1)
      }
      session.put('languageName', this.afterLanguage, true)
      session.put('language', this.afterLanguage, true)
      if (lastLanguageList) {
        this.checkFirstOpen()
      }
      EventService.emit('SHOW_NAV')
    }, 500)
  }

  transLangulage (setLanguage) {
    this.translate.use(setLanguage)
    this.afterLanguage = setLanguage
    this.diffAfterLanguage = this.translate.instant('ISO639_1')[this.afterLanguage].split(',')[0]
    if (this.diffAfterLanguage === 'English') {
      this.diffAfterLanguage = 'EN'
    }
  }

  getCustomizeConfig () {
    let self = this
    this.timerService.getCaheCustomConfig().then(resp => {
      if (resp && resp['languages'] && resp['languages'].length > 0) {
        this.languageList = resp['languages']
        session.put('CustomConfiglanguageNameList', this.languageList)
      } else {
        this.commonService.MSAErrorLog(38010, 'QueryCustomizeConfig')
        this.languageList = [{
            'ID': '1',
            'name': 'en'
          }]
        EventService.emit('NO_CHANGELANGUAGE')
      }
      this.languageNameList = _.map(this.languageList, function (language) {
        return language.name
      })
      session.put('languageNameList', this.languageNameList)
      this.afterLanguage = session.get('languageName') ? session.get('languageName') : 'zh'
      EventService.emit('GETCUSTOMCONFIGDATASUCCES')
      this.initLanguage()
    }, resp => {
      _.delay(() => {
        self.getCustomizeConfig()
      }, 50)
    })
  }

    /**
     * click the language name to change language
     */
  changeLanguage (LanguageName) {
    if (LanguageName === '中文') {
      this.afterLanguage = 'zh'
      this.selectedLanguage = 'zh'
    } else if (LanguageName === 'EN') {
      this.afterLanguage = 'en'
      this.selectedLanguage = 'en'
    } else if (LanguageName === 'español') {
        this.afterLanguage = 'es'
        this.selectedLanguage = 'es'
      } else if (LanguageName === 'Русский') {
        this.afterLanguage = 'ru'
        this.selectedLanguage = 'ru'
      } else {
        this.afterLanguage = 'en'
        this.selectedLanguage = 'en'
      }
    session.put('languageName', this.afterLanguage, true)
    session.put('language', this.selectedLanguage, true)
    this.refreshStroageData()
  }

  getLanguage (isFrist?) {
    let modifyProfileReq
    if (Cookies.getJSON('IS_PROFILE_LOGIN')) {
      modifyProfileReq = {
        profile: {
            lang: this.selectedLanguage,
            ID: session.get('modify_profileID'),
            profileType: Cookies.get('PROFILE_TYPE')
          }
      }
    } else {
      modifyProfileReq = {
        profile: {
            lang: this.selectedLanguage,
            ID: session.get('modify_profileID')
          }
      }
    }
    modifyProfile(modifyProfileReq).then(resp => {
      if (!isFrist) {
        this.heartbeatService.startHeartbeat().then(response => {
            let userType = Cookies.getJSON('USER_ID')
            // session.put('USER_FILTER', response['userFilter'], true);
            let promise = Promise.all([this.channelCacheService.refreshStatic(userType),
              this.channelCacheService.refreshDynamic(userType)])
            // tslint:disable-next-line:no-unused-variable
            promise.then(([staticResp, dynamicResp]) => {
              window.location.reload()
            })
          })
      }
    })
  }

    /**
     * reload page and refresh storage
     */
  refreshStroageData () {
    session.put('changeLanguageSuccess', true)
    this.getLanguage()
  }

    /**
     * show language list or not
     */
  clickLanguageButton () {
    if (this.clickLanguage) {
      this.clickLanguage = false
    } else {
      this.clickLanguage = true
    }
    if (this.showLanguage) {
      this.showLanguage = false
    } else {
      this.showLanguage = true
    }
  }
}
