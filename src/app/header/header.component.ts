import { Component, ViewChild, OnInit } from '@angular/core'
import { SearchHeaderComponent } from './search/search.component'
import { HistoryHeaderComponent } from './history/history.component'
import { ProfileHeaderComponent } from './profile/profile.component'
import { EventService } from 'ng-epg-sdk/services'
import { session } from 'src/app/shared/services/session'

@Component({
  selector: 'app-header',
  styleUrls: ['./header.component.scss'],
  templateUrl: './header.component.html'
})

export class HeaderComponent implements OnInit {
  // chid of search page
  @ViewChild(SearchHeaderComponent) searchHeader: SearchHeaderComponent
  // chid of history page
  @ViewChild(HistoryHeaderComponent) historyHeader: HistoryHeaderComponent
  // chid of profile page
  @ViewChild(ProfileHeaderComponent) profileHeader: ProfileHeaderComponent
  // the language is showing or not
  public enChangeLanguage = true
  public getCaheCustomConfigData = false;

  public isAuth: boolean = false;

  menu = [
    {
      link: ['/home'],
      text: 'menu_home',
    },
    {
      link: ['/live-tv'],
      text: 'menu_livetv',
    },
    {
      link: ['/video'],
      text: 'menu_vod',
    },
    {
      link: ['/video/series'],
      text: 'menu_series',
    },
  ];
  ngOnInit () {
    let firstOpen = session.get('firstOpen')
    if (!firstOpen) {
      this.getCaheCustomConfigData = false
    }
    // if get the custom config data successfully,show the language tag
    EventService.on('GETCUSTOMCONFIGDATASUCCES', () => {
      this.getCaheCustomConfigData = true
    })
    // when the suspension dialog show, the search dialog will hide
    EventService.removeAllListeners(['HIDESEARCH'])
    EventService.on('HIDESEARCH', () => {
      this.searchHeader.hideSearch()
      this.searchHeader.inputBlur()
    })
    // when change the language fail, emit this event
    EventService.removeAllListeners(['NO_CHANGELANGUAGE'])
    EventService.on('NO_CHANGELANGUAGE', () => {
      this.enChangeLanguage = false
    })

    EventService.on('LOGIN', () => {
      if (Cookies.getJSON('IS_PROFILE_LOGIN')) {
          // if the user logins,show the history adn favorite.
        this.isAuth = true;
      } else {
          // if the user doesn't login,hide the history and favorite.
        this.isAuth = false;
      }
    })
  }
  /**
   * show the my item list or not,according to mouse in or out
   */
  dealWithProfile (appear) {
    if (appear === 'hide') {
      this.profileHeader.hideProfile()
    } else if (appear === 'show') {
      this.profileHeader.showProfile()
    }
  }
  /**
   * close other dialog,when itself showing
   */
  closeOtherModule (module) {
    if (module === 'HistoryEmit') {
      this.searchHeader.hideSearch()
      this.searchHeader.inputBlur()
      this.profileHeader.hideProfile()
    } else if (module === 'ProfileEmit') {
      this.searchHeader.hideSearch()
      this.searchHeader.inputBlur()
      this.historyHeader.hideHistory()
    }
  }
}
