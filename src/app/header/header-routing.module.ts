import { NgModule } from '@angular/core'
import { Route, RouterModule } from '@angular/router'
import { AuthGuard } from '../shared/services/auth.guard'

import { BottomRoutesComponent } from './bottom/bottom.component'

export const vodRoutes: Array<Route> = [
  { path: 'bottom/:title', component: BottomRoutesComponent, canActivate: [AuthGuard] }
]

@NgModule({
  imports: [
    RouterModule.forChild(vodRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class HeaderRoutingModule { }
