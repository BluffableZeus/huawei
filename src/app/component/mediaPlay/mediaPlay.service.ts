import * as _ from 'underscore'
import { Injectable } from '@angular/core'
import { session } from 'src/app/shared/services/session'
import { AuthorizeResult } from 'ng-epg-sdk/vsp'
import { TranslateService } from '@ngx-translate/core'
import { environment } from '../../../environments/environment';

@Injectable()
export class MediaPlayService {
  public rootLicense
  public firefoxMiniStyle
  constructor (private translate: TranslateService) { }

    /**
     * the player play.
     */
  play (player) {
    player.play()
  }

    /**
     * the player pause.
     */
  pause (player) {
    player.pause()
  }

    /**
     * enter full screen to play.
     */
  enterFullscreen (player) {
    let ua = navigator.userAgent.toLowerCase()
    let isIe = !!ua.match(/msie/i) || !!ua.match(/rv:([\d.]+).*like gecko/)
    let isEdge = !!ua.match(/edge\/([\d.]+)/)
    let isFirefox = !!ua.match(/firefox\/([\d.]+)/)
    this.firefoxMiniStyle = _.extend({}, document.querySelector('#vodVideoContainer video')['attributes']['style'])
    let styles = 'position: relative; border: 0px none; bottom: 0px; right: 0px; left: 0px; top: 0px;' +
            'width: 100%; height: 100%; background: transparent none repeat scroll 0% 0%'
    if (isFirefox) {
      document.querySelector('#vodVideoContainer video')['style'] = styles
    } else if (isEdge) {
      document.querySelector('#vodVideoContainer video')['style']['cssText'] = styles
      document.querySelector('#vodVideoContainer video')['style']['cursor'] = 'default'
    } else if (isIe) {
        document.querySelector('#vodVideoContainer video')['style']['cssText'] = styles
        document.querySelector('#vodVideoContainer video')['style']['cursor'] = 'default'
      }
    this.fullScreen(document.querySelector('#vodVideoContainer'), true)
  }

    /**
     * exit the full screen.
     */
  exitFullScreen (player, birth) {
    let ua = navigator.userAgent.toLowerCase()
    let isIe = !!ua.match(/msie/i) || !!ua.match(/rv:([\d.]+).*like gecko/)
    let isEdge = !!ua.match(/edge\/([\d.]+)/)
    let isFirefox = !!ua.match(/firefox\/([\d.]+)/)
    let video
    if (birth === 'VOD') {
      video = document.querySelector('#vodVideoContainer video')
    } else {
      video = document.querySelector('#videoContainer video')
    }
    if (isFirefox) {
      document.querySelector('#vodVideoContainer video')['style'] = this.firefoxMiniStyle.value
    } else if (isEdge) {
      document.querySelector('#vodVideoContainer video')['style']['cssText'] = this.firefoxMiniStyle.value
      document.querySelector('#vodVideoContainer video')['style']['cursor'] = 'default'
    } else if (isIe) {
        document.querySelector('#vodVideoContainer video')['style']['cssText'] = this.firefoxMiniStyle.value
        document.querySelector('#vodVideoContainer video')['style']['cursor'] = 'default'
      }
    this.fullScreen(video, false)
  }

    /**
     * judge the player is full screen or not.
     */
  isFullScreen () {
    return document.fullscreenElement || document['msFullscreenElement'] || document['mozFullScreen'] || document.webkitIsFullScreen
  }

    /**
     * set the volume.
     */
  setVolume (player, value) {
    player.setVolume(value)
  }

    /**
     * get the volume.
     */
  getVolume (player) {
    return player.getVolume()
  }
  fullScreen (target: HTMLElement, onOrOff: boolean) {
    if (!target) { return }
    if (onOrOff) {
      const ua = navigator.userAgent.toLowerCase()
      const isIe = !!ua.match(/msie/i) || !!ua.match(/rv:([\d.]+).*like gecko/)
      const isFirefox = !!ua.match(/firefox\/([\d.]+)/)
      const isEdge = !!ua.match(/edge\/([\d.]+)/)
      const isSafary = !!ua.match(/version\/([\d.]+).*safari/)

      if (isIe) {
        target['msRequestFullscreen']()
      } else if (isEdge) {
          target.webkitRequestFullScreen()
        } else if (isFirefox) {
          session.put('FIREFOX_SCROLLTOP', document.documentElement.scrollTop)
          target['mozRequestFullScreen']()
        } else if (isSafary) {
          target.requestFullscreen()
        } else {
          target.webkitRequestFullScreen()
        }
    } else {
      if (!this.isFullScreen()) {
        return
      }
      if (document.exitFullscreen) {
        document.exitFullscreen()
      } else if (document['mozCancelFullScreen']) {
          document['mozCancelFullScreen']()
        } else if (document['msExitFullscreen']) {
          document['msExitFullscreen']()
        } else {
          document.webkitCancelFullScreen()
        }
    }
  }

    /**
     * close the player.
     */
  close (player) {
    player.close()
  }

    /**
     * get the current version of the player.
     */
  getVersion (player) {
    return player.getVersion()
  }

    /**
     * get the current audio laguage.
     */
  getCurrentAudioLanguage (player) {
    if (player.getCurrentAudioLanguage) {
      return player.getCurrentAudioLanguage()
    }
  }
    /**
     * get the current subtitle language.
     */
  getCurrentSubtitleLanguage (player) {
    if (player.getCurrentSubtitleLanguage) {
      return player.getCurrentSubtitleLanguage()
    }
  }
    /**
     * get the audio language list of the current sources.
     */
  getAudioLanguages (player) {
    if (player.getAudioLanguages) {
      return player.getAudioLanguages()
    }
  }
    /**
     * get the subtitle language list of the current sources.
     */
  getSubtitleLanguages (player) {
    if (player.getSubtitleLanguages) {
      return player.getSubtitleLanguages()
    }
  }

    /**
     * get the array objects containing the bit rate and index.
     */
  getAllBitRates (player) {
    return player.getAllBitRates()
  }

    /**
     * set the play track.
     */
  setQualityFor (player, type, value) {
    player.setQualityFor(type, value)
  }

  setValueByName (rootLicense) {
    this.rootLicense = rootLicense
  }

    /**
     * send to license to MEDIAPLAYER
     * @param {AuthorizeResult} authorizeResult [Authentication return information]
     */
  sendLicense (authorizeResult: AuthorizeResult) {
      //  If  there is a trigger authentication in the results
    if (authorizeResult && authorizeResult.triggers && authorizeResult.triggers.length > 0) {
        // The information sent to MEDIAPLAYER
      const rootLicense = {
        KID: authorizeResult.triggers[0]['keyID'],
        customData: authorizeResult.triggers[0].customData
      }
      if (session.get('network') === 'MultiDRM') {
        session.put('wideVine.license.server.url', authorizeResult.triggers[0].licenseURL)
        session.put('playReady.license.server.url', authorizeResult.triggers[0].licenseURL)
      }
        // Root the issuance of the License number of the interface
      this.setValueByName(rootLicense)
    }
  }

    /**
     * [PC]Support Caching of User tenant information
     */
  setPerferedLang (player, type) {
    let languageList = _.extend({}, session.get('languageNameList'))
    if (session.get('language') && session.get('language') !== languageList[0]) {
      let firstLang = languageList[0]
      let secondLang = languageList[1]
      languageList[0] = secondLang
      languageList[1] = firstLang
    }
    languageList[0] = this.translate.instant('ISO639-1_to_2')[languageList[0]]
    languageList[1] = this.translate.instant('ISO639-1_to_2')[languageList[1]]
    if (type === 'subtitle') {
      let curSubtitleLang = session.get('player.subtitle.lang') || languageList[0]
      player.setPerferedLang('subtitle', curSubtitleLang + ',' + languageList[0] + ',' + languageList[1])
    } else if (type === 'audio') {
      let curAudioLang = session.get('player.audio.lang') || languageList[0]
      player.setPerferedLang('audio', curAudioLang + ',' + languageList[0] + ',' + languageList[1])
    }
  }

  setServerUrl (protData) {
    if (session.get('wideVine.license.server.url') || session.get('playReady.license.server.url')) {
      protData = {
        'com.widevine.alpha': {
            'serverURL': session.get('wideVine.license.server.url'),
            'httpRequestHeaders': {
              'CADeviceType': 'Widevine OTT client',
              'AcquireLicense.CustomData': this.rootLicense['customData']
            }
          },
        'com.microsoft.playready': {
            'serverURL': session.get('playReady.license.server.url'),
            'httpRequestHeaders': {
              'CADeviceType': 'Playready OTT client',
              'AcquireLicense.CustomData': this.rootLicense['customData']
            }
          }
      }
    }
    return protData
  }
    /**
     * set the play url.
     * If user selected no subtitle, it will continue to select no subtitle.
     * Otherwise, it will set prefered language.
     */
  attachSource (player, url, protData) {
    let curSubtitleLang = session.get('player.subtitle.lang')
    if (curSubtitleLang === 'off') {
      player.switchStream('subtitle', { lang: 'off' })
    } else {
      this.setPerferedLang(player, 'subtitle')
    }
    let curAudioLang = session.get('player.audio.lang')
    if (curAudioLang === 'off') {
      player.switchStream('audio', { lang: 'off' })
    } else {
      this.setPerferedLang(player, 'audio')
    }

    // url DIRTY HACK
    url = url.replace(environment.TV_PROXY_REPLACE_FROM_PROTOCOL, environment.TV_PROXY_REPLACE_TO_PROTOCOL)
    url = url.replace(new RegExp(environment.TV_PROXY_REPLACE_FROM_ADDRESS, 'g'), environment.TV_PROXY_REPLACE_TO_ADDRESS)

    player.open(url, this.setServerUrl(protData))
  }

    /**
     * set that the player plays from begin.
     */
  playFromBegin (player) {
    player.play()
  }

    /**
     * set position to play.
     */
  seek (player, value) {
    player.setPosition(value)
  }

    /**
     * get the position of playing currently.
     */
  time (player) {
    return player.getPosition()
  }

    /**
     * get the duration of sources.
     */
  duration (player) {
    return player.getDuration()
  }
    /**
     * get the buffer of sources.
     */
  buffered (player) {
    return player.buffered
  }
    /**
     * judge the player is playing or pause
     */
  isPause (player) {
    return player.isPaused()
  }
  setIsNPVR (player, value) {
    return player.setIsNPVR(value)
  }
  getIsNPVR (player) {
    return player.getIsNPVR()
  }
    // set Bookmark
  setBookMark (player, value) {
    return player.setBookMark(value)
  }
    /**
     * switch the bit rate,subtitles,track.
     */
  switchStream (player, type, requirements, changeFlag?) {
    if (requirements && requirements.lang) {
      if (type === 'audio') {
        if (changeFlag) {
            session.put('player.audio.lang', requirements.lang, true)
          }
      }
      if (type === 'subtitle') {
        if (changeFlag) {
            session.put('player.subtitle.lang', requirements.lang, true)
          }
      }
    }
    return player.switchStream(type, requirements)
  }
    /**
     * get the bit rate of current player(only track).
     */
  getCurrentBitrate (player) {
    return player.getCurrentBitrate()
  }
    /**
     * get that the player can switch quality by auto or not.
     */
  getAutoSwitchQuality (player) {
    return player.getAutoSwitchQuality()
  }
    /**
     * set that the player can switch quality by auto or not.
     */
  setAutoSwitchQuality (player, autoSwitch) {
    return player.setAutoSwitchQuality(autoSwitch)
  }
}
