import * as _ from 'underscore'
import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core'
import { EventService } from 'ng-epg-sdk/services'
import { checkPassword } from 'ng-epg-sdk/vsp'
import { TranslateService } from '@ngx-translate/core'
import { session } from 'src/app/shared/services/session'

@Component({
  selector: 'app-checkpass',
  templateUrl: './checkPass.component.html',
  styleUrls: ['./checkPass.component.scss']
})

export class LockedPasswordComponent implements OnInit {
  /**
     * show different tip accroding to scene
     */
  @Input() set verifyType (data) {
    this.source = data
    // unlock channel tip
    if (data === 'lockChannel') {
      this.isCommonProfile = false
      this.passwordTitle = 'enterPassword'
      this.passwordTips = 'lockedChannel_checkPassword'
      // delete profile tip
    } else if (data === 'deleteProfile') {
      this.passwordTitle = 'password_title'
      this.passwordTips = 'delete_profile_password_tips'
      this.isCommonProfile = true
    }
  }

  @Output() closeDiv: EventEmitter<Object> = new EventEmitter()
  @Output() confirmDiv: EventEmitter<Object> = new EventEmitter()
    // declare variables.
  passwordTitle = 'enterPassword'
  passwordTips = 'lockedChannel_checkPassword'
  public deletePassWord = false
  public userPassword = ''
  public errorMessage = false
  public emptyPasswordMessage = false
  public type = ''
  public showplaceholderCheck = true
  public configData = {}
  public forgetpassword = false
  public forgot_password_url = ''
  public source = ''
  public isCommonProfile = false
    // private constructor
  constructor (private translate: TranslateService
    ) {
      // the pass input get focus when open the dialog at first.
    _.delay(() => {
      if (document.getElementById('pass-Word')) {
        document.getElementById('pass-Word').focus()
      }
    }, 600)
  }

  ngOnInit () {
    this.configData = session.get('CONFIG_DATAS') || {}
      // if the forget password url exits,show the forget password button.
    if (this.configData['forgot_password_url']) {
      this.forgetpassword = true
      this.forgot_password_url = this.configData['forgot_password_url']
    }
  }
    /**
     *  check the pass input get focus.
     */
  onplaceholdCheck () {
    if (this.userPassword === '' || this.userPassword === undefined) {
      _.delay(() => {
        document.getElementById('pass-Word').focus()
      }, 100)
    }
  }
    /**
     *  close the dialog.
     */
  close (data) {
    this.closeDiv.emit(data)
    EventService.emit('IS_CHANNELEDIT', true)
  }

    /**
     * change the password.
     */
  changePassword (e) {
      // if the password is not empty
    if (this.userPassword !== '') {
        // enter button
      if (e.keyCode === 13) {
        document.getElementById('checkConfirmBtn').click()
      }
      this.showplaceholderCheck = false
      this.deletePassWord = true
        // if the password is empty
    } else {
      this.deletePassWord = false
      this.showplaceholderCheck = true
    }
  }
    /**
     * judge the status of delete buttom and the placeholder.
     */
  showPassword () {
      // if the password is not empty
    if (this.userPassword !== '') {
      this.deletePassWord = true
      this.showplaceholderCheck = false
    }
      // if the password is empty
    if (this.userPassword === '') {
      this.showplaceholderCheck = true
    }
  }
    /**
     * the pass input lose the focus.
     */
  hidePassword () {
    let self = this
      // if the password is empty
    if (this.userPassword === '') {
      this.showplaceholderCheck = true
    }
    _.delay(function () {
      self.deletePassWord = false
    }, 300)
  }
    /**
     * click the delete buttom of the pass input.
     */
  removePassWord () {
    this.userPassword = ''
    document.getElementById('pass-Word').focus()
  }
    /**
     * click the confirm to check the password.
     */
  confirm (data) {
    let type
    if (this.type === 'lock') {
      type = '0'
    }
      // if the password is empty,doesnot call the interface
    if (this.userPassword === '') {
      this.emptyPasswordMessage = true
      this.errorMessage = false
      document.getElementById('pass-Word').focus()
      return
    }
    checkPassword({
      password: this.userPassword,
      type: type
    }).then(resp => {
        // chack lock successfully
      this.confirmDiv.emit(data)
    }).catch(() => {
        // chack lock failed
        this.userPassword = ''
        this.showplaceholderCheck = true
        this.deletePassWord = false
        this.errorMessage = true
        this.emptyPasswordMessage = false
        document.getElementById('pass-Word').focus()
      })
  }
    /**
     * when the key down,hidden the placeholder.
     */
  keydownPassword () {
    this.showplaceholderCheck = false
  }
    /**
     * show the button color according to the password input
     */
  btnStyleChange () {
    if ((document.getElementById('pass-Word')['value'] !== '')) {
      return true
    } else {
      return false
    }
  }
}
