import { Component, OnInit } from '@angular/core'
import { DialogRef, DialogParams } from 'ng-epg-ui/webtv-components/dialog'
import { DirectionScroll } from 'ng-epg-ui/webtv-components/scroll/directionScroll.component'
import { TranslateService } from '@ngx-translate/core'
import { DateUtils } from 'ng-epg-sdk/utils'
import { RecordDataService } from '../recordDialog/recordData.service'
import { EventService } from 'ng-epg-sdk/services'
import * as _ from 'underscore'
import { session } from 'src/app/shared/services/session'
import { CommonService } from '../../core/common.service'
@Component({
  selector: 'app-recording-conflict',
  templateUrl: './recording-conflict.component.html',
  styleUrls: ['./recording-conflict.component.scss']
})

export class RecordingConflictComponent implements OnInit {
    // save the PVRList that is in conflict.
  public conflictList = []
    // title of the conflict dialog that show the num of current page and total pages.
  public num
    // check the button whether is clickable.
  public delete = false
    // save the index of next conflict group.
  public nextGroupIndex
    // save the array of PVR-ID deleted.
  public cancelPVRID = []
    // save the num of current page.
  public curNum
    // save the index of conflict group. -1 means all conflicts solved.
  public returnGroupIndex

  public haveChooseObj = []

  public conflictID
    // save the array of ID choosed.
  public curChooseID = []

  public totalLength = 0

  public isLastPage = false

  public canSolveAll = false
    /**
     * name for modules that would be used in this module
     */
  constructor (
        private dialog: DialogRef,
        private config: DialogParams,
        private translate: TranslateService,
        private recordDataService: RecordDataService,
        private commonService: CommonService,
        private directionScroll: DirectionScroll
    ) {
  }

    /**
     * initialization
     */
  ngOnInit () {
    this.curNum = 1
      // get the conflict list.
    this.conflictList = this.dealWidthconflictList(this.config['resp']['conflictGroups'][0]['PVRList'],
        this.config['resp']['conflictGroups'][0]['isReserves'])
    _.delay(() => {
      this.pluseOnScroll()
    }, 0)
    this.totalLength = this.config['resp']['conflictGroups']['length']
      // if the total length equal to 1, only one page.
    if (this.totalLength === 1) {
      this.isLastPage = true
    } else {
      this.isLastPage = false
    }
      // display according to recording type.
    if (this.config['resp']['conflictGroups'][0]['PVRList'][0]['policyType'] === 'PlaybillBased' ||
            this.config['resp']['conflictGroups'][0]['PVRList'][0]['policyType'] === 'TimeBased') {
      this.num = '(' + this.curNum + '/' + this.totalLength + ')'
    } else {
      this.num = ''
    }
    this.conflictID = this.config['data'].ID
  }

    /**
     * select the conflict data
     */
  onSelect (conflict, index) {
    let self = this
    let findID = _.find(this.curChooseID, item => {
      return item === conflict['ID']
    })
    if (findID) {
      let idIndex = _.indexOf(this.curChooseID, findID)
      this.curChooseID.splice(idIndex, 1)
    } else {
      this.curChooseID.push(conflict['ID'])
    }
    if (this.num === '') {
      this.curNum = 1
    }
    if (conflict.isReserves === '1') {
      this.conflictList[index].isReserves = '0'
      let isReserves = []
      for (let i = 0; i < this.conflictList.length; i++) {
        isReserves.push(this.conflictList[i]['isReserves'])
      }
      this.config['resp']['conflictGroups'][this.curNum - 1]['isReserves'] = isReserves
      this.recordDataService.solvePVRConflict(this.config['resp']['conflictGroups'], this.curNum - 1).then(function (resp) {
        self.nextGroupIndex = Number(resp.nextGroupIndex)
        self.returnGroupIndex = resp.nextGroupIndex
        if (resp.nextGroupIndex === '-1') {
            self.canSolveAll = true
          } else {
            self.canSolveAll = false
          }
        self.checkButtonState()
      }, function (resp) {

      })
    } else {
      this.conflictList[index].isReserves = '1'
      let isReserves = []
      for (let i = 0; i < this.conflictList.length; i++) {
        isReserves.push(this.conflictList[i]['isReserves'])
      }
      this.config['resp']['conflictGroups'][this.curNum - 1]['isReserves'] = isReserves
      this.recordDataService.solvePVRConflict(this.config['resp']['conflictGroups'], this.curNum - 1).then(function (resp) {
        self.nextGroupIndex = Number(resp.nextGroupIndex)
        self.returnGroupIndex = resp.nextGroupIndex
        if (resp.nextGroupIndex === '-1') {
            self.canSolveAll = true
          } else {
            self.canSolveAll = false
          }
        self.checkButtonState()
      }, function (resp) {

      })
    }
  }

  checkButtonState () {
    let self = this
    let haveChoose
    haveChoose = _.find(this.conflictList, (list) => {
      return list['isReserves'] === '0'
    })
    if (haveChoose) {
      if (self.nextGroupIndex >= self.curNum || self.nextGroupIndex === -1) {
        this.delete = true
      } else {
        this.delete = false
      }
    } else {
      this.delete = false
    }
  }

    /**
     * close the conflict dialog
     */
  close () {
    this.dialog.close()
  }

    /**
     * solve the conflict
     */
  solvePVRConflict () {
    if (this.curChooseID.length === 0) {
      return
    }
    this.dealWidthID(this.conflictList)
    let haveChooseAddID
    haveChooseAddID = _.find(this.conflictList, (list) => {
      return list['ID'] === '-1' && list['isReserves'] === '0'
    })
    if (haveChooseAddID) {
      this.haveChooseObj.push(haveChooseAddID)
    }
    if (this.nextGroupIndex >= this.curNum) {
      this.conflictList = this.dealWidthconflictList(this.config['resp']['conflictGroups'][this.nextGroupIndex]['PVRList'],
          this.config['resp']['conflictGroups'][this.nextGroupIndex]['isReserves'])
      _.delay(() => {
        this.pluseOnScroll()
      }, 0)
      this.curNum = this.nextGroupIndex + 1
      if (this.curNum === this.totalLength) {
        this.isLastPage = true
      } else {
        this.isLastPage = false
      }
      this.curChooseID = []
      this.dealWidthID(this.conflictList)
      let haveChoose
      haveChoose = _.find(this.conflictList, (list) => {
        return list['isReserves'] === '0'
      })
      if (haveChoose) {
        this.delete = true
      } else {
        this.delete = false
      }
      this.num = '(' + this.curNum + '/' + this.totalLength + ')'
    }
      // solve the mixed conflict
    if (this.returnGroupIndex === '-1') {
      this.finishConflict(haveChooseAddID)
    }
  }

    /**
     * deal with ID
     */
  dealWidthID (conflictList) {
    for (let i = 0; i < this.conflictList.length; i++) {
      if (this.conflictList[i]['isReserves'] === '0' && this.conflictList[i]['ID'] !== '-1') {
        this.cancelPVRID.push(this.conflictList[i]['ID'])
      }
    }
  }

    /**
     * deal with the data of the single or mixed conflict
     */
  dealWidthconflictList (conflictList, isReserves) {
    let list = _.map(conflictList, (list1, index) => {
      let duration
      let recordweekTime
      let recordTime
      let name
      let channelName = this.getChannelName(list1)
      if (list1['seriesID'] || list1['parentPlanID']) {
        name = list1['name']
      } else {
        name = list1['name']
      }
      if (list1['policyType'] === 'Series' || list1['policyType'] === 'Period') {
        status = ''
      } else {
        status = list1['status']
      }
      if (status === 'RECORDING' || status === 'WAIT_RECORD') {
        recordweekTime = this.commonService.dealWithDateJson('D05', list1['startTime'])
        recordTime = DateUtils.format(list1['startTime'], ' DD.MM.YYYY')
        duration = this.commonService.dealWithDateJson('D10', list1['startTime']) +
                '-' + this.commonService.dealWithDateJson('D10', list1['endTime'])
      } else {
        recordweekTime = this.commonService.dealWithDateJson('D05', list1['startTime'])
        recordTime = DateUtils.format(list1['startTime'], ' DD.MM.YYYY')
        if (list1['duration']) {
            duration = Math.ceil(list1['duration'] / 60) + ' ' + this.translate.instant('record_min')
          } else {
            duration = this.commonService.dealWithDateJson('D10', list1['startTime']) +
                    '-' + this.commonService.dealWithDateJson('D10', list1['endTime'])
          }
      }
      return {
        recordweekTime: recordweekTime,
        recordTime: recordTime,
        duration: duration,
        channelName: channelName,
        name: name,
        ID: list1['ID'],
        definition: list1['definition'],
        startTime: list1['startTime'],
        playbillID: list1['playbillID'],
        policyType: list1['policyType'],
        isReserves: isReserves[index]
      }
    })
    return list
  }

    /**
     * method for acquire channel name.
     */
  getChannelName (list) {
    let channelName = list['extensionFields'] && list['extensionFields'][0] && list['extensionFields'][0].values &&
            list['extensionFields'][0].values[0] || list['playbill'] && list['playbill'].channel && list['playbill'].channel.name
    return channelName
  }

    /**
     * solve the mixed conflict.
     */
  finishConflict (haveChooseAddID) {
    let self = this
    haveChooseAddID = this.haveChooseObj && this.haveChooseObj[0]
    let sendMessage = {
      data: this.config['data'],
      haveChooseAddID: haveChooseAddID,
      type: this.config['type'],
      update: this.config['update']
    }
    if (session.get('MIXED_CONFLICT')) {
      if (this.cancelPVRID.length !== 0) {
        this.recordDataService.cancelPVRByID(this.cancelPVRID, 'CPVR').then(resp => {
            EventService.emit('SOLVE_CONFLICT_MIXED', sendMessage)
            self.cancelPVRID = []
            self.haveChooseObj = []
            session.pop('MIXED_CONFLICT')
            // close box
            self.close()
          })
      } else {
        EventService.emit('SOLVE_CONFLICT_MIXED', sendMessage)
        this.cancelPVRID = []
        this.haveChooseObj = []
        session.pop('MIXED_CONFLICT')
        this.close()
      }
    } else {
      sendMessage = {
        data: self.config['data'],
        haveChooseAddID: haveChooseAddID,
        type: self.config['type'],
        update: self.config['update']
      }
      if (this.cancelPVRID.length === 0) {
        self.close()
        EventService.emit('MANUAL_SET_AFTER_SOLVE_CONFLICT')
      } else {
        if (this.num === '') {
            this.recordDataService.deletePVRByIDWait('CPVR', this.cancelPVRID).then(resp => {
              if (!haveChooseAddID) {
                EventService.emit('SOLVE_CONFLICT_SERIES', sendMessage)
              } else {
                EventService.emit('MANUAL_SET_AFTER_SOLVE_CONFLICT')
              }
              self.cancelPVRID = []
              self.close()
            })
          } else {
            this.recordDataService.cancelPVRByID(this.cancelPVRID, 'CPVR').then(resp => {
              if (!haveChooseAddID) {
                EventService.emit('SOLVE_CONFLICT', sendMessage)
              } else {
                EventService.emit('MANUAL_SET_AFTER_SOLVE_CONFLICT')
              }
              let playbillIDArray = []
              for (let i = 0; i < self.cancelPVRID.length; i++) {
                let conflictItem = _.find(self.conflictList, item => {
                  return self.cancelPVRID[i] === item['ID']
                })
                if (!_.isUndefined(conflictItem)) {
                  playbillIDArray.push(conflictItem['playbillID'])
                }
              }
              EventService.emit('SOLVE_CONFLICT_BY_DELETE', playbillIDArray)
              self.cancelPVRID = []
              self.close()
            })
          }
      }
    }
  }

  pluseOnScroll () {
    if (this.conflictList.length > 3) {
      let oBox = this.dom('app-recording-conflict')
      let oConter, oUl, oScroll, oSpan
      oConter = this.dom('conflicts-wrap')
      oUl = this.dom('conflicts-list')
      oScroll = this.dom('conflict-scroll')
      if (oScroll && oScroll.getElementsByTagName('span')) {
        oSpan = oScroll.getElementsByTagName('span')[0]
      }
      this.directionScroll.setScroll(oBox, oConter, oUl, oScroll, oSpan)
    }
  }

  dom (divName: string) {
    return document.getElementById(divName)
  }
}
