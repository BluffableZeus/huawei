import * as _ from 'underscore'
import { Component, Input, Output, EventEmitter } from '@angular/core'
import { Router } from '@angular/router'
import { session } from 'src/app/shared/services/session'
import { checkPassword, modifyProfile, addProfile } from 'ng-epg-sdk/vsp'
import { TranslateService } from '@ngx-translate/core'
import { EventService } from 'ng-epg-sdk/services'
import { ProfileManageService } from '../../my-tv/profile-manage/profile-manage.service'

/**
 * this component is for password page and ID page.
 * including setting new profile password, setting new profile ID, updating sub-profile password.
 */
@Component({
  selector: 'app-password-info',
  templateUrl: './modify-pass.component.html',
  styleUrls: ['./modify-pass.component.scss']
})

export class ModifyPasswordComponent {
  @Output() closePassWord: EventEmitter<Object> = new EventEmitter()
  @Output() addProfileSuccess: EventEmitter<Object> = new EventEmitter()

  @Input() set passwordInfo (data) {
    this.type = data['type']
    this.subData = data['subInfo']
    if (this.type === 'modify') {
      this.titleName = 'modify_password'
      this.btnName = 'save'
      this.newPasswordTitle = 'new_password'
      this.isOldPwdRight = false
    } else {
      this.titleName = 'add_new_sub_profile'
      this.btnName = 'next_step'
      this.newPasswordTitle = 'password'
      this.isOldPwdRight = true
    }
  }

    /**
     * title
     */
  titleName = 'modify_password'
    /**
     * button name
     */
  btnName = 'save'
    /**
     * message that indicates the original password error
     */
  oldPwdErrorMsg = 'old_pwd_empty'
    /**
     * the title of new password
     */
  newPasswordTitle = 'new_password'

  public type = ''

  public subData = {}
    /**
     * password input requirements
     */
  public passwordInputTips = ''
    /**
     * password rule
     */
  public pwdConfig = {}

  public lastStep = false
    /**
     * there is a change in the input box
     */
  public changed = false
    /**
     * save the value of old password
     */
  public oldPwd = ''
    /**
     * save the value of new password
     */
  public newPwd = ''
    /**
     * save the value of confirm password
     */
  public confirmPwd = ''
    /**
     * show the delete button in the old password input box
     */
  public showOldDelete = false

  public showOldIcon = false
    /**
     * the old password entered is correct
     */
  public isOldPwdRight = false
    /**
     * show the delete button in the new password input box
     */
  public showNewDelete = false

  public showNewIcon = false
    /**
     * the new password is valid
     */
  public isNewPwdRight = false
    /**
     * message that indicates the new password error
     */
  public newPwdErrorMsg = ''
    /**
     * show the delete button in the confirm password input box
     */
  public showConfirmDelete = false

  public showConfirmIcon = false
    /**
     * the confirm password is the same as new password
     */
  public isConfirmPwdRight = false
    /**
     * message that indicates the confirm password error
     */
  public confirmPwdErrorMsg = ''
    /**
     * login id
     */
  public loginID = ''
    /**
     * all login ids
     */
  public loginIDs = []
    /**
     * show the delete button of id
     */
  public showIDDelete = false

  public showIDError = false

  public idCommonError = false

  public invalidID = false

  public idErrorType = ''
    /**
     * blacklist
     */
  public blackListCache = ''
    /**
     * the maximum length the input box can enter
     */
  public maxLengthCache = 128

  public loginIDTips = {}

  public blacklistTip = {}

  constructor (
        private router: Router,
        private translate: TranslateService,
        private profileManageService: ProfileManageService
    ) {
    this.pwdConfig = session.get('USER_PWD_RULER_CACHE')
    this.blackListCache = session.get('COMMON_INPUT_BLACKLIST_CACHE')
    this.maxLengthCache = session.get('COMMON_INPUT_MAXLENGTH_CACHE')
    this.blacklistTip = { 'common': this.blackListCache }
    this.getPasswordTips()
    this.getLoginIDtips()
  }
    /**
     * close the password or ID-setting dialog.
     */
  close () {
    this.closePassWord.emit()
  }
    /**
     * focus function of old password.
     */
  oldFocus (e) {
    if (e.target.value !== '') {
      this.showOldDelete = true
    }
  }
    /**
     * blur function of old password.
     */
  oldBlur (e) {
    _.delay(() => {
      this.showOldDelete = false
      if (this.oldPwd === '') {
        if (!(this.newPwd === '' && this.confirmPwd === '')) {
            this.isOldPwdRight = false
            this.showOldIcon = true
          }
      } else {
        this.isOldPwdRight = true
      }
      this.checkIsUnchanged()
    }, 200)
  }
    /**
     * clear old password.
     */
  oldPwdDelete () {
    this.oldPwd = ''
    this.showOldDelete = false
    document.getElementById('old-pwd').focus()
  }
    /**
     * keyup function of old password.
     */
  oldKeyup (e) {
    if (this.oldPwd === '') {
      this.showOldDelete = false
    } else {
      this.showOldDelete = true
    }
    this.checkIsUnchanged()
  }
    /**
     * focus function of new password.
     */
  newFocus (e) {
    if (this.newPwd !== '') {
      this.showNewDelete = true
    }
  }
    /**
     * blur function of new password.
     */
  newBlur () {
    _.delay(() => {
      this.showNewDelete = false
      this.checkNewPwd()
      this.checkIsUnchanged()
    }, 200)
  }
    /**
     * keyup function of new password.
     */
  newKeyup (e) {
    if (this.newPwd === '') {
      this.showNewDelete = false
    } else {
      this.showNewDelete = true
    }
    this.checkIsUnchanged()
  }
    /**
     * clear new password.
     */
  newPwdDelete () {
    this.newPwd = ''
    this.showNewDelete = false
    document.getElementById('new-pwd').focus()
  }
    /**
     * focus function of confirm password.
     */
  confirmFocus (e) {
    if (e.target.value !== '') {
      this.showConfirmDelete = true
    }
  }
    /**
     * blur function of confirm password.
     */
  confirmBlur () {
    _.delay(() => {
      this.showConfirmDelete = false
      this.checkConfirmPwd()
      this.checkIsUnchanged()
    }, 200)
  }
    /**
     * keyup function of confirm password.
     */
  confirmKeyup (e) {
    if (this.confirmPwd === '') {
      this.showConfirmDelete = false
    } else {
      this.showConfirmDelete = true
    }
    this.checkIsUnchanged()
  }
    /**
     * clear confirm password.
     */
  confirmPwdDelete () {
    this.confirmPwd = ''
    this.showConfirmDelete = false
    document.getElementById('confirm-pwd').focus()
  }
    /**
     * check whether the new password is valid.
     */
  checkNewPwd () {
    this.showNewIcon = true
    this.isNewPwdRight = false
    if (this.isOldPwdRight && this.newPwd === '') {
      this.newPwdErrorMsg = 'new_pwd_empty'
      return
    }
    if (this.type === 'modify') {
        // the new password should not be same as login ID.
      if (this.newPwd === this.subData['loginName']) {
        this.newPwdErrorMsg = 'password_does_not_conform_rules'
        return
      }
        // the new password should not be same as login ID by reverse.
      let reverseID = this.subData['loginName'].split('').reverse().join('')
      if (this.newPwd === reverseID) {
        this.newPwdErrorMsg = 'password_does_not_conform_rules'
        return
      }
    }
    if (!this.continueCheckPwd()) {
      return
    }
      // can not have space (if has)
    if (this.pwdConfig['userPwdSupportSpace']) {
      if (this.newPwd.indexOf(' ') === -1) {
        this.newPwdErrorMsg = 'password_does_not_conform_rules'
        return
      }
    } else {
      if (this.newPwd.indexOf(' ') !== -1) {
        this.newPwdErrorMsg = 'password_does_not_conform_rules'
        return
      }
    }
    this.isNewPwdRight = true
    this.checkConfirmPwd()
  }
    /**
     * check whether the new password is valid.
     */
  continueCheckPwd () {
      //  the limit of password length (if has)
    let hasNoEnoughLength = this.judgePwdLength()
    if (hasNoEnoughLength) {
      this.newPwdErrorMsg = 'password_does_not_conform_rules'
      return false
    }
      // including the capital letters (if has)
    if (this.pwdConfig['userPwdUpperCaseLetters']) {
      if (!/[A-Z]+/.test(this.newPwd)) {
        this.newPwdErrorMsg = 'password_does_not_conform_rules'
        return false
      }
    }
      // including the small letters (if has)
    if (this.pwdConfig['userPwdLowerCaseLetters']) {
      if (!/[a-z]+/.test(this.newPwd)) {
        this.newPwdErrorMsg = 'password_does_not_conform_rules'
        return false
      }
    }
      //  including digit (if has)
    if (this.pwdConfig['userPwdNumbers']) {
      if (!/[0-9]+/.test(this.newPwd)) {
        this.newPwdErrorMsg = 'password_does_not_conform_rules'
        return false
      }
    }
      // including special character (if has)
    let hasOtherLetters = this.judgePwdHasSpecialCharacter()
    if (hasOtherLetters) {
      let exp = new RegExp('[' + this.pwdConfig['userPwdOthersLetters'] + ']', 'g')
      if (!exp.test(this.newPwd)) {
        this.newPwdErrorMsg = 'password_does_not_conform_rules'
        return false
      }
    }
    return true
  }
    /**
     * judge the password length
     */
  judgePwdLength () {
    let hasNoEnoughLength = this.pwdConfig['userPwdMinLength'] && this.newPwd.length < Number(this.pwdConfig['userPwdMinLength'])
    return hasNoEnoughLength
  }
    /**
     * judge the password contain special character
     */
  judgePwdHasSpecialCharacter () {
    let hasOtherLetters = this.pwdConfig['userPwdOthersLetters'] !== '' && this.pwdConfig['userPwdOthersLetters'] !== undefined
    return hasOtherLetters
  }

    /**
     * check whether the confirm password is valid.
     */
  checkConfirmPwd () {
      // when the new passowrd is not empty
    if (this.newPwd !== '') {
      this.showConfirmIcon = true
        // when the confirm password is empty
      if (this.isOldPwdRight && this.isNewPwdRight && this.confirmPwd === '') {
        this.isConfirmPwdRight = false
        this.confirmPwdErrorMsg = 'confirm_pwd_empty'
        return
      }
        // the new password equires the confirm password
      if (this.confirmPwd === this.newPwd) {
        this.isConfirmPwdRight = true
      } else {
          // the new password is not the same as the confirm password
        this.isConfirmPwdRight = false
        this.confirmPwdErrorMsg = 'password_not_same'
      }
    } else {
        // when the new passowrd is empty
      this.showConfirmIcon = false
    }
  }
    /**
     * focus function of login ID.
     */
  idFocus (e) {
    if (e.target.value !== '') {
      this.showIDDelete = true
    }
  }
    /**
     * blur function of login ID.
     */
  idBlur (e) {
    this.checkIDRule()
  }
    /**
     * keyup function of login ID.
     */
  idKeyup (e) {
    if (this.loginID === '' || this.loginID === undefined) {
      this.showIDDelete = false
    } else {
      this.showIDError = false
      this.showIDDelete = true
    }
    this.loginID = e.target.value
    this.checkIsUnchanged()
  }
    /**
     * check whether the login ID is valid.
     */
  checkIDRule () {
    if (this.loginID === '') {
      return
    }
    _.delay(() => {
      this.showIDDelete = false
      if (this.blackListCache !== '') {
          // login ID only contain letters and numbers
        if (!(/^[A-Z|a-z|0-9]+$/.test(this.loginID))) {
            let numLetters = this.loginID.replace(/[A-Z|a-z|0-9]/ig, '')
            let numLettersArr = numLetters.split('')
            if (numLettersArr.length !== 0) {
              let hasLettersOfBlackList = false
              // check the character is in the black list or not
              nameOuter: for (let i = 0; i < numLettersArr.length; i++) {
                for (let j = 0; j < this.blackListCache.length; j++) {
                  if (numLettersArr[i] === this.blackListCache.charAt(j)) {
                    hasLettersOfBlackList = true
                    break nameOuter
                  }
                }
              }
              // if have show the error
              if (hasLettersOfBlackList) {
                this.showIDError = true
                this.idCommonError = false
                this.invalidID = true
                return
              }
            }
          }
      }
      let loginName = _.find(this.loginIDs, item => {
        return item === this.loginID
      })
      if (this.loginID !== '' && !_.isUndefined(loginName)) {
        this.showIDError = true
        this.idCommonError = true
        this.invalidID = false
        this.idErrorType = 'login_name_repeat'
        return
      }
      this.showIDError = false
    }, 200)
  }
    /**
     * clear the login ID.
     */
  deleteID () {
    this.loginID = ''
    this.showIDDelete = false
    this.checkIsUnchanged()
  }
    /**
     * check whether the setting is not changed.
     */
  checkIsUnchanged () {
    if (!this.lastStep) {
      if (this.oldPwd === '' && this.newPwd === '' && this.confirmPwd === '') {
        this.changed = false
      } else {
        this.changed = true
      }
    } else {
      if (this.loginID === '') {
        this.changed = false
      } else {
        this.changed = true
      }
    }
  }
    /**
     * get the inputing-tips of password.
     */
  getPasswordTips () {
    let tips = this.translate.instant('password_require') + this.translate.instant('new_password_simple')
    tips += ' ' + this.translate.instant('new_pasaword_tips')
      // user password mini length tip
    if (this.pwdConfig['userPwdMinLength'] >= 1) {
      tips += ' ' + this.translate.instant('password_min_length', { minLength: this.pwdConfig['userPwdMinLength'] })
    }
      // upper case letters
    if (this.pwdConfig['userPwdUpperCaseLetters']) {
      tips += ' ' + this.translate.instant('up_letter')
    }
      // lower case letters
    if (this.pwdConfig['userPwdLowerCaseLetters']) {
      tips += ' ' + this.translate.instant('low_letter')
    }
      // number
    if (this.pwdConfig['userPwdNumbers']) {
      tips += ' ' + this.translate.instant('digit')
    }
      // other letters
    if (this.pwdConfig['userPwdOthersLetters']) {
      tips += ' ' + this.translate.instant('special_char', { special_char: this.pwdConfig['userPwdOthersLetters'] })
    }
      // space
    if (!this.pwdConfig['userPwdSupportSpace']) {
      tips += ' ' + this.translate.instant('Space_not_allowed')
    }
    this.passwordInputTips = tips
  }
    /**
     * get the inputing-tips of login ID.
     */
  getLoginIDtips () {
    this.loginIDTips = { char: this.blackListCache }
  }
    /**
     * save the password or login ID.
     */
  saveOperation () {
    _.delay(() => {
      if (!this.changed) {
        return
      }
      if (!this.lastStep) {
        this.savePassword()
      } else {
        this.saveLoginID()
      }
    }, 300)
  }
    /**
     * set password of new profile or modify the password of sub profile.
     */
  savePassword () {
      // if modify the password and the old password is empty,show the tip
    if (this.type === 'modify') {
      if (this.oldPwd === '') {
        this.oldPwdErrorMsg = 'old_pwd_empty'
        this.isOldPwdRight = false
        this.showOldIcon = true
        return
      }
    }
      // if the new passowrd is empty
    if (this.newPwd === '') {
      this.newPwdErrorMsg = 'new_pwd_empty'
      this.isNewPwdRight = false
      this.showNewIcon = true
      return
    }
      // if the confirm passowrd is empty
    if (this.confirmPwd === '') {
      this.confirmPwdErrorMsg = 'confirm_pwd_empty'
      this.isConfirmPwdRight = false
      this.showConfirmIcon = true
      return
    }
    if (!this.isOldPwdRight || !this.isNewPwdRight || !this.isConfirmPwdRight) {
      return
    }
      // if modify the passowrd, check the old passowrd
    if (this.type === 'modify') {
      let checkReq = {
        password: this.oldPwd,
        type: '3'
      }
      checkPassword(checkReq).then(resp => {
          // old password check successfully
        if (resp['result'] && resp['result'].retCode === '000000000') {
            let modifyReq = {
              profile: {
                ID: this.subData['ID'],
                password: this.newPwd,
                profileType: '1'
              }
            }
            modifyProfile(modifyReq).then(resps => {
              // modify successfully
              if (resps['result'] && resps['result'].retCode === '000000000') {
                EventService.emit('PROFILE_INFO_SAVE_SUCCESSFUL')
                this.close()
              }
            })
          } else {
            // old password check failed
            this.showOldIcon = true
            this.isOldPwdRight = false
            this.oldPwdErrorMsg = 'password_not_correct_try_again'
          }
          // old password check
      }, respFail => {
        this.showOldIcon = true
        this.isOldPwdRight = false
        this.oldPwdErrorMsg = 'password_not_correct_try_again'
      })
    } else {
      this.lastStep = true
      this.btnName = 'save'
      this.changed = false
      this.loginIDs = session.get('ALL_LOGIN_ID')
    }
  }
    /**
     * set the login ID.
     */
  saveLoginID () {
      // the login ID cannot be empty
    if (this.profileManageService.trim(this.loginID) === '') {
      this.loginID = ''
      this.showIDError = true
      this.idCommonError = true
      this.invalidID = false
      this.idErrorType = 'login_id_empty'
      return
    }
    if (this.showIDError) {
      return
    }
      // get the avatar
    let logoUrlReq = this.subData['avatar'].slice((this.subData['avatar'].lastIndexOf('/') + 1),
        this.subData['avatar'].lastIndexOf('.'))
    if (logoUrlReq === 'ondemand_casts') {
      logoUrlReq = undefined
    }
      // get the limit
    let req = {
      profile: {
        name: this.subData['name'],
        loginName: this.loginID,
        ratingName: this.subData['ratingName'],
        ratingID: this.subData['ratingID'],
        password: this.newPwd,
        profileType: '1',
        subscriberID: this.subData['subscriberID']
      }
    }
    if (!_.isUndefined(logoUrlReq)) {
      req['profile']['logoURL'] = logoUrlReq
    }
      // add profile
    addProfile(req).then(resp => {
      let names = session.get('ALL_PROFILE_NAME')
      let loginNames = session.get('ALL_LOGIN_ID')
      names.push(req['name'])
      loginNames.push(req['name'])
      session.put('ALL_PROFILE_NAME', names)
      session.put('ALL_LOGIN_ID', loginNames)
      this.addProfileSuccess.emit()
      EventService.emit('PROFILE_INFO_SAVE_SUCCESSFUL')
      this.close()
    })
  }
}
