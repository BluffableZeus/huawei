import * as _ from 'underscore'
import { Component, OnInit, OnDestroy } from '@angular/core'
import { EventService } from 'ng-epg-sdk/services'
import { session } from 'src/app/shared/services/session'

@Component({
  selector: 'app-settingsuspension',
  templateUrl: './subTitles-audio.component.html',
  styleUrls: ['./subTitles-audio.component.scss']
})

export class SettingSuspensionComponent implements OnInit, OnDestroy {
  public subTitlesList = []
  public audioList = []
  public curSubTitles = ''
  public curAudio = ''
  public bottomImgStyle = {}
  public livetvFlag: number
  public titleIndex = -2
  public audioIndex = -2
  public selectSkip = false
  public isVod = true
  public isTrailer = false
  public showSkipOption = true
  public userID = ''

  ngOnInit () {
    let self = this
      // get the message of subtitle and audio track, show default status that is subtitle be selected
    EventService.on('GET_SETTINGLIST', function (datas) {
      self.userID = Cookies.getJSON('USER_ID')
      self.isVod = true
      if (session.get('clipFlag')) {
        self.isTrailer = true
      } else {
        self.isTrailer = false
      }
      if (session.get(self.userID + '_isSkipHeadAndTail')) {
        self.selectSkip = true
      } else {
        self.selectSkip = false
      }
      self.subTitlesList = []
      self.audioList = []
      for (let i = 0; i < datas.subtitleList.length; i++) {
        if (datas.subtitleList[i] !== '' && datas.subtitleList[i] !== 'und' && datas.subtitleList[i] !== 'undefined') {
            self.subTitlesList.push(datas.subtitleList[i])
          }
      }
      for (let i = 0; i < datas.audioList.length; i++) {
        if (datas.audioList[i] !== '' && datas.audioList[i] !== 'und' && datas.audioList[i] !== 'undefined') {
            self.audioList.push(datas.audioList[i])
          }
      }
      if (datas['livetvFlag']) {
        self.livetvFlag = 1
        self.bottomImgStyle = {
            display: 'block',
            left: '238px'
          }
      } else {
        self.livetvFlag = 0
      }
      self.subTitlesList = _.uniq(self.subTitlesList)
      self.audioList = _.uniq(self.audioList)
      self.initCurrentSubtitle(datas.curSubTitle)
      self.curAudio = datas.curAudio
    })
    EventService.on('GET_SETTINGLIST_LIVE', function (datas) {
      self.isVod = false
      self.subTitlesList = []
      self.audioList = []
      for (let i = 0; i < datas.subtitleList.length; i++) {
        if (datas.subtitleList[i] !== '' && datas.subtitleList[i] !== 'und' && datas.subtitleList[i] !== 'undefined') {
            self.subTitlesList.push(datas.subtitleList[i])
          }
      }
      for (let i = 0; i < datas.audioList.length; i++) {
        if (datas.audioList[i] !== '' && datas.audioList[i] !== 'und' && datas.audioList[i] !== 'undefined') {
            self.audioList.push(datas.audioList[i])
          }
      }
      if (datas['livetvFlag']) {
        self.livetvFlag = 1
        self.bottomImgStyle = {
            display: 'block',
            left: '238px'
          }
      } else {
        self.livetvFlag = 0
      }
      self.subTitlesList = _.uniq(self.subTitlesList)
      self.audioList = _.uniq(self.audioList)
      self.initCurrentSubtitle(datas.curSubTitle)
      self.curAudio = datas.curAudio
    })

    EventService.on('BACK_VOD_SETSUBTITLES', function (info) {
      self.curSubTitles = info
    })
  }

  ngOnDestroy () {
    EventService.removeAllListeners(['GET_SETTINGLIST'])
  }

    /**
     * choose subtitle call the player method set the corresponding subtitle
     */
  setSubTitles (list) {
    if (this.livetvFlag === 1) {
      EventService.emit('LIVE_SETSUBTITLES', list)
    } else if (this.livetvFlag === 0) {
      EventService.emit('VOD_SETSUBTITLES', list)
    }
    this.curSubTitles = list
  }

    /**
     * choose audio track call the player method set the corresponding audio track
     */
  setAudio (list) {
    if (this.livetvFlag === 1) {
      EventService.emit('LIVE_SETSAUDIO', list)
    } else if (this.livetvFlag === 0) {
      EventService.emit('VOD_SETSAUDIO', list)
    }
    this.curAudio = list
  }

  enterSubTitle (i) {
    this.titleIndex = i
  }

  leaveSubTitle () {
    this.titleIndex = -2
  }

  enterAudio (i) {
    this.audioIndex = i
  }

  leaveAudio () {
    this.audioIndex = -2
  }

    /**
     * select whether skip the head and tail.
     */
  skipSelect () {
    if (this.selectSkip) {
      this.selectSkip = false
      session.remove(this.userID + '_isSkipHeadAndTail')
      EventService.emit('REMOVE_SKIP_HEAD_AND_TAIL')
    } else {
      this.selectSkip = true
      session.put(this.userID + '_isSkipHeadAndTail', true, true)
      EventService.emit('SET_SKIP_HEAD_AND_TAIL')
    }
  }

    /**
     * set value of property 'showSkipOption'.
     */
  setShowSkipOption (value: boolean) {
    this.showSkipOption = value
  }

    /**
     * initialize the value of property 'curSubTitles'.
     */
  initCurrentSubtitle (value: string) {
    if (!value || value === '' || value === 'und' || this.subTitlesList.length === 0) {
      this.curSubTitles = 'off'
    } else {
      this.curSubTitles = value
    }
  }
}
