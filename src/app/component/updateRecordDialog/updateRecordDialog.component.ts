import { Component, OnInit, Input, Output, EventEmitter, AfterViewChecked } from '@angular/core'
import { EventService } from 'ng-epg-sdk/services'
import { RecordDataService } from '../recordDialog/recordData.service'
import { config as EPGConfig } from '../../shared/services/config'
import * as _ from 'underscore'
import { CommonService } from '../../core/common.service'
import { session } from 'src/app/shared/services/session'

@Component({
  selector: 'app-update-record-dialog',
  styleUrls: ['./updateRecordDialog.component.scss'],
  templateUrl: './updateRecordDialog.component.html'
})

export class UpDateRecordDialogComponent implements OnInit, AfterViewChecked {
  @Output() closeUpdata: EventEmitter<Object> = new EventEmitter()
    // whether show more
  public showExtendContent = false
  public prePaddingArrow = 'down'
  public postPaddingArrow = 'down'
  public preTime = 0
  public endTime = 0
  public arrowPreList = []
  public arrowPostList = []
    // whether the recording model is not NPVR
  public notNPVR = false
    // whether [Single] was selected
  public isSingle = false
    // whether [Series] was selected
  public isSeries = false
    // whether support change to [Single]
  public isSupportSingle = false
    // whether support change to [Series]
  public isSupportSeries = false
    // save the corresponding [storageTyp] which was current selected recoring type
  public storageType = 'NPVR'
    // whether show [prePadding]
  public prePaddingFalse = true
    // whether show [postPadding]
  public postPaddingFalse = true
  public deleteModelFalse = true
    // save PVR's ID
  public pvrId = ''
    // save PVRMode
  public recordingMode
    // the default definition value, SD correspond 0, HD correspond 1, 4K correspond 2
  public defaultQualityKey
    // save the [Quality] object which is current packaging
  public curQualityObj = {
    'quality': '',
    'qualityKey': -1,
    'cpvrMediaID': ''
  }
    // save the [Quality] object which is initial packaging
  public initQualityObj = {}
    // whether [Cloud] was selected
  public selectCloud = true
    // save definition
  public defaultQuality = 'HD'
    // whether show pull down list of the [Quality]
  public showQualityList = false
    // save status: 1.Scheduled recording; 2.recording; 3.recording finished
  public recordStatus
    // whether has arrow for show more
  public hasExtendArrow = true
    // the playbill whether support NPVR recording
  public isNPVR = false
    // the playbill whether support CPVRrecording
  public isCPVR = true
    // whether meet the conditions for modify [Quality],
    // series recording and the single recording of scheduled recording can modify [Quality]
  public hasQuality = true
  public supportAllCPVR = false
  public showType = true
  public npvrDefinition = []
  public config: any = {}
  public qualityList = []
  public isChanged = false
  public initIsSingle = false
  public initIsSeries = false
  public initSelectCloud = true
  public initPreTime = 0
  public initEndTime = 0

  constructor (
        private recordDataService: RecordDataService,
        private commonService: CommonService
    ) {
    this.arrowPreList = [0, 5, 10, 20, 30, 45]
    this.arrowPostList = [0, 5, 10, 20, 30, 45]
    this.getEPGConfig()
    this.setDefQualityKey()
  }

  @Input() set updateData (data) {
    this.isChanged = false
    this.config = data
  }

  ngOnInit () {
    if (this.config['update']) {
      this.pvrId = this.config['ID']
      if (this.config['policyType'] === 'PlaybillBased') {
        if (this.config['status'] === 'WAIT_RECORD') {
            // waiting for recording, the offse in before can be modify

          } else if (this.config['status'] === 'RECORDING') {
            // recording, the offse in before can not modify
            this.prePaddingFalse = false
          } else if (this.config['status'] === 'FAIL') {
            this.prePaddingFalse = false
            this.postPaddingFalse = false
            this.deleteModelFalse = false
          } else {
            this.prePaddingFalse = false
            this.postPaddingFalse = false
          }
        this.manAgeSingleRecordType('PlaybillBased')
        this.preTime = this.config['beginOffset'] / 60
        this.endTime = this.config['endOffset'] / 60
        this.recordStatus = this.config['status']
        if (this.recordStatus !== 'WAIT_RECORD') {
            this.hasQuality = false
          }
      } else if (this.config['policyType'] === 'Series') {
          if (this.config['isOverdue'] === '1') {
            this.prePaddingFalse = false
            this.postPaddingFalse = false
          }
          this.manAgeSingleRecordType('Series')
          this.preTime = this.config['beginOffset'] / 60
          this.endTime = this.config['endOffset'] / 60
          this.recordStatus = this.getRecordStatus()
        }
    }
    this.initPreTime = this.preTime
    this.initEndTime = this.endTime
    if (this.recordingMode !== 'NPVR') {
      this.notNPVR = true
    }
    this.setStorageType()
    this.getShowType()
    this.manAgeQuality()
    EventService.removeAllListeners(['CLOSE_UPDATE_RECORD_DIALOG'])
    EventService.on('CLOSE_UPDATE_RECORD_DIALOG', () => {
      this.close()
    })
  }

  getRecordStatus () {
    let status = this.config['childPVR'] && this.config['childPVR']['status']
    return status
  }

  setStorageType () {
    if (this.config['storageType'] === 'CPVR') {
      this.selectCloud = false
      this.storageType = 'CPVR'
    } else {
      this.selectCloud = true
      this.storageType = 'NPVR'
    }
    this.initSelectCloud = this.selectCloud
  }

  getShowType () {
    if (!this.supportAllCPVR && this.recordingMode === 'CPVR') {
      this.showType = false
    } else {
      this.showType = true
    }
  }

  ngAfterViewChecked () {
    if (document.getElementById('update-record-content') &&
            document.getElementById('update-record-content').getElementsByTagName('div').length > 0) {
      this.hasExtendArrow = true
    } else {
      this.hasExtendArrow = false
    }
  }

    /**
     * get EPG config data.
     */
  getEPGConfig () {
    this.recordingMode = EPGConfig['PVRMode']
    this.defaultQuality = EPGConfig['default_reccfg_definition']
    this.supportAllCPVR = EPGConfig['supportAllCPVR']
  }

    /**
     * set default quality key value.
     */
  setDefQualityKey () {
    switch (this.defaultQuality) {
      case 'SD':
        this.defaultQualityKey = 0
        break
      case 'HD':
        this.defaultQualityKey = 1
        break
      case '4K':
        this.defaultQualityKey = 2
        break
    }
  }

    /**
     * manage and package data of definition
     */
  manAgeQuality () {
    let self = this
    if (self.recordingMode !== 'NPVR') {
      let qualityList = []
      let qualityObj = {}
      let userType
      if (Cookies.getJSON('IS_PROFILE_LOGIN')) {
        userType = Cookies.getJSON('USER_ID')
      } else {
        userType = session.get('Guest')
      }
      let staticData = session.get(userType + '_Static_Version_Data')
      let dynamicData = session.get(userType + '_Dynamic_Version_Data')
      _.map(dynamicData['channelDynamaicProp'], (list1, index) => {
        _.each(staticData['channelDetails'], (list) => {
            if (list1['ID'] === list['ID']) {
              list['physicalChannelsDynamicProperties'] = list1['physicalChannelsDynamicProperties']
            }
          })
      })
      let channelDetail = _.find(staticData['channelDetails'], item => {
        return item['ID'] === self.config['channelID']
      })
      self.judgeIsNPVR()
      self.judgeIsCPVR()
      let dynamicProperty = this.commonService.judgeRecordDynamic('cpvrRecordCR', channelDetail)
      let cPhysicalChannels = _.filter(channelDetail['physicalChannels'], item => {
        let findPhysical = _.find(dynamicProperty, itemDynamic => {
            return itemDynamic['ID'] === item['ID']
          })
        if (findPhysical) {
            return true
          }
      })
      if (cPhysicalChannels.length > 0) {
        _.map(cPhysicalChannels, item => {
            if (!_.isUndefined(item['definition'])) {
              if (Number(item['definition']) === 0) {
                qualityObj = {
                  'quality': 'SD',
                  'qualityKey': 0,
                  'cpvrMediaID': item['ID']
                }
              } else if (Number(item['definition']) === 1) {
                qualityObj = {
                  'quality': 'HD',
                  'qualityKey': 1,
                  'cpvrMediaID': item['ID']
                }
              } else if (Number(item['definition']) === 2) {
                qualityObj = {
                  'quality': '4K',
                  'qualityKey': 2,
                  'cpvrMediaID': item['ID']
                }
              }
            }
            if (qualityList.length > 0) {
              let existed = _.find(qualityList, function (data) {
                return data['quality'] === qualityObj['quality']
              })
              if (!existed) {
                qualityList.push(qualityObj)
              }
            } else {
              qualityList.push(qualityObj)
            }
          })
        qualityList = _.sortBy(qualityList, ql => {
            return ql.qualityKey
          })
        this.qualityList = qualityList
        if (self.recordingMode !== 'NPVR') {
            let sameQualityObj
            if (self.config['storageType'] === 'CPVR') {
              sameQualityObj = _.find(qualityList, item => {
                return item['qualityKey'] === Number(self.config['definition'][0])
              })
            }
            if (!_.isUndefined(sameQualityObj)) {
              this.curQualityObj = sameQualityObj
            } else {
              let defaultQualityObj = _.find(qualityList, item => {
                return item['qualityKey'] === self.defaultQualityKey
              })
              if (!_.isUndefined(defaultQualityObj)) {
                this.curQualityObj = defaultQualityObj
              } else {
                let higherQualityObj = _.find(qualityList, item => {
                  return item['qualityKey'] > self.defaultQualityKey
                })
                if (!_.isUndefined(higherQualityObj)) {
                  this.curQualityObj = higherQualityObj
                } else {
                  this.curQualityObj = _.max(qualityList, item => {
                    return item['qualityKey']
                  })
                }
              }
            }
            this.initQualityObj = this.curQualityObj
          }
      }
      this.npvrDefinition = []
      let nPhysicalChannels = this.commonService.judgeRecordDynamic('npvrRecordCR', channelDetail)
      _.map(nPhysicalChannels, item => {
        this.npvrDefinition.push(Number(item['definition']))
      })
    }
  }

  judgeIsNPVR () {
    let self = this
    if (self.config['playbill'] && self.config['playbill']['isNPVR'] && self.config['playbill']['isNPVR'] === '1') {
      self.isNPVR = true
    } else {
      self.isNPVR = false
    }
  }

  judgeIsCPVR () {
    let self = this
    if (self.config['playbill'] && self.config['playbill']['isCPVR'] && self.config['playbill']['isCPVR'] === '1') {
      self.isCPVR = true
    } else {
      self.isCPVR = false
    }
  }

    /**
     * manage default value for single recording
     * whether can click about other recording status
     */
  manAgeSingleRecordType (type) {
    if (type === 'PlaybillBased') {
      this.isSingle = true
      this.isSeries = false
      this.isSupportSingle = true
      if (this.getSeriesID()) {
        this.isSupportSeries = true
      }
    } else if (type === 'Series') {
      this.isSingle = false
      this.isSeries = true
      this.isSupportSingle = false
      this.isSupportSeries = true
    }
    this.initIsSingle = this.isSingle
    this.initIsSeries = this.isSeries
  }

    /**
     * click pull down arrow, show [Quality] list
     */
  choiceQuality () {
    this.showQualityList = !this.showQualityList
    this.postPaddingArrow = 'down'
    this.prePaddingArrow = 'down'
  }

    /**
     * need to hidden list when mouse leave definition list
     */
  mouseOutQualityList () {
    this.showQualityList = false
  }

    /**
     * choose definition
     */
  selectQuality (qualityObj) {
    this.curQualityObj = qualityObj
    this.showQualityList = false
    this.checkChange()
  }

    /**
     * choose storage location
     * 1.cloud
     * 2.stb
     */
  selectStorage (data) {
    if (data === 'cloud') {
      this.storageType = 'NPVR'
      this.selectCloud = true
    } else {
      this.storageType = 'CPVR'
      this.selectCloud = false
    }
    this.checkChange()
  }

    /**
     * choose recording type
     * 1.single
     * 2.series
     */
  changeFocus (type) {
    if (type === 'single') {
      if (this.isSupportSingle) {
        this.isSingle = true
        this.isSeries = false
        if (this.recordStatus !== 'WAIT_RECORD') {
            this.hasQuality = false
            this.prePaddingFalse = false
          } else {
            this.hasQuality = true
            this.prePaddingFalse = true
          }
      }
    } else {
      if (this.isSupportSeries) {
        this.isSingle = false
        this.isSeries = true
        this.hasQuality = true
        this.prePaddingFalse = true
      }
      if (!this.supportAllCPVR) {
        this.storageType = 'NPVR'
        this.selectCloud = true
      }
    }
    this.checkChange()
  }

    /**
     * add recording
     * single recording
     * series recording
     */
  updateRecord () {
    if (!this.isChanged) {
      return
    }
    let AllMessage = {}
    let sendMessage = {}
    AllMessage['ID'] = this.pvrId
    this.getBeginOffset(AllMessage)
    this.getEndOffset(AllMessage)
    AllMessage['playbillID'] = this.config['playbillID']
    AllMessage['channelID'] = this.config['channelID']
    AllMessage['storageType'] = this.storageType
    sendMessage = { allMessage: AllMessage }
    if (this.storageType === 'CPVR') {
      AllMessage['cpvrMediaID'] = this.curQualityObj['cpvrMediaID']
      sendMessage = { allMessage: AllMessage, definition: [this.curQualityObj.qualityKey] }
    } else {
      sendMessage['definition'] = this.npvrDefinition
    }
    if (this.config['policyType'] === 'PlaybillBased') {
      if (this.isSingle === true) {
          // single recording update
        AllMessage['policyType'] = 'PlaybillBased'
        let changedMsg = this.getChangedMsg(AllMessage, this.config)
        changedMsg['ID'] = this.pvrId
        this.recordDataService.updatePVR(changedMsg, 'PlaybillBasedPVR').then(function (resp) {
            this.close()
            if (resp['isNPVRSpaceEnough'] && resp['isNPVRSpaceEnough'] === '0') {
              EventService.emit('RECORD_SPACE_NOT_ENOUGH', { type: 'successful' })
            }
            EventService.emit('RECORD_UPDATE_SUCCESS', sendMessage)
          }.bind(this), function (resp) {
            this.close()
            let conflictData = {}
            if (resp['result']['retCode'] === '147020005') {
              EventService.emit('RECORD_SPACE_NOT_ENOUGH', { type: 'failed', source: 'playbillUpdate' })
            } else if (resp.conflictGroups && resp.conflictGroups !== '') {
              conflictData = { resp: resp, data: AllMessage, type: 'PlaybillBasedPVR', update: true }
              EventService.emit('RECORD_CONFLICT', conflictData)
            }
          }.bind(this))
      } else if (this.isSeries === true) {
          // single recording to chenge series recording, add series recording
          let data = {
            beginOffset: (this.preTime * 60) + '' || '0',
            endOffset: (this.endTime * 60) + '' || '0',
            playbillID: this.config['playbillID'],
            storageType: this.storageType,
            channelID: this.config['channelID']
          }
          data['seriesID'] = this.getSeriesID()
          data['policyType'] = 'Series'
          if (this.storageType === 'CPVR') {
            data['cpvrMediaID'] = this.curQualityObj['cpvrMediaID']
          }
          this.recordDataService.addPeriodicPVR(data, 'SeriesPVR', null, true, this.config['ID']).then(function (resp) {
            this.close()
            if (resp['isNPVRSpaceEnough'] && resp['isNPVRSpaceEnough'] === '0') {
              EventService.emit('RECORD_SPACE_NOT_ENOUGH', { type: 'successful' })
            }
            EventService.emit('RECORD_UPDATE_ADD_SUCCESS')
            EventService.emit('DELETESPACE', 'querypvrspace3')
          }.bind(this), function (resp) {
            this.close()
            let conflictData = {}
            if (resp['result']['retCode'] === '147020005') {
              EventService.emit('RECORD_SPACE_NOT_ENOUGH', { type: 'failed', source: 'playbillUpdate' })
            } else if (resp.conflictGroups && resp.conflictGroups !== '') {
              conflictData = { resp: resp, data: AllMessage, type: 'SeriesPVR' }
              if (resp['conflictGroups'][0]['PVRList'][0]['policyType'] === 'PlaybillBased' ||
                            resp['conflictGroups'][0]['PVRList'][0]['policyType'] === 'TimeBased') {
                EventService.emit('MIXED_CONFLICT', conflictData)
              } else {
                EventService.emit('RECORD_CONFLICT', conflictData)
              }
            }
          }.bind(this))
        }
    } else if (this.config['policyType'] === 'Series') {
        // series recording update, can not change to single recording
      AllMessage['seriesID'] = this.getSeriesID()
      AllMessage['policyType'] = 'Series'
      let changedMsg = this.getChangedMsg(AllMessage, this.config)
      changedMsg['ID'] = this.pvrId
      this.recordDataService.updatePeriodicPVR(changedMsg, 'SeriesPVR').then(function (resp) {
          this.close()
          if (resp['isNPVRSpaceEnough'] && resp['isNPVRSpaceEnough'] === '0') {
            EventService.emit('RECORD_SPACE_NOT_ENOUGH', { type: 'successful' })
          }
          EventService.emit('RECORD_UPDATE_SUCCESS', sendMessage)
        }.bind(this), function (resp) {
          this.close()
          let conflictData = {}
          if (resp['result']['retCode'] === '147020005') {
            EventService.emit('RECORD_SPACE_NOT_ENOUGH', { type: 'failed', source: 'playbillUpdate' })
          } else if (resp.conflictGroups && resp.conflictGroups !== '') {
            conflictData = { resp: resp, data: AllMessage, type: 'SeriesPVR', update: true }
            if (resp['conflictGroups'][0]['PVRList'][0]['policyType'] === 'PlaybillBased' ||
                        resp['conflictGroups'][0]['PVRList'][0]['policyType'] === 'TimeBased') {
              EventService.emit('MIXED_CONFLICT', conflictData)
            } else {
              EventService.emit('RECORD_CONFLICT', conflictData)
            }
          }
        }.bind(this))
    }
  }

  getBeginOffset (AllMessage) {
    if (this.preTime !== this.config['beginOffset']) {
      AllMessage['beginOffset'] = (this.preTime * 60) + '' || '0'
    } else {
      AllMessage['beginOffset'] = this.config['beginOffset'] + ''
    }
  }

  getEndOffset (AllMessage) {
    if (this.endTime !== this.config['endOffset']) {
      AllMessage['endOffset'] = (this.endTime * 60) + '' || '0'
    } else {
      AllMessage['endOffset'] = this.config['endOffset'] + ''
    }
  }

    /**
     * get the data which has changed
     */
  getChangedMsg (AllMessage, config) {
    let changedMsg = {}
    if (AllMessage['storageType'] !== config['storageType']) {
      changedMsg['storageType'] = AllMessage['storageType']
    }
    if (AllMessage['storageType'] === 'CPVR') {
      if (AllMessage['storageType'] !== config['storageType']) {
        changedMsg['cpvrMediaID'] = AllMessage['cpvrMediaID']
      } else if (AllMessage['cpvrMediaID'] !== this.initQualityObj['cpvrMediaID']) {
          changedMsg['cpvrMediaID'] = AllMessage['cpvrMediaID']
        }
    }
    if (AllMessage['beginOffset'] !== Number(config['beginOffset'])) {
      changedMsg['beginOffset'] = AllMessage['beginOffset']
    }
    if (AllMessage['endOffset'] !== Number(config['endOffset'])) {
      changedMsg['endOffset'] = AllMessage['endOffset']
    }
    return changedMsg
  }

    /**
     * recording time advanced, whether delete pull down box manually
     * show, display
     */
  extendedSet () {
    if (this.showExtendContent) {
      this.showExtendContent = false
    } else {
      this.showExtendContent = true
      this.prePaddingArrow = 'down'
      this.postPaddingArrow = 'down'
    }
  }

    /**
     * deal with the arrow.
     */
  dealArrow (type) {
    if (!this.prePaddingFalse && type === 'prePaddingArrow') {
      return
    }
    if (!this.postPaddingFalse && type === 'postPaddingArrow') {
      return
    }
    if (this[type] === 'up') {
      this[type] = 'down'
    } else {
      this[type] = 'up'
      if (type === 'prePaddingArrow') {
        this.postPaddingArrow = 'down'
      } else if (type === 'postPaddingArrow') {
          this.prePaddingArrow = 'down'
        }
    }
  }

    /**
     * select value of pre-padding.
     */
  changePrePadding (data) {
    this.preTime = data
    this.prePaddingArrow = 'down'
    this.checkChange()
  }

    /**
     * select value of post-padding.
     */
  changePostPadding (data) {
    this.endTime = data
    this.postPaddingArrow = 'down'
    this.checkChange()
  }

    /**
     * close the dialog.
     */
  close () {
    this.closeUpdata.emit()
  }

    /**
     * get the array of definition.
     */
  getDefinition (qualityList) {
    let definition = []
    _.map(qualityList, item => {
      definition.push(item['qualityKey'])
    })
    return definition
  }

  getSeriesID () {
    return this.config['playbill'] && this.config['playbill']['playbillSeries'] &&
            this.config['playbill']['playbillSeries']['seriesID']
  }

  checkChange () {
    this.isChanged = false
    if (this.isTypeChanged()) {
      return
    }
    if (this.isStorageChanged()) {
      return
    }
    if (this.isQualityChanged()) {
      return
    }
    if (this.showExtendContent || !this.hasExtendArrow) {
      if (this.isPrepaddingChanged()) {
        return
      }
      if (this.isPostPaddingChanged()) {

      }
    }
  }

  isTypeChanged () {
    if (this.isSupportSeries && this.isSupportSingle && this.showType) {
      if (this.isSingle !== this.initIsSingle || this.isSeries !== this.initIsSeries) {
        this.isChanged = true
      }
    }
    return this.isChanged
  }

  isStorageChanged () {
    if ((this.recordingMode === 'CPVR&NPVR' || this.recordingMode === 'MIXPVR') &&
            this.npvrDefinition.length > 0 && this.qualityList.length > 0 &&
            (this.isSeries || (this.isSingle && this.recordStatus === 'WAIT_RECORD' && this.isNPVR && this.isCPVR))) {
      if (this.initSelectCloud !== this.selectCloud) {
        this.isChanged = true
      }
    }
    return this.isChanged
  }

  isQualityChanged () {
    if (this.notNPVR && this.hasQuality && !this.selectCloud && this.qualityList.length > 1) {
      if (!_.isEqual(this.curQualityObj, this.initQualityObj)) {
        this.isChanged = true
      }
    }
    return this.isChanged
  }

  isPrepaddingChanged () {
    if (this.prePaddingFalse && this.preTime !== this.initPreTime) {
      this.isChanged = true
    }
    return this.isChanged
  }

  isPostPaddingChanged () {
    if (this.postPaddingFalse && this.endTime !== this.initEndTime) {
      this.isChanged = true
    }
    return this.isChanged
  }
}
