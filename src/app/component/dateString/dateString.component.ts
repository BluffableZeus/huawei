import * as _ from 'underscore'
import { Injectable } from '@angular/core'
import { session } from 'src/app/shared/services/session'
import { CommonService } from '../../core/common.service'

@Injectable()
export class DateStingComponent {
  constructor (
        private commonService: CommonService
  ) { }
  /**
     * Convert date to string.
     * Date.now() String toLowerCase
     */
  dateToString (date, fmt) {
    let o = {
      'M+': date.getMonth() + 1,
      'd+': date.getDate(),
      'H+': date.getHours(),
      'm+': date.getMinutes(),
      's+': date.getSeconds(),
      'S': date.getMilliseconds()
    }

    if (/(y+)/.test(fmt)) {
      fmt = fmt.replace(RegExp.$1, (date.getFullYear() + '').substr(4 - RegExp.$1.length))
    }
    for (let k in o) {
      if (new RegExp('(' + k + ')').test(fmt)) {
        fmt = fmt.replace(RegExp.$1, (RegExp.$1.length === 1) ? (o[k])
          : (('00' + o[k]).substr(('' + o[k]).length)))
      }
    }
    return fmt
  }

  /**
     * Convert string to date.
     * String toLowerCase
     */
  stringToDate (dateString, format): any {
    let year, month, day, hour, minute, second
    switch (format) {
      case 'yyyyMMddHHmmss': {
        year = dateString.substring(0, 4)
        month = dateString.substring(4, 6)

        if (month[0] === '0') {
          month = parseInt(month[1], 10) - 1
        } else {
          month = parseInt(month, 10) - 1
        }
        day = dateString.substring(6, 8)
        hour = dateString.substring(8, 10)
        minute = dateString.substring(10, 12)
        second = dateString.substring(12, 14)

        return new Date(year, month, day, hour, minute, second)
      }
      case 'yyyy-MM-dd HH:mm:ss': {
        year = dateString.substring(0, 4)
        month = dateString.substring(5, 7)
        if (month[0] === '0') {
          month = parseInt(month[1], 10) - 1
        } else {
          month = parseInt(month, 10) - 1
        }
        day = dateString.substring(8, 10)
        hour = dateString.substring(11, 13)
        minute = dateString.substring(14, 16)
        second = dateString.substring(17, 19)

        return new Date(year, month, day, hour, minute, second)
      }
      default:
        return Date.now()
    }
  }

  /**
     * Convert second to date.
     * String toLowerCase
     */
  secondToDate (seconds) {
    let hours
    hours = new Date(parseInt(seconds, 10)).getHours()
    let minutes
    minutes = new Date(parseInt(seconds, 10)).getMinutes()

    if (Math.floor(hours) < 10) {
      hours = '0' + hours
    }
    if (Math.floor(minutes) < 10) {
      minutes = '0' + minutes
    }
    return hours + ':' + minutes
  }

  playbillRange (today, end, start, startTime, emdTime) {
    let range
    let isInYesterdayAndToday = this.judgeTimePoint(today, startTime, emdTime)
    // Time point in today's program
    if (today['getDate']() === startTime.getDate() && startTime.getDate() === emdTime.getDate()) {
      range = end - start
      // The time point is in the program of yesterday and today
    } else if (isInYesterdayAndToday) {
      range = end
      // The time point is in today's and tomorrow's program
    } else if ((today['getDate']() < emdTime.getDate() && today['getDate']() === startTime.getDate()) ||
            (today['getMonth']() < emdTime.getMonth() && today['getDate']() === startTime.getDate()) ||
            (today['getFullYear']() < emdTime.getFullYear() && today['getDate']() === startTime.getDate())) {
      range = 24 * 60 - start
    }
    return range
  }

  judgeTimePoint (today, startTime, endTime) {
    let isInYesterdayAndToday = (today['getDate']() > startTime.getDate() && today['getDate']() === endTime.getDate()) ||
            (today['getMonth']() > startTime.getMonth() && today['getDate']() === endTime.getDate()) ||
            (today['getFullYear']() > startTime.getFullYear() && today['getDate']() === endTime.getDate())
    return isInYesterdayAndToday
  }

  /**
     * Hour of difference
     */
  getEndtoStartHour (starttimeSeconds, emdTimeSeconds, newDate?: any) {
    let startTime = new Date(parseInt(starttimeSeconds, 10))
    let emdTime = new Date(parseInt(emdTimeSeconds, 10))
    let today = newDate && this.stringToDate(newDate.time, 'yyyyMMddHHmmss') || Date.now()
    let endHours, endMinutes, startHours, startMinutes
    endHours = emdTime.getHours()
    endMinutes = emdTime.getMinutes()
    startHours = startTime.getHours()
    startMinutes = startTime.getMinutes()
    const end = parseInt(endHours, 10) * 60 + parseInt(endMinutes, 10)
    const start = parseInt(startHours, 10) * 60 + parseInt(startMinutes, 10)

    return this.playbillRange(today, end, start, startTime, emdTime)
  }

  /**
     * To determine whether the time is the current time
     */
  judgeCurrentTime (starttimeSeconds, emdTimeSeconds) {
    let time
    time = Date.now()['getTime']()
    let currentTime = parseInt(time, 10)
    if (currentTime > starttimeSeconds && currentTime < emdTimeSeconds) {
      return true
    } else {
      return false
    }
  }

  getImgCount (playbill, channelRatingID) {
    let count = 0
    if (playbill.PVRTaskType === '1' || playbill.PVRTaskType === '2') {
      count = count + 1
    }
    if (!playbill.rating) {
      playbill.rating = {}
      playbill.rating.ID = session.get('PLAYBILLMAXRATING')
    }
    count = this.getCount(playbill, channelRatingID, count)
    return count
  }

  getCount (playbill, channelRatingID, count) {
    if ((playbill.rating && (Number(session.get('PROFILERATING_ID')) < playbill.rating.ID ||
            Number(session.get('PROFILERATING_ID')) < channelRatingID))) {
      count = count + 1
    }
    if (playbill.reminderStatus && playbill.reminderStatus === '1') {
      count = count + 1
    }
    if (playbill.hasRecordingPVR === '1' && (Number(playbill.endTime) > Number(Date.now()['getTime']())) ||
            playbill.hasRecordingPVR === '2' && (Number(playbill.endTime) > Number(Date.now()['getTime']()))) {
      count = count + 1
    }
    return count
  }

  /**
     * The ending time of the program is not the same, then add the corresponding program, and display as No Program
     */
  compleChannelPlaybillLites (playbillLites, newDate, channelRatingID) {
    let self = this
    let playbillArray = _.map(playbillLites, function (playbill) {
      if (playbill) {
        playbill['showtime'] = self.commonService.dealWithDateJson('D10', playbill['startTime']) + ' - ' +
                    self.commonService.dealWithDateJson('D10', playbill['endTime'])
        playbill[playbill['ID'] + 'isCurrent'] =
                    self.judgeCurrentTime(playbill['startTime'], playbill['endTime'])
        let timeWidth = (self.getEndtoStartHour(playbill['startTime'], playbill['endTime'], newDate) *
                    (228 / 30) - 2)
        if (playbill['showtime'] && playbill['showtime'].indexOf('DST') !== -1) {
          if (timeWidth === -1) {
            timeWidth = 455
          }
        }
        timeWidth = timeWidth > 0 ? timeWidth : 1
        let count = self.getImgCount(playbill, channelRatingID)
        playbill['timeWidth'] = timeWidth + 'px'
        playbill['timeWidthInt'] = timeWidth
        let timeWIdthNum = timeWidth - (50 + count * 26)
        playbill['titleWidth'] = timeWIdthNum > 16 ? timeWIdthNum + 'px' : '0px'
        playbill['titleWidthInt'] = timeWIdthNum > 16 ? timeWIdthNum : 0
        playbill['ImgWidth'] = (playbill['titleWidthInt'] === 0) ? timeWidth - 30 + 'px'
          : timeWidth - playbill['titleWidthInt'] - 50 + 'px'
        playbill['ImgWidthInt'] = (playbill['titleWidthInt'] === 0) ? timeWidth - 30
          : timeWidth - playbill['titleWidthInt'] - 50
      }
      return playbill
    })
    return playbillArray
  }
}
