import { Component, OnInit } from '@angular/core'
import { EventService } from 'ng-epg-sdk/services'
import { DialogRef, DialogParams } from 'ng-epg-ui/webtv-components/dialog'
import { session } from 'src/app/shared/services/session'
@Component({
  selector: 'app-mixed-conflict',
  templateUrl: './mixed-conflict.component.html',
  styleUrls: ['./mixed-conflict.component.scss']
})

export class MixedConflictComponent implements OnInit {
    // defined variable
  public conflictNumber
  constructor (
        private dialog: DialogRef,
        private config: DialogParams
    ) {
  }
  ngOnInit () {
      // get the number of conflict
    if (this.config && this.config['resp']['conflictGroups'] && this.config['resp']['conflictGroups']['length']) {
      this.conflictNumber = this.config['resp']['conflictGroups']['length']
      this.conflictNumber = { conflictNumber: this.conflictNumber }
    }
  }
    /**
     *  close the dialog
     */
  close () {
    this.dialog.close()
      // if the user close the dialog, show the fail tip
    EventService.emit('CONFLICT_RECORDED_FAIL')
  }
    /**
     *  turn to conflict list
     */
  solveConflicts () {
    session.put('MIXED_CONFLICT', 'true')
    EventService.emit('RECORD_CONFLICT', this.config)
    this.dialog.close()
  }
    /**
     * compel solving the conflict
     */
  recordEpisodes () {
    let self = this
    let date = {data: self.config['data'],
      haveChooseAddID: undefined,
      type: self.config['type'],
      update: self.config['update']}
    EventService.emit('SOLVE_ALL_RECORD_CONFLICT', date)
    this.dialog.close()
  }
}
