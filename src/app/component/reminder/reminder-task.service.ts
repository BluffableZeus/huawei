import * as _ from 'underscore'
import { TranslateService } from '@ngx-translate/core'
import { Injectable, ViewContainerRef, OnDestroy } from '@angular/core'
import { CommonService } from '../../core/common.service'
import { ReminderComponent } from './reminder.component'
import { session } from 'src/app/shared/services/session'
import { queryReminder } from 'ng-epg-sdk/vsp'
import { EventService } from 'ng-epg-sdk/services'
import { config as EPGConfig } from '../../shared/services/config'
@Injectable()
export class ReminderTask implements OnDestroy {
  private reminderTimer: any
  private isOpenDialog = false
  public reminders: Array<any> = []
  private isFirstLoad = true
  private reminderList: any = []
  private nextReminder: any = []
  private localReminder: any = []
  private alsoReminderData: any = []
  constructor (
        public commonService: CommonService,
        private translateService: TranslateService
    ) { }

    /**
     * open reminder window
     */
  openReminderDialog (viewContainerRef: ViewContainerRef, errorObject) {
    return this.commonService.openDialog(ReminderComponent, null, errorObject)
  }

  ngOnDestroy () {
    clearTimeout(this.reminderTimer)
  }

    /**
     * get reminder data
     */
  getReminder () {
      // if not the user login, or not support reminder, do not loading reminder data
    if (!Cookies.getJSON('IS_PROFILE_LOGIN') || !EPGConfig['reminderMode']) {
      return
    }
    queryReminder({
      count: '50',
      offset: '0'
    }).then(resp => {
        // get all reminder data
      this.reminders = resp.reminders
        // if reminder list number more than 0, and less than all number, call interface to get data again
      if (this.reminders.length > 0 && !(Number(resp.total) > this.reminders.length)) {
          this.reminders = this.sliceReplayData(this.reminders, this.alsoReminderData)
          this.orderReminderPopTime(this.reminders)
        }
    })
  }

  sliceReplayData (parentArr, childArr?) {
    if (!childArr) {
      return
    }
    let result = []
    for (let i = 0; i < parentArr.length; i++) {
      let obj = parentArr[i]
      let num = obj && obj['playbill'] && obj['playbill']['ID'] || obj[0]['playbill']['ID']
      let isExist = false
      for (let j = 0; j < childArr.length; j++) {
        let aj = childArr[j]
        let n = aj && aj['playbill'] && aj['playbill']['ID'] || aj[0]['playbill']['ID']
        if (n === num) {
            isExist = true
            break
          }
      }
      if (!isExist) {
        result.push(obj)
      }
    }
    return result
  }

    /**
     * add or delete reminder
     */
  addOrRemoveReminder (type, playbill) {
    let leadTimeForSendReminder = session.get('leadTimeForSendReminder') || 300000
    if (type === 'add') {
      let reminderPlaybill = {}
      let playbillDetail = this.getPlaybillDetail(playbill)
      reminderPlaybill['contentType'] = 'PROGRAM'
      reminderPlaybill['playbill'] = {
        'ID': playbillDetail['ID'],
        'channel': playbill['channelDetail'] || playbill['channelInfo'],
        'channelID': playbill['channelDetail'] && playbill['channelDetail']['ID'] || playbill['channelInfo'] &&
                playbill['channelInfo']['ID'],
        'endTime': playbillDetail['endTime'],
        'genres': playbillDetail['genres'],
        'hasRecordingPVR': playbillDetail['hasRecordingPVR'],
        'introduce': playbillDetail['introduce'],
        'isBlackout': playbillDetail['isBlackout'],
        'isCPVR': playbillDetail['isCPVR'],
        'isCUTV': playbillDetail['isCUTV'],
        'isNPVR': playbillDetail['isNPVR'],
        'name': playbillDetail['name'],
        'picture': playbillDetail['picture'],
        'playbillSeries': playbillDetail['playbillSeries'],
        'rating': playbillDetail['rating'],
        'reminder': {},
        'startTime': playbillDetail['startTime']
      }
      reminderPlaybill['playbill']['reminder']['contentID'] = playbillDetail['ID']
      reminderPlaybill['playbill']['reminder']['contentType'] = reminderPlaybill['contentType']
      reminderPlaybill['playbill']['reminder']['endTime'] = playbillDetail['endTime']
      reminderPlaybill['playbill']['reminder']['profileID'] = session.get('PROFILE_ID')
      reminderPlaybill['playbill']['reminder']['reminderTime'] = Number(playbillDetail['startTime']) - leadTimeForSendReminder
      reminderPlaybill['playbill']['reminder']['fromLocal'] = true
      this.reminders = this.reminders.concat(reminderPlaybill)
      this.orderReminderPopTime(this.reminders)
    } else if (type === 'remove') {
      if (playbill === 'clearAll') {
          // save current reminderList and clearAll current reminderList
          let curReminderList = session.get('REMINDERLIST')
          _.each(curReminderList, (list, index) => {
            let findObj = _.find(this.reminders, item => {
              return item['playbill']['ID'] === list['playbill']['ID']
            })
            let indexObj = _.indexOf(this.reminders, findObj)
            if (indexObj !== -1) {
              this.reminders.splice(indexObj, 1)
            }
          })
          this.alsoReminderData = this.sliceReplayData(this.alsoReminderData, curReminderList)
          curReminderList = []
          this.orderReminderPopTime(this.reminders)
        } else if (playbill === 'clearAllReminderManage') {
          clearTimeout(this.reminderTimer)
          this.reminders = []
          this.alsoReminderData = []
          this.orderReminderPopTime(this.reminders)
        } else {
          let ID = this.getID(playbill)
          let IDs = []
          let alsoReminderIDs = []
          for (let i = 0; i < this.reminders.length; i++) {
            IDs.push(this.reminders[i]['playbill']['ID'])
          }
          let index = _.indexOf(IDs, ID)
          if (index !== -1) {
            this.reminders.splice(index, 1)
          }
          alsoReminderIDs = this.getAlsoReminderIDs(alsoReminderIDs, this.alsoReminderData)
          let indexs = _.indexOf(alsoReminderIDs, ID)
          if (indexs !== -1) {
            this.alsoReminderData.splice(index, 1)
          }
          this.orderReminderPopTime(this.reminders)
        }
    }
  }

  getAlsoReminderIDs (alsoReminderIDs, alsoReminderData) {
    for (let i = 0; i < alsoReminderData.length; i++) {
      if (alsoReminderData && alsoReminderData[i] && alsoReminderData[i]['playbill'] && alsoReminderData[i]['playbill']['ID']) {
        alsoReminderIDs.push(alsoReminderData[i]['playbill']['ID'])
      } else {
        alsoReminderIDs.push(alsoReminderData[i][0]['playbill']['ID'])
      }
    }
    return alsoReminderIDs
  }

    /**
     * get playbill detail
     */
  getPlaybillDetail (playbill) {
    let detail = playbill && playbill['playbillDetail'] || playbill['playbillInfo']
    return detail
  }

    /**
     * get playbill id
     */
  getID (playbill) {
    let id = playbill['playbillDetail'] && playbill['playbillDetail']['ID'] ||
         playbill['playbillInfo'] && playbill['playbillInfo']['ID'] || playbill['playbillId']
    return id
  }

    /**
     * sort Reminder pop time
     */
  orderReminderPopTime (reminders) {
    this.localReminder = []
    this.reminderList = []
    this.nextReminder = []
    _.each(reminders, item => {
      if ((Number(item['playbill']['reminder'].reminderTime) < Date.now()['getTime']()) && !item['playbill']['reminder'].fromLocal) {
          // the program is playing but not end
        item['isStart'] = true
        this.reminderList.push(item)
      } else if ((Number(item['playbill']['reminder'].reminderTime) < Date.now()['getTime']()) &&
             item['playbill']['reminder'].fromLocal) {
          item['isStart'] = false
          this.localReminder.push(item)
        } else if (item['playbill']['reminder'].fromLocal &&
             Number(item['playbill']['reminder'].reminderTime) > Date.now()['getTime']()) {
          item['isStart'] = false
          this.nextReminder.push(item)
        } else {
          item['isStart'] = false
          this.nextReminder.push(item)
        }
    })
    if (this.localReminder.length > 0) {
      if (this.isOpenDialog) {
        this.alsoReminderData.push(this.localReminder)
        EventService.emit('ADD_REMINDER_TO_LIST', this.localReminder)
      } else {
        this.isOpenDialog = true
        this.alsoReminderData.push(this.localReminder)
        this.commonService.openDialog(ReminderComponent, null, this.localReminder)
      }
      _.each(this.localReminder, (list, index) => {
        let findObj = _.find(this.reminders, item => {
            return item['playbill']['ID'] === list['playbill']['ID']
          })
        let indexObj = _.indexOf(this.reminders, findObj)
        if (indexObj !== -1) {
            this.reminders.splice(indexObj, 1)
          }
      })
      this.orderReminderPopTime(this.reminders)
    }
      // if [reminderTime] less than current time or equal to current time, pop it immediately
    if (this.reminderList.length > 0 && this.isFirstLoad) {
      this.isFirstLoad = false
        // if the popup window has open, add it to the list, or open new popup window
      if (this.isOpenDialog) {
        this.alsoReminderData.push(this.reminderList)
        EventService.emit('ADD_REMINDER_TO_LIST', this.reminderList)
      } else {
        this.isOpenDialog = true
        this.alsoReminderData.push(this.reminderList)
        this.commonService.openDialog(ReminderComponent, null, this.reminderList)
      }
      _.each(this.reminderList, (list, index) => {
        let findObj = _.find(this.reminders, item => {
            return item['playbill']['ID'] === list['playbill']['ID']
          })
        let indexObj = _.indexOf(this.reminders, findObj)
        if (indexObj !== -1) {
            this.reminders.splice(indexObj, 1)
          }
      })
      this.orderReminderPopTime(this.reminders)
    }
    this.isFirstLoad = false
      // sort by reminder time
    this.nextReminder = _.sortBy(this.nextReminder, item => {
      return item['playbill']['reminder'].reminderTime
    })
      // if there are reminder before maturity
    if (this.nextReminder.length > 0) {
      let firstReminder = this.nextReminder[0]
        // get the nearest reminder from current time
      this.reminderList = _.filter(this.nextReminder, (item) => {
        return Number(item['playbill']['reminder'].reminderTime) ===
                    Number(firstReminder['playbill']['reminder'].reminderTime)
      })
      clearTimeout(this.reminderTimer)
      this.reminderTimer = setTimeout(() => {
        if (this.isOpenDialog) {
            EventService.emit('ADD_REMINDER_TO_LIST', this.reminderList)
            this.alsoReminderData.push(this.reminderList)
          } else {
            if (this.reminderList.length === 0) {
              return
            }
            this.isOpenDialog = true
            this.alsoReminderData.push(this.reminderList)
            this.commonService.openDialog(ReminderComponent, null, this.reminderList)
          }
          // pop reminder, call interface get data again, clear information again
        _.each(this.reminderList, (list, index) => {
            let findObj = _.find(this.reminders, item => {
              return item['playbill']['ID'] === list['playbill']['ID']
            })
            let indexObj = _.indexOf(this.reminders, findObj)
            if (indexObj !== -1) {
              this.reminders.splice(indexObj, 1)
            }
          })
        this.orderReminderPopTime(this.reminders)
      }, this.reminderList[0]['playbill']['reminder'].reminderTime - Date.now()['getTime']())
    }
  }
}
