import * as _ from 'underscore'
import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { EventService } from 'ng-epg-sdk/services'
import { DialogRef, DialogParams } from 'ng-epg-ui/webtv-components/dialog'
import { TranslateService } from '@ngx-translate/core'
import { ReminderService } from './reminder.service'
import { PlaybillAppService } from '../playPopUpDialog/playbillApp.service'
import { TVODAppService } from '../playPopUpDialog/tVodApp.service'
import { session } from 'src/app/shared/services/session'

@Component({
  selector: 'app-reminder',
  templateUrl: './reminder.component.html',
  styleUrls: ['./reminder.component.scss']
})

export class ReminderComponent implements OnInit {
  public num: any
  public reminderList: Array<any>
  public curIndex = 0
  public curReminder: any
  public sendData
  constructor (
        private router: Router,
        private dialog: DialogRef,
        private translate: TranslateService,
        private reminderData: DialogParams,
        private reminder: ReminderService,
        private playbillAppService: PlaybillAppService,
        private tVODAppService: TVODAppService
    ) { }

  ngOnInit () {
    this.reminderList = _.extend([], this.reminder.formatReminder(this.reminderData))
    session.put('HASPOPREMINDER', this.reminderList.concat(this.reminderList))
    this.sendData = _.extend([], this.reminder.formatReminderSendData(this.reminderData))
      // judge the title number on reminder box
    if (this.reminderList.length > 1) {
      this.num = { num: this.reminderList.length }
    }
    this.curReminder = this.reminderList[0]
    _.delay(() => {
      document.getElementsByTagName('md-dialog-container')[0]['style']['position'] = 'fixed'
    }, 500)
      // if the reminder box has open,  time to reminder, add in reminder list
    EventService.on('ADD_REMINDER_TO_LIST', (data) => {
      this.reminderList = this.reminderList.concat(this.reminder.formatReminder(data))
      session.put('HASPOPREMINDER', this.reminderList)
      this.num = { num: this.reminderList.length }
    })
    session.put('REMINDERLIST', this.reminderList)
  }

    /**
     * ignore one reminder
     */
  ignore () {
    this.reminder.removeReminder({
      contentIDs: [this.curReminder.playbillId],
      contentTypes: ['PROGRAM']
    }, 1).then((resp) => {
      EventService.emit('REMOVE_REMINDER', this.sendData[0])
      EventService.emit('REMOVE_REMINDER_FROM_DIALOG', { playbillId: this.curReminder.playbillId })
      EventService.emit('REMOVE_REMINDER_FOR_TVGUIDE', { playbillId: this.curReminder.playbillId })
        // delete one reminder, if the number > 1
      if (this.reminderList.length > 1) {
          // remove if according to the position in list
          this.reminderList.splice(this.curIndex, 1)
          // if delete reminder in the final, positioning to the first
          if (Number(this.curIndex) === Number(this.reminderList.length)) {
            this.curIndex = 0
          }
          this.curReminder = this.reminderList[this.curIndex]
          // modify the reminder number in title
          if (this.reminderList.length > 1) {
            this.num = { num: this.reminderList.length }
          }
        } else {
          // only has one reminder, close popup window, and change the tag for judge whether the popup window has open to [no]
          EventService.emit('CLOSE_REMINDER_DIALOG')
          this.dialog.close()
        }
    })
  }

    /**
     * ignore all reminder that has show already
     */
  ignoreAll () {
    let IDs = _.pluck(this.reminderList, 'playbillId')
    let contentTypes = _.pluck(this.reminderList, 'contentType')
    this.reminder.removeReminder({
      contentIDs: IDs,
      contentTypes: contentTypes
    }, 1).then((resp) => {
      EventService.emit('REMOVE_REMINDER', 'clearAll')
      EventService.emit('REMOVE_ALL_REMINDER_FROM_DIALOG', IDs)
      EventService.emit('REMOVE_REMINDER_FOR_TVGUIDE_ALL', IDs)
    })
    EventService.emit('CLOSE_REMINDER_DIALOG')
    this.dialog.close()
  }

    /**
     * watch playbill
     */
  watch () {
      // judge the vod is playing or not
    if (session.get('VOD_IS_PLAYING')) {
        // judge the vod is playing in full screen
      if (session.get('VOD_IS_FULLSCREEN')) {
        EventService.on('EXIT_VOD_FULLSCREEN_FOR_REMINDER', () => {
            _.delay(() => {
              this.openFullScreen()
            })
          })
      } else {
        this.openFullScreen()
      }
      session.put('REMINDER_FLAG', 1)
      EventService.emit('PAUSE_FROM_REMINDER')
    } else {
      this.openFullScreen()
    }
  }

  openFullScreen () {
      // open full screen
    let video = document.querySelector('#videoContainer video')
    let ua = navigator.userAgent.toLowerCase()
    let isIe = !!ua.match(/msie/i) || !!ua.match(/rv:([\d.]+).*like gecko/)
    let isFirefox = !!ua.match(/firefox\/([\d.]+)/)
    let isEdge = !!ua.match(/edge\/([\d.]+)/)
    if (isIe) {
      video = document.querySelector('#videoContainer')
      video['msRequestFullscreen']()
    } else if (isEdge) {
      video = document.querySelector('#videoContainer')
      video.webkitRequestFullScreen()
    } else if (isFirefox) {
        video = document.querySelector('#videoContainer')
        video['mozRequestFullScreen']()
      } else if (ua.match(/version\/([\d.]+).*safari/)) {
        video.requestFullscreen()
      } else {
        video.webkitRequestFullScreen()
      }
    if (Date.now()['getTime']() > this.curReminder['endTime']) {
        // query playbill detail and channel detail according to the playbill ID
      this.reminder.getplaybillDetail(this.curReminder).then(resp => {
          // if current time more than the end time of playbill, the playbill has timeout[TVOD], or is on live
        this.tVODAppService.playTvod(resp['playbillDetail'], resp['playbillDetail']['channelDetail'])
        EventService.emit('OPEN_FULLSCREEN_LIVETVVIDEO', {})
      })
    } else {
      this.reminder.getChannelDetail(this.curReminder).then(resp => {
        this.playbillAppService.playProgram(resp['channelPlaybills'][0]['playbillLites'][0],
            resp['channelPlaybills'][0]['channelInfos'])
        EventService.emit('OPEN_FULLSCREEN_LIVETVVIDEO', {})
      })
    }
      // close reminder
    EventService.emit('CLOSE_REMINDER_DIALOG')
    this.dialog.close()
  }

    /**
     * switch next reminder
     */
  next () {
    this.curIndex++
      // if switch the last one, change the position to first
    if (this.curIndex > this.reminderList.length - 1) {
      this.curIndex = 0
    }
    this.curReminder = this.reminderList[this.curIndex]
  }

    /**
     * switch pre reminder
     */
  previous () {
    this.curIndex--
      // if switch the first, change the position to the last one
    if (this.curIndex < 0) {
      this.curIndex = this.reminderList.length - 1
    }
    this.curReminder = this.reminderList[this.curIndex]
  }
}
