import * as _ from 'underscore'
import { Injectable } from '@angular/core'
import { TranslateService } from '@ngx-translate/core'
import { config as EPGConfig } from '../../shared/services/config'
import { createReminder, deleteReminder, getPlaybillDetail, queryPlaybillList } from 'ng-epg-sdk/vsp'
import { EventService } from 'ng-epg-sdk/services'
import { CustomConfigService } from '../../shared/services/custom-config.service'
import { CommonService } from '../../core/common.service'
import { ChannelCacheService } from '../../shared/services/channel-cache.service'
import { session } from 'src/app/shared/services/session'
@Injectable()
export class ReminderService {
  constructor (
        private translate: TranslateService,
        private customConfigService: CustomConfigService,
        private commonService: CommonService,
        private channelCacheService: ChannelCacheService
  ) { }

  /**
     * package the data for reminder
     */
  formatReminder (list) {
    let reminders = _.map(list, (item) => {
      let playbill = item['playbill']
      let time = this.commonService.dealWithDateJson('D09', playbill['startTime'])
      return {
        playbillId: playbill['ID'],
        channelId: playbill['channel'].ID,
        playbillName: playbill['name'],
        season: { num: playbill['playbillSeries'] && playbill['playbillSeries']['seasonNO'] },
        episode: { num: playbill['playbillSeries'] && playbill['playbillSeries']['sitcomNO'] },
        reminderTip: { time: time, channelname: playbill['channel']['name'] || playbill['name'] },
        channelname: playbill['channel']['name'],
        startTime: playbill['startTime'],
        endTime: playbill['endTime'],
        reminderTime: playbill['reminder'].reminderTime,
        playbill: playbill,
        isStart: item['isStart'],
        contentType: item['contentType']
      }
    })
    return reminders
  }

  /**
     * judge whether reminder show and judge the status
     */
  isSupportReminder (playbill): any {
    if (EPGConfig['reminderMode']) {
      if (playbill['reminder'] && playbill['reminder'].reminderTime) {
        return 'CANCEL'
      } else {
        if (playbill['startTime'] - Date.now()['getTime']() > 0) {
          return 'ADD'
        } else {
          return false
        }
      }
    } else {
      return false
    }
  }

  /**
    * add reminder
    */
  addReminder (req) {
    return createReminder(req).then((resp) => {
      if (resp['result']['retCode'] === '000000000') {
        EventService.emit('addReminderSuccess')
      }
      return resp
    }, (resp) => {
      return resp
    })
  }

  /**
     * delete reminder
     */
  removeReminder (req, isIgnore?) {
    return deleteReminder(req).then((resp) => {
      if (resp['result']['retCode'] === '000000000') {
        if (isIgnore !== 1) {
          EventService.emit('removeReminderSuccess')
        }
      }
      return resp
    }, (resp) => {
      return resp
    })
  }

  /**
     * get channel playbill detail
     */
  getplaybillDetail (playbill) {
    return this.customConfigService.getCustomizeConfig({ queryType: '0' }).then(config => {
      let customizeConfig = config['list']
      let nameSpace = customizeConfig['ott_channel_name_space']
      return getPlaybillDetail({
        playbillID: playbill['playbillId'],
        channelNamespace: nameSpace
      }).then(resp => {
        return resp
      })
    })
  }
  /**
     * get current channel playbill detail
     */
  getChannelDetail (playbill) {
    return queryPlaybillList({
      needChannel: '0',
      queryPlaybill: {
        type: '0',
        count: '1',
        offset: '0',
        startTime: Date.now()['getTime']() + '',
        isFillProgram: '1'
      },
      queryChannel: {
        channelIDs: [playbill['channelId']],
        isReturnAllMedia: '1'
      }
    }).then(resp => {
      let channelDtails = this.channelCacheService.getStaticChannelData().channelDetails
      let dynamicChannelData = this.channelCacheService.getDynamicChannelData().channelDynamaicProp
      // get the dynamic data from dynamic cache
      _.each(resp.channelPlaybills, (item) => {
        item['channelInfos'] = _.find(channelDtails, (detail) => {
          return detail && detail['ID'] === item['playbillLites'][0].channelID
        })
        _.find(dynamicChannelData, (detail) => {
          if (detail && item['channelInfos'] && item['channelInfos']['ID'] === detail['ID']) {
            item['channelInfos']['channelNO'] = detail['channelNO']
            let physicalChannelsDynamicProperty = _.find(detail['physicalChannelsDynamicProperties'], (property) => {
              return _.contains(property['channelNamespaces'], session.get('ott_channel_name_space'))
            })
            item['channelInfos']['physicalChannelsDynamicProperty'] =
                            physicalChannelsDynamicProperty || detail['physicalChannelsDynamicProperties'][0]
          }
        })
      })
      return resp
    })
  }
  formatReminderSendData (reminderData) {
    let reminders = _.map(reminderData, (item) => {
      let playbill = item['playbill']
      let time = this.commonService.dealWithDateJson('D09', playbill['startTime'])
      return {
        playbillDetail: item['playbill'],
        playbillId: playbill['ID'],
        channelId: playbill['channelId'],
        playbillName: playbill['name'],
        season: { num: playbill['playbillSeries'] && playbill['playbillSeries']['seasonNO'] },
        episode: { num: playbill['playbillSeries'] && playbill['playbillSeries']['sitcomNO'] },
        reminderTip: { time: time, channelname: playbill['channel']['name'] },
        channelname: playbill['channel']['name'],
        startTime: playbill['startTime'],
        endTime: playbill['endTime'],
        reminderTime: playbill['reminder'].reminderTime,
        playbill: playbill,
        isStart: item['isStart'],
        contentType: item['contentType']
      }
    })
    return reminders
  }
}
