import { Component } from '@angular/core'
import { EventService } from 'ng-epg-sdk/services'
import { session } from 'src/app/shared/services/session'
import * as _ from 'underscore'
import { MediaPlayService } from '../mediaPlay/mediaPlay.service'

@Component({
  selector: 'app-preview',
  templateUrl: './preview.component.html',
  styleUrls: ['./preview.component.scss']
})

export class PreviewComponent {
    /**
     * preview image of current focus
     */
  public currentImg
  public currentUrlBgs?: any = []
  public Maxpicture = {
    pictureList: [],
    pictureCount: 0,
    pictureUrlBgs: []
  }
  public reloadPicture = []
    /**
     * show preview pictures or not
     */
  public isshow = false
  public imgContentLeft = '0px'
  public imgContentWidth
  public ContentWidth = '993px'
  public bottomWidth = '49px'
  public duration
  public percentTime
  public preview_show = false
  public contentScreenSize = '993px'
  public currentImgInt = 0
  public num = 8
  public getCurrent
  public ids = 1
  public isreloadPicture = []
  public tempCurrentImg
  public currentMouserLeft
  public isChrome = true
  clickImgFilter = _.debounce((fc: Function) => {
    fc()
  }, 150)
  constructor (
        private mediaPlayService: MediaPlayService) {
    let self = this
    EventService.on('preview_duration', (data) => {
      if (!this.duration) {
        this.duration = data
        this.picturePosition(session.get('VODDETAIL_PICTURE_PREVIEW') || [])
        this.imgContentWidth = self.Maxpicture['pictureCount'] * 114 + 249 + 'px'
        this.currentUrlBgs = self.Maxpicture['pictureUrlBgs']
      }
    })
    EventService.on('PREVIEW_SHOW_PLAY_VOD', () => {
      this.preview_show = true
    })
    EventService.on('PREVIEW_SHOW_PLAY_END', () => {
      this.duration = null
      this.preview_show = false
      this.isshow = false
    })
    EventService.on('ScreenSize', () => {
      let clientW = window.innerWidth
      if (clientW > 1440) {
        this.contentScreenSize = '993px'
      } else {
        this.contentScreenSize = '670px'
      }
    })
    let clientW = window.innerWidth
    if (clientW > 1440) {
      this.ContentWidth = '993px'
      this.contentScreenSize = '993px'
    } else {
      this.ContentWidth = '670px'
      this.contentScreenSize = '670px'
    }
    _.delay(() => {
      let ua = navigator.userAgent.toLowerCase()
      let isFirefox = !!ua.match(/firefox\/([\d.]+)/)
      let isevent
      if (isFirefox) {
        isevent = true
      } else {
        isevent = false
      }
        // the vodSeekbar object is non-existent when leaving the VOD detail page fast.
      if (document.getElementById('vodSeekbar')) {
        document.getElementById('vodSeekbar').addEventListener('mousemove', this.mouseHandler.bind(this), isevent)
        document.getElementById('vodSeekbar').addEventListener('mouseleave', this.mouseLeaveHandler.bind(this))
      }
    }, 1000)
    let explorer = navigator.userAgent.toLowerCase()
      // Chrome
    if (explorer.indexOf('chrome') >= 0) {
      this.isChrome = true
    } else {
      this.isChrome = false
    }
  }

  mouseHandler (event) {
    let ua = navigator.userAgent.toLowerCase()
    let isFirefox = !!ua.match(/firefox\/([\d.]+)/)
    let elementID
    if (isFirefox) {
      elementID = event.target['id']
    } else {
      elementID = event.srcElement['id']
    }
    if (elementID === 'vodSeekbar') {
      if (this.currentMouserLeft !== event['offsetX']) {
        this.currentMouserLeft = event['offsetX']
        this.mouseEnterScroll(event)
      }
    }
  }

  mouseLeaveHandler (e) {
    this.mouseLeaveScroll(e)
  }

    /**
     * count picture list Messages
     */
  picturePosition (picturepre) {
    let self = this
    _.map(picturepre, function (picture) {
      if (picture.toString().indexOf('272_') !== -1) {
        self.Maxpicture['pictureList'].push(picture)
      }
    })
    self.getMaxpicture()
    self.Maxpicture['pictureCount'] = Math.ceil(this.duration / this.percentTime)
    self.Maxpicture['pictureUrlBgs'] = self.Maxpicture['pictureUrlBgs'].splice(0, Math.ceil(this.duration / this.percentTime))
    self.reloadMaxpicture(self.Maxpicture['pictureUrlBgs'])
  }

  reloadMaxpicture (pictureMaps) {
    let self = this
    _.filter(pictureMaps, function (picture) {
      if (!_.contains(self.reloadPicture, picture['picture'])) {
        let img = new Image()
        img.onload = function () {
            img.onload = null
            if (img.complete) {
              self.isreloadPicture.push(true)
            }
          }
        img.onerror = function () {
            img.onerror = null
            self.isreloadPicture.push(false)
          }
        img['src'] = picture['picture']
        img['isload'] = false
        self.reloadPicture.push(img['src'])
        return true
      }
    })
  }

    /**
     * format of the picture is preview_272_153_12_25_1_300
     * they are the width, height, the interval of each picture, number, start time and end time.
     */
  getMaxpicture () {
    let self = this
    _.map(self.Maxpicture['pictureList'], function (picture, index) {
      let picUrl = _.last(picture.split('/')) + ''
      let num = parseInt(picUrl.split('_')[4], 10)
      self.percentTime = parseInt(picUrl.split('_')[3], 10)
      let startTime = parseInt(picUrl.split('_')[5], 10)
      for (let i = 0; i < num; i++) {
        let backg = {}
        let left = i % 5 + ''
        let top = i / 5 + ''
        let currentTime = startTime + self.percentTime * i >= self.duration ? self.duration : startTime + self.percentTime * i
        self.ids = self.ids + 1
        backg['background_size'] = '500% 500%'
        backg['left'] = -parseInt(left, 10) * 110
        backg['top'] = -parseInt(top, 10) * 61
        backg['picture'] = picture
        backg['time'] = self.formatTime(currentTime)
        backg['seconds'] = currentTime
        backg['ids'] = 'ids' + self.ids
        backg['backgrounds'] = ''
        backg['backgroundloads'] = 'url(' + picture + ' ) no-repeat ' + backg['left'] + 'px ' + backg['top'] +
                    'px / ' + backg['background_size']
        self.Maxpicture['pictureUrlBgs'].push(backg)
      }
    })
  }

    /**
     * formatting time
     */
  formatTime (currentTime) {
    let hour: any = parseInt(currentTime / 3600 + '', 10)
    if (hour < 10) {
      hour = '0' + hour + ':'
    } else {
      hour = hour + ':'
    }
    let restTime = parseInt(currentTime % 3600 + '', 10)

    let minutes: any = parseInt(restTime / 60 + '', 10)
    if (minutes < 10) {
      minutes = '0' + minutes + ':'
    } else {
      minutes = minutes + ':'
    }

    let seconds: any = parseInt(restTime % 60 + '', 10)
    if (seconds < 10) {
      seconds = '0' + seconds
    } else {
      seconds = seconds
    }
    return hour + minutes + seconds
  }

    /**
     * when the mouse moves to the progress bar, show preview pictures.
     */
  mouseEnter () {
    this.isshow = true
  }

  mouseEnterScroll (event) {
    if (this.isreloadPicture.length !== this.reloadPicture.length) {
      return
    }
    this.getCurrentImg(event)
      // keep current data all the time
    let left = this.currentImgInt * 114 + 114 / 2 - event['offsetX'] + 47
    if (event['offsetX'] <= 104) {
      left = this.currentImgInt * 114 - 5
    } else if (event['offsetX'] >= (parseInt(this.ContentWidth, 10) - 104)) {
      left = this.currentImgInt * 114 - parseInt(this.ContentWidth, 10) + 203
    }
    document.getElementsByClassName('imgContent')[0]['style']['left'] = -left + 'px'
  }

    /**
     * Calculate currentImg position and which
     */
  getCurrentImg (event) {
    let MediaplayerVod = this.mediaPlayService.isFullScreen()
    let ua = navigator.userAgent.toLowerCase()
    let isFirefox = !!ua.match(/firefox\/([\d.]+)/)
    this.ContentWidth = this.contentScreenSize
    if (MediaplayerVod) {
      this.num = 10
      this.bottomWidth = '62px'
      if (isFirefox) {
        this.bottomWidth = '74px'
      }
      this.ContentWidth = document.body.clientWidth + 'px'
    } else {
      this.num = 4
      this.bottomWidth = '49px'
      if (isFirefox) {
        this.bottomWidth = '52px'
      }
    }
    this.isshow = true
    this.currentImgInt = Math.ceil(event['offsetX'] / (parseInt(this.ContentWidth, 10) / this.currentUrlBgs.length)) - 1
    if (this.currentImgInt === this.tempCurrentImg) {
      return
    }
    this.tempCurrentImg = this.currentImgInt
    this.currentImg = this.currentUrlBgs[this.currentImgInt]
    let loadminnum = this.currentImgInt - this.num > 0 ? this.currentImgInt - this.num : 0
    let loadmaxnum = this.currentImgInt + this.num > this.currentUrlBgs.length
        ? this.currentUrlBgs.length : this.currentImgInt + this.num
    this.getCurrentUrls(loadminnum, this.currentImgInt, loadmaxnum)
  }

  getCurrentUrls (loadminnum, currentImgInt, loadmaxnum) {
    for (let i = loadminnum; i < currentImgInt; i++) {
      if (this.currentUrlBgs[i]['backgrounds'] === '') {
        this.currentUrlBgs[i]['backgrounds'] = this.currentUrlBgs[i]['backgroundloads']
      }
    }

    for (let i = currentImgInt; i < loadmaxnum; i++) {
      if (this.currentUrlBgs[i] && this.currentUrlBgs[i]['backgrounds'] === '') {
        this.currentUrlBgs[i]['backgrounds'] = this.currentUrlBgs[i]['backgroundloads']
      }
    }
  }

    /**
     * mouse enter the image
     */
  hoverEnterImg (img) {
    this.currentImg = img
  }

    /**
     * when the mouse moves out of the progress bar, hide preview pictures.
     */
  mouseLeaveScroll (e) {
    if (this.isChrome) {
      if (e && e['toElement']) {
        this.isshow = false
      }
    } else {
      this.isshow = false
    }
  }

    /**
     * when you click the preview picture, skip to the time point at which the preview image is played.
     */
  changePreview (imgbg) {
    this.clickImgFilter(() => {
      EventService.emit('PREVIEW_CHANGER_PICTURE', { time: imgbg.seconds })
    })
  }
}
