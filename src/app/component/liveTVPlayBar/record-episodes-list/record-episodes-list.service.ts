import * as _ from 'underscore'
import { Injectable } from '@angular/core'
import { RecordDataService } from '../../recordDialog/recordData.service'
import { PictureService } from 'ng-epg-ui/services/picture.service'
import { DateUtils } from 'ng-epg-sdk/utils'
import { CommonService } from '../../../core/common.service'

@Injectable()
export class RecordEpisodesListService {
  constructor (
        private pictureService: PictureService,
        private recordDataService: RecordDataService,
        private commonService: CommonService
  ) { }
  // query the record task that is recorded successfully.
  getRecordTask (parentPlanID, offset) {
    let PVRCondition = {
      policyType: ['PlaybillBased', 'TimeBased'],
      layout: { childPlanLayout: '0' },
      singlePVRFilter: {
        scope: 'ALL',
        storageType: 'NPVR',
        // only query the tasks that all or part of them is recorded successfully.
        status: ['OTHER', 'SUCCESS', 'PART_SUCCESS', 'RECORDING'],
        isFilterByDevice: '1',
        periodicPVRID: parentPlanID
      }
    }
    let queryPVRRequest = {
      count: '12',
      offset: offset + '',
      sortType: 'STARTTIME:ASC',
      PVRCondition: PVRCondition
    }
    let options = {
      params: {
        SID: 'querypvr4'
      }
    }
    return this.recordDataService.queryPVR(queryPVRRequest, options).then((resp) => {
      let list = _.map(resp.PVRList, (item) => {
        let channelName = this.itemChannelName(item)
        let poster = this.getPoster(item)
        let seasonNO = item['playbill'] && item['playbill'].playbillSeries && item['playbill'].playbillSeries.seasonNO
        let sitcomNO = item['playbill'] && item['playbill'].playbillSeries &&
                    this.formatNumber(item['playbill'].playbillSeries.sitcomNO)
        let month = DateUtils.format(parseInt(item['startTime'], 10), 'MM')
        month = this.commonService.dealWithTime(month)
        let lastTime = this.commonService.dealWithDateJson('D01', item['startTime'])
        return {
          id: item['ID'],
          name: item.name,
          parentPlanID: parentPlanID,
          weekTime: DateUtils.format(parseInt(item['startTime'], 10), 'ddd'),
          time: lastTime + ' | ' + this.commonService.dealWithDateJson('D10', item['startTime']) + '-' +
                        this.commonService.dealWithDateJson('D10', item['endTime']) + ' ',
          channelName: channelName,
          channelID: item['channelID'],
          poster: poster || 'assets/img/default/ondemand_trailers.png',
          seasonNO: { num: seasonNO },
          sitcomNO: { num: sitcomNO },
          isSeriesPage: 'series',
          startTime: item['startTime'],
          endTime: item['endTime'],
          fileID: item['files'] && item['files'][0] && item['files'][0]['fileID']
        }
      })
      return {
        list: list,
        total: resp.total
      }
    })
  }

  getPoster (item) {
    let poster = item['playbill'] && item['playbill']['picture'] && item['playbill']['picture'].posters &&
            this.pictureService.convertToSizeUrl(item['playbill']['picture'].posters[0],
              { minwidth: 124, minheight: 68, maxwidth: 124, maxheight: 68 })
    return poster
  }

  itemChannelName (item) {
    return item['extensionFields'] && item['extensionFields'][0] && item['extensionFields'][0].values &&
            item['extensionFields'][0].values[0] || item['playbill'] && item['playbill'].channel && item['playbill'].channel.name
  }
  /**
     * add the zero.
     */
  formatNumber (num) {
    let result = ''
    if (Number(num) < 10) {
      result = '0' + num
    } else {
      result = num + ''
    }
    return result
  }
}
