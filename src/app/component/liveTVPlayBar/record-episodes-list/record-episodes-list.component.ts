import * as _ from 'underscore'
import { Component, Input, OnInit, OnDestroy } from '@angular/core'
import { EventService } from 'ng-epg-sdk/services'
import { DirectionScroll } from 'ng-epg-ui/webtv-components/scroll/directionScroll.component'
import { RecordEpisodesListService } from './record-episodes-list.service'
import { NPVRAppService } from '../../playPopUpDialog/nPvrApp.service'
@Component({
  selector: 'app-record-episodes',
  templateUrl: './record-episodes-list.component.html',
  styleUrls: ['./record-episodes-list.component.scss']
})

export class RecordEpisodesListComponent implements OnInit, OnDestroy {
  @Input() data: any
    // declare variables.
  public isShow = false
  public recordList: Array<any> = []
  private recordTotal: any
  private isToBottom = true
  private curPlayPVR: any
    // private constructor
  constructor (
        private directionScroll: DirectionScroll,
        private RecordEpisodsService: RecordEpisodesListService,
        private nPVRAppService: NPVRAppService

    ) { }
  ngOnInit () {
    this.curPlayPVR = this.data
    this.isShow = true
    this.getRecord()
      // lode the data when sliding the scroll bar.
    EventService.removeAllListeners(['SHOW_ALLCHANNEL_PROGRAM'])
    EventService.on('SHOW_ALLCHANNEL_PROGRAM', () => {
      if (document.getElementById('record-episodes')) {
        this.showRecordList()
      }
    })
      // change the focus of record list when play the next record task.
    EventService.on('CHANGE_PLAYING_NPVR', (npvrData) => {
      this.curPlayPVR = npvrData
    })
  }
  ngOnDestroy () { }

    /**
     * close the list of a subset of record.
     */
  close () {
    this.isShow = false
    _.delay(function () {
      EventService.emit('CLOSE_RECORD_EPISODES')
    }, 1000)
  }
    /**
     * when play.
     */
  play (npvr) {
    if (this.curPlayPVR['id'] === npvr['id']) {
      return
    }
    this.curPlayPVR = npvr
    EventService.emit('HIDE_POPUP_DIALOG')
    EventService.emit('OPEN_FULLSCREEN_LIVETVVIDEO', {})
    this.nPVRAppService.playNPVR(npvr)
  }
    /**
     * load the data when enter this page.
     */
  getRecord () {
    _.delay(() => {
      document.querySelector('.record-episodes-load')['style']['display'] = 'block'
    }, 500)
    this.RecordEpisodsService.getRecordTask(this.data['parentPlanID'], this.recordList.length).then(resp => {
      this.recordList = resp.list
      this.recordTotal = resp.total
      this.pluseOnScroll()
      _.delay(() => {
        document.querySelector('.record-episodes-load')['style']['display'] = 'none'
      }, 500)
    })
  }
    /**
     * listening to the event of scrolling,and loading the data that is not be loaded.
     */
  showRecordList () {
    if (this.isToBottom && this.recordTotal > this.recordList.length) {
      this.isToBottom = false
      document.querySelector('.record-episodes-load')['style']['display'] = 'block'
      this.RecordEpisodsService.getRecordTask(this.data['parentPlanID'], this.recordList.length).then(resp => {
        this.recordList = this.recordList.concat(resp.list)
        this.pluseOnScroll()
        this.isToBottom = true
        document.querySelector('.record-episodes-load')['style']['display'] = 'none'
      })
    }
  }
    /**
     * the event of scroll bar.
     */
  pluseOnScroll () {
    let oConter, oUl, oScroll, oSpan
    let oBox = document.getElementById('record-episodes')
    oConter = document.getElementById('record-episodes-list')
    oUl = document.getElementById('record-episodes-ul')
    oScroll = document.getElementById('record-episodes-scroll')
    oScroll.style.height = document.body.clientHeight - 118 + 'px'
    oConter.style.height = document.body.clientHeight - 112 + 'px'
      // hide the scroll bar when the data is less.
    if (this.recordList.length < 12) {
      return
    }
    if (oScroll) {
      oSpan = oScroll.getElementsByTagName('span')[0]
    }
    this.directionScroll.setScroll(oBox, oConter, oUl, oScroll, oSpan, false, 'isToBottom')
  }
    /**
     * hide the buttom of closing the list.
     */
  hidePosition () {
    return {
      'top': (document.body.clientHeight - 128) / 2 + 'px'
    }
  }
  listStyle () {
    return {
      'height': document.body.clientHeight + 'px'
    }
  }
}
