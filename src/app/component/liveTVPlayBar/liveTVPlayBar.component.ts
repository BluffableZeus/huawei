import * as _ from 'underscore'
import { Component, Input, ViewChild, OnInit, OnDestroy, AfterViewInit } from '@angular/core'
import { EventService } from 'ng-epg-sdk/services'
import { session } from 'src/app/shared/services/session'
import { config as EPGConfig } from '../../shared/services/config'
import { PlayPopUpDialogComponent } from '../playPopUpDialog/playPopUpDialog.component'
import { FullChannelComponent } from './liveTVControl/fullScreen-channelControl.component'
import { MiniEPGComponent } from '../mini-epg/mini-epg.component'
import { TVODAppService } from '../playPopUpDialog/tVodApp.service'
import { NPVRAppService } from '../playPopUpDialog/nPvrApp.service'
import { MediaPlayService } from '../mediaPlay/mediaPlay.service'
import { PlaybillAppService } from '../playPopUpDialog/playbillApp.service'
import { LiveTVMiniDetailComponent } from './live-tvbar-mini-detail/live-tvbar-mini-detail.component'
import { MiniDetailService } from './live-tvbar-mini-detail/live-tv-mini-detail.service'
import { PictureService } from 'ng-epg-ui/services/picture.service'
import { RecordDataService } from '../recordDialog/recordData.service'
import { ReminderTask } from '../reminder/reminder-task.service'
import { CommonService } from '../../core/common.service'
import { StopRecordConfirmComponent } from '../stopRecord-confirm/stopRecord-confirm.component'
import { TranslateService } from '@ngx-translate/core'
import { ChannelCacheService } from '../../shared/services/channel-cache.service'
import * as Hls from 'hls.js'

let livetv = {}

@Component({
  selector: 'app-livetvplaybar',
  templateUrl: './liveTVPlayBar.component.html',
  styleUrls: ['./liveTVPlayBar.component.scss'],
  providers: [MiniDetailService, PictureService]
})

export class LiveTVPlayBarComponent implements OnInit, OnDestroy, AfterViewInit {
    // declare variables.
  @ViewChild(PlayPopUpDialogComponent) playPopUpDialog: PlayPopUpDialogComponent
  @ViewChild(FullChannelComponent) fullChannel: FullChannelComponent
  @ViewChild(MiniEPGComponent) miniEPG: MiniEPGComponent
  @ViewChild(LiveTVMiniDetailComponent) miniDetails: LiveTVMiniDetailComponent
  public playURL = ''
  public currentPlaybill: any = {}
  public currentChannel = {}
  public player: any = {}
  public miniDetailList = {}
  public haveClicks = false
  public lastVolumeValue = 0.5
  public isShowMiniEPG = false
  public isprofileLogin = Cookies.getJSON('IS_PROFILE_LOGIN')
  public isPreview = false
  public isEpisodesShow = false
  public isPlaying = false
  private video: HTMLVideoElement
  public channeldetails
  public channelID = ''
  public channelData = ''
  public channelName = ''
  public name = ''
  public miniDetail = {}
  public definitionDataList
  public playtimeState: string
  public isPlaypltvCR = false
  public definitionDetailList
  public seekbar: Object
  public isFull = 1
  public playType: string
  public currentType: string
  public playTvType
  public livetvSettingList: {
    curSubTitle: string,
    curAudio: string,
    subtitleList: Array<any>,
    audioList: Array<any>
    livetvFlag: boolean
  }
  public curSubTitle: string
  public isFulls = 1

  public pillsDetail: Object = {}
  public pltvTime: any
  public hideBar
  public hideMiniDetail
  public isBarShow = true
  public isMiniDetailShow = true
  public isFirstPlay = true
  public previewTimer: any
  public bookmark: any
  public isFirstDelete = true
  public isSupportNPVR = false
  public isRecording = false
  public recordType: any
  public isPlayBackStart
  public isOpenPlay = true
  public isHasBookmark = false
  public isClickRecord = false
  public isShowPreviewTip = false
  public previewTime: any
  public isBlackout = false
  public liveVideoData: any
  public episodesDetail: Object = {}
  public playProgramType
  public dbClickTime: any
  public isJumpOut = false
  public _data
  public isLiveTVAUDIO = false
    // Two possible options: DMP and HLS
  private playerType = 'DMP'
    // set the data for mini detail
  @Input() set detaildata (playdata) {
    this.channeldetails = playdata
  }
    // private constructor
  constructor (
        private tVODAppService: TVODAppService,
        private mediaPlayService: MediaPlayService,
        private miniDetailService: MiniDetailService,
        private playbillAppService: PlaybillAppService,
        private nPVRAppService: NPVRAppService,
        private recordDataService: RecordDataService,
        private commonService: CommonService,
        private translate: TranslateService,
        private reminderTask: ReminderTask,
        private channelCacheService: ChannelCacheService
    ) {
    this.liveTVNotification = this.liveTVNotification.bind(this)
    document.onkeyup = this.LivePlayerKeyUp
      // monitor the full screen event according to different brower
    let ua = navigator.userAgent.toLowerCase()
    let isIe = !!ua.match(/msie/i) || !!ua.match(/rv:([\d.]+).*like gecko/)
    let isFirefox = !!ua.match(/firefox\/([\d.]+)/)
    let isEdge = !!ua.match(/edge\/([\d.]+)/)
    if (isIe) {
      document['addEventListener']('MSFullscreenChange', this.webkitfullscreenchange.bind(this))
    } else if (isFirefox) {
      document['addEventListener']('mozfullscreenchange', this.webkitfullscreenchange.bind(this))
    } else if (ua.match(/version\/([\d.]+).*safari/)) {
        document['addEventListener']('fullscreenchange', this.webkitfullscreenchange.bind(this))
      } else if (isEdge) {
        document['addEventListener']('webkitfullscreenchange', this.webkitfullscreenchange.bind(this))
      } else {
        document['addEventListener']('webkitfullscreenchange', this.webkitfullscreenchange.bind(this))
      }

      // control the volume by mouse roll
    if (document.addEventListener) {
      document.addEventListener('DOMMouseScroll', this.scrollFunc.bind(this), false)
    }
    window.onmousewheel = document.onmousewheel = this.scrollFunc.bind(this)
  }

  ngOnInit () {
    this.init()
  }

    /**
     * initialize the player.
     */
  private init (): void {
    this.pillsDetail = {}
      // create player object
    this.video = document.querySelector('#videoContainer video')
      // this.player = new DMPPlayer(this.video);
    this.initDMPPlayer()
      // create the subtitle container
    let subtitleContainer = document.getElementById('video-caption')
    this.player['setSubtitleContainer'](subtitleContainer)
      // set the top class of the subtitle
    subtitleContainer.style.zIndex = '2147483647'
    livetv = this
    this.isPlayBackStart = true
    this.isFulls = 1
      // different events
    this.allEventService()
  }

    /**
     * scroll
     */
  scrollFunc (e) {
    clearTimeout(this.hideMiniDetail)
    e = e || window.event
    if (!this.isShowMiniEPG && !this.haveClicks && !this.isEpisodesShow && !document.getElementById('app-episodes-list')) {
        // judge IE
      if (e.wheelDelta) {
          // roll to up
        if (e.wheelDelta > 0) {
            EventService.emit('upAction')
          }
          // roll to down
        if (e.wheelDelta < 0) {
            EventService.emit('downAction')
          }
      } else if (e.detail) {
          // judge Firefox
          // roll to up
          if (e.detail > 0) {
            EventService.emit('downAction')
          }
          // roll to down
          if (e.detail < 0) {
            EventService.emit('upAction')
          }
        }
    }
      // hide the mini detail when there is no operation
    this.hideMiniDetail = setTimeout(() => {
      this.isMiniDetailShow = false
      this.haveClicks = false
    }, 10000)
  }

  ngAfterViewInit () {
      // set the seek bar
    this.seekbar = this.fullChannel.seekbar
    this.seekbar['addEventListener']('change', (event) => {
      this.fullChannel.onSeekBarChange(event)
    }, true)
    this.seekbar['addEventListener']('input', (event) => {
      this.fullChannel.onSeeking(event)
    }, true)
  }
    /**
     * clear the event and timer
     */
  ngOnDestroy () {
    this.seekbar['removeEventListener']('change', () => {
      this.fullChannel.onSeekBarChange(event)
    }, true)
    this.seekbar['removeEventListener']('input', () => {
      this.fullChannel.onSeeking(event)
    }, true)
    clearTimeout(this.previewTimer)
    clearTimeout(this.hideBar)
    clearTimeout(this.hideMiniDetail)
  }
    /**
     *  handle play error and print to the console
     */
  playbackError (obj) {
    let url = obj.errorInfo && obj.errorInfo.url
    console.log('[LIVETV|TYPE|PC_PLAYER_LOG] type: ' + obj.type + ', errorCode: ' + obj.errorCode)
    console.log('[LIVETV|DESC|PC_PLAYER_LOG] errorDescription: ' + obj.errorDescription + '!')
    console.log('[LIVETV|URL|PC_PLAYER_LOG] errorURL: ' + url)
      // this.initHlsPlayer();
      // when play pvr failed, show the tip
    if (this.playProgramType && this.playProgramType === 'NPVR') {
      this.fullChannel.isBarValid = false
      this.playPopUpDialog.setPopUpPlayLiveTvFail()
    }
  }

  playBackStart () {
    livetv['isFulls'] = 1
      // play the bookmark, when there is playing TVOD or PVR and hassbookmark
      // if (this.isHasBookmark) {
      //     this.mediaPlayService.seek(this.player, Number(this.bookmark));
      //     _.delay(() => {
      //         this.isHasBookmark = false;
      //     }, 2000);
      // }
      // set the flag of setting button, when playback start
    this.fullChannel.autoGray = false
    document.getElementsByClassName('live_load_bg')[0]['style']['display'] = 'none'
    this.playBlackout(this.currentPlaybill)
  }
    /**
     * all event service.
     */
  allEventService () {
      /**
         * the event that shows mini detail page.
         */
    EventService.on('closeLiveDialog', () => {
      this.isJumpOut = false
    })
    EventService.on('openLiveDialog', () => {
      this.isJumpOut = true
    })
      // close mini detail, set the flag of hiding it false
    EventService.on('CLOSED_FULLSCREEN_MINIDETAIL', () => {
      this.haveClicks = false
    })
      // show the mini detail event
    EventService.removeAllListeners(['SHOWMINIDETAIL'])
    EventService.on('SHOWMINIDETAIL', () => {
      this.showMiniDetail()
    })
      // the event that shows the volume bar.
    EventService.on('showLivetvVolumeBar1', value => {
      EventService.emit('showLivetvVolumeBar', value)
    })
      // the event that plays or pauses.
    EventService.on('playchannelState', state => {
      this.changePlayerState(state)
    })
      // set subtitle
    EventService.removeAllListeners(['LIVE_SETSUBTITLES'])
    EventService.on('LIVE_SETSUBTITLES', info => {
      this.mediaPlayService.switchStream(this.player, 'subtitle', { lang: info }, true)
      _.delay(() => {
        this.playPopUpDialog.isLivetvSettingMenu = false
      }, 0)
    })
      // set audio
    EventService.removeAllListeners(['LIVE_SETSAUDIO'])
    EventService.on('LIVE_SETSAUDIO', info => {
      this.mediaPlayService.switchStream(this.player, 'audio', { lang: info }, true)
      _.delay(() => {
        this.playPopUpDialog.isLivetvSettingMenu = false
      }, 0)
    })
      // set definition
    EventService.on('changeLiveDefinition', (definition, index) => {
      EventService.emit('changeLiveTVDefinition', definition)
      this.setStreamQuality(index)
      _.delay(() => {
        this.playPopUpDialog.isShowLiveDefinitionDetail = false
      }, 0)
    })
      // set volume bar value
    EventService.on('liveVolumeBarValue', volume => {
      this.setPlayerVolume(volume / 20)
    })
      // set volume
    EventService.removeAllListeners(['changeLiveVolume'])
    EventService.on('changeLiveVolume', volume => {
      EventService.emit('changeLiveTVVolume', volume)
      EventService.removeAllListeners(['lastVolumeValue'])
      EventService.on('lastVolumeValue', newVolume => {
        this.lastVolumeValue = newVolume
      })
      if (volume === 0) {
        this.setPlayerVolume(volume)
      } else {
        this.setPlayerVolume(this.lastVolumeValue / 20)
      }
    })
      // close mini EPG
    EventService.on('CLOSE_MINIEPG', () => {
      this.isShowMiniEPG = false
    })
      // when the channel media file doesnot exist , close the error dialog and exit the full screen
    EventService.removeAllListeners(['EXIT_FULLSCREEN'])
    EventService.on('EXIT_FULLSCREEN', () => {
      this.exitFullScreen()
    })
      // close record episodes list ,when there is playing PVR
    EventService.on('CLOSE_RECORD_EPISODES', () => {
      this.isEpisodesShow = false
    })
      // add record
    EventService.on('RECORD_ADD_SUCCESS', (type) => {
      this.isRecording = true
      this.recordType = 'stop_recording'
    })
      // cancel record
    EventService.on('RECORD_CANCEL_SUCCESS', () => {
      this.isRecording = false
      this.recordType = 'livetv_record'
    })
      // left button event
    EventService.removeAllListeners(['leftAction'])
    EventService.on('leftAction', () => {
      if (this.liveVideoData['type'] === 'LiveTV' || this.liveVideoData['type'] === 'PLTV') {
        return
      }
      clearTimeout(this.hideBar)
      clearTimeout(this.hideMiniDetail)
      let nowTime = this.seekbar['value']
      this.mediaPlayService.seek(this.player, Number(nowTime) - 10)
      this.fullChannel.isPlay = 1
      this.playerPlay()
      this.mouseMoveBar()
    })
      // right button event
    EventService.removeAllListeners(['rightAction'])
    EventService.on('rightAction', () => {
      if (this.liveVideoData['type'] === 'LiveTV' || this.liveVideoData['type'] === 'PLTV') {
        return
      }
      clearTimeout(this.hideBar)
      clearTimeout(this.hideMiniDetail)
      let nowTime = this.seekbar['value']
      let duration = Math.ceil(this.mediaPlayService.duration(this.player))
      if (nowTime >= 0 && duration > 0 && nowTime < duration) {
        if (Number(nowTime) + 10 > duration) {
            this.mediaPlayService.seek(this.player, Number(duration))
          } else {
            this.mediaPlayService.seek(this.player, Number(nowTime) + 10)
            this.fullChannel.isPlay = 1
            this.playerPlay()
          }
      }
      this.mouseMoveBar()
    })
      // show the login dialog when the guest add favorite or lock from mini detail page
    EventService.removeAllListeners(['REFRESH_LOGIN'])
    EventService.on('REFRESH_LOGIN', () => {
      if (!this.isprofileLogin) {
        this.playPopUpDialog.guestLogin()
      }
    })
      // NPVR blackout
    EventService.removeAllListeners(['NPVR_CHANGE_BLACKOUT'])
    EventService.on('NPVR_CHANGE_BLACKOUT', (NPVRBlackout) => {
      this.currentPlaybill['isBlackout'] = NPVRBlackout
      this.playBlackout(this.currentPlaybill)
    })
      // add record when solve single conflict
    EventService.on('PLAYBILL_TASK_SOLVE_CONFLICT', (playbillID) => {
      if (playbillID === this.currentPlaybill['ID']) {
        this.isRecording = true
        this.recordType = 'stop_recording'
      }
    })
      // add record when solve series conflict
    EventService.on('SLOVE_SERIES_CONFLICT', (data) => {
      if (data['playbillID'] === this.currentPlaybill['ID']) {
        this.isRecording = true
        this.recordType = 'stop_recording'
      }
    })
  }

    /**
     * seek
     */
  onSeeked (obj) {
    this.fullChannel.player = this.player
    document.getElementById('seekbarTVOD').blur()
    this.fullChannel['seeking'] = false
  }
    /**
     * judge the status of different condition
     */
  playStatus (liveVideoData) {
    let url = ''
      // when guest login,show  the login dilag tip
    if (liveVideoData['guest'] === 'Guest') {
      this.playPopUpDialog.setProfileGuest(liveVideoData)
      this.channelID = liveVideoData['channel']['ID']
      this.fullChannel.isPlay = 0
      return
        // not subscribe
    } else if (liveVideoData['judgeSub']) {
      this.playPopUpDialog.setPopUpSubscrEmp(liveVideoData)
      this.channelID = liveVideoData['channel']['ID']
      this.fullChannel.isPlay = 0
      return
        // locked or parent controled
    } else if (liveVideoData['judgePwd']) {
        this.playPopUpDialog.setPopUpAuthorizeEmp(liveVideoData)
        this.channelID = liveVideoData['channel']['ID']
        this.fullChannel.isPlay = 0
        return
        // subscribe successfully
      } else if (liveVideoData['subscribePWD'] === 'subscribeSuccess') {
        url = liveVideoData['url']
      } else if (liveVideoData['url'] === '') {
        this.channelID = liveVideoData['channel']['ID']
        return
      } else {
        url = liveVideoData['url']
      }
    if (EPGConfig['PLAY_URL'] !== '') {
      url = EPGConfig['PLAY_URL']
    }
    this.isPlaying = true
    return url
  }

    /**
     * load the data of back out.
     */
  playBlackout (playbill) {
      // if there is blackout, set the volume mute
    if (playbill && playbill['isBlackout'] === '1') {
      this.isBlackout = true
      EventService.emit('PLAYBAR_BLACKOUT_VOLUME', this.isBlackout)
    } else {
      this.isBlackout = false
      EventService.emit('PLAYBAR_BLACKOUT_VOLUME', this.isBlackout)
    }
  }
    /**
     * set player data before play
     */
  loadBefore (liveVideoData) {
    if (!this.player) {
      return
    }
      // set the value of different flag
    this.liveVideoData = liveVideoData
    this.isPlaying = false
    this.isShowPreviewTip = false
    session.put('LiveTvVolume', true)
    this.isprofileLogin = Cookies.getJSON('IS_PROFILE_LOGIN')
    this.isPreview = liveVideoData['preview']
    this.playType = liveVideoData['type']
    this.fullChannel.isTVODEnd = false
      // if bussiness type is not PLTV or first play shift-time ,close the player
    if (liveVideoData['type'] !== 'PLTV' || this.fullChannel.isFirstPLTV) {
      document.getElementsByClassName('live_load_bg')[0]['style']['display'] = 'block'
      this.fullChannel.autoGray = true
      this.fullChannel['noSetting'] = true
      this.closePlayer()
    }
      // clear the preview timer
    this.previewTime = null
    clearTimeout(this.previewTimer)
      // if bussiness type is not PLTV or first play shift-time ,create the player
    if (liveVideoData['type'] !== 'PLTV' || this.fullChannel.isFirstPLTV) {
      this.init()
    }
      // set the play bar info
    this.fullChannel.setPlayBar(liveVideoData)
      // set the channel and playbill name
    this.setNameData(liveVideoData)
    this.setBarShow(liveVideoData)
    this.getMiniDetail(liveVideoData)
    if (liveVideoData['type'] === 'TVOD' || liveVideoData['type'] === 'NPVR') {
      this.mediaPlayService.setIsNPVR(this.player, true)
        // if there has bookmark,call the set function
      if (liveVideoData['bookmark']) {
        this.mediaPlayService.setBookMark(this.player, liveVideoData['bookmark'])
      }
      this.player['addEventListener'](DMPPlayer.events.PLAYBACK_PLAYING, this.onPlayStart.bind(this))
      this.player['addEventListener'](DMPPlayer.events.PLAYBACK_TIMEUPDATED, this.onPlayTimeUpdate.bind(this))
      this.player['addEventListener'](DMPPlayer.events.PLAYBACK_END, this.onEnded.bind(this))
      if (liveVideoData['playbill']['status'] === 'RECORDING') {
        this.player['addEventListener'](DMPPlayer.events.STREAM_DURATION_UPDATED, this.onDurationUpdate.bind(this))
      }
    } else if (liveVideoData['type'] === 'LiveTV') {
      this.ngOnDestroy()
    } else {
      this.ngOnDestroy()
    }
  }

    /**
     * load the video stream when the initialization of player is completed.
     * play or popup the subscribe dialog or password dialog according to the cideo stream incoming.
     */
  doLoad (liveVideoData) {
    this.isLiveTVAUDIO = liveVideoData['channel']['contentType'] === 'AUDIO_CHANNEL'
    EventService.emit('forFirstFav')
    this.playProgramType = liveVideoData['type']
    this.loadBefore(liveVideoData)
    let url = ''
    let protData = null
    this.isFull = 1
    this.currentType = liveVideoData['type']
    url = this.playStatus(liveVideoData)
    this.getcurCahnnelPlayBill(liveVideoData)
    this.isBarShow = true
    this.fullChannel.isShowBar = true
    if (!url) {
      return
    }
    if (url !== '') {
      this.isShowPreviewTip = true
      this.playURL = url
      this.fullChannel.playBillsInfo(liveVideoData)
      this.judegLiveTVOrVOD(liveVideoData)
        // the buttom of playing or pausing.
      this.playOrPause(liveVideoData, url, protData)
    } else {
      if (this.playProgramType && this.playProgramType === 'NPVR') {
        this.fullChannel.isBarValid = false
        this.playPopUpDialog.setPopUpPlayLiveTvFail()
      }
    }
    if (this.isOpenPlay && !Number(liveVideoData['playbill']['isBlackout'])) {
      this.isOpenPlay = false
      this.setPlayerVolume(0.5)
      EventService.emit('setInitLiveTVVolume', 0.5)
    }
      // the record of playing and the heartbeat of playing.
    this.startReportAndHeartbeat(liveVideoData)

    EventService.removeAllListeners(['showPointdata'])

    this._data = liveVideoData
    let pillTime = (this._data['playbill']['endTime'] - this._data['playbill']['startTime']) / 2 +
            Number(this._data['playbill']['startTime'])
    let req = {
      QueryChannel: {
        'channelIDs': [this._data['channel']['ID']],
        isReturnAllMedia: '1'
      },
      QueryPlaybillContext: {
        'date': pillTime + '',
        'type': '0',
        'preNumber': '0',
        'nextNumber': '1'
      },
      needChannel: '0'
    }
    if (this.currentType === 'TVOD') {
      this.tVODAppService.tvodQueryPlaybillContext(req).then(resp => {
        let channelDtails = this.channelCacheService.getStaticChannelData().channelDetails
        _.each(resp.channelPlaybillContexts, (item) => {
            item['channelInfos'] = _.find(channelDtails, (detail) => {
              return detail['ID'] === (item['currentPlaybillLite'] && item['currentPlaybillLite'].channelID) ||
                            detail['ID'] === (item['nextPlaybillLites'][0] && item['nextPlaybillLites'][0].channelID)
            })
          })
        let info = resp && resp['channelPlaybillContexts'] && resp['channelPlaybillContexts'][0]
        let nextPlaybill = info['nextPlaybillLites']
        let motion = {
            nextPlaybill: info['nextPlaybillLites'],
            channelDetail: info['channelDetail'] || info['channelInfos'],
            beginTime: nextPlaybill[0]['startTime'],
            overTime: nextPlaybill[0]['endTime']
          }
        EventService.emit('forJudgeNextbill', motion)
      })
    }
  }

  playOrPause (liveVideoData, url, protData) {
    this.pltvTime = liveVideoData['PltvTime']
    if (this.fullChannel.isPlay === 0) {
      this.fullChannel.isPlay = 1
    }
    this.bookmark = liveVideoData['bookmark']

    if (this.bookmark) {
      this.isHasBookmark = true
    }

    if (liveVideoData['type'] !== 'PLTV' || this.fullChannel.isFirstPLTV) {
      if (url.indexOf('index.m3u8') > -1) {
          // Probably, it's a job for HLS.
        if (this.playerType !== 'HLS') {
            this.initHlsPlayer()
          }
        this.player.loadSource(url)
      } else {
        if (this.playerType !== 'DMP') {
            this.initDMPPlayer()
          }
        this.mediaPlayService.attachSource(this.player, url, protData)
      }
        // this.mediaPlayService.setAutoSwitchQuality(this.player, true);
      this.setStreamQuality(-1)
      if (liveVideoData['type'] === 'PLTV') {
        this.fullChannel.isFirstPLTV = false
        this.playbillAppService.reportChannel('3', 'PLTV', liveVideoData['channel'])
      }
      if (liveVideoData['playbill']['isLive'] && liveVideoData['type'] === 'LiveTV') {
        this.playbillAppService.reportChannel('3', 'BTV', liveVideoData['channel'])
      }
    } else {
        // play directly by the point if it is PLTV.
      if (liveVideoData['type'] === 'PLTV') {
        this.pltvPlay()
      }
    }
    if (!Cookies.getJSON('IS_PROFILE_LOGIN') && liveVideoData['preview']) {
      this.previewTime = { 'time': this.dealPreiviewTime(liveVideoData) }
      this.playPopUpDialog.setGuestInfo(liveVideoData)
      this.afterPreview(liveVideoData)
    }
    if (Cookies.getJSON('IS_PROFILE_LOGIN') && liveVideoData['preview']) {
      this.previewTime = { 'num': this.dealPreiviewTime(liveVideoData) }
      this.profilePreview(liveVideoData)
    }
  }

  pltvPlay () {
    if (this.pltvTime) {
      let duration = this.fullChannel.pltvCRLength
      let timevalue = this.pltvTime * duration
      this.mediaPlayService.seek(this.player, Number(timevalue))
      this.pltvTime = null
      EventService.emit('PLAY_NEXT_PLTV_OVER')
    }
  }

    /**
     * when the guest ends the preview.
     */
  afterPreview (liveVideoData) {
    clearTimeout(this.previewTimer)
    this.previewTimer = setTimeout(() => {
      this.previewTime = null
      this.closePlayer()
      liveVideoData['guest'] = 'Guest'
      liveVideoData['type'] = 'Subscribe'
      this.fullChannel.setPlayBar(this.liveVideoData)
      this.playPopUpDialog.setProfileGuest(liveVideoData)
      this.closePlayer()
    }, liveVideoData['channel'].physicalChannels[0]['previewLength'] * 1000)
  }
    // when the profile ends the preview.
  profilePreview (liveVideoData) {
    clearTimeout(this.previewTimer)
    this.previewTimer = setTimeout(() => {
      this.previewTime = null
      this.closePlayer()
      this.liveVideoData['judgeSub'] = true
      this.fullChannel.setPlayBar(this.liveVideoData)
      this.playPopUpDialog.setPopUpSubscrEmp(liveVideoData)
      this.closePlayer()
    }, liveVideoData['channel'].physicalChannels[0]['previewLength'] * 1000)
  }
    // deal with the preview time.
  dealPreiviewTime (liveVideoData) {
    let time = liveVideoData['channel'].physicalChannels && liveVideoData['channel'].physicalChannels[0] &&
            liveVideoData['channel'].physicalChannels[0]['previewLength']
    let judageTime = this.getJudageTime(time)
    let lastTime
    switch (judageTime) {
      case 'second':
        if (this.translate.instant('livetv_minutes') === '分钟') {
          lastTime = time + this.translate.instant('livetv_s')
        } else {
          lastTime = time + ' ' + this.translate.instant('livetv_s')
        }
        break
      case 'seconds':
        if (this.translate.instant('livetv_minutes') === '分钟') {
          lastTime = time + this.translate.instant('livetv_ss')
        } else {
          lastTime = time + ' ' + this.translate.instant('livetv_ss')
        }
        break
      case 'secondMinutes':
        let timeMin = time && Math.floor(time / 60)
        let timeSec = time - Math.floor(time / 60) * 60
        lastTime = this.getFinalTime(timeMin, timeSec)
        break
    }
    return lastTime
  }

  getJudageTime (time) {
    let judageTime
    if (time === 1) {
      judageTime = 'second'
    } else if (time < 60) {
      judageTime = 'seconds'
    } else {
      judageTime = 'secondMinutes'
    }
    return judageTime
  }

  getFinalTime (timeMin, timeSec) {
    let time
    if (timeMin === 1 && timeSec === 1) {
      time = this.getTimeOneMinOneSec(timeMin, timeSec)
    } else if (timeMin !== 0 && timeMin !== 1 && timeSec === 1) {
      time = this.getTimeMoreMinOneSec(timeMin, timeSec)
    } else if (timeMin === 1 && timeSec !== 1 && timeSec !== 0) {
        time = this.getTimeOneMinMoreSec(timeMin, timeSec)
      } else if (timeMin === 1 && timeSec === 0) {
        time = this.getTimeOneMin(timeMin, timeSec)
      } else if (timeMin !== 1 && timeSec === 0) {
        time = this.getTimeOneSec(timeMin, timeSec)
      } else {
        time = this.getTimeLast(timeMin, timeSec)
      }
    return time
  }

  getTimeOneMinOneSec (timeMin, timeSec) {
    let time
    if (this.translate.instant('livetv_minutes') === '分钟') {
      time = timeMin + this.translate.instant('livetv_minute') + timeSec + this.translate.instant('livetv_s')
    } else {
      time = timeMin + ' ' + this.translate.instant('livetv_minute') + ' ' + timeSec +
                ' ' + this.translate.instant('livetv_s')
    }
    return time
  }

  getTimeMoreMinOneSec (timeMin, timeSec) {
    let time
    if (this.translate.instant('livetv_minutes') === '分钟') {
      time = timeMin + this.translate.instant('livetv_minutes') + timeSec + this.translate.instant('livetv_s')
    } else {
      time = timeMin + ' ' + this.translate.instant('livetv_minutes') + ' ' + timeSec +
                ' ' + this.translate.instant('livetv_s')
    }
    return time
  }

  getTimeOneMinMoreSec (timeMin, timeSec) {
    let time
    if (this.translate.instant('livetv_minutes') === '分钟') {
      time = timeMin + this.translate.instant('livetv_minute') + timeSec + this.translate.instant('livetv_ss')
    } else {
      time = timeMin + ' ' + this.translate.instant('livetv_minute') + ' ' + timeSec +
                ' ' + this.translate.instant('livetv_ss')
    }
    return time
  }

  getTimeOneMin (timeMin, timeSec) {
    let time
    if (this.translate.instant('livetv_minutes') === '分钟') {
      time = timeMin + this.translate.instant('livetv_minute')
    } else {
      time = timeMin + ' ' + this.translate.instant('livetv_minute')
    }
    return time
  }

  getTimeOneSec (timeMin, timeSec) {
    let time
    if (this.translate.instant('livetv_minutes') === '分钟') {
      time = timeMin + this.translate.instant('livetv_minutes')
    } else {
      time = timeMin + ' ' + this.translate.instant('livetv_minutes')
    }
    return time
  }

  getTimeLast (timeMin, timeSec) {
    let time
    if (this.translate.instant('livetv_minutes') === '分钟') {
      time = timeMin + this.translate.instant('livetv_minutes') + timeSec + this.translate.instant('livetv_ss')
    } else {
      time = timeMin + ' ' + this.translate.instant('livetv_minutes') + ' ' + timeSec +
                ' ' + this.translate.instant('livetv_ss')
    }
    return time
  }

    // the subscribe buttom when the profile previews.
  buy () {
    this.liveVideoData['judgeSub'] = true
    this.fullChannel.setPlayBar(this.liveVideoData)
    this.previewTime = null
    clearTimeout(this.previewTimer)
    this.closePlayer()
    this.playPopUpDialog.empSubData = this.liveVideoData
    this.playPopUpDialog.setPopUpSubscrEmp(this.liveVideoData)
    this.playPopUpDialog.setPopUpSubscriber()
    EventService.emit('openLiveDialog')
  }

    /**
     *  the login buttomn when the guest preview.
     */
  login () {
      // reset the positon of play bar.
    this.liveVideoData['guest'] = 'Guest'
    this.playPopUpDialog.guestLogin()
  }

    // control the show of play bar and the show of error message.
  setBarShow (liveVideoData) {
    if (!liveVideoData['NoURL']) {
      EventService.emit('CLOSED_ERROR_DIALOG')
    }
    if (!(liveVideoData['type'] === 'PLTV' && liveVideoData['playNextpltv']) && liveVideoData['isLock'] ||
            this.channelID !== liveVideoData['channel']['ID'] || liveVideoData['NoURL'] || liveVideoData['judgeSub']) {
      this.fullChannel['isShowNextBtn'] = false
      this.fullChannel['isTVODorPVR'] = false
      this.fullChannel['isShowNow'] = false
      this.fullChannel['ispltvCR'] = false
      clearInterval(this.fullChannel['seekTimer'])
    } else if (liveVideoData['isParentControl'] && liveVideoData['type'] === 'NPVR') {
      this.fullChannel['isTVODorPVR'] = false
      this.fullChannel['isShowNextBtn'] = false
      this.fullChannel['ispltvCR'] = false
    }
  }
    // the data of upper left corner when it is full screen.
  setNameData (liveVideoData) {
    this.showbigLeft(liveVideoData['channel'].channelNO,
        liveVideoData['channel'].name, liveVideoData['playbill'].name, liveVideoData['playbill'])
    if (liveVideoData['channel']) {
      this.currentChannel = liveVideoData['channel']
    }
    this.currentPlaybill = liveVideoData['playbill']
    this.setPvrState()
    this.playBlackout(this.currentPlaybill)
  }
    // set the status of record.
  setPvrState () {
    this.recordType = this.recordDataService.channelIsRecord(this.currentChannel, this.currentPlaybill)
    if (this.recordType) {
      this.isSupportNPVR = true
      if (!this.isprofileLogin && this.playType !== 'TVOD') {
        this.playType = 'LiveTV'
      }
    } else {
      this.isSupportNPVR = false
    }
    this.isRecording = this.currentPlaybill.hasRecordingPVR && this.currentPlaybill.hasRecordingPVR !== '0'
  }
    // the report of play and the heartbeat of play (enter).
  startReportAndHeartbeat (liveVideoData) {
    if (this.playType === 'NPVR') {
      this.nPVRAppService.startPlayNPVRHeartbeat(liveVideoData['playbill'])
      this.nPVRAppService.reportPVR('0', liveVideoData['playbill'])
    } else if (this.playType === 'TVOD') {
      this.tVODAppService.startPlayChannelHeartbeat(liveVideoData['channel'], liveVideoData['playbill'])
      this.tVODAppService.reportChannel('0', liveVideoData['channel'], liveVideoData['productID'])
    }
  }

    // exit the report of play and stop the heartbeat of play.
  exitReportAndHeartbeat (data) {
    if (this.playType === 'NPVR') {
      this.nPVRAppService.reportPVR('1', data['currentPlaybill'])
      this.nPVRAppService.clearDestroy()
    } else if (this.playType === 'TVOD') {
      this.tVODAppService.reportChannel('1', data['currentChannel'])
      this.nPVRAppService.clearDestroy()
    } else if (this.playType === 'LiveTV' && !data['liveVideoData']['preview']) {
        // clear the timer of the playchannelheartbeat
        this.playbillAppService.clearDestroy()
        this.playbillAppService.reportChannel('1', 'BTV', data['currentChannel'])
        session.remove('firstEnterPlay')
      } else if (this.playType === 'PLTV') {
        // clear the timer of the playchannelheartbeat
        this.playbillAppService.clearDestroy()
        this.playbillAppService.reportChannel('1', 'PLTV', data['currentChannel'])
        session.remove('firstEnterPlay')
      }
  }
    /**
     * get the playbill about the channel.
     */
  getcurCahnnelPlayBill (liveVideoData) {
    this.playTvType = liveVideoData['type']
      // get the playbill when it is the first time to enter BTV.
    if (this.isFirstPlay && (liveVideoData['type'] === 'LiveTV' ||
            liveVideoData['type'] === 'PLTV')) {
      this.isFirstPlay = false
      this.channelID = liveVideoData['channel']['ID']
      let startTime
      if (liveVideoData['type'] === 'PLTV') {
        startTime = liveVideoData['playbill']['startTime']
      }
      EventService.emit('CHANGE_PLAYING_CHANNEL',
          { channelId: liveVideoData['channel']['ID'], url: this.playURL, startTime: startTime })
    }

      // get an hour of the program when switch the BTV and authenticate successfully.
    if (this.channelID !== liveVideoData['channel']['ID'] &&
            (liveVideoData['type'] === 'LiveTV' || liveVideoData['type'] === 'PLTV')) {
      this.channelID = liveVideoData['channel']['ID']
      session.remove('unlocked_playbill_id')
      let startTime
      if (liveVideoData['type'] === 'PLTV') {
        startTime = liveVideoData['playbill']['startTime']
      }
      EventService.emit('CHANGE_PLAYING_CHANNEL',
          { channelId: liveVideoData['channel']['ID'], url: this.playURL, startTime: startTime })
    }
    if (liveVideoData['type'] === 'TVOD') {
      this.channelID = liveVideoData['channel']['ID']
    }
    this.player['addEventListener'](DMPPlayer.events.STREAM_INITIALIZED,
        this.getLiveTvData.bind(this))
    this.player['addEventListener'](DMPPlayer.events.SUBTILES_INITIALIZED, this.getLiveCurSubTitleData.bind(this))
  }
    /**
     * get the bit rate,subtitle and track information.
     */
  getLiveTvData () {
    if (this.playerType === 'DMP') {
      this.definitionDataList = this.mediaPlayService.getAllBitRates(this.player)
      this.definitionDetailList = this.mediaPlayService.getAllBitRates(this.player)
      for (let i = 0; i < this.definitionDataList.length; i++) {
        this.definitionDataList[i] = this.definitionDataList[i] / 1000
      }
    } else {
      this.definitionDetailList = this.player['levels'].map(level => level.bitrate)
      this.definitionDataList = this.definitionDetailList.slice()
      for (let i = 0; i < this.definitionDataList.length; i++) {
        this.definitionDataList[i] = this.definitionDataList[i] / 1000
      }
    }
    if ((this.playTvType === 'LiveTV' && !this.liveVideoData['playbill']['isLive']) ||
            (this.playTvType === 'TVOD' && !this.liveVideoData['playbill']['isTvodPlayNext']) ||
            this.playTvType === 'NPVR' && !this.liveVideoData['playbill']['isNpvrNext']
      ) {
      EventService.emit('liveTVDefinitionDataList', this.definitionDataList, 'LiveTV')
    }
    if (this.playerType === 'HLS') {
      return
    }
    let curAudio = this.mediaPlayService.getCurrentAudioLanguage(this.player)
    let audioList = this.mediaPlayService.getAudioLanguages(this.player)
    this.livetvSettingList = {
      curSubTitle: '',
      curAudio: curAudio,
      subtitleList: [],
      audioList: audioList,
      livetvFlag: true
    }
    this.livetvSettingList['subtitleList'] = this.mediaPlayService.getSubtitleLanguages(this.player)
    this.setCurrentSubTitle()
    if (this.livetvSettingList['subtitleList'][0] === 'und' && this.livetvSettingList['audioList'][0] === 'und') {
      this.fullChannel['noSetting'] = true
    } else {
      this.fullChannel['noSetting'] = false
      EventService.emit('GET_SETTINGLIST_LIVE', this.livetvSettingList)
    }
  }
    /**
     * get the subtitle information of current sources.
     */
  getLiveCurSubTitleData () {
    this.livetvSettingList['subtitleList'] = this.mediaPlayService.getSubtitleLanguages(this.player)
    this.setCurrentSubTitle()
    if (this.livetvSettingList['subtitleList'][0] !== 'und' && this.livetvSettingList['audioList'][0] !== 'und') {
      this.fullChannel['noSetting'] = false
      EventService.emit('GET_SETTINGLIST_LIVE', this.livetvSettingList)
    }
    this.mediaPlayService.switchStream(this.player, 'subtitle', { lang: this.curSubTitle })
  }
  setCurrentSubTitle () {
    let subtitleLang = session.get('player.subtitle.lang')
    if (subtitleLang === 'off') {
      this.curSubTitle = 'off'
    } else {
      this.curSubTitle = this.mediaPlayService.getCurrentSubtitleLanguage(this.player)
    }
    this.livetvSettingList['curSubTitle'] = this.curSubTitle
  }

    // judge the current channel is BTV or TVOD or PLTV.
  judegLiveTVOrVOD (liveVideoData) {
    let ispltv = this.judgePltvCR(liveVideoData)
    this.playtimeState = liveVideoData['type']
    if (ispltv) {
      let pltvCR = liveVideoData['channel'].physicalChannelsDynamicProperty &&
                liveVideoData['channel'].physicalChannelsDynamicProperty.pltvCR || liveVideoData['channel'].physicalChannels[0].pltvCR
      let ispltvCRlength = pltvCR.length > 0
      let isEnable = pltvCR.enable === '1'
      let isContentValid = pltvCR.isContentValid === '1'
      if (ispltvCRlength && isEnable && isContentValid && this.playtimeState === 'LiveTV') {
        this.isPlaypltvCR = true
          // query the PLTV if it supports seek.
        EventService.emit('PLTVCR_CHANNEL',
            { length: pltvCR.length, channelId: liveVideoData['channel']['ID'], endTime: liveVideoData['playbill']['endTime'] })
      } else {
        this.isPlaypltvCR = false
      }
    } else {
      this.isPlaypltvCR = false
    }
    this.fullChannel.getplaypause(this.playtimeState)
  }

  judgePltvCR (liveVideoData) {
    let ispltv = liveVideoData['channel'] && (liveVideoData['channel'].physicalChannelsDynamicProperty &&
            liveVideoData['channel'].physicalChannelsDynamicProperty.pltvCR) || (liveVideoData['channel'].physicalChannels &&
                liveVideoData['channel'].physicalChannels[0] && liveVideoData['channel'].physicalChannels[0].pltvCR)
    return ispltv
  }

  onPlayStart (obj) {
    this.fullChannel.player = this.player
    this.fullChannel.isTVODEnd = false
    this.isFirstDelete = true
    this.fullChannel.setDuration(Math.ceil(this.mediaPlayService.time(this.player)))
    this.fullChannel.updateDuration()
  }
  onEnded (obj) {
    this.fullChannel.isPlay = 0
    this.fullChannel.isTVODEnd = true
      // show the replay buttom when single record is played.
    if (!this.fullChannel.isShowNextBtn && this.currentType === 'NPVR') {
      this.fullChannel.noNextTVOD = true
      _.delay(() => {
        this.exitFullScreen()
      }, 800)
    }
    if (this.isFirstDelete) {
      this.isFirstDelete = false
      if (this.bookmark && this.currentType === 'TVOD') {
        this.bookmark = null
        this.tVODAppService.deleteBookMark(this.currentPlaybill)
      }
      if (this.bookmark && this.currentType === 'NPVR') {
        this.bookmark = null
        this.nPVRAppService.deleteBookMark(this.currentPlaybill)
      }
      this.fullChannel.playNextbil()
    }
  }
  onPlayTimeUpdate (obj) {
    this.fullChannel.player = this.player
    if (!this.fullChannel['seeking']) {
      this.fullChannel.setDuration(Math.ceil(this.mediaPlayService.time(this.player)))
      this.seekbar['value'] = Math.ceil(this.mediaPlayService.time(this.player))
    }
    let ua = navigator.userAgent.toLowerCase()
    let isIe = !!ua.match(/msie/i) || !!ua.match(/rv:([\d.]+).*like gecko/)
    let isEdge = !!ua.match(/edge\/([\d.]+)/)
    let postion = this.seekbar['value'] / this.seekbar['max'] * 100
    if (!isIe && !isEdge) {
      this.fullChannel.seekbar['style'].background = 'linear-gradient(to right, #FFCD7E ' +
                postion + '%,#979797 ' + postion + '%)'
    }
  }
  onDurationUpdate () {
    this.fullChannel.updateDuration()
  }
  changeSeek (event) {
    this.isPlayBackStart = false
    this.fullChannel.isTVODEnd = false
    this.mediaPlayService.seek(this.player, Math.ceil(this.seekbar['value']))
  }
  inputSeek (event) {
    this.fullChannel.setDuration(Math.ceil(this.seekbar['value']))
  }
    /**
     * exit the full screen.
     */
  exitFullScreen () {
    this.mediaPlayService.exitFullScreen(this['player'], 'LIVE')
    this.mediaPlayService.close(this.player)
  }

    /**
     * exit the event of full screen.
     */
  webkitfullscreenchange () {
    EventService.emit('CLOSED_WEAKTIP')
    if (document.webkitFullscreenElement || document['FullscreenElement'] || document['mozFullScreenElement'] ||
            document['msFullscreenElement']) {
      return
    }
    this.closePlayer()
      // exit play and stop heartbeat.
    livetv['exitReportAndHeartbeat'](livetv)
    this.fullChannel.autoGray = false
    document.getElementsByClassName('live_load_bg')[0]['style']['display'] = 'none'
    document.querySelector('.channel_login_load_content')['style']['display'] = 'none'
    session.pop('TVGuide')
    if (session.get('REMINDER_FLAG')) {
      session.remove('REMINDER_FLAG')
      EventService.emit('START_PLAY_VOD_FOR_REMINDER')
    }
    session.remove('LiveTvVolume')
    if (document.getElementById('fullBar') && document.getElementById('topControllers')) {
      document.getElementById('fullBar').style.visibility = 'hidden'
      document.getElementById('topControllers').style.display = 'none'
    }
    if (livetv['playType'] === 'TVOD' && livetv['seekbar']['value'] !== livetv['seekbar']['max'] && this.isPlaying) {
      livetv['tVODAppService'].addBookMark(livetv['seekbar']['value'], livetv['currentPlaybill'])
    }
    if (livetv['playType'] === 'NPVR' && livetv['seekbar']['value'] !== livetv['seekbar']['max'] && this.isPlaying) {
      livetv['nPVRAppService'].addBookMark(livetv['seekbar']['value'], livetv['currentPlaybill'])
    }
    clearTimeout(this.previewTimer)
    EventService.emit('CLOSED_ERROR_DIALOG')
    EventService.emit('CLOSED_RECORD_DIALOG')
    EventService.emit('CLOSED_FULLSCREEN_LIVETVVIDEO')
    EventService.emit('CLOSED_FULLSCREEN_MINIDETAIL')
    EventService.emit('CLOSED_FULLSCREEN_MINIEPG')
    EventService.emit('CLOSED_STOPRECORD_DIALOG')
    EventService.emit('CLOSED_LOGIN_DIALOG')
    livetv['isFull'] = 0
    livetv['isFirstPlay'] = true
    livetv['isOpenPlay'] = true
    livetv['isShowPreviewTip'] = false
    livetv['isEpisodesShow'] = false
    livetv['previewTime'] = null
    this.currentType = ''
    this.isBlackout = false
      // TODO: Think how this affects non-player pages
    if (location.pathname.indexOf('video') === -1 && location.pathname.indexOf('movie')) {
      EventService.emit('PLAYBAR_BLACKOUT_VOLUME', this.isBlackout)
    }
  }

  LivePlayerKeyUp (e) {
    if (location.pathname.indexOf('video') !== -1 || location.pathname.indexOf('movie') !== -1) {
      EventService.emit('MINI_SCREEN_CLOSED_FULLSCREEN_VODVIDEO', e.keyCode)
      EventService.emit('CLOSED_FULLSCREEN_VODVIDEO', e.keyCode)
    } else {
      if (e.keyCode === 32) {
        livetv['fullChannel'].playchannelControl()
      }
    }
  }

  changeScreen () {
    clearTimeout(this.dbClickTime)
    this.exitFullScreen()
  }

  clickScreen (e) {
    clearTimeout(this.dbClickTime)
    this.dbClickTime = setTimeout(() => {
      let isLivetvSettingMenu = this.playPopUpDialog.isLivetvSettingMenu
      let isLiveVolumeBarDivShow = this.playPopUpDialog.isLiveVolumeBarDivShow
      let isShowLiveDefinitionDetail = this.playPopUpDialog.isShowLiveDefinitionDetail
      let judgeAuthorizeEmp = this.playPopUpDialog.judgeAuthorizeEmp
      let judgeFullScreenPwd = this.playPopUpDialog.judgeFullScreenPwd
      if (this.isPlaying && !isLivetvSettingMenu && !isLiveVolumeBarDivShow && !judgeFullScreenPwd &&
                !isShowLiveDefinitionDetail && !this.isShowMiniEPG && !this.isClickRecord &&
                !judgeAuthorizeEmp && !this.isEpisodesShow) {
        livetv['fullChannel'].playchannelControl(e)
      }
    }, 300)
  }

    /**
     * get the mini detail data.
     */
  getMiniDetail (miniDetail) {
    this.miniDetail = miniDetail
  }

    /**
     * show the mini detail.
     */
  showMiniDetail () {
    this.haveClicks = false
    if (this.currentPlaybill.isFillProgram && this.currentPlaybill.isFillProgram === '1') {
      EventService.emit('SET_FILLPROGRAM', { name: null })
    } else {
      let ID = this.currentPlaybill.ID
      let endTime = this.currentPlaybill.endTime
      let startTime = this.currentPlaybill.startTime
      this.miniDetailService.getDetail(ID, endTime, startTime).then((resp) => {
        let favData = {}
        favData['channelDetail'] = this.miniDetail['channel']
        favData['playbillLites'] = []
        favData['playbillLites'][0] = this.currentPlaybill
        this.miniDetailList = [resp, favData]
        this.haveClicks = true
        this.isMiniDetailShow = true
      })
    }
  }

  showMiniEPG () {
    this.isShowMiniEPG = true
    this.miniEPG.ngOnInit(this.channelID)
  }
    // show the episodes if it is period record.
  showepisodesList () {
    this.isEpisodesShow = true
    this.episodesDetail = this.currentPlaybill
  }
    /**
     * record.
     */
  record () {
    this.isClickRecord = true
      // if the channel support NPVR,  the button can work
    if (this.playType === 'LiveTV' && this.isSupportNPVR) {
      if (!this.isprofileLogin) {
        this.playPopUpDialog.profileData = this.liveVideoData
        this.playPopUpDialog.guestLogin()
        return
      }
      if (this.recordType === 'livetv_record') {
        let playbill = this.currentPlaybill
        playbill['channelDetail'] = this.currentChannel
        playbill['channelDetail']['name'] = this.currentChannel['name']
        playbill['isNow'] = true
        EventService.emit('LIVETV_RECORD', playbill)
      } else {
        if (document.querySelector('md-dialog-container app-stop-record-confirm') === null) {
            this.commonService.openDialog(StopRecordConfirmComponent, null, this.currentPlaybill['ID'])
          }
      }
    }
  }
    /**
     * the data of upper left corner when it is full screen.
     */
  showbigLeft (channelData, channelName, name, data) {
    this.channelData = channelData || ''
    this.channelName = channelName
    this.name = name || 'not_program'
  }

  liveTVNotification (eventName, data) {
    switch (eventName) {
      case 'CHANNEL_STATIC_VERSION_CHANGED':
      case 'CHANNEL_DYNAMIC_VERSION_CHANGED':
          /**
                 * the channel dynamic data is channged,is the same to that the channel subscribe data is changed.
                 * the lack of the event of cut-out,you should cut the stream before authenticate play.
                 */
        if (session.get('LOGIN_LIVETV_PLAYING')) {
          return
        }
        if (this.currentType === 'LiveTV') {
          this.playbillAppService.playProgram(this.currentPlaybill, this.currentChannel, livetv['player'])
        } else if (this.currentType === 'PLTV') {
            this.playbillAppService.playPLTVCRpromgram(
              this.currentPlaybill, this.currentChannel, 0, 0, this.pltvTime, livetv['player'])
          }
        break
      case 'REMINDER_VERSION_CHANGED':
        this.reminderTask.getReminder()
        break
      default:
        break
    }
  }

  mouseMoveBar ($event?) {
    if ($event && $event.movementX) {
      if (Math.abs($event.movementX * $event.movementX) + Math.abs($event.movementY * $event.movementY) > 9) {
        this.isBarShow = true
        this.fullChannel.isShowBar = true
      }
    } else {
      this.isBarShow = true
      this.fullChannel.isShowBar = true
    }
    if (document.getElementById('videoContainer')) {
      document.getElementById('videoContainer')['style']['cursor'] = 'default'
    }
    clearTimeout(this.hideBar)
    clearTimeout(this.hideMiniDetail)
    this.hideBar = setTimeout(() => {
      if (document.getElementById('videoContainer')) {
        document.getElementById('videoContainer')['style']['cursor'] = 'none'
      }
      this.isBarShow = false
      this.fullChannel.isShowBar = false
      this.playPopUpDialog.isLiveVolumeBarDivShow = false
      this.playPopUpDialog.isShowLiveDefinitionDetail = false
      this.playPopUpDialog.isLivetvSettingMenu = false
    }, 5000)
    this.hideMiniDetail = setTimeout(() => {
      this.isMiniDetailShow = false
      this.haveClicks = false
    }, 10000)
  }

  public closePlayer (): void {
    if (this.playerType === 'DMP') {
      livetv['player'].close()
    } else {
      livetv['player'].destroy()
    }
  }

  private initDMPPlayer (): void {
    this.playerType = 'DMP'
    this.player = new DMPPlayer(this.video)
      // monitor the event of player
    this.player['addEventListener'](DMPPlayer.events.PLAYBACK_SEEKING, this.onSeeked.bind(this))
    this.player['addEventListener'](DMPPlayer.events.PLAYBACK_START, this.playBackStart.bind(this))
    this.player['addEventListener'](DMPPlayer.events.PLAYBACK_ERROR, this.playbackError.bind(this))
  }

  private initHlsPlayer (): void {
    this.playerType = 'HLS'
    this.player = new Hls()
      // this.player.loadSource('https://video-dev.github.io/streams/x36xhzz/x36xhzz.m3u8');
      // this.player.loadSource(url);
    this.player.attachMedia(this.video)
    this.player.on(Hls.Events.MANIFEST_PARSED, () => {
      console.log('[LIVETV|HLS] Manifest parsed')
      this.video.play()
      this.playBackStart()
      this.getLiveTvData()
    })
    this.player.on(Hls.Events.LEVEL_LOADED, () => {
      console.log('[LIVETV|HLS] Levels loaded')
      this.getLiveTvData()
    })
    this.player.on(Hls.Events.ERROR, error => {
      this.playbackError(error)
    })
  }

  private changePlayerState (state: number): void {
      // pause
    if (state === 0) {
      this.playerPause()
        // when the livetv channel pause, set the recording button unavialable
      if (this.currentType === 'LiveTV' || this.currentType === 'PLTV') {
        this.isSupportNPVR = false
      }
        // play
    } else if (state === 1) {
      this.playerPlay()
    }
  }

  private playerPlay (): void {
    if (this.playerType === 'DMP') {
      this.mediaPlayService.play(this.player)
    } else {
      this.video.play()
    }
  }

  private playerPause (): void {
    if (this.playerType === 'DMP') {
      this.mediaPlayService.pause(this.player)
    } else {
      this.video.pause()
    }
  }

  private setPlayerVolume (volume: number): void {
    if (this.playerType === 'DMP') {
      this.mediaPlayService.setVolume(this.player, volume)
    } else {
      this.video.volume = volume
    }
  }

  private setStreamQuality (index: number): void {
    if (index === -1) {
      if (this.playerType === 'DMP') {
        this.mediaPlayService.setAutoSwitchQuality(this.player, true)
      } else {
        this.player.nextLevel = -1
      }
    } else {
      if (this.playerType === 'DMP') {
        this.mediaPlayService.setAutoSwitchQuality(this.player, false)
        this.mediaPlayService.switchStream(this.player, 'video', {
            bitrate: this.definitionDetailList[index]
          })
      } else {
        this.player.nextLevel = this.player.levels[index].level
      }
    }
  }
}
