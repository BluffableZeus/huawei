import * as _ from 'underscore'
import { Component, ViewChild, ElementRef, OnInit, OnDestroy } from '@angular/core'
import { EventService } from 'ng-epg-sdk/services'
import { MediaPlayService } from '../../mediaPlay/mediaPlay.service'
import { TranslateService } from '@ngx-translate/core'
import { TVODAppService } from '../../playPopUpDialog/tVodApp.service'
import { NPVRAppService } from '../../playPopUpDialog/nPvrApp.service'
import { DateUtils } from 'ng-epg-sdk/utils'
import { queryPlaybillList } from 'ng-epg-sdk/vsp'
import { session } from 'src/app/shared/services/session'

let FullChannelObj = {}
@Component({
  selector: 'app-fullscreen-channelcontrol',
  templateUrl: './fullScreen-channelControl.component.html',
  styleUrls: ['./fullScreen-channelControl.component.scss']
})

export class FullChannelComponent implements OnInit, OnDestroy {
    // declare variables.
  public pausedchannel = 'url(assets/img/32/play_fullscreen_32.png)'
  public replay = 'url(assets/img/32/replay_32.png)'
  public isPlay = 1
  public seekbar: Element
  public seekbarLiveTV: Element
  public video: Object
  public player: Object
  public playBills: Object
  public billDuration: string
  public billTotalTime: string
  public billDurationValue: number
  public billTotalTimeValue: number
  public seekTimer: any
  public noNextTVOD = false
  public isTVODEnd = false
  public isFirstPLTV = true
  public isprofileLogin = Cookies.getJSON('IS_PROFILE_LOGIN')
  public isShowBar = true
  public isShowNextBtn = false
  public playNextPltvFlag = true
  public autoGray = false
  public isBarValid = true
  @ViewChild('seekbarLIVETV') seekbarLIVETV: ElementRef
  @ViewChild('playBarActiveLIVETV') playBarActiveLIVETV: ElementRef
  public closeVolume = 'url(assets/img/24/mute_24.png)'
  public isVolume = 1
  public isShowLiveVolumeBar = 0
  public curDefinition = 'auto'
  public playpausestate: string
  public isLivetvSettingMenu = 0
  public isShowLiveDefinition = 0
  public isTVODorPVR = false
  public ispltvCR = false
  public time: string
  public playTime: string
  public totalTime: string
  public timeChange: any
  public pltvCRLength: any
  public pltvTime: number
  public curChannel: any
  public seekTarget: any
  public seekLength: number
  public waitTime = 0
  public isClickPuase = false
  public definitionPathShow = false
  public volumePathShow = false
  public setPathShow = false
  public playbill: any
  public isShowNow = false
  public isPreview = false
  public seeking = false
  public noSetting = true
  public playPause: string
  public isPltvCRLength: any
  public nowti: number
  public isBlackout = true
  public allPVRPlaybillList: any = []
  public currentPlayBill
  public isJumpOut = false
    // private constructor
  constructor (
        private mediaPlayService: MediaPlayService,
        private translate: TranslateService,
        private tVODAppService: TVODAppService,
        private nPVRAppService: NPVRAppService
    ) { }
    /**
     * query PVR PlaybillList
     */
  queryPVRPlaybillList () {
    let options = {
      params: {
        SID: 'queryplaybilllist6',
        DEVICE: 'PC',
        DID: session.get('uuid_cookie')
      }
    }
    queryPlaybillList({
      queryChannel: {
        channelIDs: [this.playBills['channel']['channelID']],
        isReturnAllMedia: '1'
      },
      needChannel: '0',
      queryPlaybill: {
        startTime: this.playbill['beginOffsetTime'] + '',
        endTime: this.playbill['endOffsetTime'] + '',
        count: '100',
        offset: '0',
        type: '0',
        isFillProgram: '1'
      }
    }, options).then(resp => {
      this.allPVRPlaybillList = resp['channelPlaybills'] && resp['channelPlaybills'][0] &&
                resp['channelPlaybills'][0]['playbillLites']
    })
  }
    /**
     * show next button
     */
  tvodOrNpvrInfo () {
    clearInterval(this.seekTimer)
      // if the playbill belongs to series or periodical record,show the 'next' buttom.if not,don't show this buttom.
    this.isShowNextBtn = !(this.playBills['type'] === 'NPVR' && !this.playBills['playbill'].isSeriesPage)
    this.isTVODorPVR = true
    this.ispltvCR = false
    if (this.playBills['type'] === 'NPVR') {
      this.queryPVRPlaybillList()
    }
  }

  playBillsInfo (data) {
    if (!_.isUndefined(data.channel)) {
      let isPltvCR = this.playBills['channel'] && this.playBills['channel'].physicalChannelsDynamicProperty &&
                this.playBills['channel'].physicalChannelsDynamicProperty.pltvCR || this.playBills['channel'].physicalChannels &&
                this.playBills['channel'].physicalChannels[0] && this.playBills['channel'].physicalChannels[0].pltvCR
      let enable = isPltvCR && isPltvCR.enable
      let isContentValid = isPltvCR && isPltvCR.isContentValid
      this.isPltvCRLength = isPltvCR && isPltvCR.length
      clearInterval(this.timeChange)
      this.isShowNow = false
      this.showPlayBar(enable, isContentValid)
    }
  }

  showPlayBar (enable, isContentValid) {
      // show play bar according to it is TVOD or recording.
    if (this.playBills['type'] === 'TVOD' || this.playBills['type'] === 'NPVR') {
      this.tvodOrNpvrInfo()
    } else if (this.playBills['type'] === 'LiveTV') {
      clearInterval(this.seekTimer)
      this.ngOnDestroy()
      this.playTime = ''
      this.totalTime = ''
      let wid = window.screen.width
      this.seekbarLIVETV.nativeElement.style.marginLeft = wid + 'px'
        // show the progress bar when the channel playing supports to shift time.
      if (Number(enable) === 1 && Number(isContentValid) === 1 && this.isPltvCRLength > 0) {
          this.ispltvCR = true
          this.timeChange = setInterval(() => {
            this.playTime = DateUtils.format(Date.now(), 'HH:mm:ss')
          }, 1000)
        } else {
          // if the channel don't support to shift time,hidden the progress bar.
          this.ispltvCR = false
          this.timeChange = setInterval(() => {
            this.time = DateUtils.format(Date.now(), 'HH:mm:ss')
          }, 1000)
        }
      this.isTVODorPVR = false
    } else {
        // pltv
      this.isShowNow = true
      _.delay(() => {
          clearInterval(this.seekTimer)
          this.seekTimer = setInterval(() => {
            this.pltvTime = this.pltvTime + 1000
            if (Date.now()['getTime']() - this.pltvTime >= this.pltvCRLength * 1000) {
              this.pltvTime = Date.now()['getTime']() - this.pltvCRLength * 1000
            }
            this.playTime = DateUtils.format(this.pltvTime, 'HH:mm:ss')
            this.totalTime = '/ ' + DateUtils.format(Date.now(), 'HH:mm:ss')
            if (this.pltvTime >= this.playbill['endTime'] && this.playNextPltvFlag) {
              this.playNextPltvFlag = false
              let seekTime = 1 - (this.seekLength / 1000 + this.waitTime) / this.pltvCRLength
              EventService.emit('SEEK_PLAYBILL_TIME',
                { playTime: this.playbill['endTime'], seekTime: seekTime, isLiveTV: false, isNextPlaybill: true })
            }
          }, 1000)
        }, 100)
    }
  }
    /**
     * initialization
     */
  ngOnInit () {
    this.closeVolume = 'url(assets/img/24/mute_24.png)'
    this.curDefinition = 'auto'
    this.seekbar = document.querySelector('#videoControllerChannel #seekbarTVOD')
    this.seekbarLiveTV = document.querySelector('#videoControllerChannel #seekbarLIVETV')
    FullChannelObj = this
      // close the full screen.
    EventService.on('CLOSED_FULLSCREEN_LIVETVVIDEO', () => {
      clearInterval(this.timeChange)
      clearInterval(this.seekTimer)
      this.ispltvCR = false
      this.isTVODorPVR = false
      this.isShowNextBtn = false
      this.isShowNow = false
      this.isVolume = 1
      this.isTVODEnd = false
      this.noNextTVOD = false
      this.isPlay = 1
      this.seekbar['style'].background = 'linear-gradient(to right, #FFCD7E 0%,#979797 0%)'
    })
    EventService.removeAllListeners(['liveVolumeBarValue'])
    EventService.on('liveVolumeBarValue', a => {
      if (Number(a) === 0) {
        this.isVolume = 0
      } else {
        this.isVolume = 1
      }
    })
    EventService.removeAllListeners(['changeLiveTVDefinition'])
    EventService.on('changeLiveTVDefinition', definition => {
      if (definition === 'auto') {
        this.curDefinition = definition
      } else {
        this.curDefinition = definition / 1000 + ' ' + this.translate.instant('kbps')
      }
    })

    EventService.removeAllListeners(['PLAYBAR_BLACKOUT_VOLUME'])
    EventService.on('PLAYBAR_BLACKOUT_VOLUME', isBlackout => {
      if (isBlackout) {
        this.isBlackout = false
        this.isVolume = 0
        EventService.emit('changeLiveVolume', this.isVolume)
      }
    })
    EventService.on('PLAY_NEXT_PLTV_OVER', () => {
      this.playNextPltvFlag = true
    })
    EventService.on('closeLiveDialog', () => {
      this.isJumpOut = false
    })
    EventService.on('openLiveDialog', () => {
      this.isJumpOut = true
    })

    document.onkeypress = this.LivePlayerKeyUp
    document.onkeydown = this.LivePlayerKeyUp
  }
    /**
     * when exit the component, clear all the timer and events
     */
  ngOnDestroy () {
    clearInterval(this.timeChange)
    clearInterval(this.seekTimer)
  }

  prefixZero (str, len) {
    str = '' + (str || '')
    if (str.length >= len) {
      return str
    }

    let gapLen = len - str.length,
      i, arr = []
    for (i = 0; i < gapLen; i++) {
      arr[i] = '0'
    }

    return arr.join('') + str
  }
    /**
     * click to play the next playbill.
     */
  playNextbil (clickFlag?: boolean) {
      // if catch-up tv is not end,add bookmark
    if (this.playBills['type'] === 'TVOD') {
      if (this.isShowNextBtn && this.seekbar['value'] !== this.seekbar['max']) {
        this.tVODAppService.addBookMark(this.seekbar['value'], this.playbill)
      }
      document.getElementsByClassName('play_loading')[0]['style']['display'] = 'block'
      EventService.emit('nextPlaybill', this.playBills)
    }
      // if the record is not end,add bookmark
    if (this.playBills['type'] === 'NPVR') {
      if (this.isShowNextBtn && this.seekbar['value'] !== this.seekbar['max']) {
        this.nPVRAppService.addBookMark(this.seekbar['value'], this.playbill)
      }
      if (this.playBills['playbill'].isSeriesPage) {
        let clonePlaybill = _.clone(this.playBills)
        if (clickFlag) {
            clonePlaybill['clickFlag'] = clickFlag
          }
        EventService.emit('nextNPVR', clonePlaybill)
      }
    }
  }
    /**
     * change the color of progress bar when play TVOD or PVR
     */
  colorChange () {
    this.seeking = true
    let ua = navigator.userAgent.toLowerCase()
    let isIe = !!ua.match(/msie/i) || !!ua.match(/rv:([\d.]+).*like gecko/)
    let isEdge = !!ua.match(/edge\/([\d.]+)/)
    let postion = this.seekbar['value'] / this.seekbar['max'] * 100
    if (!isIe && !isEdge) {
      this.seekbar['style'].background = 'linear-gradient(to right, #FFCD7E ' + postion + '%,#979797 ' + postion + '%)'
    }
    if (this.isPlay === 0) {
      this.isPlay = 1
      EventService.emit('playchannelState', this.isPlay)
    }
  }

  onSeekBarChange (event) {
    this.seeking = true
    let ua = navigator.userAgent.toLowerCase()
    let isIe = !!ua.match(/msie/i) || !!ua.match(/rv:([\d.]+).*like gecko/)
    let isEdge = !!ua.match(/edge\/([\d.]+)/)
    let postion = this.seekbar['value'] / this.seekbar['max'] * 100
    if (!isIe && !isEdge) {
      this.seekbar['style'].background = 'linear-gradient(to right, #FFCD7E ' + postion + '%,#979797 ' + postion + '%)'
    }
    if (this.isPlay === 0) {
      this.isPlay = 1
      EventService.emit('playchannelState', this.isPlay)
    }
  }

  onSeeking (event) {
    this.seeking = true
    let ua = navigator.userAgent.toLowerCase()
    let isIe = !!ua.match(/msie/i) || !!ua.match(/rv:([\d.]+).*like gecko/)
    let isEdge = !!ua.match(/edge\/([\d.]+)/)
    let postion = this.seekbar['value'] / this.seekbar['max'] * 100
    if (!isIe && !isEdge) {
      this.seekbar['style'].background = 'linear-gradient(to right, #FFCD7E ' +
                postion + '%,#979797 ' + postion + '%)'
    }
  }

  setTotalTime (value) {
    if (!isNaN(value)) {
      this.billTotalTimeValue = value
      this.billTotalTime = '/ ' + this.convertToTimeCode(this.billTotalTimeValue)
    }
  }

  setBlackOut (value) {
    if (this.playBills['type'] === 'NPVR') {
      let currentTime = value * 1000 + parseInt(this.playbill['beginOffsetTime'], 10)
      if (this.allPVRPlaybillList && this.allPVRPlaybillList.length === 0) {
        return
      }
      let currentPlayBill = _.find(this.allPVRPlaybillList, playbill => {
        if (playbill['startTime'] < currentTime && playbill['endTime'] > currentTime) {
            return playbill
          }
      })
      if (currentPlayBill && this.currentPlayBill !== currentPlayBill) {
        this.currentPlayBill = currentPlayBill
        this.playbill['isBlackout'] = currentPlayBill['isBlackout']
        EventService.emit('NPVR_CHANGE_BLACKOUT', this.playbill['isBlackout'])
      }
    }
  }

  setDuration (value) {
    if (!isNaN(value)) {
      this.billDurationValue = Math.min(this.billTotalTimeValue, value)
      this.billDuration = this.convertToTimeCode(this.billDurationValue)
    }
    this.setBlackOut(this.billDurationValue)
  }
    /**
     * check if duration changes for live streams..
     */
  updateDuration () {
    let duration = Math.ceil(this.mediaPlayService.duration(this.player))
      // when playing recording pvr ,the duration reaches max still update
    if (Number(duration) !== Number(this.seekbar['max']) || this.playBills['status'] === 'RECORDING') {
      this.setTotalTime(duration)
      this.seekbar['max'] = duration
    }
  }
    /**
     * the progress bar of seek when plays live TV.
     */
  getOffsetX (event) {
    if (event) {
      let evt = event || window.event
      let srcObj = evt && evt.target || evt.srcElement
      this.seekTarget = srcObj && srcObj.className
      if (!this.seekTarget) {
        return
      }
      if (evt.offsetX) {
        return evt.offsetX
      } else {
        let rect = srcObj.getBoundingClientRect()
        let clientx = evt.clientX
        return clientx - rect.left
      }
    }
  }

  convertToTimeCode (value) {
    value = Math.max(value, 0)
    let h = Math.floor(value / 3600)
    let m = Math.floor(value % 3600 / 60)
    let s = Math.floor(value % 3600 % 60)
    return (h === 0 ? '00:' : h < 10 ? '0' + h.toString() + ':' : h.toString() + ':') +
            (m < 10 ? '0' + m.toString() : m.toString()) + ':' + (s < 10 ? '0' + s.toString() : s.toString())
  }

  LivePlayerKeyUp (e) {
    if ((e.keyCode >= 37 && e.keyCode <= 40 || e.keyCode === 32)) {
      if (e.target['nodeName'] === 'INPUT') {
        return
      }
    }
    if (FullChannelObj['curChannel'] && FullChannelObj['curChannel']['contentType'] === 'AUDIO_CHANNEL') {
      FullChannelObj['keyupAndDown'](e)
    }
    if (FullChannelObj['playpausestate'] === 'LiveTV' && (!Cookies.getJSON('IS_PROFILE_LOGIN') ||
            FullChannelObj['isPreview'] || !FullChannelObj['ispltvCR'])) {
      FullChannelObj['keyupAndDown'](e)
    } else {
      FullChannelObj['keyleftAndRight'](e)
      FullChannelObj['keyupAndDown'](e)
    }
  }

  keyleftAndRight (e) {
      // left key.
    if (e.keyCode === 37) {
      EventService.emit('leftAction')
    }
      // right key.
    if (e.keyCode === 39) {
      EventService.emit('rightAction')
    }
  }

  keyupAndDown (e) {
      // up key.
    if (e.keyCode === 38) {
      EventService.emit('upAction')
    }
      // down key.
    if (e.keyCode === 40) {
      EventService.emit('downAction')
    }
  }

  pltvSeekBarTwice (iconWid, wid) {
    this.waitTime = 0
    this.seekbarLIVETV.nativeElement.style.marginLeft = iconWid + 'px'
    this.pltvCRLength = this.isPltvCRLength
    this.seekLength = Math.round((1 - iconWid / wid) * this.pltvCRLength * 1000)
    this.nowti = Date.now()['getTime']() - (1 - (iconWid - 20) / wid) * this.pltvCRLength * 1000
    this.pltvTime = this.nowti
    this.playTime = DateUtils.format(this.nowti, 'HH:mm:ss')
    this.isShowNow = iconWid !== wid
    if (!this.isClickPuase) {
      let seekTime = (iconWid - 20) / wid
      EventService.emit('SEEK_PLAYBILL_TIME',
          { playTime: this.nowti, seekTime: seekTime, isLiveTV: iconWid === wid, isNextPlaybill: false, isFirst: this.isFirstPLTV })
    }
    this.isClickPuase = false
  }
    /**
     * clcik the progress bar when play PLTV
     */
  pltvSeekBar (event?) {
      // if the guest login or preview, the bar can't click
      // if the channel is audio channel,the bar can't click
    if (!Cookies.getJSON('IS_PROFILE_LOGIN') || this.isPreview ||
             (this.curChannel && this.curChannel['contentType'] === 'AUDIO_CHANNEL')) {
      return
    }
    EventService.emit('HIDE_POPUP_DIALOG')
    clearInterval(this.timeChange)
    clearInterval(this.seekTimer)
    let wid = this.playBarActiveLIVETV.nativeElement.clientWidth
    let margin = Number(this.seekbarLIVETV.nativeElement.style.marginLeft.replace('px', ''))
      // set the seek position of time
    let iconWid = this.getOffsetX(event)
    if (this.seekTarget === 'seekbarLIVETV') {
      if (margin === wid || margin === 0) {
        return
      } else {
        if (iconWid < 10) {
            iconWid = margin - iconWid
          } else if (iconWid < 20 && iconWid >= 10) {
            iconWid = margin + (iconWid - 10)
          }
      }
    } else if (this.seekTarget === 'playBar-activeLIVETV') {
      iconWid = iconWid + 20
      if (iconWid > wid - 10) {
          iconWid = wid
        }
    }
      // the time is same when paused.
    if (this.isClickPuase) {
      iconWid = margin
    }
    this.pltvSeekBarTwice(iconWid, wid)
    clearInterval(this.seekTimer)
    this.seekTimer = setInterval(() => {
      iconWid = Number(this.seekbarLIVETV.nativeElement.style.marginLeft.replace('px', ''))
      iconWid = iconWid - (wid) / this.pltvCRLength
      if (iconWid <= 20) {
        iconWid = 20
      }
      if (iconWid === wid) {
        this.isShowNow = false
      } else {
        this.isShowNow = true
      }
      this.seekbarLIVETV.nativeElement.style.marginLeft = iconWid + 'px'
      this.totalTime = '/ ' + DateUtils.format(Date.now(), 'HH:mm:ss')
    }, 1000)
  }

  turnToLive () {
    this.isShowNow = false
    this.isFirstPLTV = true
    EventService.emit('HIDE_POPUP_DIALOG')
    EventService.emit('SEEK_PLAYBILL_TIME', { playTime: Date.now()['getTime'](), seekTime: 0, isLiveTV: true, isNextPlaybill: false })
  }
    /**
     * show the setting menu dialog.
     */
  showLivetvSettingBtn () {
    if (!this.isBarValid || this.noSetting) {
      return
    }
    this.isLivetvSettingMenu = 1
    this.setPathShow = true
    EventService.emit('SHOW_LIVETVSETTINGMENU', this.isLivetvSettingMenu)
  }
    /**
     * close the setting menu dialog.
     */
  closeLivetvSettingBtn () {
    this.isLivetvSettingMenu = 0
    this.setPathShow = false
    EventService.emit('SHOW_LIVETVSETTINGMENU', this.isLivetvSettingMenu)
  }
    /**
     *  control the volume icon to be visible or hidden.
     */
  volumeIconControl () {
    if (!this.isBarValid) {
      return
    }
    if (this.isVolume === 0) {
      this.isVolume = 1
      EventService.emit('changeLiveVolume', this.isVolume)
    } else {
      this.isVolume = 0
      EventService.emit('changeLiveVolume', this.isVolume)
    }
  }
    /**
     * show the volume bar.
     */
  showVolumeBar () {
    if (!this.isBarValid) {
      return
    }
    this.isShowLiveVolumeBar = 1
    EventService.emit('showLivetvVolumeBar1', this.isShowLiveVolumeBar)
    this.volumePathShow = true
  }
    /**
     * hide the volume bar.
     */
  hiddenVolumeBar () {
    this.isShowLiveVolumeBar = 0
    EventService.emit('showLivetvVolumeBar1', this.isShowLiveVolumeBar)
    this.volumePathShow = false
  }
    /**
     * show the definition.
     */
  showDefinitionDetail () {
    if (!this.isBarValid) {
      return
    }
    this.isShowLiveDefinition = 1
    EventService.emit('showLivetvDefinitionDetail', this.isShowLiveDefinition)
    this.definitionPathShow = true
  }
    /**
     * hide the definition.
     */
  hideDefinitionDetail () {
    this.isShowLiveDefinition = 0
    this.definitionPathShow = false
    EventService.emit('showLivetvDefinitionDetail', this.isShowLiveDefinition)
  }
    /**
     * the buttom of playing or pausing.
     * if the bar is not allow to click or it is that the guest or user previews,
     * the buttom of playing or pausing is not allow to click.
     */
  playchannelControl (event?) {
    if (!this.isBarValid || !Cookies.getJSON('IS_PROFILE_LOGIN') || this.isPreview ||
            (this.curChannel && this.curChannel['contentType'] ===
                'AUDIO_CHANNEL') || (!this.ispltvCR && this.playpausestate === 'LiveTV')) {
      return
    }
    this.playOrPause()
  }
    /**
     * pause when it is playing the TVOD or record.
     */
  playOrPause () {
    if (this.isPlay === 1 && (this.playpausestate === 'TVOD' || this.playpausestate === 'NPVR')) {
      this.isPlay = 0
      EventService.emit('playchannelState', this.isPlay)
        // if the channel playing now supports the seek,when it paused,enter to seek position.
    } else if (this.isPlay === 1 && this.ispltvCR) {
      this.isClickPuase = true
      this.isPlay = 0
      this.pltvSeekBar(event)
      EventService.emit('playchannelState', this.isPlay)
    } else {
      this.fromPauseToPlay()
    }
  }
    /**
     *  when it is from pause to play,refresh the time if it is PLTV or BTV.
     */
  fromPauseToPlay () {
    this.isClickPuase = false
    this.isPlay = 1
    if (this.playpausestate === 'PLTV' || this.playpausestate === 'LiveTV') {
      this.refreshTime()
    } else if (this.noNextTVOD && this.isTVODEnd && this.playpausestate === 'TVOD') {
      this.playBills['playbill']['isTvodPlayNext'] = true
      this.tVODAppService.playTvod(this.playBills['playbill'], this.playBills['channel'])
      return
    } else if (this.noNextTVOD && this.isTVODEnd && this.playpausestate === 'NPVR') {
        this.playBills['playbill']['isNpvrNext'] = true
        this.nPVRAppService.playNPVR(this.playBills['playbill'])
        return
      }
    EventService.emit('playchannelState', this.isPlay)
  }

  refreshTime () {
    this.playNextPltvFlag = true
    clearInterval(this.seekTimer)
    this.seekTimer = setInterval(() => {
      this.pltvTime = this.pltvTime + 1000
      if (Date.now()['getTime']() - this.pltvTime >= this.pltvCRLength * 1000) {
        this.pltvTime = Date.now()['getTime']() - this.pltvCRLength * 1000
      }
      this.playTime = DateUtils.format(this.pltvTime, 'HH:mm:ss')
      this.totalTime = '/ ' + DateUtils.format(Date.now(), 'HH:mm:ss')
      if (this.pltvTime >= this.playbill['endTime'] && this.playNextPltvFlag) {
        this.playNextPltvFlag = false
        let seekTime = 1 - (this.seekLength / 1000 + this.waitTime) / this.pltvCRLength
        EventService.emit('SEEK_PLAYBILL_TIME',
            { playTime: this.playbill['endTime'], seekTime: seekTime, isLiveTV: false, isNextPlaybill: true })
      }
    }, 1000)
    if (this.playpausestate === 'LiveTV') {
      let position = Number(this.seekbarLIVETV.nativeElement.style.marginLeft.replace('px', ''))
      let all = this.playBarActiveLIVETV.nativeElement.clientWidth
      let seekTime = position / all
      EventService.emit('SEEK_PLAYBILL_TIME',
          { playTime: this.pltvTime, seekTime: seekTime, isLiveTV: false, isNextPlaybill: false, isFirst: true })
      this.isFirstPLTV = false
    }
  }
    /**
     * judge the BTV or TVOD whether supports seek.
     */
  getplaypause (playtimeState) {
    this.playpausestate = playtimeState
  }

    /**
     *  set the play bar to click or not.
     */
  setPlayBar (liveVideoData) {
    this.isPreview = false
    if (liveVideoData['type'] === 'NPVR') {
      document.querySelector('#playpopUp .liveVolumeBarDiv')['style']['right'] = '271px'
      document.querySelector('#videoControllerChannel .btn-volumePath')['style']['right'] = '175px'
    } else {
      document.querySelector('#playpopUp .liveVolumeBarDiv')['style']['right'] = '343px'
      document.querySelector('#videoControllerChannel .btn-volumePath')['style']['right'] = '273px'
    }
    if (!_.isUndefined(liveVideoData.channel)) {
      this.playBills = liveVideoData
      this.curChannel = this.playBills['channel']
      this.playbill = this.playBills['playbill']
      this.isPreview = this.playBills['preview']
    }
      // when it is not PLTV,or locked,or parental controled,or not subscribe,or no url,or the guest,the play bar is not allow to click.
    this.isBarValid = !(liveVideoData['type'] !== 'PLTV' &&
            (liveVideoData['isLock'] || liveVideoData['isParentControl'] || liveVideoData['judgeSub'] ||
                liveVideoData['NoURL'] || liveVideoData['guest'] === 'Guest'))
  }
    /**
     * show the pause buttom when the play bar is allow to click.
     */
  playBtn () {
    let style = {}
    if (this.isBarValid) {
      this.pausedchannel = 'url(assets/img/32/play_fullscreen_32.png)'
        // the pause buttom is set to be grey when it is preview or it is not supported to seek.
      if ((!Cookies.getJSON('IS_PROFILE_LOGIN') || this.isPreview || !this.ispltvCR) && !this.isTVODorPVR) {
        this.pausedchannel = 'url(assets/img/32/timeout_fullscreen2_32.png)'
        style = {
            'backgroundImage': this.pausedchannel
          }
      }
        // show the grey play buttom when the play bar is not allow to click.
    } else {
      this.pausedchannel = 'url(assets/img/32/stop_32.png)'
    }
      // if it is paused,show the play buttom,
      // if it is the last of TVOD or record,show the replay buttom.
    if (this.isPlay === 0) {
      this.playPause = 'play'
      style = {
        'backgroundImage': this.pausedchannel
      }
        // if there is not the last of TVOD or record,show the replay buttom.
      if (this.noNextTVOD && this.isTVODEnd) {
        this.playPause = 'replay'
        style = {
            'backgroundImage': this.replay
          }
      }
    } else {
      this.playPause = 'play_pause'
    }
    return style
  }
}
