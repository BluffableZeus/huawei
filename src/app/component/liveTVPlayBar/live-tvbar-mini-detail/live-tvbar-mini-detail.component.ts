import * as _ from 'underscore'
import { Component, OnInit, Input, OnDestroy } from '@angular/core'
import { EventService } from 'ng-epg-sdk/services'
import { Router } from '@angular/router'
import { ActivatedRoute } from '@angular/router'
import { MiniDetailService } from './live-tv-mini-detail.service'
import { TranslateService } from '@ngx-translate/core'
import { DirectionScroll } from 'ng-epg-ui/webtv-components/scroll/directionScroll.component'
import { CommonService } from '../../../core/common.service'
import { PlaybillAppService } from '../../playPopUpDialog/playbillApp.service'
import { TVODAppService } from '../../playPopUpDialog/tVodApp.service'
import { RecordDataService } from '../../recordDialog/recordData.service'
import { AccountLoginComponent } from '../../../login/account-login'
import { session } from 'src/app/shared/services/session'
import { ReminderService } from '../../reminder/reminder.service'
import { ChannelCacheService } from '../../../shared/services/channel-cache.service'

@Component({
  selector: 'app-livetv-mini-detail',
  styleUrls: ['./live-tvbar-mini-detail.component.scss'],
  templateUrl: './live-tvbar-mini-detail.component.html'
})

export class LiveTVMiniDetailComponent implements OnInit, OnDestroy {
    // declare variables.
    // keep the channel number.
  public channelNo = ''
    // keep the channel logo.
  public logo = ''
    // this channel logo is not display,is used to get when channel logo's src path or pic doesn't exist
  public hiddenLogo = ''
    // keep the programmme name.
  public name = ''
    // keep the channel name.
  public channel = ''
    // '0' is not favorite,'1' is favorite.
  public isFav = ''
    // keep the channel genre.
  public genre = ''
    // keep the week of the programmme.
  public weekTime = ''
    // keep the date of the programmme.
  public leftTime = ''
    // keep the start time of the programmme.
  public startTime = ''
    // keep the end time of the programmme.
  public endTime = ''
    // keep the series.
  public series = ''
    // keep the introduce.
  public introduce = ''
    // keep the rating.
  public rating = ''
    // keep the title of the recomend list.
  public recomendListTitle = ''
    // true is showing all the introduce,false is showing only 3 rows introduce.
  public showMore = false
    // true is showing the mini detail page,false is hiding the mini detail page.
  public showDetail = true
    // keep the text of play buttom when the programme is TVOD.
  public btnName = ''
    // keep the text of play buttom when the programme is BTV.
  public btnNameWatch = ''
    // keep the text of remind buttom.
  public btnNameRemind = ''
    // keep the text of record buttom.
  public btnNameRecord = ''
    // keep the channel id.
  public contentID = ''
    // keep the channel detail.
  public triggerFavData: Object
    // true is the programme is in future.
  public isFuture = false
    // true is the programme is at now.
  public isNow = false
    // true is the programme is in the past.
  public isPass = false
    // true is show the record buttom,false is hide the record buttom.
  public isRecord = false
    // keep the icons of the record.
  public recordsImgStyle = {}
    // keep the icons of the remind.
  public remindImgStyle = {}
    // keep the progamme id.
  public id = ''
    // true is the programme being watching stops recording,show the confirm dialog to stop.
  public isClearAll = false
    // keep the information of confirm dialog for stopping recording.
  public confirmInfoKey
    // the timer for refreshing the buttom.
  public refreshBtnTimer: any
    // keep the channel detail and the programme detail.
  public _data: Object
    // true is show the scroll bar,false is hide the scroll bar.
  public showScroll = true
    // true is show the remind buttom,false is hide the remind buttom.
  public isShowReminder = false
    // the timer for setting the arrow of introduce.
  public getIntervalTip
    //  the height of introduce content.
  public maxOffsetHeight
    // true is show the arrow of introduce,false is hide the arrow of introduce.
  public showHeight = false
    // keep the the scroll bar's height according to the window size
  public scrollHeight
    // get playbill data
  public isFirefox = false
  public canTvod = false
  public onerror = null
  public recomendlist: any = {}
  private recProgramTimer: any
    // control noData
  public ctrlHome

    /**
     * when the data changed
     */
  @Input() set minidata (data: Object) {
    this._data = data[0]
    this.triggerFavData = data[1]
    this.showDetail = true
      // judge the record's status
    let type = this.recordDataService.channelIsRecord(this.triggerFavData['channelDetail'], this._data['playbillDetail'])
    if (type) {
      this.isRecord = true
      this.btnNameRecord = type
      if (this.btnNameRecord === 'livetv_record') {
          // has not been recorded.
        this.recordsImgStyle = {
            background: 'url(assets/img/14/record_14.png)'
          }
      } else {
          // has been recorded.
        this.recordsImgStyle = {
            background: 'url(assets/img/14/stoprecording_14.png)'
          }
      }
    }
    this.id = this._data['playbillDetail']['ID']
      // judge the reminder's status
    if (this._data['playbillDetail']['reminder']) {
        // has been remindered.
      this.btnNameRemind = 'cancel_reminder'
      this.remindImgStyle = {
        background: 'url(assets/img/14/cancelreminder_14.png)'
      }
    } else {
        // has not been remindered.
      this.btnNameRemind = 'reminder'
      this.remindImgStyle = {
        background: 'url(assets/img/14/reminder_14.png)'
      }
    }
      // get getDynamicData into _data for physicalChannelsDynamicProperties
    let userType = this.recordDataService.getUserType()
    this.recordDataService.getDynamicData(this._data['channelDetail'], userType)
  }

    /**
     * name for modules that would be used in this module
     */
  constructor (
        private directionScroll: DirectionScroll,
        private route: ActivatedRoute,
        private router: Router,
        private miniDetailService: MiniDetailService,
        private translate: TranslateService,
        private commonService: CommonService,
        private playbillAppService: PlaybillAppService,
        private tVODAppService: TVODAppService,
        private recordDataService: RecordDataService,
        private reminderService: ReminderService,
        private channelCacheService: ChannelCacheService
    ) {
  }

    /**
     * when exit the component, clear all the timer and events
     */
  ngOnDestroy () {
    if (this.getIntervalTip) {
      clearInterval(this.getIntervalTip)
    }
    if (this.refreshBtnTimer) {
      clearTimeout(this.refreshBtnTimer)
    }
  }

    /**
     * initialization
     */
  ngOnInit () {
      // deal with the playbill data.
    let self = this
    this.dealPlaybillDatas()
    this.setWindowHeight()
      // reset the arrowhead of the introduce
    if (document.getElementById('introduce')) {
      let introduce = document.getElementById('introduce')
      introduce['style']['height'] = '63px'
      introduce['style']['-webkit-line-clamp'] = '3'
      this.showMore = false
      this.showScroll = false
    }
      // reset the scroll postion
    if (document.getElementById('mini-scrollContent')) {
      document.getElementById('mini-scrollContent')['style']['top'] = '0px'
    }
    if (document.getElementById('mini-scroll')) {
      document.getElementById('mini-scroll')['style']['top'] = '0px'
    }
    if (document.getElementById('mini-contentScroll')) {
      document.getElementById('mini-contentScroll')['style']['top'] = '0px'
    }
    this.pluseOnScroll()
      // monitor the event of  adding record successfull and change the record's status
    EventService.on('RECORD_ADD_SUCCESS', function (type) {
      if (self.isNow === true) {
          // the progamme being watched
        self.btnNameRecord = 'stop_recording'
        self.recordsImgStyle = {
            background: 'url(assets/img/14/stoprecording_14.png)'
          }
        return
      }
      if (self.isFuture === true) {
          // the progamme being in future.
        self.btnNameRecord = 'cancel_record'
        self.recordsImgStyle = {
            background: 'url(assets/img/14/stoprecording_14.png)'
          }
      }
    })
    if (this.isNow) {
      this.refreshBtn()
    }
    this.getIsReminder(this._data['playbillDetail'])
      // deal the arrowhead of the introduce
    clearInterval(this.getIntervalTip)
    this.getIntervalTip = setInterval(() => {
      if ((this.introduce.length > 0) && document.getElementById('introduce')) {
        self.showMoreHeight()
        clearInterval(this.getIntervalTip)
      }
    }, 10)
    let ua = navigator.userAgent.toLowerCase()
    this.isFirefox = !!ua.match(/firefox\/([\d.]+)/)
  }

    /**
     * refresh the progress and left time of the recommand program
     */
  refreshRecProgress () {
    this.recProgramTimer = setInterval(() => {
      this.miniDetailService.refreshProgress(this.recomendlist)
    }, 1000 * 60)
  }

  dealPlaybillDatas () {
    this.confirmInfoKey = ['stopRecord_ConfirmInfo', 'stopRecord_ConfirmTitle']
    this.channelNo = this._data['channelNo'] || this._data['channelDetail']['ID']
    this.logo = 'url(' + this._data['logo'] + ') 0% 0% / cover'
    this.hiddenLogo = this._data['logo']
    this.name = this._data['name']
    this.channel = this._data['channel']
    this.isFav = this._data['isFav'] ? '1' : '0'
    this.genre = this._data['genre'] && this._data['genre'].length > 0 ? ' | ' + this._data['genre'][0]['genreName'] : ''
    this.weekTime = this._data['weekTime']
    this.leftTime = this._data['leftTime']
    this.startTime = this._data['startTime']
    this.endTime = this._data['endTime']
    let seasonNO, sitcomNO
    if (this._data['Series'] && this._data['Series'].seasonNO < 10) {
      seasonNO = '0' + this._data['Series'].seasonNO
    }
    if (this._data['Series'] && this._data['Series'].sitcomNO < 10) {
      sitcomNO = '0' + this._data['Series'].sitcomNO
    }
    this.setSeries(seasonNO, sitcomNO)
    this.introduce = this._data['introduce']
    this.rating = this._data['rating'] ? this._data['rating'] : ''
  }

  setSeries (seasonNO, sitcomNO) {
    this.series = this._data['Series'] && this._data['Series'].seasonNO && seasonNO
        ? this.translate.instant('vod_season', { num: seasonNO }) : ''
    this.setSeriesValue(sitcomNO)
  }

  setSeriesValue (sitcomNO) {
    if (this.series !== '') {
      this.series = this._data['Series'] && this._data['Series'].sitcomNO && sitcomNO ? this.series +
                ', ' + this.translate.instant('episode', { num: sitcomNO }) + ' ' : this.series + ''
    } else {
      this.series = this._data['Series'] && this._data['Series'].sitcomNO && sitcomNO ? this.series +
                this.translate.instant('episode', { num: sitcomNO }) + ' ' : this.series + ''
    }
  }

  setWindowHeight () {
    this.contentID = this._data['playbillDetail'] && this._data['playbillDetail'].channelDetail &&
            this._data['playbillDetail'].channelDetail.ID
    let recmContents
    recmContents = this._data['recmContents'] ? this._data['recmContents'] : '0'
    this.recomendListTitle = this.translate.instant('the_viewers_also_watched') + ' ' +
            '(' + recmContents + ')'
    this.isFuture = this._data['isFuture']
    this.isNow = this._data['isNow']
    this.isPass = this._data['isPass']
    if (this._data['isCUTV'] === '0') {
      this.isPass = false
    }
    this['recomendlist'] = this.miniDetailService.getRecomend(this._data['recomendList'])
    this.refreshRecProgress()
    if (this.recomendlist.length <= 0) {
      this.ctrlHome = true
    } else {
      this.ctrlHome = false
    }
    this.btnNameWatch = 'watch_now'
      // set the scroll hight according to the window size
    this.scrollHeight = window.screen.height - 50 + 'px'

    this.judgePlayOrSubscribe(this._data)
  }

  judgePlayOrSubscribe (resp) {
    let mediaData = resp['playbillDetail']['channelDetail']['physicalChannels'][0] ||
            resp['playbillDetail']['channelDetail']['physicalChannelsDynamicProperties'][0]
    let hasOutTime = false
    let length = Number(Date.now()['getTime']()) - this.getNumber(mediaData['cutvCR']['length'])
    if (Number(resp['startTimeorg']) > length) {
      hasOutTime = true
    } else {
      hasOutTime = false
    }
    let canPlay = mediaData['cutvCR']['enable'] === '1' && mediaData['cutvCR']['isContentValid'] === '1' &&
            mediaData['cutvCR']['isSubscribed'] === '1' && mediaData['cutvCR']['isValid'] === '1' && this.isPass && hasOutTime
    this.setBtnName(canPlay, mediaData, hasOutTime)
  }

  setBtnName (canPlay, mediaData, hasOutTime) {
    let canBuy = (mediaData['cutvCR']['enable'] === '1' && mediaData['cutvCR']['isContentValid'] === '1') &&
            (mediaData['cutvCR']['isSubscribed'] !== '1' || mediaData['cutvCR']['isValid'] !== '1') && this.isPass && hasOutTime
    if (canPlay) {
      this.btnName = 'play'
      this.canTvod = true
    } else if (canBuy) {
      this.btnName = 'buy'
      this.canTvod = true
    } else {
      this.canTvod = false
    }
  }

  getNumber (length) {
    return Number(length) * 1000
  }

    /**
     * when channel logo's src path or the pic does not exist,there show default picture
     */
  showDefaultLogo () {
    document.getElementsByClassName('detailLogo')[0]['style']['background'] =
            'url(assets/img/default/minidetail_channel_poster.png)'
  }

  showDetailLogo () {
    return {
      'background': this.logo
    }
  }

  refreshBtn () {
    this.refreshBtnTimer = setTimeout(() => {
      this.isPass = true
      this.isRecord = false
      this.isNow = false
    }, Number(this._data['endtTimeData']) - Date.now()['getTime']())
  }

    /**
     * roll the scroll event
     */
  pluseOnScroll () {
    let oConter, oUl, oScroll, oSpan
    let oBox = document.getElementById('miniDetail')
    oConter = document.getElementById('mini-content-wrap')
    oUl = document.getElementById('mini-scrollContent')
    oScroll = document.getElementById('mini-scroll')
    oSpan = oScroll.getElementsByTagName('span')[0]
      // judge the scroll show
    if (this['recomendlist'].length > 6 || oUl['offsetHeight'] > window.screen.height - 50) {
      this.showScroll = true
    } else if (oUl['offsetHeight'] <= window.screen.height - 50) {
      this.showScroll = false
    }
      // deal roll scroll
    this.directionScroll.setScroll(oBox, oConter, oUl, oScroll, oSpan, true)
  }

    /**
     * show more height
     */
  showMoreHeight () {
    let self = this
    if (document.getElementById('introduceShowIcon')) {
      this.maxOffsetHeight = document.getElementById('introduceShowIcon').offsetHeight
    }
    let introduce = document.getElementById('introduce')
    let intOffsetHeight
    if (introduce) {
      intOffsetHeight = introduce.offsetHeight || 0
      if (this.introduce.length > 0 && (intOffsetHeight === 0)) {
      } else {
        if (this.maxOffsetHeight <= 63) {
            // when the row number of introduce content is less then 3,the arrowhead disappear
            this.showHeight = false
          } else {
            // when the row number of introduce content is more then 3,the arrowhead display
            self.showHeight = true
            if (self.showMore) {
              // show all introduce content
              introduce['style']['height'] = 'auto'
              introduce['style']['-webkit-line-clamp'] = 'inherit'
            } else if (!self.showMore) {
              // show 3 rows of the introduce content
              introduce['style']['height'] = '63px'
              introduce['style']['-webkit-line-clamp'] = '3'
            }
          }
      }
    }
  }

    /**
     * click the arrowhead and show all introduce
     */
  increaseMore () {
    this.showMore = true
    this.showMoreHeight()
    this.pluseOnScroll()
  }

    /**
     * click the arrowhead and show 3 rows introduce
     */
  discreaseLess () {
    this.showMore = false
    this.showMoreHeight()
      // initial the scroll position
    document.getElementById('mini-scrollContent')['style']['top'] = '0px'
    document.getElementById('mini-scroll')['style']['top'] = '0px'
    document.getElementById('mini-contentScroll')['style']['top'] = '0px'
    this.pluseOnScroll()
  }

    /**
     * close mini detail page
     */
  closeMiniDetail () {
    this.showDetail = false
    clearInterval(this.recProgramTimer)
    _.delay(() => {
      EventService.emit('CLOSED_FULLSCREEN_MINIDETAIL')
      clearTimeout(this.refreshBtnTimer)
    }, 1000)
  }

    /**
     * click favorite icon to add or delete favorite
     */
  favManger () {
    let self = this
    if (Cookies.getJSON('IS_GUEST_LOGIN')) {
      EventService.emit('REFRESH_LOGIN')
      return
    }
    if (session.get('TVGuide')) {
      EventService.emit('FAVORITE_MANGER', this.triggerFavData)
        // add favorite
      EventService.on('ADD_FAVORITE', function (data) {
        self.isFav = '1'
        EventService.emit('ADD_successful', {})
      })
        // remove favorite
      EventService.on('REMOVE_FAVORITE', function (data) {
        self.isFav = '0'
        EventService.emit('REMOVE_successful', {})
      })
    } else {
      if (self.isFav === '1') {
        let req = {
            contentIDs: [this.triggerFavData['channelDetail']['ID']],
            contentTypes: ['CHANNEL']
          }

        self.commonService.removeFavorite(req).then(resp => {
            if (resp['result']['retCode'] === '000000000') {
              self.isFav = '0'
              EventService.emit('REMOVE_FAVORITE', this.triggerFavData)
              EventService.emit('REMOVE_successful', {})
              EventService.emit('MINI_REMOVE_FAVORITE', this.triggerFavData)
              // refresh the localStroge
              let sessionDynamicData = self.channelCacheService.getDynamicChannelData()
              _.each(sessionDynamicData['channelDynamaicProp'], (list, index) => {
                if (list['ID'] === self.triggerFavData['channelDetail']['ID']) {
                  list['favorite'] = undefined
                }
              })
              let userType
              if (Cookies.getJSON('IS_PROFILE_LOGIN')) {
                userType = Cookies.getJSON('USER_ID')
              } else {
                userType = session.get('Guest')
              }
              session.put(userType + '_Dynamic_Version_Data', sessionDynamicData, true)
              self.channelCacheService.favRefresh(resp['version'], resp['preVersion'])
            }
          })
      } else {
        let req = {
            favorites: [{
              contentID: this.triggerFavData['channelDetail']['ID'],
              contentType: 'CHANNEL'
            }],
            autoCover: '0'
          }

        self.commonService.addFavorite(req).then(resp => {
            if (resp['result']['retCode'] === '000000000') {
              self.isFav = '1'
              EventService.emit('ADD_FAVORITE', this.triggerFavData)
              EventService.emit('ADD_successful', {})
              EventService.emit('MINI_ADD_FAVORITE', this.triggerFavData)
              // refresh the localStroge
              let sessionDynamicData = self.channelCacheService.getDynamicChannelData()
              _.each(sessionDynamicData['channelDynamaicProp'], (list, index) => {
                if (list['ID'] === self.triggerFavData['channelDetail']['ID']) {
                  list['favorite'] = {}
                }
              })
              let userType
              if (Cookies.getJSON('IS_PROFILE_LOGIN')) {
                userType = Cookies.getJSON('USER_ID')
              } else {
                userType = session.get('Guest')
              }
              session.put(userType + '_Dynamic_Version_Data', sessionDynamicData, true)
              self.channelCacheService.favRefresh(resp['version'], resp['preVersion'])
            }
          })
      }
    }
  }

    /**
     * click the poster or playbill name and show the mini detail of the program
     */
  getRecmDetail (list) {
    if (list.hasProcess) {
      EventService.emit('PLAYCHANNEL', list.channelID)
    }
    this.miniDetailService.getDetail(list.ID, list.endTime, list.startTime).then((resp) => {
      this._data = resp
      this.triggerFavData = {
        channelDetail: resp.channelDetail,
        playbillLites: resp.playbillLites
      }
      this.ngOnInit()
        // refresh storage
      this.refeshStroage(resp['channelDetail'])
    })
  }

  refeshStroage (channelDetail) {
    let self = this
    let sessionStaticData = self.channelCacheService.getStaticChannelData()
    let sessionDynamicData = self.channelCacheService.getDynamicChannelData()
    _.each(sessionStaticData['channelDetails'], (list, index) => {
      if (list['ID'] === channelDetail['ID']) {
        list['isLocked'] = channelDetail['isLocked']
        if (channelDetail['favorite']) {
            list['favorite'] = {}
          } else {
            list['favorite'] = undefined
          }
      }
    })
    _.each(sessionDynamicData['channelDynamaicProp'], (list, index) => {
      if (list['ID'] === channelDetail['ID']) {
        list['isLocked'] = channelDetail['isLocked']
        if (channelDetail['favorite']) {
            list['favorite'] = {}
          } else {
            list['favorite'] = undefined
          }
      }
    })
    let userType
    if (Cookies.getJSON('IS_PROFILE_LOGIN')) {
      userType = Cookies.getJSON('USER_ID')
    } else {
      userType = session.get('Guest')
    }
    session.put(userType + '_Static_Version_Data', sessionStaticData, true)
    session.put(userType + '_Dynamic_Version_Data', sessionDynamicData, true)
  }

    /**
     * set the icon's location of closing the mini detail page.
     */
  hidePosition () {
    return {
      'top': (document.body.clientHeight - 128) / 2 + 'px'
    }
  }

    /**
     * click play icon and play
     */
  goToPlay () {
    if (this.btnName === 'buy') {
      session.put('NEED_TO_BUY', 'buy')
    } else {
      session.put('NEED_TO_BUY', 'play')
    }
    this.showDetail = false
    let indexDataId = this._data['playbillDetail']['ID']
    if (indexDataId !== this.id && document.getElementsByClassName('live_load_bg') &&
            document.querySelector('.channel_login_load_content')) {
      document.getElementsByClassName('live_load_bg')[0]['style']['display'] = 'block'
      document.querySelector('.channel_login_load_content')['style']['display'] = 'block'
    }
    EventService.emit('OPEN_FULLSCREEN_LIVETVVIDEO', this._data)
    this.setVideo()
    if (this.isNow) {
      this.closeMiniDetail()
    }
    if (this.isPass) {
      this.tVODAppService.playTvod(this._data['playbillDetail'], this._data['playbillDetail'].channelDetail)
    }
  }

    /**
     * set up the video
     */
  setVideo () {
    let video = document.querySelector('#videoContainer video')
    let ua = navigator.userAgent.toLowerCase()
    let isIe = !!ua.match(/msie/i) || !!ua.match(/rv:([\d.]+).*like gecko/)
    let isFirefox = !!ua.match(/firefox\/([\d.]+)/)
    let isEdge = !!ua.match(/edge\/([\d.]+)/)
    if (isIe) {
      video = document.querySelector('#videoContainer')
      video['msRequestFullscreen']()
    } else if (isEdge) {
      video = document.querySelector('#videoContainer')
      video.webkitRequestFullScreen()
    } else if (isFirefox) {
        video = document.querySelector('#videoContainer')
        video['mozRequestFullScreen']()
      } else if (ua.match(/version\/([\d.]+).*safari/)) {
        video.requestFullscreen()
      } else {
        video.webkitRequestFullScreen()
      }
  }

    /**
     * click record button
     */
  goToRecord (type) {
    if (Cookies.getJSON('IS_GUEST_LOGIN')) {
      EventService.emit('REFRESH_LOGIN')
      return
    }
    if (type === 'livetv_record') {
        // open  record dialog
      let recordData = this._data['playbillDetail']
      recordData['isNow'] = true
      EventService.emit('LIVETV_RECORD', recordData)
    } else {
        // cancel record
        // if the playbill belong to now,open the dialog of stopping record.
      if (type === 'stop_recording') {
        this.isClearAll = true
      } else {
        this.isClearAll = false
      }
        // if the playbill belong to pass,stop record.
      if (type === 'cancel_recording') {
        this.recordDataService.cancelRecord([this._data['playbillDetail']['ID']]).then(function (resp) {
            this.btnNameRecord = 'livetv_record'
            this.recordsImgStyle = {
              background: 'url(assets/img/14/record_14.png)'
            }
            EventService.emit('RECORD_CANCEL_SUCCESS')
          }.bind(this))
      }
    }
  }

    /**
     * reminder button status(reminder or cancel reminder)
     */
  getIsReminder (playbill) {
    let state = this.reminderService.isSupportReminder(playbill)
    if (state) {
      this.isShowReminder = true
      if (state === 'ADD') {
        this.btnNameRemind = 'reminder'
      } else {
        this.btnNameRemind = 'cancel_reminder'
      }
    } else {
      this.isShowReminder = false
    }
  }

    /**
     *  click reminder button
     */
  goToRemind (type) {
    if (Cookies.getJSON('IS_GUEST_LOGIN')) {
      this.commonService.openDialog(AccountLoginComponent)
      return
    }
    if (type === 'reminder') {
        // add reminder
      let req = {
        contentID: this.id,
        contentType: 'PROGRAM'
      }
      this.reminderService.addReminder({ reminders: [req] }).then((resp) => {
        this.btnNameRemind = 'cancel_reminder'
        this.remindImgStyle = {
            background: 'url(assets/img/14/cancelreminder_14.png)'
          }
        EventService.emit('ADD_REMINDER')
      })
      return
    }
    if (type === 'cancel_reminder') {
        // cancel reminder
      this.reminderService.removeReminder({
        contentIDs: [this.id],
        contentTypes: ['PROGRAM']
      }).then((resp) => {
          this.btnNameRemind = 'reminder'
          this.remindImgStyle = {
            background: 'url(assets/img/14/reminder_14.png)'
          }
          EventService.emit('REMOVE_REMINDER')
        })
    }
  }

    /**
     * close the dialog of stopping record.
     */
  cancelData () {
    this.isClearAll = false
  }

    /**
     * click the confirm buttom of stopping recording dialog.
     */
  confirmData () {
    this.recordDataService.cancelRecord([this._data['playbillDetail']['ID']]).then(function (resp) {
      this.isClearAll = false
      this.btnNameRecord = 'livetv_record'
      this.recordsImgStyle = {
        background: 'url(assets/img/14/record_14.png)'
      }
      EventService.emit('RECORD_CANCEL_SUCCESS')
      EventService.emit('RECORDLIVE_CANCEL_SUCCESS', this.id)
      EventService.emit('REMOVE_RECORD_FOR_MINIEPG', this.id)
    }.bind(this))
  }

  showWidth () {
    if (!this.isFirefox) {
      let styles = document.querySelector('.introduce')['style']['-webkit-box-direction']
      if (styles === 'reverse') {
        document.querySelector('.introduce')['style']['-webkit-box-direction'] = 'normal'
      } else {
        document.querySelector('.introduce')['style']['-webkit-box-direction'] = 'reverse'
      }
    }
  }
}
