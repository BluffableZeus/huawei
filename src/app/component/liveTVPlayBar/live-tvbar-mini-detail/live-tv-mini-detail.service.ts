import * as _ from 'underscore'
import { Injectable } from '@angular/core'
import { PictureService } from 'ng-epg-ui/services/picture.service'
import { getPlaybillDetail } from 'ng-epg-sdk/vsp'
import { CommonService } from '../../../core/common.service'
import { DateUtils } from 'ng-epg-sdk/utils'
import { session } from 'src/app/shared/services/session'
import { TranslateService } from '@ngx-translate/core'
import { CustomConfigService } from '../../../shared/services/custom-config.service'
import { ChannelCacheService } from '../../../shared/services/channel-cache.service'

@Injectable()
export class MiniDetailService {
  constructor (
        private pictureService: PictureService,
        private commonService: CommonService,
        private translate: TranslateService,
        private customConfigService: CustomConfigService,
        private channelCacheService: ChannelCacheService
  ) { }

  getRecomend (recomendList) {
    return _.map(recomendList, (list, index) => {
      let len
      let hasProcess
      let url = list['playbillLites'] && list['playbillLites'][0] &&
                list['playbillLites'][0].picture && list['playbillLites'][0].picture.posters &&
                this.pictureService.convertToSizeUrl(list['playbillLites'][0].picture.posters[0],
                  { minwidth: 184, minheight: 104, maxwidth: 184, maxheight: 104 })
      let playbill = list['playbillLites'] && list['playbillLites'][0]
      let playbilllTime = playbill['endTime'] - playbill['startTime']
      let currentDuration = Date.now()['getTime']() - playbill['startTime']
      let leftTimes = Math.ceil((new Date(Number(playbill['endTime'])).getTime() -
                new Date(Number(Date.now()['getTime']())).getTime()) / 60000)
      if (currentDuration > playbilllTime) {
        len = '100%'
        hasProcess = false
      } else if (currentDuration < 0) {
        len = '0%'
        hasProcess = false
      } else {
        len = Number(currentDuration / playbilllTime * 100) + '%'
        hasProcess = true
      }
      return {
        name: list['playbillLites'][0].name,
        posterUrl: url || 'assets/img/default/livetv_mini.png',
        ID: list['playbillLites'][0].ID,
        endTime: list['playbillLites'][0].endTime,
        len: len,
        channelID: list['playbillLites'][0]['channelID'],
        hasProcess: hasProcess,
        startTime: list['playbillLites'][0].startTime,
        leftTime: leftTimes,
        leftTimes: { min: leftTimes },
        startTimes: DateUtils.format(parseInt(list['playbillLites'][0]['startTime'], 10), 'HH:mm'),
        channelName: list['channelDetail'] ? ' | ' + list['channelDetail']['name'] : ''
      }
    })
  }
  refreshProgress (list) {
    let recList = list
    for (let i = 0; i < recList.length; i++) {
      if (!recList[i]['endTime']) {
        continue
      }
      let length = recList[i]['endTime'] - recList[i]['startTime']
      let curLength = Date.now()['getTime']() - recList[i]['startTime']
      // if the program is end and not refresh the next one,the  progress show full
      let leftTime = Math.ceil((new Date(Number(recList[i]['endTime'])).getTime() -
                new Date(Number(Date.now()['getTime']())).getTime()) / 60000)
      // if the leftTime less then 0,leftTime is 0
      if (leftTime < 0) {
        leftTime = 0
      }
      if (curLength > length) {
        recList[i]['hasProcess'] = false
        recList[i]['len'] = '100%'
      } else if (curLength < 0) {
        recList[i]['len'] = '0%'
        recList[i]['hasProcess'] = false
      } else {
        recList[i]['len'] = curLength >= length ? '100%' : Number(curLength) / Number(length) * 100 + '%'
        recList[i]['hasProcess'] = true
      }
      recList[i]['leftTimes'] = { min: leftTime }
    }
    return recList
  }

  getDetail (playbillID, endtimes, startTimes) {
    let recmscenarios
    let isFuture
    let isNow
    let isPass
    if (endtimes > Date.now()['getTime']()) {
      recmscenarios = [{
        count: '12',
        offset: '0',
        contentType: 'PROGRAM',
        contentID: playbillID,
        entrance: 'Program_Similar_Recommendations',
        businessType: 'BTV'
      }]
      if (startTimes < Date.now()['getTime']()) {
        isNow = true
      } else {
        isFuture = true
      }
    } else if (endtimes < Date.now()['getTime']()) {
      recmscenarios = [{
        count: '12',
        offset: '0',
        contentType: 'PROGRAM',
        contentID: playbillID,
        entrance: 'CatchupTV_Similar_Recommendations',
        businessType: 'CUTV'
      }]
      isPass = true
    }
    return this.customConfigService.getCustomizeConfig({ queryType: '0' }).then(config => {
      let customizeConfig = config['list']
      let nameSpace = customizeConfig['ott_channel_name_space']
      let dynamicData = this.channelCacheService.getDynamicChannelData().channelDynamaicProp
      let staticData = this.channelCacheService.getStaticChannelData().channelDetails
      let options = {
        params: {
          SID: 'playbilldetail2',
          DEVICE: 'PC',
          DID: session.get('uuid_cookie')
        }
      }
      return getPlaybillDetail({
        playbillID: playbillID,
        channelNamespace: nameSpace,
        isReturnAllMedia: '1',
        queryDynamicRecmContent: {
          recmScenarios: recmscenarios
        }
      }, options).then(resp => {
        let weekTime = DateUtils.format(parseInt(resp.playbillDetail.startTime, 10), 'ddd')
        let month = DateUtils.format(parseInt(resp.playbillDetail.startTime, 10), 'MM')
        let day = DateUtils.format(parseInt(resp.playbillDetail.startTime, 10), 'D')
        let year = DateUtils.format(parseInt(resp.playbillDetail.startTime, 10), 'YYYY')
        let lastMonth = this.commonService.dealWithTime(month)
        let leftTime
        if (session.get('languageName') === 'zh') {
          leftTime = lastMonth + '/' + day
        } else {
          leftTime = lastMonth + ' ' + day
        }
        let nowDay = DateUtils.format(parseInt(Date.now()['getTime'](), 10), 'D')
        let nowMonth = DateUtils.format(parseInt(Date.now()['getTime'](), 10), 'MM')
        let nowYear = DateUtils.format(parseInt(Date.now()['getTime'](), 10), 'YYYY')
        if (nowDay === day && nowMonth === month && nowYear === year) {
          leftTime = this.translate.instant('today')
          weekTime = ''
        }
        let dynamicDetail = _.find(dynamicData, (detail) => {
          return detail['ID'] === resp['playbillDetail']['channelDetail']['ID']
        })
        let staticDetail = _.find(staticData, (detail) => {
          return detail['ID'] === resp['playbillDetail']['channelDetail']['ID']
        })
        let url = this.getPictureUrl(staticDetail)
        let list = {
          channelNo: dynamicDetail['channelNO'],
          logo: url,
          name: resp.playbillDetail.name,
          channel: staticDetail['name'],
          isLock: dynamicDetail['isLocked'],
          isFav: dynamicDetail['favorite'],
          genre: resp.playbillDetail.genres,
          weekTime: weekTime,
          leftTime: leftTime,
          startTimeorg: resp.playbillDetail.startTime,
          startTime: DateUtils.format(parseInt(resp.playbillDetail.startTime, 10), 'HH:mm'),
          endTime: DateUtils.format(parseInt(resp.playbillDetail.endTime, 10), 'HH:mm'),
          endtTimeData: resp.playbillDetail.endTime,
          Series: resp.playbillDetail.playbillSeries,
          introduce: resp.playbillDetail.introduce,
          rating: resp && resp.playbillDetail && resp.playbillDetail.rating && resp.playbillDetail.rating.name,
          playbillDetail: resp.playbillDetail,
          recomendList: resp['recmContents'] && resp['recmContents'][0] && resp['recmContents'][0]['recmPrograms'],
          isCUTV: dynamicDetail['physicalChannelsDynamicProperties'][0].cutvCR.enable,
          recmContents: resp['recmContents'] && resp['recmContents'][0] && resp.recmContents[0].recmPrograms &&
                    resp.recmContents[0].recmPrograms.length,
          isFuture: isFuture,
          isNow: isNow,
          isPass: isPass,
          channelDetail: resp.playbillDetail.channelDetail,
          playbillLites: [resp.playbillDetail]
        }
        return list
      })
    })
  }

  getPictureUrl (staticDetail) {
    return this.pictureService.convertToSizeUrl(staticDetail && staticDetail['picture'] &&
        staticDetail['picture']['icons'] && staticDetail['picture']['icons'][0],
    { minwidth: 40, minheight: 40, maxwidth: 40, maxheight: 40 })
  }

  prefixZero (str, len) {
    str = '' + (str || '')
    if (str.length >= len) {
      return str
    }
    let gapLen = len - str.length, i, arr = []
    for (i = 0; i < gapLen; i++) {
      arr[i] = '0'
    }
    return arr.join('') + str
  }
}
