import { Component, Output, EventEmitter, Input, OnInit } from '@angular/core'
import { EventService } from 'ng-epg-sdk/services'

@Component({
  selector: 'app-close-confirm',
  templateUrl: './closeOrConfirm.component.html',
  styleUrls: ['./closeOrConfirm.component.scss']
})

export class CloseOrConfirmComponent implements OnInit {
  // get the data.
  @Input() set confirmInfoKey (data) {
    this.confirmInfo = data[0]
    this.closeInfo = data[1]
  }
    // declare variables.
  @Output() cancelData: EventEmitter<Object> = new EventEmitter()
  @Output() confirmData: EventEmitter<Object> = new EventEmitter()
  public enableConfirm = true
  public confirmInfo
  public closeInfo
    // private constructor
  constructor (
    ) { }

  ngOnInit () {
  }
    // click the delete buttom to close the dialog.
  close (data) {
    this.cancelData.emit(data)
    EventService.emit('IS_CHANNELEDIT', false)
    EventService.emit('IS_VODEDIT', false)
    EventService.emit('IS_RECORDEDIT', false)
  }
    // click the confirm buttom to confirm.
  confirm (data) {
    if (this.enableConfirm === true) {
      this.confirmData.emit(data)
      this.enableConfirm = false
    }
  }
}
