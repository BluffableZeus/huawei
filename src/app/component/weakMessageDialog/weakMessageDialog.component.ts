import { Component, AfterViewInit, OnDestroy } from '@angular/core'
import { DialogParams, DialogRef } from 'ng-epg-ui/webtv-components/dialog'

@Component({
  selector: 'app-weakmessage-dialog',
  styleUrls: ['./weakMessageDialog.component.scss'],
  templateUrl: './weakMessageDialog.component.html'
})

export class WeakMessageDialogComponent implements AfterViewInit, OnDestroy {
  private weakTips: any = ''
  private getWeakTip: any

  constructor (
        private config: DialogParams,
        private dialog: DialogRef
    ) {
    let self = this
    this.weakTips = this.config
    this.handleTips()
    clearTimeout(self.getWeakTip)
    self.getWeakTip = setTimeout(() => {
      self.dialog.close()
    }, 3 * 1000)
  }

  ngAfterViewInit () {
    if (this.dom('login-history-info')) {
      this.dom('login-history-info')['innerHTML'] = this.weakTips
    }
  }

  ngOnDestroy () {
    clearTimeout(this.getWeakTip)
  }

  handleTips () {
    for (let i = 0; i < this.weakTips.length; i++) {
      if (this.weakTips.charAt(i) === ';' || this.weakTips.charAt(i) === '；') {
        this.weakTips = this.weakTips.substring(0, i + 1) + '<br />' + this.weakTips.substring(i + 1, this.weakTips.length)
      }
    }
  }

  dom (divName: string) {
    return document.getElementById(divName)
  }
}
