import * as _ from 'underscore'
import { Component, OnInit } from '@angular/core'
import { DirectionScroll } from 'ng-epg-ui/webtv-components/scroll/directionScroll.component'
import { Router } from '@angular/router'
import { session } from 'src/app/shared/services/session'
import { EventService } from 'ng-epg-sdk/services'
import { DialogRef, DialogParams } from 'ng-epg-ui/webtv-components/dialog'

@Component({
  selector: 'app-storageremind',
  styleUrls: ['./storageRemind.component.scss'],
  templateUrl: './storageRemind.component.html'
})

export class StorageRemindComponent implements OnInit {
  public channelDates = []
  public isShowStorage = true
  public isChooseIndex = '0'
  public type
    // flag that check whether it is in live tv playing page.
  public isNow = false
  public visibility = 'hidden'
  public source = ''

  constructor (
        private directionScroll: DirectionScroll,
        private router: Router,
        private config: DialogParams,
        private dialog: DialogRef
    ) {}

  ngOnInit () {
    _.delay(() => {
      this.visibility = 'visible'
    }, 500)
    this.type = this.config['type']
    this.isNow = false
    if (!_.isUndefined(this.config['type'])) {
      this.isNow = this.config['isNow']
    }
    if (!_.isUndefined(this.config['source'])) {
      this.source = this.config['source']
    }
  }

  closeRemind () {
    if (this.dialog) {
      this.dialog.close()
    }
  }

  closeConflict () {
    if (this.dialog) {
      if (this.source === 'manualUpdate' || this.source === 'playbillUpdate') {
        EventService.emit('RECORD_OPERATION_FAILED')
      } else {
        EventService.emit('SHOW_RECORD_FAIL_TOAST')
      }
      this.dialog.close()
    }
  }

  gotoRecordingList () {
    if (this.dialog) {
      this.dialog.close()
    }
      // if this is in live tv playing page, the dialog will be closed before going to recording-manage page.
    if (this.isNow) {
      EventService.emit('EXIT_FULLSCREEN')
      EventService.emit('CLOSED_FULLSCREEN_LIVETVVIDEO')
    }
    if (location.pathname.indexOf('recording') !== -1) {
      EventService.emit('MYTVJUMP')
    } else {
      EventService.emit('RECORDING', 'recording')
      this.router.navigate(['./profile/recording'])
      session.put('RouteMessage', './profile/recording')
    }
  }

  dom (divName: string) {
    return document.getElementById(divName)
  }
}
