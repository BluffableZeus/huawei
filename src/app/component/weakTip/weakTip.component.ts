import { Component } from '@angular/core'
import { TranslateService } from '@ngx-translate/core'

@Component({
  selector: 'app-weaktip-dialog',
  styleUrls: ['./weakTip.component.scss'],
  templateUrl: './weakTip.component.html'
})

export class WeakTipDialogComponent {
  public weakTips = ''
  public name = ''
  constructor (
        private translate: TranslateService
    ) {
  }

    /**
     * get weakTips by type
     */
  getType (type) {
    switch (type) {
      case 'do_not_have_permission':
        this.weakTips = this.translate.instant('do_not_have_permission')
        break
      case 'Last_Catch_Up':
        this.weakTips = this.translate.instant('Last_Catch_Up')
        break
      case 'no_record_program':
        this.weakTips = this.translate.instant('no_record_program')
        break
      case 'operation_successfully':
        this.weakTips = this.translate.instant('operation_successfully')
        break
      case 'package_could_not_open':
        this.weakTips = this.translate.instant('package_could_not_open')
        break
      case 'successfully_added_to_favorites':
        this.weakTips = this.translate.instant('successfully_added_to_favorites')
        break
      case 'successfully_removed_from_favorites':
        this.weakTips = this.translate.instant('successfully_removed_from_favorites')
        break
      case 'recording_time_incorrect_msg':
        this.weakTips = this.translate.instant('recording_time_incorrect_msg')
        break
      case 'add_recording_is_successful':
        this.weakTips = this.translate.instant('add_recording_is_successful')
        break
      case 'cancel_recording_is_successful':
        this.weakTips = this.translate.instant('cancel_recording_is_successful')
        break
      case 'end_date_must_later_than_start_date':
        this.weakTips = this.translate.instant('end_date_must_later_than_start_date')
        break
      case 'preference_setting_successfully':
        this.weakTips = this.translate.instant('preference_setting_successfully')
        break
      default:
        this.getTypeTwice(type)
        break
    }
  }

    /**
     * get weakTips by type
     */
  getTypeTwice (type) {
    switch (type) {
      case 'lock_sucessfully':
        this.weakTips = this.translate.instant('lock_sucessfully')
        break
      case 'unlock_successfully':
        this.weakTips = this.translate.instant('unlock_successfully')
        break
      case 'input_does_not_conform_rules':
        this.weakTips = this.translate.instant('input_does_not_conform_rules')
        break
      case 'no_program_information':
        this.weakTips = this.translate.instant('no_program_information')
        break
      case 'failed_to_add_a_recording_task':
        this.weakTips = this.translate.instant('failed_to_add_a_recording_task')
        break
      case 'repeat_and_date_conflict':
        this.weakTips = this.translate.instant('repeat_and_date_conflict')
        break
      case 'time_input_is_empty':
        this.weakTips = this.translate.instant('time_input_is_empty')
        break
      case 'add_reminder_success':
        this.weakTips = this.translate.instant('add_reminder_success')
        break
      case 'successfully_removed_from_reminder':
        this.weakTips = this.translate.instant('successfully_removed_from_reminder')
        break
      case 'failed_to_add_a_recording_task':
        this.weakTips = this.translate.instant('failed_to_add_a_recording_task')
        break
      case 'record_operation_failed':
        this.weakTips = this.translate.instant('record_operation_failed')
        break
      case 'DST_jumping_prompt':
        this.weakTips = this.translate.instant('DST_jumping_prompt')
        break
      case 'Failed_play_recording':
        this.weakTips = this.translate.instant('Failed_play_recording')
        break
      default:
        this.weakTips = ''
        break
    }
  }

  setMessage (products, alreadyTranslated?: boolean) {
    if (alreadyTranslated) {
      this.weakTips = products['type']
    } else {
      this.name = products['name']
      this.getType(products['type'])
    }
  }
}
