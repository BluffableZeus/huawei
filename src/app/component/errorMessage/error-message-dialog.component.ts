import * as _ from 'underscore'
import { Component, Input } from '@angular/core'
import { EventService } from 'ng-epg-sdk/services'
import { logout } from 'ng-epg-sdk/vsp'
import { session } from 'src/app/shared/services/session'
import { Router } from '@angular/router'

/**
 * '32217' (just for AddPVR, AddPeriodicPVR)
 * '32122' (for all, 125023001)
 * '32116', '32121', '32117', '32118', '32119', '32120', '32115', '32114', '32999' (only for Logout)
 * '26504', '26501' (for http 501, 504 error)
 */
const RELOGIN_CODE = [
  '32217', '32122', '32116', '32121', '32117', '32118', '32119', '32120', '32115', '32114', '32999', '26504', '26501'
]

@Component({
  selector: 'app-error-message-dialog',
  templateUrl: './error-message-dialog.component.html',
  styleUrls: ['./error-message-dialog.component.scss']
})

export class ErrorMessageDialogComponent {
    // declare variables.
  public errorMessageTitle = ''
  public errorMessageContent = ''
  public errorMessageCode = ''
  public dialogTitle = ''
  public btnName = ''
  public config
    // private constructor.
  constructor (
        private router: Router
    ) {
    let self = this
    this.errorMessageCode = ''
    EventService.removeAllListeners(['CLOSED_ERROR_DIALOG'])
    EventService.on('CLOSED_ERROR_DIALOG', function () {
      self.close()
    })
  }
    // get the error data
  @Input() set errorMessageConfig (data) {
    this.config = data
    this.setTitle()
    if (this.config['interfaceName'] === 'PlayVOD') {
      EventService.emit('PLAY_VOD_ERROR')
    }
    if ((this.config['interfaceName'] === 'ReportVOD' || this.config['interfaceName'] === 'PlayVODHeartbeat') &&
            this.config['e'] === '32122') {
      EventService.emit('SESSION_TIME_OUT_CLOSE_PLAYER')
    }
  }
    // deal with the error data in order to show error message.
  setTitle () {
    this.errorMessageTitle = this.config['m']
    this.errorMessageContent = this.config['s']
    this.dialogTitle = this.config['t']
    if (this.config['e'] === '32504' || this.config['e'] === '32999') {
      this.btnName = 'errorbox_cancel'
    } else {
      this.btnName = 'errorbox_confirm'
    }
  }

  removeSession () {
    Cookies.remove('IS_PROFILE_LOGIN')
    Cookies.remove('IS_GUEST_LOGIN')
    Cookies.remove('PROFILE_NAME')
    session.remove('CUSTOM_CONFIG_DATAS')
    Cookies.remove('LOGIN_PROFILE_LOGOURL')
    Cookies.remove('USER_ID')
    Cookies.remove('CUSTOM_REC_PLAYBILLLENS')
    Cookies.remove('PROFILE_TYPE')
  }
  onchange (e) {
    if (e.keyCode === 13) {
      this.close()
    }
  }
    // click the delete buttom of the error dialog.
  close (restart?: boolean) {
    let loadingElem = document.getElementsByClassName('play_loading')[0]
    if (loadingElem) {
      loadingElem['style']['display'] = 'none'
    }
    if (this.config['interfaceName'] === 'SearchContent') {
      if (this.config['e'] === '32579') {
        EventService.emit('CLOSED_LOADING_SPINNER', true)
      } else {
        EventService.emit('CLOSED_LOADING_SPINNER', false)
      }
      EventService.emit('CLOSED_ERROR_MESSAGE_DIALOG')
    }
    if (restart) {
      let findCode = _.find(RELOGIN_CODE, item => {
        return this.config['e'] === item
      })
      if (!_.isUndefined(findCode)) {
        if (this.noNeedRelogin(this.config)) {
            EventService.emit('CLOSED_ERROR_MESSAGE_DIALOG')
          } else {
            this.logoutAndReLoad()
          }
      } else {
        EventService.emit('CLOSED_ERROR_MESSAGE_DIALOG')
      }
      if (this.config['interfaceName'] === 'PlayChannel' && this.config['action'] === 'tips') {
        EventService.emit('EXIT_FULLSCREEN')
        EventService.emit('CLOSED_FULLSCREEN_LIVETVVIDEO')
      }
    } else {
      if (this.config['interfaceName'] === 'PlayChannel' && this.config['action'] === 'tips') {
        EventService.emit('EXIT_FULLSCREEN')
        EventService.emit('CLOSED_FULLSCREEN_LIVETVVIDEO')
      }
      EventService.emit('CLOSED_ERROR_MESSAGE_DIALOG')
      this.config = {}
    }
  }

  noNeedRelogin (config) {
    if (config['e'] === '32217' && (config['interfaceName'] === 'UpdatePVR' || config['interfaceName'] === 'UpdatePeriodicPVR')) {
      return true
    } else if (config['e'] !== '32122' && config['e'] !== '26501' && config['e'] !== '26504' && config['interfaceName'] !== 'Logout') {
      return true
    }
    return false
  }

  logoutAndReLoad () {
    this.router.navigate(['/home'])
    const logoutReq = { type: '0' }
    session.put('NO_POP_UP_ERROR_DIALOG', true, true)
    logout(logoutReq).then(resp => {
      this.removeSession()
      session.put('RELOGIN_POP_UP_DIALOG', true, true)
      session.clear(false)
      location.reload()
    }, respFail => {
      this.removeSession()
      session.put('RELOGIN_POP_UP_DIALOG', true, true)
      session.clear(false)
      location.reload()
    })
    EventService.emit('CLOSED_ERROR_MESSAGE_DIALOG')
  }
}
