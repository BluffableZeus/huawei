import { Component, OnInit } from '@angular/core'
import { EventService } from 'ng-epg-sdk/services'

@Component({
  selector: 'app-definition-detail',
  templateUrl: './definitionDetail.component.html',
  styleUrls: ['./definitionDetail.component.scss']
})

export class DefinitionDetailComponent implements OnInit {
    // declare variables.
  public curDefinition: any = 'auto'
  public definitionDetailLists
  public definitionShowDetailLists
  public curType = ''
  public curIndex = -1

  constructor () { }

  ngOnInit () {
      // get the definition data.
    EventService.on('liveTVDefinitionDataList', (definitionData, type) => {
      definitionData.sort((a, b) => a - b)
      this.definitionDetailLists = definitionData
      this.definitionShowDetailLists = definitionData
      for (let i = 0; i < this.definitionDetailLists.length; i++) {
        this.definitionShowDetailLists[i] = parseInt(this.definitionDetailLists[i], 10)
      }
      this.curType = type
      this.curDefinition = 'auto'
      this.curIndex = -1
    })
    EventService.on('backDefinition', definition => {
      if (definition === 'auto') {
        this.curIndex = -1
      }
      this.curDefinition = definition + ''
    })
  }
    // switch the definition.
  setDefinition (index) {
    let curDefinition
    this.curIndex = index
    if (index === -1) {
      this.curDefinition = 'auto'
      curDefinition = this.curDefinition
    } else {
      this.curDefinition = this.definitionDetailLists[index]
      curDefinition = this.curDefinition * 1000
    }
    if (this.curType === 'vod') {
      EventService.emit('changeVodDefinition', curDefinition, index)
    } else {
      EventService.emit('changeLiveDefinition', curDefinition, index)
    }
  }

  getDefinitionDetailLists (definitionDetailList) {
    definitionDetailList.sort((a, b) => {
      return a - b
    })
    this.definitionDetailLists = definitionDetailList
    this.definitionShowDetailLists = definitionDetailList
    for (let i = 0; i < this.definitionDetailLists.length; i++) {
      this.definitionShowDetailLists[i] = parseInt(this.definitionDetailLists[i], 10)
    }
    this.curType = 'vod'
  }
}
