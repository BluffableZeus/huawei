import * as _ from 'underscore'
import { Component, Input, Output, EventEmitter } from '@angular/core'
import { Router } from '@angular/router'
import { session } from 'src/app/shared/services/session'
import { EventService } from 'ng-epg-sdk/services'
import { TimerService } from '../../shared/services/timer.service'
import { ProfileManageService } from '../../my-tv/profile-manage/profile-manage.service'

/**
 * this component is for basic information page.
 * including setting basic information of new profile, updating basic information of sub profile.
 */
@Component({
  selector: 'app-basic-info',
  templateUrl: './modify-basic.component.html',
  styleUrls: ['./modify-basic.component.scss']
})

export class BasicInfoComponent {
  @Output() closeBasic: EventEmitter<Object> = new EventEmitter()
  @Output() changeSubInfo: EventEmitter<Object> = new EventEmitter()
  @Output() finishBasic: EventEmitter<Object> = new EventEmitter()

  @Input() set basicInfo (data) {
    this.type = data['type']
    this.avatarList = data['avatarList']
    this.levelList = data['levelList']
    this.limitList = data['limitList']
    this.checkArrowVisible(data['avatarList'])
    this.getInputRules()
    this.getAllNameAndID()
    if (data['type'] === 'modify') {
      this.titleName = 'basic_information'
      this.btnName = 'save'
      this.subData = data['subInfo']
      this.getOriginalInfo(data['subInfo'])
    } else {
      this.titleName = 'add_new_sub_profile'
      this.btnName = 'next_step'
      this.getDefaultValue().then(resp => {
        this.getOriginalInfo(this.subData)
      })
    }
  }

    /**
     * title
     */
  titleName = 'basic_information'
    /**
     * button name
     */
  btnName = 'save'

  public type = ''
    /**
     * save the current sub profile data
     */
  public subData: any = {}
    /**
     * save the max length of input
     */
  public maxLengthCache = 128
    /**
     * save blacklist
     */
  public blackListCache = ''
    /**
     * save all the avatar url
     */
  public avatarList = []
    /**
     * the arrow on the left of the avatar list if visible or not
     */
  public leftArrowVisible = 'hidden'
    /**
     * the arrow on the right of the avatar list if visible or not
     */
  public rightArrowVisible = 'visible'
    /**
     * the width of a list of heads displayed on a page
     */
  public panel = 630
    /**
     * length of avatar list shift
     */
  public offsetNumber = 0
    /**
     * the number of pages in the avatar list
     */
  public pages: number
    /**
     * maximum width of avatar list
     */
  public maxWidth: number
    /**
     * shwo the delete button in the profile name input box
     */
  public showNameDelete = false
    /**
     * display name error indicating language
     */
  public showNameError = false
    /**
     * common profile name errors
     */
  public nameCommonError = false
    /**
     * invalid profile name
     */
  public invalidName = false
    /**
     * profile name error type
     */
  public nameErrorType = ''
    /**
     * shwo the delete button in the login id input box
     */
  public showIDDelete = false
    /**
     * display id error indicating language
     */
  public showIDError = false
    /**
     * common login id errors
     */
  public idCommonError = false
    /**
     * invalid login id
     */
  public invalidID = false
    /**
     * login id error type
     */
  public idErrorType = ''
    /**
     * blacklist
     */
  public blacklistTip = {}
    /**
     * all profile names
     */
  public names = []
    /**
     * all login ids
     */
  public loginIDs = []
    /**
     * profile information is modified or not
     */
  public changed = false
    /**
     * original avatar
     */
  public originalAvatar = ''
    /**
     * original name
     */
  public originalName = ''
    /**
     * original login id
     */
  public originalID = ''
    /**
     * original rating name
     */
  public originalRatingName = ''
    /**
     * original quata
     */
  public originalQuata = ''
    /**
     * list of level
     */
  public levelList = []
    /**
     * show level list or not
     */
  public isShowLevelList = false
    /**
     * list of credit limit
     */
  public limitList = []
    /**
     * show list of credit limit or not
     */
  public isShowLimitList = false
    /**
     * currency symbol
     */
  public currencySymbol
    /**
     * currency rate
     */
  public currencyRate

  constructor (
        private router: Router,
        private timerService: TimerService,
        private profileManageService: ProfileManageService
    ) {
    this.currencySymbol = JSON.parse(localStorage.getItem('CURRENCY_SYMBOL'))
    this.currencyRate = JSON.parse(localStorage.getItem('CURRENCY_RATE'))
  }
    /**
     * close the basic information dialog.
     */
  close () {
    this.closeBasic.emit()
  }
    /**
     * get the rules of input.
     */
  getInputRules () {
    this.blackListCache = session.get('COMMON_INPUT_BLACKLIST_CACHE')
    this.maxLengthCache = session.get('COMMON_INPUT_MAXLENGTH_CACHE')
    this.blacklistTip = { 'common': this.blackListCache }
  }
    /**
     * get all profile name and login ID already existed.
     */
  getAllNameAndID () {
    this.names = session.get('ALL_PROFILE_NAME')
    this.loginIDs = session.get('ALL_LOGIN_ID')
  }
    /**
     * get the original information.
     */
  getOriginalInfo (data) {
    this.originalAvatar = data['avatar']
    this.originalName = data['name']
    if (this.type === 'modify') {
      this.originalID = data['loginName']
    } else {
      this.originalID = ''
    }
    this.originalRatingName = data['ratingName']
    this.originalQuata = data['quota']
  }
    /**
     * get the default value of some settings when adding profile.
     */
  getDefaultValue () {
    this.subData['avatar'] = 'assets/img/default/ondemand_casts.png'
    this.subData['name'] = ''
    this.subData['loginName'] = ''
    let minLevel = _.min(this.levelList, item => {
      return Number(item['ID'])
    })
    this.subData['ratingName'] = minLevel['name'] || ''
    this.subData['ratingID'] = minLevel['ID'] || ''
    let unlimited = _.find(this.limitList, item => {
      return item === 'unlimited'
    })
    if (!_.isUndefined(unlimited)) {
      this.subData['quota'] = 'unlimited'
    } else {
      let maxLimit = _.max(this.limitList, item => {
        return Number(item.replace(/[^0-9]/ig, ''))
      })
      this.subData['quota'] = maxLimit
    }
    return Promise.resolve()
  }
    /**
     * check whether the profile setting is changed.
     */
  checkInfoChanged () {
    if (this.subData['avatar'] !== this.originalAvatar) {
      this.changed = true
      return
    }
    if (this.subData['name'] !== this.originalName) {
      this.changed = true
      return
    }
    if (this.subData['loginName'] !== this.originalID) {
      this.changed = true
      return
    }
    if (this.subData['ratingName'] !== this.originalRatingName) {
      this.changed = true
      return
    }
    if (this.subData['quota'] !== this.originalQuata) {
      this.changed = true
      return
    }
    this.changed = false
  }
    /**
     * select the avatar for current profile.
     */
  selectAvatar (avatar) {
    this.subData['avatar'] = avatar
    this.checkInfoChanged()
    this.hideLevelLimit()
  }
    /**
     * bind to mouseenter event, preview the avatar.
     */
  previewAvatar (avatar) {
    this.subData['avatar'] = avatar
  }
    /**
     * click the left arrow.
     */
  leftArrow () {
    let offsetWidth: string
    const MINSFFSETNUMBER = 0
    if (this.offsetNumber < MINSFFSETNUMBER) {
      this.offsetNumber += this.panel
      offsetWidth = this.offsetNumber + 'px'
      document.getElementById('avatar-panel').style.transform = 'translateX(' + offsetWidth + ')'
      document.getElementById('avatar-panel').style.transition = 'all 1s linear'
      this.isShowIcon(this.offsetNumber)
    }
  }
    /**
     * click the right arrow.
     */
  rightArrow () {
    let offsetWidth: string
    this.pages = Math.ceil(this.avatarList.length / 7)
    this.maxWidth = -(this.panel * (this.pages - 1))
    if (this.offsetNumber > this.maxWidth) {
      this.offsetNumber -= this.panel
      offsetWidth = this.offsetNumber + 'px'
      document.getElementById('avatar-panel').style.transform = 'translateX(' + offsetWidth + ')'
      document.getElementById('avatar-panel').style.transition = 'all 1s linear'
      this.isShowIcon(this.offsetNumber)
    }
  }
    /**
     * check whether the right arrow is visible.
     */
  checkArrowVisible (avatarList) {
    if (avatarList.length > 7) {
      this.rightArrowVisible = 'visible'
    } else {
      this.rightArrowVisible = 'hidden'
    }
  }
    /**
     * check whether the arrows are visible.
     */
  isShowIcon (offsetNumber) {
    if (offsetNumber === 0) {
      this.leftArrowVisible = 'hidden'
      this.rightArrowVisible = 'visible'
    } else if (offsetNumber === this.maxWidth) {
      this.leftArrowVisible = 'visible'
      this.rightArrowVisible = 'hidden'
    } else {
      this.leftArrowVisible = 'visible'
      this.rightArrowVisible = 'visible'
    }
  }
    /**
     * focus function of name input.
     */
  nameFocus (e) {
    if (e.target.value !== '') {
      this.showNameDelete = true
    }
    this.hideLevelLimit()
  }
    /**
     * blur function of name input.
     */
  nameBlur (e) {
    this.checkNameRule()
  }
    /**
     * keyup function of name input.
     */
  nameKeyup (e) {
    if (this.subData['name'] === '' || this.subData['name'] === undefined) {
      this.showNameDelete = false
    } else {
      this.showNameError = false
      this.showNameDelete = true
    }
    this.subData.name = e.target.value
    this.checkInfoChanged()
  }
    /**
     * check whehter the profile name input is legal according to the rules.
     */
  checkNameRule () {
    if (this.subData['name'] === '') {
      return
    }
    _.delay(() => {
      this.showNameDelete = false
      if (this.blackListCache !== '') {
        if (!(/^[A-Z|a-z|0-9]+$/.test(this.subData['name']))) {
            let numLetters = this.subData['name'].replace(/[A-Z|a-z|0-9]/ig, '')
            let numLettersArr = numLetters.split('')
            if (numLettersArr.length !== 0) {
              let hasLettersOfBlackList = false
              nameOuter: for (let i = 0; i < numLettersArr.length; i++) {
                for (let j = 0; j < this.blackListCache.length; j++) {
                  if (numLettersArr[i] === this.blackListCache.charAt(j)) {
                    hasLettersOfBlackList = true
                    break nameOuter
                  }
                }
              }
              if (hasLettersOfBlackList) {
                this.showNameError = true
                this.nameCommonError = false
                this.invalidName = true
                return
              }
            }
          }
      }
      let name = _.find(this.names, item => {
        return item === this.subData['name']
      })
      if (this.subData['name'] !== this.originalName && !_.isUndefined(name)) {
        this.showNameError = true
        this.nameCommonError = true
        this.invalidName = false
        this.nameErrorType = 'profile_name_repeat'
        return
      }
      this.showNameError = false
    }, 200)
  }
    /**
     * focus function of login ID input.
     */
  idFocus (e) {
    if (e.target.value !== '') {
      this.showIDDelete = true
    }
    this.hideLevelLimit()
  }
    /**
     * blur function of login ID input.
     */
  idBlur (e) {
    this.checkIDRule()
  }
    /**
     * keyup function of login ID input.
     */
  idKeyup (e) {
    if (this.subData['loginName'] === '' || this.subData['loginName'] === undefined) {
      this.showIDDelete = false
    } else {
      this.showIDError = false
      this.showIDDelete = true
    }
    this.checkInfoChanged()
  }
    /**
     * check whehter the login ID input is legal according to the rules.
     */
  checkIDRule () {
    if (this.subData['loginName'] === '') {
      return
    }
    _.delay(() => {
      this.showIDDelete = false
      if (this.blackListCache !== '') {
        if (!(/^[A-Z|a-z|0-9]+$/.test(this.subData['loginName']))) {
            let numLetters = this.subData['loginName'].replace(/[A-Z|a-z|0-9]/ig, '')
            let numLettersArr = numLetters.split('')
            if (numLettersArr.length !== 0) {
              let hasLettersOfBlackList = false
              nameOuter: for (let i = 0; i < numLettersArr.length; i++) {
                for (let j = 0; j < this.blackListCache.length; j++) {
                  if (numLettersArr[i] === this.blackListCache.charAt(j)) {
                    hasLettersOfBlackList = true
                    break nameOuter
                  }
                }
              }
              if (hasLettersOfBlackList) {
                this.showIDError = true
                this.idCommonError = false
                this.invalidID = true
                return
              }
            }
          }
      }
      let loginName = _.find(this.loginIDs, item => {
        return item === this.subData['loginName']
      })
      if (this.subData['loginName'] !== this.originalID && !_.isUndefined(loginName)) {
        this.showIDError = true
        this.idCommonError = true
        this.invalidID = false
        this.idErrorType = 'login_name_repeat'
        return
      }
      this.showIDError = false
    }, 200)
  }
    /**
     * clear the profile name.
     */
  deleteName () {
    this.subData['name'] = ''
    this.showNameDelete = false
    this.checkInfoChanged()
  }
    /**
     * clear the login ID.
     */
  deleteID () {
    this.subData['loginName'] = ''
    this.showIDDelete = false
    this.checkInfoChanged()
  }
    /**
     * toggle function of level list.
     */
  showLevelList () {
    if (this.isShowLevelList) {
      this.isShowLevelList = false
    } else {
      this.isShowLevelList = true
    }
    if (this.isShowLimitList === true) {
      this.isShowLimitList = false
    }
  }
    /**
     * mouseleave function of level list.
     */
  mouseOutLevelList () {
    this.isShowLevelList = false
  }
    /**
     * select one of the level list.
     */
  selectLevel (level) {
    this.subData['ratingName'] = level.name || level.code
    this.subData['ratingID'] = level.ID
    this.isShowLevelList = false
    this.checkInfoChanged()
  }
    /**
     * toggle function of limit list.
     */
  showLimitList () {
    if (this.isShowLimitList) {
      this.isShowLimitList = false
    } else {
      this.isShowLimitList = true
    }
    if (this.isShowLevelList === true) {
      this.isShowLevelList = false
    }
  }
    /**
     * mouseleave function of limit list.
     */
  mouseOutLimitList () {
    this.isShowLimitList = false
  }
    /**
     * select one of the limit list.
     */
  selectLimit (limit) {
    this.subData['quota'] = limit
    this.isShowLimitList = false
    this.checkInfoChanged()
  }
    /**
     * modify or set new basic information.
     */
  saveBasic () {
    _.delay(() => {
      if (this.type === 'modify') {
        this.modifyBasicInfo()
      } else {
        this.addBasicInfo()
      }
    }, 150)
  }
    /**
     * modify basic information.
     */
  modifyBasicInfo () {
    if (this.profileManageService.trim(this.subData['name']) === '') {
      this.subData['name'] = ''
      this.showNameError = true
      this.nameCommonError = true
      this.invalidName = false
      this.nameErrorType = 'profile_name_empty'
      return
    }
    if (this.profileManageService.trim(this.subData['loginName']) === '') {
      this.subData['loginName'] = ''
      this.showIDError = true
      this.idCommonError = true
      this.invalidID = false
      this.idErrorType = 'login_id_empty'
      return
    }
    if (this.showNameError || this.showIDError || !this.changed) {
      return
    }
    let req = {
      ID: this.subData['ID'],
      name: this.profileManageService.toOneSpace(this.subData['name']),
      loginName: this.profileManageService.toOneSpace(this.subData['loginName']),
      logoURL: this.subData['avatar'].slice((this.subData['avatar'].lastIndexOf('/') + 1),
          this.subData['avatar'].lastIndexOf('.')),
      ratingID: this.subData['ratingID'],
      ratingName: this.subData['ratingName']
    }
    this.profileManageService.modifyProfile({ profile: req }).then(resp => {
      if (resp.result.retCode === '0' || resp.result.retCode === '000000000') {
        EventService.emit('PROFILE_INFO_SAVE_SUCCESSFUL')
        this.changeSubInfo.emit(req)
        this.updateNames(req)
        this.close()
      }
    })
  }
    /**
     * set basic information of new profile.
     */
  addBasicInfo () {
    if (this.profileManageService.trim(this.subData['name']) === '') {
      this.subData['name'] = ''
      this.showNameError = true
      this.nameCommonError = true
      this.invalidName = false
      this.nameErrorType = 'profile_name_empty'
      return
    }
    if (this.showNameError || !this.changed) {
      return
    }
    let basicData = _.clone(this.subData)
    this.finishBasic.emit(basicData)
    this.close()
  }
    /**
     * update all profile name and login ID already existed.
     */
  updateNames (req) {
    if (req['name'] !== this.originalName) {
      let index = _.indexOf(this.names, this.originalName)
      this.names[index] = req['name']
      session.put('ALL_PROFILE_NAME', this.names)
    }
    if (req['loginName'] !== this.originalID) {
      let index = _.indexOf(this.loginIDs, this.originalID)
      this.loginIDs[index] = req['loginName']
      session.put('ALL_LOGIN_ID', this.loginIDs)
    }
  }

    /**
    * hidden the list of level limit.
    */
  hideLevelLimit () {
    if (this.isShowLevelList === true) {
      this.isShowLevelList = false
    }
    if (this.isShowLimitList === true) {
      this.isShowLimitList = false
    }
  }
}
