import { Injectable } from '@angular/core'
import * as _ from 'underscore'
import { EventService } from 'ng-epg-sdk/services'
import {
  cancelPVRByID, cancelPVRByPlaybillID, deletePVRByID, deletePVRByCondition, addPVR, addPeriodicPVR,
  queryPVR, queryPVRSpaceByID, queryPVRSpace, solvePVRConflict, updatePeriodicPVR, updatePVR
} from 'ng-epg-sdk/vsp'
import { config as EPGConfig } from '../../shared/services/config'
import { session } from 'src/app/shared/services/session'
import { DeletePVRByConditionRequest } from 'ng-epg-sdk/vsp/api'

@Injectable()
export class RecordDataService {
  private physicalChannels
  private filterNpvrChannel
  private filterCpvrChannel

  constructor () {
  }

  judgeRecord (recordCR, channel): any {
    let channels = _.filter(channel['physicalChannelsDynamicProperties'], function (physicalChannels) {
      let recordCRType = physicalChannels[recordCR]
      let btvCR = physicalChannels['btvCR']
      if (recordCRType && recordCRType['enable'] === '1' && recordCRType['isSubscribed'] === '1' &&
                recordCRType['isContentValid'] === '1' && recordCRType['isValid'] === '1' &&
                btvCR && btvCR['enable'] === '1' && btvCR['isSubscribed'] === '1' &&
                btvCR['isContentValid'] === '1' && btvCR['isValid'] === '1') {
        return recordCRType
      }
    })
    return channels
  }

    /**
     * get operation based on the recording state
     */
  isCPVROrNPVR (playbill, type) {
    if (playbill[type] === '1') {
      if (playbill.hasRecordingPVR && playbill.hasRecordingPVR !== '0') {
        let currentTime = parseInt(Date.now()['getTime'](), 10)
        if (currentTime > playbill['startTime'] && currentTime < playbill['endTime']) {
            return 'stop_recording'
          } else {
            return 'cancel_record'
          }
      }
      return 'livetv_record'
    }
  }

  getDynamicData (channel, userType) {
    let dynamicData = session.get(userType + '_Dynamic_Version_Data') || {}
    _.each(dynamicData['channelDynamaicProp'], (list1, index) => {
      if (list1['ID'] === channel['ID']) {
        channel['physicalChannelsDynamicProperties'] = list1['physicalChannelsDynamicProperties']
      }
    })
  }

    /**
     * judge the channel does support record or not.
     */
  channelIsRecord (channel, playbill): any {
    let userType = this.getUserType()
    this.getDynamicData(channel, userType)
    this.physicalChannels = channel['physicalChannelsDynamicProperties']
    let isNPVR = this.judgePVRMode(channel, 'NPVR')
    let isCPVR = this.judgePVRMode(channel, 'CPVR')
    let isCPVRAndNPVR = this.judgePVRMode(channel, 'CPVR&NPVR')
    if (isNPVR) {
      this.filterNpvrChannel = this.judgeRecord('npvrRecordCR', channel)

      if (this.filterNpvrChannel.length > 0) {
        return this.isCPVROrNPVR(playbill, 'isNPVR')
      }
      return false
    } else if (isCPVR) {
      this.filterCpvrChannel = this.judgeRecord('cpvrRecordCR', channel)

      if (this.filterCpvrChannel.length > 0) {
          return this.isCPVROrNPVR(playbill, 'isCPVR')
        }
      return false
    } else if (isCPVRAndNPVR) {
        this.filterNpvrChannel = this.judgeRecord('npvrRecordCR', channel)
        if ((playbill['isNPVR'] === '1') && this.filterNpvrChannel.length > 0) {
          return this.isCPVROrNPVR(playbill, 'isNPVR')
        }
        this.filterCpvrChannel = this.judgeRecord('cpvrRecordCR', channel)
        if ((playbill['isCPVR'] === '1') && this.filterCpvrChannel.length > 0) {
          return this.isCPVROrNPVR(playbill, 'isCPVR')
        }
        return false
      } else {
        return false
      }
  }

    /**
     * judge the PVR mode
     */
  judgePVRMode (channel, mode) {
    let isMode = channel['physicalChannelsDynamicProperties'] && EPGConfig['PVRMode'] === mode
    return isMode
  }

  getUserType () {
    let userType
    if (Cookies.getJSON('IS_PROFILE_LOGIN')) {
      userType = Cookies.getJSON('USER_ID')
    } else {
      userType = session.get('Guest') || 'Guest'
    }
    return userType
  }

    /**
     * cancel NPVR by ID
     */
  cancelPVRByID (playbillIDs: Array<string>, storageType?): any {
    return cancelPVRByID({
      singlePVRIDs: playbillIDs,
      storageType: storageType
    }, {}).then(function (resp) {
      EventService.emit('DELETE_RECORDED_SUCCESS')
      return Promise.resolve(resp)
    })
  }

    /**
     * cancel NPVR
     */
  cancelRecord (playbillIDs: Array<string>, storageType?): any {
    return cancelPVRByPlaybillID({
      playbillIDs: playbillIDs
    }, {}).then(function (resp) {
      EventService.emit('DELETE_RECORDED_SUCCESS')
      return Promise.resolve(resp)
    })
  }

    /**
     * delete record task according to ID
     */
  deletePVRByID (storageType, singlePVRIDs?: Array<string>): any {
    return deletePVRByID({
      singlePVRIDs: singlePVRIDs,
      storageType: storageType
    }, {}).then(function (resp) {
      EventService.emit('DELETE_RECORDED_BYID_SUCCESS')
      return Promise.resolve(resp)
    })
  }

    /**
     * delete the record task according to ID
     */
  deletePVRByIDWait (storageType, periodicPVRIDs?: Array<string>): any {
    return deletePVRByID({
      periodicPVRIDs: periodicPVRIDs,
      storageType: storageType
    }, {}).then(function (resp) {
      EventService.emit('DELETE_RECORDED_BYIDWAIT_SUCCESS')
      return Promise.resolve(resp)
    })
  }

    /**
    * delete the record task according to conditions
    */
  deletePVRByCondition (storageType, deleteConditions?: Array<string>, periodicPVRIDs?): any {
      // @FIXME break build with is not assignable types
    return deletePVRByCondition({
      deleteConditions: deleteConditions,
      storageType: storageType
    } as DeletePVRByConditionRequest, {}).then(function (resp) {
      EventService.emit('DELETE_RECORDED_BYCONDITION_SUCCESS')
      return Promise.resolve(resp)
    })
  }

    /**
     * add single or timeBase record
     */
  addPVR (data, type) {
    let singelPVR
    if (type === 'PlaybillBasedPVR') {
      singelPVR = {
        PVR: data
      }
    } else if (type === 'TimeBasedPVR') {
      singelPVR = {
          PVR: data
        }
    }

    return addPVR(singelPVR, {}).then(function (resp) {
      return Promise.resolve(resp)
    })
  }

    /**
     * add series or period record
     */
  addPeriodicPVR (data, type, conflict?, changePolicyType?: boolean, planID?: string) {
    let PeriodicPVR
    if (type === 'SeriesPVR') {
      if (conflict) {
        if (conflict === 'true') {
            PeriodicPVR = {
              PVR: data,
              playbillBasedPVRID: data['playbillID'],
              conflictCheckType: 1
            }
          } else {
            PeriodicPVR = {
              PVR: data,
              playbillBasedPVRID: data['playbillID'],
              excludePVRKeys: [conflict]
            }
          }
      } else {
        if (changePolicyType) {
            PeriodicPVR = {
              PVR: data,
              playbillBasedPVRID: planID
            }
          } else {
            PeriodicPVR = {
              PVR: data
            }
          }
      }
    } else if (type === 'PeriodPVR') {
      if (conflict) {
          if (conflict === 'true') {
            PeriodicPVR = {
              PVR: data,
              conflictCheckType: 1
            }
          } else {
            PeriodicPVR = {
              PVR: data,
              excludePVRKeys: [conflict]
            }
          }
        } else {
          PeriodicPVR = {
            PVR: data
          }
        }
    }

    return addPeriodicPVR(PeriodicPVR, {}).then(function (resp) {
      return Promise.resolve(resp)
    })
  }

    /**
     * query record task list
     */
  queryPVR (request, option?) {
    let options = option || {}
    if (options.params) {
      options.params.DEVICE = 'PC'
      options.params.DID = session.get('uuid_cookie')
    }
    return queryPVR(request, options).then(function (resp) {
      return Promise.resolve(resp)
    })
  }

    /**
     * modify series or period record
     */
  updatePVR (data, type) {
    let updatePVRReq
    if (type === 'PlaybillBasedPVR') {
      updatePVRReq = {
        PVR: data
      }
    } else if (type === 'TimeBasedPVR') {
      updatePVRReq = {
          PVR: data
        }
    }

    return updatePVR(updatePVRReq, {}).then(function (resp) {
      return Promise.resolve(resp)
    })
  }

    /**
     *  modify single or timeBase record
     */
  updatePeriodicPVR (data, type, conflict?) {
    let updatePeriodicPVRReq
    if (type === 'SeriesPVR') {
      if (conflict) {
        if (conflict === 'true') {
            updatePeriodicPVRReq = {
              PVR: data,
              playbillBasedPVRID: data['playbillID'],
              conflictCheckType: 1
            }
          } else {
            updatePeriodicPVRReq = {
              PVR: data,
              playbillBasedPVRID: data['playbillID'],
              excludePVRKeys: [conflict]
            }
          }
      } else {
        updatePeriodicPVRReq = {
            PVR: data,
            playbillBasedPVRID: data['playbillID']
          }
      }
    } else if (type === 'PeriodPVR') {
      if (conflict) {
          if (conflict === 'true') {
            updatePeriodicPVRReq = {
              PVR: data,
              conflictCheckType: 1
            }
          } else {
            updatePeriodicPVRReq = {
              PVR: data,
              excludePVRKeys: [conflict]
            }
          }
        } else {
          updatePeriodicPVRReq = {
            PVR: data
          }
        }
    }
    return updatePeriodicPVR(updatePeriodicPVRReq, {}).then(function (resp) {
      return Promise.resolve(resp)
    })
  }

    /**
     * querythe space(time length) of period record task
     */
  queryPVRSpaceByID (singlePVRIDs, storageType, option?) {
    let options = option || {}
    if (options.params) {
      options.params.DEVICE = 'PC'
      options.params.DID = session.get('uuid_cookie')
    }
    return queryPVRSpaceByID({
      singlePVRIDs: singlePVRIDs,
      storageType: storageType
    }, options).then(function (resp) {
      return Promise.resolve(resp)
    })
  }

    /**
     * querythe space of record task
     */
  queryPVRSpace (storageType?, option?) {
    let options = option || {}
    if (options.params) {
      options.params.DEVICE = 'PC'
      options.params.DID = session.get('uuid_cookie')
    }
    return queryPVRSpace({
      storageType: storageType
    }, options).then(function (resp) {
      return Promise.resolve(resp)
    })
  }

    /**
     * solve the cpvr conflict
     */
  solvePVRConflict (conflictGroups, groupIndex?) {
    return solvePVRConflict({
      conflictGroups: conflictGroups,
      groupIndex: groupIndex + ''
    }, {}).then(function (resp) {
      return Promise.resolve(resp)
    })
  }
}
