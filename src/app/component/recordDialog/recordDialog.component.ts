import { Component, OnInit, Input } from '@angular/core'
import { config as EPGConfig } from '../../shared/services/config'
import { EventService } from 'ng-epg-sdk/services'
import { RecordDataService } from './recordData.service'
import * as _ from 'underscore'
import { CommonService } from '../../core/common.service'
import { session } from 'src/app/shared/services/session'
@Component({
  selector: 'app-record-dialog',
  styleUrls: ['./recordDialog.component.scss'],
  templateUrl: './recordDialog.component.html'
})

export class RecordDialogComponent implements OnInit {
  public hasExtended = false
  public prePaddingArrow = 'down'
  public postPaddingArrow = 'down'
  public preTime = 0
  public endTime = 0
    /**
     * list of pre-padding value
     */
  public arrowPreList = []
    /**
     * list of post-padding value
     */
  public arrowPostList = []
  public recordType = false
  public singleRecordType = true
  public periodicRecordType = false
  public singleRecordFalse = false
  public periodicRecordFalse = false
  public isNow = true
  public canSetPrePadding = true
  public storageType = 'NPVR'
    /**
     * show quality list
     */
  public showQualityList = false
  public quality = 'HD'
    /**
     * list of quality
     */
  public qualityList = []
  public recordingMode
  public defaultQualityKey
  public curQualityObj = {
    'quality': '',
    'qualityKey': -1,
    'cpvrMediaID': ''
  }
  public selectCloud = true
  public showQullity = true
  public canNPVRRecord = true
  public canCPVRRecord = true
  public configData
  public recordData
  public supportAllCPVR = false
  public PVRMode = ''
  public staticData: any = {}
  public channelDetail: any = {}

  constructor (
        private recordDataService: RecordDataService,
        private commonService: CommonService) {
    let sessionConfig = session.get('CONFIG_DATAS')
    if (!_.isUndefined(sessionConfig)) {
      this.configData = sessionConfig
    } else {
      this.configData = EPGConfig
    }
    this.supportAllCPVR = this.configData['supportAllCPVR']
    this.PVRMode = EPGConfig['PVRMode']
    let userType
    if (Cookies.getJSON('IS_PROFILE_LOGIN')) {
      userType = Cookies.getJSON('USER_ID')
    } else {
      userType = session.get('Guest')
    }
    this.staticData = session.get(userType + '_Static_Version_Data')
    let dynamicData = session.get(userType + '_Dynamic_Version_Data')
    _.map(dynamicData['channelDynamaicProp'], (list1, index) => {
      _.each(this.staticData['channelDetails'], (list) => {
        if (list1['ID'] === list['ID']) {
            list['physicalChannelsDynamicProperties'] = list1['physicalChannelsDynamicProperties']
          }
      })
    })
  }

  @Input() set recConfig (data) {
    this.recordData = data
    this.isNow = this.recordData['isNow']
    this.channelDetail = _.find(this.staticData['channelDetails'], item => {
      return item['ID'] === this.recordData['channelDetail']['ID']
    })
    this.manAgeRecordMode()
    this.manAgeRecordType()
    this.quality = this.configData['default_reccfg_definition']
    switch (this.quality) {
      case 'SD':
        this.defaultQualityKey = 0
        break
      case 'HD':
        this.defaultQualityKey = 1
        break
      case '4K':
        this.defaultQualityKey = 2
        break
      default:
        this.defaultQualityKey = 1
    }
    if (this.configData['BeginOffset']) {
      this.preTime = parseInt(this.configData['BeginOffset'], 10) / 60
    }
    if (this.configData['EndOffset']) {
      this.endTime = parseInt(this.configData['EndOffset'], 10) / 60
    }
    this.arrowPreList = [0, 5, 10, 20, 30, 45]
    this.arrowPostList = [0, 5, 10, 20, 30, 45]
  }

  ngOnInit () {
    this.recordingMode = EPGConfig.PVRMode
    if (this.canNPVRRecord && this.canCPVRRecord) {
      if (this.configData['default_reccfg_pvr_type'] === 'NPVR' && this.configData['PVRMode'] !== 'CPVR') {
        this.selectCloud = true
        this.storageType = 'NPVR'
      } else {
        this.selectCloud = false
        this.showQullity = true
        this.recordType = true
        this.storageType = 'CPVR'
      }
    } else if (this.canNPVRRecord && !this.canCPVRRecord) {
      this.selectCloud = true
      this.showQullity = false
    } else if (!this.canNPVRRecord && this.canCPVRRecord) {
        this.selectCloud = false
        this.showQullity = true
        this.recordType = true
      }
    this.qualityList = []
    this.manAgeQuality()
  }

    /**
     * get the supported qualities.
     */
  manAgeQuality () {
    if (this.recordingMode !== 'NPVR') {
      let qualityList = []
      let qualityObj = {}
      let dynamicProperty = this.commonService.judgeRecordDynamic('cpvrRecordCR', this.channelDetail)
      let cPhysicalChannels = _.filter(this.channelDetail['physicalChannels'], item => {
        let findPhysical = _.find(dynamicProperty, itemDynamic => {
            return itemDynamic['ID'] === item['ID']
          })
        if (findPhysical) {
            return true
          }
      })
      if (cPhysicalChannels.length > 0) {
        _.map(cPhysicalChannels, item => {
            if (!_.isUndefined(item['definition'])) {
              if (Number(item['definition']) === 0) {
                qualityObj = {
                  'quality': 'SD',
                  'qualityKey': 0,
                  'cpvrMediaID': item['ID']
                }
              } else if (Number(item['definition']) === 1) {
                qualityObj = {
                  'quality': 'HD',
                  'qualityKey': 1,
                  'cpvrMediaID': item['ID']
                }
              } else if (Number(item['definition']) === 2) {
                qualityObj = {
                  'quality': '4K',
                  'qualityKey': 2,
                  'cpvrMediaID': item['ID']
                }
              }
            }
            if (qualityList.length > 0) {
              let existed = _.find(qualityList, function (data) {
                return data['quality'] === qualityObj['quality']
              })
              if (!existed) {
                qualityList.push(qualityObj)
              }
            } else {
              qualityList.push(qualityObj)
            }
          })
        qualityList = _.sortBy(qualityList, ql => {
            return ql.qualityKey
          })
        this.qualityList = qualityList
        if (this.recordingMode !== 'NPVR') {
            let findQualityObj = _.find(qualityList, item => {
              return item['qualityKey'] === this.defaultQualityKey
            })
            if (!_.isUndefined(findQualityObj)) {
              this.curQualityObj = findQualityObj
            } else {
              let higherQualityObj = _.find(qualityList, item => {
                return item['qualityKey'] > this.defaultQualityKey
              })
              if (!_.isUndefined(higherQualityObj)) {
                this.curQualityObj = higherQualityObj
              } else {
                this.curQualityObj = _.max(qualityList, item => {
                  return item['qualityKey']
                })
              }
            }
          }
      } else {
        this.showQullity = false
      }
    }
  }

    /**
     * manage the default values of single or series recording
     * other record status can click or not
     */
  manAgeRecordType () {
    if (this.recordData['isNPVR'] === '1' && !this.recordData['playbillSeries']['seriesID']) {
      this.singleRecordType = true
      this.periodicRecordType = false
      this.singleRecordFalse = true
      if (this.isNow) {
        this.canSetPrePadding = false
      } else {
        this.canSetPrePadding = true
      }
    }
    this.setCanRecord()
    if (this.recordData['playbillSeries']['seriesID']) {
      if (this.configData['default_reccfg_single_or_series'] === 'Single') {
        this.singleRecordType = true
        this.periodicRecordType = false
        if (this.isNow) {
            this.canSetPrePadding = false
          } else {
            this.canSetPrePadding = true
          }
      } else {
        this.singleRecordType = false
        this.periodicRecordType = true
        this.canSetPrePadding = true
      }
      this.singleRecordFalse = true
      this.periodicRecordFalse = true
    }
  }

  setCanRecord () {
    let filterNpvrChannel = this.commonService.judgeRecordDynamic('npvrRecordCR', this.channelDetail)
    let filterCpvrChannel = this.commonService.judgeRecordDynamic('cpvrRecordCR', this.channelDetail)
    if (filterNpvrChannel && (filterNpvrChannel.length > 0) && this.recordData['isNPVR'] === '1') {
      this.canNPVRRecord = true
    } else {
      this.canNPVRRecord = false
    }
    if (filterCpvrChannel && (filterCpvrChannel.length > 0) && this.recordData['isCPVR'] === '1') {
      this.canCPVRRecord = true
    } else {
      this.canCPVRRecord = false
    }
  }

    /**
     * select cloud or stb recording storage.
     */
  selectStorage (data) {
    if (this.canNPVRRecord && data === 'cloud') {
      this.storageType = 'NPVR'
      this.selectCloud = true
      this.showQullity = false
    } else if (this.canCPVRRecord && data === 'stb') {
      this.storageType = 'CPVR'
      this.selectCloud = false
      this.showQullity = true
    }
    this.hideListContent([1, 2, 3])
  }

    /**
     * select single or series recording type.
     */
  changeFocus (type) {
    if (type === 'singleRecordType') {
      if (this.singleRecordFalse) {
        this.singleRecordType = true
        this.periodicRecordType = false
      }
      if (this.isNow) {
        this.canSetPrePadding = false
        this.preTime = 0
      } else {
        this.canSetPrePadding = true
      }
    } else {
      if (this.periodicRecordFalse) {
        this.singleRecordType = false
        this.periodicRecordType = true
        this.canSetPrePadding = true
      }
      if (!this.supportAllCPVR) {
        this.storageType = 'NPVR'
        this.selectCloud = true
        this.showQullity = false
      }
    }
    this.hideListContent([1, 2, 3])
  }

    /**
     * show the Quality list
     */
  choiceQuality () {
    this.showQualityList = !this.showQualityList
    this.hideListContent([2, 3])
  }

    /**
     * hide the quality list when mouse leave
     */
  mouseOutQualityList () {
    this.showQualityList = false
  }

    /**
     * select the quality of record task
     */
  selectQuality (qualityObj) {
    this.curQualityObj = qualityObj
    this.showQualityList = false
  }

    /**
     * click other postion then hide the Quality list
     */
  hideListContent (key: Array<number>) {
    if (_.contains(key, 1)) {
      this.showQualityList = false
    }
    if (_.contains(key, 2)) {
      this.prePaddingArrow = 'down'
    }
    if (_.contains(key, 3)) {
      this.postPaddingArrow = 'down'
    }
  }

    /**
     *  manage NPVR and CPVR record
     */
  manAgeRecordMode () {
    if (this.configData['PVRMode'] === 'NPVR') {
      this.recordType = false
      this.storageType = 'NPVR'
    } else if (this.configData['PVRMode'] === 'CPVR') {
      this.selectCloud = false
      this.recordType = true
      this.storageType = 'CPVR'
    } else if (this.configData['PVRMode'] === 'MIXPVR' || this.configData['PVRMode'] === 'CPVR&NPVR') {
        this.recordType = true
      }
  }

    /**
     * add record:
     * single record
     * series record
     */
  addRecord (event?) {
    let data
    data = {
      beginOffset: (this.preTime * 60) + '' || '0',
      endOffset: (this.endTime * 60) + '' || '0',
      playbillID: this.recordData['ID'],
      storageType: this.storageType,
      channelID: this.recordData['channelId'] || this.recordData['channelDetail']['ID'],
      channelName: this.recordData['channelDetail']['name'] || this.channelDetail['name'],
      extensionFields: [
        {
          key: 'channelName',
          values: [this.recordData['channelDetail']['name'] || this.channelDetail['name']]
        }
      ]
    }
    if (this.storageType === 'CPVR') {
      data['cpvrMediaID'] = this.curQualityObj['cpvrMediaID']
    }
      // if it is single record
    if (this.singleRecordType) {
      data['policyType'] = 'PlaybillBased'
      this.recordDataService.addPVR(data, 'PlaybillBasedPVR').then(function (resp) {
        EventService.emit('CLOSED_RECORD_DIALOG')
        if (resp['isNPVRSpaceEnough'] && resp['isNPVRSpaceEnough'] === '0') {
            EventService.emit('RECORD_SPACE_NOT_ENOUGH', { type: 'successful', isNow: this.isNow })
          }
        let type = 'single'
        let playbillRecordData = ['single', data['playbillID']]
        EventService.emit('RECORDLIVE_ADD_SUCCESS', playbillRecordData)
        EventService.emit('RECORD_ADD_SUCCESS', { type: type, id: data['playbillID'] })
        EventService.emit('ADD_RECORDED_SUCCESS')
        EventService.emit('ADD_RECORD_FOR_MINIEPG', playbillRecordData)
      }.bind(this), function (resp) {
          EventService.emit('CLOSED_RECORD_DIALOG')
          let conflictData = {}
          if (resp['result']['retCode'] === '147020005') {
            EventService.emit('RECORD_SPACE_NOT_ENOUGH', { type: 'failed', isNow: this.isNow })
          } else if (resp.conflictGroups && resp.conflictGroups !== '') {
            conflictData = { resp: resp, data: data, type: 'PlaybillBasedPVR' }
            EventService.emit('RECORD_CONFLICT', conflictData)
          }
        }.bind(this))
    } else {
      data['seriesID'] = this.recordData['playbillSeries']['seriesID']
      data['policyType'] = 'Series'
      this.recordDataService.addPeriodicPVR(data, 'SeriesPVR', event).then(function (resp) {
        EventService.emit('CLOSED_RECORD_DIALOG')
        if (resp['isNPVRSpaceEnough'] && resp['isNPVRSpaceEnough'] === '0') {
            EventService.emit('RECORD_SPACE_NOT_ENOUGH', { type: 'successful', isNow: this.isNow })
          }
        let type = 'periodic'
        let playbillRecordData = ['periodic', data['playbillID']]
        EventService.emit('RECORDLIVE_ADD_SUCCESS', playbillRecordData)
        EventService.emit('RECORD_ADD_SUCCESS', { type: type, id: data['playbillID'] })
        EventService.emit('ADD_RECORDED_SUCCESS')
        EventService.emit('ADD_RECORD_FOR_MINIEPG', playbillRecordData)
      }.bind(this), function (resp) {
          EventService.emit('CLOSED_RECORD_DIALOG')
          let conflictData = {}
          if (resp['result']['retCode'] === '147020005') {
            EventService.emit('RECORD_SPACE_NOT_ENOUGH', { type: 'failed', isNow: this.isNow })
          } else if (resp.conflictGroups && resp.conflictGroups !== '') {
            conflictData = { resp: resp, data: data, type: 'SeriesPVR' }
            if (resp['conflictGroups'][0]['PVRList'][0]['policyType'] === 'PlaybillBased' ||
                        resp['conflictGroups'][0]['PVRList'][0]['policyType'] === 'TimeBased') {
              EventService.emit('MIXED_CONFLICT', conflictData)
            } else {
              EventService.emit('RECORD_CONFLICT', conflictData)
            }
          }
        }.bind(this))
    }
  }

    /**
     * show or hide the advance setting
     */
  extendedSet () {
    if (this.hasExtended) {
      this.hasExtended = false
    } else {
      this.hasExtended = true
      this.prePaddingArrow = 'down'
      this.postPaddingArrow = 'down'
    }
    this.hideListContent([1, 2, 3])
  }

    /**
     * deal with the arrow.
     */
  dealArrow (type) {
    if (this[type] === 'up') {
      this[type] = 'down'
    } else {
      if (type === 'prePaddingArrow') {
        if (this.canSetPrePadding) {
            this[type] = 'up'
            this.postPaddingArrow = 'down'
          }
      } else if (type === 'postPaddingArrow') {
          this[type] = 'up'
          this.prePaddingArrow = 'down'
        }
    }
    if (type === 'prePaddingArrow') {
      this.hideListContent([1, 3])
    } else {
      this.hideListContent([1, 2])
    }
  }

    /**
     * select value of pre-padding.
     */
  changePrePadding (data) {
    this.preTime = data
    this.prePaddingArrow = 'down'
  }

    /**
     * select value of post-padding.
     */
  changePostPadding (data) {
    this.endTime = data
    this.postPaddingArrow = 'down'
  }

    /**
     * close the dialog.
     */
  close () {
    EventService.emit('CLOSED_RECORD_DIALOG')
  }
}
