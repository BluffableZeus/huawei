import { Component, OnInit } from '@angular/core'
import { EventService } from 'ng-epg-sdk/services'
import { DialogRef, DialogParams } from 'ng-epg-ui/webtv-components/dialog'
import { RecordDataService } from '../recordDialog/recordData.service'

@Component({
  selector: 'app-stop-record-confirm',
  templateUrl: './stopRecord-confirm.component.html',
  styleUrls: ['./stopRecord-confirm.component.scss']
})

export class StopRecordConfirmComponent implements OnInit {
    /**
     * declare variables
     */
  private enableConfirm
  private storageType
    /**
     * name for modules that would be used in this module
     */
  constructor (
        private config: DialogParams,
        private dialog: DialogRef,
        private recordDataService: RecordDataService
    ) { }

    /**
     * initialization
     */
  ngOnInit () {
    let self = this
      // the event for monitoring the box closed
    EventService.removeAllListeners(['CLOSED_STOPRECORD_DIALOG'])
    EventService.on('CLOSED_STOPRECORD_DIALOG', () => {
      self.close()
    })
    if (self.config[1] === 'stop_recording') {
      this.enableConfirm = self.config[0]
      this.storageType = self.config[2]
    } else {
      this.enableConfirm = self.config
    }
  }

    /*
     *   method for close box
     */
  close () {
    if (this.dialog) {
      this.dialog.close()
    }
  }

    /*
     *   method for click the confirm buttom on box
     */
  confirm () {
    let self = this
    if (self.storageType) {
      self.recordDataService.cancelPVRByID([this.enableConfirm], self.storageType).then(function (resp) {
        EventService.emit('LIVETV_CENCELRECORD', {})
        EventService.emit('RECORD_CANCEL_SUCCESS')
        EventService.emit('RECORDLIVE_CANCEL_SUCCESS', this.enableConfirm)
        EventService.emit('REMOVE_RECORD_FOR_MINIEPG', this.enableConfirm)
        this.dialog.close()
      }.bind(this))
    } else {
      self.recordDataService.cancelRecord([this.enableConfirm]).then(function (resp) {
        EventService.emit('LIVETV_CENCELRECORD', {})
        EventService.emit('RECORD_CANCEL_SUCCESS')
        EventService.emit('RECORDLIVE_CANCEL_SUCCESS', this.enableConfirm)
        EventService.emit('REMOVE_RECORD_FOR_MINIEPG', this.enableConfirm)
        this.dialog.close()
      }.bind(this))
    }
  }
}
