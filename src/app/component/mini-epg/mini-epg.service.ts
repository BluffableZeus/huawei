import * as _ from 'underscore'
import { Injectable } from '@angular/core'
import { queryPlaybillList } from 'ng-epg-sdk/vsp'
import { session } from 'src/app/shared/services/session'
import { PictureService } from 'ng-epg-ui/services/picture.service'
import { TranslateService } from '@ngx-translate/core'
import { ChannelCacheService } from '../../shared/services/channel-cache.service'

@Injectable()
export class MiniEPGService {
    /**
     * declare variables
     */
  public favoriteList: any
  public allChannelLength: string
  public favChannelLength: string
  private programsList: Array<any>
    /**
     * named for modules that would be used in this module
     */
  constructor (
        private pictureService: PictureService,
        private translate: TranslateService,
        private channelCacheService: ChannelCacheService
    ) { }

    /**
     *  get the information of favorite channel when the channel playing is not favorite.
     */
  getPlaybillList (req, sceneID?: string) {
    let options = {
      params: {
        SID: sceneID,
        DEVICE: 'PC',
        DID: session.get('uuid_cookie')
      }
    }
    return queryPlaybillList(req, options).then(resp => {
      let channelDtails = this.channelCacheService.getStaticChannelData().channelDetails
      let dynamicChannelData = this.channelCacheService.getDynamicChannelData().channelDynamaicProp
        // get the dynamic data from dynamic cache
      _.each(resp.channelPlaybills, (item) => {
        item['channelInfos'] = _.find(channelDtails, (detail) => {
            return detail && detail['ID'] === item['playbillLites'][0].channelID
          })
        _.find(dynamicChannelData, (detail) => {
            if (detail && item['channelInfos'] && item['channelInfos']['ID'] === detail['ID']) {
              item['channelInfos']['isLocked'] = detail['isLocked']
              item['channelInfos']['favorite'] = detail['favorite']
              item['channelInfos']['channelNO'] = detail['channelNO']
              let nameSpace = session.get('ott_channel_name_space')
              let physicalChannelsDynamicProperty = _.find(detail['physicalChannelsDynamicProperties'], (property) => {
                return _.contains(property['channelNamespaces'], nameSpace)
              })
              item['channelInfos']['physicalChannelsDynamicProperty'] =
                            physicalChannelsDynamicProperty || detail['physicalChannelsDynamicProperties'][0]
            }
          })
      })
      return this.formatList(resp)
    })
  }

    /**
     *  judge the channel whether support to shift time.
     */
  formatispltvCR (channel) {
    let ispltvCR
    if (channel.physicalChannelsDynamicProperty && channel.physicalChannelsDynamicProperty.pltvCR) {
      let pltvCR = channel.physicalChannelsDynamicProperty.pltvCR
      let ispltvCRlength = pltvCR.length > 0
      let isEnable = pltvCR.enable === '1'
      let isContentValid = pltvCR.isContentValid === '1'
      if (ispltvCRlength && isEnable && isContentValid && Cookies.getJSON('IS_PROFILE_LOGIN')) {
        ispltvCR = true
      }
    }
    return ispltvCR
  }

  formatData (url, channel, playbill, index) {
      // judge the channel whether support to shift time.
    let ispltvCR = this.formatispltvCR(channel)
    let playbillName = playbill['name']
      // if the program is filled,the name should be 'no program'.
    if (playbill['isFillProgram'] && (playbill['isFillProgram'] === '1') && (playbillName === '')) {
      playbillName = 'not_program'
    }
    if (!channel['rating']) {
      channel['rating'] = {}
    }
    let data = {
      index: index,
      channelNo: channel['channelNO'],
      channelId: channel['ID'],
      logo: url,
      channelRatingID: channel['rating']['ID'],
        // judge the channel whether is locked.
      isChannelLocked: channel['isLocked'] === '1',
        // judge the channel whether is favorite.
      isFav: channel['favorite'] !== undefined,
      channelDetail: channel,
      playbillDetail: playbill,
      playBillName: playbillName,
      startTime: playbill['startTime'],
      isPlayBillLocked: Number(playbill['rating']['ID']) > Number(session.get('PROFILERATING_ID')) ||
            Number(channel['rating']['ID']) > Number(session.get('PROFILERATING_ID')),
      endTime: playbill['endTime'],
        // judge the program whether is recording,0 is that there is no connected record plan.
      isRecording: playbill['hasRecordingPVR'] && playbill['hasRecordingPVR'] !== '0' ? playbill['hasRecordingPVR'] : '',
      ispltvCR: ispltvCR,
        // judge the program whether is filled.
      isFillProgram: playbill['isFillProgram'] && playbill['isFillProgram'] === '1'
    }
    return data
  }

    /**
     *  encapsulate the data.
     */
  formatList (resp) {
    let list: Array<any>
    list = _.map(resp['channelPlaybills'], (item, index) => {
      let channel = item['channelDetail'] || item['channelInfos']
      let playbill = item['playbillLites'] && item['playbillLites'][0]
        // deal with picture URL by specific way.
      let url = channel['picture'] && channel['picture'].icons &&
                this.pictureService.convertToSizeUrl(channel['picture'].icons[0],
                  { minwidth: 42, minheight: 42, maxwidth: 42, maxheight: 42 })
      let data = this.formatData(url, channel, playbill, index)
        // if showing the locked icon and the record icon,judge the program's width in order to show the ellipsic correctly.
      this.setWidth(data)
        // show the left time of program.
      data['leftTime'] = this.translate.instant('playbill_format_time',
        {
          min: Math.ceil((new Date(Number(playbill['endTime'])).getTime() -
                        new Date(Number(Date.now()['getTime']())).getTime()) / 60000)
        })
        // the duration of the program played.
      let currentDuration = Date.now()['getTime']() - Number(playbill['startTime'])
        // the duration of the program.
      let playbilllTime = Number(playbill['endTime']) - Number(playbill['startTime'])
        // show the progress bar of program,if the program has been played,then show 100%.
      data['len'] = currentDuration > playbilllTime ? '100%' : Number(currentDuration / playbilllTime * 100) + '%'
        // show the season number and sitcom number of program.
      this.setSeasonNoAndSitcomNo(playbill, data)
      return data
    })
    return { list: list, total: resp['total'] }
  }

    /**
     * set width
     */
  setWidth (data) {
    if (data['isPlayBillLocked'] && data['isRecording']) {
      data['width'] = '169px'
        // if only show one of them.
    } else if (data['isPlayBillLocked'] || data['isRecording']) {
      data['width'] = '200px'
    } else {
        // if both of them are not display.
      data['width'] = '231px'
    }
  }

    /**
     * deal with the season data.
     */
  setSeasonNoAndSitcomNo (playbill, data) {
    if (playbill && playbill['playbillSeries'] && playbill['playbillSeries']['seasonNO']) {
      data['seasonNo'] = { num: this.formatNumber(playbill['playbillSeries']['seasonNO']) }
    }
    if (playbill && playbill['playbillSeries'] && playbill['playbillSeries']['sitcomNO']) {
      data['sitcomNo'] = { num: this.formatNumber(playbill['playbillSeries']['sitcomNO']) }
    }
  }

    /**
     * add the zero.
     */
  formatNumber (num) {
    let result = ''
      // num < 10, add zero.
    if (Number(num) < 10) {
      result = '0' + num
    } else {
      result = num + ''
    }
    return result
  }

    /**
     * refresh program.
     */
  refreshProgramsList (i, playbillName, playbill) {
      // refresh the program's basic information.
    this.programsList[i]['playBillName'] = playbillName
    this.programsList[i]['isFillProgram'] = playbill['isFillProgram'] && playbill['isFillProgram'] === '1'
    this.programsList[i]['startTime'] = playbill['startTime']
    this.programsList[i]['isPlayBillLocked'] = Number(playbill['rating']['ID']) > Number(session.get('PROFILERATING_ID')) ||
            Number(this.programsList[i]['channelRatingID']) > Number(session.get('PROFILERATING_ID'))
    this.programsList[i]['endTime'] = playbill['endTime']
    this.programsList[i]['isRecording'] = playbill['hasRecordingPVR'] && playbill['hasRecordingPVR'] !== '0' ? '1' : ''
    this.programsList[i]['playbillDetail'] = playbill
    if (this.programsList[i]['isPlayBillLocked'] && this.programsList[i]['isRecording']) {
      this.programsList[i]['width'] = '169px'
    } else if (this.programsList[i]['isPlayBillLocked'] || this.programsList[i]['isRecording']) {
      this.programsList[i]['width'] = '200px'
    } else {
      this.programsList[i]['width'] = '231px'
    }
    this.programsList[i]['leftTime'] = this.translate.instant('playbill_format_time',
      {
        min: Math.ceil((new Date(Number(playbill['endTime'])).getTime() -
                    new Date(Number(Date.now()['getTime']())).getTime()) / 60000)
      })
  }

    /**
     * refresh the data by auto.
     */
  refreshProgress (list, sceneID?: string) {
    let self = this
    this.programsList = list
    for (let i = 0; i < this.programsList.length; i++) {
      let length = this.programsList[i]['endTime'] - this.programsList[i]['startTime']
      let curLength = Date.now()['getTime']() - this.programsList[i]['startTime']
        // if this program has passed, call interface, get next program.
      if (curLength >= length) {
        this.getPlaybillContext({
            channelId: this.programsList[i].channelId,
            time: Number(this.programsList[i].startTime) + 1000
          }, sceneID).then(playbill => {
            let playbillName = playbill['name']
            // if the program is filled,the name should be 'no program'.
            if (playbill['isFillProgram'] && (playbill['isFillProgram'] === '1') && (playbillName === '')) {
              playbillName = 'not_program'
            }
            length = Number(playbill['endTime']) - Number(playbill['startTime'])
            curLength = Date.now()['getTime']() - Number(playbill['startTime'])
            // refresh program.
            this.refreshProgramsList(i, playbillName, playbill)
            let currentDuration = Date.now()['getTime']() - Number(playbill['startTime'])
            let playbilllTime = Number(playbill['endTime']) - Number(playbill['startTime'])
            this.programsList[i]['len'] = currentDuration > playbilllTime
              ? '100%' : Number(currentDuration / playbilllTime * 100) + '%'
            if (playbill['playbillSeries'] && playbill['playbillSeries']['seasonNO']) {
              this.programsList[i]['seasonNo'] = { num: this.formatNumber(playbill['playbillSeries']['seasonNO']) }
              this.programsList[i]['sitcomNo'] = { num: this.formatNumber(playbill['playbillSeries']['sitcomNO']) }
            }
            self.programsList[i]['percent'] = curLength >= length ? '100%' : Number(curLength) / Number(length) * 100 + '%'
            return self.programsList[i]
          })
      } else {
        this.programsList[i]['leftTime'] = this.translate.instant('playbill_format_time',
            {
              min: Math.ceil((new Date(Number(this.programsList[i]['endTime'])).getTime() -
                            new Date(Number(Date.now()['getTime']())).getTime()) / 60000)
            })
        this.programsList[i]['len'] = curLength >= length ? '100%' : Number(curLength) / Number(length) * 100 + '%'
      }
    }
    return this.programsList
  }

    /**
     * query the information of program when refresh the next program.
     */
  getPlaybillContext (info, sceneID?: string) {
    let options = {
      params: {
        SID: sceneID,
        DEVICE: 'PC',
        DID: session.get('uuid_cookie')
      }
    }
    return queryPlaybillList({
      needChannel: '0',
      queryPlaybill: {
        type: '1',
        count: '1',
        offset: '0',
        startTime: String(info['time']),
          // allow to filled.
        isFillProgram: '1'
      },
      queryChannel: {
        channelIDs: [info['channelId']],
        isReturnAllMedia: '1'
      }
    }, options).then(resp => {
      let playbill = resp['channelPlaybills'][0]['playbillLites'][0]
      return playbill
    })
  }

    /**
     * deal with the cache data and reset the favorite and lock
     * @param channellist
     * @param channelCache
     */
  dealFavandlock (channellist, channelCache) {
    _.each(channellist, item => {
      _.find(channelCache, detail => {
        if (item && detail && item['channelId'] === detail['ID']) {
            item['isChannelLocked'] = detail['isLocked'] === '1'
            item['isFav'] = detail['favorite'] !== undefined
          }
      })
    })
  }
}
