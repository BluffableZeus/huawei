import * as _ from 'underscore'
import { Component, OnInit, Input, OnDestroy } from '@angular/core'
import { Router } from '@angular/router'
import { MiniEPGService } from './mini-epg.service'
import { DirectionScroll } from 'ng-epg-ui/webtv-components/scroll/directionScroll.component'
import { EventService } from 'ng-epg-sdk/services'
import { session } from 'src/app/shared/services/session'
import { PictureService } from 'ng-epg-ui/services/picture.service'
import { PlaybillAppService } from '../playPopUpDialog/playbillApp.service'
import { TranslateService } from '@ngx-translate/core'
import { ChannelCacheService } from '../../shared/services/channel-cache.service'

@Component({
  selector: 'app-mini-epg',
  styleUrls: ['./mini-epg.component.scss'],
  templateUrl: './mini-epg.component.html',
  providers: [
    MiniEPGService, DirectionScroll, PictureService
  ]
})

export class MiniEPGComponent implements OnInit, OnDestroy {
  @Input() curPlayChannelID: string
    // declare variables.
    // channel list
  public channelList: Array<any> = []
    // favorite channel list
  public channelFavList: Array<any> = []
    // favorite channel list
  public favorites: Array<any> = []
    // all channel tag is select or not
  public isSelect = true
    // the timer of refresh the progress
  public miniEPGTimer: any
    // the scroll height
  public spanHight: number
    // the favorite scroll height
  public spanFavHight: Number
    // the number of favorite channel
  public favTotal: number
    // judge current play channel is favorite or not
  public iscurPlayFav = false
    // the index of current play channel in all channel list
  public curAllPlayIndex: number
    // the index of current play channel in favorite channel list
  public curFavPlayIndex: number
    // set the height of mini EPG list by the client height.
  public height = 0
    // the background is show or not
  public isShow = false
    // the timer of refresh next playbill
  public nextPlaybillTimer: any
    // when roll the scroll,control the times of calling interface
  public isLoadData = true
  public isLoadAllData = true
    // the flag of no data show
  public isShowNoData = false
    // the flag of the load image is showing or not
  public callInterfaceOver = false
    // the flag of clicking the favorite tag
  public firstFavClick = true
  public curPlayChannel: any
    // private constructor
  constructor (
        private router: Router,
        private miniEPGService: MiniEPGService,
        private directionScroll: DirectionScroll,
        private playbillAppService: PlaybillAppService,
        private translate: TranslateService,
        private channelCacheService: ChannelCacheService

    ) { }

  ngOnInit (channelID?) {
    let self = this
    if (channelID) {
      this.callInterfaceOver = false
      this.isShow = true
      this.isSelect = true
        // the channel that is played now.
      this.curPlayChannelID = channelID
        // set the height of mini EPG list by the client height.
      this.height = document.body.clientHeight - 67
      document.getElementById('miniEPGChannels').style.height = this.height + 'px'
      document.getElementById('miniEPGFavChannels').style.height = this.height + 'px'
      this.curPlayChannel = _.find(this.channelList, item => {
        return item['channelId'] === this.curPlayChannelID
      })
        // call interface to get the data when open the mini EPG firstly.
      if (!(this.channelList.length > 0 && this.curPlayChannel)) {
        this.getPlaybillByChannelContent(null)
      } else {
          // the play window is open,the data is not empty.
          // get the location of the channel that is playing now.
        this.curAllPlayIndex = this.curPlayChannel['index']
          // load the scroll bar.
        this.pluseOnScroll(null)
          // refresh the progress bar of the channel that is playing now.
        this.miniEPGTimer = setInterval(() => {
            this.miniEPGService.refreshProgress(this.channelList, 'queryplaybilllist4')
          }, 1000 * 60)
          // refresh the next progrom.
        this.refreshNextPlaybill()
      }
        // when slide the scroll bar.
      EventService.removeAllListeners(['SHOE_PROGRAM'])
      EventService.on('SHOE_PROGRAM', function () {
        let miniEpg = document.getElementsByClassName('miniEpg') && document.getElementsByClassName('miniEpg')[0]
        if (document.getElementById('miniEPG') && miniEpg['style'] && miniEpg['style']['display'] === 'block') {
            self.scrollAllData()
            self.scrollFavData()
          }
      })
        // when close the play window,the mini EPG data should be empty.
      EventService.removeAllListeners(['CLOSED_FULLSCREEN_MINIEPG'])
      EventService.on('CLOSED_FULLSCREEN_MINIEPG', () => {
          // clear the channel list,when exit playing
        this.channelList = []
        this.channelFavList = []
        this.favorites = []
          // reset the scroll
        let allScroll = document.getElementById('mini-epg-scroll-span')
        let favScroll = document.getElementById('mini-epg-fav-scroll-span')
        if (allScroll) {
            allScroll.style.height = '0px'
          }
        if (favScroll) {
            favScroll.style.height = '0px'
          }
          // clear all the timer
        clearInterval(this.miniEPGTimer)
        clearInterval(this.nextPlaybillTimer)
          // reset the load flag
        this.isLoadAllData = true
        this.isLoadData = true
      })

        // when add favorite.
      EventService.removeAllListeners(['MINI_ADD_FAVORITE'])
      EventService.on('MINI_ADD_FAVORITE', data => {
        let curPlaybill = _.find(data['playbillLites'], item => {
            return item[item['ID'] + 'isCurrent'] === true
          })
          // encapsulate the data coming from enternal.
        let resp = {}
        resp['channelPlaybills'] = []
        resp['channelPlaybills'][0] = {}
        resp['channelPlaybills'][0]['channelDetail'] = data['channelInfos'] || data['channelDetail']
        resp['channelPlaybills'][0]['playbillLites'] = []
        resp['channelPlaybills'][0]['playbillLites'][0] = curPlaybill || data['playbillLites'][0]
        let addFav = this.miniEPGService.formatList(resp)['list'][0]
          // traverse the channel list to find the channel that is favorited,judge whether is favorited.
        for (let i = 0; i < this.channelList.length; i++) {
            let channelNoValue = data['channelInfos'] && data['channelInfos']['channelNO'] || data['channelDetail']['channelNO']
            if (this.channelList[i]['channelNo'] === channelNoValue) {
              this.channelList[i]['isFav'] = true
              break
            }
          }
          // when favorte list is empty, it will reload
        if (this.channelFavList.length === 0) {
            return
          }
          // add the item into favorite list
        this.channelFavList.push(addFav)
          // sort by channel number
        this.channelFavList = _.sortBy(this.channelFavList, item => {
            return Number(item['channelNo'])
          })
        this.isShowNoData = false
      })
        // when delete the favorite.
      EventService.removeAllListeners(['MINI_REMOVE_FAVORITE'])
      EventService.on('MINI_REMOVE_FAVORITE', data => {
          // traverse the channel list to find the channel that is favorited,changed the channel favorte flag.
        for (let i = 0; i < this.channelList.length; i++) {
            let channelNoValue = self.getChannelNoValue(data)
            if (this.channelList[i]['channelNo'] === channelNoValue) {
              this.channelList[i]['isFav'] = false
              break
            }
          }
          // if the favorite list is empty,show the no data tip
        if (this.channelFavList.length === 0) {
            this.isShowNoData = true
            return
          }
          // delete the target channel from the favorite channel list
        for (let i = 0; i < this.channelFavList.length; i++) {
            let channelNoValue = self.getChannelNoValue(data)
            if (this.channelFavList[i]['channelNo'] === channelNoValue) {
              this.channelFavList.splice(i, 1)
              this.favorites.splice(i, 1)
              break
            }
          }
      })
        // when add the record.
      EventService.removeAllListeners(['ADD_RECORD_FOR_MINIEPG'])
      EventService.on('ADD_RECORD_FOR_MINIEPG', (data) => {
          // set the recording icon according to the record status and type
        for (let i = 0; i < this.channelList.length; i++) {
            // single recording
            if (data[0] === 'single' && data[1] === this.channelList[i]['playbillDetail']['ID']) {
              this.channelList[i]['isRecording'] = '1'
              break
              // periodic recording
            } else if (data[0] === 'periodic' && data[1] === this.channelList[i]['playbillDetail']['ID']) {
              this.channelList[i]['isRecording'] = '2'
              break
            }
          }
          // set the recording icon according to the record status and type
        for (let i = 0; i < this.channelFavList.length; i++) {
            // single recording
            if (data[0] === 'single' && data[1] === this.channelFavList[i]['playbillDetail']['ID']) {
              this.channelFavList[i]['isRecording'] = '1'
              break
              // periodic recording
            } else if (data[0] === 'periodic' && data[1] === this.channelFavList[i]['playbillDetail']['ID']) {
              this.channelFavList[i]['isRecording'] = '2'
              break
            }
          }
      })
        // when delete the record.
      EventService.removeAllListeners(['REMOVE_RECORD_FOR_MINIEPG'])
      EventService.on('REMOVE_RECORD_FOR_MINIEPG', (data) => {
        for (let i = 0; i < this.channelList.length; i++) {
            if (data === this.channelList[i]['playbillDetail']['ID']) {
              this.channelList[i]['isRecording'] = ''
              break
            }
          }
        for (let i = 0; i < this.channelFavList.length; i++) {
            if (data === this.channelFavList[i]['playbillDetail']['ID']) {
              this.channelFavList[i]['isRecording'] = ''
              break
            }
          }
      })
        // for first Fav
      EventService.removeAllListeners(['forFirstFav'])
      EventService.on('forFirstFav', () => {
        self.firstFavClick = true
      })
    }
  }
    /**
     * get data of the channelNo
     * @param data
     */
  getChannelNoValue (data) {
    let channelNo = data['channelInfos'] && data['channelInfos']['channelNO'] || data['channelDetail']['channelNO']
    return channelNo
  }
    /**
     * clear the timer when destory the page
     */
  ngOnDestroy () {
    if (this.nextPlaybillTimer) {
      clearTimeout(this.nextPlaybillTimer)
    }
    if (this.miniEPGTimer) {
      clearInterval(this.miniEPGTimer)
    }
  }
    /**
     * show the title when the channel logo does not exist
     */
  isChannelLogo (channelPlaybill) {
    channelPlaybill.showChannelLogo = true
  }

    /**
     * switch the title.
     */
  clickTitle (title) {
      // when change the tag,hidden the load icon
    document.querySelector('.mini-epg-load')['style']['display'] = 'none'
    if (title === 'all') {
      this.isSelect = true
      this.pluseOnScroll(null)
    } else {
      this.isSelect = false
        // profile login show the data
      if (Cookies.getJSON('IS_PROFILE_LOGIN')) {
          // if the favorite list is not empty and current play channel is favorite,show the data direct
        if (this.channelFavList.length > 0 && this.curPlayChannel && this.curPlayChannel['isFav']) {
            this.curFavPlayIndex = _.find(this.channelFavList, (item) => {
              return item['channelId'] === this.curPlayChannelID
            })['index']
            // load the scroll
            this.favScroll(null)
          } else {
            this.isShowNoData = false
            // if current play channel is favorite
            if (this.curPlayChannel && this.curPlayChannel['isFav']) {
              this.iscurPlayFav = true
              this.getFavPlayBill(null)
              // if current play channel is not favorite
            } else {
              this.iscurPlayFav = false
              if (!this.firstFavClick) {
                if (this.channelFavList.length === 0) {
                  this.isShowNoData = true
                }
                return
              }
              this.getFavPlaybillList()
              this.firstFavClick = false
            }
          }
          // guest login show no data
      } else {
        this.isShowNoData = true
      }
    }
  }

    /**
     * close mini EPG.
     */
  close () {
    this.isShow = false
    this.isLoadData = true
    this.isLoadAllData = true
    this.miniEPGService.favChannelLength = ''
    if (this.isShowNoData) {
      this.isShowNoData = false
    }
    _.delay(function () {
      EventService.emit('CLOSE_MINIEPG')
    }, 1000)
  }

    /**
     * when play.
     */
  play (channelPlaybill) {
    this.curPlayChannel = channelPlaybill
      // change channel
    if (this.curPlayChannelID !== channelPlaybill['channelId']) {
      this.playbillAppService.playProgram(channelPlaybill['playbillDetail'], channelPlaybill['channelDetail'])
      EventService.emit('HIDE_POPUP_DIALOG')
    }
      // reset current play channel info
    if (this.isSelect) {
      this.curAllPlayIndex = channelPlaybill['index']
      this.curPlayChannelID = channelPlaybill['channelId']
    } else {
      this.curFavPlayIndex = channelPlaybill['index']
      this.curPlayChannelID = channelPlaybill['channelId']
    }
    EventService.emit('OPEN_FULLSCREEN_LIVETVVIDEO', {})
  }

    /**
     * load the data when slide the scroll bar of all channels.
     */
  scrollAllData () {
    let self = this
    if (document.getElementById('miniEPGChannels') && this.isSelect && this.isShow) {
      let span = document.getElementById('mini-epg-scroll-span')
      let scrollHeight = Number(document.getElementById('mini-epg-scroll').style.height.replace('px', ''))
      let isUp = true
      let IDList
        // slide to down
      if (Number(span.style.top.replace('px', '')) + Number(span.style.height.replace('px', '')) > scrollHeight - 20) {
        self.spanHight = Number(span.style.top.replace('px', ''))
        isUp = false
          // get channel ID from the cache
        IDList = self.channelCacheService.getChannelIDBycurChannel(self.channelList[self.channelList.length - 1].channelId,
            { up: isUp }) || []
      } else if (Number(span.style.top.replace('px', '')) <= 20) {
          // slide to up.
          self.spanHight = Number(span.style.top.replace('px', ''))
          IDList = self.channelCacheService.getChannelIDBycurChannel(self.channelList[0].channelId, { up: isUp }) || []
        } else {
          return
        }
      self.getPlaybillByChannelContent({ up: isUp, IDList: IDList })
    }
  }

    /**
     * get the information of all channels.
     */
  getPlaybillByChannelContent (param) { // income parameter when slide the scroll bar.
    if (this.isLoadAllData) {
      let IDList
        // if scroll the roll,set IDs from parameter
      if (param) {
        IDList = param['IDList']
      } else {
        IDList = this.channelCacheService.getChannelIDBycurChannel(this.curPlayChannelID, null) || []
      }
        // if there are no channel IDs, do nothing
      if (IDList.length === 0) {
        return
      }
      this.isLoadAllData = false
      let reqData = {
        needChannel: '0',
        queryPlaybill: {
            type: '0',
            count: '1',
            offset: '0',
            startTime: Date.now()['getTime']() + '',
            isFillProgram: '1'
          },
        queryChannel: {
            channelIDs: IDList,
            isReturnAllMedia: '1'
          }
      }
        // load the picture delaying 1 min when it is the first time to enter the page.
      if (!param) {
        _.delay(() => {
            if (!this.callInterfaceOver) {
              document.querySelector('.mini-epg-load')['style']['display'] = 'block'
            }
          }, 1000)
      } else {
        document.querySelector('.mini-epg-load')['style']['display'] = 'block'
      }
        // get playbill list
      this.miniEPGService.getPlaybillList(reqData, 'queryplaybilllist4').then(resp => {
          // if scroll the roll up
        if (param && param['up']) {
            // delete the last one,because it repeats
            resp['list'].pop()
            this.channelList = resp['list'].concat(this.channelList)
            this.callInterfaceOver = true
            document.querySelector('.mini-epg-load')['style']['display'] = 'none'
            // delete the first one,because it repeats
          } else if (param && !param['up']) {
            resp['list'].shift()
            this.channelList = this.channelList.concat(resp['list'])
            this.callInterfaceOver = true
            document.querySelector('.mini-epg-load')['style']['display'] = 'none'
          } else {
            this.channelList = resp['list']
            this.callInterfaceOver = true
            document.querySelector('.mini-epg-load')['style']['display'] = 'none'
          }
        this.curPlayChannel = _.find(this.channelList, (item) => {
            return item.channelId === this.curPlayChannelID
          })
          // reset channel's index
        _.each(this.channelList, (channel, index) => {
            channel.index = index
          })
        this.curAllPlayIndex = this.curPlayChannel['index']
          // load the data when it is not sliding the scroll bar.
        if (!param) {
            this.pluseOnScroll(null)
          } else {
            this.pluseOnScroll(param)
          }
        this.isLoadAllData = true
          // set the timer of refresh the progress
        this.miniEPGTimer = setInterval(() => {
            this.miniEPGService.refreshProgress(this.channelList, 'queryplaybilllist4')
          }, 1000 * 60)
          // start timer to refresh next playbill
        this.refreshNextPlaybill()
      })
    }
  }

    /**
     * refresh the next program.
     */
  refreshNextPlaybill () {
    let allTimeList = []
    let favTimeList = []
      // encapsulate the information of filtering all channels.
    for (let i = 0; i < this.channelList.length; i++) {
      if (this.channelList[i]['channelId']) {
        allTimeList.push({
            index: i,
            time: this.channelList[i]['endTime'],
            channelId: this.channelList[i]['channelId'],
            isFav: this.channelList[i]['isFav']
          })
      }
    }
      // find the program that is end fastly from all channels.
    let allMinTime = _.min(allTimeList, item => {
      if (item && item['channelId']) {
        return Number(item['time'])
      }
    })
      // encapsulate the information of filtering all favorite channels.
    for (let i = 0; i < this.channelFavList.length; i++) {
      if (this.channelFavList[i]['channelId']) {
        favTimeList.push({
            index: i, // the channel's location in where the favorite channel lsit.
            time: this.channelFavList[i]['endTime'],
            channelId: this.channelFavList[i]['channelId'],
            isFav: this.channelFavList[i]['isFav']
          })
      }
    }
      // find the program that is end fastly from favorite channels.
    let favMinTime = _.min(favTimeList, item => {
      if (item && item['channelId']) {
        return Number(item['time'])
      }
    })
      // set the timer according to the time of the program that is end fastly,
      // refresh the next program immediately when the program end.
    clearTimeout(this.nextPlaybillTimer)
    this.nextPlaybillTimer = setTimeout(() => {
      this.miniEPGService.getPlaybillContext({
          channelId: allMinTime.channelId,
          time: Number(allMinTime['time']) + 1000
        }, 'queryplaybilllist4').then(playbill => {
          // encapsulte the data of all channels.
          this.allRefreshNext(playbill, allMinTime)
          // if the channel refreshed is favorite,and belong to the favorite channel list,
          // refresh the favorite channel information.
          if (allMinTime['channelId'] === favMinTime['channelId'] && this.channelFavList.length > 0) {
            this.favRefreshNext(playbill, favMinTime)
          }
          // after the end of refreshing,start the mechanism of refresh again.
          this.refreshNextPlaybill()
        })
    }, allMinTime['time'] - Date.now()['getTime']())
      // if the channel refreshed is favorite,and belong to the favorite channel list,
      // refresh the favorite channel information.
    if (allMinTime['channelId'] !== favMinTime['channelId'] && this.channelFavList.length > 0) {
      clearTimeout(this.nextPlaybillTimer)
      this.nextPlaybillTimer = setTimeout(() => {
        this.miniEPGService.getPlaybillContext({
            channelId: favMinTime.channelId,
            time: Number(allMinTime['time']) + 1000
          }, 'queryplaybilllist4').then(playbill => {
            this.favRefreshNext(playbill, favMinTime)
            this.refreshNextPlaybill()
          })
      }, favMinTime['time'] - Date.now()['getTime']())
    }
  }
    /**
     * refresh channel list
     * @param playbillName
     * @param i
     * @param playbill
     */
  refreshChannelList (playbillName, i, playbill) {
    this.channelList[i]['playBillName'] = playbillName
    this.channelList[i]['startTime'] = playbill['startTime']
    this.channelList[i]['isPlayBillLocked'] = Number(playbill['rating']['ID']) > Number(session.get('PROFILERATING_ID')) ||
            Number(this.channelList[i]['channelRatingID']) > Number(session.get('PROFILERATING_ID'))
    this.channelList[i]['endTime'] = playbill['endTime']
    this.channelList[i]['isRecording'] = playbill['PVRTaskID'] !== undefined && playbill['PVRTaskID'] !== ''
    this.channelList[i]['playbillDetail'] = playbill
    this.channelList[i]['isFillProgram'] = playbill['isFillProgram'] && playbill['isFillProgram'] === '1'
    if (this.channelList[i]['isPlayBillLocked'] && this.channelList[i]['isRecording']) {
      this.channelList[i]['width'] = '169px'
    } else if (this.channelList[i]['isPlayBillLocked'] || this.channelList[i]['isRecording']) {
      this.channelList[i]['width'] = '200px'
    } else {
      this.channelList[i]['width'] = '231px'
    }
    this.channelList[i]['leftTime'] = this.translate.instant('playbill_format_time', {
      min: Math.ceil((new Date(Number(playbill['endTime'])).getTime() -
                new Date(Number(Date.now()['getTime']())).getTime()) / 60000)
    })
  }
    /**
     * refresh nexttime
     * @param playbill
     * @param allMinTime
     */
  allRefreshNext (playbill, allMinTime) {
    let length = Number(playbill['endTime']) - Number(playbill['startTime'])
    let curLength = Date.now()['getTime']() - Number(playbill['startTime'])
    let i = allMinTime['index']
    let playbillName = playbill['name']
    if (playbill['isFillProgram'] && playbill['isFillProgram'] === '1' && playbillName === '') {
      playbillName = 'not_program'
    }
    this.refreshChannelList(playbillName, i, playbill)
    let currentDuration = Date.now()['getTime']() - Number(playbill['startTime'])
    let playbilllTime = Number(playbill['endTime']) - Number(playbill['startTime'])
    this.channelList[i]['len'] = currentDuration > playbilllTime ? '100%' : Number(currentDuration / playbilllTime * 100) + '%'
    if (playbill['playbillSeries'] && playbill['playbillSeries']['seasonNO'] && playbill['playbillSeries']['sitcomNO']) {
      this.channelList[i]['seasonNo'] = { num: this.miniEPGService.formatNumber(playbill['playbillSeries']['seasonNO']) }
      this.channelList[i]['sitcomNo'] = { num: this.miniEPGService.formatNumber(playbill['playbillSeries']['sitcomNO']) }
    }
    this.channelList[i]['percent'] = curLength >= length ? '100%' : Number(curLength) / Number(length) * 100 + '%'
  }

  favRefreshNext (playbill, favMinTime) {
    let i = favMinTime['index']
    let playbillName = playbill['name']
    if (playbill['isFillProgram'] && playbill['isFillProgram'] === '1' && playbillName === '') {
      playbillName = 'not_program'
    }
    this.channelFavList[i]['playBillName'] = playbill['name']
    this.channelFavList[i]['isFillProgram'] = playbill['isFillProgram'] && playbill['isFillProgram'] === '1'
    this.channelFavList[i]['startTime'] = playbill['startTime']
    this.channelFavList[i]['isPlayBillLocked'] = Number(playbill['rating']['ID']) > Number(session.get('PROFILERATING_ID')) ||
            Number(this.channelFavList[i]['channelRatingID']) > Number(session.get('PROFILERATING_ID'))
    this.channelFavList[i]['endTime'] = playbill['endTime']
    this.channelFavList[i]['isRecording'] = playbill['PVRTaskID'] !== undefined && playbill['PVRTaskID'] !== ''
    this.channelFavList[i]['playbillDetail'] = playbill
    this.setWidth(i)
    this.channelFavList[i]['leftTime'] = this.translate.instant('playbill_format_time', {
      min: Math.ceil((new Date(Number(playbill['endTime'])).getTime() -
                new Date(Number(Date.now()['getTime']())).getTime()) / 60000)
    })
    let currentDuration = Date.now()['getTime']() - Number(playbill['startTime'])
    let playbilllTime = Number(playbill['endTime']) - Number(playbill['startTime'])
    this.channelFavList[i]['len'] = currentDuration > playbilllTime ? '100%' : Number(currentDuration / playbilllTime * 100) + '%'
    this.getSeasonAndSitcomNo(playbill, i)
    let length = Number(playbill['endTime']) - Number(playbill['startTime'])
    let curLength = Date.now()['getTime']() - Number(playbill['startTime'])
    this.channelFavList[i]['percent'] = curLength >= length ? '100%' : Number(curLength) / Number(length) * 100 + '%'
  }
    /**
     * set width
     * @param i
     */
  setWidth (i) {
    if (this.channelFavList[i]['isPlayBillLocked'] && this.channelFavList[i]['isRecording']) {
      this.channelFavList[i]['width'] = '169px'
    } else if (this.channelFavList[i]['isPlayBillLocked'] || this.channelFavList[i]['isRecording']) {
      this.channelFavList[i]['width'] = '200px'
    } else {
      this.channelFavList[i]['width'] = '231px'
    }
  }
    /**
     * get season and sitcomNo
     * @param playbill
     * @param i
     */
  getSeasonAndSitcomNo (playbill, i) {
    if (playbill['playbillSeries'] && playbill['playbillSeries']['seasonNO']) {
      this.channelFavList[i]['seasonNo'] = { num: this.miniEPGService.formatNumber(playbill['playbillSeries']['seasonNO']) }
      this.channelFavList[i]['sitcomNo'] = { num: this.miniEPGService.formatNumber(playbill['playbillSeries']['sitcomNO']) }
    }
  }

    /**
     * load the data when slide the scroll bar of favorite channel.
     */
  scrollFavData () {
    let self = this
    if (document.getElementById('miniEPGFavChannels') && !this.isSelect && this.iscurPlayFav) {
      let span = document.getElementById('mini-epg-fav-scroll-span')
      let scrollHeight = Number(document.getElementById('mini-epg-fav-scroll').style.height.replace('px', ''))
      let IDList
      let isUp = true
        // slide to down
      if (Number(span.style.top.replace('px', '')) + Number(span.style.height.replace('px', '')) > scrollHeight - 20) {
        self.spanFavHight = Number(span.style.top.replace('px', ''))
        isUp = false
        IDList = self.channelCacheService.getChannelIDBycurChannel(self.channelFavList[self.channelFavList.length - 1].channelId,
            { up: isUp }, true) || []
      } else if (Number(span.style.top.replace('px', '')) <= 20) {
          // slide to up.
          self.spanFavHight = Number(span.style.top.replace('px', ''))
          self.spanHight = Number(span.style.top.replace('px', ''))
          IDList = self.channelCacheService.getChannelIDBycurChannel(self.channelFavList[0].channelId, { up: isUp }, true) || []
        } else {
          return
        }
      self.getFavPlayBill({ up: isUp, IDList: IDList })
    } else if (document.getElementById('miniEPGFavChannels') && !this.isSelect && !this.iscurPlayFav) {
      if (this.favTotal > this.channelFavList.length) {
          self.getFavPlaybillList()
        }
    }
  }

    /**
     *  get the information of favorite channel when the channel playing is favorite.
     */
  getFavPlayBill (param) {
    if (this.isLoadData) {
      let IDList
        // if scroll the roll,set IDs from parameter
      if (param) {
        IDList = param['IDList'] || []
      } else {
        IDList = this.channelCacheService.getChannelIDBycurChannel(this.curPlayChannelID, null, true) || []
          // if the ID list is empty, show no data tips
        if (IDList.length === 0) {
            this.isShowNoData = true
          } else {
            this.isShowNoData = false
          }
      }
      if (IDList.length === 0) {
        return
      }
        // when call the interface,set the flag of load data
      this.isLoadData = false
      let reqData = {
        needChannel: '0',
        queryPlaybill: {
            type: '0',
            count: '1',
            offset: '0',
            startTime: Date.now()['getTime']() + '',
            isFillProgram: '1'
          },
        queryChannel: {
            channelIDs: IDList,
            isReturnAllMedia: '1'
          }
      }
      document.querySelector('.mini-epg-load')['style']['display'] = 'block'
      this.miniEPGService.getPlaybillList(reqData, 'queryplaybilllist5').then(resp => {
        if (param && param['up']) {
            // delete the last one,because it repeats
            resp['list'].pop()
            this.channelFavList = resp['list'].concat(this.channelFavList)
          } else if (param && !param['up']) {
            // delete the first one,because it repeats
            resp['list'].shift()
            this.channelFavList = this.channelFavList.concat(resp['list'])
          } else {
            this.channelFavList = resp['list']
          }
          // reset the index of favorite channel
        for (let i = 0; i < this.channelFavList.length; i++) {
            this.channelFavList[i]['index'] = i
            if (this.channelFavList[i]['channelId'] === this.curPlayChannelID) {
              this.curFavPlayIndex = i
            }
          }
          // load the scroll according to rolling or not
        if (!param) {
            this.favScroll(null)
          } else {
            this.favScroll(param)
          }
        this.isLoadData = true
          // start the timer of refreshing the progress
        this.miniEPGTimer = setInterval(() => {
            this.miniEPGService.refreshProgress(this.channelFavList, 'queryplaybilllist5')
          }, 1000 * 60)
          // start the timer to refresh the next playbill
        this.refreshNextPlaybill()
        document.querySelector('.mini-epg-load')['style']['display'] = 'none'
      })
    }
  }

    /**
     * get the information of favorite channel when the channel playing is not favorite.
     */
  getFavPlaybillList () {
    let subjectID = 'livetv_favorite'
      // get favorite channel id from cache
    let IDList = this.channelCacheService.getStroageChannelIDListBySubjectID(subjectID, true) || []
    let offset = this.channelFavList.length || '0'
    IDList = IDList.slice(Number(offset), Number(offset) + 15)
      // if the id list is not empty,load the data
    if (this.isLoadData && IDList.length > 0) {
      this.isLoadData = false
      document.querySelector('.mini-epg-load')['style']['display'] = 'block'
      this.miniEPGService.getPlaybillList({
        needChannel: '0',
        queryPlaybill: {
            type: '0',
            count: '1',
            offset: '0',
            startTime: Date.now()['getTime']() + '',
            isFillProgram: '1'
          },
        queryChannel: {
            channelIDs: IDList,
            channelFilter: {
              isFavorite: '1'
            },
            isReturnAllMedia: '1'
          }
      }, 'queryplaybilllist5').then(resp => {
          this.favTotal = Number(resp['list'].length)
          if (this.favTotal === 0) {
            this.isShowNoData = true
          } else {
            this.isShowNoData = false
          }
          this.channelFavList = this.channelFavList.concat(resp['list'])
          _.each(this.channelFavList, (item, index) => {
            if (item['channelId'] === this.curPlayChannelID) {
              this.curFavPlayIndex = index
            }
          })
          this.isLoadData = true
          this.favScroll(null)
          this.refreshNextPlaybill()
          document.querySelector('.mini-epg-load')['style']['display'] = 'none'
        })
    } else {
        // if the favorite channel list is empty show no data
      if (this.channelFavList.length === 0) {
        this.isShowNoData = true
      }
    }
  }

    /**
     * the event of scroll bar.
     */
  pluseOnScroll (param) {
    if ((this.channelList.length * 71 - 1) < (document.body.clientHeight - 67)) {
      return
    }
    let oConter, oUl, oScroll, oSpan
    let oBox = document.getElementById('miniEPG')
    oConter = document.getElementById('miniEPGChannels')
    oUl = document.getElementById('miniEPGUl')
    if (!param) {
      oUl.style.top = (Number(this.curAllPlayIndex + 1) - 1) * -71 + 'px'
    }
    oScroll = document.getElementById('mini-epg-scroll')
    oScroll.style.height = this.height - 10 + 'px'
    oSpan = oScroll.getElementsByTagName('span')[0]
    if (!param) {
      this.spanHight = ((this.height - 10) - (this.height - 10) *
                (oConter.offsetHeight / (71 * this.channelList.length))) * Number(this.curAllPlayIndex) / this.channelList.length
      oSpan.style.top = this.spanHight + 'px'
    }
    if (!param && this.channelList[this.channelList.length - 1]['index'] - this.curAllPlayIndex < 14) {
      oUl.style.top = this.height - this.channelList.length * 71 - 1 + 'px'
      oSpan.style.top = Math.floor(this.height - 10 - (this.height - 10) * this.height / (71 * this.channelList.length - 1)) + 'px'
    }
    this.directionScroll.setScroll(oBox, oConter, oUl, oScroll, oSpan)
  }

  favScroll (param) {
    if ((this.channelFavList.length * 71 - 1) < (document.body.clientHeight - 67)) {
      return
    }
    let oConter, oUl, oScroll, oSpan
    let oBox = document.getElementById('miniEPG')
    oConter = document.getElementById('miniEPGFavChannels')
    oUl = document.getElementById('miniEPGFavUl')
    if (!param) {
      oUl.style.top = (Number(this.curFavPlayIndex + 1) - 1) * -71 + 'px'
    }
    oScroll = document.getElementById('mini-epg-fav-scroll')
    oScroll.style.height = this.height - 10 + 'px'
    oSpan = oScroll.getElementsByTagName('span')[0]
    if (!param) {
      this.spanFavHight = ((this.height - 10) - (this.height - 10) *
                (oConter.offsetHeight / (71 * this.channelFavList.length))) * Number(this.curFavPlayIndex) / this.channelFavList.length
      oSpan.style.top = this.spanFavHight + 'px'
    }
    if (!param && this.channelFavList[this.channelFavList.length - 1]['index'] - this.curFavPlayIndex < 15) {
      oUl.style.top = this.height - this.channelFavList.length * 71 - 1 + 'px'
      oSpan.style.top = Math.floor(this.height - 10 - (this.height - 10) *
                this.height / (71 * this.channelFavList.length - 1)) + 'px'
    }
    this.directionScroll.setScroll(oBox, oConter, oUl, oScroll, oSpan)
  }
    /**
     * refresh the favorite channel list,if the favorite verson changed
     */
  favChanged () {
      // if the page is close
    if (!this.curPlayChannelID) {
      return
    }
      // when the dynamaic data got,emit this event
    EventService.removeAllListeners(['FAV_LOCK_VERSION_CHANGED'])
    EventService.on('FAV_LOCK_VERSION_CHANGED', () => {
      let dynamicChannelData = this.channelCacheService.getDynamicChannelData().channelDynamaicProp
        // refresh all channel list
      if (this.channelList.length > 0) {
        this.miniEPGService.dealFavandlock(this.channelList, dynamicChannelData)
      }
        // judge the current play channel is favorite or not
      let iscurFav = _.find(dynamicChannelData, item => {
        return this.curPlayChannelID === item['ID'] && item['favorite'] !== undefined
      })
        // reload favorite channel data
      if (this.channelFavList.length > 0) {
        this.channelFavList = []
      }
      if (iscurFav) {
        this.getFavPlayBill(null)
      } else {
        this.getFavPlaybillList()
      }
    })
  }
    /**
     * set hide position
     */
  hidePosition () {
    return {
      'top': (document.body.clientHeight - 128) / 2 + 'px'
    }
  }

  allListStyle () {
    return {
      'height': document.body.clientHeight - 67 + 'px',
      'display': this.isSelect ? 'inline-block' : 'none'
    }
  }
    /**
     * if favourite,set style
     */
  favListStyle () {
    return {
      'height': document.body.clientHeight - 67 + 'px',
      'display': !this.isSelect ? 'inline-block' : 'none'
    }
  }
    /**
     * if no data ,set style
     */
  noDataStyle () {
    return {
      'margin-top': (document.body.clientHeight - 67 - 120) / 2 + 'px',
      'display': this.isShowNoData ? 'inline-block' : 'none'
    }
  }
    /**
     * if no find,set style
     * @param this
     */
  nofind (this) {
    this.src = 'assets/img/default/minidetail_channel_poster.png'
    this.onerror = null
  }
}
