import * as _ from 'underscore'
import { Component, Input, OnInit } from '@angular/core'
import { EventService } from 'ng-epg-sdk/services'
import { DirectionScroll } from 'ng-epg-ui/webtv-components/scroll/directionScroll.component'
import { session } from 'src/app/shared/services/session'

@Component({
  selector: 'app-episodes-list',
  templateUrl: './episodes-list.component.html',
  styleUrls: ['./episodes-list.component.scss']
})

export class EpisodesListComponent implements OnInit {
    // Judge whether the length of episodes is zero.
  public episodesLengthIsZero = false
    // Judge whether show the episodes list.
  public isEpisodesShow = false
    // Save the data that come from parent component.
  public receiveData: Object = {}
    // Episodes will divide into several groups. There is 65 episodes in every page.
  public pages: Array<any> = []
    // Save the current episode data of the page selected.
  public episodes: Array<any> = []
    // Save total episodes data.
  public episodesTotal: Array<any> = []
    // Save how many pages there are.
  public page = 0
    // Save the episodes amount of every page.
  public pagesNum = 65
    // Save the titles of all groups, such as ‘01 - 65’，‘66 - 130’.
  public pageStr: Array<any> = []
    // Save the index of the current page.
  public pageIndex = 0
    // Count the number of sliding, [n] stand for slide to the right n times, [-n] stand for slide to the left n times.
  public count = 0
    // The width of the translation after sliding one page.
  public panel = 310
    // Save the value of offset.
  public offsetNumber = 0
    // Judge whether show the left arrow, 'visible' means show, 'hidden' means not.
  public leftIconVisible = 'hidden'
    // Judge whether show the right arrow, 'visible' means show, 'hidden' means not.
  public rightIconVisible = 'hidden'
    // Judge whether the screen height is less then 900
  public belowScreenHeight = false
    // Save the value of the height of the screen.
  public screenHeight = 0
    // Judge whether the episode of one page is beyond the height of the screen.
  public reachLimit = false
    // Save the value of episode NO.
  public sitcomNO = '1'
    // Save the value of index of the current episode.
  public epiIndex
    // There is 3 groups in one page.
  public groupNum = 3
    // Save the value of index of the current group.
  public curGroupIndex = 0
    // Save the value of the clientHeight of body.
  public lastListHeight = document.body.clientHeight
    // Save the data about playing.
  public playInfo: Object = {
    'epi': '',
    'epiIndex': '',
    'tra': ''
  }
    // Judge whether episode or trailer is selected.
  public episodeFocus = true
    // Save the max number of episode.
  public maxSitcomNO = 0
    /**
     * name for modules that would be used in this module
     */
  constructor (
        private directionScroll: DirectionScroll
    ) { }
    /**
     *  if data changed, deal with the data
     */
  @Input() set data (data) {
    this.receiveData = data
    this.isEpisodesShow = data.isEpisodesShow
    if (data['hasEpisode'] === false) {
      this.episodesLengthIsZero = true
    } else {
      this.sitcomNO = data.sitcomNO
      let curEpiData = this.getEpiBySitcomNO(data['detail'], this.sitcomNO)
      this.epiIndex = curEpiData['epiIndex']
      this.episodeFocus = data.episodeFocus
      this.setDealAll(this.receiveData)
      this.judgeScreenWidth()
    }
  }

  ngOnInit () {

  }

  nextVODHandler (index) {
    let self = this
    self.epiIndex = index
    let curEpiData = this.getEpiInfoByIndex(self.receiveData['detail'], index)
    self.sitcomNO = curEpiData['sitcomNO']
    self.translateEpiGroups()
  }

  getEpiBySitcomNO (details, sitcomNO) {
    let curEpiData = _.find(details['episodes'], item => {
      return item['sitcomNO'] === sitcomNO
    })
    return curEpiData
  }

  getEpiInfoByIndex (vodDetails, index) {
    let curEpiData = _.find(vodDetails['episodes'], item => {
      return item['epiIndex'] === index
    }) || {}
    return curEpiData
  }
    /**
     * judge whether the screen height is less then 900px
     */
  judgeScreenWidth () {
    let self = this
    self.screenHeight = document.body.clientHeight
    if (self.screenHeight >= 900) {
      self.belowScreenHeight = false
    } else {
      self.belowScreenHeight = true
    }
  }

  checkListHeight () {
    if (this.dom('episodes-fullScreen')) {
      if (Math.abs(document.body.clientHeight - this.lastListHeight) > 20) {
        this.lastListHeight = document.body.clientHeight
        this.judgeScreenWidth()
        this.judgeEpisodeLengthLimit(this.screenHeight, this.episodes)
        if (this.reachLimit) {
            _.delay(() => {
              this.pluseOnScroll()
            }, 100)
          } else {
            if (this.dom('episodes') && this.dom('episodes')['style']) {
              this.dom('episodes')['style']['top'] = '0px'
            }
          }
      }
    }
  }
    /**
     * translate epi groups
     */
  translateEpiGroups () {
    let self = this
    if (Number(this.sitcomNO) > self.pagesNum * self.groupNum) {
      this.findGroupIndex()
      let translateTimes = Math.ceil((this.curGroupIndex + 1) / self.groupNum) - 1
      _.delay(() => {
        if (self.dom('pages-lists') && self.dom('pages-lists')['style']) {
            self.dom('pages-lists')['style'].transform = 'translate(' + -(self.panel * translateTimes) + 'px)'
            self.dom('pages-lists')['style'].transition = 'none'
          }
        self.offsetNumber = -(self.panel * translateTimes)
        self.isShowIcon(self.offsetNumber)
        self.changePages(self.curGroupIndex)
      }, 100)
    } else {
      _.delay(() => {
        if (self.dom('pages-lists') && self.dom('pages-lists')['style']) {
            self.dom('pages-lists')['style'].transform = 'translate(0px)'
            self.dom('pages-lists')['style'].transition = 'none'
          }
      }, 100)
      self.offsetNumber = 0
      this.findGroupIndex()
      self.changePages(self.curGroupIndex)
    }
  }
    /**
     * find the index of the current episode group.
     */
  findGroupIndex () {
    outer: for (let i = 0; i < this.pages.length; i++) {
      for (let j = 0; j < this.pages[i].length; j++) {
        if (this.pages[i][j]['sitcomNO'] === this.sitcomNO) {
            this.curGroupIndex = i
            break outer
          }
      }
    }
  }
    /**
     * in different resolution, set the episode limit according to the screen height, judge whether show the scroll bar
     */
  judgeEpisodeLengthLimit (screenHeight, episodes) {
    let self = this
    let reachLimit
    if (episodes && episodes.length) {
      if (screenHeight <= 600) {
        if (episodes.length > 40) {
            reachLimit = true
          } else {
            reachLimit = false
          }
      } else if (screenHeight <= 768) {
          if (episodes.length > 50) {
            reachLimit = true
          } else {
            reachLimit = false
          }
        } else if (screenHeight <= 899) {
          if (episodes.length > 55) {
            reachLimit = true
          } else {
            reachLimit = false
          }
        }
    }
    if (reachLimit === false) {
      _.delay(function () {
        if (self.dom('scrollList-episodes') && self.dom('scrollList-episodes')['style']) {
            self.dom('scrollList-episodes')['style']['display'] = 'none'
          }
      }, 100)
    } else {
      _.delay(function () {
        if (self.dom('scrollList-episodes') && self.dom('scrollList-episodes')['style']) {
            self.dom('scrollList-episodes')['style']['display'] = 'block'
          }
      }, 100)
    }
    self.reachLimit = reachLimit
  }
    /**
     * wipe data and reload data
     */
  setDealAll (data) {
    this.episodes = []
    this.pages = []
    this.dealEpisodes(data)
  }
    /**
     * deal with the [episodes] data in [detail]
     */
  dealEpisodes (data) {
    let self = this
    if (data.detail && data.detail.episodes && data.detail.episodes.length > 0) {
      _.max(data.detail.episodes, item => {
        return +item['sitcomNO']
      })
      this.maxSitcomNO = data.detail.episodes.length
      let allEpisodes = data.detail.episodes
      this.episodesTotal = allEpisodes
      this.pages = this.getPages(allEpisodes, this.pagesNum)
      this.pageStr = this.getPageStr(this.pages, this.pagesNum)
      self.initialIconVisibility()
      self.page = Math.ceil(self.pages.length / self.groupNum)
      self.curGroupIndex = Math.ceil(Number(this.sitcomNO) / self.pagesNum) - 1
      self.changePages(self.curGroupIndex)
      self.episodesLengthIsZero = false
      self.translateEpiGroups()
    } else if (data.detail && data.detail.episodes && data.detail.episodes.length === 0) {
      self.episodesLengthIsZero = true
    }
  }
    /**
     * get the pages data.
     */
  getPages (episodes, pagesNum) {
    let pages = []
    let pagesResult = []
    let episodeGroupNum = Math.ceil(this.maxSitcomNO / pagesNum)
    for (let i = 0; i < episodeGroupNum; i++) {
      pages[i] = []
    }
    for (let i = 0; i < episodes.length; i++) {
      let episodeNum = Number(episodes[i]['epiIndex']) + 1
      let atIndex = Math.ceil(episodeNum / pagesNum) - 1
      pages[atIndex].push(episodes[i])
    }
    for (let i = 0; i < episodeGroupNum; i++) {
      if (pages[i].length !== 0) {
        pagesResult.push(pages[i])
      }
    }
    return pagesResult
  }
    /**
     * get the string array of grouping episode
     */
  getPageStr (pages, pagesNum) {
    let pageStr = []
    for (let i = 0; i < pages.length; i++) {
      let str = ''
      let pageNO = Math.ceil(Number(pages[i][0]['epiIndex']) / pagesNum) + 1
      if (pageNO === 1) {
        str = '01 - ' + pagesNum * pageNO
      } else {
        str = pagesNum * (pageNO - 1) + 1 + ' - ' + pagesNum * pageNO
      }
      if (i === pages.length - 1) {
        if (pages[i].length === 1) {
            str = pages[i][0]['epiIndex'] + 1
          } else {
            let length = pages[i].length
            str = pagesNum * (pageNO - 1) + 1 + ' - ' + (pages[i][length - 1]['epiIndex'] + 1)
          }
      }
      pageStr.push(str)
    }
    return pageStr
  }
    /**
     * if the length of data is 1, we should add a zero in the front of the word.
     */
  addZero (data: string) {
    if (data.length === 1) {
      return '0' + data
    } else {
      return data
    }
  }
    /**
     * initialize the show and hidden attribute of slide button
     */
  initialIconVisibility () {
    if (this.pages.length <= this.groupNum) {
      this.leftIconVisible = 'hidden'
      this.rightIconVisible = 'hidden'
    } else {
      this.rightIconVisible = 'visible'
    }
  }
    /**
     * switch grouping episode, and refresh episode list on the bottom
     */
  changePages (index) {
    this.pageIndex = index
    this.episodes = this.pages[index]
    this.judgeEpisodeLengthLimit(this.screenHeight, this.episodes)
    if (this.reachLimit) {
      _.delay(() => {
        this.pluseOnScroll()
      }, 100)
    }
    if (this.dom('episodes') && this.dom('episodes')['style']) {
      this.dom('episodes')['style']['top'] = '0px'
    }
    if ((this.belowScreenHeight && this.reachLimit) === true) {
      this.scrollToTop()
    }
  }
    /**
     * show scroll bar when the resolution less than 1440
     */
  pluseOnScroll () {
    let oBox, oConter, oUl, oScroll, oSpan
    oBox = this.dom('episodes-wrap')
    oConter = this.dom('episodes-contents')
    oUl = this.dom('episodes')
    oScroll = this.dom('scroll')
    if (oScroll) {
      oSpan = oScroll.getElementsByTagName('span')[0]
    }
    this.directionScroll.setScroll(oBox, oConter, oUl, oScroll, oSpan)
  }
    /**
     * the content area return the top when scroll bar roll to top
     */
  scrollToTop () {
    let self = this
    _.delay(function () {
      if (self.episodesTotal.length <= 65) {
        if (self.dom('scrollList-episodes') && self.dom('scrollList-episodes')['style']) {
            self.dom('scrollList-episodes')['style']['top'] = '-20px'
          }
      }
      if (self.dom('scroll-span') && self.dom('scroll-span')['style']) {
        self.dom('scroll-span')['style']['top'] = '0px'
      }
    }, 100)
  }
    /**
     * pack up episode list
     */
  hideEpisodesList () {
    let self = this
    if (self.episodesLengthIsZero !== true) {
      if (self.dom('episodes') && self.dom('episodes')['style']) {
        self.dom('episodes')['style']['top'] = '0px'
      }
      if ((self.belowScreenHeight && self.reachLimit) === true) {
        self.scrollToTop()
      }
    }
    self.isEpisodesShow = false
    EventService.emit('HIDEEPISODES', 'true')
  }
    /**
     * the method for translate to right
     */
  onClickLeft () {
    this.count--
    let offsetWidth: string
    this.offsetNumber += this.panel
    offsetWidth = this.offsetNumber + 'px'
    this.dom('pages-lists').style.transform = 'translate(' + offsetWidth + ')'
    this.dom('pages-lists').style.transition = 'all 1s linear'
    this.isShowIcon(this.offsetNumber)
  }
    /**
     * the method for translate to right
     */
  onClickRight () {
    this.count++
    let offsetWidth: string
    this.offsetNumber -= this.panel
    offsetWidth = this.offsetNumber + 'px'
    this.dom('pages-lists').style.transform = 'translate(' + offsetWidth + ')'
    this.dom('pages-lists').style.transition = 'all 1s linear'
    this.isShowIcon(this.offsetNumber)
  }
    /**
     * judge whether show slide icon
     */
  isShowIcon (offsetNumber: number) {
    if (offsetNumber === 0) {
      if (this.pages.length <= this.groupNum) {
        this.leftIconVisible = 'hidden'
        this.rightIconVisible = 'hidden'
      } else {
        this.leftIconVisible = 'hidden'
        this.rightIconVisible = 'visible'
      }
    } else if (offsetNumber === -(this.panel * (this.page - 1))) {
      this.leftIconVisible = 'visible'
      this.rightIconVisible = 'hidden'
    } else {
      this.leftIconVisible = 'visible'
      this.rightIconVisible = 'visible'
    }
  }
    /**
     * click and play the episode.
     */
  clickEpi (episode) {
    if (this.epiIndex === episode['epiIndex'] && this.episodeFocus) {
      return
    }
    session.remove('clipFlag')
    this.sitcomNO = episode['sitcomNO']
    this.epiIndex = episode['epiIndex']
    this.playInfo['epi'] = episode['sitcomNO']
    this.playInfo['epiIndex'] = episode['epiIndex']
    if (this.epiIndex === this.episodesTotal.length - 1) {
      EventService.emit('NEXT_ICON_DISAPPEAR', '1')
    } else {
      EventService.emit('NEXT_ICON_DISAPPEAR', '0')
    }
    session.put('playInfo', this.playInfo)
    EventService.emit('playEpi')
    if (this.episodeFocus === false) {
      this.episodeFocus = true
    }
  }
    /**
     * get DOM element
     */
  dom (divName: string) {
    return document.getElementById(divName)
  }
}
