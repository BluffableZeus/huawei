import * as _ from 'underscore'
import { Component, ViewChild, OnInit, OnDestroy } from '@angular/core'
import { EventService } from 'ng-epg-sdk/services'
import { VODSubscriberDialogComponent } from './subscriber/subscriber.component'
import { VODSubscrEmpDialogComponent } from './subscrEmp/subscrEmp.component'
import { VODFullScreenComponent } from './fullScreenPassInput/fullScreenPass-input-dialog.component'
import { MiniScreenComponent } from './miniScreen/miniScreen.component'
import { session } from 'src/app/shared/services/session'
import { WeakMessageDialogComponent } from '../../weakMessageDialog/weakMessageDialog.component'
import { SubscriptionsService } from '../../../my-tv/subscriptions/subscriptions.service'
import { CommonService } from 'src/app/core/common.service'
import { TranslateService } from '@ngx-translate/core'

@Component({
  selector: 'app-vod-popup-dialog',
  templateUrl: './vodPopUpDialog.component.html',
  styleUrls: ['./vodPopUpDialog.component.scss']
})

export class VodPopUpDialogComponent implements OnInit, OnDestroy {
    // declare varibles
  public dialogDoms = ['vodSubscrEmp', 'vodSubDialog', 'vodPwdDialog', 'vodAuthorizeEmp']
  public livetvVideoWidth = window.screen.width + 'px'
  public livetvVideoHeight = window.screen.height + 'px'
  public miniDetailList = {}
  public empSubData = {}
  public vodAuthorizeData = {}
  public haveClicks = false
  public vodVideoWidth = window.screen.width + 'px'
  public vodVideoHeight = window.screen.height + 'px'
  public isFullScreen = false
  public judgeWeakTip = false
  public judgeMiniScreen = false
  public isJudgeSubscrEmp = false
  public judgeSubscriber = false
  public judgeSubscrEmp = false
  public judgeFullScreenPwd = false
  public judgeAuthorizeEmp = false
  public judgeLogInBg = true
  public judgeReplayBg = false
  public judgePlayFailBg = false
  public getWeakTip

  public currentProduct = null
  public showMethodDialog = false
  public paymentMethods = null

  @ViewChild(VODSubscriberDialogComponent) subscriberDialog: VODSubscriberDialogComponent
  @ViewChild(VODSubscrEmpDialogComponent) subscrEmpDialog: VODSubscrEmpDialogComponent
  @ViewChild(VODFullScreenComponent) fullScreenPassDialog: VODFullScreenComponent
  @ViewChild(MiniScreenComponent) miniScreen: MiniScreenComponent

    /**
     * name for modules that would be used in this module
     */
  constructor (
        private subscriptionsService: SubscriptionsService,
        private commonService: CommonService,
        private translate: TranslateService
    ) {
    EventService.removeAllListeners(['HIDE_VOD_POPUP_DIALOG'])
    EventService.on('HIDE_VOD_POPUP_DIALOG', () => {
      this.judgeFullScreenPwd = false
      this.judgeSubscriber = false
      this.judgeSubscrEmp = false
      this.judgeAuthorizeEmp = false
      this.judgeMiniScreen = false
      this.judgeReplayBg = false
    })
    EventService.removeAllListeners(['EXIT_VOD_FULLSCREEN'])
    EventService.on('EXIT_VOD_FULLSCREEN', () => {
      this.exitvodFullcom()
      if (this.judgeLogInBg) {
        document.getElementById('vodLogInBg').style.position = 'relative'
        document.getElementById('vodLogInBg').style.width = '993px'
        document.getElementById('vodLogInBg').style.height = '560px'
        if (window.innerWidth > 1440) {
            document.querySelector('app-login-background')['style'].position = 'relative'
            document.querySelector('app-login-background')['style'].width = '993px'
            document.querySelector('app-login-background')['style'].height = '560px'
            document.getElementById('logInBgContent').style.top = '210px'
          } else if (window.innerWidth <= 1440) {
            document.querySelector('app-login-background')['style'].position = 'relative'
            document.querySelector('app-login-background')['style'].width = '670px'
            document.querySelector('app-login-background')['style'].height = '376px'
            document.getElementById('logInBgContent').style.top = '100px'
          }
      }
      this.isFullScreen = false
      this.setScreenStyles(this.dialogDoms, 'fixed', 'rgba(0, 0, 0, 0.7)')
      document.getElementById('vodPopUpDialog').style.zIndex = ''
    })
    EventService.removeAllListeners(['LOGININTRUE'])
    EventService.on('LOGININTRUE', state => {
      this.judgeLogInBg = state
    })
    EventService.removeAllListeners(['UNLOGIN_ENTER_FULLSCREEN_CHANGE'])
    EventService.on('UNLOGIN_ENTER_FULLSCREEN_CHANGE', () => {
      this.isFullScreen = true
      if (this.judgeLogInBg) {
        document.getElementById('vodLogInBg').style.position = 'fixed'
        document.getElementById('vodLogInBg').style.width = '100%'
        document.getElementById('vodLogInBg').style.height = '100%'
        if (window.innerWidth > 1440 && window.innerWidth <= 1920) {
            document.getElementById('logInBgContent').style.top = '350px'
          } else if (window.innerWidth <= 1440) {
            document.getElementById('logInBgContent').style.top = '250px'
          }
      }
    })
    EventService.on('changeState', () => {
      this.judgeReplayBg = false
    })
    EventService.on('ScreenSize', () => {
      if (this.isFullScreen) {

      } else if (window.innerWidth > 1440 && window.innerWidth <= 1920) {
          if (document.querySelector('app-login-background')) {
          }
        } else if (window.innerWidth <= 1440) {
          if (document.querySelector('app-login-background')) {
          }
        }
    })
    EventService.removeAllListeners(['enterFullScreens'])
    EventService.on('enterFullScreens', playEnd => {
      if (playEnd === true) {
        this.judgeReplayBg = true
      }
    })
  }

    /**
     * initialization
     * judge login background
     */
  ngOnInit () {
    if (Cookies.getJSON('IS_PROFILE_LOGIN')) {
      this.judgeLogInBg = false
    }
  }

    /**
     * when exit the component, clear all the timer and events
     */
  ngOnDestroy () {
    if (this.getWeakTip) {
      clearTimeout(this.getWeakTip)
    }
  }

    /**
     * exit vod full screen
     */
  exitvodFullcom () {
    if (this.judgeSubscrEmp === true || this.judgeAuthorizeEmp === true ||
            this.judgeSubscriber === true || this.judgeFullScreenPwd === true ||
            this.judgeReplayBg === true || this.judgePlayFailBg === true) {
      this.exitVodFullScreen()
    } else {
      this.judgeHideDialog('')
    }
  }

    /**
     * exit Vod FullScreen
     */
  exitVodFullScreen () {
    if (this.judgePlayFailBg) {
      this.judgeHideDialog('PlayFailBg')
    } else if (this.judgeSubscrEmp || this.judgeSubscriber) {
      this.judgeHideDialog('MiniScreen')
      this.miniScreen.getType('Subscribe')
    } else if (this.judgeFullScreenPwd && this.empSubData['judgeSub']) {
        this.judgeHideDialog('MiniScreen')
        this.miniScreen.getType('Subscribe')
      } else if (this.judgeReplayBg) {
        this.judgeHideDialog('ReplayBg')
      } else {
        this.judgeHideDialog('MiniScreen')
        this.miniScreen.getType('Authorizate')
      }
  }

    /**
     * Full screen and non full screen pop-up box inconsistent
     */
  setStyles () {
    if (this.isFullScreen) {
      this.vodVideoWidth = window.screen.width + 'px'
      this.vodVideoHeight = window.screen.height + 'px'
      this.setScreenStyles(this.dialogDoms, 'relative', 'none')
    } else {
      this.vodVideoWidth = document.body.clientWidth + 'px'
      this.vodVideoHeight = document.body.clientHeight + 'px'
    }
  }

    /**
     * set screen styles
     */
  setScreenStyles (dialogDoms, position, background) {
    for (let i = 0; i < dialogDoms.length; i++) {
      let dialogDomStyle
      if (document.getElementById(dialogDoms[i])) {
        dialogDomStyle = document.getElementById(dialogDoms[i]).style
      }
      if (dialogDomStyle) {
        dialogDomStyle.position = position
        dialogDomStyle.background = background
      }
    }
  }

  firFoxDelayPop () {
    let ua = navigator.userAgent.toLowerCase()
    let isFirefox = !!ua.match(/firefox\/([\d.]+)/)
    if (isFirefox) {
      _.delay(() => {
        this.judgeHideDialog('SubscrEmp')
      }, 200)
    } else {
      this.judgeHideDialog('SubscrEmp')
    }
  }

  setPopUpSubscrEmp (subData) {
    this.empSubData = subData
    this.isFullScreen = subData['isFullScreen']
    this.isJudgeSubscrEmp = true
    if (this.isFullScreen) {
      this.firFoxDelayPop()
    } else {
      subData['type'] = 'Subscribe'
      this.judgeHideDialog('MiniScreen')
      if (session.pop('IS_FIRSTLOGIN_CAN_DROP')) {
        EventService.emit('IS_FIRSTLOGIN_CAN_DROP_EVENT')
      }
      this.miniScreen.setMessage(subData)
    }
  }

    /**
     * change mini screen
     */
  changeMiniScreen (data) {
    if (data['type'] === 'Subscribe') {
      this.setPopUpSubscriber()
    } else if (data['type'] === 'Authorizate') {
      this.setAuthorizate()
    }
  }

  setPopUpSubscriber () {
    this.hideVODPlayOtherCom()
    this.judgeHideDialog('Subscriber', true)
    this.subscriberDialog.setPriceProducts(this.empSubData['pricedProducts'])
  }

  closePopup () {
    this.showMethodDialog = false
  }

  showOperCenterError () {
    this.showMethodDialog = false
    this.commonService.openDialog(WeakMessageDialogComponent, null, this.translate.instant('oper_center_error'))
  }

  selectPayment (product, payment) {
    this.subscriptionsService.createPaymentMethod(product, payment.paymentId).then(resp => {
      this.showMethodDialog = false
      window.location.reload()
    }).catch(err => {
      console.log(err)
      this.showOperCenterError()
    })
  }

  createPayment (product) {
    this.subscriptionsService.createPaymentMethod(product).then(resp => {
      if (resp['confirmation'] && resp['confirmation']['confirmation_url']) {
        window.location.href = resp['confirmation']['confirmation_url']
      } else {
        this.showOperCenterError()
      }
    }).catch(err => {
      console.log(err)
      this.showOperCenterError()
    })
  }

  subProductPackage (product) {
    this.subscriptionsService.getPaymentMethods().then((paymentMethods) => {
      console.log('product', product)
      this.currentProduct = { productID: product['ID'] }
      this.paymentMethods = paymentMethods
      this.showMethodDialog = true
    }).catch(err => {
      console.log(err)
      this.showOperCenterError()
    })
  }

  firFoxAuthDelayPop () {
    let ua = navigator.userAgent.toLowerCase()
    let isFirefox = !!ua.match(/firefox\/([\d.]+)/)
    if (isFirefox) {
      _.delay(() => {
        this.judgeHideDialog('AuthorizeEmp')
      }, 200)
    } else {
      this.judgeHideDialog('AuthorizeEmp')
    }
  }

    /**
     * Password input prompt
     */
  setPopUpAuthorizeEmp (subData) {
    this.vodAuthorizeData = subData
    this.isFullScreen = subData['isFullScreen']
    if (this.isFullScreen) {
      this.firFoxAuthDelayPop()
    } else {
      subData['type'] = 'Authorizate'
      this.judgeHideDialog('MiniScreen')
      if (session.pop('IS_FIRSTLOGIN_CAN_DROP')) {
        EventService.emit('IS_FIRSTLOGIN_CAN_DROP_EVENT')
      }
      this.miniScreen.setMessage(subData)
    }
  }

    /**
     * set Authorizate
     */
  setAuthorizate () {
    this.hideVODPlayOtherCom()
    this.isJudgeSubscrEmp = false
    this.judgeHideDialog('AuthorizeFullScreenPwd', true)
    let products = {
      product: {},
      type: 'Authorizate'
    }
    this.fullScreenPassDialog.setMessage(products)
  }
    /**
     * enter procile password to subscribe
     */

  enterProfilePwd (data) {
    if (data['type'] === 'subscribeFailed') {
      this.judgeSubscrEmp = false
      this.judgeMiniScreen = true
      this.judgeSubscriber = false
      this.judgeFullScreenPwd = false
      this.judgeReplayBg = false
    } else {
      this.judgeSubscrEmp = false
      this.judgeFullScreenPwd = false
      this.judgeMiniScreen = false
      this.judgeReplayBg = false
      let vodVideoData = _.extend({}, this.empSubData)
      vodVideoData['type'] = data.type
      vodVideoData['url'] = data.url
      vodVideoData['judgeSub'] = false
      EventService.emit('LOAD_VODPLAY_VIDEO', vodVideoData)
    }
  }

    /**
     * judge hide dialog
     */
  judgeHideDialog (type, miniScreen?: any) {
    this.judgeSubscrEmp = false
    this.judgeAuthorizeEmp = false
    this.judgeSubscriber = false
    this.judgeFullScreenPwd = false
    this.judgeReplayBg = false
    this.judgePlayFailBg = false
    if (!miniScreen) {
      this.judgeMiniScreen = false
    }
    switch (type) {
      case 'SubscrEmp':
        this.judgeSubscrEmp = true
        break
      case 'Subscriber':
        this.judgeSubscriber = true
        break
      case 'FullScreenPwd':
        this.judgeFullScreenPwd = true
        break
      case 'AuthorizeEmp':
        this.judgeAuthorizeEmp = true
        break
      case 'AuthorizeFullScreenPwd':
        this.judgeFullScreenPwd = true
        break
      case 'MiniScreen':
        this.judgeMiniScreen = true
        break
      case 'ReplayBg':
        this.judgeReplayBg = true
        break
      case 'PlayFailBg':
        this.judgePlayFailBg = true
        break
      default:
        break
    }
  }

    /**
     * close the dialog
     */
  closeDialog (type) {
    if (type === 'subscriber') {
      if (this.isFullScreen) {
        this.judgeSubscrEmp = true
        this.judgeMiniScreen = false
      } else {
        this.judgeSubscrEmp = false
        this.judgeMiniScreen = true
      }
      this.judgeSubscriber = false
      this.judgeFullScreenPwd = false

      EventService.emit('CLOSE_SUBSCRIBE_DIALOG')
    } else if (type === 'password') {
      if (this.isJudgeSubscrEmp) {
          this.judgeSubscrEmp = false
          this.judgeSubscriber = true
          this.subscriberDialog.subscribeTime()
          this.judgeFullScreenPwd = false
          this.judgeAuthorizeEmp = false
          this.judgeReplayBg = false
        } else {
          this.judgeSubscrEmp = false
          this.judgeSubscriber = false
          this.judgeFullScreenPwd = false
          this.judgeReplayBg = false
          if (this.isFullScreen) {
            this.judgeAuthorizeEmp = true
            this.judgeMiniScreen = false
          } else {
            this.judgeAuthorizeEmp = false
            this.judgeMiniScreen = true
          }
        }
    }
    if (this.judgeSubscriber) {
      return
    }
    EventService.emit('closeDialog')
  }

  public showVODPlayOtherCom (): void {
    EventService.emit('SHOW_PLAYER_DROP', 'SECOND_SHOW')
  }

  public hideVODPlayOtherCom (): void {
    EventService.emit('HIDE_PLAYER_DROP')
  }
}
