import { Component, Output, EventEmitter } from '@angular/core'
import { EventService } from 'ng-epg-sdk/services'
@Component({
  selector: 'app-vodsubscremp-dialog',
  styleUrls: ['./subscrEmp.component.scss'],
  templateUrl: './subscrEmp.component.html'
})

export class VODSubscrEmpDialogComponent {
  @Output() empSub: EventEmitter<Object> = new EventEmitter()
  constructor () {
  }

  buySubscr () {
    this.empSub.emit({})
    EventService.emit('openDialog')
  }
}
