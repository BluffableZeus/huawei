import { Component, Output, EventEmitter } from '@angular/core'
import { session } from 'src/app/shared/services/session'

@Component({
  selector: 'app-miniscreen',
  styleUrls: ['./miniScreen.component.scss'],
  templateUrl: './miniScreen.component.html'
})

export class MiniScreenComponent {
    // initialization
  @Output() changeMiniScreen: EventEmitter<Object> = new EventEmitter()
  public judgeAuthOrSub: boolean
  public btnMessage = ''
  public btn = ''
  public limitType = ''
  constructor () {
  }
    /**
     * set message
     * buy
     * @param data
     */
  setMessage (data) {
    this.getType(data['type'])
    if (session.get('buy') && data['type'] === 'Subscribe') {
      this.changeMiniScreen.emit({ type: 'Subscribe' })
      session.pop('buy')
    }
  }
    /**
     * judge type
     * @param type
     */
  getType (type) {
    switch (type) {
      case 'Subscribe':
        this.btnMessage = ''
        this.btn = 'buy'
        this.judgeAuthOrSub = false
        this.limitType = 'Subscribe'
        break
      case 'Authorizate':
        this.btnMessage = 'MovieLocked'
        this.btn = 'unlock'
        this.judgeAuthOrSub = true
        this.limitType = 'Authorizate'
        break
      default:
        break
    }
  }
    /**
     * click the unLock buttom and pop up the vodsubscriber-dialog
     */
  unLock () {
    this.changeMiniScreen.emit({ type: this.limitType })
  }
}
