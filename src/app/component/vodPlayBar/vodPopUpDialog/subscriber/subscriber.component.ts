import * as _ from 'underscore'
import { Component, ElementRef, ViewChild, Output, EventEmitter, OnDestroy } from '@angular/core'
import { session } from 'src/app/shared/services/session'
import { DirectionScroll } from 'ng-epg-ui/webtv-components/scroll/directionScroll.component'
import { TranslateService } from '@ngx-translate/core'
import { CommonService } from '../../../../core/common.service'
import { EventService } from 'ng-epg-sdk/services'

@Component({
  selector: 'app-vodsubscriber-dialog',
  styleUrls: ['./subscriber.component.scss'],
  templateUrl: './subscriber.component.html'
})

export class VODSubscriberDialogComponent implements OnDestroy {
  @ViewChild('products') productDom: ElementRef
  @Output() subProduct: EventEmitter<Object> = new EventEmitter()
  @Output() closeCurrentDialog: EventEmitter<Object> = new EventEmitter()
  public count = 1
  public panel: number
  public offsetNumber = 0
  public priceLeftVisible = 'hidden'
  public priceRightVisible = 'visible'
  public page: number
  public pricedProducts = []
  public pricedProduct: any = {}
  public subscribeTimeOut
  public lastProduct: any = {}
  public currencySymbol = ''

  constructor (
        private directionScroll: DirectionScroll,
        private translate: TranslateService,
        private commonService: CommonService
    ) {
  }
    /**
     * delete the product
     */
  ngOnDestroy () {
    if (this.subscribeTimeOut) {
      clearTimeout(this.subscribeTimeOut)
    }
  }

    /**
     * Set the style of the subscribeTime
     */
  subscribeTime () {
    document.getElementById('vod_sub_scrollSpan')['style']['top'] = '0px'
    document.getElementById('vod_sub_scrollSpan')['style']['height'] = '0px'
    document.getElementById('vod_detail_content')['style']['top'] = '0px'
    document.getElementById('vod_sub_scrollList')['style']['display'] = 'none'
    clearTimeout(this.subscribeTimeOut)
    this.subscribeTimeOut = setTimeout(() => {
      if (document.getElementById('vod_detail_content')['offsetHeight'] > 145) {
        document.getElementById('vod_sub_scrollList')['style']['display'] = 'block'
        this.subscribeScroll()
      } else {
        document.getElementById('vod_sub_scrollList')['style']['display'] = 'none'
      }
    }, 100)
  }

    /**
     * change product
     */
  changeProduct (product) {
    if (!_.isEqual(product, this.lastProduct)) {
      this.pricedProduct = product
      this.lastProduct = product
      this.subscribeTime()
    }
  }

    /**
     * set the style of PriceProducts
     */
  setPriceProducts (pricedProduct) {
    this.currencySymbol = session.get('CURRENCY_SYMBOL') || ''
      // set the style of  pricesProduct
    let pricesProduct = _.map(pricedProduct, (product) => {
      let lastTime = this.commonService.dealWithDateJson('D02', product['endTime'])
      return {
        ID: product && product['ID'],
        name: product && product['name'],
        price: this.getPrice(product),
        duration_subscruber: {
            rentPeriod: product && product['rentPeriod'] || '',
            endTime_day_year_string: lastTime || '',
            endTime_hour_mins_string: this.getEndTimeStr(product)
          },
        introduce: product && product['introduce'],
        priceObject: product['priceObject']
      }
    })
    this.pricedProduct = pricesProduct[0] || {}
    this.lastProduct = pricesProduct[0] || {}
    this.pricedProducts = pricesProduct
    this.pricedProductsResult()
    this.subscribeTime()
  }

    /**
     * get the product price
     */
  getPrice (product) {
    return product && product['price'] &&
            ' ' + (product['price'] / session.get('CURRENCY_RATE')).toFixed(2) || '0'
  }

    /**
     * get the end time
     */
  getEndTimeStr (product) {
    return product && this.commonService.dealWithDateJson('D09', product['endTime']) || ''
  }

    /**
     * set the style of LastTime
     */
  setLastTime (year, month, day) {
    if (session.get('languageName') === 'zh') {
      return year + '/' + month + '/' + day
    }

    return month + ' ' + day + ', ' + year
  }

    /**
     * get the products by page
     */
  pricedProductsResult () {
    this.page = Math.floor(this.pricedProducts['length'] / 4)
    if ((this.pricedProducts['length'] % 4) === 0) {
      this.page -= 1
    }
    if (this.pricedProducts['length'] <= 4) {
      this.priceLeftVisible = 'hidden'
      this.priceRightVisible = 'hidden'
    }
  }

    /**
     * click the price-leftIcon,set styles
     */
  onClickLeft () {
    this.count--
    let offsetWidth: string
    let clientW: number = document.body.clientWidth
    if (clientW > 1440) {
      this.panel = 720
    } else {
      this.panel = 720
    }

    if (this.offsetNumber <= 0) {
      this.offsetNumber += this.panel
      offsetWidth = this.offsetNumber + 'px'
      this.productDom.nativeElement.style.transform = 'translate(' + offsetWidth + ',0px)'
      this.productDom.nativeElement.style.transition = 'all 1s linear'
      this.isShowIcon(this.offsetNumber)
    }
  }

    /**
    * click the price-rightIcon,get new products and set styles
    */
  onClickRight () {
    this.count++
    let offsetWidth: string
    let clientW: number = document.body.clientWidth
    if (clientW > 1440) {
      this.panel = 720
    } else {
      this.panel = 720
    }
    let maxWidth: number = -(this.panel * (this.page - 1))
    if (this.offsetNumber >= maxWidth) {
      this.offsetNumber -= this.panel
      offsetWidth = this.offsetNumber + 'px'
      this.productDom.nativeElement.style.transform = 'translate(' + offsetWidth + ',0px)'
      this.productDom.nativeElement.style.transition = 'all 1s linear'
      this.isShowIcon(this.offsetNumber)
    }
  }

  isShowIcon (offsetNumber: number) {
    if (offsetNumber === 0) {
      this.priceLeftVisible = 'hidden'
      this.priceRightVisible = 'visible'
    } else if (offsetNumber === -(this.panel * this.page)) {
      this.priceLeftVisible = 'visible'
      this.priceRightVisible = 'hidden'
    } else {
      this.priceLeftVisible = 'visible'
      this.priceRightVisible = 'visible'
    }
  }

    /**
     * turn to vodpassword  dialog
     * @param product
     */
  subEmpProduct (product) {
    this.subProduct.emit(product)
    EventService.emit('openVodDialog')
  }

    /**
     * close  the current dialog
     */
  close () {
    this.closeCurrentDialog.emit('subscriber')
  }

    /**
     * subscribe scroll:
     * oConter
     * oUl
     * oBox
     * oSpan
     */
  subscribeScroll () {
    let oConter, oUl, oScroll, oSpan
    let oBox = document.getElementById('vod-subscriber-dialog')
    oConter = document.getElementById('vod_bottom')
    oUl = document.getElementById('vod_detail_content')
    oScroll = document.getElementById('vod_sub_scroll')
    if (oScroll) {
      oSpan = oScroll.getElementsByTagName('span')[0]
    }
    this.directionScroll.setScroll(oBox, oConter, oUl, oScroll, oSpan, true)
  }
}
