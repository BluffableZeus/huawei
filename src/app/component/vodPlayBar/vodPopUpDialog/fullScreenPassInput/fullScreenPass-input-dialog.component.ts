import * as _ from 'underscore'
import { Component, Output, EventEmitter, OnInit } from '@angular/core'
import { playVOD } from 'ng-epg-sdk/vsp'
import { EventService } from 'ng-epg-sdk/services'
import { session } from 'src/app/shared/services/session'
import { subscribeProduct } from 'ng-epg-sdk/vsp'
import { TranslateService } from '@ngx-translate/core'
import { MediaPlayService } from '../../../mediaPlay/mediaPlay.service'
import { PlayVodService } from '../../../../on-demand/voddetail/vod-player/vod-player.service'

@Component({
  selector: 'app-vodpassword-dialog',
  templateUrl: './fullScreenPass-input-dialog.component.html',
  styleUrls: ['./fullScreenPass-input-dialog.component.scss']
})

export class VODFullScreenComponent implements OnInit {
  @Output() enterProPwd: EventEmitter<Object> = new EventEmitter()
  @Output() closePINDialog: EventEmitter<Object> = new EventEmitter()
    // declare variables.
  public epmProduct = {}
  public deletePassWord = false
  public userPassword = ''
  public errorMessage = false
  public emptyPasswordMessage = false
  public contentInfo = ''
  public types = ''
  public VOD = ''
  public showplaceholderVod = true
  public configData = {}
  public forgetpassword = false
  public forgot_password_url = ''
  public errorfocus = true
  public priceObjects = []
    // private constructor
  constructor (
        private translate: TranslateService,
        private mediaPlayService: MediaPlayService,
        private playVodService: PlayVodService
    ) {
  }
  ngOnInit () {
    EventService.removeAllListeners(['openVodDialog'])
    EventService.on('openVodDialog', () => {
      this.errorfocus = true
    })
  }
    /**
     * check the password background to hide the background,then the input get focus.
     */
  onplaceholdVod () {
    if (this.userPassword === '' || this.userPassword === undefined) {
      _.delay(() => {
        document.getElementById('pass-Wordss').focus()
      }, 100)
    }
  }
    /**
     * get the type of the pass input.
     */
  getType (type) {
      // the pass input get focus when open the dialog at first.
    _.delay(() => {
      if (document.getElementById('pass-Wordss')) {
        document.getElementById('pass-Wordss').focus()
      }
    }, 600)
    this.configData = session.get('CONFIG_DATAS') || {}
      // if the forget password url exits,show the forget password button.
    if (this.configData['forgot_password_url']) {
      this.forgetpassword = true
      this.forgot_password_url = this.configData['forgot_password_url']
    }
    document.getElementById('pass-Input')['style']['top'] = 50 + '%'
    document.getElementById('pass-Input')['style']['marginTop'] = -(329 / 2) + 'px'
    switch (type) {
        // the film should be subscribed.
      case 'Subscribe':
        this.contentInfo = this.translate.instant('SubscribePWD')
        break
          // the film is locked or parental control.
      case 'Authorizate':
        this.contentInfo = this.translate.instant('VodRatePWD')
        break
      default:
        break
    }
  }
    /**
     *  deal with the data.
     */
  setMessage (products) {
    this.errorMessage = false
    this.emptyPasswordMessage = false
    this.removePassWord()
    this.VOD = products['vod']
    this.epmProduct = products['product']
    this.types = products['type']
    this.priceObjects = []
    if (products['product']['priceObject']) {
      this.priceObjects = [products['product']['priceObject']]
    }
    this.getType(products['type'])
  }
    /**
     * change the password
     */
  changePassword (e) {
    if (e.keyCode === 13) {
      document.getElementById('confirmButtom').click()
    }
    this.showplaceholderVod = false
    this.deletePassWord = true
  }
    /**
     * judge the status of delete buttom and the placeholder.
     */
  showPassword () {
    if (this.userPassword !== '') {
      this.deletePassWord = true
      this.showplaceholderVod = false
    }
    if (this.userPassword === '') {
      this.showplaceholderVod = true
    }
  }
    /**
     * the pass input lose the focus.
     */
  hidePassword () {
    let self = this
    if (this.userPassword === '') {
      this.showplaceholderVod = true
    }
    _.delay(function () {
      self.deletePassWord = false
    }, 300)
  }
    /**
     * click the delete buttom of the pass input
     */
  removePassWord () {
    this.userPassword = ''
    document.getElementById('pass-Wordss').focus()
  }

    /*
     * Order password checksum
     *  */
  subscribePwdConfirm () {
    let self = this
    let subRequest = {
      subscribe: {
        subscribeType: '0',
        productID: self.epmProduct['ID'],
        payment: {},
        isAutoExtend: '0'
      },
      correlator: this.userPassword,
      type: '1'
    }
    if (this.priceObjects.length > 0) {
      subRequest.subscribe['priceObjects'] = this.priceObjects
    }
    subscribeProduct(subRequest).then(respss => {
      this.errorMessage = false
      this.emptyPasswordMessage = false
      EventService.emit('CHECK_PASSWORD', respss)
      self.playCheckLock(self.VOD).then(resps => {
        let sucessSub = {
            type: 'subscribeSuccess',
            url: resps['playURL']
          }
        self.enterProPwd.emit(sucessSub)
        this.playVodService.reportVOD('0')
        EventService.emit('closeDialog')
      })
    }, resps => {
      this.errorMessage = false
      this.emptyPasswordMessage = false
      if (resps && resps.result && resps.result.retCode === '155020007') {
        self.playCheckLock(self.VOD).then(resps1 => {
            let sucessSub = {
              type: 'subscribeSuccess',
              url: resps1['playURL']
            }
            self.enterProPwd.emit(sucessSub)
            EventService.emit('closeDialog')
          })
      } else if (resps && resps.result && resps.result.retCode === '157021009') {
          this.userPassword = ''
          this.showplaceholderVod = true
          this.deletePassWord = false
          this.errorMessage = true
          this.emptyPasswordMessage = false
          document.getElementById('pass-Wordss').focus()
        } else {
          this.userPassword = ''
          this.showplaceholderVod = true
          this.deletePassWord = false
          this.errorMessage = false
          this.emptyPasswordMessage = false
          document.getElementById('pass-Wordss').focus()
        }
    })
  }
    /**
     * click the confirm button to check the password.
     */
  confirm () {
    let self = this
    this.errorfocus = false
    if (this.userPassword === '') {
      this.emptyPasswordMessage = true
      this.errorMessage = false
      document.getElementById('pass-Wordss').focus()
      return
    }
    if (this.types === 'Subscribe') {
      this.subscribePwdConfirm()
    } else if (this.types === 'Authorizate') {
      EventService.removeAllListeners(['SHOW_VOD_AUTHRITE_ERROR_MESSAGE'])
      EventService.on('SHOW_VOD_AUTHRITE_ERROR_MESSAGE', function () {
          self.userPassword = ''
          self.showplaceholderVod = true
          self.deletePassWord = false
          self.errorMessage = true
          self.emptyPasswordMessage = false
          document.getElementById('pass-Wordss').focus()
        })
      EventService.emit('CHECK_VOD_PASSWORD', this.userPassword)
    }
  }

    /**
     * Order completed call playchannel authorizate
     */
  playCheckLock (VOD) {
    let self = this
    return playVOD({
      VODID: VOD['VODID'],
      mediaID: VOD['mediaID'] || '',
      isReturnProduct: '0'
    }).then(resp => {
      self.mediaPlayService.sendLicense(resp.authorizeResult)
      return resp
    }, resp => {
        return Promise.reject(resp)
      })
  }
    // close the dialog.
  close () {
    this.errorMessage = false
    this.emptyPasswordMessage = false
    this.removePassWord()
    this.closePINDialog.emit('password')
    this.errorfocus = true
  }
    // when the key down,hidden the placeholder.
  keydownPassword () {
    this.showplaceholderVod = false
  }

  btnStyleChange () {
    if ((document.getElementById('pass-Wordss')['value'] !== '')) {
      this.showplaceholderVod = false
      return true
    } else {
      this.showplaceholderVod = true
      return false
    }
  }
}
