import { Component } from '@angular/core'
import { TranslateService } from '@ngx-translate/core'
import { EventService } from 'ng-epg-sdk/services'

@Component({
  selector: 'app-playfail-background',
  styleUrls: ['./playFailBg.component.scss'],
  templateUrl: './playFailBg.component.html'
})

export class PlayFailBgComponent {
  constructor (
        private translate: TranslateService) {
  }
  /**
     * if play failed, set 'vodVideoContainer app-playfail-background' style
     */
  showPlayFail () {
    let style = {}
    if (document.querySelector('#vodVideoContainer app-playfail-background')) {
      let replayDom = document.querySelector('#vodVideoContainer app-playfail-background')
      if (replayDom['clientWidth'] > 0) {
        style['visibility'] = 'visible'
      } else {
        style['visibility'] = 'hidden'
      }
    }
    return style
  }
  /**
     * refresh
     */
  refresh () {
    EventService.emit('PLAY_FAIL_AND_REFRESH')
  }
}
