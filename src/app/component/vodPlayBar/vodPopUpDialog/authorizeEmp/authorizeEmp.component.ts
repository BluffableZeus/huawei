import { Component, Output, EventEmitter } from '@angular/core'
import { EventService } from 'ng-epg-sdk/services'
@Component({
  selector: 'app-vodauthorize-dialog',
  styleUrls: ['./authorizeEmp.component.scss'],
  templateUrl: './authorizeEmp.component.html'
})

export class VODAuthorizeEmpDialogComponent {
  @Output() authorizeEmpSub: EventEmitter<Object> = new EventEmitter()
  public judgeMiniOrFull = true
  constructor () {
    this.judgeMiniOrFull = true
  }

  unLock () {
    this.authorizeEmpSub.emit({})
    // WORKAROUND
    EventService.emit('AUTHENTICATE_OPEN_DIALOG')
    //EventService.emit('openDialog')
  }
}
