import { Component } from '@angular/core'
import { TranslateService } from '@ngx-translate/core'
import { CommonService } from '../../../../core/common.service'
import { EventService } from 'ng-epg-sdk/services'

@Component({
  selector: 'app-replay-background',
  styleUrls: ['./replayBg.component.scss'],
  templateUrl: './replayBg.component.html'
})

export class ReplayBgComponent {
  constructor (
        private translate: TranslateService,
        private commonService: CommonService) {
  }
  /**
     * replay
     */
  replay () {
    EventService.emit('playState', 2)
    EventService.emit('changeState')
  }
}
