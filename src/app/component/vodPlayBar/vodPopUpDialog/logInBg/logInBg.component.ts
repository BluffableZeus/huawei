import { Component } from '@angular/core'
import { EventService } from 'ng-epg-sdk/services'
import { TranslateService } from '@ngx-translate/core'
import { CommonService } from '../../../../core/common.service'
import { AccountLoginComponent } from '../../../../login/account-login'
import { ViewTransferService } from '../../../../core/view-transfer.service'

@Component({
  selector: 'app-login-background',
  styleUrls: ['./logInBg.component.scss'],
  templateUrl: './logInBg.component.html'
})

export class LogInBgComponent {
  private judgeAuthOrSub: boolean
  private btnMessage = ''
  private btn = ''
  private limitType = ''
  constructor (
        private translate: TranslateService,
        private commonService: CommonService,
        private viewTransferService: ViewTransferService
    ) {
  }

    /**
     * deal with data
     * @param data
     */
  setMessage (data) {
    this.getType(data['type'])
  }

    /**
     * deal with data  by type
     * @param type
     */
  getType (type) {
    switch (type) {
      case 'Subscribe':
        this.btnMessage = ''
        this.btn = 'Buy'
        this.judgeAuthOrSub = false
        this.limitType = 'Subscribe'
        break
      case 'Authorizate':
        this.btnMessage = this.translate.instant('MovieLocked')
        this.btn = 'UnLock'
        this.judgeAuthOrSub = true
        this.limitType = 'Authorizate'
        break
      default:
        break
    }
  }

    /**
     * unlock
     */
  unLock () {
      // judge whether it is already logged in
    if (Cookies.getJSON('IS_GUEST_LOGIN')) {
        // close login dialog
      EventService.emit('CLOSED_LOGIN_DIALOG')
      EventService.on('MD_DIALOG_CONTAINER', () => {
        document.getElementsByTagName('md-dialog-container')[0]['style'].position = 'fixed'
      })

      this.viewTransferService.openDialog(AccountLoginComponent)
    }
  }
}
