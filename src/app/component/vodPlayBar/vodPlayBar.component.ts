import * as _ from 'underscore'
import { AfterViewInit, Component, DoCheck, Input, OnDestroy, OnInit, ViewChild } from '@angular/core'
import { EventService } from 'ng-epg-sdk/services'
import { config as EPGConfig } from '../../shared/services/config'
import { VodPopUpDialogComponent } from './vodPopUpDialog/vodPopUpDialog.component'
import { SmallVodComponent } from './smallScreen-vodControl/smallScreen-vodControl.component'
import { VolumeBarDivComponent } from '../volumeBarDiv/volumeBarDiv.component'
import { DefinitionDetailComponent } from '../definitionDetail/definitionDetail.component'
import { MediaPlayService } from '../mediaPlay/mediaPlay.service'
import { session } from 'src/app/shared/services/session'
import { getVODDetail } from 'ng-epg-sdk/vsp'
import { Logger } from 'ng-epg-sdk/utils'
import { EpisodesListComponent } from './episodes-list/episodes-list.component'
import { ActivatedRoute } from '@angular/router'
import { SettingSuspensionComponent } from '../subTitles-audio/subTitles-audio.component'

const log = new Logger('vodPlayBar.component.ts')

@Component({
  selector: 'app-vodplaybar',
  templateUrl: './vodPlayBar.component.html',
  styleUrls: ['./vodPlayBar.component.scss']
})

export class VodPlayBarComponent implements OnInit, DoCheck, OnDestroy, AfterViewInit {
  static RATE_TIME = 8300
  @ViewChild(SmallVodComponent) smallVod: SmallVodComponent
  @ViewChild(VolumeBarDivComponent) volumeBarDiv: VolumeBarDivComponent
  @ViewChild(VodPopUpDialogComponent) vodPopUpDialog: VodPopUpDialogComponent
  @ViewChild(DefinitionDetailComponent) definitionDetail: DefinitionDetailComponent
  @ViewChild(EpisodesListComponent) episodesList: EpisodesListComponent
  @ViewChild(SettingSuspensionComponent) settingComponent: SettingSuspensionComponent
    // public seekbar: any = {};
  public details: any = {}
  public vodDetailsSmall: Object
  public vodDetailsFull: Object
  public isFullScreen = false
  public seeking = false
  public lastVolumeValue = 0.5
  public video: any = {}
  public player: any = {}
  public isVolumeBarDivShow = false
  public version = ''
  public isShowDefinitionDetail = false
  public isEpisodesShow = false
  public episodesDetail: Object = {}
  public vodVolumeSeekBarID = 'vodVolumeSeekBar'
  public isSettingMenu = false
  public sitcomNO = ''
  public epiIndex
  public isAUDIO = false
  public showDetails = true
  public settingList: {
    curSubTitle: string,
    curAudio: string,
    subtitleList: Array<any>,
    audioList: Array<any>,
    livetvFlag: boolean
  }
  public curSubTitle: string
  public curSeriesSubTitle
  public curDefinition = 'auto'
  public hideBar = null
  public definitionDataList = []
  public definitionDetailList
  public bookmarktime
  public vodVideoData = {}
  public isPaused = true
  public isPlayingEndReset = false
  public episodeFocus = true
  public leftOrRight = true
  public vodName = ''
    /**
     * show episodes number
     */
  public showVODEpiNum = false
  public epiNumParam = {}
  public isOpenPlay = true
  public isdropControlBar = false
  public dbClickTime = null
  public playUrl
  public playTra = false
  public traToEpi = true
  public judgeEpi = true
  public isReplay
  public isLimited = false
  public previewIsLocked = false
  public isJumpOut = false
  public headTime = 0
  public isInitPlay = true
  public hasHeadDuration = false
  public hasTailDuration = false
  public isFromHead = false
  public isFireFox = false
  public errorType = ''
  public isChrome = false
  public visibility = 'hidden'

  private rateTimeout = 0
  private ageMark = false

  @Input() episodes = []
  @Input() set detailInfo (playVodInfo) {
    this.details = playVodInfo
    this.vodDetailsSmall = this.details
    this.vodDetailsFull = this.details
    if (playVodInfo['VODType'] !== '0' && playVodInfo['episodes'].length > 0) {
      this.sitcomNO = playVodInfo['sitcomNO']
      let curEpiData = this.getEpiBySitcomNO(this.details, this.sitcomNO)
      this.epiIndex = curEpiData['epiIndex']
    }
  }
  hideFilter = _.debounce((fc: Function) => {
    fc()
  }, 500)

  constructor (private mediaPlayService: MediaPlayService,
                public route: ActivatedRoute) {
    let ua = navigator.userAgent.toLowerCase()
    this.isFireFox = !!ua.match(/firefox\/([\d.]+)/)
    this.isChrome = ua.indexOf('chrome') > -1
  }

    /**
     * get episode by episode's sitcomNo
     */
  getEpiBySitcomNO (details, sitcomNO) {
    return _.find(details['episodes'], item => {
      return item['sitcomNO'] === sitcomNO
    })
  }

    /**
     * get episode by episode's index
     */
  getEpiInfoByIndex (vodDetails, index) {
    return _.find(vodDetails['episodes'], item => {
      return item['epiIndex'] === index
    }) || {}
  }

  ngOnInit () {
    this.route.params.subscribe(params => {
      this.playTra = false
      this.isReplay = false
      this.isLimited = false
      this.isJumpOut = false
      this.episodeFocus = true
      session.put('VOD_IS_FULLSCREEN', false)
    })
    session.remove('VOD_IS_PLAYING')
    this.vodVolumeSeekBarID = 'vodVolumeSeekBar'
    this.video = document.querySelector('#vodVideoContainer video')
    this.player = new DMPPlayer(this.video)
    let subtitleContainer = document.getElementById('video-caption-vod')
    this.player.setSubtitleContainer(subtitleContainer)
    EventService.emit('GETPLAYERTRA', this.player)
    this.version = this.mediaPlayService.getVersion(this.player)
    this.player['addEventListener'](DMPPlayer.events.PLAYBACK_TIMEUPDATED, this.onPlayTimeUpdate.bind(this))
    this.player['addEventListener'](DMPPlayer.events.PLAYBACK_SEEKING, this.onSeeked.bind(this))
    this.player['addEventListener'](DMPPlayer.events.PLAYBACK_END, this.onEnded.bind(this))
    this.player['addEventListener'](DMPPlayer.events.PLAYBACK_START, this.playBackStart.bind(this))
    this.player['addEventListener'](DMPPlayer.events.PLAYBACK_PAUSE, this.playBackPause.bind(this))
    this.player['addEventListener'](DMPPlayer.events.PLAYBACK_PLAYING, this.playBackPlaying.bind(this))
    this.allEventService(this.player)
  }

  ngDoCheck (): void {
    if (this.isFireFox) {
      this.visibility = 'visible'
    } else {
      if (this.isAUDIO || (this.smallVod && this.smallVod.playEnd)) {
        this.visibility = 'hidden'
      } else {
        this.visibility = 'visible'
      }
    }
  }

  ngAfterViewInit () {
    document.getElementById('vod-video').oncontextmenu = () => false
  }

  ngOnDestroy () {
    if (this.smallVod && this.smallVod.seekbar) {
      EventService.emit('GETPLAYER', this.smallVod.seekbar.nativeElement.value)
    }
    EventService.removeAllListeners(['rightAction'])
    EventService.removeAllListeners(['leftAction'])
    EventService.removeAllListeners(['PREVIEW_CHANGER_PICTURE'])
    EventService.removeAllListeners(['CHANGELOADSTATE'])
    this.player.removeEventListener(DMPPlayer.events.STREAM_INITIALIZED, () => {
    }, true)
    this.video.removeEventListener('ended', () => {
    }, true)
    if (this.hideBar) {
      clearTimeout(this.hideBar)
    }
    if (this.dbClickTime) {
      clearTimeout(this.dbClickTime)
    }
    this.mediaPlayService.close(this.player)
  }

  playBackStart () {
    this.smallVod.isJumpOut = false
    if (this.smallVod.playFail && this.errorType === 'player') {
      return
    }
    if (this.isInitPlay) {
      this.onSTREAMINITIALIZED()
      this.isInitPlay = false
      this.setSkipMessage()
    }
    session.put('VOD_IS_PLAYING', true)
    this.smallVod.isBarShow = true
    this.smallVod.playing = true
    this.mouseMoveBar()
    _.delay(() => {
      EventService.emit('VOD_VIDEO_START')
    }, 200)
    this.onAgeMark()
  }

  playBackPlaying () {
    if (this.rateTimeout === 0) {
      this.countdownAgeMark()
    }
  }

  playBackPause () {
    this.offAgeMark()
  }

    /**
     * if the vod has head duration data, and you have open the skip-setting,
     * it will skip the head and show message over the bar.
     */
  setSkipMessage () {
    if (this.hasHeadDuration && this.isFromHead && !session.get('clipFlag')) {
      this.smallVod.skipHeadMessage()
    }
  }

  getVideoPaused () {
    return this.isPaused
  }

  getNext () {
    return this.episodes.find((episode) => {
      return +episode.sitcomNO === +this.details.series[0].sitcomNO + 1
    })
  }

  allEventService (player) {
    EventService.removeAllListeners(['NO_VOD_MEDIA_ID'])
    EventService.on('NO_VOD_MEDIA_ID', () => {
      this.playFailAndClosePlayer('no_media_id')
    })
    EventService.removeAllListeners(['END'])
    EventService.on('END', () => {
      this.smallVod.playEnd = true
      this.smallVod.playing = false

      if (this.getNext()) {
          // this.vodRouter.navigate(this.getNext().VOD.ID)
        EventService.emit('XVOD_NEXT')
      } else {
        this.vodPopUpDialog.judgeReplayBg = true
      }
    })
    EventService.removeAllListeners(['SHOW_SETTINGMENU'])
    EventService.on('SHOW_SETTINGMENU', value => {
      this.isSettingMenu = parseInt(value, 10) === 1
    })
    EventService.removeAllListeners(['VOD_SETSUBTITLES'])
    EventService.on('VOD_SETSUBTITLES', info => {
      this.isSettingMenu = false
      this.curSeriesSubTitle = info
      this.curSubTitle = info
      this.mediaPlayService.switchStream(this.player, 'subtitle', { lang: info }, true)
    })
    EventService.removeAllListeners(['VOD_SETSAUDIO'])
    EventService.on('VOD_SETSAUDIO', info => {
      this.isSettingMenu = false
      this.mediaPlayService.switchStream(this.player, 'audio', { lang: info }, true)
    })
    EventService.removeAllListeners(['showVolumeBar'])
    EventService.on('showVolumeBar', value => {
      this.isVolumeBarDivShow = parseInt(value, 10) === 1
    })
    EventService.removeAllListeners(['showDefinitionDetail'])
    EventService.on('showDefinitionDetail', value => {
      this.isShowDefinitionDetail = parseInt(value, 10) === 1
    })
    EventService.removeAllListeners(['playState'])
    EventService.on('playState', state => {
      if (parseInt(state, 10) === 0) {
        this.isPaused = false
        this.mediaPlayService.pause(this.player)
      } else if (parseInt(state, 10) === 2) {
          EventService.emit('REPLAYVOD')
          this.onAgeMark()
          this.isReplay = true
        } else {
          if (this.isPlayingEndReset) {
            this.isPlayingEndReset = false
            EventService.emit('RESET_PLAY_VOD')
            this.mediaPlayService.attachSource(this.player, this.playUrl, null)
            EventService.emit('PREVIEW_SHOW_PLAY_VOD')
            return
          }
          this.isPaused = true
          this.mediaPlayService.play(this.player)
        }
    })
    EventService.removeAllListeners(['enterFullScreen'])
    EventService.on('enterFullScreen', () => {
      console.log('--------enterFullScreen-------', this.isFullScreen)
      this.hideBar = null
      this.mouseMoveBar()
      this.mediaPlayService.enterFullscreen(player)
      if (!this.isChrome) {
        document.querySelector('#vodVideoContainer app-settingsuspension')['style']['visibility'] = 'visible'
      }
      session.put('VOD_IS_FULLSCREEN', true)
      EventService.emit('GET_CURRENT_FULLSCREEN')
        // FIXME Probably, there is a better solution to fix bug with data update after it was checked
      setTimeout(() => {
        this.isFullScreen = true
        this.isEpisodesShow = false
        this.leftOrRight = true
      }, 0)
    })
    EventService.removeAllListeners(['exitFullScreen'])
    EventService.on('switchToSmallScreen', () => {
      console.log('--------switchToSmallScreen-------', this.isFullScreen)
      this.isFullScreen = false;
    })
    EventService.on('exitFullScreen', () => {
      console.log('--------exitFullScreen-------', this.isFullScreen)
      if (this.smallVod.paused === 'url(assets/img/24/play_24.png)' || this.smallVod.paused ===
                'url(assets/img/32/play_fullscreen_32.png)') {
        this.smallVod.paused = (this.smallVod.isSmallScreen === 0
            ? 'url(assets/img/24/play_24.png)' : 'url(assets/img/32/play_fullscreen_32.png)')
      } else {
        this.smallVod.playPause = 'replay'
      }
      this.mediaPlayService.exitFullScreen(player, 'VOD')
      if (this.isFullScreen) {
        this.isEpisodesShow = false
        this.isFullScreen = false
        session.put('VOD_IS_FULLSCREEN', false)
        EventService.emit('CLOSED_FULLSCREEN_LIVETVVIDEO')
        EventService.emit('EXIT_VOD_FULLSCREEN')
        EventService.emit('EXIT_VOD_FULLSCREEN_FOR_REMINDER')
        EventService.emit('GET_EXIT_CURRENT_FULLSCREEN')
        EventService.emit('SERIES_EXIT_FULLSCREEN')
      }
      _.delay(() => {
        if (document.getElementById('smallScreenVodControl')) {
            document.getElementById('smallScreenVodControl').style.display = 'block'
          }
      }, 500)
      this.isJumpOut = false
      this.smallVod.isJumpOut = false
      this.leftOrRight = false
    })
    EventService.removeAllListeners(['changeVodVolumeBar'])
    EventService.on('changeVodVolumeBar', volume => {
      EventService.emit('changeVodVolume', volume)
      if (volume === 0) {
        this.lastVolumeValue = this.volumeBarDiv.lastVolumeLevel
        this.mediaPlayService.setVolume(this.player, volume)
      } else {
        this.mediaPlayService.setVolume(this.player, this.lastVolumeValue / 20)
      }
    })
    EventService.removeAllListeners(['changeVodDefinition'])
    EventService.on('changeVodDefinition', (definition, index) => {
        // definition is used to display the data
      EventService.emit('changeVod_Definition', definition)
      this.curDefinition = this.definitionDetailList[index]
      this.isShowDefinitionDetail = false
      if (parseInt(index, 10) === -1) {
        this.curDefinition = 'auto'
        this.mediaPlayService.setAutoSwitchQuality(this.player, true)
      } else {
        this.mediaPlayService.setAutoSwitchQuality(this.player, false)
        this.mediaPlayService.switchStream(this.player, 'video', { bitrate: this.definitionDetailList[index] })
      }
    })
    EventService.removeAllListeners(['SHOW_PLAYER_LEFT_RIGHT'])
    EventService.on('SHOW_PLAYER_LEFT_RIGHT', () => {
      this.leftOrRight = false
    })
    EventService.removeAllListeners(['HIDE_PLAYER_LEFT_RIGHT'])
    EventService.on('HIDE_PLAYER_LEFT_RIGHT', () => {
      this.leftOrRight = true
    })
      // play episode.
    EventService.on('playEpi', () => {
      let playInfo = session.get('playInfo')
      this.sitcomNO = playInfo['epi']
      if (this.episodeFocus === false) {
        this.episodeFocus = true
      }
      if (this.playTra) {
        this.playTra = false
      }
      this.isReplay = false
      this.mouseMoveBar()
    })
    EventService.on('NEXT', (index) => {
      this.epiIndex = index
      let curEpiData = this.getEpiInfoByIndex(this.details, index)
      this.sitcomNO = curEpiData['sitcomNO']
      this.smallVod.nextVODHandler(index)
      if (this.episodesList) {
        this.episodesList.nextVODHandler(index)
      }
    })
      // fast forward and rewind
    EventService.removeAllListeners(['leftAction'])
    EventService.on('leftAction', () => {
      if (this.leftOrRight && this.smallVod && this.smallVod.playing && this.smallVod.isSmallScreen === 1) {
        this.smallVod.clearMessageAndTimer()
        this.mouseMoveBar()
        let nowTime = this.smallVod.seekbar.nativeElement.value
        this.mediaPlayService.seek(this.player, Number(nowTime) - 10)
        this.smallVod.isPlay = 1
        this.mediaPlayService.play(this.player)
        if (this.isPlayingEndReset) {
            this.isPlayingEndReset = false
            EventService.emit('RESET_PLAY_VOD')
          }
      }
    })
    EventService.removeAllListeners(['rightAction'])
    EventService.on('rightAction', () => {
      if (this.leftOrRight && this.smallVod && this.smallVod.playing && this.smallVod.isSmallScreen === 1) {
        this.smallVod.clearMessageAndTimer()
        this.mouseMoveBar()
        let nowTime = this.smallVod.seekbar.nativeElement.value
        let duration = this.mediaPlayService.duration(this.player)
        if (nowTime >= 0 && duration > 0 && nowTime < duration) {
            if (Number(nowTime) + 10 > duration) {
              this.mediaPlayService.seek(this.player, Number(duration))
            } else {
              this.mediaPlayService.seek(this.player, Number(nowTime) + 10)
              this.smallVod.isPlay = 1
              this.mediaPlayService.play(this.player)
            }
          }
      }
    })
    EventService.removeAllListeners(['CHANGESUBTITLE'])
    EventService.on('CHANGESUBTITLE', () => {
      if (!this.playTra) {
        _.delay(() => {
            this.judgeEpi = true
            this.traToEpi = true
          }, 600)
      }
    })
    EventService.removeAllListeners(['Refresh_Doload'])
    EventService.on('Refresh_Doload', () => {
      let vodVideoData = this.vodVideoData
      if (_.isUndefined(this.vodVideoData) || _.isEmpty(this.vodVideoData)) {
        vodVideoData = session.get('Temporary_VideoData')
      }
      this.doLoad(vodVideoData)
    })
    EventService.removeAllListeners(['PREVIEW_CHANGER_PICTURE'])
    EventService.on('PREVIEW_CHANGER_PICTURE', (event) => {
      this.smallVod.previewHandler(event)
      this.changeSeek(event)
    })
    EventService.removeAllListeners(['PLAY_VOD_ERROR_DATA'])
    EventService.on('PLAY_VOD_ERROR_DATA', data => {
      if (!_.isUndefined(data) && !_.isUndefined(data['name'])) {
        this.showfulltopleft(data['name'])
      }
    })
    EventService.removeAllListeners(['REPLAY_VOD_LOAD'])
    EventService.on('REPLAY_VOD_LOAD', () => {
      setTimeout(() => {
        this.smallVod.playFail = false
        this.smallVod.isShowOnLoad = true
      }, 0)
      this.mouseMoveBar()
      this.vodPopUpDialog.judgeHideDialog('')
    })
    EventService.removeAllListeners(['CHANGELOADSTATE'])
    EventService.on('CHANGELOADSTATE', (traFlag) => {
      setTimeout(() => {
        this.smallVod.playFail = false
        this.smallVod.isShowOnLoad = true
      }, 0)
      if (traFlag) {
        this.playTra = true
        this.smallVod.isPlayTraInfo = true
        if (this.smallVod.vodType === '0' || this.smallVod.vodType === 'tra') {
            this.smallVod.isPlayTra = true
          }
      } else {
        this.playTra = false
      }
      this.vodPopUpDialog.judgeHideDialog('')
    })
    EventService.removeAllListeners(['PLAY_VOD_ERROR'])
    EventService.on('PLAY_VOD_ERROR', () => {
      this.playFailAndClosePlayer('interface')
    })
    EventService.removeAllListeners(['CLOSE_VIDEO_PLAYER'])
    EventService.on('CLOSE_VIDEO_PLAYER', () => {
      if (!_.isEmpty(this.player)) {
        this.mediaPlayService.close(this.player)
      }
    })
    EventService.removeAllListeners(['closeDialog'])
    EventService.on('closeDialog', () => {
      this.isJumpOut = false
      this.smallVod.isJumpOut = false
    })
    EventService.removeAllListeners(['openDialog'])
    EventService.on('openDialog', () => {
      this.isJumpOut = true
      this.smallVod.isJumpOut = true
    })
    EventService.removeAllListeners(['SESSION_TIME_OUT_CLOSE_PLAYER'])
    EventService.on('SESSION_TIME_OUT_CLOSE_PLAYER', () => {
      this.playFailAndClosePlayer('interface')
    })
    EventService.removeAllListeners(['PLAY_HEARTBEAT_INVALID'])
    EventService.on('PLAY_HEARTBEAT_INVALID', () => {
      this.playFailAndClosePlayer('interface')
    })
      // if the reminder dialog open in the vod which is playing, and the user click to watch program,pause the vod
    EventService.removeAllListeners(['PAUSE_FROM_REMINDER'])
    EventService.on('PAUSE_FROM_REMINDER', () => {
      if (this.smallVod.isSmallScreen === 1) {
        EventService.emit('exitFullScreen')
      }
      if (this.player && !this.mediaPlayService.isPause(this.player)) {
        this.smallVod.playControl()
      }
    })
    EventService.removeAllListeners(['START_PLAY_VOD_FOR_REMINDER'])
    EventService.on('START_PLAY_VOD_FOR_REMINDER', () => {
      if (this.player && this.mediaPlayService.isPause(this.player)) {
        this.smallVod.playControl()
      }
    })
    EventService.removeAllListeners(['SKIP_TAIL_TIME'])
    EventService.on('SKIP_TAIL_TIME', () => {
      this.onEnded(true)
    })
    EventService.removeAllListeners(['SET_SKIP_HEAD_AND_TAIL'])
    EventService.on('SET_SKIP_HEAD_AND_TAIL', () => {
      if (Number(this.smallVod.seekbar.nativeElement.value) < this.headTime) {
        let event = { time: this.headTime }
        this.smallVod.previewHandler(event)
        this.changeSeek(event)
        this.smallVod.skipHeadMessage()
      } else if (Number(this.smallVod.seekbar.nativeElement.value) >= this.smallVod.duration - this.smallVod.tailTime) {
          this.onEnded(true)
        }
    })
    EventService.removeAllListeners(['REMOVE_SKIP_HEAD_AND_TAIL'])
    EventService.on('REMOVE_SKIP_HEAD_AND_TAIL', () => {
      this.smallVod.clearMessageAndTimer()
    })
  }

    /**
     * when play fail, the player will be closed.
     */
  playFailAndClosePlayer (errorType: string) {
    session.remove('VOD_IS_PLAYING')
    this.errorType = errorType
    if (errorType === 'player' || errorType === 'no_media_id') {
      if (!_.isEmpty(this.player)) {
        this.mediaPlayService.close(this.player)
      }
    }
    this.vodPopUpDialog.judgeHideDialog('PlayFailBg')
    this.offAgeMark()
      // This is a workaround to avoid exception "Data was changed after it was checked"
      // Probably, it's a temporary one
      // But temporary workarounds usually exist forever
    setTimeout(() => {
      this.smallVod.playFail = true
      this.smallVod.isShowOnLoad = false
      this.smallVod.playing = false
      this.isJumpOut = false
      this.smallVod.isJumpOut = false
    }, 0)
  }

    /**
     * hide and display of broadcast control strip
     */
  mouseMoveBar ($event?) {
    clearTimeout(this.hideBar)
    if ($event && $event.movementX) {
      if (Math.abs($event.movementX * $event.movementX) + Math.abs($event.movementY * $event.movementY) > 9) {
          // this.showDetails = true;
        if (document.getElementById('smallScreenVodControl')) {
            document.getElementById('smallScreenVodControl')['style']['display'] = 'block'
          }
        if (document.getElementById('vodVideoContainer')) {
            document.getElementById('vodVideoContainer')['style']['cursor'] = 'default'
          }
      }
    } else {
        // this.showDetails = true;
      if (document.getElementById('smallScreenVodControl')) {
        document.getElementById('smallScreenVodControl')['style']['display'] = 'block'
      }
      if (document.getElementById('vodVideoContainer')) {
        document.getElementById('vodVideoContainer')['style']['cursor'] = 'default'
      }
    }
    this.showDetails = true
    this.hideFilter(() => {
      this.hideBar = setTimeout(() => {
        if (this.isdropControlBar) {
            return
          }
        this.showDetails = false
        if (document.getElementById('smallScreenVodControl')) {
            document.getElementById('smallScreenVodControl')['style']['display'] = 'none'
          }
        this.isVolumeBarDivShow = false
        this.isShowDefinitionDetail = false
        this.isSettingMenu = false
        if (document.getElementById('vodVideoContainer')) {
            document.getElementById('vodVideoContainer')['style']['cursor'] = 'none'
          }
      }, 5000)
    })
  }

  onAgeMark () {
    this.ageMark = true
    this.clearAgeMarkTM()
  }

  offAgeMark () {
    this.ageMark = false
    this.clearAgeMarkTM()
  }

  clearAgeMarkTM () {
    clearTimeout(this.rateTimeout)
    this.rateTimeout = 0
  }

  countdownAgeMark () {
    if (this.rateTimeout !== 0) {
      this.clearAgeMarkTM()
    }
    this.rateTimeout = _.delay(() => {
      this.offAgeMark()
    }, VodPlayBarComponent.RATE_TIME)
  }

  changeSeek (event) {
    this.isdropControlBar = false
    this.hideBar = null
    this.mouseMoveBar()
    this.mediaPlayService.seek(this.player, Number(this.smallVod.seekbar.nativeElement.value))
  }

  inputSeek (event) {
    this.isdropControlBar = true
    this.smallVod.setNowTime(Number(this.smallVod.seekbar.nativeElement.value), true)
  }

    /**
     * vod stream initialize.
     */
  onSTREAMINITIALIZED () {
    let seekTime = this.getInitSeekTime()
    this.mediaPlayService.seek(this.player, seekTime)
    this.isReplay = false
    this.bookmarktime = undefined
  }

    /**
     * get initial seek time.
     */
  getInitSeekTime () {
    this.isFromHead = false
    let duration = Math.ceil(this.mediaPlayService.duration(this.player))
    let seekTime = 0
    this.getHeadAndTailDuration(duration)
    if (!this.hasHeadDuration && !this.hasTailDuration) {
      this.settingComponent.setShowSkipOption(false)
    } else {
      this.settingComponent.setShowSkipOption(true)
    }
    this.smallVod.userID = Cookies.getJSON('USER_ID')
    if (session.get(this.smallVod.userID + '_isSkipHeadAndTail')) {
      if (!session.get('clipFlag')) {
        if (this.bookmarktime) {
            if (duration === this.bookmarktime) {
              this.bookmarktime = 0
              seekTime = this.headTime
              this.isFromHead = true
            } else if (duration < this.bookmarktime) {
              this.bookmarktime = 0
              seekTime = this.bookmarktime
            } else {
              seekTime = this.bookmarktime
            }
          } else {
            seekTime = this.headTime
            this.isFromHead = true
          }
      }
    } else {
      if (this.bookmarktime && !session.get('clipFlag')) {
        if (duration <= this.bookmarktime) {
            this.bookmarktime = 0
          } else {
            seekTime = this.bookmarktime
          }
      }
    }
    return seekTime
  }

    /**
     * get head and tail duration.
     */
  getHeadAndTailDuration (duration) {
    let mediaFiles = this.getMediaFiles()
    if (mediaFiles && mediaFiles[0]) {
      if (mediaFiles[0]['headDuration']) {
        this.headTime = Number(mediaFiles[0]['headDuration'])
        this.hasHeadDuration = true
      } else {
        this.headTime = 0
        this.hasHeadDuration = false
      }
      if (mediaFiles[0]['tailDuration']) {
        this.smallVod.tailTime = Number(mediaFiles[0]['tailDuration'])
        this.hasTailDuration = true
      } else {
        this.smallVod.tailTime = duration
        this.hasTailDuration = false
      }
    } else {
      this.headTime = 0
      this.smallVod.tailTime = undefined
      this.hasHeadDuration = false
      this.hasTailDuration = false
    }
    if (duration <= this.headTime) {
      this.headTime = 0
    }
  }

    /**
     * get media files by vod type.
     */
  getMediaFiles () {
    let vodType = Number(this.details['VODType'])
    let mediaFiles
    if (vodType === 0) {
      mediaFiles = this.details['mediaFiles']
    } else if (vodType === 1) {
      if (this.details['episodes']) {
          let episode = _.find(this.details['episodes'], epi => {
            return epi['VOD']['ID'] === this.vodVideoData['VODID']
          })
          if (episode) {
            mediaFiles = episode['VOD']['mediaFiles']
          }
        }
    } else {
      if (this.details['episodes'] && this.details['episodes'].length > 0) {
          let episode = _.find(this.details['episodes'], epi => {
            return epi['VOD']['ID'] === this.vodVideoData['VODID']
          })
          if (episode) {
            mediaFiles = episode['VOD']['mediaFiles']
          }
        } else {
          mediaFiles = this.details['mediaFiles']
        }
    }
    return mediaFiles
  }

  onPlayTimeUpdate (obj) {
    setTimeout(() => {
      this.smallVod.isShowOnLoad = false
    }, 0)
    document.getElementsByClassName('load_bg_sus')[0]['style']['display'] = 'none'
    this.smallVod.updateDuration(this.player)
    let duration = Math.ceil(this.mediaPlayService.duration(this.player))
    if (this.previewIsLocked && duration) {
      this.previewIsLocked = false
      EventService.emit('preview_duration', duration)
    }
    if (!this.smallVod.seeking) {
      this.smallVod.seekbar.nativeElement.value = Math.ceil(this.mediaPlayService.time(this.player))
      this.smallVod.setNowTime(this.smallVod.seekbar.nativeElement.value)
    }
    let postion = this.smallVod.seekbar.nativeElement.value / this.smallVod.seekbar.nativeElement.max * 100
    let ua = navigator.userAgent.toLowerCase()
    let isIe = !!ua.match(/msie/i) || !!ua.match(/rv:([\d.]+).*like gecko/)
    let isEdge = !!ua.match(/edge\/([\d.]+)/)
    if (!isIe && !isEdge) {
      this.smallVod.seekbar.nativeElement.style.background =
                'linear-gradient(to right, #FFCD7E ' + postion + '%,#979797 ' + postion + '%)'
    }
  }

  onSeeked (obj) {
    document.getElementById('vodSeekbar') && document.getElementById('vodSeekbar').blur()
    this.smallVod.seeking = false
    this.smallVod.paused = (this.smallVod.isSmallScreen === 1
        ? 'url(assets/img/32/play_fullscreen_32.png)' : 'url(assets/img/24/play_24.png)')
    if (this.isPlayingEndReset) {
      EventService.emit('RESET_ONSEEK_PLAY_VOD')
    }
  }

    /**
     * Flow end
     */
  onEnded (isSkip?: Boolean) {
    session.remove('VOD_IS_PLAYING')
    this.isShowDefinitionDetail = false
    this.isVolumeBarDivShow = false
    this.isSettingMenu = false
    this.smallVod.isPlay = 0
    clearTimeout(this.hideBar)
    document.getElementById('smallScreenVodControl')['style']['display'] = 'block'
    if (document.getElementById('vodVideoContainer')) {
      document.getElementById('vodVideoContainer')['style']['cursor'] = 'default'
    }
    this.isPlayingEndReset = true
    let duration = Math.ceil(this.mediaPlayService.duration(this.player))
    EventService.emit('VOD_FLOW_END', duration)
    EventService.emit('PREVIEW_SHOW_PLAY_END')
    if (this.vodVideoData['data']['VODType'] !== '0' &&
            (this.epiIndex + 1) !== Number(this.vodVideoData['data']['episodeCount']) && this.smallVod.isSmallScreen === 1) {
      this.smallVod.paused = 'url(assets/img/32/play_fullscreen_32.png)'
    }
    if (session.get('clipFlag') && this.smallVod.vodetails['clipfiles'] && this.smallVod.curClipIndex !==
            this.smallVod.vodetails['clipfiles'].length - 1) {
      this.smallVod.paused = 'url(assets/img/32/play_fullscreen_32.png)'
    }
    if (isSkip) {
      this.mediaPlayService.close(this.player)
    }
  }

  setSmallScreenIcon () {
    if (this.smallVod.isSmallScreen === 1 && this.smallVod.playEnd) {
      EventService.emit('exitFullScreen')
      this.smallVod.isSmallScreen = 0
    }
  }

    /**
     * stream cache
     */
  lookBuffer () {
    let startBuf = this.mediaPlayService.buffered(this.video).start(0)
    let endBuf = this.mediaPlayService.buffered(this.video).end(0)
    let val = Math.floor(endBuf - startBuf) / this.smallVod.seekbar.nativeElement.max * 100
    this.smallVod.showBuffer(val)
  }

    /**
     * Access rate, subtitles, audio information
     */
  getTrackData () {
      // Data for display
    this.definitionDataList = this.mediaPlayService.getAllBitRates(this.player)

    this.definitionDetailList = this.mediaPlayService.getAllBitRates(this.player)
    let i
    for (i = 0; i < this.definitionDataList.length; i++) {
      this.definitionDataList[i] = this.definitionDataList[i] / 1000
    }
    this.definitionDetail.getDefinitionDetailLists(this.definitionDataList)
    let curAudio = this.mediaPlayService.getCurrentAudioLanguage(this.player)
    let audioList = this.mediaPlayService.getAudioLanguages(this.player)
    this.settingList = {
      curSubTitle: '',
      curAudio: curAudio,
      subtitleList: [],
      audioList: audioList,
      livetvFlag: false
    }
      // set the subtitle because when reset audio the event of STREAM_INITIALIZED will be called
    let subtitleList = this.mediaPlayService.getSubtitleLanguages(this.player)
    if (subtitleList) {
      this.settingList['subtitleList'] = subtitleList
    }
    this.setCurrentSubTitle()
    this.settingList['curSubTitle'] = this.curSubTitle
    EventService.emit('GET_SETTINGLIST', this.settingList)
  }

    /**
     * get the current subtitle information sources
     */
  getCurSubTitleData () {
    let subtitleList = this.mediaPlayService.getSubtitleLanguages(this.player)
    this.settingList['subtitleList'] = subtitleList
    if (this.settingList['subtitleList'][0] === 'und' && this.settingList['audioList'][0] === 'und') {
      this.smallVod['noSettingMenu'] = true
    }
      /**
         * If user selected no subtitle, the <curSubTitle> will be 'off'.
         * Otherwise, the subtitle language will come from function <getCurrentSubtitleLanguage>.
         */
    this.setCurrentSubTitle()
    this.settingList['curSubTitle'] = this.curSubTitle
    EventService.emit('GET_SETTINGLIST', this.settingList)
    EventService.emit('RESET_PLAY_VOD')
    EventService.emit('CHANGESUBTITLE')
    if (this.vodVideoData && this.vodVideoData['data'] && this.vodVideoData['data'].episodes.length > 1 &&
            !session.get('clipFlag') && this.judgeEpi || this.isReplay) {
        // select series subtitle according to the last select of user.
      if (this.curSeriesSubTitle) {
        this.mediaPlayService.switchStream(this.player, 'subtitle', { lang: this.curSeriesSubTitle })
        EventService.emit('BACK_VOD_SETSUBTITLES', this.curSeriesSubTitle)
      }
    } else {
        // select single-video subtitle according to the last select of user.
      this.mediaPlayService.switchStream(this.player, 'subtitle', { lang: this.curSubTitle })
      EventService.emit('BACK_VOD_SETSUBTITLES', this.curSubTitle)
      this.curSeriesSubTitle = ''
    }
  }

  setCurrentSubTitle () {
    let subtitleLang = session.get('player.subtitle.lang')
    if (subtitleLang === 'off') {
      this.curSubTitle = 'off'
      this.curSeriesSubTitle = 'off'
    } else {
      this.curSubTitle = this.mediaPlayService.getCurrentSubtitleLanguage(this.player)
    }
  }

  playbackError (e) {
    let errorCode = e && e['errorCode']
    if (!errorCode) {
      errorCode = 'default'
    }
    let url = e.errorInfo && e.errorInfo.url
    console.log('[VOD|TYPE|PC_PLAYER_LOG] type: ' + e.type + ', errorCode: ' + e.errorCode)
    console.log('[VOD|DESC|PC_PLAYER_LOG] errorDescription: ' + e.errorDescription + '!')
    console.log('[VOD|URL|PC_PLAYER_LOG] errorURL: ' + url)
    let codeKey = 'PCPlayer.' + errorCode
    this.logPlayerError(codeKey)
    this.playFailAndClosePlayer('player')
  }

  logPlayerError (codeKey) {
    let errorJson = _.extend(require('../../../assets/i18n/' + 'log.json'))
    let errorSource = errorJson['messageResource']['PCPlayer']
    let errorObject = errorSource && errorSource[codeKey] || errorSource['PCPlayer.default']
    let message = errorObject && errorObject['m']
    log.error('{message: %s}', message)
  }

  doLoad (vodVideoData) {
    if (this.player === null) {
      return
    }
    if (session.get('NO_MEDIA_ID')) {
      return
    }
    EventService.emit('PREVIEW_SHOW_PLAY_END')
    this.isInitPlay = true
      // large screen display data
    this.isPlayingEndReset = false
    this.isPaused = true
    session.remove('VOD_IS_PLAYING')
    this.mediaPlayService.close(this.player)
    this.isAUDIO = vodVideoData.data.contentType === 'AUDIO_VOD'
    this.bookmarktime = undefined
    if (this.vodVideoData['data']) {
      this.vodVideoData['data']['sitcomNO'] = this.sitcomNO
    }
    this.showfulltopleft(vodVideoData['data'].name)
    this.dealVideoDataCom(vodVideoData)
  }

  dealVideoDataCom (vodVideoData) {
    let url = ''
    this.video = document.querySelector('#vodVideoContainer video')
      // the video object is non-existent when leaving the VOD detail page.
    if (!this.video) {
      return
    }
    this.player = new DMPPlayer(this.video)
    let subtitleContainer = document.getElementById('video-caption-vod')
    this.player.setSubtitleContainer(subtitleContainer)
    EventService.emit('GETPLAYERTRA', this.player)
    this.previewIsLocked = true
    this.player['addEventListener'](DMPPlayer.events.SUBTILES_INITIALIZED, this.getCurSubTitleData.bind(this))
    this.player['addEventListener'](DMPPlayer.events.PLAYBACK_TIMEUPDATED, this.onPlayTimeUpdate.bind(this))
    this.player['addEventListener'](DMPPlayer.events.PLAYBACK_SEEKING, this.onSeeked.bind(this))
    this.player['addEventListener'](DMPPlayer.events.PLAYBACK_END, this.onEnded.bind(this))
    this.player['addEventListener'](DMPPlayer.events.PLAYBACK_START, this.playBackStart.bind(this))
    this.player['addEventListener'](DMPPlayer.events.PLAYBACK_PAUSE, this.playBackPause.bind(this))
    this.player['addEventListener'](DMPPlayer.events.PLAYBACK_PLAYING, this.playBackPlaying.bind(this))
    this.player['addEventListener'](DMPPlayer.events.PLAYBACK_ERROR, this.playbackError.bind(this))
    this.vodVideoData = vodVideoData
    session.put('Temporary_VideoData', this.vodVideoData)
    vodVideoData['isFullScreen'] = this.isFullScreen
    if (vodVideoData['judgeSub']) {
      session.remove('VOD_IS_PLAYING')
      this.vodPopUpDialog.setPopUpSubscrEmp(vodVideoData)
      session.put('CAN_NOT_PRESS_PLAY', true)
      this.mouseMoveBar()
      setTimeout(() => {
        this.smallVod.isShowOnLoad = false
        this.smallVod.isPlay = 0
        this.isLimited = true
      }, 0)
      return
    } else if (vodVideoData['judgePwd']) {
      session.remove('VOD_IS_PLAYING')
      this.vodPopUpDialog.setPopUpAuthorizeEmp(vodVideoData)
      session.put('CAN_NOT_PRESS_PLAY', true)
      this.mouseMoveBar()
      setTimeout(() => {
          this.smallVod.isShowOnLoad = false
          this.smallVod.isPlay = 0
          this.isLimited = true
        }, 0)
      return
    } else if (vodVideoData['type'] === 'subscribeSuccess') {
        url = vodVideoData['url']
      } else {
        url = vodVideoData['url']
        this.vodPopUpDialog.judgeHideDialog('')
      }
    session.pop('IS_FIRSTLOGIN_CAN_DROP')
    if (EPGConfig['PLAY_URL'] !== '') {
      url = EPGConfig['PLAY_URL']
    }
    this.playUrl = url
    session.pop('CAN_NOT_PRESS_PLAY')
      // play button
    if (this.smallVod.isPlay === 0) {
      this.smallVod.isPlay = 1
    }
    this.dealProtDataCom(vodVideoData, url)
    setTimeout(() => {
      this.isLimited = false
      this.smallVod.isShowOnLoad = true
    }, 0)
  }

  dealProtDataCom (vodVideoData, url) {
    let protData = null
    let replayFlag = session.get('REPLAYVOD')
    if (!_.isUndefined(replayFlag) && replayFlag === true) {
      this.bookmarktime = 0
      session.pop('REPLAYVOD')
      _.delay(() => {
        this.mediaPlayService.attachSource(this.player, url, protData)
        EventService.emit('PREVIEW_SHOW_PLAY_VOD')
      }, 500)
    } else {
      if (!session.get('clipFlag')) {
        if (vodVideoData['data'] && vodVideoData['data']['bookmark']) {
            this.bookmarktime = Number(vodVideoData['data']['bookmark'])
          } else {
            this.bookmarktime = 0
          }
        _.delay(() => {
            this.mediaPlayService.attachSource(this.player, url, protData)
            EventService.emit('PREVIEW_SHOW_PLAY_VOD')
          }, 500)
      } else {
        _.delay(() => {
            this.mediaPlayService.attachSource(this.player, url, protData)
            EventService.emit('PREVIEW_SHOW_PLAY_VOD')
          }, 500)
      }
    }
    this.player['addEventListener'](DMPPlayer.events.STREAM_INITIALIZED, this.getTrackData.bind(this))
    this.player['addEventListener'](DMPPlayer.events.PLAYBACK_ERROR, this.playbackError.bind(this))
    this.dealepisodesCom(vodVideoData)
  }

  dealepisodesCom (vodVideoData) {
    if (vodVideoData && vodVideoData.data && vodVideoData.data.episodes && vodVideoData.data.episodes.length > 1 &&
            !session.get('clipFlag') && this.traToEpi || this.isReplay) {
      this.dealWithEpisode()
    } else {
      this.curDefinition = 'auto'
      this.mediaPlayService.setAutoSwitchQuality(this.player, true)
      EventService.emit('backDefinition', 'auto')
    }
    if (this.isOpenPlay) {
      this.isOpenPlay = false
      this.mediaPlayService.setVolume(this.player, 0.5)
    }
    if (this.smallVod.isPlay === 0) {
      this.smallVod.isPlay = 1
    }
  }

  dealWithEpisode () {
    if (!(this.definitionDetailList) || this.definitionDetailList.indexOf(this.curDefinition) === -1) {
      this.mediaPlayService.setAutoSwitchQuality(this.player, true)
      EventService.emit('backDefinition', this.curDefinition)
    } else {
      this.mediaPlayService.setAutoSwitchQuality(this.player, false)
      EventService.emit('backDefinition', parseInt((parseInt(this.curDefinition, 10) / 1000) + '', 10))
      this.mediaPlayService.switchStream(this.player, 'video', { bitrate: this.curDefinition })
    }
    if (this.curSeriesSubTitle) {
      this.mediaPlayService.switchStream(this.player, 'subtitle', { lang: this.curSeriesSubTitle })
      EventService.emit('BACK_VOD_SETSUBTITLES', this.curSeriesSubTitle)
    } else {
      this.mediaPlayService.switchStream(this.player, 'subtitle', { lang: this.curSubTitle })
      EventService.emit('BACK_VOD_SETSUBTITLES', this.curSubTitle)
    }
  }

    /**
     * get vod detail by interface getVODDetail
     */
  getVodDetail (req: any) {
    return getVODDetail(req).then(resp => {
      return resp
    })
  }

    /**
     * when double click on the small screen, enter full screen.
     */
  changeScreen () {
      /*
        //Removed due to DWD-118
        if (this.smallVod && this.smallVod.playing) {
            clearTimeout(this.dbClickTime);
            if (!this.isFullScreen) {
                EventService.emit('UNLOGIN_ENTER_FULLSCREEN_CHANGE');
            }
            EventService.emit('CHANGE_SCREEN');
        }
        */
  }

  clickScreen () {
    EventService.on('HIDEEPISODES', () => {
      this.isEpisodesShow = false
    })
    clearTimeout(this.dbClickTime)
    this.dbClickTime = setTimeout(() => {
      if (!this.isEpisodesShow) {
        if (session.pop('VOD_PLAYBAR_DROP_MOUSEMOVE')) {
            return
          }
        EventService.emit('CLICK_SCREEN')
        EventService.emit('CLICK_DIALOG_SCREEN')
      }
    }, 300)
  }

    /**
     * display settings menu box
     */
  showSettingMenu () {
    this.isSettingMenu = true
  }

    /**
     * close settings menu box.
     */
  closeSettingMenu () {
    this.isSettingMenu = false
  }

    /**
     * show volume box
     */
  showVolumeBar () {
    this.isVolumeBarDivShow = true
  }

    /**
     * hide volume box
     */
  hiddenVolumeBar () {
    this.isVolumeBarDivShow = false
  }

    /**
     * data from the volume component
     */
  volumeValue (curVolume) {
    if (curVolume > 20) {
      curVolume = 20
    }
    EventService.emit('volumeBarValue', curVolume)
    this.mediaPlayService.setVolume(this.player, curVolume / 20)
  }

    /**
     * show definition detail box
     */
  showDefinitionDetail () {
    this.isShowDefinitionDetail = true
  }

    /**
     * hide definition detail box
     */
  hideDefinitionDetail () {
    this.isShowDefinitionDetail = false
  }

    /**
     * display selected list, and corresponding data transmission
     */
  showEpisodesList () {
    this.isEpisodesShow = true
    if (this.details && this.details['episodes'] && this.details['episodes'].length > 0) {
      this.episodesDetail = {
        'hasEpisode': true,
        'isEpisodesShow': this.isEpisodesShow,
        'episodeFocus': this.episodeFocus,
        'sitcomNO': this.sitcomNO,
        'detail': this.details
      }
    } else {
      this.episodesDetail = {
        'hasEpisode': false,
        'isEpisodesShow': this.isEpisodesShow,
        'episodeFocus': this.episodeFocus
      }
    }
  }

    /**
     * large screen display data
     */
  showfulltopleft (name) {
    this.vodName = name
    let clipFlag = session.get('clipFlag')
    if (this.details && this.details['episodes'] && this.details['episodes'].length > 0 && clipFlag !== 'true') {
      this.showVODEpiNum = true
      this.epiNumParam = {
        'num': this.sitcomNO
      }
    } else {
      this.showVODEpiNum = false
    }
  }

    /**
     * the three button(close button, play or pause button and drag button) appear when mouse enter the float screen
     */
  showPlayerDrop () {
    EventService.emit('SHOW_PLAYER_DROP')
  }

    /**
     * the three button(close button, play or pause button and drag button) hide when mouse enter the float screen
     */
  hidePlayerDrop () {
    EventService.emit('HIDE_PLAYER_DROP')
  }

  playTraHandler () {
    if (this.episodeFocus === true) {
      this.episodeFocus = false
    }
    this.playTra = true
    this.traToEpi = false
    this.judgeEpi = false
    this.isReplay = false
    this.mouseMoveBar()
    if (this.smallVod) {
      this.smallVod.playTraHandler()
    }
  }

    // videoVisibility() {
    //     let visibility = '';
    //     if (this.isFireFox) {
    //         visibility = 'visible';
    //     } else {
    //         if (this.isAUDIO || (this.smallVod && this.smallVod.playEnd)) {
    //             visibility = 'hidden';
    //         } else {
    //             visibility = 'visible';
    //         }
    //     }
    //     return visibility;
    // }

  showBarMessage () {
    this.mouseMoveBar()
  }
}
