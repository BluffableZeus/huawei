import * as _ from 'underscore'
import { Component, Input, Output, EventEmitter, ViewChild, ElementRef, OnDestroy, OnInit } from '@angular/core'
import { EventService } from 'ng-epg-sdk/services'
import { MediaPlayService } from '../../mediaPlay/mediaPlay.service'
import { session } from 'src/app/shared/services/session'
import { TranslateService } from '@ngx-translate/core'
import { ActivatedRoute } from '@angular/router'

@Component({
  selector: 'app-smallscreen-vodcontrol',
  templateUrl: './smallScreen-vodControl.component.html',
  styleUrls: ['./smallScreen-vodControl.component.scss']
})

export class SmallVodComponent implements OnInit, OnDestroy {
  @Output() parentVod: EventEmitter<Object> = new EventEmitter()
  @Output() showPlayBar: EventEmitter<Object> = new EventEmitter()
  @ViewChild('playBarBuffer') playBarBuffer: ElementRef
  @ViewChild('seekbar') seekbar: ElementRef
    /**
     * declare variables.
     */
  public videoNowTime = '00:00:00'
  public videoTotalTime = '/ 00:00:00'
  public seeking = false
  public vodetails: any
  public player = {}
  public context = {}
  public isShowDefinitionDetail = 0
  public isVolume = 1
  public isShowVolumeBar = 0
  public isPlay = 0
  public paused = 'url(assets/img/24/play_24.png)'
  public playPause = 'play_pause'
  public isSettingMenu = 0
  public definitionValue
  public isSmallScreen = 0
  public curEpiNum = '1'
  public epiIndex
  public isPlayTra = false
  public isPlayTraInfo = false
  public isLastOne = false
  public curClipIndex = 0
  public vodType = ''
  public noSettingMenu = false
  public normalMargin = true
  public isBarShow = true
    // Determine whether to load
  public isShowOnLoad = true
  public onBlur
  public smallFullScreen = 'play_fullscreen'
    // the flag that checks whether the video is ended.
  public playEnd = false
    // the flag that checks whether the video is failed to play.
  public playFail = false
    // the flag that checks whether the video is playing.
  public playing = false
  public isJumpOut = false
  public tailTime
  public duration = 0
  public userID = ''
  public skipMessage = 'SkipCreditsStart'
  public showSkipMessage = false
  public skipMessageTimer = null
  public isFirefox = false
  public subtitleContainerTimer
  public isSubscribed = false
    /**
     * name for modules that would be used in this module
     */
  constructor (private mediaPlayService: MediaPlayService,
        private translate: TranslateService,
        public route: ActivatedRoute) {

  }
    /**
     *  if data changed, deal with the vod details
     */
  @Input() set vodInfo (data) {
    this.vodetails = data
    this.curEpiNum = data['sitcomNO']
    this.vodType = this.vodetails['VODType']
    this.isLastOne = (data.sitcomNO && data.episodes &&
            parseInt(data.sitcomNO, 10) === parseInt(data.episodes[data.episodes.length - 1]['sitcomNO'], 10)) ||
            !(data.episodes) || data.episodes.length === 0
    this.isPlayTra = false
    this.isPlayTraInfo = false
  }
    /**
     * initialization
     */
  ngOnInit () {
    console.log("10000")
    //debugger;
    this.route.params.subscribe(() => {
      this.playEnd = false
      this.playFail = false
      this.playing = false
      this.isJumpOut = false
    })
    if (session.get('CAN_NOT_PRESS_PLAY')) {
      this.isBarShow = false
    }
    this.definitionValue = 'auto'
      // type of browser
    let ua = navigator.userAgent.toLowerCase()
    let isIe = !!ua.match(/msie/i) || !!ua.match(/rv:([\d.]+).*like gecko/)
    this.isFirefox = !!ua.match(/firefox\/([\d.]+)/)
    let isEdge = !!ua.match(/edge\/([\d.]+)/)
    if (isIe) {
      document['addEventListener']('MSFullscreenChange', this.vodExitFullScreen.bind(this))
    } else if (this.isFirefox) {
      document['addEventListener']('mozfullscreenchange', this.vodExitFullScreen.bind(this))
    } else if (ua.match(/version\/([\d.]+).*safari/)) {
        document['addEventListener']('fullscreenchange', this.vodExitFullScreen.bind(this))
      } else if (isEdge) {
        document['addEventListener']('webkitfullscreenchange', this.vodExitFullScreen.bind(this))
      } else {
        document['addEventListener']('webkitfullscreenchange', this.vodExitFullScreen.bind(this))
      }
    EventService.on('onBlur', (blur) => {
      this.onBlur = blur
    })
    EventService.on('playEpi', () => {
      this.isPlayTraInfo = false
    })
      /** keyCode table
         * 32: Space
         * 37: Left Arrow
         * 38: Up Arrow
         * 39: Right Arrow
         * 40: Down Arrow
         */
    if (window.addEventListener) {
      document.addEventListener('keydown', e => {
        let keyCode = e.keyCode
        if (keyCode === 37 || keyCode === 39 || keyCode === 32) {
            if (e && e.preventDefault && (e.target['id'] === 'input-text-overflow' || e.target['id'] === 'pass-Wordss' ||
                        e.target['id'] === 'login-ID' || e.target['id'] === 'pass-Word' || e.target['id'] === 'pass-Wordsss')
            ) {

            } else {
              if (e && e.preventDefault) {
                if (keyCode === 32) {
                  e.preventDefault()
                }
              } else {
                window.event.returnValue = false
              }
            }
          }
      })
    } else if (window['attachEvent']) {
      document['attachEvent']('onkeydown', e => {
          let keyCode = e.keyCode
          if (keyCode === 37 || keyCode === 39 || parseInt(keyCode, 10) === 32) {
            if (e && e.preventDefault && (e.target['id'] === 'input-text-overflow' || e.target['id'] === 'pass-Wordss' ||
                        e.target['id'] === 'login-ID' || e.target['id'] === 'pass-Word' || e.target['id'] === 'pass-Wordsss')
            ) {

            } else {
              if (e && e.preventDefault) {
                if (keyCode === 32) {
                  e.preventDefault()
                }
              } else {
                window.event.returnValue = false
              }
            }
          }
        })
    }
    EventService.on('ScreenSize', () => {
      let subtitleContainer = document.getElementById('video-caption-vod')
      if (subtitleContainer) {
        clearTimeout(this.subtitleContainerTimer)
        subtitleContainer.style.display = 'none'
        this.subtitleContainerTimer = setTimeout(() => {
            subtitleContainer.style.display = 'block'
          }, 500)
      }
    })

    EventService.removeAllListeners(['CHANGETRA'])
    EventService.on('CHANGETRA', () => {
      this.isPlayTra = true
      this.isPlayTraInfo = true
    })
    EventService.removeAllListeners(['CHANGETRAINFO'])
    EventService.on('CHANGETRAINFO', () => {
      this.isPlayTraInfo = true
    })
    if (this.seekbar) {
      this.seekbar.nativeElement.addEventListener('keydown', (e) => {
        if ((e.keyCode === 38 || e.keyCode === 40) && (this.isSmallScreen === 1) && (this.isShowVolumeBar !== 1)) {
            e.preventDefault()
          }
      }, true)
      this.seekbar.nativeElement.addEventListener('change', (event) => {
        this.onSeekBarChange(event)
      }, true)
      this.seekbar.nativeElement.addEventListener('input', (event) => {
        this.onSeeking(event)
      }, true)
    }

      // Control volume icon conversion
    EventService.removeAllListeners(['volumeBarValue'])
    EventService.on('volumeBarValue', a => {
      if (a === 0) {
        this.isVolume = 0
      } else {
        this.isVolume = 1
      }
    })
    EventService.removeAllListeners(['changeVod_Definition'])
    EventService.on('changeVod_Definition', definition => {
      if (definition === 'auto') {
        this.definitionValue = definition
      } else {
        this.definitionValue = definition / 1000 + ' ' + this.translate.instant('kbps')
      }
    })
      /*
    EventService.removeAllListeners(['CLOSED_FULLSCREEN_VODVIDEO'])
    EventService.on('CLOSED_FULLSCREEN_VODVIDEO', keycode => {
      this.LivePlayerKeyUp(keycode)
    })
    */
      // Drama series events
    EventService.on('playEpi', () => {
      let playInfo = session.get('playInfo')
      this.curEpiNum = playInfo['epi']
      this.epiIndex = playInfo['epiIndex']
      this.isBarShow = false
    })
    EventService.removeAllListeners(['CHANGE_SCREEN'])
      /*
    EventService.on('CHANGE_SCREEN', () => {
      this.enterFullScreen()
    })
    */
    EventService.removeAllListeners(['CLICK_SCREEN'])
    EventService.on('CLICK_SCREEN', () => {
      this.playControl()
    })
    EventService.on('NEXTCLIP', (clipIndex) => {
      this.isBarShow = false
      this.curClipIndex = clipIndex
      if (this.vodetails['clipfiles']) {
        let lastclipNum = this.vodetails['clipfiles'].length
        if (this.curClipIndex === lastclipNum - 1) {
            this.isLastOne = true
          }
      }
    })
    EventService.on('backDefinition', definition => {
      if (definition === 'auto') {
        this.definitionValue = definition
      } else {
        this.definitionValue = definition + ' ' + this.translate.instant('kbps')
      }
    })
    EventService.removeAllListeners(['NEXT_ICON_DISAPPEAR'])
    EventService.on('NEXT_ICON_DISAPPEAR', disappearFlag => {
      if (disappearFlag === '1') {
        this.isLastOne = true
      } else if (disappearFlag === '0') {
          this.isLastOne = false
        }
    })
    EventService.removeAllListeners(['RESET_PLAYING_STATE'])
    EventService.on('RESET_PLAYING_STATE', () => {
      this.resetPlayingState()
    })



    if (this.vodetails && this.vodetails.isSubscribed === '1') {
      this.isSubscribed = true
      //this.enterFullScreen()
    }

  }
    /**
    * when exit the component, clear all the events
    */
  ngOnDestroy () {
    clearTimeout(this.skipMessageTimer)
    if (this.seekbar) {
      this.seekbar.nativeElement.removeEventListener('change', (event) => {
        this.onSeekBarChange(event)
      }, true)
      this.seekbar.nativeElement.removeEventListener('input', (event) => {
        this.onSeeking(event)
      }, true)
    }
  }

  previewHandler (event) {
    this.seekbar.nativeElement.value = event.time
    this.onSeekBarChange(event.time)
    this.onSeeking(event.time)
    this.setNowTime(event.time, true)
  }
    /**
     * Positive play
     */
  playFilm () {
    console.log('playFilm');
    session.remove('clipFlag')
    EventService.emit('PLAYFILM')
    this.isPlayTra = false
    this.isPlayTraInfo = false
    this.isBarShow = false
    this.vodType = '0'
  }
    /**
     * get epiInfo by index
     */
  getEpiInfoByIndex (vodDetails, index) {
    return _.find(vodDetails['episodes'], item => item['epiIndex'] === index) || {}
  }

    // Play cache
  showBuffer (val) {
    this.playBarBuffer.nativeElement.style.width = val + '%'
  }
    // Progress bar monitoring
  onSeekBarChange (event) {
    let position = this.seekbar.nativeElement.value / this.seekbar.nativeElement.max * 100
    let ua = navigator.userAgent.toLowerCase()
    let isIe = !!ua.match(/msie/i) || !!ua.match(/rv:([\d.]+).*like gecko/)
    let isEdge = !!ua.match(/edge\/([\d.]+)/)
    if (!isIe && !isEdge) {
      this.seekbar.nativeElement.style.background = 'linear-gradient(to right, #FFCD7E ' +
                position + '%,#979797 ' + position + '%)'
    }
    this.dropControlBar()
  }
  onSeeking (event) {
    this.seeking = true
    let position = this.seekbar.nativeElement.value / this.seekbar.nativeElement.max * 100
    let ua = navigator.userAgent.toLowerCase()
    let isIe = !!ua.match(/msie/i) || !!ua.match(/rv:([\d.]+).*like gecko/)
    let isEdge = !!ua.match(/edge\/([\d.]+)/)
    if (!isIe && !isEdge) {
      this.seekbar.nativeElement.style.background = 'linear-gradient(to right, #FFCD7E ' +
                position + '%,#979797 ' + position + '%)'
    }
  }
    /**
     * Progress bar start time
     */
  setNowTime (value, isClick?: Boolean) {
    if (!isNaN(value)) {
      if (session.get(this.userID + '_isSkipHeadAndTail') && !session.get('clipFlag')) {
        if (isClick) {
            if (this.skipMessage === 'SkipCreditsEnd') {
              this.clearMessageAndTimer()
            }
          } else {
            if (+value === (this.duration - this.tailTime - 5)) {
              clearTimeout(this.skipMessageTimer)
              this.skipMessage = 'SkipCreditsEnd'
              this.showSkipMessage = true
              this.showPlayBar.emit()
              this.skipMessageTimer = setTimeout(() => {
                this.showSkipMessage = false
                if (this.isPlay === 1 && session.get(this.userID + '_isSkipHeadAndTail')) {
                  EventService.emit('SKIP_TAIL_TIME')
                }
              }, 5000)
            }
          }
      }
      this.videoNowTime = this.convertToTimeCode(value)
    }
  }

  skipHeadMessage () {
    clearTimeout(this.skipMessageTimer)
    this.skipMessage = 'SkipCreditsStart'
    this.showSkipMessage = true
    this.skipMessageTimer = setTimeout(() => {
      this.showSkipMessage = false
    }, 5500)
  }
    /**
     * Progress bar end time
     */
  setTotalTime (player, value) {
    if (!isNaN(value)) {
      this.videoTotalTime = '/ ' + this.convertToTimeCode(value)
    }
  }
    /**
     * Listen play
     */
  updateDuration (player) {
    this.duration = Math.ceil(this.mediaPlayService.duration(player))
    if (this.duration !== parseFloat(this.seekbar.nativeElement.max)) {
      this.setTotalTime(player, this.duration)
      this.seekbar.nativeElement.max = this.duration
    }
  }
    /**
     *  Volume control
     */
  volumeControl () {
    if (this.isJumpOut) {
      return
    }
    if (this.isVolume === 1) {
      this.isVolume = 0
      EventService.emit('changeVodVolumeBar', this.isVolume)
    } else {
      this.isVolume = 1
      EventService.emit('changeVodVolumeBar', this.isVolume)
    }
  }
    /**
     * Drag progress bar play
     */
  dropControlBar () {
    this.paused = (this.isSmallScreen === 0 ? 'url(assets/img/24/play_24.png)' : 'url(assets/img/32/play_fullscreen_32.png)')
    if (this.isPlay === 0) {
      this.isPlay = 1
      this.playPause = 'play_pause'
      EventService.emit('playState', this.isPlay)
    }
  }
    /**
     * Play / pause button operation
     */
  playControl () {
    if (this.playing && !this.isShowOnLoad) {
      this.paused = (this.isSmallScreen === 0
          ? 'url(assets/img/24/play_24.png)' : 'url(assets/img/32/play_fullscreen_32.png)')
      if (this.isPlay === 1) {
        this.clearMessageAndTimer()
        this.isPlay = 0
        this.playPause = 'play'
        EventService.emit('playState', this.isPlay)
      } else {
        this.isPlay = 1
        this.playPause = 'play_pause'
        EventService.emit('playState', this.isPlay)
        //if (this.isSmallScreen === 0) { this.enterFullScreen() }
      }
    }
  }
    /**
     * Display settings menu box
     */
  showSettingBtn () {
    if (Cookies.getJSON('IS_GUEST_LOGIN')) {
      return
    }
    let pressDisable = session.get('CAN_NOT_PRESS_PLAY')
    if (pressDisable !== true && this.isBarShow) {
      this.isSettingMenu = 1
      if (!this.noSettingMenu && this.isBarShow && !this.playEnd && !this.playFail && !this.isShowOnLoad) {
        EventService.emit('SHOW_SETTINGMENU', this.isSettingMenu)
      }
    }
  }
    /**
     * Close settings menu box
     */
  closeSettingBtn () {
    this.isSettingMenu = 0
    if (!this.noSettingMenu) {
      EventService.emit('SHOW_SETTINGMENU', this.isSettingMenu)
    }
  }
    /**
     * Display volume progress bar
     */
  showVolumeBar () {
    if (Cookies.getJSON('IS_GUEST_LOGIN') || this.isJumpOut) {
      return
    }
    this.isShowVolumeBar = 1
    EventService.emit('showVolumeBar', this.isShowVolumeBar)
  }
    /**
     *  Hide volume progress bar
     */
  hiddenVolumeBar () {
    this.isShowVolumeBar = 0
    EventService.emit('showVolumeBar', this.isShowVolumeBar)
  }
    /**
     * Display rate selection
     */
  showDefinitionDetail () {
    if (Cookies.getJSON('IS_GUEST_LOGIN')) {
      return
    }
    let pressDisable = session.get('CAN_NOT_PRESS_PLAY')
    if (pressDisable !== true && this.isBarShow && !this.playEnd && !this.playFail && !this.isShowOnLoad) {
      this.isShowDefinitionDetail = 1
      EventService.emit('showDefinitionDetail', this.isShowDefinitionDetail)
    }
  }
    /**
     *  Hidden rate selection
     */
  hideDefinitionDetail () {
    this.isShowDefinitionDetail = 0
    EventService.emit('showDefinitionDetail', this.isShowDefinitionDetail)
  }
    /**
     * when enter full screen
     */
  enterFullScreen () {

      if (this.isJumpOut) {
        return
      }

      this.smallFullScreen = (this.isSmallScreen === 0 ? 'play_smallscreen' : 'play_fullscreen')
      this.getPauseIcon(1)
      let pressDisable = session.get('CAN_NOT_PRESS_PLAY')
      let subtitleContainer = document.getElementById('video-caption-vod')
      if (subtitleContainer) {
        clearTimeout(this.subtitleContainerTimer)
        subtitleContainer.style.display = 'none'
        this.subtitleContainerTimer = setTimeout(() => {
          subtitleContainer.style.display = 'block'
        }, 500)
      }
      if (this.isSmallScreen === 0) {
        EventService.emit('enterFullScreen', this.playEnd)
          this.isSmallScreen = 1
          subtitleContainer.style.zIndex = '2147483647'
          if (pressDisable === true && this.playFail === false) {
            EventService.emit('Refresh_Doload')
          }
      } else {
        EventService.emit('switchToSmallScreen')
          subtitleContainer.style.zIndex = '2'
          this.isSmallScreen = 1
          //EventService.emit('Refresh_Doload')
          this.isPlay=0

          if (document.exitFullscreen) {
            document.exitFullscreen()
          } else if (document['mozCancelFullScreen']) {
            document['mozCancelFullScreen']()
          } else if (document['msExitFullscreen']) {
            document['msExitFullscreen']()
          } else {
            document.webkitCancelFullScreen()
          }

          //EventService.emit('PLAYFILM')
          //this.playControl()
      }
      if (pressDisable === true) {
        this.paused = 'url(assets/img/32/stop_32.png)'
      }
  }

    /**
     * convert to time code
     */
  convertToTimeCode (value) {
    value = Math.max(value, 0)
    let h = Math.floor(value / 3600)
    let m = Math.floor(value % 3600 / 60)
    let s = Math.floor(value % 3600 % 60)
    return (h === 0 ? '00:' : h < 10 ? '0' + h.toString() + ':' : h.toString() + ':') +
            (m < 10 ? '0' + m.toString() : m.toString()) + ':' + (s < 10 ? '0' + s.toString() : s.toString())
  }
    /**
     * Key events
     */
  LivePlayerKeyUp (keyCode) {
      // space key
    if (keyCode === 32 && !this.onBlur && session.get('REMINDER_FLAG') !== 1) {
      this.playControl()
    }
  }
    /**
     * when exit fullscreen
     */
  vodExitFullScreen () {
    if (document.webkitFullscreenElement || document['FullscreenElement'] || document['mozFullScreenElement'] || document['msFullscreenElement']) {
      if (document.getElementById('smallScreenVodControl')) {
        document.getElementById('smallScreenVodControl').style.visibility = 'visible'
      }
      return
    }
    if (this.isFirefox) {
      if (session.get('FIREFOX_SCROLLTOP')) {
        document.documentElement.scrollTop = session.get('FIREFOX_SCROLLTOP')
      }
    }
    let subtitleContainer = document.getElementById('video-caption-vod')
    if (subtitleContainer) {
      clearTimeout(this.subtitleContainerTimer)
      subtitleContainer.style.zIndex = '2'
      subtitleContainer.style.display = 'none'
      this.subtitleContainerTimer = setTimeout(() => {
        subtitleContainer.style.display = 'block'
      }, 500)
    }
    this.isSmallScreen = 0
    this.smallFullScreen = 'play_fullscreen'
    if (document.getElementById('smallScreenVodControl') && document.getElementById('topControllerfull')) {
      document.getElementById('smallScreenVodControl').style.display = 'none'
      document.getElementById('topControllerfull').style.display = 'none'
    }
    this.getPauseIcon(0)
    //EventService.emit('exitFullScreen')
    EventService.emit('SHOW_SETTINGMENU', 0)
      //debugger
  }

  getPauseIcon (isSmall) {
    if (this.paused === 'url(assets/img/24/play_24.png)' || this.paused === 'url(assets/img/32/play_fullscreen_32.png)') {
      this.paused = (this.isSmallScreen === isSmall
          ? 'url(assets/img/24/play_24.png)' : 'url(assets/img/32/play_fullscreen_32.png)')
    }
  }
    /**
     * Next play
     */
  playNextVideo () {
    let lastEpiNum
    let lastclipNum
    let isClip = session.get('clipFlag')
    if (isClip === 'true') {
      if (this.vodetails['clipfiles']) {
        let clipData = this.vodetails['clipfiles']
        lastclipNum = clipData.length
      }
      if (this.curClipIndex !== lastclipNum) {
        EventService.emit('playNextVod')
      }
    } else {
      if (this.vodetails['episodes']) {
        let episodeData = this.vodetails['episodes']
        let epiLength = episodeData.length
        lastEpiNum = episodeData[epiLength - 1]['sitcomNO']
      }
      if (Number(this.curEpiNum) !== lastEpiNum) {
        EventService.emit('playNextVod')
      }
    }
  }

  nextVODHandler (index) {
    this.epiIndex = index
    let curEpiData = this.getEpiInfoByIndex(this.vodetails, index)
    this.curEpiNum = curEpiData['sitcomNO']
    this.isBarShow = false
  }

  playTraHandler () {
    let playInfo = session.get('playInfo')
    this.curClipIndex = playInfo['traIndex']
    this.vodType = 'tra'
    this.isBarShow = false
      // this.enterFullScreen();
  }

  resetPlayingState () {
    this.clearMessageAndTimer()
    this.isBarShow = false
    this.playing = false
    this.playEnd = false
    this.playFail = false
    this.closeSettingBtn()
  }

  clearMessageAndTimer () {
    clearTimeout(this.skipMessageTimer)
    this.showSkipMessage = false
  }
}
