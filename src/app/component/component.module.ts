import { LockedPasswordComponent } from './checkPass/checkPass.component'
import { ReplayBgComponent } from './vodPlayBar/vodPopUpDialog/replayBg/replayBg.component'
import { MiniScreenComponent } from './vodPlayBar/vodPopUpDialog/miniScreen/miniScreen.component'
import { EpisodesListComponent } from './vodPlayBar/episodes-list/episodes-list.component'
import { VodPopUpDialogComponent } from './vodPlayBar/vodPopUpDialog/vodPopUpDialog.component'
import { SmallVodComponent } from './vodPlayBar/smallScreen-vodControl/smallScreen-vodControl.component'
import { VodPlayBarComponent } from './vodPlayBar/vodPlayBar.component'
import { PlayFailBgComponent } from './vodPlayBar/vodPopUpDialog/playFailBg/playFailBg.component'
import { PlayLiveTvFailBgComponent } from './playPopUpDialog/playFailLiveTv/playFailLiveTv.component'
//import { ErrorMessageDialogComponent } from './errorMessage/error-message-dialog.component'
import { MediaPlayService } from './mediaPlay/mediaPlay.service'
import { TVODAppService } from './playPopUpDialog/tVodApp.service'
import { GuestPlayAppService } from './live-tv-guest/guestPlayApp.service'
import { PlaybillAppService } from './playPopUpDialog/playbillApp.service'
import { NPVRAppService } from './playPopUpDialog/nPvrApp.service'
import { RecordDataService } from './recordDialog/recordData.service'
import { FormsModule } from '@angular/forms'
import { DefinitionDetailComponent } from './definitionDetail/definitionDetail.component'
//import { WeakTipDialogComponent } from './weakTip/weakTip.component'
//import { RecordDialogComponent } from './recordDialog/recordDialog.component'
//import { UpDateRecordDialogComponent } from './updateRecordDialog/updateRecordDialog.component'
import { AuthorizeEmpDialogComponent } from './playPopUpDialog/authorizeEmp/authorizeEmp.component'
import { GuestDialogComponent } from './live-tv-guest/livetv-guest.component'
import { SettingSuspensionComponent } from './subTitles-audio/subTitles-audio.component'
import { FullScreenPassDialogComponent } from './playPopUpDialog/fullScreenPassInput/fullScreenPass-input-dialog.component'
import { VolumeBarDivComponent } from './volumeBarDiv/volumeBarDiv.component'
import { SubscriberDialogComponent } from './playPopUpDialog/subscriber/subscriber.component'
import { SubscrEmpDialogComponent } from './playPopUpDialog/subscrEmp/subscrEmp.component'
import { VODFullScreenComponent } from './vodPlayBar/vodPopUpDialog/fullScreenPassInput/fullScreenPass-input-dialog.component'
import { VODSubscriberDialogComponent } from './vodPlayBar/vodPopUpDialog/subscriber/subscriber.component'
import { VODSubscrEmpDialogComponent } from './vodPlayBar/vodPopUpDialog/subscrEmp/subscrEmp.component'
import { VODAuthorizeEmpDialogComponent } from './vodPlayBar/vodPopUpDialog/authorizeEmp/authorizeEmp.component'
import { LiveTVMiniDetailComponent } from './liveTVPlayBar/live-tvbar-mini-detail/live-tvbar-mini-detail.component'
import { MiniEPGComponent } from './mini-epg/mini-epg.component'
import { FullChannelComponent } from './liveTVPlayBar/liveTVControl/fullScreen-channelControl.component'
import { PlayPopUpDialogComponent } from './playPopUpDialog/playPopUpDialog.component'
import { UISharedModule } from 'ng-epg-ui/webtv-components/shared/shared.module'
import { LiveTVPlayBarComponent } from './liveTVPlayBar/liveTVPlayBar.component'
import { PassInputDialogComponent } from './passInput/pass-input-dialog.component'
import { StorageRemindComponent } from './storageRemind/storageRemind.component'
import { SharedModule } from '../shared/shared.module'
import { NgModule } from '@angular/core'
import { ReminderComponent } from './reminder/reminder.component'
import { ReminderService } from './reminder/reminder.service'
import { ReminderTask } from './reminder/reminder-task.service'
import { WeakMessageDialogComponent } from './weakMessageDialog/weakMessageDialog.component'
import { DateStingComponent } from './dateString/dateString.component'
import { CloseOrConfirmComponent } from './closeOrConfirm/closeOrConfirm.component'
import { StopRecordConfirmComponent } from './stopRecord-confirm/stopRecord-confirm.component'
import { NoDataModule } from 'ng-epg-ui/webtv-components/noData/no-data.module'
import { RecordEpisodesListComponent } from './liveTVPlayBar/record-episodes-list/record-episodes-list.component'
import { RecordEpisodesListService } from './liveTVPlayBar/record-episodes-list/record-episodes-list.service'
//import { RecordingConflictComponent } from './recording-conflict'
import { MixedConflictComponent } from './mixed-conflict'
import { PreviewComponent } from './preview/preview.component'
import { BasicInfoComponent } from './modifyBasicInfo/modify-basic.component'
import { ModifyPasswordComponent } from './modifyPassword/modify-pass.component'
import { PipesModule } from '../shared/pipes/pipes.module'
import { LogInBgComponent } from './vodPlayBar/vodPopUpDialog/logInBg/logInBg.component'
@NgModule({
  imports: [
    SharedModule,
    UISharedModule,
    FormsModule,
    NoDataModule,
    PipesModule
  ],
  exports: [
    LiveTVPlayBarComponent,
    //WeakTipDialogComponent,
    LiveTVMiniDetailComponent,
    VodPlayBarComponent,
    LockedPasswordComponent,
    PassInputDialogComponent,
    WeakMessageDialogComponent,
    CloseOrConfirmComponent,
    StopRecordConfirmComponent,
    //RecordDialogComponent,
    //ErrorMessageDialogComponent,
    PreviewComponent,
    BasicInfoComponent,
    ModifyPasswordComponent,
    //UpDateRecordDialogComponent,
    LogInBgComponent
  ],
  declarations: [
    LiveTVPlayBarComponent,
    PlayPopUpDialogComponent,
    FullChannelComponent,
    MiniEPGComponent,
    LiveTVMiniDetailComponent,
    SubscrEmpDialogComponent,
    SubscriberDialogComponent,
    VolumeBarDivComponent,
    FullScreenPassDialogComponent,
    SettingSuspensionComponent,
    GuestDialogComponent,
    AuthorizeEmpDialogComponent,
    //WeakTipDialogComponent,
    DefinitionDetailComponent,
    //ErrorMessageDialogComponent,
    VodPlayBarComponent,
    SmallVodComponent,
    VodPopUpDialogComponent,
    EpisodesListComponent,
    MiniScreenComponent,
    ReplayBgComponent,
    PlayFailBgComponent,
    PlayLiveTvFailBgComponent,
    LockedPasswordComponent,
    PassInputDialogComponent,
    VODFullScreenComponent,
    VODSubscriberDialogComponent,
    VODSubscrEmpDialogComponent,
    VODAuthorizeEmpDialogComponent,
    //RecordDialogComponent,
    //UpDateRecordDialogComponent,
    StorageRemindComponent,
    ReminderComponent,
    WeakMessageDialogComponent,
    CloseOrConfirmComponent,
    StopRecordConfirmComponent,
    RecordEpisodesListComponent,
    //RecordingConflictComponent,
    MixedConflictComponent,
    PreviewComponent,
    BasicInfoComponent,
    ModifyPasswordComponent,
    LogInBgComponent
  ],
  entryComponents: [
    //ErrorMessageDialogComponent,
    PassInputDialogComponent,
    //RecordDialogComponent,
    //UpDateRecordDialogComponent,
    ReminderComponent,
    WeakMessageDialogComponent,
    CloseOrConfirmComponent,
    StopRecordConfirmComponent,
    StorageRemindComponent,
    //RecordingConflictComponent,
    MixedConflictComponent,
    BasicInfoComponent,
    ModifyPasswordComponent
  ],
  providers: [
    PlaybillAppService,
    GuestPlayAppService,
    TVODAppService,
    MediaPlayService,
    RecordDataService,
    ReminderService,
    ReminderTask,
    DateStingComponent,
    NPVRAppService,
    RecordEpisodesListService
  ]
})
export class ComponentModule { }
