import { Component, Output, EventEmitter } from '@angular/core'
import { TranslateService } from '@ngx-translate/core'
import { EventService } from 'ng-epg-sdk/services'
@Component({
  selector: 'app-subscremp-dialog',
  styleUrls: ['./subscrEmp.component.scss'],
  templateUrl: './subscrEmp.component.html'
})

export class SubscrEmpDialogComponent {
  @Output() empSub: EventEmitter<Object> = new EventEmitter()
  @Output() closeEmpSub: EventEmitter<Object> = new EventEmitter()
  public channelName: any
  constructor (
        private translate: TranslateService
    ) {
  }
    /**
     * show the subsrcibe list
     */
  buySubscr () {
    EventService.emit('openLiveDialog')
    this.empSub.emit({})
  }
    /**
     * close the subsrcibe list
     */
  close () {
    this.closeEmpSub.emit('EmpSub')
  }
    /**
     * deal with the channel name showing in the tips
     */
  getDate (channelname) {
    let name = channelname
    if (name && name.length > 20) {
      name = name.substring(0, 20) + '...'
    }
    this.channelName = { channelname: name }
  }
}
