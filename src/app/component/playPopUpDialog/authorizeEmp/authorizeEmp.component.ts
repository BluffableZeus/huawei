import { Component, Output, OnInit, EventEmitter } from '@angular/core'
import { EventService } from 'ng-epg-sdk/services'
import { TranslateService } from '@ngx-translate/core'
@Component({
  selector: 'app-authorize-dialog',
  styleUrls: ['./authorizeEmp.component.scss'],
  templateUrl: './authorizeEmp.component.html'
})

export class AuthorizeEmpDialogComponent implements OnInit {
  @Output() authorizeEmpSub: EventEmitter<Object> = new EventEmitter()
    /**
     * declare variables
     */
  public isShowLock = false
  public isParentControl = false
  public channelName: any
  public playbillName: any
    /**
     * name for modules that would be used in this module
     */
  constructor (
        private translate: TranslateService
    ) { }
    /**
     * show locked page
     */
  ngOnInit () {
    let self = this
      // when play program and it have been locked or under control,show the tips
    EventService.on('SHOW_AUTHORIZE_TIP', (data) => {
        // if the channel is locked
      if (data['isLock']) {
        let name = data['channel']['name']
        if (name && name.length > 53) {
            name = name.substring(0, 53) + '...'
          }
        self.channelName = { name: name }
        self.isParentControl = false
        self.isShowLock = true
      } else if (data['isParentControl']) {
          // if the program is under control
          let name = data.playbill['name'] + ' '
          // if the name's length is more the 53,show '...'
          if (name && name.length > 53) {
            name = name.substring(0, 53) + '... '
            // if there is no program name,show 'this program'
          } else if (name === ' ') {
            name = this.translate.instant('this_program')
          }
          self.playbillName = { name: name }
          self.isShowLock = false
          self.isParentControl = true
        }
    })
  }
    /**
     * click the unLock buttom and pop up the dialog that input passowrd
     */
  unLock () {
    this.authorizeEmpSub.emit({})
    EventService.emit('openLiveDialog')
  }
}
