import * as _ from 'underscore'
import { Injectable } from '@angular/core'
import { EventService } from 'ng-epg-sdk/services'
import { CommonService } from '../../core/common.service'
import { playPVR, playNPVRHeartbeat, createBookmark, deleteBookmark, reportPVR } from 'ng-epg-sdk/vsp'
import { RecordDataService } from '../../component/recordDialog/recordData.service'
import { TimerService } from '../../shared/services/timer.service'

@Injectable()
export class NPVRAppService {
    /**
     * declare variables
     */
  private videoData = {}
  private npvrData = {}
  private playNPVRHeartbeatTime: any
    /**
     * name for modules that would be used in this module
     */
  constructor (
        private commonService: CommonService,
        private recordDataService: RecordDataService,
        private timerService: TimerService
    ) {
  }
    /**
     * destroy this page when leave.
     */
  clearDestroy () {
    if (this.playNPVRHeartbeatTime) {
      clearTimeout(this.playNPVRHeartbeatTime)
    }
  }
    /**
     * replay NPVR.
     */
  liveTvReplay (data) {
    this.playNPVR(data['npvr'])
  }

  playNPVR (npvr) {
    this.npvrData = npvr
    document.getElementsByClassName('live_load_bg')[0]['style']['display'] = 'block'
    document.querySelector('.channel_login_load_content')['style']['display'] = 'block'
    if (!Cookies.getJSON('IS_PROFILE_LOGIN')) {
      return
    }
    this.npvrCheckLock(npvr, { name: npvr.channelName, channelID: npvr.channelID })
  }

    /**
     * deal with the pvr data
     * @param {status}  string [the record task's status,reocrding, success and so on]
     * @param {channel} object [the channel info]
     * @param {judgeSub} boolean [is subcribed]
     * @param {pricedProducts} Product[]
     * @param {judgePwd} boolean [the channel or program is under control]
     */
  dealWithData (status, channel, url, judgeSub, pricedProducts?: any, judgePwd?: boolean) {
    this.videoData['channel'] = channel
    this.videoData['url'] = url
    this.videoData['type'] = 'NPVR'
    this.videoData['status'] = status
    this.videoData['judgeSub'] = judgeSub
    if (judgeSub) {
      this.videoData['pricedProducts'] = pricedProducts
    }
    this.videoData['judgePwd'] = judgePwd
  }

    /**
     * NPVR
     *  check the lock of NPVR
     * check the channel is lock and the npvr is under parent controled
     */
  npvrCheckLock (npvr, channel) {
    let self = this
    self.videoData['isLock'] = false
    self.videoData['isParentControl'] = false
    self.videoData['NoURL'] = false
    self.videoData['npvr'] = npvr
    this.npvrPlayCheckLock(npvr, null, '1').then(resp => {
        // the NPVR is not lock, playing the npvr depend on the returned url
      self.dealWithData(npvr.status, channel, resp['playURL'], false)
      self.videoData['playbill'] = npvr
      self.videoData['bookmark'] = resp['bookmark']
      EventService.emit('LOAD_FULLSCREEN_LIVETVVIDEO', self.videoData)
    }, resp => {
      if (['146020016', '146021006'].includes(resp['result']['retCode'])) { // not subcribed
        self.dealWithData(npvr.status, channel, resp['playURL'], true, resp['authorizeResult']['pricedProducts'])
        self.videoData['playbill'] = npvr
        EventService.emit('LOAD_FULLSCREEN_LIVETVVIDEO', self.videoData)
        return
      }
      if (resp.result.retCode === '111020302' || resp.result.retCode === '111020301') {
        self.dealWithData(npvr.status, channel, '', false)
        self.videoData['NoURL'] = true
        EventService.emit('LOAD_FULLSCREEN_LIVETVVIDEO', self.videoData)
      }
        // it is locked and parent control,show the check passowrd dialog
      if (resp['result'] && resp['result']['retCode'] &&
                (resp['result']['retCode'] === '146021007' || resp['result']['retCode'] === '146021008')) {
        if (resp['authorizeResult']['isLocked'] === '1' ||
                    resp['authorizeResult']['isParentControl'] === '1') {
            self.dealWithData(npvr.status, channel, resp['playURL'], false, {}, true)
            self.checkLockAndParentControl(resp, channel)
            self.videoData['playbill'] = npvr
            EventService.emit('LOAD_FULLSCREEN_LIVETVVIDEO', self.videoData)
            // input the passowrd and  verify
            EventService.removeAllListeners(['CHECK_NPVR_PASSWORD'])
            EventService.on('CHECK_NPVR_PASSWORD', function (password) {
              self.npvrCheckAuthorizeLock(npvr, channel, password)
            })
          }
      }
    })
  }
    /**
     * check lock and parrnt control.
     */
  checkLockAndParentControl (resp, channel) {
    let self = this
    if (resp['authorizeResult']['isLocked'] === '1') {
      self.videoData['isLock'] = true
    } else {
      self.videoData['isParentControl'] = true
    }
  }

    /**
     * verify the password becuase of lock and parent control
     */
  npvrCheckAuthorizeLock (npvr, channel, password) {
    let self = this
    let checkLock = {
      checkType: '1',
      password: password,
      type: '3'
    }
    return this.npvrPlayCheckLock(npvr, checkLock, '1').then(resp => {
      self.dealWithData(npvr.status, channel, resp['playURL'], false)
      self.videoData['isLock'] = false
      self.videoData['isParentControl'] = false
      self.videoData['bookmark'] = resp['bookmark']
        // load the player and playing the pvr
      EventService.emit('LOAD_FULLSCREEN_LIVETVVIDEO', self.videoData)
      EventService.emit('HIDE_POPUP_DIALOG')
    }, resp => {
        // show the error message
      EventService.emit('SHOW_NPVR_AUTHRITE_ERROR_MESSAGE')
      return resp
    })
  }

    /**
     * check the lock when playing the pvr
     */
  npvrPlayCheckLock (npvr, checkLock, isReturnProduct) {
    let self = this
    return playPVR({
      planID: npvr.ID || npvr.id,
      fileID: npvr.fileID,
      checkLock: checkLock || {
        checkType: '0'
      },
      isReturnProduct: isReturnProduct
    }).then(resp => { // location,authenticate,subscribe is done
      self.playPVRMSALog(resp, true)
      return resp
    }, resp => { // one of location,authenticate,subscribe is not content
        self.playPVRMSALog(resp, false)
        return Promise.reject(resp)
      })
  }

    /**
     * add npvr bookmark
     */
  addBookMark (time, pvr) {
    createBookmark({
      bookmarks: [{
        bookmarkType: 'NPVR',
        itemID: pvr.id,
        rangeTime: time + ''
      }]
    })
  }

    /**
     * remove the pvr bookmark
     */
  deleteBookMark (pvr) {
    deleteBookmark({
      bookmarkTypes: ['NPVR'],
      itemIDs: [pvr.id]
    })
  }

    /**
     * [startPlayNPVRHeartbeat  start the heartbeat of playing pvr]
     */
  startPlayNPVRHeartbeat (npvr) {
    clearTimeout(this.playNPVRHeartbeatTime)
    const req = {
      planID: npvr.ID || npvr.id,
      fileID: npvr.fileID
    }
    this.timerService.getCaheCustomConfig().then((config) => {
      let configData = this.timerService.getVSPCustomConfig(config)
      let time = configData['playHeartBitInterval'] * 1000 || 5 * 60 * 1000
      this.playNPVRHeartbeatTime = setTimeout(() => {
        playNPVRHeartbeat(req).then(resp => {
            if (resp.isValid === '0') {
              EventService.emit('CLOSED_FULLSCREEN_LIVETVVIDEO')
            }
          })
        this.startPlayNPVRHeartbeat(npvr)
      }, time)
    })
  }

    /**
     * report the pvr
     */
  reportPVR (action, npvr) {
    reportPVR({
      planID: npvr.id || npvr.ID,
      action: action,
      isDownload: '0',
      fileID: npvr.fileID
    })
  }

  playPVRMSALog (resp, successFlag) {
    if (_.isUndefined(resp.authorizeResult)) {
      this.commonService.commonLog('PlayPVR', 'authorizeResult')
    }
    if (successFlag && _.isUndefined(resp.playURL)) {
      this.commonService.commonLog('PlayPVR', 'playURL')
    }
  }
}
