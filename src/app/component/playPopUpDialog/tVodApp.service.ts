import * as _ from 'underscore'
import { Injectable, OnDestroy } from '@angular/core'
import { EventService } from 'ng-epg-sdk/services'
import { CommonService } from '../../core/common.service'
import { playChannel, queryPlaybillContext, createBookmark, deleteBookmark } from 'ng-epg-sdk/vsp'
import { reportChannel } from 'ng-epg-sdk/vsp'
import { playChannelHeartbeat } from 'ng-epg-sdk/vsp'
import { MediaPlayService } from '../mediaPlay/mediaPlay.service'
import { CustomConfigService } from '../../shared/services/custom-config.service'
import { ChannelCacheService } from '../../shared/services/channel-cache.service'
import { TimerService } from '../../shared/services/timer.service'
import { session } from 'src/app/shared/services/session'
@Injectable()
export class TVODAppService implements OnDestroy {
  private vdeoData = {}
  private tvodData = {}
  private playChannelHeartbeatTime: any
  private productID
  constructor (
        private commonService: CommonService,
        private mediaPlayService: MediaPlayService,
        private channelCacheService: ChannelCacheService,
        private customConfigService: CustomConfigService,
        private timerService: TimerService) {
  }

  ngOnDestroy () {
    clearTimeout(this.playChannelHeartbeatTime)
  }

    /**
     * play the catch-up TV
     */
  playTvod (playbill, channel) {
    let self = this
    document.getElementsByClassName('live_load_bg')[0]['style']['display'] = 'block'
    document.querySelector('.channel_login_load_content')['style']['display'] = 'block'
    this.tvodData = channel
    this.tvodData['playbill'] = playbill
    self.tVodCheckLock(playbill, channel, 'CUTV')
  }

    /**
     * deal with the channel and program data
     * @param {channel} channel [the channel info]
     * @param {url} string [the play url]
     * @param {judgeSub} boolean [is subcribed]
     * @param {pricedProducts} Product[]
     * @param {judgePwd} boolean [the channel or program is under control]
     */
  dealWithData (channel, url, judgeSub, pricedProducts?: any, judgePwd?: boolean) {
    if (!Cookies.getJSON('IS_PROFILE_LOGIN')) {
      this.vdeoData['guest'] = 'Guest'
      this.vdeoData['type'] = 'Authorizate'
    } else {
      this.vdeoData['guest'] = ''
      this.vdeoData['type'] = 'Authorizate'
    }
    this.vdeoData['channel'] = channel
    this.vdeoData['url'] = url
    this.vdeoData['type'] = 'TVOD'
    this.vdeoData['judgeSub'] = judgeSub
    if (judgeSub) {
      this.vdeoData['pricedProducts'] = pricedProducts
    }
    this.vdeoData['judgePwd'] = judgePwd
  }

    /**
     * catch-up TV
     * check the catch-up TV lock
     * check the channel and program is under control
     */
  tVodCheckLock (playbill, channel, businessType) {
    let self = this
    self.vdeoData['isLock'] = false
    self.vdeoData['isParentControl'] = false
    self.vdeoData['NoURL'] = false
    this.tvodPlayCheckLock(playbill, channel, businessType, null, '1').then(resp => {
        // the  catch-up TV is not lock, playing the npvr depend on the returned url
      self.dealWithData(channel, resp['playURL'], false)
      self.vdeoData['playbill'] = playbill
      self.vdeoData['bookmark'] = resp['bookmark']
      EventService.emit('LOAD_FULLSCREEN_LIVETVVIDEO', self.vdeoData)
      this.startPlayChannelHeartbeat(channel, playbill)
    }, resp => {
      if (['146020016', '146021006'].includes(resp['result']['retCode'])) {
          // not subcribed
        self.dealWithData(channel, resp['playURL'], true, resp['authorizeResult']['pricedProducts'])
        self.vdeoData['playbill'] = playbill
        EventService.emit('LOAD_FULLSCREEN_LIVETVVIDEO', self.vdeoData)
        return
      }
        // if The channel media file does not exist or the channel does not exist,show the error tips
      if (resp.result.retCode === '111020302' || resp.result.retCode === '111020301') {
        self.dealWithData(channel, '', false)
        self.vdeoData['NoURL'] = true
        EventService.emit('LOAD_FULLSCREEN_LIVETVVIDEO', self.vdeoData)
      }
        // it is locked and parent control,show the check passowrd dialog
      if (resp['result'] && resp['result']['retCode'] &&
                (resp['result']['retCode'] === '146021007' || resp['result']['retCode'] === '146021008')) {
        if (resp['authorizeResult']['isLocked'] === '1' || resp['authorizeResult']['isParentControl'] === '1') {
            self.dealWithData(channel, resp['playURL'], false, {}, true)
            self.checkLockAndParentControl(resp, channel)
            self.vdeoData['playbill'] = playbill
            EventService.emit('LOAD_FULLSCREEN_LIVETVVIDEO', self.vdeoData)
            // input the passowrd and  verify
            EventService.removeAllListeners(['CHECK_TVOD_PASSWORD'])
            EventService.on('CHECK_TVOD_PASSWORD', function (password) {
              self.tVODCheckAuthorizeLock(playbill, channel, businessType, password)
            })
          }
      }
    })
  }

  checkLockAndParentControl (resp, channel) {
    let self = this
    if (resp['authorizeResult']['isLocked'] === '1' && channel['isLocked'] === '1') {
      self.vdeoData['isLock'] = true
    } else {
      self.vdeoData['isParentControl'] = true
    }
  }

    /**
     *  check the catch-up tv lock
     */
  tVODCheckAuthorizeLock (playbill, channel, businessType, password) {
    let self = this
    let checkLock = {
      checkType: '1',
      password: password,
      type: '3'
    }
    return this.tvodPlayCheckLock(playbill, channel, businessType, checkLock, '1').then(resp => {
      self.dealWithData(channel, resp['playURL'], false)
      self.vdeoData['isLock'] = false
      self.vdeoData['isParentControl'] = false
      self.vdeoData['bookmark'] = resp['bookmark']
      EventService.emit('LOAD_FULLSCREEN_LIVETVVIDEO', self.vdeoData)
      this.startPlayChannelHeartbeat(channel, playbill)
      EventService.emit('HIDE_POPUP_DIALOG')
    }, resp => {
        // if the passowrd is not right ,show the error message
      EventService.emit('SHOW_TVOD_AUTHRITE_ERROR_MESSAGE')
      return resp
    })
  }

    /**
     * play authenticate and check the lock
     */
  tvodPlayCheckLock (playbill, channel, businessType, checkLock, isReturnProduct) {
    let self = this
    let liveTvMiniMediaID
    if (!(channel && channel['physicalChannelsDynamicProperty'] && channel['physicalChannelsDynamicProperty'].ID)) {
      let nameSpace = session.get('ott_channel_name_space')
      let physicalChannelsDynamicProperty = _.find(channel && channel['physicalChannelsDynamicProperties'], (property) => {
        return _.contains(property['channelNamespaces'], nameSpace)
      })
      liveTvMiniMediaID = physicalChannelsDynamicProperty && physicalChannelsDynamicProperty['ID']
    }
    let mediaID = channel && channel['physicalChannelsDynamicProperty'] && channel['physicalChannelsDynamicProperty'].ID ||
            liveTvMiniMediaID ||
            channel.physicalChannels && channel.physicalChannels[0] && channel.physicalChannels[0].ID

    return playChannel({
      channelID: channel.ID,
      mediaID: mediaID || '',
      playbillID: playbill.ID || '',
      businessType: businessType,
      checkLock: checkLock || {
        checkType: '0'
      },
      isReturnProduct: isReturnProduct
    }).then(resp => {
        // location,authenticate,subscribe is done
      self.mediaPlayService.sendLicense(resp.authorizeResult)
      self.productID = resp.authorizeResult && resp.authorizeResult.productID
      return resp
    }, resp => {
        // one of location,authenticate,subscribe is not content
        self.productID = resp.authorizeResult && resp.authorizeResult.productID
        return Promise.reject(resp)
      })
  }

    /**
     * play next TVOD
     */
  tvodQueryPlaybillContext (req: any) {
    return queryPlaybillContext(req).then(resp => {
      return resp
    })
  }

    /**
     * add catch-up tv bookmark
     */
  addBookMark (time, playbill) {
    createBookmark({
      bookmarks: [{
        bookmarkType: 'PROGRAM',
        itemID: playbill.ID,
        rangeTime: time + ''
      }]
    })
  }

    /**
     * remove catch-up tv bookmark
     */
  deleteBookMark (playbill) {
    deleteBookmark({
      bookmarkTypes: ['PROGRAM'],
      itemIDs: [playbill.ID]
    })
  }

    /**
     * [startPlayChannelHeartbeat start the heartbeat of palying catch-up tv]
     */
  startPlayChannelHeartbeat (channel, playbill) {
    clearTimeout(this.playChannelHeartbeatTime)
    let mediaID = channel['physicalChannelsDynamicProperty'] && channel['physicalChannelsDynamicProperty'].ID ||
            channel.physicalChannels && channel.physicalChannels[0] && channel.physicalChannels[0].ID
    const req = { channelID: channel.ID, playbillID: playbill.ID, mediaID: mediaID || '' }
    this.timerService.getCaheCustomConfig().then((config) => {
      let configData = this.timerService.getVSPCustomConfig(config)
      let time = configData['playHeartBitInterval'] * 1000 || 5 * 60 * 1000
      this.playChannelHeartbeatTime = setTimeout(() => {
        playChannelHeartbeat(req).then(resp => {
            if (resp.isValid === '0') {
              EventService.emit('CLOSED_FULLSCREEN_LIVETVVIDEO')
            }
          })
        this.startPlayChannelHeartbeat(channel, playbill)
      }, time)
    })
  }

    /**
     * report catch-up tv
     */
  reportChannel (action, channelDetail, productID?) {
    let self = this
    let businessType = 'CUTV'
    let productId = self.productID || productID || ''
    let mediaID = channelDetail['physicalChannelsDynamicProperty'] && channelDetail['physicalChannelsDynamicProperty'].ID ||
            channelDetail.physicalChannels && channelDetail.physicalChannels[0] && channelDetail.physicalChannels[0].ID
    reportChannel({
      subjectID: channelDetail && channelDetail['subjectIDs'] && channelDetail['subjectIDs'][0]
          ? channelDetail['subjectIDs'][0] : '-1',
      playbillID: channelDetail['playbill'] && channelDetail['playbill']['ID'],
      action: action,
      channelID: channelDetail['ID'],
      mediaID: mediaID || '',
      businessType: businessType,
      productID: productId
    })
  }
}
