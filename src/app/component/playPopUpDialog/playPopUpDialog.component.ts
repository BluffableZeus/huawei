import { Component, ViewChild, OnInit, OnDestroy } from '@angular/core'
import { EventService } from 'ng-epg-sdk/services'
import { AccountLoginComponent } from '../../login/account-login'
import { CommonService } from '../../core/common.service'
import { SubscriberDialogComponent } from './subscriber/subscriber.component'
import { SubscrEmpDialogComponent } from './subscrEmp/subscrEmp.component'
import { FullScreenPassDialogComponent } from './fullScreenPassInput/fullScreenPass-input-dialog.component'
import { GuestDialogComponent } from '../live-tv-guest/livetv-guest.component'
import { PictureService } from 'ng-epg-ui/services/picture.service'
import { session } from 'src/app/shared/services/session'

@Component({
  selector: 'app-play-popup-dialog',
  templateUrl: './playPopUpDialog.component.html',
  styleUrls: ['./playPopUpDialog.component.scss'],
  providers: [PictureService]
})

export class PlayPopUpDialogComponent implements OnInit, OnDestroy {
    /**
     * declare variables
     */
  public liveTvVolumeSeekBarID = 'liveTvVolumeSeekBar'
  public livetvVideoWidth = window.screen.width + 'px'
  public livetvVideoHeight = window.screen.height + 'px'
  public isLiveVolumeBarDivShow: Boolean = false
  public empSubData = {}
  public empAuthorizeData = {}
  public profileData
  public isShowLiveDefinitionDetail = false
  public isJudgeSubscrEmp = false
  public isLivetvSettingMenu = false

  public judgeWeakTip = false
  public judgeAuthorizeEmp = false
  public judgeSubscriber = false
  public judgeSubscrEmp = false
  public judgeFullScreenPwd = false
  public judgeGuest = false

  public getWeakTip

  public judgePlayFailLiveTv = false

  @ViewChild(SubscriberDialogComponent) subscriberDialog: SubscriberDialogComponent
  @ViewChild(SubscrEmpDialogComponent) subscrEmpDialog: SubscrEmpDialogComponent
  @ViewChild(FullScreenPassDialogComponent) fullScreenPassDialog: FullScreenPassDialogComponent
  @ViewChild(GuestDialogComponent) guestDialog: GuestDialogComponent

    /**
     * name for modules that would be used in this module
     */
  constructor (
        private commonService: CommonService) {
      // event for hidden popup dialog
    EventService.on('HIDE_POPUP_DIALOG', () => {
      this.judgeHideDialog('')
    })
  }

    /**
     * initialization
     */
  ngOnInit () {
      // show the setting menu
    EventService.on('SHOW_LIVETVSETTINGMENU', value => {
      this.isLivetvSettingMenu = parseInt(value, 10) === 1
    })
      // exit the full screen and close the miniEPG and all other dialog
    EventService.on('CLOSED_FULLSCREEN_LIVETVVIDEO', () => {
      EventService.emit('CLOSE_MINIEPG')
      this.judgeHideDialog('')
    })
      // hide the setting menu
    EventService.on('CLOSE_LIVESETTING', () => {
      this.isLivetvSettingMenu = false
    })
      // show the volume bar
    EventService.on('showLivetvVolumeBar', value => {
      this.isLiveVolumeBarDivShow = parseInt(value, 10) === 1
    })
      // show the definition list
    EventService.on('showLivetvDefinitionDetail', value => {
      this.isShowLiveDefinitionDetail = parseInt(value, 10) === 1
    })
  }

    /**
     * destroy this page when leave
     */
  ngOnDestroy () {
    if (this.getWeakTip) {
      clearTimeout(this.getWeakTip)
    }
  }

    /**
     * do this when play filed
     */
  setPopUpPlayLiveTvFail () {
    this.judgeHideDialog('playLiveTvfail')
    document.getElementById('LiveTvPlayFailBg').style.width = this.livetvVideoWidth
    document.getElementById('LiveTvPlayFailBg').style.height = this.livetvVideoHeight
    document.getElementById('LiveTvPlayFailBg')['style']['top'] = (window.screen.height - 288) / 2 + 'px'
  }

    /**
     * show the tips of subcribe
     */
  setPopUpSubscrEmp (subData) {
    this.empSubData = subData
    this.subscrEmpDialog.getDate(subData['channel']['name'])
    this.isJudgeSubscrEmp = true
    this.judgeHideDialog('SubscrEmp')
    document.getElementById('subscrEmp').style.width = this.livetvVideoWidth
    document.getElementById('subscrEmp').style.height = this.livetvVideoHeight
    document.getElementById('subscrEmp')['style']['top'] = (window.screen.height - 288) / 2 + 'px'
    if (session.get('NEED_TO_BUY') === 'buy') {
      this.setPopUpSubscriber()
      session.remove('NEED_TO_BUY')
    }
  }

    /**
     * show the subscribe list
     */
  setPopUpSubscriber () {
    this.judgeHideDialog('Subscriber')
    this.isJudgeSubscrEmp = true
    document.getElementById('subDialog').style.width = this.livetvVideoWidth
    document.getElementById('subDialog').style.height = this.livetvVideoHeight
    document.getElementById('subDialog')['style']['top'] = (window.screen.height - 450) / 2 + 'px'
    this.subscriberDialog.setPriceProducts(this.empSubData['pricedProducts'])
  }

    /**
     * show the password check dialog
     */
  subProductPackage (product) {
    this.judgeHideDialog('FullScreenPwd')
    document.getElementById('pwdDialog').style.width = this.livetvVideoWidth
    document.getElementById('pwdDialog').style.height = this.livetvVideoHeight
    document.getElementById('pwdDialog')['style']['top'] = (window.screen.height - 329) / 2 + 'px'
    let products = {
      product: product,
      channel: this.empSubData['channel'],
      type: 'Subscribe',
      playbillType: this.empSubData['type'],
      playbill: this.empSubData['playbill']
    }
    this.fullScreenPassDialog.setMessage(products)
  }

    /**
     * show the channel or program been locked or under parent control
     */
  setPopUpAuthorizeEmp (subData) {
    this.empAuthorizeData = subData
    EventService.emit('SHOW_AUTHORIZE_TIP', this.empAuthorizeData)
    this.judgeHideDialog('AuthorizeEmp')
      // set the postion of box
    document.getElementById('authorizeEmp').style.width = this.livetvVideoWidth
    document.getElementById('authorizeEmp').style.height = this.livetvVideoHeight
    document.getElementById('authorizeEmp')['style']['top'] = (window.screen.height - 288) / 2 + 'px'
  }

    /**
     * show the dialog of check password because of locked or under parent control
     */
  setAuthorizate () {
    let product = this.empAuthorizeData
    this.isJudgeSubscrEmp = false
    this.judgeHideDialog('AuthorizeFullScreenPwd')
    document.getElementById('pwdDialog').style.width = this.livetvVideoWidth
    document.getElementById('pwdDialog').style.height = this.livetvVideoHeight
    document.getElementById('pwdDialog')['style']['top'] = (window.screen.height - 329) / 2 + 'px'
    let products = {
      product: {},
      channel: product['channel'],
      type: 'Authorizate',
      playbillType: product['type'],
      playbill: product['playbill'],
      isLock: product['isLock'],
      isParentControl: product['isParentControl']
    }
      // send some info to the check dialog
    this.fullScreenPassDialog.setMessage(products)
  }

    /**
     * when the channel or program haven been checked or subscribed,playing the content
     */
  enterProfilePwd (data) {
      // if subscribe failed ,show the weakTip tips which color is red
    if (data['type'] === 'subscribeFailed') {
      this.judgeHideDialog('SubscrEmp')
    } else {
      this.judgeFullScreenPwd = false
      EventService.emit('LOAD_FULLSCREEN_LIVETVVIDEO', data)
    }
  }

  setGuestInfo (profile) {
    this.profileData = profile
  }

    /**
     * show the tips for login
     */
  setProfileGuest (profile) {
    this.profileData = profile
    this.judgeHideDialog('Guest')
    document.getElementById('guestDialog').style.width = this.livetvVideoWidth
    document.getElementById('guestDialog').style.height = this.livetvVideoHeight
    document.getElementById('guestDialog')['style']['top'] = (window.screen.height - 329) / 2 + 'px'
    this.guestDialog.setMessage(profile)
  }

    /**
     * show the login dialog
     */
  guestLogin () {
    EventService.emit('CLOSED_LOGIN_DIALOG')
    if (this.profileData['preview']) {
      this.profileData['guest'] = 'Guest'
    }
    this.commonService.openDialog(AccountLoginComponent, null, this.profileData).then(resp => {
      if (Cookies.getJSON('IS_PROFILE_LOGIN')) {
        this.judgeGuest = false
      }
    })
  }

    /**
     * close the dialog according to need
     */
  closeDialog (type) {
    if (type === 'subscriber') {
      this.judgeSubscrEmp = true
      this.judgeSubscriber = false
      this.judgeFullScreenPwd = false
    } else if (type === 'password') {
      if (this.isJudgeSubscrEmp) {
          this.judgeSubscrEmp = false
          this.judgeSubscriber = true
          this.subscriberDialog.subscribeTime()
          this.judgeFullScreenPwd = false
        } else {
          this.judgeSubscrEmp = false
          this.judgeSubscriber = false
          this.judgeFullScreenPwd = false
          this.judgeAuthorizeEmp = true
        }
    } else if (type === 'EmpSub') {
        this.judgeSubscrEmp = false
        this.judgeSubscriber = false
        this.judgeFullScreenPwd = false
      } else if (type === 'GuestLogin') {
        this.judgeGuest = false
        this.judgeSubscrEmp = false
        this.judgeSubscriber = false
        this.judgeFullScreenPwd = false
      }
    EventService.emit('closeLiveDialog')
  }

    /**
     * show the dialog according to need
     */
  judgeHideDialog (type) {
    this.judgeSubscrEmp = false
    this.judgeWeakTip = false
    this.judgeAuthorizeEmp = false
    this.judgeSubscriber = false
    this.judgeFullScreenPwd = false
    this.judgeGuest = false
    switch (type) {
      case 'SubscrEmp':
        this.judgeSubscrEmp = true
        break
      case 'Subscriber':
        this.judgeSubscriber = true
        break
      case 'FullScreenPwd':
        this.judgeFullScreenPwd = true
        break
      case 'AuthorizeEmp':
        this.judgeAuthorizeEmp = true
        break
      case 'AuthorizeFullScreenPwd':
        this.judgeFullScreenPwd = true
        break
      case 'Guest':
        this.judgeGuest = true
        break
      case 'WeakTip':
        this.judgeWeakTip = true
        break
      case 'playLiveTvfail':
        this.judgePlayFailLiveTv = true
        break
      default:
        break
    }
  }

    /**
    * show the setting menu
    */
  showLivetvSettingMenu () {
    this.isLivetvSettingMenu = true
  }

    /**
    * close the setting menu
    */
  closeLivetvSettingMenu () {
    this.isLivetvSettingMenu = false
  }

    /**
    * show the volume bar
    */
  showVolumeBar () {
    this.isLiveVolumeBarDivShow = true
  }

    /**
    * close the volume bar
    */
  hiddenVolumeBar () {
    this.isLiveVolumeBarDivShow = false
  }

    /**
    * set the volume value
    */
  volumeValue (volume) {
    EventService.emit('liveVolumeBarValue', volume)
  }

    /**
    * show the definition list
    */
  showLiveDefinitionDetail () {
    this.isShowLiveDefinitionDetail = true
  }

    /**
    * close the definition list
    */
  hideLiveDefinitionDetail () {
    this.isShowLiveDefinitionDetail = false
  }
}
