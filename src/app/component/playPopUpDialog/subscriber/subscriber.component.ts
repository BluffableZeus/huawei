import * as _ from 'underscore'
import { Component, ElementRef, ViewChild, Output, EventEmitter, OnDestroy } from '@angular/core'
import { session } from 'src/app/shared/services/session'
import { DirectionScroll } from 'ng-epg-ui/webtv-components/scroll/directionScroll.component'
import { EventService } from 'ng-epg-sdk/services'
import { TranslateService } from '@ngx-translate/core'
import { CommonService } from '../../../core/common.service'

@Component({
  selector: 'app-subscriber-dialog',
  styleUrls: ['./subscriber.component.scss'],
  templateUrl: './subscriber.component.html'
})

export class SubscriberDialogComponent implements OnDestroy {
  @ViewChild('products') productDom: ElementRef
  @Output() subProduct: EventEmitter<Object> = new EventEmitter()
  @Output() closeCurrentDialog: EventEmitter<Object> = new EventEmitter()
    // declare variables
  public count = 1
  public panel: number
  public offsetNumber = 0
  public priceLeftVisible = 'hidden'
  public priceRightVisible = 'visible'
  public page: number
  public pricedProducts = []
  public pricedProduct: any = {}
  public subscribeLiveTimeOut
  public lastProduct: any = {}
  public currencySymbol = ''
    /**
     * name for modules that would be used in this module
     */
  constructor (
        private directionScroll: DirectionScroll,
        private translate: TranslateService,
        private commonService: CommonService
    ) {
  }
    /**
     * when exit the component, clear all the timer and events
     */
  ngOnDestroy () {
    clearTimeout(this.subscribeLiveTimeOut)
  }

  subscribeTime () {
    let self = this
    document.getElementById('livetv_sub_scrollSpan')['style']['top'] = '0px'
    document.getElementById('livetv_sub_scrollSpan')['style']['height'] = '0px'
    document.getElementById('detail_content')['style']['top'] = '0px'
    document.getElementById('livetv_sub_scrollList')['style']['display'] = 'none'
    clearTimeout(self.subscribeLiveTimeOut)
    self.subscribeLiveTimeOut = setTimeout(() => {
      if (document.getElementById('detail_content')['offsetHeight'] > 145) {
        document.getElementById('livetv_sub_scrollList')['style']['display'] = 'block'
        self.subscribeScroll()
      } else {
        document.getElementById('livetv_sub_scrollList')['style']['display'] = 'none'
      }
    }, 100)
  }
    /**
     * when change product
     */
  changeProduct (product) {
    if (!_.isEqual(product, this.lastProduct)) {
      this.pricedProduct = product
      this.lastProduct = product
      this.subscribeTime()
    }
  }
    /**
     * set price products
     */
  setPriceProducts (pricedProduct) {
    this.productDom.nativeElement.style.transform = 'translate(0px,0px)'
    this.offsetNumber = 0
    this.currencySymbol = session.get('CURRENCY_SYMBOL') || ''
    let pricesProduct = _.map(pricedProduct, (product) => {
      let lastTime = this.commonService.dealWithDateJson('D02', product['endTime'])
      return {
        ID: product && product['ID'],
        name: product && product['name'],
        price: product && product['price'] && ' ' +
                (product['price'] / session.get('CURRENCY_RATE')).toFixed(2) || '0',
        duration_subscruber: {
            rentPeriod: product && product['rentPeriod'] || '',
            endTime_day_year_string: lastTime || '',
            endTime_hour_mins_string: this.getEndTimeStr(product, 'HH:mm:ss')
          },
        introduce: product && product['introduce'],
        priceObject: product['priceObject']
      }
    })
    this.pricedProduct = pricesProduct[0]
    this.lastProduct = pricesProduct[0]
    this.pricedProducts = pricesProduct
    this.pricedLiveTVProductsResult()
    this.subscribeTime()
  }
    /**
     * get end time
     */
  getEndTimeStr (product, style: string) {
    let timeStr = product && this.commonService.dealWithDateJson('D09', product['endTime']) || ''
    return timeStr
  }
    /**
     * priced livetv product result
     */
  pricedLiveTVProductsResult () {
    this.page = Math.floor(this.pricedProducts['length'] / 4)
    if (this.pricedProducts['length'] % 4 === 0) {
      this.page -= 1
    }
    if (this.pricedProducts['length'] <= 4) {
      this.priceLeftVisible = 'hidden'
      this.priceRightVisible = 'hidden'
    } else {
      this.priceLeftVisible = 'hidden'
      this.priceRightVisible = 'visible'
    }
  }

  onClickLiveTVLeft () {
    this.count--
    let offsetWidth: string
    let clientW: number = document.body.clientWidth
    if (clientW > 1440) {
      this.panel = 680
    } else {
      this.panel = 680
    }
    const MINSFFSETNUMBER = 0
    if (this.offsetNumber <= MINSFFSETNUMBER) {
      this.offsetNumber += this.panel
      offsetWidth = this.offsetNumber + 'px'
      this.productDom.nativeElement.style.transform = 'translate(' + offsetWidth + ',0px)'
      this.productDom.nativeElement.style.transition = 'all 1s linear'
      this.isShowIcon(this.offsetNumber)
    }
  }

  onClickLiveTVRight () {
    this.count++
    let offsetWidth: string
    let clientW: number = document.body.clientWidth
    if (clientW > 1440) {
      this.panel = 680
    } else {
      this.panel = 680
    }
    let maxWidth: number = -(this.panel * (this.page - 1))
    if (this.offsetNumber >= maxWidth) {
      this.offsetNumber -= this.panel
      offsetWidth = this.offsetNumber + 'px'
      this.productDom.nativeElement.style.transform = 'translate(' + offsetWidth + ',0px)'
      this.productDom.nativeElement.style.transition = 'all 1s linear'
      this.isShowIcon(this.offsetNumber)
    }
  }
    /**
     * decide if show icon or not
     */
  isShowIcon (offsetNumber: number) {
    if (offsetNumber === 0) {
      this.priceLeftVisible = 'hidden'
      this.priceRightVisible = 'visible'
    } else if (offsetNumber === -(this.panel * this.page)) {
      this.priceLeftVisible = 'visible'
      this.priceRightVisible = 'hidden'
    } else {
      this.priceLeftVisible = 'visible'
      this.priceRightVisible = 'visible'
    }
  }
    /**
     * get dom element
     */
  dom (divName: string) {
    return document.getElementById(divName)
  }

  subLiveTVEmpProduct (product) {
    this.subProduct.emit(product)
  }
    /**
     * close livetv
     */
  closeLiveTV () {
    this.closeCurrentDialog.emit('subscriber')
    if (session.get('ISCURRENT') === false) {
      EventService.emit('EXIT_FULLSCREEN')
      EventService.emit('CLOSED_FULLSCREEN_LIVETVVIDEO')
    }
  }
    /**
     * set subscribe scroll
     */
  subscribeScroll () {
    let oConter, oUl, oScroll, oSpan
    let oBox = document.getElementById('livetv-subscriber-dialog')
    oConter = document.getElementById('bottom')
    oUl = document.getElementById('detail_content')
    oScroll = document.getElementById('livetv_sub_scroll')
    if (oScroll) {
      oSpan = oScroll.getElementsByTagName('span')[0]
    }
    this.directionScroll.setScroll(oBox, oConter, oUl, oScroll, oSpan, true)
  }

    /**
     * set the style of LastTime
     */
  setLastTime (year, month, day) {
    let lastTime
    if (session.get('languageName') === 'zh') {
      lastTime = year + '/' + month + '/' + day
    } else {
      lastTime = month + ' ' + day + ', ' + year
    }
    return lastTime
  }
}
