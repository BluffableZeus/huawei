import { Component } from '@angular/core'
import { TranslateService } from '@ngx-translate/core'
import { EventService } from 'ng-epg-sdk/services'

@Component({
  selector: 'app-playlivetvfail-background',
  styleUrls: ['./playFailLiveTv.component.scss'],
  templateUrl: './playFailLiveTv.component.html'
})

export class PlayLiveTvFailBgComponent {
  constructor (
        private translate: TranslateService) {
  }

  refresh () {
    EventService.emit('PLAY_LiveTv_FAIL_AND_REFRESH')
  }
}
