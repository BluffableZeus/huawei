import * as _ from 'underscore'
import { Component, Output, EventEmitter, OnInit } from '@angular/core'
import { EventService, HeartbeatService } from 'ng-epg-sdk/services'
import { playChannel, playPVR } from 'ng-epg-sdk/vsp'
import { session } from 'src/app/shared/services/session'
import { checkPassword, subscribeProduct } from 'ng-epg-sdk/vsp'
import { TranslateService } from '@ngx-translate/core'
import { MediaPlayService } from '../../mediaPlay/mediaPlay.service'
import { ChannelCacheService } from '../../../shared/services/channel-cache.service'
import { TVODAppService } from '../tVodApp.service'
import { PlaybillAppService } from '../playbillApp.service'
import { NPVRAppService } from '../nPvrApp.service'
import { CustomConfigService } from '../../../shared/services/custom-config.service'
@Component({
  selector: 'app-password-dialog',
  templateUrl: './fullScreenPass-input-dialog.component.html',
  styleUrls: ['./fullScreenPass-input-dialog.component.scss']
})

export class FullScreenPassDialogComponent implements OnInit {
  @Output() enterProPwd: EventEmitter<Object> = new EventEmitter()
  @Output() closePINDialog: EventEmitter<Object> = new EventEmitter()
    /**
     * declare variables.
     */
  public epmProduct = {}
  public channel = {}
  public deletePassWord = false
  public userPassword = ''
  public errorMessage = false
  public emptyPasswordMessage = false
  public contentInfo = ''
  public types = ''
  public playbillType = ''
  public playbill = {}
  public isLock = false
  public isParentControl = false
  public showplaceholderLive = true
  public passLoadDisplay = false
  public configData = {}
  public forgetpassword = false
  public errorlivefocus = true
  public forgot_password_url = ''
  public priceObjects = []
    /**
     * name for modules that would be used in this module
     */
  constructor (
        private translate: TranslateService,
        private mediaPlayService: MediaPlayService,
        private channelCacheService: ChannelCacheService,
        private playbillAppService: PlaybillAppService,
        private nPVRAppService: NPVRAppService,
        private tVODAppService: TVODAppService,
        private customConfigService: CustomConfigService,
        private heartbeatService: HeartbeatService
    ) {
  }
    /**
     * initialization
     */
  ngOnInit () {
    EventService.removeAllListeners(['HIDE_LIVETV_LOAD'])
    EventService.on('HIDE_LIVETV_LOAD', () => {
      this.passLoadDisplay = false
    })
    EventService.on('openLiveDialog', () => {
      this.errorlivefocus = true
    })
  }
    /**
     * check the password background to hide the background,then the input get focus.
     */
  onplaceholdLive () {
    if (this.userPassword === '' || this.userPassword === undefined) {
      _.delay(() => {
        document.getElementById('pass-Wordsss').focus()
      }, 100)
    }
  }
    /**
     *  get the type of the pass input.
     */
  getType (type) {
      // the pass input get focus when open the dialog at first.
    _.delay(() => {
      if (document.getElementById('pass-Wordsss')) {
        document.getElementById('pass-Wordsss').focus()
      }
    }, 600)
    this.configData = session.get('CONFIG_DATAS') || {}
      // if the forget password url exits,show the forget password button.
    if (this.configData['forgot_password_url']) {
      this.forgetpassword = true
      this.forgot_password_url = this.configData['forgot_password_url']
    }
    switch (type) {
        // the playbill should be subscribed.
      case 'Subscribe':
        this.contentInfo = this.translate.instant('subscription_check_password')
        break
          // the playbill is locked or parental control.
      case 'Authorizate':
        if (this.isLock) {
          this.contentInfo = this.translate.instant('enter_super_profile')
        } else if (this.isParentControl) {
            let name = this.playbill['name'] + ' '
            if (name && name.length > 30) {
              name = name.substring(0, 30) + '... '
            } else if (name === ' ') {
              name = this.translate.instant('this_program')
            }
            this.contentInfo = this.translate.instant('ChannelRatePWD', { name: name })
          }
        break
      default:
        break
    }
  }
    /**
     *  deal with the data.
     */
  setMessage (products) {
    this.errorMessage = false
    this.emptyPasswordMessage = false
    this.passLoadDisplay = false
    this.removePassWord()
    this.channel = products['channel']
    this.epmProduct = products['product']
    this.types = products['type']
    this.playbillType = products['playbillType']
    this.isLock = products['isLock']
    this.isParentControl = products['isParentControl']
    this.playbill = products['playbill'] || {}
    this.priceObjects = []
    if (products['product']['priceObject']) {
      this.priceObjects = [products['product']['priceObject']]
    }
    this.getType(products['type'])
  }
    /**
     *  change the password
     */
  changePassword (e) {
    if (e.keyCode === 13) {
      document.getElementById('confirmBut').click()
    }
    if (this.userPassword !== '') {
      this.showplaceholderLive = false
      this.deletePassWord = true
    } else {
      this.deletePassWord = false
      this.showplaceholderLive = true
    }
  }
    /**
     *  judge the status of delete buttom and the placeholder.
     */
  showPassword () {
    if (this.userPassword !== '') {
      this.deletePassWord = true
      this.showplaceholderLive = false
    }
    if (this.userPassword === '') {
      this.showplaceholderLive = true
    }
  }
    /**
     *  the pass input lose the focus.
     */
  hidePassword () {
    let self = this
    if (this.userPassword === '') {
      this.showplaceholderLive = true
    }
    _.delay(function () {
      self.deletePassWord = false
    }, 300)
  }
    /**
     *  click the delete buttom of the pass input.
     */
  removePassWord () {
    this.userPassword = ''
    document.getElementById('pass-Wordsss').focus()
  }
    /**
     *  check subscribing password
     */
  subscribePwdConfirm () {
    let self = this
    let subRequest = {
      subscribe: {
        subscribeType: '0',
        productID: self.epmProduct['ID'],
        payment: {},
        isAutoExtend: '0'
      },
      correlator: this.userPassword,
      type: '1'
    }
    if (this.priceObjects.length > 0) {
      subRequest.subscribe['priceObjects'] = this.priceObjects
    }
    subscribeProduct(subRequest).then(resps => {
      EventService.emit('CHECK_PASSWORD', resps)
      if (this.playbillType === 'PLTV') {
        self.playCheckLock('PLTV', '1').then(respss => {
            let sucessSub = {
              subscribePWD: 'subscribeSuccess',
              type: 'PLTV',
              url: respss['playURL'],
              channel: self.channel,
              playbill: self.playbill,
              isLock: false,
              isParentControl: false
            }
            self.enterProPwd.emit(sucessSub)
            this.playbillAppService.reportChannel('0', 'PLTV', self.channel)
            EventService.emit('closeLiveDialog')
          })
      } else if (this.playbillType === 'TVOD') {
          self.playCheckLock('CUTV', '1').then(respss => {
            let sucessSub = {
              subscribePWD: 'subscribeSuccess',
              type: 'TVOD',
              url: respss['playURL'],
              channel: self.channel,
              playbill: self.playbill,
              isLock: false,
              isParentControl: false,
              productID: self.epmProduct['ID']
            }
            self.enterProPwd.emit(sucessSub)
            EventService.emit('closeLiveDialog')
          })
        } else if (this.playbillType === 'NPVR') {
          self.playPvrCheckLock('1').then(respss => {
            let sucessSub = {
              subscribePWD: 'subscribeSuccess',
              type: 'NPVR',
              url: respss['playURL'],
              channel: self.channel,
              playbill: self.playbill,
              isLock: false,
              isParentControl: false
            }
            self.enterProPwd.emit(sucessSub)
            this.nPVRAppService.reportPVR('0', self.playbill)
            EventService.emit('closeLiveDialog')
          })
        } else {
          self.playCheckLock('BTV', '0').then(respss => {
            let sucessSub = {
              subscribePWD: 'subscribeSuccess',
              type: 'LiveTV',
              url: respss['playURL'],
              channel: self.channel,
              playbill: self.playbill,
              isLock: false,
              isParentControl: false
            }
            self.enterProPwd.emit(sucessSub)
            this.playbillAppService.reportChannel('0', 'BTV', self.channel)
            EventService.emit('closeLiveDialog')
          })
        }
        // subscribe successfully and refresh the other channel's subscribe relationship
      this.heartbeatService.startHeartbeat()
    }, resps => {
      self.userPassword = ''
      self.showplaceholderLive = true
      self.deletePassWord = false
      self.errorMessage = true
      self.emptyPasswordMessage = false
      self.passLoadDisplay = false
      document.getElementById('pass-Wordsss').focus()
    })
  }
    /**
     *  click the confirm button to check the password.
     */
  confirm () {
    let self = this
    this.errorlivefocus = false
    if (this.userPassword === '') {
      this.emptyPasswordMessage = true
      this.errorMessage = false
      self.passLoadDisplay = false
      document.getElementById('pass-Wordsss').focus()
      return
    }
    self.passLoadDisplay = true
    if (this.types === 'Subscribe') {
      this.subscribePwdConfirm()
    } else if (this.types === 'Authorizate') {
      if (this.playbillType === 'TVOD') {
          EventService.on('SHOW_TVOD_AUTHRITE_ERROR_MESSAGE', function () {
            self.userPassword = ''
            self.showplaceholderLive = true
            self.deletePassWord = false
            self.errorMessage = true
            self.emptyPasswordMessage = false
            self.passLoadDisplay = false
            document.getElementById('pass-Wordsss').focus()
          })
          EventService.emit('CHECK_TVOD_PASSWORD', this.userPassword)
        } else if (this.playbillType === 'LiveTV') {
          EventService.on('SHOW_LIVETV_AUTHRITE_ERROR_MESSAGE', function () {
            self.userPassword = ''
            self.showplaceholderLive = true
            self.deletePassWord = false
            self.errorMessage = true
            self.emptyPasswordMessage = false
            self.passLoadDisplay = false
            document.getElementById('pass-Wordsss').focus()
          })
          EventService.emit('CHECK_LIVETV_PLAY_PASSWORD', this.userPassword)
        } else if (this.playbillType === 'NPVR') {
          EventService.on('SHOW_NPVR_AUTHRITE_ERROR_MESSAGE', function () {
            self.userPassword = ''
            self.showplaceholderLive = true
            self.deletePassWord = false
            self.errorMessage = true
            self.passLoadDisplay = false
            document.getElementById('pass-Wordsss').focus()
          })
          EventService.emit('CHECK_NPVR_PASSWORD', this.userPassword)
        } else if (this.playbillType === 'NextBTV' || this.playbillType === 'PLTV') {
          checkPassword({
            password: this.userPassword,
            type: '0'
          }).then(resp => {
            let playbillids = []
            if (session.get('unlocked_playbill_id')) {
              playbillids = session.get('unlocked_playbill_id')
            }
            playbillids.push(this.playbill['ID'])
            session.put('unlocked_playbill_id', playbillids)
            if (this.playbillType === 'NextBTV') {
              EventService.emit('LOCAL_AUTHORIZATE')
            } else {
              EventService.emit('CHECK_PLTV_PLAY_PASSWORD')
            }
          }, resp => {
            self.userPassword = ''
            self.showplaceholderLive = true
            self.deletePassWord = false
            self.errorMessage = true
            self.emptyPasswordMessage = false
            self.passLoadDisplay = false
            document.getElementById('pass-Wordsss').focus()
          })
        }
    }
  }

    /**
     * finish subscribing then call playchannel to  authenticate again
     */
  playCheckLock (businessType, isReturnProduct) {
    let self = this
    let mediaID = this.channel['physicalChannelsDynamicProperty'] && this.channel['physicalChannelsDynamicProperty'].ID ||
            this.channel['physicalChannels'] && this.channel['physicalChannels'][0] && this.channel['physicalChannels'][0].ID

    return playChannel({
      channelID: this.channel['ID'],
      mediaID: mediaID || '',
      businessType: businessType,
      isReturnProduct: isReturnProduct
    }).then(resp => {
      self.mediaPlayService.sendLicense(resp.authorizeResult)
      return resp
    }, resp => {
        return Promise.reject(resp)
      })
  }
    /**
     *  finish subscribing then call playPVR to  authenticate again
     */
  playPvrCheckLock (isReturnProduct) {
    return playPVR({
      fileID: this.playbill['fileID'],
      planID: this.playbill['id'],
      isReturnProduct: isReturnProduct
    }).then(resp => {
      return resp
    }, resp => {
        return Promise.reject(resp)
      })
  }
    /**
     * close the dialog.
     */
  close () {
    this.errorlivefocus = true
    this.errorMessage = false
    this.emptyPasswordMessage = false
    this.passLoadDisplay = false
    this.removePassWord()
    this.closePINDialog.emit('password')
  }
    /**
     * when the key down,hidden the placeholder.
     */
  keydownPassword () {
    this.showplaceholderLive = false
  }

  btnStyleChange () {
    if ((document.getElementById('pass-Wordsss')['value'] !== '')) {
      return true
    } else {
      return false
    }
  }
}
