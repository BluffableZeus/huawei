import * as _ from 'underscore'
import { Injectable } from '@angular/core'
import { environment } from '../../../environments/environment';
import { EventService } from 'ng-epg-sdk/services'
import { session } from 'src/app/shared/services/session'
import { CommonService } from '../../core/common.service'
import { queryPlaybillList, playChannel } from 'ng-epg-sdk/vsp'
import { GuestPlayAppService } from '../live-tv-guest/guestPlayApp.service'
import { reportChannel } from 'ng-epg-sdk/vsp'
import { playChannelHeartbeat } from 'ng-epg-sdk/vsp'
import { MediaPlayService } from '../mediaPlay/mediaPlay.service'
import { ChannelCacheService } from '../../shared/services/channel-cache.service'
import { CustomConfigService } from '../../shared/services/custom-config.service'
import { TimerService } from '../../shared/services/timer.service'

@Injectable()
export class PlaybillAppService {
    /**
     * declare variables
     */
  public previewHistoryList = []
  private videoData = {}
  private playChannelHeartbeatTime: any
  private playbill = ''
  private channelInfo = ''
  private isHeartBeatPlay = {}
  private subscriberpricedProducts = {}
  private productID
    /**
     * name for modules that would be used in this module
     */
  constructor (
        private commonService: CommonService,
        private guestPlayAppService: GuestPlayAppService,
        private mediaPlayService: MediaPlayService,
        private channelCacheService: ChannelCacheService,
        private customConfigService: CustomConfigService,
        private timerService: TimerService) {
    this.previewHistoryList = session.get('PREVIED_HISTORY_LIST') || []
  }
    /**
     * when exit the component, clear all the timer and events
     */
  clearDestroy () {
    clearInterval(this.playChannelHeartbeatTime)
  }

    /**
     * play the program
     */
  playProgram (playbill, channel, player?: any) {
    document.getElementsByClassName('live_load_bg')[0]['style']['display'] = 'block'
    if (!Cookies.getJSON('IS_PROFILE_LOGIN')) {
      this.guestPlayAppService.guestPlayProgram(playbill, channel)
      return
    }
    this.isHeartBeatPlay = player
    this.playbill = playbill
    this.channelInfo = channel
    this.subscriberpricedProducts = {}
    this.liveTVCheckLock(channel, 'BTV')
  }

    /**
     * deal with the channel and program data
     * @param {channel} channel [the channel info]
     * @param {string} url [the play url]
     * @param {boolean} judgeSub [is subcribed]
     * @param {Product[]} pricedProducts
     * @param {boolean} judgePwd [the channel or program is under control]
     */
  dealWithData (channel, url, judgeSub, pricedProducts?: any, judgePwd?: boolean) {
    this.videoData['channel'] = channel
    this.videoData['url'] = url
    this.videoData['type'] = 'LiveTV'
    this.videoData['judgeSub'] = judgeSub
    this.videoData['pltvCR'] = channel['physicalChannelsDynamicProperty'] &&
            channel['physicalChannelsDynamicProperty'].pltvCR || channel['physicalChannels'][0]['pltvCR']
    this.videoData['playbill'] = this.playbill
    if (judgeSub) {
      this.videoData['pricedProducts'] = pricedProducts
    }
    this.videoData['judgePwd'] = judgePwd
  }

    /**
     * if the channel is not subcribed,show the subcribe tip or play the preview
     */
  livetvUnSubscriber (resp, channel) {
    this.dealWithData(channel, resp['playURL'], true, resp['authorizeResult']['pricedProducts'])
    this.subscriberpricedProducts = _.extend({}, this.videoData)
    let channelPreviewCount = parseInt(channel['physicalChannels'][0]['previewCount'], 10)
    let channelPreviewLength = parseInt(channel['physicalChannels'][0]['previewLength'], 10)
    if (channelPreviewCount >= 0 && channelPreviewLength >= 0) {
        // close the player if the heartbeat play
      if (this.isHeartBeatPlay) {
        this.isHeartBeatPlay['close']()
      }
        // if the preview count had used up,the user can't preview and show the subcribe list
      EventService.emit('LOAD_FULLSCREEN_LIVETVVIDEO', this.videoData)
    }
    session.remove('LOGIN_LIVETV_PLAYING')
  }

    /**
     * LiveTV
     *check the channel lock
     * check the channel and program is under control
     */
  liveTVCheckLock (channel, businessType) {
    this.videoData['isLock'] = false
    this.videoData['isParentControl'] = false
    this.videoData['NoURL'] = false
    this.videoData['preview'] = false
    this.playCheckLock(channel, businessType, null, '1').then(resp => {
      if (this.isHeartBeatPlay) {
        return
      }
        // the cahnnel or program is not lock, playing the npvr depend on the returned url
      this.dealWithData(channel, resp['playURL'], false)
      EventService.emit('LOAD_FULLSCREEN_LIVETVVIDEO', this.videoData)
      if (!session.get('firstEnterPlay')) {
        session.put('firstEnterPlay', true)
        this.reportChannel('0', businessType, channel)
      }
      this.startPlayChannelHeartbeat(channel, this.playbill)
      session.remove('LOGIN_LIVETV_PLAYING')
    }).catch(resp => {
      if (resp.result && ['146020016', '146021006'].includes(resp.result.retCode)) {
          // not subcribed
          this.livetvUnSubscriber(resp, channel)
          return
        }
      if (resp.result && ['111020302', '111020301'].includes(resp.result.retCode)) {
          this.dealWithData(channel, '', false)
          this.videoData['NoURL'] = true
          EventService.emit('LOAD_FULLSCREEN_LIVETVVIDEO', this.videoData)
        }
        // it is locked and parent control,show the check passowrd dialog
      if (resp['authorizeResult'] && (resp['authorizeResult']['isLocked'] === '1' ||
                resp['authorizeResult']['isParentControl'] === '1') && !this.isHeartBeatPlay) {
          let isUnlock = _.find(session.get('unlocked_playbill_id'), item => {
            return item === this.playbill['ID']
          })
          // if the lock have been checked, play the program without checking again
          if (isUnlock) {
            this.dealWithData(channel, resp['playURL'], false)
            EventService.emit('LOAD_FULLSCREEN_LIVETVVIDEO', this.videoData)
            if (!session.get('firstEnterPlay')) {
              session.put('firstEnterPlay', true)
              this.reportChannel('0', businessType, channel)
            }
          } else {
            this.dealWithData(channel, resp['playURL'], false, {}, true)
            this.checkLockAndParentControl(resp, channel)
            // play on fullscreen
            EventService.emit('LOAD_FULLSCREEN_LIVETVVIDEO', this.videoData)
            EventService.removeAllListeners(['CHECK_LIVETV_PLAY_PASSWORD'])
            EventService.on('CHECK_LIVETV_PLAY_PASSWORD', password => {
              // call interface
              this.liveTVCheckAuthorizeLock(channel, businessType, password).then(authorizeLock => { }, authorizeLock2 => {
                if (!session.get('firstEnterPlay')) {
                  session.put('firstEnterPlay', true)
                  this.reportChannel('0', businessType, channel)
                }
                EventService.emit('HIDE_LIVETV_LOAD')
              })
            })
          }
        }
      session.remove('LOGIN_LIVETV_PLAYING')
    })
  }
    /**
     * check lock or parent control
     */
  checkLockAndParentControl (resp, channel) {
    if (resp['authorizeResult']['isLocked'] === '1' && channel['isLocked'] === '1') {
      this.videoData['isLock'] = true
    } else {
      this.videoData['isParentControl'] = true
    }
  }

    /**
     * play the channel preview
     */
  LiveTVPreviewPlay (channel, businessType) {
    this.videoData['isLock'] = false
    this.videoData['isParentControl'] = false
    this.videoData['NoURL'] = false
    this.videoData['preview'] = false
    this.playCheckLock(channel, businessType, null, '0').then(resp => {
        // the preview is not lock, playing the preview depend on the returned url
      this.dealWithData(channel, resp['playURL'], false)
      this.videoData['preview'] = true
      EventService.emit('LOAD_FULLSCREEN_LIVETVVIDEO', this.videoData)
      this.AddpreviewHistory(channel)
    }).catch(resp => {
        // it is locked and parent control,show the check passowrd dialog
      if (resp['authorizeResult'] && (resp['authorizeResult']['isLocked'] === '1' ||
                resp['authorizeResult']['isParentControl'] === '1')) {
          this.dealWithData(channel, resp['playURL'], false, {}, true)
          if (resp['authorizeResult']['isLocked'] === '1' && channel['isLocked'] === '1') {
            this.videoData['isLock'] = true
          } else {
            this.videoData['isParentControl'] = true
          }
          this.videoData['preview'] = true
          EventService.emit('LOAD_FULLSCREEN_LIVETVVIDEO', this.videoData)
          // input the passowrd and  verify
          EventService.removeAllListeners(['CHECK_LIVETV_PLAY_PASSWORD'])
          EventService.on('CHECK_LIVETV_PLAY_PASSWORD', password => {
            this.liveTVCheckAuthorizeLock(channel, 'PREVIEW', password).then(resps => {
              this.videoData['preview'] = true
            }, respfail => {
              // if the passowrd is not right,show the error message
              if (respfail['result']['retCode'] !== '157021009') {
                EventService.emit('LOAD_FULLSCREEN_LIVETVVIDEO', this.subscriberpricedProducts)
              }
            })
          })
        } else {
          // if The channel media file does not exist or the channel does not exist,show the error tips
          if (resp.result.retCode === '111020302' || resp.result.retCode === '111020301') {
            this.dealWithData(channel, '', false)
            this.videoData['NoURL'] = true
            EventService.emit('LOAD_FULLSCREEN_LIVETVVIDEO', this.videoData)
          }
          // preview authenticate failed,show the error message and add the preview times
          this.AddpreviewHistory(channel)
        }
    })
  }

    /**
     * check the LiveTV lock
     */
  liveTVCheckAuthorizeLock (channel, businessType, password) {
    let checkLock = {
      checkType: '1',
      password: password,
      type: '3'
    }

    return this.playCheckLock(channel, businessType, checkLock, '1').then(resp => {
        // if current play is preview,add the preview times and show preview tips
      if (businessType === 'PREVIEW') {
        this.AddpreviewHistory(channel)
        this.dealWithData['preview'] = true
      }
        // if the playbill is deblocking,put it in the unlocked playbill list
      let playbillids = []
      if (session.get('unlocked_playbill_id')) {
        playbillids = session.get('unlocked_playbill_id')
      }
      playbillids.push(this.playbill['ID'])
      session.put('unlocked_playbill_id', playbillids)
        // play the program
      this.dealWithData(channel, resp['playURL'], false)
      this.videoData['isLock'] = false
      this.videoData['isParentControl'] = false
      EventService.emit('LOAD_FULLSCREEN_LIVETVVIDEO', this.videoData)
      EventService.emit('HIDE_POPUP_DIALOG')
    }).catch(resp => {
        // if the passowrd is not right ,show the error message
      if (resp['result']['retCode'] === '157021009') {
          EventService.emit('SHOW_LIVETV_AUTHRITE_ERROR_MESSAGE')
        }
      return Promise.reject(resp)
    })
  }

    /**
     * play authenticate and check the lock
     */
  playCheckLock (channel, businessType, checkLock, isReturnProduct) {
    let mediaID = channel['physicalChannelsDynamicProperty'] && channel['physicalChannelsDynamicProperty'].ID ||
            channel.physicalChannels && channel.physicalChannels[0] && channel.physicalChannels[0].ID || ''
    let req = {
      channelID: channel.ID,
      mediaID: mediaID,
      businessType: businessType,
      isReturnProduct: isReturnProduct
    }
    if (!this.isHeartBeatPlay) {
      req['checkLock'] = checkLock || {
        checkType: '0'
      }
    }

    return playChannel(req).then(resp => {
      // location,authenticate,subscribe is done
      this.playChannelMSALog(resp, true)
      this.mediaPlayService.sendLicense(resp.authorizeResult)
      this.productID = resp.authorizeResult && resp.authorizeResult.productID
      return resp
    }).catch(resp => {
        // one of location,authenticate,subscribe is not content
      this.playChannelMSALog(resp, false)
      this.productID = resp.authorizeResult && resp.authorizeResult.productID
      return Promise.reject(resp)
    })
  }

    /**
     * add the preview count
     */
  AddpreviewHistory (channel) {
    let currentChannel = _.filter(this.previewHistoryList, previewHistory => {
      if (previewHistory.channelID === channel['ID']) {
        return previewHistory
      }
    })[0] || {}
    if (!currentChannel.channelNo) {
      let previewHistory = {
        subscriberID: session.get('SUBSCRIBER_ID'),
        channelID: channel['ID'],
        channelNo: channel['channelNO'],
        previewTotalCount: 1
      }
      this.previewHistoryList.push(previewHistory)
    } else {
      this.previewHistoryList = _.map(this.previewHistoryList, previewHistory => {
        if (previewHistory.channelID === channel['ID']) {
            previewHistory.previewTotalCount = previewHistory.previewTotalCount + 1
          }
        return previewHistory
      })
    }
    session.put('PREVIED_HISTORY_LIST', this.previewHistoryList, true)
  }

    /**
     * start playing current program(include preview),before the next program start,get the detail of next program
     */
  getNextPlayBill (req) {
    let options = {
      params: {
        SID: 'queryplaybilllist3',
        DEVICE: 'PC',
        DID: session.get('uuid_cookie')
      }
    }
    return queryPlaybillList(req, options).then(resp => {
      let playbill = resp['channelPlaybills'][0]
      let channelDtails = this.channelCacheService.getStaticChannelData().channelDetails
      let dynamicChannelData = this.channelCacheService.getDynamicChannelData().channelDynamaicProp
      playbill['channelInfos'] = _.find(channelDtails, (detail) => {
        return detail['ID'] === playbill['playbillLites'][0]['channelID']
      })
      _.find(dynamicChannelData, (detail) => {
        if (detail && playbill['channelInfos'] && playbill['channelInfos']['ID'] === detail['ID']) {
            playbill['channelInfos']['channelNO'] = detail['channelNO']
            let nameSpace = session.get('ott_channel_name_space')
            let physicalChannelsDynamicProperty = _.find(detail['physicalChannelsDynamicProperties'], (property) => {
              return _.contains(property['channelNamespaces'], nameSpace)
            })
            playbill['channelInfos']['physicalChannelsDynamicProperty'] =
                    physicalChannelsDynamicProperty || detail['physicalChannelsDynamicProperties'][0]
          }
      })
      let channel = playbill['channelInfos']
      let playbills = playbill['playbillLites']

      return {
        playbill: playbills,
        channel: channel
      }
    })
  }

    /**
     *  play the PLTV
     */
  playPLTVCRpromgram (playbill, channel, time, length, seekTime, player?: any) {
    this.playbill = playbill
    this.channelInfo = channel
    this.isHeartBeatPlay = player
    this.videoData['checkPssword'] = false
    this.videoData['playNextpltv'] = false
    this.videoData['isLock'] = false
    this.videoData['isParentControl'] = false
    this.videoData['preview'] = false
    this.checkPLTVCRpromgram(playbill, channel).then(resp => {
        // the cahnnel authenticate successfully, play depend on the returned url
      let isUnlock = _.find(session.get('unlocked_playbill_id'), item => {
        return item === playbill['ID']
      })
      this.videoData['PltvTime'] = seekTime
      if (!isUnlock && Number(playbill['rating']['ID']) > Number(session.get('PROFILERATING_ID')) && !this.isHeartBeatPlay) {
        this.dealWithData(channel, resp['playURL'], false, {}, true)
        this.videoData['type'] = 'PLTV'
        this.videoData['isParentControl'] = true
        EventService.emit('LOAD_FULLSCREEN_LIVETVVIDEO', this.videoData)
        EventService.removeAllListeners(['CHECK_PLTV_PLAY_PASSWORD'])
        EventService.on('CHECK_PLTV_PLAY_PASSWORD', () => {
            // if the shift time large then the shift length,playing from the left border
            if (Date.now()['getTime']() - time > length * 1000) {
              EventService.emit('SEEK_PLAYBILL_TIME', {
                playTime: Date.now()['getTime']() -
                            length * 1000,
                isLiveTV: false,
                seekTime: 0
              })
              EventService.emit('HIDE_POPUP_DIALOG')
            } else {
              this.dealWithData(channel, resp['playURL'], false)
              this.videoData['type'] = 'PLTV'
              this.videoData['checkPssword'] = true
              this.videoData['isLock'] = false
              this.videoData['isParentControl'] = false
              EventService.emit('LOAD_FULLSCREEN_LIVETVVIDEO', this.videoData)
              EventService.emit('HIDE_POPUP_DIALOG')
            }
          })
      } else {
        if (this.isHeartBeatPlay) {
            return
          }
        this.dealWithData(channel, resp['playURL'], false)
        this.videoData['type'] = 'PLTV'
        EventService.emit('LOAD_FULLSCREEN_LIVETVVIDEO', this.videoData)
      }
    }).catch(resp => {
        // not subcribed
      if (['146020016', '146021006'].includes(resp['result']['retCode'])) {
          // close the player
          if (this.isHeartBeatPlay) {
            this.isHeartBeatPlay['close']()
          }
          this.dealWithData(channel, resp['playURL'], true, resp['authorizeResult']['pricedProducts'])
          this.videoData['type'] = 'PLTV'
          this.videoData['PltvTime'] = seekTime
          EventService.emit('LOAD_FULLSCREEN_LIVETVVIDEO', this.videoData)
          return
        }
      if (resp.result.retCode === '111020302' || resp.result.retCode === '111020301') {
          this.dealWithData(channel, '', false)
          this.videoData['NoURL'] = true
          EventService.emit('LOAD_FULLSCREEN_LIVETVVIDEO', this.videoData)
        }
    })
  }

    /**
     * PLTV authenticate
     */
  checkPLTVCRpromgram (playbill, channel) {
    let mediaID = channel['physicalChannelsDynamicProperty'] && channel['physicalChannelsDynamicProperty'].ID ||
            channel.physicalChannels && channel.physicalChannels[0] && channel.physicalChannels[0].ID || ''
    return playChannel({
      channelID: channel.ID,
      playbillID: playbill.ID,
      mediaID: mediaID,
      businessType: 'PLTV',
      isReturnProduct: '1'
    }).then(resp => {
        // location,authenticate,subscribe is done
      this.playChannelMSALog(resp, true)
      this.mediaPlayService.sendLicense(resp.authorizeResult)
      this.productID = resp.authorizeResult && resp.authorizeResult.productID
      return resp
    }, resp => {
        // one of location,authenticate,subscribe is not content
        this.playChannelMSALog(resp, false)
        this.productID = resp.authorizeResult && resp.authorizeResult.productID
        return Promise.reject(resp)
      })
  }

    /**
     * [startPlayChannelHeartbeat start the heartbeat of playing PLTV]
     */
  private startPlayChannelHeartbeat (channel, playbill) {
    clearInterval(this.playChannelHeartbeatTime)
    this.timerService.getCaheCustomConfig().then((config) => {
      let configData = this.timerService.getVSPCustomConfig(config)
      let time = configData && configData['playHeartBitInterval'] * 1000 || 5 * 60 * 1000
      let mediaID = channel['physicalChannelsDynamicProperty'] && channel['physicalChannelsDynamicProperty'].ID ||
                channel.physicalChannels && channel.physicalChannels[0] && channel.physicalChannels[0].ID || ''
      this.playChannelHeartbeatTime = setInterval(() => {
        const req = { channelID: channel.ID, playbillID: playbill.ID, mediaID: mediaID }
        playChannelHeartbeat(req).then(resp => {
            if (resp.isValid === '0') {
              EventService.emit('CLOSED_FULLSCREEN_LIVETVVIDEO')
            }
          })
      }, time)
    })
  }

    /**
     * play behaviour,the values includeL action: string
     *   >- 0：start play
     *   >- 1：exit play
     *   >- 2：change channel
     *   >- 3：change live tv and pltv
     *
     * channel ID of playing or before changing. channelID: string;
     *
     * if action=2，mean channel ID of after changing. nextChannelID?: string;
     *
     * if play Catch-up TV playbill,mean playbill ID. playbillID?: string;
     *
     * media IDof playing or before changing. mediaID: string;
     *
     * if action=2，mean media ID of after changing. nextMediaID?: string;
     *
     * the business type of authenticate ,values include :businessType: string;
     *   >- BTV：live tv
     *   >- PLTV：network time-shifted
     *   >- CPLTV：local time-shifted
     *   >- CUTV：Catch-up TV
     *   >- IR：DT custom made business, like Catch-up TV
     *
     * if play Catch-up TV, does play the content which haven been downloaded in local, values include isDownload?: string;
     *   >- 0：no
     *   >- 1：yes
     *
     * if enter to the channel from appointed subject, carry the subject ID,
     * useing report the user behaviour according to subject. subjectID?: string;
     *
     * product ID which had authenticated. productID?: string;
     */
  reportChannel (action, businessType, channelDetail) {
    if (action === '0' || action === '1' || action === '3') {
      this.reportPlayAction(action, businessType, channelDetail)
    }
    if (action === '2') {
        // call interface get the OTT media
      let mediaID = channelDetail['physicalChannelsDynamicProperty'] && channelDetail['physicalChannelsDynamicProperty'].ID ||
            channelDetail.physicalChannels && channelDetail.physicalChannels[0] && channelDetail.physicalChannels[0].ID || ''
      reportChannel({
        subjectID: '-1',
        action: action,
        channelID: channelDetail['ID'],
        nextChannelID: channelDetail['nextChannelID'],
        mediaID: mediaID,
        nextMediaID: channelDetail['nextMediaID'],
        businessType: businessType,
        productID: this.productID ? this.productID : '0'
      })
    }
  }
    /*
    * the method for report the action
    */
  reportPlayAction (action, businessType, channelDetail) {
    let mediaID = channelDetail && channelDetail['physicalChannelsDynamicProperty'] &&
         channelDetail['physicalChannelsDynamicProperty'].ID ||
            channelDetail.physicalChannels && channelDetail.physicalChannels[0] && channelDetail.physicalChannels[0].ID
    if (mediaID) {
        // report channel data
      reportChannel({
        subjectID: '-1',
        action: action,
        channelID: channelDetail['ID'],
        mediaID: mediaID || '',
        businessType: businessType,
        productID: this.productID ? this.productID : '0'
      })
    }
  }
    /*
    * the method for check the [resp]
    */
  playChannelMSALog (resp, successFlag) {
    if (_.isUndefined(resp.authorizeResult)) {
        // show error message
      this.commonService.commonLog('PlayChannel', 'authorizeResult')
    }
    if (successFlag && _.isUndefined(resp.playURL)) {
        // show error message
      this.commonService.commonLog('PlayChannel', 'playURL')
    }
  }
}
