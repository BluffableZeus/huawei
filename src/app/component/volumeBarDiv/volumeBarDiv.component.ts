import { Component, Input, OnInit, Output, EventEmitter, OnDestroy } from '@angular/core'
import { EventService } from 'ng-epg-sdk/services'
import { session } from 'src/app/shared/services/session'

@Component({
  selector: 'app-volumebardiv',
  templateUrl: './volumeBarDiv.component.html',
  styleUrls: ['./volumeBarDiv.component.scss']
})

export class VolumeBarDivComponent implements OnInit, OnDestroy {
  static MAX_VOLUME = 20
  private _volume = 10
  protected position = 50
  protected sliderIsCan = true

  @Output() changeVolume: EventEmitter<number> = new EventEmitter<number>()
  @Input() volumeID: string
  public lastVolumeLevel = 10
  public isEdge = false

  get gradient () {
    return `linear-gradient(to right, #FFCD7E ${this.position}%,#979797 ${this.position}%)`
  }

  get volume (): number {
    return this._volume
  }

  set volume (value: number) {
    if (value > VolumeBarDivComponent.MAX_VOLUME) {
      value = VolumeBarDivComponent.MAX_VOLUME
    } else if (value < 0) {
      value = 0
    }
    if (value === 0 && this._volume > 0) {
      this.lastVolumeLevel = this._volume
    }

    this._volume = value
    this.position = value / VolumeBarDivComponent.MAX_VOLUME * 100
    this.changeVolume.emit(value)
  }

  constructor () {
    let userAgent = navigator.userAgent.toLowerCase()
    this.isEdge = userAgent.indexOf('edge') > -1
  }

  ngOnInit () {
    EventService.removeAllListeners(['setInitLiveTVVolume'])
    EventService.removeAllListeners(['downAction'])
    EventService.removeAllListeners(['upAction'])
    EventService.removeAllListeners(['changeVodVolume'])
    EventService.removeAllListeners(['changeLiveTVVolume'])
      // VOD volume click event
    EventService.on('changeVodVolume', volume => {
      this.volume = parseInt(volume, 10) === 0 ? 0 : this.lastVolumeLevel
    })
      // Live volume icon click event
    EventService.on('changeLiveTVVolume', (volume) => {
      this.volume = parseInt(volume, 10) === 0 ? 0 : this.lastVolumeLevel
      EventService.emit('lastVolumeValue', this.lastVolumeLevel)
    })
      // Keyboard control volume
    EventService.on('upAction', () => {
      if (session.get('VOD_IS_FULLSCREEN') || session.get('LiveTvVolume')) {
        this.lastVolumeLevel++
        this.volume++
        if (this.lastVolumeLevel >= VolumeBarDivComponent.MAX_VOLUME) {
            this.lastVolumeLevel = VolumeBarDivComponent.MAX_VOLUME
          }
        EventService.emit('volumeBarValue', this.volume)
        EventService.emit('liveVolumeBarValue', this.volume)
      }
    })
    EventService.on('downAction', () => {
      if (session.get('VOD_IS_FULLSCREEN') || session.get('LiveTvVolume')) {
        this.lastVolumeLevel--
        this.volume--
        if (this.lastVolumeLevel <= 0) {
            this.lastVolumeLevel = 0
          }
        EventService.emit('volumeBarValue', this.volume)
        EventService.emit('liveVolumeBarValue', this.volume)
      }
    })
    EventService.on('setInitLiveTVVolume', () => this.volume = 10)
  }

  ngOnDestroy () {
    EventService.removeAllListeners(['setInitLiveTVVolume'])
    EventService.removeAllListeners(['downAction'])
    EventService.removeAllListeners(['upAction'])
    EventService.removeAllListeners(['changeVodVolume'])
    EventService.removeAllListeners(['changeLiveTVVolume'])
  }

  notSlide () {
    this.sliderIsCan = false
  }

  inSlide () {
    this.sliderIsCan = true
  }
}
