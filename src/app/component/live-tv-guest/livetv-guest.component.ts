import { Component, Output, EventEmitter } from '@angular/core'
import { TranslateService } from '@ngx-translate/core'
@Component({
  selector: 'app-guest-dialog',
  styleUrls: ['./livetv-guest.component.scss'],
  templateUrl: './livetv-guest.component.html'
})

export class GuestDialogComponent {
  @Output() guestLogin: EventEmitter<Object> = new EventEmitter()
  @Output() closeGuestLogin: EventEmitter<Object> = new EventEmitter()
    // declare variables.
  public guestAuthorizeType = ''
  public judgeAuthOrSub = false
  public playbill: any
    // private constructor
  constructor (
        private translate: TranslateService
    ) { }
    /**
     * show different tips according to the type
     */
  getType (type) {
    switch (type) {
        // the tip of no subscribe
      case 'Subscribe':
          // load the subscribe class
        this.judgeAuthOrSub = false
        this.guestAuthorizeType = this.translate.instant('guest_no_subscribed')
        break
          // the tip of no Authorization
      case 'Authorizate':
          // load the Authorization class
        this.judgeAuthOrSub = true
        this.guestAuthorizeType = this.translate.instant('guest_isParentControl', { name: this.playbill['name'] })
        break
          // default tip
      default:
          // load the subscribe class
        this.judgeAuthOrSub = false
        this.guestAuthorizeType = this.translate.instant('guest_no_subscribed')
        break
    }
  }
    /**
     * set the message
     */
  setMessage (profile) {
    this.playbill = profile['playbill']
    this.getType(profile['type'])
  }
    /**
     * show login dialog
     */
  livetvLogin () {
    this.guestLogin.emit({})
  }
}
