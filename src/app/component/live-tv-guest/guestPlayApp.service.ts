import * as _ from 'underscore'
import { Injectable } from '@angular/core'
import { EventService } from 'ng-epg-sdk/services'
import { session } from 'src/app/shared/services/session'
import { CommonService } from '../../core/common.service'
import { playChannel } from 'ng-epg-sdk/vsp'
import { MediaPlayService } from '../mediaPlay/mediaPlay.service'

@Injectable()
export class GuestPlayAppService {
    // declare variables.
  private videoData = {}
  private previewHistoryList = []
  private playbill = {}
    // private constructor
  constructor (
        private commonService: CommonService,
        private mediaPlayService: MediaPlayService) {
    this.previewHistoryList = session.get('GUEST_PREVIED_HISTORY_LIST') || []
  }

    /**
     * the guest play the BTV or current program.
     */
  guestPlayProgram (playbill, channel) {
    this.playbill = playbill
    this.liveTVCheckLock(channel, 'BTV')
  }

  dealWithData (channel, url, judgeSub, type?: any) {
    this.videoData['channel'] = channel
    this.videoData['playbill'] = this.playbill
    this.videoData['url'] = url
    if (judgeSub) {
      this.videoData['guest'] = 'Guest'
      this.videoData['type'] = type
    } else {
      this.videoData['guest'] = ''
      this.videoData['type'] = 'LiveTV'
    }
  }

    /**
     * check the guest lock the channel.
     * check the channel's parental control level and check the program 's control level.
     */
  liveTVCheckLock (channel, businessType) {
    let self = this
    this.videoData['NoURL'] = false
    this.playCheckLock(channel, businessType, null, '0').then(resp => {
        // the channel's parental control is limited,play it according to the returned url.
      self.dealWithData(channel, resp['playURL'], false)
      EventService.emit('LOAD_FULLSCREEN_LIVETVVIDEO', self.videoData)
    }, resp => {
      if (['146020016', '146021006'].includes(resp['result']['retCode'])) { // without subscribe.
        self.dealWithData(channel, resp['playURL'], true, 'Subscribe')
        let channelPreviewCount = parseInt(channel['physicalChannels'][0]['previewCount'], 10)
        let channelPreviewLength = parseInt(channel['physicalChannels'][0]['previewLength'], 10)
        if (channelPreviewCount >= 0 && channelPreviewLength >= 0) {
            let currentChannel = _.filter(self.previewHistoryList, function (previewHistory) {
              if (previewHistory.channelID === channel['ID']) {
                return previewHistory
              }
            })[0] || {}
            if (currentChannel.previewTotalCount >= channelPreviewCount) {
              // popup error message or login dialog when it is not allow user to preview.
              EventService.emit('LOAD_FULLSCREEN_LIVETVVIDEO', self.videoData)
            } else {
              // authenticate the channel to preview.
              self.LiveTVPreviewPlay(channel, 'PREVIEW')
            }
          }
        return
      }
      if (resp['result'] && resp['result']['retCode'] &&
                (resp['result']['retCode'] === '146021007' || resp['result']['retCode'] === '146021008')) {
        self.dealWithLock(resp, channel)
      }
    })
  }

  dealWithLock (resp, channel) {
    let self = this
    if (resp['authorizeResult']['isLocked'] === '1' || resp['authorizeResult']['isParentControl'] === '1') {
        // locked or the parental control,popup error message or login dialog.
      self.dealWithData(channel, resp['playURL'], true, 'Authorizate')
      EventService.emit('LOAD_FULLSCREEN_LIVETVVIDEO', self.videoData)
    }
  }

    /**
     * the channel to preview.
     */
  LiveTVPreviewPlay (channel, businessType) {
    let self = this
    this.playCheckLock(channel, businessType, null, '0').then(resp => {
        // if authenticate the channel to preview successfully,play it according to the returned url.
      self.videoData['preview'] = true
      self.dealWithData(channel, resp['playURL'], false)
      EventService.emit('LOAD_FULLSCREEN_LIVETVVIDEO', self.videoData)
      self.AddpreviewHistory(channel)
    }, resp => {
      if (resp['result'] && resp['result']['retCode'] &&
                (resp['result']['retCode'] === '146021007' || resp['result']['retCode'] === '146021008')) {
        if (resp['authorizeResult']['isLocked'] === '1' || resp['authorizeResult']['isParentControl'] === '1') {
            // locked or the parental control,popup error message or login dialog.
            self.dealWithData(channel, resp['playURL'], true, 'Authorizate')
            self.videoData['preview'] = true
            EventService.emit('LOAD_FULLSCREEN_LIVETVVIDEO', self.videoData)
          }
      } else {
        if (resp.result.retCode === '111020302') {
            self.dealWithData(channel, '', false)
            self.videoData['NoURL'] = true
            EventService.emit('LOAD_FULLSCREEN_LIVETVVIDEO', self.videoData)
          }
          // if authenticate the channel to preview failed，show the error message and add the preview times.
        self.AddpreviewHistory(channel)
      }
    })
  }

    /**
     * authenticate locked to play.
     */
  playCheckLock (channel, businessType, checkLock, isReturnProduct) {
    let self = this
    let mediaID = channel['physicalChannelsDynamicProperty'] && channel['physicalChannelsDynamicProperty'].ID ||
            channel.physicalChannels && channel.physicalChannels[0] && channel.physicalChannels[0].ID

    return playChannel({
      channelID: channel.ID,
      mediaID: mediaID || '',
      businessType: businessType,
      checkLock: checkLock || {
        checkType: '0'
      },
      isReturnProduct: isReturnProduct
    }).then(resp => { // meet the geographical position,authentication and subscribe.
      self.mediaPlayService.sendLicense(resp.authorizeResult)
      return resp
    }, resp => { // the geographical position or authentication or subscribe is not met.
        return Promise.reject(resp)
      })
  }

    /**
     * add the preview times.
     */
  AddpreviewHistory (channel) {
    let currentChannel = _.filter(this.previewHistoryList, function (previewHistory) {
      if (previewHistory.channelID === channel['ID']) {
        return previewHistory
      }
    })[0] || {}
    if (!currentChannel.channelNo) {
      let previewHistory = {
        subscriberID: session.get('SUBSCRIBER_ID'),
        channelID: channel['ID'],
        channelNo: channel['channelNO'],
        previewTotalCount: 1
      }
      this.previewHistoryList.push(previewHistory)
    } else {
      this.previewHistoryList = _.map(this.previewHistoryList, function (previewHistory) {
        if (previewHistory.channelID === channel['ID']) {
            previewHistory.previewTotalCount += previewHistory.previewTotalCount
          }
        return previewHistory
      })
    }
    session.put('GUEST_PREVIED_HISTORY_LIST', this.previewHistoryList, true)
  }
}
