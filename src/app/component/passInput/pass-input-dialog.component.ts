import * as _ from 'underscore'
import { Component, OnInit } from '@angular/core'
import { DialogRef, DialogParams } from 'ng-epg-ui/webtv-components/dialog'
import { EventService } from 'ng-epg-sdk/services'
import { checkPassword } from 'ng-epg-sdk/vsp'
import { TranslateService } from '@ngx-translate/core'
import { session } from 'src/app/shared/services/session'

@Component({
  selector: 'app-pass-input-dialog',
  templateUrl: './pass-input-dialog.component.html',
  styleUrls: ['./pass-input-dialog.component.scss']
})

export class PassInputDialogComponent implements OnInit {
    // declare variables.
  public deletePassWord = false
  public userPassword = ''
  public errorMessage = false
  public emptyPasswordMessage = false
  public type = ''
  public contentInfo = ''
  public name = ''
  public showplaceholderTvguide = true
  public configData = {}
  public forgetpassword = false
  public forgot_password_url = ''
    // private constructor
  constructor (
        private config: DialogParams,
        private dialog: DialogRef,
        private translate: TranslateService
    ) {
      // the pass input get focus when open the dialog at first.
    _.delay(() => {
      if (document.getElementById('pass-Words')) {
        document.getElementById('pass-Words').focus()
      }
    }, 600)
    this.getType()
  }

  ngOnInit () {
  }
    // check the password background to hide the background,then the input get focus.
  onplaceholdTvguide () {
    if (this.userPassword === '' || this.userPassword === undefined) {
      _.delay(() => {
        document.getElementById('pass-Words').focus()
      }, 100)
    }
  }
    // get the type of the pass input.
  getType () {
    let self = this
    this.type = this.config['type']
    this.name = this.config['name']
    if (this.name.length > 20) {
      this.name = this.name.substring(0, 20) + '...'
    }
    this.configData = session.get('CONFIG_DATAS') || {}
      // if the forget password url exits,show the forget password button.
    if (this.configData['forgot_password_url']) {
      this.forgetpassword = true
      this.forgot_password_url = this.configData['forgot_password_url']
    }
    switch (this.type) {
        // the playbill's rate is higher than the use.
      case 'rating':
        this.contentInfo = self.translate.instant('rating_check_password',
            { name: this.name })
        break
          // the channel is locked.
      case 'lock':
        this.contentInfo = self.translate.instant('channel_lock_check_password',
            { name: this.name })
        break
          // the playbill should be subscribed.
      case 'Subscribe':
        this.contentInfo = self.translate.instant('subscription_check_password')
        break
      default:
        break
    }
  }
    // change the password
  changePassword (e) {
    if (this.userPassword !== '') {
        // click the Enter key,just to confirm the password.
      if (e.keyCode === 13) {
        document.getElementById('confirmPass').click()
      }
      this.showplaceholderTvguide = false
      this.deletePassWord = true
    } else {
      this.deletePassWord = false
      this.showplaceholderTvguide = true
    }
  }
    // judge the status of delete buttom and the placeholder.
  showPassword () {
    if (this.userPassword !== '') {
      this.deletePassWord = true
      this.showplaceholderTvguide = false
    }
    if (this.userPassword === '') {
      this.showplaceholderTvguide = true
    }
  }
    // the pass input lose the focus.
  hidePassword () {
    let self = this
    if (this.userPassword === '') {
      this.showplaceholderTvguide = true
    }
    _.delay(function () {
      self.deletePassWord = false
    }, 300)
  }
    // click the delete buttom of the pass input.
  removePassWord () {
    this.userPassword = ''
    document.getElementById('pass-Words').focus()
  }
    // click the confirm button to check the password.
  confirm () {
    let type
    if (this.type === 'lock') {
      type = '0'
    }
      // when input is empty,show the empty message.
      // the input get the focus.
    if (this.userPassword === '') {
      this.emptyPasswordMessage = true
      this.errorMessage = false
      document.getElementById('pass-Words').focus()
      return
    }
    checkPassword({
      password: this.userPassword,
      type: type
    }).then(resp => {
        // if check passowrd successfully
      EventService.emit('CHECK_PASSWORDS', resp)
      this.dialog.close()
    }, resp => {
        // initial the input if check failed.
        this.userPassword = ''
        this.showplaceholderTvguide = true
        this.deletePassWord = false
        this.errorMessage = true
        this.emptyPasswordMessage = false
        document.getElementById('pass-Words').focus()
      })
  }
    // close the dialog.
  close () {
    this.dialog.close()
  }
    // when the key down,hidden the placeholder.
  keydownPassword () {
    this.showplaceholderTvguide = false
  }
    /**
     * button style snoop
     */
  btnStyleChange () {
      // if the password is not empty,change the button color
    if ((document.getElementById('pass-Words')['value'] !== '')) {
      return true
    } else {
      return false
    }
  }
}
