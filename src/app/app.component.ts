import * as _ from 'underscore'
import { Component, ViewEncapsulation, ViewContainerRef, ViewChild, OnInit, OnDestroy, AfterViewInit } from '@angular/core'

import { Router, NavigationEnd, ActivatedRoute, Data } from '@angular/router'
import { session } from 'src/app/shared/services/session'
import { AccountLoginComponent } from './login/account-login'
import { EventService, HeartbeatService } from 'ng-epg-sdk/services'
import { HeaderComponent } from './header'
import { HeaderMobComponent } from './headerMob'
import { TimerService } from './shared/services/timer.service'
import { ErrorMessageService } from './shared/services/error-message.service'
import { TrackService } from './shared/services/track.service'
import { TranslateService } from '@ngx-translate/core'
import { CommonService } from './core/common.service'
import { LiveTvService } from './shared/services/livetv.service'
import { PlaybillAppService } from './component/playPopUpDialog/playbillApp.service'
import { TVODAppService } from './component/playPopUpDialog/tVodApp.service'
import { HookConfig } from './hook-config'
import { WeakTipDialogComponent } from './component/weakTip/weakTip.component'
// import { StopRecordConfirmComponent } from './component/stopRecord-confirm/stopRecord-confirm.component'
import { StorageRemindComponent } from './component/storageRemind/storageRemind.component'
// import { RecordDataService } from './component/recordDialog/recordData.service'
import { ReminderTask } from './component/reminder/reminder-task.service'
import { ReminderService } from './component/reminder/reminder.service'
// import { ManualRecordPopUpComponent } from './my-tv/recording/manual-popup/manual-popup.component'
// import { NPVRAppService } from './component/playPopUpDialog/nPvrApp.service'
// import { RecordingConflictComponent } from './component/recording-conflict'
import { MixedConflictComponent } from './component/mixed-conflict'
import { DateUtils } from 'ng-epg-sdk/utils'
import { http } from 'ng-epg-sdk/sdk'
import { MediaPlayService } from './component/mediaPlay/mediaPlay.service'
import { ChannelCacheService } from './shared/services/channel-cache.service'
import { CustomConfigService } from './shared/services/custom-config.service'
import { config as EPGConfig } from './shared/services/config'
import { ProfileService } from './shared/services/profile.service'
import { xr } from 'oper-center/api/requests'
import { ViewTransferService } from './core/view-transfer.service'
import { Store, select } from '@ngrx/store'
import { Observable } from 'rxjs'
import { TitleService } from './shared/services/title.service'
import { filter, map } from 'rxjs/operators'

import {
  APP_INIT_LOAD_START,
  APP_INIT_LOAD_END
} from './store/app'


const pageTracker = require('../assets/lib/PageTracker')

@Component({
  selector: 'app-root',
  encapsulation: ViewEncapsulation.None,
  styleUrls: [
    './app.style.scss'
  ],
  templateUrl: './app.component.html'
})

export class AppComponent implements OnInit, OnDestroy, AfterViewInit {

  profileLogoName = this.translate.instant('log_in')
  disAppearTimer
  mailto = 'mailto:'
  judgeGuide = true
  isHighLight = false
  links = {
    privacy: EPGConfig.privacyLink,
    softwareLicense: EPGConfig.softwareLicenseLink,
    faq: EPGConfig.FAQLink,
    feedbackEmail: `mailto:${EPGConfig.feedbackEmail}?subject=${this.translate.instant('feedback_mail_subject')}&body=${this.translate.instant('feedback_mail_body')}`
  }

  @ViewChild(HeaderComponent) header: HeaderComponent
  @ViewChild(HeaderMobComponent) headerMob: HeaderMobComponent
  @ViewChild(WeakTipDialogComponent) weakTipDialog: WeakTipDialogComponent
  // @ViewChild(ManualRecordPopUpComponent) manualRecordPopUp: ManualRecordPopUpComponent
  currentRouteUrl = location.pathname
  public judgeWeakit = false
  public getWeakit
  public nextWeakTip
  public showPlaybillRecord = false
  // public recordDialogConfig
  public showErrorDialog = false
  public errorDialogConfig
  public showDeviceListDialog = false
  public deviceDialogConfig
  public getCaheCustomConfigData = false
  public toastQueue = []
  public toastStartTime = 0
  public toastShowTime = 5000
  public showNav = false
  // public showUpdateReocrd = false
  public updateRecordData: any = {}
  public showChangePassword = false
  public realplaybill
  public realchannel

  auth$: Observable<any>
  profiles$: Observable<any>
  constructor (
        private translate: TranslateService,
        private commonService: CommonService,
        private viewContainerRef: ViewContainerRef,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private _titleService: TitleService,
        private trackService: TrackService,
        private timerService: TimerService,
        private liveTvService: LiveTvService,
        private errorMessageService: ErrorMessageService,
        private playbillAppService: PlaybillAppService,
        private tVODAppService: TVODAppService,
        private _hookConfig: HookConfig,
        // private recordDataService: RecordDataService,
        private reminderTask: ReminderTask,
        private reminderService: ReminderService,
        // private nPVRAppService: NPVRAppService,
        private mediaPlayService: MediaPlayService,
        private heartbeatService: HeartbeatService,
        private customConfigService: CustomConfigService,
        private channelCacheService: ChannelCacheService,
        private profileService: ProfileService,
        private viewTransferService: ViewTransferService,
        private store: Store<any>
    ) {
    this.store.dispatch({ type: APP_INIT_LOAD_START })
    session.remove('NO_POP_UP_ERROR_DIALOG')
    this.commonService.viewContainerRef = viewContainerRef
    this.commonService.translateService = this.translate
      // FIXME Due to unknown circumstances I had to create this additional service
      // to be able to use the same data everywhere as a data from Singleton
    this.viewTransferService.viewContainerRef = viewContainerRef
    this.viewTransferService.translateService = this.translate
    this.notification = this.notification.bind(this)
    this._hookConfig.init()
    this.auth$ = store.pipe(select('auth'))
    this.profiles$ = store.pipe(select('profiles'))
  }

  ngOnInit () {

    this.router.events
      .pipe(filter(ev => ev instanceof NavigationEnd))
      .subscribe((response: any) => this._titleService.setTitle({ route: response.url }))

    xr.configure(GLOBAL_CONFIG.ocURL)
      // save the flag of reminder for the suspension dialog
    session.put('reminderMode', EPGConfig['reminderMode'])

    let firstOpen = session.get('firstOpen')
    if (!firstOpen) {
      this.getCaheCustomConfigData = false
      this.showNav = false
    }
      // when the language is loaded,show the navigation
    EventService.removeAllListeners(['SHOW_NAV'])
    EventService.on('SHOW_NAV', () => {
      this.showNav = true
    })
    EventService.removeAllListeners(['GETCUSTOMCONFIGDATASUCCES'])
    EventService.on('GETCUSTOMCONFIGDATASUCCES', () => {
      this.getCaheCustomConfigData = true
    })
      // show the top of the page
    _.delay(() => {
      document.body.scrollTop = 0
      document.documentElement.scrollTop = 0
    }, 0)
      // load the i18n sources
    this.initTranslate()
      // set the scroll event
    this.scrollfixed()
      // get the email address for forget password,if the address existed
    this.getSugesstion()
      // if visitors enter the application from my-tv,jump to home page
    this.isMytvIn()
      // open or close the record of user behavior path.
    pageTracker.setStatus(session.get('privacy_agree') ? 0 : 1)
    //  // handle the conflict of mixed recording
    // EventService.removeAllListeners(['SOLVE_CONFLICT_MIXED'])
    // EventService.on('SOLVE_CONFLICT_MIXED', (event) => {
    //   if (event.haveChooseAddID) {
    //     let date
    //     if (event.haveChooseAddID['startTime']) {
    //       date = DateUtils.format(event.haveChooseAddID['startTime'], 'YYYY') +
    //                     DateUtils.format(event.haveChooseAddID['startTime'], 'MM') +
    //                     DateUtils.format(event.haveChooseAddID['startTime'], 'DD')
    //     } else {
    //       date = event.haveChooseAddID.playbillID
    //     }
    //     if (event.update) {
    //       this.conflictSeries(event.data, event.type, true, date)
    //     } else {
    //       this.conflictSeries(event.data, event.type, false, date)
    //     }
    //   } else {
    //     if (event.update) {
    //       this.conflictSeries(event.data, event.type, true)
    //     } else {
    //       this.conflictSeries(event.data, event.type, false)
    //     }
    //   }
    // })
    //   // handle the conflict of series recording
    // EventService.removeAllListeners(['SOLVE_CONFLICT_SERIES'])
    // EventService.on('SOLVE_CONFLICT_SERIES', (event) => {
    //   if (event.update) {
    //     this.conflictSeries(event.data, event.type, true)
    //   } else {
    //     this.conflictSeries(event.data, event.type, false)
    //   }
    // })
    //   // handle the conflict of all recording
    // EventService.removeAllListeners(['SOLVE_ALL_RECORD_CONFLICT'])
    // EventService.on('SOLVE_ALL_RECORD_CONFLICT', (event) => {
    //   if (event.update) {
    //     this.conflictSeries(event.data, event.type, true, 'true')
    //   } else {
    //     this.conflictSeries(event.data, event.type, false, 'true')
    //   }
    // })
    //   //  handle the conflict
    // EventService.removeAllListeners(['SOLVE_CONFLICT'])
    // EventService.on('SOLVE_CONFLICT', (event) => {
    //   if (event.update) {
    //     this.conflict(event.data, event.type, true)
    //   } else {
    //     this.conflict(event.data, event.type)
    //   }
    // })
      // if the user doesnot handle the conflict show the weak tip
    EventService.removeAllListeners(['CONFLICT_RECORDED_FAIL'])
    EventService.on('CONFLICT_RECORDED_FAIL', () => {
      this.showWeakit({ type: 'failed_to_add_a_recording_task' })
    })
      // play npvr from the record of suspension
    EventService.on('PLAYNPVR', (data) => {
      if (document.getElementsByClassName('live_load_bg') && document.querySelector('.channel_login_load_content')) {
        document.getElementsByClassName('live_load_bg')[0]['style']['display'] = 'block'
        document.querySelector('.channel_login_load_content')['style']['display'] = 'block'
      }
        // judge the status of the record which can play
      if (data.status === 'SUCCESS' || data.status === 'OTHER' || data.status === 'PART_SUCCESS' || data.status === 'RECORDING') {
        // this.nPVRAppService.playNPVR(data)
          // open the full screen according to the different browser
        EventService.emit('OPEN_FULLSCREEN_LIVETVVIDEO', {})
        let video = document.querySelector('#videoContainer video')
        let ua = navigator.userAgent.toLowerCase()
        let isIe = !!ua.match(/msie/i) || !!ua.match(/rv:([\d.]+).*like gecko/)
        let isFirefox = !!ua.match(/firefox\/([\d.]+)/)
        let isEdge = !!ua.match(/edge\/([\d.]+)/)
        if (isIe) {
          video = document.querySelector('#videoContainer')
          video['msRequestFullscreen']()
        } else if (isEdge) {
          video = document.querySelector('#videoContainer')
          video.webkitRequestFullScreen()
        } else if (isFirefox) {
          video = document.querySelector('#videoContainer')
          video['mozRequestFullScreen']()
        } else if (ua.match(/version\/([\d.]+).*safari/)) {
          video.requestFullscreen()
        } else {
          video.webkitRequestFullScreen()
        }
      }
    })
      // close the weak tip dialog
    EventService.on('CLOSED_WEAKTIP', () => {
      this.judgeWeakit = false
      this.clearVideoChild()
    })
      // when the reminder dialog is close,set the open flag false in the reminder task
    EventService.removeAllListeners(['CLOSE_REMINDER_DIALOG'])
    EventService.on('CLOSE_REMINDER_DIALOG', () => {
      this.reminderTask['isOpenDialog'] = false
    })
      // when add reminder,put the new reminder in the cache
    EventService.removeAllListeners(['ADD_REMINDER'])
    EventService.on('ADD_REMINDER', (data) => {
      this.reminderTask.addOrRemoveReminder('add', data)
    })
      // when delete reminder,delete the reminder from the cache
    EventService.removeAllListeners(['REMOVE_REMINDER'])
    EventService.on('REMOVE_REMINDER', (data) => {
      EventService.emit('REMOVE_REMINDER_BY_SUSPENSION')
      this.reminderTask.addOrRemoveReminder('remove', data)
    })
      // delete favorite
    EventService.on('ADDFAVORITE', req => {
      this.commonService.removeFavorite(req)
    })
      // add favorite
    EventService.on('REMOVEFAVORITE', req => {
      this.commonService.addFavorite(req)
    })
      // add or delete Reminder success
    EventService.removeAllListeners(['addReminderSuccess'])
    EventService.on('addReminderSuccess', () => {
      this.showWeakit({ type: 'add_reminder_success' })
    })
    EventService.removeAllListeners(['removeReminderSuccess'])
    EventService.on('removeReminderSuccess', () => {
      this.showWeakit({ type: 'successfully_removed_from_reminder' })
    })
      // the weak tip of delete favorite
    EventService.on('REMOVE_successful', () => {
      this.showWeakit({ type: 'successfully_removed_from_favorites' })
    })
      // the weak tip of add favorite
    EventService.on('ADD_successful', () => {
      this.showWeakit({ type: 'successfully_added_to_favorites' })
    })
      // the weak tip of delete favorite in the vod detail page
    EventService.removeAllListeners(['REMOVEVODDETAIL_successful'])
    EventService.on('REMOVEVODDETAIL_successful', () => {
      this.showWeakit({ type: 'successfully_removed_from_favorites' })
    })
      // the weak tip of add favorite in the vod detail page
    EventService.removeAllListeners(['ADDVODDETAIL_successful'])
    EventService.on('ADDVODDETAIL_successful', () => {
      this.showWeakit({ type: 'successfully_added_to_favorites' })
    })
      // the weak tip of add record successfully
    EventService.removeAllListeners(['ADD_RECORDED_SUCCESS'])
    EventService.on('ADD_RECORDED_SUCCESS', () => {
      this.showWeakit({ type: 'add_recording_is_successful' })
    })
      // the weak tip of delete record successfully
    EventService.removeAllListeners(['DELETE_RECORDED_SUCCESS'])
    EventService.on('DELETE_RECORDED_SUCCESS', () => {
      this.showWeakit({ type: 'cancel_recording_is_successful' })
    })
      // the weak tip of period date setting
    EventService.removeAllListeners(['END_DATA_MUST_LATER'])
    EventService.on('END_DATA_MUST_LATER', () => {
      this.showWeakit({ type: 'end_date_must_later_than_start_date' })
    })
      // the wak tip of time-based recording when the start time is illegal.
    EventService.removeAllListeners(['START_TIME_SHOULD_BE_LATER'])
    EventService.on('START_TIME_SHOULD_BE_LATER', () => {
      this.showWeakit({ type: 'recording_time_incorrect_msg' })
    })
      // the weak tip of modify subprofile info
    EventService.removeAllListeners(['PROFILE_INFO_SAVE_SUCCESSFUL'])
    EventService.on('PROFILE_INFO_SAVE_SUCCESSFUL', () => {
      this.showWeakit({ type: 'operation_successfully' })
    })
      // when user click the package content in my purchase , it will show toast to notify user
    EventService.removeAllListeners(['PACKAGE_CANT_OPEN'])
    EventService.on('PACKAGE_CANT_OPEN', () => {
      this.showWeakit({ type: 'package_could_not_open' })
    })
      // the weak tip of add lock
    EventService.removeAllListeners(['ADDLOCK_successful'])
    EventService.on('ADDLOCK_successful', () => {
      this.showWeakit({ type: 'lock_sucessfully' })
    })
      // the weak tip of delete lock
    EventService.removeAllListeners(['UNLOCK_successful'])
    EventService.on('UNLOCK_successful', () => {
      this.showWeakit({ type: 'unlock_successfully' })
    })
      // the weak tip of illegal search input
    EventService.removeAllListeners(['VERIFICATE_SEARCHKEY'])
    EventService.on('VERIFICATE_SEARCHKEY', () => {
      this.showWeakit({ type: 'input_does_not_conform_rules' })
    })
      // the weak tip of the period record's illegal repeat date
    EventService.removeAllListeners(['REPEAT_AND_DATE_ILLEGAL'])
    EventService.on('REPEAT_AND_DATE_ILLEGAL', () => {
      this.showWeakit({ type: 'repeat_and_date_conflict' })
    })
      // the weak tip of the record when the time is empty
    EventService.removeAllListeners(['TIME_INPUT_EMPTY'])
    EventService.on('TIME_INPUT_EMPTY', () => {
      this.showWeakit({ type: 'time_input_is_empty' })
    })
      // the weak tip of the record when the name is incorrect
    EventService.removeAllListeners(['RECORD_NAME_INPUT_INCORRECT'])
    EventService.on('RECORD_NAME_INPUT_INCORRECT', checkMsg => {
      this.showWeakit({ type: checkMsg }, true)
    })
      // get the vod detail info for the vod suspension
    EventService.removeAllListeners(['GETVODDETAIL'])
    EventService.on('GETVODDETAIL', id => {
      this.getVodDetail(id)
    })
      // get the playbill detail info the  for the program suspension
    EventService.removeAllListeners(['GET_PLAYBILLDETAIL'])
    EventService.on('GET_PLAYBILLDETAIL', req => {
      this.liveTvService.getDetail(req['playbillID'], req['sceneID']).then((resp) => {
        EventService.emit('SET_PLAYBILLDETAIL', resp)
      })
    })
      // get the playbill detail info the  for the channel suspension
    EventService.removeAllListeners(['GET_CHANNELDETAIL'])
    EventService.on('GET_CHANNELDETAIL', req => {
      this.liveTvService.getDetail(req).then((resp) => {
        EventService.emit('SET_SEARCH_CHANNELDETAIL', resp)
      })
    })
      // when the user login change the user name in the navigation
    EventService.on('LOGIN', () => {
      this.changeProfileName()
    })
      // when the user doesnot login, show the login dialog
    EventService.removeAllListeners(['UILOGIN'])
    EventService.on('UILOGIN', () => {
      this.login()
    })
      // when the user modify his profile name,change the user name in the navigation
    EventService.on('CHANGE_PROFILE_NAME', () => {
      this.changeProfileName()
    })
      // play program from suspension or commponent
    EventService.on('PLAYLIVETV', data => {
      if (document.getElementsByClassName('live_load_bg') && document.querySelector('.channel_login_load_content')) {
        document.getElementsByClassName('live_load_bg')[0]['style']['display'] = 'block'
        document.querySelector('.channel_login_load_content')['style']['display'] = 'block'
      }
      EventService.emit('OPEN_FULLSCREEN_LIVETVVIDEO', {})
      let video = document.querySelector('#videoContainer video')
      let ua = navigator.userAgent.toLowerCase()
      let isIe = !!ua.match(/msie/i) || !!ua.match(/rv:([\d.]+).*like gecko/)
      let isFirefox = !!ua.match(/firefox\/([\d.]+)/)
      let isEdge = !!ua.match(/edge\/([\d.]+)/)
      if (isIe) {
        video = document.querySelector('#videoContainer')
        video['msRequestFullscreen']()
      } else if (isEdge) {
        video = document.querySelector('#videoContainer')
        video.webkitRequestFullScreen()
      } else if (isFirefox) {
        video = document.querySelector('#videoContainer')
        video['mozRequestFullScreen']()
      } else if (ua.match(/version\/([\d.]+).*safari/)) {
        video.requestFullscreen()
      } else {
        video.webkitRequestFullScreen()
      }
      this.playbillAppService.playProgram(data['playbillInfo'], data['channelInfo'])
    })
      // play TVOD from suspension or commponent
    EventService.on('PLAYTVOD', (data) => {
      if (document.getElementsByClassName('live_load_bg') && document.querySelector('.channel_login_load_content')) {
        document.getElementsByClassName('live_load_bg')[0]['style']['display'] = 'block'
        document.querySelector('.channel_login_load_content')['style']['display'] = 'block'
      }
      let playbillInfo
      this.liveTvService.getDetail(data['id']).then(resp => {
        playbillInfo = resp['playbillDetail']
        this.tVODAppService.playTvod(playbillInfo, playbillInfo['channelDetail'])
      })
      EventService.emit('OPEN_FULLSCREEN_LIVETVVIDEO', {})
      let video = document.querySelector('#videoContainer video')
      let ua = navigator.userAgent.toLowerCase()
      let isIe = !!ua.match(/msie/i) || !!ua.match(/rv:([\d.]+).*like gecko/)
      let isFirefox = !!ua.match(/firefox\/([\d.]+)/)
      let isEdge = !!ua.match(/edge\/([\d.]+)/)
      if (isIe) {
        video = document.querySelector('#videoContainer')
        video['msRequestFullscreen']()
      } else if (isEdge) {
        video = document.querySelector('#videoContainer')
        video.webkitRequestFullScreen()
      } else if (isFirefox) {
        video = document.querySelector('#videoContainer')
        video['mozRequestFullScreen']()
      } else if (ua.match(/version\/([\d.]+).*safari/)) {
        video.requestFullscreen()
      } else {
        video.webkitRequestFullScreen()
      }
    })
      // play channel from suspension or commponent
    EventService.on('PLAYCHANNEL', (id) => {
      if (document.getElementsByClassName('live_load_bg') && document.querySelector('.channel_login_load_content')) {
        document.getElementsByClassName('live_load_bg')[0]['style']['display'] = 'block'
        document.querySelector('.channel_login_load_content')['style']['display'] = 'block'
      }
        // get the program of this channel
      let channelDe
      let options = {
        params: {
          SID: 'queryplaybilllist3',
          DEVICE: 'PC',
          DID: session.get('uuid_cookie')
        }
      }
      this.liveTvService.getQueryPlaybill({
        needChannel: '0',
        queryPlaybill: {
          type: '1',
          count: '1',
          offset: '0'
        },
        queryChannel: {
          channelIDs: [id],
          isReturnAllMedia: '1'
        }
      }, options).then(resp => {
          // set the channel info from the channel cache
        channelDe = resp.channelPlaybills[0]
        let channelDtails = this.channelCacheService.getStaticChannelData().channelDetails
        let dynamicChannelData = this.channelCacheService.getDynamicChannelData().channelDynamaicProp
        channelDe['channelInfos'] = _.find(channelDtails, (detail) => {
          return detail['ID'] === channelDe['playbillLites'][0]['channelID']
        })
        _.find(dynamicChannelData, (detail) => {
          if (detail && channelDe['channelInfos'] && channelDe['channelInfos']['ID'] === detail['ID']) {
            channelDe['channelInfos']['channelNO'] = detail['channelNO']
            let nameSpace = session.get('ott_channel_name_space')
            let physicalChannelsDynamicProperty = _.find(detail['physicalChannelsDynamicProperties'], (property) => {
              return _.contains(property['channelNamespaces'], nameSpace)
            })
            channelDe['channelInfos']['physicalChannelsDynamicProperty'] =
                            physicalChannelsDynamicProperty || detail['physicalChannelsDynamicProperties'][0]
          }
        })
        this.playbillAppService.playProgram(channelDe['playbillLites'][0], channelDe['channelInfos'])
          // close all the opened dialog,when play a new channel
        EventService.emit('HIDE_POPUP_DIALOG')
      })
        // open the full screen according to different browser
      EventService.emit('OPEN_FULLSCREEN_LIVETVVIDEO', {})
      let video = document.querySelector('#videoContainer video')
      let ua = navigator.userAgent.toLowerCase()
      let isIe = !!ua.match(/msie/i) || !!ua.match(/rv:([\d.]+).*like gecko/)
      let isFirefox = !!ua.match(/firefox\/([\d.]+)/)
      let isEdge = !!ua.match(/edge\/([\d.]+)/)
      if (isIe) {
        video = document.querySelector('#videoContainer')
        video['msRequestFullscreen']()
      } else if (isEdge) {
        video = document.querySelector('#videoContainer')
        video.webkitRequestFullScreen()
      } else if (isFirefox) {
        video = document.querySelector('#videoContainer')
        video['mozRequestFullScreen']()
      } else if (ua.match(/version\/([\d.]+).*safari/)) {
        video.requestFullscreen()
      } else {
        video.webkitRequestFullScreen()
      }
    })
      // get query play bill for playing channel from suspension only
    EventService.on('GET_QUERYPLAYBILL', (data) => {
      let channelDe
      let curTime = Date.now()['getTime']()
      let options = {
        params: {
          SID: data['sceneID'],
          DEVICE: 'PC',
          DID: session.get('uuid_cookie')
        }
      }
      this.liveTvService.getQueryPlaybill({
        needChannel: '0',
        queryPlaybill: {
          type: '0',
          count: '1',
          offset: '0',
          startTime: curTime + ''
        },
        queryChannel: {
          channelIDs: [data['id']],
          isReturnAllMedia: '1'
        }
      }, options).then(resp => {
          // set the channel info from the channel cache
        channelDe = resp.channelPlaybills[0]
        let channelDtails = this.channelCacheService.getStaticChannelData().channelDetails
        let dynamicChannelData = this.channelCacheService.getDynamicChannelData().channelDynamaicProp
        channelDe['channelInfos'] = _.find(channelDtails, (detail) => {
          return detail['ID'] === channelDe['playbillLites'][0]['channelID']
        })
        _.find(dynamicChannelData, (detail) => {
          if (detail && channelDe['channelInfos'] && channelDe['channelInfos']['ID'] === detail['ID']) {
            channelDe['channelInfos']['channelNO'] = detail['channelNO']
            let nameSpace = session.get('ott_channel_name_space')
            let physicalChannelsDynamicProperty = _.find(detail['physicalChannelsDynamicProperties'], (property) => {
              return _.contains(property['channelNamespaces'], nameSpace)
            })
            channelDe['channelInfos']['physicalChannelsDynamicProperty'] =
                            physicalChannelsDynamicProperty || detail['physicalChannelsDynamicProperties'][0]
          }
        })
        this.realplaybill = channelDe['playbillLites'][0]
        this.realchannel = channelDe['channelInfos']
        EventService.emit('SET_CHANNELDETAIL', resp)
      })
    })
      // play channel from suspension only
    EventService.on('PLAY_MYCHANNEL', (isHighLightName) => {
      this.playbillAppService.playProgram(this.realplaybill, this.realchannel)
      EventService.emit('HIDE_POPUP_DIALOG')
      EventService.emit('OPEN_FULLSCREEN_LIVETVVIDEO', {})
      let video = document.querySelector('#videoContainer video')
      let ua = navigator.userAgent.toLowerCase()
      let isIe = !!ua.match(/msie/i) || !!ua.match(/rv:([\d.]+).*like gecko/)
      let isFirefox = !!ua.match(/firefox\/([\d.]+)/)
      let isEdge = !!ua.match(/edge\/([\d.]+)/)
      if (isIe) {
        video = document.querySelector('#videoContainer')
        video['msRequestFullscreen']()
      } else if (isEdge) {
        video = document.querySelector('#videoContainer')
        video.webkitRequestFullScreen()
      } else if (isFirefox) {
        video = document.querySelector('#videoContainer')
        video['mozRequestFullScreen']()
      } else if (ua.match(/version\/([\d.]+).*safari/)) {
        video.requestFullscreen()
      } else {
        video.webkitRequestFullScreen()
      }
    })
      // when the mouse into the user name, reset the name in high light
    EventService.on('HIGH_LIGHT_NAME', (isHighLightName) => {
      this.isHighLight = isHighLightName === '1'
    })

    // --------------- recording
    //   // open the record dialog,when set record from commponet
    // EventService.on('LIVETV_RECORD', (playbillDetail) => {
    //   this.showPlaybillRecord = true
    //   this.recordDialogConfig = playbillDetail
    //   _.delay(() => {
    //     this.firefoxUp(document.querySelector('app-record-dialog'))
    //   }, 200)
    // })
    //   // close the record dialog
    // EventService.on('CLOSED_RECORD_DIALOG', () => {
    //   this.showPlaybillRecord = false
    //   this.recordDialogConfig = {}
    // })
    //   // canncel or stop the record dialog from recording suspension
    // EventService.removeAllListeners(['RECORD_CENCEL'])
    // EventService.on('RECORD_CENCEL', (data) => {
    //     // cancel the record
    //   if (data[1] && data[1] === 'cancel_record') {
    //     this.recordDataService.cancelPVRByID([data[0]], data[2]).then(() => {
    //       EventService.emit('LIVETV_CENCELRECORD', {})
    //     })
    //   }
    //     // stop the record
    //   if (data[1] && data[1] === 'stop_recording') {
    //     if (document.querySelector('md-dialog-container app-stop-record-confirm') === null) {
    //       this.commonService.openDialog(StopRecordConfirmComponent, null, data)
    //     }
    //   }
    // })
    //   // canncel or stop the record dialog from channel or program suspension
    // EventService.on('LIVETV_CENCEL', (data) => {
    //   if (data[1] && data[1] === 'cancel_record') {
    //     this.recordDataService.cancelRecord([data[0]]).then(() => {
    //       EventService.emit('LIVETV_CENCELRECORD', {})
    //     })
    //   }
    //   if (data[1] && data[1] === 'stop_recording') {
    //       // open the stop record confirm dialog
    //     if (document.querySelector('md-dialog-container app-stop-record-confirm') === null) {
    //       this.commonService.openDialog(StopRecordConfirmComponent, null, data[0])
    //     }
    //   }
    // })

    // --------------

      // add reminder from suspension
    EventService.removeAllListeners(['ADD_SUSPENSIONREMINDER'])
    EventService.on('ADD_SUSPENSIONREMINDER', (resp, respa) => {
      this.reminderService.addReminder({ reminders: [resp] }).then(() => {
        EventService.emit('ADD_REMINDER', respa)
      })
    })
      // delete reminder from suspension
    EventService.removeAllListeners(['CANCEL_SUSPENSIONREMINDER'])
    EventService.on('CANCEL_SUSPENSIONREMINDER', (resp, respa) => {
      this.reminderService.removeReminder({ contentIDs: resp['contentID'], contentTypes: resp['contentType'] }).then(() => {
        EventService.emit('REMOVE_REMINDER', respa)
      })
    })
    //   // open the recording confilcit dialog
    // EventService.removeAllListeners(['RECORD_CONFLICT'])
    // EventService.on('RECORD_CONFLICT', (type) => {
    //   if (document.querySelector('md-dialog-container app-recording-conflict') === null) {
    //     this.commonService.openDialog(RecordingConflictComponent, null, type)
    //   }
    // })
      // open the recording mixed confilcit dialog
    EventService.removeAllListeners(['MIXED_CONFLICT'])
    EventService.on('MIXED_CONFLICT', (type) => {
      if (document.querySelector('md-dialog-container app-mixed-conflict') === null) {
        this.commonService.openDialog(MixedConflictComponent, null, type)
      }
    })
    //   /**
    //      * when recording space is not enough, the Storage Reminder dialog will show.
    //      */
    // EventService.removeAllListeners(['RECORD_SPACE_NOT_ENOUGH'])
    // EventService.on('RECORD_SPACE_NOT_ENOUGH', (data) => {
    //     // if the Storage Reminder Dialog is not on the page, it will show. Otherwise, it will not show again.
    //   if (document.querySelector('md-dialog-container app-storageremind') === null) {
    //     if (data['type'] === 'successful') {
    //       this.commonService.openDialog(StorageRemindComponent, null, data)
    //     } else if (data['type'] === 'failed') {
    //       this.commonService.openDialog(StorageRemindComponent, null, data)
    //     }
    //   }
    // })
    //   // open the update dialog of recording task
    // EventService.removeAllListeners(['POP_UP_NPVR_RECORD_SETTING'])
    // EventService.on('POP_UP_NPVR_RECORD_SETTING', (recordDetails) => {
    //   recordDetails['update'] = true
    //   this.updateRecordData = recordDetails
    //   this.showUpdateReocrd = true
    // })
      // open the update dialog of manual recording task
    // EventService.removeAllListeners(['POP_UP_MANUAL_RECORD_SETTING'])
    // EventService.on('POP_UP_MANUAL_RECORD_SETTING', (recordDetails) => {
    //   this.manualRecordPopUp.isShowManualPopUp = true
    //   this.manualRecordPopUp.loadData(recordDetails)
    // })
      // show the error message dialog
    EventService.removeAllListeners(['ERROR_MESSAGE_DIALOG'])
    EventService.on('ERROR_MESSAGE_DIALOG', (errorObject) => {
      this.showErrorDialog = true
      this.errorDialogConfig = errorObject
      _.delay(() => {
        this.firefoxUp(document.querySelector('app-error-message-dialog'))
        if (document.getElementById('error-container-wrapper')) {
          document.getElementById('error-container-wrapper').focus()
        }
      }, 200)
    })
      // close the error message dialog
    EventService.removeAllListeners(['CLOSED_ERROR_MESSAGE_DIALOG'])
    EventService.on('CLOSED_ERROR_MESSAGE_DIALOG', () => {
      this.showErrorDialog = false
      this.errorDialogConfig = {}
    })
      // show the device list dialog
    EventService.removeAllListeners(['DEVICELIST_OPEN'])
    EventService.on('DEVICELIST_OPEN', (data) => {
      this.showDeviceListDialog = true
      this.deviceDialogConfig = data
    })
      // close the device list dialog
    EventService.removeAllListeners(['DEVICELIST_CLOSE'])
    EventService.on('DEVICELIST_CLOSE', () => {
      this.showDeviceListDialog = false
      this.deviceDialogConfig = {}
    })
      // preference setting successfully
    EventService.removeAllListeners(['preference_setting_successfully'])
    EventService.on('preference_setting_successfully', () => {
      this.showWeakit({ type: 'preference_setting_successfully' })
    })
      // if the program of the programSuspension is fillProgram and watching now
    EventService.removeAllListeners(['SET_FILLPROGRAM'])
    EventService.on('SET_FILLPROGRAM', req => {
      let product
      product = { type: 'no_program_information', name: req.name }
      this.showWeakit(product)
    })
    //   // the weak tip of recording failed
    // EventService.removeAllListeners(['SHOW_RECORD_FAIL_TOAST'])
    // EventService.on('SHOW_RECORD_FAIL_TOAST', () => {
    //   this.showWeakit({ type: 'failed_to_add_a_recording_task' })
    // })
    //   // the weak tip of recording operation failed
    // EventService.removeAllListeners(['RECORD_OPERATION_FAILED'])
    // EventService.on('RECORD_OPERATION_FAILED', () => {
    //   this.showWeakit({ type: 'record_operation_failed' })
    // })
      // the weak tip of vsp error according to the error type of the vsp inferface
    EventService.removeAllListeners(['SHOW_VSP_ERROR_TOAST'])
    EventService.on('SHOW_VSP_ERROR_TOAST', message => {
      this.showWeakit({ type: message }, true)
    })
      // open the login dialog
    EventService.removeAllListeners(['SHOW_LOGIN_DIALOG'])
    EventService.on('SHOW_LOGIN_DIALOG', () => {
      if (!document.querySelector('md-dialog-container app-account-login')) {
        this.commonService.openDialog(AccountLoginComponent)
      }
    })
      // the weak tip of DST jumping
    EventService.removeAllListeners(['DST_JUMPING_TOAST'])
    EventService.on('DST_JUMPING_TOAST', () => {
      this.showWeakit({ type: 'DST_jumping_prompt' })
    })
      // the weak tip of no program
    EventService.removeAllListeners(['NO_PROGRAM_TOAST'])
    EventService.on('NO_PROGRAM_TOAST', () => {
      this.showWeakit({ type: 'no_program_information' })
    })
      // the weak tip of that sub profile cannot add lock
    EventService.removeAllListeners(['CHANNEL_LOCK_TOAST'])
    EventService.on('CHANNEL_LOCK_TOAST', () => {
      this.showWeakit({ type: 'do_not_have_permission' })
    })
      // the weak tip of no next tvod
    EventService.removeAllListeners(['NoNextTVOD'])
    EventService.on('NoNextTVOD', (resp) => {
      this.showWeakit({ type: 'Last_Catch_Up' })
    })
      // open the dialog of modify init password
    EventService.removeAllListeners(['OPEN_CHANGE_INIT_PASSWORD_DIALOG'])
    EventService.on('OPEN_CHANGE_INIT_PASSWORD_DIALOG', (resp) => {
      this.showChangePassword = true
    })
    EventService.on('SETWHITECSSSTYLE', (languageType) => {
      this.setFontFamily(languageType)
    })
    EventService.removeAllListeners(['PROFILE_UPDATED'])
    EventService.on('PROFILE_UPDATED', () => {
      this.changeProfileName()
    })

    EventService.removeAllListeners(['RECORDFAILED'])
    EventService.on('RECORDFAILED', (data) => {
      this.showWeakit({ type: 'Failed_play_recording' })
    })
      // start the heartbeat
    // this.heartbeatStart()
  }

  ngAfterViewInit () {
    this.store.dispatch({ type: APP_INIT_LOAD_END })
  }

    /**
     * when the profile login,start the online heartbeat
     */
  heartbeatStart () {
    if (Cookies.getJSON('IS_PROFILE_LOGIN') || Cookies.getJSON('IS_GUEST_LOGIN')) {
      this.heartbeatService.startHeartbeat().then(response => {
        if (_.isUndefined(response['personalDataVersions']) || _.isUndefined(response['personalDataVersions']['channel']) ||
                    response['personalDataVersions']['channel'] === '') {
          this.commonService.MSAErrorLog(38054, 'OnLineHeartbeat')
        }
      })
    }
  }

    /**
     * clear the timer when destory the page
     */
  ngOnDestroy () {
    clearInterval(this.disAppearTimer)
    clearTimeout(this.getWeakit)
    clearTimeout(this.nextWeakTip)
  }

    /**
     * judge the browser
     */
  firefoxUp (documentSelector, type?: any, isShow?: any) {
    let ua = navigator.userAgent.toLowerCase()
    let isIe = !!ua.match(/msie/i) || !!ua.match(/rv:([\d.]+).*like gecko/)
    let isEdge = !!ua.match(/edge\/([\d.]+)/)
    let isFirefox = !!ua.match(/firefox\/([\d.]+)/)
      // not chrome
    if (isIe || isEdge || isFirefox) {
      this.setVideo(type, isShow, documentSelector)
    }
  }

    /**
     * if the browser is not chrome,set the video element
     */
  setVideo (type, isShow, documentSelector) {
    let channelvideo = document.querySelector('#videoContainer')
    let vodVideo = document.querySelector('#vodVideoContainer')
    let MediaplayerChannel = this.mediaPlayService.isFullScreen()
    let MediaplayerVod = this.mediaPlayService.isFullScreen()
      // TODO: Think how this affects non-player pages
    if (MediaplayerChannel && (location.pathname.indexOf('video') === -1 || location.pathname.indexOf('movie') === -1)) {
      if (type === 'component_weaktip_dialog') {
        if (!document.querySelector('#videoContainer #component_weaktip_dialog') && isShow) {
          let documentSelector2 = documentSelector.cloneNode(true)
          channelvideo.appendChild(documentSelector2)
          document.querySelector('#videoContainer #component_weaktip_dialog')['style']['display'] = 'block'
        } else if (!isShow) {
          channelvideo.removeChild(documentSelector)
        }
      } else {
        channelvideo.appendChild(documentSelector)
      }
    } else if (MediaplayerVod) {
      vodVideo.appendChild(documentSelector)
    } else if (!MediaplayerChannel) {
      this.clearVideoChild()
    }
  }

    /**
     * A common function used to the situation when not Fullscreen , clear the dialog which in the videoContainer
     */
  clearVideoChild () {
    let clearParentDom = document.querySelector('#videoContainer')
    let clearDom = document.querySelector('#videoContainer #component_weaktip_dialog')
    if (clearDom) {
      clearParentDom.removeChild(clearDom)
    }
  }

    /**
     * solve the series of conflict and mixed conflict
     */
  // conflictSeries (data, type, updat, event?) {
  //     // if update the series conflict
  //   if (updat) {
  //     this.recordDataService.updatePeriodicPVR(data, type, event).then(resp => {
  //       if (resp['isNPVRSpaceEnough'] && resp['isNPVRSpaceEnough'] === '0') {
  //         EventService.emit('RECORD_SPACE_NOT_ENOUGH', { type: 'successful' })
  //       }
  //     }, resp => {
  //       let conflictData = { resp: resp, data: data, type: type, update: true }
  //       if (resp['result']['retCode'] === '147020005') {
  //         EventService.emit('RECORD_SPACE_NOT_ENOUGH', { type: 'failed' })
  //       } else if (resp.conflictGroups && resp.conflictGroups !== '') {
  //         if (resp['conflictGroups'][0]['PVRList'][0]['policyType'] === 'PlaybillBased' ||
  //                       resp['conflictGroups'][0]['PVRList'][0]['policyType'] === 'TimeBased') {
  //             EventService.emit('MIXED_CONFLICT', conflictData)
  //           } else {
  //             EventService.emit('RECORD_CONFLICT', conflictData)
  //           }
  //       }
  //     })
  //   } else {
  //     this.recordDataService.addPeriodicPVR(data, type, event).then(resp => {
  //       if (resp['isNPVRSpaceEnough'] && resp['isNPVRSpaceEnough'] === '0') {
  //         EventService.emit('RECORD_SPACE_NOT_ENOUGH', { type: 'successful' })
  //       } else {
  //         EventService.emit('ADD_RECORDED_SUCCESS')
  //         EventService.emit('SLOVE_SERIES_CONFLICT', data)
  //       }
  //       EventService.emit('MANUAL_SET_AFTER_SOLVE_CONFLICT')
  //     }, resp => {
  //       let conflictData = {}
  //         // A PVR task has been created for the program.
  //       if (resp['result']['retCode'] === '126014701') {
  //         EventService.emit('ADD_RECORDED_SUCCESS')
  //         EventService.emit('SLOVE_SERIES_CONFLICT', data)
  //         EventService.emit('MANUAL_SET_AFTER_SOLVE_CONFLICT')
  //           // There is no sufficient nPVR space for immediate recording.
  //       } else if (resp['result']['retCode'] === '147020005') {
  //         EventService.emit('RECORD_SPACE_NOT_ENOUGH', { type: 'failed' })
  //       } else if (resp.conflictGroups && resp.conflictGroups !== '') {
  //           conflictData = { resp: resp, data: data, type: type }
  //           if (resp['conflictGroups'][0]['PVRList'][0]['policyType'] === 'PlaybillBased' ||
  //                       resp['conflictGroups'][0]['PVRList'][0]['policyType'] === 'TimeBased') {
  //             EventService.emit('MIXED_CONFLICT', conflictData)
  //           } else {
  //             EventService.emit('RECORD_CONFLICT', conflictData)
  //           }
  //         }
  //     })
  //   }
  // }

    // solve single conflict
  // conflict (data, type, updat?) {
  //   if (updat) {
  //     this.recordDataService.updatePVR(data, type).then(resp => {
  //       if (resp['isNPVRSpaceEnough'] && resp['isNPVRSpaceEnough'] === '0') {
  //         EventService.emit('RECORD_SPACE_NOT_ENOUGH', { type: 'successful' })
  //       }
  //     }, resp => {
  //       let conflictData = {}
  //         // There is no sufficient nPVR space for immediate recording
  //       if (resp['result']['retCode'] === '147020005') {
  //         EventService.emit('RECORD_SPACE_NOT_ENOUGH', { type: 'failed' })
  //       } else if (resp.conflictGroups && resp.conflictGroups !== '') {
  //         conflictData = { resp: resp, data: data, type: type, update: true }
  //         EventService.emit('RECORD_CONFLICT', resp)
  //       }
  //     })
  //   } else {
  //     this.recordDataService.addPVR(data, type).then(resp => {
  //       if (resp['isNPVRSpaceEnough'] && resp['isNPVRSpaceEnough'] === '0') {
  //         EventService.emit('RECORD_SPACE_NOT_ENOUGH', { type: 'successful' })
  //       } else {
  //         EventService.emit('ADD_RECORDED_SUCCESS')
  //       }
  //       EventService.emit('MANUAL_SET_AFTER_SOLVE_CONFLICT')
  //       if (type === 'PlaybillBasedPVR') {
  //         EventService.emit('PLAYBILL_TASK_SOLVE_CONFLICT', data['playbillID'])
  //       }
  //     }, resp => {
  //       let conflictData = {}
  //         // A PVR task has been created for the program.
  //       if (resp['result']['retCode'] === '126014701') {
  //         EventService.emit('ADD_RECORDED_SUCCESS')
  //         EventService.emit('MANUAL_SET_AFTER_SOLVE_CONFLICT')
  //         if (type === 'PlaybillBasedPVR') {
  //           EventService.emit('PLAYBILL_TASK_SOLVE_CONFLICT', data['playbillID'])
  //         }
  //       } else if (resp['result']['retCode'] === '147020005') {
  //         EventService.emit('RECORD_SPACE_NOT_ENOUGH', { type: 'failed' })
  //       } else if (resp.conflictGroups && resp.conflictGroups !== '') {
  //           conflictData = { resp: resp, data: data, type: type }
  //           EventService.emit('RECORD_CONFLICT', conflictData)
  //         }
  //     })
  //   }
  // }

    /**
     * get the vod detail for the suspension
     */
  getVodDetail (id) {
    this.commonService.getVodDetail({ VODID: id || '' }).then((resp) => {
      EventService.emit('SETVODDETAIL', resp)
    })
  }

    /**
     * get the email address if it exists
     */
  getSugesstion () {
    if (!session.get('SUGGESTION_EMAIL_TEST')) {
      EventService.on('LANGUAGE_QUERYCUSTOMIZE', () => {
        this.timerService.getSugess().then((resp) => {
          this.mailto = resp
        })
      })
    } else {
      this.mailto = session.get('SUGGESTION_EMAIL_TEST')
    }
  }

    /**
     * set the navigation style according to the route
     */
  setStyles (id): void {
    const detectWords = {
      'on-demand': ['video', 'series', 'season', 'movie', 'cast', 'category'],
      'home': ['home'],
      'live-tv': ['live-tv']
    }

    let isFocused = false
    let words = detectWords[id]
    for (let i = 0; i < words.length; i++) {
      if (location.pathname.indexOf(words[i]) !== -1) {
        isFocused = true
      }
    }

    let className = isFocused ? 'title-classOne' : 'title-classTwo'
    if (document.getElementById(id)) {
      document.getElementById(id).className = className
    }

    this.judgeGuide = location.pathname.indexOf('live-tv/guide') === -1

    if (this.currentRouteUrl !== location.pathname) {
      _.delay(() => {
        document.body.scrollTop = 0
        document.documentElement.scrollTop = 0
      }, 0)
      this.trackService.onPageChange(this.currentRouteUrl, location.pathname)
      this.currentRouteUrl = location.pathname
    }
  }

    /**
     * when is at my-tv page, highlight the profile-name
     */
  setStyle () {
    let style
    if (location.pathname.indexOf('profile') !== -1) {
      style = {
        color: '#f5f5f5'
      }
    }
    return style
  }

    /**
     * if the guest logins and click the login button,show the login dialog
     * if the profile logins and click the avatar, inter my page
     */
  login () {
    if (Cookies.getJSON('IS_GUEST_LOGIN')) {
      EventService.emit('CLOSED_LOGIN_DIALOG')
      // Disable for memory leak
      // EventService.on('MD_DIALOG_CONTAINER', () => {
        // document.getElementsByTagName('md-dialog-container')[0]['style']['position'] = 'fixed'
      // })
      this.commonService.openDialog(AccountLoginComponent)
    } else if (Cookies.getJSON('IS_PROFILE_LOGIN')) {
      this.router.navigate(['./profile/historyVod'])
      session.put('RouteMessage', './profile/historyVod')
      EventService.emit('HISTORYVOD', 'historyVod')
    }
  }

    /**
     * show the my item list or not,according to mouse in or out
     */
  dealProfile (appear) {
    this.header.dealWithProfile(appear)
  }

    /**
     * when the page is tvguide and the window width less than 1440
     * change the scroll position
     */
  windowChangeSize () {
    let clientW: number = window.innerWidth
    if (location.pathname.indexOf('live-tv/guide') !== -1 && clientW <= 1440) {
      EventService.emit('LIVE_SCREEN_SIZE')
    }
  }

    /**
     * listen the screen changed
     */
  scrollfixed () {
    window.onscroll = () => {
      this.windowChangeSize()
    }
    let changeState = 0
    let windowWidth = window.innerWidth
    if (windowWidth > 1440 && windowWidth <= 1920) {
      changeState = 1
    } else {
      changeState = 2
    }
    window.onresize = () => {
      let clientW: number = window.innerWidth
        // TODO: Think how this affects non-player pages
      if (location.pathname.indexOf('video') !== -1 || location.pathname.indexOf('movie') !== -1) {
        EventService.emit('VOD_SCREEN_SIZE')
      }
      if (clientW > 1440 && clientW <= 1920) {
        if (changeState === 1) {
          return
        }
        if (window.location.pathname.indexOf('search') === 2) {
          EventService.emit('ScreenSizeSearch', clientW)
        } else {
          EventService.emit('ScreenSize', clientW)
        }
        changeState = 1
      } else {
        if (changeState === 2) {
          return
        }
        if (window.location.pathname.indexOf('search') === 2) {
          EventService.emit('ScreenSizeSearch', clientW)
        } else {
          EventService.emit('ScreenSize', clientW)
        }
        changeState = 2
      }
    }

    this.disAppearTime()
  }

    /**
     * handle the top button disppeared
     */
  disAppearTime () {
    clearInterval(this.disAppearTimer)
    this.disAppearTimer = setInterval(() => {
      this.timerService.disAppear()
    }, 2500)
  }

    /**
     * update the profile name on the navigation
     */
  changeProfileName () {
    if (Cookies.get('PROFILE_NAME')) {
      this.profileLogoName = Cookies.get('PROFILE_NAME')
    } else {
      this.profileLogoName = this.translate.instant('log_in')
    }
  }

    /**
     * go to the top of the page
     */
  gotoTop () {
    this.timerService.backTop()
  }

    /**
     * skip router message
     */
  skipRouteMessage (data) {
    document.body.scrollTop = 0
    this.router.navigate(['./bottom/', data])
    session.put('RouteMessage', './bottom/' + data)
  }

    /**
     * listen event
     */
  notification (eventName, data) {
    let message = {}
    switch (eventName) {
      case 'AJAX_FAIL':
        if (data.request.isIgnoreError) {
          return
        }
        message['url'] = data.request.url
        message['req'] = data.request.data
        message['resp'] = data.response.data
        message['usedTime'] = data.response.usedTime
        this.errorMessageService.handlePlatformError(this.viewContainerRef, message)
        break
      case 'AJAX_SUCCESS':
        message['url'] = data.request.url
        message['req'] = data.request.data
        message['resp'] = data.response.data
        if (!_.isUndefined(message['resp'].result) && !_.isUndefined(message['resp'].result.retCode) &&
                    message['resp'].result.retCode === '') {
          this.errorMessageService.handlePlatformError(this.viewContainerRef, message)
        }
          // this.saveUserFilter(data);
        break
      case 'PROFILE_VERSION_CHANGED':
        this.profileService.updateProfile()
        break
      default:
        break
    }
  }

    /**
     * get userfilter param
     */
  saveUserFilter (data) {
    if (data && data['request'] && data['request']['inf'] === 'OnLineHeartbeat') {
      if (data && data['response'] && data['response']['data'] && data['response']['data']['userFilter']) {
        session.put('USER_FILTER', data['response']['data']['userFilter'], true)
      }
    }
  }

    /**
     * pop weak tip when add favorite success
     */
  showWeakit (message, translated?: boolean) {
    if (this.trim(this.translate.instant(message.type)) === '' || this.trim(message.type) === '') {
      return
    }
    let toastObj = {
      'message': message,
      'translated': translated
    }
    if (!_.isEqual(this.toastQueue[this.toastQueue.length - 1], toastObj)) {
      this.toastQueue.push(toastObj)
    } else {
      return
    }
    if (this.toastQueue.length > 0) {
      if (this.toastQueue.length === 1) {
        this.toastShowTime = 5000
        this.toastStartTime = Date.now()['getTime']()
      } else {
        let enterTime = Date.now()['getTime']()
        let hasShowTime = enterTime - this.toastStartTime
        if (hasShowTime >= 1000) {
          this.toastQueue.shift()
          if (this.toastQueue.length === 1) {
            this.toastShowTime = 5000
          } else {
            this.toastShowTime = 1000
          }
          this.toastStartTime = Date.now()['getTime']()
        } else {
          this.toastShowTime = 1000 - hasShowTime
        }
      }
      this.judgeWeakit = true
      this.weakTipDialog.setMessage(this.toastQueue[0]['message'], this.toastQueue[0]['translated'])
      clearTimeout(this.nextWeakTip)
      clearTimeout(this.getWeakit)
      this.getWeakit = setTimeout(() => {
        this.toastQueue.shift()
        this.judgeWeakit = false
        this.firefoxUp(document.querySelector('#component_weaktip_dialog'), 'component_weaktip_dialog', false)
        if (this.toastQueue.length !== 0) {
          this.nextToastHandler()
        }
      }, this.toastShowTime)
      _.delay(() => {
        this.firefoxUp(document.querySelector('#component_weaktip_dialog'), 'component_weaktip_dialog', true)
      }, 200)
    }
  }

  nextToastHandler () {
    if (this.toastQueue.length === 1) {
      this.toastShowTime = 5000
    } else {
      this.toastShowTime = 1000
    }
    this.toastStartTime = Date.now()['getTime']()
    this.judgeWeakit = true
    this.weakTipDialog.setMessage(this.toastQueue[0]['message'], this.toastQueue[0]['translated'])
    clearTimeout(this.nextWeakTip)
    this.nextWeakTip = setTimeout(() => {
      this.toastQueue.shift()
      this.judgeWeakit = false
      this.firefoxUp(document.querySelector('#component_weaktip_dialog'), 'component_weaktip_dialog', false)
      if (this.toastQueue.length !== 0) {
        this.nextToastHandler()
      }
    }, this.toastShowTime)
    _.delay(() => {
      this.firefoxUp(document.querySelector('#component_weaktip_dialog'), 'component_weaktip_dialog', true)
    }, 200)
  }

    /**
     * load the multi language
     */
  private initTranslate () {
      /* @HWP-17-DISABLE-FEATURES
        let language = session.get('language');
        if (!language) {
            let browerLanguage = window.navigator.language.substring(0, 2);
            language = _.contains(EPGConfig.SupportLanguageList, browerLanguage)
                ? browerLanguage : EPGConfig.SupportLanguageList[0];
            session.put('language', language, true);
            this.setFontFamily(language);
        }
        */
    let language = 'ru'
    session.put('language', language, true)
    this.setFontFamily(language)

    this.translate.use(language)

    window['t'] = (key: string, interpolateParams: Object) => {
      return this.translate.instant(key, interpolateParams)
    }
  }

  setFontFamily (language: string) {
    const obj = document.getElementsByTagName('body')[0]
    let path1: string

    http.request({
      url: `assets/font_${language}.json`,
      method: 'GET'
    }).then(({ body }) => {
      const { F01, F02 } = JSON.parse(body)
      const fontFamily = [F01.value, F02.value]
      obj.style.fontFamily = fontFamily.join(',')

      const allowed_lng: string[] = ['zh', 'en', 'ru']
      // path1 = `assets/css/mixin_${allowed_lng.indexOf(language) > -1 ? language : 'es'}.css`
      // this.setWhiteCssStyle(path1)

      http.request({
        url: `assets/i18n/date/date_${language}.json`,
        method: 'GET'
      }).then(dateStrJson => {
        session.put('DATESTRJSON', JSON.parse(dateStrJson.body), true)
      })
    })
  }

  setWhiteCssStyle (path1: string) {
    const head = document.getElementsByTagName('head')[0]
    const link1 = document.createElement('link')
    link1.href = path1
    link1.rel = 'stylesheet'
    link1.type = 'text/css'
    link1.id = 'path1'
    head.appendChild(link1)
  }

    /**
     * go home according to the hash
     */
  goHome () {
    if (location.pathname.indexOf('home') !== -1) {
      this.timerService.backTop()
    } else {
      this.router.navigate(['./home'])
    }
  }

    /**
     * if visitors enter the application from my-tv,jump to home page
     */
  isMytvIn () {
    if (location.pathname.indexOf('profile') !== -1 && !Cookies.getJSON('IS_PROFILE_LOGIN')) {
      this.router.navigate(['./home'])
    }
  }

  //   /**
  //    * close the update record dialog
  //    */
  // closeUpdateRecord () {
  //   this.showUpdateReocrd = false
  // }

    /**
     * close the change password dialog
     */
  closeChangePassword () {
    this.showChangePassword = false
  }

    /**
     * remove the blank space of the head and the tail.
     * @param {any} str [string to be processed].
     */
  trim (str) {
    if (typeof str === 'string') {
      str = str.replace(/(^\s)|(\s*$)/g, '').replace(/(^\s*)/g, '').replace(/(\s*$)/g, '')
      return str
    }
  }
}
