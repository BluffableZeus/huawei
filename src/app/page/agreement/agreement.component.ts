import { Component, OnInit } from '@angular/core'
import { Store, select } from '@ngrx/store'
import { Observable } from 'rxjs'

import {
  OFFER_DATA
} from '../../store/offers'

@Component({
  selector: 'app-agreement',
  templateUrl: './agreement.component.html',
  styleUrls: ['./agreement.component.scss']
})
export class AgreementComponent implements OnInit {

  offers$: Observable<any>

  constructor(
    private store: Store<any>
  ) { }

  ngOnInit() {
    this.offers$ = this.store.pipe(select('offers'))
    this.store.dispatch({ type: OFFER_DATA })
  }

}
