import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { AgreementComponent } from './agreement/agreement.component'

const routes: Routes = [
  { path: 'agreement', component: AgreementComponent },
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PageRoutingModule { }
