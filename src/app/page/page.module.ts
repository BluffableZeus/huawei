import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core'

import { PageRoutingModule } from './page-routing.module';
import { AgreementComponent } from './agreement/agreement.component';

@NgModule({
  imports: [
    CommonModule,
    PageRoutingModule,
    TranslateModule
  ],
  declarations: [AgreementComponent]
})
export class PageModule { }
