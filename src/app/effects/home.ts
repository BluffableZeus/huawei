import { Injectable } from '@angular/core'
import { Store } from '@ngrx/store'
import { HeartbeatService } from 'ng-epg-sdk/services'
import { logout } from 'ng-epg-sdk/vsp'
import { AuthService } from '../shared/services/auth.service'
import { session } from '../shared/services/session'
import { HomeService } from '../home/home.service'
import { CustomConfigService } from '../shared/services/custom-config.service'
import { Actions, Effect, ofType } from '@ngrx/effects'
import { Observable, of, from } from 'rxjs'
import { combineLatest, catchError, map, mergeMap, switchMap, withLatestFrom } from 'rxjs/operators'

import {
  HomeAction,
  HOME_LOADER_START,
  HOME_DATA,
  HOME_DATA_NOTHING,
  HOME_DATA_RELOAD,
  HOME_DATA_ERROR,
  HOME_DATA_SUCCESS,
} from '../store/home'

import {
  APP_NAVIGATION_START,
} from '../store/app'

import {
  AppAction,
  APP_GUEST_LOGIN,
  APP_GUEST_LOGIN_SUCCESS
} from '../store/app'

@Injectable()
export class HomeEffects {

  @Effect()
  loader$: Observable<HomeAction> = this.actions$.pipe(
    ofType(HOME_DATA, APP_NAVIGATION_START),
    switchMap(() => of({ type: HOME_LOADER_START }))
  )

  @Effect()
  homeData$: Observable<HomeAction> = this.actions$.pipe(
    ofType(HOME_DATA),
    withLatestFrom(this.store$),
    mergeMap(([action, state]) => {

      if (state.home.data) {
        return of({ type: HOME_DATA_NOTHING })
      }

      return from(this.getData()).pipe(
        map(data => {
          return { type: HOME_DATA_SUCCESS, payload: data }
        }),
        catchError(error => {
          if (error && error.result && error.result.retCode && error.result.retCode === '125023001') {
            return of({ type: APP_GUEST_LOGIN }, { type: HOME_DATA_RELOAD })
          }
          return of({ type: HOME_DATA_ERROR, payload: error })
        })
      )
    })
  )

  @Effect()
  homeDataReload$: Observable<HomeAction> = this.actions$.pipe(
    ofType(HOME_DATA_RELOAD),
    combineLatest(this.actions$.ofType(APP_GUEST_LOGIN_SUCCESS)),
    switchMap(() => of({ type: HOME_DATA }))
  )

  constructor(
    private customConfigService: CustomConfigService,
    private homeService: HomeService,
    private actions$: Actions,
    private store$: Store<any>
  ) {}

  private customizeConfig = {}

  async getData () {

    const resp = await this.customConfigService.getCustomizeConfig({ queryType: '0', key: 'banner_subject_id,special_cluster_id,vod_subject_id' })
    this.customizeConfig = resp['list']
    session.put('vodSubjectId', this.customizeConfig['vod_subject_id'])
    session.put('vodBannerID', this.customizeConfig['banner_subject_id'])
    let KaraokSubjectId = this.customizeConfig['karaok_subject_id'] || ''
    session.put('vodKaraokSubjectId', KaraokSubjectId)

    return this.homeService.queryHomeData({
      recmDataCount: '30',
      bannerSubjectID: this.customizeConfig['banner_subject_id'],
      bannerDataCount: '5',
      subjectID: this.customizeConfig['special_cluster_id'],
      subjectDataCount: '30',
      hotTVCount: '30'
    })

  }

}
