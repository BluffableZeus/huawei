import { Injectable } from '@angular/core'
import { Router, NavigationStart } from '@angular/router'
import { Store } from '@ngrx/store'
import { HeartbeatService } from 'ng-epg-sdk/services'
import { logout } from 'ng-epg-sdk/vsp'
import { AuthService } from '../shared/services/auth.service'
import { session } from '../shared/services/session'
import { Actions, Effect, ofType } from '@ngrx/effects'
import { Observable, of, from } from 'rxjs'
import { catchError, concatMap, exhaustMap, map, mergeMap, switchMap, withLatestFrom, filter } from 'rxjs/operators'

import {
  AppAction,
  APP_INIT_LOAD_START,
  APP_START_HEARTBEAT,
  APP_START_HEARTBEAT_FAILED,
  APP_GUEST_LOGIN,
  APP_GUEST_LOGIN_SUCCESS,
  APP_GUEST_LOGIN_FAILED,
  APP_NAVIGATION_START,
  APP_LOGOUT
} from '../store/app'

@Injectable()
export class AppEffects {

  @Effect()
  appInit$: Observable<AppAction> = this.actions$.pipe(
    ofType(APP_INIT_LOAD_START),
    exhaustMap(() => {
      return from(this.heartbeatService.startHeartbeat()).pipe(
        mergeMap(data => {
          const actions = []
          if (data && data.result && data.result.retCode && data.result.retCode === '125023001') {
            actions.push({ type: APP_GUEST_LOGIN })
          }
          return actions
        }),
        catchError(error => of({ type: APP_START_HEARTBEAT_FAILED, payload: error }))
      )
    })
  )

  @Effect()
  guestLogin$: Observable<AppAction> = this.actions$.pipe(
    ofType(APP_GUEST_LOGIN),
    exhaustMap(() => {
      return from(this.authService.guestLogin()).pipe(
        map(data => {
          return { type: APP_GUEST_LOGIN_SUCCESS, payload: data }
        }),
        catchError(error => of({ type: APP_GUEST_LOGIN_FAILED, payload: error }))
      )
    })
  )

  @Effect()
  appLogout$: Observable<AppAction> = this.actions$.pipe(
    ofType(APP_LOGOUT),
    switchMap(() => {

      Cookies.remove('IS_PROFILE_LOGIN')
      Cookies.remove('IS_GUEST_LOGIN')
      Cookies.remove('PROFILE_NAME')
      Cookies.remove('LOGIN_PROFILE_LOGOURL')
      Cookies.remove('USER_ID')
      Cookies.remove('CUSTOM_REC_PLAYBILLLENS')
      Cookies.remove('PROFILE_TYPE')

      session.remove('CUSTOM_CONFIG_DATAS')
      session.clear(false)

      return of({ type: APP_GUEST_LOGIN })
    })
  )

  constructor(
    private router: Router,
    private heartbeatService: HeartbeatService,
    private authService: AuthService,
    private actions$: Actions,
    private store$: Store<any>
  ) {

    this.router.events.pipe(
      filter(event => event instanceof NavigationStart)
    )
    .subscribe(event => {
      this.store$.dispatch({ type: APP_NAVIGATION_START })
    })
  }

}
