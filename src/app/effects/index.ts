import { AuthEffects } from './auth'
import { AppEffects } from './app'
import { HomeEffects } from './home'
import { OffersEffects } from './offers'

export const forRoot = [AuthEffects, AppEffects, HomeEffects, OffersEffects]

