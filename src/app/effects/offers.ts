import { Injectable } from '@angular/core'
import { Store } from '@ngrx/store'
import { HttpClient } from '@angular/common/http'
import { HeartbeatService } from 'ng-epg-sdk/services'
import { logout } from 'ng-epg-sdk/vsp'
import { AuthService } from '../shared/services/auth.service'
import { session } from '../shared/services/session'
import { HomeService } from '../home/home.service'
import { CustomConfigService } from '../shared/services/custom-config.service'
import { Actions, Effect, ofType } from '@ngrx/effects'
import { Observable, of, from } from 'rxjs'
import { retry, catchError, map, mergeMap, switchMap, withLatestFrom } from 'rxjs/operators'
import { environment } from '../../environments/environment'

const publicOffer = '/offers/actions/get-last-published-offer'

import {
  OffersAction,
  OFFER_LOADER_START,
  OFFER_DATA,
  OFFER_DATA_NOTHING,
  OFFER_DATA_ERROR,
  OFFER_DATA_SUCCESS,
} from '../store/offers'

import {
  APP_INIT_LOAD_START,
} from '../store/app'

@Injectable()
export class OffersEffects {

  @Effect()
  loader$: Observable<OffersAction> = this.actions$.pipe(
    ofType(OFFER_DATA, APP_INIT_LOAD_START),
    switchMap(() => of({ type: OFFER_LOADER_START }))
  )

  @Effect()
  offerData$: Observable<OffersAction> = this.actions$.pipe(
    ofType(OFFER_DATA),
    withLatestFrom(this.store$),
    mergeMap(([action, state]) => {

      if (state.offers.data) {
        return of({ type: OFFER_DATA_NOTHING })
      }

      return this.http.post(environment.TV_HOUSE_API_OFFERS_BASE + publicOffer, {})
        .pipe(
          retry(3),
          map(data => {
            return { type: OFFER_DATA_SUCCESS, payload: data }
          }),
          catchError(error => {
            return of({ type: OFFER_DATA_ERROR, payload: error })
          })
        )
    })
  )

  constructor(
    private http: HttpClient,
    private actions$: Actions,
    private store$: Store<any>
  ) {}

}
