import { Injectable, OnDestroy } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { Router } from '@angular/router'
import { Store } from '@ngrx/store'
import { Actions, Effect, ofType } from '@ngrx/effects'
import { Observable, of, timer } from 'rxjs'
import { catchError, map, takeUntil, mergeMap, switchMap, withLatestFrom } from 'rxjs/operators'

// LEGASY
import { modifyDeviceInfo } from 'ng-epg-sdk/vsp'
import { DialogRef, DialogParams } from 'ng-epg-ui/webtv-components/dialog'
import { isUndefined, find, uniq } from 'underscore'
import { AuthService } from '../shared/services/auth.service'
import { EventService } from 'ng-epg-sdk/services'
import { session } from 'src/app/shared/services/session'
import { CommonService } from '../core/common.service'
//const pageTracker = require('../../assets/lib/PageTracker')

import {
  UPDATE_PROFILES_DATA
} from '../store/profiles'

import {
  OFFER_DATA
} from '../store/offers'

import {
  AuthAction,
  MODAL_CLOSE,
  MODAL_OPEN,
  TYPE_SWITCH,
  FORM_LOADING,
  FOCUS_PASSWORD,
  DO_LOGOUT,
  DO_SIGNIN,
  DO_SIGNIN_SUBMIT,
  DO_SIGNIN_SUCCESS,
  DO_SIGNIN_FULL_INIT,
  DO_SIGNIN_FULL_ERROR,
  DO_SIGNIN_FULL_SUCCESS,
  DO_SIGNIN_ERROR,
  GET_PASSWORD_VIA_SMS,
  GET_PASSWORD_VIA_SMS_SUBMIT,
  GET_PASSWORD_VIA_SMS_TICK,
  GET_PASSWORD_VIA_SMS_TICK_FINISH,
  GET_PASSWORD_VIA_SMS_SUCCESS,
  GET_PASSWORD_VIA_SMS_ERROR
} from '../store/auth'

import {
  AppAction,
  APP_INIT_LOAD_START
} from '../store/app'

@Injectable()
export class AuthEffects implements OnDestroy {

  @Effect()
  modalOpen$: Observable<AuthAction> = this.actions$.pipe(
    ofType(MODAL_OPEN),
    switchMap(() => of({ type: OFFER_DATA }))
  )

  @Effect()
  authFinish$: Observable<AuthAction> = this.actions$.pipe(
    ofType(DO_SIGNIN_FULL_SUCCESS),
    switchMap(() => {

      if (session.get('prevAuthPage')) {
        this.router.navigate([session.get('prevAuthPage')])
        session.remove('prevAuthPage')
      }

      return of({ type: MODAL_CLOSE })
    })
  )

  @Effect()
  loadingStart$: Observable<AuthAction> = this.actions$.pipe(
    ofType(
      GET_PASSWORD_VIA_SMS,
      DO_SIGNIN
    ),
    switchMap(() => of({ type: FORM_LOADING, payload: true }))
  )

  @Effect()
  loadingEnd$: Observable<AuthAction> = this.actions$.pipe(
    ofType(
      GET_PASSWORD_VIA_SMS_SUCCESS,
      GET_PASSWORD_VIA_SMS_ERROR,
      DO_SIGNIN_SUCCESS,
      DO_SIGNIN_ERROR
    ),
    switchMap(() => of({ type: FORM_LOADING, payload: false }))
  )

  @Effect()
  getPasswordViaSmsSubmit$: Observable<AuthAction> = this.actions$.pipe(
    ofType(GET_PASSWORD_VIA_SMS_SUBMIT),
    withLatestFrom(this.store$),
    mergeMap(([action, state]) => {

      const { invalid } = state.auth

      if (invalid) {
        return of({ type: 'NOTHING' })
      }

      return of({ type: GET_PASSWORD_VIA_SMS })
    })
  )

  @Effect()
  getPasswordViaSms$: Observable<AuthAction> = this.actions$.pipe(
    ofType(GET_PASSWORD_VIA_SMS),
    withLatestFrom(this.store$),
    mergeMap(([action, state]) => {

      const { tvHouseLoginBase } = state.api
      const { login } = state.auth

      return this.http.post(`${ tvHouseLoginBase }/login/actions/send-code?msisdn=${ login }`, {}).pipe(
        map(data => {
          return { type: GET_PASSWORD_VIA_SMS_SUCCESS, payload: data }
        }),
        catchError(error => {
          const payload = this.handleHttpErrorMessage(error)
          return of({ type: GET_PASSWORD_VIA_SMS_ERROR, payload })
        })
      )

    })
  )

  @Effect()
  getPasswordViaSmsSuccess$: Observable<AuthAction> = this.actions$.pipe(
    ofType(GET_PASSWORD_VIA_SMS_SUCCESS),
    switchMap(action => {
      return of({ type: TYPE_SWITCH, payload: 'signin' })
    })
  )

  @Effect()
  getPasswordViaSmsSuccessFocus$: Observable<AuthAction> = this.actions$.pipe(
    ofType(GET_PASSWORD_VIA_SMS_SUCCESS),
    switchMap(action => {
      return of({ type: FOCUS_PASSWORD })
    })
  )

  @Effect()
  getPasswordTTL$: Observable<AuthAction> = this.actions$.pipe(
    ofType(GET_PASSWORD_VIA_SMS_SUCCESS, APP_INIT_LOAD_START),
    withLatestFrom(this.store$),
    switchMap(([action, state]) => {

      const { passwordSmsTTL } = state.auth

      return timer(0, 1000).pipe(
        takeUntil(this.actions$.ofType(GET_PASSWORD_VIA_SMS_TICK_FINISH)),
        switchMap(() => {

          const now = new Date().getTime()

          if (passwordSmsTTL > now) {

            const distance = (passwordSmsTTL - now) / 1000

            let m: string | number = Math.floor(distance / 60)
            let s: string | number = Math.floor(distance % 60)
            m = m < 10 ? "0" + m : m
            s = s < 10 ? "0" + s : s

            return of({ type: GET_PASSWORD_VIA_SMS_TICK, payload: m + ":" + s })
          } else {
            return of({ type: GET_PASSWORD_VIA_SMS_TICK_FINISH, payload: null })
          }

        })
      )

    })
  )

  @Effect()
  doSigninSubmit$: Observable<AuthAction> = this.actions$.pipe(
    ofType(DO_SIGNIN_SUBMIT),
    withLatestFrom(this.store$),
    mergeMap(([action, state]) => {

      const { invalid } = state.auth

      if (invalid) {
        return of({ type: 'NOTHING' })
      }

      return of({ type: DO_SIGNIN })
    })
  )

  @Effect()
  doSignin$: Observable<AuthAction> = this.actions$.pipe(
    ofType(DO_SIGNIN),
    withLatestFrom(this.store$),
    mergeMap(([action, state]) => {

      const { tvHouseLoginBase } = state.api
      const { login, password } = state.auth
      const { terminalType, deviceId, deviceName, deviceModel, deviceOS } = state.device

      const payload = {
        code: password.replace(/#/g, ''),
        login,
        terminalType,
        deviceId,
        deviceName,
        deviceModel,
        deviceOS
      }

      return this.http.post(`${ tvHouseLoginBase }/login?mobile=true`, payload).pipe(
        map(data => ({ type: DO_SIGNIN_SUCCESS, payload: data })),
        catchError(error => {

          // Dev Workaround for not worked tv-house
          // C50
          //return of({ type: DO_SIGNIN_SUCCESS, payload: { login: 'smartit_6', password: 'aA1111' }})
          // C40
          // return of({ type: DO_SIGNIN_SUCCESS, payload: { login: '323232', password: '111111' }})

          const payload = this.handleHttpErrorMessage(error)
          return of({ type: DO_SIGNIN_ERROR, payload })
        })
      )

    })
  )

  @Effect()
  doSigninFull$: Observable<AuthAction> = this.actions$.pipe(
    ofType(DO_SIGNIN_SUCCESS),
    withLatestFrom(this.store$),
    mergeMap(([action, state]) => {

      // Dev workaround for c40
      //state.auth.signinSuccess.login = '323232'
      //state.auth.signinSuccess.password = '111111'

      const { login, password } = state.auth.signinSuccess

      EventService.emit('EXTERNAL_AUTH', { login, password })
      return of({ type: DO_SIGNIN_FULL_INIT })
    })
  )

  constructor(
    private commonService: CommonService,
    private authService: AuthService,
    private router: Router,
    //private liveTvParams: DialogParams,
    private http: HttpClient,
    private actions$: Actions,
    private store$: Store<any>
  ) {

    EventService.on('AUTHENTICATE_LOGOUT', res => {
      this.store$.dispatch({ type: DO_LOGOUT })
      this.store$.dispatch({ type: UPDATE_PROFILES_DATA, payload: {} })
    })

    EventService.on('AUTHENTICATE_FULL_SUCCESS', res => {
      this.store$.dispatch({ type: DO_SIGNIN_FULL_SUCCESS })
    })

    EventService.on('AUTHENTICATE_RESPONSE_SUCCESS', res => {
      const payload = res.profiles
      this.store$.dispatch({ type: FORM_LOADING, payload: false })
      this.store$.dispatch({ type: UPDATE_PROFILES_DATA, payload })
    })

    EventService.on('AUTHENTICATE_RESPONSE_ERROR', res => {
      const payload = 'auth.login_or_password_incorrect';
      this.store$.dispatch({ type: FORM_LOADING, payload: false })
      this.store$.dispatch({ type: DO_SIGNIN_FULL_ERROR, payload })
    })

    EventService.on('LOGIN_OR_PASSWORD_INCORRECT', () => {
      const payload = 'auth.login_or_password_incorrect';
      this.store$.dispatch({ type: DO_SIGNIN_FULL_ERROR, payload })
    })

    EventService.on('AUTHENTICATE_OPEN_DIALOG', () => {
      this.store$.dispatch({ type: MODAL_OPEN })
    })

    EventService.on('EXTERNAL_AUTH_INIT', () => {
      this.store$.dispatch({ type: FORM_LOADING, payload: true })
      this.store$.dispatch({ type: 'EXTERNAL_AUTH_INIT' })
    })

    EventService.on('EXTERNAL_AUTH', auth => {
      EventService.emit('EXTERNAL_AUTH_INIT')
      this.authenticate(auth.login, auth.password)
    })

  }

  handleHttpErrorMessage(error) {
    if (!error || !error.status) {
      return 'auth.unknown_error'
    }
    return `auth.http_error_${ error.status }`
  }

  ngOnDestroy() {
    console.log('-ngOnDestroy----')
  }

  /***
   *
   * LEGASY AREA
   */

  /**
   * LEGASY login authentication
   */
  authenticate (userID?: string, password?: string): any {
    userID = userID.trim() || Cookies.getJSON('USER_ID')
    session.put('INIT_CHECK_PASSWORD', password)
    if (userID && password) {
      let req = {
        authenticateBasic: {
            userID: userID,
            userType: '1'
          },
        password: password
      }
      return this.authService.authenticate(req).then(resp => {
        EventService.emit('AUTHENTICATE_RESPONSE_SUCCESS', resp)
        if (resp && resp.result && resp.result.retCode === '157021017') {
            EventService.on('CONFIRM_CHECK_INIT_PWD', passwords => {
              this.accountIsLogin(resp, userID, passwords)
            })
          } else {
            this.accountIsLogin(resp, userID, password)
          }
      }, resp => {
        EventService.emit('AUTHENTICATE_RESPONSE_ERROR', resp)
        if ((resp && resp.result && resp.result.retCode === '157022007') ||
                 (resp && resp.result && resp.result.retCode === '157022012')) {
            if (isUndefined(resp.devices) || resp.devices.length === 0) {
              this.commonService.commonLog('Authenticate', 'devices')
            }
            //this.close()
            /*
            let data: any = {
              subscriberID: resp.subscriberID,
              userID: userID,
              password: password,
              devices: resp.devices,
              liveTvParams: this.liveTvParams
            }
            EventService.emit('DEVICELIST_OPEN', data)
            */
          }
        return Promise.reject(resp)
      })
    } else {
      EventService.emit('LOGIN_OR_PASSWORD_INCORRECT')
    }
  }

  /**
   * LEGASY
   */
  accountIsLogin (resp, userID, password) {
    EventService.emit('LOGININTRUE', false)
    let curProfile = find(resp.profiles, item => {
      return item['loginName'] === userID
    })
    this.commonService.resetSessionProfile(curProfile)
    Cookies.set('PROFILE_TYPE', curProfile['profileType'])
    //Cookies.set('PROFILE_NAME', curProfile.name)
    //EventService.emit('PROFILE_UPDATED')
    session.put('LOGIN_NAME', curProfile['loginName'], true)
    session.put('PROFILERATING_ID', curProfile['ratingID'])
    session.put('SUBSCRIBER_ID', resp.subscriberID)
    Cookies.set('USER_ID', userID)
    session.put('deviceID', resp.deviceID)
    session.put('modify_profileID', resp.profileID, true)
    //this.getReminderTime(curProfile)
    session.remove('INIT_CHECK_PASSWORD')
    this.putLoginNameList(userID)
    if (!resp.deviceName || resp.deviceName === '') {
      this.checkDeviceID(resp)
      let adminProfile = find(resp.profiles, item => {
        return Number(item['profileType']) === 0
      })
      let device: any = {
        ID: resp.deviceID,
        name: 'PC' + adminProfile['name'],
        isSupportPVR: '1',
        deviceType: '1'
      }
      session.put('IS_MANUAL_SET_DEVICE_NAME', false)
      modifyDeviceInfo({
        device: device
      }).then(() => {
          session.put('deviceName', device.name)
        })
    } else {
      session.put('deviceName', resp.deviceName)
    }
    if (resp.profileID) {
        // user behavior reported
      session.put('PROFILE_ID', resp.profileID)
      session.put('profileID', resp.subscriberID)
      session.put('tracker', resp.pageTrackers)
      session.put('profileSN', resp.profileSN)
      session.put('profileSn', resp.profileSn)
      const tracker = session.get('tracker') && session.get('tracker')[0]
      const profileID = session.get('profileSN') ? session.get('profileSN') : session.get('profileID')
      const deviceID = session.get('uuid_cookie')
      const trackParams = {
        actionUrl: tracker ? tracker.pageTrackerServerURL : '',
        subscriberID: profileID,
        deviceID: deviceID,
        appid: tracker ? tracker.appID : '',
        apppwd: tracker ? tracker.appPassword : '',
        needSend: tracker ? tracker.isSupportedUserLogCollect === '1' : false,
        deviceType: tracker ? tracker.type : '',
        sendCount: 5,
        sendTime: 10000
      }
      //pageTracker.setTrackerParams(trackParams)
      //pageTracker.setStatus(this.disagreePrivacy ? 0 : 1)
      //session.put('privacy_agree', this.disagreePrivacy)
    }
    EventService.emit('AUTHENTICATE_FULL_SUCCESS')
  }

  checkDeviceID (resp) {
    if (isUndefined(resp.deviceID) || resp.deviceID === '') {
      this.commonService.commonLog('Authenticate', 'deviceID')
    }
  }

  loginElect = true

  putLoginNameList (name) {
    let profileName = {}
    profileName['name'] = name
    profileName['loginElect'] = this.loginElect
    let list = session.get('LOGIN_NAME_LIST') ? session.get('LOGIN_NAME_LIST') : []
    if (this.loginElect) { // if you choose 'remember me' to login,this userID will been first.
      let arrlist = list.filter(profile => profile['name'] !== profileName['name'])
      arrlist.unshift(profileName)
      session.put('LOGIN_NAME_LIST', uniq(arrlist), true)
    } else {
        /*
            * if you don't choose 'remember me' to login，we should judge whether the userID has been remembered.
            * if remembered,remove 'remember me' when the user logins again,but the userName list unchanged.
            */
      for (let i = 0; i < list.length; i++) {
        if (list[i]['name'] === profileName['name']) {
            list.splice(i, 1)
          }
      }
      session.put('LOGIN_NAME_LIST', uniq(list), true)
    }
  }

}
