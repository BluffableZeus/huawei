import { Component, Input, OnInit, OnDestroy } from '@angular/core'
import { Router } from '@angular/router'
import { VODRouterService } from 'src/app/shared/services/vod-router.service'
import { PictureService } from 'ng-epg-ui/services/picture.service'
import { session } from 'src/app/shared/services/session'
import { VOD } from 'ng-epg-sdk'
const pageTracker = require('../../../../assets/lib/PageTracker')

@Component({
  selector: 'app-vod-banner',
  styleUrls: ['./vod-banner.component.scss'],
  templateUrl: './vod-banner.component.html'
})
export class VodBannerComponent implements OnInit, OnDestroy {
    /**
     * declare variables
     */
  @Input() public nodatatype = 'ondemand'
    // judge whether there is banner poster
  public hasVodBanner = true
    // array to save banner posters
  private vodBannerList: VOD[] = []
    // current banner poster that showing on the browser
  private curVodBanner: VOD = null
    // index of current banner poster that showing on the browser
  private curIndex = 0
    // interval timer
  private vodHomeTimer: any
  private bannerCover: string
    // control show 'play' or 'buy'
  private showPlay: boolean
    // variable to control ellipsis compatible to IE browser
  private introduceEllipsis = false
    // control show or hide the left and right icons
  private onlyOneVod = false

    /**
     * name for modules that would be used in this module
     */
  constructor (
        private router: Router,
        private vodRouter: VODRouterService,
        private pictureService: PictureService
    ) { }

    /**
     * when the data changed
     */
  @Input() set data (vodBannerList: VOD[]) {
    this.vodBannerList = vodBannerList
    if (this.vodBannerList && this.vodBannerList.length > 0) {
        // there is only one banner poster
      if (this.vodBannerList.length === 1) {
        this.onlyOneVod = true
      }

      /** DISABLE FOR C50 */
        // package data of the vodbanner
      this.vodBannerList.forEach((banner: VOD) => {
        if (banner.picture.titles[0]) {
            banner.picture.titles[0] = this.pictureService.convertToSizeUrl(banner.picture.titles, {})
          }
        if (!(banner.picture && banner.picture.drafts && banner.picture.drafts.length > 0)) {
            banner.picture = banner.picture || {
              drafts: ['assets/img/default/livetv_home_landscape_program.png']
            }
          } else {
            banner.picture.drafts = banner.picture.drafts.map(draft => (
              this.pictureService.convertToSizeUrl(
                draft,
                { minwidth: 1280, minheight: 720, maxwidth: 1280, maxheight: 720 }
              )
            ))
          }
          })
      /** DISABLE FOR C50 -- end */
      this.curVodBanner = this.vodBannerList[this.curIndex]
      this.rotateVodBanner()
        // judge the button shows 'buy' or 'play'
      this.showPlay = this.isSubscribed
      this.hasVodBanner = true
    } else {
      this.hasVodBanner = false
    }
  }

  get isSubscribed (): boolean {
    return !!(this.curVodBanner.isSubscribed === '1' && Cookies.getJSON('IS_PROFILE_LOGIN'))
  }

    /**
     * initialization
     */
  ngOnInit () {
    this.bannerCover = 'assets/img/vodbannercover.png'
  }
    /**
     * when exit the component, clear all the timer and events
     */
  ngOnDestroy () {
    if (this.vodHomeTimer) {
      clearInterval(this.vodHomeTimer)
    }
  }

    /**
     * play
     */
  plays () {
    this.play()
    if (this.curVodBanner['isSubscribed'] === '0' && !Cookies.getJSON('IS_GUEST_LOGIN')) {
      session.put('buy', true)
    }
  }

  play () {
    const trackParams = {
      businessType: 1,
      contentCode: this.curVodBanner.ID,
      source: 2
    }
    pageTracker.pushBrowseRecmBehavior(trackParams)
    this.vodRouter.navigate(this.curVodBanner.ID)
  }
    /**
     * living broadcast on VOD home page
     */
  rotateVodBanner () {
    clearInterval(this.vodHomeTimer)
    this.vodHomeTimer = setInterval(() => {
      if (this.curIndex < this.vodBannerList.length - 1) {
        this.onClickNext()
        this.showPlay = this.isSubscribed
      } else {
        this.curIndex = 0
        this.curVodBanner = this.vodBannerList[this.curIndex]
        this.showPlay = this.curVodBanner && this.isSubscribed
      }
    }, 6000)
  }
    /**
     * click the point to change the banner
     */
  changeBanner (banner, index) {
    clearInterval(this.vodHomeTimer)
    this.curVodBanner = banner
    this.curIndex = index
    this.rotateVodBanner()
  }
    /**
     * to the right page
     */
  onClickNext () {
    clearInterval(this.vodHomeTimer)
    this.curIndex++
    if (this.curIndex > this.vodBannerList.length - 1) {
      this.curIndex = 0
    }
    this.curVodBanner = this.vodBannerList[this.curIndex]
    this.rotateVodBanner()
    this.showPlay = this.isSubscribed
  }
    /**
     * to the left page
     */
  onClickPrevious () {
    clearInterval(this.vodHomeTimer)
    this.curIndex--
    if (this.curIndex < 0) {
      this.curIndex = this.vodBannerList.length - 1
    }
    this.curVodBanner = this.vodBannerList[this.curIndex]
    this.rotateVodBanner()
    this.showPlay = this.isSubscribed
  }
    /**
     * add the ellipsis when the content exceeding at IE browser
     */
  showWidth () {
      // judge the browser
    let ua = navigator.userAgent
    if (ua.indexOf('Chrome') !== -1 && ua.indexOf('Edge') === -1) {
      return
    }
    if (document.getElementById('contentVodBannerBg')) {
      let contentBg = document.getElementById('contentVodBannerBg')
      if (contentBg['offsetHeight'] > 60) {
        this.introduceEllipsis = true
      } else {
        this.introduceEllipsis = false
      }
    }
  }
    /**
     * add the ellipsis when the content exceeding at google chrome
     */
  showContent () {
      // judge the browser
    let ua = navigator.userAgent
    if (ua.indexOf('Chrome') !== -1 && ua.indexOf('Edge') === -1) {
      let styles = document.getElementById('vodbanner-introduce')['style']['-webkit-box-direction']
      let style = {
      }
      if (styles === 'reverse') {
        style['-webkit-box-direction'] = 'normal'
      } else {
        style['-webkit-box-direction'] = 'reverse'
      }
      return {
        'height': '88px',
        '-webkit-line-clamp': '4',
        'word-break': 'normal',
        '-webkit-box-direction': style['-webkit-box-direction']
      }
    }
  }
  showBlur (): any {
      // according to different browsers to add blur
    let ua = navigator.userAgent
    if (ua.indexOf('Chrome') !== -1 && ua.indexOf('Edge') === -1) {
      return {
        '-webkit-filter': 'blur(20px)'
      }
    } else if (ua.indexOf('Firefox') !== -1 && ua.indexOf('Edge') === -1) {
      return {
          'filter': 'blur(20px)'
        }
    }
  }
}
