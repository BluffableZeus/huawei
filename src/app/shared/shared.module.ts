import { CustomConfigService } from './services/custom-config.service'
import { CacheConfigService } from './services/cache-config.service'
import { ChannelCacheService } from './services/channel-cache.service'
import { AuthService } from './services/auth.service'
import { AuthGuard } from './services/auth.guard'
import { ErrorMessageService } from './services/error-message.service'
import { LiveTvService } from './services/livetv.service'
import { TimerService } from './services/timer.service'
import { TrackService } from './services/track.service'
import { MobileService } from './services/mobile.service'
import { PopupErrorsService } from './services/popup.errors'
import { CountdownService } from './services/countdown'
import { ValidationService } from './services/validation'
import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { TranslateModule, TranslateService } from '@ngx-translate/core'
import { ProfileService } from './services/profile.service'
import { VodBannerComponent } from './component/vod-banner/vod-banner.component'
import { NoDataModule } from 'ng-epg-ui/webtv-components/noData/no-data.module'
import { TitleService } from './services/title.service'
import { BannerCarouselModule } from 'src/modules/banner-carousel/banner-carousel.module'
import { ChildColumnShowModule } from 'ng-epg-ui/webtv-components/childcolumnshow';
import { ContinueWatchingComponent } from '../home/continue-watching/continue-watching.component';

@NgModule({
  imports: [
    CommonModule,
    NoDataModule,
    TranslateModule,
    ChildColumnShowModule
  ],
  exports: [
    CommonModule,
    TranslateModule,
    VodBannerComponent,
    ContinueWatchingComponent,
    BannerCarouselModule,
    ChildColumnShowModule
  ],
  declarations: [
    VodBannerComponent,
    ContinueWatchingComponent
  ],
  providers: [
    TranslateService,
    TrackService,
    TimerService,
    LiveTvService,
    ErrorMessageService,
    AuthGuard,
    AuthService,
    CustomConfigService,
    CacheConfigService,
    ChannelCacheService,
    ProfileService,
    PopupErrorsService,
    CountdownService,
    ValidationService,
    MobileService,
    TitleService
  ]
})
export class SharedModule { }
