import { NgModule } from '@angular/core'
import { cmsImage } from './backgroundImage.pipe'

@NgModule({
  imports: [
  ],
  exports: [
    cmsImage
  ],
  declarations: [
    cmsImage
  ],
  entryComponents: [

  ],
  providers: [
    cmsImage
  ]
})
export class PipesModule { }
