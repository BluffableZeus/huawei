import { Pipe, PipeTransform } from '@angular/core'
import { config as EPGConfig } from '../services/config'

@Pipe({ name: 'cmsImage' })
export class cmsImage implements PipeTransform {
  transform (value: string) {
    return `url(${EPGConfig.crmBaseUrl}/${value})`
  }
}
