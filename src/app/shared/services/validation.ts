import { Injectable } from '@angular/core'

@Injectable()
export class ValidationService {
  private email = /^(?=^.{6,32}$)(\w+([-.]\w+)*@\w+([-.]\w+)*\.\w{2,}([-.]\w+)*$)/
  private pwd = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,12}$/
  private ru = /[а-яА-ЯёЁ]/

  	constructor () { }

  	login (val) {
  		return this.email.test(val)
  	}

  	password (val) {
  		return this.pwd.test(val)
  	}

  	notEmpty (val) {
  		return val !== ''
  	}

  	hasRU (val) {
  		return this.ru.test(val)
  	}

  	hasNoRU (val) {
  		return !this.hasRU(val)
  	}

  	byRules (cfg) {
  		let fieldError = false

  		Object.keys(cfg.should).map(name => {
  			const ext = cfg.should[name].ext !== undefined ? cfg.should[name].ext : false,
  				msg = cfg.should[name].msg

  			if (!this[name](cfg.value) && !ext) {
  				fieldError = fieldError || true
  				cfg.errors.add(msg)
  			} else {
  				fieldError = fieldError || false
  				cfg.errors.remove(msg)
  			}

  			cfg.errors.setField(cfg.field, fieldError)
  		}, true)

    return this[cfg.result](cfg.value)
  	}
}
