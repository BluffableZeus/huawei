import { Injectable } from '@angular/core'
import { queryProfile } from 'ng-epg-sdk/vsp'
import { EventService } from 'ng-epg-sdk/services'

export interface ProfileData {
  id: string
  profileId: string
  isAdmin: boolean
  loginName: string
  name: string
  avatar: string
  level: string
  levelId: string
  quota: string
  profileType: string
}

@Injectable()
export class ProfileService {
  constructor () { }

  /**
     * if the profile version data has changed, update the profile information.
     */
  updateProfile () {
    if (Cookies.getJSON('IS_PROFILE_LOGIN')) {
      queryProfile({ type: '0', count: '1', offset: '0' }).then(resp => {
        if (resp && resp['profiles'] && resp['profiles'][0]) {
          let respProfile = resp['profiles'][0]
          let nowProfile: ProfileData = {
            id: 'profile',
            profileId: respProfile['ID'],
            isAdmin: respProfile['profileType'] === '0',
            loginName: respProfile['loginName'],
            name: respProfile['name'],
            avatar: respProfile['logoURL'] ? 'assets/img/avatars/' + respProfile['logoURL'] + '.png'
              : 'assets/img/default/ondemand_casts.png',
            level: respProfile['ratingName'] || '',
            levelId: respProfile['ratingID'],
            quota: this.getQuota(respProfile),
            profileType: respProfile['profileType']
          }
          localStorage.setItem('curProfile', JSON.stringify(nowProfile))
          Cookies.set('PROFILE_NAME', nowProfile.name)
          Cookies.set('LOGIN_PROFILE_LOGOURL', respProfile['logoURL'])
          EventService.emit('PROFILE_UPDATED')
          EventService.emit('CHANGE_PROFILE_AVATAR')
        }
      })
    }
  }

  /**
     * get the value of quota by profile data.
     */
  getQuota (profile) {
    let currencySymbol = JSON.parse(localStorage.getItem('CURRENCY_SYMBOL'))
    let currencyRate = JSON.parse(localStorage.getItem('CURRENCY_RATE'))
    let quotaValue
    if (profile['quota'] === '' || profile['quota'] === undefined || profile['quota'] === null) {
      quotaValue = ''
    } else {
      if (profile['quota'] === '-1') {
        quotaValue = 'unlimited'
      } else {
        quotaValue = currencySymbol + Number(profile['quota']) / Number(currencyRate)
      }
    }
    return quotaValue
  }
}
