export const SdkCacheConfig = {
  'Global': {
    'CLEANING_CONDITION': [
      '/VSP/V3/ModifyProfile',
      '/VSP/V3/SwitchProfile',
      '/VSP/V3/Authenticate'
    ],
    'MAX_PERSISTENCY_CACHE_SIZE': 20480,
    'MAX_MEMORY_CACHE_SIZE': 20480,
    'FILTER_PARAMETERS': [
      'scene',
      'SID',
      'DEVICE',
      'DID'
    ]
  },
  'Default': {
    'CACHE_NUMBER': 10,
    'MAX_AGE': 3600,
    'GUEST_MAX_AGE': 86400,
    'TOKEN_NUMBER': 10,
    'TOKEN_RATE': 3,
    'CACHE_PERSISTENCY': false,
    'SUCCESS_KEY': [
      '"retCode":"000000000"'
    ]
  },
  '/VSP/MediaService/jumpURL.jsp': {},
  '/VSP/V3/AddFavoCatalog': {},
  '/VSP/V3/AddPeriodicPVR': {},
  '/VSP/V3/AddPlaylistContent': {},
  '/VSP/V3/AddProfile': {},
  '/VSP/V3/AddPVR': {},
  '/VSP/V3/Authenticate': {
    'TOKEN_NUMBER': 5,
    'TOKEN_RATE': 30,
    'SUCCESS_KEY': [
      '"retCode":"000000000"',
      '"retCode":"157021017"'
    ]
  },
  '/VSP/V3/BatchGetResStrategyData': {
    'CACHE_SWITCH': true
  },
  '/VSP/V3/BindProfile': {},
  '/VSP/V3/CancelPVRByID': {},
  '/VSP/V3/CancelPVRByPlaybillID': {},
  '/VSP/V3/CancelSubscribe': {},
  '/VSP/V3/CheckPassword': {},
  '/VSP/V3/CreateBookmark': {},
  '/VSP/V3/CreateContentScore': {},
  '/VSP/V3/CreateFavorite': {},
  '/VSP/V3/CreateLock': {},
  '/VSP/V3/CreatePlaylist': {},
  '/VSP/V3/CreateReminder': {},
  '/VSP/V3/CreateSubscriber': {},
  '/VSP/V3/DeleteBookmark': {},
  '/VSP/V3/DeleteCPVRByDiskID': {},
  '/VSP/V3/DeleteFavoCatalog': {},
  '/VSP/V3/DeleteFavorite': {},
  '/VSP/V3/DeleteLock': {},
  '/VSP/V3/DeletePlaylist': {},
  '/VSP/V3/DeletePlaylistContent': {},
  '/VSP/V3/DeleteProfile': {},
  '/VSP/V3/DeletePVRByCondition': {},
  '/VSP/V3/DeletePVRByID': {},
  '/VSP/V3/DeleteReminder': {},
  '/VSP/V3/DownloadCUTV': {},
  '/VSP/V3/DownloadNPVR': {},
  '/VSP/V3/DownloadVOD': {},
  '/VSP/V3/GetBindCode': {},
  '/VSP/V3/GetContentConfig': {
    'CACHE_SWITCH': true,
    'MAX_AGE': 86400
  },
  '/VSP/V3/GetLatestResources': {
    'CACHE_SWITCH': true
  },
  '/VSP/V3/GetMasterSTBID': {},
  '/VSP/V3/GetPlaybillDetail': {
    'TOKEN_NUMBER': 20,
    'TOKEN_RATE': 1
  },
  '/VSP/V3/GetVODDetail': {
    'TOKEN_NUMBER': 20,
    'TOKEN_RATE': 1
  },
  '/VSP/V3/Login': {},
  '/VSP/V3/LoginRoute': {},
  '/VSP/V3/Logout': {
    'TOKEN_NUMBER': 5,
    'TOKEN_RATE': 30
  },
  '/VSP/V3/ModifyDeviceInfo': {},
  '/VSP/V3/ModifyPassword': {},
  '/VSP/V3/ModifyProfile': {},
  '/VSP/V3/OnLineHeartbeat': {},
  '/VSP/V3/PlayChannel': {
    'TOKEN_NUMBER': 20,
    'TOKEN_RATE': 1
  },
  '/VSP/V3/PlayChannelHeartbeat': {
    'TOKEN_NUMBER': 20,
    'TOKEN_RATE': 1
  },
  '/VSP/V3/PlayNPVRHeartbeat': {},
  '/VSP/V3/PlayPVR': {},
  '/VSP/V3/PlayVOD': {},
  '/VSP/V3/PlayVODHeartbeat': {},
  '/VSP/V3/PushMsgByTag': {},
  '/VSP/V3/QueryAllChannel': {
    'TOKEN_NUMBER': 20,
    'TOKEN_RATE': 1
  },
  '/VSP/V3/QueryAllChannelDynamicProperties': {
    'TOKEN_NUMBER': 20,
    'TOKEN_RATE': 1
  },
  '/VSP/V3/QueryAllHomeData': {
    'CACHE_SWITCH': true,
    'MAX_AGE': 1,
    'CACHE_PERSISTENCY': true
  },
  '/VSP/V3/QueryBookmark': {
    'TOKEN_NUMBER': 20,
    'TOKEN_RATE': 1
  },
  '/VSP/V3/QueryChannelListBySubject': {
    'TOKEN_NUMBER': 20,
    'TOKEN_RATE': 1
  },
  '/VSP/V3/QueryChannelSubjectList': {
    'TOKEN_NUMBER': 20,
    'TOKEN_RATE': 1
  },
  '/VSP/V3/QueryCustomizeConfig': {
    'TOKEN_NUMBER': 5,
    'TOKEN_RATE': 30
  },
  '/VSP/V3/QueryDeviceList': {
    'TOKEN_NUMBER': 20,
    'TOKEN_RATE': 1
  },
  '/VSP/V3/QueryEpgHomeRecmPlaybill': {},
  '/VSP/V3/QueryEpgHomeSubjects': {
    'CACHE_SWITCH': true
  },
  '/VSP/V3/QueryEpgHomeVod': {},
  '/VSP/V3/QueryFavoCatalog': {
    'TOKEN_NUMBER': 20,
    'TOKEN_RATE': 1
  },
  '/VSP/V3/QueryFavorite': {
    'TOKEN_NUMBER': 20,
    'TOKEN_RATE': 1
  },
  '/VSP/V3/QueryHomeData': {
    'CACHE_SWITCH': true,
    'MAX_AGE': 1,
    'CACHE_PERSISTENCY': true
  },
  '/VSP/V3/QueryHotPlaybill': {
    'TOKEN_NUMBER': 20,
    'TOKEN_RATE': 1
  },
  '/VSP/V3/QueryLauncher': {
    'CACHE_SWITCH': true,
    'MAX_AGE': 600
  },
  '/VSP/V3/QueryLocation': {},
  '/VSP/V3/QueryLock': {
    'TOKEN_NUMBER': 20,
    'TOKEN_RATE': 1
  },
  '/VSP/V3/QueryMoreRecommend': {
    'TOKEN_NUMBER': 20,
    'TOKEN_RATE': 1
  },
  '/VSP/V3/QueryMyContent': {
    'TOKEN_NUMBER': 20,
    'TOKEN_RATE': 1
  },
  '/VSP/V3/QueryMyTVHomeData': {},
  '/VSP/V3/QueryOTTLiveTVHomeData': {
    'CACHE_SWITCH': true,
    'MAX_AGE': 1,
    'CACHE_PERSISTENCY': true
  },
  '/VSP/V3/QueryPCVodHome': {
    'CACHE_SWITCH': true,
    'MAX_AGE': 1,
    'CACHE_PERSISTENCY': true
  },
  '/VSP/V3/QueryPlaybillContext': {
    'TOKEN_NUMBER': 20,
    'TOKEN_RATE': 1
  },
  '/VSP/V3/QueryPlaybillContextByChannelContext': {
    'TOKEN_NUMBER': 20,
    'TOKEN_RATE': 1
  },
  '/VSP/V3/QueryPlaybillList': {
    'TOKEN_NUMBER': 20,
    'TOKEN_RATE': 1
  },
  '/VSP/V3/QueryPlaybillListByChannelContext': {
    'TOKEN_NUMBER': 20,
    'TOKEN_RATE': 1
  },
  '/VSP/V3/QueryPlaybillVersion': {},
  '/VSP/V3/QueryPlaylist': {
    'TOKEN_NUMBER': 20,
    'TOKEN_RATE': 1
  },
  '/VSP/V3/QueryPlaylistContent': {
    'TOKEN_NUMBER': 20,
    'TOKEN_RATE': 1
  },
  '/VSP/V3/QueryProduct': {
    'TOKEN_NUMBER': 20,
    'TOKEN_RATE': 1
  },
  '/VSP/V3/QueryProfile': {},
  '/VSP/V3/QueryPVR': {
    'TOKEN_NUMBER': 20,
    'TOKEN_RATE': 1
  },
  '/VSP/V3/QueryPVRByID': {},
  '/VSP/V3/QueryPVRSpace': {},
  '/VSP/V3/QueryPVRSpaceByID': {},
  '/VSP/V3/QueryRecmContent': {
    'TOKEN_NUMBER': 20,
    'TOKEN_RATE': 1
  },
  '/VSP/V3/QueryRecmVODList': {
    'TOKEN_NUMBER': 20,
    'TOKEN_RATE': 1
  },
  '/VSP/V3/QueryReminder': {
    'TOKEN_NUMBER': 20,
    'TOKEN_RATE': 1
  },
  '/VSP/V3/QuerySubjectDetail': {
    'CACHE_SWITCH': true,
    'CACHE_NUMBER': 20
  },
  '/VSP/V3/QuerySubjectVODBySubjectID': {},
  '/VSP/V3/QuerySubscription': {
    'TOKEN_NUMBER': 20,
    'TOKEN_RATE': 1
  },
  '/VSP/V3/QueryTemplate': {
    'CACHE_SWITCH': true
  },
  '/VSP/V3/QueryVodHomeData': {
    'CACHE_SWITCH': true,
    'MAX_AGE': 1,
    'CACHE_PERSISTENCY': true
  },
  '/VSP/V3/QueryVODListBySubject': {
    'TOKEN_NUMBER': 20,
    'TOKEN_RATE': 1
  },
  '/VSP/V3/QueryVodSubjectAndVodList': {},
  '/VSP/V3/QueryVODSubjectList': {
    'CACHE_SWITCH': true,
    'CACHE_NUMBER': 20,
    'TOKEN_NUMBER': 20,
    'TOKEN_RATE': 1
  },
  '/VSP/V3/ReplaceDevice': {},
  '/VSP/V3/ReportChannel': {
    'TOKEN_NUMBER': 20,
    'TOKEN_RATE': 1
  },
  '/VSP/V3/ReportPVR': {},
  '/VSP/V3/ReportVOD': {},
  '/VSP/V3/ReSetPassword': {},
  '/VSP/V3/SearchContent': {
    'TOKEN_NUMBER': 20,
    'TOKEN_RATE': 1
  },
  '/VSP/V3/SearchHotKey': {
    'CACHE_SWITCH': true,
    'MAX_AGE': 86400
  },
  '/VSP/V3/SearchPVR': {
    'TOKEN_NUMBER': 20,
    'TOKEN_RATE': 1
  },
  '/VSP/V3/SendSMS': {},
  '/VSP/V3/SetContentLike': {},
  '/VSP/V3/SetGlobalFilterCond': {},
  '/VSP/V3/SetMasterSTBID': {},
  '/VSP/V3/SortFavorite': {},
  '/VSP/V3/SortPlaylistContent': {},
  '/VSP/V3/SubmitDeviceInfo': {},
  '/VSP/V3/SubscribeProduct': {},
  '/VSP/V3/SuggestKeyword': {},
  '/VSP/V3/SwitchProfile': {
    'TOKEN_NUMBER': 5,
    'TOKEN_RATE': 30
  },
  '/VSP/V3/UnBindProfile': {},
  '/VSP/V3/UpdateCPVRStatus': {},
  '/VSP/V3/UpdateFavoCatalog': {},
  '/VSP/V3/UpdatePeriodicPVR': {},
  '/VSP/V3/UpdatePlaylist': {},
  '/VSP/V3/UpdatePVR': {},
  '/VSP/V3/VodHome': {}
}
