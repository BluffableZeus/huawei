import { Injectable } from '@angular/core'
import { Title } from '@angular/platform-browser'

@Injectable()
export class TitleService {

  private suffix = 'МТС | ТВ'
  private title = null
  private route = null
  private routeTitleList = {
    '/home': 'Главная',
    '/video': 'Видео',
    '/video/category': 'Категории Видео',
    '/video/series': 'Сериалы',
    '/live-tv': 'Телеканалы',
    '/live-tv/guide': 'Телепрограмма',
    '/profile': 'Профиль',
    '/profile/historyVod': 'История просмотров',
    '/profile/favoriteVod': 'Избранное видео'
  }
  constructor (
    private _titleService: Title
  ) {}

  setTitle (options: {title?: string, route?: string } = null): void {
    if (options) {
      if (options.title) {
        localStorage.setItem('title', options.title)
      } else {
        localStorage.removeItem('title')
      }
      if (options.route) { localStorage.setItem('route', options.route) }
    }

    let title = localStorage.getItem('title')
    let route = localStorage.getItem('route')
    let customTitle = title || ''
    customTitle += route ? ' ' + (this.getRouteTitle(route) || '') : ''
    customTitle += ' ' + this.suffix
    this._titleService.setTitle(customTitle)
  }

  getRouteTitle (routeFind): string {
    let result = this.routeTitleList[routeFind]
    if (result) {
      return result
    } else {
      for (let route in this.routeTitleList) {
        if (routeFind.indexOf(route) + 1) {
          result = this.routeTitleList[route]
        }
      }
      return result
    }
  }

  setOnlyTitle (title: string): void {
    if (title) localStorage.setItem('title', title)
    this.setTitle()
  }
}
