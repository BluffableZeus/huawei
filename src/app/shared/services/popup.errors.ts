import { Injectable } from '@angular/core'

@Injectable()
export class PopupErrorsService {
  public messages = []
  fields = {}

  constructor () { }

  clearFields () {
    this.fields = {}
  }

  registerField (name) {
    this.fields[name] = false
  }

  on (field) {
    this.fields[field] = true
  }

  off (field) {
    this.fields[field] = false
  }

  setField (field, state) {
    this.fields[field] = state
  }

  getField (field) {
    return this.fields[field]
  }

  add (msg) {
    if (this.messages.indexOf(msg) < 0) {
      this.messages.push(msg)
    }
  }

  remove (msg) {
    const index = this.messages.indexOf(msg)

    if (index > -1) {
      this.messages.splice(index, 1)
    }
  }

  toggle (msg, state) {
    if (state) {
      this.add(msg)
    } else {
      this.remove(msg)
    }
  }

  clear () {
    this.messages = []
  }

  count () {
    return this.messages.length
  }

  anyError () {
    return Object.keys(this.fields).reduce((res, field) => res || this.fields[field], false)
  }
}
