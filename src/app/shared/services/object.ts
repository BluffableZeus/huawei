export function meaSureParam (object, params: any[], isArray?: any) {
  let objectPro = object || {}
  let objectParams = params || []
  let length = objectParams.length
  for (let i = 0; i < length; i++) {
    if (i !== (length - 1)) {
      objectPro = objectPro[params[i]] || {}
    } else {
      objectPro = objectPro[params[i]] ? objectPro[params[i]]
        : (isArray || {})
    }
  }
  return objectPro
}
