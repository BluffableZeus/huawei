import * as _ from 'underscore'
import { Injectable } from '@angular/core'
import { queryChannelListBySubject } from 'ng-epg-sdk/vsp'

/**
 * export class CacheConfigService.
 */
@Injectable()
export class CacheConfigService {
  /**
     * get all channel information when staring up.
     */
  getSubChannel () {
    return queryChannelListBySubject({
      subjectID: '-1',
      contentTypes: [],
      count: '1000',
      offset: '0',
      isReturnAllMedia: '1',
      filter: {
        subscriptionTypes: ['0', '1']
      }
    }).then(resp => {
      return resp
    })
  }

  /**
     * to judge whether the channel support NPVR or CPVR.
     * @param {Object} recordCRType [npvrRecordCR or cpvrRecordCR Object]
     * @param {Object} btvCR [btvCR Object]
     */
  judgeIsSupportCPVR (recordCRType, btvCR) {
    let isSupport = recordCRType && recordCRType['enable'] === '1' && recordCRType['isSubscribed'] === '1' &&
            recordCRType['isContentValid'] === '1' && recordCRType['isValid'] === '1' &&
            btvCR && btvCR['enable'] === '1' && btvCR['isSubscribed'] === '1' &&
            btvCR['isContentValid'] === '1' && btvCR['isValid'] === '1'
    return isSupport
  }

  /**
     * according to the params judge whether channel support NPVR or CPVR
     * @param {Object} recordCR [judge whether support NPVR when recordCR is 'npvrRecordCR',
     *                           judge whether support NPVR when recordCR is 'cpvrRecordCR'].
     * @param {Object} channel [the data of one chennel.]
     */
  judgeRecordManual (recordCR, channel): any {
    let self = this
    let channels = _.filter(channel['physicalChannelsDynamicProperties'], function (physicalChannels) {
      let recordCRType = physicalChannels[recordCR]
      let btvCR = physicalChannels['btvCR']
      let isSupport = self.judgeIsSupportCPVR(recordCRType, btvCR)
      if (isSupport) {
        return recordCRType
      }
    })
    if (channels.length > 0) {
      return true
    } else {
      return false
    }
  }

  /**
     * get the chennel list which support NPVR
     * @param {Array<any>} allChannelCache [the data of chennel list which support PVR]
     */
  getNPVRChannelManual (allChannelCache: Array<any>) {
    let self = this
    let nPVRChannelCache = []
    nPVRChannelCache = _.filter(allChannelCache, function (channel) {
      if (channel['physicalChannelsDynamicProperties']) {
        let supportNPVR = self.judgeRecordManual('npvrRecordCR', channel)
        if (supportNPVR) {
          return channel
        }
      }
    })
    return nPVRChannelCache
  }

  /**
     * get the chennel list which support CPVR
     * @param {Array<any>} allChannelCache [the data of chennel list which support PVR]
     */
  getCPVRChannelManual (allChannelCache: Array<any>) {
    let self = this
    let cPVRChannelCache = []
    cPVRChannelCache = _.filter(allChannelCache, function (channel) {
      if (channel['physicalChannelsDynamicProperties']) {
        let supportCPVR = self.judgeRecordManual('cpvrRecordCR', channel)
        if (supportCPVR) {
          return channel
        }
      }
    })
    return cPVRChannelCache
  }
}
