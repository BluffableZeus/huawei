import * as _ from 'underscore'
import * as moment from 'moment'
import { Injectable } from '@angular/core'
import { EventService } from 'ng-epg-sdk/services'
import { DateUtils } from 'ng-epg-sdk/utils'
import { session } from 'src/app/shared/services/session'
const pageTracker = require('../../../assets/lib/PageTracker')
// let timeStyle like DateUtils.format(Date.now(), 'YYYYMMDDHHmmss') change to moment().utc().format('YYYYMMDDHHmmss');
const timestamp = moment().utc().format('YYYYMMDDHHmmss')
const list = [{
  key: 'contentList',
  url: 'QueryVODListBySubject',
  getParam: function (request) {
    let year = ''
    let productZone = ''
    let genre = ''
    if (request.VODFilters && request.VODFilters.length > 0) {
      const filter = request.VODFilters[0]
      year = filter.produceDates ? filter.produceDates[0] : ''
      productZone = filter.producezones ? filter.producezones[0] : ''
      genre = filter.genreIDs ? filter.genreIDs[0] : ''
    }
    return {
      contentCode: request.subjectID,
      businessType: '6',
      year: year,
      productZone: productZone,
      genre: genre,
      timestamp: timestamp
    }
  }

}, {
  key: 'searchContent',
  url: 'SearchContent',
  getParam: function (request) {
    return {
      keyword: request.searchKey
    }
  }

}, {
  key: 'GetVODDetail',
  url: 'GetVODDetail',
  getParam: function (request) {
    return {
      contentCode: request.subjectID,
      businessType: '6',
      timestamp: timestamp
    }
  }

}, {
  key: 'contentList',
  url: 'QueryRecmVODList',
  getParam: function (request) {
    return {
      contentCode: request.subjectID,
      businessType: '6',
      timestamp: timestamp
    }
  }
}, {
  key: 'recmBehavior',
  url: 'CreateFavorite',
  getParam: function (request) {
    const contentCode = request.favorites ? request.favorites[0].contentID : ''
    const contentType = request.favorites ? request.favorites[0].contentType : ''
    let businessType
    if (contentType === 'VIDEO_VOD' || contentType === 'AUDIO_VOD') {
      businessType = '1'
    } else if (contentType === 'VAS') {
      businessType = '5'
    } else {
      businessType = '7'
    }
    return {
      contentCode: contentCode,
      businessType: businessType,
      recmActionType: '2',
      timestamp: timestamp
    }
  }
}, {
  key: 'recmBehavior',
  url: 'CreateReminder',
  getParam: function (request) {
    const contentCode = request.reminders ? request.reminders[0].contentID : ''
    const contentType = request.reminders ? request.reminders[0].contentType : ''
    let businessType
    if (contentType === 'VIDEO_VOD' || contentType === 'AUDIO_VOD') {
      businessType = '1'
    } else if (contentType === 'PROGRAM') {
      businessType = '7'
    }
    return {
      contentCode: contentCode,
      businessType: businessType,
      recmActionType: '3',
      timestamp: timestamp
    }
  }
}, {
  key: 'recmBehavior',
  url: 'SubscribeProduct',
  getParam: function (request) {
    const contentCode = request.subscribe ? request.subscribe.contentID : ''
    const contentType = request.subscribe ? request.subscribe.contentType : ''
    const productID = request.subscribe ? request.subscribe.productID : ''
    let businessType
    if (contentType === 'VIDEO_VOD' || contentType === 'AUDIO_VOD' || contentType === 'VOD') {
      businessType = '1'
    } else if (contentType === 'PROGRAM') {
      businessType = '7'
    } else if (contentType === 'VAS') {
      businessType = '5'
    } else {
      businessType = '2'
    }
    return {
      contentCode: contentCode,
      businessType: businessType,
      recmActionType: '1',
      productID: productID,
      timestamp: timestamp
    }
  }
}, {
  key: 'recmBehavior',
  url: 'ReportChannel',
  getParam: function (request) {
    const contentCode = request.channelID
    const contentType = request.businessType
    let businessType
    if (contentType === 'PLTV' || contentType === 'CPLTV' || contentType === 'VOD') {
      businessType = '3'
    } else {
      businessType = '2'
    }
    return {
      contentCode: contentCode,
      businessType: businessType,
      recmActionType: '0',
      timestamp: timestamp
    }
  }
}, {
  key: 'recmBehavior',
  url: 'PlayVOD',
  getParam: function (request) {
    const contentCode = request.VODID
    return {
      contentCode: contentCode,
      businessType: '1',
      recmActionType: '0',
      timestamp: timestamp
    }
  }
}]

@Injectable()
export class TrackService {
  constructor () {
    this.initBind()
  }

  initBind () {
    EventService.on('AJAX_SUCCESS', (data) => {
      // report to track user behavior according to related to the interface
      this.onAjaxSuccess(data)
    })
  }

  onAjaxSuccess (data) {
    const inf = data.request.inf
    const req = data.request.data

    _.find(list, (item) => {
      if (item.url === inf) {
        if (!session.get('clipFlag')) {
          this.sendTrackMessage(item.key, item.getParam(req))
        }
        return true
      }
    })
  }
  /**
     * send track message
     */
  sendTrackMessage (key, param) {
    if (key === 'contentList') {
      // (vod subject) and (top 30) behavior reported
      pageTracker.pushBrowseContentList(param)
    }
    if (key === 'searchContent') {
      // behavior reported of search page
      pageTracker.pushBrowseSearchContent(param)
    }
    if (key === 'recmBehavior') {
      // the content of the recommended behavior reported
      this.browseRecmBehavior(param)
    }
  }
  /**
     * on page change
     */
  onPageChange (currentUrl, nextUrl) {
    pageTracker.pushBrowsePage({ pageFrom: currentUrl, pageTo: nextUrl, timestamp: timestamp })
  }
  /**
     * push browse content detail
     */
  browseContentDetail (enterPage) {
    const searchResultsIndex = enterPage.data.searchIndex
    const recmActionID = enterPage.data.recmActionID
    const endTime = enterPage.data.endTime
    const entrance = enterPage.data.entrance
    const isTVOD = endTime < Date.now()
    const businessType = enterPage.name === 'Playbilldetail' ? (isTVOD ? '4' : '7') : '1'
    const contentCode = enterPage.name === 'Playbilldetail' ? enterPage.data.playbillID : enterPage.data.id
    const trackParams = {
      businessType: businessType,
      contentCode: contentCode,
      source: !_.isNaN(+searchResultsIndex) ? 1 : ((recmActionID || entrance) ? 3 : 2),
      entrance: entrance,
      recmActionID: recmActionID || '',
      number: !_.isNaN(+searchResultsIndex) ? +searchResultsIndex + 1 : '',
      timestamp: enterPage.data.timestamp ? enterPage.data.timestamp : DateUtils.format(Date.now(), 'YYYYMMDDHHmmss')
    }
    pageTracker.pushBrowseContentDetail(trackParams)
  }
  /**
     * browse recm behavior
     */
  browseRecmBehavior (param) {
    pageTracker.pushBrowseRecmBehavior(param)
  }
}
