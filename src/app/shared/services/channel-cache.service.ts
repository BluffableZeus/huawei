import * as _ from 'underscore'
import { Injectable } from '@angular/core'
import { queryAllChannel, queryAllChannelDynamicProperties, queryFavorite, queryLock } from 'ng-epg-sdk/vsp'
import { session } from 'src/app/shared/services/session'
import { EventService, HeartbeatService } from 'ng-epg-sdk/services'

/**
 * export class ChannelCacheService.
 */
@Injectable()
export class ChannelCacheService {
    // quote of queryAllChannel interface.
  private _queryAllChannel = queryAllChannel
    // quote of queryAllChannelDynamicProperties interface.
  private _queryAllChannelDynamicProperties = queryAllChannelDynamicProperties
    // queryChannel of favorite
  private _queryFavorite = queryFavorite
    // queryChannel of lock
  private _queryLock = queryLock
    // to judge whether has gotten the version message.
  private isNowGetVersionMessage = false
    // all favFynamic datas
  private favResp
    // all lockDynamic datas
  private lockResp

    /**
     * constructor of class ChannelCacheService.
     */
  constructor (
        private heartbeatService: HeartbeatService
    ) {
      /**
         * update static version data if the version has changed.
         */
    EventService.removeAllListeners(['CHANNEL_VERSION_CHANGED'])
    EventService.on('CHANNEL_VERSION_CHANGED', (resp) => {
      this.staticVersionRefresh(resp)
    })
      /**
         * update static version data if the version has changed.
         */
    EventService.removeAllListeners(['USERFILTER_VERSION_CHANGED'])
    EventService.on('USERFILTER_VERSION_CHANGED', (resp) => {
      if (!this.isNowGetVersionMessage) {
        this.userFilterRefresh(resp)
      }
    })
      /**
         * dynamic version of channel has changed.
         */
    EventService.removeAllListeners(['CHANNEL_DYNAMIC_VERSION_CHANGED'])
    EventService.on('CHANNEL_DYNAMIC_VERSION_CHANGED', (resp) => {
      this.dynamicVersionRefresh(resp)
    })
      /**
         * version of favorite has changed.
         */
    EventService.removeAllListeners(['FAVORITE_VERSION_CHANGED'])
    EventService.on('FAVORITE_VERSION_CHANGED', (resp) => {
      _.delay(() => {
        this.favoriteVersionRefresh(resp)
      }, 200)
    })

      /**
         * lock_version: version of lock channel.
         * resp: value of version.
         * lock_heart_version: version of last responce of heartbeat.
         * resp_pre_version: last version of current heartbeat.
         */
    EventService.removeAllListeners(['LOCK_VERSION_CHANGED'])
    EventService.on('LOCK_VERSION_CHANGED', (resp) => {
      _.delay(() => {
        this.lockVersionRefresh(resp)
      }, 200)
    })
  }

  userFilterRefresh (resp) {
    let versionType = Cookies.getJSON('USER_ID') || 'Guest'
    if (!!resp && session.get('USER_FILTER') !== resp) {
      session.put('USER_FILTER', resp, true)
      if (versionType === 'Guest') {
        this.staticSdkClearCache('Guest', versionType)
      } else if (versionType !== 'Guest') {
          this.staticSdkClearCache(session.get('profileID'), versionType)
        }
    }
  }

    /**
     * refresh the static version data.
     * @param {Object} resp [data from event 'CHANNEL_VERSION_CHANGED']
     */
  staticVersionRefresh (resp) {
    let channel_version = resp.split('|')[0], versionType = Cookies.getJSON('USER_ID') || 'Guest',
      static_version = session.get(versionType + 'static_channel_version')
    if (static_version && channel_version) {
      if (channel_version > static_version) {
        this.staticSdkClearCache(session.get('profileID'), versionType)
      }
    } else if (!static_version && !channel_version && versionType === 'Guest') {
      this.staticSdkClearCache('Guest', versionType)
    } else if (!static_version && channel_version && versionType && versionType !== 'Guest') {
        this.staticSdkClearCache(session.get('profileID'), versionType)
      }
  }

  staticSdkClearCache (profileID, versionType) {
    this.refreshStatic(versionType)
    this.refreshDynamic(versionType)
  }

    /**
     * refresh the dynamic version data.
     * @param {Object} resp [data from event 'CHANNEL_DYNAMIC_VERSION_CHANGED']
     */
  dynamicVersionRefresh (resp) {
    let channel_version = resp && resp.split('|')[1], versionType = Cookies.getJSON('USER_ID') || 'Guest',
      dynamic_version = session.get(versionType + 'dynamic_channel_version')
    if (versionType === 'Guest') {
      return
    }
    if (!dynamic_version && channel_version) {
      session.put(versionType + 'dynamic_channel_version', channel_version, true)
      this.refreshDynamic(versionType)
    } else if (dynamic_version && channel_version) {
      if (channel_version > dynamic_version) {
          session.put(versionType + 'dynamic_channel_version', channel_version, true)
          this.refreshDynamic(versionType)
        }
    }
  }

    /**
     * refresh the favorite version data.
     * @param {Object} resp [data from event 'FAVORITE_VERSION_CHANGED']
     */
  favoriteVersionRefresh (resp) {
    let self = this
    let versionType = Cookies.getJSON('USER_ID') || 'Guest',
      favorite_version = session.get(versionType + 'favorite_channel_version') || '-1',
      favorite_heart_before = session.get(versionType + 'favorite_channel_version_before') || '-1',
      pre_favorite_version = this.heartbeatService.getVersion('preFavorite')
    if (favorite_version < resp || favorite_heart_before < pre_favorite_version) {
      session.put(versionType + 'favorite_channel_version', resp, true)
      session.put(versionType + 'favorite_channel_version_before', pre_favorite_version, true)
      EventService.removeAllListeners(['FAVORITERECIEVED'])
      if (session.put('firstLogin', true)) {
        EventService.on('FAVORITERECIEVED', () => {
            self.getFavChannel()
          })
      } else {
        self.getFavChannel()
      }
    }
  }

    /**
     * refresh the lock channel version data.
     * @param {Object} resp [data from event 'LOCK_VERSION_CHANGED']
     */
  lockVersionRefresh (resp) {
    let self = this
    let versionType = Cookies.getJSON('USER_ID') || 'Guest',
      lock_version = session.get(versionType + 'lock_channel_version') || '-1',
      lock_heart_before = session.get(versionType + 'lock_channel_version_before') || '-1',
      pre_lock_version = this.heartbeatService.getVersion('preLock')
    if (lock_version < resp || lock_heart_before < pre_lock_version) {
      session.put(versionType + 'lock_channel_version', resp, true)
      session.put(versionType + 'lock_channel_version_before', pre_lock_version, true)
      EventService.removeAllListeners(['LOCKRECIEVED'])
      if (session.put('firstLogin', true)) {
        EventService.on('LOCKRECIEVED', () => {
            self.getLockChannel()
          })
      } else {
        self.getLockChannel()
      }
    }
  }

    /**
     * refresh the static and dynamic data.
     */
  refresh () {
    const staticVersion = Cookies.getJSON('USER_ID') || 'Guest'
    const dynamicVersion = Cookies.getJSON('USER_ID') || 'Guest'
    return Promise.all([this.refreshStatic(staticVersion, true), this.refreshDynamic(dynamicVersion, true)])
  }

    /**
     * set lock channel version data at localStorage.
     * @param {string} version [value of version]
     */
  lockedRefresh (version, preVersion) {
    let versionType = Cookies.getJSON('USER_ID') || 'Guest',
      beforeVersion = session.get(versionType + 'lock_channel_version')
    if (preVersion !== beforeVersion) {
      session.put(versionType + 'lock_channel_version', version, true)
    } else if (preVersion === beforeVersion) {
      session.put(versionType + 'lock_channel_version', version, true)
      session.put(versionType + 'lock_channel_version_before', preVersion, true)
    }
  }

    /**
     * set favorite channel version data at localStorage.
     * @param {string} version [value of version]
     */
  favRefresh (version, preVersion) {
    let versionType = Cookies.getJSON('USER_ID') || 'Guest',
      beforeVersion = session.get(versionType + 'favorite_channel_version')
    if (preVersion !== beforeVersion) {
      session.put(versionType + 'favorite_channel_version', version, true)
    } else if (preVersion === beforeVersion) {
      session.put(versionType + 'favorite_channel_version', version, true)
      session.put(versionType + 'favorite_channel_version_before', preVersion, true)
    }
  }

    /**
     * refresh static data.
     *
     * @param {string} staticVersion
     * @returns
     *
     * @memberOf ChannelCacheService
     *
     * @param cache is true; check the channel Cache; if cache exit no get _queryAllChannel or get _queryAllChannel
     * @param cache is false; get _queryAllChannel
     */
  refreshStatic (staticVersion: string, cache?: boolean) {
    let self = this
      // refresh static data.
    const cachedResp = session.get(staticVersion + '_Static_Version_Data')
    if (cachedResp && cache) {
    } else {
      return this._queryAllChannel({
        isReturnAllMedia: '1',
        userFilter: this.getUserFilter()
      }).then((staticResp) => {
          session.put(staticVersion + 'static_channel_version', staticResp['channelVersion'], true)
          session.put(staticVersion + '_Static_Version_Data', staticResp, true)
        }, resp => {
          self.heartbeatService.setVersion('channel', '0|0')
        })
    }
  }

    /**
     * refresh dynamic data.
     *
     * @param {string} dynamicVersion
     * @returns
     *
     * @memberOf ChannelCacheService
     */
  refreshDynamic (dynamicVersion: string, cache?: boolean) {
      // refresh dynamic data.
    if (this.isNowGetVersionMessage) {
    } else {
      const cachedResp = session.get(dynamicVersion + '_Dynamic_Version_Data')
      if (cachedResp && cache) {
        this.isNowGetVersionMessage = false
      } else {
        this.isNowGetVersionMessage = true
        return this._queryAllChannelDynamicProperties({
            isReturnAllMedia: '1'
          }).then((dynamicResp) => {
            let favOrLock = session.get(dynamicVersion + '_Dynamic_Version_Data') || {}
            _.each(dynamicResp['channelDynamaicProp'], (item) => {
              _.each(favOrLock['channelDynamaicProp'], favlockitem => {
                if (item['ID'] === (favlockitem && favlockitem['favorite'] &&
                                favlockitem['favorite']['contentID'])) {
                  // add data to dynamic
                  item['favorite'] = favlockitem['favorite']
                  item['isLocked'] = favlockitem['isLocked']
                }
                if (item['ID'] === (favlockitem && favlockitem['LockedID'])) {
                  item['isLocked'] = favlockitem['isLocked']
                }
              })
            })
            session.put(dynamicVersion + '_Dynamic_Version_Data', dynamicResp, true)
            EventService.emit('LOCKRECIEVED')
            EventService.emit('FAVORITERECIEVED')
            this.isNowGetVersionMessage = false
          }, resp => {
            if (!session.get(dynamicVersion + 'dynamic_channel_version')) {
              session.put(dynamicVersion + 'dynamic_channel_version', '', true)
            }
            this.isNowGetVersionMessage = false
            this.heartbeatService.setVersion('channel_dynamic', '0')
            EventService.emit('LOCKRECIEVED')
            EventService.emit('FAVORITERECIEVED')
          })
      }
    }
  }
    /**
     * get favoriteChannel from interface of queryFavorite
     */
  getFavChannel () {
    let self = this
      // get login type
    let userType = Cookies.getJSON('USER_ID') || 'Guest'
    if (userType === 'Guest') {
      return
    }
      // call interface of queryFavorite
    return this._queryFavorite({
      'count': '50',
      'offset': '0',
      'contentTypes': ['CHANNEL'],
      'sortType': 'FAVO_TIME:DESC'
    }).then(favresp1 => {
        // get session data
      self.favResp = session.get(userType + '_Dynamic_Version_Data') || { 'channelDynamaicProp': [{}] }
      let favResp = self.favResp && self.favResp['channelDynamaicProp']
      let favorites = favresp1['favorites']

      if (Number(favresp1.total) <= 50) {
          _.each(favResp, (item) => {
            item['favorite'] = undefined
            _.each(favorites, favitem => {
              if (favitem['channel']['ID'] === item['ID']) {
                // add data to dynamic
                item['favorite'] = {}
                item['favorite']['collectTime'] = undefined
                item['favorite']['contentID'] = favitem['channel']['ID']
                item['favorite']['contentType'] = favitem['contentType']
                item['favorite']['profileID'] = session.get('PROFILE_ID')
              }
            })
          })

          // program not exist, direct add favorite
          if (favResp.length === 1 && favorites.length > 0) {
            for (let i = 0; i < favorites.length; i++) {
              self.favResp['channelDynamaicProp'].push({
                favorite: {
                  'collectTime': undefined,
                  'contentID': favorites[i]['channel']['ID'],
                  'contentType': favorites[i]['contentType'],
                  'profileID': session.get('PROFILE_ID')
                }
              })
            }
          }
          if (self.favResp) {
            session.put(userType + '_Dynamic_Version_Data', self.favResp, true)
          }
        } else {
          this.getFavChannel()
        }
    })
  }
    /**
     * get lockChannel form interface of querylocked
     */
  getLockChannel () {
    let self = this
    let userType = Cookies.getJSON('USER_ID') || 'Guest'
    if (userType === 'Guest') {
      return
    }
      // call interface of queryLock
    return this._queryLock({
      'profileID': session.get('PROFILE_ID'),
      'count': '50',
      'offset': '0',
      'lockTypes': ['CHANNEL'],
      'sortType': 'CHAN_NO:ASC'
    }).then(lockResp1 => {
        // get session data
      self.lockResp = session.get(userType + '_Dynamic_Version_Data') || { 'channelDynamaicProp': [{}] }
      let lockResp = self.lockResp && self.lockResp['channelDynamaicProp']
      let locks = lockResp1['locks']

      if (Number(lockResp1.total) <= 50) {
          _.each(lockResp, (item) => {
            item['isLocked'] = undefined
            _.each(locks, lockitem => {
              if (lockitem['channel']['ID'] === item['ID']) {
                item['isLocked'] = {}
                item['isLocked'] = '1'
              }
            })
          })
          if (lockResp.length === 1 && locks.length > 0) {
            for (let i = 0; i < locks.length; i++) {
              self.lockResp['channelDynamaicProp'].push({
                isLocked: '1',
                ID: locks[i]['channel']['ID']
              })
            }
          }
          if (self.lockResp) {
            session.put(userType + '_Dynamic_Version_Data', self.lockResp, true)
          }
        } else {
          this.getLockChannel()
        }
    })
  }
    /**
     * Get the static version data according to the user-type.
     * If the data is undefined, it will return reject to notify its' caller.
     * Ohterwise, it will return the data gotten.
     */
  getStaticData () {
    return new Promise((resolve, reject) => {
      let userType = ''
      if (Cookies.getJSON('IS_PROFILE_LOGIN')) {
        userType = Cookies.getJSON('USER_ID')
      } else if (Cookies.getJSON('IS_GUEST_LOGIN')) {
          userType = 'Guest'
        }
      let userStatic = session.get(userType + '_Static_Version_Data')
      if (userStatic) {
        return resolve(userStatic)
      } else {
        return reject()
      }
    })
  }

    /**
     * Get the static version data asynchronously.
     */
  asynGetStaticData () {
    return new Promise((resolve, reject) => {
      this.getStaticData().then(resp => {
        return resolve(resp)
      }, respFail => {
        _.delay(() => {
            this.asynGetStaticData()
          }, 50)
      })
    })
  }

    /**
     * Get the dynamic version data according to the user-type.
     * If the data is undefined, it will return reject to notify its' caller.
     * Ohterwise, it will return the data gotten.
     */
  getDynamicData () {
    return new Promise((resolve, reject) => {
      let userType = ''
      if (Cookies.getJSON('IS_PROFILE_LOGIN')) {
        userType = Cookies.getJSON('USER_ID')
      } else if (Cookies.getJSON('IS_GUEST_LOGIN')) {
          userType = 'Guest'
        }
      let userDynamic = session.get(userType + '_Dynamic_Version_Data')
      if (userDynamic) {
        return resolve(userDynamic)
      } else {
        return reject()
      }
    })
  }

    /**
     * Get the dynamic version data asynchronously.
     */
  asynGetDynamicData () {
    return new Promise((resolve, reject) => {
      this.getDynamicData().then(resp => {
        return resolve(resp)
      }, respFail => {
        return reject({})
      })
    })
  }

    /**
     *
     *
     * @private
     * @returns {string}
     *
     * @memberOf ChannelService
     */
  getUserFilter (): string {
    return session.get('USER_FILTER') ? session.get('USER_FILTER') : null
  }

    /**
     * get static channel data.
     */
  getStaticChannelData () {
    if (Cookies.getJSON('IS_PROFILE_LOGIN')) {
      return session.get(Cookies.getJSON('USER_ID') + '_Static_Version_Data')
    } else if (Cookies.getJSON('IS_GUEST_LOGIN')) {
      return session.get('Guest' + '_Static_Version_Data')
    }
  }

    /**
     * get dynamic channel data.
     */
  getDynamicChannelData () {
    if (Cookies.getJSON('IS_PROFILE_LOGIN')) {
      return session.get(Cookies.getJSON('USER_ID') + '_Dynamic_Version_Data')
    } else if (Cookies.getJSON('IS_GUEST_LOGIN')) {
      return session.get('Guest' + '_Dynamic_Version_Data')
    }
  }

    /**
     * get all channel ID of cache.
     */
  getStorageChannelIDList () {
    let staticChannelData = this.getStaticChannelData().channelDetails
    let IDList = _.pluck(staticChannelData, 'ID')
    return IDList
  }

    /**
     * get channel ID list by ChannelDetail.subjectIds.
     */
  getStroageChannelIDListBySubjectID (subjectid, isFav?) {
    let channelData
    let subjectIDList
    if (isFav) {
      channelData = this.getDynamicChannelData().channelDynamaicProp || []
      subjectIDList = _.filter(channelData, function (channelDetail) {
        return channelDetail['favorite']
      })
    } else {
      channelData = this.getStaticChannelData().channelDetails
      subjectIDList = _.filter(channelData, function (channelDetail) {
        return channelDetail['subjectIDs'][0] === subjectid
      })
    }

    let IDList = _.pluck(subjectIDList, 'ID')
    return IDList
  }

    /**
     * get channel ID context according to curchannel.
     * @param {String} channelID [channel ID]
     * @param {Object} param [parameters]
     * @param {Boolean} fav [whether is favorite]
     */
  getChannelIDBycurChannel (channelID, param, fav?) {
    let start = 0
    let end = 0
      // get the static channel data from cache
    let channelData = this.getDynamicChannelData().channelDynamaicProp || []
      // find the index of target channel in all channel list
    if (fav) {
      channelData = _.filter(channelData, function (channelDetail) {
        return channelDetail['favorite']
      })
    }
    let index = _.findIndex(channelData, (item) => {
      return item['ID'] === channelID
    })
      // if the index less than 15,the start with 0
    if (index - 15 <= 0) {
      start = 0
      end = index + 16
    } else {
      start = index - 15
      end = index + 15
    }
      // if roll the scroll up, the end equal to the index.
    if (param && param['up']) {
        // if it is the first data, return empty.
      if (channelData[0] && channelData[0].ID === channelID) {
        return []
      }
      end = index
        // if roll the scroll down, the start equal to the index.
    } else if (param && !param['up']) {
      if (channelData[channelData.length - 1] && channelData[channelData.length - 1].ID === channelID) {
          return []
        }
      start = index
    }
    let channelList = channelData.slice(start, end)
    let IDList = _.pluck(channelList, 'ID')
    return IDList
  }
}
