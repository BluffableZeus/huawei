import * as _ from 'underscore'
import { TranslateService } from '@ngx-translate/core'
import { Injectable, ViewContainerRef } from '@angular/core'
import { CommonService } from '../../core/common.service'
import { EventService } from 'ng-epg-sdk/services'
import { Logger } from 'ng-epg-sdk/utils'
import { session } from 'src/app/shared/services/session'
import { AuthService } from '../../shared/services/auth.service'

const log = new Logger('error-message.service')

const errorType = 6

const interfaceError = [
  { 'interfaceName': 'PlayPVR', 'errorCode': '146021006' },
  { 'interfaceName': 'Authenticate', 'errorCode': '157021017' },
  { 'interfaceName': 'Authenticate', 'errorCode': '157022007' },
  { 'interfaceName': 'Authenticate', 'errorCode': '157022012' },
  { 'interfaceName': 'Authenticate', 'errorCode': '157021002' },
  { 'interfaceName': 'Authenticate', 'errorCode': '157021001' },
  { 'interfaceName': 'AddPVR', 'errorCode': '00379918' },
  { 'interfaceName': 'AddPeriodicPVR', 'errorCode': '126014701' },
  { 'interfaceName': 'AddPeriodicPVR', 'errorCode': '00379918' },
  { 'interfaceName': 'UpdatePVR', 'errorCode': '00379918' },
  { 'interfaceName': 'UpdatePeriodicPVR', 'errorCode': '00379918' }
]

const PLAYCHANNEL_TIPS_CODE = ['146020011', '146020012', '146020013', '146020014']

@Injectable()
export class ErrorMessageService {
  constructor (
        public commonService: CommonService,
        private translateService: TranslateService,
        private authService: AuthService
  ) { }
  /**
     * call the method named 'openDialog' for pop error dialog
     */
  openErrorDialog (viewContainerRef: ViewContainerRef, errorObject) {
    if (session.get('NO_POP_UP_ERROR_DIALOG')) {
      return
    }
    EventService.emit('ERROR_MESSAGE_DIALOG', errorObject)
  }
  /**
     * filter error code, no pop-up error code dialog
     * deal with them with a special way which is the filter error code
     * interfaceName：interface name；
     * errorCode：error code.
     */
  filterInterfaceName (interfaceName, errorCode) {
    let errorlist = _.filter(interfaceError, function (object, index) {
      if (object['interfaceName'] === interfaceName && object['errorCode'] === errorCode) {
        return true
      }
    })
    if ((errorlist && errorlist.length >= 1) || errorCode === '157021009') {
      return true
    }
  }

  errorObjectNone (errorCode, errorObj, interfaceName) {
    let errorObject = errorObj
    if (errorCode === '38079') {
      errorObject = _.clone(this.getErrorObject('MSA.38079', 'MSA'))
      errorObject['m'] = errorObject['m'] + '(' + errorType + errorCode + ')'
      EventService.emit('SHOW_VSP_ERROR_TOAST', errorObject['m'])
      return
    }
    let codeArray = errorCode.split('.')
    if (codeArray[0] === 'http') {
      errorObject = _.clone(this.getErrorObject('MSA.' + codeArray[1], 'MSA')) ||
                _.clone(this.getErrorObject('MSA.default', 'MSA'))
    } else if (['157021002', '125023001'].includes(errorCode)) {
      errorObject = _.clone(this.getErrorObject('Logout.125023001', 'VSP'))
    } else {
      errorObject = _.clone(this.getErrorObject(interfaceName +
                '.' + 'default', 'VSP')) || _.clone(this.getErrorObject('MSA.default', 'MSA'))
      if (errorCode === '110011001') {
        EventService.emit('HIDE_VOD_POPUP_DIALOG')
      }
    }
    return errorObject
  }
  /**
     * get interface name and error code by errorEvent
     * get error code object from json file, deal with error code
     */
  handlePlatformError (viewContainerRef: ViewContainerRef, errorEvent) {
    let interfaceName = _.last(errorEvent.url.split('/')).toString()
    if (/[?]+/.test(interfaceName)) {
      interfaceName = interfaceName.split('?')[0]
    }
    if (!_.isUndefined(errorEvent['usedTime']) && errorEvent['usedTime'] > 15 * 1000) {
      this.commonService.MSAErrorLog(26752, interfaceName + '')
    }
    let errorCode = errorEvent.resp.result && errorEvent.resp.result.retCode
    if (this.translateService.instant('messageResource') && this.translateService.instant('messageResource')['VSP']) {
      this.errorHandler(interfaceName, errorCode, viewContainerRef)
    } else {
      _.delay(() => {
        this.errorHandler(interfaceName, errorCode, viewContainerRef)
      }, 1000)
    }
  }

  /**
     * error object handler.
     */
  errorHandler (interfaceName, errorCode, viewContainerRef) {
    let errorObject = _.clone(this.getErrorObject(interfaceName + '.' + errorCode, 'VSP'))
    if (!errorObject) {
      errorObject = this.errorObjectNone(errorCode, errorObject, interfaceName)
      if (!errorObject) {
        return
      }
    }
    errorObject['interfaceName'] = interfaceName
    this.checkErrorLogic(errorObject, errorCode, viewContainerRef)
  }

  /**
     * return error code object accordding to interface name, error code, error code type
     * key：interface name + error code(if you can not find it, as default)
     * type：error code type ,there are MSA\PCPlayer\VSP and so on.
     */
  getErrorObject (key: String, type: String) {
    let errorResources = this.translateService.instant('messageResource')[type + '']
    return errorResources[key + '']
  }
  /**
     * check the action type first, then handle the error event according to the type.
     */
  checkErrorLogic (errorObject, errorCode, viewContainerRef) {
    this.logVSPError(errorObject['interfaceName'], errorCode)
    if (errorObject['action'] === 'log') {

    } else if (errorObject['action'] === 'toast') {
      let codeKey = errorObject['interfaceName'] + '.' + errorCode
      let tipsMessage = this.translateService.instant('messageResource')['VSP'][codeKey + '']['m']
      EventService.emit('SHOW_VSP_ERROR_TOAST', tipsMessage)
    } else if (errorObject['action'] === 'ServiceLogic') {
      if (errorObject['interfaceName'] === 'AddPVR' && errorCode === '126014701') {
        let tipsMessage = this.translateService.instant('program_recorded')
        EventService.emit('SHOW_VSP_ERROR_TOAST', tipsMessage)
      }
    } else if (errorObject['action'] === 'conditional') {
      this.handleConditionalType(errorObject)
    } else {
      this.handleTipsError(errorObject, errorCode, viewContainerRef)
    }
  }
  /**
     * handle error event whose type is 'tips'.
     */
  handleTipsError (errorObject, errorCode, viewContainerRef) {
    errorObject.m += ' ' + this.translateService.instant('platform_error_code', { 'errorCode': ('' + errorType + errorObject.e) })
    if (errorObject['action'] === 'tips' && errorObject['interfaceName'] === 'Authenticate' &&
          document.querySelector('md-dialog-container app-account-login')) {
      EventService.emit('SHOW_AUTH_ERROR_MESSAGE', errorObject.m)
    } else {
      if (this.filterInterfaceName(errorObject['interfaceName'], errorCode)) {
        return
      }
      // don't show error dialog about PlayChannel except geographical location limit.
      if (errorObject['interfaceName'] === 'PlayChannel' && !this.isPlayChannelTipsCode(errorCode)) {
        return
      }

//    /**  Disable auto login in error message */
//    /** bug-1849 */
//
//      if ((errorObject['e'] === '32122') && (Cookies.get('AUTH_TOKEN') !== 'undefined')) {
//        let req = {
//          authenticateBasic: {
//            userID: Cookies.get('USER_ID'),
//            userType: '1',
//            authToken: Cookies.get('AUTH_TOKEN')
//          }
//        }
//        this.authService.authenticate(req).then(() => {
//          location.reload()
//        }, () => {
//          Cookies.set('AUTH_TOKEN')
//        })
//      } else {
//        EventService.emit('CLOSED_ERROR_DIALOG')
//        console.log(errorObject)
//        this.openErrorDialog(viewContainerRef, errorObject)
//      }

    }
  }
  /**
     * log the error information no matter what type the error is.
     */
  logVSPError (interfaceName, errorCode) {
    let errorJson = _.extend(require('../../../assets/i18n/' + 'log.json'))
    let errorSource = errorJson['messageResource']['VSP']
    let codeKey = interfaceName + '.' + errorCode
    let errorObject = errorSource && errorSource[codeKey]
    let message = ''
    if (!errorObject) {
      codeKey = interfaceName + '.default'
      errorObject = errorSource && errorSource[codeKey]
    }
    message = errorObject && errorObject['m']
    log.error('{interface: %s, message: %s}', interfaceName, message)
  }
  /**
     * handle error event whose type is 'conditional'.
     */
  handleConditionalType (errorObject) {
    if (errorObject['interfaceName'] === 'ModifyDeviceInfo') {
      let isManualModify = session.get('IS_MANUAL_SET_DEVICE_NAME')
      if (isManualModify) {
        let failMessage = errorObject['m']
        EventService.emit('SHOW_VSP_ERROR_TOAST', failMessage)
      }
    } else if (errorObject['interfaceName'] === 'QueryCustomizeConfig') {
      EventService.emit('SHOW_VSP_ERROR_TOAST', errorObject['m'])
    } else if (errorObject['interfaceName'] === 'QueryRecmVODList') {
      let isDefaultKeyWord = session.get('IS_DEFAULT_HOT_KEYWORD')
      if (!isDefaultKeyWord) {
        session.remove('IS_DEFAULT_HOT_KEYWORD')
        EventService.emit('SHOW_VSP_ERROR_TOAST', errorObject['m'])
      }
    }
  }

  isPlayChannelTipsCode (errorCode) {
    let findCode = _.find(PLAYCHANNEL_TIPS_CODE, item => {
      return item === errorCode
    })
    if (findCode) {
      return true
    } else {
      return false
    }
  }
}
