import { Injectable } from '@angular/core'
import { getPlaybillDetail } from 'ng-epg-sdk/vsp'
import { queryPlaybillList } from 'ng-epg-sdk/vsp'
import { CommonService } from '../../core/common.service'
import { CustomConfigService } from './custom-config.service'
import { session } from 'src/app/shared/services/session'

@Injectable()
export class LiveTvService {
  /**
     * name for modules that would be used in this module
     */
  constructor (
        private commonService: CommonService,
        private customConfigService: CustomConfigService
  ) { }
  /**
     * get playbilldetail by playbillID
     */
  getDetail (playbillID, sceneID?: string) {
    return this.customConfigService.getCustomizeConfig({ queryType: '0' }).then(config => {
      let customizeConfig = config['list']
      let nameSpace = customizeConfig['ott_channel_name_space']
      let options = {
        params: {
          SID: sceneID,
          DEVICE: 'PC',
          DID: session.get('uuid_cookie')
        }
      }
      return getPlaybillDetail({
        channelNamespace: nameSpace,
        playbillID: playbillID,
        isReturnAllMedia: '1'
      }, options).then(resp => {
        let channelDet = resp['playbillDetail']
        let leftTime
        if (resp.playbillDetail.endTime > Date.now()['getTime']()) {
          leftTime = this.formatTime(resp.playbillDetail.endTime)
        } else {
          leftTime = '0'
        }
        let list = {
          name: resp.playbillDetail.name,
          channel: channelDet['channelDetail'] ? channelDet['channelDetail']['name'] : '',
          leftTime: leftTime,
          progress: this.formatProgress(resp.playbillDetail.startTime, resp.playbillDetail.endTime),
          startTimeorg: resp.playbillDetail.startTime,
          startTime: this.commonService.dealWithDateJson('D10', resp.playbillDetail.startTime),
          endTime: this.commonService.dealWithDateJson('D10', resp.playbillDetail.endTime),
          Series: resp.playbillDetail.playbillSeries,
          introduce: resp.playbillDetail.introduce,
          rating: resp.playbillDetail.rating.name,
          playbillDetail: resp.playbillDetail
        }
        return list
      })
    })
  }

  prefixZero (str, len) {
    str = '' + (str || '')
    if (str.length >= len) {
      return str
    }

    let gapLen = len - str.length, i, arr = []
    for (i = 0; i < gapLen; i++) {
      arr[i] = '0'
    }

    return arr.join('') + str
  }

  formatTime (time) {
    let date = Date.now()
    let nowTime = date['getTime']()
    let minute
    minute = (time - nowTime) / 60000
    return this.commonService.dealWithPlaybillTimeReduce(nowTime, time)
  }

  formatProgress (start, end) {
    let date = Date.now()
    let nowTime = date['getTime']()
    let progress
    if (window.innerWidth <= 1440) {
      progress = (nowTime - start) / (end - start) * 153
    } else {
      progress = (nowTime - start) / (end - start) * 186
    }
    return Math.round(Number(progress))
  }

  /**
     * format time string
     */
  formatStr (str, index) {
    let weekend
    let day
    let hour
    let minute
    let month
    let allweekends = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
    let arr = []
    if (str.length === 13) {
      let date = parseInt(str, 10)
      let datestr = new Date(date)
      month = (datestr.getMonth() + 1) < 10 ? '0' + (datestr.getMonth() + 1) : (datestr.getMonth() + 1)
      weekend = datestr.getDay()
      day = datestr.getDate() < 10 ? '0' + datestr.getDate() : datestr.getDate()
      hour = datestr.getHours() < 10 ? '0' + datestr.getHours() : datestr.getHours()
      minute = datestr.getMinutes() < 10 ? '0' + datestr.getMinutes() : datestr.getMinutes()
      weekend = allweekends[weekend]
    } else {
      month = str.slice(4, 6)
      day = str.slice(6, 8)
      hour = str.slice(8, 10)
      minute = str.slice(10, 12)
    }
    arr.push(month)
    arr.push(weekend)
    arr.push(day)
    arr.push(hour)
    arr.push(minute)
    return arr[index]
  }

  /**
     * call the interface and obtain the list of the specified programs according to the query criteria
     */
  getQueryPlaybill (req: any, option?: any) {
    let options = option || {}
    return queryPlaybillList(req, options).then(resp => {
      return resp
    })
  }
}
