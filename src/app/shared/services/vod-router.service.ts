import { Injectable } from '@angular/core'
import { Router } from '@angular/router'
import { getVODDetail } from 'ng-epg-sdk/vsp'
import { GetVODDetailResponse } from 'ng-epg-sdk/vsp/api'

/**
 * Service, responsible for VOD routing and cachning.
 *
 * Instead of using Angular Router, VODs should be navigated by importing VODRouterService as following:
 * `constructor(private vodRouter: VODRouterService) {}`
 *
 * To navigate to given content, use `this.vodRouter.navigate(VODId)`.
 * It resolves to one of the following routes:
 * /movie/2422 - for movie content
 * /series/1383 - for series
 * /series/1383/season/1 - for seasons in series
 * /video/1371 - for episodes
 *
 * Respective components then can reuse VODDetail information without sending a request to server:
 * `this.vodRouter.getVOD(VODId).then(resp => { this.VODDetail = resp.VODDetail })`
 * In case of the season, parent series is also cached and can be obtained:
 * `this.vodRouter.getSeries(seriesId).then(resp => { this.seriesDetail = resp.VODDetail })`
 */
@Injectable()
export class VODRouterService {
  private cachedVODId: string
  private cachedVODReponse: GetVODDetailResponse
  private cachedSeasonId: string
  private cachedSeasonResponse: GetVODDetailResponse
  private cachedSeriesId: string
  private cachedSeriesResponse: GetVODDetailResponse

  constructor (
        private router: Router
    ) {
    console.log('[VODRouter]: Constructor called')
    this.cachedVODId = null
    this.cachedVODReponse = null
    this.cachedSeriesId = null
    this.cachedSeriesResponse = null
  }

    /**
     * Returns backend VOD Detail response. If the information was requested previously - returns cached response
     * @param {string} VODId [VOD ID]
     * @returns {Promise<GetVODDetailResponse>} [API response]
     */
  getVOD (VODId: string): Promise<GetVODDetailResponse> {
    if (this.cachedVODId == VODId) {
      console.log('[VODRouter]: Serving cached VOD', VODId)
      return new Promise(resolve => resolve(this.cachedVODReponse))
    } else {
      console.log('[VODRouter]: Requesting and caching VOD', VODId)
      return new Promise(resolve => {
        getVODDetail({ VODID: VODId,
            queryDynamicRecmContent: {
              recmScenarios: [{ contentType: 'VOD',
                entrance: 'VOD_Like_Many_of_its_Kind',
                contentID: VODId,
                count: '12',
                offset: '0' },
              { contentType: 'VOD',
                entrance: 'VOD_Viewers_also_Watched',
                contentID: VODId,
                count: '12',
                offset: '0' }]}}).then(resp => {
                  this.cachedVODId = VODId
                  this.cachedVODReponse = resp
                  resolve(resp)
                })
      })
    }
  }

    /**
     * Returns backend VOD Detail response for series. If the information was requested previously - returns cached response
     * @param {string} seriesId [series ID]
     * @returns {Promise<GetVODDetailResponse>} [API response]
     */
  getSeries (seriesId: string): Promise<GetVODDetailResponse> {
    if (this.cachedSeriesId == seriesId) {
      console.log('[VODRouter]: Serving cached series', seriesId)
      return new Promise(resolve => resolve(this.cachedSeriesResponse))
    } else {
      console.log('[VODRouter]: Requesting and caching series', seriesId)
      return new Promise(resolve => {
        getVODDetail({ VODID: seriesId,
            queryDynamicRecmContent: {
              recmScenarios: [{ contentType: 'VOD',
                entrance: 'VOD_Like_Many_of_its_Kind',
                contentID: seriesId,
                count: '12',
                offset: '0' },
              { contentType: 'VOD',
                entrance: 'VOD_Viewers_also_Watched',
                contentID: seriesId,
                count: '12',
                offset: '0' }]}}).then(resp => {
                  this.cachedSeriesId = seriesId
                  this.cachedSeriesResponse = resp
                  resolve(resp)
                })
      })
    }
  }

    /**
     * Returns backend VOD Detail response for season. If the information was requested previously - returns cached response
     * @param {string} seasonId [season ID]
     * @returns {Promise<GetVODDetailResponse>} [API response]
     */
  getSeason (seasonId: string): Promise<GetVODDetailResponse> {
    if (this.cachedSeasonId == seasonId) {
      console.log('[VODRouter]: Serving cached season', seasonId)
      return new Promise(resolve => resolve(this.cachedSeasonResponse))
    } else {
      console.log('[VODRouter]: Requesting and caching season', seasonId)
      return new Promise(resolve => {
        getVODDetail({ VODID: seasonId,
            queryDynamicRecmContent: {
              recmScenarios: [{ contentType: 'VOD',
                entrance: 'VOD_Like_Many_of_its_Kind',
                contentID: seasonId,
                count: '12',
                offset: '0' },
              { contentType: 'VOD',
                entrance: 'VOD_Viewers_also_Watched',
                contentID: seasonId,
                count: '12',
                offset: '0' }]}}).then(resp => {
                  this.cachedSeasonId = seasonId
                  this.cachedSeasonResponse = resp
                  resolve(resp)
                })
      })
    }
  }

    /**
     * Determines season number from VODDetail
     * @param {GetVODDetailResponse['VODDetail']} VODDetail [VOD information]
     * @returns {number} [season number]
     */
  getSeasonNumber (VODDetail: GetVODDetailResponse['VODDetail']): number {
    for (let i = 0; i < VODDetail.brotherSeasonVODs.length; i++) {
      let season = VODDetail.brotherSeasonVODs[i]
      if (season.VOD.ID == VODDetail.ID) {
        return Number(season.sitcomNO)
      }
    }
      // No valid season found in brother VODs
    return 0
  }

    /**
     * Gets season information by series id and season number. Reuses `getVOD` and `getSeries` to get cached responses.
     * @param {string} seriesId [Series ID]
     * @param {number} seasonNumber [Season Number]
     * @returns {Promise<GetVODDetailResponse>} [API response]
     */
  getSeasonByNumber (seriesId: string, seasonNumber: number): Promise<GetVODDetailResponse> {
    return new Promise(resolve => {
      this.getSeries(seriesId).then(seriesResponse => {
        let seasonsInfo = seriesResponse.VODDetail.episodes
        for (let i = 0; i < seasonsInfo.length; i++) {
            let seasonInfo = seasonsInfo[i]
            if (Number(seasonInfo.sitcomNO) == seasonNumber) {
              this.getVOD(seasonInfo.VOD.ID).then(resp => {
                resolve(resp)
              })
              break
            }
          }
      })
    })
  }

    /**
     * Determines parent ID from VODDetail
     * @param {GetVODDetailResponse['VODDetail']} VODDetail [VOD information]
     * @returns {string} [series ID]
     */
  getParentId (VODDetail: GetVODDetailResponse['VODDetail']): string {
    return VODDetail.series[0] && VODDetail.series[0].VODID
  }

    /**
     * Uses Angular.Router to navigate to respective VOD url
     * @param {any} VODId
     */
  navigate (VODId) {
    VODId = String(VODId)
    this.getVOD(VODId).then(resp => {
      const VODDetail = resp.VODDetail
      switch (Number(VODDetail.VODType)) {
        case 1:
        case 2:
          this.router.navigate(['video', 'series', VODId])
          break
        case 3: {
            console.log(`navigating series, type ${VODDetail.VODType}`, VODDetail)
            let seriesId = this.getParentId(VODDetail)
            if (seriesId) {
              this.router.navigate(['video', 'series', seriesId, 'season', this.getSeasonNumber(VODDetail)])
            } else {
              console.log('[VODRouter]: ERROR! Could not find series for season', VODId)
            }
            break
          }
        case 0: {
            if (VODDetail.series.length > 0) {
              this.router.navigate(['video', 'video', VODId])
            } else {
              this.router.navigate(['video', 'movie', VODId])
            }
            break
          }
        default:
          console.log('[VODRouter]: Unknown VODType', VODDetail.VODType, ', VODID: ', VODId)
      }
    })
    document.body.scrollTop = 0
    document.documentElement.scrollTop = 0
  }
}
