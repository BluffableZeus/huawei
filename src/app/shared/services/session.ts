import * as _ from 'underscore'
let json = {}

/**
 *
 */
export const session = {
  /**
     * Obtain value by key
     *
     * @param {String} key
     * @return {Object} value
     */
  get: function (key) {
    if (!_.has(json, key)) {
      const valueStr = sessionStorage.getItem(key) || localStorage.getItem(key)
      if (valueStr) {
        json[key] = JSON.parse(valueStr)
      }
    }

    return json[key]
  },

  /**
     * Save value to session
     *
     * @param {String} key
     * @param {Object} value
     * @param {Boolean} [persist=false] whether store value to local
     */
  put: function (key: string, value: any, isPersistence = false) {
    if (_.isNull(value) || _.isUndefined(value) || _.isNaN(value)) {
      this.remove(key)

      return
    }

    json[key] = value

    const valueStr = JSON.stringify(value)

    if (isPersistence) {
      localStorage.setItem(key, valueStr)
    } else {
      sessionStorage.setItem(key, valueStr)
    }
  },

  /**
     * Clear values
     *
     * @param {Boolean} [persist=false] If true, will clear all values include data stored on local
     */
  clear: function (isAlsoClearPersistent) {
    json = {}
    sessionStorage.clear()
    if (isAlsoClearPersistent) {
      localStorage.clear()
    }
  },
  /**
     * Remove value by key
     *
     * @param {String} key
     */
  remove: function (key) {
    delete json[key]
    sessionStorage.removeItem(key)
    localStorage.removeItem(key)
  },

  pop: function (key) {
    const val = this.get(key)
    this.remove(key)
    return val
  }
}
