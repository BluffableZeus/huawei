import * as moment from 'src/assets/lib/Timezone'
export const config = {
  'crmBaseUrl': GLOBAL_CONFIG.crmURL,
  'base_url': 'http://try2.mediastage.tv',
  'site_name': 'MediaStage',
  'logLevel': 'none',
  'vspUrl': '',
  'vspHttpsUrl': '',
  'signatureKeyAlgorithm': 'DH',
  'isSupportPrepare': false,
  'UTCEnable': '1',
  'timeZone': moment(),
  'preDateNum': '7',
  'nextDateNum': '7',
  /**
     * judge whether open debug mode, can test kpi and so on after open debug mode, can extend this ability in the future
     */
  'openDebugModel': false,
  'Version': 'Video MSA V600R001C30B001',
  'Carrier': 'Huawei',
  /**
     * recording business configuration values：CPVR，NPVR，CPVR&NPVR, MIXPVR
     */
  'PVRMode': 'CPVR&NPVR',
  /**
     * the cache of all channels
     */
  'allChannelCache': [],
  /**
     * the cache of all channels which support NPVR
     */
  'nPVRChannelCache': [],

  'cPVRChannelCache': [],
  /**
     * judge whether support reminder business
     */
  'reminderMode': true,

  'PLAY_URL': '',
  /**
     * check whether support all types of CPVR.
     * true: means support all types of CPVR.
     * false: means only support playbill-based CPVR.
     */
  supportAllCPVR: true,
  /**
     * length of avatar.
     */
  avatarLength: 16,
  /**
     * Language types in configuration files
     */
  'SupportLanguageList': ['en', 'zh', 'ru', 'es'],
  /**
     * URL to a privacy page.
     */
  privacyLink: 'https://www.mtstv.ru/privacy',
  /**
     * URL to a software license page.
     */
  softwareLicenseLink: 'https://www.mtstv.ru/terms',
  /**
     * URL to a FAQ page.
     */
  FAQLink: 'https://www.mtstv.ru/faq',
  /**
     * Email address for feedback.
     */
  feedbackEmail: 'help@mtstv.ru',
  /**
    * Google Play App Link
    */
  playMarketApp: 'https://play.google.com/store/apps/details?id=com.finchmil.thtclub',
  /**
    * AppStore Link
    */
  appStoreApp: 'https://itunes.apple.com/ru/app/%D1%82%D0%BD%D1%82-club/id911345730'
}

if (console) {
  console.log(`[APP_Version]: ${config.Version}`)
}
