import { Injectable } from '@angular/core'
import { CanActivate, Router } from '@angular/router'
import { session } from 'src/app/shared/services/session'
import { AuthService } from './auth.service'
import { TimerService } from './timer.service'
import { EventService } from 'ng-epg-sdk/services'
import { ChannelCacheService } from './channel-cache.service'

const pageTracker = require('../../../assets/lib/PageTracker')

/**
 * export class AuthGuard.
 */
@Injectable()
export class AuthGuard implements CanActivate {
  constructor (
        private authService: AuthService,
        private router: Router,
        private channelCacheService: ChannelCacheService,
        private timerService: TimerService
  ) { }

  canActivate (): Promise<boolean> | boolean {
    let self = this
    /**
         * judge whethe user log in
         * start check heartbeat after user log in
         * check language of this user and switch
         * start browsing collection server address
         * refresh the page after the success of the user authentication, if the data of session not exist to call interface,
         *  or exist send relevant events
         */
    if (this.authService.isProfileLogin()) {
      if (!session.get('CUSTOM_CONFIG_DATAS')) {
        this.timerService.getCustomConfig()
      }
      Cookies.remove('IS_GUEST_LOGIN')
      EventService.emit('LANGUAGE_QUERYCUSTOMIZE')
      EventService.emit('LOGIN')
      this.setTrackerParams()
      return true
    }
    /**
         * visitors log in judgment
         * not start check heartbeat after visitors log in
         * check language of this visitor and switch
         * start browsing collection server address
         * refresh the page after the success of the user authentication, if the data of session not exist to call interface,
         *  or exist send relevant events
         */
    if (this.authService.isGuestLogin()) {
      if (!session.get('CUSTOM_CONFIG_DATAS')) {
        this.timerService.getCustomConfig('guest')
      }
      EventService.emit('LANGUAGE_QUERYCUSTOMIZE')
      EventService.emit('LOGIN')
      this.setTrackerParamsGuest()
      return true
    }
    /**
         * open the browser to access the page for the first time visitors
         * start the visitors log in access, authentication interface
         */
    return new Promise((resolve) => {
      this.authService.guestLogin().then(() => {
        self.channelCacheService.refresh().then(() => {
          resolve(true)
        }, () => {
          resolve(true)
        })
      }, () => {
        resolve(false)
      })
    })
  }

  /**
     * the beginning of the track user behavior acquisition
     * browsing the server address
     */
  setTrackerParams () {
    /**
         * define user behavior trajectory acquisition constants
         * tracker:
         * profileID:
         * deviceID:
         * trackParams:
         */
    const tracker = session.get('tracker') && session.get('tracker')[0]
    const profileID = session.get('profileSN') ? session.get('profileSN') : session.get('profileID')
    const deviceID = session.get('uuid_cookie')
    const trackParams = {
      actionUrl: tracker ? tracker.pageTrackerServerURL : '',
      subscriberID: profileID,
      deviceID: deviceID,
      appid: tracker ? tracker.appID : '',
      apppwd: tracker ? tracker.appPassword : '',
      needSend: tracker ? (tracker.isSupportedUserLogCollect === '1') : false,
      deviceType: tracker ? tracker.type : '',
      sendCount: 5,
      sendTime: 10000
    }
    pageTracker.setTrackerParams(trackParams)
  }

  /**
     * define visitors behavior trajectory acquisition constants
     * tracker:
     * profileID:
     * deviceID:
     * trackParams:
     */
  setTrackerParamsGuest () {
    const tracker = session.get('tracker') && session.get('tracker')[0]
    const profileID = session.get('profileSN') ? session.get('profileSN') : session.get('profileID')
    const deviceID = session.get('uuid_cookie')
    const trackParams = {
      actionUrl: tracker ? tracker.pageTrackerServerURL : '',
      subscriberID: profileID,
      deviceID: deviceID,
      appid: tracker ? tracker.appID : '',
      apppwd: tracker ? tracker.appPassword : '',
      needSend: tracker ? (tracker.isSupportedUserLogCollect === '1') : false,
      deviceType: tracker ? tracker.type : '',
      sendCount: 5,
      sendTime: 10000
    }
    pageTracker.setTrackerParams(trackParams)
  }
}
