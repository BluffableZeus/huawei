import * as _ from 'underscore'
import { Injectable } from '@angular/core'
import { QueryCustomizeConfigRequest, QueryCustomizeConfigResponse } from 'ng-epg-sdk/vsp'
import { TimerService } from './timer.service'
import { session } from 'src/app/shared/services/session'

@Injectable()
export class CustomConfigService {
  constructor (
        private timerService: TimerService
  ) { }
  /*
    query configuration parameters
    */
  getCustomizeConfig (req: QueryCustomizeConfigRequest): Promise<QueryCustomizeConfigResponse> {
    return this.timerService.getCaheCustomConfig().then((resp: QueryCustomizeConfigResponse) => {
      if (req.queryType === '0') {
        resp['list'] = {}
        let configuration = _.find(resp['configurations'], (data) => {
          return data['cfgType'] === '0'
        })
        _.each(configuration && configuration['values'], (item, index) => {
          resp['list'][item['key']] = item['values'][0]
        })
      }
      session.put('vodSubjectId', resp['list']['vod_subject_id'])
      return resp
    })
  }
}
