import { Injectable, OnDestroy } from '@angular/core'
import { environment } from '../../../environments/environment'
import * as _ from 'underscore'
import Fingerprint from 'fingerprintjs'
import { authenticate, login, AuthenticateTolerant, AuthenticateResponse, setGlobalFilterCond } from 'ng-epg-sdk/vsp'
import { session } from 'src/app/shared/services/session'
import { EventService, HeartbeatService } from 'ng-epg-sdk/services'
import { config as EPGConfig } from './config'
import { Config as STBConfig } from 'ng-epg-sdk/core/config'
import { CommonService } from '../../core/common.service'
import { CacheConfigService } from './cache-config.service'
import { TimerService } from './timer.service'
import { meaSureParam } from './object'

const pageTracker = require('../../../assets/lib/PageTracker')

@Injectable()
export class AuthService implements OnDestroy {
    // timer for ntp domain.
  public ntpDomainTimeout
    // url of ntp domain.
  public ntpDomainUrl
    // date of the client.
  public clientDate: any

    /**
     * constructor of class AuthService.
     */
  constructor (
        private commonService: CommonService,
        private cacheConfigService: CacheConfigService,
        private heartbeatService: HeartbeatService,
        private timerService: TimerService
    ) {
  }

    /**
     * destory function.
     * clear the timer.
     */
  ngOnDestroy () {
    clearTimeout(this.ntpDomainTimeout)
  }

    /**
     * make client time is synchronous with the server.
     * @param {any} ntpDomain [value of url of ntp domain]
     */
  getClidentTime (ntpDomain?: any) {
    let xhr = new XMLHttpRequest()
    let ntpDomainUrl = ntpDomain
    clearTimeout(this.ntpDomainTimeout)
    xhr.open('HEAD', '/' + '?t=' + new Date()['getTime'](), true)
    xhr.onreadystatechange = () => {
      if (xhr.readyState === 4) {
        if (xhr.status === 200 || xhr.status === 304) {
            this.clientDate = new Date()
            let serverDate = (Date['_parse'] || Date.parse)(xhr.getResponseHeader('Date')) || this.clientDate
            let offset = serverDate - this.clientDate
            session.put('timeoffset', offset)
            this.ntpDomainTimeout = setTimeout(() => {
              this.getClidentTime(ntpDomainUrl)
            }, 60 * 60 * 1000)
          } else {
            this.commonService.MSAErrorLog(23000)
            this.ntpDomainTimeout = setTimeout(() => {
              this.getClidentTime(ntpDomainUrl)
            }, 5 * 60 * 1000)
          }
      }
    }
    xhr.send()
  }

    /**
     * to get the value of session whose key is 'IS_GUEST_LOGIN'.
     */
  isGuestLogin () {
    return Cookies.getJSON('IS_GUEST_LOGIN')
  }

    /**
     * to get the value of session whose key is 'IS_PROFILE_LOGIN'.
     */
  isProfileLogin () {
    if (session.get('loginOccasion') === 1) {
      STBConfig.loginOccasion = true
    }
    return Cookies.getJSON('IS_PROFILE_LOGIN')
  }

    /**
     * filter the content of VR
     */
  filterVRContent () {
    setGlobalFilterCond({
      globalFilter: {
        dimension: ['2']
      }
    })
  }

    /**
     * handler after authentication is successful.
     * @param {Object} resp [responce of interface Authenticate]
     */
  authenticateSuccess (resp) {
    this.openUserSkipSetting()
    this.getClidentTime()
      // filter the content of VR after authenticate successfully
    this.filterVRContent()
    let profileId = resp.profileID
    let profileInfo = _.find(resp.profiles, item => {
      return item['ID'] === profileId
    })
    Cookies.set('PROFILE_TYPE', profileInfo['profileType'])
    if (resp.profileID) {
      Cookies.set('IS_PROFILE_LOGIN', true)
      Cookies.remove('IS_GUEST_LOGIN')
    } else {
      this.commonService.commonLog('Authenticate', 'profileID')
    }
    EventService.emit('AUTH_SUCCESS_CLOSE_LOGIN_DIALOG')
    EventService.emit('LOGIN')
    if (resp['xrResp']) {
      delete resp['xrResp']
    }
    session.put('AUTHENTICATE_RESP', resp)
    session.remove('CUSTOM_CONFIG_DATAS')
    this.timerService.getCustomConfig()
    EventService.on('CUSTOM_CACHE_CONFIG_DATAS', () => {
      this.timerService.getRecordMessage()
      this.timerService.getPwdData()
      this.timerService.getCurrencyRate()
      EventService.emit('LANGUAGE_QUERYCUSTOMIZE')
      EventService.emit('FIRST_TIME_LOGIN')
    })
    session.put('areaid', resp.areaCode)
    session.put('templateName', resp.templateName)
    session.put('subnetId', resp.subnetID)
    session.put('bossId', resp.bossID)
    session.put('UserGroupNMB', resp.userGroup)
    session.put('EPGCONFIG', EPGConfig['PVRMode'], true)
    session.put('reminderMode', EPGConfig['reminderMode'])
    if ((+resp.loginOccasion) === 1) {
      session.put('loginOccasion', 1)
    }
    this.setMultiRights(resp)
    if (resp.VUID) {
      session.put('AuthResp_VUID', resp.VUID, true)
    }
  }

    /**
     * skip the head and tail of vod by default.
     */
  openUserSkipSetting () {
    let userID = Cookies.getJSON('USER_ID')
    session.put(userID + '_isSkipHeadAndTail', true, true)
  }

    /**
     * more screen support login authentication disaster ability demand
     */
  createAuthenticateTolerant () {
    let authenticateTolerant: AuthenticateTolerant = {}
    let areaCode = session.get('areaid')
    let templateName = session.get('templateName')
    let subnetID = session.get('subnetId')
    let bossID = session.get('bossId')
    let userGroup = session.get('UserGroupNMB')

    _.extend(authenticateTolerant, {
      areaCode: this.getAreaCode(areaCode),
      templateName: templateName && templateName !== '' ? templateName : '',
      subnetID: subnetID && subnetID !== '' ? subnetID : '',
      bossID: bossID && bossID !== '' ? bossID : '',
      userGroup: userGroup && userGroup !== '' ? userGroup : ''
    })
    return authenticateTolerant
  }

    /**
     * get value of area code.
     * @param {string} areaCode [area code of session]
     */
  getAreaCode (areaCode) {
    return areaCode && areaCode !== '' ? areaCode : ''
  }

    /**
     * set value of multirights.
     * @param {AuthenticateResponse} authenResp [responce of interface Authenticate]
     */
  setMultiRights (authenResp: AuthenticateResponse) {
    let verimatrix = meaSureParam(authenResp, ['CA', 'verimatrix'])
    this.caInfoMSALog(authenResp)
    let multiRightsWideVine = verimatrix && verimatrix.multirights_widevine
    let LA_URL = multiRightsWideVine && multiRightsWideVine.LA_URL
    if (authenResp['network'] === 'MultiDRM') {
      session.put('network', 'MultiDRM')
      return
    }
    if (authenResp['network'] === 'MultiRights' && LA_URL && authenResp.VUID) {
      LA_URL = `${LA_URL}?deviceId=${authenResp.VUID}`
      session.put('wideVine.license.server.url', LA_URL)
    }

    let multiRightsPlayready = verimatrix && verimatrix.multiRights_playready
    let LA_URL_READY = multiRightsPlayready && multiRightsPlayready.LA_URL
    if (authenResp['network'] === 'MultiRights' && LA_URL_READY && authenResp.VUID) {
      LA_URL_READY = `${LA_URL_READY}?deviceId=${authenResp.VUID}`
      session.put('playReady.license.server.url', LA_URL_READY)
    }
    session.put('playReady.License.sessionID', authenResp.jSessionID)
  }

    /**
     * set user filter.
     * @param {AuthenticateResponse} resp [responce of interface Authenticate]
     */
  putUserFilter (resp) {
    if (resp && resp.userFilter) {
      session.put('USER_FILTER', resp.userFilter, true)
    }
  }

    /**
     * call authenticate interface.
     * @param {any} req [request of interface Authenticate]
     */
  authenticate (req: any) {
    req.authenticateBasic.needPosterTypes = ['1', '2', '3', '4', '5', '6', '7']
    req.authenticateBasic.timeZone = EPGConfig.timeZone
    req.authenticateBasic.isSupportWebpImgFormat = '0'
    if (req.password) {
      req.authenticateBasic.clientPasswd = req.password
    }
    req.authenticateBasic.lang = session.get('language') ? session.get('language') : 'ru'
    req.authenticateDevice = this.setDeviceInfo()
    delete req.password
    let authenticateTolerant = this.createAuthenticateTolerant()
    if (!_.isEmpty(authenticateTolerant)) {
      _.extend(req, {
        authenticateTolerant: authenticateTolerant
      })
    }
    if (session.get('AuthResp_VUID')) {
      req.authenticateBasic.VUID = session.get('AuthResp_VUID')
    }
    this.heartbeatService.resetLastVersionInfo()
    session.remove('CUSTOM_CONFIG_DATAS')
    return login({ subscriberID: req.authenticateBasic.userID, deviceModel: 'PC' }).then(loginresp => {
      this.loginLogInfo(loginresp)

        // Dirty Hack
      loginresp.vspHttpsURL = environment.TV_PROXY_HTTPS
      loginresp.vspURL = environment.TV_PROXY_HTTP
      session.put('config_vspHttpsURL', loginresp.vspHttpsURL, true)
      session.put('config_vspURL', loginresp.vspURL, true)
      if (req.authenticateBasic.authToken) {
        req.authenticateBasic.authType = '2'
      } else {
        req.authenticateBasic.authType = '1'
      }
      req.authenticateDevice.authTerminalID = req.authenticateDevice.terminalID
      return authenticate(req).then(resp => {
        const oper_center = {
            subscriberId: resp.subscriberID,
            sessionId: resp.jSessionID,
            host: loginresp.vspHttpsURL.replace(/(^\w+:|^)\/\//, '')
          }
        this.authenticateLogInfo(resp)
        Cookies.set('USER_ID', req.authenticateBasic.userID)
        if (req.authenticateBasic.authType === '2') {
            Cookies.set('AUTH_TOKEN', req.authenticateBasic.authToken)
          } else {
            Cookies.set('AUTH_TOKEN', resp.authToken)
          }
        localStorage.setItem('oper_center', JSON.stringify(oper_center))
        this.putUserFilter(resp)
        return this.heartbeatService.startHeartbeat().then(response => {
            this.authenticateSuccess(resp)
            return resp
          }, () => {
            this.authenticateSuccess(resp)
            return resp
          })
      }, resp => {
        if (resp && resp.result && resp.result.retCode === '157021017') {
            this.authenticateLogInfo(resp)
            Cookies.set('USER_ID', req.authenticateBasic.userID)
            EventService.emit('CLOSED_LOGIN_DIALOG')
            EventService.removeAllListeners(['CONFIRM_CHECK_INIT_PWD'])
            EventService.on('CONFIRM_CHECK_INIT_PWD', () => {
              this.heartbeatService.startHeartbeat().then(response => {
                this.authenticateSuccess(resp)
              }, () => {
                this.authenticateSuccess(resp)
              })
            })
            EventService.emit('OPEN_CHANGE_INIT_PASSWORD_DIALOG')
            return Promise.resolve(resp)
          } else {
            return Promise.reject(resp)
          }
      })
    })
  }

    /**
     * get map of https port.
     */
  getHttpsPort () {
    let GlobaleConfig = {
      ssh_enable: 1,
        // the mapping definition of https port and http port
      protocolPortMap: {
        '443': '80',
        '33207': '33200',
        '8441': '8081',
        '8442': '8082',
        '8443': '8083',
        '8444': '8084',
        '8445': '8085',
        '8446': '8086',
        '8447': '8087',
        '8448': '8088',
        '8449': '8089',
        '8450': '8090',
        '8451': '8091',
        '8452': '8092',
        '8453': '8093',
        '8454': '8094',
        '8455': '8095',
        '8456': '8096',
        '8457': '8097',
        '8458': '8098',
        '8459': '8099',
        '8460': '8100'
      }
    }
    if (location.protocol === 'https') {
      return location.port
    }
    for (let prop in GlobaleConfig.protocolPortMap) {
      if (GlobaleConfig.protocolPortMap[prop] === location.port) {
        return prop
      }
    }
    return location.port
  }

    /**
     * login and authentication of guest.
     */
  guestLogin () {
    let lang
    if (!session.get('firstOpen')) {
      let browserLanguage = window.navigator.language.substring(0, 2)
      let languageList = EPGConfig.SupportLanguageList
      let tempLang = _.find(languageList, item => {
        return item === browserLanguage
      })
      lang = tempLang || 'ru'
    } else {
      lang = session.get('language') ? session.get('language') : 'ru'
    }
    let req = {
      authenticateBasic: {
        userType: '3',
        isSupportWebpImgFormat: '0',
        needPosterTypes: ['1', '2', '3', '4', '5', '6', '7'],
        lang: lang
      },
      authenticateDevice: this.setDeviceInfo()
    }
    let authenticateTolerant = this.createAuthenticateTolerant()
    if (!_.isEmpty(authenticateTolerant)) {
      _.extend(req, {
        authenticateTolerant: authenticateTolerant
      })
    }
    let ua = navigator.userAgent.toLowerCase()
    let port = ':' + this.getHttpsPort()
    let isIe = !!ua.match(/msie/i) || !!ua.match(/rv:([\d.]+).*like gecko/)
    let login_https_origin
    if (isIe) {
      let host = window.location.host.replace(/:\d+/, port)
      let protocol = window.location.protocol.replace('http', 'https')
      login_https_origin = protocol + '//' + host
    } else {
      login_https_origin = window.location.origin.replace('http', 'https').replace(/:\d+/, port)
    }
    if (ua.indexOf('windows nt 10.0 bgg') !== -1) {
      const isRedirectToHttps = location.protocol.toLocaleLowerCase().indexOf('https') === -1
      if (isRedirectToHttps) {
        STBConfig.vspUrl = login_https_origin
        STBConfig.vspHttpsUrl = login_https_origin
      }
    }
    this.heartbeatService.resetLastVersionInfo()
    session.remove('CUSTOM_CONFIG_DATAS')
    return login({ subscriberID: '', deviceModel: 'PC' }).then(loginresp => {
      this.loginLogInfo(loginresp)

      // Dirty Hack
      loginresp.vspHttpsURL = environment.TV_PROXY_HTTPS
      loginresp.vspURL = environment.TV_PROXY_HTTP

      session.put('config_vspHttpsURL', loginresp.vspHttpsURL, true)
      session.put('config_vspURL', loginresp.vspURL, true)
      let terminalParm = loginresp.terminalParm || {}
      session.put('terminalParm', terminalParm)
      this.timerService.getTerminalParam()
      return authenticate(req).then(resp => {
        this.authenticateLogInfo(resp)
        Cookies.set('IS_GUEST_LOGIN', true)
        EventService.emit('LOGIN')
        this.getClidentTime()
          // filter the content of VR after authenticate successfully
        this.filterVRContent()
          // user behavior reported
        session.put('tracker', resp.pageTrackers)
        session.put('profileIDGuest', resp.subscriberID)
        session.put('profileID', resp['profileID'])
        session.put('profileSN', resp['profileSN'])
        session.put('profileSn', resp['profileSn'])
        session.put('modify_profileID', resp.profileID, true)
        if (resp && resp.userFilter) {
            session.put('USER_FILTER', resp.userFilter, true)
          }
        const tracker: any = _.first(resp.pageTrackers)
        const trackParams = {
            actionUrl: tracker ? tracker.pageTrackerServerURL : '',
            subscriberID: resp['profileSN'] || resp['profileID'],
            deviceID: resp.deviceID,
            appid: this.getAppid(tracker),
            apppwd: tracker ? tracker.appPassword : '',
            needSend: tracker ? (tracker.isSupportedUserLogCollect === '1') : false,
            deviceType: tracker ? tracker.type : '',
            sendCount: 5,
            sendTime: 10000
          }
        pageTracker.setTrackerParams(trackParams)
        session.put('areaid', resp.areaCode)
        session.put('templateName', resp.templateName)
        session.put('subnetId', resp.subnetID)
        session.put('bossId', resp.bossID)
        session.put('UserGroupNMB', resp.userGroup)
        session.put('EPGCONFIG', EPGConfig['PVRMode'], true)
        if ((+resp.loginOccasion) === 1) {
            session.put('loginOccasion', 1)
          }
        this.timerService.getCustomConfig('guest')
        EventService.on('CUSTOM_CACHE_CONFIG_DATAS', () => {
            this.timerService.getRecordMessage()
            this.timerService.getPwdData()
            this.timerService.getCurrencyRate()
            EventService.emit('LANGUAGE_QUERYCUSTOMIZE')
          })
//        Disable for bug-1849
//        this.heartbeatService.startHeartbeat().then(response => {
//            if (_.isUndefined(response['personalDataVersions']) || _.isUndefined(response['personalDataVersions']['channel']) ||
//                        response['personalDataVersions']['channel'] === '') {
//              this.commonService.MSAErrorLog(38054, 'OnLineHeartbeat')
//            }
//          })
        return resp
      })
    })
  }

    /**
     * get app id of the client.
     * @param {String} tracker [value of tracker of session]
     */
  getAppid (tracker) {
    return tracker ? tracker.appID : ''
  }

    /**
     * set information of device.
     */
  setDeviceInfo () {
    let uuid = new Fingerprint({ screen_resolution: true }).get() + ''
    let authenticateDevice = {}
    authenticateDevice['physicalDeviceID'] = uuid
    authenticateDevice['terminalID'] = uuid
    authenticateDevice['deviceModel'] = 'PC'
    let ua = navigator.userAgent.toLowerCase()
    let isIe = !!ua.match(/msie/i) || !!ua.match(/rv:([\d.]+).*like gecko/)
    let isEdge = !!ua.match(/edge\/([\d.]+)/)
    let caDeviceType = (isIe || isEdge) ? '6' : '7'
    authenticateDevice['CADeviceInfos'] = [{ CADeviceType: caDeviceType, CADeviceID: uuid }]
    session.put('uuid_cookie', uuid, true)
    return authenticateDevice
  }

    /**
     * set value of cookie.
     * @param {String} name [uuid]
     * @param {String} value [uppercase of uuid]
     * @param {String} iDay [36500]
     */
  setCookie (name, value, iDay) {
    let date = Date.now()
    date['setDate'](date['getDate']() + iDay)
    session.put('uuid_cookie', value, true)
  }

    /**
     * get value of cookie.
     * @param {String} name [uuid]
     */
  getCookie (name) {
    return session.get('uuid_cookie') || ''
  }

    /**
     * log the error information about response of Login interface.
     * @param {Object} resp [responce of Login interface]
     */
  loginLogInfo (resp) {
    if (_.isUndefined(resp.vspHttpsURL) || resp.vspHttpsURL === '') {
      this.commonService.commonLog('Login', 'vspHttpsURL')
    }
    if (_.isUndefined(resp.vspURL) || resp.vspURL === '') {
      this.commonService.commonLog('Login', 'vspURL')
    }
    if (_.isUndefined(resp.upgradeDomain) && _.isUndefined(resp.upgradeDomainBackup)) {
      this.commonService.MSAErrorLog(38039)
    }
    this.moreLoginLogInfo(resp)
  }

    /**
     * log more error information about response of Login interface.
     * @param {Object} resp [responce of Login interface]
     */
  moreLoginLogInfo (resp) {
    if (_.isUndefined(resp.NTPDomain) && _.isUndefined(resp.NTPDomainBackup)) {
      this.commonService.MSAErrorLog(38040)
    }
    if (_.isUndefined(resp.terminalParm) || _.isEmpty(resp.terminalParm)) {
      this.commonService.MSAErrorLog(38041)
    }
    if (_.isUndefined(resp.parameters) || _.isUndefined(resp.parameters.SQMURL) || resp.parameters.SQMURL === '') {
      this.commonService.MSAErrorLog(38042)
    }
  }

    /**
     * log the error information about response of Authenticate interface.
     * @param {Object} resp [responce of Authenticate interface]
     */
  authenticateLogInfo (resp) {
    if (_.isUndefined(resp.pageTrackers) || resp.pageTrackers.length === 0) {
      this.commonService.commonLog('Authenticate', 'pageTrackers')
    }
    if (!_.isUndefined(resp.loginOccasion) && Number(resp.loginOccasion) === 1) {
      this.commonService.MSAErrorLog(38028, 'Authenticate')
    }
    if (_.isUndefined(resp.userToken) || resp.userToken === '') {
      this.commonService.MSAErrorLog(38044, 'Authenticate')
    }
    if (_.isUndefined(resp.jSessionID) || resp.jSessionID === '') {
      this.commonService.MSAErrorLog(38045, 'Authenticate')
    }
    this.moreAuthenticateLogInfo(resp)
  }

    /**
     * log more error information about response of Authenticate interface.
     * @param {Object} resp [responce of Authenticate interface]
     */
  moreAuthenticateLogInfo (resp) {
    if (_.isUndefined(resp.VUID) || resp.VUID === '') {
      this.commonService.MSAErrorLog(38046, 'Authenticate')
    }
    if (_.isUndefined(resp.deviceID) || resp.deviceID === '') {
      this.commonService.MSAErrorLog(38048, 'Authenticate')
    }
    this.disasterTolerantLog(resp)
    this.profileErrorLog(resp)
  }

    /**
     * log the error information about disaster tolerant.
     * @param {Object} resp [responce of Authenticate interface]
     */
  disasterTolerantLog (resp) {
    if (_.isUndefined(resp.areaCode) || resp.areaCode === '' ||
            _.isUndefined(resp.templateName) || resp.templateName === '' ||
            _.isUndefined(resp.subnetID) || resp.subnetID === '' ||
            _.isUndefined(resp.bossID) || resp.bossID === '') {
      this.commonService.MSAErrorLog(38003, 'Authenticate')
    }
  }

    /**
     * log the error information about profile id.
     * @param {Object} resp [responce of Authenticate interface]
     */
  profileErrorLog (resp) {
    if (_.isUndefined(resp.profileID) || resp.profileID === '') {
      this.commonService.MSAErrorLog(38047, 'Authenticate')
    } else if (_.isUndefined(resp.profiles) || _.isEmpty(resp.profiles) || this.checkNoExistInProfiles(resp.profileID, resp.profiles)) {
      this.commonService.MSAErrorLog(38047, 'Authenticate')
    }
  }

    /**
     * check whether the Profile-ID is not in the Profile lists.
     * if not in, return true;
     * otherwise, return false.
     * @param {Object} id [value of profileID]
     * @param {Object} profiles [profiles of responce of Authenticate interface]
     */
  checkNoExistInProfiles (id, profiles) {
    let findProfile = profiles.find(item => item['ID'] === id)
    return _.isUndefined(findProfile)
  }

    /**
     * log the error information about CA.
     * @param {Object} authenResp [responce of Authenticate interface]
     */
  caInfoMSALog (authenResp) {
    if (_.isUndefined(authenResp.CA) || _.isEmpty(authenResp.CA)) {
      this.commonService.commonLog('Authenticate', 'CA')
    } else {
      if (_.isUndefined(authenResp.CA.playReady) || _.isEmpty(authenResp.CA.playReady)) {
        this.commonService.MSAErrorLog(38051, 'Authenticate')
      }
      if (_.isUndefined(authenResp.CA.verimatrix) || _.isEmpty(authenResp.CA.verimatrix) ||
                _.isUndefined(authenResp.CA.verimatrix.multirights_widevine) ||
                _.isEmpty(authenResp.CA.verimatrix.multirights_widevine)) {
        this.commonService.MSAErrorLog(38052, 'Authenticate')
      }
    }
  }
}
