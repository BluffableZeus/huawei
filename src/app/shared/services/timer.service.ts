import * as _ from 'underscore'
import { Injectable, OnDestroy } from '@angular/core'
import { EventService } from 'ng-epg-sdk/services'
import { session } from 'src/app/shared/services/session'
import { queryCustomizeConfig } from 'ng-epg-sdk/vsp'
import { config as EPGConfig } from './config'
import { CommonService } from '../../core/common.service'
import { http } from 'ng-epg-sdk/sdk'
import { Logger } from 'ng-epg-sdk/utils'

const log = new Logger('timer.service')

@Injectable()
export class TimerService implements OnDestroy {
    // declare variables
  public goToTopDisplay = false
  private timer
  private scrollTop = 0
  private currentScrollTop = 0
  private mailto = 'mailto:'
  private agoScrollTop = 0

  constructor (
        private commonService: CommonService
    ) { }
    /**
     * when exit, clear all the timer
     */
  ngOnDestroy () {
    if (this.timer) {
      clearInterval(this.timer)
    }
  }
    /**
     * get customer config
     */
  getCustomConfig (isGuest?: any) {
    let queryType = '0,2,4,5,7'
    if (isGuest === 'guest') {
      queryType = '0,2,5,7'
    }
    if (!session.get('CUSTOM_CONFIG_DATAS')) {
      return queryCustomizeConfig({
        queryType: queryType
      }).then(resp => {
          session.put('CUSTOM_CONFIG_DATAS', resp, true)
          EventService.emit('CUSTOM_CACHE_CONFIG_DATAS')
          if (resp['ratings'] && resp['ratings'][0] && resp['ratings'][0]['ratings']) {
            let maxRating = _.max(resp['ratings'][0]['ratings'], item => {
              return Number(item['ID'])
            })
            if (maxRating) {
              session.put('PLAYBILLMAXRATING', maxRating['ID'])
            }
          }
          this.updateSdkCache(resp)
          return resp
        })
    }
  }

    /**
     * update the sdk cache policy.
     */
  updateSdkCache (resp) {
    if (resp['configurations']) {
      let config = this.filterCustomConfig(resp)
      if (config && config['values']) {
        let networkSdkCache = _.find(config['values'], item => {
            return item['key'] === 'network_interface_policy'
          })
        if (networkSdkCache && networkSdkCache['values'] && networkSdkCache['values'][0] && networkSdkCache['values'][0] !== '') {
            let policy = null
            try {
              policy = JSON.parse(networkSdkCache['values'][0])
            } catch (e) {
              log.error(e)
            }
            http.setCachePolicy(policy)
          }
      }
    }
  }

  getCaheCustomConfig () {
    let self = this
    let customConfigData = session.get('CUSTOM_CONFIG_DATAS')
    return new Promise((resolve, reject) => {
      if (customConfigData) {
        return resolve(session.get('CUSTOM_CONFIG_DATAS'))
      } else {
        if (Cookies.getJSON('IS_PROFILE_LOGIN')) {
            return resolve(self.getCustomConfig())
          } else {
            return resolve(self.getCustomConfig('guest'))
          }
      }
    })
  }
    /**
     * return to the top
     */
  backTop () {
    let self = this
    clearInterval(this.timer)
    self.agoScrollTop = 0
    this.timer = setInterval(function () {
      let now = self.scrollTop
      let speed = (0 - now) / 10
      speed = speed > 0 ? Math.ceil(speed) : Math.floor(speed)
      if (self.scrollTop === 0) {
        clearInterval(self.timer)
      }
      if (self.agoScrollTop === 0) {
        self.agoScrollTop = self.scrollTop
      } else if (self.agoScrollTop < self.scrollTop) {
          clearInterval(self.timer)
        } else if (self.agoScrollTop > self.scrollTop) {
          self.agoScrollTop = self.scrollTop
        }
      document.documentElement.scrollTop = self.scrollTop + speed
      document.body.scrollTop = self.scrollTop + speed
    }, 30)
  }

  onscroll (dv, dvTop) {
    let self = this
    self.scrollTop = Math.max(document.body.scrollTop || document.documentElement.scrollTop)
    let isBottom = document.body.scrollHeight - self.scrollTop - document.body.clientHeight
      // TODO: Think how this affects non-player pages
    if (location.pathname.indexOf('video') !== -1 || location.pathname.indexOf('movie') !== -1) {
      EventService.emit('SCROLL_SHOW_SUSPEND')
    }
    EventService.emit('HIDE_SEARCH_KEY')
    if (self.scrollTop > 0) {
      dv.background = 'rgba(0, 0, 0, 0.9)'
      if (self.scrollTop >= document.body.clientHeight * 2 && self.goToTopDisplay === false) {
        self.goToTopDisplay = true
        document.getElementById('common_goto_top')['style']['display'] = 'inline'
      }
      if (self.scrollTop < document.body.clientHeight * 2 && self.goToTopDisplay === true) {
        self.goToTopDisplay = false
        document.getElementById('common_goto_top')['style']['display'] = 'none'
      }
    } else if (self.scrollTop === 0) {
      dv.background = 'none'
      document.getElementById('common_goto_top')['style']['display'] = 'none'
    }
    if (isBottom <= 0) {
      EventService.emit('LOAD_DATA')
    }
  }

  disAppear () {
    if (this.scrollTop !== this.currentScrollTop) {
      this.currentScrollTop = this.scrollTop
    } else if (this.scrollTop === this.currentScrollTop && this.goToTopDisplay === true) {
      this.goToTopDisplay = false
      document.getElementById('common_goto_top')['style']['display'] = 'none'
    }
  }

  getSugess (): any {
    let self = this
    return this.getCaheCustomConfig().then(resp => {
      let configData = this.filterCustomConfig(resp)
      self.mailto = 'mailto:' + configData.values[0].values[0]
      session.put('SUGGESTION_EMAIL_TEST', self.mailto, true)
      return self.mailto
    })
  }

  upLoadRute (currentUrl, nextUrl) {
  }

  filterCustomConfig (resp) {
    let data
    if (resp['configurations']) {
      data = _.find(resp['configurations'], item => {
        return item['cfgType'] === '0'
      })
    }
    return data
  }

  getVSPCustomConfig (resp) {
    let data
    if (resp['configurations']) {
      data = _.find(resp['configurations'], item => {
        return item['cfgType'] === '2'
      })
    }
    return data
  }

  getRecordMessage () {
    this.getCaheCustomConfig().then(resp => {
      let configData = this.filterCustomConfig(resp)
      if (configData && configData['values']) {
        let values = configData['values']
        let default_reccfg_definition = _.find(values, item => {
            return this.trim(item['key']) === 'default_reccfg_definition'
          })
        let default_reccfg_pvr_type = _.find(values, item => {
            return this.trim(item['key']) === 'default_reccfg_pvr_type'
          })
        let default_reccfg_single_or_series = _.find(values, item => {
            return this.trim(item['key']) === 'default_reccfg_single_or_series'
          })
        let forgot_password_url = _.find(values, item => {
            return this.trim(item['key']) === 'forgot_password_url'
          })
        let ratingList = _.find(values, item => {
            return this.trim(item['key']) === 'rating_list'
          })
        if (ratingList) {
          this.getRatingList(ratingList['values'][0])
        }
        this.checkCustomConfig(default_reccfg_single_or_series, 38070)
        this.checkCustomConfig(default_reccfg_definition, 38071)
        this.checkCustomConfig(default_reccfg_pvr_type, 38072)
        this.checkCustomConfig(forgot_password_url, 38073)
        EPGConfig['default_reccfg_definition'] = default_reccfg_definition && default_reccfg_definition['values'] &&
                    default_reccfg_definition['values'][0]
        EPGConfig['default_reccfg_pvr_type'] = default_reccfg_pvr_type && default_reccfg_pvr_type['values'] &&
                    default_reccfg_pvr_type['values'][0]
        EPGConfig['default_reccfg_single_or_series'] = default_reccfg_single_or_series &&
                    default_reccfg_single_or_series['values'] && default_reccfg_single_or_series['values'][0]
        EPGConfig['forgot_password_url'] = forgot_password_url && forgot_password_url['values'] && forgot_password_url['values'][0]
      } else {
        EPGConfig['default_reccfg_single_or_series'] = 'Single'
        EPGConfig['default_reccfg_definition'] = 'HD'
        EPGConfig['default_reccfg_pvr_type'] = 'NPVR'
      }
        // handle EPGConfig data.
      let EPGConfigs = {}
      EPGConfigs['default_reccfg_definition'] = EPGConfig['default_reccfg_definition']
      EPGConfigs['BeginOffset'] = EPGConfig['BeginOffset']
      EPGConfigs['EndOffset'] = EPGConfig['EndOffset']
      EPGConfigs['Default_RecCfgDeleteMode'] = EPGConfig['Default_RecCfgDeleteMode']
      EPGConfigs['PVRMode'] = EPGConfig['PVRMode']
      EPGConfigs['default_reccfg_pvr_type'] = EPGConfig['default_reccfg_pvr_type']
      EPGConfigs['default_reccfg_single_or_series'] = EPGConfig['default_reccfg_single_or_series']
      EPGConfigs['allChannelCache'] = EPGConfig['allChannelCache']
      EPGConfigs['cPVRChannelCache'] = EPGConfig['cPVRChannelCache']
      EPGConfigs['forgot_password_url'] = ''
      EPGConfigs['supportAllCPVR'] = EPGConfig['supportAllCPVR']
      session.put('CONFIG_DATAS', EPGConfigs, true)
    })

    this.getCaheCustomConfig().then(resp => {
      let configData = this.filterCustomConfig(resp)
      if (configData && configData['values']) {
        let values = configData['values']
        let BeginOffset = _.find(values, item => {
            return item['key'] === 'BeginOffset'
          })
        let EndOffset = _.find(values, item => {
            return item['key'] === 'EndOffset'
          })
        this.checkCustomConfig(BeginOffset, 'BeginOffset')
        this.checkCustomConfig(EndOffset, 'EndOffset')
        EPGConfig['BeginOffset'] = BeginOffset && BeginOffset['values'] && BeginOffset['values'][0]
        EPGConfig['EndOffset'] = EndOffset && EndOffset['values'] && EndOffset['values'][0]
      } else {
        EPGConfig['BeginOffset'] = 0
        EPGConfig['EndOffset'] = 0
      }
        // handle EPGConfig data.
      let EPGConfigs = {}
      EPGConfigs['default_reccfg_definition'] = EPGConfig['default_reccfg_definition']
      EPGConfigs['BeginOffset'] = EPGConfig['BeginOffset']
      EPGConfigs['EndOffset'] = EPGConfig['EndOffset']
      EPGConfigs['Default_RecCfgDeleteMode'] = EPGConfig['Default_RecCfgDeleteMode']
      EPGConfigs['PVRMode'] = EPGConfig['PVRMode']
      EPGConfigs['default_reccfg_pvr_type'] = EPGConfig['default_reccfg_pvr_type']
      EPGConfigs['default_reccfg_single_or_series'] = EPGConfig['default_reccfg_single_or_series']
      EPGConfigs['allChannelCache'] = EPGConfig['allChannelCache']
      EPGConfigs['cPVRChannelCache'] = EPGConfig['cPVRChannelCache']
      EPGConfigs['forgot_password_url'] = ''
      EPGConfigs['supportAllCPVR'] = EPGConfig['supportAllCPVR']
      session.put('CONFIG_DATAS', EPGConfigs, true)
    })
  }
    /**
     * get Rating List data.
     */
  getRatingList (ratingList) {
    let ratingArray = ratingList.split(',')
    for (let i = 0; i < ratingArray.length; i++) {
      let ratingID = ratingArray[i].split(':')[0]
      let ratingName = ratingArray[i].split(':')[1]
      ratingArray[i] = {
        'ID': ratingID,
        'name': ratingName
      }
    }
    session.put('TERMINAL_RATING_LIST', ratingArray, true)
  }
    /**
     * check whether the following parameter is valid.
     * such as: default_reccfg_single_or_series, default_reccfg_definition, default_reccfg_pvr_type, forgot_password_url,
     *          BeginOffset, EndOffset.
     */
  checkCustomConfig (item, errorMsg: any) {
    if (_.isUndefined(item) || _.isUndefined(item['values']) || _.isUndefined(item['values'][0]) ||
            item['values'][0] === '' || item['values'][0] === null) {
      switch (typeof errorMsg) {
        case 'number':
          this.commonService.MSAErrorLog(errorMsg, 'QueryCustomizeConfig')
          break
        case 'string':
          this.commonService.commonLog('QueryCustomizeConfig', errorMsg)
      }
    }
  }
    /**
     * get current rate
     */
  getCurrencyRate () {
    this.getCaheCustomConfig().then(resps => {
      if (resps && resps['result']) {
        if (resps['configurations']) {
            let config0 = _.find(resps['configurations'], item => {
              return item['cfgType'] === '0'
            })
            if (_.isUndefined(config0) || this.hasNoCurrencySymbol(config0)) {
              this.commonService.MSAErrorLog(38067, 'QueryCustomizeConfig')
            }
          }
        if (_.isUndefined(resps['currencyAlphCode']) || resps['currencyAlphCode'] === '') {
            this.commonService.commonLog('QueryCustomizeConfig', 'currencyAlphCode')
          }
        EPGConfig['CURRENCY_RATE'] = resps['currencyRate']
        session.put('CURRENCY_RATE', EPGConfig['CURRENCY_RATE'], true)
        EPGConfig['CURRENCY_ALPH_CODE'] = resps['currencyAlphCode']
        session.put('CURRENCY_ALPH_CODE', EPGConfig['CURRENCY_ALPH_CODE'], true)
      }
    })
  }
    /**
     * check the currency_symbol whether exists.
     */
  hasNoCurrencySymbol (config) {
    if (_.isUndefined(config['values'])) {
      return true
    } else {
      let values = config['values']
      let currencySymbol = _.find(values, item => {
        return item['key'] === 'currency_symbol'
      })
      if (_.isUndefined(currencySymbol) || _.isUndefined(currencySymbol['values']) ||
                _.isUndefined(currencySymbol['values'][0]) || currencySymbol['values'][0] === '' || currencySymbol['values'][0] === null) {
        session.put('CURRENCY_SYMBOL', '', true)
        return true
      } else {
        session.put('CURRENCY_SYMBOL', currencySymbol['values'][0], true)
      }
    }
    return false
  }

    /**
     * dislodge the blank space of the head and the tail.
     */
  trim (str) {
    str = str.replace(/(^\s)|(\s*$)/g, '').replace(/(^\s*)/g, '').replace(/(\s*$)/g, '')
    return str
  }
    /**
     * deal with the password check rules, include:
     * 1.whether or not must contain lowercase.
     * 2.the min length of password.
     * 3.whether or not must contain digit.
     * 4.whether or not must contain special character.
     * 5.whether or not support blank space.
     * 6.whether or not must contain special uppercase.
     */
  dealPwdData (data) {
    let userPwdLowerCaseLetters = data['UserPwdLowerCaseLetters']
    let userPwdMinLength = data['UserPwdMinLength']
    let userPwdNumbers = data['UserPwdNumbers']
    let userPwdOthersLetters = data['UserPwdOthersLetters']
    let userPwdSupportSpace = data['UserPwdSupportSpace']
    let userPwdUpperCaseLetters = data['UserPwdUpperCaseLetters']
    this.checkPwdConfig(userPwdLowerCaseLetters, 'UserPwdLowerCaseLetters')
    this.checkPwdConfig(userPwdMinLength, 'UserPwdMinLength')
    this.checkPwdConfig(userPwdNumbers, 'UserPwdNumbers')
    this.checkPwdConfig(userPwdOthersLetters, 'UserPwdOthersLetters')
    this.checkPwdConfig(userPwdSupportSpace, 'UserPwdSupportSpace')
    this.checkPwdConfig(userPwdUpperCaseLetters, 'UserPwdUpperCaseLetters')
    let userPwd = {
      userPwdLowerCaseLetters: Number(userPwdLowerCaseLetters) === 1,
      userPwdMinLength: userPwdMinLength,
      userPwdNumbers: Number(userPwdNumbers) === 1,
      userPwdOthersLetters: userPwdOthersLetters,
      userPwdSupportSpace: Number(userPwdSupportSpace) === 1, // 1 support
      userPwdUpperCaseLetters: Number(userPwdUpperCaseLetters) === 1
    }
    return userPwd
  }
    /**
     * check whether or not the customized parameter has value.
     * If not, call common function to print log.
     */
  checkPwdConfig (item, type) {
    if (_.isUndefined(item) || item === '' || item === null) {
      this.commonService.MSAErrorLog(38041, 'Login', type)
    }
  }
    /**
     * deal with the device name check rules, include:
     * 1.whether or not support contain lowercase.
     * 2.the min length of password.
     * 3.whether or not support contain digit.
     * 4.whether or not support contain special character.
     * 5.whether or not support blank space.
     * 6.whether or not support contain special uppercase.
     */
  dealDeviceNameData (data) {
    let deviceNameLowerCaseLetters = _.find(data, item => {
      return item['key'] === 'DeviceNameLowerCaseLetters'
    })
    let deviceNameMinLength = _.find(data, item => {
      return item['key'] === 'DeviceNameMinLength'
    })
    let deviceNameMaxLength = _.find(data, item => {
      return item['key'] === 'DeviceNameMaxLength'
    })
    let deviceNameNumbers = _.find(data, item => {
      return this.trim(item['key']) === 'DeviceNameNumbers'
    })
    let deviceNameOthersLetters = _.find(data, item => {
      return this.trim(item['key']) === 'DeviceNameOthersLetters'
    })
    let deviceNameSupportSpace = _.find(data, item => {
      return this.trim(item['key']) === 'DeviceNameSupportSpace'
    })
    let deviceNameUpperCaseLetters = _.find(data, item => {
      return this.trim(item['key']) === 'DeviceNameUpperCaseLetters'
    })
    let deviceName = {
      deviceNameLowerCaseLetters: deviceNameLowerCaseLetters &&
            deviceNameLowerCaseLetters['values'] && Number(deviceNameLowerCaseLetters['values'][0]) === 1,
      deviceNameMinLength: deviceNameMinLength &&
            deviceNameMinLength['values'] && deviceNameMinLength['values'][0],
      deviceNameMaxLength: deviceNameMaxLength &&
            deviceNameMaxLength['values'] && deviceNameMaxLength['values'][0],
      deviceNameNumbers: deviceNameNumbers &&
            deviceNameNumbers['values'] && Number(deviceNameNumbers['values'][0]) === 1,
      deviceNameOthersLetters: deviceNameOthersLetters &&
            deviceNameOthersLetters['values'] && deviceNameOthersLetters['values'][0],
      deviceNameSupportSpace: deviceNameSupportSpace &&
            deviceNameSupportSpace['values'] && Number(deviceNameSupportSpace['values'][0]) === 1, // 1 support
      deviceNameUpperCaseLetters: deviceNameUpperCaseLetters &&
            deviceNameUpperCaseLetters['values'] && Number(deviceNameUpperCaseLetters['values'][0]) === 1
    }
    return deviceName
  }

    /**
     * get the blacklist to deal with other input scene.
     * if we has set the customized blacklist, get it direct.
     * if not, we get the default blacklist.the blacklist is as follows.
     * &<>"'()%+-
     */
  dealBlackList (data) {
    let blackList
    let ProfileNameOthersLetters = data['ProfileNameOthersLetters']
    if (!_.isUndefined(ProfileNameOthersLetters) && ProfileNameOthersLetters !== '') {
      blackList = ProfileNameOthersLetters
    } else {
      this.commonService.MSAErrorLog(38041, 'Login', 'ProfileNameOthersLetters')
      blackList = '&<>\"\'()%+-'
    }
    return blackList
  }
    /**
     * get the max length of input to deal with other input scene.
     * if we has set the customized max length, get it direct.
     * if not, we get the default max length.the max length is 128.
     */
  dealInputMaxLen (data) {
    let maxLength
    let ProfileNameMaxLength = data['ProfileNameMaxLength']
    if (!_.isUndefined(ProfileNameMaxLength) && ProfileNameMaxLength !== '') {
      maxLength = Number(ProfileNameMaxLength)
    } else {
      this.commonService.MSAErrorLog(38041, 'Login', 'ProfileNameMaxLength')
      maxLength = 128
    }
    return maxLength
  }

  RecPlaybillLen () {
    this.getCaheCustomConfig().then(resp => {
      let configData = this.getVSPCustomConfig(resp)
      let PlaybillLen = configData['PlaybillLen']
      let RecPlaybillLen = configData['RecPlaybillLen']
      if (PlaybillLen) {
        session.put('PLAYBILL_LEN', PlaybillLen, true)
      } else {
        this.commonService.commonLog('QueryCustomizeConfig', 'PlaybillLen')
      }
      if (RecPlaybillLen) {
        session.put('REC_PLAYBILL_LEN', RecPlaybillLen, true)
      } else {
        this.commonService.commonLog('QueryCustomizeConfig', 'RecPlaybillLen')
      }
      EventService.emit('CUSTOM_REFRESH_REC_PLAYBILLLENS')
      Cookies.set('CUSTOM_REC_PLAYBILLLENS', true)
    })
  }
    /**
     * get terminal parameter by call Login interface.
     * 1.get password complexity rules by terminal parameter.
     * 2.get blacklist of other input scene by terminal parameter.
     * 3.get max input length of other input scene by terminal parameter.
     */
  getTerminalParam () {
    let self = this
    let terminalParm = session.get('terminalParm')
    if (!_.isUndefined(terminalParm)) {
      session.put('USER_PWD_RULER_CACHE', self.dealPwdData(terminalParm), true)
      session.put('COMMON_INPUT_BLACKLIST_CACHE', self.dealBlackList(terminalParm), true)
      session.put('COMMON_INPUT_MAXLENGTH_CACHE', self.dealInputMaxLen(terminalParm), true)
    }
  }
    /**
     * call QueryCustomizeConfig interface to query customized parameter about input. Include:
     * 1.password check rules.
     * 2.profile name check rules.
     * 3.user ID check rules.
     * 4.search key check rules.
     * 5.device name check rules.
     */
  getPwdData () {
    let self = this
    this.getCaheCustomConfig().then(resp => {
      self.RecPlaybillLen()
      let configData = this.filterCustomConfig(resp)
      if (configData && configData['values']) {
        let data = configData['values']
        let namespace = _.find(data, item => {
            return item['key'] === 'ott_channel_name_space'
          })
        if (namespace) {
          session.put('ott_channel_name_space', namespace['values'][0])
        }
        if (data) {
          session.put('DEVICE_NAME_RULER_CACHE', self.dealDeviceNameData(data), true)
        }
      }
    })
  }
}
