import { Injectable } from '@angular/core'

@Injectable()
export class CountdownService {
  countdown = 0
  countdownMinutes = 5
  countdownInterval = null

  	constructor () { }

  	init (minutes?) {
  		this.countdownMinutes = minutes || this.countdownMinutes
  	}

  	get () {
  		return this.countdown
  	}

  readable = () => {
    const minutes = Math.floor(this.countdown / 60 / 1000),
      seconds = Math.floor((this.countdown - minutes * 60000) / 1000)

    return minutes + ':' + (seconds < 10 ? '0' + seconds : seconds)
  }

  loop (self) {
    self.countdown -= 1000

    if (self.countdown <= 0) {
      clearInterval(self.countdownInterval)
    }
  }

  start () {
    this.countdown = 1000 * 60 * this.countdownMinutes
    this.countdownInterval = setInterval(this.loop, 1000, this)
  }

  stop () {
    if (this.countdownInterval) {
      clearInterval(this.countdownInterval)
    }
  }
}
