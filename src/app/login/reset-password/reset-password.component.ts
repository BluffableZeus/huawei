import * as _ from 'underscore'

import { Component, OnInit } from '@angular/core'
import { DialogRef, DialogParams } from 'ng-epg-ui/webtv-components/dialog'
import { TranslateService } from '@ngx-translate/core'
import { EventService } from 'ng-epg-sdk/services'
import { PopupErrorsService } from '../../shared/services/popup.errors'
import { CountdownService } from '../../shared/services/countdown'
import { ValidationService } from '../../shared/services/validation'
import { AccountLoginComponent } from '../account-login'
import { CommonService } from '../../core/common.service'
import { session } from 'src/app/shared/services/session'

import { userAPI } from 'oper-center/api/user'
declare const grecaptcha: any

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})

export class ResetPasswordComponent implements OnInit {
    /**
     * declare variables.
     */
  btnClass = 'btn btn-fluent disabled'

  tempLoginName = ''
  userID = ''
  captchaResponse = ''
  captchaID = ''

  deleteLoginName = false
  showplaceholderId = true

  emailSent = false
  sending = false

    /**
     * private constructor
     */
  constructor (
        private dialog: DialogRef,
        private liveTvParams: DialogParams,
        private translate: TranslateService,
        private commonService: CommonService,
        private userapi: userAPI,
        public errors: PopupErrorsService,
        public countdown: CountdownService,
        private validate: ValidationService
    ) {
    let self = this
      // Listening to logging out,close the Register dialog.
    EventService.on('CLOSED_RESETPASS_DIALOG', function () {
      self.close()
    })

    errors.clear()
    errors.registerField('login')

    countdown.init()
  }
    /**
    * initialization
    */
  ngOnInit () {
      // Listening other browser backBtn to close Register dialog
    window.addEventListener('popstate', this.browserHashchange)
    this.recaptchaInit()
  }

    /**
     * Listening browser backBtn
     */
  browserHashchange (e) {
    EventService.emit('CLOSED_REGISTER_DIALOG')
  }

    /**
     * close the dialog of Register
     */
  close () {
    this.countdown.stop()
    this.dialog.close()
  }

    /**
     * change button of register when input userName and input loginName
     */
  public btnStyleChange (): boolean {
    if (this.emailSent) {
      return true
    }

    return (this.userID !== '') &&
            !this.errors.getField('login') &&
            !this.sending &&
            this.validate.login(this.userID)
  }
    /**
     * hidden the clues of userID,and the userID box gets focus.
     */
  onplaceholdId () {
    if (this.userID === '' || this.userID === undefined) {
      _.delay(() => {
        document.getElementById('login-ID-reset').focus()
      }, 100)
    }
  }
    /**
     * click the loginID list to choose one loginID
     */
  setLoginID (message) {
    this.userID = message
    this.showplaceholderId = false // hidden the clues of userID
  }
    /**
     * when the loginID dialog focused,save the input.
     */
  loginNameFocus (e) {
    this.tempLoginName = e.target.value
  }

  loginNameBlur (e) {
    if (e.target.value === '') {
      this.deleteLoginName = false
      this.showplaceholderId = true
    }

    this.validateLogin(false)
  }
    /**
     * changeLoginName on input your userName
     */
  changeLoginName (e) {
    const msg = 'wrong_char'
    this.errors.clear()

    if (this.validate.hasRU(e.target.value)) {
      this.errors.on('login')
      this.errors.add(msg)
    } else {
      this.errors.off('login')
      this.errors.remove(msg)
    }

    this.showplaceholderId = false

    if ((this.userID !== '') || (e.target.value !== '')) {
      if (e.keyCode === 13) {
        document.getElementById('logBtn').click()
      }
      this.showplaceholderId = false
      this.deleteLoginName = true
    } else {
      this.deleteLoginName = false
      this.showplaceholderId = true
    }
  }
    /**
     * clear the userName of input
     */
  removeLoginName () {
    this.errors.off('login')
    this.errors.remove('wrong_char')
    this.errors.remove('not_correct_login')

    this.userID = ''
    document.getElementById('login-ID-reset').focus()
  }
    /**
     * Listening to when the key has been pressed
     */
  keydownLogin (e) {
    this.errors.setField('login', /[а-яА-ЯёЁ]/.test(e.target.value + e.key))
    this.errors.toggle('wrong_char', this.errors.getField('login'))
    this.showplaceholderId = false
  }

  validateLogin (checkEmpty: boolean) {
    return this.validate.byRules({
      value: this.userID,
      errors: this.errors,
      field: 'login',
      result: 'login',
      should: {
        hasNoRU: {
            msg: 'wrong_char'
          },
        notEmpty: {
            ext: !checkEmpty,
            msg: 'field_cannot_be_empty'
          },
        login: {
            ext: (this.userID === ''),
            msg: 'not_correct_login'
          }
      }
    })
  }

  sendConfirmationEmail () {
    this.userapi.resetPassword({
      login: this.userID,
      gRecaptchaResponse: this.captchaResponse,
      success: this.emailSendSuccess.bind(this),
      error: this.emailSendError.bind(this)
    })
  }

  emailSendSuccess () {
    this.lockForm()
  }

  emailSendError (message) {
    this.errors.add(message)
    this.sending = false
  }

  lockForm () {
    this.emailSent = true
    this.countdown.start()
  }

  recaptchaCallback (response) {
    this.captchaResponse = response
    this.sendConfirmationEmail()
  }

  recaptchaInit () {
    const container = document.getElementById('recaptcha-c-reset'),
      params = {
        sitekey: '6Le3kUwUAAAAAJ0KnDEisXv_L9xOlTVjGFPjHGO8',
        callback: this.recaptchaCallback.bind(this),
        size: 'invisible'
      }
    this.captchaID = grecaptcha.render(container, params)
  }

  recaptcha () {
    if (document.getElementById('recaptcha-c-reset').innerHTML !== '') {
      grecaptcha.reset(this.captchaID)
    }

    grecaptcha.execute(this.captchaID)
  }

  getTopOffset () {
    if (this.emailSent) return 45

    const errs = document.querySelector('app-reset-password .error_message'),
      infos = document.querySelector('app-reset-password.info_message'),
      errH = errs ? errs.clientHeight : 0,
      infoH = infos ? infos.clientHeight : 0

    return Math.max(20, errH + infoH - 15)
  }

  resendEmail () {
    this.emailSent = false
    this.onRecover()
  }

  onRecover () {
    this.errors.off('login')
    this.deleteLoginName = false
    this.errors.clear()

    if (this.emailSent) {
      session.put('LOGIN_NAME_LIST', [{ name: this.userID }])
      this.close()
      this.commonService.openDialog(AccountLoginComponent)
    } else {
      if (this.validateLogin(true)) {
        this.sending = true
        this.recaptcha()
      } else {
        this.deleteLoginName = true
      }
    }
  }
}
