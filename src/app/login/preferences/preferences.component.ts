import * as _ from 'underscore'
import { Component, OnInit } from '@angular/core'
import { queryRecmContent, QueryRecmContentRequest, UserPreference } from 'ng-epg-sdk/vsp'
import { TranslateService } from '@ngx-translate/core'
import { DialogRef } from 'ng-epg-ui/webtv-components/dialog'
import { Router } from '@angular/router'
import { PreferenceDeactivate } from './preferences-deactivate.service'
import { EventService } from 'ng-epg-sdk/services'
import { session } from 'src/app/shared/services/session'

interface ButtonInformation {
  id?: string
  btnName?: string
  onClick?: Function
  enableFocus?: boolean
}

@Component({
  selector: 'app-preferences-home',
  templateUrl: './preferences.component.html',
  styleUrls: ['./preferences.component.scss']
})

export class PreferencesHomeComponent implements OnInit {
    /**
     * declare variables.
     */
  public recmList: Array<any>
    // show initial current page
  public currentPage = 1
    // show current totalPage of initial
  public totalPage = 3
    // define el as HTMLElement
  public el: HTMLElement
    // define focusPoster as HTMLElement
  public focusPoster: HTMLElement
    // define buttons contain attribute and method
  public buttons: Array<ButtonInformation> = []

  public userPreference: UserPreference
    // define preferContentIDs to contain like id
  public preferContentIDs: Array<string> = []

  public preferSubTitle: string
    // variables to control like
  public isPaging = false
    // variables to control subtitle
  public isExistRecms = true

  public emptyMsgList: Array<string>

  public hasSendFlag = false
    // a method to control Next
  public selectNext = true
    // if true,there is a preference setting for home.if false,there is a preference setting for my TV.
  public isExit = true
    /**
     * define a method of interface
     */
  public recmContentsReq: QueryRecmContentRequest = {
    queryDynamicRecmContent: {
      recmScenarios: [{
        contentType: 'VOD',
        businessType: 'VOD',
        recmType: '4',
        count: '12',
        offset: '0',
        userPreference: {}
      }]
    }
  }

  public showPreferSubmit = false
  private preferCount = 1
  private panel = false
  public isSelected = -1
  public hasRecm = false
  public hasNoRecm = false

  infoKey = 'do_you_want_submit_preference_setting '
    /**
     * set delay method
     */
  delayFilter = _.debounce((fc: Function) => {
    fc()
  }, 300, true)
    /**
     * name for modules that would be used in this module
     */
  constructor (
        private dialog: DialogRef,
        private router: Router,
        private translate: TranslateService,
        private preferenceDeactivate: PreferenceDeactivate) {
    if (location.pathname.indexOf('profile/preferences') !== -1) {
      this.isExit = false
    }
  }
    /**
    * initialization
    */
  ngOnInit (): void {
    this.isSmallScreen()
    this.initRecmData()
    EventService.on('POP_UP_PREFERENCES_SUBMIT', () => {
      if (this.isExit === false) {
        this.showPreferSubmit = true
      }
    })
  }
    /**
    * call a interface
    * deal with the data come from the interface
    * get the data from queryRecmContent
    */
  initRecmData (): void {
    this.currentPage = 1
    queryRecmContent(this.recmContentsReq).then((resp) => {
      let respRecms
      if (resp.recmContents && !_.isEmpty(resp.recmContents) && !_.isEmpty(resp.recmContents[0].recmVODs)) {
        this.hasRecm = true
        this.hasNoRecm = false
        respRecms = resp.recmContents[0].recmVODs
        this.preferSubTitle = 'select_one_continute'
        this.isExistRecms = true
        this.recmList = this.getRecmList(resp.recmContents[0].recmVODs)
        this.isPaging = true
        if (location.pathname.indexOf('profile/preferences') !== -1 &&
                document.getElementsByClassName('app-preferences-home')[0] &&
                document.getElementsByClassName('app-preferences-home')[0]['style']) {
            document.getElementsByClassName('app-preferences-home')[0]['style']['background'] = 'none'
          }
        this.buttons = [
            {
              id: 'next_step',
              btnName: 'next_step',
              // define a method of onClick
              onClick: () => this.nextStep(),
              enableFocus: false
            }]
      } else {
        this.hasRecm = false
        this.hasNoRecm = true
      }
    }, (resp) => {
    })
  }
    /**
     * traverse list to get recmList
     */
  getRecmList (list: Array<any>): Array<any> {
    let recmList = _.map(list, (ele, index) => {
      let pic
      if (!_.isEmpty(ele.picture) && !_.isEmpty(ele.picture.posters)) {
        pic = ele.picture.posters[0]
      } else {
        pic = ''
      }
      return { dataType: 'common_vod', id: ele.ID, name: ele.name, posters: pic, selected: false }
    })
    return recmList
  }
    /**
     * click button of Next.
     */
  nextStep (): void {
    _.each(this.recmList, (ele, index) => {
      if (ele.selected) {
        this.preferContentIDs.push(ele.id)
        this.selectNext = false
      }
    })
    let recm = this.recmContentsReq.queryDynamicRecmContent['recmScenarios'][0]
    let req = recm.userPreference
    req.preferContentIDs = this.preferContentIDs
    if (this.selectNext) {
      return
    }
    if (this.hasSendFlag) {
      return
    }
    this.hasSendFlag = true
      // call interface
    queryRecmContent(this.recmContentsReq).then((resp) => {
      this.recmList = this.getRecmList(resp.recmContents[0].recmVODs)
      this.currentPage++
      this.hasSendFlag = false
      this.selectNext = true
        // switch the next title.
      this.preferSubTitle = 'prefer_subtitle_next'
      if (this.currentPage === this.totalPage - 1) {
        this.buttons = this.buttons.splice(0, 1)
        this.buttons[0].enableFocus = false
      } else if (this.currentPage === this.totalPage) {
          this.buttons = [{
            id: 'reday_enjoy',
            btnName: 'start_test',
            // define a method of get filter content
            onClick: () => { this.goHomePage() },
            enableFocus: false
          }]
        }
      this.isPaging = true
      this.isSelected = -1
      this.checkInfoChanged()
    }, () => {
    })
  }
    /**
     * provide a method to return fliter data
     */
  goHomePage (): void {
    this.delayFilter(() => {
      this.goHomePageAfterFilter()
    })
  }
    /**
     * get content of filter
     */
  goHomePageAfterFilter () {
    _.each(this.recmList, (ele, index) => {
      if (ele.selected) {
        this.preferContentIDs.push(ele.id)
        this.selectNext = false
      }
    })
    if (this.selectNext) {
      return
    }
    let recm = this.recmContentsReq.queryDynamicRecmContent['recmScenarios'][0]
    let req = recm.userPreference
    req.preferContentIDs = this.preferContentIDs
    queryRecmContent(this.recmContentsReq).then((resp) => {
      this.preferenceDeactivate.isPreferenceChanged = false
      this.selectNext = true
      this.initRecmData()
      EventService.emit('preference_setting_successfully')
      if (this.isExit) {
        this.dialog.close()
      }
    }, () => {
      this.preferenceDeactivate.isPreferenceChanged = false
      this.initRecmData()
      EventService.emit('preference_setting_successfully')
      if (this.isExit) {
        this.dialog.close()
      }
    })
  }
    /**
     * selecte current item.
     */
  selectItem (item): void {
    this.isPaging = false
    item.selected = !item.selected
    this.isSelected = _.findIndex(this.recmList, { selected: true })
    if (this.currentPage === (this.totalPage - 2)) {
      this.buttons[0].enableFocus = this.isSelected >= 0
    } else if (this.currentPage === (this.totalPage - 1) || this.currentPage === this.totalPage) {
      this.buttons[0].enableFocus = this.isSelected >= 0
    }
    this.checkInfoChanged()
  }
    /**
     * call a interface of DialogRef
     * Closes the dialog.
     */
  close () {
    this.dialog.close()
  }
    /**
     * judge like
     */
  checkInfoChanged () {
    if (this.isSelected !== -1) {
      this.preferenceDeactivate.isPreferenceChanged = true
    } else {
      this.preferenceDeactivate.isPreferenceChanged = false
    }
  }
    /**
     * receive childComponent message.
     */
  closeSubDialog ($event) {
    this.showPreferSubmit = false
    EventService.emit('PREFERENCES_FOCUS')
  }
    /**
     * receive childComponent message.
     */
  cancelOperation ($event) {
    let self = this
    this.showPreferSubmit = false
    this.preferenceDeactivate.isPreferenceChanged = false
    if (session.get('PROFILE_NOTSAVE_LOGOUT_TYPE')) {
      _.delay(() => {
        EventService.emit('PROFILE_NOTSAVE_LOGOUT')
        self.router.navigate(['' + session.get('RouteMessage')])
      }, 1000)
    } else {
      this.router.navigate(['' + session.get('RouteMessage')])
    }
  }
    /**
     * receive childComponent message.
     */
  submitData ($event) {
    let self = this
    this.showPreferSubmit = false
    this.preferenceDeactivate.isPreferenceChanged = false
    this.nextStep()
    if (session.get('PROFILE_NOTSAVE_LOGOUT_TYPE')) {
      _.delay(() => {
        EventService.emit('PROFILE_NOTSAVE_LOGOUT')
        self.router.navigate(['' + session.get('RouteMessage')])
      }, 1000)
    } else {
      this.router.navigate(['' + session.get('RouteMessage')])
    }
  }
    /**
     * click leftIcon to show left content.
     */
  loadLeftPrefer () {
    let currentDom = document.getElementsByClassName('boxlist')[0]
    let onceWidth = -184 * 4 * (this.preferCount - 2) + 'px'
    if (this.preferCount > 1) {
      document.getElementsByClassName('prefer-rightIcon')[0]['style']['display'] = 'block'
      currentDom['style']['transform'] = 'translateX(' + onceWidth + ')'
      currentDom['style']['transition'] = 'all 1s linear'
      this.preferCount--
      if (this.preferCount === (this.totalPage - 2)) {
        document.getElementsByClassName('prefer-leftIcon')[0]['style']['display'] = 'none'
      }
    }
  }
    /**
     * click rightIcon to show rigth content.
     */
  loadRightPrefer () {
    let currentDom = document.getElementsByClassName('boxlist')[0]
    let onceWidth = -(184 * 4 * this.preferCount) + 'px'
    if (this.preferCount < 3) {
      document.getElementsByClassName('prefer-leftIcon')[0]['style']['display'] = 'block'
      currentDom['style']['transform'] = 'translateX(' + onceWidth + ')'
      currentDom['style']['transition'] = 'all 1s linear'
      this.preferCount++
      if (this.preferCount === this.totalPage) {
        document.getElementsByClassName('prefer-rightIcon')[0]['style']['display'] = 'none'
      }
    }
  }
    /**
     * a method to return dom element
     */
  dom (divName: string) {
    return document.getElementById(divName)
  }
    /**
     * judge screenSize.
     */
  isSmallScreen () {
    let clientW: number = window.innerWidth
    if (clientW > 1440) {
      this.panel = false
    } else {
      this.panel = true
    }
  }
}
