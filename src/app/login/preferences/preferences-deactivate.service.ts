import { Injectable } from '@angular/core'
import { CanDeactivate } from '@angular/router'
import { Observable } from 'rxjs'
import { EventService } from 'ng-epg-sdk/services'
import { session } from 'src/app/shared/services/session'

export interface CanComponentDeactivate {
  canDeactivate: () => Observable<boolean> | Promise<boolean> | boolean
}
/**
 * handling unsaved changes
 */
@Injectable()
export class PreferenceDeactivate implements CanDeactivate<CanComponentDeactivate> {
  public isPreferenceChanged = false
  public currentUrl = ''
    /**
     * make RouteMessage put in session
     */
  getRouteMessage (e) {
    if (e['srcElement']['hash']) {
      let hash = e['srcElement']['hash'] + ''
      this.currentUrl = hash.split('#')[1] || hash
      session.put('RouteMessage', this.currentUrl)
    }
  }
    /**
     * set CanDeactivate guard
     */
  canDeactivate (component: CanComponentDeactivate) {
    if (this.isPreferenceChanged) {
      document.onclick = this.getRouteMessage
      EventService.emit('POP_UP_PREFERENCES_SUBMIT')
      return false
    } else {
      return true
    }
  }
}
