import * as _ from 'underscore'
import { Component, OnInit } from '@angular/core'
import { session } from 'src/app/shared/services/session'
import { replaceDevice, modifyDeviceInfo } from 'ng-epg-sdk/vsp'
import { EventService, HeartbeatService } from 'ng-epg-sdk/services'
import { DialogRef, DialogParams } from 'ng-epg-ui/webtv-components/dialog'
import { AuthService } from '../../shared/services/auth.service'
import { Router } from '@angular/router'
import { CommonService } from '../../core/common.service'

@Component({
  selector: 'app-device-replace',
  templateUrl: './device-replace.component.html',
  styleUrls: ['./device-replace.component.scss']
})

export class DeviceReplaceComponent implements OnInit {
    // declare variables.
  private loginProfile
    /**
     * private constructor
     */
  constructor (
        private dialog: DialogRef,
        private authService: AuthService,
        private router: Router,
        private repalceData: DialogParams,
        private heartbeatService: HeartbeatService,
        private commonService: CommonService
    ) {
  }
  ngOnInit () {
  }
    /**
     * close the dialog for replacing the device,
     * return the dialog of device list
     */
  close () {
      // close dialog of subscriber
    this.dialog.close()
    let data: any = {
      subscriberID: this.repalceData['subscriberID'],
      userID: this.repalceData['userID'],
      password: this.repalceData['password'],
      devices: this.repalceData['devices']
    }
      // send event and deal with data
    EventService.emit('DEVICELIST_OPEN', data)
  }
    /**
     * run replacing the device.
     * if replacing the device successfully,you should authenticate again.
     */
  doReplaceDevice () {
    let orgDevice = this.repalceData['selectDevice']
    let subscriberId = this.repalceData['subscriberID']
      // call a interface of replaceDevice
    replaceDevice({
      subscriberID: subscriberId,
      orgDeviceID: orgDevice['id'],
      destDeviceID: this.authService.getCookie('uuid')
    }).then((resp) => {
      if (resp.result.retCode === '0' || resp.result.retCode === '000000000') {
          this.authenticate(this.repalceData['userID'], this.repalceData['password'])
        }
    })
  }
    /**
     * login authentication
     */
  authenticate (userID?: string, password?: string): any {
    let self = this
      // deal with userID
    userID = userID.trim()
    session.put('INIT_CHECK_PASSWORD', password)
    let req = {
      authenticateBasic: {
        userID: userID,
        userType: '1'
      },
      password: password
    }
      // call a interface of authenticate
    return this.authService.authenticate(req).then(resp => {
        // if the user login firstly,there is a message abount resetting the password.
      if (resp && resp.result && resp.result.retCode === '157021017') {
        EventService.on('CONFIRM_CHECK_INIT_PWD', function () {
            self.accountIsLogin(resp, userID, password)
          })
      } else {
        self.accountIsLogin(resp, userID, password)
      }
    }, resp => {
      return Promise.reject(resp)
    })
  }
    /**
     * deal with datas after logining successfully.
     */
  accountIsLogin (resp, userID, password) {
    let self = this
    EventService.emit('LOGININTRUE', false)
      /**
        * Disabled for bug-1849
         * open the heartbeat
         */
//    this.heartbeatService.startHeartbeat().then(response => {
//      if (_.isUndefined(response['personalDataVersions']) || _.isUndefined(response['personalDataVersions']['channel']) ||
//                response['personalDataVersions']['channel'] === '') {
//          // print MSAErrorLog
//        self.commonService.MSAErrorLog(38054, 'OnLineHeartbeat')
//      }
//    })
    let curProfile = _.find(resp.profiles, item => {
      return item['loginName'] === userID
    })
    this.commonService.resetSessionProfile(curProfile)
      // save datas of session
    session.put('modify_profileID', resp.profileID, true)
    session.remove('INIT_CHECK_PASSWORD')
    Cookies.set('PROFILE_TYPE', curProfile['profileType'])
    session.put('LOGIN_NAME', curProfile['loginName'], true)
    session.put('PROFILERATING_ID', curProfile['ratingID'])
    session.put('SUBSCRIBER_ID', resp.subscriberID)
    Cookies.set('USER_ID', userID)
    session.put('deviceID', resp.deviceID)
    if (resp && resp.userFilter) {
      session.put('USER_FILTER', resp.userFilter, true)
    }
      // if the device name is undefinded or empty,you should give a default name for the device.
    if (!resp.deviceName || resp.deviceName === '') {
      this.checkDeviceID(resp)
      let adminProfile = _.find(resp.profiles, item => {
        return Number(item['profileType']) === 0
      })
      let device: any = {
        ID: resp.deviceID,
        name: 'PC' + adminProfile['name'],
        isSupportPVR: '1',
        deviceType: '1'
      }
      session.put('IS_MANUAL_SET_DEVICE_NAME', false)
        // call a interface of modifyDeviceInfo
      modifyDeviceInfo({
        device: device
      }).then(() => {
          session.put('deviceName', device.name)
        })
    } else {
      session.put('deviceName', resp.deviceName)
    }
    this.loginProfile = _.filter(resp.profiles, (profile) => {
      return resp.profileID === profile['ID']
    })
    Cookies.set('LOGIN_PROFILE_LOGOURL', this.loginProfile[0].logoURL)
    Cookies.set('PROFILE_NAME', this.loginProfile[0].name)
    session.put('PROFILE_ID', resp.profileID)
    EventService.emit('LOGIN')
      // if logins when playing the program,the user should authenticate to play again after logining successfully.
    if (this.repalceData['liveTvParams']['guest'] === 'Guest') {
      session.put('LOGIN_LIVETV_PLAYING', true)
      EventService.emit('GUEST_TO_LOGIN_LIVETV_PLAYING', this.repalceData['liveTvParams'])
    }
    this.dialog.close()
  }
    /**
     * judge deviceID
     */
  checkDeviceID (resp) {
    if (_.isUndefined(resp.deviceID) || resp.deviceID === '') {
      this.commonService.commonLog('Authenticate', 'deviceID')
    }
  }
}
