import { isUndefined, delay, find, uniq } from 'underscore'
import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { modifyDeviceInfo } from 'ng-epg-sdk/vsp'
import { EventService } from 'ng-epg-sdk/services'
import { session } from 'src/app/shared/services/session'
import { DialogRef, DialogParams } from 'ng-epg-ui/webtv-components/dialog'
//import { DeviceListComponent } from '../device-list'
import { CommonService } from '../../core/common.service'
import { AuthService } from '../../shared/services/auth.service'
import { TranslateService } from '@ngx-translate/core'
import { UserLoginHistoryInfo } from './userLoginHistoryInfo.service'
import { ChannelCacheService } from '../../shared/services/channel-cache.service'
import { AccountRegisterComponent } from '../account-register'
import { ResetPasswordComponent } from '../reset-password'
import { socialAPI } from 'oper-center/api/social'

const pageTracker = require('../../../assets/lib/PageTracker')

@Component({
  selector: 'app-account-login',
  templateUrl: './account-login.component.html',
  styleUrls: ['./account-login.component.scss']
})

export class AccountLoginComponent implements OnInit {
    /**
     * declare variables.
     */
  visibility = 'hidden'
  isFullFiled = false
  btnClass = 'btn btn-fluent disabled'
  userID = ''
  userPassword = ''
  tempLoginName = ''
  checkedTbBg = 'none'
  checkedPrivacyBg = 'url(' + 'assets/img/16/select_16.png' + ') 0px 0px no-repeat'
  loginElect = true
  disagreePrivacy = false
  loginProfile = {}
  loginLoadDisplay = false
  errorMessage = false
  loginNameDisplay = false
  loginNameList: Array<string>
  deleteLoginName = false
  deletePassWord = false
  showplaceholder = true
  showplaceholderId: boolean
  public iStyle = false
  public engAgree = 'i_agree'
  public engAgreeChange = false
  registration = ''

  checkRouler = {}
  pwdOrLoginNameErrorMessage = 'loginname_or_password_is_incorrect_try_again'
  configData = {}
  forgetpassword = false
  forgot_password_url = ''
    /**
     * private constructor
     */
  constructor (
        private authService: AuthService,
        private router: Router,
        private dialog: DialogRef,
        private commonService: CommonService,
        private liveTvParams: DialogParams,
        private translate: TranslateService,
        private channelCacheService: ChannelCacheService,
        private userLoginHistoryInfo: UserLoginHistoryInfo,
        private socialApi: socialAPI
    ) {
    let loginNameArr = session.get('LOGIN_NAME_LIST') // Access to the loginName list.
    this.loginNameList = loginNameArr && loginNameArr.slice(0, 4) || [] // if the loginName list existed, only take four loginName.
      // save the loginName list.
    session.put('LOGIN_NAME_LIST', this.loginNameList)
    this.userID = loginNameArr && loginNameArr.length > 0 ? loginNameArr[0]['name'] : ''
    this.loginElect = loginNameArr && loginNameArr.length > 0 ? loginNameArr[0]['loginElect'] : false
    if (this.loginElect) {
      this.checkedTbBg = 'url(' + 'assets/img/16/select_16.png' + ') 0px 0px no-repeat'
    }
      // if the userID is empty,show the clues by default.
    if (this.userID === '') {
      this.showplaceholderId = true
    } else {
      delay(() => {
        document.getElementById('pass-Word').focus()
      }, 1000)
    }
      // Listening to logging out,close the login dialog.
    EventService.on('CLOSED_LOGIN_DIALOG', () => {
      this.close()
    })
    EventService.removeAllListeners(['AUTH_SUCCESS_CLOSE_LOGIN_DIALOG'])
    EventService.on('AUTH_SUCCESS_CLOSE_LOGIN_DIALOG', () => {
      this.loginLoadDisplay = false
      this.close()
    })
    delay(() => {
      if (document.getElementById('login-ID') && this.loginNameList.length === 0) {
        document.getElementById('login-ID').focus()
        if (session.get('IS_PROFILE_LOGIN_SCROLL_TOP')) {
            session.remove('IS_PROFILE_LOGIN_SCROLL_TOP')
            document.body.scrollTop = 0
            document.documentElement.scrollTop = 0
          }
      }
    }, 500)
    this.checkRouler = session.get('USER_PWD_RULER_CACHE')
    this.configData = session.get('CONFIG_DATAS') || {} // Access to the config datas.
      // show the forgot password,and save the forgot password url
      // TODO as for now we don't have a propper forgot_password_url, change it after get the right forgot_password_url
    this.forgetpassword = true
    this.forgot_password_url = ''
  }
    /**
    * initialization
    */
  ngOnInit () {
    this.engAgree = this.translate.instant('i_agree')
      // language of English set space
    if (this.engAgree === 'Agree ') {
      this.engAgreeChange = true
    }

    EventService.on('EXTERNAL_AUTH', auth => {
      EventService.emit('EXTERNAL_AUTH_INIT')
      this.authenticate(auth.login, auth.password)
    })

    EventService.removeAllListeners(['SHOW_AUTH_ERROR_MESSAGE'])
    EventService.on('SHOW_AUTH_ERROR_MESSAGE', (message) => {
      this.pwdOrLoginNameErrorMessage = message
      this.errorMessage = true
    })
      // Listening other browser backBtn to close login dialog
    window.addEventListener('popstate', this.browserHashchange)
  }
    /**
     * Listening browser backBtn
     */
  browserHashchange (e) {
    EventService.emit('CLOSED_LOGIN_DIALOG')
  }
    /**
     * hidden the clues of userPassword,and the password box gets focus.
     */
  onplacehold () {
    if (this.userPassword === '' || this.userPassword === undefined) {
      delay(() => {
        document.getElementById('pass-Word').focus()
      }, 100)
    }
  }
    /**
     * hidden the clues of userID,and the userID box gets focus.
     */
  onplaceholdId () {
    if (this.userID === '' || this.userID === undefined) {
      delay(() => {
        document.getElementById('login-ID').focus()
      }, 100)
    }
  }
    /**
     * click the loginID list to choose one loginID
     */
  setLoginID (message) {
    this.userID = message
    this.showplaceholderId = false // hidden the clues of userID
  }
    /**
     * when the loginID dialog focused,save the input.
     */
  loginNameFocus (e) {
    this.tempLoginName = e.target.value
  }
    /**
     * changeLoginName on input your userName
     */
  changeLoginName (e) {
    this.errorMessage = false
    this.showplaceholderId = false
    if (e.target.value !== this.tempLoginName && this.userPassword !== '') {
      this.userPassword = ''
      this.showplaceholder = true
    }
    if ((this.userID !== '') || (e.target.value !== '')) {
      if (e.keyCode === 13) {
        document.getElementById('login-ID').blur()
        document.getElementById('pass-Word').focus()
      }
      this.showplaceholderId = false
      this.deleteLoginName = true
    } else {
      this.deleteLoginName = false
      this.showplaceholderId = true
    }
  }
    /**
     * input your password
     */
  changePassword (e) {
    this.errorMessage = false
    if (this.userPassword !== '') {
      if (e.keyCode === 13) {
        document.getElementById('logBtn').click()
      }
      this.showplaceholder = false
      this.deletePassWord = true
    } else {
      this.deletePassWord = false
      this.showplaceholder = true
    }
  }
    /**
     * show historical record of loginName
     */
  showLoginNameList () {
    if (this.loginNameList && this.loginNameList.length > 0) {
      this.loginNameDisplay = true
      this.showplaceholderId = false
    }
    if (this.userID !== '') {
      this.deleteLoginName = true
      this.showplaceholderId = false
    }
    if (this.userID === '') {
      this.deleteLoginName = false
      this.showplaceholderId = true
    }
  }
    /**
     * hide historical record of loginName
     */
  hideLoginNameList () {
    if (this.userID === '') {
      this.showplaceholderId = true
    }
    delay(() => {
      this.deleteLoginName = false
      this.loginNameDisplay = false
    }, 300)
  }
    /**
     * show historical record of passwords
     */
  showPassword () {
    if (this.userPassword !== '') {
      this.deletePassWord = true
      this.showplaceholder = false
    }
    if (this.userPassword === '') {
      this.showplaceholder = true
    }
  }
    /**
     * hide historical record of passwords
     */
  hidePassword () {
    if (this.userPassword === '') {
      this.showplaceholder = true
    }
    delay(() => {
      this.deletePassWord = false
    }, 300)
  }
    /**
     * clear the userName of input
     */
  removeLoginName () {
    this.errorMessage = false
    this.userID = ''
    if (this.userPassword !== '') {
      this.userPassword = ''
      this.showplaceholder = true
    }
    document.getElementById('login-ID').focus()
  }
    /**
     * clear the password of input
     */
  removePassWord () {
    this.errorMessage = false
    this.userPassword = ''
    document.getElementById('pass-Word').focus()
  }

    /**
     * show the login names which have logined before
     */
  putLoginNameList (name) {
    let profileName = {}
    profileName['name'] = name
    profileName['loginElect'] = this.loginElect
    let list = session.get('LOGIN_NAME_LIST') ? session.get('LOGIN_NAME_LIST') : []
    if (this.loginElect) { // if you choose 'remember me' to login,this userID will been first.
      let arrlist = list.filter(profile => profile['name'] !== profileName['name'])
      arrlist.unshift(profileName)
      session.put('LOGIN_NAME_LIST', uniq(arrlist), true)
    } else {
        /*
            * if you don't choose 'remember me' to login，we should judge whether the userID has been remembered.
            * if remembered,remove 'remember me' when the user logins again,but the userName list unchanged.
            */
      for (let i = 0; i < list.length; i++) {
        if (list[i]['name'] === profileName['name']) {
            list.splice(i, 1)
          }
      }
      session.put('LOGIN_NAME_LIST', uniq(list), true)
    }
  }
    // /**
    //  * you can choose 'remember me',or not.
    //  */
    // checkautoLogin() {
    //     // if (this.loginElect) {
    //     //     this.loginElect = false;
    //     //     this.checkedTbBg = 'none';
    //     // } else {
    //         this.loginElect = true;
    //         this.checkedTbBg = 'url(' + 'assets/img/16/select_16.png' + ') 0px 0px no-repeat';
    //     // }
    // }
    /**
     * you can choose 'agree the agreement',or not.
     */
  checkagree () {
    if (this.disagreePrivacy) {
      this.disagreePrivacy = false
      this.checkedPrivacyBg = 'url(' + 'assets/img/16/select_16.png' + ') 0px 0px no-repeat'
    } else {
      this.disagreePrivacy = true
      this.checkedPrivacyBg = 'none'
    }
  }
    /**
     * click the agreement information,jump to privacy page.
     */
  jumpToPrivacy () {
    window.open(window.location.href.split('#')[0] + '#/bottom/' + 'privacy_policy')
  }
    /**
     * keep the profiles, including ID and logourl.
     */
  keepProfiles (profileID, profiles: any[]) {
      // filter the user who logins
    this.loginProfile = profiles.filter(profile => profileID === profile['ID'])
    Cookies.set('LOGIN_PROFILE_LOGOURL', this.loginProfile[0].logoURL)
    Cookies.set('PROFILE_NAME', this.loginProfile[0].name)
  }
    /**
     * manual authentication
     */
  onAuthenticate (): void {
      // the userID and the userPassword should not been empty or underfined
    if (this.userID === '' || this.userID === undefined || this.userPassword === '' || this.userPassword === undefined) {
      this.errorMessage = true
      this.pwdOrLoginNameErrorMessage = 'profileName_or_pwd_empty'
      return
    }
      // keep the error message when the password or loginID is mistaken
    this.pwdOrLoginNameErrorMessage = ''
    this.authenticate(this.userID, this.userPassword)
  }
    /**
     * login authentication
     */
  authenticate (userID?: string, password?: string): any {
    userID = userID.trim() || Cookies.getJSON('USER_ID')
    session.put('INIT_CHECK_PASSWORD', password)
    if (userID && password) {
      this.loginLoadDisplay = true
      let req = {
        authenticateBasic: {
            userID: userID,
            userType: '1'
          },
        password: password
      }
      return this.authService.authenticate(req).then(resp => {
        EventService.emit('AUTHENTICATE_RESPONSE_SUCCESS', resp)
        if (resp && resp.result && resp.result.retCode === '157021017') {
            EventService.on('CONFIRM_CHECK_INIT_PWD', passwords => {
              this.accountIsLogin(resp, userID, passwords)
            })
          } else {
            this.accountIsLogin(resp, userID, password)
          }
      }, resp => {
        EventService.emit('AUTHENTICATE_RESPONSE_ERROR', resp)
        this.userPassword = ''
        document.getElementById('pass-Word') && document.getElementById('pass-Word').focus()
        this.visibility = 'visible'
        this.loginLoadDisplay = false
          // the errorCode of device reach the limit.
        if ((resp && resp.result && resp.result.retCode === '157022007') ||
                 (resp && resp.result && resp.result.retCode === '157022012')) {
            if (isUndefined(resp.devices) || resp.devices.length === 0) {
              this.commonService.commonLog('Authenticate', 'devices')
            }
            this.close()
            let data: any = {
              subscriberID: resp.subscriberID,
              userID: userID,
              password: password,
              devices: resp.devices,
              liveTvParams: this.liveTvParams
            }
            EventService.emit('DEVICELIST_OPEN', data)
          }
        return Promise.reject(resp)
      })
    } else {
      EventService.emit('LOGIN_OR_PASSWORD_INCORRECT')
      this.pwdOrLoginNameErrorMessage = 'loginname_or_password_is_incorrect_try_again'
      this.errorMessage = true
      this.visibility = 'visible'
    }
  }

  accountIsLogin (resp, userID, password) {
    EventService.emit('LOGININTRUE', false)
    let curProfile = find(resp.profiles, item => {
      return item['loginName'] === userID
    })
    this.commonService.resetSessionProfile(curProfile)
    Cookies.set('PROFILE_TYPE', curProfile['profileType'])
    session.put('LOGIN_NAME', curProfile['loginName'], true)
    session.put('PROFILERATING_ID', curProfile['ratingID'])
    session.put('SUBSCRIBER_ID', resp.subscriberID)
    Cookies.set('USER_ID', userID)
    session.put('deviceID', resp.deviceID)
    session.put('modify_profileID', resp.profileID, true)
    this.getReminderTime(curProfile)
    session.remove('INIT_CHECK_PASSWORD')
    this.putLoginNameList(userID)
    if (!resp.deviceName || resp.deviceName === '') {
      this.checkDeviceID(resp)
      let adminProfile = find(resp.profiles, item => {
        return Number(item['profileType']) === 0
      })
      let device: any = {
        ID: resp.deviceID,
        name: 'PC' + adminProfile['name'],
        isSupportPVR: '1',
        deviceType: '1'
      }
      session.put('IS_MANUAL_SET_DEVICE_NAME', false)
      modifyDeviceInfo({
        device: device
      }).then(() => {
          session.put('deviceName', device.name)
        })
    } else {
      session.put('deviceName', resp.deviceName)
    }
    if (resp.profileID) {
        // user behavior reported
      session.put('PROFILE_ID', resp.profileID)
      session.put('profileID', resp.subscriberID)
      session.put('tracker', resp.pageTrackers)
      session.put('profileSN', resp.profileSN)
      session.put('profileSn', resp.profileSn)
      const tracker = session.get('tracker') && session.get('tracker')[0]
      const profileID = session.get('profileSN') ? session.get('profileSN') : session.get('profileID')
      const deviceID = session.get('uuid_cookie')
      const trackParams = {
        actionUrl: tracker ? tracker.pageTrackerServerURL : '',
        subscriberID: profileID,
        deviceID: deviceID,
        appid: tracker ? tracker.appID : '',
        apppwd: tracker ? tracker.appPassword : '',
        needSend: tracker ? tracker.isSupportedUserLogCollect === '1' : false,
        deviceType: tracker ? tracker.type : '',
        sendCount: 5,
        sendTime: 10000
      }
      pageTracker.setTrackerParams(trackParams)
        // keep the choose whether the user agree the privacy agreement.
      pageTracker.setStatus(this.disagreePrivacy ? 0 : 1)
      session.put('privacy_agree', this.disagreePrivacy)
    }

    this.keepProfiles(resp.profileID, resp.profiles)
    EventService.emit('LOGIN')

    if (this.liveTvParams['guest'] === 'Guest') {
      if (resp.profileID) {
        Cookies.set('IS_PROFILE_LOGIN', true)
        Cookies.remove('IS_GUEST_LOGIN')
      } else {
        this.commonService.commonLog('Authenticate', 'profileID')
      }
      session.put('LOGIN_LIVETV_PLAYING', true)
      EventService.emit('GUEST_TO_LOGIN_LIVETV_PLAYING', this.liveTvParams)
      this.loginLoadDisplay = false
      this.close()
    }
    this.errorMessage = false
    this.userLoginHistoryInfo.showLoginHistoryInfo(resp['userLoginHistoryInfo'])
    EventService.emit('AUTHENTICATE_FULL_SUCCESS')
  }
    /**
     * save reminder time of pop
     */
  getReminderTime (curProfile) {
    if (curProfile && curProfile['leadTimeForSendReminder']) {
      session.put('leadTimeForSendReminder', curProfile['leadTimeForSendReminder'], true)
    }
  }
    /**
     * close the dialog of login
     */
  close () {
    this.dialog.close()
  }
    /**
     * close the login dialog,show the deviceList dialog
     */
  openRepaceDevice () {
    this.close()
    //this.commonService.openDialog(DeviceListComponent)
  }
    /**
     * close the login dialog, show the register dialog
     */
  openRegister (e) {
    e.preventDefault()
    this.close()
    this.commonService.openDialog(AccountRegisterComponent)
  }
    /**
     * close the login dialog, show the reset password dialog
     */
  openResetPassword (e) {
    e.preventDefault()
    this.close()
    this.commonService.openDialog(ResetPasswordComponent)
  }
    /**
     * Listening to when the key has been pressed
     */
  keydownLogin () {
    this.showplaceholderId = false
  }
    /**
     * Listening to password when the key has been pressed
     */
  keydownPassword () {
    this.showplaceholder = false
  }
  checkDeviceID (resp) {
    if (isUndefined(resp.deviceID) || resp.deviceID === '') {
      this.commonService.commonLog('Authenticate', 'deviceID')
    }
  }
    /**
     * Listening to privacy_agree when the mouseover
     */
  iStyleChange () {
    this.iStyle = true
  }
    /**
     * Listening to privacy_agree when the mouseout
     */
  iStyleChangeded () {
    this.iStyle = false
  }

  loginVK () {
    this.socialApi.login('vk', window.location.origin + '/home').then(url => {
      window.location.href = url
    })
  }

  ocData () {
    return JSON.parse(localStorage.getItem('oper_center'))
  }
}
