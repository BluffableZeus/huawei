import { Injectable } from '@angular/core'
import * as _ from 'underscore'
import { TranslateService } from '@ngx-translate/core'
import { WeakMessageDialogComponent } from '../../component/weakMessageDialog/weakMessageDialog.component'
import { CommonService } from '../../core/common.service'
import { DateStingComponent } from '../../component/dateString/dateString.component'

@Injectable()
export class UserLoginHistoryInfo {
  constructor (
        private commonService: CommonService,
        private dateStingComponent: DateStingComponent,
        private translate: TranslateService
  ) {
  }
  /**
     *show the login history information
     * @param authMessage
     */
  showLoginHistoryInfo (authMessage) {
    let userLoginHistoryInfo = authMessage
    let lastLoginSuccessInfo: any = ''
    let curValidLoginInfo: any = ''
    // if userLoginHistoryInfo is statable
    if (userLoginHistoryInfo) {
      // Empty object is valid to for the first check
      if (userLoginHistoryInfo.latestSuccessItem && Object.keys(userLoginHistoryInfo.latestSuccessItem).length > 0) {
        let lastLoginInfo = {
          lastLoginTime: this.dateStingComponent
            .stringToDate(userLoginHistoryInfo['latestSuccessItem']['logindate'], 'yyyyMMddHHmmss'),
          lastLoginIP: userLoginHistoryInfo['latestSuccessItem']['clientIP']
        }
        lastLoginSuccessInfo = this.translate.instant('last_login_info', lastLoginInfo)
      }
      if (userLoginHistoryInfo['curValidLoginItem'] && userLoginHistoryInfo['curValidLoginItem'].length > 0) {
        curValidLoginInfo += this.translate.instant('valid_login_info') + '<br />'
        _.each(userLoginHistoryInfo['curValidLoginItem'], (item) => {
          curValidLoginInfo += this.translate.instant('current_login_devices', {
            lastLoginTime: this.dateStingComponent.stringToDate(item['logindate'], 'yyyyMMddHHmmss'),
            lastLoginIP: item['clientIP']
          })
        })
      }
      // if lastLoginSuccess information is null
      if (lastLoginSuccessInfo !== '') {
        let loginHisInfo
        if (curValidLoginInfo !== '') {
          loginHisInfo = lastLoginSuccessInfo + ';' + curValidLoginInfo
        } else {
          loginHisInfo = lastLoginSuccessInfo
        }
        this.commonService.openDialog(WeakMessageDialogComponent, null, loginHisInfo)
      }
    }
  }
}
