import * as _ from 'underscore'

import { Component, OnInit, ViewEncapsulation } from '@angular/core'
import { DialogRef, DialogParams } from 'ng-epg-ui/webtv-components/dialog'
import { TranslateService } from '@ngx-translate/core'
import { EventService } from 'ng-epg-sdk/services'
import { PopupErrorsService } from '../../shared/services/popup.errors'
import { CountdownService } from '../../shared/services/countdown'
import { ValidationService } from '../../shared/services/validation'
import { AccountLoginComponent } from '../account-login'
import { CommonService } from '../../core/common.service'
import { config } from '../../shared/services/config'
import { session } from 'src/app/shared/services/session'

import { userAPI } from 'oper-center/api/user'

declare const grecaptcha: any

@Component({
  selector: 'app-account-register',
  templateUrl: './account-register.component.html',
  styleUrls: ['./account-register.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class AccountRegisterComponent implements OnInit {
    /**
     * declare variables.
     */
  visibility = 'hidden'
  btnClass = 'btn btn-fluent disabled'
  checkedPrivacyBg = 'none'

  disagreePrivacy = true

  userID = ''
  userPassword = ''
  userPasswordRepeat = ''
  tempLoginName = ''
  captchaResponse = ''
  captchaID = ''

  deleteLoginName = false
  deletePassWord = false
  deletePassWordRep = false
  showplaceholder = true
  showplaceholderId = true
  showplaceholderRep = true

  emailSent = false
  sending = false
  sentFault = false

  loginLoadDisplay: false
  termsUrl: string = ''
  privacyUrl: string = ''

    /**
     * private constructor
     */
  constructor (
        private dialog: DialogRef,
        private liveTvParams: DialogParams,
        private translate: TranslateService,
        private commonService: CommonService,
        private userapi: userAPI,
        public errors: PopupErrorsService,
        public countdown: CountdownService,
        private validate: ValidationService
    ) {
    let self = this
      // Listening to logging out,close the Register dialog.
    EventService.on('CLOSED_REGISTER_DIALOG', function () {
      self.close()
    })

    errors.clear()
    errors.registerField('login')
    errors.registerField('pass')
    errors.registerField('repeat')

    countdown.init()

    this.termsUrl = config.softwareLicenseLink
    this.privacyUrl = config.privacyLink
  }

    /**
     * initialization
     */
  ngOnInit () {
    EventService.removeAllListeners(['SHOW_AUTH_ERROR_MESSAGE'])
    EventService.on('SHOW_AUTH_ERROR_MESSAGE', (message) => {
      this.errors.add(message)
    })
      // Listening other browser backBtn to close Register dialog
    window.addEventListener('popstate', this.browserHashchange)
    this.recaptchaInit()
  }

    /**
     * Listening browser backBtn
     */
  browserHashchange (e) {
    EventService.emit('CLOSED_REGISTER_DIALOG')
  }

    /**
     * close the dialog of Register
     */
  close () {
    this.countdown.stop()
    this.dialog.close()
  }

    /**
     * change button of register when input userName and input loginName
     */
  btnStyleChange () {
    if (this.emailSent) {
      return true
    }
    if (this.errors.anyError()) {
      return false
    }

    return (this.userID !== '') && (this.userPassword !== '') && (this.userPasswordRepeat !== '') &&
            (this.userPassword === this.userPasswordRepeat) && !this.disagreePrivacy && !this.sending
  }

    /**
     * hidden the clues of userPassword,and the password box gets focus.
     */
  onplacehold () {
    if (this.userPassword === '' || this.userPassword === undefined) {
      _.delay(() => {
        document.getElementById('pass-Word').focus()
      }, 100)
    }
  }

    /**
     * hidden the clues of userPasswordRepeat,and the repeat box gets focus.
     */
  onplaceholdRep () {
    if (this.userPasswordRepeat === '' || this.userPasswordRepeat === undefined) {
      _.delay(() => {
        document.getElementById('pass-Word-repeat').focus()
      }, 100)
    }
  }

    /**
     * hidden the clues of userID,and the userID box gets focus.
     */
  onplaceholdId () {
    if (this.userID === '' || this.userID === undefined) {
      _.delay(() => {
        document.getElementById('login-ID').focus()
      }, 100)
    }
  }

    /**
     * click the loginID list to choose one loginID
     */
  setLoginID (message) {
    this.userID = message
    this.showplaceholderId = false // hidden the clues of userID
  }

    /**
     * when the loginID dialog focused,save the input.
     */
  loginNameFocus (e) {
    this.tempLoginName = e.target.value
  }

  loginNameBlur (e) {
    if (e.target.value === '') {
      this.deleteLoginName = false
      this.showplaceholderId = true
    }

    this.validateLogin(false)
  }

    /**
     * changeLoginName on input your userName
     */
  changeLoginName (e) {
    const msg = 'wrong_char'

    if (this.sentFault) {
      this.errors.clear()
      this.sentFault = false
    } else {
      this.errors.remove('not_correct_login')
      this.errors.remove('field_cannot_be_empty')
    }

    if (this.validate.hasRU(e.target.value)) {
      this.errors.on('login')
      this.errors.add(msg)
    } else {
      this.errors.off('login')
      this.errors.remove(msg)
    }

    this.showplaceholderId = false

    if (e.target.value !== this.tempLoginName && this.userPassword !== '') {
      this.userPassword = ''
      this.userPasswordRepeat = ''
      this.showplaceholder = true
      this.errors.off('pass')
      this.errors.off('repeat')
      this.errors.remove('not_correct_password')
    }

    if ((this.userID !== '') || (e.target.value !== '')) {
      if (e.keyCode === 13) {
        document.getElementById('login-ID').blur()
        document.getElementById('pass-Word').focus()
      }
      this.showplaceholderId = false
      this.deleteLoginName = true
    } else {
      this.deleteLoginName = false
      this.showplaceholderId = true
    }
  }

  passwordBlur (e) {
    if (e.target.value === '') {
      this.deletePassWord = false
      this.showplaceholder = true
    }

    this.validatePass(false)
  }

  passwordRepBlur (e) {
    if (e.target.value === '') {
      this.deletePassWordRep = false
      this.showplaceholderRep = true
    }
  }

    /**
     * input your password
     */
  changePassword (e) {
    const msg = 'wrong_char'

    if (this.sentFault) {
      this.errors.clear()
      this.sentFault = false
    } else {
      this.errors.remove('not_correct_password')
      this.errors.remove('passwords_not_equal')
      this.errors.remove('field_cannot_be_empty')
    }

    this.userPasswordRepeat = ''

    if (this.validate.hasRU(e.target.value)) {
      this.errors.on('pass')
      this.errors.add(msg)
    } else {
      this.errors.off('pass')
      this.errors.remove(msg)
    }

    if (this.userPassword !== '') {
      if (e.keyCode === 13) {
        document.getElementById('pass-Word').blur()
        document.getElementById('pass-Word-repeat').focus()
      }
      this.showplaceholder = false
      this.deletePassWord = true
    } else {
      this.deletePassWord = false
      this.showplaceholder = true
    }
  }

    /**
     * repeat input
     */
  changePasswordRepeat (e) {
    if (this.sentFault) {
      this.errors.clear()
      this.sentFault = false
    } else {
      this.errors.remove('field_cannot_be_empty')
      this.errors.remove('passwords_not_equal')
    }

    if (this.validate.hasRU(e.target.value)) {
      this.errors.on('repeat')
      this.errors.add('wrong_char')
    } else {
      this.errors.off('repeat')
      this.errors.remove('wrong_char')
    }

    if (this.userPasswordRepeat !== '') {
      if (e.keyCode === 13) {
        document.getElementById('logBtn').click()
      }
      this.showplaceholderRep = false
      this.deletePassWordRep = true
    } else {
      this.deletePassWordRep = false
      this.showplaceholderRep = true
    }
  }

    /**
     * show historical record of passwords
     */
  showPassword () {
    if (this.userPassword !== '') {
      this.deletePassWord = true
      this.showplaceholder = false
    }
    if (this.userPassword === '') {
      this.showplaceholder = true
    }
  }

    /**
     * show historical record of passwords
     */
  showPasswordRepeat () {
    this.errors.off('repeat')

    if (this.userPasswordRepeat !== '') {
      this.deletePassWordRep = true
      this.showplaceholderRep = false
    }
    if (this.userPasswordRepeat === '') {
      this.showplaceholderRep = true
    }
  }

    /**
     * hide historical record of passwords
     */
  hidePassword () {
    let self = this
    if (this.userPassword === '') {
      this.showplaceholder = true
    }
    _.delay(function () {
      self.deletePassWord = false
    }, 300)
  }

    /**
     * hide historical record of passwords repeats
     */
  hidePasswordRepeat (e) {
    let self = this
    if (this.userPasswordRepeat === '') {
      this.showplaceholderRep = true
    }
    _.delay(function () {
      self.deletePassWordRep = false
    }, 300)
  }

    /**
     * clear the userName of input
     */
  removeLoginName () {
    this.errors.off('login')
    this.errors.clear()

    this.userID = ''
    if (this.userPassword !== '') {
      this.userPassword = ''
      this.userPasswordRepeat = ''
      this.showplaceholder = true
      this.showplaceholderRep = true
      this.errors.off('pass')
      this.errors.off('repeat')
      this.errors.remove('not_correct_password')
    }
    if (this.userPasswordRepeat !== '') {
      this.userPasswordRepeat = ''
      this.showplaceholderRep = true
    }
    document.getElementById('login-ID').focus()
  }

    /**
     * Listening to when the key has been pressed
     */
  keydownLogin (e) {
    this.errors.setField('login', this.validate.hasRU(e.target.value + e.key))
    this.errors.toggle('wrong_char', this.errors.getField('login'))
    this.showplaceholderId = false
  }

    /**
     * Listening to password when the key has been pressed
     */
  keydownPassword (e) {
    this.errors.setField('pass', this.validate.hasRU(e.target.value + e.key))
    this.errors.toggle('wrong_char', this.errors.getField('pass'))
    this.showplaceholder = false
  }

    /**
     * Listening to password when the key has been pressed
     */
  keydownPasswordRepeat () {
    this.errors.off('repeat')
    this.showplaceholderRep = false
  }

    /**
     * you can choose 'agree the agreement',or not.
     */
  checkagree () {
    if (this.disagreePrivacy) {
      this.disagreePrivacy = false
      this.checkedPrivacyBg = 'url(' + 'assets/img/16/select_16.png' + ') 0px 0px no-repeat'
    } else {
      this.disagreePrivacy = true
      this.checkedPrivacyBg = 'none'
    }
  }

  validateLogin (checkEmpty: boolean) {
    return this.validate.byRules({
      value: this.userID,
      errors: this.errors,
      field: 'login',
      result: 'login',
      should: {
        hasNoRU: {
            msg: 'wrong_char'
          },
        notEmpty: {
            ext: !checkEmpty,
            msg: 'field_cannot_be_empty'
          },
        login: {
            ext: (this.userID === ''),
            msg: 'not_correct_login'
          }
      }
    })
  }

  validatePass (checkEmpty: boolean) {
    return this.validate.byRules({
      value: this.userPassword,
      errors: this.errors,
      field: 'pass',
      result: 'password',
      should: {
        hasNoRU: {
            msg: 'wrong_char'
          },
        notEmpty: {
            ext: !checkEmpty,
            msg: 'field_cannot_be_empty'
          },
        password: {
            ext: (this.userPassword === ''),
            msg: 'not_correct_password'
          }
      }
    })
  }

  validateRepeat () {
    this.errors.off('repeat')

    const isEqual = (this.userPassword === this.userPasswordRepeat)

    if (!isEqual) {
      this.errors.on('repeat')
      this.errors.add('passwords_not_equal')
    }

    if (this.userPasswordRepeat === '') {
      this.errors.off('repeat')
      this.errors.remove('passwords_not_equal')

      if (this.userPassword !== '') {
        this.errors.on('repeat')
        this.errors.add('field_cannot_be_empty')
      }
    }

    return isEqual && this.userPasswordRepeat !== ''
  }

  validateFields () {
    return this.validateLogin(true) && this.validatePass(true) && this.validateRepeat() && !this.disagreePrivacy
  }

  sendConfirmationEmail () {
    this.userapi.register({
      login: this.userID,
      password: this.userPassword,
      gRecaptchaResponse: this.captchaResponse,
      success: this.emailSendSuccess.bind(this),
      error: this.emailSendError.bind(this)
    })
  }

  emailSendSuccess () {
    this.lockForm()
  }

  emailSendError (message) {
    this.errors.add(message)
    this.sending = false
    this.sentFault = true
    this.deleteLoginName = true
  }

  resendEmail () {
    this.emailSent = false
    this.onRegister()
  }

  lockForm () {
    this.emailSent = true
    this.countdown.start()
  }

  getTopOffset () {
    if (this.emailSent) return 60
    if (this.sending) return 40

    const errs = document.querySelector('app-account-register .error_message'),
      infos = document.querySelector('app-account-register.info_message'),
      errH = errs ? errs.clientHeight : 0,
      infoH = infos ? infos.clientHeight : 0

    return errH + infoH
  }

  recaptchaCallback (response) {
    this.captchaResponse = response
    this.sendConfirmationEmail()
  }

  recaptchaInit () {
    const container = document.getElementById('recaptcha-c'),
      params = {
        sitekey: '6Le3kUwUAAAAAJ0KnDEisXv_L9xOlTVjGFPjHGO8',
        callback: this.recaptchaCallback.bind(this),
        size: 'invisible'
      }

    this.captchaID = grecaptcha.render(container, params)
  }

  recaptcha () {
    if (document.getElementById('recaptcha-c').innerHTML !== '') {
      grecaptcha.reset(this.captchaID)
    }

    grecaptcha.execute(this.captchaID)
  }

  onRegister () {
    this.errors.off('login')
    this.errors.off('pass')
    this.errors.off('repeat')
    this.deleteLoginName = false
    this.errors.clear()

    if (this.emailSent) {
      session.put('LOGIN_NAME_LIST', [{ name: this.userID }])
      this.close()
      this.commonService.openDialog(AccountLoginComponent)
    } else {
      if (this.validateFields()) {
        this.sending = true
        this.recaptcha()
      }
    }
  }
}
