import { FormsModule } from '@angular/forms'
import { TranslateModule } from '@ngx-translate/core'
import { DeviceReplaceComponent } from './device-replace/device-replace.component'
import { DeviceListComponent } from './device-list/device-list.component'
import { CheckInitPasswordComponent } from './checkInit-password/checkInitPassword.component'
import { AccountLoginComponent } from './account-login/account-login.component'
import { AccountRegisterComponent } from './account-register/account-register.component'
import { ResetPasswordComponent } from './reset-password/reset-password.component'
import { UserLoginHistoryInfo } from './account-login/userLoginHistoryInfo.service'
import { PreferencesHomeComponent } from './preferences/preferences.component'
import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'
import { MyTVModule } from '../my-tv/my-tv.module'

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    TranslateModule,
    MyTVModule
  ],
  exports: [
    AccountLoginComponent,
    AccountRegisterComponent,
    ResetPasswordComponent,
    CheckInitPasswordComponent,
    PreferencesHomeComponent,
    //DeviceListComponent
  ],
  declarations: [
    AccountLoginComponent,
    AccountRegisterComponent,
    ResetPasswordComponent,
    CheckInitPasswordComponent,
    DeviceListComponent,
    DeviceReplaceComponent,
    PreferencesHomeComponent
  ],
  entryComponents: [
    AccountLoginComponent,
    AccountRegisterComponent,
    ResetPasswordComponent,
    //DeviceListComponent,
    DeviceReplaceComponent,
    CheckInitPasswordComponent,
    PreferencesHomeComponent
  ],
  providers: [UserLoginHistoryInfo]
})
export class LoginModule { }
