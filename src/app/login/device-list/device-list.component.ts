import * as _ from 'underscore'
import { Component, OnInit, Input } from '@angular/core'
import { Router } from '@angular/router'
import { EventService } from 'ng-epg-sdk/services'
import { CommonService } from '../../core/common.service'
import { AccountLoginComponent } from '../account-login'
import { DeviceReplaceComponent } from '../device-replace'
import { TranslateService } from '@ngx-translate/core'
import { AuthService } from '../../shared/services/auth.service'
import { DirectionScroll } from 'ng-epg-ui/webtv-components/scroll/directionScroll.component'
@Component({
  selector: 'app-device-list',
  templateUrl: './device-list.component.html',
  styleUrls: ['./device-list.component.scss']
})

export class DeviceListComponent implements OnInit {
    // declare variables.
  public selectDevice: any = {}
  public deviceList: Array<any>
  public height = '180px'
  public isShowAll = false
  public deviceData
    // private constructor
  constructor (
        private router: Router,
        private commonService: CommonService,
        private translate: TranslateService,
        private directionScroll: DirectionScroll,
        private authService: AuthService
    ) { }

  @Input() set deviceListConfig (data) {
    this.deviceData = data
  }
  ngOnInit () {
      // load device list.
    this.getDeviceList()
    this.pluseOnScroll()
    document.getElementsByClassName('app-device-list')[0]['style']['top'] =
            (window.screen.height - parseInt(document.getElementsByClassName('app-device-list')[0]['offsetHeight'], 10)) / 2 + 'px'
  }
    /**
     * choose one device to replace,and must choose one device.
     */
  onSelect (device) {
    device.isSelect = true
    this.selectDevice = device
    _.each(this.deviceList, (item) => {
      if (item.id !== device.id) {
        item.isSelect = false
      }
    })
  }
    /**
     * if the length of device list is more than three,show 'more' button
     */
  showMore () {
    if (this.isShowAll) {
      this.isShowAll = false
      this.height = '180px'
    } else {
      this.isShowAll = true
      this.height = 'initial'
    }
  }
    /**
     * scroll event
     */
  pluseOnScroll () {
    if (this.deviceList && this.deviceList.length < 4) {
      return
    }
    let oConter, oUl, oScroll, oSpan
    let oBox = document.getElementById('deviceList-container')
    oConter = document.getElementById('deviceList')
    oUl = document.getElementById('deviceUl')
    oScroll = document.getElementById('deviceScroll')
    if (oScroll) {
      oSpan = oScroll.getElementsByTagName('span')[0]
    }
    this.directionScroll.setScroll(oBox, oConter, oUl, oScroll, oSpan, true)
  }
    /*
     * cancel or close the dialog
     */
  close () {
      // close the dialog of the device list.
    EventService.emit('DEVICELIST_CLOSE')
      // open the login dialog.
    this.commonService.openDialog(AccountLoginComponent)
      // then the guest should login again.
    this.authService.guestLogin()
  }
    /*
     * replce the device
     */
  replaceDevice () {
    EventService.emit('DEVICELIST_CLOSE')
    let data: any = {
      subscriberID: this.deviceData['subscriberID'],
      userID: this.deviceData['userID'],
      password: this.deviceData['password'],
      selectDevice: this.selectDevice,
      devices: this.deviceData['devices'],
      liveTvParams: this.deviceData['liveTvParams']
    }
      // open the dialog to replace the device
    this.commonService.openDialog(DeviceReplaceComponent, null, data)
  }
    /*
     * access to the device list
     */
  getDeviceList () {
    this.deviceList = _.map(this.deviceData['devices'], (device) => {
      return {
        id: device['ID'],
        name: device['name'] || device['terminalVendor'],
        onlineState: Number(device['onlineState']) === 1 ? this.translate.instant('online') : this.translate.instant('offline'),
        isSelect: false
      }
    })
      // choose the first device by default
    this.selectDevice = this.deviceList[0]
    this.deviceList[0].isSelect = true
  }
}
