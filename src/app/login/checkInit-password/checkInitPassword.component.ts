import { Component, OnInit, Output, EventEmitter } from '@angular/core'
import { ProfileManageService } from '../../my-tv/profile-manage/profile-manage.service'
import { EventService } from 'ng-epg-sdk/services'
import { session } from 'src/app/shared/services/session'
import { CommonService } from '../../core/common.service'
import { TranslateService } from '@ngx-translate/core'
import * as _ from 'underscore'
import { AuthService } from '../../shared/services/auth.service'

@Component({
  selector: 'app-check-init-password',
  templateUrl: './checkInitPassword.component.html',
  styleUrls: ['./checkInitPassword.component.scss']
})

export class CheckInitPasswordComponent implements OnInit {
  @Output() closeDialog: EventEmitter<Object> = new EventEmitter()
    // declare variables.
  newPassword
  confirmNewPassword
  subscriberPwd = ''
  reSubscriberPwd = ''

  profileLogPwdErrorMsg = ''
  isNewPwdRight = true
  isShowNewVerify = false
  tipMsg = ''
  checkRouler = {}

  isSame = true
  profileSamePwdErrorMsg = ''
  isShowSame = false
    // if true,the delete button of new password input is visible.if false, the delete button is hidden.
  deleteNewPwd = false
    // if true,the delete button of confirm password input is visible.if false, the delete button is hidden.
  deleteConfirmPwd = false
  changedPWd = false
    // if true,the clues of new password input by default is visible.if false, the delete button is hidden.
  showplaceholderNewPass = true
    // if true,the clues of new password input by default  is visible.if false, the delete button is hidden.
  showplaceholderConfirmPass = true
  longErrorMessage = false
    // private constructor
  constructor (
        private commonService: CommonService,
        private profileManageService: ProfileManageService,
        private translate: TranslateService,
        private authService: AuthService
    ) {
    this.checkRouler = session.get('USER_PWD_RULER_CACHE')
      // the new password box get focus when the dialog has been visibility first.
    _.delay(() => {
      if (document.getElementById('new-pass-Word')) {
        document.getElementById('new-pass-Word').focus()
      }
    }, 500)
  }

  ngOnInit () {

  }
    // hidden the clues of userPassword,and the new password box gets focus.
  onplaceholdNewPass () {
    if (this.subscriberPwd === '' || this.subscriberPwd === undefined) {
      _.delay(() => {
        document.getElementById('new-pass-Word').focus()
      }, 100)
    }
  }
    // hidden the clues of userPassword,and the confirm password box gets focus.
  onplaceholdConfirmPass () {
    if (this.reSubscriberPwd === '' || this.reSubscriberPwd === undefined) {
      _.delay(() => {
        document.getElementById('confirm-pass-Word').focus()
      }, 100)
    }
  }
    // click to confirm
  confirmCheckLogin () {
    if (this.changedPWd) {
      this.checkPasswordRules()
      if (this.isNewPwdRight && this.reSubscriberPwd === this.subscriberPwd) {
        this.profileManageService.modifyPassword({
            oldPwd: session.get('INIT_CHECK_PASSWORD'),
            newPwd: this.reSubscriberPwd,
            confirmPwd: this.reSubscriberPwd
          }).then(resp => {
            if (resp['result'].retCode === '000000000') {
              session.put('INIT_CHECK_PASSWORD', this.reSubscriberPwd)
              this.close()
              EventService.emit('CONFIRM_CHECK_INIT_PWD', this.reSubscriberPwd)
              EventService.emit('PROFILE_INFO_SAVE_SUCCESSFUL')
            }
          })
      }
    }
  }
  cancel () {
    this.closeDialog.emit()
    this.authService.guestLogin()
    EventService.emit('UILOGIN')
  }
    // close the dialog
  close () {
    this.closeDialog.emit()
  }

    /**
     * when get focus
     */
  profilePwdFocus () {
    this.showNextBtn()
    this.deleteConfirmPwd = false
    if (this.subscriberPwd === '' || this.subscriberPwd === undefined) {
      this.deleteNewPwd = false
      this.showplaceholderNewPass = true
    } else {
      this.deleteNewPwd = true
      this.showplaceholderNewPass = false
    }
  }
  profileConfirmFocus () {
    this.showNextBtn()
    this.deleteNewPwd = false
    if (this.reSubscriberPwd === '' || this.reSubscriberPwd === undefined) {
      this.deleteConfirmPwd = false
      this.showplaceholderConfirmPass = true
    } else {
      this.deleteConfirmPwd = true
      this.showplaceholderConfirmPass = false
    }
  }
    /**
     * when lose focus
     */
  profilePwdBlur () {
    if (this.changedPWd) {
      this.checkPasswordRules()
    }
    if (this.subscriberPwd === '') {
      this.showplaceholderNewPass = true
    }
  }

  newPwdBlur () {
    if (this.changedPWd) {
      this.checkPasswordRules()
    }
    if (this.reSubscriberPwd === '') {
      this.showplaceholderConfirmPass = true
    }
  }

  checkPasswordRulesTwice () {
    if (this.subscriberPwd === '') {
      this.longErrorMessage = true
      this.profileLogPwdErrorMsg = this.translate.instant('new_pwd_empty')
      return
    }
      // the password should not been too simple.
    if (this.subscriberPwd === session.get('INIT_CHECK_PASSWORD')) {
      this.longErrorMessage = true
      this.profileLogPwdErrorMsg = this.translate.instant('new_password_simple')
      return
    }
      // the password should include up_letter.
    if (this.checkRouler['userPwdUpperCaseLetters']) {
      if (!/[A-Z]+/.test(this.subscriberPwd)) {
        this.longErrorMessage = true
        this.profileLogPwdErrorMsg = this.translate.instant('new_pasaword_tips') + ' ' +
                 this.translate.instant('up_letter').replace(',', '.')
        return
      }
    }
      // the password should include low_letter.
    if (this.checkRouler['userPwdLowerCaseLetters']) {
      if (!/[a-z]+/.test(this.subscriberPwd)) {
        this.longErrorMessage = true
        this.profileLogPwdErrorMsg = this.translate.instant('new_pasaword_tips') + ' ' +
                 this.translate.instant('low_letter').replace(',', '.')
        return
      }
    }
      // the password should include digits.
    if (this.checkRouler['userPwdNumbers']) {
      if (!/[0-9]+/.test(this.subscriberPwd)) {
        this.longErrorMessage = true
        this.profileLogPwdErrorMsg = this.translate.instant('new_pasaword_tips') + ' ' +
                 this.translate.instant('digit').replace(',', '.')
        return
      }
    }
    return true
  }
    /**
     * judge whether the input compliance whith password rules
     */
  checkPasswordRules () {
    this.showNextBtn()
    this.isNewPwdRight = false
    this.isShowNewVerify = true
    this.isShowSame = false
    this.isSame = true
      // the password should not been empty.
    if (!this.checkPasswordRulesTwice()) {
      return
    }
      // the password should not been too simple.
    if (this.checkRouler['userPwdMinLength'] && this.subscriberPwd.length < this.checkRouler['userPwdMinLength']) {
      this.longErrorMessage = true
      this.profileLogPwdErrorMsg = this.translate.instant('new_password_simple')
      return
    }
      // the password should include special char.
    if (this.checkRouler['userPwdOthersLetters'] !== '' && this.checkRouler['userPwdOthersLetters'] !== undefined) {
      let exp = new RegExp('[' + this.checkRouler['userPwdOthersLetters'] + ']', 'g')
      if (!exp.test(this.subscriberPwd)) {
        this.longErrorMessage = true
        this.profileLogPwdErrorMsg = this.translate.instant('new_pasaword_tips') + ' ' +
                    this.translate.instant('special_char_firstlog', { spacial_char: ' ' + this.checkRouler['userPwdOthersLetters'] })
        return
      }
    }
    if (this.checkRouler['userPwdSupportSpace']) {
      if (this.subscriberPwd.indexOf(' ') === -1) {
        this.longErrorMessage = true
        this.profileLogPwdErrorMsg = this.tipMsg.replace(this.translate.instant('new_password_simple'), '')
        return
      }
    } else {
        // the password should not have blank space.
      if (this.subscriberPwd.indexOf(' ') !== -1) {
        this.longErrorMessage = true
        this.profileLogPwdErrorMsg = this.translate.instant('Space_not_allowed')
        return
      }
    }
    this.isNewPwdRight = true
    this.isShowNewVerify = false
      // the confirm password should not been empty.
    if (this.reSubscriberPwd === '') {
      this.longErrorMessage = true
      this.profileLogPwdErrorMsg = this.translate.instant('confirm_pwd_empty')
      return
    }
      // the resubscriber password should equal to the subscriber password
    if (this.reSubscriberPwd !== '' && this.subscriberPwd !== this.reSubscriberPwd) {
      this.isSame = false
      this.isShowSame = true
      this.longErrorMessage = true
      this.profileSamePwdErrorMsg = this.translate.instant('password_not_same')
    } else { // if the resubscriber password don't equal to the subscriber password, show the clues
      this.isShowSame = false
      this.profileSamePwdErrorMsg = ''
      this.isSame = true
    }
  }
    // Listening to the keyboard events of new password box.
  isNewPwdChanged (e) {
    this.profilePwdBlur()
    this.showNextBtn()
    if (this.subscriberPwd !== '') {
      if (e.keyCode === 13) { // if the 'enter' is pressed,the new password box lose focus and the confirm password get focus.
        document.getElementById('new-pass-Word').blur()
        document.getElementById('confirm-pass-Word').focus()
      }
      this.deleteNewPwd = true
      this.showplaceholderNewPass = false
    } else {
      this.deleteNewPwd = false
      this.showplaceholderNewPass = true
    }
  }
    // Listening to the keyboard events of confirm password box.
  isConfirmPwdChanged (e) {
    this.newPwdBlur()
    this.showNextBtn()
    if (this.reSubscriberPwd !== '') {
      if (e.keyCode === 13) { // if the 'enter' is pressed,click to confirm.
        document.getElementById('btnConfirmCheck').click()
      }
      this.deleteConfirmPwd = true
      this.showplaceholderConfirmPass = false
    } else {
      this.deleteConfirmPwd = false
      this.showplaceholderConfirmPass = true
    }
  }
    // Listening to the keyboard events of confirm button
  confirmcheck (e) {
    if (e.keyCode === 13) { // if the 'enter' is pressed,click to confirm.
      document.getElementById('btnConfirmCheck').click()
    }
  }
    // judge the style of the confirm button.
  showNextBtn () {
    if ((this.subscriberPwd === '' || this.subscriberPwd === undefined) &&
         (this.reSubscriberPwd === '' || this.reSubscriberPwd === undefined)) {
      this.changedPWd = false
    } else {
      this.changedPWd = true
    }
  }
    // click the delete button of the new password box
  newPwdDelete () {
    this.subscriberPwd = ''
    this.deleteNewPwd = false
    this.showNextBtn()
    document.getElementById('new-pass-Word').focus()
  }
    // click the delete button of the confirm password box
  confirmPwdDelete () {
    this.reSubscriberPwd = ''
    this.deleteConfirmPwd = false
    this.showNextBtn()
    document.getElementById('confirm-pass-Word').focus()
  }
    // Listening to when the key has been pressed.
  keydownNewPass () {
    this.showplaceholderNewPass = false
  }
  keydownConfirmPass () {
    this.showplaceholderConfirmPass = false
  }

  changedPWds () {
    if ((document.getElementById('new-pass-Word')['value'] !== '') && (document.getElementById('confirm-pass-Word')['value'] !== '')) {
      return true
    } else {
      return false
    }
  }
}
