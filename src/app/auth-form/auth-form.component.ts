import { Component, OnInit, HostListener, ViewChild, ElementRef, ChangeDetectionStrategy, OnDestroy } from '@angular/core'
import { Store, select, ActionsSubject } from '@ngrx/store'
import { session } from '../shared/services/session'
import { Observable, Subscription } from 'rxjs'
import { IMaskDirective } from 'angular-imask';
import { Router } from '@angular/router'

import {
  MODAL_OPEN,
  MODAL_CLOSE,
  FOCUS_LOGIN,
  FOCUS_PASSWORD,
  TYPE_SWITCH,
  GET_PASSWORD_VIA_SMS_SUBMIT,
  GET_PASSWORD_VIA_SMS_VALIDATE,
  GET_PASSWORD_VIA_SMS_TICK_FINISH,
  DO_SIGNIN_VALIDATE,
  DO_SIGNIN_SUBMIT,
  ON_PHONE_NUMBER,
  ON_PASSWORD
} from '../store/auth'

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'app-auth-form',
  templateUrl: './auth-form.component.html',
  styleUrls: ['./auth-form.component.scss']
})
export class AuthFormComponent implements OnDestroy {

  @ViewChild('loginInput') loginInput: ElementRef
  @ViewChild('passwordInput') passwordInput: ElementRef
  @ViewChild(IMaskDirective) imask: IMaskDirective

  mask: string = '+7 (___) ___-__-__';
  auth$: Observable<any>
  profiles$: Observable<any>

  subsc = new Subscription()

  constructor(
    private router: Router,
    private store: Store<any>,
    private actionsSubject: ActionsSubject
  ) {
    this.auth$ = store.pipe(select('auth'))
    this.profiles$ = store.pipe(select('profiles'))

    this.subsc = actionsSubject.subscribe(data => {
      if (data.type === FOCUS_PASSWORD) {
        return setTimeout(() => this.passwordInputFocus(), 100)
      }
      if (data.type === FOCUS_LOGIN) {
        return this.loginInputFocus()
      }
    })
  }

  @HostListener('document:keydown.escape', ['$event']) onKeydownHandler(evt: KeyboardEvent) {
    this.store.dispatch({ type: MODAL_CLOSE })
  }

  passwordInputFocus() {
    if (this.passwordInput && this.passwordInput.nativeElement) {
      this.passwordInput.nativeElement.focus()
    }
  }

  loginInputFocus() {
    if (this.loginInput && this.loginInput.nativeElement) {
      this.loginInput.nativeElement.focus()
    }
  }

  openModal() {
    this.store.dispatch({ type: MODAL_OPEN })
  }

  closeModal() {
    this.store.dispatch({ type: MODAL_CLOSE })
  }

  switchType(type: string) {
    this.store.dispatch({ type: TYPE_SWITCH, payload: type })
  }

  onPhoneNumberKeydown(value) {
    this.store.dispatch({ type: ON_PHONE_NUMBER, payload: value })
  }

  clearLogin() {
    this.loginInput.nativeElement.value = ''
    this.store.dispatch({ type: ON_PHONE_NUMBER, payload: this.mask })
    this.store.dispatch({ type: GET_PASSWORD_VIA_SMS_TICK_FINISH })
    this.imask.maskRef.updateValue()
  }

  checkLogin(value) {
    if (!value) {
      this.clearLogin()
    }
  }

  onPasswordKeydown(value) {

    const code = value.replace(/[^0-9]/g, '')

    this.store.dispatch({ type: ON_PASSWORD, payload: value })

    if (code && code.length === 6) {
      return this.doSignIn()
    }
  }

  clearPassword() {
    this.passwordInput.nativeElement.value = ''
    this.store.dispatch({ type: ON_PASSWORD, payload: '' })
    this.imask.maskRef.updateValue()
  }

  doSignIn() {
    this.store.dispatch({ type: DO_SIGNIN_VALIDATE })
    this.store.dispatch({ type: DO_SIGNIN_SUBMIT })
  }

  doJustSignIn() {
    this.store.dispatch({ type: DO_SIGNIN_SUBMIT })
  }

  doSignupValidate() {
    this.store.dispatch({ type: DO_SIGNIN_VALIDATE })
  }

  getPasswordViaSmsValidate() {
    this.store.dispatch({ type: GET_PASSWORD_VIA_SMS_VALIDATE })
  }

  getPasswordViaSms() {
    this.store.dispatch({ type: GET_PASSWORD_VIA_SMS_VALIDATE })
    this.store.dispatch({ type: GET_PASSWORD_VIA_SMS_SUBMIT })
  }

  forgotPassword() {
    this.store.dispatch({ type: FOCUS_LOGIN })
    this.switchType('signup')
  }

  showAgreement() {
    session.put('prevAuthPage', this.router.url)
    this.router.navigate(['/page/agreement'])
    this.store.dispatch({ type: MODAL_CLOSE })
  }

  ngOnDestroy() {
    this.subsc.unsubscribe();
  }

}
