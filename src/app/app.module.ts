import { APP_BASE_HREF } from '@angular/common'
//import { LoginModule } from './login/login.module'
import { HookConfig } from './hook-config'
import { HeaderModule } from './header'
// import { HeaderMobModule } from './headerMob'
//import { ComponentModule } from './component/component.module'
import { WeakTipDialogComponent } from './component/weakTip/weakTip.component'
import { APP_PROVIDERS } from './app.provider'
// import { RecordDialogComponent } from './component/recordDialog/recordDialog.component'
// import { UpDateRecordDialogComponent } from './component/updateRecordDialog/updateRecordDialog.component'
import { ErrorMessageDialogComponent } from './component/errorMessage/error-message-dialog.component'
import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'
import { FormsModule } from '@angular/forms'
import { HttpClientModule } from '@angular/common/http'
// import { QRCodeModule } from 'angular2-qrcode'
import {IMaskModule} from 'angular-imask';
import { UISharedModule } from 'ng-epg-ui/webtv-components/shared'

import { AppComponent } from './app.component'
import { AuthFormComponent } from './auth-form/auth-form.component'
import { AppRoutingModule } from './app-routing.module'
import { CoreModule } from './core/core.module'
import { SharedModule } from './shared/shared.module'
//import { LiveTVModule } from './live-tv'
import { HomeModule } from './home/home.module'
//import { StorybookModule } from './storybook/storybook.module'
//import { OnDemandModule } from './on-demand/on-demand.module'
//import { MyTVModule } from './my-tv/my-tv.module'
import { PipesModule } from './shared/pipes/pipes.module'

import { environment } from '../environments/environment'

import { EffectsModule } from '@ngrx/effects'
import { StoreModule } from '@ngrx/store'
import { StoreDevtoolsModule } from '@ngrx/store-devtools'

import { forRoot as forRootReducers,  metaReducers } from './store'
import { forRoot as forRootEffects } from './effects'
import { FooterComponent } from './core/components/footer/footer.component';

@NgModule({
  declarations: [
    AppComponent,
    AuthFormComponent,
    FooterComponent,
    WeakTipDialogComponent,
    // RecordDialogComponent,
    // UpDateRecordDialogComponent,
    ErrorMessageDialogComponent,
  ],
  imports: [
    IMaskModule,
    EffectsModule.forRoot(forRootEffects),
    StoreModule.forRoot(forRootReducers, { metaReducers }),
    StoreDevtoolsModule.instrument({ maxAge: 25, logOnly: environment.production }),
    AppRoutingModule,
    BrowserModule,
    FormsModule,
    HttpClientModule,
    UISharedModule,
    SharedModule,
    //ComponentModule,
    //LoginModule,
    HeaderModule,
    // HeaderMobModule,
    HomeModule,
    //StorybookModule,
    //LiveTVModule,
    //OnDemandModule,
    //MyTVModule,
    // QRCodeModule,
    PipesModule,
    CoreModule
  ],
  providers: [
    ...APP_PROVIDERS,
    { provide: APP_BASE_HREF, useValue: '/' },
    HookConfig
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
