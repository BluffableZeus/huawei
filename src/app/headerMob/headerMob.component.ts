import { Component, OnInit } from '@angular/core'
import { EventService } from 'ng-epg-sdk/services'
import { session } from 'src/app/shared/services/session'
import { TimerService } from '../shared/services/timer.service'
import { TranslateService } from '@ngx-translate/core'

const DETECT_WORDS = {
  'on-demand': ['video', 'series', 'season', 'movie', 'cast', 'category'],
  'home': ['home'],
  'live-tv': ['live-tv']
}

@Component({
  selector: 'app-header-mob',
  styleUrls: ['./headerMob.component.scss'],
  templateUrl: './headerMob.component.html'
})

export class HeaderMobComponent implements OnInit {
  public loginT = this.translate.instant('log_in')
  public isLogined = false
  public getCaheCustomConfigData = false
  public isMenuShow = false

  constructor (
        private translate: TranslateService,
        private timerService: TimerService
    ) {
    this.loginT = this.changeProfileName()
  }

  ngOnInit () {
    let firstOpen = session.get('firstOpen')
    if (!firstOpen) {
      this.getCaheCustomConfigData = false
    }
    EventService.on('GETCUSTOMCONFIGDATASUCCES', () => this.getCaheCustomConfigData = true)
    EventService.on('LOGIN', () => this.changeProfileName())
    EventService.on('CHANGE_PROFILE_NAME', () => this.changeProfileName())
    EventService.on('PROFILE_UPDATED', () => this.changeProfileName())
  }
  onClickMenuBar () {
    this.isMenuShow = true
  }
  onClickCloseMenu () {
    this.isMenuShow = false
  }
  setClass (id) {
    if (id in DETECT_WORDS) {
      const word = DETECT_WORDS[id]
      const finded = word.find(item => location.pathname.indexOf(item) !== -1)

      if (finded) {
        return 'active'
      }
    }

    return ''
  }
  gotoTop () {
    this.onClickCloseMenu()
    this.timerService.backTop()
  }
  login () {
    this.onClickCloseMenu()
    EventService.emit('UILOGIN')
  }
  onLogout () {
    this.onClickCloseMenu()
    EventService.emit('UILOGOUT')
  }
  changeProfileName () {
    this.isLogined = Boolean(Cookies.get('PROFILE_NAME'))

    return this.loginT = this.translate.instant(this.isLogined ? 'my_profile_link' : 'log_in')
  }
}
