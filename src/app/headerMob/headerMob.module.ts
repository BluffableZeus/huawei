import { CommonModule } from '@angular/common'
import { FormsModule } from '@angular/forms'
import { NgModule } from '@angular/core'
import { HeaderMobComponent } from './headerMob.component'
import { HeaderRoutingModule } from '../header/header-routing.module'
import { SharedModule } from '../shared/shared.module'

@NgModule({
  imports: [
    FormsModule,
    CommonModule,
    SharedModule,
    HeaderRoutingModule
  ],
  exports: [
    HeaderMobComponent
  ],
  declarations: [
    HeaderMobComponent
  ],
  providers: []
})

export class HeaderMobModule { }
