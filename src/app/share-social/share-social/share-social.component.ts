import { Component, OnInit, Input } from '@angular/core';
declare let Ya: any;

@Component({
  selector: 'app-share-social',
  templateUrl: './share-social.component.html',
  styleUrls: ['./share-social.component.scss']
})
export class ShareSocialComponent implements OnInit {
  @Input() dataAboutPageFromParent: any;
  constructor() {  }
  isShowSharedIcons: boolean = true;
  ngOnInit() {
    let share = Ya.share2('id-share-social', {
      content: {
        url: document.URL,
        title: this.dataAboutPageFromParent.name ,
        description: this.dataAboutPageFromParent.introduce,
        image: this.dataAboutPageFromParent.picture && this.dataAboutPageFromParent.picture.ads && this.dataAboutPageFromParent.picture.ads[0]
      },
      theme: {
        services: 'vkontakte,twitter,facebook,odnoklassniki',
        lang: 'en'
      }
    });
  }

  sharedIconClick() {
    this.isShowSharedIcons=!this.isShowSharedIcons;
  }
}
