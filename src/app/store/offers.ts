import { Action } from '@ngrx/store'

export interface OffersAction extends Action {
  type: string
  payload?: any
}

export const OFFER_LOADER_START = '[Offer] Loader Start'
export const OFFER_DATA = '[Offer] Load Data'
export const OFFER_DATA_NOTHING = '[Offer] Data Nothing'
export const OFFER_DATA_RELOAD = '[Offer] Data Reload'
export const OFFER_DATA_SUCCESS = '[Offer] Data Success'
export const OFFER_DATA_ERROR = '[Offer] Data Error'

const initialState = {
  loading: false,
  data: null,
  error: null,
}

export function offersReducer(state: any = initialState, action: OffersAction) {
  switch (action.type) {

    case OFFER_LOADER_START:
      return {
        ...state,
        loading: true
      }

    case OFFER_DATA_NOTHING:
      return {
        ...state,
        loading: false
      }

    case OFFER_DATA_SUCCESS:
      return {
        ...state,
        loading: false,
        data: action.payload
      }

    case OFFER_DATA_ERROR:
      return {
        ...state,
        loading: false,
        error: action.payload
      }

    default:
      return state
  }
}
