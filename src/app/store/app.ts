import { Action } from '@ngrx/store'

export interface AppAction extends Action {
  type: string
  payload?: any
}

export const APP_INIT_LOAD_START = '[App] Init Load Start'
export const APP_INIT_LOAD_END = '[App] Init Load End'
export const APP_HOME_LOAD_START = '[App] Home Load Start'
export const APP_HOME_LOAD_END = '[App] Home Load End'
export const APP_START_HEARTBEAT = '[App] Start Heartbeat'
export const APP_START_HEARTBEAT_FAILED = '[App] Start Heartbeat Failed'
export const APP_GUEST_LOGIN = '[App] Guest Login'
export const APP_GUEST_LOGIN_SUCCESS = '[App] Guest Login Success'
export const APP_GUEST_LOGIN_FAILED = '[App] Guest Login Failed'
export const APP_NAVIGATION_START = '[App] Navigation Start'
export const APP_LOGOUT = '[App] Logout'

const initialState = {
  initLoadStartedAt: null,
  initLoadEndedAt: null,
  homeLoadStartedAt: null,
  homeLoadEndedAt: null,
}

export function appReducer(state: any = initialState, action: AppAction) {
  switch (action.type) {

    case APP_INIT_LOAD_START:
      return {
        ...state,
        initLoadStartedAt: new Date()
      }

    case APP_INIT_LOAD_END:
      return {
        ...state,
        initLoadEndedAt: new Date()
      }

    case APP_HOME_LOAD_START:
      return {
        ...state,
        homeLoadStartedAt: new Date()
      }

    case APP_HOME_LOAD_END:
      return {
        ...state,
        homeLoadEndedAt: new Date()
      }

    default:
      return state
  }
}
