import { Action } from '@ngrx/store'

export interface HomeAction extends Action {
  type: string
  payload?: any
}

export const HOME_LOADER_START = '[Home] Loader Start'
export const HOME_DATA = '[Home] Load Data'
export const HOME_DATA_NOTHING = '[Home] Data Nothing'
export const HOME_DATA_RELOAD = '[Home] Data Reload'
export const HOME_DATA_SUCCESS = '[Home] Data Success'
export const HOME_DATA_ERROR = '[Home] Data Error'

const initialState = {
  loading: false,
  data: null,
  error: null,
}

export function homeReducer(state: any = initialState, action: HomeAction) {
  switch (action.type) {

    case HOME_LOADER_START:
      return {
        ...state,
        loading: true
      }

    case HOME_DATA_NOTHING:
      return {
        ...state,
        loading: false
      }

    case HOME_DATA_SUCCESS:
      return {
        ...state,
        loading: false,
        data: action.payload
      }

    case HOME_DATA_ERROR:
      return {
        ...state,
        loading: false,
        error: action.payload
      }

    default:
      return state
  }
}
