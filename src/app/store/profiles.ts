import { Action } from '@ngrx/store'

export interface ProfilesAction extends Action {
  type: string
  payload?: any
}

export const UPDATE_PROFILES_DATA = '[Profiles] Update Profiles Data'

const initialState = {}

export function profilesReducer(state: any = initialState, action: ProfilesAction) {
  switch (action.type) {

    case UPDATE_PROFILES_DATA:
      return {
        ...state,
        ...action.payload
      }

    default:
      return state
  }
}
