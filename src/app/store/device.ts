import { Action } from '@ngrx/store'
import Fingerprint from 'fingerprintjs'

export interface DeviceAction extends Action {
  type: string
  payload?: any
}

export const UPDATE_DEVICE_DATA = '[Device] Update Data'

const initialState = {
  terminalType: '2',
  deviceId: 'web_' + new Fingerprint({ screen_resolution: true }).get(),
  deviceName: 'Web',
  deviceModel: 'PC',
  deviceOS: navigator.platform
}

export function deviceReducer(state: any = initialState, action: DeviceAction) {
  switch (action.type) {

    case UPDATE_DEVICE_DATA:
      return {
        ...state,
        ...action.payload
      }

    default:
      return state
  }
}
