import { Action } from '@ngrx/store'

export interface AuthAction extends Action {
  type: string
  payload?: any
}

export const MODAL_OPEN = '[Auth] Open Modal'
export const MODAL_CLOSE = '[Auth] Close Modal'
export const TYPE_SWITCH = '[Auth] Switch Type'
export const ON_PHONE_NUMBER = '[Auth] Phone Number'
export const ON_PASSWORD = '[Auth] On Password'
export const FORM_LOADING = '[Auth] Form Loading'

export const GET_PASSWORD_VIA_SMS = '[Auth] Get Password via SMS'
export const GET_PASSWORD_VIA_SMS_VALIDATE = '[Auth] Get Password via SMS Validate'
export const GET_PASSWORD_VIA_SMS_SUBMIT = '[Auth] Get Password via SMS Submit'
export const GET_PASSWORD_VIA_SMS_SUCCESS = '[Auth] Get Password via SMS Sucess'
export const GET_PASSWORD_VIA_SMS_ERROR = '[Auth] Get Password via SMS Error'
export const GET_PASSWORD_VIA_SMS_TICK = '[Auth] Get Password via SMS Tick'
export const GET_PASSWORD_VIA_SMS_TICK_FINISH = '[Auth] Get Password via SMS Tick Finish'

export const DO_SIGNIN = '[Auth] Do Sign in'
export const DO_SIGNIN_VALIDATE = '[Auth] Do Sign in Validate'
export const DO_SIGNIN_SUCCESS = '[Auth] Do Sign in Success'
export const DO_SIGNIN_ERROR = '[Auth] Do Sign in Error'
export const DO_SIGNIN_SUBMIT = '[Auth] Do Sign in Submit'

export const DO_SIGNIN_FULL_INIT = '[Auth] Do Sign In Full Init'
export const DO_SIGNIN_FULL_SUCCESS = '[Auth] Do Sign In Full Success'
export const DO_SIGNIN_FULL_ERROR = '[Auth] Do Sign In Full Error'

export const DO_LOGOUT = '[Auth] Do Logout'

export const FOCUS_LOGIN = '[Auth] Focus Login'
export const FOCUS_PASSWORD = '[Auth] Focus Password'

const SMS_CODE_TTL = 1000 * 60 * 5

const initialState = {
  modal: false,
  login: '',
  loginDisplay: '',
  loginMask: '+7 (___) ___-__-__',
  loginError: '',
  password: '',
  passwordError: '',
  passwordSmsSuccess: null,
  passwordSmsError: null,
  passwordSmsAt: null,
  passwordSmsTTL: null,
  passwordSmsTick: null,
  signinSuccess: null,
  signinError: null,
  signinFullSuccess: null,
  signinFullError: null,
  invalid: false,
  loading: false,
  type: 'signup',
}

export const validateLogin = state => {
  if (state.login[0] !== '7' || state.login.length !== 11) {
    return 'auth.phone_number_format_error'
  }
  return ''
}

export const validatePassword = state => {
  if (state.password.length < 3) {
    return 'auth.password_length_error'
  }
  return ''
}

export const getLoginDisplay = login => {
  return login
}

let loginError
let passwordError

export function authReducer(state: any = initialState, action: AuthAction) {
  switch (action.type) {

    case DO_LOGOUT:
      return {
        ...state,
        signinFullSuccess: null
      }

    case FORM_LOADING:
      return {
        ...state,
        loading: action.payload
      }

    case MODAL_OPEN:
      return {
        ...state,
        loading: false,
        passwordSmsError: null,
        passwordSmsTick: null,
        passwordError: null,
        signinError: null,
        modal: true
      }

    case MODAL_CLOSE:
      return {
        ...state,
        password: '',
        passwordSmsSuccess: null,
        passwordError: null,
        signinFullError: null,
        modal: false
      }

    case TYPE_SWITCH:
      return {
        ...state,
        loginError: '',
        passwordError: '',
        passwordSmsError: null,
        signinFullError: null,
        signinError: null,
        password: '',
        type: action.payload
      }

    case GET_PASSWORD_VIA_SMS:
      return {
        ...state,
        passwordSmsSuccess: null,
        passwordSmsError: null
      }

    case GET_PASSWORD_VIA_SMS_SUCCESS:
      return {
        ...state,
        passwordSmsTTL: SMS_CODE_TTL + new Date().getTime(),
        passwordSmsAt: new Date().getTime(),
        passwordSmsSuccess: true
      }

    case GET_PASSWORD_VIA_SMS_ERROR:
      return {
        ...state,
        passwordSmsError: action.payload
      }

    case GET_PASSWORD_VIA_SMS_TICK_FINISH:
      return {
        ...state,
        passwordSmsTick: null
      }

    case GET_PASSWORD_VIA_SMS_TICK:
      return {
        ...state,
        passwordSmsTick: action.payload
      }

    case DO_SIGNIN_SUCCESS:
      return {
        ...state,
        signinFullError: null,
        signinSuccess: action.payload
      }

    case DO_SIGNIN_ERROR:
      return {
        ...state,
        signinError: action.payload
      }

    case DO_SIGNIN_FULL_INIT:
      return {
        ...state,
        signinFullError: null,
        signinError: null
      }

    case DO_SIGNIN_FULL_SUCCESS:
      return {
        ...state,
        signinFullSuccess: true
      }

    case DO_SIGNIN_FULL_ERROR:
      return {
        ...state,
        signinFullError: action.payload
      }

    case GET_PASSWORD_VIA_SMS_VALIDATE:

      loginError = validateLogin(state)

      return {
        ...state,
        loginError,
        invalid: Boolean(loginError)
      }

    case DO_SIGNIN_VALIDATE:

      loginError = validateLogin(state)
      passwordError = validatePassword(state)

      return {
        ...state,
        loginError,
        passwordError,
        invalid: Boolean(passwordError || loginError)
      }

    case ON_PHONE_NUMBER:
      return {
        ...state,
        loginError: '',
        login: action.payload.replace(/[^0-9]/g, ''),
        loginDisplay: getLoginDisplay(action.payload)
      }

    case ON_PASSWORD:
      return {
        ...state,
        passwordError: '',
        signinFullError: null,
        signinError: null,
        password: action.payload
      }

    default:
      return state
  }
}
