import { Action } from '@ngrx/store'

import { environment } from '../../environments/environment';

export interface DeviceAction extends Action {
  type: string
  payload?: any
}

export const UPDATE_DEVICE_DATA = '[Device] Update Data'

const initialState = {
  tvHouseLoginBase: environment.TV_HOUSE_API_LOGIN_BASE
}

export function apiReducer(state: any = initialState, action: DeviceAction) {
  switch (action.type) {

    case UPDATE_DEVICE_DATA:
      return {
        ...state,
        ...action.payload
      }

    default:
      return state
  }
}
