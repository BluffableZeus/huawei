import { localStorageSync } from 'ngrx-store-localstorage'
import { ActionReducer, MetaReducer } from '@ngrx/store'

import { authReducer } from './auth'
import { deviceReducer } from './device'
import { profilesReducer } from './profiles'
import { homeReducer } from './home'
import { apiReducer } from './api'
import { appReducer } from './app'
import { offersReducer } from './offers'

export const forRoot = {
  app: appReducer,
  api: apiReducer,
  auth: authReducer,
  home: homeReducer,
  offers: offersReducer,
  device: deviceReducer,
  profiles: profilesReducer
}

export function localStorageSyncReducer(reducer: ActionReducer<any>): ActionReducer<any> {
  return localStorageSync({ keys: ['auth', 'device', 'profiles'], rehydrate: true })(reducer)
}

export const metaReducers: Array<MetaReducer<any, any>> = [localStorageSyncReducer]
