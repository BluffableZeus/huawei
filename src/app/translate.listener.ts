import { TranslateService } from '@ngx-translate/core'
import { session } from 'src/app/shared/services/session'
import { config as EPGConfig } from './shared/services/config'
import * as _ from 'underscore'

export function init (translate: TranslateService) {
  return () => {
    let language = session.get('language')
    if (!language) {
      let browerLanguage = window.navigator.language.substring(0, 2)
      language = _.contains(EPGConfig.SupportLanguageList, browerLanguage)
        ? browerLanguage : EPGConfig.SupportLanguageList[0]
    }
    return new Promise((resolve, reject) => {
      translate.use(language)
        .subscribe(() => {
          resolve(true)
        }, reject)
    })
  }
}
