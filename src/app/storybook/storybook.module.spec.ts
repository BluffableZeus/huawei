import { StorybookModule } from './storybook.module'

describe('StorybookModule', () => {
  let storybookModule: StorybookModule

  beforeEach(() => {
    storybookModule = new StorybookModule()
  })

  it('should create an instance', () => {
    expect(storybookModule).toBeTruthy()
  })
})
