import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'

import { TranslateModule } from '@ngx-translate/core';

import { StorybookRoutingModule } from './storybook-routing.module'
import { StorybookComponent } from './storybook.component'

@NgModule({
  imports: [
    CommonModule,
    TranslateModule.forRoot(),
    StorybookRoutingModule
  ],
  declarations: [StorybookComponent]
})
export class StorybookModule { }
