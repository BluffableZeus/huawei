import * as _ from 'underscore'
import { Injectable } from '@angular/core'
import { querySubjectVODBySubjectID, queryVODListBySubject, queryRecmVODList, querySubjectDetail } from 'ng-epg-sdk/vsp'
import { PictureService } from 'ng-epg-ui/services/picture.service'

@Injectable()
export class RecomvodAllService {
  constructor (private pictureService: PictureService) {}

  getCategoryInfo (id) {
    return querySubjectDetail({ subjectIDs: [id] }).then(resp => resp.subjects[0])
  }

  getData (id, childrennumber, filterName, offset) {
    if (childrennumber === 1) {
      return querySubjectVODBySubjectID({
        subjectID: id,
        VODSortType: 'STARTTIME:DESC',
        subjectCount: '5',
        VODCount: '24',
        offset: offset
      }).then(resp => {
        let homeData
        let vodListSuspension = []
        let oscarVODInfo = []
        let total = 0
        let returndata = {}
        if (resp['subjectVODLists'] && resp['subjectVODLists'].length > 0) {
          homeData = resp['subjectVODLists']
          for (let i = 0; i < homeData.length; i++) {
            vodListSuspension[i] = _.map(homeData[i].VODs, (list, index) => {
              list['focusRoute'] = 'on-demand'
              return {
                titleName: homeData[i].subject.name,
                floatInfo: list
              }
            })
          }
          oscarVODInfo = _.map(homeData, (list, index) => {
            return {
              subjectName: list['subject']['name'],
              hasChildren: list['subject']['hasChildren'],
              subjectID: list['subject']['ID'],
              VODList: list['VODs'] ? list['VODs'] : ''
            }
          })
          oscarVODInfo = _.map(oscarVODInfo, (lists, index) => {
            return _.map(lists['VODList'], (list, index1) => {
              this.score(list)
              if (list['picture'] && list['picture'].posters && list['picture'].posters[0]) {
                let url = this.pictureService.convertToSizeUrl(list['picture'].posters[0],
                  { minwidth: 164, minheight: 230, maxwidth: 164, maxheight: 230 })
                list['picture'].posters = []
                list['picture'].posters[0] = url || 'assets/img/default/home_poster.png'
              }
              return list
            })
          })
        }
        if (resp['total']) {
          total = Number(resp['total'])
        }
        returndata[0] = oscarVODInfo
        returndata[1] = vodListSuspension
        returndata[2] = total
        return returndata
      })
    } else {
      if (filterName === 'top_this_week') {
        return queryRecmVODList({
          count: '30',
          offset: offset,
          action: '5',
          sortType: 'AVERAGESCORE:DESC',
          subjectID: id
        }).then(resp => {
          let list = resp.VODs.map(item => {
            let url = item['picture'] && item['picture'].posters &&
                            this.pictureService.convertToSizeUrl(item['picture'].posters[0],
                              { minwidth: 164, minheight: 230, maxwidth: 164, maxheight: 230 })
            this.score(item)
            item['focusRoute'] = 'on-demand'
            item['picture'].posters = [url || 'assets/img/default/home_poster.png']
            return {
              vodId: item.ID,
              name: item.name,
              posterUrl: url || 'assets/img/default/home_poster.png',
              score: item.averageScore || '0.0',
              floatInfo: item,
              titleName: filterName,
              subjectID: '-1'
            }
          })
          return list
        })
      } else {
        return queryVODListBySubject({
          subjectID: id,
          sortType: 'STARTTIME:DESC',
          count: '30',
          offset: offset
        }).then(resp => {
          let list = _.map(resp.VODs, (item, index) => {
            let url = item['picture'] && item['picture'].posters &&
                         this.pictureService.convertToSizeUrl(item['picture'].posters[0],
                           { minwidth: 164, minheight: 230, maxwidth: 164, maxheight: 230 })
            this.score(item)
            item['focusRoute'] = 'on-demand'
            item['picture'].posters = [url || 'assets/img/default/home_poster.png']
            return {
              vodId: item.ID,
              name: item.name,
              posterUrl: url || 'assets/img/default/home_poster.png',
              score: item.averageScore || '0.0',
              floatInfo: item,
              titleName: filterName,
              subjectID: id
            }
          })
          list['total'] = resp.total
          return list
        })
      }
    }
  }

  score (list) {
    if (list.averageScore) {
      if (list.averageScore.length === 1) {
        list.averageScore += '.0'
      } else {
        if (list.averageScore.substring(0, 2) === '10') {
          list.averageScore = '10'
        } else {
          let score = list.averageScore
          list.averageScore = score.substring(0, 4)
          list.averageScore = Number(list.averageScore)
          list.averageScore = list.averageScore.toFixed(1)
        }
      }
    } else {
      list.averageScore = '0.0'
    }
    return list
  }
}
