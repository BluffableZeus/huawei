import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { Router } from '@angular/router'
import { VODRouterService } from '../../shared/services/vod-router.service'
import { RecomvodAllService } from './recomvodAll.service'
import { PictureService } from 'ng-epg-ui/services/picture.service'
import { EventService } from 'ng-epg-sdk/services'
import { TitleService } from 'src/app/shared/services/title.service';

const THREE = 3

@Component({
  selector: 'app-recomvod-all',
  templateUrl: './recomvodAll.component.html',
  styleUrls: ['./recomvodAll.component.scss']
})

export class RecomVodMoreComponent implements OnInit {
  moreVodList: {
    'dataList': {},
    'row': string,
    'suspensionID': string
  }

  public homeData = {}
  public oscarVODInfo = []
  public titles
  public isShow = 'false'
  public isShowNodata = false
  public isShowChildNodata = false
  public susTime
  public childrennumber
  public vodListSuspension = []
  public recomVodAllId
  public recomvodAllShow
  public filterID
  public filterName
  public totalNumber
  public totalVODNumber
  public totalVOD
  public hasGetData = true

  constructor (
        private route: ActivatedRoute,
        private router: Router,
        private vodRouter: VODRouterService,
        private recomvodAllService: RecomvodAllService,
        private pictureService: PictureService,
        private _titleService: TitleService
    ) {}

  ngOnInit () {
    let self = this
    document.body.scrollTop = 0
    document.documentElement.scrollTop = 0
    this.recomVodAllId = ['recomVodAllsuspension', 'recomVodAllList', 'recomVodAllLeft', 'recomVodAllRight']
    this.recomvodAllShow = [true, true, true]
    this.route
        .params
        .subscribe(params => {
          let id = params['id']
          this.filterID = id
          this.recomvodAllService.getCategoryInfo(id).then(resp => {
            this.childrennumber = Number(resp.hasChildren)
            this.titles = this.filterName = resp.name
            this.hasGetData = true
            this.isShowChildNodata = false
            this.isShowNodata = false
            this.getData(id, this.childrennumber, this.filterName);

            this._titleService.setOnlyTitle(this.titles)
          })
        })
    EventService.removeAllListeners(['LOAD_DATA'])
    EventService.on('LOAD_DATA', function () {
      if (self.hasGetData) {
        self.hasGetData = false
        self.loadMoreOscarData()
        self.loadMoreVodData()
      }
    })
  }

  loadMoreOscarData () {
    let self = this
    if (self.oscarVODInfo && self.oscarVODInfo.length && self.oscarVODInfo.length < self.totalNumber) {
      if (document.querySelector('.ondemand-recomvod .vod-filter-load2')) {
        document.querySelector('.vod-filter-load2')['style']['display'] = 'block'
      }
      let offset = self.oscarVODInfo.length + ''
      self.recomvodAllService.getData(self.filterID, self.childrennumber, self.filterName, offset).then((resp) => {
        let loadData = resp[0]
        self.oscarVODInfo = self.oscarVODInfo.concat(loadData)
        self.vodListSuspension = self.vodListSuspension.concat(resp[1])
        if (document.querySelector('.ondemand-recomvod .vod-filter-load2')) {
            document.querySelector('.vod-filter-load2')['style']['display'] = 'none'
          }
        self.hasGetData = true
      })
    }
  }

  loadMoreVodData () {
    let self = this
    if (self.moreVodList && self.moreVodList.dataList && self.moreVodList.dataList['length'] &&
            self.moreVodList.dataList['length'] < self.totalVODNumber) {
      if (document.querySelector('.recomvod-content .vod-filter-load2')) {
        document.querySelector('.vod-filter-load2')['style']['display'] = 'block'
      }
      let offset = self.moreVodList.dataList['length'] + ''
      self.recomvodAllService.getData(self.filterID, self.childrennumber, self.filterName, offset).then((resp) => {
        self.totalVOD = self.totalVOD.concat(resp)
        self.moreVodList = {
            'dataList': self.totalVOD,
            'row': 'All',
            'suspensionID': 'oscarAllContent'
          }
        if (document.querySelector('.recomvod-content .vod-filter-load2')) {
            document.querySelector('.vod-filter-load2')['style']['display'] = 'none'
          }
        self.hasGetData = true
      })
    }
  }

  getData (id, childrennumber, filterName) {
    if (this.hasGetData) {
      this.hasGetData = false
      let offset = '0'
      if (childrennumber === 1) {
        this.vodListSuspension = []
        this.oscarVODInfo = []
        this.recomvodAllService.getData(id, childrennumber, filterName, offset).then((resp) => {
            this.oscarVODInfo = resp[0]
            this.vodListSuspension = resp[1]
            this.isShowChildNodata = true
            this.isShowNodata = false
            this.totalNumber = resp[2]
            this.hasGetData = true
          })
      } else {
        this.recomvodAllService.getData(id, childrennumber, filterName, offset).then((resp) => {
            this.moreVodList = {
              'dataList': resp,
              'row': 'All',
              'suspensionID': 'oscarAllContent'
            }
            this.totalVOD = resp
            this.totalVODNumber = resp['total']
            this.isShowNodata = true
            this.isShowChildNodata = false
            this.hasGetData = true
          })
      }
    }
  }

  onClickDetail (event) {
    this.vodRouter.navigate(event[0])
  }

  moreShow (datas) {
    document.body.scrollTop = 0
    document.documentElement.scrollTop = 0
    this.router.navigate(['video', 'category', datas[0]])
  }

  goback (backRoute) {
    if (backRoute.indexOf('Home') !== -1) {
      this.router.navigate(['/home'])
    }
  }
}
