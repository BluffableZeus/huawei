import * as _ from 'underscore'
import { Component, Input, OnInit } from '@angular/core'
import { VodFilterService } from './vodFilter.service'
import { ActivatedRoute } from '@angular/router'
import { TranslateService } from '@ngx-translate/core'
import { Router } from '@angular/router'
import { session } from 'src/app/shared/services/session'

@Component({
  selector: 'app-vodfilter',
  styleUrls: ['./vodFilter.component.scss' ],
  templateUrl: './vodFilter.component.html'
})
export class VodFilterComponent implements OnInit {
    // variable definition
  @Input() clickId
  @Input() subjectName
  public genres = []
  public areas = []
  public years = []
  public filterName = ''
  constructor (
        private vodFilterService: VodFilterService,
        private translate: TranslateService,
        public route: ActivatedRoute,
        private router: Router
    ) {}

    /**
    * initialization
    */
  ngOnInit () {
    this.getFilmYears()
    this.route.params.subscribe(params => {
      this.filterName = params['id']
    })
    this.vodFilterService.getContentConfig({}).then(resp => {
        // @HWP-43-DISABLE-FEATURES this.getFilmAreas(_.clone(resp['produceZones']));
      this.getFilmGenres(_.clone(resp['genres']))
    })
  }
    /**
     * circulate the years
     */
  getFilmYears () {
    let time = Date.now()
    let year = time['getFullYear']()
    for (let i = 0; i < 8; i++) {
      this.years.push({ ID: (Number(year) - i).toString(), name: (Number(year) - i).toString() })
    }
    this.years.push({ ID: 'earlier', name: 'earlier' })
  }
    /**
     * get the areas
     */
  getFilmAreas (list) {
    this.areas = _.map(list, (data) => {
      return { ID: data['ID'], name: data['name'] }
    })
  }
    /**
     * get the genres
     */
  getFilmGenres (list) {
    this.genres = list
        .filter(data => data['genreID'] !== '256')
        .map(data => ({ ID: data['genreID'], name: data['genreName'] }))
  }
    /**
     * get the router of the kind of type
     */
  toFilterPage (option, type, index) {
    session.put('filter', 'true')
    session.put('VOD_DETAIL_CRUMBS', location.href)
    this.router.navigate(['video', 'category', this.clickId, 'filter'])
  }
}
