import { Injectable } from '@angular/core'
import { getContentConfig } from 'ng-epg-sdk/vsp'
import { session } from 'src/app/shared/services/session'
@Injectable()
export class VodFilterService {
  constructor () { }
  /**
    * call a interface
    * deal with the data come from the interface
    * get the data from getContentConfig
    */
  getContentConfig (req: any) {
    if (session.get('FILTER_CONTENT_CONFIG')) {
      return Promise.resolve(session.get('FILTER_CONTENT_CONFIG'))
    } else {
      return getContentConfig({
        types: ['1', '2']
      }).then(resp => {
        session.put('FILTER_CONTENT_CONFIG', resp)
        return resp
      })
    }
  }
}
