import * as _ from 'underscore'
import { Injectable } from '@angular/core'
import { getContentConfig, queryVODListBySubject } from 'ng-epg-sdk/vsp'
import { PictureService } from 'ng-epg-ui/services/picture.service'
import { AbortAjaxHook } from 'ng-epg-sdk/hooks'
import { session } from 'src/app/shared/services/session'

@Injectable()
export class VodFilterDetailService {
  /**
    * name for modules that would be used in this module
    */
  constructor (
        private pictureService: PictureService,
        private _abortAjaxHook: AbortAjaxHook) {}
  /**
     * get the years of filter
     */
  excludeYears () {
    // define the time of filter
    let years: Array<string> = []
    let time = Date.now()
    let year = time['getFullYear']()
    for (let i = 0; i <= 7; i++) {
      years.push((Number(year) - i).toString())
    }
    return years
  }
  /**
    * call a interface
    * deal with the data come from the interface
    */
  getContentConfigs (req: any) {
    if (session.get('FILTER_CONTENT_CONFIG')) {
      return Promise.resolve(session.get('FILTER_CONTENT_CONFIG'))
    } else {
      return getContentConfig({
      }).then(resp => {
        session.put('FILTER_CONTENT_CONFIG', resp)
        return resp
      })
    }
  }
  /**
    * get the conditions of filter
    */
  createFilterCondition (filterDataList) {
    // define the variable of filter
    let filter = {}
    let vodFilters = {}
    let sortType = ''
    let genreIDs
    let produceYears
    let produceZoneIDs
    // define the object to set the produceYears
    let vODExcluders = {}
    /**
         * circulate the type of filter
         */
    for (let i = 0; i < filterDataList.length; i++) {
      if (filterDataList[i].type === 'sortBy') {
        sortType = filterDataList[i].ID
        break
      } else {
        sortType = 'new'
      }
    }
    /**
         * circulate the genre of filter
         */
    for (let i = 0; i < filterDataList.length; i++) {
      if (filterDataList[i].type === 'genre') {
        genreIDs = [filterDataList[i].ID]
        break
      } else {
        genreIDs = undefined
      }
    }
    /**
         * circulate the time of filter
         */
    for (let i = 0; i < filterDataList.length; i++) {
      if (filterDataList[i].type === 'year' && filterDataList[i].ID !== 'earlier') {
        produceYears = [filterDataList[i].ID]
        break
      } else if (filterDataList[i].ID === 'earlier') {
        produceYears = undefined
        vODExcluders['produceYears'] = this.excludeYears()
      } else {
        produceYears = undefined
      }
    }
    /**
         * circulate the area of filter
         */
    for (let i = 0; i < filterDataList.length; i++) {
      if (filterDataList[i].type === 'area') {
        produceZoneIDs = [filterDataList[i].ID]
        break
      } else {
        produceZoneIDs = undefined
      }
    }
    /**
         * define the object of filter
         */
    vodFilters = { genreIDs: genreIDs, produceYears: produceYears, produceZoneIDs: produceZoneIDs }
    filter = { sortType: sortType, vodFilters: vodFilters, VODExcluders: vODExcluders }
    return filter
  }
  /**
    * call a interface
    * deal with the data come from the interface
    */
  queryVODListBySubject (offset, ID, count, filter?) {
    let sortType = ''
    if (!filter) {
      sortType = 'STARTTIME:DESC'
    }
    if (filter && filter.sortType === 'new' || filter.sortType === '') {
      sortType = 'STARTTIME:DESC'
    }
    if (filter && filter.sortType === 'top') {
      sortType = 'AVGSCORE:DESC'
    }
    if (filter && filter.sortType === 'hot') {
      sortType = 'PLAYTIMES:DESC'
    }
    let options = { isIgnoreError: true }
    this._abortAjaxHook.abortAjax('QueryVODListBySubject')
    // call a interface
    return queryVODListBySubject({
      subjectID: ID,
      offset: offset,
      count: count,
      sortType: sortType,
      VODFilter: filter ? filter.vodFilters : undefined,
      VODExcluder: filter.VODExcluders
    }, options).then(resp => {
      return resp
    }, resps => {
      return resps
    })
  }
  /**
    * get the filter list
    */
  getFilterVod (viewalsoData, isPrevLoad?: boolean) {
    let list = _.map(viewalsoData, (list1, index) => {
      this.score(list1)
      let url = list1['picture'] && list1['picture'].posters &&
             this.pictureService.convertToSizeUrl(list1['picture'].posters[0],
               { minwidth: 164, minheight: 230, maxwidth: 164, maxheight: 230 })
      list1['focusRoute'] = 'on-demand'
      list1['picture'].posters = [url || 'assets/img/default/home_poster.png']
      let data = {
        vodId: list1['ID'],
        name: list1['name'],
        posterUrl: url,
        score: list1['averageScore'],
        floatInfo: list1,
        subjectID: '-1'
      }
      if (isPrevLoad) {
        data['hidden'] = true
      } else {
        data['hidden'] = false
      }
      return data
    })
    return list
  }
  /**
     * get the averageScore of filter
     */
  score (list) {
    if (list.averageScore) {
      if (list.averageScore.length === 1) {
        list.averageScore += '.0'
      } else {
        if (list.averageScore.substring(0, 2) === '10') {
          list.averageScore = '10'
        } else {
          let score = list.averageScore
          list.averageScore = score.substring(0, 4)
          list.averageScore = Number(list.averageScore)
          list.averageScore = list.averageScore.toFixed(1)
        }
      }
    } else {
      list.averageScore = '0.0'
    }
    return list
  }
}
