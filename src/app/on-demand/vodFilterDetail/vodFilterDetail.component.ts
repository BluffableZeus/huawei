import * as _ from 'underscore'
import { Component, OnInit, OnDestroy } from '@angular/core'
import { VodFilterDetailService } from './vodFilterDetail.service'
import { ActivatedRoute, Router } from '@angular/router'
import { VODRouterService } from 'src/app/shared/services/vod-router.service'
import { TranslateService } from '@ngx-translate/core'
import { EventService } from 'ng-epg-sdk/services'
import { session } from 'src/app/shared/services/session'

@Component({
  selector: 'app-vodfilterdetail',
  styleUrls: ['./vodFilterDetail.component.scss' ],
  templateUrl: './vodFilterDetail.component.html'
})
export class VodFilterDetailComponent implements OnInit, OnDestroy {
  vodFilterLists: {
    'dataList': Array<any>,
    'suspensionID': string
  }
    /**
    * variable definition
    */
  public isShow = 'true'
  public haveVodFilterLists
  public isNodata = false
  public vodFilterList: Array<any> = []
  public showgenreMore: boolean
  public showMoreArea: boolean
  public hasMoreGenre: boolean
  public hasMoreArea: boolean
  public filterName
  public filterType = ''
  public index = ''
  public vodId = ''
  public subjectName = ''
  public sortByIndex = 0
  public areaIndex = 0
  public yearIndex = 0
  public genreIndex = 0
  public sortBy = []
  public area = []
  public year = []
  public genre = []
  public filter = {}
  public genres = []
  public areas = []
  public years = []
  public chosens = []
  public chosensShow = []
  public sortBys
  public total
  public height
  public filterCrumbsRoutes
  public showContent = false
  private visibleLength = 0
  private prevLoadData = []
  private hasGetPrevData = false
  private lastLength = 0
    /**
     * name for modules that would be used in this module
     */
  constructor (
        private vodFilterDetailService: VodFilterDetailService,
        private translate: TranslateService,
        public route: ActivatedRoute,
        private router: Router,
        private vodRouter: VODRouterService
    ) {
  }
    /**
    * initialization
    */
  ngOnInit () {
    this.sortBys = [{
      ID: 'top',
      name: 'top'
    }, {
      ID: 'hot',
      name: 'hot'
    }, {
        ID: 'new',
        name: 'new'
      }]
      /**
        * get the years of initial
        */
    let self = this
    this.getYears()
    this.route.params.subscribe(params => {
      this.filterName = { ID: params['ID'], name: params['name'] }
      this.filterType = params['type']
      this.index = params['index']
      this.vodId = params['id']
      this.subjectName = params['subjectName']
    })
      /**
        * call a interface
        * deal with the data come from the interface
        * get the data from getContentConfig
        */
    this.vodFilterDetailService.getContentConfigs({}).then(resp => {
        /**
            * get the areas and genres of initial
            */
        // @HWP-43-DISABLE-FEATURES this.getAreas(_.clone(resp.produceZones));
      this.getGenres(_.clone(resp.genres))
      if (session.get('filter')) {
        session.pop('leavedata')
        this.selectFilter(this.filterName, this.filterType, this.index)
      } else {
          // define the data from session
        let leavedata = session.get('leavedata')
        if (leavedata && leavedata.length > 0) {
            for (let i = 0; i < leavedata.length - 1; i++) {
              this.selectFilter(leavedata[i], leavedata[i].type, leavedata[i].index, true)
            }
            this.selectFilter(leavedata[leavedata.length - 1],
              leavedata[leavedata.length - 1].type, leavedata[leavedata.length - 1].index)
          } else {
            let list = { ID: 'new', name: 'new' }
            this.selectFilter(list, 'sortBy', 0)
          }
      }
      session.pop('filter')
        /**
            * show the different part
            */
      _.delay(function () {
          // define the current genre height
        let currentGenreTarget = document.getElementById('genre').offsetHeight
        if (currentGenreTarget > 53) {
            self.hasMoreGenre = true
            self.showgenreMore = true
          } else {
            self.hasMoreGenre = false
          }
          // define the current area height
          /* @HWP-43-DISABLE-FEATURES let currentGenreTargetArea = document.getElementById('area').offsetHeight;
                if (currentGenreTargetArea > 55) {
                    self.showMoreArea = true;
                    self.hasMoreArea = true;
                } else {
                    self.showMoreArea = true;
                    self.hasMoreArea = false;
                } */
        self.showContent = true
      }, 10)
    })
      /**
        * the event of screen size change
        */
    EventService.on('ScreenSize', function () {
      if (document.querySelector('.vodFilter-content .genreMoreHidden') &&
                document.querySelector('.vodFilter-content .genreMoreHidden')['offsetHeight']) {
          // define the genreMoreHidden height
        let currentGenreTarget = document.querySelector('.vodFilter-content .genreMoreHidden')['offsetHeight']
        if (currentGenreTarget > 53) {
            self.hasMoreGenre = true
          } else {
            self.hasMoreGenre = false
          }
          // define the areaMoreHidden height
        let currentGenreTargetArea = document.querySelector('.vodFilter-content .areaMoreHidden')['offsetHeight']
        if (currentGenreTargetArea > 53) {
            self.hasMoreArea = true
          } else {
            self.hasMoreArea = false
          }
      }
    })
  }
    /**
     * when exit the component, clear all the timer and events
     */
  ngOnDestroy () {
    session.put('leavedata', this.chosens)
    EventService.removeAllListeners(['LOAD_DATA'])
  }
    /**
    * set the crumbs routers
    */
  setCrumbsRoutes () {
    if (location.href !== session.get('VOD_DETAIL_CRUMBS')) {
      session.put('VOD_DETAIL_CRUMBS_FILTER', session.get('VOD_DETAIL_CRUMBS'))
    } else if (location.href === session.get('VOD_DETAIL_CRUMBS')) {
      session.put('VOD_DETAIL_CRUMBS', session.get('VOD_DETAIL_CRUMBS_FILTER'))
    }
      // get the crumbs data
    let crumbs = [{
      title: this.subjectName || '',
      hash: session.get('VOD_DETAIL_CRUMBS')
    }, {
      title: 'filter',
      hash: ''
    }]
    this.filterCrumbsRoutes = crumbs
  }
    /**
    * get the year of screening conditions
    */
  getYears () {
      // define the time
    let time = Date.now()
    let year = time['getFullYear']()
    this.years.push({ ID: 'All', name: 'all_years' })
    for (let i = 0; i < 8; i++) {
      this.years.push({ ID: (Number(year) - i).toString(), name: (Number(year) - i).toString() })
    }
    this.years.push({ ID: 'earlier', name: 'earlier' })
  }
    /**
    * get the area of screening conditions
    */
  getAreas (list) {
    list.splice(0, 0, { ID: 'All', name: 'searchAll' })
    this.areas = _.map(list, (data) => {
      return { IDs: data['ID'], ID: data['ID'], name: data['name'] }
    })
  }
    /**
    * get the type of screening conditions
    */
  getGenres (list) {
    list.splice(0, 0, { genreID: 'All', genreType: 'all', genreName: 'all_genres' })
    this.genres = list
        .filter(data => data['genreID'] !== '256')
        .map(data => ({ genreIDs: data['genreID'], ID: data['genreID'], name: data['genreName'] }))
  }
    /**
    * click the same type of screening conditions
    */
  selectFilterTwice (type, index, ID) {
    if (type === 'sortBy') {
      this.sortByIndex = index
      this.sortBy.push(ID)
      if (this.sortBy.length > 1) {
        let i = this.getIndex(this.chosens, this.sortBy[0])
        this.chosens.splice(i, 1)
        this.sortBy.shift()
      }
    }
    if (type === 'area') {
      this.areaIndex = index
      if (ID === 'All') {
        let i = this.removeResult(this.chosens, type)
        if (i || i === 0) {
            this.chosens.splice(i, 1)
          }
        this.area = []
      } else {
        this.area.push(ID)
        if (this.area.length > 1) {
            let i = this.getIndex(this.chosens, this.area[0])
            this.chosens.splice(i, 1)
            this.area.shift()
          }
      }
    }
    if (type === 'year') {
      this.yearIndex = index
      if (ID === 'All') {
        let i = this.removeResult(this.chosens, type)
        if (i || i === 0) {
            this.chosens.splice(i, 1)
          }
        this.year = []
      } else {
        this.year.push(ID)
        if (this.year.length > 1) {
            let i = this.getIndex(this.chosens, this.year[0])
            this.chosens.splice(i, 1)
            this.year.shift()
          }
      }
    }
  }
    /**
    * select the type of screening conditions
    */
  selectFilter (list, type, index, flag?) {
    let ID = list.ID
    let name = list.name
    if (ID === 'searchAll') {
      ID = 'All'
    }
    if (ID !== 'All') {
      this.chosens.push({ name: name, ID: ID, type: type, index: index })
    }
    this.selectFilterTwice(type, index, ID)
    if (type === 'genre') {
      this.genreIndex = index
      if (ID === 'All') {
        const ind: number = this.removeResult(this.chosens, type)
        if (ind || ind === 0) {
            this.chosens.splice(ind, 1)
          }
        this.genre = []
      } else {
        this.genre.push(ID)
        if (this.genre.length > 1) {
            const ind: number = this.getIndex(this.chosens, this.genre[0])
            this.chosens.splice(ind, 1)
            this.genre.shift()
          }
      }
    }
    this.chosensShow = _.extend([], this.chosens)
    let i = this.removeResult(this.chosensShow, 'sortBy')
    if (i || i === 0) {
      this.chosensShow.splice(i, 1)
    }
      // define the kind of selectors
    let chosens1
    let chosens2
    let chosens3
    _.map(this.chosensShow, (chosens) => {
      if (chosens.type === 'genre') {
        chosens1 = chosens
      } else if (chosens.type === 'area') {
          chosens2 = chosens
        } else if (chosens.type === 'year') {
          chosens3 = chosens
        }
    })
    this.chosensShow = []
    if (chosens1) {
      this.chosensShow.push(chosens1)
    }
    if (chosens2) {
      this.chosensShow.push(chosens2)
    }
    if (chosens3) {
      this.chosensShow.push(chosens3)
    }
    this.filter = this.vodFilterDetailService.createFilterCondition(this.chosens)
    if (!flag) {
      this.initCategory(this.vodId, this.filter)
    }
    session.put('leavedata', this.chosens)
  }
    /**
    * call a interface
    * deal with the data come from the interface
    * get the filters of screening conditions
    */
  initCategory (ID, filter?) {
    let self = this
    document.body.scrollTop = 0
    document.documentElement.scrollTop = 0
    this.vodFilterList.length = 0
    this.vodFilterLists = {
      'dataList': this.vodFilterList,
      'suspensionID': 'filterVod'
    }
    if (document.querySelector('.vod-filter-load')) {
      document.querySelector('.vod-filter-load')['style']['display'] = 'block'
      this.isNodata = false
    }
      // deal with the data come from the interface
    this.vodFilterDetailService.queryVODListBySubject('0', ID, '30', filter).then(resp => {
      if (this.filter['sortType'] !== filter.sortType) {
        return
      }
      let filterList = this.vodFilterDetailService.getFilterVod(resp.VODs)
      this.vodFilterList = filterList
      _.each(this.vodFilterList, (list, index) => {
        list['titleName'] = this.subjectName
      })
      this.setCrumbsRoutes()
      self.total = Number(resp.total)
      if (filterList.length > 0) {
        this.vodFilterLists = {
            'dataList': filterList,
            'suspensionID': 'filterVod'
          }
        this.haveVodFilterLists = true
        this.isNodata = false
      } else {
        this.haveVodFilterLists = false
        this.isNodata = true
      }
      if (document.querySelector('.vod-filter-load')) {
        document.querySelector('.vod-filter-load')['style']['display'] = 'none'
      }
      this.visibleLength = filterList.length
      this.lastLength = filterList.length
      this.getPrevLoadData()
        // judge the type of browser
      let ua = navigator.userAgent.toLowerCase()
      let isIe = !!ua.match(/msie/i) || !!ua.match(/rv:([\d.]+).*like gecko/)
      let isEdge = !!ua.match(/edge\/([\d.]+)/)
      if (isIe || isEdge) {
        let itemInfoDom = document.querySelectorAll('.items .itemInfo')
        _.each(itemInfoDom, (item) => {
            item['style']['lineHeight'] = 20 + 'px'
          })
      }
    })
      // accept an event to tell the browser to load data
    EventService.removeAllListeners(['LOAD_DATA'])
    EventService.on('LOAD_DATA', function () {
      if (self.visibleLength < self.total) {
        self.makePrevLoadVisible()
      }
    })
  }
    /**
    * call a interface
    * deal with the data come from the interface
    * load all data in advance
    */
  getPrevLoadData () {
    if (this.vodFilterList.length < this.total) {
        // define the all datas length
      let curLength = this.vodFilterList.length + ''
        // deal with the data come from the interface
      this.vodFilterDetailService.queryVODListBySubject(curLength, this.vodId, '30', this.filter).then(resp => {
        this.lastLength = Number(curLength)
        this.prevLoadData = this.vodFilterDetailService.getFilterVod(resp.VODs, true)
        this.vodFilterList = this.vodFilterList.concat(this.prevLoadData)
        _.each(this.vodFilterList, (list, index) => {
            list['titleName'] = this.subjectName
          })
        this.vodFilterLists = {
            'dataList': this.vodFilterList,
            'suspensionID': 'filterVod'
          }
        this.hasGetPrevData = true
      })
    }
  }
    /**
    * control the load logo
    */
  makePrevLoadVisible () {
    if (this.vodFilterList.length === this.lastLength) {
      _.delay(() => {
        this.makePrevLoadVisible()
      }, 30)
    } else {
      if (document.querySelector('.vod-filter-load')) {
        document.querySelector('.vod-filter-load')['style']['display'] = 'block'
      }
      for (let i = this.visibleLength; i < this.vodFilterList.length; i++) {
        this.vodFilterLists['dataList'][i]['hidden'] = false
        if (document.querySelector('#item-' + i) && document.querySelector('#item-' + i)['style']) {
            document.querySelector('#item-' + i)['style']['display'] = 'block'
          }
      }
      this.visibleLength = this.vodFilterList.length
      if (this.vodFilterList && this.vodFilterList.length && this.vodFilterList.length < this.total) {
        this.refreshPrevData()
      } else {
        _.delay(() => {
            if (document.querySelector('.vod-filter-load')) {
              document.querySelector('.vod-filter-load')['style']['display'] = 'none'
            }
          }, 300)
      }
    }
  }
    /**
    * call a interface
    * deal with the data come from the interface
    * refresh the data from filters
    */
  refreshPrevData () {
    let self = this
    if (!this.hasGetPrevData) {
      _.delay(() => {
        self.refreshPrevData()
      }, 30)
    } else {
        // define the current datas length
      let curLength = this.vodFilterList.length
      if (this.vodFilterLists['dataList'][curLength - 1]['hidden']) {
        _.delay(() => {
            self.refreshPrevData()
          }, 30)
      } else if (curLength < this.total) {
          this.hasGetPrevData = false
          // deal with the data come from the interface
          this.vodFilterDetailService.queryVODListBySubject(curLength + '', this.vodId, '30', this.filter).then(resp => {
            self.lastLength = curLength
            if (document.querySelector('.vod-filter-load')) {
              document.querySelector('.vod-filter-load')['style']['display'] = 'none'
            }
            // get the datas in advance
            // get the filterDatas
            self.prevLoadData = self.vodFilterDetailService.getFilterVod(resp.VODs, true)
            self.vodFilterList = self.vodFilterList.concat(self.prevLoadData)
            _.each(self.vodFilterList, (list, index) => {
              list['titleName'] = self.subjectName
            })
            // define the vodFilterLists
            self.vodFilterLists = {
              'dataList': self.vodFilterList,
              'suspensionID': 'filterVod'
            }
            self.hasGetPrevData = true
          })
        }
    }
  }
    /**
    * control the sorts of screening conditions
    */
  changeFilter (chosensShow) {
    if (this.isShow === 'true') {
      this.isShow = 'false'
      if (this.chosensShow.length) {
        this.height = '158px'
      } else {
        this.height = '116px'
      }
    } else {
      this.isShow = 'true'
      this.height = undefined
    }
    if (this.isShow === 'true') {
      if (document.getElementsByClassName('vodFilterList')[0]) {
        document.getElementsByClassName('vodFilterList')[0]['style']['marginTop'] = 20 + 'px'
      }
    } else {
      if (document.getElementsByClassName('vodFilterList')[0]) {
        document.getElementsByClassName('vodFilterList')[0]['style']['marginTop'] = 0
      }
    }
  }
    /**
    * close the chosens of screening conditions
    */
  removeFilter (i, list) {
      // "l" stand for the currentIndex
      // removeName stand for the currentFilterName
    let removeName = this.chosensShow.splice(i, 1)
    let l = this.getIndex(this.chosens, list.ID)
    this.chosens.splice(l, 1)
    if (removeName[0].type === 'year') {
      this.year = []
      this.yearIndex = 0
    }
    if (removeName[0].type === 'genre') {
      this.genre = []
      this.genreIndex = 0
    }
    if (removeName[0].type === 'area') {
      this.area = []
      this.areaIndex = 0
    }
    this.filter = this.vodFilterDetailService.createFilterCondition(this.chosens)
    if (this.isShow === 'false') {
      if (this.chosensShow.length) {
        this.height = '158px'
      } else {
        this.height = '116px'
      }
    }
    this.initCategory(this.vodId, this.filter)
  }
    /**
    * remove the same type of screening conditions
    */
  removeResult (chosens, list) {
    for (let i = 0; i < chosens.length; i++) {
      if (chosens[i].type === list) {
        return i
      }
    }
  }
    /**
    * get the currentIndex of screening conditions
    */
  getIndex (chosens, list) {
    for (let i = 0; i < chosens.length; i++) {
      if (chosens[i].ID === list) {
        return i
      }
    }
  }
    /**
    * get the url of the waterfall-vod
    */
  vodDtail (event) {
    this.vodRouter.navigate(event[0])
  }
    /**
    * get more genres of screening conditions
    */
  showMoreGenreInfo () {
    this.showgenreMore = this.showgenreMore === false
  }
    /**
    * get more areas of screening conditions
    */
  showMoreAreaInfo () {
    this.showMoreArea = this.showMoreArea === false
  }
    /**
    * set style of content null
    */
  allNoDataHeight () {
      // define the height of "noData"
    let height = ''
    if (document.getElementById('vodFilterDetail') && document.getElementById('header-wrap')) {
      height = document.getElementById('vodFilterDetail')['clientHeight'] -
                document.getElementById('header-wrap')['clientHeight'] + 'px'
    }
    return height
  }
}
