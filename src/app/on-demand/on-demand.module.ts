import { TranslateModule } from '@ngx-translate/core'
import { VODPlayerDrop } from './voddetail/vod-player/vod-player-drop.service'
import { TopWeekService } from './vodhome/top-week/top-week.service'
import { PlayVodService } from './voddetail/vod-player/vod-player.service'
import { StillsMoreComponent } from './voddetail/stills-vod/stills-vod-more/stills-vod-more.component'
import { CastMoreComponent } from './voddetail/cast-more/cast-more.component'
import { VodFilterService } from './vodFilter/vodFilter.service'
import { RecomvodAllService } from './recomvodAll/recomvodAll.service'
import { VodFilterComponent } from './vodFilter/vodFilter.component'
import { RecomVodMoreComponent } from './recomvodAll/recomvodAll.component'
import { VodFilterDetailService } from './vodFilterDetail/vodFilterDetail.service'
import { VodFilterDetailComponent } from './vodFilterDetail/vodFilterDetail.component'
import { SeasonsComponent } from './voddetail/seasons/seasons.component'
import { ScoreService } from './voddetail/score/score.service'
import { ShareVodComponent } from './voddetail/share-vod/share.component'
import { ScoreComponent } from './voddetail/score/score.component'
import { FavoriteVodComponent } from './voddetail/favorite-vod/favorite.component'
import { VodDetailService } from './voddetail/vod-detail/vod-detail.service'
import { VodDetailComponent } from './voddetail/vod-detail/vod-detail.component'
import { CastService } from './voddetail/cast-vod/cast-vod.service'
import { CastShowComponent } from './voddetail/cast-vod/cast-vod.component'
import { VodEpisodeComponent } from './voddetail/vod-episode/vod-episode.component'
import { VodDetailHomeService } from './voddetail/vodDetail.service'
import { ComponentModule } from '../component/component.module'
import { VodPlayerComponent } from './voddetail/vod-player/vod-player.component'
import { ViewAlsoService } from './voddetail/viewalso-vod/viewalso-vod.service'
import { ViewAlsoComponent } from './voddetail/viewalso-vod'
import { LikeManyService } from './voddetail/likemany-vod/likemany-vod.service'
import { WaterfallVodModule } from 'ng-epg-ui/webtv-components/waterfall-vod/waterfall-vod.module'
import { LikeManyComponent } from './voddetail/likemany-vod'
import { VodStillModule } from 'ng-epg-ui/webtv-components/vod-still/vod-still.module'
import { StillsVODComponent } from './voddetail/stills-vod/stills-vod.component'
import { VoddetailHomeComponent } from './voddetail/voddetail.component'
import { ChildColumnShowModule } from 'ng-epg-ui/webtv-components/childcolumnshow'
import { VODSuspensionModule } from 'ng-epg-ui/webtv-components/vodSuspension/vod-suspension.module'
import { NoDataModule } from 'ng-epg-ui/webtv-components/noData/no-data.module'
import { SharedModule } from '../shared/shared.module'
import { OnDemandService } from './vodhome/on-demand.service'
import { RecomVod } from './vodhome/ondemand-recomvod/recomvod.component'
import { TopThisWeekComponent } from './vodhome/top-week/top-week.component'
import { CategoryHomeComponent } from './vodhome/category-home/category-home.component'
import { OnDemandComponent } from './vodhome/on-demand.component'
import { SeriesComponent } from './content/series/series.component'
import { SeasonComponent } from './content/season/season.component'
import { EpisodeComponent } from './content/episode/episode.component'
import { MovieComponent } from './content/movie/movie.component'
import { ContentCastComponent } from './content/shared/content-cast/content-cast.component'
import { ContentItemListComponent } from './content/shared/content-item-list/content-item-list.component'
import { ContentPreviewListComponent } from './content/shared/content-preview-list/content-preview-list.component'
import { ContentDropdownListComponent } from './content/shared/content-dropdown-list/content-dropdown-list.component'
import { ContentIntroduceComponent } from './content/shared/content-introduce/content-introduce.component'
import { NgModule } from '@angular/core'
import { OnDemandRoutingModule } from './on-demand.routing.module'
import { CrumbsModule } from 'ng-epg-ui/webtv-components/crumbs/crumbs.module'
// import { QRCodeModule } from 'angular2-qrcode'
import { ContentTrailerModule } from 'src/app/on-demand/content/shared/content-trailer'
import { ContentScoreInfoComponent } from 'src/app/on-demand/content/shared/content-score-info/content-score-info.component'
import { ContentTitleComponent } from 'src/app/on-demand/content/shared/content-title/content-title.component'
import { ContentGenresComponent } from 'src/app/on-demand/content/shared/content-genres/content-genres.component'
import { ContentPlayerComponent } from './content/shared/content-player/content-player.component'
import { ContentMetaComponent } from './content/shared/content-meta/content-meta.component'
import { ShareSocialComponent } from 'src/app/share-social/share-social/share-social.component'
import 'src/assets/lib/share-yandex.js'

import '/src/assets/lib/LibDMPPlayer'
import '/src/assets/lib/DMPPlayer'
import { SectionService } from '../home/sections/section.service';

@NgModule({
  imports: [
    WaterfallVodModule,
    VodStillModule,
    VODSuspensionModule,
    NoDataModule,
    // ChildColumnShowModule,
    OnDemandRoutingModule,
    SharedModule,
    ComponentModule,
    TranslateModule,
    CrumbsModule,
    // QRCodeModule,
    ContentTrailerModule
  ],
  exports: [],
  declarations: [
    OnDemandComponent,
    CategoryHomeComponent,
    TopThisWeekComponent,
    RecomVod,
// VodCastComponent,
    VoddetailHomeComponent,
    StillsVODComponent,
    LikeManyComponent,
    ViewAlsoComponent,
    VodPlayerComponent,
    VodEpisodeComponent,
    CastShowComponent,
    VodDetailComponent,
    FavoriteVodComponent,
    ScoreComponent,
    ShareVodComponent,
    SeasonsComponent,
    VodFilterDetailComponent,
    RecomVodMoreComponent,
    VodFilterComponent,
    CastMoreComponent,
    StillsMoreComponent,
    SeriesComponent,
    SeasonComponent,
    EpisodeComponent,
    MovieComponent,
    ContentCastComponent,
    ContentItemListComponent,
    ContentPreviewListComponent,
    ContentDropdownListComponent,
    ContentIntroduceComponent,
    ContentScoreInfoComponent,
    ContentPlayerComponent,
    ContentTitleComponent,
    ContentGenresComponent,
    ContentMetaComponent,
    ShareSocialComponent
  ],
  providers: [
    OnDemandService,
    LikeManyService,
    ViewAlsoService,
    VodDetailHomeService,
    VodDetailService,
    ScoreService,
    CastService,
    VodFilterDetailService,
    RecomvodAllService,
    VodFilterService,
    CastService,
    PlayVodService,
    TopWeekService,
    VODPlayerDrop,
    SectionService
  ]
})
export class OnDemandModule { }
