import { Input, Output, Component, EventEmitter, OnInit, ElementRef } from '@angular/core'
import { TranslateService } from '@ngx-translate/core'

@Component({
  selector: 'content-preview-list',
  templateUrl: './content-preview-list.component.html',
  styleUrls: ['../../content.common.scss', 'content-preview-list.component.scss']
})

export class ContentPreviewListComponent implements OnInit {
  public windowWidth = window.innerWidth
  public scrollIndex = 0
  private itemWidth = (window.innerWidth <= 1120) ? 236 : 324
  private scrollTimer = null
  private currentItem = null
  private showPopup = false
  private popupInverse = false

  @Input() titleKey = ''
  @Input() itemKey = ''

  @Input() short = false
  @Input() allowMobile = false

  @Input() season = 1
  @Input() items = []
  @Input() currentPosition = 0

  @Output() onSelect: EventEmitter<any> = new EventEmitter()

  constructor (
    public translate: TranslateService,
    private el: ElementRef
  ) {
  }

  ngOnInit () {
    this.scrollIndex = Math.max(0, this.currentPosition - 1)
  }

  itemPosition (item) {
    return parseInt(item.sitcomNO, 10)
  }

  epMinutes (item) {
    if (!item.VOD.mediaFiles) {
      return
    }

    return Math.floor(item.VOD.mediaFiles[0].elapseTime / 60)
  }

  epSeconds (item) {
    if (!item.VOD.mediaFiles) {
      return
    }

    return item.VOD.mediaFiles[0].elapseTime - this.epMinutes(item) * 60
  }

  setItem (item) {
    this.onSelect.emit(item)
  }

  index (item) {
    return {
      num: item.sitcomNO
    }
  }

  scroll (offset, sustain?) {
    const min = 0,
      max = this.items.length - this.swipePower()

    this.scrollIndex = Math.max(min, Math.min(max, this.scrollIndex + offset))

    if (this.firstScroll() || this.lastScroll()) {
      this.scrollStop()
      sustain = false
    }

    if (sustain) {
      this.scrollTimer = setInterval(() => {
        this.scroll(offset)
      }, 250)
    }
  }

  firstScroll () {
    return this.scrollIndex <= 0
  }

  lastScroll () {
    return this.scrollIndex >= this.items.length - this.swipePower()
  }

  scrollStop () {
    clearInterval(this.scrollTimer)
    this.scrollTimer = null
  }

  getTransform () {
    if (this.items.length < 1) {
      this.scrollIndex = 0
    }

    return 'translateX(' + (this.scrollIndex * -this.itemWidth) + 'px)'
  }

  getPopupLeft () {
    // let's some magic calculations
    return ((this.items.indexOf(this.currentItem) + 1) * this.itemWidth - this.scrollIndex * this.itemWidth + 60) + 'px'
  }

  showDesc (item) {
    // to prevent from showing popup on mobile\tablet screens
    if (this.windowWidth < 1120) {
      this.showPopup = false
      return
    }
    this.currentItem = item
    this.showPopup = true
    setTimeout(() => {
      this.popupInverse = !this.isPopupFits()
    })
  }

  isPopupFits () {
    const container = this.el.nativeElement.querySelector('.preview-list'),
      popup = container.querySelector('.preview-item-info') as HTMLElement

    // I like magic calculations
    return parseInt(popup.style.left, 10) + popup.offsetWidth < container.offsetWidth + 60
  }

  image (item) {
    if (!item.VOD.picture) {
      return ''
    }

    return item.VOD.picture.drafts[0] || ''
  }

  getProgress (item) {
    if (!item.VOD.bookmark) {
      return 0
    }

    return ((item.VOD.bookmark.rangeTime / item.VOD.mediaFiles[0].elapseTime) * 100) + '%'
  }

  swipePower () {
    const container = this.el.nativeElement.querySelector('.preview-list')
    return Math.floor(container.offsetWidth / this.itemWidth)
  }

  rightArrowHidden () {
    return this.scrollIndex >= this.items.length - Math.floor(this.el.nativeElement.querySelector('.preview-list').offsetWidth / this.itemWidth)
  }
}
