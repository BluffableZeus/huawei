import { Input, Component, OnInit } from '@angular/core'
import * as _ from 'underscore'
import { VOD } from 'ng-epg-sdk'

@Component({
  selector: 'content-score-info',
  templateUrl: './content-score-info.component.html',
  styleUrls: ['../../content.common.scss']
})

export class ContentScoreInfoComponent implements OnInit {
  @Input() data: VOD
  public starArray: number[] = []
  public scoreTimes: 0
  public ProduceZoneName = ''
  public RatingName = ''
  public definitions = []
  public produceDate = ''
  public rating = '0'
  public score = 0

  constructor () { }

  ngOnInit () {
    this.rating = this.data.averageScore
    this.score = Number(this.data.scoreTimes)
    this.ProduceZoneName = this.data.produceZone && this.data.produceZone.name
    this.RatingName = this.data.rating.name && this.data.rating.name || '0+'
    this.produceDate = this.data.produceDate && this.data.produceDate.substring(0, 4)
    this.refreshRatingStar(this.rating)
    if (this.data['mediaFiles'] && this.data['mediaFiles'].length > 0) {
      this.definitions = []
      _.each(this.data['mediaFiles'], item => {
        this.definitions.push(this.definitionFormat(item['definition']))
      })
    }
  }

    /**
     * refresh rating star according to the averagescore
     * @param averscore score that get from data
     */
  refreshRatingStar (averscore?) {
    this.rating = Number(averscore).toFixed(1)
    if (parseInt(this.rating, 10) === 10) {
      this.rating = '10'
    }
    for (let i = 0; i < 5; i++) {
      let result = Math.round(parseFloat(this.rating)) - (2 * (i + 1))
      if (result >= 0) {
        this.starArray[i] = 2
      } else if (result === -1) {
          this.starArray[i] = 1
        } else {
          this.starArray[i] = 0
        }
    }
  }
    /**
     * choose different articulation
     * @param definition help to choose articulation
     */
  definitionFormat (definition) {
    switch (definition) {
      case '0':
        return 'SD'
      case '1':
        return 'HD'
      case '2':
        return '4K'
      default:
        return ''
    }
  }
}
