import { VOD } from 'ng-epg-sdk/vsp/api'
import { VODRouterService } from 'src/app/shared/services/vod-router.service'
import { Input, Component, OnInit } from '@angular/core'
import * as _ from 'underscore'

@Component({
  selector: 'content-genres',
  templateUrl: './content-genres.component.html',
  styleUrls: ['../../content.common.scss']
})

export class ContentGenresComponent implements OnInit {
  @Input() seasonGenres: any

  constructor (
    ) { }

  ngOnInit () {
  }
}
