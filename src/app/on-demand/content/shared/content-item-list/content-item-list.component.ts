import { GetVODDetailResponse } from 'ng-epg-sdk/vsp/api'
import { VODRouterService } from 'src/app/shared/services/vod-router.service'
import { Input, Component } from '@angular/core'
import { UpperCasePipe } from '@angular/common'

@Component({
  selector: 'content-item-list',
  templateUrl: './content-item-list.component.html',
  styleUrls: ['../../content.common.scss']
})

export class ContentItemListComponent {
  constructor (
        private vodRouter: VODRouterService
  ) { }

  @Input() titleKey: string = ''
  @Input() items = []
  @Input() currentPosition: number = 0

  itemPosition (item) {
    return parseInt(item.sitcomNO)
  }

  vodNavigation (data) {
    this.vodRouter.navigate(data.ID)
  }
}
