import { TranslateModule } from '@ngx-translate/core'
import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'
import { PictureService } from 'ng-epg-ui/services/picture.service'
import { DirectionScroll } from 'ng-epg-ui/webtv-components/scroll/directionScroll.component'
import { TransverseScroll } from 'ng-epg-ui/webtv-components/scroll/transverseScroll.component'

import { ContentTrailerComponent } from './content-trailer.component'

@NgModule({
  imports: [CommonModule, TranslateModule],
  exports: [ContentTrailerComponent],
  declarations: [ContentTrailerComponent],
  providers: [PictureService, DirectionScroll, TransverseScroll]
})
export class ContentTrailerModule { }
