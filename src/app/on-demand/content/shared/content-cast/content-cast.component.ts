import { Router } from '@angular/router'
import { Input, Component, OnInit } from '@angular/core'
import * as _ from 'underscore'
import { session } from 'src/app/shared/services/session'

@Component({
  selector: 'content-cast',
  templateUrl: './content-cast.component.html',
  styleUrls: ['../../content.common.scss']
})

export class ContentCastComponent implements OnInit {
  public list = []
  constructor (
        private router: Router
    ) { }

  @Input() castRoles = []
  @Input() neededRole = null
  @Input() full = false

    // 0: cast member Актеры
    // 1: director Режиссеры
    // 2: composer or lyricist Композиторы
    // 3: singer Певцы
    // 4: producer Продюссеры
    // 5: screenwriter Сценаристы
    // 6: commentator Комментаторы
    // 7: owner Владельцы
    // 8: dresser Костюмеры
    // 9: sound engineer Звукорежиссеры
    // 10: Compere Ведущие
    // 11: Guest Гости
    // 12: Reporter Корреспонденты
    // 100: others Прочие
  public roleTranslations = {
    '0': 'cast_names.actors',
    '1': 'cast_names.directors',
    '2': 'cast_names.composer',
    '3': 'cast_names.singer',
    '4': 'cast_names.producer',
    '5': 'cast_names.screenwriter',
    '6': 'cast_names.commentator',
    '7': 'cast_names.owner',
    '8': 'cast_names.dresser',
    '9': 'cast_names.sound',
    '10': 'cast_names.compere',
    '11': 'cast_names.guest',
    '12': 'cast_names.reporter',
    '100': 'cast_names.other'
  }

  ngOnInit () {
    this.filterPersons()
  }

  filterPersons () {
    if (!this.neededRole) {

    } else {
      this.castRoles = _.filter(this.castRoles, (castRole) => {
        return castRole.roleType == this.neededRole
      })
    }
  }

  personNavigation (cast) {
    session.put('castVodRole', cast.castName, true)
    session.put('headUrl', undefined, true)
    session.put('CAST_ID_DATA', cast.castID, true)
    this.router.navigate(['cast', cast.castID])
  }

  log (val) {
    console.log('content-cast logging', val)
  }
}
