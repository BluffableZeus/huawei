import { Input, Component, OnInit, ElementRef } from '@angular/core'

@Component({
  selector: 'content-introduce',
  templateUrl: './content-introduce.component.html',
  styleUrls: ['../../content.common.scss']
})
export class ContentIntroduceComponent implements OnInit {
  @Input() introduce = ''
  @Input() full = false
  @Input() noDivider = false

  private collapsedSize = 38

  constructor (private element: ElementRef) {}

  ngOnInit () {
  }
    /**
     * opening and closing introduce div
     */
  toggleIntroduce () {
    if (this.full) return

    const introduce = this.element.nativeElement.querySelector('.introduce'),
      introduceToggle = introduce.querySelector('.introduce-toggle')

    if (introduce.clientHeight === this.collapsedSize) {
      introduce.style.height = 'auto'
      introduce.style.transition = 'none'

      const h = introduce.clientHeight

      introduce.style.height = this.collapsedSize + 'px'
      introduce.style.transition = '' // drop to css
      setTimeout(() => { introduce.style.height = h + 'px' }, 10)

      introduceToggle.classList.remove('introduce-more')
      introduceToggle.classList.add('introduce-less')
    } else {
      introduce.style.height = this.collapsedSize + 'px'
      introduceToggle.classList.remove('introduce-less')
      introduceToggle.classList.add('introduce-more')
    }
  }
}
