import { session } from '../../../../../../ng-epg-ui/demos-app/sdk/session'
import { VODRouterService } from '../../../../shared/services/vod-router.service'
import { Component, Input, OnInit } from '@angular/core'
import * as _ from 'underscore'
import { VOD, EventService } from 'ng-epg-sdk'
import { AccountLoginComponent } from '../../../../login/account-login/account-login.component'
import { CommonService } from '../../../../core/common.service'

@Component({
  selector: 'content-title',
  templateUrl: './content-title.component.html',
  styleUrls: ['../../content.common.scss']
})

export class ContentTitleComponent implements OnInit {
  @Input() data: any
  @Input() parent: boolean
  public isScoreShow: boolean
  public isShowShare: boolean
  public addFavorite: string
  public isFav: string
  public leaveScore
  public leaveShare
  public eventflag: boolean
  public eventflagScore: boolean
  public score: string
  public scoreShow = 'false'
  public info: any

  constructor (
        private vodRouter: VODRouterService,
        private commonService: CommonService
    ) { }

  ngOnInit () {
    this.isFav = this.data && this.data.favorite && '1' || '0'
    this.info = {
      'contentIDs': this.data['ID'],
      'contentTypes': this.data['contentType'],
      'userScore': this.data['userScore'] || 0
    }
  }

    /**
     * add or remove favorite
     */
  favoriteManger () {
    let self = this
      // guest login
    if (Cookies.getJSON('IS_GUEST_LOGIN')) {
      EventService.emit('CLOSED_LOGIN_DIALOG')
      EventService.on('MD_DIALOG_CONTAINER', function () {
        document.getElementsByTagName('md-dialog-container')[0]['style'].position = 'fixed'
      })
      this.commonService.openDialog(AccountLoginComponent)
      return
    }
      // the vod is added favorite, need to remove
    if (this.isFav === '1') {
      let req = {
        contentIDs: [self.data['ID']],
        contentTypes: ['VOD']
      }
        // call an interface
      self.commonService.removeFavorite(req).then(resp => {
        if (resp['result']['retCode'] === '000000000') {
            self.isFav = '0'
            this.addFavorite = 'true'
            // document.getElementById('favoriteIcon').style.background = 'url(assets/img/20/ondemand_rio2-iocn4_20x20.png)';
            EventService.emit('REMOVEVODDETAIL_successful', {})
          }
      })
        // the vod isn't added favorite, need to add
    } else {
      let req = {
        favorites: [{
            contentID: self.data['ID'],
            contentType: 'VOD'
          }],
        autoCover: '0'
      }
        // call an interface
      self.commonService.addFavorite(req).then(resp => {
        if (resp['result']['retCode'] === '000000000') {
            self.isFav = '1'
            this.addFavorite = 'false'
            // document.getElementById('favoriteIcon').style.background = 'url(assets/img/20/ondemand_rio2-iocn_2.png)';
            EventService.emit('ADDVODDETAIL_successful', {})
          }
      })
    }
    console.log(this.data.favorite)
    console.log(this.isFav)
  }

    /**
     * diaplay the share suspension frame
     */
  mouseenterShare () {
    this.isShowShare = true
  }

    /**
     * hide the share suspension frame
     */
  mouseleaveShare () {
    let self = this
    EventService.on('ISSHOW_SHARE', function (boo) {
      self.eventflag = true
      if (boo) {
        self.eventflag = true
        self.isShowShare = true
      } else if (!boo) {
          self.eventflag = false
          clearTimeout(self.leaveShare)
          self.leaveShare = setTimeout(() => {
            self.isShowShare = false
          }, 300)
        }
    })
    clearTimeout(self.leaveShare)
    self.leaveShare = setTimeout(() => {
      if (!self.eventflag) {
        self.isShowShare = false
      }
    }, 300)
  }

  scoreClick (event) {
    let self = this
      // is guest login
    if (Cookies.getJSON('IS_GUEST_LOGIN')) {
      EventService.emit('CLOSED_LOGIN_DIALOG')
      EventService.on('MD_DIALOG_CONTAINER', function () {
        document.getElementsByTagName('md-dialog-container')[0]['style'].position = 'fixed'
      })
      this.commonService.openDialog(AccountLoginComponent)
      return
    }
    _.delay(function () {
      if (!_.isUndefined(session.get('scoreValue'))) {
        self.score = 'true'
      } else {
        self.score = 'false'
      }
    }, 0)
  }

    /**
     * diaplay the score suspension frame
     */
  mouseenterScore () {
    this.scoreShow = 'true'
    this.isScoreShow = true
    EventService.emit('IS_SHOWSCORE', false)
  }

    /**
     * hide the score suspension frame
     */
  mouseleaveScore () {
    let self = this
    EventService.on('SHOW_SCORE', function (boo) {
      self.eventflagScore = true
      if (boo) {
        self.eventflagScore = true
        self.isScoreShow = true
        self.scoreShow = 'true'
      } else if (!boo) {
          self.eventflagScore = false
          clearTimeout(self.leaveScore)
          self.leaveScore = setTimeout(() => {
            self.isScoreShow = false
          }, 300)
        }
    })
    clearTimeout(self.leaveScore)
    self.leaveScore = setTimeout(() => {
      if (!self.eventflagScore) {
        self.isScoreShow = false
        this.scoreShow = 'false'
      }
    }, 300)
  }

  vodNavigation () {
    if (this.parent) {
      this.vodRouter.navigate(this.data.series[0].VODID)
    }
  }

  getnewscore () {
    this.data.coreTimes += 1
  }
}
