import { Input, Output, Component, EventEmitter, OnInit } from '@angular/core'

@Component({
  selector: 'content-dropdown-list',
  templateUrl: './content-dropdown-list.component.html',
  styleUrls: ['../../content.common.scss', 'content-dropdown-list.component.scss']
})

export class ContentDropdownListComponent implements OnInit {
  public open = false
  public selected = 0

  private height = 20
  private itemsHeight = 20

  @Input() titleKey = ''
  @Input() items = []
  @Input() currentPosition = 0

  @Output() onSelect: EventEmitter<any> = new EventEmitter()

  constructor () {}

  ngOnInit () {
    this.items.map((item, index) => {
      if (this.itemPosition(item) === this.currentPosition) {
        this.selected = index
      }

      this.itemsHeight = this.height = document.getElementById('dropdown-selector').offsetHeight
    })
  }

  itemPosition (item) {
    return parseInt(item.sitcomNO, 10)
  }

  setItem (item) {
    this.selected = this.items.indexOf(item)
    this.toggle(false)
    this.onSelect.emit(item)
  }

  getStyle () {
    return {
      'height': (this.itemsHeight) + 'px'
    }
  }

  getItemsStyle () {
    const shift = (this.open) ? 0 : this.selected * -this.height
    return {
      'transform': 'translateY(' + shift + 'px)'
    }
  }

  toggle (val?) {
    const newVal = val || !this.open

    this.itemsHeight = this.height = document.getElementById('dropdown-selector').offsetHeight

    setTimeout(() => {
      this.open = newVal
      this.itemsHeight = (this.open) ? this.height * this.items.length : this.height
    })
  }
}
