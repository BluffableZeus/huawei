import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core'
import { EventService, VODDetail } from 'ng-epg-sdk'
import { PictureService } from 'ng-epg-ui/services'
import { MobileService } from '../../../../shared/services/mobile.service'
import { session } from '../../../../shared'
import { AccountLoginComponent } from '../../../../login/account-login'
import { ViewTransferService } from '../../../../core/view-transfer.service'

@Component({
  selector: 'app-content-player',
  templateUrl: './content-player.component.html',
  styleUrls: ['./content-player.component.scss']
})
export class ContentPlayerComponent implements OnInit {
  private _enabled = false
  private _data: VODDetail

  @Output() enabledChanged: EventEmitter<boolean> = new EventEmitter<boolean>()
  @Input() get enabled (): boolean {
    return this._enabled
  }
  @Input() episodes = []
  @Input() picture = []

  set enabled (value: boolean) {
    this._enabled = value
    this.enabledChanged.emit(value)
  }

  @Input() get data (): VODDetail {
    return this._data
  }

  set data (value: VODDetail) {
    this._data = value
  }

  constructor (
        protected pictureService: PictureService,
        private viewTransferService: ViewTransferService,
        public mobile: MobileService
    ) {
  }

  ngOnInit () {
    EventService.on('AUTHENTICATE_FULL_SUCCESS', () => {
      this.play()
    })
    EventService.on('XVOD_PLAY_TRAILER', data => {
      this.enabled = true
      setTimeout(() => {
        EventService.emit('playTra', data)
      })
    })
    EventService.removeAllListeners(['SERIES_EXIT_FULLSCREEN'])
    EventService.on('SERIES_EXIT_FULLSCREEN', () => {
      this.enabled = false
    })
    EventService.removeAllListeners(['SERIES_PLAY_FULLSCREEN'])
    EventService.on('SERIES_PLAY_FULLSCREEN', () => {
      this.play()
    })
    EventService.removeAllListeners(['CLOSE_SUBSCRIBE_DIALOG'])
    EventService.on('CLOSE_SUBSCRIBE_DIALOG', () => {
      this.enabled = false
    })
  }

  play () {

    if (!Cookies.getJSON('IS_PROFILE_LOGIN')) {
      return this.viewTransferService.openDialog(AccountLoginComponent)
    }

    if (this.data.isSubscribed === '0' && !Cookies.getJSON('IS_GUEST_LOGIN')) {
      session.put('buy', true)
    }
    this.enabled = true
  }
}
