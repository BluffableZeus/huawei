import { Input, Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { Meta } from '@angular/platform-browser'
import { config } from '../../../../shared/services/config'
import { VODRouterService } from '../../../../shared/services/vod-router.service'

@Component({
  selector: 'content-meta',
  templateUrl: './content-meta.component.html'
})
export class ContentMetaComponent implements OnInit {
  @Input() series: any
  @Input() season: any
  @Input() get data (): any {
    return this._data
  }

  set data (value: any) {
    this._data = value
    this.tagsAppend()
  }

  private _data: any
  public roleTranslations = {
    '0': 'actors',
    '1': 'directors',
    '2': 'composer',
    '3': 'singer',
    '4': 'producer',
    '5': 'screenwriter',
    '6': 'commentator',
    '7': 'owner',
    '8': 'dresser',
    '9': 'sound',
    '10': 'compere',
    '11': 'guest',
    '12': 'reporter',
    '100': 'other'
  }

  constructor (private router: Router,
                private meta: Meta,
                private vodRouter: VODRouterService) {
  }

  ngOnInit () {
    this.tagsAppend()
  }

  ngOnDestroy () {
    this.removeTags()
  }
  tagsAppend () {
    this.removeTags()
    this.meta.addTags([
        { name: 'og:title', content: this._data.name },
        { name: 'og:site_name', content: config.site_name },
        { name: 'og:type', content: 'video' },
        { name: 'og:description', content: this._data && this._data.introduce || 'Empty' },
        { name: 'og:image', content: this._data.picture && this._data.picture.drafts[0] || config.base_url + '/assets/img/default/home_poster.png' },
        { name: 'og:url', content: config.base_url + this.router.url },
        { name: 'og:site_name', content: config.site_name },
        { name: 'id', content: this._data.ID },
        { name: 'age_rating', content: this._data.rating && this._data.rating.name || '0+' },
        { name: 'rating', content: this._data.averageScore },
      {
        name: 'genres',
        content: this._data.genres.map(genre => {
          return genre.genreName
        }) || 'N.A.'
      }
    ])

    if (this._data.produceZone && this._data.produceZone.ID) {
      this.meta.addTag({ name: 'produced_zone', content: this._data.produceZone.ID })
    }
    if (this._data.produceDate) {
      this.meta.addTag({ name: 'produced_date', content: this._data.produceDate.substring(0, 4) })
    }

    if (this.season) {
      this.meta.addTags([
          { name: 'related_season_name', content: this.season && this.season.name },
          { name: 'related_season_id', content: this.season && this.season.ID },
        {name: 'related_season_url',
          content: config.base_url + `/series/${this.series &&
                this.series.ID}/season/${this.season.series[0].sitcomNO}` }
      ])
    }
    if (this.series) {
      this.meta.addTags([
          { name: 'related_series_name', content: this.series && this.series.name },
          { name: 'related_series_id', content: this.series && this.series.ID },
        {name: 'related_series_url',
          content: config.base_url + `/series/${this.series &&
                this.series.ID}` }])
    }
    this._data.castRoles.forEach(role => {
      this.meta.addTag({
        name: this.roleTranslations[role.roleType],
        content: role.casts.slice(0, 3).map(human => {
            return human.castName && human.castName.toString() || 'N.A.'
          })
      })
    })
    if (!(this._data.series && this._data.series) || +this._data.VODType === 2) {
      this.meta.removeTag("name='related_series_name'")
      this.meta.removeTag("name='related_series_id'")
      this.meta.removeTag("name='related_series_url'")
      this.meta.removeTag("name='related_season_name'")
      this.meta.removeTag("name='related_season_id'")
      this.meta.removeTag("name='related_season_url'")
    }
  }

  removeTags () {
    this.meta.removeTag("name='og:title'")
    this.meta.removeTag("name='og:site_name'")
    this.meta.removeTag("name='og:url'")
    this.meta.removeTag("name='og:description'")
    this.meta.removeTag("name='og:type'")
    this.meta.removeTag("name='og:image'")
    this.meta.removeTag("name='id'")
    this.meta.removeTag("name='related_series_name'")
    this.meta.removeTag("name='related_series_id'")
    this.meta.removeTag("name='related_series_url'")
    this.meta.removeTag("name='related_season_name'")
    this.meta.removeTag("name='related_season_id'")
    this.meta.removeTag("name='related_season_url'")
    this.meta.removeTag("name='age_rating'")
    this.meta.removeTag("name='rating'")
    this.meta.removeTag("name='produced_zone'")
    this.meta.removeTag("name='produced_date'")
    this.meta.removeTag("name='genres'")
    this._data.castRoles.forEach(role => {
      this.meta.removeTag(`name='${this.roleTranslations[role.roleType]}'`)
    })
  }
}
