import { ActivatedRoute } from '@angular/router'
import { VODRouterService } from '../../../shared/services/vod-router.service'
import { Component, OnInit } from '@angular/core'
import { VOD, VODDetail } from 'ng-epg-sdk'
import { PictureService } from 'ng-epg-ui/services'

@Component({
  selector: 'app-content-season',
  templateUrl: './season.component.html',
  styleUrls: ['../content.common.scss']
})

export class SeasonComponent implements OnInit {
  public series: VODDetail = null
  public seasonEpisodes: any[] = []
  public isFav: string
  public addFavorite: string
  public seasonsList = []
  public seasonList: any[]
  public seriesName = ''
  public isLoaded = false
  public seriesId = ''
  public seasonNumber: number
  public VODId = ''
  public VODDetail: VODDetail
  public seasonName = ''
  public seasonGenres = []
  public seasonId = '0'
  public starArray: number[] = []
  public picture = ''
  public firstSeasonSerieId: VOD = null

  constructor (
        private vodRouter: VODRouterService,
        private route: ActivatedRoute,
        protected pictureService: PictureService
    ) {
  }

  ngOnInit () {
    this.route.params.subscribe(params => {
      this.seriesId = params['series_id']
      this.seasonNumber = params['season_number']
      this.vodRouter.getSeasonByNumber(this.seriesId, this.seasonNumber).then(resp => {
        this.VODId = resp.VODDetail.ID
        this.VODDetail = resp.VODDetail
        this.firstSeasonSerieId = this.VODDetail.episodes[0].VOD
        this.seasonName = this.VODDetail.name
        this.seasonEpisodes = resp.VODDetail.episodes
        this.seasonGenres = []
        this.seasonList = this.VODDetail.brotherSeasonVODs
        if (this.VODDetail.picture.drafts && this.VODDetail.picture.drafts.length > 0) {
            this.picture = this.pictureService.convertToSizeUrl(
              this.VODDetail.picture.drafts[0],
              { minwidth: 1280, minheight: 720, maxwidth: 1280, maxheight: 720 }
            )
          } else {
            this.picture = '../assets/img/default/home_poster.png'
          }
        this.seasonId = this.VODDetail.ID
        this.seasonsList = this.VODDetail.brotherSeasonVODs
        this.VODDetail.genres.forEach(genre => {
            this.seasonGenres.push(genre.genreName)
          })
        let seriesId = this.vodRouter.getParentId(this.VODDetail)
        this.vodRouter.getSeries(seriesId).then(seriesResp => {
            this.seriesName = seriesResp.VODDetail.name
            this.series = seriesResp.VODDetail
          })
        console.log('[SeasonComponent]', 'VODDetail', this.VODDetail)
        this.isLoaded = true
      })
    })
  }
  vodNavigation (vodId: string) {
    this.vodRouter.navigate(vodId)
  }
}
