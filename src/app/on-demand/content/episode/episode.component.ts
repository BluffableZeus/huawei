import { ActivatedRoute } from '@angular/router'
import { VODRouterService } from '../../../shared/services/vod-router.service'
import { Component, OnInit } from '@angular/core'
import { VODDetail } from 'ng-epg-sdk'
import { TitleService } from 'src/app/shared/services/title.service';

@Component({
  selector: 'app-content-episode',
  templateUrl: './episode.component.html',
  styleUrls: ['../content.common.scss']
})

export class EpisodeComponent implements OnInit {
  addFavorite: string
  isFav: string
  public isLoaded = false
  public VODId = ''
  public VODDetail: VODDetail
  public definitions = []
  public seasonGenres = []
  public showMore: boolean
  public seasonsList = []
  public seasonEpisodes = []
  public seasonId = 0
  public seriesName = ''
  public seasonName = ''
  public seriesId = ''
  public series = null
  public season = null
  public score: string

  public episodeHaveTrailer = false
  public seasonNumber = 0
  public info: any

  constructor (
        private vodRouter: VODRouterService,
        private route: ActivatedRoute,
        private _titleService: TitleService
    ) {}

  ngOnInit () {
    this.route.params.subscribe(params => {
      this.VODId = params['id']
      this.vodRouter.getVOD(this.VODId).then(resp => {
        this.VODDetail = resp.VODDetail
        this.episodeHaveTrailer = this.VODDetail.clipfiles.length > 0
        this.info = {
            'contentIDs': this.VODDetail['ID'],
            'contentTypes': this.VODDetail['contentType'],
            'userScore': this.VODDetail['userScore'] || 0
          }
        this.seasonId = Number(this.VODDetail.series[0]['VODID'])
          // this.isLoaded = true;
        this.showMore = false

        let seasonId = this.vodRouter.getParentId(this.VODDetail)

        this._titleService.setOnlyTitle(this.VODDetail.name)

        this.vodRouter.getSeason(seasonId).then(seasonResp => {
            let seasonDetail = seasonResp.VODDetail
            this.season = seasonResp.VODDetail
            this.seasonNumber = this.vodRouter.getSeasonNumber(seasonResp.VODDetail)
            this.seasonName = this.season.name
            this.seasonEpisodes = seasonResp.VODDetail.episodes
            let seriesId = this.vodRouter.getParentId(seasonDetail)
            this.vodRouter.getSeries(seriesId).then(seriesResp => {
              let seriesDetail = seriesResp.VODDetail
              this.series = seriesDetail
              this.seriesId = seriesDetail.ID
              this.seasonsList = seriesDetail.episodes
              this.seriesName = seriesDetail.name
              this.seasonGenres = []
              this.VODDetail.subjectIDs = seriesDetail.subjectIDs
              seriesResp.VODDetail.genres.forEach(genre => {
                this.seasonGenres.push(genre.genreName)
              })
              this.isLoaded = true
              this.vodRouter.navigate(this.seriesId)
            })
          })

        console.log('[EpisodeComponent]', 'VODDetail', this.VODDetail)
      })
    })
  }

  navigateTo (vodId: string) {
    this.vodRouter.navigate(vodId)
  }
}
