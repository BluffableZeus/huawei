import { ActivatedRoute } from '@angular/router'
import { EventService } from 'ng-epg-sdk/services'
import { VODRouterService } from '../../../shared/services/vod-router.service'
import { Component, OnInit } from '@angular/core'
import { VODDetail } from 'ng-epg-sdk'
import { PictureService } from 'ng-epg-ui/services'
import { session } from 'src/app/shared/services/session'
import { MobileService } from '../../../shared/services/mobile.service'
import { TitleService } from 'src/app/shared/services/title.service';

@Component({
  selector: 'app-content-series',
  templateUrl: './series.component.html',
  styleUrls: ['../content.common.scss', './series.component.scss']
})

export class SeriesComponent implements OnInit {
  public isLoaded = false
  public picsrc = ''
  public forcePlay = false
  public noMobilePlay = false

  public SeriesId = ''
  public VODId = ''
  public VODSeries: VODDetail
  public VODDetail: VODDetail

  public seriesName = ''

  public seasonGenres = []
  public seasonsList = []
  public seasonEpisodes = []
  public seasonId = ''
  public season = null
  public seasonNumber = 1

  public seasonHaveTrailers = false
  public trailerNumber = 0
  public seasonTrailers = []
  public trailersLoaded = false

  public showMore: boolean
  public episodeNumber = 1
  public info: any

  private ckLastSeason = 'Last_Season_'
  private ckLastEpisode = 'Last_Episode_'

  constructor (
        private vodRouter: VODRouterService,
        private route: ActivatedRoute,
        protected pictureService: PictureService,
        private mobile: MobileService,
        private _titleService: TitleService
    ) {
  }

  ngOnInit () {
    this.route.params.subscribe(params => {
      this.SeriesId = params['id']
      this.getSeries()

      console.log('[SeriesComponent]', 'VODDetail', this.VODDetail)
    })

    EventService.removeAllListeners(['XVOD_NEXT'])
    EventService.on('XVOD_NEXT', () => {
      if (!this.VODId) {
        if (this.trailerNumber < this.seasonTrailers.length - 1) {
            this.setTrailer(this.seasonTrailers[+this.trailerNumber + 1])
          } else {
            EventService.emit('SERIES_EXIT_FULLSCREEN')
          }
      } else {
        if (this.episodeNumber < this.seasonEpisodes.length) {
            this.setVOD(this.seasonEpisodes[+this.episodeNumber])
          }
      }
    })
    EventService.on('LOGIN', () => {
      this.forcePlay = false
    })
  }

  cookieSeasonKey () {
    return this.ckLastSeason + this.VODSeries.ID
  }

  cookieEpisodeKey () {
    return this.ckLastEpisode + this.season.ID + '_' + this.VODSeries.ID
  }

  getLastSeasonIndex () {
    const ckKey = this.cookieSeasonKey(),
      ckSeason = Cookies.get(ckKey)

    if (!ckSeason) {
      Cookies.set(ckKey, 0)
      return 0
    }

    return ckSeason
  }

  setLastSeasonIndex (index) {
    Cookies.set(this.cookieSeasonKey(), index)
  }

  getLastEpisodeIndex () {
    const ckKey = this.cookieEpisodeKey(),
      ckEpisode = Cookies.get(ckKey)

    if (!ckEpisode) {
      Cookies.set(ckKey, 0)
      return 0
    }

    return ckEpisode
  }

  setLastEpisodeIndex (index) {
    Cookies.set(this.cookieEpisodeKey(), index)
  }

  getSeries () {
    this.vodRouter.getSeries(this.SeriesId).then(resp => {
      this.VODSeries = resp.VODDetail

      this.seasonGenres = []
      this.VODSeries.genres.forEach(genre => {
        this.seasonGenres.push(genre.genreName)
      })

      this.seasonsList = this.VODSeries.episodes

      const lastSeason = this.seasonsList[this.getLastSeasonIndex()]
      this.seasonNumber = lastSeason.sitcomNO
      this.seasonId = lastSeason.VOD.ID

      this._titleService.setOnlyTitle(this.VODSeries.name)
      this.getSeason()
    })
  }

  getSeason (ID?) {
    const sid = ID || this.seasonId

    this.seasonId = sid

    this.vodRouter.getSeason(sid).then(seasonResp => {
      this.season = seasonResp.VODDetail
      this.seasonEpisodes = seasonResp.VODDetail.episodes

      // this.picsrc = this.season.picture.posters[0] || '../assets/img/default/home_poster.png'
      /** DISABLE FOR C50 */
      if (this.season.picture.drafts && this.season.picture.drafts.length > 0) {
        this.picsrc = this.pictureService.convertToSizeUrl(
            this.season.picture.drafts[0],
            { minwidth: 1280, minheight: 720, maxwidth: 1280, maxheight: 720 }
          )
      } else {
        this.picsrc = '../assets/img/default/home_poster.png'
      }
      /** DISABLE FOR C50 -- end */

      const lastEpisode = this.season.episodes[this.getLastEpisodeIndex()]
      this.episodeNumber = lastEpisode.sitcomNO
      this.VODId = lastEpisode.VOD.ID

      this.getTrailers()
      this.getVOD()

      this.isLoaded = true
    })
  }

  getTrailers () {
    this.vodRouter.getVOD(this.season.episodes[0].VOD.ID).then(vodresp => {
      this.seasonHaveTrailers = vodresp.VODDetail.clipfiles.length > 0
      if (this.seasonHaveTrailers) {
        this.seasonTrailers = vodresp.VODDetail.clipfiles.map((item, index) => {
            return {
              sitcomNO: index + 1,
              traIndex: index,
              VOD: item,
              details: vodresp.VODDetail
            }
          })
      }
      this.trailersLoaded = true
    })
  }

  getVOD (ID?, force?) {
    const vid = ID || this.VODId

    this.VODId = vid

    this.vodRouter.getVOD(vid).then(vodresp => {
      this.VODDetail = vodresp.VODDetail
      this.info = {
        'contentIDs': this.VODDetail['ID'],
        'contentTypes': this.VODDetail['contentType'],
        'userScore': this.VODDetail['userScore'] || 0
      }

      this.showMore = false
      this.isLoaded = true
      this.forcePlay = force
    })
  }

  setSeason (item) {
    if (item.VOD.ID === this.seasonId) {
      return
    }
    this.seasonNumber = item.sitcomNO
    this.setLastSeasonIndex(this.VODSeries.episodes.indexOf(item))
    this.seasonEpisodes = []
    this.seasonTrailers = []
    this.trailersLoaded = false
    setTimeout(() => this.getSeason(item.VOD.ID), 300)
  }

  setVOD (item) {
      // if (item.VOD.ID === this.VODId) { return }
    if (this.testMobile()) {
      return
    }

    this.episodeNumber = item.sitcomNO
    this.setLastEpisodeIndex(this.season.episodes.indexOf(item))
    EventService.emit('SERIES_PLAY_FULLSCREEN')
    this.getVOD(item.VOD.ID, true)
  }

  setTrailer (item) {
    if (this.testMobile()) {
      return
    }

    let playInfo = {}

    playInfo['traIndex'] = item.traIndex
    playInfo['tra'] = item.VOD.ID

    this.trailerNumber = this.seasonTrailers.indexOf(item)
    this.VODDetail = item.details
    this.VODId = ''

    session.put('playInfo', playInfo)
    EventService.emit('XVOD_PLAY_TRAILER', item.details)
  }

  testMobile () {
    if (this.mobile.isMobile) {
      this.noMobilePlay = true
    }

    return this.noMobilePlay
  }

  scrollToVideo () {
    const start = new Date().getTime(),
      from = window.scrollY,
      to = document.getElementById('player_content').offsetTop,
      time = 400,
      timer = setInterval(() => {
        const t = Math.min(1, (new Date().getTime() - start) / time),
            step = t < 0.5 ? 2 * t * t : -1 + (4 - 2 * t) * t

        window.scrollTo(window.scrollX, from + Math.min(1, step * (to - from)))

        if (step === 1) {
            clearInterval(timer)
          }
      })
  }
}
