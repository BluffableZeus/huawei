import { ActivatedRoute } from '@angular/router'
import { VODRouterService } from '../../../shared/services/vod-router.service'
import { EventService } from 'ng-epg-sdk/services'
import { Component, OnInit } from '@angular/core'
import { VODDetail } from 'ng-epg-sdk'
import { PictureService } from 'ng-epg-ui/services'
import { session } from 'src/app/shared/services/session'
import { TitleService } from 'src/app/shared/services/title.service'

@Component({
  selector: 'app-content-movie',
  templateUrl: './movie.component.html',
  styleUrls: ['../content.common.scss', './movie.component.scss']
})

export class MovieComponent implements OnInit {
  public isLoaded = false
  public picsrc = ''

  public id = ''
  public data: VODDetail = null
  public playerEnabled = false
  public movieGenres = []

  public movieHaveTrailer = false
  public trailerNumber = 0
  public movieTrailers = []
  public trailersLoaded = false

  constructor (
        private vodRouter: VODRouterService,
        private route: ActivatedRoute,
        private pictureService: PictureService,
        private _titleService: TitleService
    ) {
  }

  ngOnInit () {
    this.route.params.subscribe(params => {
      this.id = params['id']
      this.getMovie()
    })
  }

  getMovie () {
    this.vodRouter.getVOD(this.id).then(resp => {
      this.data = resp.VODDetail

      const pic = (this.data.picture.backgrounds && this.data.picture.backgrounds[0]) || (this.data.picture.stills && this.data.picture.stills[0]) || null;
      this.picsrc = pic ? this.pictureService.convertToSizeUrl(pic, { minwidth: 1280, minheight: 720, maxwidth: 1280, maxheight: 720 })
        :
        '../assets/img/default/home_poster.png'

      this.movieGenres = []
      this.data.genres.forEach(genre => {
        this.movieGenres.push(genre.genreName)
      })

      this.movieHaveTrailer = this.data.clipfiles.length > 0
      if (this.movieHaveTrailer) { this.getTrailers() }

      this.isLoaded = true
      console.log('[MovieComponent]', 'VODDetail', this.data)
      this._titleService.setOnlyTitle(this.data.name)
    })
  }

  getTrailers () {
    if (this.movieHaveTrailer) {
      this.movieTrailers = this.data.clipfiles.map((item, index) => {
        return {
            sitcomNO: index + 1,
            traIndex: index,
            VOD: item,
            details: this.data
          }
      })
    }

    this.trailersLoaded = true
  }

  setTrailer (item) {
    let playInfo = {}

    playInfo['traIndex'] = item.traIndex
    playInfo['tra'] = item.VOD.ID

    this.data = item.details
    this.id = ''

    session.put('playInfo', playInfo)
    EventService.emit('XVOD_PLAY_TRAILER', item.details)
  }
}
