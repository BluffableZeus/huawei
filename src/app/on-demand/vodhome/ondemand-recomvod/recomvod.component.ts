import * as _ from 'underscore'
import { Component, Input, OnInit } from '@angular/core'
import { CustomConfigService } from '../../../shared/services/custom-config.service'
import { Router } from '@angular/router'
import { VODRouterService } from 'src/app/shared/services/vod-router.service'
import { PictureService } from 'ng-epg-ui/services/picture.service'
import { SectionService } from 'src/app/home/sections/section.service';

@Component({
// tslint:disable-next-line:component-selector-name component-selector-prefix
  selector: 'recomvod',
  templateUrl: './recomvod.component.html',
  styleUrls: ['./recomvod.component.scss']
})

// tslint:disable-next-line:component-class-suffix
export class RecomVod implements OnInit {
    /**
     * declare variables
     */
  public vodListSuspension = {}
  public datas
  public recomVodId
  public recomVodshow
    /**
     * when the data changed
     */
  @Input() set recomData (data) {
    this.getData(data)
  }
    /**
     * name for modules that would be used in this module
     */
  constructor (
        private customConfigService: CustomConfigService,
        private route: Router,
        private vodRouter: VODRouterService,
        private pictureService: PictureService,
        private sectionService: SectionService
    ) { }
    /**
     * initialization
     */
  ngOnInit () {
    this.recomVodId = ['recomVodsuspension', 'recomVodList', 'recomVodLeft', 'recomVodRight']
    this.recomVodshow = [true, true, true]
  }
    /**
     * deal data come from parent module
     * @param data come from a interface
     */
  getData (data) {
    this.datas = data
    this.datas = _.map(this.datas, (list, index) => {
      return {
        VODList: list['VODs'],
        subjectName: list['subject'].name,
        subjectID: list['subject'].ID,
        hasChildren: list['subject'].hasChildren
      }
    })
      // no more than 24 data
    this.datas = this.datas.slice(0, 24)
    for (let i = 0; i < this.datas.length; i++) {
      this.vodListSuspension[i] = _.map(this.datas[i].VODList, (list, index) => {
        list['focusRoute'] = 'on-demand'
        return {
            titleName: this.datas[i].subjectName,
            floatInfo: list
          }
      })
    }
    _.map(this.datas, (lists, index) => {
      _.map(lists['VODList'], (list, index1) => {
        this.score(list)
          // use the url you get or default
          // resize the poster
        if (list['picture'] && list['picture'].posters && list['picture'].posters[0]) {
            let url = this.pictureService.convertToSizeUrl(list['picture'].posters[0],
            this.sectionService.getImageSize())
            list['picture'].posters[0] = url || 'assets/img/default/home_poster.png'
          }
      })
    })
  }
  
    /**
     * jump to a new page according to the route
     * @param datas target vod
     */
  moreShow (datas) {
    this.route.navigate(['video', 'category', datas[0]])
  }
    /**
     * when you click the poster of a vod
     * @param event target vod you click
     */
  onClickDetail (event) {
    this.vodRouter.navigate(event[0])
  }
  /**
      * package a function to get element
      * @param divName id of the target element
      */
  dom (divName: string) {
    return document.getElementById(divName)
  }
    /**
     * unify the format of score
     * @param list target vod
     */
  score (list) {
      // the scored vod
    if (list.averageScore) {
        // integer score
      if (list.averageScore.length === 1) {
        list.averageScore += '.0'
      } else {
          // when the score is 10
        if (list.averageScore.substring(0, 2) === '10') {
            list.averageScore = '10'
            // noninteger score between 0 and 10
          } else {
            let score = list.averageScore
            list.averageScore = score.substring(0, 4)
            list.averageScore = Number(list.averageScore)
            // ensure this kind of score has one decimal
            list.averageScore = list.averageScore.toFixed(1)
          }
      }
        // vod which hasn't been scored
    } else {
      list.averageScore = '0.0'
    }
    return list
  }
}
