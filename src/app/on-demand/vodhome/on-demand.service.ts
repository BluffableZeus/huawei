import * as _ from 'underscore'
import { Injectable } from '@angular/core'
import { queryPCVodHome, queryVODListStcPropsBySubject, queryVODSubjectList } from 'ng-epg-sdk/vsp'

@Injectable()
export class OnDemandService {
  constructor () {}
 /**
  * call a interface
  * deal with the data come from the interface
  * get the data from queryPCVodHome
  */
  getPCVodHome (req: any) {
    return queryPCVodHome(req).then(resp => {
      /**
       * package data of the castContent Vod
       */
      let castContentVod
      if (resp.castContentsList && resp.castContentsList[0] && (resp.castContentsList[0].recmVODs.length > 0)) {
        castContentVod = resp.castContentsList[0].recmVODs
      } else {
        castContentVod = resp.castContentsList && resp.castContentsList[1] && resp.castContentsList[1].recmVODs
      }
      for (let i = 0; i < resp.castContentsList.length; i++) {
        let recmVOD: any
        // get the details of recmVOD
        recmVOD = _.map(castContentVod, vod => {
          if (vod['averageScore']) {
            if (vod['averageScore'] !== '10') {
              let averageScore = vod['averageScore']
              if (averageScore.length === 1) {
                averageScore += '.0'
              } else {
                averageScore = averageScore.substring(0, 4)
                averageScore = Number(averageScore).toFixed(1)
              }
              vod['averageScore'] = averageScore
            }
          } else {
            vod['averageScore'] = '0.0'
          }
          vod['focusRoute'] = 'on-demand'
          /**
           * get the url of the poster
           */
          let pictureurl = vod['picture'] && vod['picture'].posters && vod['picture'].posters[0]
          return {
            id: vod['ID'],
            score: parseInt(vod['averageScore'], 10) === 10 ? '10' : Number(vod['averageScore']).toFixed(1),
            name: vod['name'],
            picture: pictureurl || 'assets/img/default/home_poster.png',
            floatInfo: vod,
            titleName: 'cast',
            isOnDemand: true,
            castName: resp.castContentsList[i]['cast'] ? resp.castContentsList[i]['cast']['castName'] : undefined
          }
        })
        resp.castContentsList[i]['recmVODs'] = recmVOD
      }
      return resp
    })
  }

  /**
   * QueryVODListStcPropsBySubject
   * @param req
   */
  queryBySubject (req: any) {
    return queryVODListStcPropsBySubject(req).then(resp => {
      return resp
    })
  }

  getSubjectList (req: any) {
    return queryVODSubjectList(req).then(resp => {
      return resp
    })
  }
}
