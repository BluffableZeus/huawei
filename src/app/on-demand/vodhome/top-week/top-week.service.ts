import { Injectable } from '@angular/core'
import { queryHomeData } from 'ng-epg-sdk/vsp'

@Injectable()
export class TopWeekService {
  constructor () { }

  /**
     * call a interface
     */
  queryHomeData (req: any) {
    return queryHomeData(req).then(resp => {
      // deal with the data come from the interface
      return resp
    })
  }
}
