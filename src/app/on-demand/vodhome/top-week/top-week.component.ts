import * as _ from 'underscore'
import { Component, OnInit, Input } from '@angular/core'
import { Router } from '@angular/router'
import { VODRouterService } from '../../../shared/services/vod-router.service'
import { TopWeekService } from './top-week.service'
import { PictureService } from 'ng-epg-ui/services/picture.service'
import { TranslateService } from '@ngx-translate/core'

@Component({
  selector: 'app-top-this-week',
  styleUrls: ['./top-week.component.scss'],
  templateUrl: './top-week.component.html'
})

export class TopThisWeekComponent implements OnInit {
    /**
     * when the data changed
     */
  @Input() subjectID: Array<any>
  @Input() set topWeekData (data) {
    this.handleData(data)
  }
    /**
     * declare variables
     */
  public vodListSuspension = []
  public topWeekVODInfo = []
  public singleData = {}
  public topWeekId
  public topWeekShow

    /**
     * name for modules that would be used in this module
     */
  constructor (
        private route: Router,
        private vodRouter: VODRouterService,
        private topWeekService: TopWeekService,
        private pictureService: PictureService,
        private translate: TranslateService
    ) { }

    /**
     * initialization
     */
  ngOnInit () {
      // ID in component ：suspension box ID\ listID\move left ID\move right ID
    this.topWeekId = ['topWeeksuspension', 'topWeekList', 'topWeekLeft', 'topWeekRight']
    this.topWeekShow = [true, true, true]
  }
    /**
     * when data of TOP THIE WEEK change
     */
  handleData (data) {
    if (data) {
      this.singleData = data
      this.topWeekVODInfo = []
      this.topWeekVODInfo.push(this.singleData)
      this.topWeekVODInfo = _.map(this.topWeekVODInfo, (list, index) => {
        _.map(list, (vod, indexTwo) => {
            // when the vod has been scored
            if (vod['averageScore']) {
              // the score is between 0 and 10 except 0 and 10
              if (vod['averageScore'] !== '10' && vod['averageScore'].substring(0, 2) !== '10') {
                let averageScore = vod['averageScore']
                // when the score is integer number
                if (averageScore.length === 1) {
                  averageScore += '.0'
                } else {
                  averageScore = averageScore.substring(0, 4)
                  averageScore = Number(averageScore)
                  averageScore = averageScore.toFixed(1)
                }
                vod['averageScore'] = averageScore
              } else {
                // when the score is 10
                vod['averageScore'] = '10'
              }
            } else {
              // when the vod hasn't been scored
              vod['averageScore'] = '0.0'
            }
          })
        return {
            VODList: list.slice(0, 24),
            subjectName: 'top_this_week',
            subjectID: this.subjectID,
            hasChildren: '0'
          }
      })
      for (let i = 0; i < this.topWeekVODInfo.length; i++) {
        this.vodListSuspension[i] = _.map(this.topWeekVODInfo[i].VODList, (list, index) => {
            list['focusRoute'] = 'on-demand'
            return {
              titleName: this.topWeekVODInfo[i].subjectName,
              floatInfo: list
            }
          })
      }
      _.map(this.topWeekVODInfo, (lists, index) => {
        _.map(lists['VODList'], (list, index1) => {
            if (list['picture'] && list['picture'].posters && list['picture'].posters[0]) {
              let url = this.pictureService.convertToSizeUrl(list['picture'].posters[0],
                { minwidth: 180, minheight: 240, maxwidth: 214, maxheight: 284 })
              list['picture'].posters[0] = url || 'assets/img/default/home_poster.png'
            }
          })
      })
    }
  }

    /**
     * when click the title of the column
     */
  moreShow (topWeekVODInfo) {
      // jump to a new page according to the route
    this.route.navigate(['home', 'magic', 'top_this_week'])
  }

    /**
     * when click the poster of the column
     */
  onClickDetail (event) {
    this.vodRouter.navigate(event[0])
  }
}
