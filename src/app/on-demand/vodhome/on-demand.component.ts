import * as _ from 'underscore'
import { Component, ViewChild, OnInit, OnDestroy } from '@angular/core'
import { session } from 'src/app/shared/services/session'
import { CustomConfigService } from '../../shared/services/custom-config.service'
import { OnDemandService } from './on-demand.service'
import { CategoryHomeComponent } from './category-home/category-home.component'
import { VodBannerComponent } from '../../shared/component/vod-banner/vod-banner.component'
import { EventService } from 'ng-epg-sdk/services'
import { CommonService } from '../../core/common.service'
import { ActivatedRoute, Router } from '@angular/router'
import { VODListSubject, OnJumpToEvent } from 'ng-epg-ui/webtv-components/childcolumnshow'
import { PictureService } from 'ng-epg-ui/services'
import { SectionService } from 'src/app/home/sections/section.service'
import { VODRouterService } from 'src/app/shared/services/vod-router.service'
import { HomeService } from 'src/app/home/home.service'
// import { Observable } from 'rxjs';
// import { async } from 'rxjs/internal/scheduler/async';

@Component({
  selector: 'app-on-demand',
  styleUrls: ['./on-demand.component.scss'],
  templateUrl: './on-demand.component.html'
})
export class OnDemandComponent implements OnInit, OnDestroy {
   /**
    * variable definition
    */
  @ViewChild(VodBannerComponent) vodBanner: VodBannerComponent
  @ViewChild(CategoryHomeComponent) categoryHome: CategoryHomeComponent
  public oscarVODInfo = []
  public data1: VODListSubject[] = []
  public data2: VODListSubject[] = []
  public vodHomeData = {}
  public vodListSuspension = []
  public vodBannerData = []
  public vodCastContentsData = []
  public vodSubjectData = []
  public rankData = []
  public successfulGetData = false
  public subjectID
  public recomSubject = []
  public isSeries: boolean = false
    /**
     * show column of a cast and related videos
     */
  public showVodCastData = true
    /**
     * name for modules that would be used in this module
     */


  public bannerData = null
  constructor (
        private onDemandService: OnDemandService,
        private customConfigService: CustomConfigService,
        private commonService: CommonService,
        private activeRoute: ActivatedRoute,
        private _pictureService: PictureService,
        private _sectionService: SectionService,
        private vodRouter: VODRouterService,
        private homeService: HomeService,
        private router: Router,
        protected service: SectionService
    ) {}

 /**
  * initialization
  */
  ngOnInit () {

    this.isSeries = (this.activeRoute.routeConfig.path === 'series') ? true : false
      // according to the profile to get vod datas
    if (!session.get('CUSTOM_CONFIG_DATAS')) {
      EventService.on('CUSTOM_CACHE_CONFIG_DATAS', () => {
        this.getData()
      })
    } else {
      this.getData()
    }
    if (Cookies.getJSON('IS_GUEST_LOGIN')) {
      EventService.on('CUSTOM_CACHE_CONFIG_DATAS', () => {
        this.getData()
      })
    }

    this.getBanner()
    this.getSubjectList()
  }

    /**
     * when exit the component, clear all the timer and events
     */
  ngOnDestroy () {
    document.querySelector('.ondemand-filter-load')['style']['display'] = 'none'
    EventService.removeAllListeners(['CUSTOM_CACHE_CONFIG_DATAS'])
  }

  getData () {
    this.data1 = []
    this.data2 = []
    document.querySelector('.ondemand-filter-load')['style']['display'] = 'block'
    let arr = [
      { subjectS: 'showsall', subjectV: 'moviesall', subjectID: 'MNovelties', subjectName: 'Новинки кино' },
      { subjectS: 'showspopular', subjectV: 'moviespopular', subjectID: 'MPopular', subjectName: 'Популярное' },
      { subjectS: 'showschildren', subjectV: 'movieschildren', subjectID: 'MKids', subjectName: 'Детям' },
      { subjectS: 'showsrecommended', subjectV: 'moviesrecommended', subjectID: 'MRecommend', subjectName: 'Рекомендуем' },
      { subjectS: 'showspremier', subjectV: 'moviespremieres', subjectID: 'MPremieres', subjectName: 'Суперпремьеры' },
      { subjectS: 'adultVOD', subjectV: 'adultVOD', subjectID: 'adultVOD', subjectName: 'Для взрослых' }
    ]
    arr.forEach((el, index) => {
      let target = index > 0 ? this.data2 : this.data1
      this.getSubject(this.isSeries ? el.subjectS : el.subjectV, el.subjectID, el.subjectName, target, index)
      console.log(this.data1, this.data2)
    })
  }

  getSubject (subject = 'moviesall', subjectID = 'MNovelties', subjectName = 'Новинки кино', TARGET, index?) {

    this.onDemandService.queryBySubject({
      subjectID: subject,
      count: 50,
      offsset: 1,
      SortType: 'STARTTIME:DESC'
    }).then((response: any) => {
      this.onDemandLogInfo(response)
      this.successfulGetData = true
      document.querySelector('.ondemand-filter-load')['style']['display'] = 'none'
      if (response.VODs && response.VODs.length) {
        TARGET[index] = {
          VODList: response.VODs,
          subjectName: subjectName,
          subjectID: subjectID,
          hasChildren: -1
        }
      }
    })
  }

  /**
   * show MSA_ERROR information according to the error code.
   */
  onDemandLogInfo (resp1) {
      // MSA_ERROR information of bannerVODs
    if (_.isUndefined(resp1['bannerVODs']) || resp1['bannerVODs'].length === 0) {
      this.commonService.MSAErrorLog(38033, 'QueryPCVodHome', 'bannerVODs')
    }
      // MSA_ERROR information of rankVODs
    if (_.isUndefined(resp1['rankVODs']) || resp1['rankVODs'].length === 0) {
      this.commonService.MSAErrorLog(38033, 'QueryPCVodHome', 'rankVODs')
    }
      /**
       * get recmSubjectDetailList from  the interface
       */
    if (_.isUndefined(resp1['recmSubjectDetailList']) || resp1['recmSubjectDetailList'].length === 0) {
      this.commonService.commonLog('QueryPCVodHome', 'recmSubjectDetailList')
    } else {
      let recSubjectLengthZero = _.find(resp1['recmSubjectDetailList'], item => {
        return item['VODs'].length === 0
      })
        // MSA_ERROR information of recSubjectLengthZero
      if (!_.isUndefined(recSubjectLengthZero)) {
        this.commonService.MSAErrorLog(38033, 'QueryPCVodHome', 'recmSubjectDetailList')
      }
    }
    /**
     * get castContentsList from  the interface
     */
    this.moreOnDemandLogInfo(resp1)
  }

  moreOnDemandLogInfo (resp1) {
    if (_.isUndefined(resp1['castContentsList']) || resp1['castContentsList'].length === 0) {
      this.commonService.commonLog('QueryPCVodHome', 'castContentsList')
    } else {
      let castVodLengthZero = _.find(resp1['castContentsList'], item => {
        return item['recmVODs'].length === 0
      })
        // MSA_ERROR information of castVodLengthZero
      if (!_.isUndefined(castVodLengthZero)) {
        this.commonService.MSAErrorLog(38033, 'QueryPCVodHome', 'recmVODs')
      }
    }
  }

  getBanner () {
    this.homeService.queryHomeData({
      bannerSubjectID: 'VODALL',
      bannerDataCount: 5,
      recmDataCount: 1,
      subjectDataCount: 1,
      hotTVCount: 1
    }).then(response => {
      this.onDemandLogInfo(response)
      this.bannerData = response['banner']
    })

  }

  getSubjectList () {
    this.onDemandService.getSubjectList({
      subjectID: this.isSeries ? 'genresshows' : 'genresmovies',
      count: 50,
      offsset: 1
    }).then((response: any) => {
      this.onDemandLogInfo(response)
      this.vodSubjectData = response.subjects
    })
  }

  /**
   * when you click the poster of a vod
   * @param event target vod you click
   */
  onClickDetail (event) {
    this.vodRouter.navigate(event[0])
  }

  /**
   *  turn to more movies page.
   */
  moreShow (event: OnJumpToEvent) {
    const [subjectId, hasChildren, subjectName] = event

    switch (subjectId) {
      case SectionService.EIW_SUBJECT_ID:
        this.router.navigate(['home', 'magic', this.service.getTitle(subjectName)])
        break
      default:
        this.router.navigate(['video', 'category', subjectId])
        break
    }
  }
}
