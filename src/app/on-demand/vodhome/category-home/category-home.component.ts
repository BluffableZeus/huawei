import * as _ from 'underscore'
import { Component, Input } from '@angular/core'
import { Router } from '@angular/router'
import { PictureService } from 'ng-epg-ui/services/picture.service'

@Component({
  selector: 'app-category-home',
  templateUrl: './category-home.component.html',
  styleUrls: ['./category-home.component.scss']
})

export class CategoryHomeComponent {
    /**
     * declare variables
     */
  public hasRootCategoryData = false
  public hasRootCategory = true
  public isShow = 'false'
    // the category array
  private categoryLists = []
    // get upward integer by length of categoryLists divide pageNum
  private page: number
    // init number of the categories
  private count = 1

    /**
     * name for modules that would be used in this module
     */
  constructor (
        private route: Router,
        private pictureService: PictureService
    ) { }

    /**
     * when the data changed
     */
  @Input() set setvodSubjectList (vodSubjectList) {
    this.categoryLists = _.map(vodSubjectList, (category, index) => {
      let url = ''
      if (category && category['picture'] && category['picture'].icons) {
        url = this.pictureService.convertToSizeUrl(category['picture'].icons[0],
            { minwidth: 56, minheight: 56, maxwidth: 56, maxheight: 56 })
      }
      return {
        categoryId: category['ID'],
        name: category['name'],
        posterUrl: category['picture'] && category['picture'].icons && url,
        hasChildren: category['hasChildren']
      }
    })
    if (this.categoryLists && this.categoryLists.length > 0) {
      let clientW: number = window.innerWidth
        // the number of the category to show
      let pageNum
        // according to the screen size, the number is different
      if (clientW > 1440) {
        pageNum = 6
      } else {
        pageNum = 5
      }
      this.page = Math.ceil(this.categoryLists.length / pageNum)
      this.hasRootCategory = true
    } else {
      this.hasRootCategory = false
    }
    this.count = this.categoryLists.length
  }

    /**
     * jump to a new page according to the route
     * @param id id of the category
     */
  vodDetail (id) {
    this.route.navigate(['video', 'category', id])
  }
}
