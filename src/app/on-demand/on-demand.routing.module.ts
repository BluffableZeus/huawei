import { NgModule } from '@angular/core'
import { Route, RouterModule } from '@angular/router'

import { AuthGuard } from '../shared/services/auth.guard'

import { SeriesComponent } from './content/series/series.component'
import { SeasonComponent } from './content/season/season.component'
import { EpisodeComponent } from './content/episode/episode.component'
import { MovieComponent } from './content/movie/movie.component'
import { OnDemandComponent } from './vodhome/on-demand.component'
import { VodFilterDetailComponent } from './vodFilterDetail/vodFilterDetail.component'
import { CastMoreComponent } from './voddetail/cast-more/cast-more.component'
import { RecomVodMoreComponent } from './recomvodAll/recomvodAll.component'
/**
 * vod router maps
 */
export const vodRoutes: Array<Route> = [
  { path: '', component: OnDemandComponent, canActivate: [AuthGuard] },
  { path: 'series/:id', component: SeriesComponent, canActivate: [AuthGuard] },
  { path: 'series/:series_id/season/:season_number', component: SeasonComponent, canActivate: [AuthGuard] },
  { path: 'video/:id', component: EpisodeComponent, canActivate: [AuthGuard] },
  { path: 'movie/:id', component: MovieComponent, canActivate: [AuthGuard] },

  { path: 'series', component: OnDemandComponent, canActivate: [AuthGuard] },
  { path: 'cast/:id', component: CastMoreComponent, canActivate: [AuthGuard] },
  { path: 'category/:id', component: RecomVodMoreComponent, canActivate: [AuthGuard] },
  { path: 'category/:id/filter', component: VodFilterDetailComponent, canActivate: [AuthGuard] },

  { path: 'on-demand', component: OnDemandComponent, canActivate: [AuthGuard] }
  // { path: 'on-demand/stillsMore/:id/:index/:subjectName', component: StillsMoreComponent, canActivate: [AuthGuard] },
]

@NgModule({
  imports: [
    RouterModule.forChild(vodRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class OnDemandRoutingModule { }
