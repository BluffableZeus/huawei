import * as _ from 'underscore'
import { Component, Input, OnInit, AfterViewInit } from '@angular/core'
import { DirectionScroll } from 'ng-epg-ui/webtv-components/scroll/directionScroll.component'
import { EventService } from 'ng-epg-sdk/services'
import { session } from 'src/app/shared/services/session'
import { VodDetailHomeService } from '../vodDetail.service'
import { PictureService } from 'ng-epg-ui/services/picture.service'
import { MediaPlayService } from '../../../component/mediaPlay/mediaPlay.service'
import { ActivatedRoute } from '@angular/router'

/**
 * the width of the episodes in the big screen
 */
const bigPanel = 308
/**
 * the number of episodes displayed per page in big screen
 */
const bigPagesNum = 35
/**
 * the number of episodes in groups displayed in big screen
 */
const bigGroupNum = 3
const bigMinScrollIndex = 10
const bigGeneralScroll = 96

/**
 * the width of the episodes in the small screen
 */
const smallPanel = 254
/**
 * the number of episodes displayed per page in small screen
 */
const smallPagesNum = 16
/**
 * the number of episodes in groups displayed in small screen
 */
const smallGroupNum = 2
const smallMinScrollIndex = 8
const smallGeneralScroll = 73

@Component({
  selector: 'app-vod-episode',
  styleUrls: ['./vod-episode.component.scss'],
  templateUrl: './vod-episode.component.html'
})

export class VodEpisodeComponent implements OnInit, AfterViewInit {
  public episodes = []
  public clipfilesVODs = []
  public pages = []
  public bigPages = []
  public smallPages = []
    /**
     * episode details
     */
  public details
    /**
     * episodes is focused or not
     */
  public isEpisodesFocus = false
    /**
     * trailers is focused or not
     */
  public isTrailerssFocus = false
    /**
     * the vod has episodes
     */
  public ifEpisodesLength = false
    /**
     * the vod has trailers
     */
  public ifclipfilesVODsLength = false
  public pageIndex = 0
    /**
     * number of click the category left icon and right icon
     */
  public count = 0
  public panel = 310
  public offsetNumber = 0
    /**
     * show category left icon
     */
  public categoryLeftVisible = 'hidden'
    /**
     * show category right icon
     */
  public categoryRightVisible = 'hidden'
  public page: number
    /**
     * the number of episodes displayed per page in screen
     */
  public pagesNum = 35
    /**
     * show trailers scroll
     */
  public clipfilesVisible = 'hidden'
    /**
     * the array of all episodes
     */
  public totalEpisodes: Array<any> = []
  public pageStr = []
  public bigPageStr = []
  public smallPageStr = []
    /**
     * the number of episodes in groups displayed in screen
     */
  public groupNum = 3
  public playInfo: Object = {
    'epi': '',
    'epiIndex': '',
    'tra': '',
    'traIndex': 0
  }
  public episodeNum = '1'
    /**
     * index of current group
     */
  public curGroupIndex = 0
    /**
     * index of trailer
     */
  public trailerIndex: number = -1
  public minScrollIndex = 10
  public generalScroll = 96
  public changeRate = 1.44
    /**
     * small screen state
     */
  public isSmallScreen = false
  public epiIndex = 0
  public isEnterFullscreen = {}
  public maxSitcomNO = 0
    /**
     * play trailers
     */
  public isPlayTrailer = false

  constructor (
        private directionScroll: DirectionScroll,
        private voddetail: VodDetailHomeService,
        private pictureService: PictureService,
        private mediaPlayService: MediaPlayService,
        public route: ActivatedRoute) { }
    /**
     * set episode details
     */
  @Input() set setEpisode (details) {
    this.details = details
    if (details['VODType'] !== '0' && details['episodes'].length > 0) {
      this.episodeNum = details.sitcomNO
      let curEpiData = _.find(details['episodes'], item => {
        return item['sitcomNO'] === this.episodeNum
      })
      this.epiIndex = _.findIndex(details['episodes'], curEpiData)
    }
    this.trailerIndex = -1
    this.judgeScreen()
    this.setDealAll(this.details)
    if (this.ifclipfilesVODsLength) {
      this.pluseOnScroll(this.clipfilesVODs.length)
    }
    _.delay(() => {
      this.translateEpiGroups()
      this.judgeEpiExist()
    }, 100)
  }
  ngOnInit () {
    let self = this
    self.route.params.subscribe(params => {
      self.isPlayTrailer = false
    })
      // monitor window size change events
    EventService.on('ScreenSize', () => {
      let MediaplayerVod = self.mediaPlayService.isFullScreen()
      if (MediaplayerVod) {
        self.isEnterFullscreen['Fullscreen'] = true
        return
      }
      if (self.isEnterFullscreen['Fullscreen']) {
        self.isEnterFullscreen['Fullscreen'] = false
        return
      }
      if (self.dom('scrollSpan') && self.dom('scrollSpan')['style']) {
        self.dom('scrollSpan')['style']['height'] = '0px'
      }
      self.judgeScreen()
      if (self.isEpisodesFocus) {
        self.handleEpisodeData()
        self.translateEpiGroups()
      } else {
        if (this.clipfilesVODs.length > this.minScrollIndex) {
            this.clipfilesVisible = 'visible'
          } else {
            this.clipfilesVisible = 'hidden'
          }
        if (self.ifclipfilesVODsLength) {
            self.pluseOnScroll(self.clipfilesVODs.length)
            _.delay(() => {
              self.setScrollTop()
            }, 100)
          }
      }
    })
      // monitor play next episode events
    EventService.on('NEXT', (index) => {
      self.isPlayTrailer = false
      self.epiIndex = index
      let curEpiData = self.getEpiInfoByIndex(self.details, index)
      self.episodeNum = curEpiData['sitcomNO']
      _.delay(() => {
        self.translateEpiGroups()
      }, 110)
    })
      // monitor play next trailers events
    EventService.removeAllListeners(['NEXTCLIP'])
    EventService.on('NEXTCLIP', (clipIndex) => {
      self.trailerIndex = clipIndex
      self.setScrollTop()
    })
      // monitor trailers cut back film events
    EventService.on('PLAYFILM', () => {
      self.trailerIndex = -1
      self.isPlayTrailer = false
    })
      // monitor click episode
    EventService.on('playEpi', () => {
      if (Cookies.getJSON('IS_PROFILE_LOGIN')) {
        self.trailerIndex = -1
        let playInfo = session.get('playInfo')
        self.episodeNum = playInfo['epi']
        self.epiIndex = playInfo['epiIndex']
        self.isEpisodesFocus = true
        self.isTrailerssFocus = false
        self.isPlayTrailer = false
      }
    })
      // exit vod fullscreen
    EventService.on('EXIT_VOD_FULLSCREEN', () => {
      _.delay(() => {
        self.judgeScreen()
        if (self.isEpisodesFocus) {
            self.handleEpisodeData()
            self.translateEpiGroups()
          }
      }, 100)
    })
  }
  ngAfterViewInit () {
    this.pluseOnScroll(this.clipfilesVODs.length)
  }
    /**
     * set attribute on the basis of window size
     */
  judgeScreen () {
    let clientW: number = window.innerWidth
    if (clientW > 1440) {
      this.isSmallScreen = false
      this.panel = bigPanel
      this.pagesNum = bigPagesNum
      this.groupNum = bigGroupNum
      this.minScrollIndex = bigMinScrollIndex
      this.generalScroll = bigGeneralScroll
    } else {
      this.isSmallScreen = true
      this.panel = smallPanel
      this.pagesNum = smallPagesNum
      this.groupNum = smallGroupNum
      this.minScrollIndex = smallMinScrollIndex
      this.generalScroll = smallGeneralScroll
    }
  }
    /**
     * if only one film, show trailers
     */
  judgeEpiExist () {
    if (this.details['VODType'] === '0') {
      this.changeFocus('trailers')
    } else {
      this.changeFocus('episodes')
    }
  }
    /**
     * the tranlation method of grouping item page
     * set style of pages-list
     */
  translateEpiGroups () {
    let self = this
    if (this.details && this.details['episodes'] && this.details['episodes'].length > 0) {
      if (Number(this.episodeNum) > self.pagesNum * self.groupNum && self.dom('pages-list')) {
        this.findGroupIndex()
        let translateTimes = Math.ceil((this.curGroupIndex + 1) / self.groupNum) - 1
        self.offsetNumber = -(self.panel * translateTimes)
        self.changePages(self.curGroupIndex)
        self.isShowIcon(self.offsetNumber)
        if (self.dom('pages-list') && self.dom('pages-list')['style']) {
            self.dom('pages-list')['style'].transform = 'translate(' + -(self.panel * translateTimes) + 'px)'
            self.dom('pages-list')['style'].transition = 'none'
          }
      } else {
        self.offsetNumber = 0
        this.findGroupIndex()
        self.changePages(self.curGroupIndex)
        self.isShowIcon(self.offsetNumber)
        if (self.dom('pages-list') && self.dom('pages-list')['style']) {
            self.dom('pages-list')['style'].transform = 'translate(0px)'
            self.dom('pages-list')['style'].transition = 'none'
          }
      }
    }
  }
    /**
     * find the index of the current episode group.
     */
  findGroupIndex () {
    outer: for (let i = 0; i < this.pages.length; i++) {
      for (let j = 0; j < this.pages[i].length; j++) {
        if (this.pages[i][j]['sitcomNO'] === this.episodeNum) {
            this.curGroupIndex = i
            break outer
          }
      }
    }
  }
    /**
     * judge grouping item page whether exceeding on page , whether show arrow icon
     */
  dealPage (groupNum) {
    this.page = Math.ceil(this.pages.length / groupNum)
    if (this.pages.length <= groupNum) {
      this.categoryLeftVisible = 'hidden'
      this.categoryRightVisible = 'hidden'
    } else {
      this.categoryRightVisible = 'visible'
    }
  }
    /**
     * select grouping item page
     */
  changePages (index) {
    this.pageIndex = index
    this.episodes = this.pages[index]
  }
    /**
     * deal with data of episodes and trailers
     */
  setDealAll (details) {
    this.episodes = []
    this.pages = []
    this.clipfilesVODs = []
    this.dealEpisodes(details)
    this.handleEpisodeData()
    this.dealClip(details)
  }
    /**
     * handle the data of episode
     */
  handleEpisodeData () {
    if (this.isSmallScreen) {
      this.pages = this.smallPages
      this.pageStr = this.smallPageStr
    } else {
      this.pages = this.bigPages
      this.pageStr = this.bigPageStr
    }
    this.dealPage(this.groupNum)
  }
    /**
     * deal with data of the episodes
     */
  dealEpisodes (details) {
    let self = this
    let episodes = details.episodes || []
    self.totalEpisodes = episodes
    if (details && details.episodes && details.episodes.length > 0) {
      _.max(details.episodes, item => {
        return +item['sitcomNO']
      })
      self.maxSitcomNO = details.episodes.length
      self.smallPages = self.getPages(episodes, smallPagesNum)
      self.bigPages = self.getPages(episodes, bigPagesNum)
      self.bigPageStr = self.getPageStr(self.bigPages, bigPagesNum)
      self.smallPageStr = self.getPageStr(self.smallPages, smallPagesNum)
      self.ifEpisodesLength = false
    } else {
      self.ifEpisodesLength = true
    }
  }
    /**
     * get the pages data.
     */
  getPages (episodes, pagesNum) {
    let pages = []
    let pagesResult = []
    let episodeGroupNum = Math.ceil(this.maxSitcomNO / pagesNum)
    for (let i = 0; i < episodeGroupNum; i++) {
      pages[i] = []
    }
    for (let i = 0; i < episodes.length; i++) {
      let episodeNum = Number(episodes[i]['epiIndex']) + 1
      let atIndex = Math.ceil(episodeNum / pagesNum) - 1
      pages[atIndex].push(episodes[i])
    }
    for (let i = 0; i < episodeGroupNum; i++) {
      if (pages[i].length !== 0) {
        pagesResult.push(pages[i])
      }
    }
    return pagesResult
  }
    /**
     * get the data of episodes
     */
  getEpiInfoByIndex (vodDetails, index) {
    let epiInfo = _.find(vodDetails['episodes'], item => {
      return item['epiIndex'] === index
    }) || {}
    return epiInfo
  }
    /**
     * get grouping item String array of episodes
     */
  getPageStr (pages, pagesNum) {
    let pageStr = []
    for (let i = 0; i < pages.length; i++) {
      let str = ''
      let pageNO = Math.ceil(Number(pages[i][0]['epiIndex']) / pagesNum) + 1
      if (pageNO === 1) {
        str = '01 - ' + pagesNum * pageNO
      } else {
        str = pagesNum * (pageNO - 1) + 1 + ' - ' + pagesNum * pageNO
      }
      if (i === pages.length - 1) {
        if (pages[i].length === 1) {
            str = pages[i][0]['epiIndex'] + 1
          } else {
            let length = pages[i].length
            str = pagesNum * (pageNO - 1) + 1 + ' - ' + (pages[i][length - 1]['epiIndex'] + 1)
          }
      }
      pageStr.push(str)
    }
    return pageStr
  }
    /**
     * deal with data
     */
  addZero (data: string) {
    if (data.length === 1) {
      return '0' + data
    } else {
      return data
    }
  }
    /**
     * deal with data of  trailers
     */
  dealClip (details) {
    let self = this
    if (details && details.clipfiles && details.clipfiles.length > 0) {
      let elapseTime = ''
      let time = ''
      let second = ''
      this.clipfilesVODs = _.map(details.clipfiles, (clip, index) => {
        if (Number(clip['elapseTime']) / 60 < 10) {
            time = '0' + Math.floor(Number(clip['elapseTime']) / 60)
          } else {
            time = '' + Math.floor(Number(clip['elapseTime']) / 60)
          }
        if (Number(clip['elapseTime']) % 60 < 10) {
            second = '0' + Number(clip['elapseTime']) % 60
            elapseTime = time + ':' + second
          } else {
            elapseTime = time + ':' + Math.floor(Number(clip['elapseTime']) % 60)
          }
        let url
        if (clip['picture']) {
            url = self.pictureService.convertToSizeUrl(clip['picture'].posters,
              { minwidth: 117, minheight: 63, maxwidth: 155, maxheight: 84 })
          }
        return {
            'clipIndex': index,
            'id': clip['ID'],
            'picture': url || '',
            'elapseTime': elapseTime
          }
      })
      this.ifclipfilesVODsLength = true
      if (this.clipfilesVODs.length > this.minScrollIndex) {
        this.clipfilesVisible = 'visible'
      } else {
        this.clipfilesVisible = 'hidden'
      }
    } else if (details && details.clipfiles && details.clipfiles.length === 0) {
      this.ifclipfilesVODsLength = false
    }
  }
    /**
     * choose trailers or episodes
     */
  changeFocus (name) {
    let self = this
    if (name === 'episodes') {
      self.isEpisodesFocus = true
      self.isTrailerssFocus = false
      self.handleEpisodeData()
      self.translateEpiGroups()
      self.isShowIcon(self.offsetNumber)
    } else if (name === 'trailers') {
      self.isEpisodesFocus = false
      self.isTrailerssFocus = true
      self.pluseOnScroll(self.clipfilesVODs.length)
    }
  }
    /**
     * set up scroll bar of trailers
     */
  pluseOnScroll (length) {
    if (length > this.minScrollIndex) {
      let oBox = this.dom('vod-episode')
      let oConter, oUl, oScroll, oSpan
      oConter = this.dom('trailerHasData')
      oUl = this.dom('clipfile')
      oScroll = this.dom('trailers-scroll')
      if (oScroll && oScroll.getElementsByTagName('span')) {
        oSpan = oScroll.getElementsByTagName('span')[0]
      }
      this.directionScroll.setScroll(oBox, oConter, oUl, oScroll, oSpan)
    }
  }
    /**
     * click left.
     */
  onClickLeft () {
    this.count--
    let offsetWidth: string
    const MINSFFSETNUMBER = 0
    if (this.offsetNumber <= MINSFFSETNUMBER) {
      this.offsetNumber += this.panel
      offsetWidth = this.offsetNumber + 'px'
      this.dom('pages-list')['style'].transform = 'translate(' + offsetWidth + ')'
      this.dom('pages-list')['style'].transition = 'all 1s linear'
      this.isShowIcon(this.offsetNumber)
    }
  }
    /**
     * click right
     */
  onClickRight () {
    this.count++
    let offsetWidth: string
    let maxWidth: number = -(this.panel * (this.page - 1))
    if (this.offsetNumber >= maxWidth) {
      this.offsetNumber -= this.panel
      offsetWidth = this.offsetNumber + 'px'
      this.dom('pages-list')['style'].transform = 'translate(' + offsetWidth + ')'
      this.dom('pages-list')['style'].transition = 'all 1s linear'
      this.isShowIcon(this.offsetNumber)
    }
  }
    /**
     * whether show arrow icon
     */
  isShowIcon (offsetNumber: number) {
    if (offsetNumber === 0) {
      if (this.pages.length <= this.groupNum) {
        this.categoryLeftVisible = 'hidden'
        this.categoryRightVisible = 'hidden'
      } else {
        this.categoryLeftVisible = 'hidden'
        this.categoryRightVisible = 'visible'
      }
    } else if (offsetNumber === -(this.panel * (this.page - 1))) {
      this.categoryLeftVisible = 'visible'
      this.categoryRightVisible = 'hidden'
    } else {
      this.categoryLeftVisible = 'visible'
      this.categoryRightVisible = 'visible'
    }
  }
    /**
     *  get DOM element by ID
     */
  dom (divName: string) {
    return document.getElementById(divName)
  }
    /**
     * click episodes
     */
  clickEpi (data) {
    if (Cookies.getJSON('IS_PROFILE_LOGIN')) {
      if (this.epiIndex === data['epiIndex'] && !this.isPlayTrailer) {
        return
      }
      session.remove('clipFlag')
      this.trailerIndex = -1
      this.episodeNum = data['sitcomNO']
      this.epiIndex = data['epiIndex']
      if (this.epiIndex === this.totalEpisodes.length - 1) {
        EventService.emit('NEXT_ICON_DISAPPEAR', '1')
      } else {
        EventService.emit('NEXT_ICON_DISAPPEAR', '0')
      }
      this.isPlayTrailer = false
      this.isEpisodesFocus = true
      this.isTrailerssFocus = false
    }
    this.playInfo['epi'] = data['sitcomNO']
    this.playInfo['epiIndex'] = data['epiIndex']
    session.put('playInfo', this.playInfo)
    EventService.emit('playEpi')
  }
    /**
     * click trailers
     */
  clickTra (data) {
    this.playInfo['traIndex'] = data.clipIndex
    this.playInfo['tra'] = data.id
    if (Cookies.getJSON('IS_PROFILE_LOGIN')) {
      if (this.trailerIndex === data.clipIndex) {
        return
      }
      session.put('clipFlag', 'true')
      this.trailerIndex = data.clipIndex
      if (this.trailerIndex === this.clipfilesVODs.length - 1) {
        EventService.emit('NEXT_ICON_DISAPPEAR', '1')
      } else {
        EventService.emit('NEXT_ICON_DISAPPEAR', '0')
      }
      this.isPlayTrailer = true
      this.isEpisodesFocus = false
      this.isTrailerssFocus = true
    }
    session.put('playInfo', this.playInfo)
    EventService.emit('playTra', this.details)
  }
    /**
     * get the position of scroll bar by itself
     */
  setScrollTop () {
    let contentTop, scrollTop
    if (this.clipfilesVODs.length > this.minScrollIndex) {
      if (this.dom('clipfile') && this.dom('scrollSpan') && this.dom('trailerHasData') && this.dom('trailers-scroll')) {
        this.changeRate = (this.dom('clipfile')['clientHeight'] - this.dom('trailerHasData')['clientHeight']) /
                    (this.dom('trailers-scroll')['clientHeight'] - this.dom('scrollSpan')['clientHeight'])
      }
      if (this.trailerIndex < this.minScrollIndex) {
        contentTop = 0
        scrollTop = 0
      } else {
        if ((this.trailerIndex % 2) === 0) {
            contentTop = 0 - (((this.trailerIndex - this.minScrollIndex) / 2 + 1) * this.generalScroll)
          } else {
            contentTop = 0 - (((this.trailerIndex - this.minScrollIndex - 1) / 2 + 1) * this.generalScroll)
          }
      }
      scrollTop = -contentTop / this.changeRate
      this.dom('clipfile')['style']['top'] = contentTop + 'px'
      this.dom('scrollSpan')['style']['top'] = scrollTop + 'px'
    } else {
      if (this.dom('clipfile')) {
        this.dom('clipfile')['style']['top'] = '0px'
      }
    }
  }
}
