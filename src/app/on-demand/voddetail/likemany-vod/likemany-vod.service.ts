import * as _ from 'underscore'
import { Injectable } from '@angular/core'
import { PictureService } from 'ng-epg-ui/services/picture.service'
import { TranslateService } from '@ngx-translate/core'

@Injectable()
export class LikeManyService {
  /**
     * name for modules that would be used in this module
     */
  constructor (
        private pictureService: PictureService,
        private translate: TranslateService
  ) { }

  /**
     * package data of more like
     */
  getlikeVod (likeManyData) {
    let list = _.map(likeManyData, (list1, index) => {
      this.score(list1)
      let curSubjectName = 'like_many_of_this_kind'
      // get the url of the poster
      // resize the poster
      let url = list1['picture'] && list1['picture'].posters &&
                  this.pictureService.convertToSizeUrl(list1['picture'].posters[0],
                    { minwidth: 180, minheight: 240, maxwidth: 200, maxheight: 280 })
      return {
        vodId: list1['ID'],
        name: list1['name'],
        posterUrl: url,
        score: list1['averageScore'] || '0.0',
        floatInfo: list1,
        titleName: curSubjectName,
        subjectID: '-1'
      }
    })
    return list
  }
  /**
     * function to deal with score
     */
  score (list) {
    // the vod has been scored
    if (list.averageScore) {
      // the score is between 0 and 10, include 0 but except 10
      if (list.averageScore !== '10' && list.averageScore.substring(0, 2) !== '10') {
        // the score is integer number
        if (list.averageScore.length === 1) {
          list.averageScore += '.0'
          // the score is not integer number
        } else {
          let score = list.averageScore
          list.averageScore = score.substring(0, 4)
          list.averageScore = Number(list.averageScore)
          // ensure the scroe has one decimal number
          list.averageScore = list.averageScore.toFixed(1)
        }
        // the score is 10
      } else {
        list.averageScore = '10'
      }
      // the vod hasn't been scored
    } else {
      list.averageScore = '0.0'
    }
    return list
  }
}
