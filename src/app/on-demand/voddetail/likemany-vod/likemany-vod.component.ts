import { Component, Input, OnInit } from '@angular/core'
import { LikeManyService } from './likemany-vod.service'
import { Router } from '@angular/router'
import { VODRouterService } from 'src/app/shared/services/vod-router.service'
import { ActivatedRoute } from '@angular/router'
import { TranslateService } from '@ngx-translate/core'
const pageTracker = require('../../../../assets/lib/PageTracker')
import { session } from 'src/app/shared/services/session'

@Component({
  selector: 'app-like-many',
  styleUrls: ['./likemany-vod.component.scss'],
  templateUrl: './likemany-vod.component.html'
})

export class LikeManyComponent implements OnInit {
  /**
   * declare variables
   */
  private likeManyData
  likeTitle = this.translate.instant('like_many_of_this_kind').toUpperCase()
  /**
   * when the data changed
   */
  @Input() set setLikeManyData (likeManyData) {
    this.likeManyData = likeManyData
    this.showLikeVOD()
  }
  moreVodList: {
    'dataList': Array<any>,
    'row': string,
    'clientW': number,
    'suspensionID': string
  }
  // judge hide or show waterfall-noData
  public hasRecmList: boolean
  moreVodLists: {
    'dataList': Array<any>,
    'row': string,
    'clientW': number,
    'suspensionID': string
  }
  private likeManyDate: Array<any>
  /**
   * name for modules that would be used in this module
   */
  constructor (
    private likeManyService: LikeManyService,
    private route: ActivatedRoute,
    private router: Router,
    private vodRouter: VODRouterService,
    private translate: TranslateService
  ) { }
  /**
   * initialization
   */
  ngOnInit () {
    this.hasData()
    this.showLikeVOD()
  }

  showLikeVOD () {
    this.likeManyDate = this.likeManyService.getlikeVod(this.likeManyData[0].recmVODs).slice(0, 12)
    this.moreVodLists = {
      'dataList': this.likeManyDate,
      'row': '2',
      'clientW': document.body.clientWidth,
      'suspensionID': 'likemany'
    }
    this.moreVodList = this.moreVodLists
  }
  /**
   * function to judge there is or isn't recm vod
   */
  hasData () {
    if (this.likeManyService.getlikeVod(this.likeManyData[0].recmVODs).length !== 0) {
      this.hasRecmList = true
    } else {
      this.hasRecmList = false
    }
  }
  /**
   * open the detail page of the vod you click
   */
  vodDetail (event) {
    let curSubjectName = 'like_many_of_this_kind'
    const trackParams = {
      businessType: 1,
      contentCode: event[0],
      source: 3
    }
    pageTracker.pushBrowseRecmBehavior(trackParams)
    this.vodRouter.navigate(event[0])
  }
}
