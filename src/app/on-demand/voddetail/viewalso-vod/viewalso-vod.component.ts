import { Component, OnInit, Input } from '@angular/core'
import { ViewAlsoService } from './viewalso-vod.service'
import { Router } from '@angular/router'
import { VODRouterService } from 'src/app/shared/services/vod-router.service'
import { ActivatedRoute } from '@angular/router'
import { TranslateService } from '@ngx-translate/core'
const pageTracker = require('../../../../assets/lib/PageTracker')

@Component({
  selector: 'app-view-also',
  styleUrls: ['./viewalso-vod.component.scss'],
  templateUrl: './viewalso-vod.component.html'
})
export class ViewAlsoComponent implements OnInit {
  /**
   * declare variables
   */
  private viewAlsoData
  viewTitle = this.translate.instant('viewers_also_watched').toUpperCase()
  /**
   * when the data changed
   */
  @Input() set setViewAlsoData (viewAlsoData) {
    this.viewAlsoData = viewAlsoData
    this.showViewVOD()
  }
  moreVodList: {
    'dataList': Array<any>,
    'row': string,
    'clientW': number,
    'suspensionID': string
  }
  moreVodLists: {
    'dataList': Array<any>,
    'row': string,
    'clientW': number,
    'suspensionID': string
  }
  // help to judge show or hide an element
  public hasRecmList: boolean
  private viewAlsoDate: Array<any>
  /**
   * name for modules that would be used in this module
   */
  constructor (
    private viewAlsoService: ViewAlsoService,
    private route: ActivatedRoute,
    private router: Router,
    private vodRouter: VODRouterService,
    private translate: TranslateService
  ) { }
  /**
   * initialization
   */
  ngOnInit () {
    this.hasData()
    this.showViewVOD()
  }
  /**
   * show the vods data
   */
  showViewVOD () {
    let clientW = document.body.clientWidth
    // get the first 12 data
    this.viewAlsoDate = this.viewAlsoService.getviewVod(this.viewAlsoData[1].recmVODs).slice(0, 12)
    this.moreVodLists = {
      'dataList': this.viewAlsoDate,
      'row': '2',
      'clientW': clientW,
      'suspensionID': 'viewalso'
    }
    this.moreVodList = this.moreVodLists
  }
  /**
   * endow value to hasRecmList
   */
  hasData () {
    // there are this kind of vods
    if (this.viewAlsoService.getviewVod(this.viewAlsoData[1].recmVODs).length !== 0) {
      this.hasRecmList = true
    // there is no this kind of vod
    } else {
      this.hasRecmList = false
    }
  }
  /**
   * when you click a poster
   * @param event the target poster
   */
  vodDetail (event) {
    let curSubjectName = 'viewers_also_watched'
    const trackParams = {
      businessType: 1,
      contentCode: event[0],
      source: 3
    }
    pageTracker.pushBrowseRecmBehavior(trackParams)
    this.vodRouter.navigate(event[0])
  }
}
