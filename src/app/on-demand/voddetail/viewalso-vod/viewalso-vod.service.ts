import * as _ from 'underscore'
import { Injectable } from '@angular/core'
import { PictureService } from 'ng-epg-ui/services/picture.service'
import { TranslateService } from '@ngx-translate/core'

@Injectable()

export class ViewAlsoService {
  /**
     * name for modules that would be used in this module
     */
  constructor (
        private pictureService: PictureService,
        private translate: TranslateService
  ) { }

  getviewVod (viewalsoData) {
    let list = _.map(viewalsoData, (list1, index) => {
      this.score(list1)
      let curSubjectName = 'viewers_also_watched'
      // get url of the poster
      // resize the poster
      let url = list1['picture'] && list1['picture'].posters &&
                 this.pictureService.convertToSizeUrl(list1['picture'].posters[0],
                   { minwidth: 180, minheight: 240, maxwidth: 214, maxheight: 284 })
      return {
        vodId: list1['ID'],
        name: list1['name'],
        posterUrl: url,
        score: list1['averageScore'],
        floatInfo: list1,
        titleName: curSubjectName,
        subjectID: '-1'
      }
    })
    return list
  }
  /**
     * unify the format of score
     * @param list target vod
     */
  score (list) {
    // the scored vod
    if (list.averageScore) {
      // when the score is not 10
      if (list.averageScore !== '10' && list.averageScore.substring(0, 2) !== '10') {
        // integer score
        if (list.averageScore.length === 1) {
          list.averageScore += '.0'
          // noninteger score between 0 and 10
        } else {
          let score = list.averageScore
          list.averageScore = score.substring(0, 4)
          list.averageScore = Number(list.averageScore)
          // ensure this kind of score has one decimal
          list.averageScore = list.averageScore.toFixed(1)
        }
        // when the score is 10
      } else {
        list.averageScore = '10'
      }
      // vod which hasn't been scored
    } else {
      list.averageScore = '0.0'
    }
    return list
  }
}
