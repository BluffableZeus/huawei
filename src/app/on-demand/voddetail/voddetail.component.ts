import * as _ from 'underscore'
import { Component, ViewChild, OnInit, OnDestroy } from '@angular/core'
import { VodDetailHomeService } from './vodDetail.service'
import { ActivatedRoute } from '@angular/router'
import { VodPlayerComponent } from './vod-player/vod-player.component'
import { VodDetailComponent } from './vod-detail/vod-detail.component'
import { EventService } from 'ng-epg-sdk/services'
import { session } from 'src/app/shared/services/session'
import { MediaPlayService } from '../../component/mediaPlay/mediaPlay.service'

@Component({
  selector: 'app-voddetail-home',
  styleUrls: ['./voddetail.component.scss'],
  templateUrl: './voddetail.component.html'
})

export class VoddetailHomeComponent implements OnInit, OnDestroy {
  public homeData = {}
    /**
     * list of the recommended result
     */
  public likeManyInfo = []
    /**
     * list of the recommended result
     */
  public viewAlsoInfo = []
  public vodDetail = {}
  public stillsVodInfo: Array<any>
    /**
     * cast information
     */
  public castRolesInfo = []
    /**
     * detail of the VOD
     */
  public details: Object
    /**
     * has details of the VOD
     */
  public vodPlayInfo: Object
  public seasonsInfo = []
  public VODcrumbsRoutes

  @ViewChild(VodPlayerComponent) vodplayer: VodPlayerComponent
  @ViewChild(VodDetailComponent) vodDetailObject: VodDetailComponent
    /**
     * name of the VOD
     */
  private curName = ''
  private curSubjectID = ''
  private VODID: string = ''
  private showSeasons = false
  private stillHaveData
  private curHost
  private isGetSubjectName = false
  private isCastName: boolean = false
  private isCastcurSubjectID
  constructor (
        private voddetail: VodDetailHomeService,
        public route: ActivatedRoute,
        public mediaPlayService: MediaPlayService
    ) {
  }

  ngOnInit () {
    document.body.scrollTop = 0
    document.documentElement.scrollTop = 0
    this.route.params.subscribe(params => {
      session.remove('clipFlag')
      this.VODID = params['id']
      this.curHost = params['host']
      if (params['name'] === 'banner') {
        this.curHost = 'banner'
        this.curSubjectID = ''
      } else if (params['host'] === 'Search' || params['host'] === 'search' ||
                params['name'] === 'Search' || params['name'] === 'search') {
          this.curHost = 'search'
          this.curSubjectID = ''
        } else {
          this.curSubjectID = params['name']
        }
      if (params['subjectid'] && params['subjectid'] === 'isCast') {
        this.isCastName = true
        this.isCastcurSubjectID = { name: '' }
      }
      if (params['subjectid'] && params['subjectid'] === 'isDetailsCast') {
        this.curSubjectID = ''
        this.isCastName = true
        this.isCastcurSubjectID = { name: '' }
      }
      this.getData(this.VODID)
        // send report param subjectID
      EventService.removeAllListeners(['subjectiddd'])
      EventService.on('subjectiddd', () => {
        if (params['subjectid']) {
            this.vodplayer.getsubjectid(params['subjectid'])
          } else {
            this.vodplayer.getsubjectid('-1')
          }
      })
    })
      // the event of play episode
    EventService.on('playEpi', () => {
      let playInfo = session.get('playInfo')
      this.vodplayer.playEpi(playInfo)
      let tempEpi
      if (playInfo.epi.length === 1) {
        if (Cookies.getJSON('IS_PROFILE_LOGIN')) {
            tempEpi = this.prefixZero(playInfo.epi, 2)
            this.vodDetailObject.epiNumber = {
              'num': tempEpi
            }
          }
      } else {
        if (Cookies.getJSON('IS_PROFILE_LOGIN')) {
            this.vodDetailObject.epiNumber = {
              'num': playInfo.epi
            }
          }
      }
    })
    EventService.removeAllListeners(['scoreSuccess'])
    EventService.on('scoreSuccess', (averscore) => {
      this.vodDetailObject.refreshRatingStar(averscore)
    })
    EventService.on('FIRST_TIME_LOGIN', () => {
      this.getData(this.VODID, true)
    })
    if (document.addEventListener) {
      document.addEventListener('keydown', this.keyDownHandler.bind(this))
    } else if (document['attachEvent']) {
      document['attachEvent']('onkeydown', this.keyDownHandler.bind(this))
    }
  }

  keyDownHandler (e) {
    if (this.mediaPlayService.isFullScreen()) {
      e = e || window.event
      if (e.keyCode && e.keyCode === 40) {
        if (e && e.preventDefault()) {
            e.preventDefault()
          } else {
            e.returnValue = false
          }
        return false
      }
    }
  }

    /**
     * remove listeners
     */
  ngOnDestroy () {
    session.remove('VOD_IS_PLAYING')
    EventService.removeAllListeners(['ScreenSize'])
    EventService.removeAllListeners(['playEpi'])
    EventService.removeAllListeners(['PLAYFILM'])
    EventService.removeAllListeners(['playTra'])
    EventService.removeAllListeners(['NEXT'])
    EventService.removeAllListeners(['EXIT_VOD_FULLSCREEN'])
    if (document.removeEventListener) {
      document.removeEventListener('keydown', this.keyDownHandler.bind(this))
    } else if (document['detachEvent']) {
      document['detachEvent']('onkeydown', this.keyDownHandler.bind(this))
    }
  }

  prefixZero (str, len) {
    str = '' + (str || '')
    if (str.length >= len) {
      return str
    }
    let gapLen = len - str.length, i, arr = []
    for (i = 0; i < gapLen; i++) {
      arr[i] = '0'
    }

    return arr.join('') + str
  }

  setCrumbsRoutes (resp) {
    let crumbs
    crumbs = [{
      title: this.curSubjectID || '',
      hash: session.get('VOD_DETAIL_CRUMBS')
    }, {
      title: resp['VODDetail'].name || '',
      hash: ''
    }]
    if (crumbs[0]['title'] === 'like_many_of_this_kind' || crumbs[0]['title'] === 'viewers_also_watched' ||
            crumbs[0]['title'] === 'seasons' || crumbs[0]['title'] === 'CAST'
      ) {
      crumbs = crumbs.slice(-1)
    }
    this.VODcrumbsRoutes = crumbs
  }

  getDataTwice (resp, flag?: any) {
    let self = this
    this.vodPlayInfo = this.details
    let respOrg = resp
    if (this.curHost === 'search') {
      if (resp['VODDetail'].subjectIDs[0]) {
        this.voddetail.querySubjectDetail({
            'subjectIDs': [resp['VODDetail'].subjectIDs[0]]
          }).then((respData) => {
            this.curSubjectID = respData && respData.subjects && respData.subjects[0] &&
                        respData.subjects[0].name ? respData.subjects[0].name : ''
            this.setCrumbsRoutes(resp)
            this.isGetSubjectName = true
          }, respa => {
            this.isGetSubjectName = true
            if (flag) {
              return
            }
            this.setCrumbsRoutes(respOrg)
          })
      } else {
        this.setCrumbsRoutes(respOrg)
      }
    } else {
      this.isGetSubjectName = true
      if (flag) {
        return
      }
      this.setCrumbsRoutes(resp)
    }
  }

    /**
     * show data of the vod detail page
     */
  getData (VODID, flag?: any) {
    let self = this
    this.voddetail.getVodDetail({
      VODID: VODID || '',
      queryDynamicRecmContent: {
        recmScenarios: [{ contentType: 'VOD', entrance: 'VOD_Like_Many_of_its_Kind', contentID: VODID, count: '12', offset: '0' },
            { contentType: 'VOD', entrance: 'VOD_Viewers_also_Watched', contentID: VODID, count: '12', offset: '0' }]
      }
    }).then((resp) => {
      this.stillsVodInfo = this.getStillsVodInfo(resp)
      if (resp['VODDetail'] && resp['VODDetail'].picture && resp['VODDetail'].picture.stills && resp['VODDetail'].picture.stills[0] &&
                (resp['VODDetail'].picture.stills[0] !== '')) {
          this.stillHaveData = true
        } else {
          this.stillHaveData = false
        }
      if (resp && resp.VODDetail && resp.VODDetail.picture && resp.VODDetail.picture['previews']) {
          session.put('VODDETAIL_PICTURE_PREVIEW', resp.VODDetail.picture['previews'])
        }
      this.castRolesInfo = resp['VODDetail'].castRoles
      this.likeManyInfo = resp['recmContents']
      this.viewAlsoInfo = resp['recmContents']
      this.curName = resp['VODDetail'].name

      this.showSeason(resp)
      this.episodesCom(resp)
      this.getDataTwice(resp, flag)
    })
  }

    /**
     * get vod information
     */
  getStillsVodInfo (resp) {
    let self = this
    let info = [resp['VODDetail'].picture && resp['VODDetail'].picture.stills, resp['VODDetail'], this.curSubjectID]
    return info
  }

    /**
     * judge show seasons or not
     */
  showSeason (resp) {
    let self = this
    if (resp['VODDetail']['VODType'] === '2' || resp['VODDetail']['VODType'] === '3') {
      this.showSeasons = true
      if (resp['VODDetail'].brotherSeasonVODs && resp['VODDetail'].brotherSeasonVODs.length > 0) {
        this.seasonsInfo = resp['VODDetail'].brotherSeasonVODs
      } else {
        this.seasonsInfo = []
      }
    } else {
      this.showSeasons = false
    }
  }

    /**
     * fill in details
     */
  episodesCom (resp) {
    let self = this
    this.details = resp['VODDetail']
    if (this.curSubjectID && this.curSubjectID !== '') {
      this.details['subjectid'] = this.curSubjectID
    } else {
      this.details['subjectid'] = ''
    }
    if (this.curHost === 'search') {
      this.details['hostname'] = 'sharesearch'
    } else if (this.curHost === 'banner') {
      this.details['hostname'] = 'banner'
    }
    if (resp['VODDetail']['episodes'] && resp['VODDetail']['episodes'].length > 0) {
      if (resp['VODDetail']['bookmark'] && resp['VODDetail']['bookmark']['sitcomNO']) {
        let sitcomNO = resp['VODDetail']['bookmark']['sitcomNO']
        this.details['sitcomNO'] = sitcomNO
      } else {
        this.details['sitcomNO'] = resp['VODDetail']['episodes'][0]['sitcomNO']
      }
      _.map(this.details['episodes'], (item, index) => {
        item['epiIndex'] = index
      })
    }
  }
}
