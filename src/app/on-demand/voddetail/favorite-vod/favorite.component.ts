import { Component, Input } from '@angular/core'
import { TranslateService } from '@ngx-translate/core'

@Component({
  selector: 'app-favorite-vod',
  styleUrls: ['./favorite.component.scss'],
  templateUrl: './favorite.component.html'
})

export class FavoriteVodComponent {
    /**
     * declare variables
     */
  public favInfoMes: string = ''
    /**
     * when the data changed
     */
  @Input() set favInfo (message) {
    this.favInfoMes = message
      // according to different conditions, show different words
    if (this.favInfoMes === '0') {
      this.favMessage = this.translate.instant('add_Favorite')
    } else {
      this.favMessage = this.translate.instant('vod_remove_fav')
    }
  }

  public favMessage: string = ''
    /**
     * name for modules that would be used in this module
     */
  constructor (
        private translate: TranslateService
    ) { }
}
