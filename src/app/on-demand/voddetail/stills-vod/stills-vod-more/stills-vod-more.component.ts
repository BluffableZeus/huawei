import * as _ from 'underscore'
import { Component, OnInit } from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'
import { VodDetailHomeService } from '../../../voddetail/vodDetail.service'
import { PictureService } from 'ng-epg-ui/services/picture.service'

const LEFT_KEY = 37, RIGHT_KEY = 39

@Component({
  selector: 'app-stills-vod-more',
  styleUrls: ['./stills-vod-more.component.scss'],
  templateUrl: './stills-vod-more.component.html'
})

export class StillsMoreComponent implements OnInit {
  public curStillsIndex: number
  public stillsLength: number
  public stillsIndex: number
  public stillsLists: Array<string>
  public stillsItem
  public stillsFlag: string = 'true'
  public title: string
  public stillsListsStill = []
  public stillsLarge = []
  public id: string
  public index: string
  public name: string
  public subjectId
  public showTitle: boolean = false
  public stillsCrumbsRoutes
    // mouse offset on x-axis of the target element
  private x: number = 0
  constructor (
        private route: Router,
        private _route: ActivatedRoute,
        private voddetail: VodDetailHomeService,
        private pictureService: PictureService) {
  }
    /**
     * Initialization
     */
  ngOnInit () {
    let self = this
    if (window.addEventListener) {
      document.addEventListener('keyup', function (e) {
        if (e.keyCode === LEFT_KEY || e.which === LEFT_KEY) {
            self.onClickLeft()
          } else if (e.keyCode === RIGHT_KEY || e.which === RIGHT_KEY) {
            self.onClickRight()
          }
      })
    }
    this._route
        .params
        .subscribe(params => {
          this.id = params['id']
          this.index = params['index']
          this.subjectId = params['subjectName']
          if (this.index === 'all') {
            // show more stage photo
            this.getData(this.id)
            this.stillsFlag = 'true'
          } else {
            // a page for show stage photo
            this.getData(this.id)
            this.stillsFlag = 'false'
          }
        })
  }
    /**
     * deal with data
     */
  getData (VODID) {
    let self = this
    this.voddetail.getVodDetail({
      VODID: VODID || ''
    }).then((resp) => {
      self.stillsListsStill = resp['VODDetail'] && resp['VODDetail'].picture && resp['VODDetail'].picture.stills
      self.stillsLists = _.extend([], resp['VODDetail'] && resp['VODDetail'].picture && resp['VODDetail'].picture.stills)
        // set picture style
      for (let i = 0; i < self.stillsLists.length; i++) {
          self.stillsLarge[i] = self.pictureService.convertToSizeUrl(self.stillsLists[i],
            { minwidth: 980, minheight: 511, maxwidth: 1280, maxheight: 776 })
          self.stillsLists[i] = self.pictureService.convertToSizeUrl(self.stillsLists[i],
            { minwidth: 180, minheight: 180, maxwidth: 180, maxheight: 180 })
        }
      self.name = resp['VODDetail'].name
      if (this.stillsFlag === 'false') {
          this.show(this.index)
        }
        // get film name
      this.title = this.name
      this.setCrumbsRoutes()
      this.showTitle = true
    })
  }
  setCrumbsRoutes () {
    let crumbs = [{
      title: 'stills',
      hash: ''
    }]
    this.stillsCrumbsRoutes = crumbs
  }

    /**
     * the order number under the page of show stage photo
     */
  show (index) {
    this.stillsIndex = Number(index)
    this.stillsItem = this.stillsListsStill[this.stillsIndex]
    this.stillsItem = this.pictureService.convertToSizeUrl(this.stillsItem,
        { minwidth: 980, minheight: 511, maxwidth: 1280, maxheight: 776 })
    this.stillsLength = this.stillsLists && this.prefixZero(this.stillsLists.length, 2)
    this.curStillsIndex = this.prefixZero((this.stillsIndex + 1), 2)
  }
    /**
     * click more stage photo,close big stage photo page,show more stage photo page
     */
  changeFlag () {
    this.stillsFlag = 'true'
  }
    /**
     * click left
     */
  onClickLeft () {
    this.stillsIndex--
    if (this.stillsIndex < 0) {
      this.stillsIndex = this.stillsLarge && this.stillsLarge.length - 1
    }
    this.stillsItem = this.stillsLarge[this.stillsIndex]
    this.curStillsIndex = this.prefixZero((this.stillsIndex + 1), 2)
  }
    /**
     * click right
     */
  onClickRight () {
    this.stillsIndex++
    if (this.stillsIndex > this.stillsLarge.length - 1) {
      this.stillsIndex = 0
    }
    this.stillsItem = this.stillsLarge[this.stillsIndex]
    this.curStillsIndex = this.prefixZero((this.stillsIndex + 1), 2)
  }
    /**
     * not enough double-digit,add zero
     */
  prefixZero (str, len) {
    str = '' + (str || '')
    if (str.length >= len) {
      return str
    }

    let gapLen = len - str.length, i, arr = []
    for (i = 0; i < gapLen; i++) {
      arr[i] = '0'
    }

    return arr.join('') + str
  }
    /**
     * close more stage photo page,show big stage photo
     */
  stillMoreEnlarge (event) {
    this.show(event)
    this.stillsFlag = 'false'
  }
    /**
     * get your mouse offset on x-axis
     * @param event target element
     */
  getOffsetX (event) {
    let evt = event || window.event
    let srcObj = evt.target || evt.srcElement
    if (evt.offsetX) {
      return evt.offsetX
    } else {
      let rect = srcObj.getBoundingClientRect()
      let clientx = evt.clientX
      return clientx - rect.left
    }
  }
  getStyle (event) {
    this.x = this.getOffsetX(event)
  }
    /**
     * call the click left or right function
     */
  changePic () {
      // depend on different screen size, the judge condition will change
    let clientW: number = window.innerWidth
      // big screen
    if (clientW > 1440) {
        // more than half of the picture on x-axis on big screen
      if (this.x > 690) {
        this.onClickRight()
      } else {
        this.onClickLeft()
      }
        // small screen
    } else {
        // more than half of the picture on x-axis on small screen
      if (this.x > 490) {
        this.onClickRight()
      } else {
        this.onClickLeft()
      }
    }
  }
}
