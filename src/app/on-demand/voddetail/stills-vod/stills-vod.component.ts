import { Component, Input, OnInit } from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'
import { PictureService } from 'ng-epg-ui/services/picture.service'
import { TrackService } from '../../../shared/services/track.service'
import { TranslateService } from '@ngx-translate/core'

@Component({
  selector: 'app-stills-vod',
  styleUrls: ['./stills-vod.component.scss'],
  templateUrl: './stills-vod.component.html'
})

/**
  Deprecated component, there will be no image galleries.
 */
export class StillsVODComponent implements OnInit {
    /**
     * declare variables
     */
  public hasRecmList: boolean
  public stillsLists = []
  public moreStillsLists: Array<any>
  public id: string
  public name: string
  public subjectID
  stillTittle = this.translate.instant('stills').toUpperCase()
  currentRouteUrl = location.pathname
    /**
     * name for modules that would be used in this module
     */
  constructor (
        private route: Router,
        private _route: ActivatedRoute,
        private pictureService: PictureService,
        private trackService: TrackService,
        private translate: TranslateService
    ) { }
    /**
     * get the data of the stage photo
     */
  @Input() set stillsResult (stillsVod) {
    this.stillsLists = stillsVod[0]
    this.name = stillsVod[1]
    this.subjectID = stillsVod[1]['ID']
    if (this.stillsLists && this.stillsLists.length > 0 && this.stillsLists[0] !== '') {
      this.hasRecmList = true
      for (let i = 0; i < this.stillsLists.length; i++) {
          // resize the photo
        this.stillsLists[i] = this.pictureService.convertToSizeUrl(this.stillsLists[i],
            { minwidth: 180, minheight: 180, maxwidth: 180, maxheight: 180 })
      }
    } else {
      this.hasRecmList = false
    }
  }
    /**
     * initialization
     */
  ngOnInit () {
    this._route
        .params
        .subscribe(params => {
          this.id = params['id']
        })
      // show 7 stage photo in large screen
    this.moreStillsLists = this.stillsLists && this.stillsLists.slice(0, 7)
  }
    /**
     * click the title of stills to jump to another page to view more stills.
     */
  moreStillShow () {
      // let changePage = '#/on-demand/stillsMore/' + Number(this.id) + '/' + 'all' + '/' + this.subjectID;
      // this.trackService.onPageChange(this.currentRouteUrl, changePage);
      // if (this.hasRecmList) {
      //     session.put('VOD_DETAIL_CRUMBS_HREF', true);
      //     session.put('VOD_DETAIL_CRUMBS', location.href);
      //     window.open(window.location.href.split('#')[0]
      //         + '#/on-demand/stillsMore/' + Number(this.id) + '/' + 'all' + '/' + this.subjectID );
      // }
  }
    /**
     * click one of the stills to view big still.
     */
  stillEnlarge (event) {
      // session.put('VOD_DETAIL_CRUMBS_HREF', true);
      // let changePage = '#/on-demand/stillsMore/' + Number(this.id) + '/' + event + '/' + this.subjectID;
      // this.trackService.onPageChange(this.currentRouteUrl, changePage);
      // window.open(window.location.href.split('#')[0] + '#/on-demand/stillsMore/' + Number(this.id) + '/' + event + '/' + this.subjectID );
  }
}
