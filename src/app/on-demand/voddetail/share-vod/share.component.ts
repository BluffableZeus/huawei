import * as _ from 'underscore'
import { Component, OnInit, Input } from '@angular/core'
import { EventService } from 'ng-epg-sdk/services'
import { CommonService } from '../../../core/common.service'
import { AccountLoginComponent } from '../../../login/account-login'
import { TimerService } from '../../../shared/services/timer.service'

@Component({
  selector: 'app-share-vod',
  styleUrls: ['./share.component.scss'],
  templateUrl: './share.component.html'
})

export class ShareVodComponent implements OnInit {
    /**
     * declare variables
     */
  private showShare = false
  private shareDetail = {}
    // array of share ways
  public text_share = []
    /**
     * name for modules that would be used in this module
     */
  constructor (
        private commonService: CommonService,
        private timerService: TimerService
    ) { }
    /**
     * when the data changed
     */
  @Input() set shareVODData (data: Array<any>) {
    let self = this
    this.shareDetail = data
      // call a interface
    this.timerService.getCaheCustomConfig().then(resp => {
        // deal with the data come from the interface
      let configData = self.timerService.filterCustomConfig(resp)
      if (configData && configData['values']) {
        let values = configData['values']
        let text_share_app = _.find(values, item => {
            return self.trim(item['key']) === 'text_share_app'
          })
          // data from the interface or default
        self.text_share = text_share_app && text_share_app['values'] && text_share_app['values'][0].split(',') ||
                    ['facebook', 'twitter']
        self.text_share = _.unique(self.text_share)
        self.text_share = _.filter(self.text_share, function (share_app) {
            if (_.contains(['facebook', 'twitter'], share_app)) {
              return true
            }
          })
        let rownum = Math.ceil((self.text_share.length) / 6)
          // every 6 or less than 6 text_share occupy one line
        if (self.text_share.length <= 6) {
            document.getElementsByClassName('shareContent')[0]['style']['height'] = 170 + 'px'
            document.getElementsByClassName('share-vod')[0]['style']['width'] = Math.ceil(self.text_share.length * 100) + 40 + 'px'
          } else {
            document.getElementsByClassName('shareContent')[0]['style']['height'] = rownum * 130 + 40 + 'px'
            document.getElementsByClassName('share-vod')[0]['style']['width'] = 640 + 'px'
          }
      }
    })
  }
    /**
     * initialization
     */
  ngOnInit () {
    EventService.removeAllListeners(['miss_close'])
    EventService.on('miss_close', function (closeState) {
      if (!closeState) {
        document.body.onclick = function () {
            if (!document.querySelector('.weixin_img')) {
              EventService.emit('ISSHOW_SHARE', false)
            }
          }
        document.querySelector('.share-vod').addEventListener('click', function myfun (e) {
            e.stopPropagation()
          })
      }
    })
  }
    /**
     * delete the space in a string
     * @param str target string
     */
  trim (str) {
    str = str.replace(/(^\s)|(\s*$)/g, '').replace(/(^\s*)/g, '').replace(/(\s*$)/g, '')
    return str
  }
    /**
     * when click the shareTitle
     * @param type name of the share title
     */
  share (type) {
      // if is guest login, eject the login prompt
    if (Cookies.getJSON('IS_GUEST_LOGIN')) {
      EventService.emit('CLOSED_LOGIN_DIALOG')
      EventService.on('MD_DIALOG_CONTAINER', function () {
        document.getElementsByTagName('md-dialog-container')[0]['style'].position = 'fixed'
      })
      this.commonService.openDialog(AccountLoginComponent)
      return
    }
    let name = this.shareDetail['name']
    if (name && name.length >= 35) {
      name = name.slice(0, 32) + '...'
    }
    let title = this.shareDetail['introduce']
    let titleLength = 258 - name.length - 3
    if (title && title.length >= titleLength) {
      title = title.slice(0, titleLength - 3) + '...'
    }
      // use the url you get or the default
    let posters = this.shareDetail['picture'] && this.shareDetail['picture']['posters'] || 'assets/img/default/home_poster.png'
    let subjectid = this.shareDetail['subjectid']
    let share_url
    if (this.shareDetail['hostname'] === 'sharesearch') {
      share_url = location.href.split('index.html')[0] + 'share/index.html?id=' +
                'search' + '/' + this.shareDetail['ID'] + '/' + subjectid
    } else if (this.shareDetail['hostname'] === 'banner') {
      share_url = location.href.split('index.html')[0] + 'share/index.html?id=' + this.shareDetail['ID'] + '/' + 'banner'
    } else {
      share_url = location.href.split('index.html')[0] + 'share/index.html?id=' + this.shareDetail['ID'] + '/' + subjectid
    }
      // open a new page by the share title
    if (type === 'facebook') {
      window.open('http://www.facebook.com/share.php?u='.concat(encodeURIComponent(share_url)))
    } else if (type === 'twitter') {
      window.open('http://twitter.com/home/?status='.concat(encodeURIComponent(document.title))
          .concat(' ').concat(encodeURIComponent(share_url)))
    }
  }
    /**
     * when the mouse enter the share-vod
     */
  mouseEnterShare () {
    this.showShare = true
      // emit an event to tell the browser to do something
    EventService.emit('ISSHOW_SHARE', this.showShare)
  }
    /**
     * when the mouse leave the share-vod
     */
  mouseLeaveShare () {
    this.showShare = false
    if (!document.querySelector('.weixin_img')) {
      EventService.emit('ISSHOW_SHARE', this.showShare)
    }
  }
}
