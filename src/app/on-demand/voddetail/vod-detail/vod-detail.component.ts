import * as _ from 'underscore'
import { Component, Input, OnInit, OnDestroy } from '@angular/core'
import { VodDetailService } from './vod-detail.service'
import { session } from 'src/app/shared/services/session'
import { EventService } from 'ng-epg-sdk/services'
import { CommonService } from '../../../core/common.service'
import { AccountLoginComponent } from '../../../login/account-login'

const FIVE = 5

@Component({
  selector: 'app-vod-detail',
  styleUrls: ['./vod-detail.component.scss'],
  templateUrl: './vod-detail.component.html'
})

export class VodDetailComponent implements OnInit, OnDestroy {
    /**
     * declare variables
     */
  public epiNumber = {
    num: ''
  }
  public details: any = []
    // array to put info about actors and directors
  public castRoles: any
  public starArray: number[] = new Array()
    // name of the director
  public directors
  public directorAllshow = []
    // name of the actor
  public actors
  public actorAllShow = []
  public addFavorite: string = ''
  public isFav: string = ''
    // show or hide the app-favorite-vod drop down list
  public isShow: string = 'false'
    // show or hide the share suspension frame
  public isShowShare: boolean = false
    // the date of the produce
  public produceDate: string = ''
    // the zone of the produce
  public produceZoneName: string = ''
    // type of the vod
  public vodGenres: string = ''
  public vodAllGenres = []
    // rating
  public ratingName: string = ''
    // array of articulation
  public definitions = []
    // init the averagescore
  public averageScore: string = ''
  public scoreTimes: number
    // judge show or hide the extra introduce
  public showMore: boolean = false
    // help to judge the direction of introduce more or less arrow
  public showHeight: boolean = false
    // show or hide the score suspension frame
  public scoreShow: string = 'false'
  public isScoreShow: boolean = false
  public isGenerMore: boolean = false
    // show or hide the director information frame
  public isDirectorShow: boolean = false
    // show or hide the actor information frame
  public isActorShow: boolean = false
  public score: string
  public leaveFav
  public leaveScore
  public leaveShare
  public info: any
  public eventflag: boolean = false
  public eventflagScore: boolean = false
  public getWeakTip
  public getIntervalTip
  public maxOffsetHeight = 0
    // whether to show episode
  public isShowEpi: boolean = false
    // whether to show season
  public isShowSeason: boolean = false
    // the season number
  public seasonNumber = {
    num: ''
  }
  public isFirefox = false
  public shareVODDetail = []
  public isweixin = false
  public share_url = ''
    /**
     * name for modules that would be used in this module
     */
  constructor (
        private vodDetailService: VodDetailService,
        private commonService: CommonService
    ) { }
    /**
     * initialization
     */
  ngOnInit () {
    let self = this
      // clear session
    session.remove('scoreValue')
    clearTimeout(this.getWeakTip)
    this.getWeakTip = setTimeout(() => {
      self.showMoreHeight()
    }, 2000)
    let ua = navigator.userAgent.toLowerCase()
    this.isFirefox = !!ua.match(/firefox\/([\d.]+)/)
      // play next soap opera
    EventService.on('NEXT', function (index) {
      let epiData = self.getEpiInfoByIndex(self.details, index)
      self.epiNumber = {
        'num': self.prefixZero(epiData['sitcomNO'], 2)
      }
    })
      // when the screen size change
    EventService.on('ScreenSize', function () {
      self.showMoreHeight()
    })
    EventService.removeAllListeners(['ISSHOW_WEIXIN_SHARE'])
    EventService.on('ISSHOW_WEIXIN_SHARE', function (share_url) {
      self.isweixin = true
      self.share_url = share_url
    })
    EventService.removeAllListeners(['CHANGE_VODDETAILFAV'])
    EventService.on('CHANGE_VODDETAILFAV', function (isFavReq) {
      if (isFavReq['vodID'] === self.details['ID']) {
        if (isFavReq['isFav'] === '0') {
            self.isFav = '0'
            self.addFavorite = 'true'
            if (document.getElementById('favoriteIcon')) {
              document.getElementById('favoriteIcon').style.background = 'url(assets/img/20/ondemand_rio2-iocn4_20x20.png)'
            }
          } else {
            self.isFav = '1'
            self.addFavorite = 'false'
            if (document.getElementById('favoriteIcon')) {
              document.getElementById('favoriteIcon').style.background = 'url(assets/img/20/ondemand_rio2-iocn_2.png)'
            }
          }
      }
    })
  }
    /**
     * get the data of episodes
     */
  getEpiInfoByIndex (vodDetails, index) {
    let epiInfo = _.find(vodDetails['episodes'], item => {
      return item['epiIndex'] === index
    }) || {}
    return epiInfo
  }
    /**
     * when exit the component, clear all the timer and events
     */
  ngOnDestroy () {
    clearInterval(this.getIntervalTip)
    clearTimeout(this.getWeakTip)
    clearTimeout(this.leaveFav)
    clearTimeout(this.leaveScore)
    clearTimeout(this.leaveShare)
  }
    /**
     * when the data changed
     */
  @Input() set setDetail (details) {
    let self = this
    this.details = details
    this.shareVODDetail = details
    clearInterval(this.getIntervalTip)
    this.getIntervalTip = setInterval(() => {
      if ((this.details['introduce'] && this.details['introduce'].length > 0) && document.getElementById('introduce')) {
        self.showMoreHeight()
        clearInterval(this.getIntervalTip)
      }
    }, 10)
    this.dealRoles()
    this.initContent()
    this.refreshRatingStar(this.averageScore)
    this.info = {
      'contentIDs': this.details['ID'],
      'contentTypes': this.details['contentType'],
      'userScore': this.details['userScore'] || 0
    }
      // guest login
    if (Cookies.getJSON('IS_GUEST_LOGIN')) {
      this.isFav = '0'
        // profile login
    } else {
      self.isFav = details.favorite ? '1' : '0'
    }
      // vod hasn't been added favorite
    if (this.isFav === '0') {
      this.addFavorite = 'true'
      document.getElementById('favoriteIcon').style.background = 'url(assets/img/20/ondemand_rio2-iocn4_20x20.png)'
        // vod has been added favorite
    } else {
      this.addFavorite = 'false'
      document.getElementById('favoriteIcon').style.background = 'url(assets/img/20/ondemand_rio2-iocn_2.png)'
    }
    if (Cookies.getJSON('IS_PROFILE_LOGIN') === true) {
      if (!_.isUndefined(this.details['userScore'])) {
        if (this.details['userScore'] !== 0) {
            this.score = 'true'
          } else {
            this.score = 'false'
          }
      } else {
        this.score = 'false'
      }
    } else {
      this.score = 'false'
    }
  }
    /**
     * refresh rating star according to the averagescore
     * @param averscore score that get from data
     */
  refreshRatingStar (averscore?) {
    this.averageScore = Number(averscore).toFixed(1)
    if (parseInt(this.averageScore, 10) === 10) {
      this.averageScore = '10'
    }
    for (let i = 0; i < 5; i++) {
      let result = Math.round(parseFloat(this.averageScore)) - (2 * (i + 1))
      if (result >= 0) {
        this.starArray[i] = 2
      } else if (result === -1) {
          this.starArray[i] = 1
        } else {
          this.starArray[i] = 0
        }
    }
  }
    /**
     * when you score the vod
     */
  getnewscore () {
    this.scoreTimes += 1
  }
    /**
     * deal with actor's and director's data
     */
  dealRoles () {
    this.directors = ''
    this.actors = ''
    let self = this
    this.castRoles = this.details['castRoles']
    _.map(this.castRoles, function (role, index) {
        // accroding to roleType is 0, ensure this is actor
      if (parseInt(role['roleType'], 10) === 0) {
        _.map(role['casts'], function (cast, i) {
            self.actorAllShow.push(cast['castName'])
            // the first actor
            if (i === 0) {
              self.actors = self.actors + cast['castName']
              // actors after the first one, split them by comma
            } else {
              self.actors = self.actors + ', ' + cast['castName']
            }
          })
      }
    })
      // special condition, the name of actor is empty
    if (self.actors === '') {
      self.actors = '/'
    }
    _.map(this.castRoles, function (role) {
        // according to roleType is 1, ensure this is director
      if (parseInt(role['roleType'], 10) === 1) {
        _.map(role['casts'], function (cast, j) {
            self.directorAllshow.push(cast['castName'])
            // the first director
            if (j === 0) {
              self.directors = self.directors + cast['castName']
              // directors after the first one, split them by comma
            } else {
              self.directors = self.directors + ', ' + cast['castName']
            }
          })
      }
    })
      // special condition, the name of director is empty
    if (self.directors === '') {
      self.directors = '/'
    }
  }
    /**
     * the show state of introduce and its arrow
     */
  showMoreHeight () {
    let self = this
    if (document.getElementById('introduceShowIcon')) {
      this.maxOffsetHeight = document.getElementById('introduceShowIcon').offsetHeight
    }
    let introduce = document.getElementById('introduce')
    let intOffsetHeight
    if (introduce) {
      intOffsetHeight = introduce.offsetHeight || 0
      if (this.details['introduce'] && this.details['introduce'].length > 0 && (intOffsetHeight === 0)) {
      } else {
          // hide the introduce more or less arrow when the height is not enough
        if (this.maxOffsetHeight < 44) {
            this.showHeight = false
            // show the introduce more or less arrow
          } else {
            self.showHeight = true
            // show the extra introduce
            if (self.showMore) {
              introduce['style']['height'] = 'auto'
              introduce['style']['-webkit-line-clamp'] = 'inherit'
              // hide the extra introduce
            } else if (!self.showMore) {
              introduce['style']['height'] = '40px'
              introduce['style']['-webkit-line-clamp'] = '2'
            }
          }
      }
    }
  }
    /**
     * init content info
     */
  initContentTwice () {
    this.produceZoneName = this.getProduceZoneName()
    this.ratingName = this.getRatingName()
    this.produceDate = this.timeFormat(this.details['produceDate'])
      // when vod genres has data
    if (this.details['genres'] && this.details['genres'][0] && this.details['genres'][0].genreName) {
      this.vodAllGenres = []
      this.vodGenres = ''
      for (let i = 0; i < this.details['genres'].length; i++) {
        this.vodAllGenres.push(this.details['genres'][i].genreName)
        if (i < 5) {
            if (i === 0) {
              if (this.produceZoneName) {
                // split produceZoneName and vodGenres by comma
                this.vodGenres = this.vodGenres + ', ' + this.details['genres'][i].genreName
              } else {
                this.vodGenres = this.vodGenres + ' ' + this.details['genres'][i].genreName
              }
            } else {
              // split vodGenres by space
              this.vodGenres = this.vodGenres + '  ' + this.details['genres'][i].genreName
            }
          } else if (i === FIVE) {
            this.vodGenres = this.vodGenres + '...'
            this.isGenerMore = true
          }
      }
        // when vod genres doesn't have data
    } else {
      this.vodGenres = ''
    }
  }
    /**
     * return zone name of produce, else return ''
     */
  getProduceZoneName () {
    let name = this.details['produceZone'] ? this.details['produceZone'].name : ''
    return name
  }
    /**
     * return rating of produce, else return ''
     */
  getRatingName () {
    let name = this.details['rating'] && this.details['rating'].name ? this.details['rating'].name : ''
    return name
  }
    /**
     * data initialization
     */
  initContent () {
    if (this.details['ID']) {
      if (this.details['brotherSeasonVODs'] && this.details['brotherSeasonVODs'].length !== 0 && this.details['VODType'] === '3') {
        this.isShowSeason = true
        for (let i = 0; i < this.details['brotherSeasonVODs'].length; i++) {
            if (this.details['ID'] === this.details['brotherSeasonVODs'][i].VOD.ID) {
              this.seasonNumber = { num: this.prefixZero(this.details['brotherSeasonVODs'][i].sitcomNO, 2) }
            }
          }
      } else {
        this.isShowSeason = false
      }
      if (this.details['sitcomNO']) {
        this.isShowEpi = true
        this.epiNumber = { num: this.prefixZero(this.details['sitcomNO'], 2) }
      } else {
        this.isShowEpi = false
      }
      this.scoreTimes = this.getScoreTimes()
      this.initAverageScore()
      this.initContentTwice()
      this.definitions = []
      if (this.details['mediaFiles'] && this.details['mediaFiles'].length > 0) {
        _.each(this.details['mediaFiles'], item => {
            this.definitions.push(this.definitionFormat(item['definition']))
          })
        this.definitions = _.uniq(this.definitions, true)
      }
    }
  }
    /**
     * the times that this vod has been scored
     */
  getScoreTimes () {
    let times = +(this.details['scoreTimes'] && this.details['scoreTimes'] !== '' ? this.details['scoreTimes'] : '0')
    return times
  }
    /**
     * init the average score
     */
  initAverageScore () {
      // interface returns the average score and it's not empty
    if (this.details['averageScore'] && this.details['averageScore'] !== '') {
        // average score exists and is not 10
      if (this.details['averageScore'].substring(0, 2) !== '10') {
        let averageScore = this.details['averageScore']
          // interger number
        if (averageScore.length === 1) {
            averageScore += '.0'
          } else {
            averageScore = averageScore.substring(0, 4)
            averageScore = Number(averageScore)
            averageScore = averageScore.toFixed(1)
          }
        this.details['averageScore'] = averageScore
      } else {
          // average score is 10
        this.details['averageScore'] = '10'
      }
      this.averageScore = this.details['averageScore']
        // interface doesn't returns an average score
    } else {
      this.averageScore = '0.0'
    }
  }
    /**
     * function to add 'zero' in front of the target number
     * @param str target number
     * @param len the minimum length of str, if not enough, add 'zero'
     */
  prefixZero (str, len) {
      // change the data type from Number to String
    str = '' + (str || '')
    if (str.length >= len) {
      return str
    }
      // number of 'zero' that need to add
    let gapLen = len - str.length, i, arr = []
    for (i = 0; i < gapLen; i++) {
      arr[i] = '0'
    }
    return arr.join('') + str
  }
    /**
     * add or remove favorite
     */
  favoriteManger () {
    let self = this
      // guest login
    if (Cookies.getJSON('IS_GUEST_LOGIN')) {
      EventService.emit('CLOSED_LOGIN_DIALOG')
      EventService.on('MD_DIALOG_CONTAINER', function () {
        document.getElementsByTagName('md-dialog-container')[0]['style'].position = 'fixed'
      })
      this.commonService.openDialog(AccountLoginComponent)
      return
    }
      // the vod is added favorite, need to remove
    if (this.isFav === '1') {
      let req = {
        contentIDs: [self.details['ID']],
        contentTypes: ['VOD']
      }
        // call an interface
      self.commonService.removeFavorite(req).then(resp => {
        if (resp['result']['retCode'] === '000000000') {
            self.isFav = '0'
            this.addFavorite = 'true'
            document.getElementById('favoriteIcon').style.background = 'url(assets/img/20/ondemand_rio2-iocn4_20x20.png)'
            EventService.emit('REMOVEVODDETAIL_successful', {})
          }
      })
        // the vod isn't added favorite, need to add
    } else {
      let req = {
        favorites: [{
            contentID: self.details['ID'],
            contentType: 'VOD'
          }],
        autoCover: '0'
      }
        // call an interface
      self.commonService.addFavorite(req).then(resp => {
        if (resp['result']['retCode'] === '000000000') {
            self.isFav = '1'
            this.addFavorite = 'false'
            document.getElementById('favoriteIcon').style.background = 'url(assets/img/20/ondemand_rio2-iocn_2.png)'
            EventService.emit('ADDVODDETAIL_successful', {})
          }
      })
    }
  }
    /**
     * get time of year
     * @param timeData detail time
     */
  timeFormat (timeData) {
    let times = timeData || ''
    let year = times.substring(0, 4)
    return year
  }
    /**
     * choose different articulation
     * @param definition help to choose articulation
     */
  definitionFormat (definition) {
    switch (definition) {
      case '0':
        return 'SD'
      case '1':
        return 'HD'
      case '2':
        return '4K'
      default:
        return ''
    }
  }
    /**
     * show the app-favorite-vod drop down list
     */
  mouseenterFav () {
    this.isShow = 'true'
  }
    /**
     * hide the app-favorite-vod drop down list after 300ms
     */
  mouseleaveFav () {
    clearTimeout(this.leaveFav)
    this.leaveFav = setTimeout(() => {
      this.isShow = 'false'
    }, 300)
  }
    /**
     * diaplay the director information frame
     */
  mouseenterDirector () {
    if (document.querySelector('.directors').scrollWidth >= 200) {
      this.isDirectorShow = true
    }
  }
    /**
     * hide the director information frame
     */
  mouseleaveDirector () {
    this.isDirectorShow = false
  }
    /**
     * display the actor information frame
     */
  mouseenterActor () {
    let actorWidth = document.querySelector('.actors').scrollWidth
    let clientW: number = document.body.clientWidth
    if (clientW > 1440 && actorWidth >= 800) {
      this.isActorShow = true
    }
    if (clientW <= 1440 && actorWidth >= 600) {
      this.isActorShow = true
    }
  }
    /**
     * hide the actor information frame
     */
  mouseleaveActor () {
    this.isActorShow = false
  }
    /**
     * score of film
     */
  scoreClick (event) {
    let self = this
      // is guest login
    if (Cookies.getJSON('IS_GUEST_LOGIN')) {
      EventService.emit('CLOSED_LOGIN_DIALOG')
      EventService.on('MD_DIALOG_CONTAINER', function () {
        document.getElementsByTagName('md-dialog-container')[0]['style'].position = 'fixed'
      })
      this.commonService.openDialog(AccountLoginComponent)
      return
    }
    _.delay(function () {
      if (!_.isUndefined(session.get('scoreValue'))) {
        self.score = 'true'
      } else {
        self.score = 'false'
      }
    }, 0)
  }
    /**
     * diaplay the score suspension frame
     */
  mouseenterScore () {
    this.scoreShow = 'true'
    this.isScoreShow = true
    EventService.emit('IS_SHOWSCORE', false)
  }
    /**
     * hide the score suspension frame
     */
  mouseleaveScore () {
    let self = this
    EventService.on('SHOW_SCORE', function (boo) {
      self.eventflagScore = true
      if (boo) {
        self.eventflagScore = true
        self.isScoreShow = true
        self.scoreShow = 'true'
      } else if (!boo) {
          self.eventflagScore = false
          clearTimeout(self.leaveScore)
          self.leaveScore = setTimeout(() => {
            self.isScoreShow = false
          }, 300)
        }
    })
    clearTimeout(self.leaveScore)
    self.leaveScore = setTimeout(() => {
      if (!self.eventflagScore) {
        self.isScoreShow = false
        this.scoreShow = 'false'
      }
    }, 300)
  }
    /**
     * diaplay the share suspension frame
     */
  mouseenterShare () {
    this.isShowShare = true
  }
    /**
     * hide the share suspension frame
     */
  mouseleaveShare () {
    let self = this
    EventService.on('ISSHOW_SHARE', function (boo) {
      self.eventflag = true
      if (boo) {
        self.eventflag = true
        self.isShowShare = true
      } else if (!boo) {
          self.eventflag = false
          clearTimeout(self.leaveShare)
          self.leaveShare = setTimeout(() => {
            self.isShowShare = false
          }, 300)
        }
    })
    clearTimeout(self.leaveShare)
    self.leaveShare = setTimeout(() => {
      if (!self.eventflag) {
        self.isShowShare = false
      }
    }, 300)
  }
    /**
     * show the extra introduce
     */
  increaseMore () {
    this.showMore = true
    this.showMoreHeight()
  }
    /**
     * hide the extra introduce
     */
  discreaseLess () {
    this.showMore = false
    this.showMoreHeight()
  }
    /**
     * set style of the target element
     */
  showWidth () {
      // the browser is not firefox
    if (!this.isFirefox) {
      let styles = document.querySelector('.introduce')['style']['-webkit-box-direction']
      if (styles === 'reverse') {
        document.querySelector('.introduce')['style']['-webkit-box-direction'] = 'normal'
      } else {
        document.querySelector('.introduce')['style']['-webkit-box-direction'] = 'reverse'
      }
    }
  }
    /**
     * hide the WeiXin share eject frame
     */
  hideWeiXin () {
    let self = this
    setTimeout(() => {
      self.isweixin = false
      EventService.emit('miss_close', false)
    }, 1)
  }
}
