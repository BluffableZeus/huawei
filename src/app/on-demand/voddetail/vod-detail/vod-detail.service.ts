import { Injectable } from '@angular/core'
import { createFavorite, deleteFavorite } from 'ng-epg-sdk/vsp'

@Injectable()
export class VodDetailService {
  constructor () { }
  /**
     * call a interface
     */
  addFavorite (req: any) {
    return createFavorite(req).then((resp) => {
      // add successfull
      return resp
    })
  }
  /**
     * call a interface
     */
  removeFavorite (req: any) {
    return deleteFavorite(req).then((resp) => {
      // remove successfull
      return resp
    })
  }
}
