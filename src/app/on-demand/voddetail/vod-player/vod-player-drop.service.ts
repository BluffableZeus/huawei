import * as _ from 'underscore'
import { Injectable } from '@angular/core'
import { EventService } from 'ng-epg-sdk/services'
import { session } from 'src/app/shared/services/session'
import { MediaPlayService } from '../../../component/mediaPlay/mediaPlay.service'
// width of miniscreen which can drag
const BOX_WIDTH = 462
// height of miniscreen which can drag
const BOX_HEIGHT = 260

const TWO = 2, THREE = 3

@Injectable()
export class VODPlayerDrop {
    /**
     * declare variables
     */
    // dragging Dialog
  public draggingObj = [null, null, null, null, null, null, null, null]
    // check whether user can drag the video window
  public isCanDrag = false
    // save the drag position
  public dragNowPosition = {}
  public isEnterFullscreen = {}
  public isDragging = false
  public paramXRight
  public paramXBottom
    /**
     * name for modules that would be used in this module
     */
  constructor (
        private mediaPlayService: MediaPlayService) {
    let self = this
    this.dragStart()
    EventService.removeAllListeners(['DRAG_OVER_START'])
      /**
         * monitor DRAG_OVER_START event
         */
    EventService.on('DRAG_OVER_START', function () {
      self.dragNowPosition = {}
      self.dragStart()
    })
    EventService.removeAllListeners(['IS_FIRSTLOGIN_CAN_DROP_EVENT'])
      /**
         * monitor IS_FIRSTLOGIN_CAN_DROP_EVENT event
         */
    EventService.on('IS_FIRSTLOGIN_CAN_DROP_EVENT', function () {
      self.draggingObjSet()
      self.draggingObj[0] = document.querySelector('#vodMiniScreen')
      if (!_.isUndefined(self.dragNowPosition['paramX']) && !_.isUndefined(self.dragNowPosition['paramY'])) {
        self.movePosition(self.dragNowPosition['paramX'], self.dragNowPosition['paramY'])
      }
      self.dragStart()
    })
    EventService.removeAllListeners(['IS_NOT_CAN_DRAG'])
      /**
         * monitor IS_NOT_CAN_DRAG event
         */
    EventService.on('IS_NOT_CAN_DRAG', function () {
      self.isCanDrag = false
    })
    EventService.removeAllListeners(['IS_CAN_DRAG'])
      /**
         * monitor IS_CAN_DRAG event
         */
    EventService.on('IS_CAN_DRAG', function () {
      self.isCanDrag = true
      self.draggingObjSet()
      if (!_.isUndefined(self.dragNowPosition['paramX']) && !_.isUndefined(self.dragNowPosition['paramY'])) {
        self.movePosition(self.dragNowPosition['paramX'], self.dragNowPosition['paramY'])
      }
    })
    EventService.removeAllListeners(['DRAG_OVER'])
      /**
         * monitor DRAG_OVER event
         */
    EventService.on('DRAG_OVER', function () {
      self.dragStart(true)
    })
    EventService.removeAllListeners(['VOD_SCREEN_SIZE'])
      /**
         * monitor VOD_SCREEN_SIZE event
         */
    EventService.on('VOD_SCREEN_SIZE', function () {
        // use the packaged function in mediaPlayService
      let MediaplayerVod = self.mediaPlayService.isFullScreen()
      if (MediaplayerVod) {
        self.isEnterFullscreen['clientX'] = self.dragNowPosition['paramX']
        self.isEnterFullscreen['clientY'] = self.dragNowPosition['paramY']
        if (_.isUndefined(self.dragNowPosition['paramX']) && _.isUndefined(self.dragNowPosition['paramY'])) {
            self.isEnterFullscreen['clientX'] = document.querySelector('#vodVideoContainer video')['offsetLeft']
            self.isEnterFullscreen['clientY'] = document.querySelector('#vodVideoContainer video')['offsetTop']
          }
        return
      }
      if (self.isCanDrag) {
        self.dragWindowNone()
        let event = {}
        self.draggingObjSet()
        if (!_.isUndefined(self.isEnterFullscreen['clientX'])) {
            event['clientX'] = self.isEnterFullscreen['clientX']
            event['clientY'] = self.isEnterFullscreen['clientY']
            self.isEnterFullscreen['clientX'] = undefined
            self.isEnterFullscreen['clientY'] = undefined
          } else {
            event['clientX'] = document.querySelector('#vodVideoContainer video')['offsetLeft']
            event['clientY'] = document.querySelector('#vodVideoContainer video')['offsetTop']
          }
        self.isDragging = true
        self.isOverScreenSize(event, 0, 0)
      }
    })
  }
    /**
     * self.dragNowPosition['paramX']  switch screen need change self.dragNowPosition['paramX'] when float screen display
     */
  dragWindowNone () {
    let self = this
      // choose the bigger one as scrollTop
    let scrollTop = Math.max(document.body.scrollTop, document.documentElement.scrollTop)
    let clientW: number = window.innerWidth
      // big screen
    if (clientW > 1440 && clientW <= 1920) {
      if (scrollTop < 670) {
          // set current drag position
        self.dragNowPosition['paramX'] = document.body.clientWidth - self.paramXRight - BOX_WIDTH
        self.dragNowPosition['paramY'] = document.body.clientHeight - self.paramXBottom - BOX_HEIGHT
      }
    } else {
        // small screen
      if (scrollTop < 450) {
        self.dragNowPosition['paramX'] = (document.body.clientWidth - self.paramXRight - BOX_WIDTH) > 0
            ? document.body.clientWidth - self.paramXRight - BOX_WIDTH : 0
        self.dragNowPosition['paramY'] = (document.body.clientHeight - self.paramXBottom - BOX_HEIGHT) > 0
            ? document.body.clientHeight - self.paramXBottom - BOX_HEIGHT : 80
      }
    }
  }
    /**
     * move over screen size
     * @param e target element
     * @param diffX distance on x-axis
     * @param diffY distance on y-axis
     */
  isOverScreenSize (e, diffX, diffY) {
    if ((diffX === 0 && diffY === 0) && this.isDragging === false) {
      return
    }
    if (this.draggingObj[1] || this.draggingObj[0] && !_.isUndefined(diffX) && !_.isUndefined(diffY)) {
      session.put('VOD_PLAYBAR_DROP_MOUSEMOVE', true)
      this.movePosition(e.clientX - diffX, e.clientY - diffY)
    }
  }
    /**
     * set position of the drag object
     * @param e target element
     */
  diffXOrYstart (e) {
    let self = this
    let diffX
    let diffY
    if (self.draggingObj[1] !== null) {
      diffX = e.clientX - self.draggingObj[1].offsetLeft
      diffY = e.clientY - self.draggingObj[1].offsetTop
    } else if (self.draggingObj[0] !== null) {
      diffX = e.clientX - self.draggingObj[0].offsetLeft
      diffY = e.clientY - self.draggingObj[0].offsetTop
    }
    return { 'diffX': diffX, 'diffY': diffY }
  }

  dragStart (isStart?: any) {
    let self = this
      // if params is click areas and params is move areas, return move elements, or return null
    let dragging = function (validateHandler?: any) {
      let diffX
      let diffY
      function mouseHandler (e) {
        switch (e.type) {
            case 'mousedown':
              if (!self.draggingObj[0] && !self.draggingObj[1]) {
                return
              }
              if (self.isLeftBeyond(e)) {
                return
              }
              if (self.isTopBeyond(e)) {
                return
              }
              if (!self.isCanDrag) {
                return
              }
              if (document.querySelector('app-account-login') || document.querySelector('app-vod-detail .weixin')) {
                return
              }
              document.removeEventListener('mousedown', mouseHandler)

              self.draggingObjSet()
              disable()
              let diffXOrY = self.diffXOrYstart(e)
              diffX = diffXOrY['diffX']
              diffY = diffXOrY['diffY']
              break
            case 'mousemove':
              if (!_.isUndefined(diffX) && !_.isUndefined(diffY)) {
                self.isDragging = true
                self.isOverScreenSize(e, diffX, diffY)
              }
              break
            case 'mouseup':
              diffX = undefined
              diffY = undefined
              break
            case 'mouseleave':
              break
            default:
              break
          }
      }

      function disable () {
        for (let i = 0; i < 8; i++) {
            if (self.draggingObj[i]) {
              self.draggingObj[i].removeEventListener('mousedown', mouseHandler)
              self.draggingObj[i].removeEventListener('mouseup', mouseHandler)
              self.draggingObj[i].addEventListener('mousedown', mouseHandler)
              self.draggingObj[i].addEventListener('mouseup', mouseHandler)
            }
          }
      }

      return {
        enable: function () {
            document.addEventListener('mousedown', mouseHandler)
            document.addEventListener('mousemove', mouseHandler)
            document.addEventListener('mouseup', mouseHandler)
            document.addEventListener('mouseleave', mouseHandler)
          },
        disable: function () {
            for (let i = 0; i < 8; i++) {
              if (self.draggingObj[i]) {
                self.draggingObj[i].removeEventListener('mousedown', mouseHandler)
                self.draggingObj[i].removeEventListener('mouseup', mouseHandler)
              }
            }
          }
      }
    }

    function getDraggingDialog () {
      if (document.querySelector('#vodMiniScreen')['style']['display'] === 'block') {
        return true
      } else if (document.querySelector('#vodMiniScreen')['style']['display'] === 'none') {
          return false
        }
    }
    if (isStart) {
      dragging(getDraggingDialog).disable()
    } else {
      dragging(getDraggingDialog).enable()
    }
  }
    /**
     * initialize float screen, create float screen element
     */
  draggingObjSet () {
    this.draggingObj[0] = document.querySelector('#vodMiniScreen')
    this.draggingObj[1] = document.querySelector('#vodVideoContainer video')
    this.draggingObj[2] = document.getElementsByClassName('vodPlayBar-close')[0]
    this.draggingObj[3] = document.getElementsByClassName('btn-play-pause-drop')[0]
    this.draggingObj[4] = document.getElementsByClassName('miniScreens')[0]
    this.draggingObj[5] = document.getElementsByClassName('load_bg_sus')[0]
    this.draggingObj[6] = document.querySelector('#vodReplayBg')
    this.draggingObj[7] = document.querySelector('#vodVideoContainer')
  }

  isLeftBeyond (e) {
    if (e.clientX < this.draggingObj[1].offsetLeft ||
            e.clientX > (this.draggingObj[1].offsetLeft + BOX_WIDTH)) {
      return true
    } else {
      return false
    }
  }

  isTopBeyond (e) {
    if (e.clientY < this.draggingObj[1].offsetTop ||
            e.clientY > (this.draggingObj[1].offsetTop + BOX_HEIGHT)) {
      return true
    } else {
      return false
    }
  }
    /**
     * move screen object
     * @param paramX the x-axis position
     * @param paramY the y-axis position
     */
  movePosition (paramX: number, paramY: number) {
    let self = this
    if (paramX === 0 || paramY === 0) {
      return
    }
    let isFullScreen = self.mediaPlayService.isFullScreen()
    if (isFullScreen) {
      return
    }
    let NaviHeight = 80
    if (window.innerWidth <= 1100) {
      NaviHeight = 0
    } else {
      NaviHeight = 80
    }
    self.dragNowPosition['paramX'] = paramX
    self.dragNowPosition['paramY'] = paramY
    let rightValue = document.body.clientWidth - paramX - BOX_WIDTH
    let bottomValue = document.body.clientHeight - paramY - BOX_HEIGHT
    rightValue = rightValue > 0 ? rightValue : 0
    bottomValue = bottomValue > 0 ? bottomValue : 0
    if (rightValue > document.body.clientWidth - BOX_WIDTH) {
      rightValue = document.body.clientWidth - BOX_WIDTH
    }
    if (bottomValue > document.body.clientHeight - BOX_HEIGHT - NaviHeight) {
      bottomValue = document.body.clientHeight - BOX_HEIGHT - NaviHeight
    }
    self.paramXRight = rightValue
    self.paramXBottom = bottomValue
    for (let i = 0; i < 8; i++) {
      if (self.draggingObj[i]) {
        self.draggingObj[i].style.right = rightValue + 'px'
        self.draggingObj[i].style.bottom = bottomValue + 'px'
        if (i === TWO) {
            self.draggingObj[i].style.right = (rightValue + 1) + 'px'
            self.draggingObj[i].style.bottom = (bottomValue + 235) + 'px'
          }
        if (i === THREE) {
            self.draggingObj[i].style.right = (rightValue + BOX_WIDTH) + 'px'
            self.draggingObj[i].style.bottom = (bottomValue + 42) + 'px'
          }
      }
    }
  }
    /**
     * init the param isDragging
     */
  initIsDrag () {
    this.isDragging = false
  }
}
