import * as _ from 'underscore'
import { Component, Input, ViewChild, OnInit, OnDestroy, DoCheck } from '@angular/core'
import { VodPlayBarComponent } from '../../../component/vodPlayBar/vodPlayBar.component'
import { PlayVodService } from './vod-player.service'
import { CommonService } from '../../../core/common.service'
import { session } from 'src/app/shared/services/session'
import { EventService } from 'ng-epg-sdk/services'
import { VodDetailHomeService } from '../vodDetail.service'
import { MediaPlayService } from '../../../component/mediaPlay/mediaPlay.service'
import { VODPlayerDrop } from './vod-player-drop.service'
import { TrackService } from '../../../shared/services/track.service'
import { DateUtils } from 'ng-epg-sdk/utils'
import { ActivatedRoute } from '@angular/router'

const SPACE_KEY = 32

@Component({
  selector: 'app-vod-player',
  styleUrls: ['./vod-player.component.scss'],
  templateUrl: './vod-player.component.html'
})

export class VodPlayerComponent implements OnInit, DoCheck, OnDestroy {
  public ind: number
  public clipIndex: number
    /**
     * vod details
     */
  public playVodInfo = {}
  public reportIntervalId
  public player
  public scrollTop = 0
  public nowEpiInfo = {}
  public leaveTime
  public isFullScreen = false
  public issuspendClose = true
  public issuspend = false
  public paused = 'url(assets/img/36/ondemand_suspension-box_play_36.png)'
  public leaveTimeTra
  public playEnd = false
  public resetOnseek = false
  private firstInit = true
  private hideBar = null
  private secondShow
  private dbClickTime = null
  private showDragSubsup = true
  private trailerEnd = false
  private sitcomNO = ''
  public isLoading = true
  public isChrome = true
  public FirstDragPositonX = 0
  public subtitleContainerTimer
  public display = 'none'
  private isFireFox = false

  @Input() episodes = []
  @ViewChild(VodPlayBarComponent) vodPlayBar: VodPlayBarComponent

  constructor (
        private playVodService: PlayVodService,
        private commonService: CommonService,
        private voddetail: VodDetailHomeService,
        private mediaPlayService: MediaPlayService,
        private vodPlayerDrop: VODPlayerDrop,
        private trackService: TrackService,
        public route: ActivatedRoute
    ) {
    let ua = navigator.userAgent.toLowerCase()
    this.isChrome = ua.indexOf('chrome') >= 0
    this.isFireFox = !!ua.match(/firefox\/([\d.]+)/)
      // this.differ = this.differs.find([]).create(null);
  }

  @Input() set vodPlayData (details) {
    this.playVodInfo = details
    let isProfile = Cookies.getJSON('IS_PROFILE_LOGIN')
    if (isProfile === true) {
      this.beforePlay()
    }
  }

  ngOnInit () {
    this.route.params.subscribe(params => {
      this.initPlayState()
      this.resetElementStyle()
      this.vodPlayerDrop.isCanDrag = false
      this.vodPlayerDrop.dragNowPosition = {}
      this.vodPlayerDrop.isEnterFullscreen = {}
      this.vodPlayerDrop.draggingObj = [null, null, null, null, null, null, null, null]
      this.vodPlayerDrop.paramXRight = undefined
      this.vodPlayerDrop.paramXBottom = undefined
    })
    EventService.on('FIRST_TIME_LOGIN', (event) => {
      session.put('IS_FIRSTLOGIN_CAN_DROP', true)
      this.initPlayState()
    })
    EventService.removeAllListeners(['GETPLAYER'])
    EventService.on('GETPLAYER', time => {
      this.leaveTime = time
    })
      // float screen response keyboard event
    EventService.removeAllListeners(['MINI_SCREEN_CLOSED_FULLSCREEN_VODVIDEO'])
    EventService.on('MINI_SCREEN_CLOSED_FULLSCREEN_VODVIDEO', keycode => {
      this.LivePlayerKeyUp(keycode)
    })
    EventService.removeAllListeners(['LOAD_VODPLAY_VIDEO'])
    EventService.on('LOAD_VODPLAY_VIDEO', vodVideoData => {
      this.doLoad(vodVideoData)
    })
      // the button for click next episode
    EventService.removeAllListeners(['playNextVod'])
    EventService.on('playNextVod', () => {
      let clipFlag = session.get('clipFlag')
      if (clipFlag) {
        let clips = this.playVodInfo['clipfiles']
        if (this.clipIndex === clips.length - 2) {
            EventService.emit('NEXT_ICON_DISAPPEAR', '1')
          }
        this.clipIndex = this.clipIndex + 1
        if (this.clipIndex < clips.length) {
            EventService.emit('NEXTCLIP', this.clipIndex)
            let traData = clips[this.clipIndex]
            this.playVodService.playVod(this.playVodInfo, this.playVodInfo['ID'], traData['ID'], '', true)
          } else {
            EventService.emit('END')
          }
      } else {
        this.ind = this.ind + 1
        this.nowEpiInfo = this.getEpiInfoByIndex(this.playVodInfo, this.ind)
        let ess = this.playVodInfo['episodes']
        if (this.ind < ess.length) {
            EventService.emit('NEXT', this.ind)
            if (this.ind === ess.length - 1) {
              EventService.emit('NEXT_ICON_DISAPPEAR', '1')
            }
            let esData = ess[this.ind]
            if (!_.isUndefined(esData['VOD']['mediaFiles'][0])) {
              this.playVodService.playVod(this.playVodInfo, esData['VOD']['ID'],
                esData['VOD']['mediaFiles'][0]['ID'] || '', this.playVodInfo['ID'])
            } else {
              this.playVodService.playVod(this.playVodInfo, esData['VOD']['ID'], '', this.playVodInfo['ID'])
            }
          } else {
            EventService.emit('END')
          }
      }
    })
    EventService.removeAllListeners(['SCROLL_SHOW_SUSPEND'])
    EventService.on('SCROLL_SHOW_SUSPEND', () => {
      this.viewVodVideo()
    })
    EventService.removeAllListeners(['GET_CURRENT_FULLSCREEN'])
    EventService.on('GET_CURRENT_FULLSCREEN', () => {
      this.isFullScreen = true
      this.viewVodVideo()
    })
    EventService.removeAllListeners(['GET_EXIT_CURRENT_FULLSCREEN'])
    EventService.on('GET_EXIT_CURRENT_FULLSCREEN', () => {
      if (!this.isFireFox) {
        this.isFullScreen = false
        this.viewVodVideo()
      } else {
        _.delay(() => {
            this.showDragSubsup = true
            this.isFullScreen = false
            this.viewVodVideo()
          }, 100)
      }
    })
    EventService.removeAllListeners(['RESET_PLAY_VOD'])
    EventService.on('RESET_PLAY_VOD', () => {
      this.playEnd = false
      this.resetOnseek = false
      this.judgePaused()
    })
    EventService.removeAllListeners(['RESET_ONSEEK_PLAY_VOD'])
    EventService.on('RESET_ONSEEK_PLAY_VOD', () => {
      this.resetOnseek = false
      this.judgePaused()
    })
    EventService.removeAllListeners(['CLICK_DIALOG_SCREEN'])
    EventService.on('CLICK_DIALOG_SCREEN', () => {
      this.judgePaused()
    })
    EventService.removeAllListeners(['SHOW_PLAYER_DROP'])
    EventService.on('SHOW_PLAYER_DROP', request => {
      this.secondShow = request
      this.showVODPlayOtherCom()
    })
    EventService.removeAllListeners(['HIDE_PLAYER_DROP'])
    EventService.on('HIDE_PLAYER_DROP', () => {
      this.hideVODPlayOtherCom()
    })
    EventService.removeAllListeners(['changeState'])
    EventService.on('changeState', () => {
      this.isLoading = true
    })
    EventService.removeAllListeners(['REPLAYVOD'])
    EventService.on('REPLAYVOD', () => {
      this.vodPlayBar.smallVod.playEnd = false
      this.vodPlayBar.smallVod.isShowOnLoad = true
      session.put('REPLAYVOD', true)
      this.beforePlay(true)
    })
    EventService.removeAllListeners(['VOD_VIDEO_START'])
    EventService.on('VOD_VIDEO_START', () => {
      this.isLoading = false
      this.judgePaused()
    })
    EventService.removeAllListeners(['playTra'])
    EventService.on('playTra', data => {
      let playInfo = session.get('playInfo')
      this.playTra(playInfo, data)
    })
    EventService.on('PLAYFILM', () => {
      session.remove('clipFlag')
      let playInfo = session.get('playInfo')
      this.playFilm(playInfo)
    })
    EventService.removeAllListeners(['VOD_FLOW_END'])
    EventService.on('VOD_FLOW_END', duration => {
      this.addBookmark(duration)
      if (!session.get('clipFlag')) {
        this.playVodService.reportVOD('1')
      }
      this.playEnd = true
      this.resetOnseek = false
      this.judgePaused()
      this.reportPlayEnd()
      let clipFlag = session.get('clipFlag')
      if (clipFlag || this.trailerEnd === true) {
        this.clipIndex = this.clipIndex + 1
        let clips = this.playVodInfo['clipfiles']
        if (this.clipIndex < clips.length) {
            EventService.emit('NEXTCLIP', this.clipIndex)
            let traData = clips[this.clipIndex]
            this.playVodService.playVod(this.playVodInfo, this.playVodInfo['ID'], traData['ID'], '', true)
          } else {
            document.getElementsByClassName('play-btn')[0]['style']['visibility'] = 'hidden'
            EventService.emit('END')
            EventService.emit('TRAILER_PLAY_END')
            this.clipIndex = clips.length - 1
            this.trailerEnd = true
          }
      } else {
        if (parseInt(this.playVodInfo['VODType'], 10) === 0 && !clipFlag) {
            document.getElementsByClassName('play-btn')[0]['style']['visibility'] = 'hidden'
            EventService.emit('END')
          } else {
            if (this.ind === this.playVodInfo['episodes'].length - 2) {
              EventService.emit('NEXT_ICON_DISAPPEAR', '1')
            }
            this.ind = this.ind + 1
            if (this.ind !== this.playVodInfo['episodes'].length) {
              this.nowEpiInfo = this.getEpiInfoByIndex(this.playVodInfo, this.ind)
            }
            let ess = this.playVodInfo['episodes']
            if (this.ind < ess.length) {
              let esData = ess[this.ind]
              if (!_.isUndefined(esData['VOD']['mediaFiles'][0])) {
                this.playVodService.playVod(this.playVodInfo, esData['VOD']['ID'],
                  esData['VOD']['mediaFiles'][0]['ID'] || '', this.playVodInfo['ID'])
              } else {
                this.playVodService.playVod(this.playVodInfo, esData['VOD']['ID'], '', this.playVodInfo['ID'])
              }
              EventService.emit('NEXT', this.ind)
            } else {
              document.getElementsByClassName('play-btn')[0]['style']['visibility'] = 'hidden'
              EventService.emit('END')
            }
          }
      }
    })
  }

  ngDoCheck (): void {
    if (this.vodPlayBar && this.vodPlayBar.smallVod && this.vodPlayBar.smallVod.isShowOnLoad && this.vodPlayerDrop.isCanDrag) {
      this.isLoading = true
      this.display = 'block'
    } else if (this.isLoading) {
      this.isLoading = false
      this.display = 'none'
    }
  }

    /**
     * reset the style of elements
     */
  resetElementStyle () {
    if (document.getElementById('suspend-close') && document.getElementById('suspend-close')['style']) {
      this.setRightAndBottom(document.getElementById('suspend-close')['style'])
    }
    if (document.getElementById('suspend-play') && document.getElementById('suspend-play')['style']) {
      this.setRightAndBottom(document.getElementById('suspend-play')['style'])
    }
    if (document.getElementById('suspend-miniScreen') && document.getElementById('suspend-miniScreen')['style']) {
      this.setRightAndBottom(document.getElementById('suspend-miniScreen')['style'])
    }
    if (document.getElementById('suspend-load') && document.getElementById('suspend-load')['style']) {
      this.setRightAndBottom(document.getElementById('suspend-load')['style'])
    }
  }

    /**
     * reset the right and bottom margins of the element
     */
  setRightAndBottom (style) {
    style.right = ''
    style.bottom = ''
  }

  initPlayState () {
    session.remove('clipFlag')
    this.trailerEnd = false
    this.dealViewVodVideo(700)
    this.vodPlayerDrop.initIsDrag()
    if (this.firstInit) {
      this.beforePlay()
    }
    if (!session.get('IS_FIRSTLOGIN_CAN_DROP')) {
      EventService.emit('DRAG_OVER_START')
    }
  }

  episodeBeforePlay (details, replayFlag) {
    let ess = details['episodes']
    let esData
    if (!_.isUndefined(replayFlag) && replayFlag === true) {
      esData = ess[ess.length - 1]
    } else {
      if (!_.isUndefined(details['bookmark']) && !_.isUndefined(details['bookmark']['subContentID'])) {
          // have bookmark
        let esID = details['bookmark']['subContentID']
        esData = _.find(ess, item => {
            return item['VOD']['ID'] === esID
          })
      } else {
        esData = ess[0]
      }
    }
    this.ind = esData['epiIndex']
    this.sitcomNO = esData['sitcomNO']
    this.getCurrEpiInfo(this.sitcomNO)
    let esDataID = this.getEsDataID(esData)
    let esDataMediaFiles = esData && esData['VOD'] && esData['VOD']['mediaFiles']

    if (esDataMediaFiles && !_.isUndefined(esDataMediaFiles[0])) {
      this.playVodService.playVod(details, esDataID, esDataMediaFiles[0]['ID'] || '', details['ID'])
    } else {
      this.playVodService.playVod(details, esDataID, '', details['ID'])
    }
  }

    /**
     * get id of episode
     */
  getEsDataID (esData) {
    return esData && esData['VOD'] && esData['VOD']['ID']
  }

  beforePlay (replayFlag?: boolean) {
    let details = this.playVodInfo
    this.firstInit = false
    if (details) {
      let clipFlag = session.get('clipFlag')
      if (clipFlag && !_.isUndefined(replayFlag)) {
        let length = details['clipfiles'].length
        let traData = details['clipfiles'][length - 1]
        this.playVodService.playVod(details, details['ID'], traData['ID'], '', true)
      } else {
        if (parseInt(details['VODType'], 10) === 0) {
            if (details['mediaFiles'] && details['mediaFiles'][0]) {
              this.playVodService.playVod(details, details['ID'], details['mediaFiles'][0]['ID'])
            } else {
              this.playVodService.playVod(details, details['ID'], '')
            }
          } else {
            if (details['episodes'].length > 0) {
              this.episodeBeforePlay(details, replayFlag)
            } else {
              if (details['mediaFiles'] && details['mediaFiles'][0]) {
                this.playVodService.playVod(details, details['ID'], details['mediaFiles'][0]['ID'])
              } else {
                this.playVodService.playVod(details, details['ID'], '')
              }
            }
          }
      }
    }
  }

    /**
     * get information of current episode
     */
  getCurrEpiInfo (sitcomNO) {
    let vodDetails = this.playVodInfo
    this.nowEpiInfo = _.find(vodDetails['episodes'], item => {
      return item['sitcomNO'] === sitcomNO
    }) || {}
  }

    /**
     * get episode information by index
     */
  getEpiInfoByIndex (vodDetails, index) {
    let epiInfo = _.find(vodDetails['episodes'], item => {
      return item['epiIndex'] === index
    }) || {}
    return epiInfo
  }

  doLoad (vodVideoData) {
    this.vodPlayBar.doLoad(vodVideoData)
    if (location.pathname.indexOf('video') !== -1 || location.pathname.indexOf('movie') !== -1) {
      this.viewVodVideo()
    }
  }

    /**
     * get subject id
     */
  getsubjectid (subjectid) {
    this.playVodService.getsubjectID(subjectid)
  }

    /**
     * the three button appear when mouse enter the float screen
     */
  showVODPlayOtherCom () {
    if (this.issuspend) {
      this.judgePaused()
      document.getElementsByClassName('vodPlayBar-close')[0]['style']['visibility'] = 'visible'
      if (this.secondShow === 'SECOND_SHOW' && !this.isLoading) {
        document.getElementsByClassName('play-btn')[0]['style']['visibility'] = 'hidden'
      } else {
        document.getElementsByClassName('play-btn')[0]['style']['visibility'] = 'visible'
      }
      if (document.getElementsByClassName('btn-play-pause-drop')[0]) {
        document.getElementsByClassName('btn-play-pause-drop')[0]['style']['visibility'] = 'visible'
      }
    }
  }

    /**
     * the three button hide when mouse move out of the float screen
     */
  hideVODPlayOtherCom (e?) {
    if (this.isChrome && e && !e['toElement']) {
      return
    }
    if (document.getElementsByClassName('btn-play-pause-drop')[0]) {
      document.getElementsByClassName('btn-play-pause-drop')[0]['style']['visibility'] = 'hidden'
    }
    document.getElementsByClassName('vodPlayBar-close')[0]['style']['visibility'] = 'hidden'
    document.getElementsByClassName('play-btn')[0]['style']['visibility'] = 'hidden'
  }

  showMinOtherCom ($event) {
    let self = this
    if ($event && Math.abs($event.movementX * $event.movementX) + Math.abs($event.movementY * $event.movementY) > 9) {
      if (document.getElementById('vod-player')) {
        document.getElementById('vod-player')['style']['cursor'] = 'default'
      }
    } else if (!$event) {
      if (document.getElementById('vod-player')) {
          document.getElementById('vod-player')['style']['cursor'] = 'default'
        }
    }
    clearTimeout(this.hideBar)
    this.hideBar = setTimeout(() => {
      if (document.getElementById('vod-player')) {
        document.getElementById('vod-player')['style']['cursor'] = 'none'
      }
    }, 5000)
    EventService.emit('SHOW_PLAYER_DROP')
  }

  hideMinOtherCom () {
    EventService.emit('HIDE_PLAYER_DROP')
  }

    /**
     * play by click episodes or poster
     */
  playEpi (info) {
    this.trailerEnd = false
    let playInfo = info
    let vodDetail = this.playVodInfo
    let epiData = _.find(this.playVodInfo['episodes'], item => {
      return item['sitcomNO'] === playInfo['epi']
    })
    this.sitcomNO = playInfo['epi']
    this.ind = playInfo['epiIndex']
    this.getCurrEpiInfo(playInfo['epi'])
    if (!_.isUndefined(epiData['VOD']['mediaFiles'][0])) {
      this.playVodService.playVod(vodDetail, epiData['VOD']['ID'], epiData['VOD']['mediaFiles'][0]['ID'] || '', vodDetail['ID'])
    } else {
      this.playVodService.playVod(vodDetail, epiData['VOD']['ID'], '', vodDetail['ID'])
    }
  }

    /**
     * play trailers
     */
  playTra (info, data) {
    this.vodPlayBar.playTraHandler()
    this.trailerEnd = false
    let playInfo = info
    let vodDetail = data
    let traData = _.find(data['clipfiles'], item => {
      return item['ID'] === playInfo['tra']
    })
    this.clipIndex = playInfo['traIndex']
    this.playVodService.playVod(vodDetail, vodDetail['ID'], traData['ID'], '', true)
    if (parseInt(vodDetail['VODType'], 10) === 0) {
      EventService.emit('showPlayFilmButton')
    }
  }

  playFilm (info) {
    this.trailerEnd = false
    this.beforePlay()
  }

    /**
     * add bookmark and clean timer
     */
  ngOnDestroy () {
    if (Cookies.getJSON('IS_PROFILE_LOGIN')) {
      if (!session.get('clipFlag') && session.get('VOD_IS_PLAYING')) {
        if (parseInt(this.leaveTime, 10) !== 0) {
            let limitedFlag = session.get('CAN_NOT_PRESS_PLAY')
            if (limitedFlag !== true) {
              session.put('QUERY_HISTORY_DELAY', 400)
              this.addBookmark(this.leaveTime)
            }
          }
        clearTimeout(this.reportIntervalId)
        this.playVodService.reportVOD('1')
        if (!this.playEnd) {
            this.reportPlayEnd(true)
          }
      }
    }
    clearTimeout(this.hideBar)
    clearTimeout(this.dbClickTime)
    this.playVodService.ngOnDestroy()
  }

    /**
     * report the recommendable vod when it is end
     * @param isleavePage  the flag of jugding the use leaving the page
     */
  reportPlayEnd (isleavePage?) {
      // if the vod is recommendable  and current playing vod is firm
    let timestamp = session.get('tracker') && session.get('tracker')[0] && session.get('tracker')[0]['timestamp']
        ? session.get('tracker')[0]['timestamp'] : DateUtils.format(Date.now(), 'YYYYMMDDHHmmss')
    if (session.get('isRecommandVOD') && !session.get('clipFlag')) {
      const trackParams = {
        contentCode: this.playVodInfo['ID'],
        businessType: '1',
        recmActionType: '4',
        timestamp: timestamp
      }
      this.trackService.browseRecmBehavior(trackParams)
    }
      // when the use leave the page, clear the flag of recommendation
    if (isleavePage) {
      session.pop('isRecommandVOD')
    }
  }

    /**
     * add bookmark
     * @param {string} currentTime [bookmark message]
     */
  private addBookmark (currentTime) {
    if (session.get('clipFlag')) {
      return
    }
    let playVodInfo
    if (this.nowEpiInfo['VOD']) {
      playVodInfo = this.nowEpiInfo['VOD']
    } else {
      playVodInfo = this.playVodInfo
    }
    let time = parseInt(currentTime, 10)
    let bookmark = {
      bookmarkType: this['playType'],
      itemID: this.playVodInfo['ID'],
      rangeTime: time + ''
    }
    if (playVodInfo['mediaFiles'] && playVodInfo['mediaFiles'][0]) {
      if (this.nowEpiInfo['VOD']) {
        this.playVodService.addBookmark(playVodInfo, 'VOD', time + '', {}, this.playVodInfo['ID']).then(() => {
            EventService.emit('ADD_BOOKMARK_SUCCESS', bookmark)
          })
      } else {
        this.playVodService.addBookmark(playVodInfo, 'VOD', time + '', {}).then(() => {
            EventService.emit('ADD_BOOKMARK_SUCCESS', bookmark)
          })
      }
    }
  }

  private hideSuspend (video, elementStyles) {
    document.querySelector('#vodVideoContainer app-settingsuspension')['style']['visibility'] = 'visible'
    let subtitleContainer = document.getElementById('video-caption-vod')
    if (subtitleContainer) {
      clearTimeout(this.subtitleContainerTimer)
      subtitleContainer.style.display = 'none'
      this.subtitleContainerTimer = setTimeout(() => {
        subtitleContainer.style.display = 'block'
      }, 500)
    }
    this.issuspend = false
    for (let i = 0; i < video.length; i++) {
      video[i]['position'] = 'relative'
      video[i]['bottom'] = '0px'
      video[i]['right'] = '0px'
      video[i]['left'] = '0px'
      video[i]['top'] = '0px'
      video[i]['width'] = '100%'
      video[i]['height'] = '100%'
      if (i !== 3) {
        video[i]['border'] = 'none'
      }
    }
    video[0]['background'] = 'none'
    video[3]['zIndex'] = ''
    for (let i = 0; i < elementStyles.length; i++) {
      elementStyles[i]['top'] = '0px'
      elementStyles[i]['left'] = '0px'
    }
    document.getElementById('vodVideoContainer').classList.remove('mini-screen')
    let loadElement = document.getElementsByClassName('load_bg')[0]
    loadElement['style']['visibility'] = 'visible'
    document.getElementsByClassName('miniScreens')[0]['style']['visibility'] = 'hidden'
    document.getElementsByClassName('load_bg_sus')[0]['style']['display'] = 'none'
    EventService.emit('DRAG_OVER')
    EventService.emit('IS_NOT_CAN_DRAG')
  }

  private showSuspend (video, className: Array<any>, elementStyles) {
    return false
      /* document.querySelector('#vodVideoContainer app-settingsuspension')['style']['visibility'] = 'hidden';
        this.issuspend = true;
        for (let i = 0; i < video.length; i++) {
            video[i]['position'] = 'fixed';
            video[i]['width'] = '460px';
            video[i]['height'] = '258px';
            video[i]['bottom'] = '1px';
            video[i]['right'] = '131px';
            if (i !== 3) {
                video[i]['border'] = '1px solid white';
            }
            let ua = navigator.userAgent.toLowerCase();
            let isIe = !!ua.match(/msie/i) || !!ua.match(/rv:([\d.]+).*like gecko/);
            if (isIe) {
                video[i]['left'] = 'auto';
                video[i]['top'] = 'auto';
            } else {
                video[i]['left'] = 'initial';
                video[i]['top'] = 'initial';
            }
        }
        video[0]['background'] = 'rgba(0, 0, 0, 0.9)';
        video[3]['zIndex'] = '12';
        document.getElementById('vodVideoContainer').classList.add('mini-screen');

        for (let i = 0; i < elementStyles.length; i++) {
            elementStyles[i]['top'] = '-1px';
            elementStyles[i]['left'] = '-1px';
        }
        this.setLoadElementDisplay();
        for (let i = 0; i < className.length; i++) {
            if (className[i] === 'btn-play-pause-drop') {
                if (document.getElementsByClassName(className[i])[1]) {
                    document.getElementsByClassName(className[i])[1]['style']['left'] = 'inherit';
                    document.getElementsByClassName(className[i])[1]['style']['top'] = 'inherit';
                }
            } else {
                if (document.getElementsByClassName(className[i])[0]) {
                    document.getElementsByClassName(className[i])[0]['style']['left'] = 'inherit';
                    document.getElementsByClassName(className[i])[0]['style']['top'] = 'inherit';
                }
            }
        }
        if (this['playVodInfo']['contentType'] === 'AUDIO_VOD') {
            document.getElementsByClassName('miniScreens')[0]['style']['visibility'] = 'visible';
        }
        EventService.emit('IS_CAN_DRAG'); */
  }

  setLoadElementDisplay () {
    let loadElement = document.getElementsByClassName('load_bg')[0]
    loadElement['style']['visibility'] = 'hidden'
    let isPlayTrue = loadElement['style']['display']
    let minivideoSub = document.getElementById('vodMiniScreen')['style']['display']
    let vodLogInBg = document.getElementById('logInBgContent')['style']['display']
    let replayBg = document.getElementById('replayBgContent')['style']['display']
    let playFailBg = document.getElementById('playFailBg')['style']['display']
    if (isPlayTrue === 'block' && minivideoSub !== 'block' && vodLogInBg !== 'block' &&
            replayBg !== 'block' && playFailBg !== 'block') {
      document.getElementsByClassName('load_bg_sus')[0]['style']['display'] = 'block'
    } else {
      document.getElementsByClassName('load_bg_sus')[0]['style']['display'] = 'none'
    }
  }

  private viewVodVideo () {
    this.scrollTop = Math.max(document.body.scrollTop, document.documentElement.scrollTop)
    if (!document.querySelector('#vodVideoContainer video')) {
      return
    }
    if (document.body.scrollWidth > 1440) {
      this.dealViewVodVideo(700)
    } else {
      this.dealViewVodVideo(516)
    }
  }

  private dealViewVodVideo (scrollTopSize) {
    if (!document.querySelector('#vodVideoContainer video')) {
      return
    }

    let video = document.querySelector('#vodVideoContainer video')['style']
    let vodpopUpDialog = document.getElementById('vodPopUpDialog')['style']
    let minivideo = document.querySelector('#vodMiniScreen')['style']
    let vodReplayBg = document.getElementById('vodReplayBg')['style']
    let videoContainer = document.querySelector('#vodVideoContainer')['style']
    let audioBg = document.querySelector('#audio-bg') && document.querySelector('#audio-bg')['style']
    if (this.scrollTop > scrollTopSize) {
      if (this.issuspendClose) {
        this.issuspendClose = false
        if (this.showDragSubsup) {
            EventService.emit('SHOW_PLAYER_LEFT_RIGHT')
            if (document.getElementById('smallScreenVodControl') && document.getElementById('smallScreenVodControl')['style']) {
              document.getElementById('smallScreenVodControl')['style']['visibility'] = 'hidden'
            }
            this.showSuspend([video, minivideo, vodReplayBg, videoContainer],
              ['load_bg_sus', 'miniScreens', 'vodPlayBar-close', 'btn-play-pause-drop'], [vodpopUpDialog, audioBg])
            this.showDragSubsup = false
          }
        if (!this.isFullScreen) {
            vodpopUpDialog['z-index'] = 'initial'
          }
      }
    } else {
      if (!this.issuspendClose) {
        this.issuspendClose = true
        this.hideSuspend([video, minivideo, vodReplayBg, videoContainer], [vodpopUpDialog, audioBg])
        if (document.getElementById('smallScreenVodControl') && document.getElementById('smallScreenVodControl')['style']) {
            document.getElementById('smallScreenVodControl')['style']['visibility'] = 'visible'
          }
        this.showDragSubsup = true
        EventService.emit('HIDE_PLAYER_DROP')
        EventService.emit('HIDE_PLAYER_LEFT_RIGHT')
        this.vodPlayerDrop.initIsDrag()
        if (!this.isFullScreen) {
            vodpopUpDialog['z-index'] = 'initial'
          }
      }
    }
    if (this.isFullScreen) {
      vodpopUpDialog['z-index'] = 2200000000
    }
  }

    /**
     * close the small window play box
     */
  closeSupsend () {
    let video = document.querySelector('#vodVideoContainer video')['style']
    let vodpopUpDialog = document.getElementById('vodPopUpDialog')['style']
    let minivideo = document.querySelector('#vodMiniScreen')['style']
    let vodReplayBg = document.getElementById('vodReplayBg')['style']
    let videoContainer = document.querySelector('#vodVideoContainer')['style']
    let audioBg = document.querySelector('#audio-bg') && document.querySelector('#audio-bg')['style']
    this.issuspendClose = false
    this.hideSuspend([video, minivideo, vodReplayBg, videoContainer], [vodpopUpDialog, audioBg])
    this.vodPlayerDrop.initIsDrag()
    this.showDragSubsup = true
    document.getElementsByClassName('vodPlayBar-close')[0]['style']['visibility'] = 'hidden'
    document.getElementsByClassName('miniScreens')[0]['style']['visibility'] = 'hidden'
    if (document.getElementsByClassName('btn-play-pause-drop')[0]) {
      document.getElementsByClassName('btn-play-pause-drop')[0]['style']['visibility'] = 'hidden'
    }
    document.getElementsByClassName('load_bg_sus')[0]['style']['display'] = 'none'
    document.getElementsByClassName('play-btn')[0]['style']['visibility'] = 'hidden'
    if (!this.isFullScreen) {
      vodpopUpDialog['z-index'] = 'initial'
    }
  }

    /**
     * show icon by judge the play button
     */
  private judgePaused () {
    let ispaused = this.vodPlayBar.getVideoPaused()
    if (!ispaused || this.resetOnseek || this.isLoading) {
        // pause
      this.paused = 'url(assets/img/36/ondemand_suspension-box_play_36.png)'
    } else {
      this.paused = 'url(assets/img/36/ondemand_suspension-box_pause_36.png)'
    }
    document.getElementsByClassName('play-btn')[0]['style']['background'] = this.paused
  }

  playControl () {
    if (document.getElementsByClassName('load_bg_sus')[0]['style']['display'] === 'block') {
      return
    }
    this.vodPlayBar.smallVod.playControl()
    this.judgePaused()
  }

    /**
     * press the mouse to get the start position
     */
  setFirstPosition (event) {
    this.FirstDragPositonX = event.clientX
  }

    /**
     * release the mouse and get the end position
     */
  getEndPosition (event) {
    if (this.FirstDragPositonX !== event.clientX) {
        // the window position has been changed and the window has been dragged.
      return
    }
    if (document.getElementsByClassName('load_bg_sus')[0]['style']['display'] === 'block') {
      return
    }
    this.vodPlayBar.smallVod.playControl()
      // the location of the playback window has not changed, judge the button is 'play' or 'pause'.
    this.judgePaused()
  }

    /**
     * when you double-click the small screen, switch to full screen
     */
  changeScreen () {
    clearTimeout(this.dbClickTime)
      /*
        //Remove ability to doubleclick due to DWD-118
        if (!this.isFullScreen) {
            EventService.emit('UNLOGIN_ENTER_FULLSCREEN_CHANGE');
        }
        EventService.emit('CHANGE_SCREEN');
        */
  }

    /**
     * pause or play vod when you click the small screen
     */
  clickScreen () {
    let self = this
    clearTimeout(this.dbClickTime)
    this.dbClickTime = setTimeout(() => {
      if (session.pop('VOD_PLAYBAR_DROP_MOUSEMOVE')) {
        return
      }
      this.playControl()
    }, 300)
  }

    /**
     * play and pause event in float screen
     */
  private LivePlayerKeyUp (keyCode) {
    if (keyCode === SPACE_KEY) {
      _.delay(() => {
        this.judgePaused()
      }, 200)
    }
  }

    /**
     * show load circle or not
     */
    // suspendLoadDisplay() {
    //     let display = '';
    //     if (this.vodPlayBar && this.vodPlayBar.smallVod && this.vodPlayBar.smallVod.isShowOnLoad && this.vodPlayerDrop.isCanDrag) {
    //         this.isLoading = true;
    //         display = 'block';
    //     } else {
    //         this.isLoading = false;
    //         display = 'none';
    //     }
    //     return display;
    // }
}
