import * as _ from 'underscore'
import * as moment from 'moment'
import { Injectable, OnDestroy } from '@angular/core'
import { environment } from '../../../../environments/environment'
import { CommonService } from '../../../core/common.service'
import { playVOD } from 'ng-epg-sdk/vsp'
import { session } from 'src/app/shared/services/session'
import { EventService } from 'ng-epg-sdk/services'
import { reportVOD, createBookmark } from 'ng-epg-sdk'
import { playVODHeartbeat } from 'ng-epg-sdk'
import { MediaPlayService } from '../../../component/mediaPlay/mediaPlay.service'
import { TimerService } from '../../../shared/services/timer.service'

const pageTracker = require('../../../../assets/lib/PageTracker')
@Injectable()
export class PlayVodService implements OnDestroy {
  public reportIntervalId
  private videoData = {}
  private reportData = {}
  private subjectID
  private productID
  private player
  private curData
  private curVODID
  private curMediaID
  private curSeriesID
  private curIsTra
  private lastAction = ''
  private noMediaIdTimer = null
  playFilter = _.debounce((fc: Function) => {
    fc()
  }, 1000)

  constructor (
        private commonService: CommonService,
        private mediaPlayService: MediaPlayService,
        private timerService: TimerService) {
    EventService.removeAllListeners(['PLAY_FAIL_AND_REFRESH'])
    EventService.on('PLAY_FAIL_AND_REFRESH', () => {
      EventService.emit('REPLAY_VOD_LOAD')
      _.delay(() => {
        if (!_.isUndefined(this.curSeriesID) && !_.isUndefined(this.curIsTra)) {
            this.playVod(this.curData, this.curVODID, this.curMediaID, this.curSeriesID, this.curIsTra)
          } else if (!_.isUndefined(this.curSeriesID)) {
            this.playVod(this.curData, this.curVODID, this.curMediaID, this.curSeriesID)
          } else if (!_.isUndefined(this.curIsTra)) {
            this.playVod(this.curData, this.curVODID, this.curMediaID, this.curIsTra)
          } else {
            this.playVod(this.curData, this.curVODID, this.curMediaID)
          }
      }, 0)
    })
    EventService.removeAllListeners(['GETPLAYERTRA'])
    EventService.on('GETPLAYERTRA', (player) => {
      this.player = player
    })
  }

    /**
     * clean timer
     */
  ngOnDestroy () {
    clearTimeout(this.reportIntervalId)
    clearTimeout(this.noMediaIdTimer)
  }

  playVod (data, VODID, mediaID, seriesID?: string, isTra?: boolean) {
    clearTimeout(this.noMediaIdTimer)
    let self = this
    this.curData = data
    this.curVODID = VODID
    this.curMediaID = mediaID
    this.getCurSeriesID(seriesID)
    if (isTra) {
      this.curIsTra = isTra
    } else {
      this.curIsTra = undefined
    }
    if (!mediaID) {
      this.noMediaIdTimer = setTimeout(() => {
        session.put('NO_MEDIA_ID', true)
        EventService.emit('NO_VOD_MEDIA_ID')
      }, 500)
      return
    }
    session.remove('NO_MEDIA_ID')
    if (!Cookies.getJSON('IS_PROFILE_LOGIN')) {
        // @HWP-56-DISABLE-LOGIN-DIALOG-SHOW
        // session.put('IS_PROFILE_LOGIN_SCROLL_TOP', 'true');
        // EventService.emit('CLOSED_LOGIN_DIALOG');
        // this.commonService.openDialog(AccountLoginComponent).then(resp => {
        //     if (Cookies.getJSON('IS_PROFILE_LOGIN')) {
        //         EventService.emit('LOGININTRUE', false);
        //         if (data['VODType'] === '0') {
        //             self.playFilter(() => {
        //                 self.VodCheckLock(data, VODID, mediaID);
        //             });
        //         }
        //     }
        // });
      return
    } else {
      EventService.emit('LOGININSTATE', true)
    }
    EventService.emit('RESET_PLAYING_STATE')
    if (document.getElementsByClassName('load_bg')[0]) {
      document.getElementsByClassName('load_bg')[0]['style']['display'] = 'block'
    }
      // Do not show progress bar
    if (isTra) {
      EventService.emit('CHANGELOADSTATE', true)
    } else {
      EventService.emit('CHANGELOADSTATE', false)
    }
    if (self.player) {
      if (mediaID === '') {
        self.player.close()
      }
      if (session.get('VOD_IS_PLAYING')) {
        self.addLastBookmark()
      }
    }
    EventService.emit('CLOSE_VIDEO_PLAYER')
    _.delay(() => {
      if (!session.get('clipFlag')) {
        EventService.emit('subjectiddd', {})
      }
    }, 200)
    this.reportData = data
    if (_.isUndefined(isTra) || !isTra) {
      session.pop('videoData')
    }
    if (data['VODType'] === '0') {
      self.playFilter(() => {
        self.VodCheckLock(data, VODID, mediaID)
      })
    } else {
      self.playFilter(() => {
        self.VodCheckLock(data, VODID, mediaID, seriesID)
      })
    }
  }

    /**
     * get current series id
     */
  getCurSeriesID (seriesID) {
    if (seriesID) {
      this.curSeriesID = seriesID
    } else {
      this.curSeriesID = undefined
    }
  }

  addLastBookmark () {
    let self = this
    let time = self.player.getPosition()
    if (this.videoData && this.videoData['data'] && this.videoData['data'].episodes && this.videoData['data'].episodes[0]) {
      if (this.videoData['clip'] === 'false') {
        if (!this.videoData['judgePwd'] && !this.videoData['judgeSub']) {
            if (time) {
              this.addBookmark(this.videoData['data'], 'VOD', time + '', 'true', this.videoData['data']['ID'])
            }
            this.reportVOD('1')
          }
      }
    } else {
      if (this.videoData['clip'] === 'false') {
        if (!this.videoData['judgePwd'] && !this.videoData['judgeSub']) {
            if (time) {
              this.addBookmark(this.videoData['data'], 'VOD', time + '', {})
              this.reportVOD('1')
            }
          }
      }
    }
  }

    /**
     * get video data
     */
  dealWithData (VODID, mediaID, url, judgeSub, pricedProducts?: any, judgePwd?: boolean) {
    this.videoData['VODID'] = VODID
    this.videoData['url'] = url
    this.videoData['type'] = 'VOD'
    this.videoData['judgeSub'] = judgeSub
    if (session.get('clipFlag')) {
      this.videoData['clip'] = 'true'
    } else {
      this.videoData['clip'] = 'false'
    }
    if (judgeSub) {
      this.videoData['pricedProducts'] = pricedProducts
    }
    this.videoData['judgePwd'] = judgePwd
    this.videoData['mediaID'] = mediaID
  }

    /**
     * check if vod has a lock
     */
  VodCheckLock (data, VODID, mediaID, seriesID?: string) {
    let self = this
    if (data['VODType'] === '0') {
      this.vodPlayCheckLock(data, VODID, mediaID, null, '1').then(resp => {
          // parental control if not limited，play by the url adress which return
        if (resp && resp['bookmark']) {
            data['bookmark'] = resp['bookmark']
          } else {
            data['bookmark'] = '0'
          }
        self.dealWithData(VODID, mediaID, resp['playURL'], false)
        self.videoData['data'] = data
        self.videoData['resp'] = resp
        EventService.emit('LOAD_VODPLAY_VIDEO', self.videoData)
        if (!session.get('clipFlag')) {
            EventService.emit('subjectiddd', {})
            self.reportVOD('0')
            self.startPlayVODHeartbeat()
          } else {
            EventService.emit('CHANGETRA')
          }
      }, resp => {
        self.getBookmark(resp, data)
        if (!session.get('clipFlag')) {
            EventService.emit('subjectiddd', {})
          } else {
            EventService.emit('CHANGETRA')
          }
        let retCode = resp['result'] && resp['result']['retCode'] && resp['result']['retCode']
        if (parseInt(retCode, 10) === 144020008 || parseInt(retCode, 10) === 114020006) { // not purchased
            self.dealWithData(VODID, mediaID, resp['playURL'], true, resp['authorizeResult']['pricedProducts'])
            self.videoData['data'] = data
            self.videoData['resp'] = resp
            EventService.emit('LOAD_VODPLAY_VIDEO', self.videoData)
            return
          }
        if (retCode === '144020011') {
            let isInControl = self.isInControl(resp)
            if (isInControl) {
              // locked and parental control will pop password box, continue to play when input correct
              self.dealWithData(VODID, mediaID, resp['playURL'], false, {}, true)
              self.videoData['data'] = data
              self.videoData['resp'] = resp
              if (!session.get('clipFlag')) {
                session.put('videoData', self.videoData)
              }
              EventService.emit('LOAD_VODPLAY_VIDEO', self.videoData)
              EventService.removeAllListeners(['CHECK_VOD_PASSWORD'])
              EventService.on('CHECK_VOD_PASSWORD', function (password) {
                self.VODCheckAuthorizeLock(data, VODID, mediaID, password)
              })
            }
          }
      })
    } else {
      this.vodPlayCheckLock(data, VODID, mediaID, null, '1', seriesID).then(resp => {
          //  parental control if not limited，play by the url adress which return
        if (resp && resp['bookmark']) {
            data['bookmark'] = resp['bookmark']
          } else {
            data['bookmark'] = '0'
          }
        self.dealWithData(VODID, mediaID, resp['playURL'], false)
        self.videoData['data'] = data
        self.videoData['resp'] = resp
        EventService.emit('LOAD_VODPLAY_VIDEO', self.videoData)
        if (!session.get('clipFlag')) {
            EventService.emit('subjectiddd', {})
            self.reportVOD('0')
            self.startPlayVODHeartbeat()
          } else {
            EventService.emit('CHANGETRAINFO')
          }
      }, resp => {
        if (resp && resp['bookmark']) {
            data['bookmark'] = resp['bookmark']
          } else {
            data['bookmark'] = '0'
          }
        if (!session.get('clipFlag')) {
            EventService.emit('subjectiddd', {})
          } else {
            EventService.emit('CHANGETRAINFO')
          }
        let retCode = resp['result'] && resp['result']['retCode'] && resp['result']['retCode']
        if (parseInt(retCode, 10) === 144020008 || parseInt(retCode, 10) === 114020006) { // not purchased
            self.dealWithData(VODID, mediaID, resp['playURL'], true, resp['authorizeResult']['pricedProducts'])
            self.videoData['data'] = data
            self.videoData['resp'] = resp
            EventService.emit('LOAD_VODPLAY_VIDEO', self.videoData)
            return
          }
        if (retCode === '144020011') {
            let isInControl = self.isInControl(resp)
            if (isInControl) {
              // locked, parents are controlled pop box, continue to playing after input the correct
              self.dealWithData(VODID, mediaID, resp['playURL'], false, {}, true)
              self.videoData['data'] = data
              self.videoData['resp'] = resp
              EventService.emit('LOAD_VODPLAY_VIDEO', self.videoData)
              EventService.removeAllListeners(['CHECK_VOD_PASSWORD'])
              EventService.on('CHECK_VOD_PASSWORD', function (password) {
                self.VODCheckAuthorizeLock(data, VODID, mediaID, password, seriesID)
              })
            }
          }
      })
    }
  }

    /**
     * determine whether the user is controlled
     */
  isInControl (resp) {
    let isInControl = resp['authorizeResult'] && (resp['authorizeResult']['isLocked'] === '1' ||
            resp['authorizeResult']['isParentControl'] === '1')
    return isInControl
  }

    /**
     * get bookmark
     */
  getBookmark (resp, data) {
    if (resp && resp['bookmark']) {
      data['bookmark'] = resp['bookmark']
    } else {
      data['bookmark'] = '0'
    }
  }

    /**
     * vod password verifiers
     */
  VODCheckAuthorizeLock (data, VODID, mediaID, password, seriesID?: string) {
    let self = this
    let checkLock = {
      checkType: '1',
      password: password,
      type: '3'
    }
    if (data['VODType'] === '0') {
      return this.vodPlayCheckLock(data, VODID, mediaID, checkLock, '1').then(resp => {
        if (resp && resp['bookmark']) {
            data['bookmark'] = resp['bookmark']
          } else {
            data['bookmark'] = '0'
          }
        self.dealWithData(VODID, mediaID, resp['playURL'], false)
        if (!session.get('clipFlag')) {
            EventService.emit('subjectiddd', {})
            self.reportVOD('0')
            self.startPlayVODHeartbeat()
          }
        self.videoData['data'] = data
        self.videoData['resp'] = resp
        EventService.emit('LOAD_VODPLAY_VIDEO', self.videoData)
        EventService.emit('HIDE_VOD_POPUP_DIALOG')
        EventService.emit('closeDialog')
      }, resp => {
        EventService.emit('SHOW_VOD_AUTHRITE_ERROR_MESSAGE')
        return resp
      })
    } else {
      return this.vodPlayCheckLock(data, VODID, mediaID, checkLock, '1', seriesID).then(resp => {
        if (resp && resp['bookmark']) {
            data['bookmark'] = resp['bookmark']
          } else {
            data['bookmark'] = '0'
          }
        self.dealWithData(VODID, mediaID, resp['playURL'], false)
        if (!session.get('clipFlag')) {
            EventService.emit('subjectiddd', {})
            self.reportVOD('0')
            self.startPlayVODHeartbeat()
          }
        self.videoData['data'] = data
        self.videoData['resp'] = resp
        EventService.emit('LOAD_VODPLAY_VIDEO', self.videoData)
        EventService.emit('HIDE_VOD_POPUP_DIALOG')
        EventService.emit('closeDialog')
      }, resp => {
        EventService.emit('SHOW_VOD_AUTHRITE_ERROR_MESSAGE')
        return resp
      })
    }
  }

    /**
     * play authentication add lock
     */
  vodPlayCheckLock (data, VODID, mediaID, checkLock, isReturnProduct, seriesID?: string) {
    let self = this
    let req
    if (data['VODType'] === '0') {
      req = {
        VODID: VODID,
        mediaID: mediaID || '',
        checkLock: checkLock || {
            checkType: '0'
          },
        isReturnProduct: isReturnProduct
      }
    } else {
      req = {
        VODID: VODID,
        mediaID: mediaID || '',
        seriesID: seriesID,
        checkLock: checkLock || {
            checkType: '0'
          },
        isReturnProduct: isReturnProduct
      }
    }
    return playVOD(req).then(resp => { // geographical position, authentication, subscribe ,all above meet the condition
      self.playVODMSALog(resp, true)
      self.mediaPlayService.sendLicense(resp.authorizeResult)
      return resp
    }, resp => { // geographical position, authentication, subscribe ,one of them not meet the condition
      self.playVODMSALog(resp, false)
      EventService.emit('PLAY_VOD_ERROR_DATA', data)
      return Promise.reject(resp)
    })
  }

    /**
     * add bookmark
     */
  addBookmark (vod, type, time: string, episode, vodId?) {
    let req
    let contentId
    let subID
    if (time) {
      if (episode === 'true') {
        let sitcomNO = vod.sitcomNO
        vod = _.find(vod['episodes'], item => {
            return item['sitcomNO'] === sitcomNO
          }) || {}
        subID = vod['VOD']['ID']
      }
      contentId = this.getContentId(type, vod)

      if (vodId) {
        if (subID) {
            req = {
              bookmarks: [{
                bookmarkType: type,
                itemID: Number(vodId) + '',
                rangeTime: parseInt(time, 10) + '',
                subContentID: Number(subID) + '',
                subContentType: 'VOD'
              }]
            }
          } else {
            req = {
              bookmarks: [{
                bookmarkType: type,
                itemID: Number(vodId) + '',
                rangeTime: parseInt(time, 10) + '',
                subContentID: Number(vod.ID) + '',
                subContentType: 'VOD'
              }]
            }
          }
      } else {
        req = {
            bookmarks: [{
              bookmarkType: type,
              itemID: Number(contentId) + '',
              rangeTime: parseInt(time, 10) + ''
            }]
          }
      }
      return createBookmark(req).then(resp => {
        session.pop('QUERY_HISTORY_DELAY')
      })
    }
  }

    /**
     * get id of the content
     */
  getContentId (type, vod) {
    let contentId
    if (type === 'VOD' || type === 'PROGRAM') {
      let vodseries = vod.VOD && vod.VOD.series && vod.VOD.series[0]
      contentId = vod.ID ? vod.ID : vodseries && vodseries.VODID
    } else if (type === 'NPVR') {
      contentId = vod.taskID
    }
    return contentId
  }

  getsubjectID (subjectID) {
    this.subjectID = subjectID
    let timestamp = moment().utc().format('YYYYMMDDHHmmss')
    if (subjectID === '2') {
      const trackParams = {
        businessType: 1,
        contentCode: this.reportData['ID'],
        source: 3,
        timestamp: timestamp
      }
      pageTracker.pushBrowseRecmBehavior(trackParams)
      session.put('isRecommandVOD', true)
    } else {
      const trackParams = {
        businessType: 6,
        contentCode: subjectID,
        source: 2,
        timestamp: timestamp,
        recmActionID: session.get('LOG_TRACKER_RECMACTIONID')
      }
      pageTracker.pushBrowseContentDetail(trackParams)
    }
  }

    /**
     * [startPlayChannelHeartbeat open VOD playing heartbeat]
     */
  private startPlayVODHeartbeat () {
    clearInterval(this.reportIntervalId)
    this.timerService.getCaheCustomConfig().then((config) => {
      let configData = this.timerService.getVSPCustomConfig(config)
      let time = configData && configData['playHeartBitInterval'] * 1000 || 5 * 60 * 1000
      this.reportIntervalId = setInterval(() => {
        playVODHeartbeat({
            VODID: this.reportData['ID'],
            mediaID: this.curMediaID
          }).then(resp => {
            if (resp['isValid'] && Number(resp['isValid']) === 0) {
              EventService.emit('PLAY_HEARTBEAT_INVALID')
            }
          }, respFail => {
            if (respFail['isValid'] && Number(respFail['isValid']) === 0) {
              EventService.emit('PLAY_HEARTBEAT_INVALID')
            }
          })
      }, time)
    })
  }
    /**
     * report VOD play record
     */
  reportVOD (action) {
      /**
         * play behavior，value contains：action: string;
         *   >- 0：start play
         *   >- 1：exit play
         *
         * ID OF VOD.VODID: string;
         *
         * playing media's ID mediaID: string;
         *
         * whether play VOD in local downloaded，value contains：isDownload?: string;
         *   >- 0：no
         *   >- 1：yes
         *
         * if enter VOD by the specified subject, carry subject ID for reporter statistics user behavior by subject. subjectID?: string;
         *
         * the product's ID which authentication successful.productID?: string;
         */
    if (this.lastAction === action) {
      return
    }
    this.lastAction = action
    let mediaID = ''
    if (action === '0') {
      mediaID = this.curMediaID
    } else {
      mediaID = this.videoData['mediaID']
      if (this.videoData && this.videoData['resp'] && this.videoData['resp']['authorizeResult'] &&
                this.videoData['resp']['authorizeResult']['productID']) {
        this.productID = this.videoData['resp']['authorizeResult']['productID']
      } else {
        this.productID = '-1'
      }
    }
    reportVOD({
      action: action,
      VODID: this.reportData['ID'],
      mediaID: mediaID,
      productID: this.productID ? this.productID : '-1',
      subjectID: this.subjectID || this.reportData['subjectIDs'] ? this.reportData['subjectIDs'][0] : undefined
    })
  }

  playVODMSALog (resp, successFlag) {
    if (_.isUndefined(resp.authorizeResult)) {
      this.commonService.commonLog('PlayVOD', 'authorizeResult')
    }
    if (successFlag && _.isUndefined(resp.playURL)) {
      this.commonService.commonLog('PlayVOD', 'playURL')
    }
  }
}
