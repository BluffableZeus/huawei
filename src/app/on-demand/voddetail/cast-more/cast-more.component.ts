import * as _ from 'underscore'
import { Component, OnInit } from '@angular/core'
import { EventService } from 'ng-epg-sdk/services'
import { ActivatedRoute } from '@angular/router'
import { session } from 'src/app/shared/services/session'
import { Router } from '@angular/router'
import { VODRouterService } from '../../../shared/services/vod-router.service'
import { CastService } from '../cast-vod/cast-vod.service'
import { PictureService } from 'ng-epg-ui/services/picture.service'
import { TranslateService } from '@ngx-translate/core'

@Component({
  selector: 'app-cast-more',
  templateUrl: './cast-more.component.html',
  styleUrls: ['./cast-more.component.scss']
})

export class CastMoreComponent implements OnInit {
    /**
     * declare variables
     */
  public titles: Array<string> = []
    // name of the cast
  public castName = ''
    // id of the cast
  public castID = ''
    // url of current cast's head sculpture
  public nowHead = ''
  public moreVodList: {
    dataList: Array<any>,
    suspensionID: string
  }
    // array to store vods about the cast
  public vodData = []
    // variable help to judge there is data or not
  public hasCastData = false
    // number of vods about the cast
  public totalAll = 0
    /**
     * name for modules that would be used in this module
     */
  constructor (
        private route: ActivatedRoute,
        private router: Router,
        private vodRouter: VODRouterService,
        private castService: CastService,
        private pictureService: PictureService,
        private translate: TranslateService
    ) { }
    /**
     * initialization
     */
  ngOnInit () {
    let self = this
      // ensure the web page is at top
    document.body.scrollTop = 0
    document.documentElement.scrollTop = 0
    this.route
        .params
        .subscribe(params => {
          // get cast information from session if it's not available from params
          this.castName = params['nowName'] || session.get('castVodRole')
          this.castID = params['id'] || session.get('CAST_ID_DATA')
          this.nowHead = session.get('headUrl')
          this.titles.push(this.castName)
          this.dataHandle(this.castID)
        })
      // monitor the mouse scroll event
    EventService.on('LOAD_DATA', () => {
        // the current number of vods that shown is not all
      if (this.vodData.length > 0 && this.vodData.length < self.totalAll) {
        this.castService.searchContent({
            searchKey: this.castID,
            contentTypes: ['VOD'],
            searchScopes: ['CAST_ID'],
            count: '30',
            offset: this.vodData.length + '',
            sortType: ['RELEVANCE']
          }).then(resp => {
            let newData = _.map(resp.contents, (list, index) => {
              let url = list['VOD'] && list['VOD'].picture && list['VOD'].picture.posters &&
                        self.pictureService.convertToSizeUrl(list['VOD'].picture.posters[0],
                          { minwidth: 180, minheight: 240, maxwidth: 214, maxheight: 284 })
              if (list['VOD'].averageScore !== '10') {
                if (list['VOD'].averageScore) {
                  let averageScore
                  averageScore = list['VOD'].averageScore
                  if (averageScore.length === 1) {
                    averageScore += '.0'
                  } else {
                    averageScore = averageScore.substring(0, 4)
                    averageScore = Number(averageScore)
                    averageScore = averageScore.toFixed(1)
                  }
                  list['VOD'].averageScore = averageScore
                } else {
                  list['VOD'].averageScore = '0.0'
                }
              }
              list['VOD']['focusRoute'] = 'on-demand'
              return {
                vodId: list['VOD'].ID,
                name: list['VOD'].name,
                posterUrl: url || 'assets/img/default/home_poster.png',
                score: list['VOD'].averageScore || '0.0',
                titleName: 'cast',
                floatInfo: list['VOD'],
                subjectID: '-1',
                castName: this.castName,
                isOnDemand: true
              }
            })
            self.vodData = self.vodData.concat(newData)
            self.moreVodList = {
              dataList: self.vodData,
              suspensionID: 'moreData'
            }
          })
      }
    })
  }
    /**
     * handle the data with the parameter id
     * @param id id of the target cast
     */
  dataHandle (id) {
      // call a interface
    return this.castService.searchContent({
      searchKey: id,
      contentTypes: ['VOD'],
      searchScopes: ['CAST_ID'],
        // each time load no more than 30 vods
      count: '30',
      offset: '0',
      sortType: ['RELEVANCE']
    }).then(resp => {
        // deal with the data come from the interface
      let self = this
      self.vodData = _.map(resp.contents, (list, index) => {
          // get url of poster
          // resize the poster
          let url = list['VOD'] && list['VOD'].picture && list['VOD'].picture.posters &&
                 this.pictureService.convertToSizeUrl(list['VOD'].picture.posters[0],
                   { minwidth: 180, minheight: 240, maxwidth: 214, maxheight: 284 })
          let averageScore
          if (list['VOD'].averageScore !== '10') {
            // the vod has been scored
            if (list['VOD'].averageScore) {
              // ensure the scroe has one decimal number
              averageScore = list['VOD'].averageScore
              // if the score is integer smaller than 10
              if (averageScore.length === 1) {
                averageScore += '.0'
              } else {
                // if the score is not an integer
                averageScore = averageScore.substring(0, 4)
                averageScore = Number(averageScore)
                averageScore = averageScore.toFixed(1)
              }
              list['VOD'].averageScore = averageScore
            } else {
              // the vod hasn't been scored
              list['VOD'].averageScore = '0.0'
            }
          }
          list['VOD']['focusRoute'] = 'on-demand'
          return {
            vodId: list['VOD'].ID,
            name: list['VOD'].name,
            // use the url you get, else use the default
            posterUrl: url || 'assets/img/default/home_poster.png',
            // ues the average score you get, else use the default
            score: list['VOD'].averageScore || '0.0',
            titleName: 'cast',
            floatInfo: list['VOD'],
            subjectID: '-1',
            castName: this.castName,
            isOnDemand: true
          }
        })
        // the vod data that to loading
      self.moreVodList = {
          dataList: self.vodData,
          suspensionID: 'moreData'
        }
        // use the number you get, else use the default
      self.totalAll = Number(resp['total']) || 0
      self.hasCastData = true
    })
  }
    /**
     * open the detail page of the vod you click
     */
  vodDetail (event) {
    this.vodRouter.navigate(event[0])
  }
}
