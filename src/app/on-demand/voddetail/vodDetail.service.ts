import { Injectable } from '@angular/core'
import { getVODDetail, querySubjectDetail } from 'ng-epg-sdk/vsp'

@Injectable()
export class VodDetailHomeService {
  constructor () {}

  /**
     * when you enter the details page of the VOD, call the interface [getVODDetail] to get the information of the VOD.
     */
  getVodDetail (req: any) {
    return getVODDetail(req).then(resp => {
      return resp
    })
  }

  /**
     * call the interface [querySubjectDetail] to get column details
     */
  querySubjectDetail (req: any) {
    return querySubjectDetail(req).then(resp => {
      return resp
    })
  }
}
