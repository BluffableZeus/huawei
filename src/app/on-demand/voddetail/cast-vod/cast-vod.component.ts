import * as _ from 'underscore'
import { Component, Input, OnInit, OnDestroy } from '@angular/core'
import { EventService } from 'ng-epg-sdk/services'
import { session } from 'src/app/shared/services/session'
import { Router } from '@angular/router'
import { VODRouterService } from '../../../shared/services/vod-router.service'
import { CastService } from './cast-vod.service'
import { PictureService } from 'ng-epg-ui/services/picture.service'
import { TranslateService } from '@ngx-translate/core'
const pageTracker = require('../../../../assets/lib/PageTracker')

@Component({
  selector: 'app-cast-vod',
  styleUrls: ['./cast-vod.component.scss'],
  templateUrl: './cast-vod.component.html'
})

export class CastShowComponent implements OnInit, OnDestroy {
  /**
     * when the data changed
     */
  @Input() set setCastRolesData (castRolesData) {
    this.castRolesData = castRolesData
  }
    /**
     * declare variables
     */
  private castRolesData
  private contentDatas = []
  private vodlist = []
  private dataObj = {
    VODList: [],
    subjectName: ''
  }
  private datas = []
  private vodListSuspension = []
  private initID = ''
    // save the first actor's name when initialization
  private initName: string
    // record actor's name when mouseleave
  private lastName: string
  private nowID = ''
    // record actor's name when mouseenter
  private nowName: string
    // record actor's url of the photo when mouseleave
  private nowHead: string
    // save array index of the item temporary
  private tempIndex = 0
  private castData = []
  private castId = ['castVodSuspension', 'castVodList', 'castVodLeft', 'castVodRight']
  private castShow = [true, false, false]
  public hasContent = false
  private isEnter = []
  private avatar = []
  private castName = []
  private castIDs = []
  private castShowData = []
  private requestID = ''
  castTitle = this.translate.instant('cast').toUpperCase()
    /**
     * name for modules that would be used in this module
     */
  constructor (
        private router: Router,
        private vodRouter: VODRouterService,
        private castService: CastService,
        private pictureService: PictureService,
        private translate: TranslateService
    ) { }

    /**
     * initialization
     */
  ngOnInit () {
    this.castId = ['castVodSuspension', 'castVodList', 'castVodLeft', 'castVodRight']
    this.castShow = [true, false, false]
    if (this.castRolesData.length !== 0) {
      this.hasContent = true
      this.castData = this.castRolesData
      this.castData.sort((a, b) => {
        return Number(b.roleType === '1') - Number(a.roleType === '1')
      })
      this.getAvarta()
      this.initName = this.castName[0]
      this.nowName = this.initName
      this.lastName = this.initName
      this.initID = this.castIDs[0]
      this.nowID = this.castIDs[0]
      this.isEnter[0] = true
      this.tempIndex = 0
      this.requestID = this.nowID
      this.dataHandle(this.initID)
      EventService.on('ScreenSize', () => {
        if (document.body.clientWidth < 1440) {
            if (this.tempIndex > 4) {
              this.requestID = this.initID
              this.clearData()
              this.dataHandle(this.initID)
              this.isEnter = []
              this.isEnter[0] = true
              this.nowName = this.initName
              this.lastName = this.initName
            }
          }
      })
    }
  }
    /**
     * when exit the component, clear all the timer and events
     */
  ngOnDestroy () {
  }
    /**
     * clear the related datas.
     */
  clearData () {
    this.vodlist = []
    this.dataObj = {
      VODList: [],
      subjectName: ''
    }
    this.datas = []
  }

  getAvarta () {
    let self = this
    this.handleData(self.castData, 1)
    this.handleData(self.castData, 0)
    this.handleData(self.castData, 2)
    this.handleData(self.castData, 3)
    this.handleData(self.castData, 4)
    this.handleData(self.castData, 5)
    this.handleData(self.castData, 6)
    this.handleData(self.castData, 7)
    this.handleData(self.castData, 8)
    this.handleData(self.castData, 9)
    this.handleData(self.castData, 100)
    this.castShowData = this.filterSameCast(this.castShowData.splice(0, 7))
  }
    /**
     * get the unique cast array.
     * @param {Array<Object>} castArray
     * @return {Array<Object>}
     */
  filterSameCast (castArray: Array<Object>) {
    let existIDs = []
    let filterCast = []
    for (let i = 0; i < castArray.length; i++) {
      let isAlreadyExist = _.find(existIDs, item => {
        return item === castArray[i]['castID']
      })
      if (!isAlreadyExist) {
        filterCast.push(castArray[i])
        existIDs.push(castArray[i]['castID'])
      }
    }
    return filterCast
  }
    /**
     * deal with the data
     */
  handleData (data, type) {
    let self = this
    let number = this.castShowData && this.castShowData.length
    _.map(data, (castInfo, index) => {
      if (parseInt(castInfo['roleType'], 10) === type) {
        this.castShowData = this.castShowData.concat(castInfo['casts'])
        _.map(castInfo['casts'], function (cast, i) {
            let castName = cast['castName']
            let pictures = cast['picture'] && cast['picture']['icons']
              ? cast['picture'].icons[0] : 'assets/img/default/ondemand_casts.png'
            let id = cast['castID']
            self['castName'][i + number] = castName
            // resize the picture
            self['avatar'][i + number] = self.pictureService.convertToSizeUrl(pictures,
              { minwidth: 88, minheight: 88, maxwidth: 88, maxheight: 88 })
            self.castIDs[i + number] = id
          })
      }
    })
  }
    /**
     * when the mouse is over the cast avatar, it will get new related video data.
     */
  dataHandle (id) {
    let self = this
      // call an interface
    this.castService.searchContent({
      searchKey: id,
      contentTypes: ['VOD'],
      searchScopes: ['CAST_ID'],
      count: '24',
      offset: '0',
      sortType: ['RELEVANCE']
    }).then(resp => {
        // deal with the data come from the interface
      if (self.nowID === id) {
          self.contentDatas = resp.contents
          self.clearData()
          for (let i = 0; i < self.contentDatas.length; i++) {
            // the score is between 0 and 10 except 10 but include 0
            if (self.contentDatas[i].VOD.averageScore !== '10' && self.contentDatas[i].VOD.averageScore !== '10.0') {
              let averageScore = self.contentDatas[i].VOD.averageScore
              // when the score integer number
              if (averageScore.length === 1) {
                averageScore += '.0'
              } else {
                averageScore = averageScore.substring(0, 4)
                averageScore = Number(averageScore)
                averageScore = averageScore.toFixed(1)
              }
              self.contentDatas[i].VOD.averageScore = averageScore || '0.0'
            } else {
              // average score is 10
              self.contentDatas[i].VOD.averageScore = '10'
            }
            self.vodlist.push(self.contentDatas[i].VOD)
          }
          self.dataObj = {
            VODList: self.vodlist,
            subjectName: self.nowName
          }
          self.datas.push(self.dataObj)
          _.map(self.datas, (lists, index) => {
            _.map(lists['VODList'], (list, nextIndex) => {
              if (list['picture'] && list['picture'].posters && list['picture'].posters[0]) {
                let url = self.pictureService.convertToSizeUrl(list['picture'].posters[0],
                  { minwidth: 180, minheight: 240, maxwidth: 214, maxheight: 284 })
                list['picture'].posters = []
                list['picture'].posters[0] = url || 'assets/img/default/home_poster.png'
              }
            })
          })
          for (let i = 0; i < self.datas.length; i++) {
            self.vodListSuspension = []
            self.vodListSuspension[i] = _.map(self.datas[i].VODList, (list, index) => {
              return {
                titleName: 'cast',
                floatInfo: list,
                castName: self.nowName
              }
            })
          }
        }
    })
  }
    /**
     * deal with mouseenter Event.
     */
  mouseenter (i, e) {
    this.nowName = this.castName[i]
    this.nowHead = this.avatar[i]
    this.nowID = this.castIDs[i]
    this.isEnter = []
    this.isEnter[i] = true
    if (this.castIDs.length === 1) {

    } else {
      if (this.requestID === this.nowID) {
        return
      }
      this.requestID = this.nowID
      this.clearData()
      this.dataHandle(this.nowID)
    }
  }
    /**
     * deal with mouseleave Event.
     */
  mouseleave (i, e) {
    this.lastName = this.nowName
    this.tempIndex = i
  }
    /**
     * if user clicks the cast avatar, it will open another page which show more videos of this cast.
     */
  showMore () {
    session.put('castVodRole', this.nowName, true)
    session.put('headUrl', this.nowHead, true)
    session.put('CAST_ID_DATA', this.nowID, true)
    this.router.navigate(['video', 'cast', this.nowID])
  }
    /**
     * if user clicks the video poster, it will navigate to play the video.
     */
  onClickDetail (event) {
    const trackParams = {
      businessType: 1,
      contentCode: event[0],
      source: 2
    }
    pageTracker.pushBrowseRecmBehavior(trackParams)

    this.vodRouter.navigate(event[0])
  }
}
