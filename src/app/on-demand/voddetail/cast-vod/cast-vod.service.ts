import { Injectable } from '@angular/core'
import { searchContent } from 'ng-epg-sdk/vsp'
import { session } from 'src/app/shared/services/session'

@Injectable()
export class CastService {
  constructor () { }
  /**
     * call the interface [searchContent] for content search
     */
  searchContent (req: any) {
    return searchContent(req).then(resp => {
      // session storage
      session.put('LOG_TRACKER_RECMACTIONID', new Date().getTime())
      return resp
    })
  }
}
