import * as _ from 'underscore'
import { Component, Input, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { VODRouterService } from 'src/app/shared/services/vod-router.service'
import { PictureService } from 'ng-epg-ui/services/picture.service'
const pageTracker = require('../../../../assets/lib/PageTracker')
import { TranslateService } from '@ngx-translate/core'

@Component({
  selector: 'app-seasons',
  templateUrl: './seasons.component.html',
  styleUrls: ['./seasons.component.scss']
})

export class SeasonsComponent implements OnInit {
    /**
     * when the data changed
     */
  @Input() seasonsData
    /**
     * declare variables
     */
    // whether there is data or not
  public hasContent = false
    // array of season vods
  public receiveData = []
  public vodlist = []
  public dataObj = {
    VODList: [],
    subjectName: ''
  }
  public datas = []
  public vodListSuspension = []
  public seasonsId = ['seasonsVodSuspension', 'seasonsVodList',
    'seasonsVodLeft', 'seasonsVodRight']
  public seasonsShow = [true, false, false]
  seasonTitle = this.translate.instant('seasons').toUpperCase()
    /**
     * name for modules that would be used in this module
     */
  constructor (
        private route: Router,
        private vodRouter: VODRouterService,
        private pictureService: PictureService,
        private translate: TranslateService
    ) { }

    /**
     * initialization
     */
  ngOnInit () {
    this.seasonsId = ['seasonsVodSuspension', 'seasonsVodList', 'seasonsVodLeft', 'seasonsVodRight']
    this.seasonsShow = [true, false, false]
      // get the first 24 data
    this.receiveData = this.seasonsData.slice(0, 24)
    this.checkData()
  }
    /**
     * whether there is season vods
     */
  checkData () {
    if (this.receiveData.length === 0) {
      this.hasContent = false
    } else {
      this.hasContent = true
      this.dealData()
    }
  }
    /**
     * deal with the data
     */
  dealData () {
    for (let i = 0; i < this.receiveData.length; i++) {
      if (this.receiveData && this.receiveData[i] && this.receiveData[i].VOD) {
          // the vod hasn't been scored
        if (!this.receiveData[i].VOD.averageScore) {
            this.receiveData[i].VOD.averageScore = '0.0'
          } else {
            // the score is between 0 and 10 except 10 but include 0
            if (this.receiveData[i].VOD.averageScore !== '10' && this.receiveData[i].VOD.averageScore !== '10.0') {
              let averageScore = this.receiveData[i].VOD.averageScore
              // when the score integer number
              if (averageScore.length === 1) {
                averageScore += '.0'
              } else {
                averageScore = averageScore.substring(0, 4)
                averageScore = Number(averageScore)
                // ensure the score has one decimal place
                averageScore = averageScore.toFixed(1)
              }
              this.receiveData[i].VOD.averageScore = averageScore
              // the score is 10
            } else {
              this.receiveData[i].VOD.averageScore = '10'
            }
          }
      }
      this.vodlist.push(this.receiveData[i].VOD)
    }
    this.dataObj = {
      VODList: this.vodlist,
      subjectName: 'Seasons'
    }
    this.datas.push(this.dataObj)
    _.map(this.datas, (lists, index) => {
      _.map(lists['VODList'], (list, nextIndex) => {
          // get url of the poster
          // resize the poster
        let url = list['picture'] && list['picture'].posters && this.pictureService
            .convertToSizeUrl(list['picture'].posters[0], { minwidth: 180, minheight: 240, maxwidth: 200, maxheight: 280 })
        if (list['picture']) {
            list['picture'].posters = []
            list['picture'].posters[0] = url || 'assets/img/default/home_poster.png'
          }
      })
    })
    for (let i = 0; i < this.datas.length; i++) {
      this.vodListSuspension[i] = _.map(this.datas[i].VODList, (list, index) => {
        return {
            titleName: 'seasons',
            floatInfo: list
          }
      })
    }
  }
    /**
     * when click a season vod poster
     * @param event the target season vod
     */
  onClickDetail (event) {
    const trackParams = {
      businessType: 1,
      contentCode: event[0],
      source: 2
    }
    pageTracker.pushBrowseRecmBehavior(trackParams)
    this.vodRouter.navigate(event[0])
  }
}
