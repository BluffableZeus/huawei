import { Injectable } from '@angular/core'
import { createContentScore } from 'ng-epg-sdk/vsp'

@Injectable()
export class ScoreService {
  constructor () { }
  /**
     * call an interface
     */
  score (req: any) {
    // deal with the data come from the interface
    return createContentScore(req).then((resp) => {
      // add successfull
      return resp
    })
  }
}
