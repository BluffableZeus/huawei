import * as _ from 'underscore'
import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core'
import { EventService } from 'ng-epg-sdk/services'
import { session } from 'src/app/shared/services/session'
import { ScoreService } from './score.service'
import { ContentTitleComponent } from '../../content/shared/content-title/content-title.component'
@Component({
  selector: 'app-score',
  templateUrl: './score.component.html',
  styleUrls: ['./score.component.scss']
})

export class ScoreComponent implements OnInit {
  @Output() child: EventEmitter<Object> = new EventEmitter()
    /**
     * declare variables
     */
  private showScoreRate: boolean = false
  private starArray: Array<number>
  private scoreValue: number = 0
  private m: number = 0
  private n: number = 0
  private vodScore: number
  private userScore: number
  public info: Object
    // hide or show score-rate
  private clickFlag: boolean = false
    /**
     * name for modules that would be used in this module
     */
  constructor (
        private scoreService: ScoreService,
        private ContentTitleComponent: ContentTitleComponent
    ) { }
    /**
     * when the data changed
     */
  @Input() set vodinfo (data) {
    this.info = data
    this.setDate()
  }
    /**
     * initialization
     */
  ngOnInit () {
    this.setDate()
    let self = this
    EventService.on('IS_SHOWSCORE', function (boo) {
      self.clickFlag = boo
    })
  }
    /**
     * set star state and score
     */
  setDate () {
      // init the star array
    this.starArray = [0, 0, 0, 0, 0]
    this.userScore = this.info['userScore']
      // profile login
    if (Cookies.getJSON('IS_PROFILE_LOGIN') === true) {
      this.vodScore = this.userScore
      if (this.vodScore) {
        session.put('scoreValue', this.vodScore)
      }
      this.scoreValue = this.vodScore
      if (this.vodScore === 0) {
        this.starArray = [0, 0, 0, 0, 0]
      } else {
        this.starArray = this.arrayScore([0, 0, 0, 0, 0], this.vodScore)
        this.scoreValue = this.vodScore
      }
        // guest login
    } else {
      this.starArray = [0, 0, 0, 0, 0]
      this.scoreValue = 0
    }
  }
    /**
     * when mouse move in the star array to score the vod
     * @param data the index of the star your mouse move in
     * @param event the target star
     */
  move (data, event) {
      // variable to judge the target star should be filled by half or full
    let x = this.getOffsetX(event)
    let y = this.getOffsetY(event)
      // the score is odd number
    if (x > 0 && x < 12 && y < 24) {
      if (this.m === 0) {
        this.starArray = this.arrayScore([0, 0, 0, 0, 0], 2 * data + 1)
        this.scoreValue = 2 * data + 1
      }
      this.m = 1
      this.n = 0
        // the score is even number
    } else if (x > 12 && x < 24 && y < 24) {
      if (this.n === 0) {
          this.starArray = this.arrayScore([0, 0, 0, 0, 0], 2 * data + 2)
          this.scoreValue = 2 * data + 2
        }
      this.m = 0
      this.n = 1
    }
  }
    /**
     * get the array for stars to be filled with color
     * @param data the star array
     * @param part the score of the vod you give
     */
  arrayScore (data: Array<number>, part: number) {
    let pos1 = part / 2
    let pos2 = part % 2
    for (let i = 0; i < data.length; i++) {
        // the vod only get 1 score
      if (Math.floor(pos1) === 0) {
        return data = [1, 0, 0, 0, 0]
      }
        // the full star
      if (i < Math.floor(pos1)) {
        data[i] = 2
      }
    }
      // the half star
    if (pos2 === 1) {
      data[Math.floor(pos1)] = 1
    }
    return data
  }
    /**
     * show the score suspension frame
     */
  mouseEnterScore () {
    this.showScoreRate = true
    EventService.emit('SHOW_SCORE', this.showScoreRate)
  }
    /**
     * hide the score suspension frame
     */
  mouseLeaveScore () {
    this.showScoreRate = false
    EventService.emit('SHOW_SCORE', this.showScoreRate)
  }
    /**
     * mouse enter the star
     * @param data the index of the star your mouse enter
     */
  enter (data) {
    this.showScoreRate = true
    this.move(data, event)
  }
    /**
     * your mouse leave the score suspension frame
     */
  leave () {
      // profile login
    if (Cookies.getJSON('IS_PROFILE_LOGIN') === true) {
        // you do not score and the vod hasn't been scored
      if (this.vodScore === 0) {
        this.starArray = [0, 0, 0, 0, 0]
        this.scoreValue = 0
          // you do not score but the vod has been scored
      } else {
        this.starArray = this.arrayScore([0, 0, 0, 0, 0], this.vodScore)
        this.scoreValue = this.vodScore
      }
      this.m = 0
      this.n = 0
        // guest login
    } else {
      this.starArray = [0, 0, 0, 0, 0]
      this.scoreValue = 0
    }
  }
    /**
     * click to choose the score
     */
  click () {
    let self = this
    if (Cookies.getJSON('IS_PROFILE_LOGIN') === true) {
      let score = this.vodScore
      this.vodScore = this.scoreValue
      session.put('scoreValue', this.vodScore)
      if (this.vodScore !== Number(this.userScore)) {
        let req = {
            scores: [{
              contentID: this.info['contentIDs'],
              contentType: 'VOD',
              score: this.vodScore.toString()
            }]
          }
        this.scoreService.score(req)
            .then(resp => {
              this.child.emit(this.vodScore)
              _.delay(function () {
                EventService.emit('scoreSuccess', parseFloat(resp['newScores'][0]))
              }, 200)
              if (Number(this.userScore) === 0) {
                self.ContentTitleComponent.getnewscore()
              }
              this.userScore = this.vodScore
            }, () => {
              self.vodScore = score
              session.remove('scoreValue')
              this.setDate()
              this.userScore = this.vodScore
            })
      } else {
        this.child.emit(this.vodScore)
      }
    }
      // hide the score suspension frame
    this.clickFlag = true
  }
    /**
     * get your mouse offset on x-axis
     * @param event target element
     */
  getOffsetX (event) {
    let evt = event || window.event
    let srcObj = evt.target || evt.srcElement
    if (evt.offsetX) {
      return evt.offsetX
    } else {
      let rect = srcObj.getBoundingClientRect()
      let clientx = evt.clientX
      return clientx - rect.left
    }
  }
    /**
     * get your mouse offset on y-axis
     * @param event target element
     */
  getOffsetY (event) {
    let evt = event || window.event
    let srcObj = evt.target || evt.srcElement
    if (evt.offsetY) {
      return evt.offsetY
    } else {
      let rect = srcObj.getBoundingClientRect()
      let clienty = evt.clientY
      return clienty - rect.left
    }
  }
}
