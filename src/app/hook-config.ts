import { Injectable } from '@angular/core'
import { CheckResponseHook, AbortAjaxHook, LoginOccationHook } from 'ng-epg-sdk/hooks'
import { HookService } from 'ng-epg-sdk/services'

@Injectable()
export class HookConfig {
  constructor (private _hookService: HookService,
        private _checkResponseHook: CheckResponseHook,
        private _abortAjaxHook: AbortAjaxHook,
        private _loginOccationHook: LoginOccationHook) {
  }
  /**
     * initialization of register hook
     */
  init () {
    this._hookService.registerHook('ajax', this._loginOccationHook)
    this._hookService.registerHook('ajax', this._abortAjaxHook)
    this._hookService.registerHook('ajax', this._checkResponseHook)
  }
}
