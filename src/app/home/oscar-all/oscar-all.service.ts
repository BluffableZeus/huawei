import * as _ from 'underscore'
import { Injectable } from '@angular/core'
import { querySubjectVODBySubjectID, queryVODListBySubject } from 'ng-epg-sdk/vsp'
import { PictureService } from 'ng-epg-ui/services/picture.service'

@Injectable()
export class OscarAllService {
  constructor (private pictureService: PictureService) {}
  /**
     *  get and handle data from interface QuerySubjectVODBySubjectID.
     */
  getData (id, childrennumber, titles) {
    if (parseInt(childrennumber, 10) === 1) {
      return querySubjectVODBySubjectID({
        subjectID: id,
        subjectSortType: 'NAME:ASC',
        subjectCount: '50',
        VODCount: '24',
        offset: '0'
      }).then(resp => {
        let homeData = resp['subjectVODLists']
        let vodListSuspension = []
        let oscarVODInfo
        for (let i = 0; i < homeData.length; i++) {
          vodListSuspension[i] = _.map(homeData[i].VODs, (list, index) => {
            list['focusRoute'] = 'home'
            return {
              titleName: homeData[i]['subject'].name,
              floatInfo: list
            }
          })
        }
        oscarVODInfo = _.map(homeData, (list, index) => {
          return {
            subjectName: list['subject'].name,
            hasChildren: list['subject'].hasChildren,
            subjectID: list['subject'].ID,
            VODList: list['VODs']
          }
        })
        _.map(oscarVODInfo, (lists, index) => {
          _.map(lists['VODList'], (list, index1) => {
            this.score(list)
            let url = list['picture'] && list['picture'].posters &&
                         this.pictureService.convertToSizeUrl(list['picture'].posters[0],
                           { minwidth: 180, minheight: 240, maxwidth: 214, maxheight: 284 })
            if (!list['picture']) {
              list['picture'] = {}
              list['picture'].posters = []
            }
            list['picture'].posters[0] = url || 'assets/img/default/home_poster.png'
          })
        })
        let returndata = {}
        returndata[0] = oscarVODInfo
        returndata[1] = vodListSuspension
        return returndata
      })
    } else {
      return queryVODListBySubject({
        subjectID: id,
        sortType: 'CNTARRANGE',
        count: '50',
        offset: '0'
      }).then(resp => {
        let list = _.map(resp.VODs, (item, index) => {
          this.score(item)
          let url = item['picture'] && item['picture'].posters &&
                     this.pictureService.convertToSizeUrl(item['picture'].posters[0],
                       { minwidth: 180, minheight: 240, maxwidth: 214, maxheight: 284 })
          item['focusRoute'] = 'home'
          return {
            vodId: item['ID'],
            name: item['name'],
            posterUrl: url || 'assets/img/default/home_poster.png',
            score: item['averageScore'] || '0.0',
            floatInfo: item,
            titleName: titles,
            subjectID: id
          }
        })
        return list
      })
    }
  }
  /**
     *  handle average score of VOD.
     */
  score (list) {
    if (list.averageScore) {
      if (list.averageScore.length === 1) {
        list.averageScore += '.0'
      } else {
        if (list.averageScore.substring(0, 2) === '10') {
          list.averageScore = '10'
        } else {
          let score = list.averageScore
          list.averageScore = score.substring(0, 4)
          list.averageScore = Number(list.averageScore)
          list.averageScore = list.averageScore.toFixed(1)
        }
      }
    } else {
      list.averageScore = '0.0'
    }
    return list
  }
}
