import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { Router } from '@angular/router'
import { VODRouterService } from '../../shared/services/vod-router.service'
import { OscarAllService } from './oscar-all.service'
import { PictureService } from 'ng-epg-ui/services/picture.service'

@Component({
  selector: 'app-oscar-all',
  templateUrl: './oscar-all.component.html',
  styleUrls: ['./oscar-all.component.scss'],
  providers: [OscarAllService]
})

/*
This component is deprecated in favor of RecomVodMoreComponent
*/
export class OscarVodMoreComponent implements OnInit {
    // save data of recommended VOD column.
  public oscarVODInfo = []
    // save information of bread crumbs.
  public titles: Array<string> = ['Home >']
    // save value of id from routes.
  public id = ''
    // save VOD data handled.
  moreVodList: {
    'dataList': {},
    'row': string,
    'suspensionID': string
  }
    // check whether the VOD column has child column.
  public childrennumber
    // save information for VOD suspension.
  public vodListSuspension = []
    // save ID information for UI component.
  public oscarAllID
    // save flag information for UI component.
  public oscarAllShow = [true, false, true]

  constructor (
        private route: ActivatedRoute,
        private router: Router,
        private vodRouter: VODRouterService,
        private oscarallservice: OscarAllService,
        private pictureService: PictureService
    ) {}
    /**
     * lifecycle function 'ngOnInit'.
     */
  ngOnInit () {
    document.body.scrollTop = 0
    document.documentElement.scrollTop = 0
    this.oscarAllID = ['oscarAllSuspension', 'oscarAllList', 'oscarAllLeft', 'oscarAllRight']
    this.oscarAllShow = [true, false, true]
    this.route
        .params
        .subscribe(params => {
          this.id = params['id']
          this.childrennumber = params['childrennumber']
          let subjectName = params['name']
          this.getData(this.id, this.childrennumber, subjectName)
          this.titles = params['name']
        })
  }
    /**
     * get data from interface QuerySubjectVODBySubjectID.
     */
  getData (id, childrennumber, subjectName) {
    if (parseInt(childrennumber, 10) === 1) {
      this.oscarallservice.getData(id, childrennumber, subjectName).then((resp) => {
        this.oscarVODInfo = resp[0]
        this.vodListSuspension = resp[1]
      })
    } else {
      this.oscarallservice.getData(id, childrennumber, subjectName).then((resp) => {
        this.moreVodList = {
            'dataList': resp,
            'row': 'All',
            'suspensionID': 'oscarAllContent'
          }
      })
    }
  }
    /**
     * click the column title to jump to VOD waterfall page.
     */
  moreShow (datas) {
    this.router.navigate(['video', 'category', datas[0]])
  }
    /**
     * click the VOD poster to jump to VOD detail page.
     */
  onClickDetail1 (event) {
    this.vodRouter.navigate(event[0])
  }
    /**
     * click the VOD poster to jump to VOD detail page.
     */
  onClickDetail2 (event) {
    this.vodRouter.navigate(event[0])
  }
  /**
     * click the title to jump to Home page.
     */
  goback (backRoute) {
    if (backRoute.indexOf('Home') !== -1) {
      this.router.navigate(['/home'])
    }
  }
}
