import { Injectable } from '@angular/core'
import { queryHomeData } from 'ng-epg-sdk/vsp'

@Injectable()
export class HomeService {
  constructor () { }
  // the interface of home
  queryHomeData (req: any) {
    return queryHomeData(req).then(resp => {
      return resp
    })
  }
}
