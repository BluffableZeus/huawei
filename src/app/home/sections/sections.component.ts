import { Component, Input } from '@angular/core'
import { Router } from '@angular/router'
import { VODRouterService } from 'src/app/shared/services/vod-router.service'
import { OnChildEvent, OnJumpToEvent, VODListSubject } from 'ng-epg-ui/webtv-components/childcolumnshow'
import { QueryHomeDataResponse, VOD } from 'ng-epg-sdk'
import { SectionService } from './section.service'
import { RecSubject } from 'ng-epg-sdk/vsp/api'

@Component({
  selector: 'app-home-content',
  templateUrl: './sections.component.html',
  styleUrls: ['./sections.component.scss'],
  providers: [SectionService]
})

export class HomeSectionsComponent {
    // save data for VOD suspension.
    // save data of recommended VOD columns.
  public sections: VODListSubject[]
    // save ID information for UI component.
  public suspensionIds = ['homeVodSuspension']
    // save flag information for UI component.
  public visibilityFlags = [true, true, true]

  constructor (
        private router: Router,
        private vodRouter: VODRouterService,
        protected service: SectionService
    ) { }

    /**
     *  lifecycle function 'Input'.
     */
  @Input() set data (data: QueryHomeDataResponse) {
    this.sections = []

      // Order of calling this methods change order of categories on the page
    if (data) {
      this.processRecommendations(data.recSubject)
    }
  }

  checkEmpty (list: VODListSubject) {
    if (list.VODList.length > 0) {
      this.sections.push(list)
    }
  }

    /**
     *  handle VOD data for showing.
     */
  processRecommendations (subjects: RecSubject[] = []) {
    if (!subjects) {
      return
    }
    subjects.forEach(list => {
      if (list && list.VODList && list.subject) {
        this.checkEmpty(this.service.processRecommendation(list))
      }
    })
  }

  processTopForYou (data: VOD[]) {
    if (data) {
      this.checkEmpty(this.service.processTopForYou(data))
    }
  }

  processEIW (data: VOD[]) {
    if (data) {
      this.checkEmpty(this.service.processEIW(data))
    }
  }

    /**
     *  turn to more movies page.
     */
  moreShow (event: OnJumpToEvent) {
    const [subjectId, hasChildren, subjectName] = event

    switch (subjectId) {
      case SectionService.EIW_SUBJECT_ID:
        this.router.navigate(['home', 'magic', this.service.getTitle(subjectName)])
        break
      default:
        this.router.navigate(['video', 'category', subjectId])
        break
    }
  }

    /**
     *  turn to vod detail page.
     */
  onClickDetail (event: OnChildEvent) {
    const [vodId] = event
    this.vodRouter.navigate(vodId)
  }
}
