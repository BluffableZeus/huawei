import { Injectable } from '@angular/core'
import { VOD } from 'ng-epg-sdk'
import { PictureService } from 'ng-epg-ui/services/picture.service'
import { VodSuspension } from 'ng-epg-ui/webtv-components/vodSuspension'
import { RecSubject } from 'ng-epg-sdk/vsp/api'
import { VODListSubject } from 'ng-epg-ui/webtv-components/childcolumnshow'

@Injectable()
export class SectionService {
  static IMAGE_SIZE = { minwidth: 164, minheight: 230, maxwidth: 164, maxheight: 230 }

  static TOP_FOR_YOU_SUBJECT_NAME = 'picks_for_you'

  static EIW_SUBJECT_NAME = 'everyone_is_watching'
  static EIW_SUBJECT_ID = '2'

  static EIW_TITLE = 'every_one_is_watching'
  static TOP_FOR_YOU_TITLE = 'top_picks_for_you'

  static COUNTING_WATCHING_ID = 'counting_watching';
  static COUNTING_WATCHING_NAME = 'counting_watching';

  constructor (protected pictureService: PictureService) {
  }

  getTitle (name: string) {
    switch (name) {
      case SectionService.EIW_SUBJECT_NAME:
        return SectionService.EIW_TITLE
      case SectionService.TOP_FOR_YOU_SUBJECT_NAME:
        return SectionService.TOP_FOR_YOU_TITLE
    }
  }

  processRecommendation (list: RecSubject): VODListSubject {
    list.VODList.forEach((vod: VOD) => {
      this.processVOD(vod)
    })

    return {
      VODList: list.VODList,
      subjectName: list.subject.name,
      subjectID: list.subject.ID,
      hasChildren: Number(list.subject.hasChildren)
    }
  }

  processTopForYou (data: VOD[]): VODListSubject {
    return {
      VODList: data.map(vod => this.processVOD(vod)),
      subjectName: SectionService.TOP_FOR_YOU_SUBJECT_NAME,
      subjectID: SectionService.EIW_SUBJECT_ID,
      hasChildren: -1
    }
  }

  processEIW (data: VOD[]): VODListSubject {
    return {
      VODList: data.map(vod => this.processVOD(vod)),
      subjectName: SectionService.EIW_SUBJECT_NAME,
      subjectID: SectionService.EIW_SUBJECT_ID,
      hasChildren: -1
    }
  }

  processContinueWatching(data: VOD[]): VODListSubject {
    return {
      VODList: data.map(vod => this.processVOD(vod)),
      subjectName: SectionService.COUNTING_WATCHING_NAME,
      subjectID: SectionService.COUNTING_WATCHING_ID,
      hasChildren: -1
    }
    return 
  }

  protected processVOD (vod: VOD): VOD {
    vod['focusRoute'] = 'home'

    /** DISABLE FOR C50 */
    if (vod.picture && vod.picture.posters) {
      const url = this.pictureService.convertToSizeUrl(vod.picture.posters[0], SectionService.IMAGE_SIZE)

      vod.picture.posters = [
        url || 'assets/img/default/home_poster.png'
      ]
    }
    /** DISABLE FOR C50 -- end */

    return vod
  }
  
  getImageSize() {
    return SectionService.IMAGE_SIZE;
  }
}
