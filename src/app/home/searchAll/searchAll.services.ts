import * as _ from 'underscore'
import { Injectable } from '@angular/core'
import { searchContent, queryRecmVODList, getPlaybillDetail, searchPVR } from 'ng-epg-sdk/vsp'
import { PictureService } from 'ng-epg-ui/services/picture.service'
import { TranslateService } from '@ngx-translate/core'
import { DateUtils } from 'ng-epg-sdk/utils'
import { config } from '../../shared/services/config'
import { CommonService } from '../../core/common.service'
import { session } from 'src/app/shared/services/session'

@Injectable()
export class SearchService {
  private recordingMode = config.PVRMode
  private recordState
  private duration
  private recordTime
  private recordweekTime
  constructor (
        private pictureService: PictureService,
        private translate: TranslateService,
        private commonService: CommonService
    ) { }

    /**
     * deal with score
     */
  score (list) {
    if (list.averageScore.length === 1) {
      list.averageScore += '.0'
    } else {
      let score = list.averageScore
      list.averageScore = score.substring(0, 4)
      list.averageScore = Number(list.averageScore)
      list.averageScore = list.averageScore.toFixed(1)
    }
    return list
  }

    /**
     * get playbill detail by interface getPlaybillDetail
     */
  getQueryPlaybill (id) {
    return getPlaybillDetail({
      playbillID: id,
      isReturnAllMedia: '1'
    }).then(resp => {
      return resp
    })
  }

    /**
     * get search result by interface searchContent
     */
  searchContent (req: any) {
    const request: any = {
      'searchKey': req.id.replace(/(\ ){2,}/g, ' '),
      'contentTypes': req.type,
      'searchScopes': ['ALL'],
      'count': '31',
      'offset': req.offset,
      'excluder': { 'subSitcomSearchFlag': 0 },
      'sortType': ['RELEVANCE']
    }
    if (!req.KaraokSubjectId || (req.KaraokSubjectId && req.KaraokSubjectId === '')) {
      return searchContent(request).then(resp => {
        session.put('LOG_TRACKER_RECMACTIONID', new Date().getTime())
        return resp
      })
    } else {
      request.excluder.subjectID = [req.KaraokSubjectId]
      return searchContent(request).then(resp => {
        session.put('LOG_TRACKER_RECMACTIONID', new Date().getTime())
        return resp
      })
    }
  }

    /**
     * call interface when the [searchContent] interface have no data
     */
  queryRecmVODList () {
    return queryRecmVODList({
      offset: '0',
      count: '6',
      sortType: 'PLAYTIMES:DESC',
      action: '4'
    }).then(resp => {
      let list5 = _.map(resp.VODs, (list, index) => {
          if (list.averageScore !== '10' && list.averageScore.substring(0, 2) !== '10') {
            this.score(list)
          } else {
            list.averageScore = '10'
          }
          list['focusRoute'] = 'on-demand'
          let url = list.picture && list.picture.posters && list.picture.posters[0]
          return {
            vodId: list.ID,
            name: list.name,
            score: list.averageScore || '0.0',
            posterUrl: url,
            titleName: 'nodata',
            floatInfo: list,
            subjectID: '-1'
          }
        })
      return list5
    })
  }

    /**
     * add prefix 0
     */
  prefixZero (str, len) {
    str = '' + (str || '')
    if (str.length >= len) {
      return str
    }

    let gapLen = len - str.length,
      i, arr = []
    for (i = 0; i < gapLen; i++) {
      arr[i] = '0'
    }

    return arr.join('') + str
  }

    /**
     * deal with the data for search result
     */
  showSearchResult (data) {
    let lists = _.map(data, (list, index) => {
      if (list['contentType'] === 'VIDEO_VOD' || list['contentType'] === 'AUDIO_VOD') {
        return this.dealWithVod(list)
      }
      if (list['contentType'] === 'VIDEO_CHANNEL' || list['contentType'] === 'AUDIO_CHANNEL') {
        return this.dealWithChannel(list)
      }
      if (list['contentType'] === 'PROGRAM') {
        return this.dealWithProgram(list)
      }
      if (list['contentType'] === '') {
        return this.dealWithRecording(list)
      }
    })
    return lists
  }

    /**
     * deal with vod of search result
     */
  dealWithVod (list) {
    if (list['VOD']['averageScore'] && list['VOD']['averageScore'].substring(0, 2) !== '10') {
      this.score(list['VOD'])
    } else {
      list['VOD']['averageScore'] = '10'
    }
    let url = list['VOD']['picture'] && list['VOD']['picture'].posters &&
            this.pictureService.convertToSizeUrl(list['VOD']['picture'].posters[0],
              { minwidth: 180, minheight: 240, maxwidth: 214, maxheight: 284 })
    return {
      vodId: list['VOD']['ID'],
      name: list['VOD']['name'],
      contentType: 'VIDEO_VOD',
      posterUrl: url || 'assets/img/default/home_poster.png',
      score: list['VOD']['averageScore'] || '0.0',
      titleName: 'Search',
      floatInfo: list['VOD']
    }
  }

    /**
     * deal with the channel data
     */
  dealWithChannel (list) {
    let timeDiffs = this.dealTimeDiffCom(list)
    let startTime
    let endTime
    if (list['channel'] && list['channel']['currentPlaybill'] && list['channel']['currentPlaybill']['startTime']) {
      startTime = this.commonService.dealWithDateJson('D10', parseInt(list['channel']['currentPlaybill']['startTime'], 10))
    }
    if (list['channel'] && list['channel']['currentPlaybill'] && list['channel']['currentPlaybill']['endTime']) {
      endTime = this.commonService.dealWithDateJson('D10', parseInt(list['channel']['currentPlaybill']['endTime'], 10))
    }
    return {
      channelId: list['channel']['ID'],
      name: list['channel']['name'],
      contentType: 'VIDEO_CHANNEL' || 'AUDIO_CHANNEL',
      channelName: list['channel']['name'],
      leftTime: timeDiffs,
      timeScope: startTime + '-' + endTime,
      posterUrl: this.getUrl(list) || 'assets/img/default/search_channel_poster.png',
      floatInfo: list['channel'],
      isSearchRecord: 'isSearchRecord',
      param: {
        'min': timeDiffs
      }
    }
  }

    /**
     * get url of poster
     */
  getUrl (list) {
    let url = list['channel'] && list['channel']['picture'] && list['channel']['picture'].channelPics &&
            this.pictureService.convertToSizeUrl(list['channel']['picture'].channelPics[0],
              { minwidth: 100, minheight: 100, maxwidth: 100, maxheight: 100 })
    return url
  }

  dealTimeDiffCom (list) {
    let end
    let timeDiff
    if (list['channel'] && list['channel']['currentPlaybill'] && list['channel']['currentPlaybill']['endTime']) {
      end = list['channel']['currentPlaybill']['endTime']
      if (end > Date.now()['getTime']()) {
        timeDiff = this.commonService.dealWithPlaybillTimeReduce(new Date(Number(Date.now()['getTime']())).getTime(),
            new Date(Number(end)).getTime())
      } else {
        timeDiff = 0
      }
    } else {
      timeDiff = 0
    }
    return timeDiff
  }

    /**
     * deal with the program data
     */
  dealWithProgram (list) {
    let url = list['playbill']['picture'] && list['playbill']['picture'].posters &&
            this.pictureService.convertToSizeUrl(list['playbill']['picture'].posters[0],
              { minwidth: 180, minheight: 120, maxwidth: 214, maxheight: 160 })
    let startTime = this.commonService.dealWithDateJson('D10', list['playbill']['startTime'])
    let endTime = this.commonService.dealWithDateJson('D10', list['playbill']['endTime'])
    let start = list['playbill']['startTime']
    let end = list['playbill']['endTime']
    let playLength = this.commonService.dealWithPlaybillTimeReduce(new Date(Number(Date.now()['getTime']())).getTime(),
        new Date(Number(end)).getTime())
    if (Date.now()['getTime']() < start) {
      list['playbill']['timeState'] = 'future'
      list['playbill']['weekTime'] = this.commonService.dealWithDateJson('D05', parseInt(list['playbill']['startTime'], 10))
      list['playbill']['time'] = DateUtils.format(parseInt(list['playbill']['startTime'], 10), ' DD.MM')
      let month = DateUtils.format(parseInt(list['playbill']['startTime'], 10), 'MM')
      let day = DateUtils.format(parseInt(list['playbill']['startTime'], 10), 'D')
      month = this.commonService.dealWithTime(month)
      if (session.get('languageName') === 'zh') {
        list['playbill']['time'] = month + '/' + day
      } else {
        list['playbill']['time'] = month + ' ' + day
      }
    } else if (Date.now()['getTime']() > end) {
      list['playbill']['timeState'] = 'before'
      list['playbill']['weekTime'] = this.commonService.dealWithDateJson('D05', parseInt(list['playbill']['startTime'], 10))
      let month = DateUtils.format(parseInt(list['playbill']['startTime'], 10), 'MM')
      let day = DateUtils.format(parseInt(list['playbill']['startTime'], 10), 'D')
      month = this.commonService.dealWithTime(month)
      if (session.get('languageName') === 'zh') {
          list['playbill']['time'] = month + '/' + day
        } else {
          list['playbill']['time'] = month + ' ' + day
        }
    } else {
      list['playbill']['timeState'] = 'now'
      list['playbill']['time'] = { 'min': playLength }
    }
    return {
      id: list['playbill']['ID'],
      programId: list['playbill']['ID'],
      name: list['playbill']['name'],
      contentType: 'PROGRAM',
      posterUrl: url || 'assets/img/default/search_result.png',
      endTime: list['playbill']['endTime'],
      weekTime: list['playbill']['weekTime'],
      timeLength: list['playbill']['time'],
      leftTime: playLength,
      timeScope: startTime + '-' + endTime,
      timeState: list['playbill']['timeState'],
      floatInfo: list['playbill']
    }
  }

    /**
     * deal with the recording data
     */
  dealWithRecording (list) {
    let posterUrl
    let recordingMode
    let introduce
    let seriesName
    let seasonNO
    let sitcomNO
    let log
    let genres
    let name
    let channelName
    let rating
    let subNum

    recordingMode = this.getRecordingMode()
    subNum = list['childPVRIDs'] && list['childPVRIDs'].length
    channelName = list['extensionFields'] && list['extensionFields'][0] && list['extensionFields'][0].values &&
            list['extensionFields'][0].values[0]
    let fileID = list['files'] && list['files'][0] && list['files'][0]['fileID']
      // judge record task state
    this.judgeRecordState(list)
    if (list['policyType'] === 'PlaybillBased') {
      posterUrl = this.dealWithPosterUrl(list)
      introduce = this.dealWithIntroduce(list)
      log = this.dealWithLog(list)
      seasonNO = this.getRecordSeasonNO(list)
      sitcomNO = this.getRecordSitcomNO(list)
      seriesName = this.dealWithSeasonNO(seasonNO, sitcomNO)
      genres = this.dealWithGenres(list)
      rating = this.dealWithRating(list)
      channelName = this.dealWithChannelName(list)
    } else {
      introduce = this.getRecordIntroduce(list)
    }

    name = list['name']
    return {
      id: list['ID'],
      name: name,
      posterUrl: posterUrl || 'assets/img/default/search_result.png',
      log: log || 'assets/img/defaultchannel00x80.png',
      contentType: '',
      seriesName: seriesName,
      genres: genres,
      rating: rating,
      introduce: introduce,
      floatInfo: list,
      status: list['status'],
      policyType: list['policyType'],
      duration: this.duration,
      recordweekTime: this.recordweekTime,
      recordTime: this.recordTime,
      recordState: this.recordState,
      recordingMode: recordingMode,
      endTime: list['endTime'],
      startTime: list['startTime'],
      storageType: list['storageType'],
      channelID: list['channelID'],
      subNum: subNum,
      channelName: channelName,
      isSearchRecord: 'isSearchRecord',
      fileID: fileID,
      beginOffsetTime: Number(list['startTime']) - Number(list['beginOffset'] * 1000),
      endOffsetTime: Number(list['endTime']) + Number(list['endOffset'] * 1000)
    }
  }

    /**
     * get seasonNo of record
     */
  getRecordSeasonNO (list) {
    let seasonNO = list['playbill'] && list['playbill'].playbillSeries && list['playbill'].playbillSeries.seasonNO
    return seasonNO
  }

    /**
     * get sitcomNO of record
     */
  getRecordSitcomNO (list) {
    let sitcomNO = list['playbill'] && list['playbill'].playbillSeries && list['playbill'].playbillSeries.sitcomNO
    return sitcomNO
  }

    /**
     * get introduce of record
     */
  getRecordIntroduce (list) {
    let introduce = list['childPVR'] && list['childPVR']['playbill'] && list['childPVR']['playbill'].introduce
        ? list['childPVR']['playbill'].introduce : ''
    return introduce
  }

    /**
     * get channel name
     */
  dealWithChannelName (list) {
    let channeName = list['playbill'] && list['playbill']['channel'] && list['playbill']['channel']['name']
    return channeName
  }

    /**
     * get rating
     */
  dealWithRating (list) {
    let rating = list['playbill'] && list['playbill'].rating && list['playbill'].rating.name
    return rating
  }

    /**
     * get introduce of playbill
     */
  dealWithIntroduce (list) {
    let introduce = list['playbill'] && list['playbill'].introduce ? list['playbill'].introduce : ''
    return introduce
  }

    /**
     * get the absolute path of the title map
     */
  dealWithLog (list) {
    let log = list['playbill'] && list['playbill'].picture &&
            list['playbill'].picture.posters && list['playbill'].picture.titles[0]
    return log
  }

    /**
     * get name of genres
     */
  dealWithGenres (list) {
    let genres = list['playbill'] && list['playbill'].genres &&
            list['playbill'].genres[0] && list['playbill'].genres[0].genreName
    return genres
  }

    /**
     * get the absolute path of the poster
     */
  dealWithPosterUrl (list) {
    let url = list['playbill'] && list['playbill'].picture &&
            list['playbill'].picture.posters && list['playbill'].picture.posters[0]
    return url
  }

    /**
     * get series name
     */
  dealWithSeasonNO (seasonNO, sitcomNO) {
    let seriesName
    if (seasonNO && sitcomNO) {
      seriesName = this.translate.instant('vod_season', { num: seasonNO }) +
                ', ' + this.translate.instant('episode', { num: sitcomNO })
    } else if (seasonNO && !sitcomNO) {
      seriesName = this.translate.instant('vod_season', { num: seasonNO })
    } else if (!seasonNO && sitcomNO) {
        seriesName = this.translate.instant('episode', { num: sitcomNO })
      } else {
        seriesName = ''
      }
    return seriesName
  }

    /**
     * get the recording status and recording time
     */
  judgeRecordState (list) {
    if (list['status'] === 'SUCCESS') {
      this.recordState = 'record'
      this.duration = Math.ceil(list['duration'] / 60) + ' ' + this.translate.instant('record_min')
      this.recordweekTime = this.commonService.dealWithDateJson('D01', list['startTime'])
      this.recordTime = this.commonService.dealWithDateJson('D01', list['startTime'])
    } else if (list['status'] === 'RECORDING') {
      this.recordState = 'recording'
      this.recordweekTime = ''
      this.recordTime = 'now'
      this.duration = DateUtils.format(list['startTime'], 'HH:mm') + '-' + DateUtils.format(list['endTime'], 'HH:mm')
      this.duration = this.commonService.dealWithDateJson('D10', list['startTime']) +
              '-' + this.commonService.dealWithDateJson('D10', list['endTime'])
    } else {
      this.recordState = 'record'
      this.recordweekTime = DateUtils.format(list['startTime'], 'ddd')
      this.recordTime = this.commonService.dealWithDateJson('D01', list['startTime'])
      this.duration = Math.ceil(list['duration'] / 60) + ' ' + this.translate.instant('record_min')
    }
  }

    /**
     * get recording mode
     * if recording mode is 'MIXPVR', the value is '0'
     * if recording mode is 'CPVR'，'NPVR' or 'CPVR&NPVR', the value is '1'
     */
  getRecordingMode () {
    let recordingMode
    if (this.recordingMode === 'MIXPVR') {
      recordingMode = '0'
    } else {
      recordingMode = '1'
    }
    return recordingMode
  }

    /**
     * the interface for PVR
     */
  searchPVR (req: any) {
    let singlePVRFilter = {
      scope: 'ALL',
      storageType: 'NPVR',
      status: ['RECORDING', 'OTHER', 'SUCCESS', 'PART_SUCCESS'],
      isFilterByDevice: '1'
    }
    return searchPVR({
      searchKey: req.id,
      singlePVRfilter: singlePVRFilter,
      count: '31',
      offset: req.offset,
      sortType: 'STARTTIME:DESC'
    }, {}).then(function (resp) {
      return Promise.resolve(resp)
    })
  }
}
