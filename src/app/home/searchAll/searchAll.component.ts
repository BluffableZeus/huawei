import * as _ from 'underscore'
import { Component, OnInit, ViewChild, Output, EventEmitter, OnDestroy } from '@angular/core'
import { Router } from '@angular/router'
import { ActivatedRoute } from '@angular/router'
import { VODRouterService } from '../../shared/services/vod-router.service'
import { SuspensionComponentVod } from 'ng-epg-ui/webtv-components/vodSuspension/vodSuspension.component'
import { SuspensionComponentProgram } from 'ng-epg-ui/webtv-components/programSuspension/programSuspension.component'
import { SuspensionComponentChannel } from 'ng-epg-ui/webtv-components/channelSuspension/channelSuspension.component'
import { RecordSuspensionComponent } from 'ng-epg-ui/webtv-components/recordSuspension/recordSuspension.component'
import { EventService } from 'ng-epg-sdk/services'
import { session } from 'src/app/shared/services/session'
import { SearchService } from './searchAll.services'
import { LiveTvService } from '../../shared/services/livetv.service'
import { TranslateService } from '@ngx-translate/core'
import { PlaybillAppService } from '../../component/playPopUpDialog/playbillApp.service'
import { CommonService } from '../../core/common.service'

@Component({
  selector: 'app-search-all',
  styleUrls: ['./searchAll.component.scss'],
  templateUrl: './searchAll.component.html'
})
export class SearchAllComponent implements OnInit, OnDestroy {
  @Output() detailRecord: EventEmitter<Object> = new EventEmitter()
  isAlls: Array<any>
  isVods: Array<any>
  isChannels: Array<any>
  isPrograms: Array<any>

  noAllData: {
    'dataList': Array<any>,
    'row': string,
    'clientW': number,
    'suspensionID': string
  }
  noVodData: {
    'dataList': Array<any>,
    'row': string,
    'clientW': number,
    'suspensionID': string
  }
  noChannelData: {
    'dataList': Array<any>,
    'row': string,
    'clientW': number,
    'suspensionID': string
  }
  noProgramData: {
    'dataList': Array<any>,
    'row': string,
    'clientW': number,
    'suspensionID': string
  }
  noDataList: {
    'dataList': Array<any>,
    'row': string,
    'clientW': number,
    'suspensionID': string
  }
  noDataLists: {
    'dataList': Array<any>,
    'row': string,
    'clientW': number,
    'suspensionID': string
  }
    /**
     * have no data of search result
     */
  searchNoData: boolean
    /**
     * have data of search result
     */
  searchData: boolean
    /**
     * list of search result
     */
  dataList: Array<any>
  type: Array<string>
  vodSubjectId: string
  KaraokSubjectId: string
  isLogin = false
    /**
     * show VOD suspension box
     */
  public isShow = 'false'
    /**
     * show program suspension box
     */
  public hotShow = 'false'
    /**
     * show channel suspension box
     */
  public channelShow = 'false'
  public searchValue: string
  public illegals

  @ViewChild(SuspensionComponentVod) suspension: SuspensionComponentVod
  @ViewChild(SuspensionComponentProgram) suspensionprogram: SuspensionComponentProgram
  @ViewChild(SuspensionComponentChannel) suspensionchannel: SuspensionComponentChannel
  @ViewChild(RecordSuspensionComponent) recordSuspension: RecordSuspensionComponent
  private id: string
  private searchBoxID: string
    /**
     * the current search type that gets the focus
     */
  private isFocus = 'all'
  private susTime
  private susTimeChannel
  private susTimeProgram
  private moveEnterIndex
  private vodDataLength
  private channelDataLength
  private programDataLength
  private susFilter = _.debounce((fc: Function) => { fc() }, 400)
  private susUse = true

  constructor (
        private router: Router,
        private vodRouter: VODRouterService,
        private route: ActivatedRoute,
        private searchService: SearchService,
        private liveTvService: LiveTvService,
        private translate: TranslateService,
        private playbillAppService: PlaybillAppService,
        private commonService: CommonService
    ) { }

  ngOnInit () {
    this.vodSubjectId = session.get('vodSubjectId')
    this.KaraokSubjectId = session.get('vodKaraokSubjectId')
    this.route
        .params
        .subscribe(params => {
          document.body.scrollTop = 0
          document.documentElement.scrollTop = 0
          this.isLogin = !!Cookies.getJSON('IS_PROFILE_LOGIN')
          this.isAlls = []
          this.isVods = []
          this.isChannels = []
          this.isPrograms = []
          this.noAllData = undefined
          this.noVodData = undefined
          this.noChannelData = undefined
          this.noProgramData = undefined
          this.id = params['id']
          this.searchBoxID = params['id']
          this.id = this.trimRight(this.id)

          if (this.id.length > 0) {
            this.searchValue = `"${this.id}"`
          }

          this.isFocus = session.get('lastFocus')
          this.showVod()
        })
      // to prevent the hotkey turn default hotkey when refresh page
    EventService.emit('searchAllId', this.searchBoxID)
      // monitor screen change
    this.isSearchIn()
      // when mouse slide down will show more
    EventService.removeAllListeners(['LOAD_DATA'])
    EventService.on('LOAD_DATA', () => {
        // when the search result page have data to do something
      if (this.dataList && this.dataList.length > 0 && !this.noDataList) {
        let offset = this.dataList.length.toString()
        let pOffset = this.dataList.length
        let KaraokSubjectId
        if (this.type[0] === 'CHANNEL' || this.type[0] === 'PROGRAM') {
            KaraokSubjectId = ''
          } else {
            KaraokSubjectId = this.KaraokSubjectId
          }
        this.focusOnSearchResult(pOffset, offset, KaraokSubjectId)
      }
    })
    EventService.on('CLOSED_LOADING_SPINNER', illegals => {
      if (document.getElementsByClassName('spinner')[0]) {
        document.getElementsByClassName('searchAll-filter-load')[0]['style']['display'] = 'none'
        this.illegals = illegals
        if (this.illegals) {
            this.searchNoData = false
            let clientW: number = document.body.clientWidth
            this.searchService.queryRecmVODList().then((resp2) => {
              this.noDataList = {
                'dataList': resp2,
                'row': '1',
                'clientW': clientW,
                'suspensionID': 'noData'
              }
              this.noDataLists = this.noDataList
              this.dataList = []
            })
          } else {

          }
      }
    })
  }

  public onSuspensionClick (event): void {
    this.vodRouter.navigate(event.vodId)
  }

    /**
     * when focusing on the search type, displays the corresponding search results.
     */
  focusOnSearchResult (pOffset, offset, KaraokSubjectId) {
    let dataLength = this.getSearchResultLength()
    if (dataLength > 30) {
      document.querySelector('.searchAll-filter-load')['style']['display'] = 'block'
      this.searchService.searchContent({
        'id': this.id,
        'type': this.type,
        'offset': offset,
        'KaraokSubjectId': KaraokSubjectId
      }).then((resp) => {
          let data = this.dataList
          if (resp.contents) {
            this.vodDataLength = resp.contents.length
            this.channelDataLength = resp.contents.length
            this.programDataLength = resp.contents.length
            let molist = this.searchService.showSearchResult(resp.contents.slice(0, 30))
            let moredata = data.concat(molist)
            this.dataList = moredata
          }
          document.querySelector('.searchAll-filter-load')['style']['display'] = 'none'
        })
    }
  }

    /**
     * get length of search result
     */
  getSearchResultLength () {
    let dataLength
    if (this.isFocus === 'vod') {
      dataLength = this.vodDataLength
    } else if (this.isFocus === 'channel') {
      dataLength = this.channelDataLength
    } else if (this.isFocus === 'program') {
        dataLength = this.programDataLength
      }
    return dataLength
  }

    /**
     * trim the right space of the string
     */
  trimRight (s) {
    return s.replace(/^\s+|\s+$/g, '')
  }

    /**
     * leave this page, destroy timer
     */
  ngOnDestroy () {
    session.put('lastFocus', this.isFocus)
    session.put('isSearch', '')
    if (this.susTime) {
      clearTimeout(this.susTime)
    }
    if (this.susTimeChannel) {
      clearTimeout(this.susTimeChannel)
    }
    if (this.susTimeProgram) {
      clearTimeout(this.susTimeProgram)
    }
    EventService.removeAllListeners(['LOAD_DATA'])
  }

    /**
     * set style of the playbill
     */
  programType (data) {
    let classes = {
      before: data === 'before',
      future: data === 'future',
      now: data === 'now'
    }
    return classes
  }

    /**
     * when the focus on [On Demand], this function will be run
     */
  showVod () {
    let clientW: number = document.body.clientWidth
    this.isFocus = 'vod'
    this.searchNoData = false
    this.searchData = false
    this.type = ['VOD']
    if (_.isUndefined(this.isVods) || this.isVods.length === 0) {
      if (!_.isUndefined(this.noVodData)) {
        this.illegals = false
        this.searchNoData = true
        this.searchData = false
        this.noVodData['clientW'] = clientW
        this.noDataList = this.noVodData
        this.noDataLists = this.noDataList
        this.dataList = []
      } else {
        this.dataList = []
        document.querySelector('.searchAll-filter-load')['style']['display'] = 'block'
        this.searchService.searchContent({
            'id': this.id,
            'type': this.type,
            'offset': '0',
            'KaraokSubjectId': this.KaraokSubjectId
          }).then((resp) => {
            if (_.isUndefined(resp['contents']) || resp['contents'].length === 0) {
              this.illegals = false
              this.searchNoData = true
              this.searchData = false
              this.searchService.queryRecmVODList().then((resp2) => {
                this.noVodData = {
                  'dataList': resp2,
                  'row': '1',
                  'clientW': clientW,
                  'suspensionID': 'noData'
                }
                this.noDataList = this.noVodData
                this.noDataLists = this.noDataList
                this.dataList = []
              })
            } else {
              this.illegals = false
              if (this.isFocus === 'vod') {
                this.searchNoData = false
                this.searchData = true
                this.isVods = resp.contents
                this.vodDataLength = resp.contents.length
                this.dataList = this.searchService.showSearchResult(this.isVods).slice(0, 30)
                this.noDataList = undefined
              }
            }
            document.querySelector('.searchAll-filter-load')['style']['display'] = 'none'
          })
      }
    } else {
      this.vodDataLength = this.isVods.length
      this.searchData = true
      this.dataList = this.searchService.showSearchResult(this.isVods).slice(0, 30)
      this.noDataList = undefined
      if (document.querySelector('.searchAll-filter-load') &&
                document.querySelector('.searchAll-filter-load')['style']['display'] === 'block') {
        document.querySelector('.searchAll-filter-load')['style']['display'] = 'none'
      }
    }
  }

    /**
     * when you click VOD where is in [On Demand] page
     */
  vodDtail (data) {
    this.vodRouter.navigate(data.vodId)
  }

    /**
     * when you click VOD where is in [Channels] page
     */
  channelDtail (data) {
    session.put('lastFocus', this.isFocus)
    EventService.emit('PLAYCHANNEL', data.channelId)
  }

    /**
     * when you click VOD where is in [Program] page
     */
  programDtail (data) {
    if (data.timeState === 'now') {
      EventService.emit('PLAYCHANNEL', data['floatInfo'].channel.ID)
    } else {
      session.put('lastFocus', this.isFocus)
      this.router.navigate(['live-tv/live-tv-detail', data.programId, data.endTime])
    }
  }

    /**
     * show VOD suspension box
     */
  mouseenter (option, event) {
    this.susUse = true
    this.susFilter(() => {
      if (this.susUse === false) {
        return
      }
      this.moveEnterIndex = undefined
      this.isShow = 'false'
      clearTimeout(this.susTime)
      let time1 = new Date().getTime()
      let currentTarget = window.event && window.event.currentTarget['offsetParent'] || event && event.target['offsetParent']
      EventService.removeAllListeners(['LOADDATASUCCESS'])
      EventService.on('LOADDATASUCCESS', () => {
        this.commonService.moveEnter(currentTarget, 'suspensionSearchInfo', document.body.clientWidth, '', 'searchAll')
        let time2 = new Date().getTime()
        if ((time2 - time1) < 500) {
            let time = 500 - (time2 - time1) + 100
            this.susTime = setTimeout(() => {
              this.isShow = 'true'
            }, time)
          } else {
            this.susTime = setTimeout(() => {
              this.isShow = 'true'
            }, 100)
          }
      })
      this.suspension.setDetail(option, 'suspensionSearchInfo', currentTarget.firstElementChild)
    })
  }

    /**
     * close VOD suspension box
     */
  closeDialog () {
    this.susUse = false
    clearTimeout(this.susTime)
    EventService.removeAllListeners(['showSuspension'])
    EventService.on('showSuspension', option => {
      this.isShow = 'true'
    })
    EventService.removeAllListeners(['closeSuspension'])
    EventService.on('closeSuspension', () => {
      this.isShow = 'false'
      clearTimeout(this.susTime)
    })
    this.isShow = 'false'
    EventService.removeAllListeners(['LOADDATASUCCESS'])
    EventService.removeAllListeners(['REMOVE_SUSDATE_successful'])
    EventService.removeAllListeners(['ADD_SUSDATA_successful'])
  }

    /**
     * show Program suspension box
     */
  mouseenterProgram (option, event) {
    this.susUse = true
    this.susFilter(() => {
      if (this.susUse === false) {
        return
      }
      this.moveEnterIndex = undefined
      this.hotShow = 'false'
      let time1 = new Date().getTime()
      let currentTarget = window.event && window.event.currentTarget['offsetParent'] || event && event.target['offsetParent']
      let left
      if (document.body.clientWidth <= 1440) {
        left = currentTarget.offsetLeft % 980
      } else {
        left = currentTarget.offsetLeft % 1280
      }
      EventService.removeAllListeners(['LOADDATAPROGRAMSUCCESS'])
      EventService.on('LOADDATAPROGRAMSUCCESS', () => {
        this.commonService.moveEnter(currentTarget, 'suspensionSearchProgram', document.body.clientWidth, 'program')
        let time2 = new Date().getTime()
        if ((time2 - time1) < 500) {
            let time = 500 - (time2 - time1) + 100
            clearTimeout(this.susTimeProgram)
            this.susTimeProgram = setTimeout(() => {
              this.hotShow = 'true'
            }, time)
          } else {
            clearTimeout(this.susTimeProgram)
            this.susTimeProgram = setTimeout(() => {
              this.hotShow = 'true'
            }, 100)
          }
      })
      this.suspensionprogram.setDetail(option, 'suspensionSearchProgram', currentTarget.firstElementChild)
    })
  }

    /**
     * close Program suspension box
     */
  closeDialogProgram () {
    this.susUse = false
    session.pop('ID')
    clearTimeout(this.susTimeProgram)
    EventService.removeAllListeners(['LOADDATAPROGRAMSUCCESS'])
    EventService.removeAllListeners(['showProgramSuspension'])
    EventService.on('showProgramSuspension', option => {
      this.hotShow = 'true'
    })
    EventService.removeAllListeners(['closeProgramSuspension'])
    EventService.on('closeProgramSuspension', () => {
      this.hotShow = 'false'
      clearTimeout(this.susTimeProgram)
    })
    this.hotShow = 'false'
  }

    /**
     * show Channel suspension box
     */
  mouseenterChannel (option, event) {
    this.susUse = true
    this.susFilter(() => {
      let self = this
      if (this.susUse === false) {
        return
      }
      this.moveEnterIndex = undefined
      this.channelShow = 'false'
      let currentTarget = window.event && window.event.currentTarget['offsetParent'] || event && event.target['offsetParent']
      let time1 = new Date().getTime()
      EventService.removeAllListeners(['LOADDATACHANNELSUCCESS'])
      EventService.on('LOADDATACHANNELSUCCESS', () => {
        this.commonService.moveEnter(currentTarget, 'suspensionSearchChannel', document.body.clientWidth, 'myChannel')
        let time2 = new Date().getTime()
        if ((time2 - time1) < 500) {
            let time = 500 - (time2 - time1) + 100
            clearTimeout(this.susTimeChannel)
            this.susTimeChannel = setTimeout(() => {
              this.channelShow = 'true'
            }, time)
          } else {
            clearTimeout(this.susTimeChannel)
            this.susTimeChannel = setTimeout(() => {
              this.channelShow = 'true'
            }, 100)
          }
      })
      this.suspensionchannel.setDetail(option, 'suspensionSearchChannel')
    })
  }

    /**
     * close Channel suspension box
     */
  closeDialogChannel () {
    let self = this
    this.susUse = false
    session.pop('mouseenterChannelID')
    clearTimeout(this.susTimeChannel)
    EventService.removeAllListeners(['showChannelSuspension'])
    EventService.on('showChannelSuspension', option => {
      this.channelShow = 'true'
    })
    EventService.removeAllListeners(['closeChannelSuspension'])
    EventService.on('closeChannelSuspension', () => {
      this.channelShow = 'false'
      clearTimeout(this.susTimeChannel)
    })
    this.channelShow = 'false'
  }

    /**
     * get document element by element id
     */
  dom (id) {
    return document.getElementById(id)
  }

  isSearchIn () {
    EventService.emit('SEARCH_LOCATION_HASH_KEY')
  }
}
