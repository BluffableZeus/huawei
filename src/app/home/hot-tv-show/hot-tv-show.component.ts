import { Component, OnInit, Input, ViewChild } from '@angular/core'
import { SuspensionComponentProgram } from 'ng-epg-ui/webtv-components/programSuspension/programSuspension.component'
import { HotTvService } from './hot-tv-show.service'
import { Router } from '@angular/router'

// @Component({
//     selector: 'app-hot-tv',
//     styleUrls: ['./hot-tv-show.component.scss'],
//     templateUrl: './hot-tv-show.component.html'
// })
export class HotTvShowComponent implements OnInit {
    // declare variables.
  @ViewChild(SuspensionComponentProgram) suspensionprogram: SuspensionComponentProgram
    // true is there has returned the data.
  public hotTvShowdata = false
    // keep the data after dealing with.
  public tvodLists = []
    // the title for show more programme.
  private isOne = 'hot_tv_show'
    // set the flag.
  private hotTvId = ['suspensionHotTv', 'hotTv']
    // keep data.
  private hotTv
    // private constructor
  constructor (
        private route: Router,
        private hotTvService: HotTvService
    ) { }
    // get the data of recommend
  @Input() set setHotTv (hotTv) {
    this.hotTv = hotTv
      // deal data
    this.tvodLists = this.hotTvService.queryHotTVList(this.hotTv)
  }
  ngOnInit () {
    this.hotTvId = ['suspensionHotTv', 'hotTv']
  }
    // turn to more programs
  moreShow (data) {
    this.route.navigate(['home', this.isOne])
  }
    // click postor or playbill name turn to the detail page
  onClickDetail (jumpdata) {
    this.route.navigate(['live-tv', 'live-tv-detail', jumpdata[0], jumpdata[1]])
  }
}
