import * as _ from 'underscore'
import { Injectable } from '@angular/core'
import { PictureService } from 'ng-epg-ui/services/picture.service'
import { TranslateService } from '@ngx-translate/core'
import { DateUtils } from 'ng-epg-sdk/utils'
import { CommonService } from '../../core/common.service'

@Injectable()
export class HotTvService {
  constructor (private pictureService: PictureService,
                private translate: TranslateService,
                private commonService: CommonService) { }
  // deal with the data.
  queryHotTVList (hotTv) {
    let reclist = _.map(hotTv, (list, index) => {
      let lens = ''
      let url = list['picture'] && list['picture'].posters && this.pictureService.convertToSizeUrl(list['picture'].posters[0],
        { minwidth: 313, minheight: 177, maxwidth: 330, maxheight: 186 })
      let currentTime = Date.now()['getTime']()
      if (parseInt(list['startTime'], 10) < currentTime && currentTime < parseInt(list['endTime'], 10)) {
        let currentDuration = currentTime - list['startTime']
        let channelTime = list['endTime'] - list['startTime']
        lens = Number(currentDuration / channelTime * 100).toFixed(4) + '%'
      }
      let channelDet = list['channel']
      let starTimeForDate = this.commonService.dealWithDateJson('D10', list['startTime'])
      return {
        id: list['ID'],
        name: list['name'],
        posterUrl: url,
        startTime: DateUtils.format(list['startTime'], 'HH:mm'),
        startTimes: list['startTime'],
        starTimeForDate: starTimeForDate,
        endTime: list['endTime'],
        channel: channelDet ? (' | ' + channelDet.name) : '',
        floatInfo: list,
        floatflag: 'hotTv',
        len: lens
      }
    })
    let tvodLists = []
    tvodLists[0] = reclist.slice(0, 16)
    _.map(tvodLists, (list1, index) => {
      list1['name'] = 'hot_tv_show'
    })
    return tvodLists
  }
}
