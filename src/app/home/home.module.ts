import { SearchService } from './searchAll/searchAll.services'
import { Title } from './title/title.service'
import { HomeService } from './home.service'
import { ChannelSuspensionModule } from 'ng-epg-ui/webtv-components/channelSuspension/channel-suspension.module'
import { ProgramSuspensionModule } from 'ng-epg-ui/webtv-components/programSuspension/program-suspension.module'
import { RecordSuspensionModule } from 'ng-epg-ui/webtv-components/recordSuspension/record-suspension.module'
import { WaterfallVodModule } from 'ng-epg-ui/webtv-components/waterfall-vod/waterfall-vod.module'
import { HotTvService } from './hot-tv-show/hot-tv-show.service'
import { ProgramComponentModule } from 'ng-epg-ui/webtv-components/programComponent/program-component.module'
import { BannerScroll } from './banner/bannerScroll.service'
import { BannerService } from './banner/banner.service'
import { SharedModule } from '../shared/shared.module'
import { NoDataModule } from 'ng-epg-ui/webtv-components/noData/no-data.module'
import { VODSuspensionModule } from 'ng-epg-ui/webtv-components/vodSuspension/vod-suspension.module'
import { OscarVodMoreComponent } from './oscar-all/oscar-all.component'
import { SearchAllComponent } from './searchAll/searchAll.component'
import { HomeMoreComponent } from './home-more/home-more.component'
import { NgModule } from '@angular/core'

import { WaterfallChannelModule } from 'ng-epg-ui/webtv-components/waterfall-channel'
import { ChildColumnShowModule } from 'ng-epg-ui/webtv-components/childcolumnshow'

import { HomeComponent } from './home.component'
import { HomeRoutingModule } from './home.routing.module'
import { HomeSectionsComponent } from './sections/sections.component'
import { TranslateModule } from '@ngx-translate/core'

import { ContinueWatchingComponent } from './continue-watching/continue-watching.component'
import { SectionService } from './sections/section.service'

@NgModule({
  imports: [
    WaterfallChannelModule,
    VODSuspensionModule,
    NoDataModule,
    ProgramComponentModule,
    // ChildColumnShowModule,
    WaterfallVodModule,
    ProgramSuspensionModule,
    ChannelSuspensionModule,
    RecordSuspensionModule,
    SharedModule,
    TranslateModule,
    HomeRoutingModule
  
  ],
  exports: [],
  declarations: [
    HomeComponent,
    HomeMoreComponent,
    SearchAllComponent,
    OscarVodMoreComponent,
    HomeSectionsComponent,
    // ContinueWatchingComponent
  ],
  providers: [
    BannerService,
    BannerScroll,
    HotTvService,
    HomeService,
    Title,
    SearchService,
    SectionService
  ]
})
export class HomeModule { }
