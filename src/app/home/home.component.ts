import * as _ from 'underscore'
import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core'
import { Title } from './title'
import { EventService } from 'ng-epg-sdk/services'
import { session } from 'src/app/shared/services/session'
import { HomeService } from './home.service'
import { CustomConfigService } from '../shared/services/custom-config.service'
import { CommonService } from '../core/common.service'
import { config } from '../shared/services/config'
import { QueryHomeDataResponse } from 'ng-epg-sdk'
import { VOD } from 'ng-epg-sdk/vsp/api'
import { socialAPI } from 'oper-center/api/social'
import { AuthService } from '../shared/services/auth.service'
import { Store, select } from '@ngrx/store'
import { Observable } from 'rxjs'

import {
  APP_HOME_LOAD_START,
  APP_HOME_LOAD_END,
} from '../store/app'

import {
  HOME_DATA,
} from '../store/home'

const pageTracker = require('../../assets/lib/PageTracker')

@Component({
// The selector is what angular internally uses
// for `document.querySelectorAll(selector)` in our index.html
// where, in this case, selector is the string 'home'
  selector: 'app-home',
// Our list of styles in our component. We may add more to compose many styles together
  styleUrls: ['./home.style.scss'],
// Every Angular template is first compiled by the browser before Angular runs it's compiler
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit, OnDestroy, AfterViewInit {

  homeData$: Observable<any>

  // DEPRECATED
  public homeData: QueryHomeDataResponse
  // DEPRECATED
  public bannerData: VOD[] = []

  public isAuth: boolean = Cookies.getJSON('IS_PROFILE_LOGIN') || false;
  public loading = false

  localState = { value: '' }
  private customizeConfig = {}
    // Set our default values
    // TypeScript public modifiers
  constructor (
        public title: Title,
        private homeService: HomeService,
        private customConfigService: CustomConfigService,
        private commonService: CommonService,
        private socialApi: socialAPI,
        private authService: AuthService,
        private store: Store<any>
    ) {
    this.store.dispatch({ type: APP_HOME_LOAD_START })
    this.homeData$ = store.pipe(select('home'))
  }

  ngOnInit () {

    this.store.dispatch({ type: HOME_DATA })

    EventService.on('LOGIN', () => {
      console.log('home', Cookies.getJSON('IS_PROFILE_LOGIN'))
      if (Cookies.getJSON('IS_PROFILE_LOGIN')) {
          // if the user logins,show the history adn favorite.
        this.isAuth = true;
      } else {
          // if the user doesn't login,hide the history and favorite.
        this.isAuth = false;
      }
    })
  }

  ngAfterViewInit() {
    this.store.dispatch({ type: APP_HOME_LOAD_END })
  }

  ngOnDestroy () {
    EventService.removeAllListeners(['CUSTOM_CACHE_CONFIG_DATAS'])
  }

  submitState (value) {
    this.localState.value = ''
  }

    /**
     * DEPRECATED. USE app/effects/home.ts
     * get all home data
     */
  getData () {
    this.loading = true
      // document.querySelector('.home-filter-load')['style']['display'] = 'block';
    this.customConfigService.getCustomizeConfig({ queryType: '0', key: 'banner_subject_id,special_cluster_id,vod_subject_id' })
        .then((resp) => {
          // get the custom config params
          this.customizeConfig = resp['list']
          this.configMSALog(this.customizeConfig)
          session.put('vodSubjectId', this.customizeConfig['vod_subject_id'])
          session.put('vodBannerID', this.customizeConfig['banner_subject_id'])
          let KaraokSubjectId = this.customizeConfig['karaok_subject_id'] || ''
          session.put('vodKaraokSubjectId', KaraokSubjectId)
          return this.homeService.queryHomeData({
            recmDataCount: '30',
            bannerSubjectID: this.customizeConfig['banner_subject_id'],
            bannerDataCount: '5',
            subjectID: this.customizeConfig['special_cluster_id'],
            subjectDataCount: '30',
            hotTVCount: '30'
          }).then(reg => {
            this.homeLogInfo(reg)
            // all the data
            this.homeData = reg
            this.bannerData = this.homeData['banner']
            // // the hot key word for search page
            if (this.homeData['hotKeyword']) {
              EventService.emit('HOTKEY_WORD', this.homeData['hotKeyword'])
            } else {
              EventService.emit('HOTKEY_WORD', '')
            }

            this.loading = false
            // if (document.querySelector('.home-filter-load') && document.querySelector('.home-filter-load')['style']) {
            //     document.querySelector('.home-filter-load')['style']['display'] = 'none';
            // }
            let popupLoginDialog = session.get('RELOGIN_POP_UP_DIALOG')
            if (popupLoginDialog) {
              EventService.emit('SHOW_LOGIN_DIALOG')
              session.remove('RELOGIN_POP_UP_DIALOG')
            }
          })
        })
  }

    /**
     * if the following value is undefined or empty, it will log related information.
     * special_cluster_id, banner_subject_id
     */
  private configMSALog (customizeConfig) {
    let special_cluster_id = customizeConfig['special_cluster_id']
    if (_.isUndefined(special_cluster_id) || special_cluster_id === '' || special_cluster_id === null) {
      this.commonService.MSAErrorLog(38062, 'QueryCustomizeConfig')
    }
    let banner_subject_id = customizeConfig['banner_subject_id']
    if (_.isUndefined(banner_subject_id) || banner_subject_id === '' || banner_subject_id === null) {
      this.commonService.MSAErrorLog(38077, 'QueryCustomizeConfig')
    }
  }

    /**
     * if any of the VOD list is missing or empty, it will log the related information.
     */
  private homeLogInfo (reg) {
    if (_.isUndefined(reg['banner']) || reg['banner'].length === 0) {
      this.commonService.MSAErrorLog(38033, 'QueryHomeData', 'banner')
    }
    if (_.isUndefined(reg['everyoneIsWatching']) || reg['everyoneIsWatching'].length === 0) {
      this.commonService.MSAErrorLog(38033, 'QueryHomeData', 'everyoneIsWatching')
    }
    if (_.isUndefined(reg['topPicksForYou']) || reg['topPicksForYou'].length === 0) {
      this.commonService.MSAErrorLog(38033, 'QueryHomeData', 'topPicksForYou')
    }
    if (_.isUndefined(reg['hotTVShow']) || reg['hotTVShow'].length === 0) {
      this.commonService.MSAErrorLog(38031, 'QueryHomeData', 'hotTVShow')
    }
    this.noRecSubject(reg)
  }

  private noRecSubject (reg) {
    if (_.isUndefined(reg['recSubject']) || reg['recSubject'].length === 0) {
      this.commonService.commonLog('QueryHomeData', 'recSubject')
    } else {
      let recSubjectLengthZero = _.find(reg['recSubject'], item => {
        return item['VODList'].length === 0
      })
      if (!_.isUndefined(recSubjectLengthZero)) {
        this.commonService.MSAErrorLog(38033, 'QueryHomeData', 'recSubject')
      }
    }
  }

    /**
     * Checks if there is an available list of the received content.
     */
  private isAvailableContent (content) {
    return content && content.length > 0
  }
}
