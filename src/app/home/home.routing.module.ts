import { RouterModule } from '@angular/router'
import { NgModule } from '@angular/core'

import { HomeComponent } from './home.component'
import { HomeMoreComponent } from './home-more/home-more.component'
import { OscarVodMoreComponent } from './oscar-all/oscar-all.component'
import { SearchAllComponent } from './searchAll/searchAll.component'
import { AuthGuard } from '../shared/services/auth.guard'
/*
* where is the route configuration
* import the url after jump and configuration to [routes]
*/

@NgModule({
  imports: [
    RouterModule.forChild([
      { path: '', component: HomeComponent, canActivate: [AuthGuard] },
      { path: 'magic/:id', component: HomeMoreComponent, canActivate: [AuthGuard] },
      { path: 'search/:id', component: SearchAllComponent, canActivate: [AuthGuard] }
    ])
  ],
  exports: [
    RouterModule
  ]
})
export class HomeRoutingModule { }
