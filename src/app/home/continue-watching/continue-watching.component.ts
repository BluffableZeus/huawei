import * as _ from 'underscore'
import { Component, OnInit } from '@angular/core'
import { EventService } from 'ng-epg-sdk/services'
import { queryBookmark } from 'ng-epg-sdk/vsp'
import { PictureService } from 'ng-epg-ui/services'
import { SectionService } from '../sections/section.service'
import { OnChildEvent } from 'ng-epg-ui/webtv-components/childcolumnshow'
import { VODRouterService } from 'src/app/shared/services/vod-router.service'
import { Observable, of } from 'rxjs'
import { Router } from '@angular/router'
@Component({
  selector: 'app-continue-watching',
  templateUrl: './continue-watching.component.html',
  styleUrls: ['./continue-watching.component.scss']
})
export class ContinueWatchingComponent implements OnInit {

  public list: any = []
  public listCount: Observable<any> = of([])

  public isAuth: boolean = Cookies.getJSON('IS_PROFILE_LOGIN') || false
  public historyKeys = []
  public dataDisplay: boolean
  public noDataDisplay: boolean
  public moreDisplay: boolean
  public historyVOD: any = {}
  public historyVODs = []
  public hasLeave = false
  public finishDelete = true

  constructor (
    private pictureService: PictureService,
    private sectionService: SectionService,
    private vodRouter: VODRouterService,
    private router: Router
  ) { }

  ngOnInit () {

    EventService.on('LOGIN',() => {
      if (Cookies.getJSON('IS_PROFILE_LOGIN')) {
          // if the user logins,show the history buttom.
        this.isAuth = true
        this.getHistory()
      } else {
          // if the user doesn't login,hide the history buttom.
        this.isAuth = false
      }
    })
    if (this.isAuth) { this.getHistory() }
    
  }

  getHistory () {
    queryBookmark({
      bookmarkTypes: ['VOD'],
      count: '50',
      offset: '0'
    }).then(resp => {

        // only when there returns values and the mouse does't leave,show the list.
      if (resp && resp['bookmarks'].length > 0) {
        this.historyVODs = resp.bookmarks
        this.historyVODs.forEach((item, index) => {
          this.historyVODs[index] = item.VOD
          if (item.VOD.picture && item.VOD.picture.posters && item.VOD.picture.posters[0]) {
            let url = this.pictureService.convertToSizeUrl(item.VOD.picture.posters[0],
              this.sectionService.getImageSize())
            item.VOD.picture.posters[0] = url || 'assets/img/default/home_poster.png'
          }
          item.VOD['videoViewBar'] = (item.VOD.mediaFiles && item.VOD.mediaFiles.length) ? (item.VOD.bookmark.rangeTime * 100) / item.VOD.mediaFiles[0].elapseTime : 0
        })
        this.list.push(this.sectionService.processContinueWatching(this.historyVODs))
        this.listCount = of(this.historyVODs)
      } else {
        this.historyVODs = []
      }
    }
    )
  }

  /**
   *  turn to vod detail page.
   */
  onClickDetail (event: OnChildEvent) {
    const [vodId] = event
    this.vodRouter.navigate(vodId)
  }
  /**
   *  turn to more movies page.
   */
  moreShow () {
    this.router.navigate(['profile', 'historyVod'])
  }
}
