import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'

@Injectable()
export class Title {
  // variable about value.
  value = 'Angular 2'
  constructor (public http: HttpClient) {

  }
  /**
   * get data of value.
   */
  getData () {
    return {
      value: 'AngularClass'
    }
  }
}
