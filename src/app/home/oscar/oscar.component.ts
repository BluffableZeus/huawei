import * as _ from 'underscore'
import { Component, OnInit, Input } from '@angular/core'
import { PictureService } from 'ng-epg-ui/services/picture.service'
import { Router } from '@angular/router'
import { VODRouterService } from '../../shared/services/vod-router.service'

// @Component({
//     selector: 'app-oscar',
//     templateUrl: './oscar.component.html',
//     styleUrls: ['./oscar.component.scss']
// })

export class OscarComponent implements OnInit {
    // check whether has data.
  public hasOscarData = false
    // check whether the recommended VOD column is existed.
  public hasOscar: boolean
    // save the value of offset of VOD posters.
  private offsetNumber = []
    // save data for VOD suspension.
  private vodListsuspension = []
    // save data of recommended VOD columns.
  private datas: Array<any>
    // save ID information for UI component.
  private oscarId
    // save flag information for UI component.
  private oscarShow = [true, true, true]
    // save data from parent component.
  private oscarVod

  constructor (
        private route: Router,
        private vodRouter: VODRouterService,
        private pictureService: PictureService
    ) { }

    /**
     *  lifecycle function 'Input'.
     */
  @Input() set setOscarVod (oscarVod) {
    this.oscarVod = oscarVod
    this.vodListResult()
    let self = this
    if (oscarVod) {
      for (let i = 0; i < self.oscarVod.length; i++) {
        this.offsetNumber.push(0)
      }
    }
  }
    /**
     *  lifecycle function 'ngOnInit'.
     */
  ngOnInit () {
    this.oscarId = ['oscarVodSuspension', 'oscarVodList', 'oscarVodLeft', 'oscarVodRight']
    this.oscarShow = [true, true, true]
  }
    /**
     *  handle VOD datas for showing.
     */
  vodListResult () {
    this.datas = this.oscarVod
    this.datas = _.map(this.datas, (list, index) => {
      if (list && list['VODList'] && list['subject']) {
        return {
            VODList: list['VODList'].slice(0, 24),
            subjectName: list['subject'].name,
            subjectID: list['subject'].ID,
            hasChildren: list['subject'].hasChildren
          }
      }
    })
    _.map(this.datas, (lists, index) => {
      _.map(lists['VODList'], (list, index1) => {
        if (list) {
            this.score(list)
            if (list['picture'] && list['picture'].posters) {
              let url = list['picture'] && list['picture'].posters &&
                            this.pictureService.convertToSizeUrl(list['picture'].posters[0],
                              { minwidth: 180, minheight: 240, maxwidth: 214, maxheight: 284 })
              list['picture'].posters = []
              list['picture'].posters[0] = url || 'assets/img/default/home_poster.png'
            }
          }
      })
    })
    if (this.oscarVod && this.oscarVod.length) {
      for (let i = 0; i < this.oscarVod.length; i++) {
        if (this.oscarVod && this.oscarVod[i] && this.oscarVod[i].VODList) {
            this.vodListsuspension[i] = _.map(this.oscarVod[i].VODList, (list, index) => {
              list['focusRoute'] = 'home'
              return {
                titleName: this.oscarVod[i].subject.name,
                floatInfo: list
              }
            })
          }
      }
    }
    if (this.datas && this.datas.length > 0) {
      this.hasOscar = true
    } else {
      this.hasOscar = false
    }
  }

    /**
     *  turn to more movies page.
     */
  moreShow (datas) {
    this.route.navigate(['category', datas[0]])
  }

    /**
     *  turn to vod detail page.
     */
  onClickDetail (event) {
    this.vodRouter.navigate(event[0])
  }
    /**
     *  handle average score of VOD.
     */
  score (list) {
    if (list.averageScore) {
      if (list.averageScore.length === 1) {
        list.averageScore += '.0'
      } else {
        if (list.averageScore.substring(0, 2) === '10') {
            list.averageScore = '10'
          } else {
            let score = list.averageScore
            list.averageScore = score.substring(0, 4)
            list.averageScore = Number(list.averageScore)
            list.averageScore = list.averageScore.toFixed(1)
          }
      }
    } else {
      list.averageScore = '0.0'
    }
    return list
  }
}
