import { Component, OnInit } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { VODRouterService } from '../../shared/services/vod-router.service'
import { HomeMoreService } from './home-more.service'
import { PictureService } from 'ng-epg-ui/services/picture.service'
import { session } from 'src/app/shared/services/session'

@Component({
  selector: 'app-home-more',
  templateUrl: './home-more.component.html',
  styleUrls: ['./home-more.component.scss'],
  providers: [HomeMoreService]
})

export class HomeMoreComponent implements OnInit {
    // save value of column key.
  public id = ''
    // save object of VOD information.
  moreVodList: {
    'dataList': Array<any>,
    'row': string,
    'suspensionID': string
  }
    // save object of Channel information.
  moreChannelList: {
    'dataList': Array<any>,
    'row': string,
    'suspensionID': string
  }
    // save title of VOD or Channel waterfall page.
  public titles: Array<string> = []

  constructor (
        private route: ActivatedRoute,
        private router: Router,
        private vodRouter: VODRouterService,
        private homeMoreService: HomeMoreService,
        private pictureService: PictureService) {
  }
    /**
     * lifecycle function 'ngOnInit'.
     * handle with route information and get related data.
     */
  ngOnInit () {
    document.body.scrollTop = 0
    document.documentElement.scrollTop = 0
    this.route
        .params
        .subscribe(params => {
          this.id = params['id']
          if (this.id === 'every_one_is_watching') {
            this.titles.push('every_one_is_watching')
            this.showRecmVodMore()
          } else if (this.id === 'top_picks_for_you') {
            this.titles.push('top_picks_for_you')
            this.showRecmListMore()
          } else if (this.id === 'top_this_week') {
            this.titles.push('top_this_week')
            this.showTopWeek()
          } else if (this.id === 'hot_tv_show') {
            this.titles.push('hot_tv_show')
            this.showHotVodMore()
          }
        })
  }
  showTopWeek () {
    this.homeMoreService.recmTopWeek().then((resp) => {
      this.moreVodList = {
        'dataList': resp,
        'row': 'All',
        'suspensionID': 'moreData'
      }
    })
  }
    /**
     * get VOD list that belong to column 'every one is watching'.
     */
  showRecmVodMore () {
    this.homeMoreService.recmVodMore().then((resp) => {
      this.moreVodList = {
        'dataList': resp,
        'row': 'All',
        'suspensionID': 'moreData'
      }
    })
  }
    /**
     * get VOD list that belong to column 'top picks for you'.
     */
  showRecmListMore () {
    this.homeMoreService.recmListMore().then((resp) => {
      this.moreVodList = {
        'dataList': resp,
        'row': 'All',
        'suspensionID': 'moreData'
      }
    })
  }
    /**
     * get VOD list that belong to column 'hot tv show'.
     */
  showHotVodMore () {
    this.homeMoreService.hotVodMore().then((resp) => {
      this.moreChannelList = {
        'dataList': resp,
        'row': 'All',
        'suspensionID': 'moreData'
      }
    })
  }
    /**
     * click the VOD poster to jump to VOD playing page.
     */
  vodDtail (event) {
    this.vodRouter.navigate(event[0])
  }
    /**
     * click the playbill poster to jump to playbill detail page.
     */
  onClickDetail (jumpdata) {
    this.router.navigate(['live-tv', 'live-tv-detail', jumpdata[0], jumpdata[1]])
  }
    /**
     * go to home page.
     */
  goback (backRoute) {
    if (backRoute.indexOf('Home') !== -1) {
      this.router.navigate(['/home'])
    }
  }
}
