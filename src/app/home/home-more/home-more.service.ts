import * as _ from 'underscore'
import { Injectable } from '@angular/core'
import { queryRecmVODList, queryHotPlaybill, queryRecmContent, getPlaybillDetail } from 'ng-epg-sdk/vsp'
import { PictureService } from 'ng-epg-ui/services/picture.service'
import { CommonService } from '../../core/common.service'

@Injectable()
export class HomeMoreService {
  constructor (
        private pictureService: PictureService,
        private commonService: CommonService
  ) { }
  /**
     * get data from interface 'GetPlaybillDetail'.
     */
  getQueryPlaybill (id) {
    return getPlaybillDetail({
      playbillID: id,
      isReturnAllMedia: '1'
    }).then(resp => {
      return resp
    })
  }
  /**
     * handle VOD data of column 'every one is watching'.
     */
  recmVodMore () {
    return queryRecmVODList({
      count: '30',
      offset: '0',
      action: '5'
    }).then(resp => {
      return _.map(resp.VODs, (list, index) => {
        this.score(list)
        let url = list.picture && list.picture.posters && this.pictureService.convertToSizeUrl(list.picture.posters[0],
          { minwidth: 180, minheight: 240, maxwidth: 214, maxheight: 284 })
        list['focusRoute'] = 'home'
        return {
          vodId: list['ID'],
          name: list['name'],
          posterUrl: url,
          score: list['averageScore'],
          titleName: 'every_one_is_watching',
          floatInfo: list,
          subjectID: '-1'
        }
      })
    })
  }
  /**
     * handle top week VODs
     */
  recmTopWeek () {
    return queryRecmVODList({
      count: '30',
      offset: '0',
      action: '5',
      sortType: 'AVERAGESCORE:DESC',
      subjectID: 'VODAll'
    }).then(resp => {
      return _.map(resp.VODs, (list, index) => {
        this.score(list)
        let url = list.picture && list.picture.posters && this.pictureService.convertToSizeUrl(list.picture.posters[0],
          { minwidth: 180, minheight: 240, maxwidth: 214, maxheight: 284 })
        list['focusRoute'] = 'home'
        return {
          vodId: list['ID'],
          name: list['name'],
          posterUrl: url,
          score: list['averageScore'],
          titleName: 'top_this_week',
          floatInfo: list,
          subjectID: '-1'
        }
      })
    })
  }
  /**
     * handle VOD data of column 'top picks for you'.
     */
  recmListMore () {
    return queryRecmContent({
      queryDynamicRecmContent: {
        recmScenarios: [{
          contentType: 'VOD',
          businessType: 'VOD',
          recmType: '2',
          count: '30',
          offset: '0'
        }]
      }
    }).then(resp => {
      return _.map(resp.recmContents[0].recmVODs, (list, index) => {
        if (list.averageScore !== '10') {
          this.score(list)
        }
        let url = list.picture && list.picture.posters && this.pictureService.convertToSizeUrl(list.picture.posters[0],
          { minwidth: 180, minheight: 240, maxwidth: 214, maxheight: 284 })
        list['focusRoute'] = 'home'
        return {
          vodId: list['ID'],
          name: list['name'],
          posterUrl: url,
          score: list['averageScore'],
          titleName: 'picks_for_you',
          floatInfo: list,
          subjectID: '-1'
        }
      })
    })
  }
  /**
     * handle playbill data of column 'hot tv show'.
     */
  hotVodMore () {
    return queryHotPlaybill({
      count: '30',
      offset: '0',
      boardType: '2'
    }).then(resp => {
      return _.map(resp.playbills, (list, index) => {
        let url = list.picture && list.picture.posters && this.pictureService.convertToSizeUrl(list.picture.posters[0],
          { minwidth: 230, minheight: 129, maxwidth: 260, maxheight: 146 })
        list['focusRoute'] = 'home'
        return {
          ID: list['ID'],
          startTime: list['startTime'],
          endTime: list['endTime'],
          name: list['name'],
          posterUrl: url,
          idType: 'isChannel',
          floatflag: 'moreList',
          channelDetailFlag: 'channeldetail',
          floatInfo: list,
          len: '',
          isLen: '0',
          startTimes: this.commonService.dealWithDateJson('D10', parseInt(list['startTime'], 10)),
          channelName: ' | ' + list['channel']['name'],
          leftTimes: ''
        }
      })
    })
  }
  /**
     * handle with average score of VOD.
     */
  score (list) {
    if (list.averageScore) {
      if (list.averageScore !== '10' && list.averageScore.substring(0, 2) !== '10') {
        if (list.averageScore.length === 1) {
          list.averageScore += '.0'
        } else {
          let score = list.averageScore
          list.averageScore = score.substring(0, 4)
          list.averageScore = Number(list.averageScore)
          list.averageScore = list.averageScore.toFixed(1)
        }
      } else {
        list.averageScore = '10'
      }
    } else {
      list.averageScore = '0.0'
    }
    return list
  }
}
