import * as _ from 'underscore'
import { Injectable } from '@angular/core'
import { PictureService } from 'ng-epg-ui/services/picture.service'

@Injectable()
export class BannerService {
  constructor (private pictureService: PictureService) { }
  /*
     * get the info of actors and directors
     */
  getCasts (resp: Array<any>) {
    let directors = ''
    let actors = ''
    _.each(resp, (item, index) => {
      for (let i = 0; i < item.casts.length; i++) {
        // roleType equals 1 that meaning directors
        if (parseInt(item.roleType, 10) === 1) {
          directors += item.casts[i].castName + ', '
        } else if (parseInt(item.roleType, 10) === 0) { // roleType equals 0 that meaning actors
          actors += item.casts[i].castName + ', '
        }
      }
    })
    // delete the comma in the string of actors and directors
    directors = directors.slice(0, directors.lastIndexOf(','))
    actors = actors.slice(0, actors.lastIndexOf(','))

    return { directors: directors, actors: actors }
  }
  /*
     *get the banner list
     */
  getBannerList (bannerList) {
    bannerList = _.map(bannerList, (item, index) => {
      let casts = this.getCasts(item['castRoles'])
      // greater poster address
      let url = item['picture'] && item['picture'].drafts && this.pictureService.convertToSizeUrl(item['picture'].drafts[0],
        { minwidth: 1920, minheight: 600, maxwidth: 1920, maxheight: 600 })
      return {
        id: item['ID'],
        name: item['name'],
        director: casts['directors'] || '/', // if there is no actor, use '/'
        actor: casts['actors'] || '/',
        picture: url || 'assets/img/default/home_banner_1000x480.png',
        poster: (item['picture'] && item['picture'].drafts &&
                        this.pictureService.convertToSizeUrl(item['picture'].drafts[0],
                          { minwidth: 128, minheight: 72, maxwidth: 156, maxheight: 88 })) || 'assets/img/default/home_banner_small.png',
        intruduce: item['introduce'],
        index: index,
        isSubscribed: item['isSubscribed'] ? item['isSubscribed'] : '0'
      }
    })
    return bannerList
  }
}
