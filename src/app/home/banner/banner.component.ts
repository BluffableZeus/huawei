import * as _ from 'underscore'
import { Component, Input, OnInit, OnDestroy } from '@angular/core'
import { CommonService } from '../../core/common.service'

import { BannerService } from './banner.service'
import { Router } from '@angular/router'
import { VODRouterService } from '../../shared/services/vod-router.service'
import { session } from 'src/app/shared/services/session'
import { TranslateService } from '@ngx-translate/core'
import { EventService } from 'ng-epg-sdk/services'
import { BannerScroll } from './bannerScroll.service'
const pageTracker = require('../../../assets/lib/PageTracker')

// @Component({
//     selector: 'app-banner',
//     styleUrls: ['./banner.component.scss'],
//     templateUrl: './banner.component.html'
// })

export class BannerComponent implements OnInit, OnDestroy {
  public hasBannerdata = false
    /**
     * current banner
     */
  public curBanner: any = {}
  public hasBanner = true
  public maxOffsetLenght = 0
  public homeTimer: any
  public cover = 'assets/img/banner_cover.png'
    /**
     * show play button
     */
  public showPlay: boolean
    /**
     * list of banner
     */
  public bannerList
  public showIcon
    /**
     * if the introduce is over-long, showing the ellipsis
     */
  public introduceEllipsis = false
    /**
     * if the title is over-long, showing the ellipsis
     */
  public titleEllipsis = false
  public realItemHeight: number
    /**
     * if the mouse enter the picture,stop the rotate
     */
  public mouseEnter = false

  constructor (
        private commonService: CommonService,
        private bannerService: BannerService,
        private bannerScroll: BannerScroll,
        private router: Router,
        private vodRouter: VODRouterService,
        private translate: TranslateService
    ) { }

  @Input() set setBannerList (bannerList) {
    this.bannerList = bannerList || []
      // if the number of banner is greater than 0,show the data
    if (this.bannerList.length > 0) {
        // if the number of banner is less than 4,hide the buttom
      if (this.bannerList.length <= 4) {
        this.showIcon = false
      } else {
        this.showIcon = true
      }
      clearInterval(this.homeTimer)
      this.bannerList = this.bannerService.getBannerList(this.bannerList)
      this.maxOffsetLenght = -(Math.ceil(this.bannerList.length / 4) - 1) * 384
      this.curBanner = this.bannerList[0] || {}
        // if the profile log in and subscribed,show the play buttom,otherwise show the subscribe buttom
      if (this.curBanner['isSubscribed'] === '1' && Cookies.getJSON('IS_PROFILE_LOGIN')) {
          // play
        this.showPlay = true
      } else {
          // buy
        this.showPlay = false
      }
        // clear the timer
      this.bannerScroll.destroy()
      this.hasBanner = true
      _.delay(function () {
        let clientW: number = document.body.clientWidth
        if (clientW > 1440) {
            this.realItemHeight = 96
          } else {
            this.realItemHeight = 82
          }
        this.bannerScroll.startBanner(document.getElementById('cycle-list'), this.bannerList.length, this.realItemHeight)
        this.rotateBanner()
      }.bind(this), 1000)
    } else {
        // if the number of banner is 0, show the tips of no data
      this.hasBanner = false
    }
  }

  ngOnInit () {
    this.cover = 'assets/img/banner_cover.png'
    let self = this
    EventService.on('FIRST_TIME_LOGIN', () => {
      clearInterval(this.homeTimer)
    })
      // when the screen size changed
    EventService.on('ScreenSize', function () {
      let clientW: number = window.innerWidth
      if (clientW > 1440) {
        self.realItemHeight = 96
        self.bannerScroll.startBanner(document.getElementById('cycle-list'), this.bannerList.length,
            self.realItemHeight, 'unchange')
        let index = this.bannerScroll.getFocus()
        this.curBanner = this.bannerList[index] || {}
      } else {
        self.realItemHeight = 82
        self.bannerScroll.startBanner(document.getElementById('cycle-list'), this.bannerList.length,
            self.realItemHeight, 'unchange')
        let index = this.bannerScroll.getFocus()
        this.curBanner = this.bannerList[index] || {}
      }
    }.bind(this))
  }

  ngOnDestroy () {
      // clear the timer
    if (this.homeTimer) {
      clearInterval(this.homeTimer)
    }
  }

    /*
     * play event
     */
  plays () {
    this.play()
    if (this.curBanner['isSubscribed'] === '0' && !Cookies.getJSON('IS_GUEST_LOGIN')) {
      session.put('buy', true)
    }
  }

    /**
     * enter the VOD details page and play
     */
  play () {
    const trackParams = {
      businessType: 1,
      contentCode: this.curBanner['id'],
      source: 2
    }
      // collect usage data
    pageTracker.pushBrowseRecmBehavior(trackParams)
    this.vodRouter.navigate(this.curBanner.id)
  }

    /*
     * click the small poster event
     */
  onChange (banner, num) {
      // clear the timer and stop rotating when click
    clearInterval(this.homeTimer)
    this.curBanner = banner || {}
      // start rotating when finish changing the banner
    this.rotateBanner()
    this.bannerScroll.setFocus(parseInt(banner['index'], 10), parseInt(num, 10))
  }

    /*
     * banner rotate
     */
  rotateBanner () {
    clearInterval(this.homeTimer)
    this.homeTimer = setInterval(function () {
        if (!this.mouseEnter) {
          this.bannerScroll.next()
          let index = this.bannerScroll.getFocus()
          this.curBanner = this.bannerList[index] || {}
        }
      }.bind(this), 6000)
  }

    /*
     * change the banner downward
     */
  onClickDown () {
    clearInterval(this.homeTimer)
    this.bannerScroll.next()
    let index = this.bannerScroll.getFocus()
    this.curBanner = this.bannerList[index] || {}
    this.rotateBanner()
  }

    /*
     *change the banner upward
     */
  onClickUp () {
    clearInterval(this.homeTimer)
    this.bannerScroll.prev()
    let index = this.bannerScroll.getFocus()
    this.curBanner = this.bannerList[index] || {}
    this.rotateBanner()
  }

    /**
     * when the content is overflow, add the ellipses in the last column with the IE browser
     */
  showWidth () {
    let ua = navigator.userAgent
    if (ua.indexOf('Chrome') !== -1 && ua.indexOf('Edge') === -1) {
      return
    }
    if (document.getElementById('contentBannerBg')) {
      let contentBg = document.getElementById('contentBannerBg')
      if (contentBg['offsetHeight'] > 60) {
        this.introduceEllipsis = true
      } else {
        this.introduceEllipsis = false
      }
    }
  }

    /**
     * when the title is overflow, add the ellipses in the last column with the IE browser
     */
  showTitleWidth () {
    let ua = navigator.userAgent
    let clientW: number = window.innerWidth
    if (ua.indexOf('Chrome') !== -1 && ua.indexOf('Edge') === -1) {
      return
    }
    if (document.getElementById('banner-title')) {
      let changeDom = document.querySelector('.InfoWrap .title')
      if (clientW > 1440) {
        if (changeDom['innerText'].length > 18) {
            this.titleEllipsis = true
          } else {
            this.titleEllipsis = false
          }
      } else {
        if (changeDom['innerText'].length > 27) {
            this.titleEllipsis = true
          } else {
            this.titleEllipsis = false
          }
      }
    }
  }

    /**
     * when the content is overflow, add css style with the Chrome browser
     */
  showContent () {
    let ua = navigator.userAgent
    if (ua.indexOf('Chrome') !== -1 && ua.indexOf('Edge') === -1) {
      let styles = document.getElementById('banner-introduce')['style']['-webkit-box-direction']
      let style = {
      }
      if (styles === 'reverse') {
        style['-webkit-box-direction'] = 'normal'
      } else {
        style['-webkit-box-direction'] = 'reverse'
      }
      return {
        'max-height': '80px',
        '-webkit-line-clamp': '4',
        'word-break': 'normal',
        '-webkit-box-direction': style['-webkit-box-direction']
      }
    }
  }

    /**
     * set style of banner title
     */
  showTitleContent () {
    let ua = navigator.userAgent
    if (ua.indexOf('Chrome') !== -1 && ua.indexOf('Edge') === -1) {
      let styles = document.getElementById('banner-title')['style']['-webkit-box-direction']
      let style = {
      }
      if (styles === 'reverse') {
        style['-webkit-box-direction'] = 'normal'
      } else {
        style['-webkit-box-direction'] = 'reverse'
      }
      return {
        'max-height': '114px',
        '-webkit-line-clamp': '2',
        'word-break': 'break-all',
        '-webkit-box-direction': style['-webkit-box-direction']
      }
    }
  }

    /**
     * when the mouse moves up, turn off the timer.
     */
  closeTimer (curBanner) {
    if (curBanner) {
      this.mouseEnter = true
    }
  }

    /**
     * when the mouse moves out, start the timer.
     */
  startTimer (curBanner) {
    if (curBanner) {
      this.mouseEnter = false
    }
  }
}
