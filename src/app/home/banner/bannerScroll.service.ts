import { Injectable } from '@angular/core'

@Injectable()
export class BannerScroll {
  public _listBox = {}
  public focus = 0
  public firstDatum = 0
  public pieceOnShow = 0
  public len = 0
  public transA = 0
  public transB = 0
  public realTotalHeight = 0
  public realItemHeight = 0
  public dom = {}
  public _pieces = {}
  public dynamicCssUp = ''
  public _dynamicStyle
  private isExecNext = true

  destroy () {
    this._listBox = {}
    this.focus = 0
    this.firstDatum = 0
    this.pieceOnShow = 0
    this.len = 0
    this.transA = 0
    this.transB = 0
    this.realTotalHeight = 0
    this.realItemHeight = 0
    this.dom = {}
    this._pieces = {}
    this.dynamicCssUp = ''
  }

  startBanner (dom, length, height, unchange?: any) {
    let head = document.getElementsByTagName('head')[0]
    this._dynamicStyle = document.createElement('style')
    this._dynamicStyle['id'] = 'cycle-list-style'
    head.appendChild(this._dynamicStyle)
    this.dom = dom
      // draw a box in the target node which can identificate css
    this._listBox = this.dom && this.dom['children'] && this.dom['children'][0]

    let itemHeight = height || 96
    this.realItemHeight = itemHeight
    this.len = length
    this.realTotalHeight = this.realItemHeight * this.len

    let _listBoxC = this._listBox && this._listBox['children']
    let _listBody = this._listBox && this._listBox['children'] && _listBoxC[1]
    this._pieces = _listBody && _listBody.children

      // set up the keyframes animation again according to the real position of A side and B side
    this.dynamicCssUp = ''

    if (!unchange) {
      this._pieces[0].children[0].setAttribute('class', 'whiteBorder')
      this._pieces[0].style.cssText = 'animation:none'
      this._pieces[1].style.cssText += 'animation: none'
        // record the  A side and B side' translate
      this.transA = 0
      this.transB = this.realTotalHeight
    } else {
      if (this.realItemHeight === 82) {
        if (this.transA % 96 !== 0) {
            return
          }
        this.transA = this.transA / 96 * 82 + this.realItemHeight
        this.transB = this.transB / 96 * 82 + this.realItemHeight
      } else {
        if (this.transA % 82 !== 0) {
            return
          }
        this.transA = this.transA / 82 * 96 + this.realItemHeight
        this.transB = this.transB / 82 * 96 + this.realItemHeight
      }
      this.focus = this.focus - 1
      this.firstDatum--
      if (this.firstDatum < 0) { this.firstDatum += this.len }
      this.next()
      this.previewScreenNext()
    }
  }

    // screen change preview click next
  previewScreenNext () {
    let diff = this.focus - this.firstDatum
    if (diff < 3) {
      if (this.transA === 0) {
        this.transB = this.realTotalHeight
      } else if (this.transB === 0) {
          this.transA = this.realTotalHeight
        }
      this.cssUpConcat(this.transA, this.transA - this.realItemHeight, this.transB, this.transB - this.realItemHeight)
      this.transA -= this.realItemHeight
      this.transB -= this.realItemHeight
      if (this._pieces) {
        this._pieces[0].style.cssText += 'animation: nb-cycle-list-up' + this.transA +
                    ' 0.7s both ease-out; -webkit-animation: nb-cycle-list-up' + this.transA + ' 0.7s both ease-out;'
        this._pieces[1].style.cssText += 'animation: nb-cycle-list-up' + this.transB +
                    ' 0.7s both ease-out; -webkit-animation: nb-cycle-list-up' + this.transB + ' 0.7s both ease-out;'
      }
      this.firstDatum = (this.firstDatum + 1) % this.len
    }
  }

  setFocus (newFocus, newpiece) {
    this.focus = newFocus
  }

  getFocus () { return this.focus }

  cssUpConcat (posA1, posA2, posB1, posB2) {
    this.dynamicCssUp = ''
    let ua = navigator.userAgent.toLowerCase()
    let isIe = !!ua.match(/msie/i) || !!ua.match(/rv:([\d.]+).*like gecko/)
    if (isIe) {
      this.dynamicCssUp = '@keyframes nb-cycle-list-up' + posA2 +
                '{0%{transform:translate3d(0,' + posA1 + 'px,0)} 100%{transform:translate3d(0,' + posA2 + 'px,0)}}' +
                '@keyframes nb-cycle-list-up' + posB2 +
                '{0%{transform:translate3d(0,' + posB1 + 'px,0)} 100%{transform:translate3d(0,' + posB2 + 'px,0)}}'
    } else {
      this.dynamicCssUp = '@-webkit-keyframes nb-cycle-list-up' + posA2 +
                '{0%{-webkit-transform:translate3d(0,' + posA1 + 'px,0)} 100%{-webkit-transform:translate3d(0,' + posA2 + 'px,0)}}' +
                '@-webkit-keyframes nb-cycle-list-up' + posB2 +
                '{0%{-webkit-transform:translate3d(0,' + posB1 + 'px,0)} 100%{-webkit-transform:translate3d(0,' + posB2 + 'px,0)}}'
    }
    this._dynamicStyle.innerText = this.dynamicCssUp
  }

  cssDownConcat (posA1, posA2, posB1, posB2) {
    this.dynamicCssUp = ''
    let ua = navigator.userAgent.toLowerCase()
    let isIe = !!ua.match(/msie/i) || !!ua.match(/rv:([\d.]+).*like gecko/)
    if (isIe) {
      this.dynamicCssUp = '@keyframes nb-cycle-list-down' + posA2 +
                '{0%{transform:translate3d(0,' + posA1 + 'px,0)} 100%{transform:translate3d(0,' + posA2 + 'px,0)}}' +
                '@keyframes nb-cycle-list-down' + posB2 +
                '{0%{transform:translate3d(0,' + posB1 + 'px,0)} 100%{transform:translate3d(0,' + posB2 + 'px,0)}}'
    } else {
      this.dynamicCssUp = '@-webkit-keyframes nb-cycle-list-down' + posA2 +
                '{0%{-webkit-transform:translate3d(0,' + posA1 + 'px,0)} 100%{-webkit-transform:translate3d(0,' + posA2 + 'px,0)}}' +
                '@-webkit-keyframes nb-cycle-list-down' + posB2 +
                '{0%{-webkit-transform:translate3d(0,' + posB1 + 'px,0)} 100%{-webkit-transform:translate3d(0,' + posB2 + 'px,0)}}'
    }
    this._dynamicStyle.innerText = this.dynamicCssUp
  }

    // click next
  next () {
    if (this.isExecNext) {
      this.isExecNext = false
      let newFocus = this.focus + 1
      if (newFocus >= this.len) {
        this['setFocus'](newFocus % this.len, 1 - this.pieceOnShow)
      } else {
        this['setFocus'](newFocus, this.pieceOnShow)
      }
      let diff = this.focus - this.firstDatum
      if (diff < 0) { diff += this.len }
      if (diff > 3) {
        if (this.transA === 0) {
            this.transB = this.realTotalHeight
          } else if (this.transB === 0) {
            this.transA = this.realTotalHeight
          }
        this.cssUpConcat(this.transA, this.transA - this.realItemHeight, this.transB, this.transB - this.realItemHeight)
        this.transA -= this.realItemHeight
        this.transB -= this.realItemHeight
        if (this._pieces) {
            this._pieces[0].style.cssText += 'animation: nb-cycle-list-up' + this.transA +
                        ' 0.7s both ease-out; -webkit-animation: nb-cycle-list-up' + this.transA + ' 0.7s both ease-out;'
            this._pieces[1].style.cssText += 'animation: nb-cycle-list-up' + this.transB +
                        ' 0.7s both ease-out; -webkit-animation: nb-cycle-list-up' + this.transB + ' 0.7s both ease-out;'
          }
        this.firstDatum = (this.firstDatum + 1) % this.len
      }
      this.isExecNext = true
    }
  }

  prev () {
    if (this.focus === this.firstDatum) {
      if (this.transA === 0) {
        this.transB = -this.realTotalHeight
      } else if (this.transB === 0) {
          this.transA = -this.realTotalHeight
        }
      this.cssDownConcat(this.transA, this.transA + this.realItemHeight, this.transB, this.transB + this.realItemHeight)
      this.transA += this.realItemHeight
      this.transB += this.realItemHeight

      this._pieces[0].style.cssText += 'animation: nb-cycle-list-down' + this.transA +
                ' 0.7s both ease-out; -webkit-animation: nb-cycle-list-down' + this.transA + ' 0.7s both ease-out;'
      this._pieces[1].style.cssText += 'animation: nb-cycle-list-down' + this.transB +
                ' 0.7s both ease-out; -webkit-animation: nb-cycle-list-down' + this.transB + ' 0.7s both ease-out;'

      this.firstDatum--
      if (this.firstDatum < 0) { this.firstDatum += this.len }
    }

    let newFocus = this.focus - 1
    if (newFocus < 0) {
      this.setFocus(newFocus + this.len, 1 - this.pieceOnShow)
    } else {
      this.setFocus(newFocus, this.pieceOnShow)
    }
  }
}
