import { NgModule, APP_INITIALIZER, Optional, SkipSelf } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { TranslateModule, TranslateService, TranslateLoader } from '@ngx-translate/core'
import { TranslateHttpLoader } from '@ngx-translate/http-loader'

import { init } from '../translate.listener'
import { SDK_PROVIDERS } from 'ng-epg-sdk'
import { OC_PROVIDERS } from 'oper-center'
import { DialogModule } from 'ng-epg-ui/webtv-components/dialog/dialog.module'
import { PictureService } from 'ng-epg-ui/services/picture.service'
import { DirectionScroll } from 'ng-epg-ui/webtv-components/scroll/directionScroll.component'
import { VODRouterService } from '../shared/services/vod-router.service'
import { CommonService } from './common.service'
import { SharedModule } from '../shared/shared.module'
import { ViewTransferService } from './view-transfer.service';

export function translateLoaderProvide (http: HttpClient) {
  return new TranslateHttpLoader(http, 'assets/i18n/', '.json')
}

@NgModule({
  imports: [
    SharedModule,
    TranslateModule.forRoot(),
    DialogModule
  ],
  exports: [
    TranslateModule,
    DialogModule
  ],
  declarations: [],
  providers: [
    CommonService,
    ViewTransferService,
    TranslateService,
    {
      provide: TranslateLoader,
      useFactory: translateLoaderProvide,
      deps: [HttpClient]
    },
    {
      provide: APP_INITIALIZER,
      useFactory: init,
      deps: [TranslateService],
      multi: true
    },
    ...SDK_PROVIDERS,
    ...OC_PROVIDERS,
    PictureService,
    DirectionScroll,
    VODRouterService,
  ]
})
export class CoreModule {
  constructor (@Optional() @SkipSelf() parentModule: CoreModule) {
    if (parentModule) {
      throw new Error('CoreModule is already loaded. Import it in the AppModule only')
    }
  }
}
