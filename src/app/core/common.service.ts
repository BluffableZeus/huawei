import * as _ from 'underscore'
import { Injectable, ViewContainerRef, Type } from '@angular/core'
import { DialogService, DialogConfig, DialogRef, DialogParams } from '../../../ng-epg-ui/src/webtv-components/dialog/index'
import { TranslateService } from '@ngx-translate/core'
import { MediaPlayService } from '../component/mediaPlay/mediaPlay.service'
import { createFavorite, deleteFavorite, queryFavorite, deleteLock, createLock, getVODDetail } from 'ng-epg-sdk'
import { EventService } from 'ng-epg-sdk'
import { Logger, DateUtils } from 'ng-epg-sdk'
import { session } from '../shared'

const log = new Logger('common.service')

@Injectable()
export class CommonService {

  private _viewContainerRef: ViewContainerRef
  private _translateService: TranslateService

  constructor (
        private mediaPlayService: MediaPlayService,
        private dialogService: DialogService,
        private translate: TranslateService
    ) { }

  get viewContainerRef () {
    return this._viewContainerRef
  }

  set viewContainerRef (vcr: ViewContainerRef) {
    this._viewContainerRef = vcr
  }

  get translateService () {
    return this._translateService
  }

  set translateService (ts: TranslateService) {
    this._translateService = ts
  }

  loginPlay () {
    let ua = navigator.userAgent.toLowerCase()
    let isIe = !!ua.match(/msie/i) || !!ua.match(/rv:([\d.]+).*like gecko/)
    let isEdge = !!ua.match(/edge\/([\d.]+)/)
    let isFirefox = !!ua.match(/firefox\/([\d.]+)/)
    if (isIe || isEdge || isFirefox) {
      let channelvideo = document.querySelector('#videoContainer')
      let vodVideo = document.querySelector('#vodVideoContainer')
      let MediaplayerChannel = this.mediaPlayService.isFullScreen()
      let MediaplayerVod = this.mediaPlayService.isFullScreen()
            // TODO: Think how this affects non-player pages
      if (MediaplayerChannel && (location.pathname.indexOf('video') !== -1 || location.pathname.indexOf('movie') !== -1)) {
        channelvideo.appendChild(document.querySelector('md-dialog-container'))
      } else if (MediaplayerVod) {
          vodVideo.appendChild(document.querySelector('md-dialog-container'))
        }
    }
  }

    /**
     * open dialog
     */
  openDialog (component: Type<any>, config?: DialogConfig, params?: DialogParams) {

    // WORKAROUND
    if (component.name === 'AccountLoginComponent') {
      EventService.emit('AUTHENTICATE_OPEN_DIALOG')
      return Promise.resolve()
    }

    config = (config || new DialogConfig())
            .parent(document.querySelector('main') as HTMLElement)
            .clickOutsideToClose(false)
            .containerCss('page')
    return this.dialogService.open(component,
            this._viewContainerRef, config, [{ provide: TranslateService, useValue: this.translateService }], params)
            .then((ref: DialogRef) => {
              if (component.name === 'AccountLoginComponent') {
                this.LoginDialgStyle('app-account-login')
                this.loginPlay()
              } else if (component.name === 'AccountRegisterComponent') {
                this.LoginDialgStyle('app-account-register')
              } else if (component.name === 'ResetPasswordComponent') {
                this.LoginDialgStyle('app-reset-password')
              } else if (component.name === 'ErrorMessageDialogComponent') {
                  this.LoginDialgStyle('app-error-message-dialog')
                } else if (component.name === 'PassInputDialogComponent') {
                  this.LoginDialgStyle('app-pass-input-dialog')
                } else if (component.name === 'RecordDialogComponent') {
                  this.LoginDialgStyle('app-record-dialog')
                } else if (component.name === 'PreferencesHomeComponent') {
                  this.LoginDialgStyle('app-preferences-home')
                } else if (component.name === 'StopRecordConfirmComponent') {
                  this.LoginDialgStyle('app-stop-record-confirm')
                  this.loginPlay()
                } else if (component.name === 'StorageRemindComponent') {
                  this.LoginDialgStyle('app-storageremind')
                } else if (component.name === 'DeviceReplaceComponent') {
                  this.LoginDialgStyle('app-device-replace')
                } else if (component.name === 'ReminderComponent') {
                  this.LoginDialgStyle('app-reminder')
                } else if (component.name === 'RecordingConflictComponent') {
                  this.LoginDialgStyle('app-recording-conflict')
                } else if (component.name === 'MixedConflictComponent') {
                  this.LoginDialgStyle('app-mixed-conflict')
                }
              return ref.whenClosed
            })
  }

  passDialgStyle (tag) {
    let livetvVideoWidth = window.screen.width + 'px'
    let livetvVideoHeight = window.screen.height + 'px'
    if (document.getElementsByTagName(tag)[0] && document.getElementsByTagName('md-dialog-container')[0]) {
      document.getElementsByTagName(tag)[0]['style']['position'] = 'relative'
      document.getElementsByTagName(tag)[0]['style']['width'] = livetvVideoWidth
      document.getElementsByTagName(tag)[0]['style']['height'] = livetvVideoHeight
      document.getElementsByTagName(tag)[0]['style']['display'] = 'block'
      document.getElementsByTagName('md-dialog-container')[0]['style'].position = 'fixed'
      document.getElementsByTagName('md-dialog-container')[0]['style']['z-index'] = '2200000000'
      document.getElementsByClassName(tag)[0]['style']['top'] = (window.screen.height - 302) / 2 + 'px'
    }
  }

    /**
     * set style of dialog
     */
  LoginDialgStyle (tag) {
    let livetvVideoWidth = window.screen.width + 'px'
    let livetvVideoHeight = window.screen.height + 'px'
    if (document.getElementsByTagName(tag)[0] && document.getElementsByTagName('md-dialog-container')[0]) {
      document.getElementsByTagName(tag)[0]['style']['position'] = 'relative'
      document.getElementsByTagName(tag)[0]['style']['background'] = 'rgba(0, 0, 0, 0.7)'
      document.getElementsByTagName(tag)[0]['style']['width'] = livetvVideoWidth
      document.getElementsByTagName(tag)[0]['style']['height'] = livetvVideoHeight
      document.getElementsByTagName(tag)[0]['style']['display'] = 'block'
      document.getElementsByTagName('md-dialog-container')[0]['style'].position = 'fixed'
      document.getElementsByTagName('md-dialog-container')[0]['style']['z-index'] = '2200000000'
      if (tag === 'app-account-login' && document.getElementsByClassName(tag)[0]) {
        document.getElementsByClassName(tag)[0]['style']['top'] = (window.screen.height - 367) / 2 + 'px'
        return
      }
      if (tag === 'app-account-register' && document.getElementsByClassName(tag)[0]) {
        document.getElementsByClassName(tag)[0]['style']['top'] =
                    (window.screen.height - (document.getElementsByClassName(tag)[0] as HTMLElement).offsetHeight) / 2 + 'px'
        return
      }
      if (tag === 'app-reset-password' && document.getElementsByClassName(tag)[0]) {
        document.getElementsByClassName(tag)[0]['style']['top'] =
                    (window.screen.height - (document.getElementsByClassName(tag)[0] as HTMLElement).offsetHeight) / 2 + 'px'
        return
      }
      if (tag === 'app-device-replace' && document.getElementsByClassName(tag)[0]) {
        document.getElementsByClassName(tag)[0]['style']['top'] = (window.screen.height - 288) / 2 + 'px'
        return
      }
      if (tag !== 'app-storageremind' && document.getElementsByClassName(tag)[0]) {
        document.getElementsByClassName(tag)[0]['style']['top'] = (window.screen.height - 331) / 2 + 'px'
      }
    }
  }

    /**
     * the method of add favorite, call the interface named createFavorite；
     * req：the params of createFavorite interface
     */
  addFavorite (req: any) {
    return createFavorite(req).then((resp) => {
            // add successful
      if (resp['result']['retCode'] === '000000000') {
        EventService.emit('ADD_SUSDATA_successful', {})
        EventService.emit('ADD_successful', {})
      }
      return resp
    })
  }

    /**
     * the method of delete favorite, call the interface named deleteFavorite；
     * req：the params of deleteFavorite interface
     */
  removeFavorite (req: any) {
    return deleteFavorite(req).then((resp) => {
            // delete successful
      if (resp['result']['retCode'] === '000000000') {
        EventService.emit('REMOVE_SUSDATE_successful', {})
        EventService.emit('REMOVE_successful', {})
      }
      return resp
    })
  }

    /**
     * delete channel lock
     */
  unLockChannel (channelIDs?, types?, isToast?) {
    const req = { itemIDs: channelIDs, lockTypes: types }
    return deleteLock(req).then((resp) => {
      return resp
    }, (resp) => {
      return resp
    })
  }

    /**
     * add channel lock
     */
  createLock (locks) {
    const req = { locks: locks }
    return createLock(req).then((resp) => {
      return resp
    }, (resp) => {
      return resp
    })
  }

    /**
     * format time string
     */
  formatStr (str, index) {
    let weekend
    let month
    let day
    let hour
    let minute
    let allweekends = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']
    let arr = []
    if (str.length === 13) {
      let date = parseInt(str, 10)
      let datestr = new Date(date)
      month = (datestr.getMonth() + 1) < 10 ? '0' + (datestr.getMonth() + 1) : (datestr.getMonth() + 1)
      weekend = datestr.getDay()
      day = datestr.getDate() < 10 ? '0' + datestr.getDate() : datestr.getDate()
      hour = datestr.getHours() < 10 ? '0' + datestr.getHours() : datestr.getHours()
      minute = datestr.getMinutes() < 10 ? '0' + datestr.getMinutes() : datestr.getMinutes()
      weekend = allweekends[weekend]
    } else {
      month = str.slice(4, 6)
      day = str.slice(6, 8)
      hour = str.slice(8, 10)
      minute = str.slice(10, 12)
    }
    arr.push(month)
    arr.push(weekend)
    arr.push(day)
    arr.push(hour)
    arr.push(minute)
    return arr[index]

  }

    /**
     * call the interface to obtain the specified collection result based on the query condition
     */
  showFavoriteVod (req: any) {
    return queryFavorite(req).then(resp => {
      return resp
    })
  }

    /**
     * when the mouse moves up, set the style of the suspension box
     */
  moveEnter (currentTarget, ID, clientWidth, type, origin?) {
    let self = this
    _.delay(function () {
      if (document.getElementById(ID)) {
        let width = currentTarget.offsetWidth
        let height = currentTarget.offsetHeight
        let left = 0
        switch (type) {
            case 'record':
              if (origin === 'searchAll') {
                  left = currentTarget.offsetLeft
                } else {
                  left = currentTarget.offsetLeft + currentTarget.offsetParent.offsetLeft
                }
              document.getElementById(ID).style.left = left - 0.05 * width + 'px'
              document.getElementById(ID).style.top = currentTarget.offsetTop - 0.05 * height + 'px'
              break
            case 'reminder':
            case 'myChannel':
            case 'program':
            case 'purchased':
              left = currentTarget.offsetLeft
              document.getElementById(ID).style.left = left - 0.05 * width + 'px'
              document.getElementById(ID).style.top = currentTarget.offsetTop - 0.05 * height + 'px'
              break
            case 'future':
              left = self.getLeft(clientWidth, currentTarget)
              document.getElementById(ID).style.left = left + 60.5 - 0.05 * width + 'px'
              document.getElementById(ID).style.top = currentTarget.offsetTop +
                            currentTarget.offsetParent.offsetParent.offsetTop - 0.05 * height + 'px'
              break
            default:
              left = self.getLeft(clientWidth, currentTarget)
              if (origin === 'searchAll') {
                  document.getElementById(ID).style.left = left - 0.05 * width + 'px'
                  document.getElementById(ID).style.top = currentTarget.offsetTop - 0.05 * height + 'px'
                } else {
                  document.getElementById(ID).style.left = left + 60.5 - 0.05 * width + 'px'
                  document.getElementById(ID).style.top = currentTarget.offsetTop +
                                currentTarget.offsetParent.offsetParent.offsetTop - 0.05 * height + 'px'
                }
              break
          }
        document.getElementById(ID).style.width = 1.1 * width + 'px'
        document.getElementById(ID).style.height = 1.1 * height + 'px'

        if (document.querySelector('#' + ID + ' .tvod-detail-secondary')) {
            document.querySelector('#' + ID + ' .tvod-detail-secondary')['style']['width'] = 1.1 * width - 50 + 'px'
          }

        if (document.querySelector('#' + ID + ' .contentsTvod')) {
            document.querySelector('#' + ID + ' .contentsTvod')['style']['width'] = 1.1 * width - 50 + 'px'
          }

      }
    }, 100)
  }

    /**
     * set the diatance from the element to the left, depending on the width of the page
     */
  getLeft (clientWidth, currentTarget) {
    let left = 0
    if (clientWidth <= 1440) {
      left = currentTarget.offsetLeft % 980
    } else {
      left = currentTarget.offsetLeft % 1280
    }
    return left
  }

    /**
     * set the diatance from the element to the top
     */
  getTop (type, currentTarget, susHeight, clientWidth) {
    let top = 0
    if (type === 'recmVod') {
      top = (currentTarget.offsetHeight - susHeight) / 2 + 97
    } else {
      top = (currentTarget.offsetHeight - susHeight) / 2 + 44
    }

    if (currentTarget.offsetTop !== 0) {
      if (clientWidth <= 1440) {
        top = top + currentTarget.offsetHeight + 20
      } else {
        top += currentTarget.offsetHeight
      }
    }
    return top
  }

    /**
     * call the interface to get details of the vod
     */
  getVodDetail (req: any) {
    return getVODDetail(req).then(resp => resp)
  }

    /**
     * classify the left time of subscribe, there are more then 7 days, 2~7 days, 2hour~48hour, 0min~120min, exceed the time limit
     * list：the subscribe product message
     */
  timeClassify (list) {
    let nowTime = Date.now()['getTime']()
    let leftTime = Number(list['endTime']) - Number(nowTime)
    if (leftTime > 604800000) {  // >7days show icon
      list['leftTime'] = Math.ceil(leftTime / 86400000) + ''
      list['leftTimeType'] = 'moreDays'
    } else if (leftTime > 172800000) {  // >48hours show day
      list['leftTimeType'] = 'Days'
      list['leftTime'] = Math.ceil(leftTime / 86400000) + ''
    } else if (leftTime > 7200000) {  // >120mins show hour
        list['leftTimeType'] = 'Hours'
        list['leftTime'] = Math.ceil(leftTime / 3600000) + ''
      } else if (leftTime > 0) {  // >0 show min
          list['leftTimeType'] = 'Mins'
          list['leftTime'] = Math.ceil(leftTime / 60000) + ''
        } else if (leftTime <= 0) {  // <=0 show expired
          list['leftTimeType'] = 'Expired'
          list['leftTime'] = '0'
        }
  }

    /**
     * judge whether to support CPVR
     */
  judgeIsSupportCPVR (recordCRType, btvCR) {
    let isSupportCPVR = recordCRType && recordCRType['enable'] === '1' && recordCRType['isSubscribed'] === '1'
            && recordCRType['isContentValid'] === '1' && recordCRType['isValid'] === '1'
            && btvCR && btvCR['enable'] === '1' && btvCR['isSubscribed'] === '1'
            && btvCR['isContentValid'] === '1' && btvCR['isValid'] === '1'
    return isSupportCPVR
  }

    /**
     * judge whether the channel support CPVR, judge whether the channel support definition under the physical channel in order
     * physicalChannel：the message of physicalChannelsDynamicProperties
     */
  isQualitySupport (physicalChannel) {
    let recordCRType = physicalChannel['cpvrRecordCR']
    let btvCR = physicalChannel['btvCR']
    let isSupportCPVR = this.judgeIsSupportCPVR(recordCRType, btvCR)
    if (isSupportCPVR) {
      return true
    } else {
      return false
    }
  }

    /**
     * filtering the channel which support CPVR
     */
  judgeRecordDynamic (recordCR, channel): any {
    let self = this
    let channels = _.filter(channel['physicalChannelsDynamicProperties'], function (physicalChannels) {
      let recordCRType = physicalChannels[recordCR]
      let btvCR = physicalChannels['btvCR']
      let isSupportCPVR = self.judgeIsSupportCPVR(recordCRType, btvCR)
      if (isSupportCPVR) {
        return recordCRType
      }
    })
    return channels
  }

  commonLog (interfaceName, param) {
    let errorJson = _.extend(require('../../assets/i18n/' + 'log.json'))
    let errorSource = errorJson['messageResource']['MSA']
    let codeKey = 'MSA.default'
    let errorObject = errorSource && errorSource[codeKey]
    let message = errorObject && errorObject['m']
    log.error('[ERROR_CODE] {message: %s}', message)
  }

    /**
     * log the related MSA_ERROR information according to the error code.
     */
  MSAErrorLog (errorCode: number, interfaceName?: string, extraMsg?: string) {
    let terminalType = '6'
    let errorJson = _.extend(require('../../assets/i18n/' + 'log.json'))
    let errorSource = errorJson['messageResource']['MSA']
    let codeKey = 'MSA.' + errorCode
    let errorObject = errorSource && errorSource[codeKey]
    let message = errorObject && errorObject['m']
        // console.log(log);
    if (!_.isUndefined(interfaceName)) {
            // log.error('[ERROR_CODE] %s {interface: %s, message: %s}', terminalType + errorCode, interfaceName, message);
    } else {
            // log.error('[ERROR_CODE] %s {message: %s}', terminalType + errorCode, message);
    }
  }

    /**
     * reset the profile data of local storage.
     */
  resetSessionProfile (curProfile) {
    let currencySymbol = JSON.parse(localStorage.getItem('CURRENCY_SYMBOL'))
    let currencyRate = JSON.parse(localStorage.getItem('CURRENCY_RATE'))
    let quotaValue
    if (curProfile['quota'] === '' || curProfile['quota'] === undefined || curProfile['quota'] === null) {
      quotaValue = ''
    } else {
      if (curProfile['quota'] === '-1') {
        quotaValue = 'unlimited'
      } else {
        quotaValue = currencySymbol + Number(curProfile['quota']) / Number(currencyRate)
      }
    }
    let nowProfile = {
      id: 'profile',
      profileId: curProfile['ID'],
      isAdmin: curProfile['profileType'] === '0',
      loginName: curProfile['loginName'],
      name: curProfile['name'],
      avatar: curProfile['logoURL'] ? 'assets/img/avatars/' + curProfile['logoURL'] + '.png' :
                'assets/img/default/ondemand_casts.png',
      level: curProfile['ratingName'] || '',
      levelId: curProfile['ratingID'],
      quota: quotaValue,
      profileType: curProfile['profileType']
    }
    localStorage.setItem('curProfile', JSON.stringify(nowProfile))
  }
    /**
     * translate months from numbers into English abbreviations
     */
  dealWithTime (month) {
    let Month
    if (month === '01') {
      Month = 'jan_tvguide'
    } else if (month === '02') {
      Month = 'feb_tvguide'
    } else if (month === '03') {
        Month = 'mar_tvguide'
      } else if (month === '04') {
          Month = 'apr_tvguide'
        } else if (month === '05') {
          Month = 'may_tvguide'
        } else if (month === '06') {
          Month = 'jun_tvguide'
        } else if (month === '07') {
          Month = 'jul_tvguide'
        } else if (month === '08') {
          Month = 'aug_tvguide'
        } else if (month === '09') {
          Month = 'sep_tvguide'
        } else if (month === '10') {
          Month = 'oct_tvguide'
        } else if (month === '11') {
          Month = 'nov_tvguide'
        } else if (month === '12') {
          Month = 'dec_tvguide'
        }
    Month = this.translate.instant(Month)
    return Month
  }

    /**
     * formatting time according to language
     */
  dealWithOrgTime (orgTime) {
    orgTime = orgTime.split('.')
    orgTime[1] = this.dealWithTime(orgTime[1])
    if (orgTime[0].charAt(0) === '0') {
      orgTime[0] = orgTime[0].charAt(1)
    }
    let date1
    let date2
    if (session.get('languageName') === 'zh') {
      date1 = orgTime[2] + '/' + orgTime[1] + '/' + orgTime[0]
      return date1
    } else {
      date2 = orgTime[1] + ' ' + orgTime[0] + ', ' + orgTime[2]
      return date2
    }
  }

    /**
     * format date
     */
  dealDateToNumber (date) {
    if (session.get('languageName') === 'zh') {
      date = date.split('/')
      date[1] = this.addZero(date[1])
      date[2] = this.addZero(date[2])

      return date[2] + '.' + date[1] + '.' + date[0]
    } else {
      date = date.split(' ')
      date[1] = date[1].replace(',', '')
      if (date[1].length === 1) {
        date[1] = '0' + date[1]
      }
      if (date[0] === 'Jan') {
        date[0] = '01'
      } else if (date[0] === 'Feb') {
          date[0] = '02'
        } else if (date[0] === 'Mar') {
            date[0] = '03'
          } else if (date[0] === 'Apr') {
              date[0] = '04'
            } else if (date[0] === 'May') {
              date[0] = '05'
            } else if (date[0] === 'Jun') {
              date[0] = '06'
            } else if (date[0] === 'Jul') {
              date[0] = '07'
            } else if (date[0] === 'Aug') {
              date[0] = '08'
            } else if (date[0] === 'Sep') {
              date[0] = '09'
            } else if (date[0] === 'Oct') {
              date[0] = '10'
            } else if (date[0] === 'Nov') {
              date[0] = '11'
            } else if (date[0] === 'Dec') {
              date[0] = '12'
            }
      return date = date[1] + '.' + date[0] + '.' + date[2]
    }
  }

    /**
     * add 0 before the date of less than 10
     */
  addZero (date) {
    if (parseInt(date, 10) < 10) {
      date = '0' + date
    }
    return date
  }

    /**
     * format millisecond string
     */
  dealWithMillisecondTime (time) {
    if (time) {
      let month = DateUtils.format(time, 'MM')
      month = this.dealWithTime(month)
      let day = DateUtils.format(time, 'D')
      let year = DateUtils.format(time, 'YYYY')
      if (session.get('languageName') === 'zh') {
        return year + '/' + month + '/' + day
      } else {
        return month + ' ' + day + ', ' + year
      }
    }
  }

    /**
     * get the time slot
     */
  dealWithPlaybillTimeReduce (timeOne, timeTwo) {
    let oneMin = 1000 * 60
    timeOne = Math.floor(timeOne / oneMin)
    timeTwo = Math.floor(timeTwo / oneMin)
    return timeTwo - timeOne
  }

  dealWithVODTimeReduce (time) {
    let oneMin = 1000 * 60
    time = Math.ceil(time / oneMin)
    return time
  }

    /**
     * deal with date
     */
  dealWithDateJson (dateType, timeStr) {
    if (!dateType || !timeStr) {
      return ''
    }
    let dateStrJson = session.get('DATESTRJSON')
    let dateStr
    for (let i = 0; i < dateStrJson['date'].length; i++) {
      if (dateStrJson['date'][i]['name'] === dateType) {
        dateStr = dateStrJson['date'][i]['value']
      }
    }

    let weekTime = DateUtils.format(parseInt(timeStr, 10), 'ddd')
    weekTime = this.translate.instant(weekTime)
    let weekTimeAll = DateUtils.format(parseInt(timeStr, 10), 'dddd')
    weekTimeAll = this.translate.instant(weekTimeAll)
    let month = DateUtils.format(parseInt(timeStr, 10), 'M')
    let monthZero = DateUtils.format(parseInt(timeStr, 10), 'MM')
    let monthSimple = DateUtils.format(parseInt(timeStr, 10), 'MMM')
    monthSimple = this.translate.instant(monthSimple)
    let monthAll = DateUtils.format(parseInt(timeStr, 10), 'MMMM')
    monthAll = this.translate.instant(monthAll)
    let day = DateUtils.format(parseInt(timeStr, 10), 'D')
    let zeroDay = DateUtils.format(parseInt(timeStr, 10), 'DD')
    let year = DateUtils.format(parseInt(timeStr, 10), 'YYYY')
    let hour12 = DateUtils.format(parseInt(timeStr, 10), 'h')
    let hour24 = DateUtils.format(parseInt(timeStr, 10), 'H')
    let hour12Two = DateUtils.format(parseInt(timeStr, 10), 'hh')
    let hour24Two = DateUtils.format(parseInt(timeStr, 10), 'HH')
    let minute = DateUtils.format(parseInt(timeStr, 10), 'm')
    let minuteZero = DateUtils.format(parseInt(timeStr, 10), 'mm')
    let second = DateUtils.format(parseInt(timeStr, 10), 's')
    let secondZero = DateUtils.format(parseInt(timeStr, 10), 'ss')
    let AmOrPm

    dateStr = dateStr.replace(/{d}/g, day)
    dateStr = dateStr.replace(/{dd}/g, zeroDay)
    dateStr = dateStr.replace(/{ddd}/g, weekTime)
    dateStr = dateStr.replace(/{dddd}/g, weekTimeAll)
    dateStr = dateStr.replace(/{m}/g, month)
    dateStr = dateStr.replace(/{mm}/g, monthZero)
    dateStr = dateStr.replace(/{mmm}/g, monthSimple)
    dateStr = dateStr.replace(/{mmmm}/g, monthAll)
    dateStr = dateStr.replace(/{yyyy}/g, year)
    dateStr = dateStr.replace(/{yy}}/g, year.substring(year.length - 1, year.length))
    dateStr = dateStr.replace(/{H12}/g, hour12)
    dateStr = dateStr.replace(/{H24}/g, hour24)
    dateStr = dateStr.replace(/{hh12}/g, hour12Two)
    dateStr = dateStr.replace(/{hh24}/g, hour24Two)
    dateStr = dateStr.replace(/{N}/g, minute)
    dateStr = dateStr.replace(/{nn}/g, minuteZero)
    dateStr = dateStr.replace(/{s}/g, second)
    dateStr = dateStr.replace(/{ss}/g, secondZero)
    if (Number(hour24) > 12) {
      AmOrPm = this.translate.instant('times_pm')
    } else {
      AmOrPm = this.translate.instant('times_am')
    }
    dateStr = dateStr.replace(/{AP}/g, AmOrPm)

    if (dateStr.indexOf(' DST') > -1) {
      dateStr = dateStr.replace(/ DST/g, '')
      dateStr = dateStr + ' DST'
    }
    return dateStr
  }
}
