import { Injectable, ViewContainerRef, Type } from '@angular/core'
import { DialogService, DialogConfig, DialogRef, DialogParams } from '../../../ng-epg-ui/src/webtv-components/dialog/index'
import { TranslateService } from '@ngx-translate/core'
import { MediaPlayService } from '../component/mediaPlay/mediaPlay.service'
import { EventService } from 'ng-epg-sdk'

@Injectable()
export class ViewTransferService {

  private _viewContainerRef: ViewContainerRef
  private _translateService: TranslateService

  constructor (
        private mediaPlayService: MediaPlayService,
        private dialogService: DialogService
    ) { }

  get viewContainerRef () {
    return this._viewContainerRef
  }

  set viewContainerRef (vcr: ViewContainerRef) {
    this._viewContainerRef = vcr
  }

  get translateService () {
    return this._translateService
  }

  set translateService (ts: TranslateService) {
    this._translateService = ts
  }

  loginPlay () {
    let ua = navigator.userAgent.toLowerCase()
    let isIe = !!ua.match(/msie/i) || !!ua.match(/rv:([\d.]+).*like gecko/)
    let isEdge = !!ua.match(/edge\/([\d.]+)/)
    let isFirefox = !!ua.match(/firefox\/([\d.]+)/)
    if (isIe || isEdge || isFirefox) {
      let channelvideo = document.querySelector('#videoContainer')
      let vodVideo = document.querySelector('#vodVideoContainer')
      let MediaplayerChannel = this.mediaPlayService.isFullScreen()
      let MediaplayerVod = this.mediaPlayService.isFullScreen()
            // TODO: Think how this affects non-player pages
      if (MediaplayerChannel && (location.pathname.indexOf('video') !== -1 || location.pathname.indexOf('movie') !== -1)) {
        channelvideo.appendChild(document.querySelector('md-dialog-container'))
      } else if (MediaplayerVod) {
          vodVideo.appendChild(document.querySelector('md-dialog-container'))
        }
    }
  }

    /**
     * open dialog
     */
  openDialog (component: Type<any>, config?: DialogConfig, params?: DialogParams) {

    // WORKAROUND
    if (component.name === 'AccountLoginComponent') {
      EventService.emit('AUTHENTICATE_OPEN_DIALOG')
      return Promise.resolve()
    }

    config = (config || new DialogConfig())
            .parent(document.querySelector('main') as HTMLElement)
            .clickOutsideToClose(false)
            .containerCss('page')
    return this.dialogService.open(component,
            this._viewContainerRef, config, [{ provide: TranslateService, useValue: this.translateService }], params)
            .then((ref: DialogRef) => {
              if (component.name === 'AccountLoginComponent') {
                this.LoginDialgStyle('app-account-login')
                this.loginPlay()
              } else if (component.name === 'AccountRegisterComponent') {
                this.LoginDialgStyle('app-account-register')
              } else if (component.name === 'ResetPasswordComponent') {
                this.LoginDialgStyle('app-reset-password')
              } else if (component.name === 'ErrorMessageDialogComponent') {
                  this.LoginDialgStyle('app-error-message-dialog')
                } else if (component.name === 'PassInputDialogComponent') {
                  this.LoginDialgStyle('app-pass-input-dialog')
                } else if (component.name === 'RecordDialogComponent') {
                  this.LoginDialgStyle('app-record-dialog')
                } else if (component.name === 'PreferencesHomeComponent') {
                  this.LoginDialgStyle('app-preferences-home')
                } else if (component.name === 'StopRecordConfirmComponent') {
                  this.LoginDialgStyle('app-stop-record-confirm')
                  this.loginPlay()
                } else if (component.name === 'StorageRemindComponent') {
                  this.LoginDialgStyle('app-storageremind')
                } else if (component.name === 'DeviceReplaceComponent') {
                  this.LoginDialgStyle('app-device-replace')
                } else if (component.name === 'ReminderComponent') {
                  this.LoginDialgStyle('app-reminder')
                } else if (component.name === 'RecordingConflictComponent') {
                  this.LoginDialgStyle('app-recording-conflict')
                } else if (component.name === 'MixedConflictComponent') {
                  this.LoginDialgStyle('app-mixed-conflict')
                }
              return ref.whenClosed
            })
  }

    /**
     * set style of dialog
     */
  LoginDialgStyle (tag) {
    let livetvVideoWidth = window.screen.width + 'px'
    let livetvVideoHeight = window.screen.height + 'px'
    if (document.getElementsByTagName(tag)[0] && document.getElementsByTagName('md-dialog-container')[0]) {
      document.getElementsByTagName(tag)[0]['style']['position'] = 'relative'
      document.getElementsByTagName(tag)[0]['style']['background'] = 'rgba(0, 0, 0, 0.7)'
      document.getElementsByTagName(tag)[0]['style']['width'] = livetvVideoWidth
      document.getElementsByTagName(tag)[0]['style']['height'] = livetvVideoHeight
      document.getElementsByTagName(tag)[0]['style']['display'] = 'block'
      document.getElementsByTagName('md-dialog-container')[0]['style'].position = 'fixed'
      document.getElementsByTagName('md-dialog-container')[0]['style']['z-index'] = '2200000000'
      if (tag === 'app-account-login' && document.getElementsByClassName(tag)[0]) {
        document.getElementsByClassName(tag)[0]['style']['top'] = (window.screen.height - 367) / 2 + 'px'
        return
      }
      if (tag === 'app-account-register' && document.getElementsByClassName(tag)[0]) {
        document.getElementsByClassName(tag)[0]['style']['top'] =
                    (window.screen.height - (document.getElementsByClassName(tag)[0] as HTMLElement).offsetHeight) / 2 + 'px'
        return
      }
      if (tag === 'app-reset-password' && document.getElementsByClassName(tag)[0]) {
        document.getElementsByClassName(tag)[0]['style']['top'] =
                    (window.screen.height - (document.getElementsByClassName(tag)[0] as HTMLElement).offsetHeight) / 2 + 'px'
        return
      }
      if (tag === 'app-device-replace' && document.getElementsByClassName(tag)[0]) {
        document.getElementsByClassName(tag)[0]['style']['top'] = (window.screen.height - 288) / 2 + 'px'
        return
      }
      if (tag !== 'app-storageremind' && document.getElementsByClassName(tag)[0]) {
        document.getElementsByClassName(tag)[0]['style']['top'] = (window.screen.height - 331) / 2 + 'px'
      }
    }
  }

}
