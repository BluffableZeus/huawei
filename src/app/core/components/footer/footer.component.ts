import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { config as EPGConfig } from '../../../shared/services/config';
import { Router } from '@angular/router';
import { session } from 'src/app/shared/services/session';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  links = {
    privacy: EPGConfig.privacyLink,
    softwareLicense: EPGConfig.softwareLicenseLink,
    faq: EPGConfig.FAQLink,
    feedbackEmail: `mailto:${EPGConfig.feedbackEmail}?subject=${this.translate.instant('feedback_mail_subject')}&body=${this.translate.instant('feedback_mail_body')}`
  }
  constructor(
    private translate: TranslateService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  /**
   *skip router message
  */
  skipRouteMessage (data) {
    document.body.scrollTop = 0
    this.router.navigate(['./bottom/', data])
    session.put('RouteMessage', './bottom/' + data)
  }
}
