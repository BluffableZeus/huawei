/* @author: @AngularClass */

const webpack = require('webpack');
const helpers = require('./helpers');

/* Webpack Plugins*/
// problem with copy-webpack-plugin
const AssetsPlugin = require('assets-webpack-plugin');
const ContextReplacementPlugin = require('webpack/lib/ContextReplacementPlugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const CheckerPlugin = require('awesome-typescript-loader').CheckerPlugin;
const HtmlElementsPlugin = require('./html-elements-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const LoaderOptionsPlugin = require('webpack/lib/LoaderOptionsPlugin');
const ScriptExtHtmlWebpackPlugin = require('script-ext-html-webpack-plugin');
const CircularDependencyPlugin = require('circular-dependency-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')

/* Webpack Constants */
const HMR = helpers.hasProcessFlag('hot');
const AOT = process.env.BUILD_AOT || helpers.hasNpmFlag('aot');
const METADATA = {
  title: 'MTC-TB',
  baseUrl: process.env.BASE_URL || '/',
  isDevServer: helpers.isWebpackDevServer()
};

const GLOBAL_CONFIG = {
  ocURL: `"${process.env.OC_URL || ''}"`,
  crmURL: `"${process.env.CRM_URL || 'http://try2.mediastage.tv'}"`
}

/* Webpack configuration
 *
 * See: http://webpack.github.io/docs/configuration.html#cli
 */
module.exports = function (options) {
  const env = options.env;

  return {

    mode: 'development',

    /* Cache generated modules and chunks to improve performance for multiple incremental builds.
     * This is enabled by default in watch mode.
     * You can pass false to disable it.
     *
     * See: http://webpack.github.io/docs/configuration.html#cache
     */
    //cache: false,

    /* The entry point for the bundle
     * Our Angular.js app
     *
     * See: http://webpack.github.io/docs/configuration.html#entry
     */
    entry: {
        polyfills: './src/polyfills.ts',
        vendor: './src/vendor.ts',
        main: AOT ? './src/main.aot.ts' : './src/main.ts',
    },

    /*
     * Options affecting the resolving of modules.
     *
     * See: http://webpack.github.io/docs/configuration.html#resolve
     */
    resolve: {

      alias: {
        '/src/assets': helpers.root('src/assets'),
        '/src/share': helpers.root('src/share')
      },

      /* An array of extensions that should be used to resolve modules.
       *
       * See: http://webpack.github.io/docs/configuration.html#resolve-extensions
       */
      extensions: ['.ts', '.js', '.json'],

      // An array of directory names to be resolved to the current directory
      modules: [helpers.root('src'), 'node_modules', helpers.root()]
    },

    /* Options affecting the normal modules.
     *
     * See: http://webpack.github.io/docs/configuration.html#module
     */
    module: {

      rules: [

        /* to string and css loader support for *.css files
         * Returns file content as string
         */
        {
          test: /\.css$/,
          use: [
            {
              loader: MiniCssExtractPlugin.loader
            },
            {
              loader: 'css-loader'
            }
          ]
        }, {
          exclude: [
            "../src/styles.scss",
            "../ng-epg-ui/src/webtv-components/assets/css/common-variables.scss"
          ],
          test: /\.scss$|\.sass$/,
          use: [
            {
              loader: 'raw-loader'
            },
            {
              loader: 'postcss-loader'
            },
            {
              loader: 'sass-loader'
            }
          ]
        },

        /* Raw loader support for *.html
         * Returns file content as string
         *
         * See: https://github.com/webpack/raw-loader
         */
        {
          test: /\.html$/,
          loader: 'raw-loader',
          exclude: [helpers.root('src/index.html')]
        },

        // File loader for supporting images, for example, in CSS files.
        {
          test: /\.(jpg|png|gif)$/,
          loader: 'file'
        },

      ],

    },

    optimization: {
      runtimeChunk: 'single',
      splitChunks: {
        cacheGroups: {
          vendor: {
            test: /[\\/]node_modules[\\/]/,
            name: 'vendor',
            chunks: 'all'
          },
          assets: {
            test: /DMPPlayer/,
            name: 'player',
            chunks: 'all'
          }
        }
      }
    },

    /* Add additional plugins to the compiler.
     *
     * See: http://webpack.github.io/docs/configuration.html#plugins
     */
    plugins: [

      new MiniCssExtractPlugin({
        // Options similar to the same options in webpackOptions.output
        // both options are optional
        filename: "[name].css",
        chunkFilename: "[id].css"
      }),

      new CircularDependencyPlugin({
        // exclude detection of files based on a RegExp
        exclude: /a\.js|node_modules/,
        // add errors to webpack instead of warnings
        failOnError: false,
        // allow import cycles that include an asyncronous import,
        // e.g. via import(/* webpackMode: "weak" */ './file.js')
        allowAsyncCycles: false,
        // set the current working directory for displaying module paths
        cwd: process.cwd(),
      }),

      new webpack.NormalModuleReplacementPlugin(
          /(.*)\/core\/constants\/(.*)\.constant/,
          resource => {
              resource.request = resource.request.replace(/core\/constants\//, `core/constants/env/${env}/`);
          }
      ),
      new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /ru/),

      new AssetsPlugin({
        path: helpers.root('build/dist'),
        filename: 'webpack-assets.json',
        prettyPrint: true
      }),

      /* Plugin: CheckerPlugin
       * Description: Do type checking in a separate process, so webpack don't need to wait.
       *
       * See: https://github.com/s-panferov/awesome-typescript-loader#forkchecker-boolean-defaultfalse
       */
      new CheckerPlugin(),
      /* Plugin: ContextReplacementPlugin
       * Description: Provides context to Angular's use of System.import
       *
       * See: https://webpack.github.io/docs/list-of-plugins.html#contextreplacementplugin
       * See: https://github.com/angular/angular/issues/11580
       */
      new ContextReplacementPlugin(
        // The (\\|\/) piece accounts for path separators in *nix and Windows
        /angular(\\|\/)core(\\|\/)(esm(\\|\/)src|src)(\\|\/)linker/,
        helpers.root('src') // location of your src
      ),

      /* Plugin: CopyWebpackPlugin
       * Description: Copy files and directories in webpack.
       *
       * Copies project static assets.
       *
       * See: https://www.npmjs.com/package/copy-webpack-plugin
       */
      new CopyWebpackPlugin([
          {
              from: 'ng-epg-ui/src/webtv-components/assets',
              ignore: 'ng-epg-ui/src/webtv-components/assets/**/*.scss',
              to: 'assets'
          },
          {
              from: 'src/assets',
              ignore: 'src/assets/**/*.scss',
              to: 'assets',
          },
          {
              from:'src/mobiles',
              to:'.well-known'
          }
      ]),

      /* Plugin: HtmlWebpackPlugin
       * Description: Simplifies creation of HTML files to serve your webpack bundles.
       * This is especially useful for webpack bundles that include a hash in the filename
       * which changes every compilation.
       *
       * See: https://github.com/ampedandwired/html-webpack-plugin
       */
      new HtmlWebpackPlugin({
        template: 'src/index.html',
        title: METADATA.title,
        chunksSortMode: 'auto',
        metadata: METADATA,
        inject: 'head'
      }),

      /* Plugin: ScriptExtHtmlWebpackPlugin
       * Description: Enhances html-webpack-plugin functionality
       * with different deployment options for your scripts including:
       *
       * See: https://github.com/numical/script-ext-html-webpack-plugin
       */
       // Disabled for HtmlWebpackPlugin 4.+
      //new ScriptExtHtmlWebpackPlugin({
        //sync: ['config.js'],
        //defaultAttribute: 'defer'
      //}),

      /* Plugin: HtmlElementsPlugin
       * Description: Generate html tags based on javascript maps.
       *
       * If a publicPath is set in the webpack output configuration, it will be automatically added to
       * href attributes, you can disable that by adding a "=href": false property.
       * You can also enable it to other attribute by settings "=attName": true.
       *
       * The configuration supplied is map between a location (key) and an element definition object (value)
       * The location (key) is then exported to the template under then htmlElements property in webpack configuration.
       *
       * Example:
       *  Adding this plugin configuration
       *  new HtmlElementsPlugin({
       *    headTags: { ... }
       *  })
       *
       *  Means we can use it in the template like this:
       *  <%= webpackConfig.htmlElements.headTags %>
       *
       * Dependencies: HtmlWebpackPlugin
       */
       // Disabled for HtmlWebpackPlugin 4.+
      //new HtmlElementsPlugin({
        //headTags: require('./head-config.common')
      //}),

      /* Plugin LoaderOptionsPlugin (experimental)
       *
       * See: https://gist.github.com/sokra/27b24881210b56bbaff7
       */
      new LoaderOptionsPlugin({}),


      new webpack.DefinePlugin({
        GLOBAL_CONFIG: GLOBAL_CONFIG
      })
    ],

    /* Webpack Development Server configuration
     * Description: The webpack-dev-server is a little node.js Express server.
     * The server emits information about the compilation state to the client,
     * which reacts to those events.
     *
     * See: https://webpack.github.io/docs/webpack-dev-server.html
     */
    devServer: {
      port: METADATA.port,
      host: METADATA.host,
      historyApiFallback: {
        disableDotRule: true
      },
      watchOptions: {
        aggregateTimeout: 300,
        poll: 1000
      },
      proxy: {
        '/VSP/V3': {
          target: 'https://tv-test.rhrn.ru:443',
          changeOrigin: true,
          secure: true
        },
        '/v1': {
            target: 'https://oc.tnt-premier.ru',
            changeOrigin: true,
            secure: true
        },
        '/yandex': {
            target: 'https://oc.tnt-premier.ru',
            changeOrigin: true,
            secure: true
        }
      }
    },

    /* Include polyfills or mocks for various node stuff
     * Description: Node configuration
     *
     * See: https://webpack.github.io/docs/configuration.html#node
     */
    node: {
      global: true,
      crypto: 'empty',
      process: true,
      module: false,
      clearImmediate: false,
      setImmediate: false
    }

  };
}
