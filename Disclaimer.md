##免责声明
华为所提供的包括但不限于SampleCode/DemoCode/文档中的源代码、示例、注释等知识产权为华为所有（使用的开源软件除外），示例代码仅供第三方客户端厂商用作开发时的参考，不建议在项目中直接使用，华为不保证该代码在前后版本中的兼容性，不承担代码使用后的问题和风险。其它未尽事宜以与华为签署的协议为准。

##Disclaimer
Disclaimer About Source Code of the Resources for Download in Developer Community Huawei possesses the intellectual property of, including but not limited to, source code, examples, and comments in the SampleCode, DemoCode, and documents (excluding open-source software). The sample code is only reference for third-party client vendors to develop software, and therefore the code is not recommended to be directly used in projects. Huawei shall not have any obligation to guarantee the compatibility of the code in different versions, and shall not be responsible for the problems or risks that may occur during or after the code is used. Other issues that are not covered shall be subject to the agreement signed with Huawei.
