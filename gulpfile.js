var gulp = require('gulp');
var replace = require('gulp-replace-task');
var rimraf = require('rimraf');
var rename = require('gulp-rename');
var clean = require('gulp-clean');
var _ = require('underscore');
var Stream = require('stream');
var zip = require('gulp-zip');
var mergeJson = require('gulp-merge-json');
var fs = require('fs');
var mergeStream = require('merge-stream');
var path = require('path');

gulp.task('i18n', function () {
  var fs = require('fs');
  var ii8nPath = 'src/assets/i18n/';
  var streams = [];

  var dirs = fs.readdirSync(ii8nPath).filter(function (file) {
    return fs.statSync(ii8nPath + '/' + file).isDirectory();
  });

  _.each(_.without(dirs, 'shared'), function (lang) {
    var stream = gulp.src([ii8nPath + lang + '/**/*.json', ii8nPath + 'shared/**/*.json'])
      .pipe(mergeJson({
        jsonSpace: '  ',
        fileName: `${lang}.json`,
      }))
      .pipe(gulp.dest(ii8nPath));
    streams.push(stream);
  });
  return mergeStream.apply(null, streams);
});

gulp.task('whitei18n', function () {
  var deepParse = function(dict) {
    if (dict.content) {
      return dict.content;
    } else {
      return _.mapObject(dict, (val) => {
        return deepParse(val);
      });
    }
  };

  var fs = require('fs');
  var ii8nPath = 'src/assets/i18n/';
  var streams = [];

  var dirs = fs.readdirSync(ii8nPath).filter(function (file) {
    return fs.statSync(ii8nPath + '/' + file).isDirectory();
  });

  _.each(_.without(dirs, 'shared'), function (lang) {
    var stream = gulp.src([ii8nPath + lang + '/**/*.json', ii8nPath + 'shared/**/*.json'])
      .pipe(mergeJson({
        jsonSpace: '  ',
        fileName: `${lang}.json`,
        edit: (parsedJson, file) => {
          if (path.basename(file.history.toString()) === `${lang}.json`) {
            parsedJson = deepParse(parsedJson);
            return parsedJson;
          } else {
            return parsedJson;
          }
        }
      }))
      .pipe(gulp.dest(ii8nPath));
    streams.push(stream);
  });
  return mergeStream.apply(null, streams);
});

gulp.task('repareTslintReporter', function() {
    gulp.src('build/reporters/tslint.checkstyle.xml')
        .pipe(replace({
            patterns: [{
                match: /<\/checkstyle><\?xml version=\"1.0\" encoding=\"utf-8\"\?><checkstyle version=\"4.3\">/g,
                replacement: '',
            }]
        }))
        .pipe(replace({
            patterns: [{
                match: /^$/,
                replacement: '<?xml version="1.0" encoding="utf-8"?><checkstyle version="4.3"></checkstyle>',
            }]
        }))
        .pipe(clean({
            force: true
        }))
        .pipe(gulp.dest('build/reporters/'));

});

gulp.task('reset-config', function () {
  return gulp.src(['build/dist/config.bundle.*.js'])
    .pipe(replace({
      patterns: [{
        match: /'logLevel'.*?,/,
        replacement: '\'logLevel\':\'error\',',
      }]
    }))
    .pipe(replace({
      patterns: [{
        match: /'vspUrl'.*?,/,
        replacement: '\'vspUrl\':\'\',',
      }]
    }))
    .pipe(replace({
      patterns: [{
        match: /'vspHttpsUrl'.*?,/,
        replacement: '\'vspHttpsUrl\':\'\',',
      }]
    }))
    .pipe(replace({
      patterns: [{
        match: /'signatureKeyAlgorithm'.*?,/,
        replacement: '\'signatureKeyAlgorithm\':\'\',',
      }]
    }))
    .pipe(gulp.dest('build/dist'));
});

gulp.task('copy', ['reset-config'], () => {
  return mergeStream(
    gulp.src('build/dist/**')
      .pipe(gulp.dest('build/.tmp/V6WEBTV')),
    gulp.src('build/dist_debug/**')
      .pipe(gulp.dest('build/.tmp/V6WEBTV_DEBUG')));
});

gulp.task('zip', function () {
  return mergeStream(
    gulp.src('build/.tmp/V6WEBTV/**', {})
      .pipe(zip('V6WEBTV.zip', {
        compress: true
      }))
      .pipe(clean({
        force: true
      }))
      .pipe(gulp.dest('build')),
    gulp.src('build/.tmp/V6WEBTV_DEBUG/**', {})
      .pipe(zip('V6WEBTV_DEBUG.zip', {
        compress: true
      }))
      .pipe(clean({
        force: true
      }))
      .pipe(gulp.dest('build'))
  );
});

gulp.task('package', ['copy'], function (cb) {
  gulp.start('zip', cb);
});

function countTestCase() {

    var stream = new Stream.Transform({
        objectMode: true
    });

    stream._transform = function(file, unused, callback) {
        var fileStr = new Buffer(file.contents).toString();
        var matchs;
        var reg = /\s(\w{1,3}\d+)\s/ig;
        var ids = [];
        while (matchs = reg.exec(fileStr)) {
            ids.push(matchs[0].trim());
        }
        var outputList = _.map(_.countBy(ids), function(count, id) {
            return id + '\t' + count;
        });

        outputList.sort();

        var outputStr = outputList.join('\n');
        file.contents = new Buffer(outputStr);
        callback(null, file);
    };

    return stream;
}

gulp.task('testCaseCountReporter', function() {
    gulp.src('build/reporters/unit-test/junit.xml')
        .pipe(countTestCase())
        .pipe(rename('testCaseCount.txt'))
        .pipe(gulp.dest('build/reporters/unit-test'));
});

gulp.task('default', ['repareTslintReporter']);
