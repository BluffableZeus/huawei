import { IDB } from './../idb'
import * as _ from 'underscore'

describe('IDB', () => {

  let idb: IDB<any>

  const dataList = _.times(100, (i) => {
      return { id: i, name: i }
    })

  beforeAll((done) => {
      idb = new IDB<any>({
          storeName: 'playbill',
          keyPath: 'id',
          dbVersion: Date.now(),
          indexes: [{
              name: 'nameIndex',
              keyPath: 'name',
              unique: true,
              multiEntry: false
            }]
        })

      idb.put(dataList).then(done)
    })

  it('put & get object', (done) => {
      idb.get(1).then((result) => {
          expect(result).toEqual({ id: 1, name: 1 })
          done()
        }, () => {
          fail()
          done()
        })
    })

  it('query object bound', (done) => {
      const promise = idb.queryList({
          index: 'nameIndex',
          keyRange: IDBKeyRange.bound(0, 5)
        })

      promise.then((result: Array<any>) => {
          expect(result.length).toEqual(6)
          done()
        }, (resp) => {
          fail(JSON.stringify(resp))
          done()
        })
    })

  it('query object lowerBound', (done) => {
      const promise = idb.queryList({
          index: 'nameIndex',
          keyRange: IDBKeyRange.lowerBound(10),
          count: 10
        })

      promise.then((result: Array<any>) => {
          expect(result).toEqual(dataList.slice(10, 20))
          done()
        }, (resp) => {
          fail(JSON.stringify(resp))
          done()
        })
    })

  it('query object context 1', (done) => {
      const promise = idb.queryContext({
          index: 'nameIndex',
          key: 10
        })

      promise.then((resp) => {
          expect(resp.current).toEqual({ id: 10, name: 10 })
          done()
        }, (resp) => {
          fail(JSON.stringify(resp))
          done()
        })
    })

  it('query object context 2', (done) => {
      const promise = idb.queryContext({
          index: 'nameIndex',
          key: 10,
          nextNumber: 2
        })

      promise.then((resp) => {
          expect(resp.current).toEqual({ id: 10, name: 10 })
          expect(resp.nextList).toEqual([{ id: 11, name: 11 }, { id: 12, name: 12 }])
          done()
        }, (resp) => {
          fail(JSON.stringify(resp))
          done()
        })
    })

  it('query object context 3', (done) => {
      const promise = idb.queryContext({
          index: 'nameIndex',
          key: 10,
          preNumber: 2
        })

      promise.then((resp) => {
          expect(resp.current).toEqual({ id: 10, name: 10 })
          expect(resp.preList).toEqual([{ id: 8, name: 8 }, { id: 9, name: 9 }])
          done()
        }, (resp) => {
          fail(JSON.stringify(resp))
          done()
        })
    })

  it('query object context 4', (done) => {
      const promise = idb.queryContext({
          index: 'nameIndex',
          key: 10,
          nextNumber: 2,
          preNumber: 2
        })

      promise.then((resp) => {
          expect(resp.current).toEqual({ id: 10, name: 10 })
          expect(resp.preList).toEqual([{ id: 8, name: 8 }, { id: 9, name: 9 }])
          expect(resp.nextList).toEqual([{ id: 11, name: 11 }, { id: 12, name: 12 }])
          done()
        }, (resp) => {
          fail(JSON.stringify(resp))
          done()
        })
    })

  it('autoIncrement', (done) => {
      idb = new IDB<any>({
          storeName: 'autoIncrement',
          keyPath: 'id',
          dbVersion: Date.now(),
          indexes: [{
              name: 'nameIndex',
              keyPath: 'name',
              unique: true,
              multiEntry: false
            }]
        })

      idb.put({ name: 'test' }).then(() => {
          return idb.getAll().then((list: Array<any>) => {
              expect(list.length).toEqual(1)
              expect(list[0].id).toBeDefined()
            })
        }).then(done, done)

    })
})
