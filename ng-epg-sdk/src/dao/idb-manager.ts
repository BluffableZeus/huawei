import { PlaybillLite, PlaybillVersion } from './../vsp/api'
import { IDB } from './idb'

export interface IDBPlaybillVersion extends PlaybillVersion {

    /**
     * channelID_date
     */
  channelID_date: string
}

export class IDBManager {

  private static instance: IDBManager

  private playbillIDBByChannelID: { [channelID: string]: IDB<PlaybillLite> } = {}

  private playbillVersionIDB: IDB<IDBPlaybillVersion>

  public static getInstance (): IDBManager {
      IDBManager.instance = IDBManager.instance || new IDBManager()
      return IDBManager.instance
    }

    /**
     * playbill object store
     */
  public getPlaybillIDBByChannelID (channelID: string): IDB<PlaybillLite> {
      if (!this.playbillIDBByChannelID[channelID]) {
          this.playbillIDBByChannelID[channelID] = new IDB<PlaybillLite>({
              keyPath: 'ID',
              storeName: 'playbill',
              indexes: [{
                  name: 'startTime',
                  keyPath: 'startTime',
                  unique: true,
                  multiEntry: false
                }]
            })
        }

      return this.playbillIDBByChannelID[channelID]
    }

    /**
     * playbill version object store
     */
  public getPlaybillVersionIDB (): IDB<IDBPlaybillVersion> {

      if (!this.playbillVersionIDB) {

          this.playbillVersionIDB = new IDB<IDBPlaybillVersion>({
              keyPath: 'channelID_date',
              storeName: 'playbill-version'
            })
        }

      return this.playbillVersionIDB
    }
}
