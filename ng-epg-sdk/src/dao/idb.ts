import { IDBIndexParameters } from './idb'
import * as _ from 'underscore'

const READ_ONLY = 'readonly'
const READ_WRITE = 'readwrite'

const DEFAULT_IDBOPTIONS: IDBOptions = {
  storePrefix: 'idb-',
  dbVersion: 1,
  autoIncrement: false,
  onStoreReady: () => { },
  onStoreError: () => { },
  indexes: []
} as any

export class IDB<T> {

  private _db: IDBDatabase

  constructor (private options: IDBOptions) {
      _.defaults(options, DEFAULT_IDBOPTIONS)
    }

  public getDBName () {
      const o = this.options
      return `${o.storePrefix}${o.storeName}`
    }

  private openDatabase (): Promise<IDBDatabase> {
      const o = this.options

      if (this._db) {
          o.onStoreReady()
          return Promise.resolve(this._db)
        } else {
          return new Promise((resolve, reject) => {

              const openRequest: IDBOpenDBRequest = indexedDB.open(this.getDBName(), o.dbVersion)

              openRequest.onerror = (event) => {
                  reject(event)
                  o.onStoreError(event)
                }

              openRequest.onsuccess = (event: any) => {
                  this._db = event.target.result
                  resolve(this._db)
                  o.onStoreReady(event)
                }

              openRequest.onupgradeneeded = (event: any) => {
                  const db: IDBDatabase = event.target.result
                  let store: IDBObjectStore
                  if (db.objectStoreNames.contains(o.storeName)) {
                      db.deleteObjectStore(o.storeName)
                    }

                  store = db.createObjectStore(o.storeName, {
                      keyPath: o.keyPath,
                      autoIncrement: o.autoIncrement
                    })

                  o.indexes.forEach((indexParam: IDBIndexParameters) => {
                      if (store.indexNames.contains(indexParam.name)) {
                          return
                        } else {
                          store.createIndex(indexParam.name, indexParam.keyPath, {
                              unique: indexParam.unique,
                              multiEntry: indexParam.multiEntry
                            })
                        }
                    })
                }
            })
        }
    }

    /**
     * This is for adding new records, or updating existing records in an object store when the transaction's mode is readwrite.
     */
  public put (value: T | Array<T>): Promise<any> {

      const o = this.options
      let list = []

      if (Array.isArray(value)) {
          list = value
        } else {
          list = [value]
        }

      if (!list.length) {
          return Promise.resolve()
        }

      return this.openDatabase().then((db: IDBDatabase) => {
          return new Promise((resolve, reject) => {

              const transaction = db.transaction([o.storeName], READ_WRITE)

              transaction.onabort = (event: any) => reject(event.target.error)
              transaction.onerror = (event: any) => reject(event.target.error)
              transaction.oncomplete = resolve

              const objectStore: IDBObjectStore = transaction.objectStore(o.storeName)

              list.map((val) => {
                  objectStore.put(val)
                })
            })
        })
    }

  public delete (keyorKeyRange: string | IDBKeyRange): Promise<any> {
      return this.openDatabase().then((db: IDBDatabase) => {
          return new Promise((resolve, reject) => {

              const transaction = db.transaction([this.options.storeName], READ_WRITE)
              const objectStore = transaction.objectStore(this.options.storeName)

              transaction.onabort = reject
              transaction.onerror = reject
              transaction.oncomplete = resolve

              objectStore.delete(keyorKeyRange)
            })
        })
    }

  public count (optionalKeyRange?: IDBKeyRange): Promise<number> {

      return this.openDatabase().then((db: IDBDatabase) => {
          return new Promise<number>((resolve, reject) => {

              let idbRequest: IDBRequest
              const transaction = db.transaction([this.options.storeName], READ_ONLY)
              const objectStore = transaction.objectStore(this.options.storeName)

              transaction.onabort = reject
              transaction.onerror = reject
              transaction.oncomplete = () => resolve(idbRequest ? idbRequest.result : 0)

              idbRequest = objectStore.count()
            })
        })
    }

  public clear (): Promise<any> {

      return this.openDatabase().then((db: IDBDatabase) => {
          return new Promise((resolve, reject) => {
              const transaction = db.transaction([this.options.storeName], READ_WRITE)
              const objectStore = transaction.objectStore(this.options.storeName)

              transaction.onabort = reject
              transaction.onerror = reject
              transaction.oncomplete = resolve

              objectStore.clear()
            })
        })
    }

    /**
     *
     */
  public queryList (req: IDBQueryRequest): Promise<Array<T>> {
      _.defaults(req, { offset: 0, count: Number.MAX_SAFE_INTEGER })

      const o = this.options

      return this.openDatabase().then((db: IDBDatabase) => {

          return new Promise<Array<T>>((resolve, reject) => {
              let retList: Array<T> = []
              const transaction = db.transaction([o.storeName], READ_ONLY)
              let cursorTarget: IDBObjectStore | IDBIndex = transaction.objectStore(o.storeName)
              if (req.index) {
                  cursorTarget = cursorTarget.index(req.index)
                }

              transaction.onabort = reject
              transaction.onerror = reject
              transaction.oncomplete = () => {
                  resolve(retList)
                }

              const cursorRequest: IDBRequest = cursorTarget.openCursor(req.keyRange)
              retList = this.iterate<T>(cursorRequest, req.offset, req.count)
            })
        })
    }

  public queryContext (reqx: QueryContextRequest): Promise<{
      preList: Array<T>,
      current: T,
      nextList: Array<T>
    }> {
      _.defaults(reqx, { preNumber: 0, nextNumber: 0 })

      const { index: indexName, key, preNumber, nextNumber } = reqx

      const o = this.options

      return this.openDatabase().then((db: IDBDatabase) => {

          return new Promise<any>((resolve, reject) => {

              const transaction = db.transaction([o.storeName], READ_ONLY)
              const objectStore: IDBObjectStore = transaction.objectStore(o.storeName)
              const cursorTarget: IDBIndex = objectStore.index(indexName)

              let preList = []
              let currentAndNextList = []

              transaction.onabort = () => reject()
              transaction.onerror = () => reject()
              transaction.oncomplete = () => {
                  const [current, ...nextList] = currentAndNextList
                  preList.reverse()
                  resolve({ preList, current, nextList })
                }

              if (preNumber) {
                  preList = this.iterate(cursorTarget.openCursor(IDBKeyRange.upperBound(key), 'prev'), 1, preNumber)
                }

              currentAndNextList = this.iterate(cursorTarget.openCursor(IDBKeyRange.lowerBound(key), 'next'), 0, nextNumber + 1)
            })
        })
    }

    // tslint:disable-next-line:no-shadowed-variable
  private iterate<T> (cursorRequest: IDBRequest, offset: number, count: number): Array<T> {
      const retList = []
      cursorRequest.onsuccess = function (event: any) {
          const cursor: IDBCursor = event.target.result
          if (cursor) {
              if (offset) {
                  cursor.advance(offset)
                  offset = 0
                } else {
                  retList.push(cursor['value'])
                  if (retList.length < count) {
                      cursor.continue()
                    }
                }
            }
        }
      return retList
    }

    /**
     * Retrieves an object from the store.
     */
  public get (key): Promise<T> {
      const o = this.options

      return this.openDatabase().then((db: IDBDatabase) => {
          return new Promise<T>((resolve, reject) => {
              const transaction = db.transaction([o.storeName], READ_ONLY)
              const objectStore = transaction.objectStore(o.storeName)
              const objectStoreRequest: IDBRequest = objectStore.get(key)

              objectStoreRequest.onsuccess = () => resolve(objectStoreRequest.result)
              objectStoreRequest.onerror = reject
            })
        })
    }

    /**
     * Retrieves all object from the store.
     */
  public async getAll (): Promise<Array<T>> {
      const o = this.options

      return this.openDatabase().then((db: IDBDatabase) => {

          return new Promise<Array<T>>((resolve, reject) => {
              const transaction = db.transaction([o.storeName], READ_ONLY)
              const objectStore: IDBObjectStoreExtend = transaction.objectStore(o.storeName) as IDBObjectStoreExtend
              const objectStoreRequest: IDBRequest = objectStore.getAll()

              objectStoreRequest.onsuccess = () => resolve(objectStoreRequest.result)
              objectStoreRequest.onerror = reject
            })
        })
    }
}

export interface IDBQueryRequest {
    /**
     * A name of an IDBIndex to operate on
     */
  index: string

    /**
     * An IDBKeyRange to use
     */
  keyRange: IDBKeyRange

    /**
     * Skip the provided number of results in the resultset
     */
  offset?: number

    /**
     * the number of returned results to this number
     */
  count?: number
}

export interface IDBOptions {

    /**
     * The store name.
     */
  storeName: string

    /**
     * The prefix to prepend to the store name
     */
  storePrefix?: string

    /**
     * The version of db store.
     */
  dbVersion?: number

    /**
     * The key path to use.
     */
  keyPath: string

    /**
     * If true, the object store has a key generator. Defaults to false.
     */
  autoIncrement?: boolean

    /**
     * A callback to be called when the store is ready to be used.
     */
  onStoreReady?: Function

    /**
     * A callback to be called when an error occurred during instantiation of the store.
     */
  onStoreError?: Function

  indexes?: Array<IDBIndexParameters>
}

export interface IDBIndexParameters {

    /**
     * The name of the index
     */
  name: string

    /**
     * The key path of the index
     */
  keyPath: string

    /**
     * Whether the index is unique
     */
  unique: boolean

    /**
     * Whether the index is multi entry
     */
  multiEntry: boolean
}

export interface IDBObjectStoreExtend extends IDBObjectStore {
  getAll (query?: IDBKeyRange, count?: number): IDBRequest
}

export interface QueryContextRequest {
    /**
     * index name
     */
  index: string
  key: any
  preNumber?: number
  nextNumber?: number
}
