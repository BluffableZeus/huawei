export declare const STORE: {
  [key: string]: any;
}
export declare const Config: {
  get: (key: string, fallbackValue?: any) => any;
  set: (key: string, value: any) => void;
}
