import { Config } from '../../core/config'
import { isSTB } from '../../utils/is_stb'
import * as _ from 'underscore'

import { Logger } from '../'

const log = Logger.get('sqm.service')

export const SQMErrorType = {
  normal: 'Error Message Popup',
  login: 'Login Error'
}

export interface SQMError {
  errorType?: string
  errorCode?: string
  errorMessage?: string
}

export interface STBSQMError extends SQMError {
  stbErrorCode: string
}

export interface VSPSQMError extends SQMError {
  interfaceName: string
  retCode: string
}

export interface MSASQMError extends SQMError {
  msaCode: string
  interfaceName?: string
}

export class SQMService {
  private reportError (e) {
      const error = _.pick(e, 'errorType', 'errorUrl', 'errorIP', 'errorCode', 'errorMessage')

        // C30: STB is not support SQM, will be added in C31
      log.debug(`setValueByName('reportErrorEvent', %s)`, error)
    }

  public reportMSAError (e: MSASQMError, errorMessage?: string) {
      if (!isSTB) {
          return
        }
      let message = errorMessage

      if (!message) {
          message = `MSA.${e.msaCode}, externalCode: ${e.errorCode}, message: ${e.errorMessage}`
        }

      this.reportError({
          interfaceName: e.interfaceName,
          errorType: e.errorType,
          errorMessage: message
        })
    }

  public reportVSPError (e: VSPSQMError) {
      if (!isSTB) {
          return
        }
      const prefix = Config.get('enableHttps', true) ? Config.vspHttpsUrl : Config.vspUrl
      const errorIP = prefix.split('//').pop().split(':').shift()
      const errorUrl = `${prefix}/${e.interfaceName}`
      const errorType = e.errorType || this.isLoginInf(e.interfaceName) ? SQMErrorType.login : SQMErrorType.normal

      this.reportError({
          errorType: errorType,
          errorUrl: errorUrl,
          errorIP: errorIP,
          errorMessage: `VSP.${e.errorCode}, ${e.interfaceName}.${e.retCode}, message: ${e.errorMessage}`
        })
    }

  public reportSTBError (e: STBSQMError) {
      if (!isSTB) {
          return
        }
      this.reportError({
          errorType: e.errorType,
          errorCode: e.stbErrorCode,
          errorMessage: `STB.${e.errorCode}, message: ${e.errorMessage}`
        })
    }

  private isLoginInf (interfaceName) {
      return ['Login', 'LoginRoute', 'Authenticate', 'SwitchProfile', 'Logout'].indexOf(interfaceName)
    }
}

export const sqmService: SQMService = new SQMService()
