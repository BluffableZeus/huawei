import { PolicyManager } from './policy/policy_manager'
import { Connection, RequestOptionsArgs, ResponseOptionsArgs, FullCachePolicy } from './interfaces'
import { CacheBackend } from './backends/cache-backend/cache_backend'
import { RetryBackend } from './backends/retry_backend'
import { ScatterBackend } from './backends/scatter_backend'
import { SpeedLimitBackend } from './backends/speed_limit_backend'
import { XHRBackend } from './backends/xhr-backend/xhr_backend'
import { Config } from '../config'

export class Http implements Connection {

  private connection: Connection
  private cacheBackend: CacheBackend
  private policyManager: PolicyManager

  constructor () {
      this.policyManager = new PolicyManager()
      let connection: Connection = new XHRBackend()
      connection = new RetryBackend(connection)
      connection = new ScatterBackend(connection)
      connection = new SpeedLimitBackend(connection, this.policyManager)
      this.connection = this.cacheBackend = new CacheBackend(connection, this.policyManager)
    }

  public request (request: RequestOptionsArgs): Promise<ResponseOptionsArgs> {
      return new Promise((resolve, reject) => {
          if (request.abort && typeof request.abort === 'function') {
                // to abort request before receive response.
              request.abort(() => {
                  reject({
                      status: 0
                    })
                })
            }
          this.connection.request(request).then(resolve, reject)
        })
    }

  public setProfileID (profileID: string): void {
        // to avoid js call function error, retain the method temporarily.
      return
    }

  public setCachePolicy (policy: FullCachePolicy) {
      this.policyManager.setCachePolicy(policy)
    }

  public clearCache (url: string): Promise<boolean> {
      return this.cacheBackend.clearCache(url)
    }

  public setTimeout (second: number): boolean {
      if (second !== null && second * 1 >= 0) {
          Config.set('timeout', second)
          return true
        } else {
          return false
        }
    }

  public count (inf: string): Promise<number> {
      return this.cacheBackend.getCacheCount(inf)
    }

  public getCache (inf: string, key: string): Promise<ResponseOptionsArgs> {
      return this.cacheBackend.getCache(inf, key)
    }

}
