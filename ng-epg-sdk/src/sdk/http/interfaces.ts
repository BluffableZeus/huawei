export interface Connection {
  request (request: RequestOptionsArgs): Promise<ResponseOptionsArgs>
}

export type ResponseType = 'basic' | 'cors' | 'default' | 'error' | 'opaque' | 'opaqueredirect'

export type RequestMethod = 'GET' | 'POST' | 'PUT' | 'DELETE' | 'PATCH' | 'OPTIONS' | 'HEAD'

// 0：来自服务器的实时响应 1：来自未过期缓存的数据  2：来自已过期缓存的数据
export type CacheDataType = 'FROM_SERVER' | 'FROM_VALID_CACHE' | 'FROM_EXPIRED_CACHE'

export interface RequestOptionsArgs {

  url: string

  method?: RequestMethod | null

  header?: { [key: string]: any } | null

  body?: { [key: string]: any } | null

  withCredentials?: boolean | null

    /**
     * 请求超时时间，单位秒。
     */
  timeout?: number

    /**
     * 接口调用异常时，如果错误码在retryErrorCodes内，进行重试的次数。默认为0，不重试，最大不超过3
     */
  retryTimes?: number

    /**
     * 需要重试场景，对应的网络异常错误码。为空则不会重试。
     */
  retryExceptionCode?: number[]

    /**
     * 单位秒。表示该请求将在这段时间内，随机的一个时间点发送。不传则直接发送。
     */
  scatterTimeRange?: number

    /**
     * 返回abort给调用者
     */
  abort?: (fn: () => void) => void

  keepAlive?: boolean
}

export interface ResponseOptionsArgs {
  status?: number | null
  body?: string | null
  header?: { [key: string]: any } | null
  type?: ResponseType | null
  url?: string | null
  dataSource?: CacheDataType
  originCode?: number
}

export type FullCachePolicy = {
  [inf: string]: InfCachePolicy;
} & {
  Global?: {
            /**
             * 全局缓存清理条件，说明哪些接口被调用时，需要清空所有缓存。
             * 清理缓存都限于当前Profile中的缓存；不同Profile缓存相互不可见。
             * 已知需要进行全局清理的接口包括：Authenticate、SwitchProfile、ModifyProfile、Logout。
             */
          CLEANING_CONDITION?: string[];
          FILTER_PARAMETERS?: string[];
        };

  Default?: {
            /**
             * 默认接口调用成功的关键字。
             */
          SUCCESS_KEY?: string[];

            /**
             * 默认缓存池大小。
             */
          CACHE_NUMBER?: number;

            /**
             * 默认缓存有效期。
             */
          MAX_AGE?: number;

            /**
             * 游客登录缓存默认有效期，表示每条缓存记录多久失效。数值类型，单位秒。
             */
          GUEST_MAX_AGE?: number;

            /**
             * 5，最大存放5个令牌。
             */
          TOKEN_NUMBER?: number;

            /**
             * 1，表示1秒增加一个令牌。
             */
          TOKEN_RATE?: number;

          CACHE_PERSISTENCY?: boolean;
        }
}

export type CacheRule = 'DO_NOT_CACHE' | 'USE_CACHE_NOT_EXPIRED' | 'USE_CACHE_ON_FAIL'

export interface InfCachePolicy {
    /**
     * 是否使用SDK缓存能力。
     * 默认为false，不使用缓存。该缓存值代码中写死。
     * 只有明确配置为true的接口才会使用SDK缓存。打开之后，提供有效期内的缓存能力，以及在接口异常时提供过期数据的应急缓存数据。
     *
     */
  CACHE_SWITCH?: boolean

    /**
     * 缓存清理条件，支持多值。
     * 说明哪些接口被调用时，需要清理当前接口的缓存池。清理缓存都限于当前Profile中的缓存；不同Profile缓存相互不可见。
     */
  CLEANING_CONDITION?: string[]

    /**
     * 表示接口调用成功的关键字，字符串。
     * 由于只有成功结果才会被计入缓存，所以需要告知每个接口如何判断接口调用成功。这里配置表示成功的关键字，SDK在接口响应消息中进行搜索，命中即表示成功消息。
     */
  SUCCESS_KEY?: string[]

    /**
     * 表示在生成key时，url中携带的参数中需要过滤的字符串。
     */
  FILTER_PARAMETERS?: string[]

    /**
     * 缓存池大小，数值类型。这里缺一个允许缓存的最大个数，待开发排查当前基线已有缓存池大小之后，再补充。
     */
  CACHE_NUMBER?: number

    /**
     * 缓存有效期，表示每条缓存记录多久失效。数值类型，单位秒。缓存有效期必须>0。
     */
  MAX_AGE?: number

    /**
     * 游客登录缓存有效期，表示每条缓存记录多久失效。数值类型，单位秒。
     */
  GUEST_MAX_AGE?: number

    /**
     * 令牌桶中最大令牌个数。每个令牌表示一次API调用的机会。最小为1。
     */
  TOKEN_NUMBER?: number

    /**
     * 令牌桶中令牌增长速度，表示多少秒增加一个令牌。
     */
  TOKEN_RATE?: number

    /**
     * 缓存是否需要持久化到存储，布尔类型。true，表示持久化到存储；false，表示不持久化，使用内存缓存。默认为true。
     * 涉及隐私数据的接口数据，必须使用内存存储，配置时需要注意。
     *
     */
  CACHE_PERSISTENCY?: boolean
}

export interface LocalForageDbMethods {
  getItem<T> (key: string): Promise<T>
  setItem<T> (key: string, value: T): Promise<T>
  length (): Promise<number>
  keys (): Promise<string[]>
  removeItem (key: string): Promise<void>
  clear (): Promise<void>
}
