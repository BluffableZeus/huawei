import { PolicyManager } from './../policy_manager'
describe('policy manager', () => {
  let policyManager: PolicyManager
  beforeEach(() => {
      policyManager = new PolicyManager()
    })

  it('getCachePolicyByInterface custom policy', () => {
      policyManager.setCachePolicy({
          '/VSP/V3/QueryFavorite': {
              CLEANING_CONDITION: ['/VSP/V3/DeleteFavorite', '/VSP/V3/CreateFavorite',
                  '/VSP/V3/SortFavorite', '/VSP/V3/AddFavorite'],
              CACHE_SWITCH: true
            }
        })
      const policy = policyManager.getCachePolicyByInterface('/VSP/V3/QueryFavorite')
      expect(policy && policy.CLEANING_CONDITION).toEqual([
          '/VSP/V3/DeleteFavorite',
          '/VSP/V3/CreateFavorite',
          '/VSP/V3/SortFavorite',
          '/VSP/V3/AddFavorite'
        ])

      expect(policy && policy['CACHE_SWITCH']).toEqual(true)
    })

  it('getCleanConditionByInf custom policy', () => {
      policyManager.setCachePolicy({
          '/VSP/V3/QueryFavorite1': {
              CLEANING_CONDITION: ['/VSP/V3/DeleteFavorite1', '/VSP/V3/CreateFavorite1',
                  '/VSP/V3/SortFavorite1', '/VSP/V3/AddFavorite1']
            }
        })
      const conditionDelete = policyManager.getCleanConditionByInf('/VSP/V3/DeleteFavorite1')
      const conditionCreate = policyManager.getCleanConditionByInf('/VSP/V3/CreateFavorite1')
      const conditionSort = policyManager.getCleanConditionByInf('/VSP/V3/SortFavorite1')
      const conditionAdd = policyManager.getCleanConditionByInf('/VSP/V3/AddFavorite1')
      expect(conditionDelete).toEqual(['/VSP/V3/QueryFavorite1'])
      expect(conditionCreate).toEqual(['/VSP/V3/QueryFavorite1'])
      expect(conditionSort).toEqual(['/VSP/V3/QueryFavorite1'])
      expect(conditionAdd).toEqual(['/VSP/V3/QueryFavorite1'])
    })

})
