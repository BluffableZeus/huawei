import { FullCachePolicy, InfCachePolicy } from '../interfaces'
export declare class PolicyManager {
  setCachePolicy (policy: FullCachePolicy): void
  getCachePolicyByInterface (inf?: string): InfCachePolicy | null
  getCleanConditionByInf (inf: string): string[]
  matchGlocalCleanCondition (inf: string): boolean
  getGlobalCleanCondition (): string[]
  getFilterFromPolicy (): string[]
  getSuccessKeyByInf (inf: any): string[] | undefined
  private createCleanIndex ()
  private getInfCleanCondition (policy)
}
