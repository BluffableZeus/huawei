import { Logger } from './../../../utils/logger'
import { FullCachePolicy, InfCachePolicy } from '../interfaces'

let localCachePolicy: FullCachePolicy = {}
let cleanCondition: { [key: string]: string[] } = {}
const log = new Logger('PolicyManager')
export class PolicyManager {

  public setCachePolicy (policy: FullCachePolicy) {
      if (!policy || policy.constructor.name !== 'Object') {
          log.error('setCachePolicy failed , please check policy')
          return
        }
      const fullPolicy = {}
      for (const key in policy) {
          if (key) {
              fullPolicy[key] = Object.assign({}, localCachePolicy[key], policy[key])
            }
        }
      localCachePolicy = Object.assign({}, localCachePolicy, fullPolicy)
      this.createCleanIndex()
    }

  public getCachePolicyByInterface (inf?: string): InfCachePolicy | null {

      if (inf && localCachePolicy[inf]) {
          const infCachePolicy: InfCachePolicy = Object.assign({},
                localCachePolicy.Global,
                localCachePolicy.Default,
                localCachePolicy[inf])
          return infCachePolicy
        }
      return null
    }

  public getCleanConditionByInf (inf: string): string[] {
      return cleanCondition[inf] || []
    }

  public matchGlocalCleanCondition (inf: string): boolean {
      const condition = this.getGlobalCleanCondition()
      return condition.indexOf(inf) > -1
    }

  public getGlobalCleanCondition (): string[] {
      const policy: InfCachePolicy = localCachePolicy.Global || {}
      return policy.CLEANING_CONDITION || []
    }

  public getFilterFromPolicy (): string[] {
      const policy: InfCachePolicy = localCachePolicy.Global || {}
      return policy.FILTER_PARAMETERS || []
    }

  public getSuccessKeyByInf (inf) {
      const policy = this.getCachePolicyByInterface(inf) || localCachePolicy.Default
      return policy && policy.SUCCESS_KEY
    }

  private createCleanIndex () {
      const condition = this.getInfCleanCondition(localCachePolicy)
      cleanCondition = {}
      for (const key in condition) {
          if (condition[key]) {
              condition[key].forEach((value) => {
                  if (cleanCondition[value]) {
                      if (cleanCondition[value].indexOf(key) < 0) {
                          cleanCondition[value].push(key)
                        }
                    } else {
                      cleanCondition[value] = [key]
                    }
                })
            }
        }
    }

  private getInfCleanCondition (policy): { [key: string]: string[] } {
      const condition = {}
      for (const key in policy) {
          if (key !== 'Global' && key !== 'Default' && policy[key] && policy[key].CLEANING_CONDITION) {
              condition[key] = policy[key].CLEANING_CONDITION
            }
        }
      return condition
    }

}
