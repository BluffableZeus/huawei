import { headerParse } from './../../../lang/headerparse'
import { Logger } from './../../../../utils/logger'
import { ResponseOptionsArgs } from './../../interfaces'
import { encode } from './../../../lang/encode'

const log = new Logger('xhr')

export interface Methods {
  GET: 'GET'
  POST: 'POST'
  PUT: 'PUT'
  DELETE: 'DELETE'
  PATCH: 'PATCH'
  OPTIONS: 'OPTIONS'
  HEAD: 'HEAD'
}

export interface Events {
  READY_STATE_CHANGE: 'readystatechange'
  LOAD_START: 'loadstart'
  PROGRESS: 'progress'
  ABORT: 'abort'
  ERROR: 'error'
  LOAD: 'load'
  TIMEOUT: 'timeout'
  LOAD_END: 'loadend'
}

export interface Headers {
  [key: string]: string
}

export interface EventCallbacks {
  [key: string]: any
}

export type MethodType = 'GET' | 'POST' | 'PUT' | 'DELETE' | 'PATCH' | 'OPTIONS' | 'HEAD'

export interface Config {
  url?: string
  method: MethodType
  body?: any
  header?: Headers | null
  dump?: (data: any) => string
  load?: (key: string) => any
  xmlHttpRequest?: () => XMLHttpRequest
  promise?: (fn: () => Promise<any>) => Promise<any>
  abort?: any
  params?: any
  withCredentials?: boolean
  raw?: boolean
  events?: EventCallbacks
  timeout?: number
  keepAlive?: boolean
}

export interface Response {
  status: number
  response: any
  data?: any
  xhr: XMLHttpRequest
}

export interface DynamicObject {
  [key: string]: any
}

export const METHODS: Methods = {
  GET: 'GET',
  POST: 'POST',
  PUT: 'PUT',
  DELETE: 'DELETE',
  PATCH: 'PATCH',
  OPTIONS: 'OPTIONS',
  HEAD: 'HEAD'
}

export const EVENTS: Events = {
  READY_STATE_CHANGE: 'readystatechange',
  LOAD_START: 'loadstart',
  PROGRESS: 'progress',
  ABORT: 'abort',
  ERROR: 'error',
  LOAD: 'load',
  TIMEOUT: 'timeout',
  LOAD_END: 'loadend'
}

export const XR_DEFAULTS = {
  method: METHODS.GET,
  body: undefined,
  header: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
  dump: JSON.stringify,
  load: JSON.parse,
  xmlHttpRequest: (): XMLHttpRequest => new XMLHttpRequest(),
  promise: (fn: () => Promise<any>) => new Promise(fn),
  withCredentials: false
}

const res = (xr: XMLHttpRequest, data?: any): ResponseOptionsArgs => ({
  status: xr.status,
  header: headerParse(xr.getAllResponseHeaders()),
  body: data,
  dataSource: 'FROM_SERVER'
})

export class XHR {

  private config: Config = Object.assign({}, XR_DEFAULTS)

  public configure (opts: Partial<Config>): void {
      this.config = Object.assign({}, this.config, opts)
    }

  public send (args: Config): Promise<any> {
      return this.promise(args, (resolve: any, reject: any) => {
          const opts: Config = Object.assign({}, this.config, args)
          const xr = opts.xmlHttpRequest && opts.xmlHttpRequest() || new XMLHttpRequest()
          if (opts.abort) {
              args.abort(() => {
                  reject(res(xr))
                  xr.abort()
                })
            }

          if (opts.url === undefined) {
              throw new Error('No URL defined')
            }

          const openUrl = opts.params ? `${opts.url.split('?')[0]}?${encode(opts.params)}` : opts.url

          xr.open(opts.method, openUrl, true)

            // setting after open for compatibility with IE versions <=10
          xr.withCredentials = !!opts.withCredentials
          try {
              xr.timeout = (opts.timeout || 15) * 1000
            } catch (e) {
              log.error(e)
              log.error('HttpXMLRequest timeout error')
            }

          xr.addEventListener(EVENTS.LOAD, () => {
              if (xr.status >= 200 && xr.status < 300) {
                  resolve(res(xr, xr.responseText))
                } else {
                  reject(res(xr))
                }
            })

          xr.addEventListener(EVENTS.ABORT, () => reject(res(xr)))

          xr.addEventListener(EVENTS.ERROR, () => reject(res(xr)))

          xr.addEventListener(EVENTS.TIMEOUT, () => reject(res(xr)))

          this.setXHRHeader(xr, opts)

          if (opts.events) {
              for (const k in opts.events) {
                  if (!{}.hasOwnProperty.call(opts.events, k)) {
                      continue
                    }
                  xr.addEventListener(k, opts.events[k].bind(null, xr), false)
                }
            }

          const xhrData = (typeof opts.body === 'object' && !opts.raw) ? opts.dump && opts.dump(opts.body) : opts.body
          if (xhrData !== undefined) {
              xr.send(xhrData)
            } else {
              xr.send()
            }
        })
    }

  public get (url: string, params: any, args: Partial<Config>) {
      return this.send(Object.assign({ url, method: METHODS.GET, params }, args))
    }

  public put (url: string, data: any, args: Partial<Config>) {
      return this.send(Object.assign({ url, method: METHODS.PUT, data }, args))
    }

  public post (url: string, data: any, args: Partial<Config>) {
      return this.send(Object.assign({ url, method: METHODS.POST, data }, args))
    }

  public patch (url: string, data: any, args: Partial<Config>) {
      return this.send(Object.assign({ url, method: METHODS.PATCH, data }, args))
    }

  public del (url: string, args: Partial<Config>) {
      return this.send(Object.assign({ url, method: METHODS.DELETE }, args))
    }

  public options (url: string, args: Partial<Config>) {
      return this.send(Object.assign({ url, method: METHODS.OPTIONS }, args))
    }

  private setXHRHeader (xr: XMLHttpRequest, opts: Config) {
      for (const k in (opts.header || {})) {
          if (!{}.hasOwnProperty.call(opts.header, k)) {
              continue
            }
          if (opts.header && opts.header[k]) {
              xr.setRequestHeader(k, opts.header[k])
            }
        }

      if (opts.keepAlive === false) {
          try {
              xr.setRequestHeader('Connection', 'close')
            } catch (e) {
              log.error(e)
              log.error('HttpXMLRequest set Connection error')
            }
        }
    }

  private promise (args: Config, fn: any) {
      if (args && args.promise) {
          return args.promise(fn)
        } else {
          return (this.config.promise || XR_DEFAULTS.promise)(fn)
        }
    }
}

export const xhr = new XHR()
