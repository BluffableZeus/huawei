import { Connection, RequestOptionsArgs, ResponseOptionsArgs } from '../../interfaces'
export declare class XHRBackend implements Connection {
  request (request: RequestOptionsArgs | any): Promise<ResponseOptionsArgs>
}
