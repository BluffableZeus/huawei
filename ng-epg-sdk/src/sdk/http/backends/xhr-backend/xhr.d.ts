export interface Methods {
  GET: 'GET'
  POST: 'POST'
  PUT: 'PUT'
  DELETE: 'DELETE'
  PATCH: 'PATCH'
  OPTIONS: 'OPTIONS'
  HEAD: 'HEAD'
}
export interface Events {
  READY_STATE_CHANGE: 'readystatechange'
  LOAD_START: 'loadstart'
  PROGRESS: 'progress'
  ABORT: 'abort'
  ERROR: 'error'
  LOAD: 'load'
  TIMEOUT: 'timeout'
  LOAD_END: 'loadend'
}
export interface Headers {
  [key: string]: string
}
export interface EventCallbacks {
  [key: string]: any
}
export declare type MethodType = 'GET' | 'POST' | 'PUT' | 'DELETE' | 'PATCH' | 'OPTIONS' | 'HEAD'
export interface Config {
  url?: string
  method: MethodType
  body?: any
  header?: Headers | null
  dump?: (data: any) => string
  load?: (key: string) => any
  xmlHttpRequest?: () => XMLHttpRequest
  promise?: (fn: () => Promise<any>) => Promise<any>
  abort?: any
  params?: any
  withCredentials?: boolean
  raw?: boolean
  events?: EventCallbacks
  timeout?: number
  keepAlive?: boolean
}
export interface Response {
  status: number
  response: any
  data?: any
  xhr: XMLHttpRequest
}
export interface DynamicObject {
  [key: string]: any
}
export declare const METHODS: Methods
export declare const EVENTS: Events
export declare const XR_DEFAULTS: {
  method: 'GET';
  body: undefined;
  header: {
      'Accept': string;
      'Content-Type': string;
    };
  dump: {
      (value: any, replacer?: ((key: string, value: any) => any) | undefined, space?: string | number | undefined): string;
      (value: any, replacer?: (string | number)[] | null | undefined, space?: string | number | undefined): string;
    };
  load: (text: string, reviver?: ((key: any, value: any) => any) | undefined) => any;
  xmlHttpRequest: () => XMLHttpRequest;
  promise: (fn: () => Promise<any>) => Promise<{}>;
  withCredentials: boolean;
}
export declare class XHR {
  private config
  configure (opts: Partial<Config>): void
  send (args: Config): Promise<any>
  get (url: string, params: any, args: Partial<Config>): Promise<any>
  put (url: string, data: any, args: Partial<Config>): Promise<any>
  post (url: string, data: any, args: Partial<Config>): Promise<any>
  patch (url: string, data: any, args: Partial<Config>): Promise<any>
  del (url: string, args: Partial<Config>): Promise<any>
  options (url: string, args: Partial<Config>): Promise<any>
  private setXHRHeader (xr, opts)
  private promise (args, fn)
}
export declare const xhr: XHR
