import { Logger } from './../../../../utils/logger'
import { Config } from './../../../config'
import { Connection, RequestOptionsArgs, ResponseOptionsArgs } from '../../interfaces'
import { xhr } from './xhr'

const log = new Logger('xhr_backend')

export class XHRBackend implements Connection {

  public request (request: RequestOptionsArgs | any): Promise<ResponseOptionsArgs> {
      log.debug('begin send.')

      request.method = request.method || 'POST'
      request.timeout = request.timeout || Config.get('timeout')

      return xhr.send(request)
    }

}
