import { ScatterBackend } from './../scatter_backend'
import { Connection } from './../../interfaces'
describe('scatter_backend', () => {

  let connect: Connection

  beforeEach(() => {
      connect = {
          request: (req) => {
              return new Promise((resolve, reject) => {
                  if (req.url) {

                      resolve({ body: '{}', status: 200, dataSource: 'FROM_SERVER' })
                    } else {
                      reject({ status: 0 })
                    }
                })
            }
        }
    })

  it('scatterBackend direct send success', (done) => {
      const scatterBackend = new ScatterBackend(connect)
      scatterBackend.request({
          url: 'url'
        }).then((resp) => {
          expect(resp.status).toBe(200)
          done()
        })
    })

  it('scatterBackend direct send fail', (done) => {
      const scatterBackend = new ScatterBackend(connect)
      scatterBackend.request({
          url: ''
        }).then(null, (resp) => {
          expect(resp.status).toBe(0)
          done()
        })
    })

  it('scatterBackend random send success', (done) => {
      const scatterBackend = new ScatterBackend(connect)
      scatterBackend.request({
          url: 'url',
          scatterTimeRange: 5
        }).then((resp) => {
          expect(resp.status).toBe(200)
          done()
        })
    })

  it('scatterBackend random send fail', (done) => {
      const scatterBackend = new ScatterBackend(connect)
      scatterBackend.request({
          url: '',
          scatterTimeRange: 5
        }).then(null, (resp) => {
          expect(resp.status).toBe(0)
          done()
        })
    })

})
