import { RetryBackend } from './../retry_backend'
import { Connection } from './../../interfaces'
describe('retry_backend', () => {

  let connect: Connection
  let count = 0

  beforeEach(() => {
      count = 0
      connect = {
          request: (req) => {
              count++
              return new Promise((resolve, reject) => {
                  if (req.url && count === (req.retryTimes || 0) + 1) {
                      resolve({
                          body: '{}',
                          status: 200,
                          dataSource: 'FROM_SERVER'
                        })
                    } else {
                      reject({
                          status: 504
                        })
                    }
                })
            }
        }
    })

  it('retrySend failed', (done) => {
      const retryBackend = new RetryBackend(connect)
      const retryTimes = 3
      retryBackend.request({ url: '', retryExceptionCode: [504], retryTimes }).then(null, (resp) => {
          expect(count).toEqual(retryTimes + 1)
          expect(resp.status).toBe(504)
          done()
        })

    })

  it('retrySend failed no retry', (done) => {
      const retryBackend = new RetryBackend(connect)
      const retryTimes = 3
      retryBackend.request({ url: '', retryExceptionCode: [], retryTimes }).then(null, (resp) => {
          expect(count).toEqual(1)
          expect(resp.status).toBe(504)
          done()
        })

    })

  it('retrySend success', (done) => {
      const retryBackend = new RetryBackend(connect)
      const retryTimes = 3
      retryBackend.request({ url: 'url', retryExceptionCode: [504], retryTimes }).then((resp) => {
          expect(count).toEqual(retryTimes + 1)
          expect(resp.status).toBe(200)
          done()
        })

    })

})
