import { Connection } from './../../interfaces'
import { SpeedLimitBackend } from './../speed_limit_backend'
import { PolicyManager } from './../../policy/policy_manager'
describe('speed_limit_backend', () => {
  let policyManager: PolicyManager
  let connect: Connection
  let originalTimeout
  beforeEach(() => {
      originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL
      jasmine.DEFAULT_TIMEOUT_INTERVAL = 10000
      connect = {
          request: (req) => {
              return new Promise((resolve, reject) => {
                  if (req.url) {
                      resolve({
                          body: '{}',
                          status: 200,
                          dataSource: 'FROM_SERVER'
                        })
                    } else {
                      reject({
                          status: 504
                        })
                    }
                })
            }
        }

      policyManager = new PolicyManager()
      const policy = {
          '/VSP/V3/SpeedLimit': {
              TOKEN_NUMBER: 5,
              TOKEN_RATE: 1
            },
          'Default': {
              TOKEN_NUMBER: 2,
              TOKEN_RATE: 1
            }
        }
      policyManager.setCachePolicy(policy)
    })

  it('no enter speed limit backend', (done) => {
      const speedLimitBackend = new SpeedLimitBackend(connect, policyManager)
      const promises: Array<Promise<any>> = []
      for (let i = 0; i < 6; i++) {
          const fn = (resolve) => {
              speedLimitBackend.request({
                  url: '/VSP/V3/SpeedLimit'
                }).then(resolve)
            }
          const promise = new Promise<any>((resolve, reject) => {
              if (i < 5) {
                  fn(resolve)
                } else {
                  setTimeout(() => { fn(resolve) }, 2000)
                }
            })
          promises.push(promise)
        }
      Promise.all(promises).then((results) => {
          expect(results[0].status).toEqual(200)
          expect(results[1].status).toEqual(200)
          expect(results[2].status).toEqual(200)
          expect(results[3].status).toEqual(200)
          expect(results[4].status).toEqual(200)
          expect(results[5].status).toEqual(200)
          done()
        })
    })

  it('enter speed limit backend', (done) => {
      const speedLimitBackend = new SpeedLimitBackend(connect, policyManager)
      const promises: Array<Promise<any>> = []
      for (let i = 0; i < 6; i++) {
          const promise = new Promise<any>((resolve, reject) => {
              speedLimitBackend.request({
                  url: '/VSP/V3/SpeedLimit'
                }).then(resolve, resolve)
            })
          promises.push(promise)
        }
      Promise.all(promises).then((results) => {
          expect(results[0].status).toEqual(200)
          expect(results[1].status).toEqual(200)
          expect(results[2].status).toEqual(200)
          expect(results[3].status).toEqual(200)
          expect(results[4].status).toEqual(200)
          expect(JSON.parse(results[5].body).result.retCode).toEqual('38079')
          done()
        })
    })

  it('enter no limit speed backend', (done) => {
      const speedLimitBackend = new SpeedLimitBackend(connect, policyManager)
      const promises: Array<Promise<any>> = []
      for (let i = 0; i < 6; i++) {
          const promise = new Promise<any>((resolve, reject) => {
              speedLimitBackend.request({
                  url: '/VSP/V3/SpeedLimit' + (i < 5 ? '' : 'i')
                }).then(resolve, resolve)
            })
          promises.push(promise)
        }
      Promise.all(promises).then((results) => {
          expect(results[0].status).toEqual(200)
          expect(results[1].status).toEqual(200)
          expect(results[2].status).toEqual(200)
          expect(results[3].status).toEqual(200)
          expect(results[4].status).toEqual(200)
          expect(results[5].status).toEqual(200)
          done()
        })
    })

  it('enter limit number 2 speed backend', (done) => {
      const policy = {
          '/VSP/V3/SpeedLimit': {
              TOKEN_NUMBER: 2,
              TOKEN_RATE: 1
            },
          'Default': {
              TOKEN_NUMBER: 5,
              TOKEN_RATE: 1
            }
        }
      policyManager.setCachePolicy(policy)
      const speedLimitBackend = new SpeedLimitBackend(connect, policyManager)
      const promises: Array<Promise<any>> = []
      for (let i = 0; i < 6; i++) {
          const promise = new Promise<any>((resolve, reject) => {
              speedLimitBackend.request({
                  url: '/VSP/V3/SpeedLimit' + (i < 5 ? '' : 'i')
                }).then(resolve, resolve)
            })
          promises.push(promise)
        }
      Promise.all(promises).then((results) => {
          expect(results[0].status).toEqual(200)
          expect(results[1].status).toEqual(200)
          expect(JSON.parse(results[2].body).result.retCode).toEqual('38079')
          expect(JSON.parse(results[3].body).result.retCode).toEqual('38079')
          expect(JSON.parse(results[4].body).result.retCode).toEqual('38079')
          expect(results[5].status).toEqual(200)
          done()
        })
    })

  afterEach(() => {
      jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout
    })

})
