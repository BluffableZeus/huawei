import { Connection, RequestOptionsArgs, ResponseOptionsArgs } from '../interfaces'
export declare class RetryBackend implements Connection {
  private connection
  constructor (connection: Connection);
  request (request: RequestOptionsArgs): Promise<ResponseOptionsArgs>
  private retrySend (request, count)
}
