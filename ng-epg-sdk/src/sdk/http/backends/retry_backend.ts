import { Logger } from './../../../utils/logger'
import { Connection, RequestOptionsArgs, ResponseOptionsArgs } from '../interfaces'
const log = new Logger('retry_backend')
export class RetryBackend implements Connection {

  constructor (private connection: Connection) {

    }

  public request (request: RequestOptionsArgs): Promise<ResponseOptionsArgs> {
      log.debug('begin send.')
      return this.retrySend(request, request.retryTimes || 0)
    }

    /**
     *
     * retry
     * @private
     * @param {any} request
     * @param {any} count
     * @returns
     * @memberof RetryBackend
     */
  private retrySend (request: RequestOptionsArgs, count: number) {
      if (count) {
          log.debug('begin retrySend send count:%s', count)
          return this.connection.request(request).then(null, (resp: ResponseOptionsArgs) => {
              const retryExceptionCode = request.retryExceptionCode
              const retStatus = resp.status || resp.status === 0
              if (retryExceptionCode && retStatus && retryExceptionCode.indexOf(resp.status || 0) > -1) {
                  return this.retrySend(request, --count)
                } else {
                  return Promise.reject(resp)
                }
            })
        } else {
          log.debug('begin retrySend send request')
          return this.connection.request(request)
        }
    }

}
