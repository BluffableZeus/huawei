import { PolicyManager } from './../policy/policy_manager'
import { Connection, RequestOptionsArgs, ResponseOptionsArgs } from '../interfaces'
export declare class SpeedLimitBackend implements Connection {
  private connection
  private policyManager
  constructor (connection: Connection, policyManager: PolicyManager);
  request (request: RequestOptionsArgs): Promise<ResponseOptionsArgs>
  private speedLimit (request)
  private getTokenNumber (inf, tokenRate, tokenMaxNum)
  private getLeftTokenNumber (req, tokenRate, tokenMaxNum)
}
