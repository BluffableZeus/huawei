import { Logger } from './../../../utils/logger'
import { PolicyManager } from './../policy/policy_manager'
import { Connection, RequestOptionsArgs, ResponseOptionsArgs } from '../interfaces'
import { extractUrlPath } from './../../lang/extract_url_path'
const log = new Logger('speed_limit_backend')

const tokenMap: { [key: string]: TokenRequest } = {}

interface TokenRequest {
  lastUpdateTime: number
  lastTokenNumber: number
}

export class SpeedLimitBackend implements Connection {

  constructor (private connection: Connection, private policyManager: PolicyManager) {

    }

  public request (request: RequestOptionsArgs): Promise<ResponseOptionsArgs> {
      log.debug('begin send.')
      return this.speedLimit(request)
    }

  private speedLimit (request: RequestOptionsArgs): Promise<ResponseOptionsArgs> {
      log.debug('begin speedLimit')
      const inf = extractUrlPath(request.url)
      const policy = this.policyManager.getCachePolicyByInterface(inf)
      if (!policy) {
          return this.connection.request(request)
        }
      const tokenRate = policy.TOKEN_RATE || 2
      const tokenMaxNum = policy.TOKEN_NUMBER || 1
      const tokenNum = this.getTokenNumber(inf, tokenRate, tokenMaxNum)
      if (tokenNum > 0) {
          tokenMap[inf] = {
              lastUpdateTime: new Date().getTime(),
              lastTokenNumber: Math.max(tokenNum - 1, 0)
            }
          log.debug('end speedLimit send.')
          return this.connection.request(request)
        } else {
          log.error('NetworkSDK, url:%s is limited. TOKEN_NUMBER:%s, TOKEN_RATE:%s', request.url, tokenMaxNum, tokenRate)
          return Promise.resolve({
              status: 200,
              body: '{"result":{"retCode":"38079","retMsg":"Interface call too frequently, causing the SDK flow control."}}'
            })
        }

    }

  private getTokenNumber (inf: string, tokenRate: number, tokenMaxNum: number): number {
      log.debug('begin getTokenNumber inf:%, tokenRate:%s, tokenMaxNum:%s', inf, tokenRate, tokenMaxNum)
      let tokenNum
      if (tokenMap[inf]) {
          tokenNum = this.getLeftTokenNumber(tokenMap[inf], tokenRate, tokenMaxNum)
        } else {
          tokenNum = tokenMaxNum
        }
      log.debug('end getTokenNumber tokenNum:%s', tokenNum)
      return tokenNum
    }

  private getLeftTokenNumber (req: TokenRequest, tokenRate: number, tokenMaxNum: number): number {
      const now = new Date().getTime()
      const addedNumber = Math.floor((now - req.lastUpdateTime) / (tokenRate * 1000))
      return Math.min(req.lastTokenNumber + addedNumber, tokenMaxNum)
    }

}
