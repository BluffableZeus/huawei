import { Connection, RequestOptionsArgs, ResponseOptionsArgs } from '../interfaces'
export declare class ScatterBackend implements Connection {
  private connection
  constructor (connection: Connection);
  request (request: RequestOptionsArgs): Promise<ResponseOptionsArgs>
}
