import { Logger } from './../../../utils/logger'
import { Connection, RequestOptionsArgs, ResponseOptionsArgs } from '../interfaces'
const log = new Logger('scatter_backend')
export class ScatterBackend implements Connection {

  constructor (private connection: Connection) {

    }

  public request (request: RequestOptionsArgs): Promise<ResponseOptionsArgs> {
      if (request.scatterTimeRange) {
          log.debug('begin scatter send')
          const randomTime = Math.random() * request.scatterTimeRange
          return new Promise((resolve, reject) => {
              setTimeout(() => {
                  this.connection.request(request).then(resolve, reject)
                }, randomTime * 1000)
            })
        } else {
          log.debug('begin direct send')
          return this.connection.request(request)
        }
    }

}
