import { ResponseOptionsArgs } from '../../interfaces'

export class Record {

  public modificationTimestamp: number = new Date().getTime()

  constructor (public key, public response: ResponseOptionsArgs, public timestamp: number = new Date().getTime()) {
    }

  public touch () {
      this.modificationTimestamp = new Date().getTime()
    }

}
