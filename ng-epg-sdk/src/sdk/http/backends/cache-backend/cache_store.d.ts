import { PolicyManager } from './../../policy/policy_manager'
import { ResponseOptionsArgs } from '../../interfaces'
export declare class CacheStore {
  private profileID
  private inf
  private policyManager
  private store
  private size
  constructor (profileID: string, inf: string, policyManager: PolicyManager);
  get (key: string, ignoreExpire?: boolean): Promise<ResponseOptionsArgs>
  remove (key: string): void
  getSize (): number
  clear (): void
  ensureSize (): void
  put (key: string, response: ResponseOptionsArgs): boolean
  private isValid (record)
  private getReservedSize ()
  private getSortedRecordList ()
  private resetStore (recordList)
  private isSuccessResponse (body)
  private getInfCachePolicy ()
}
