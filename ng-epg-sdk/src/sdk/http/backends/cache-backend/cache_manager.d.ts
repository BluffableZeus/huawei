import { PolicyManager } from './../../policy/policy_manager'
import { RequestOptionsArgs, ResponseOptionsArgs } from '../../interfaces'
export declare class CacheManager {
  private policyManager
  constructor (policyManager: PolicyManager);
  get (request: RequestOptionsArgs, useCacheOnFail?: boolean): Promise<ResponseOptionsArgs>
  put (request: RequestOptionsArgs, response: ResponseOptionsArgs): Promise<boolean>
  setProfileID (profileID: string): void
  clearCache (url: string): Promise<boolean>
  getCacheCount (inf: string): Promise<number>
  getCache (inf: string, key: string): Promise<ResponseOptionsArgs>
  private clearMemoryCache ()
  private clearLocalCache (profileID, url)
  private getCacheStore (inf)
  private getIDBStore (inf)
  private getCachePolicyByInterface (inf?)
  private getFilterFromPolicy ()
  private createCacheKey (inf, request)
  private matchCleanCondition (url, response)
  private needClearCache (inf, body?)
  private useCache (inf, infCachePolicy)
  private checkUseCache (inf, infCachePolicy?)
}
