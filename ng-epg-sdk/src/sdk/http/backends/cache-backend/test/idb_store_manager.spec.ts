import { IDBStoreManager } from './../idb_store_manager'

describe('idb_store_manager', () => {
  let idbManager: IDBStoreManager
  beforeEach(() => {
      idbManager = new IDBStoreManager()
    })

  it('addDataBaseStore', (done) => {
      idbManager.addDataBaseStore('id').then(() => {
          idbManager['dbLocalForage'].getItem('id').then((resp) => {
              expect(resp).not.toBeNull()
              done()
            })
        })
    })

  it('addDataBaseStore three times', (done) => {
      const promises: Array<Promise<void>> = []
      for (let i = 0; i < 3; i++) {
          promises.push(idbManager.addDataBaseStore('id' + i))
        }
      Promise.all(promises).then(() => {
          idbManager.addDataBaseStore('id').then(() => {
              idbManager['dbLocalForage'].length().then((length) => {
                  expect(length).toEqual(1)
                  done()
                })
            })
        })
    })

  it('deleteDataBaseStore', (done) => {
      idbManager['dbLocalForage'].keys().then((keys: string[]) => {
          if (keys) {
              const promises: Array<Promise<boolean>> = []
              keys.forEach((key) => {
                  promises.push(idbManager.deleteDataBaseStore(key))
                })
              Promise.all(promises).then(() => {
                  idbManager['dbLocalForage'].length().then((length) => {
                      expect(length).toEqual(0)
                      done()
                    })
                })
            } else {
              idbManager['dbLocalForage'].length().then((length) => {
                  expect(length).toEqual(0)
                  done()
                })
            }
        })
    })

  it('clearCache', (done) => {
      idbManager.addDataBaseStore('ProfileDB').then(() => {
          idbManager.clearCache('ProfileDB', 'Profile').then(() => {
              idbManager['dbLocalForage'].length().then((length) => {
                  expect(length).toEqual(0)
                  done()
                })
            })
        })
    })

  it('clearAll', (done) => {
      idbManager.clearAll().then(() => {
          idbManager['dbLocalForage'].length().then((length) => {
              expect(length).toEqual(0)
              done()
            })
        })
    })

  it('getSize', (done) => {
      idbManager.addDataBaseStore('ProfileDB').then(() => {
          idbManager.getSize('ProfileDB', 'Profile').then((length) => {
              expect(length).toBe(0)
              done()
            })
        })
    })

})
