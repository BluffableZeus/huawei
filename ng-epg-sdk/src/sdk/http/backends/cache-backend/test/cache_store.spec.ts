import { PolicyManager } from './../../../policy/policy_manager'
import { CacheStore } from '../cache_store'
import { Config } from '../../../../config'
import { Record } from '../cache_record'
import { ResponseOptionsArgs, FullCachePolicy } from '../../../interfaces'

declare function spyOn<T> (object: T, method: string): jasmine.Spy

// tslint:disable no-angle-bracket-type-assertion
describe('CacheStore', () => {

  let cacheStore: CacheStore
  let infCachePolicy: FullCachePolicy
  const RecordCount = 5
  beforeEach(() => {
      infCachePolicy = {
          url: {
              CACHE_SWITCH: true,
              SUCCESS_KEY: [''],
              CACHE_NUMBER: RecordCount,
              MAX_AGE: 1
            }
        }
      const policyManager = new PolicyManager()
      policyManager.setCachePolicy(infCachePolicy)
      cacheStore = new CacheStore('id', 'url', policyManager)

      for (let index = 0; index < RecordCount; index++) {
          const record = new Record('' + index, <any> { body: '' + index })
          cacheStore['store'][index] = record
        }
      cacheStore['size'] = RecordCount

    })

  it('get, hit cache', (done) => {
      const record = new Record('key', <any> {})
      cacheStore['store']['key'] = record

      spyOn(cacheStore, 'ensureSize').and.returnValue(null)

      cacheStore.get('key').then((resp) => {
          expect(resp).toEqual(resp)
          done()
        }, fail)
    })

  it('get, cache expired', (done) => {
      const record = new Record('key', <any> {}, 0)
      cacheStore['store']['key'] = record

      spyOn(cacheStore, 'ensureSize').and.returnValue(null)

      cacheStore.get('key').then(fail, done)
    })

  it('get, cache expired, ignore expire', (done) => {
      const record = new Record('key', <any> {})
      cacheStore['store']['key'] = record

      spyOn(cacheStore, 'ensureSize').and.returnValue(null)

      cacheStore.get('key', true).then(done, fail)
    })

  it('remove', (done) => {
      const key = '2'
      expect(cacheStore.getSize()).toEqual(RecordCount)

        // key缓存存在
      cacheStore.get(key).then((resp: ResponseOptionsArgs) => {
          expect(resp.body).toEqual(key)
          cacheStore.remove(key)

            // 删除后，不存在，且Size减少1
          cacheStore.get(key).then(fail, () => {
              expect(cacheStore.getSize()).toEqual(RecordCount - 1)
              done()
            })
        }, fail)
    })

  it('clear', () => {
      const record = new Record('key', <any> {})
      cacheStore['store']['key'] = record
      cacheStore['size'] = 1

      cacheStore.clear()

      expect(cacheStore['store']).toEqual({})
      expect(cacheStore.getSize()).toEqual(0)
    })

  it('ensureSize', () => {
      Config.set('backend_cache_clear_ratio', 0.2)

      for (let index = 0; index < RecordCount; index++) {
          const newKey = '' + (RecordCount + index)
          const record = new Record(newKey, <any> newKey)
          cacheStore['store'][newKey] = record
        }
      cacheStore['size'] = RecordCount * 2

      spyOn(cacheStore, 'resetStore').and.callFake((recordList: Record[]) => {
          expect(recordList.length).toEqual((infCachePolicy.url.CACHE_NUMBER || RecordCount))
          const keys = recordList && recordList.map((record) => record.key)
          expect(keys).toEqual(['5', '6', '7', '8', '9'])
        })

      expect(cacheStore.getSize()).toEqual(RecordCount * 2)

      cacheStore.ensureSize()
    })

  it('ensureSize', (done) => {
      Config.set('backend_cache_clear_ratio', 0.2)

      for (let index = 0; index < RecordCount; index++) {
          const newKey = '' + (RecordCount + index)
          const record = new Record(newKey, <any> newKey)
          cacheStore['store'][newKey] = record
        }
      cacheStore['size'] = RecordCount * 2

      spyOn(cacheStore, 'resetStore').and.callFake((recordList: Record[]) => {
          expect(recordList.length).toEqual((infCachePolicy.url.CACHE_NUMBER || RecordCount))
          const keys = recordList.map((record) => record.key)
          expect(keys).toEqual(['6', '7', '8', '9', '2'])
        })

      expect(cacheStore.getSize()).toEqual(RecordCount * 2)

      setTimeout(() => {
          cacheStore['store']['2'].touch()
          cacheStore.ensureSize()
          done()
        }, 100)
    })
})
