import { PolicyManager } from './../../../policy/policy_manager'
import { CacheBackend } from '../cache_backend'
import { Connection, RequestOptionsArgs, ResponseOptionsArgs } from '../../../interfaces'

declare function spyOn<T> (object: T, method: string): jasmine.Spy

describe('CacheBackend', () => {

  let backend: CacheBackend
  let connection: Connection
  let policyManager: PolicyManager

  beforeEach(() => {
      connection = {
          request (request: RequestOptionsArgs): Promise<ResponseOptionsArgs> {
              throw new Error('Method not implemented.')
            }
        }
      policyManager = new PolicyManager()
      backend = new CacheBackend(connection, policyManager)
    })

  it('hit cache', (done) => {

      spyOn(backend, 'getCachedResponse').and.returnValue(Promise.resolve('success'))

      backend.request({ url: '' }).then((resp) => {
          expect(resp).toEqual('success' as any)
        }).then(done, fail)

    })

  it('hit cache expired', (done) => {
      spyOn(backend, 'getCachedResponse').and.callFake((...args) => {
          if (args[1]) {
              return Promise.resolve({ body: 'success' })
            } else {
              return Promise.reject(null)
            }
        })
      spyOn(connection, 'request').and.returnValue(Promise.reject({ status: 0 }))
      backend.request({ url: '' }).then((resp) => {
          expect(resp.body).toEqual('success')
        }).then(done, fail)

    })

  describe('does not hit cache', () => {

      it('request success', (done) => {

          spyOn(backend, 'getCachedResponse').and.returnValue(Promise.reject('fail'))
          spyOn(backend, 'onSendSuccess').and.returnValue(Promise.resolve(null))
          spyOn(backend, 'onSendFail').and.returnValue(Promise.resolve(null))
          spyOn(connection, 'request').and.returnValue(Promise.resolve('success'))

          backend.request({ url: '' }).then((resp) => {
              expect(backend['onSendSuccess']).toHaveBeenCalled()
            }).then(done, fail)

        })

      it('request fail', (done) => {

          spyOn(backend, 'getCachedResponse').and.returnValue(Promise.reject(null))
          spyOn(backend, 'onSendSuccess').and.returnValue(Promise.resolve(null))
          spyOn(backend, 'onSendFail').and.returnValue(Promise.resolve(null))
          spyOn(connection, 'request').and.returnValue(Promise.reject(null))

          backend.request({ url: '' }).then((resp) => {
              expect(backend['onSendFail']).toHaveBeenCalled()
            }).then(done, fail)

        })
    })
})
