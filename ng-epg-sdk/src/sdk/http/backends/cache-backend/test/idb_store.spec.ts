import { PolicyManager } from './../../../policy/policy_manager'
import { IDBStore } from '../idb_store'
import { FullCachePolicy } from '../../../interfaces'

// tslint:disable no-angle-bracket-type-assertion
describe('idb_store', () => {

  let idbStore: IDBStore
  let infCachePolicy: FullCachePolicy
  const RecordCount = 5
  let response
  beforeEach(() => {
      infCachePolicy = {
          url: {
              CACHE_SWITCH: true,
              SUCCESS_KEY: ['success'],
              CACHE_NUMBER: RecordCount,
              MAX_AGE: 1
            }
        }
      const policyManager = new PolicyManager()
      policyManager.setCachePolicy(infCachePolicy)
      idbStore = new IDBStore('id', 'url', policyManager)

      response = {
          status: 200,
          body: 'success',
          cacheDataType: 0
        }

    })

  it('get, hit cache', (done) => {
      idbStore.put('key', response).then(() => {
          idbStore.get('key').then((resp) => {
              expect(resp).not.toBeNull()
              done()
            })
        })
    })

  it('get, cache expired', (done) => {
      idbStore.put('key', response).then(() => {
          setTimeout(() => {
              idbStore.get('key').then(null, (resp) => {
                  expect(resp).toBeNull()
                  done()
                })
            }, 3000)
        })
    })

  it('get, cache expired, ignore expire', (done) => {
      idbStore.put('key', response).then(() => {
          setTimeout(() => {
              idbStore.get('key', true).then((resp) => {
                  expect(resp).not.toBeNull()
                  done()
                })
            }, 3000)
        })
    })

  it('clearCache', (done) => {
      idbStore.clearCache('key', 'url').then(() => {
          idbStore.getSize().then((count) => {
              expect(count).toEqual(0)
              done()
            })
        })
    })

  xit('ensureSize', (done) => {
      const promises: Array<Promise<boolean>> = []
      for (let index = 0; index <= RecordCount + 5; index++) {
          promises.push(idbStore.put(index + '', response))
        }
      Promise.all(promises).then(() => {
          idbStore.ensureSize().then(() => {
              idbStore.getSize().then((count) => {
                  expect(count).toEqual(Math.ceil(RecordCount * 0.75))
                  done()
                })
            })

        })
    })

})
