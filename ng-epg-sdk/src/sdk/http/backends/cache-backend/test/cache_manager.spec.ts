import { PolicyManager } from './../../../policy/policy_manager'
import { CacheManager } from '../cache_manager'
declare function spyOn<T> (object: T, method: string): jasmine.Spy

describe('CacheManager', () => {

  let cacheManager: CacheManager
  let policy: PolicyManager

  beforeEach(() => {
      policy = new PolicyManager()
      cacheManager = new CacheManager(policy)
    })

  it('get, profileID is empty', (done) => {

      spyOn(cacheManager, 'getCacheStore').and.returnValue(null)
      cacheManager.get({ url: '' }).then(fail, () => {
          expect(cacheManager['getCacheStore']).not.toHaveBeenCalled()
          done()
        })

    })

  it('get, profileID is not empty', (done) => {

      spyOn(cacheManager, 'getCacheStore').and.returnValue(null)
      cacheManager.setProfileID('1')
      policy.setCachePolicy({
          url: {
              CACHE_SWITCH: true
            }
        })

      cacheManager.get({ url: 'url' }).then(fail, () => {
          expect(cacheManager['getCacheStore']).toHaveBeenCalled()
          done()
        })

    })

  it('get, USE_CACHE', (done) => {

      spyOn(cacheManager, 'getCacheStore').and.returnValue(null)
      cacheManager.setProfileID('1')
      policy.setCachePolicy({
          url: {
              CACHE_SWITCH: true
            }
        })

      cacheManager.get({ url: 'url' }).then(fail, () => {
          expect(cacheManager['getCacheStore']).toHaveBeenCalled()
          done()
        })

    })

  it('get before ajax, USE_CACHE', (done) => {

      spyOn(cacheManager, 'getCacheStore').and.returnValue(null)
      cacheManager.setProfileID('1')
      policy.setCachePolicy({
          url: {
              CACHE_SWITCH: true
            }
        })

      cacheManager.get({ url: 'url' }).then(fail, () => {
          expect(cacheManager['getCacheStore']).toHaveBeenCalled()
          done()
        })

    })

  it('get after ajax, USE_CACHE', (done) => {

      spyOn(cacheManager, 'getCacheStore').and.returnValue(null)
      cacheManager.setProfileID('1')
      policy.setCachePolicy({
          url: {
              CACHE_SWITCH: true
            }
        })

      cacheManager.get({ url: 'url' }, true).then(fail, () => {
          expect(cacheManager['getCacheStore']).toHaveBeenCalled()
          done()
        })

    })

  it('setProfileID no need clear', (done) => {
      cacheManager.setProfileID('1')
      policy.setCachePolicy({
          url: {
              SUCCESS_KEY: ['"retCode":"000000000"'],
              CACHE_SWITCH: true,
              MAX_AGE: 36000,
              CACHE_NUMBER: 1
            }
        })
      cacheManager.put({ url: 'url' }, { status: 200, body: '"retCode":"000000000"', dataSource: 'FROM_SERVER' })
      cacheManager.setProfileID('1')
      cacheManager.get({ url: 'url' }).then((resp) => {
          expect(resp).toEqual({ status: 200, body: '"retCode":"000000000"', dataSource: 'FROM_VALID_CACHE' })
          done()
        })
    })

  it('setProfileID need clear', (done) => {
      cacheManager.setProfileID('1')
      policy.setCachePolicy({
          url: {
              SUCCESS_KEY: ['"retCode":"000000000"'],
              CACHE_SWITCH: true
            }
        })
      cacheManager.put({ url: 'url' }, { status: 200, body: '"retCode":"000000000"', dataSource: 'FROM_SERVER' })
      cacheManager.setProfileID('2')
      cacheManager.get({ url: 'url' }).then(null, (resp) => {
          expect(resp).toBeNull(null)
          done()
        })
    })

  it('getCacheStore success', () => {
      spyOn(cacheManager, 'getCachePolicyByInterface').and.returnValue({ CACHE_SWITCH: true })

      const cacheStore = cacheManager['getCacheStore']('inf')

      expect(cacheStore).not.toBeNull()
    })

  it('getCacheStore fail', () => {
      spyOn(cacheManager, 'getCachePolicyByInterface').and.returnValue(null)
      cacheManager.setProfileID('1')
      cacheManager.setProfileID('2')
      const cacheStore = cacheManager['getCacheStore']('inf')

      expect(cacheStore).toBeNull()
    })

  it('getCachePolicyByInterface', () => {
      policy.setCachePolicy({ inf: {} })
      expect(cacheManager['getCachePolicyByInterface']('inf')).not.toBeNull()
    })

  it('createCacheKey', () => {
      const req = { url: 'inf?a=1', body: { b: 2 } }

      expect(cacheManager['createCacheKey']('inf', req)).toEqual('1d55ccb7aca7002dd0942485149487912d4a727dd8152837aef2a7931da7702f')
    })
})
