import { GUEST } from './../../../lang/const'
import { PolicyManager } from './../../policy/policy_manager'
import { Logger } from './../../../../utils/logger'
import { DRIVER_SET, idbStoreManager } from './idb_store_manager'
import { Record } from './idb_record'
import { InfCachePolicy, LocalForageDbMethods, ResponseOptionsArgs } from './../../interfaces'
import { Config } from './../../../config'
import * as localforage from 'localforage'
const log = new Logger('IDBStore')
const KEYINDEX = 'keyIndex'

export class IDBStore {

  private currentLocalForage: LocalForageDbMethods

  private keyIndexes: string[] | null

  constructor (private profileID: string, private inf: string, private policyManager: PolicyManager) {
      idbStoreManager.addDataBaseStore(profileID)
    }

    /**
     *
     * get response from cache
     * @param {string} key
     * @param {boolean} [ignoreExpire=false]
     * @returns {Promise<ResponseOptionsArgs>}
     * @memberof IDBStore
     */
  public get (key: string, ignoreExpire = false): Promise<ResponseOptionsArgs> {
      log.debug('begin get key:%s ignoreExpire:%s', key, ignoreExpire)
      const currentDB = this.getCurrentDB()
      return currentDB.getItem(key).then((result: string) => {
          if (result) {
              this.getKeyIndexes().then((values: string[]) => {
                  values = values || []
                  const index = values.findIndex((value) => {
                      return value === key
                    })
                  if (index >= 0) {
                      values.splice(index, 1)
                    }
                  values.push(key)
                  this.keyIndexes = values
                })
              const record: Record = JSON.parse(result)
              log.debug('end get record:%s', record)
              if (record) {
                  const isValid = this.isValid(record)
                  if (ignoreExpire || isValid) {
                      record.response.dataSource = isValid ? 'FROM_VALID_CACHE' : 'FROM_EXPIRED_CACHE'
                      return Promise.resolve(record.response)
                    }
                }
            }
          return Promise.reject(null)

        }, (error) => {
          log.error('get value from cache failed, error:%s', error)
          return Promise.reject(null)
        })

    }

    /**
     *
     * save response to cache
     * @param {string} key
     * @param {ResponseOptionsArgs} response
     * @returns
     * @memberof IDBStore
     */
  public put (key: string, response: ResponseOptionsArgs): Promise<boolean> {
      log.debug('begin put key:%s', key)
        // when cache number is 0, not cache
      const infCachePolicy = this.getInfCachePolicy()
      const cacheNumber = infCachePolicy.CACHE_NUMBER || 0
      if (cacheNumber <= 0) {
          return Promise.resolve(false)
        }

      if (this.isSuccessResponse(response.body || '')) {
          return new Promise((resolve, reject) => {
              this.ensureSize().then(() => {
                  const record = new Record(response)
                  const currentDB = this.getCurrentDB()
                  const promise = new Promise((nResolve, nReject) => {
                      this.getKeyIndexes().then((values: string[]) => {
                          values = values || []
                          const index = values.findIndex((value) => {
                              return value === key
                            })
                          if (index >= 0) {
                              values.splice(index, 1)
                            }
                          values.push(key)
                          this.keyIndexes = values
                          currentDB.setItem(KEYINDEX, values).then(nResolve, (error) => {
                              log.error('put keyIndex error:%s', error)
                              nReject()
                            })
                        })
                    })
                  Promise.all([promise, currentDB.setItem(key, record.getResult())]).then(() => {
                      log.debug('end put key:%s', key)
                      resolve(true)
                    }, (e) => {
                      log.error('put key to cache error :%s', e)
                      reject(false)
                    })
                }).catch((e) => {
                  log.error('ensure size failed, error:%s', e)
                  reject(false)
                })
            })
        }

      return Promise.resolve(false)
    }

  public clearCache (profileID: string, url: string): Promise<boolean> {
      if (profileID) {
          log.debug('begin clearCache url:%s', url)
          if (url) {
              const currentDB = this.getCurrentDB()
              return new Promise((resolve, reject) => {
                  currentDB.clear().then(() => resolve(true), reject)
                })
            } else {
              return idbStoreManager.deleteDataBaseStore(profileID)
            }
        }
      return Promise.resolve(false)
    }

    /**
     * clear cache when reach limit size
     */
  public ensureSize (): Promise<void> {
      log.debug('begin ensureSize')
      const infCachePolicy = this.getInfCachePolicy()
      const LimitSize = infCachePolicy.CACHE_NUMBER || 0

      if (LimitSize <= 0) {
          idbStoreManager.clearAll()
          return Promise.resolve()
        }

      return new Promise<void>((resolve, reject) => {
          const currentDB = this.getCurrentDB()
          currentDB.keys().then((keys) => {
              log.debug('begin ensureSize keys:%s', keys)
                // check if is limited
              const isExceedLimit = keys && (keys.length >= LimitSize + 1)
              if (isExceedLimit) {
                  let reservedSize = Math.ceil(LimitSize - LimitSize * (Config.get('cache_clear_rate') || 0.25))
                  reservedSize = Math.min(LimitSize, reservedSize)
                    // when keys is more than LimitSize, delete all the extra keys to avoid reservedSize is more than LimitSize;
                  const maxSize = (keys.length - 1) > LimitSize ? keys.length : LimitSize
                  const deleteSize = maxSize - reservedSize || 1
                  return this.getKeyIndexes().then((values: string[]) => {
                      values = values || []
                      const promises: Array<Promise<void>> = []
                      for (let i = 0; i < deleteSize; i++) {
                          promises.push(currentDB.removeItem(values[i]))
                        }
                      values.splice(0, deleteSize)
                      this.keyIndexes = values
                      return Promise.all(promises).then(() => resolve(), reject)
                    }, () => {
                      return reject()
                    })
                }
              return resolve()
            })
        })

    }

    /**
     * keep keyIndex in memory
     */
  private getKeyIndexes (): Promise<string[]> {
      if (this.keyIndexes) {
          return Promise.resolve(this.keyIndexes)
        } else {
          const currentDB = this.getCurrentDB()
          return currentDB.getItem(KEYINDEX).then((values: string[]) => {
              this.keyIndexes = values || []
              return Promise.resolve(this.keyIndexes)
            }, () => {
              return Promise.resolve([])
            })
        }
    }

    /**
     * get cache size
     */
  public getSize (): Promise<number> {
      return new Promise((resolve, reject) => {
          const currentDB = this.getCurrentDB()
          currentDB.length().then((length) => {
                // keyIndex no need to count.
              resolve(Math.max(length - 1, 0))
            }, reject)
        })
    }

  private isValid (record: Record): boolean {
      const infCachePolicy = this.getInfCachePolicy()
      const maxAge = (this.profileID === GUEST ? infCachePolicy.GUEST_MAX_AGE : infCachePolicy.MAX_AGE) || 0
      return record.timestamp + maxAge * 1000 > Date.now()
    }

    /**
     * check if a success response
     */
  private isSuccessResponse (body: string | null): boolean {

      const infCachePolicy = this.getInfCachePolicy()
      if (body && infCachePolicy.SUCCESS_KEY) {
          return !!infCachePolicy.SUCCESS_KEY.find((key) => {
              return body.indexOf(key) >= 0
            })
        }

      if (!body && !infCachePolicy.SUCCESS_KEY) {
          return true
        }

      return false
    }

  private getCurrentDB (): LocalForageDbMethods {
      if (!this.currentLocalForage) {
          this.currentLocalForage = localforage.createInstance({
              storeName: this.inf,
              name: this.profileID,
              driver: DRIVER_SET
            })
        }
      return this.currentLocalForage
    }

  private getInfCachePolicy (): InfCachePolicy {
      return this.policyManager.getCachePolicyByInterface(this.inf) || {}
    }

}
