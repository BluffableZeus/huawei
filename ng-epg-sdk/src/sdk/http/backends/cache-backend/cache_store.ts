import { GUEST } from './../../../lang/const'
import { PolicyManager } from './../../policy/policy_manager'
import { Logger } from './../../../../utils/logger'
import { InfCachePolicy, ResponseOptionsArgs } from '../../interfaces'
import { Config } from '../../../config'
import { Record } from './cache_record'

const log = new Logger('CacheStore')

export class CacheStore {

  private store: { [key: string]: Record } = {}

  private size = 0

  constructor (private profileID: string, private inf: string, private policyManager: PolicyManager) {

    }

  public get (key: string, ignoreExpire = false): Promise<ResponseOptionsArgs> {
      const record = this.store[key]
      log.debug('begin get key:%s ignoreExpire:%s record:%s', key, ignoreExpire, record)

      if (record) {
          const isValid = this.isValid(record)
          if (ignoreExpire || isValid) {
              record.touch()
              record.response.dataSource = isValid ? 'FROM_VALID_CACHE' : 'FROM_EXPIRED_CACHE'
              return Promise.resolve(record.response)
            }
        }

      return Promise.reject(null)
    }

  public remove (key: string): void {
      log.debug('begin remove key:%s', key)
      const record = this.store[key]
      if (record) {
          delete this.store[key]
          this.size--
        }
    }

    /**
     * cache size
     */
  public getSize (): number {
      const size = this.size
      log.debug('getSize size:%s', size)
      return size
    }

    /**
     * clear cache
     */
  public clear (): void {
      log.debug('begin clear')
      this.store = {}
      this.size = 0
    }

    /**
     * clear cache when reach max-size
     */
  public ensureSize (): void {
      log.debug('begin ensureSize')

      const infCachePolicy = this.getInfCachePolicy()
      const LimitSize = infCachePolicy.CACHE_NUMBER || 0

      if (LimitSize <= 0) {
          log.debug('ensureSize clear')
          this.clear()
          return
        }

        // if size is max-size
      const isExceedLimit = this.getSize() > LimitSize
      if (isExceedLimit) {
          log.debug('ensureSize to delete cache')
          let sortedRecordList = this.getSortedRecordList()

            // save some record
          sortedRecordList = sortedRecordList.slice(-this.getReservedSize())

          this.resetStore(sortedRecordList)
        }
    }

    /**
     * save cache
     */
  public put (key: string, response: ResponseOptionsArgs): boolean {
      log.debug('begin put key:%s response:%s', key, response)
        // when cache number is 0, not cache
      const infCachePolicy = this.getInfCachePolicy()
      const cacheNumber = infCachePolicy.CACHE_NUMBER || 0
      if (cacheNumber <= 0) {
          return false
        }

      if (this.isSuccessResponse(response.body || '')) {

          if (!this.store[key]) {
              this.size++
            }

          this.store[key] = new Record(key, response)

          this.ensureSize()

          return true
        }

      return false
    }

  private isValid (record: Record): boolean {
      const infCachePolicy = this.getInfCachePolicy()
      const maxAge = (this.profileID === GUEST ? infCachePolicy.GUEST_MAX_AGE : infCachePolicy.MAX_AGE) || 0
      return record.timestamp + maxAge * 1000 > new Date().getTime()
    }

  private getReservedSize (): number {
      const reservedRatio = 1 - Config.get('backend_cache_clear_ratio', 0.25)
      const infCachePolicy = this.getInfCachePolicy()
      const cacheNumber = infCachePolicy.CACHE_NUMBER || 0
        // After the lastest result was cached, reservedSize need add one.
      const reservedSize = Math.ceil(cacheNumber * reservedRatio) + 1
      log.debug('getReservedSize reservedSize:%s', reservedSize)
      return Math.min(cacheNumber, reservedSize)
    }

  private getSortedRecordList (): Record[] {
      const keys = Object.keys(this.store)
      const recordList = keys.map((key: string) => {
          return this.store[key]
        })

      recordList.sort((a, b) => {
          return a.modificationTimestamp - b.modificationTimestamp
        })

      return recordList
    }

    /**
     * reset cache
     */
  private resetStore (recordList: Record[]): void {
      log.debug('resetStore')
      this.clear()

      recordList.forEach((record: Record) => {
          this.store[record.key] = record
        })

      this.size = recordList.length
    }

    /**
     * check if a success response
     */
  private isSuccessResponse (body: string | null): boolean {

      const infCachePolicy = this.getInfCachePolicy()
      if (body && infCachePolicy.SUCCESS_KEY) {
          return !!infCachePolicy.SUCCESS_KEY.find((key) => {
              return body.indexOf(key) >= 0
            })
        }

      if (!body && !infCachePolicy.SUCCESS_KEY) {
          return true
        }

      return false
    }

  private getInfCachePolicy (): InfCachePolicy {
      return this.policyManager.getCachePolicyByInterface(this.inf) || {}
    }

}
