import { PolicyManager } from './../../policy/policy_manager'
import { Connection, RequestOptionsArgs, ResponseOptionsArgs } from '../../interfaces'
export declare class CacheBackend implements Connection {
  private connection
  private policyManager
  private cacheManager
  constructor (connection: Connection, policyManager: PolicyManager);
  request (request: RequestOptionsArgs): Promise<ResponseOptionsArgs>
  setProfileID (profileID: string): void
  clearCache (url: string): Promise<boolean>
  getCacheCount (inf: string): Promise<number>
  getCache (inf: string, key: string): Promise<ResponseOptionsArgs>
  private getCachedResponse (request, isOnFail?)
  private onSendSuccess (request, response)
  private onSendFail (request, failResp)
  private mergeFailedResponse (resp, originCode)
}
