import { Logger } from './../../../../utils/logger'
import { idbStoreManager } from './idb_store_manager'
import { PolicyManager } from './../../policy/policy_manager'

import { RequestOptionsArgs, ResponseOptionsArgs, InfCachePolicy } from '../../interfaces'
import { extractUrlPath } from '../../../lang/extract_url_path'
import { sha256 } from '../../../lang/sha256'

import { CacheStore } from './cache_store'

import { IDBStore } from './idb_store'
import { urlParse } from '../../../lang/urlparse'

const log = new Logger('cache_manager')

let cacheStoreMapping: { [inf: string]: CacheStore } = {}
let idbStoreMapping: { [inf: string]: IDBStore } = {}
let currentProfileID = 'defaultKey'

export class CacheManager {

  constructor (private policyManager: PolicyManager) {

    }

    /**
     *
     * get response from cache
     * @param {RequestOptionsArgs} request
     * @param {boolean} [useCacheOnFail=false]
     * @returns {Promise<ResponseOptionsArgs>}
     * @memberof CacheManager
     */
  public get (request: RequestOptionsArgs, useCacheOnFail = false): Promise<ResponseOptionsArgs> {
      log.debug('begin get useCacheOnFail:%s', useCacheOnFail)
      if (!currentProfileID) {
          log.debug('profile is null,cannot get cache')
          return Promise.reject(null)
        }
      const inf = extractUrlPath(request.url)
      const useCache = this.checkUseCache(inf)
      if (!useCache) {
          log.debug('cacheswitch is false, no cache')
          return Promise.reject(null)
        }

      const cacheStore = this.getCacheStore(inf)
      const cacheKey = this.createCacheKey(inf, request)
      if (cacheStore) {
          log.debug('get cache from memory')
          return cacheStore.get(cacheKey, useCacheOnFail).then(null, () => {
              const idbStore = this.getIDBStore(inf)
              if (idbStore) {
                  log.debug('get cache from local')
                  return idbStore.get(cacheKey, useCacheOnFail)
                }
              return Promise.reject(null)
            })
        }

      log.debug('cannot get cache')
      return Promise.reject(null)
    }

    /**
     *
     * put response to cache
     * @param {RequestOptionsArgs} request
     * @param {ResponseOptionsArgs} response
     * @memberof CacheManager
     */
  public put (request: RequestOptionsArgs, response: ResponseOptionsArgs): Promise<boolean> {
      log.debug('begin put request:%s', request)
      const promise = this.matchCleanCondition(request.url, response)
      if (!currentProfileID) {
          log.debug('profile is null,cannot put cache')
          return promise
        }
      const inf = extractUrlPath(request.url)
      const useCache = this.checkUseCache(inf)
      if (!useCache) {
          log.debug('cacheswitch is false, no cache')
          return promise
        }

      const cacheStore = this.getCacheStore(inf)
      const cacheKey = this.createCacheKey(inf, request)

      if (cacheStore) {
          log.debug('put cache in memory')
          cacheStore.put(cacheKey, response)
        }
      const idbStore = this.getIDBStore(inf)
      if (idbStore) {
          log.debug('put cache in local')
          idbStore.put(cacheKey, response).then((resp) => {
              delete cacheStoreMapping[inf]
              return resp
            })
        }
      return promise
    }

    /**
     *set current profileID
     * @param {string} profileID current profileID
     * @memberof CacheManager
     */
  public setProfileID (profileID: string): void {
      const needClearCache = currentProfileID !== profileID
      log.debug('setProfileID needClearCache:%s', needClearCache)
        // if profileID changed, clear cache
      if (needClearCache) {
          this.clearMemoryCache()
        }

      currentProfileID = profileID
    }

    /**
     *
     * clear cache
     * @param {string} profileID  user profileID
     * @param {string} url  url(/VSP/V3/XXX)
     * @memberof CacheManager
     */
  public clearCache (url: string): Promise<boolean> {
      log.debug('clear memory and local cache by url:%s', url)
      if (url) {
          delete cacheStoreMapping[url]
          return this.clearLocalCache(currentProfileID, url)
        } else {
          log.debug('clear memory cache by profileID')
          this.clearMemoryCache()
          return idbStoreManager.deleteDataBaseStore(currentProfileID).then(() => {
              return Promise.resolve(true)
            })
        }
    }

  public getCacheCount (inf: string): Promise<number> {
      const useCache = this.checkUseCache(inf)
      if (!useCache) {
          log.debug('cacheswitch is false, no cache')
          return Promise.resolve(0)
        }
      return new Promise((resolve, reject) => {
          let count = 0
          const cacheStore = this.getCacheStore(inf)
          if (cacheStore) {
              count = cacheStore.getSize()
            }
          if (count === 0) {
              const idbStore = this.getIDBStore(inf)
              if (idbStore) {
                  return idbStore.getSize().then(resolve)
                }
            }
          return resolve(count)
        })
    }

  public getCache (inf: string, key: string): Promise<ResponseOptionsArgs> {
      if (!currentProfileID) {
          return Promise.reject(null)
        }
      const useCache = this.checkUseCache(inf)
      if (!useCache) {
          log.debug('cacheswitch is false, no cache')
          return Promise.reject(null)
        }
      const cacheStore = this.getCacheStore(inf)
      if (cacheStore) {
          return cacheStore.get(key, true).then(null, () => {
              const idbStore = this.getIDBStore(inf)
              if (idbStore) {
                  return idbStore.get(key, true)
                }
              return Promise.reject(null)
            })
        }
      return Promise.reject(null)
    }

    /**
     * clear memory cache
     */
  private clearMemoryCache () {
      cacheStoreMapping = {}
      idbStoreMapping = {}
    }

  private clearLocalCache (profileID: string, url: string): Promise<boolean> {
      const idbStore = this.getIDBStore(url)
      if (idbStore) {
          delete idbStoreMapping[url]
          return idbStore.clearCache(profileID, url)
        }
      return Promise.resolve(false)
    }

    /**
     * get cacheStore
     */
  private getCacheStore (inf: string): CacheStore | null {
      if (cacheStoreMapping[inf]) {
          log.debug('getCacheStore success from cache')
          return cacheStoreMapping[inf]
        }

      const useCache = this.checkUseCache(inf)
      if (useCache) {
          log.debug('getCacheStore success from new')
          const cacheStore = new CacheStore(currentProfileID, inf, this.policyManager)
          cacheStoreMapping[inf] = cacheStore
          return cacheStoreMapping[inf]
        }

      return null

    }

  private getIDBStore (inf: string): IDBStore | null {
        /**
         * no need to support persistency cache.
         * const infCachePolicy = this.getCachePolicyByInterface(inf);
         * const cachePersistency = infCachePolicy && infCachePolicy.CACHE_PERSISTENCY;
         * log.debug('getIDBStore cachePersistency:%s', cachePersistency);
         * if (!cachePersistency) {
         *     return null;
         * }
         * if (idbStoreMapping[inf]) {
         *     log.debug('getIDBStore success from cache');
         *     return idbStoreMapping[inf];
         * }
         * const useCache = this.checkUseCache(inf, infCachePolicy);
         * if (useCache) {
         *     log.debug('getIDBStore success from new');
         *     const idbStore = new IDBStore(currentProfileID, inf, this.policyManager);
         *     idbStoreMapping[inf] = idbStore;
         *     return idbStoreMapping[inf];
         * }
         */

      return null

    }

  private getCachePolicyByInterface (inf?: string): InfCachePolicy | null {
      return this.policyManager.getCachePolicyByInterface(inf)
    }

  private getFilterFromPolicy (): string[] {
      return this.policyManager.getFilterFromPolicy()
    }

    /**
     * generate cachekey
     */
  private createCacheKey (inf: string, request: RequestOptionsArgs): string {
      const params = urlParse(request.url)
      const filters = this.getFilterFromPolicy()
      filters.forEach((filter) => {
          delete params[filter]
        })
      return sha256(JSON.stringify({ url: inf, params, body: request.body }))
    }

  private matchCleanCondition (url: string, response: ResponseOptionsArgs): Promise<boolean> {
      log.debug('begin clean cache by url:%s', url)
      const inf = extractUrlPath(url)
      if (!this.needClearCache(inf, response.body)) {
          return Promise.resolve(false)
        }
      const matchGlobalCleanCondition = this.policyManager.matchGlocalCleanCondition(inf)
      if (matchGlobalCleanCondition) {
          log.debug('clean memeory cache')
          this.clearMemoryCache()
          return idbStoreManager.clearAll().then(() => {
              return Promise.resolve(true)
            }, () => {
              return Promise.resolve(false)
            })
        } else {
          const infCleanCondition = this.policyManager.getCleanConditionByInf(inf)
          if (infCleanCondition && infCleanCondition.length > 0) {
              log.debug('clean url cache infCleanCondition:%s', infCleanCondition)
              const promises: Array<Promise<boolean>> = []
              infCleanCondition.forEach((condition) => {
                  promises.push(this.clearCache(condition))
                })
              return Promise.all(promises).then(() => {
                  return Promise.resolve(true)
                }, () => {
                  return Promise.resolve(false)
                })
            }
          return Promise.resolve(false)
        }
    }

    /**
     * check if need clear cache
     */
  private needClearCache (inf: string, body?: string | null): boolean {
      const keys = this.policyManager.getSuccessKeyByInf(inf)
      if (body && keys) {
          return !!keys.find((key) => {
              return body.indexOf(key) >= 0
            })
        }

      if (!body && !keys) {
          return true
        }

      return false
    }

  private useCache (inf: string, infCachePolicy: InfCachePolicy | null): boolean {
      const cacheRule = infCachePolicy && infCachePolicy.CACHE_SWITCH
      const result = !!cacheRule
      log.debug('useCacheNotExpired result:%s', result)
      return result
    }

  private checkUseCache (inf: string, infCachePolicy?: InfCachePolicy | null): boolean {
      const policy = infCachePolicy || this.getCachePolicyByInterface(inf)
      return this.useCache(inf, policy)
    }

}
