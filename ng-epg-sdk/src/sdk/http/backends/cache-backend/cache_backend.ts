import { Logger } from './../../../../utils/logger'
import { PolicyManager } from './../../policy/policy_manager'
import { Connection, RequestOptionsArgs, ResponseOptionsArgs } from '../../interfaces'
import { CacheManager } from './cache_manager'
const log = new Logger('cache_backend')

export class CacheBackend implements Connection {

  private cacheManager: CacheManager

  constructor (private connection: Connection, private policyManager: PolicyManager) {
      this.cacheManager = new CacheManager(this.policyManager)
    }

  public request (request: RequestOptionsArgs): Promise<ResponseOptionsArgs> {
      log.debug('NetworkSDK send, url:%s request:%s', request.url, request)
      return this.getCachedResponse(request).then((cachedResp) => {
          log.debug('NetworkSDK send, before sendRequest, get response from cache success. url:%s response:%s', request.url, cachedResp)

            // get response from cache, return
          return cachedResp

        }, () => {
          log.debug('NetworkSDK send, before sendRequest, get response from cache failed.')
          const resp = this.connection.request(request)

          return resp.then((response) => {
                // send request success, return
              return this.onSendSuccess(request, response)
            }, (failResp) => {
                // send request failed, try get response from cache
              return this.onSendFail(request, failResp)
            })
        })
    }

    /**
     * set current profile
     *
     * @param {string} profileID
     * @memberof CacheBackend
     */
  public setProfileID (profileID: string): void {
      log.debug('begin setProfileID')
      this.cacheManager.setProfileID(profileID)
    }

    /**
     *
     * clear cache
     * @param {string} profileID
     * @param {string} url
     * @memberof CacheBackend
     */
  public clearCache (url: string): Promise<boolean> {
      log.debug('begin clearCache')
      return this.cacheManager.clearCache(url)
    }

  public getCacheCount (inf: string): Promise<number> {
      log.debug('begin getCacheCount inf:%s', inf)
      return this.cacheManager.getCacheCount(inf)
    }

  public getCache (inf: string, key: string): Promise<ResponseOptionsArgs> {
      log.debug('begin getCacheCount inf:%s key:%s', inf, key)
      return this.cacheManager.getCache(inf, key)
    }

    /**
     * try get response from cache
     */
  private getCachedResponse (request: RequestOptionsArgs, isOnFail = false): Promise<ResponseOptionsArgs> {
      log.debug('begin getCachedResponse')
      return this.cacheManager.get(request, isOnFail)
    }

    /**
     * send request success, cache it
     */
  private onSendSuccess (request: RequestOptionsArgs, response: ResponseOptionsArgs): Promise<ResponseOptionsArgs> {
      log.debug('NetworkSDK send, sendRequest success, url:%s response:%s', request.url, response)
      return this.cacheManager.put(request, response).then(() => {
          return Promise.resolve(response)
        })
    }

    /**
     * send request failed, try get response from cache
     */
  private onSendFail (request: RequestOptionsArgs, failResp: ResponseOptionsArgs): Promise<ResponseOptionsArgs> {
      log.debug('NetworkSDK send, sendRequest failed, url:%s response:%s', request.url, failResp)
      return this.getCachedResponse(request, true).then((resp) => {
          log.error('NetworkSDK send, sendRequest failed, get response from cache success. url:%s', request.url)
          log.debug('NetworkSDK send, sendRequest failed, get response from cache success. url:%s response:%s', request.url, resp)
          const response = this.mergeFailedResponse(resp, failResp.status || 0)
          return Promise.resolve(response)
        }, () => {
          log.error('NetworkSDK send, sendRequest failed, get response failed. url:%s', request.url)
          log.debug('NetworkSDK send, sendRequest failed, get response failed. url:%s, response:%s', request.url, failResp)
          return Promise.reject(failResp)
        })
    }

    /**
     *
     * when failed, merge response that get from cache
     * @private
     * @param {ResponseOptionsArgs} resp
     * @param {number} originStatus
     * @returns
     * @memberof CacheBackend
     */
  private mergeFailedResponse (resp: ResponseOptionsArgs, originCode: number): ResponseOptionsArgs {
      const result: ResponseOptionsArgs = {
          status: resp.status,
          body: resp.body,
          header: resp.header,
          dataSource: resp.dataSource,
          originCode
        }

      return result
    }

}
