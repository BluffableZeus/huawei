export declare class IDBStoreManager {
  private dbLocalForage
  constructor ();
  addDataBaseStore (dbName: string): Promise<void>
  deleteDataBaseStore (dbName: string): Promise<boolean>
  clearCache (profileID: string, url: string): Promise<boolean>
  clearAll (): Promise<void>
  getSize (profileID: string, url: string): Promise<number>
  private deleteDatabase (dbName)
}
export declare const idbStoreManager: IDBStoreManager
