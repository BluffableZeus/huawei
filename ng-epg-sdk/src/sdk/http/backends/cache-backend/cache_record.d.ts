import { ResponseOptionsArgs } from '../../interfaces'
export declare class Record {
  key: any
  response: ResponseOptionsArgs
  timestamp: number
  modificationTimestamp: number
  constructor (key: any, response: ResponseOptionsArgs, timestamp?: number);
  touch (): void
}
