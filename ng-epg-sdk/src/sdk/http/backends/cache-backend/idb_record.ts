import { ResponseOptionsArgs } from '../../interfaces'

export class Record {

  constructor (public response: ResponseOptionsArgs, public timestamp: number = new Date().getTime()) {
    }

  getResult (): string {
      return JSON.stringify({
          timestamp: this.timestamp,
          response: this.response
        })
    }

}
