import { GUEST } from './../../../lang/const'
import { Logger } from './../../../../utils/logger'
import { LocalForageDbMethods } from './../../interfaces'
import * as localforage from 'localforage'

const log = new Logger('IDBStoreManager')
const DBNAME = 'ProfileDB'
const STORENAME = 'Profile'

export const DRIVER_SET = [
  localforage.INDEXEDDB,
  localforage.WEBSQL,
  localforage.LOCALSTORAGE
]

export class IDBStoreManager {

  private dbLocalForage: LocalForageDbMethods

  constructor () {
      this.dbLocalForage = localforage.createInstance({
          storeName: STORENAME,
          name: DBNAME,
          driver: DRIVER_SET
        })
    }

    /**
     *
     * add a table that record profile, when reach max-size, delete
     * @private
     * @param {string} dbName
     * @memberof IDBStore
     */
  public addDataBaseStore (dbName: string): Promise<void> {
      log.debug('begin addDataBaseStore dbName:%s', dbName)
      return new Promise<void>((resolve, reject) => {
          this.dbLocalForage.keys().then((keys) => {
              log.debug('addDataBaseStore keys:%s', keys)
              if (!keys) {
                  return this.dbLocalForage.setItem(dbName, new Date().getTime()).then(() => resolve(), reject)
                }
              const promises: Array<Promise<void>> = []
              if (dbName === GUEST) {
                  promises.push(Promise.resolve())
                } else {
                  const deleteKeys = keys.filter((key) => {
                      return key !== dbName && key !== GUEST
                    })
                  if (deleteKeys) {
                      deleteKeys.forEach((deleteKey) => {
                          promises.push(this.deleteDatabase(deleteKey))
                          promises.push(this.dbLocalForage.removeItem(deleteKey))
                        })
                    }
                }
              return Promise.all(promises).then(() => {
                  this.dbLocalForage.setItem(dbName, new Date().getTime()).then(() => resolve(), reject)
                }, () => {
                  this.dbLocalForage.setItem(dbName, new Date().getTime()).then(() => resolve(), reject)
                })
            }).catch(resolve)
        })
    }

    /**
     * delete db
     * @param dbName
     */
  public deleteDataBaseStore (dbName: string): Promise<boolean> {
      log.debug('begin deleteDataBaseStore dbName:%s', dbName)
      return new Promise((resolve, reject) => {
          this.deleteDatabase(dbName).then(() => {
              this.dbLocalForage.keys().then((keys) => {
                  log.debug('deleteDataBaseStore keys:%s', keys)
                  if (!keys) {
                      return resolve(false)
                    }

                  const oldKey = keys.find((key) => {
                      return key === dbName
                    })
                  if (oldKey) {
                      this.dbLocalForage.removeItem(oldKey).then(() => resolve(true), reject)
                    } else {
                      resolve(false)
                    }
                }).catch(reject)
            })
        })
    }

  public clearCache (profileID: string, url: string): Promise<boolean> {
      log.debug('begin clearCache url:%s', url)
      return new Promise((resolve, reject) => {
          this.dbLocalForage.keys().then((keys) => {
              log.debug('clearCache keys:%s', keys)
              if (!keys) {
                  return resolve(false)
                }

              const oldKey = keys.find((key) => {
                  return key === profileID
                })
              if (oldKey) {
                  log.debug('clearCache from local')
                  const profileLocalForage = localforage.createInstance({
                      storeName: url,
                      name: profileID,
                      driver: DRIVER_SET
                    })
                  profileLocalForage.clear().then(() => resolve(true), reject)
                } else {
                  resolve(false)
                }
            }).catch(reject)
        })
    }

    /**
     * clear cache
     */
  public clearAll (): Promise<void> {
      log.debug('begin clearAll')
      return new Promise<void>((resolve, reject) => {
          this.dbLocalForage.keys().then((keys) => {
              log.debug('clearAll keys:%s', keys)
              if (!keys) {
                  return resolve()
                }
              const promises: Array<Promise<void>> = []
              keys.forEach((key) => {
                  promises.push(this.deleteDatabase(key))
                })
              promises.push(this.dbLocalForage.clear())
              Promise.all(promises).then(() => {
                  resolve()
                }, reject)
            })
        })
    }

  public getSize (profileID: string, url: string): Promise<number> {
      log.debug('begin getSize url:%s', url)
      return new Promise((resolve, reject) => {
          this.dbLocalForage.keys().then((keys) => {
              log.debug('getSize keys:%s', keys)
              if (!keys) {
                  return resolve(0)
                }

              const oldKey = keys.find((key) => {
                  return key === profileID
                })
              if (oldKey) {
                  log.debug('getSize from local')
                  const profileLocalForage = localforage.createInstance({
                      storeName: url,
                      name: profileID,
                      driver: DRIVER_SET
                    })
                  return profileLocalForage.length().then((length) => {
                        // keyIndex no need to count.
                      resolve(Math.max(length - 1, 0))
                    }, reject)
                } else {
                  return resolve(0)
                }
            })
        })
    }

  private deleteDatabase (dbName: string): Promise<void> {
      return new Promise<void>((resolve, reject) => {
          const idbRequest = indexedDB.open(dbName)
          idbRequest.onsuccess = (event) => {
              const db = idbRequest.result
              const promises: Array<Promise<void>> = []
              const list: string[] = db.objectStoreNames || []
              for (let i = 0; i < list.length; i++) {
                  const dbLocalforage = localforage.createInstance({
                      storeName: list[i],
                      name: dbName,
                      driver: DRIVER_SET
                    })
                  promises.push(dbLocalforage.clear())
                }
              Promise.all(promises).then(() => {
                  db.close()
                  resolve()
                }, (e) => {
                  log.error('deleteDatabase error:%s', e)
                  db.close()
                  resolve()
                })
            }
          idbRequest.onerror = (event) => {
              log.error('deleteDatabase onerror:%s', event)
              resolve()
            }
          idbRequest.onblocked = (event) => {
              log.error('deleteDatabase onblocked:%s', event)
              resolve()
            }
        })
    }
}

export const idbStoreManager = new IDBStoreManager()
