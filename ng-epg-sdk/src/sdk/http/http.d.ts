import { Connection, RequestOptionsArgs, ResponseOptionsArgs, FullCachePolicy } from './interfaces'
export declare class Http implements Connection {
  private connection
  private cacheBackend
  private policyManager
  constructor ();
  request (request: RequestOptionsArgs): Promise<ResponseOptionsArgs>
  setProfileID (profileID: string): void
  setCachePolicy (policy: FullCachePolicy): void
  clearCache (url: string): Promise<boolean>
  setTimeout (second: number): boolean
  count (inf: string): Promise<number>
  getCache (inf: string, key: string): Promise<ResponseOptionsArgs>
}
