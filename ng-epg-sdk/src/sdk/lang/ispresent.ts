export function isPresent (o: any): boolean {
  return o !== null && o !== undefined
}
