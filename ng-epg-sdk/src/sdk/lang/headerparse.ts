export const headerParse = (headerStr: string | null): { [key: string]: string } | null => {
  if (!headerStr) {
      return null
    }
  const result: { [key: string]: string } = {}
  const headers: string[] = headerStr.split('\n') || []
  headers.forEach((header) => {
      const index = header.indexOf(':')
      if (index > 0) {
          const key = header.slice(0, index)
          const value = header.slice(index + 1)
          result[key] = value.trim()
        }
    })
  return result
}
