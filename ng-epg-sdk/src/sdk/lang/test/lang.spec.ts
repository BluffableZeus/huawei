import { extractUrlPath, LimitCount, urlCache, resetCache } from '../extract_url_path'
import { sha256 } from '../sha256'
describe('extract_url_path', () => {

  beforeEach(() => {
      resetCache()
    });

  [{
      url: 'http://ip:port/a/b/c',
      inf: '/a/b/c'
    }, {
      url: 'http://127.0.0.1:8080/a/b/c',
      inf: '/a/b/c'
    }, {
      url: 'http://127.0.0.1/a/b/c',
      inf: '/a/b/c'
    }, {
      url: 'http://sohu.com:8080/a/b/c',
      inf: '/a/b/c'
    }, {
      url: 'http://sohu.com/a/b/c#a=b',
      inf: '/a/b/c'
    }, {
      url: 'http://www.sohu.com/a/b/c',
      inf: '/a/b/c'
    }, {
      url: 'https://www.sohu.com/a/b/c?a=b',
      inf: '/a/b/c'
    }, {
      url: '/a/b/c',
      inf: '/a/b/c'
    }, {
      url: 'a/b/c',
      inf: 'a/b/c'
    }].forEach(({ url, inf }: { url: string, inf: string }) => {
      it(`extractUrlPath("${url}") => ${inf}`, () => {
          expect(extractUrlPath(url)).toEqual(inf)
        })
    })

  it('cache limit', () => {
      expect(Object.keys(urlCache).length).toEqual(0)
      let count = 0
      Array(LimitCount).fill('').forEach(() => {
          extractUrlPath('' + count++)
        })

      expect(Object.keys(urlCache).length).toEqual(LimitCount)

      extractUrlPath('' + count++)

      expect(Object.keys(urlCache).length).toEqual(1)
    })

  it('sha256', () => {
      const str = '/VSP/V3/Authenticate'
      expect(sha256(str)).toEqual('92b62f52ced396e4ff3317b5a21cc525a61f5a92296dcb5ef27665a1112dfc9e')
    })

})
