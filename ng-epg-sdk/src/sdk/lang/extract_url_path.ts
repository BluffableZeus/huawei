const HttpPathReg = /(https?:\/\/[^\/]*)?([\/]?[^?#]*)/

export let urlCache: { [url: string]: string } = {}
export const LimitCount = 100

let count = 0

export function extractUrlPath (url: string): string {

  if (!Object.prototype.hasOwnProperty.call(urlCache, url)) {

        // 缓存超过上限后清空
      if (count >= LimitCount) {
          resetCache()
        }

        // 缓存解析结果
      count++
      const matchs = HttpPathReg.exec(url)
      if (matchs) {
          urlCache[url] = matchs[2]
        } else {
          urlCache[url] = ''
        }
    }

  return urlCache[url]
}

export function resetCache (): void {
  count = 0
  urlCache = {}
}
