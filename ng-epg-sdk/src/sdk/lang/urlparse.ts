/**
 *
 * 从url中获取?后的参数
 * @export
 * @param {string} url
 * @returns {{ [key: string]: string }}
 */
export function urlParse (url: string): { [key: string]: string } {
  const urls = url.split('?')
  const result = {}
  if (urls) {
      const param = urls[1]
      const params: string[] = param && param.split('&') || []
      params.sort().forEach((value) => {
          if (value) {
              const values = value.split('=')
              if (values) {
                  result[values[0]] = values[1] || null
                }
            }
        })
    }
  return result
}
