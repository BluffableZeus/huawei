export interface TokenRequest {
  lastUpdateTime: number
  lastTokenNumber: number
  tokenRate: number
  maxTokenNumber: number
}

export function getTokenNumber (req: TokenRequest): number {
  const now = new Date().getTime()
  const addedNumber = Math.floor((now - req.lastUpdateTime) / (req.tokenRate * 1000))
  return Math.min(req.lastTokenNumber + addedNumber, req.maxTokenNumber)
}
