export declare let urlCache: {
  [url: string]: string;
}
export declare const LimitCount = 100
export declare function extractUrlPath (url: string): string
export declare function resetCache (): void
