export declare const headerParse: (headerStr: string | null) => {
  [key: string]: string;
} | null
