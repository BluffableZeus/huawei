declare const unescape
const K: number[] = []
const isPrime = (num) => {
  const sqrtN = Math.sqrt(num)
  for (let factor = 2; factor <= sqrtN; factor++) {
      if (!(num % factor)) {
          return false
        }
    }

  return true
}

const getFractionalBits = (num) => {
    // tslint:disable-next-line:no-bitwise
  return ((num - (num | 0)) * 0x100000000) | 0
}

let n = 2
let nPrime = 0
while (nPrime < 64) {
  if (isPrime(n)) {
      K[nPrime] = getFractionalBits(Math.pow(n, 1 / 3))
      nPrime++
    }
  n++
}

const W: number[] = []

const processBlock = (H, M, offset) => {
    // Working variables
  let a = H[0]
  let b = H[1]
  let c = H[2]
  let d = H[3]
  let e = H[4]
  let f = H[5]
  let g = H[6]
  let h = H[7]
    // Computation
  for (let i = 0; i < 64; i++) {
      if (i < 16) {
            // tslint:disable-next-line:no-bitwise
          W[i] = M[offset + i] | 0
        } else {
          const gamma0x = W[i - 15]
            // tslint:disable-next-line:no-bitwise
          const gamma0 = ((gamma0x << 25) | (gamma0x >>> 7)) ^ ((gamma0x << 14) | (gamma0x >>> 18)) ^ (gamma0x >>> 3)

          const gamma1x = W[i - 2]
            // tslint:disable-next-line:no-bitwise
          const gamma1 = ((gamma1x << 15) | (gamma1x >>> 17)) ^ ((gamma1x << 13) | (gamma1x >>> 19)) ^ (gamma1x >>> 10)

          W[i] = gamma0 + W[i - 7] + gamma1 + W[i - 16]
        }

        // tslint:disable-next-line:no-bitwise
      const ch = (e & f) ^ (~e & g)
        // tslint:disable-next-line:no-bitwise
      const maj = (a & b) ^ (a & c) ^ (b & c)

        // tslint:disable-next-line:no-bitwise
      const sigma0 = ((a << 30) | (a >>> 2)) ^ ((a << 19) | (a >>> 13)) ^ ((a << 10) | (a >>> 22))
        // tslint:disable-next-line:no-bitwise
      const sigma1 = ((e << 26) | (e >>> 6)) ^ ((e << 21) | (e >>> 11)) ^ ((e << 7) | (e >>> 25))

      const t1 = h + sigma1 + ch + K[i] + W[i]
      const t2 = sigma0 + maj

      h = g
      g = f
      f = e
        // tslint:disable-next-line:no-bitwise
      e = (d + t1) | 0
      d = c
      c = b
      b = a
        // tslint:disable-next-line:no-bitwise
      a = (t1 + t2) | 0
    }

    // Intermediate hash value
    // tslint:disable-next-line:no-bitwise
  H[0] = (H[0] + a) | 0
    // tslint:disable-next-line:no-bitwise
  H[1] = (H[1] + b) | 0
    // tslint:disable-next-line:no-bitwise
  H[2] = (H[2] + c) | 0
    // tslint:disable-next-line:no-bitwise
  H[3] = (H[3] + d) | 0
    // tslint:disable-next-line:no-bitwise
  H[4] = (H[4] + e) | 0
    // tslint:disable-next-line:no-bitwise
  H[5] = (H[5] + f) | 0
    // tslint:disable-next-line:no-bitwise
  H[6] = (H[6] + g) | 0
    // tslint:disable-next-line:no-bitwise
  H[7] = (H[7] + h) | 0
}

const bytesToWords = (bytes) => {
  const words: number[] = []
  for (let i = 0, b = 0; i < bytes.length; i++ , b += 8) {
        // tslint:disable-next-line:no-bitwise
      words[b >>> 5] |= bytes[i] << (24 - b % 32)
    }
  return words
}

const wordsToBytes = (words: number[]) => {
  const bytes: number[] = []
  for (let b = 0; b < words.length * 32; b += 8) {
        // tslint:disable-next-line:no-bitwise
      bytes.push((words[b >>> 5] >>> (24 - b % 32)) & 0xFF)
    }
  return bytes
}

const stringToBytes = (str: string) => {
  return str.split('').map((x) => {
      return x.charCodeAt(0)
    })
}

const padLeft = (orig, len) => {
  if (orig.length > len) {
      return orig
    }
  return Array(len - orig.length + 1).join('0') + orig
}

const bytesToHex = (bytes) => {
  return bytes.map((x) => {
      return padLeft(x.toString(16), 2)
    }).join('')
}
export function sha256 (message: string): string {
  const messageBytes = stringToBytes(unescape(encodeURIComponent(message)))

  const H = [0x6A09E667, 0xBB67AE85, 0x3C6EF372, 0xA54FF53A, 0x510E527F, 0x9B05688C, 0x1F83D9AB, 0x5BE0CD19]

  const m = bytesToWords(messageBytes)
  const l = messageBytes.length * 8

    // tslint:disable-next-line:no-bitwise
  m[l >> 5] |= 0x80 << (24 - l % 32)
    // tslint:disable-next-line:no-bitwise
  m[((l + 64 >> 9) << 4) + 15] = l

  for (let i = 0; i < m.length; i += 16) {
      processBlock(H, m, i)
    }

  const digestbytes = wordsToBytes(H)
  return bytesToHex(digestbytes)
}
