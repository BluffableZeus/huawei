import { isPresent } from './lang/ispresent'

export const STORE: { [key: string]: any } = {}

export const Config = {
    /**
     *
     */
  get: (key: string, fallbackValue: any = undefined): any => {
      const value = STORE[key]

      if (isPresent(value)) {
          return value
        }

      return fallbackValue
    },

    /**
     *
     */
  set: (key: string, value: any) => {
      STORE[key] = value
    }

}
