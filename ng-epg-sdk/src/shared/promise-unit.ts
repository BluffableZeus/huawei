export class PromiseWrapper {
  static completer (): PromiseCompleter<any> {
      let resolve, reject
      let promise = new Promise((res, rej) => {
          resolve = res
          reject = rej
        })
      return {
          promise,
          resolve,
          reject
        }
    }
}

export interface PromiseCompleter<T> {
  promise: Promise<T>
  resolve: (value?: T) => void
  reject: (value?: T) => void
}
