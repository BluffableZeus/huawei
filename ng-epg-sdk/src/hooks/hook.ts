
/**
 *
 * Hooks can be dynamically injected with new features
 *
 * @export
 * @interface Hook
 */
export interface Hook<T, K> {

    /**
     *
     *
     * @param {any} data Request parameter
     * @param {(data: any) => any} next Original business method
     * @returns {*}
     *
     * @memberOf Hook
     */
  onHook (data: T, next: (data: T) => K, hookName: string): K | Promise<K>
}
