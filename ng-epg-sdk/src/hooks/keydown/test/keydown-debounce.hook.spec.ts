import { PromiseWrapper, PromiseCompleter } from './../../../shared/promise-unit'
import { Subject } from 'rxjs/Rx'
import { AbortAjaxHook } from './../../ajax/abort-ajax.hook'
import { KeydownService } from './../../../services/keydown.service'
import { KeydownDebounceHook } from './../keydown-debounce.hook'
describe('keydown-debounce.hook', () => {

  let hook: KeydownDebounceHook

  let abortAjaxHook, keydownService

  beforeEach(() => {
      abortAjaxHook = {
          ajaxComplete: Promise.resolve()
        }
      keydownService = {
          keyupObservable: new Subject<any>()
        }
      hook = new KeydownDebounceHook(abortAjaxHook as AbortAjaxHook, keydownService as KeydownService)
    })

  it('isRestrictKeyCodes true', () => {
      spyOn(hook, 'isRestrictKeyCodes').and.returnValue(true)
      spyOn(hook, 'disablePressAndHold').and.returnValue(Promise.reject(null))
      hook.onHook({} as KeyboardEvent, () => {
          return null
        })

      expect(hook.disablePressAndHold).toHaveBeenCalled()
    })

  it('isRestrictKeyCodes false', () => {
      spyOn(hook, 'isRestrictKeyCodes').and.returnValue(false)
      spyOn(hook, 'disablePressAndHold').and.returnValue(Promise.reject(null))
      hook.onHook({} as KeyboardEvent, () => {
          return null
        })

      expect(hook.disablePressAndHold).not.toHaveBeenCalled()
    })

  it('disablePressAndHold', (done) => {
      let count = 0

      spyOn(hook, 'isRestrictKeyCodes').and.returnValue(true)
      const cb = () => {
          return hook.disablePressAndHold({} as KeyboardEvent).then(() => {
              count++
            }, () => { })
        }

      cb().then(() => {
            // The first time Keydown, normal handling
          expect(count).toEqual(1)
        }).then(() => {
            // In the absence of Keyup, no longer accept keydown
          return cb().then(() => {
              expect(count).toEqual(2)
            })
        }).then(() => {
            // Issue a Keyup event
          keydownService.keyupObservable.next()
        }).then(() => {
            // Again Keydown, normal handling
          return cb().then(() => {
              expect(count).toEqual(3)
            })
        }).then(() => {
            // Before Keyup, Keydown does not handle
          return cb().then(() => {
              expect(count).toEqual(4)
            })
        }).then(done)
    })

  it('disableDuringAjax', (done) => {
      const promiseCompleter: PromiseCompleter<any> = PromiseWrapper.completer()
      abortAjaxHook.ajaxComplete = () => promiseCompleter.promise

      let count = 0
      let handledEvent

      const next = (_event: KeyboardEvent) => {
          count++
          handledEvent = _event
          return Promise.resolve(_event)
        }

      const cb = (keyCode = 0) => {
          return hook.disableDuringAjax({ keyCode: keyCode } as KeyboardEvent, next).then(() => {

            }, () => {

            })
        }

      cb(0)
            .then(() => {
                // For the first time can be handled normally
              expect(count).toBe(1)
                // After a few times, will not deal with, so Ajax has not yet finished processing
              return cb(1)
            }).then(() => {
              expect(count).toBe(1)
              return cb(2)
            }).then(() => {
              expect(count).toBe(1)
              return cb(3)
            }).then(() => {
                // End the Ajax request
              return promiseCompleter.resolve()
            }).then(() => {
                // The last event was processed
              expect(handledEvent.keyCode).toBe(3)
              expect(count).toBe(2)

                // And then re-handle the button
              return cb(10)
            }).then(() => {
              expect(handledEvent.keyCode).toBe(10)
              expect(count).toBe(3)
              done()
            })
    })

  it('executeNext, same event', () => {
      let event

      hook['executeNext'](<OnKeyDownCallback>(e) => {
          event = e
        }, { keyCode: 0 } as KeyboardEvent, { keyCode: 0 } as KeyboardEvent)

      expect(event).not.toBeDefined()
    })

  it('executeNext, different event', () => {
      let event

      hook['executeNext'](<OnKeyDownCallback>(e) => {
          event = e
        }, { keyCode: 0 } as KeyboardEvent, { keyCode: 1 } as KeyboardEvent)

      expect(event).toBeDefined()
      expect(event.keyCode).toEqual(1)
    })
})
