import { Logger } from './../../utils/logger'
import { KeydownService, OnKeyDownCallback } from './../../services/keydown.service'
import { AbortAjaxHook } from './../ajax/abort-ajax.hook'
import * as _ from 'underscore'
import { Injectable } from '@angular/core'
import { Hook } from '../hook'
import { Config } from '../../core'

const log = new Logger('keydown-debounce.hook')

/**
 *
 * When disaster recovery, prohibit some requests to the background to send
 *
 * @export
 * @implements {Hook}
 */
@Injectable()
export class KeydownDebounceHook implements Hook<KeyboardEvent, Promise<KeyboardEvent>> {

  private index

  private isAjaxing = false

  private lastEvent: KeyboardEvent

  constructor (private abortAjaxHook: AbortAjaxHook, private keydownService: KeydownService) {
    }

  onHook (event: KeyboardEvent, next: (event: KeyboardEvent) => Promise<KeyboardEvent>): Promise<KeyboardEvent> {

      if (this.isRestrictKeyCodes(event.keyCode)) {
          return this.disablePressAndHold(event)
                .then(() => {
                  return this.disableDuringAjax(event, next)
                })
        } else {
          this.lastEvent = null
          return next(event)
        }
    }

  disablePressAndHold (event: KeyboardEvent): Promise<KeyboardEvent> {
      return Promise.resolve(event)
        // wroker todo
        // if (this.isHolding) {
        //     return Promise.reject(`keycode:${event.keyCode} is holding, so ignore.`);
        // } else {
        //     this.isHolding = true;
        //     log.error('disablePressAndHold, begin holding.');
        //     this.keydownService.keyupObservable.take(1).subscribe(() => {
        //         log.debug('disablePressAndHold, end holding.');
        //         this.isHolding = false;
        //     });
        //     return Promise.resolve(event);
        // }
    }

    /**
     * The event is not processed in the execution of the Ajax process
     */
  disableDuringAjax (event: KeyboardEvent, next: (event: KeyboardEvent) => Promise<KeyboardEvent>): Promise<KeyboardEvent> {

      if (this.isAjaxing) {
          const msg = `pre keydown's ajax is not complete, so ignore current keydown:${event.keyCode}`
          log.warn(msg)
          this.lastEvent = event
          return Promise.reject(msg)
        } else {

          this.lastEvent = null
          this.isAjaxing = true
          const timestamp = Date.now()
          return next(event).then(() => {
              this.abortAjaxHook.ajaxComplete(timestamp).then(() => {
                    // Reset, prevent callback throw exception, can not reset the state
                  this.isAjaxing = false
                  const lastEvent = this.lastEvent
                  this.lastEvent = null

                  this.executeNext(next, event, lastEvent)
                }).catch(() => this.isAjaxing = false)

              return event
            }).catch(() => {
              this.isAjaxing = false
              return event
            })
        }
    }

  private executeNext (next: OnKeyDownCallback, oldEvent: KeyboardEvent, lastEvent: KeyboardEvent) {
      if (lastEvent && lastEvent.keyCode !== oldEvent.keyCode) {
          next(lastEvent)
        }
    }

  private isRestrictKeyCodes (keycode: number) {
      if (!this.index) {
          this.index = _.object(_.zip(Config.get('debounce_keycodes', [])))
          log.warn('debounce_keycodes index:%s', this.index)
        }
      return _.has(this.index, '' + keycode)
    }

}
