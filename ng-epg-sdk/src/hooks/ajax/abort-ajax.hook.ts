import { AjaxResponse, AjaxRequest } from './../../utils/ajax'
import * as _ from 'underscore'
import { Injectable, NgZone } from '@angular/core'
import { Hook } from '../hook'
import { Logger } from '../../utils'

const log = new Logger('abort-ajax.hook')

type CacheItem = { request: AjaxRequest, result: Promise<AjaxResponse>, timestamp: number }

/**
 *
 * Block all Ajax requests and provide Abort requests
 *
 * @export
 * @class AbortAjaxHook
 * @implements {Hook}
 */
@Injectable()
export class AbortAjaxHook implements Hook<AjaxRequest, AjaxResponse> {

  private _runningRequestList: Array<CacheItem> = []

  constructor (private ngZone: NgZone) {

    }

  onHook (request: AjaxRequest, next): Promise<AjaxResponse> {

        // If the incoming abort method, then saved, if not pass, then the implementation of the last abort method
      request.abort = (() => {
          let _xhrAbort
          return (xhrAbort?: Function) => {
              if (xhrAbort) {
                  _xhrAbort = xhrAbort
                } else {
                  if (_xhrAbort) {
                      _xhrAbort()
                    }
                }
            }
        })()

      const retPromise: Promise<AjaxResponse> = next(request)
      const cacheItem = { request: request, result: retPromise, timestamp: Date.now() }

      this.addRequest(cacheItem)
      return retPromise.then((resp) => {
          this.removeRequest(cacheItem)
          return resp
        }, (resp) => {
          this.removeRequest(cacheItem)
          return Promise.reject(resp)
        })
    }

  private addRequest (cacheItem: CacheItem) {
      this._runningRequestList.push(cacheItem)
    }

  private removeRequest (cacheItem: CacheItem) {
      this._runningRequestList = _.without(this._runningRequestList, cacheItem)
    }

    /**
     *
     * Cancel the Ajax request, if you do not pass the interface name, then cancel all
     *
     * The interface is named the last paragraph in the URL
     *
     */
  abortAjax (...interfaceNames) {

        // If there is no interface name, then interrupt all the normal request Ajax
      if (_.isEmpty(interfaceNames)) {
          interfaceNames.push('all')
        }

      _.each(interfaceNames, (interfaceName: string) => {

          _.each(this._runningRequestList, ({ request, result }: CacheItem) => {

              const shouldInterupt = 'all' === interfaceName || request.inf === interfaceName
              if (shouldInterupt) {
                  const abort = request.abort as Function
                  if (_.isFunction(abort)) {
                      log.warn('abort ajax:%s', request.inf)
                      abort()
                    }
                }
            })
        })
    }

    /**
     * - Ajax request, then wait for Ajax after the implementation of the notice
     * - There is no Ajax request, then notice immediately
     */
  ajaxComplete (timestampArg: number = Date.now()): Promise<any> {

      const promises: Array<Promise<AjaxResponse>> = this.getRunningPromises(timestampArg)

      if (promises.length) {
          log.debug('there are running ajax request, count:%s, timestamp:%s', promises.length, timestampArg)
          const cb = () => {
              return new Promise((resolve, reject) => {
                  this.ngZone.runOutsideAngular(() => {
                      setTimeout(() => {
                          if (this.getRunningPromises(timestampArg).length) {
                              return this.ajaxComplete(timestampArg).then(resolve, resolve)
                            } else {
                              log.debug('running ajax complete, timestamp:%s.', timestampArg)
                              this.ngZone.run(resolve)
                            }
                        })
                    })
                })
            }

          return Promise.all(promises).then(cb, cb)
        } else {
          return Promise.resolve(null)
        }
    }

  private getRunningPromises (timestampArg): Array<Promise<any>> {
      const promises: Array<Promise<AjaxResponse>> = this._runningRequestList.filter(({ timestamp }: CacheItem) => {
            // Filter out the request after pressing the button
            return timestampArg <= timestamp
        }).map(({ result }: CacheItem) => {
          return result
        })

      return promises
    }
}
