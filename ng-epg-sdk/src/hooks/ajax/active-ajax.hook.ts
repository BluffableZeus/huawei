import { Injectable } from '@angular/core'
import { Hook } from '../hook'
import { AjaxRequest, AjaxResponse } from '../../utils'
import { EventEmitter2 } from 'eventemitter2'

/**
 *
 * Judge the Ajax interface to return success or failure
 *
 * - The retCode returned by the background is 00000000, or if retCode is not returned, it is successful
 * - Http Status is not 200 when it fails, retCode is set to http.xxx
 *
 * @export
 * @class CheckResponseHook
 * @implements {Hook}
 */
@Injectable()
export class ActiveAjaxHook implements Hook<AjaxRequest, AjaxResponse> {

  private emitter = new EventEmitter2()

  private semaphore = 0

  private active = true

  public setActive (active: boolean) {
      this.active = active
    }

  public onHook (request: AjaxRequest, next) {

      const promise = next(request)

      if (this.active) {
          this.monitorAjax(promise, request)
        }

      return promise
    }

  public ajaxStart (cb: (req: AjaxRequest) => void) {
      this.emitter.on('ajaxStart', cb)
    }

  public ajaxStop (cb: (resp: AjaxResponse) => void) {
      this.emitter.on('ajaxStop', cb)
    }

  private monitorAjax (promise: Promise<any>, request: AjaxRequest) {

      this.changeSemaphore(1, request)

      const afterAjax = (resp) => {
          this.changeSemaphore(-1, resp)
        }

      promise.then(afterAjax, afterAjax)
    }

  private changeSemaphore (offset: number, arg) {

      if (0 === this.semaphore && 1 === offset) {
          this.emitter.emit('ajaxStart', arg)
        } else if (1 === this.semaphore && -1 === offset) {
          this.emitter.emit('ajaxStop', arg)
        }

      this.semaphore += offset
    }
}
