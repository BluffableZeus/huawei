import { LimitMap } from './../../utils/limit-map'
import { PromiseWrapper } from './../../shared/promise-unit'
import { Config } from './../../core/config'
import { AjaxResponse, AjaxRequest } from './../../utils/ajax'
import * as _ from 'underscore'
import { Injectable, NgZone } from '@angular/core'
import { Hook } from '../hook'
import { Logger } from '../../utils'

const log = new Logger('ajax-cache.hook')

const DEFAULT_CACHE_CONFIG = {
    // max cached item size
  limit: 100,
    // invalid item after timeout
  timeout: 15 * 60 * 1000,
    // generate cache key
  key: (req: any) => {
      return JSON.stringify(req)
    },

  transform: (req: any) => {
      return req
    }
}

export interface CacheItemConfig {
  suffix: string
  timeout?: number
  limit?: number
  key?: ((req: any) => string) | string | Array<string>
  rejectSuffixes?: Array<string>
  transform?: (data: any) => any
}

/**
 *
 * Block all Ajax requests and provide Abort requests
 *
 * @export
 * @class AbortAjaxHook
 * @implements {Hook}
 */
@Injectable()
export class AjaxCacheHook implements Hook<AjaxRequest, AjaxResponse> {

  private limitMapping: { [inf: string]: LimitMap<Promise<any>> } = {}

  private noCache = {}

  private infToItemConfigMapping: { [key: string]: CacheItemConfig } = {}

  private suffixToInf: { [key: string]: string } = {}

  private infToSuffix: { [key: string]: string } = {}

  private rejectSuffixIndex: { [key: string]: Array<string> }

  constructor (private ngZone: NgZone) {
    }

  onHook (request: AjaxRequest, next): Promise<AjaxResponse> {

      let map: LimitMap<Promise<any>> = this.getCacheMap(request.inf)
      let retPromise: Promise<AjaxResponse>
        // Support cache processing
      if (map) {
          request = this.transformData(request)

          const key = this.getKey(request)
          const resultPromise = map.get(key)
          if (resultPromise) {
              log.debug('hit cache:%s request:%s', request.inf, request.data)
              retPromise = resultPromise.then((data) => {
                  log.debug('get cache response:%s data:%s', request.inf, data)

                  let resp: AjaxResponse = {}
                  resp.data = data
                  resp.status = 200
                  resp.usedTime = 0
                  return resp
                }, () => {
                  log.debug('pre cache fail:%s request:%s', request.inf, request.data)
                  return this.nextWithCache(key, map, request, next)
                })
            } else {
              log.debug('not hit cache:%s', request.inf)
              retPromise = this.nextWithCache(key, map, request, next)
            }
        } else {
          log.debug('does not hit cache:%s', request.inf)
          retPromise = next(request)
        }

      if (retPromise) {
          retPromise.then(() => {
              this.clearConflictCache(request.inf)
            })
        }

      return retPromise
    }

    /**
     * transform request data
     */
  transformData (request: AjaxRequest): AjaxRequest {
      const cacheItemConfig = this.getCacheItemConfig(request.inf)
      if (cacheItemConfig && cacheItemConfig.transform) {
          request.data = cacheItemConfig.transform(request.data)
        }
      return request
    }

    /**
     * Gets the cache of the interface
     */
  getCacheMap (inf: string): LimitMap<Promise<any>> {
      if (this.noCache[inf]) {
          return
        }

      const cacheItemConfigs = this.getCacheItemConfigs()

      this.buildRejectIndex(cacheItemConfigs)

      let map: LimitMap<Promise<any>> = this.limitMapping[inf]
      if (!map) {
          const cacheItemConfig = this.getCacheItemConfig(inf)
          if (cacheItemConfig) {
              const limit = cacheItemConfig.limit || DEFAULT_CACHE_CONFIG.limit
              const timeout = cacheItemConfig.timeout || DEFAULT_CACHE_CONFIG.timeout
              map = new LimitMap<Promise<any>>(limit, timeout)
              this.limitMapping[inf] = map
            } else {
              this.noCache[inf] = true
            }
        }

      return map
    }

  buildRejectIndex (cacheItemConfigs: Array<CacheItemConfig>) {
      if (!this.rejectSuffixIndex && cacheItemConfigs) {
          this.rejectSuffixIndex = {}

            // Create an index of the mutex interface
          cacheItemConfigs.forEach((itemConfig: CacheItemConfig) => {
              _.each(itemConfig.rejectSuffixes || [], (rejectSuffix: string) => {
                  this.rejectSuffixIndex[rejectSuffix] = this.rejectSuffixIndex[rejectSuffix] || []
                  this.rejectSuffixIndex[rejectSuffix].push(itemConfig.suffix)
                })
            })
        }
    }

  getCacheItemConfigs (): Array<CacheItemConfig> {
      return Config.get('ajax_cache_config')
    }

  getCacheItemConfig (inf: string): CacheItemConfig {
      if (_.has(this.infToItemConfigMapping, inf)) {
          return this.infToItemConfigMapping[inf]
        }

      const cacheItemConfigs = this.getCacheItemConfigs()
      if (cacheItemConfigs) {
          this.infToItemConfigMapping[inf] = _.find(cacheItemConfigs, (itemConfig: CacheItemConfig) => {
              const regex = new RegExp(itemConfig.suffix + '$')
              if (regex.test(inf)) {
                  this.suffixToInf[itemConfig.suffix] = inf
                  this.suffixToInf[inf] = itemConfig.suffix
                  return true
                }
            })

          return this.infToItemConfigMapping[inf]
        }
    }

  getKey (request: AjaxRequest): string {
      const cacheItemConfig: CacheItemConfig = this.getCacheItemConfig(request.inf)

      const keyCfg = cacheItemConfig.key || DEFAULT_CACHE_CONFIG.key
      if (keyCfg instanceof Array) {
          return keyCfg.map((subKey: string) => {
              return request.data[subKey]
            }).join('_')
        } else if (keyCfg instanceof Function) {
          return keyCfg(request.data)
        } else {
          return request.data[keyCfg]
        }
    }

  nextWithCache (key: string, map: LimitMap<Promise<any>>, request: AjaxRequest, next): Promise<AjaxResponse> {

      const promiseWrapper = PromiseWrapper.completer()
      log.debug('add cache key:%s, inf:%s', key, request.inf)
      map.set(key, promiseWrapper.promise)
      const retPromise: Promise<AjaxResponse> = next(request)

      retPromise.then((resp: AjaxResponse) => {
          promiseWrapper.resolve(resp.data)
        }, (resp: AjaxResponse) => {
          map.remove(key)
          log.debug('remove cache key:%s, because ajax failed.')
          promiseWrapper.reject(resp.data)
        })

      return retPromise
    }

  clearConflictCache (inf: string) {
      const suffix = this.infToSuffix[inf]
      if (suffix) {
          const tobeClearSuffixes = this.rejectSuffixIndex[suffix] || []
          tobeClearSuffixes.forEach((clearSuffix: string) => {
              const _inf = this.suffixToInf[clearSuffix]
              const map = this.limitMapping[_inf]
              if (map) {
                  log.debug('clear cache:%s, because request:%s', _inf, inf)
                  map.clear()
                }
            })
        }
    }
}
