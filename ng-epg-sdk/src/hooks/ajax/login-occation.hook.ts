import { AjaxResponse } from './../../utils/ajax'
import * as _ from 'underscore'
import { Injectable } from '@angular/core'
import { Hook } from '../hook'
import { AjaxRequest } from '../../utils'
import { Config } from '../../core'
import { EventService } from '../../services'

/**
 *
 * When disaster recovery, prohibit some requests to the background to send
 *
 * @export
 * @class LoginOccationHook
 * @implements {Hook}
 */
@Injectable()
export class LoginOccationHook implements Hook<AjaxRequest, AjaxResponse> {

  onHook (request: AjaxRequest, next) {

      if (Config.loginOccasion) {

          if (_.contains(Config.loginOccasionInterfaces, request.inf)) {

              EventService.emitAsync('LOGIN_OCCASION')
              return Promise.reject({
                  data: {
                      result: {
                          retcode: 'LOGIN_OCCASION'
                        }
                    }
                })
            }
        }

      return next(request)
    }

}
