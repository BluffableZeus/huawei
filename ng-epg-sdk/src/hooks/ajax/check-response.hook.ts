import * as _ from 'underscore'
import { Injectable } from '@angular/core'
import { Hook } from '../hook'
import { AjaxRequest, AjaxResponse } from '../../utils'
import { Const } from '../../core'

/**
 *
 * Judge the Ajax interface to return success or failure
 *
 * - The retCode returned by the background is 00000000, or if retCode is not returned, it is successful
 * - Http Status is not 200 when it fails, retCode is set to http.xxx
 *
 * @export
 * @class CheckResponseHook
 * @implements {Hook}
 */
@Injectable()
export class CheckResponseHook implements Hook<AjaxRequest, AjaxResponse> {

  onHook (request: AjaxRequest, next) {
      return next(request)
            .then(this.checkAjaxResponseSuccess.bind(this), this.checkAjaxResponseFail.bind(this))
    }

  private checkAjaxResponseSuccess (response: AjaxResponse): Promise<AjaxResponse> | AjaxResponse {
      _.defaults(response, { data: { result: { retCode: Const.defaultRetCode } } })
      _.defaults(response.data, { result: { retCode: Const.defaultRetCode } })
      _.defaults(response.data.result, { retCode: Const.defaultRetCode })
        // if retCode is '',modify it to '32999'
      if (!response.data.result.retCode) {
          response.data.result.retCode = Const.defaultRetCode
        }
        // Determine whether the background processing is successful
      if (0 !== +response.data.result.retCode) {
          return Promise.reject<AjaxResponse>(response)
        }
      return response
    }

  private checkAjaxResponseFail (response: AjaxResponse): Promise<AjaxResponse> {
      response.data = response.data || {
          result: { retCode: 'http.' + response.status }
        }
      _.extend(response.data, {
          result: { retCode: 'http.' + response.status },
          message: response.xhr && response.xhr.statusText || response.status
        })

      return Promise.reject<AjaxResponse>(response)
    }

}
