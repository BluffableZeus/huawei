import { AjaxRequest, AjaxResponse } from './../../../utils/ajax'
import { NgZone } from '@angular/core'
import { AjaxCacheHook } from './../ajax-cache.hook'
describe('ajax-cache', () => {

  let ajaxCacheHook: AjaxCacheHook

  beforeEach(() => {
      ajaxCacheHook = new AjaxCacheHook(new NgZone())
    })

  it('does not support cache', () => {
      let flag = false
      const next = () => {
          flag = true
          return Promise.reject(null)
        }

      spyOn(ajaxCacheHook, 'getCacheMap').and.returnValue(null)

      ajaxCacheHook.onHook({} as AjaxRequest, next)

      expect(flag).toBeTruthy()
    })

  it('support cache, and current requst is cached', (done) => {
      let resp: AjaxResponse
      const map = {
          get: () => {
              return Promise.resolve('cached result')
            }
        }

      spyOn(ajaxCacheHook, 'getKey').and.returnValue('key1')
      spyOn(ajaxCacheHook, 'getCacheMap').and.returnValue(map)

      ajaxCacheHook.onHook({} as AjaxRequest, () => { }).then((r) => {
          resp = r
        })

      setTimeout(() => {
          expect(resp.data).toEqual('cached result')
          done()
        })
    })

  it('support cache, but current requst is not cached', () => {
      const map = {
          get: () => {
            }
        }

      spyOn(ajaxCacheHook, 'getKey').and.returnValue('key1')
      spyOn(ajaxCacheHook, 'getCacheMap').and.returnValue(map)
      spyOn(ajaxCacheHook, 'nextWithCache').and.returnValue('')

      ajaxCacheHook.onHook({} as AjaxRequest, () => { })

      expect(ajaxCacheHook.nextWithCache).toHaveBeenCalled()

    })

  it('support cache, current requst is caching, but failed', (done) => {
      const map = {
          get: () => {
              return Promise.reject('cached result')
            }
        }

      spyOn(ajaxCacheHook, 'getKey').and.returnValue('key1')
      spyOn(ajaxCacheHook, 'getCacheMap').and.returnValue(map)
      spyOn(ajaxCacheHook, 'nextWithCache').and.returnValue('')

      ajaxCacheHook.onHook({} as AjaxRequest, () => { })

      setTimeout(() => {
          expect(ajaxCacheHook.nextWithCache).toHaveBeenCalled()
          done()
        })
    })

  it('buildRejectIndex', () => {
      const cacheItemConfigs = []
      cacheItemConfigs.push({ suffix: 'A', rejectSuffixes: ['B', 'C'] })
      cacheItemConfigs.push({ suffix: 'D', rejectSuffixes: ['A', 'C'] })

      ajaxCacheHook.buildRejectIndex(cacheItemConfigs)

      expect(ajaxCacheHook['rejectSuffixIndex']['A']).toEqual(['D'])
      expect(ajaxCacheHook['rejectSuffixIndex']['C']).toEqual(['A', 'D'])
    })

  it('getCacheMap', () => {
      spyOn(ajaxCacheHook, 'buildRejectIndex').and.returnValue(null)
      spyOn(ajaxCacheHook, 'getCacheItemConfig').and.returnValue({
          suffix: 'API',
          timeout: 100,
          limit: 10
        })

      let map = ajaxCacheHook.getCacheMap('API')
      expect(map).toBeDefined()
    })

  it('getCacheItemConfig', () => {
      spyOn(ajaxCacheHook, 'getCacheItemConfigs').and.returnValue([{
          suffix: 'API',
          timeout: 100,
          limit: 10
        }])

      let cacheItemConfig = ajaxCacheHook.getCacheItemConfig('API')
      expect(cacheItemConfig).toBeDefined()

      cacheItemConfig = ajaxCacheHook.getCacheItemConfig('XXAPI')
      expect(cacheItemConfig).toBeDefined()

      cacheItemConfig = ajaxCacheHook.getCacheItemConfig('XXAPIX')
      expect(cacheItemConfig).not.toBeDefined()
    })

})
