
/**
 * Listens for events in the EventService
 *
 * @export
 * @interface OnNotification
 */
export interface OnNotification {

  /**
   * Event handling callback
   *
   * @param {string} eventName
   * @param {any} [value1]
   * @param {any} [value2]
   * @param {any} [value3]
   * @param {any} [value4]
   * @param {any} [value5]
   * @returns {*}
   *
   * @memberOf OnNotification
   */
  onNotification (eventName: string, value1?, value2?, value3?, value4?, value5?): any

}
