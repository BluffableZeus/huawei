export const Const = {
  'defaultRetCode': '32999'
}

export const msaErrorCodeMap = {
  http: {
      '0': '26000',
      '400': '26400',
      '401': '26401',
      '402': '26402',
      '403': '26403',
      '404': '26404',
      '405': '26405',
      '406': '26406',
      '407': '26407',
      '408': '26408',
      '409': '26409',
      '410': '26410',
      '411': '26411',
      '412': '26412',
      '413': '26413',
      '414': '26414',
      '415': '26415',
      '416': '26416',
      '417': '26417',
      '500': '26500',
      '501': '26501',
      '502': '26502',
      '503': '26503',
      '504': '26504',
      '505': '26505',
        // platform response longer than 10s
      '26752': '26752'
    },

  interface: {
        // Authenticate response missing ca info
      '38000': '38000',
        // QueryCustomizeConfig response missing ratings
      '38001': '38001',
        // tv_guide_display_mode ia invalid, must be '0' or '1'
      '38002': '38002',
        // loginOccasion params is empty
      '38003': '38003',
        // the password complexity error
      '38004': '38004',
        // TVMS config error
      '38005': '38005',
        // vod_4k_ability is invalid, must be '0' or '1'
      '38006': '38006',
        // currency_rate is invalid
      '38007': '38007',
        // CURRENCY_SYMBOL is invalid
      '38008': '38008',
        // CREDIT_LIMIT_LIST is invalid
      '38009': '38009',
        // language error
      '38010': '38010',
        // home recommend root category id is empty
      '38011': '38011',
      '38012': '38012',
      '38014': '38014',
        // The content has not been subscribed to and has no product available for subscription.
      '38015': '38015',
        // vod play url is empty
      '38016': '38016',
        // vod root category is empty
      '38018': '38018',
      '38019': '38019',
        // profile avatar is empty
      '38020': '38020',
      '38021': '38021',
      '38022': '38022',
        // json is invalid
      '38023': '38023',
      '38024': '38024',
      '38025': '38025',
        // has no master profile
      '38026': '38026',
      '38027': '38027',
        // platform is in loginOccasion status
      '38028': '38028',
      '38029': '38029',
      '38030': '38030',
        // channel or program is empty
      '38031': '38031',
        // bookmark is invalid
      '38032': '38032',
        // vod list is empth
      '38033': '38033',
      '38034': '38034',
        // report sqm info fail
      '38035': '38035'
    }
}

export class MSAError {
  static LOGIN_ROUTER_VSPURL_INVALID: string = '38036'
  static LOGIN_ROUTER_VSPHTTPSURL_INVALID: string = '38037'
  static LOGIN_ROUTER_VSPURL_DISABLED: string = '38038'

  static LOGIN_UPGRADE_INVALID: string = '38039'
  static LOGIN_NTP_INVALID: string = '38040'
  static LOGIN_TERMINALPARAM_INVALID: string = '38041'
  static LOGIN_SQM_INVALID: string = '38042'
  static LOGIN_MQMCURL_INVALID: string = '38043'

  static AUTHENTICATE_CA_INVALID: string = '38000'
  static AUTHENTICATE_DISASTER_RECOVERY_INVALID: string = '38003'
  static AUTHENTICATE_LOGOURL_INVALID: string = '38020'
  static AUTHENTICATE_MISSING_MASTER_PROFILE: string = '38026'
  static AUTHENTICATE_DISASTER_RECOVERY_STATE: string = '38028'

  static AUTHENTICATE_TOKEN_INVALID: string = '38044'
  static AUTHENTICATE_SESSIONID_INVALID: string = '38045'
  static AUTHENTICATE_VUID_INVALID: string = '38046'
  static AUTHENTICATE_PROFILEID_INVALID: string = '38047'
  static AUTHENTICATE_DEVICE_INVALID: string = '38048'
  static AUTHENTICATE_OTHER_PARAM_INVALID: string = '38049'
  static AUTHENTICATE_CA_VM_INVALID: string = '38050'
  static AUTHENTICATE_CA_PLAYREADY_INVALID: string = '38051'

  static QUERY_PROFILE_PROFILES_INVALID: string = '38053'

  static PERSONAL_VERSIONS_INVALID: string = '38054'

  static QUERY_LAUNCHER_LINK_INVALID: string = '38055'
  static QUERY_LAUNCHER_JSON_PARSE: string = '38056'

  static MASTER_ID_INVALID: string = '38060'

  static CHANNEL_DYNAMAIC_PROP_INVALID: string = '38061'

  static CONFIG_SPECIAL_CLUSTER_ID_INVALID: string = '38062'
  static CONFIG_VOD_SUBJECT_ID_INVALID: string = '38063'
  static CONFIG_VOD_4K_SUBJECT_ID_INVALID: string = '38064'
  static CONFIG_LIVETV_4K_SUBJECT_ID_INVALID: string = '38066'
  static CONFIG_CURRENCY_SYMBOL_INVALID: string = '38067'
  static CONFIG_CREDIT_LIMIT_LIST_INVALID: string = '38068'
  static CONFIG_OTT_CHANNEL_NAME_SPACE_INVALID: string = '38069'
  static CONFIG_DEFAULT_RECCFG_SINGLE_OR_SERIES_INVALID: string = '38070'
  static CONFIG_DEFAULT_RECCFG_DEFINITION_INVALID: string = '38071'
  static CONFIG_DEFAULT_RECCFG_PVR_TYPE_INVALID: string = '38072'
  static CONFIG_FORGOT_PASSWORD_URL_INVALID: string = '38073'
  static CONFIG_SQM_URL_INVALID: string = '38074'
  static CONFIG_SQM_USERNAME_INVALID: string = '38075'
  static CONFIG_SQM_KEY_INVALID: string = '38076'
  static CONFIG_BANNER_SUBJECT_ID_INVALID: string = '38077'
  static CONFIG_BANNER_DATA_COUNT_INVALID: string = '38078'
  static CONFIG_RATING_INVALID: string = '38001'
  static CONFIG_TVMS_INVAID: string = '38005'

  static VOD_LIST_EMPTY: string = '38033'

  static PLAY_NO_AVAILABLE_PRODUCTION_PACKAGE: string = '38015'
  static PLAY_NO_AVALIABLE_PLAYURL: string = '38016'
  static PLAY_BOOKMARK_INVALID: string = '38032'

  static QUERY_ALL_CHANNEL_LIST_EMPTY: string = '38031'
}

export const msaErrorMap = {
  '0': {
      'e': '26000',
      't': 'Operation failed',
      'm': 'No available network connection.',
      's': 'Please try again or contact your service provider for help.'
    },
  '400': {
      'e': '26400',
      't': 'Operation failed',
      'm': 'Network connection failed.',
      's': 'Please try again or contact your service provider for help.'
    },
  '401': {
      'e': '26401',
      't': 'Operation failed',
      'm': 'Network connection failed.',
      's': 'Please try again or contact your service provider for help.'
    },
  '402': {
      'e': '26402',
      't': 'Operation failed',
      'm': 'Network connection failed.',
      's': 'Please try again or contact your service provider for help.'
    },
  '403': {
      'e': '26403',
      't': 'Operation failed',
      'm': 'Access forbidden.',
      's': 'Please try again or contact your service provider for help.'
    },
  '404': {
      'e': '26404',
      't': 'Operation failed',
      'm': 'Network connection failed.',
      's': 'Please try again or contact your service provider for help.'
    },
  '405': {
      'e': '26405',
      't': 'Operation failed',
      'm': 'Network connection failed.',
      's': 'Please try again or contact your service provider for help.'
    },
  '406': {
      'e': '26406',
      't': 'Operation failed',
      'm': 'Network connection failed.',
      's': 'Please try again or contact your service provider for help.'
    },
  '407': {
      'e': '26407',
      't': 'Operation failed',
      'm': 'Network connection failed.',
      's': 'Please try again or contact your service provider for help.'
    },
  '408': {
      'e': '26408',
      't': 'Operation failed',
      'm': 'Network connection failed.',
      's': 'Please try again or contact your service provider for help.'
    },
  '409': {
      'e': '26409',
      't': 'Operation failed',
      'm': 'Network connection failed.',
      's': 'Please try again or contact your service provider for help.'
    },
  '410': {
      'e': '26410',
      't': 'Operation failed',
      'm': 'Network connection failed.',
      's': 'Please try again or contact your service provider for help.'
    },
  '411': {
      'e': '26411',
      't': 'Operation failed',
      'm': 'Network connection failed.',
      's': 'Please try again or contact your service provider for help.'
    },
  '412': {
      'e': '26412',
      't': 'Operation failed',
      'm': 'Network connection failed.',
      's': 'Please try again or contact your service provider for help.'
    },
  '413': {
      'e': '26413',
      't': 'Operation failed',
      'm': 'Network connection failed.',
      's': 'Please try again or contact your service provider for help.'
    },
  '414': {
      'e': '26414',
      't': 'Operation failed',
      'm': 'Network connection failed.',
      's': 'Please try again or contact your service provider for help.'
    },
  '415': {
      'e': '26415',
      't': 'Operation failed',
      'm': 'Network connection failed.',
      's': 'Please try again or contact your service provider for help.'
    },
  '416': {
      'e': '26416',
      't': 'Operation failed',
      'm': 'Network connection failed.',
      's': 'Please try again or contact your service provider for help.'
    },
  '417': {
      'e': '26417',
      't': 'Operation failed',
      'm': 'Network connection failed.',
      's': 'Please try again or contact your service provider for help.'
    },
  '500': {
      'e': '26500',
      't': 'Operation failed',
      'm': 'Network connection failed.',
      's': 'Please try again or contact your service provider for help.'
    },
  '501': {
      'e': '26501',
      't': 'Operation failed',
      'm': 'Network connection failed.',
      's': 'Please try again or contact your service provider for help.'
    },
  '502': {
      'e': '26502',
      't': 'Operation failed',
      'm': 'Network connection failed.',
      's': 'Please try again or contact your service provider for help.'
    },
  '503': {
      'e': '26503',
      't': 'Operation failed',
      'm': 'Network connection failed.',
      's': 'Please try again or contact your service provider for help.'
    },
  '504': {
      'e': '26504',
      't': 'Operation failed',
      'm': 'Network connection failed.',
      's': 'Please try again or contact your service provider for help.'
    },
  '505': {
      'e': '26505',
      't': 'Operation failed',
      'm': 'Network connection failed.',
      's': 'Please try again or contact your service provider for help.'
    },
  '26752': {
      'e': '26752',
      't': 'Operation failed',
      'm': 'Response timed out. Please try again.',
      's': 'Please try again or contact your service provider for help.'
    },
  '38001': {
      'e': '38001',
      't': 'Operation failed',
      'm': 'RatingList from VSP is empty. No data or ratings is emptyp.',
      's': 'Please try again or contact your service provider for help.'
    },
  '38002': {
      'e': '38002',
      't': 'Operation failed',
      'm': 'The terminal paramter tv_guide_display_mode is invalid(not 0 or 1). ',
      's': 'Please try again or contact your service provider for help.'
    },
  '38003': {
      'e': '38003',
      't': 'Operation failed',
      'm': 'The disaster recovery parameter is empty. PS：areaid,subnetId,bossIdtemplateName',
      's': 'Please try again or contact your service provider for help.'
    },
  '38004': {
      'e': '38004',
      't': 'Operation failed',
      'm': 'The password complexity configration is invalid(reserve).',
      's': 'Please try again or contact your service provider for help.'
    },
  '38005': {
      'e': '38005',
      't': 'Operation failed',
      'm': 'The TVMS configuration parameter is invalid. PS：IPTVIMPIP or IPTVIMPPort or IMDomain is empty.',
      's': 'Please try again or contact your service provider for help.'
    },
  '38006': {
      'e': '38006',
      't': 'Operation failed',
      'm': 'The terminal configuration parameter vod_4k_ability is invalid(not 0, 1).',
      's': 'Please try again or contact your service provider for help.'
    },
  '38007': {
      'e': '38007',
      't': 'Operation failed',
      'm': 'The VSP configuration parameter currency_rate is invalid. PS：empty or <=0.',
      's': 'Please try again or contact your service provider for help.'
    },
  '38010': {
      'e': '38010',
      't': 'Operation failed',
      'm': 'Lauguage configuration is invalid.\n1. The language list from VSP is empty, 2 Then language list from VSP does not contain local language list.',
      's': 'Please try again or contact your service provider for help.'
    },
  '38015': {
      'e': '38015',
      't': 'Operation failed',
      'm': 'No available production package.',
      's': 'Please try again or contact your service provider for help.'
    },
  '38016': {
      'e': '38016',
      't': 'Operation failed',
      'm': 'No avaliable play URL.',
      's': 'Please try again or contact your service provider for help.'
    },
  '38019': {
      'e': '38019',
      't': 'Operation failed',
      'm': 'The karaoke category ID format is invalid.',
      's': 'Please try again or contact your service provider for help.'
    },
  '38020': {
      'e': '38020',
      't': 'Operation failed',
      'm': 'No avaliable profile avatar.',
      's': 'Please try again or contact your service provider for help.'
    },
  '38021': {
      'e': '38021',
      't': 'Operation failed',
      'm': 'No avaliable poster.',
      's': 'Please try again or contact your service provider for help.'
    },
  '38022': {
      'e': '38022',
      't': 'Operation failed',
      'm': 'VOD data from VSP is abnormal.',
      's': 'Please try again or contact your service provider for help.'
    },
  '38024': {
      'e': '38024',
      't': 'Operation failed',
      'm': 'No avaliabel picture.',
      's': 'Please try again or contact your service provider for help.'
    },
  '38025': {
      'e': '38025',
      't': 'Operation failed',
      'm': 'No avaliabel channel logo.',
      's': 'Please try again or contact your service provider for help.'
    },
  '38026': {
      'e': '38026',
      't': 'Operation failed',
      'm': 'The admin profile is missing.',
      's': 'Please try again or contact your service provider for help.'
    },
  '38028': {
      'e': '38028',
      't': 'Operation failed',
      'm': 'The VSP is in disastar recovory state.（loginOccasion of authenticate response is 1）',
      's': 'Please try again or contact your service provider for help.'
    },
  '38029': {
      'e': '38029',
      't': 'Operation failed',
      'm': 'The playbill is overlap in TV guide page.',
      's': 'Please try again or contact your service provider for help.'
    },
  '38030': {
      'e': '38030',
      't': 'Operation failed',
      'm': 'No fill playbill return.',
      's': 'Please try again or contact your service provider for help.'
    },
  '38031': {
      'e': '38031',
      't': 'Operation failed',
      'm': 'The playbill or channenl is empty due to content binding is incorrect.',
      's': 'Please try again or contact your service provider for help.'
    },
  '38032': {
      'e': '38032',
      't': 'Operation failed',
      'm': 'The bookmark data is abnormal so play can\'t be started from bookmark.',
      's': 'Please try again or contact your service provider for help.'
    },
  '38033': {
      'e': '38033',
      't': 'Operation failed',
      'm': 'The VOD list is empty.',
      's': 'Please try again or contact your service provider for help.'
    },
  '38034': {
      'e': '38034',
      't': 'Operation failed',
      'm': 'The episode configuration is incomplete.',
      's': 'Please try again or contact your service provider for help.'
    },
  '38035': {
      'e': '38035',
      't': 'Operation failed',
      'm': 'Faild to report SQM.',
      's': 'Please try again or contact your service provider for help.'
    },
  '38036': {
      'e': '38036',
      't': 'Operation failed',
      'm': 'vspURL is empty(LoginRoute).',
      's': 'Please try again or contact your service provider for help.'
    },
  '38037': {
      'e': '38037',
      't': 'Operation failed',
      'm': 'vspHttpsUR is empty(LoginRoute).',
      's': 'Please try again or contact your service provider for help.'
    },
  '38038': {
      'e': '38038',
      't': 'Operation failed',
      'm': 'vspURL is equal to disableVSPAddr(LoginRoute).',
      's': 'Please try again or contact your service provider for help.'
    },
  '38039': {
      'e': '38039',
      't': 'Operation failed',
      'm': 'upgradeDomain and upgradeDomainBackups is empty(Login)',
      's': 'Please try again or contact your service provider for help.'
    },
  '38040': {
      'e': '38040',
      't': 'Operation failed',
      'm': 'NTPDomain and NTPDomainBackups is empty(Login).',
      's': 'Please try again or contact your service provider for help.'
    },
  '38041': {
      'e': '38041',
      't': 'Operation failed',
      'm': 'TerminalParm is incomplete(Login)',
      's': 'Please try again or contact your service provider for help.'
    },
  '38042': {
      'e': '38042',
      't': 'Operation failed',
      'm': 'Parameters SQMURL is empty(Login)',
      's': 'Please try again or contact your service provider for help.'
    },
  '38043': {
      'e': '38043',
      't': 'Operation failed',
      'm': 'MQMCURL is empty(Login).',
      's': 'Please try again or contact your service provider for help.'
    },
  '38044': {
      'e': '38044',
      't': 'Operation failed',
      'm': 'userToken is empty(Authenticate)',
      's': 'Please try again or contact your service provider for help.'
    },
  '38045': {
      'e': '38045',
      't': 'Operation failed',
      'm': 'jSessionID is empty.',
      's': 'Please try again or contact your service provider for help.'
    },
  '38046': {
      'e': '38046',
      't': 'Operation failed',
      'm': 'VUID is empty.',
      's': 'Please try again or contact your service provider for help.'
    },
  '38047': {
      'e': '38047',
      't': 'Operation failed',
      'm': 'profilID is empty or profileID is not in profiles.',
      's': 'Please try again or contact your service provider for help.'
    },
  '38048': {
      'e': '38048',
      't': 'Operation failed',
      'm': 'deviceID is empty.',
      's': 'Please try again or contact your service provider for help.'
    },
  '38049': {
      'e': '38049',
      't': 'Operation failed',
      'm': 'Other parameter from VSP is empty(Authenticate).',
      's': 'Please try again or contact your service provider for help.'
    },
  '38050': {
      'e': '38050',
      't': 'Operation failed',
      'm': 'CA.verimatrix is empty.',
      's': 'Please try again or contact your service provider for help.'
    },
  '38051': {
      'e': '38051',
      't': 'Operation failed',
      'm': 'CA.playReady is empty.',
      's': 'Please try again or contact your service provider for help.'
    },
  '38053': {
      'e': '38053',
      't': 'Operation failed',
      'm': 'Profiles is empty.(QueryProfile)',
      's': 'Please try again or contact your service provider for help.'
    },
  '38054': {
      'e': '38054',
      't': 'Operation failed',
      'm': 'personalDataVersions.channel  is empty.',
      's': 'Please try again or contact your service provider for help.'
    },
  '38055': {
      'e': '38055',
      't': 'Operation failed',
      'm': 'LauncherLink of QueryLauncher is empty.',
      's': 'Please try again or contact your service provider for help.'
    },
  '38056': {
      'e': '38056',
      't': 'Operation failed',
      'm': 'Failed to parse the dynamic layout JSON file.',
      's': 'Please try again or contact your service provider for help.'
    },
  '38060': {
      'e': '38060',
      't': 'Operation failed',
      'm': 'masterSTBID is empty (GetMasterSTBID).',
      's': 'Please try again or contact your service provider for help.'
    },
  '38061': {
      'e': '38061',
      't': 'Operation failed',
      'm': 'channelDynamaicProp is empty(QueryAllChannelDynamicProperties)',
      's': 'Please try again or contact your service provider for help.'
    },
  '38062': {
      'e': '38062',
      't': 'Operation failed',
      'm': 'special_cluster_id is invalid.',
      's': 'Please try again or contact your service provider for help.'
    },
  '38063': {
      'e': '38063',
      't': 'Operation failed',
      'm': 'vod_subject_id is invalid.',
      's': 'Please try again or contact your service provider for help.'
    },
  '38064': {
      'e': '38064',
      't': 'Operation failed',
      'm': 'vod_4k_subject_id is invalid.',
      's': 'Please try again or contact your service provider for help.'
    },
  '38066': {
      'e': '38066',
      't': 'Operation failed',
      'm': 'livetv_4k_subject_id is invalid.',
      's': 'Please try again or contact your service provider for help.'
    },
  '38067': {
      'e': '38067',
      't': 'Operation failed',
      'm': 'currency_symbol is invalid.',
      's': 'Please try again or contact your service provider for help.'
    },
  '38068': {
      'e': '38068',
      't': 'Operation failed',
      'm': 'credit_limit_list is invalid.',
      's': 'Please try again or contact your service provider for help.'
    },
  '38069': {
      'e': '38069',
      't': 'Operation failed',
      'm': 'ott_channel_name_space is invalid.',
      's': 'Please try again or contact your service provider for help.'
    },
  '38070': {
      'e': '38070',
      't': 'Operation failed',
      'm': 'default_reccfg_single_or_series is invalid.',
      's': 'Please try again or contact your service provider for help.'
    },
  '38071': {
      'e': '38071',
      't': 'Operation failed',
      'm': 'default_reccfg_definition is invalid.',
      's': 'Please try again or contact your service provider for help.'
    },
  '38072': {
      'e': '38072',
      't': 'Operation failed',
      'm': 'default_reccfg_pvr_type is invalid.',
      's': 'Please try again or contact your service provider for help.'
    },
  '38073': {
      'e': '38073',
      't': 'Operation failed',
      'm': 'forgot_password_url is invalid.',
      's': 'Please try again or contact your service provider for help.'
    },
  '38074': {
      'e': '38074',
      't': 'Operation failed',
      'm': 'sqm_url is invalid.',
      's': 'Please try again or contact your service provider for help.'
    },
  '38075': {
      'e': '38075',
      't': 'Operation failed',
      'm': 'sqm_username is invalid.',
      's': 'Please try again or contact your service provider for help.'
    },
  '38076': {
      'e': '38076',
      't': 'Operation failed',
      'm': 'sqm_key is invalid.',
      's': 'Please try again or contact your service provider for help.'
    },
  'default': {
      'e': '10099',
      't': 'Operation failed',
      'm': 'The service is temporarily unavailable.',
      's': 'Please try again or contact your service provider for help.'
    }
}
