import * as _ from 'underscore'
import { environment } from '../../../src/environments/environment';

export const STORE: { [key: string]: any } = {}

const parseFacadeUrlFromVspUrl = (url: string) => {
  return url.replace(/(http:\/\/.*:)(\d+)/, '$133208').replace(/(https:\/\/.*:)(\d+)/, '$133209')
}

/**
 * Configuration class
 *
 * @export
 * @class Config
 */
export const Config = {

    /**
     *
     */
  get: (key: string, fallbackValue: any = undefined): any => {
      const value = STORE[key]

      if (_.isUndefined(value) || _.isNull(value)) {
          return fallbackValue
        }

      return value
    },

    /**
     *
     */
  set: (key: string, value: any) => {
      STORE[key] = value
      return this
    },

  getBoolean: (key: string, fallbackValue: any = null): boolean => {
      let val = this.get(key)
      if (val === null) {
          return fallbackValue
        }
      if (typeof val === 'string') {
          return val === 'true'
        }
      return !!val
    },

  getNumber: (key: string, fallbackValue: number = NaN): number => {
      let val = parseFloat(this.get(key))
      return isNaN(val) ? fallbackValue : val
    },

    /**
     * vsp http address
     */
  vspUrl: environment.TV_PROXY_HTTP,

    /**
     * vsp https address
     */
  vspHttpsUrl: environment.TV_PROXY_HTTPS,

    /**
     * User login status flag.
    * Return after successful authentication.
    * Ranges：
    * - 0：Normal login
    * - 1：Log on
    * If the system is configured to support the release, if the database or VSP fails,
    * the user gateway (VSP) server will allow the user to pass the authentication, then the user's login status is the log on.
     *
     * @memberOf ConfigClass
     */
  loginOccasion: false,

    /**
     *
     * Node Server http address
     */
  get facadeUrl (): string {
      return parseFacadeUrlFromVspUrl(this.vspUrl)
    },

    /**
    *
    * Node Server https address
    */
  get facadeHttpsUrl (): string {
      return parseFacadeUrlFromVspUrl(this.vspHttpsUrl)
    },

    /**
     *
     * Encryption
     *
     *
     */
  encryptionType: '0002',

    /**
     * temporary Token
     * When the client initiates a request for authentication, it needs to generate the challenge word and generate the challenge word.
     */
  encryptToken: '',

    /**
     *
     * Equipment MAC address
     */
  physicalDeviceID: '00:00:00:00:00:00',

    /**
     * Equipment type
     */
  deviceModel: 'Q22',

    /**
     * Equipment ID
     *
     */
  deviceID: '',

  authenticatorMode: 2,

  signatureKeyAlgorithm: '',

    /**
     * Home Page Expiry Time, Unit (ms)
     */
  subjectExpireCircle: 120 * 60 * 1000,

    /**
     * Home VOD Expiry Time, Unit (ms)
     */
  vodExpireCircle: 15 * 60 * 1000,

    /**
     * Whether to support the SQM probe
     * @boolean {true}
     */
  isSupportSQMProbe: true,

    /**
     * Page jump shortcuts
     * BTV_KEY: 1108/81
     * TVOD_KEY: 1184/1110/84
     * MENU_KEY: 272/72
     * GREEN_KEY: 276/83
     * VOD_KEY: 1109/222
     */
  quickKeys: [1108, 81, 1184, 1110, 84, 272, 72, 276, 83, 1109, 222],

    /**
     * After disaster recovery, the following interfaces can not be accessed
     *
     * @type {Array<string>}
     */
  loginOccasionInterfaces: ['AddProfile', 'ModifyProfile', 'DeleteProfile', 'QueryProfile', 'QueryDeviceList', 'ReplaceDevice', 'ModifyDeviceInfo',
      'CheckPassword', 'ModifyPassword', 'ResetPassword', 'SubscribeProduct', 'CancelSubscribe', 'QueryProductsByContent', 'QueryMyContent',
      'QuerySubscription', 'AddFavoCatalog', 'DeleteFavoCatalog', 'UpdateFavoCatalog', 'QueryFavoCatalog', 'CreateFavorite', 'DeleteFavorite',
      'QueryFavorite', 'SortFavorite', 'CreateLock', 'DeleteLock', 'QueryLock', 'DeleteBookmark', 'QueryBookmark', 'CreateContentScore'],
  userFilter: '',

    /**
     * shouldClearCache, reserveChannelCount and reserveCacheDays are used for playbills' cache, which are used in tvguide
     * if you want to used EPG template in high performance STB, advise you set shouldClearCache false, then reserveChannelCount
     * and reserveCacheDays are unused,  if in lower performance STB, advise you set shouldClearCache true, and reserveChannelCount
     * is used to set reserve how many channelList's cache,  is better 5, 10, ...  which can be divisible 5, and reserveCacheDays
     * is used to set reserve how many playbillList's cache of each channel, it can be set 1, 2 and so on
     *
     */
  shouldClearCache: false,

  reserveChannelCount: 5,

  reserveCacheDays: 2,

  openDebugModel: false,

    /**
     * Default value of URLFormat parameter for play requests:
     * 0 - default format (DASH), 1 - HLS.
     *
     * @type {boolean}
     */
    // playUrlFormat: 0,

    /**
     * Default value of isHTTPS parameter for play requests:
     * 0 - get HTTP URL, 1 - get HTTPS URL.
     *
     * @type {boolean}
     */
  playHttps: 0
}
