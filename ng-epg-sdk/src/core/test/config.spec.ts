import { Config, STORE } from '../config'
describe('config test', () => {

  it('get default value', () => {

      delete STORE['name']
      expect(Config.get('name')).toBeUndefined()
      expect(Config.get('name', 'hwc')).toEqual('hwc')

      Config.set('isValid', false)
      expect(Config.get('isValid', true)).toBeFalsy()
    })

})
