import { IDB } from './../dao/idb'

interface StorgeItem {
  key: string
  data: any
}

class IDBStorge {

  private storeIDB: IDB<StorgeItem>

  constructor () {
      this.storeIDB = new IDB({
          keyPath: 'key',
          storeName: 'storge'
        })
    }

  public get length (): Promise<number> {
      return this.storeIDB.count()
    }

  public clear (): Promise<void> {
      return this.storeIDB.clear()
    }

  public getItem<T> (key: string): Promise<T> {
      return this.storeIDB.get(key).then((item: StorgeItem) => {
          return item ? item.data : undefined
        })
    }

  public getAll (): Promise<any> {
      return this.storeIDB.getAll().then((itemList: Array<StorgeItem>) => {
          itemList = itemList || []
          return itemList.map((item: StorgeItem) => {
              return item.data
            })
        })
    }

  public removeItem (key: string): Promise<void> {
      return this.storeIDB.delete(key)
    }

  public setItem (key: string, data: any): Promise<void> {
      const item: StorgeItem = { key, data }
      return this.storeIDB.put(item)
    }
}

export const idbStorge = new IDBStorge()
