import { win } from './win'
/**
 * @class Token
 *
 * @version 1.0.1
 */
export class Token {
  static tokenCount = 0
  private uniqId: number
  private keyword: string

  static createId () {
      return ++Token.tokenCount
    }

  constructor () {
      this.uniqId = Token.createId()
      this.keyword = ''
    }

  setKeyword (keyword: string) {
      this.keyword = keyword
    }

  getKeyword () {
      return this.keyword
    }

  toString () {
      return this.uniqId
    }
}

/**
 * @class LogMessage
 *
 * @version 1.0.1
 */
class LogMessage {
  private type: string
  private level: string
  private msg: string
  private scenario: string
  private category: string
  private versionNumber: string
  private token: Token
  private fileName: string

  constructor (extensions: any, msg: string, ...args: any[]) {
      this.type = extensions.type
      this.level = extensions.level
      this.msg = msg
      this.scenario = extensions.scenario
      this.category = extensions.category
      this.versionNumber = extensions.versionNumber
      this.token = extensions.token
      this.fileName = extensions.fileName
    }

  private buildTimeString () {
      let now = new Date()
      let dateString = now.getFullYear() + '-' + this.prependZero(now.getMonth() + 1, 2) + '-' + this.prependZero(now.getDate(), 2)
      let timeString = this.prependZero(now.getHours(), 2) + ':' + this.prependZero(now.getMinutes(), 2)
            + ':' + this.prependZero(now.getSeconds(), 2) + '.' + this.prependZero(now.getMilliseconds(), 3)

      let timezone = this.prependZero(Math.abs(now.getTimezoneOffset() / 60), 2) + this.prependZero(now.getTimezoneOffset() % 60, 2)
      let isEastZone = now.getTimezoneOffset() < 0
      timezone = isEastZone ? '+' + timezone : '-' + timezone

      return dateString + 'T' + timeString + timezone
    }

  private prependZero (num: number, bits: number) {
      return this.fullfillChar(num, bits, '0', true)
    }

  private appendEmptyChar (level: string, bits: number) {
      return this.fullfillChar(level, bits, ' ')
    }

  private fullfillChar (origin: number | string, bits: number, char: string, isPrepended?: boolean) {
      let originStr = origin + ''
      let count = bits - originStr.length

      while (count > 0) {
          if (isPrepended) {
              originStr = char + originStr
            } else {
              originStr += char
            }

          count--
        }

      return originStr
    }

  private buildLogHead () {
      return '[' + this.appendEmptyChar(this.level, 5) + ' | ' + this.buildTimeString() + ']'
    }

  private buildLogBody () {
      return '[' + this.msg + ']'
    }

  private buildLogTail () {
      return '[' + this.scenario + ' | ' + this.category + ' | ' + this.token + ' | ' + this.fileName + ' | ' + this.type + ']'
    }

  public toString () {
      return this.buildLogHead() + '-' + this.buildLogBody() + '-' + this.buildLogTail()
    }
}

export const VALVE_MAP: any = {
  'debug': 10,
  'info': 8,
  'warn': 6,
  'kpi': 4,
  'error': 2,
  'none': 0
}

const MESSAGE_TYPE: any = {
  SCENARIO: 'Scenario',
  METHOD: 'Method',
  FETCH: 'Fetch',
  KEY: 'Key',
  BUSINESS: 'Business',
  TIPTOAST: 'TipToast'
}

declare const console: any

/**
 * @class Logger
 *
 * A logger class follow certain rules which can be recognized by issue predator
 *
 * @version 1.0.1
 */
export class Logger {
  static version = 'V0.0.1'
  static valve = 2
  static security = 'strict'
  static maxLength = 1024
  static sensitiveKeyMap: any = {}
  static physicalPrinter: any = null

  protected fileName: string
  protected scenario: string
  protected category: string
  protected type: string

    /**
        * @static
        *
        * Set version number to distinguish different projects
        *
        * @param {string} versionNumber
        */
  static setVersion (versionNumber: string) {
      if (versionNumber) {
          Logger.version = versionNumber
        }
    }

    /**
     * @static
     *
     * Set level valve, all logs larger than this valve won't be printed
     *
     * @param {string} level level options(case-insensitive): debug > info > warn > kpi > error > none
     */
  static setLevel (level: string) {
      let v = VALVE_MAP[level.toLowerCase()]
      if (!v && v !== 0) {
          console.error('Wrong log level set!')
          return
        }
      Logger.valve = v
    }

    /**
     * @static
     *
     * Set security print strength
     *
     * @param {string} strength Value options(case-insensitive): strict > loose > none
     */
  static setSecurity (strength: string) {
      Logger.security = strength
    }

    /**
     * @static
     *
     * Set sensitive keys
     *
     * @param {Array<string>} keys sensitive key list
     */
  static setSensitiveKeys (keys: Array<string>) {
      keys.forEach((key) => {
          Logger.sensitiveKeyMap[key.toLowerCase()] = true
        })
    }

    /**
     * @static
     *
     * Set max log length due to STB limitation
     *
     * @param {number} maxLength
     */
  static setMaxLogLength (maxLength: number) {
      Logger.maxLength = maxLength
    }

    /**
     * @static
     *
     * Set physical printer to output final logs, such as network, printer, etc.
     */
  static setPhysicalPrinter (physicalPrinter: any) {
      Logger.physicalPrinter = physicalPrinter
    }

    /**
     * Factory method: Get logger instance
     *
     * @param {Array<string>} extensions
     * - extensions[0]: fileName,
     * - extensions[1]: scenario,
     * - extensions[2]: category
     */
  static get (...extensions: Array<string>) {
      return new Logger(...extensions)
    }

    /**
     * @param {Array<string>} extensions
     * - extensions[0]: fileName,
     * - extensions[1]: scenario,
     * - extensions[2]: category
     */
  constructor (...extensions: Array<string>) {
      this.fileName = extensions[0]
      this.scenario = this.fileName
      this.category = 'default'

      let length = extensions.length

      if (length > 1) {
          this.scenario = extensions[1]
        }

      if (length > 2) {
          this.category = extensions[2]
        }

      this.type = MESSAGE_TYPE.BUSINESS
    }

    /**
     * @deprecated
     *
     * This method is used to print key event.
     */
  log (msg: string, ...args: Array<any>) {
      console['info'](msg, ...args)
    }

    /**
     * Check if current level equal to or higher than log level
     *
     * @return {Boolean}
     */
  isLog () {
      return this.isDebug()
    }

    /**
     * Print debug level log
     *
     * @param {String} msg Message to be printed
     *
     * @param {Array<any>} args Arguments to be printed
     *
     * @return {Token}
     */
  debug (msg: string, ...args: Array<any>) {
      this.type = MESSAGE_TYPE.BUSINESS

      let token = this.parseToken(args)
      this.print('debug', msg, args, token)

      return token
    }

    /**
     * Check if current level equal to or higher than debug level
     *
     * @return {Boolean}
     */
  isDebug () {
      return Logger.valve >= VALVE_MAP['debug']
    }

    /**
     * Print info level log
     *
     * @param {String} msg Message to be printed
     *
     * @param {Array<any>} args Arguments to be printed
     *
     * @return {Token}
     */
  info (msg: string, ...args: Array<any>) {
      this.type = MESSAGE_TYPE.BUSINESS

      let token = this.parseToken(args)
      this.print('info', msg, args, token)

      return token
    }

    /**
     * Check if current level equal to or higher than info level
     *
     * @return {Boolean}
     */
  isInfo () {
      return Logger.valve >= VALVE_MAP['info']
    }

    /**
     * Print warning level log
     *
     * @param {String} msg Message to be printed
     *
     * @param {Array<any>} args Arguments to be printed
     *
     * @return {Token}
     */
  warn (msg: string, ...args: Array<any>) {
      this.type = MESSAGE_TYPE.BUSINESS

      let token = this.parseToken(args)
      this.print('warn', msg, args, token)

      return token
    }

    /**
     * Check if current level equal to or higher than warn level
     *
     * @return {Boolean}
     */
  isWarn () {
      return Logger.valve >= VALVE_MAP['warn']
    }

    /**
     * Print error level log
     *
     * @param {String} msg Message to be printed
     *
     * @param {Array<any>} args Arguments to be printed
     *
     * @return {Token}
     */
  error (msg: string, ...args: Array<any>) {
      this.type = MESSAGE_TYPE.BUSINESS

      let token = this.parseToken(args)
      this.print('error', msg, args, token)

      return token
    }

    /**
     * Check if current level equal to or higher than error level
     *
     * @return {Boolean}
     */
  isError () {
      return Logger.valve >= VALVE_MAP['error']
    }

    /**
     * Print kpi level log
     *
     * @param {String} msg Message to be printed
     *
     * @param {Array<any>} args Arguments to be printed
     *
     * @return {Token}
     */
  kpi (msg: string, ...args: Array<any>) {
      this.type = MESSAGE_TYPE.BUSINESS

      let token = this.parseToken(args)
      this.print('kpi', '[KPI]' + msg, args, token)

      return token
    }

    /**
     * Check if current level equal to or higher than kpi level
     *
     * @return {Boolean}
     */
  isKpi () {
      return Logger.valve >= VALVE_MAP['kpi']
    }

    /**
     * @deprecated
     *
     * @param {string} timeName time duration id
     */
  time (timeName: string) {
        // Deprecated: do nothing here
    }

    /**
     * @deprecated
     *
     * @param {string} timeName time duration id
     */
  timeEnd (timeName: string) {
        // Deprecated: do nothing here
    }

    /**
     * @deprecated
     *
     * @param {string} timeName time duration id
     */
  timeStamp (name: string) {
        // Deprecated: do nothing here
    }

    /**
     * Print method begin information, records input parameters
     *
     * @param {string} methodName Method name
     *
     * @param {Array<any>} args Arguments to be printed
     * - args[0]: level?
     * - args[...]: data list
     * - args[last]: token?
     *
     * @return {Token}
     */
  methodBegin (methodName: string, ...args: Array<any>) {
      return this.methodPrint('begin function', methodName, args)
    }

    /**
     * Print method end information, records output data
     *
     * @param {string} methodName Method name
     *
     * @param {Array<any>} outs Data to be printed
     * - outs[0]: level?
     * - outs[...]: data list
     * - outs[last]: token?
     *
     * @return {Token}
     */
  methodEnd (methodName: string, ...outs: Array<any>) {
      return this.methodPrint('end function', methodName, outs)
    }

    /**
     * Record scenario begin data to compare expected data format
     *
     * @param {Array<any>} data Data list to be recored
     * - data[0]: level?
     * - data[...]: data list
     * - data[last]: token?
     *
     * @return {Token}
     */
  scenarioBegin (...data: Array<any>) {
        // format: []--[#begin scenario#, data:>>{}<<, data:>>10<<]--[]
      return this.scenarioPrint('begin scenario', data)
    }

    /**
     * Record scenario end data to compare expected data format
     *
     * @param {Array<any>} data Data list to be recored
     * - data[0]: level?
     * - data[...]: data list
     * - data[last]: token?
     *
     * @return {Token}
     */
  scenarioEnd (...data: Array<any>) {
        // format: []--[#end scenario#, data:>>{}<<, data:>>10<<]--[]
      return this.scenarioPrint('end scenario', data)
    }

    /**
     * Log network request and parameters
     *
     * @param {string} api request api name
     *
     * @param {Array<any>} data request parameters
     *
     * @return {Token}
     */
  fetchBegin (api: string, ...data: Array<any>) {
      return this.fetchPrint('#send request#', api, data)
    }

    /**
     * Log network response and returned data
     *
     * @param {string} api response api name
     *
     * @param {Array<any>} data response data
     *
     * @return {Token}
     */
  fetchEnd (api: string, ...data: Array<any>) {
      this.transformNativeObjects(data)
      return this.fetchPrint('#receive response#', api, data)
    }

    /**
     *
     * Log user behaviors
     *
     * @param {string} action
     * - click
     * - dbClick
     * - drag
     * - slide
     * - longPress
     * - remotePress
     *
     * @param {string} msg message to be printed
     *
     * @param {Array<any>} data data list to be printed
     *
     * @return {Token}
     */
  behave (action: string, msg: string, ...data: Array<any>) {
        // TODO: Phase-2：This method is not used in EPG project. EPG only care about key press.
      return this.info('#' + action + '# ' + msg, ...data)
    }

    /**
     * Log key press message
     *
     * @param {KeyboardEvent} event key press event
     *
     * @param {Array<any>} data data list to be printed
     * - data[0]: level?
     * - data[...]: data list
     * - data[last]: token?
     *
     * @return {Token}
     */
  keyPress (event: any, ...data: Array<any>) {
      return this.keyPrint('pressed', event, data)
    }

    /**
     * Log key release message
     *
     * @param {KeyboardEvent} event key release event
     *
     * @param {Array<any>} data data list to be printed
     * - data[0]: level?
     * - data[...]: data list
     * - data[last]: token?
     *
     * @return {Token}
     */
  keyRelease (event: any, ...data: Array<any>) {
      return this.keyPrint('released', event, data)
    }

    /**
     * Attention: This method may lead memory leak, it should be used to snapshot global object
     * Capture global object and snapshot memory every certain time, useless to non-refrence object
     * It belongs to Intelligent Analytics scope.
     *
     * @param {number} duration
     * @param {Array<any>} data
     */
  snapshot (duration: number, ...data: Array<any>) {
        // TODO: Implement in phase-2
    }

    /**
     * @deprecated
     *
     * This method is no longer used and will be replaced by memory debug tool
     *
     * @return {Array<string>}
     */
  returnDebugLogers () {
      return ['This function is deprecated, please wait for next memorize debug tool...']
    }

    /**
     * Log tip or toast message
     *
     * @param {string} key format is ApiName.retCode, like Authenticate.102001
     *
     * @param {any} errorInfo error code info in word document
     *
     * @param {string} innerDescription inner message in log.json
     *
     * @param {boolean} isDefault indicate if this is default error code
     *
     * @return {Token}
     */
  tipToast (key: string, errorInfo: any, innerDescroption: string, isDefault?: boolean) {
      this.type = MESSAGE_TYPE.TIPTOAST

      let prefix = ''
      if (errorInfo.action === 'tips') {
          prefix = '[Dialog]'
        } else if (errorInfo.action === 'toast') {
          prefix = '[Toast]'
        }

      let message = prefix + '[ERROR_CODE] key:>>' + key + '<<, innerDescription:>>' + innerDescroption + '<<, #default#'
      if (!isDefault) {
          message = message.replace('#default#', '')
        }

      let token = new Token()

        // dialog and toast belong to errors, should use error level to print.
      this.dataPrint(message, [errorInfo], token, 'error')

      return token
    }

    /**
     * @private
     *
     * print key press and key release information
     *
     * @param {string} diff indicate key press or key release
     *
     * @param {Event} event key press or key release event
     *
     * @param {Array<any>} data data list to be printed
     *
     * @return {Token}
     */
  private keyPrint (diff: string, event: any, data: Array<any>) {
      this.type = MESSAGE_TYPE.KEY

      let keyName = event.key || event.char
      let keyCode = event.keyCode

      let prefix = '[BEHAVIOR]'
      let stbKeyCode = 768
      if (parseInt(keyCode, 10) === stbKeyCode) {
          prefix = '[STB_EVENT]'
        }

      let message = prefix + 'keyName:>>' + keyName + '<<, keyCode:>>' + keyCode + '<< is #' + diff + '#.'
        // Remove token from data and return it
      let token = this.parseToken(data)
        // Remove level from data and return it
      let level = this.parseLevel(data)

      this.dataPrint(message, data, token, level)

      return token
    }

    /**
     * @private
     *
     * print scenario type log
     *
     * @param {string} prefix prefix of scenario type log message
     *
     * @param {Array<any>} data data list to be printed
     *
     * @param {Token} token log token
     *
     * @return {void}
     */
  private scenarioPrint (prefix: string, data: Array<any>) {
      this.type = MESSAGE_TYPE.SCENARIO

      let message = '#' + prefix + '#, scenario:>>' + this.scenario + '<<'
        // Remove token from data and return it
      let token = this.parseToken(data)
        // Remove level from data and return it
      let level = this.parseLevel(data)

      this.dataPrint(message, data, token, level)

      return token
    }

    /**
     * @private
     *
     * print method type log
     *
     * @param {string} prefix prefix of method type log message
     *
     * @param {string} methodName method name to be printed
     *
     * @param {Array<any>} args data list to be printed
     *
     * @return {void}
     */
  private methodPrint (prefix: string, methodName: string, args: Array<any>) {
      this.type = MESSAGE_TYPE.METHOD

      let message = prefix + ':>>' + methodName + '<<'
        // Remove token from args and return it
      let token = this.parseToken(args)
        // Remove level from args and return it
      let level = this.parseLevel(args)

      this.dataPrint(message, args, token, level)

      return token
    }

    /**
     * @private
     *
     * print fetch type log
     *
     * @param {string} prefix
     *
     * @param {string} api
     *
     * @param {Array<any>} data data list to be printed
     */
  private fetchPrint (prefix: string, api: string, data: Array<any>) {
      this.type = MESSAGE_TYPE.FETCH

      let message = '[VSP_HTTP]' + prefix + ', api:>>' + api + '<<'
        // Remove response from args and return it
      let response = this.parseResponse(data)
      if (response) {
          let responseString = this.stringifyArgs([response])[0]
          message += ', ' + responseString.replace(/data:>>/g, 'response:>>')
        }

        // Remove token from args and return it
      let token = this.parseToken(data)
        // Remove level from args and return it
      let level = this.parseLevel(data)

      this.dataPrint(message, data, token, level)

      return token
    }

    /**
     * @private
     *
     * print data list
     *
     * @param {string} msg message to be printed
     *
     * @param {Array<any>} data data list to be printed
     *
     * @param {Token} token log token
     *
     * @return {void}
     */
  private dataPrint (msg: string, data: Array<any>, token: Token, level?: string) {
      data.forEach(() => {
          msg += ', %s'
        })

      if (!level) {
          level = 'info'
        }

      this.print(level, msg, data, token)
    }

    /**
     * @private
     *
     * Parse token info from argument list
     *
     * @param {Array<any>} args argument list which may contain log token
     *
     * @return {Token}
     */
  private parseToken (args: Array<any>) {
      let token

      if (args.length > 0) {
          let lastArg = args[args.length - 1]

          if (lastArg && lastArg.constructor === Token) {
              token = args.splice(args.length - 1, 1)[0]
            }
        }

      if (!token) {
          token = new Token()
        }

      return token
    }

    /**
     * @private
     *
     * Parse level info from argument list
     *
     * @param args
     */
  private parseLevel (args: Array<any>) {
      if (args.length > 0 && this.isLevel(args[0])) {
          return args.splice(0, 1)[0]
        }

      return null
    }

    /**
     * @private
     *
     * Parse response from argument list
     *
     * @param args
     */
  private parseResponse (args: Array<any>) {
      let response = null

      for (let i = 0, l = args.length, arg: any = null; i < l; i++) {
          arg = args[i]
          if (arg && typeof arg.status === 'number' && typeof arg.statusText === 'string') {
              response = arg
              args.splice(i, 1)
              break
            }
        }

      if (!response) {
          return null
        }

      let copiedResponse: any = {}
      Object.assign(copiedResponse, response)

      delete copiedResponse.custom
      delete copiedResponse.response
      delete copiedResponse.responseText

      return copiedResponse
    }

    /**
     * Iterative data list and transform js native objects to printable objects
     *
     * @param {Array<any>} dataList data list to be printed.
     */
  private transformNativeObjects (dataList: Array<any>) {
      let l = dataList.length
      let data: any = null

      for (let i = 0; i < l; i++) {
          data = dataList[i]

          if (data && typeof Response !== 'undefined' && data.constructor === Response) {
              dataList[i] = this.transformNativeResponse(data)
            }
        }
    }

    /**
     * Transform native Response object to normal json object, so it can be printed
     *
     * @param {Response} response native Response object
     */
  private transformNativeResponse (response: Response) {
      return {
          url: response.url,
          ok: response.ok,
          status: response.status,
          statusText: response.statusText,
          type: response.type,
          bodyUsed: response.bodyUsed
        }
    }

    /**
     * @private
     *
     * Check if parameter is level info
     *
     * @param {any} level
     *
     * @return {boolean}
     */
  private isLevel (level: any) {
      if (typeof level !== 'string') {
          return false
        }

      return typeof VALVE_MAP[level] === 'number'
    }

    /**
     * @protected
     *
     * Raw method to print log information
     *
     * @param {string} level log message level
     *
     * @param {string} msg message passed by caller
     *
     * @param {Array<any>} args data list to be printed
     *
     * @param {Token} token log token
     *
     * @return {void}
     */
  protected print (level: string, msg: string, args: Array<any>, token: Token | null) {
      if (VALVE_MAP[level] > Logger.valve) {
          return
        }

      let stringArgs = this.stringifyArgs(args)
      msg = this.appendExtraReplacement(this.howManyIn(msg), stringArgs.length, msg)

      let formattedMsg = this.buildFormat(msg, level, token)

      let printMethod = console[level]
      if (!printMethod) {
          printMethod = console['info']
        }

      let stringList = this.sliceToLimitedStrings(formattedMsg, ...stringArgs)
      stringList.forEach((stringFragment: string) => {
          if (Logger.physicalPrinter) {
              Logger.physicalPrinter.print(stringFragment)
            } else {
              printMethod.call(console, stringFragment)
            }
        })
    }

    /**
     * Append extra %s sign if data list is larger than sign count
     *
     * @param {number} signCount %s sign count
     *
     * @param {number} dataCount data count
     *
     * @param {string} msg log message
     *
     * @return {string}
     */
  private appendExtraReplacement (signCount: number, dataCount: number, msg: string) {
      if (signCount < dataCount) {
          let delta = dataCount - signCount
          do {
              msg += ', %s'
              delta--
            } while (delta > 0)
        }

      return msg
    }

    /**
     * Find how many of %s in search string
     *
     * @param searchString
     *
     * @return {number}
     */
  private howManyIn (searchString: string) {
      let regExp = /%s/g
      let result: any = null
      let count = 0

      do {
          result = regExp.exec(searchString)
          if (result) {
              count++
            }
        } while (result)

      return count
    }

    /**
     * Slice long log string to server small fragments
     *
     * @param {string} msg main message
     *
     * @param {Array<string>} replacements data string list to be replaced
     *
     * @return {Array<string>}
     */
  private sliceToLimitedStrings (msg: string, ...replacements: Array<string>) {
      replacements.forEach((replacement) => {
          const kpiValue = 4
          if (Logger.valve === kpiValue && replacement.length > Logger.maxLength) {
              msg = msg.replace(/%s/, 'Omit giant data, length: ' + replacement.length)
            } else {
              msg = msg.replace(/%s/, replacement)
            }
        })

      let startIndex = 0
      let maxLength = Logger.maxLength
      let list: string[] = []

      while (startIndex < msg.length) {
          list.push(msg.substring(startIndex, startIndex + maxLength))
          startIndex += maxLength
        }

      return list
    }

    /**
     * @private
     *
     * Use LogMessage class to build basic log format
     *
     * @param {string} msg message passed by caller
     *
     * @param {string} level log message level
     *
     * @param {Token} token log token
     *
     * @return {string}
     */
  private buildFormat (msg: string, level: string, token: Token | null) {
      let extensions = {
          level: level,
          scenario: this.scenario,
          category: this.category,
          type: this.type,
          fileName: this.fileName,
          versionNumber: Logger.version,
          token: token
        }

      let logMsg = new LogMessage(extensions, msg)
      return logMsg.toString()
    }

    /**
     * @private
     *
     * Stringify data object list
     *
     * @param {Array<any>} args data list to be stringified
     *
     * @return {string}
     */
  private stringifyArgs (args: Array<any>) {
      return args.map((arg) => {
          if (arg === null || arg === undefined) {
              return '#undefined#'
            }

          try {
              let objectList: Array<any> = []

              let dataString = JSON.stringify(arg, (key, value) => {
                  if (typeof value === 'object') {
                      if (this.contains(objectList, value)) {
                          return '#CYCLE#'
                        }

                      objectList.push(value)
                    }

                  if (typeof value === 'function') {
                      return 'function ' + key + '(){}'
                    }

                  if (this.isSensitive(key)) {
                      return this.decorateSensitiveInfo(key, value)
                    }

                  return value
                })

              return 'data:>>' + dataString + '<<'
            } catch (e) {
              return '[STRINGIFY ERROR]' + e
            }
        })
    }

    /**
     * @private
     *
     * Check if key is a sensitive info
     *
     * @param {string} key information to be checked
     *
     * @return {boolean}
     */
  isSensitive (key: string) {
      return Logger.sensitiveKeyMap[key.toLowerCase()]
    }

    /**
     * @private
     *
     * decorate sensitive information to make it invisible
     *
     * @param {string} key sensitive key info
     *
     * @param {string} value sensitive value info
     *
     * @return {string}
     */
  private decorateSensitiveInfo (key: string, value: string) {
        // TODO: will be implemented in phase-2

      if (Logger.security === 'strict') {
          return '********'
        }

      return ''
    }

    /**
     * @private
     *
     * Check if parameter item exist in list
     *
     * @param {Array<any>} list list which may contain item
     *
     * @param {any} item item to be found
     *
     * @return {boolean}
     */
  contains (list: Array<any>, item: any) {
      let i = 0
      let l = list.length
      for (; i < l; i++) {
          if (list[i] === item) {
              return true
            }
        }

      return false
    }
}

/**
 * @annotation
 *
 * @version 1.0.1
 *
 * MethodLog annotation, used to print method begin and method end log
 */
export function MethodLog (logger: Logger, level?: string) {
  return (target: any, methodName: string, descriptor: PropertyDescriptor) => {
        // origin method
      let method = descriptor.value

        // decorate origin method
      descriptor.value = function (...args: any[]) {
          if (level) {
              args.unshift(level)
            }
            // print method begin log with parameters
          let token = logger.methodBegin(methodName, ...args)

          let dataHandler = function (data: any) {
                // print method end log with outputs
              let outputs: any[] = [token]
              if (data !== null && data !== undefined) {
                  outputs.unshift(data)
                }

              if (level) {
                  outputs.unshift(level)
                }

              logger.methodEnd(methodName, ...outputs)
              return data
            }

          let result = method.apply(this, arguments)

            // For async method, handle after data returned
          if (result && result.constructor === Promise) {
              result.then(dataHandler).catch(dataHandler)
            } else {
              dataHandler(result)
            }

          return result
        }
    }
}

/**
 * start up
 *
 * @version 1.0.1
 */
(() => {
  if (win) {
      let logger = new Logger('Logger.ts', 'Global', 'Business')

      win.onerror = (message: string, file: string, line: string, column: string, error: any) => {
          logger.error('file: %s, line: %s, column: %s, message: %s, stack: %s', file, line, column, message, error && error.stack)
        }

      win['onunhandledrejection'] = (event: any) => {
          let reason = event.reason || {
            }
          let printableInfo = {
              label: reason.name,
              code: reason.code,
              message: reason.message
            }

          logger.error('[unhandledrejection] event.reason: %s', printableInfo)
        }
    }
})()
