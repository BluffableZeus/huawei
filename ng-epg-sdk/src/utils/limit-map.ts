import * as _ from 'underscore'

class Item<T> {
  private timestamp = Date.now()

  constructor (public data: T, private timeout: number) {
    }

  isExpire () {
      return (Date.now() - this.timestamp) > this.timeout
    }
}

export class LimitMap<V> {

  private size: number = 0

  private index: { [key: string]: Item<V> } = {}

  private timeoutId

  constructor (private limit = 100, private timeout = 15 * 600 * 1000) {
    }

    /**
     * Add
     */
  set (key: string, value: V | Item<V>) {
      if (!this.index[key]) {
          this.size++
        }

      if (value instanceof Item) {
          this.index[key] = value
        } else {
          this.index[key] = new Item(value, this.timeout)
        }

      this.ensureLimit()
      this.clearExpireData()
    }

    /**
     * get
     */
  get (key: string): V | undefined {
      const item = this.index[key]
      if (item) {
          if (item.isExpire()) {
              this.remove(key)
            } else {
              return item.data
            }
        }
    }

    /**
     * delete
     */
  remove (key: string) {
      if (this.index[key]) {
          delete this.index[key]
          this.size--
        }

      if (0 === this.size) {
          this.stopTimeout()
        }
    }

    /**
     * Clear the cached data
     */
  clear () {
      this.stopTimeout()
      this.size = 0
      this.index = {}
    }

    /**
     * The number of data
     */
  getSize () {
      return this.size
    }

    /**
     * Make sure the total does not exceed the limit
     */
  ensureLimit () {
      if (this.size > this.limit) {
          const itemList: Array<[string, Item<V>]> = []
          _.each(this.index, (item: Item<V>, key: string) => {
              itemList.push([key, item])
            })

          const limitList = itemList.slice(-this.limit * 0.8)

          this.clear()
          limitList.forEach(([key, item]) => {
              this.set(key, item)
            })
        }
    }

  getKeys () {
      return Object.keys(this.index)
    }

    /**
     * Keep the data that satisfies the condition
     */
  private clearExpireData () {
      if (this.timeoutId || this.size === 0) {
          return
        }

      this.timeoutId = setTimeout(() => {
          this.stopTimeout()

          _.each(this.index, (item: Item<V>, key: string) => {
              if (item.isExpire()) {
                  this.remove(key)
                }
            })

          this.clearExpireData()
        }, this.timeout)
    }

  private stopTimeout () {
      clearTimeout(this.timeoutId)
      this.timeoutId = null
    }
}
