import * as _ from 'underscore'

class XmlUtils {

    /**
     * convert xml string to json
     */
  public toJson (xmlStr: string): any {
      const doc = new DOMParser().parseFromString(xmlStr, 'application/xml')
      const json: any = {}
      this.parseElement(doc.firstElementChild, json)
      return json
    }

  private unescapeXmlChars (str) {
      return str.replace(/&amp;/g, '&').replace(/&lt;/g, '<').replace(/&gt;/g, '>').replace(/&quot;/g, '"').replace(/&#x27;/g, '\'').replace(/&#x2F;/g, '\/')
    }

  private parseElement (element: Element, json) {
      if (!element) {
          return
        }

      let child
      if (element.firstElementChild) {
          child = {}
          this.parseElement(element.firstElementChild, child)
        } else {
          child = this.unescapeXmlChars(element.textContent)
          this.parseElement(element.nextElementSibling, json)
        }

      if (Reflect.has(json, element.tagName)) {
          json[element.tagName] = _.flatten([child, json[element.tagName]])
        } else {
          json[element.tagName] = child
        }
    }
}

export const xmlUtils = new XmlUtils()
