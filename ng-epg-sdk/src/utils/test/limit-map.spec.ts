import * as _ from 'underscore'
import { LimitMap } from './../limit-map'
describe('limit-map', () => {
  let map: LimitMap<any>
  beforeEach(() => {
      map = new LimitMap(10, 10)
    })

  afterEach(() => {
    })

  it('set', () => {
      map.set('key', 'value')
      map.set('key1', 'value1')
      expect(map.getSize()).toEqual(2)
      expect(map.get('key')).toEqual('value')
      expect(map.get('key1')).toEqual('value1')
    })

  it('remove', () => {
      map.set('key', 'value')
      map.set('key1', 'value1')
      expect(map.get('key1')).toEqual('value1')
      map.remove('key1')
      expect(map.get('key1')).not.toBeDefined()
    })

  it('expire', (done) => {
      map.set('key', 'value')
      expect(map.get('key')).toEqual('value')
      setTimeout(() => {
          expect(map.get('key')).toBeUndefined()
          done()
        }, 11)
    })

  it('limit', () => {

      _.times(10, (i) => {
          map.set('' + i, i)
        })

      expect(map.get('0')).toEqual(0)
      expect(map.get('9')).toEqual(9)

      expect(map.getSize()).toEqual(10)

      map.set('' + 10, 10)
      expect(map.getSize()).toEqual(8)
      expect(map.get('0')).not.toBeDefined()
      expect(map.get('1')).not.toBeDefined()
      expect(map.get('2')).not.toBeDefined()

      expect(map.get('10')).toEqual(10)
    })

  it('clear', () => {
      _.times(10, (i) => {
          map.set('' + i, i)
        })
      expect(map.get('0')).toEqual(0)
      expect(map.get('9')).toEqual(9)

      map.clear()
      expect(map.get('0')).toBeUndefined()
      expect(map.get('9')).toBeUndefined()
      expect(map.getSize()).toEqual(0)

    })
})
