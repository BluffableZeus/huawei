import { Config } from './../../core/config'
import { Logger } from '../logger'
import * as _ from 'underscore'

describe('Log', () => {

  let log: Logger
  let message: string

  beforeEach(() => {
      message = undefined
      log = Logger.get('TEST')
    })

  it('debug level, can call debug', function () {
      Logger.setLevel('debug')
      spyOn(console, 'debug')
      log.debug('hello')
      expect(console['debug']).toHaveBeenCalled()
    })

  it('info level, can not call debug', function () {
      Logger.setLevel('info')
      spyOn(console, 'debug')
      log.debug('hello')
      expect(console['debug']).not.toHaveBeenCalled()
    })

  it('info level, can call info', function () {
      Logger.setLevel('info')
      spyOn(console, 'info')
      log.info('hello')
      expect(console['info']).toHaveBeenCalled()
    })

  it('kpi level, can not call info', function () {
      Logger.setLevel('kpi')
      spyOn(console, 'info')
      log.info('hello')
      expect(console['info']).not.toHaveBeenCalled()
    })

  it('warn level, can call kpi', function () {
      Logger.setLevel('warn')
      spyOn(console, 'info')
      log.kpi('hello')
      expect(console['info']).toHaveBeenCalled()
    })

  it('warn level, can call warn', function () {
      Logger.setLevel('warn')
      spyOn(console, 'warn')
      log.warn('hello')
      expect(console['warn']).toHaveBeenCalled()
    })

  it('error level, can not call warn', function () {
      Logger.setLevel('error')
      spyOn(console, 'warn')
      log.warn('hello')
      expect(console['warn']).not.toHaveBeenCalled()
    })

  it('error level, can call error', function () {
      Logger.setLevel('error')
      spyOn(console, 'error')
      log.error('hello')
      expect(console['error']).toHaveBeenCalled()
    })

  it('log level, can call error', function () {
      Logger.setLevel('log')
      spyOn(console, 'error')
      log.error('hello')
      expect(console['error']).toHaveBeenCalled()
    })

  it('log level, can call log', function () {
      Logger.setLevel('log')
      spyOn(console, 'info')
      log.log('hello')
      expect(console['info']).toHaveBeenCalled()
    })

  it('undefined level, can not call log', function () {
      Logger.setLevel('')
      spyOn(console, 'log')
      log.log('hello')
      expect(console['log']).not.toHaveBeenCalled()
    })

  it('error level, can not log time', function () {
      Logger.setLevel('error')
      spyOn(console, 'error')
      log.timeEnd('time test')
      expect(console['error']).not.toHaveBeenCalled()
    })

})
