import { Map } from '../map'
import * as _ from 'underscore'

describe('Map test', () => {

  let map: Map<any>
  beforeEach(() => {
      map = new Map<any>()
      map.set('1', '11')
      map.set('2', '21')
      map.set('3', '31')
      map.set('4', '41')
    })

  it('init list', () => {
      map = new Map(_.times(10, (i) => { return { id: i } }))
      expect(map.get('9')).toEqual({ id: 9 })
      expect(map.size()).toEqual(10)
    })

  it('set', () => {
      expect(map.get('3')).toEqual('31')
      expect(map.size()).toEqual(4)
    })

  it('remove', () => {
      map.remove('3')
      expect(map.get('3')).not.toBeDefined()
      expect(map.get('4')).toEqual('41')
      expect(map.size()).toEqual(3)
    })

  it('keys', () => {
      expect(map.getKeys()).toEqual(['1', '2', '3', '4'])
    })

  it('values', () => {
      expect(map.getValues()).toEqual(['11', '21', '31', '41'])
    })
})
