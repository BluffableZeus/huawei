import { ContentUtils } from '../content-utils'
import { Content } from '../../vsp/api'
import * as  _ from 'underscore'

describe('ContentUtils', () => {

  it('normalizeName', () => {
      expect(ContentUtils.normalizeName('VIDEO_CHANNEL')).toEqual('channel')
      expect(ContentUtils.normalizeName('AUDIO_CHANNEL')).toEqual('channel')
      expect(ContentUtils.normalizeName('VIDEO_VOD')).toEqual('VOD')
      expect(ContentUtils.normalizeName('AUDIO_VOD')).toEqual('VOD')
      expect(ContentUtils.normalizeName('GENRE')).toEqual('genre')
      expect(ContentUtils.normalizeName('PROGRAM')).toEqual('playbill')
      expect(ContentUtils.normalizeName('SUBJECT')).toEqual('subject')
    })

  it('flatten content list', () => {
      const list = []
      list.push({
          contentType: 'VIDEO_CHANNEL',
          channel: { ID: 0 }
        })

      list.push({
          contentType: 'VIDEO_CHANNEL',
          channel: { ID: 1 }
        })

      list.push({
          contentType: 'AUDIO_CHANNEL',
          channel: { ID: 2 }
        })

      list.push({
          contentType: 'VIDEO_VOD',
          VOD: { ID: 3 }
        })

      list.push({
          contentType: 'AUDIO_VOD',
          VOD: { ID: 4 }
        })

      list.push({
          contentType: 'SUBJECT',
          subject: { ID: 5 }
        })

      const flattenList = ContentUtils.flatten(list)
      expect(flattenList).toEqual(_.times(6, (i) => {
          return {
              ID: i
            }
        }))
    })

  it('normalizeName', () => {
      expect(ContentUtils.normalizeName(null)).toBeUndefined()
    })

  it('sort content list', () => {

      let contentList: Array<Content> = _.chain(5).times(i => i).shuffle().map((i) => {
            return { channel: { ID: i } }
        }).value() as any

      contentList = ContentUtils.sortContentList(contentList, { getChannelByID: (id) => { return { channelNO: id } } } as any)

      const ids: Array<number> = _.chain(contentList).pluck('channel').pluck('ID').value()
      expect(ids).toEqual([0, 1, 2, 3, 4])

    })

})
