import { session } from '../session'

describe('session', () => {
  function setStorage () {
      sessionStorage.setItem('session', '0000')
      localStorage.setItem('session', '0000')
    }

  beforeEach(() => {
      sessionStorage.clear()
      localStorage.clear()
    })

  it('remove', () => {
      setStorage()
      session.remove('session')

      expect(sessionStorage.length).toBe(1)
      expect(localStorage.length).toBe(1)
    })

  it('put value is null', () => {
      session.put('session', null)
      expect(sessionStorage.length).toBe(0)
      expect(localStorage.length).toBe(0)
    })

  it('put persistence true', () => {
      session.put('session', '0000')
      expect(session.get('session')).toBe('0000')
      expect(sessionStorage.length).toBe(0)
    })

  it('put persistence false', () => {
      session.put('session', '0000')
      expect(localStorage.length).toBe(0)
      expect(session.get('session')).toBe('0000')
    })

  it('get from json', () => {
      session.put('session', '0000')
      expect(session.get('session')).toBe('0000')
    })

  it('pop', () => {
      setStorage()
      expect(session.pop('session')).toBeUndefined()
      expect(sessionStorage.getItem('session')).toBe('0000')
      expect(localStorage.getItem('session')).toBe('0000')
    })
})
