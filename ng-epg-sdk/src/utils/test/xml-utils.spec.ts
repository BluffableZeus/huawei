import { xmlUtils } from './../xml-utils'

const xmlStr = `<?xml version="1.0" encoding="UTF-8"?>
<GetMsgTaskResp>
    <RollMessage>
        <messageCode>3215</messageCode>
        <type>0</type>
        <mediaCode></mediaCode>
        <priority>5</priority>
        <confirmFlag>1</confirmFlag>
        <resend_time>2400</resend_time>
        <send_time>20170318072019</send_time>
        <mode>0</mode>
        <title>gundongxiaoxi123</title>
        <content>gundongxiaoxi123body</content>
        <icon1>http://10.162.147.137:8089/EPG/messg/roll1.jpg</icon1>
        <icon2>http://10.162.147.137:8089/EPG/messg/roll2.jpg</icon2>
        <bg_color>#1A10CA</bg_color>
        <font>SegoeWP-Semibold</font>
        <font_color>#E50B0B</font_color>
        <font_size>28</font_size>
        <overlay_height>45</overlay_height>
        <overlay_width>720</overlay_width>
        <overlay_X>0</overlay_X>
        <overlay_Y>0</overlay_Y>
        <roll_times>5</roll_times>
        <roll_speed>10</roll_speed>
        <roll_place>UP</roll_place>
        <roll_mode>LW</roll_mode>
    </RollMessage>
</GetMsgTaskResp> `

const jsonResult = {
  GetMsgTaskResp: {
      RollMessage: {
          messageCode: '3215',
          type: '0',
          mediaCode: '',
          priority: '5',
          confirmFlag: '1',
          resend_time: '2400',
          send_time: '20170318072019',
          mode: '0',
          title: 'gundongxiaoxi123',
          content: 'gundongxiaoxi123body',
          icon1: 'http://10.162.147.137:8089/EPG/messg/roll1.jpg',
          icon2: 'http://10.162.147.137:8089/EPG/messg/roll2.jpg',
          bg_color: '#1A10CA',
          font: 'SegoeWP-Semibold',
          font_color: '#E50B0B',
          font_size: '28',
          overlay_height: '45',
          overlay_width: '720',
          overlay_X: '0',
          overlay_Y: '0',
          roll_times: '5',
          roll_speed: '10',
          roll_place: 'UP',
          roll_mode: 'LW'
        }
    }
}

describe('XmlUtils', () => {

  it('toJson, normal', () => {
      const json = xmlUtils.toJson(xmlStr)
      expect(json).toEqual(jsonResult)
    })

  it('toJson, array', () => {
      const json = xmlUtils.toJson(`
        <root>
            <type>0</type>
            <type>1</type>
            <type>2</type>
            <type>3</type>
            <type>4</type>
        </root>
        `)
      expect(json).toEqual({ root: { type: ['0', '1', '2', '3', '4'] } })
    })

  it('toJson, unescape', () => {
      const json = xmlUtils.toJson(`
        <root>
            <type>&gt;0</type>
        </root>
        `)
      expect(json).toEqual({ root: { type: '>0' } })
    })
})
