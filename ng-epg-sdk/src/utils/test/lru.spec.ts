import { LRU } from './../lru'
describe('lru test', () => {

  it('exeed max limit', () => {
      const lru = new LRU({ max: 2 })
      lru.set('1', '1')
      lru.set('2', '2')
      lru.set('3', '3')

      expect(lru.get('1')).toBeUndefined()
      expect(lru.length).toEqual(2)
    })

  it('last recent used', () => {
      const lru = new LRU({ max: 2 })
      lru.set('1', '1')
      lru.set('2', '2')
      lru.get('1')
      lru.set('3', '3')

      expect(lru.get('1')).toEqual('1')
      expect(lru.get('2')).toBeUndefined()
      expect(lru.length).toEqual(2)
    })
})
