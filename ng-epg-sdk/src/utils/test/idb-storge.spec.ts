import { idbStorge } from './../idb-storge'

describe('idbStorge', () => {

  const key = '' + Date.now();

  [1, 'test', { name: 'hwc' }, new Date()].forEach((value) => {
      it(`setItem,value type:${typeof value}, value:${JSON.stringify(value)}`, (done) => {
          const promise = idbStorge.setItem(key, value).then(() => {
              return idbStorge.getItem(key)
            })

          promise.then((result) => {
              expect(result).toEqual(value)
              done()
            }, fail)
        })
    })

  it('delete', (done) => {
      idbStorge.setItem('a', 'a').then(() => {
          return idbStorge.getItem('a').then((result) => {
              expect(result).toEqual('a')
            })
        }).then(() => {
          return idbStorge.removeItem('a')
        }).then(() => {
          return idbStorge.getItem('a').then((result) => {
              expect(result).toBeUndefined()
            })
        }).then(done, fail)
    })

  it('count & clear', (done) => {

      idbStorge.setItem('a', 'a').then(() => {
          return idbStorge.length.then((count) => {
              expect(count).toBeGreaterThan(0)
            })
        }).then(() => {
          return idbStorge.getItem('a').then((result) => {
              expect(result).toEqual('a')
            })
        }).then(() => {
          return idbStorge.clear()
        }).then(() => {
          return idbStorge.length.then((count) => {
              expect(count).toEqual(0)
            })
        }).then(done, fail)

    })

})
