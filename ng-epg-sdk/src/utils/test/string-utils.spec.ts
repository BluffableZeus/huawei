import { StringUtils } from '../string-utils'

describe('StringUtils', () => {

  const inputStrings = ['Adds a listener that', 'The listener is invoked only']

  it('highlight keywords', () => {
      const keywords = ['list']
      const className = 'highlight'
      const results = StringUtils.highlightKeywords(inputStrings, keywords, className)

      expect(results).toEqual(['Adds a <span class="highlight">list</span>ener that', 'The <span class="highlight">list</span>ener is invoked only'])
    })

  it('highlight keywords, embeded', () => {
      const keywords = ['list', 'li']
      const className = 'highlight'
      const results = StringUtils.highlightKeywords(inputStrings, keywords, className)

      expect(results).toEqual(['Adds a <span class="highlight"><span class="highlight">li</span>st</span>ener that',
          'The <span class="highlight"><span class="highlight">li</span>st</span>ener is invoked only'])
    })

  it('highlight keywords,ignore case', () => {
      const keywords = ['LIST']
      const className = 'highlight'
      const results = StringUtils.highlightKeywords(inputStrings, keywords, className)

      expect(results).toEqual(['Adds a <span class="highlight">list</span>ener that', 'The <span class="highlight">list</span>ener is invoked only'])
    })

  it('highlight keywords,ignore case', () => {
      const keywords = ['.']
      const className = 'highlight'
      const results = StringUtils.highlightKeywords(['1.2'], keywords, className)

      expect(results).toEqual(['1<span class="highlight">.</span>2'])
    })

  it('toString', () => {
      expect(StringUtils.toString({ password: 'hwc' })).toEqual(JSON.stringify({ password: 'hwc' }))
    })

  it('toString santinize', () => {
      expect(StringUtils.toString({ password: 'hwc' }, true)).toEqual(JSON.stringify({ password: '******' }))
    })

  it('toString function', () => {
      expect(StringUtils.toString({ test: () => { } }, true)).toEqual('{"test":"Function()"}')
    })
})
