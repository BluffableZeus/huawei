import * as _ from 'underscore'

const defaultGetId = (value): string => {

  const ids = ['ID', 'id']
  const id = ids.find((_id) => {
      return _.has(value, _id)
    })

  return value[id]
}

export class Map<T> {

  private list: Array<T> = []

  private index: { [id: string]: number } = {}

  constructor (list: Array<T> = [], getId: (T) => string = defaultGetId) {
      list.forEach((value: T) => {
          this.set(getId(value), value)
        })
    }

    /**
     *
     */
  set (key: string, value: T): void {
      if (_.isUndefined(key) || _.isNull(key)) {
          return
        }
      const i = this.index[key]
      const oldValue = this.list[i]
      if (oldValue) {
          this.list[i] = value
        } else {
          this.index[key] = this.list.length
          this.list.push(value)
        }
    }

  remove (key: string) {
      if (this.has(key)) {
          const entries = this.getEntries()

          const list: Array<T> = []
          const index = {}

          entries.forEach(([_key, _value]) => {
              if (_key !== key) {
                  index[_key] = list.length
                  list.push(_value)
                }
            })

          this.list = list
          this.index = index
        }
    }

  get (key: string): T {
      const i = this.index[key]
      const oldValue = this.list[i]
      return oldValue
    }

  has (key: string) {
      return !!this.get(key)
    }

  getEntries (): Array<[string, T]> {
      const keys = this.getKeys()
      return keys.map((key) => {
          return [key, this.get(key)]
        }) as any
    }

  getValues (): Array<T> {
      return this.list
    }

  getKeys (): Array<string> {
      return Object.keys(this.index)
    }

  size () {
      return this.list.length
    }
}
