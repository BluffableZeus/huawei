export declare class Token {
  static tokenCount: number
  private uniqId
  private keyword
  static createId (): number
  constructor ();
  setKeyword (keyword: string): void
  getKeyword (): string
  toString (): number
}
export declare class Logger {
  static version: string
  static valve: number
  static security: string
  static maxLength: number
  static sensitiveKeyMap: any
  static physicalPrinter: any
  private fileName
  private scenario
  private category
  private type
  static setVersion (versionNumber: string): void
  static setLevel (level: string): void
  static setSecurity (strength: string): void
  static setSensitiveKeys (keys: Array<string>): void
  static setMaxLogLength (maxLength: number): void
  static setPhysicalPrinter (physicalPrinter: any): void
  static get (...extensions: Array<string>): Logger
  constructor (...extensions: Array<string>);
  log (msg: string, ...args: Array<any>): void
  isLog (): boolean
  debug (msg: string, ...args: Array<any>): any
  isDebug (): boolean
  info (msg: string, ...args: Array<any>): any
  isInfo (): boolean
  warn (msg: string, ...args: Array<any>): any
  isWarn (): boolean
  error (msg: string, ...args: Array<any>): any
  isError (): boolean
  kpi (msg: string, ...args: Array<any>): any
  isKpi (): boolean
  time (timeName: string): void
  timeEnd (timeName: string): void
  timeStamp (name: string): void
  methodBegin (methodName: string, ...args: Array<any>): any
  methodEnd (methodName: string, ...outs: Array<any>): any
  scenarioBegin (...data: Array<any>): any
  scenarioEnd (...data: Array<any>): any
  fetchBegin (api: string, ...data: Array<any>): any
  fetchEnd (api: string, ...data: Array<any>): any
  behave (action: string, msg: string, ...data: Array<any>): any
  keyPress (event: any, ...data: Array<any>): any
  keyRelease (event: any, ...data: Array<any>): any
  snapshot (duration: number, ...data: Array<any>): void
  returnDebugLogers (): string[]
  tipToast (key: string, errorInfo: any, innerDescroption: string, isDefault?: boolean): Token
  private keyPrint (diff, event, data)
  private scenarioPrint (prefix, data)
  private methodPrint (prefix, methodName, args)
  private fetchPrint (prefix, api, data)
  private dataPrint (msg, data, token, level?)
  private parseToken (args)
  private parseLevel (args)
  private parseResponse (args)
  private transformNativeObjects (dataList)
  private transformNativeResponse (response)
  private isLevel (level)
  private print (level, msg, args, token)
  private appendExtraReplacement (signCount, dataCount, msg)
  private howManyIn (searchString)
  private sliceToLimitedStrings (msg, ...replacements)
  private buildFormat (msg, level, token)
  private stringifyArgs (args)
  isSensitive (key: string): any
  private decorateSensitiveInfo (key, value)
  contains (list: Array<any>, item: any): boolean
}
export declare function MethodLog (logger: Logger, level?: string): (target: any, methodName: string, descriptor: PropertyDescriptor) => void
