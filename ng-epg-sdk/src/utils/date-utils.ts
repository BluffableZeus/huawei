import * as moment from 'moment'
import * as _ from 'underscore'

const ONE_DAY = 24 * 60 * 60 * 1000
let LEAVE_DST_HOUR: { [year: string]: Date } = {}

/**
 * Date tool class
 */
export class DateUtils {

    /**
     * To determine whether it was in the hour of summer time
     *
     * @static
     *
     * @memberOf DateUtils
     */
  static isDSTRepeatedTime (date: Date | moment.Moment) {
      let time: Date
      if (date instanceof Date) {
          time = date
        } else {
          time = date.toDate()
        }

      const year: number = time.getUTCFullYear()
      if (_.isUndefined(LEAVE_DST_HOUR[year])) {

            // Find the time of day, day, timezone transition time, only to find out when the summer time
          const infos: Array<{ unit: string, count: number }> = [{ unit: 'month', count: 12 }, { unit: 'day', count: 60 }, { unit: 'hour', count: 24 }]

          const jumpEndDate = _.reduce<{ unit: string, count: number }, Date>(infos, (_jumpDate: Date, info: { unit: string, count: number }) => {
              return _jumpDate && this.calcJumpDate(_jumpDate, info.unit, info.count)
            }, new Date(year, 11, 30))

          if (jumpEndDate) {
              LEAVE_DST_HOUR[year] = moment(jumpEndDate).add(-1, 'hour').toDate()
            } else {
              LEAVE_DST_HOUR[year] = null
            }
        }

      if (LEAVE_DST_HOUR[year]) {
          const jumpBeginDate = LEAVE_DST_HOUR[year]
            // Judge UTC month, day, the same
          return time.getUTCMonth() === jumpBeginDate.getUTCMonth() && time.getUTCDate() === jumpBeginDate.getUTCDate() && time.getUTCHours() === jumpBeginDate.getUTCHours()
        } else {
          return false
        }
    }

    /**
     *
     * Format date
     *  - If less than 1 day, it can only be formatted when minutes, seconds
     *  - If the format after the inclusion of hours, while the time for the daylight saving time overlap,
     *  will automatically add DST suffix
     *
     * | Input | Example | Description |
     * | --- | --- | --- |
     * | `YYYY` | `2014` | 4 or 2 digit year |
     * | `YY` | `14` | 2 digit year |
     * | `Y` | `-25` | Year with any number of digits and sign |
     * | `Q` | `1..4` | Quarter of year. Sets month to first month in quarter. |
     * | `M MM` | `1..12` | Month number |
     * | `MMM MMMM` | `Jan..December` | Month name in locale set by `moment.locale()` |
     * | `D DD` | `1..31` | Day of month |
     * | `Do` | `1st..31st` | Day of month with ordinal |
     * | `DDD DDDD` | `1..365` | Day of year |
     * | `X` | `1410715640.579` | Unix timestamp |
     * | `x` | `1410715640579` | Unix ms timestamp |
     * | `H HH` | `0..23` | 24 hour time |
     * | `h hh` | `1..12` | 12 hour time used with `a A`. |
     * | `a A` | `am pm` | Post or ante meridiem (Note the one character `a p` are also considered valid) |
     * | `m mm` | `0..59` | Minutes |
     * | `s ss` | `0..59` | Seconds |
     * | `S SS SSS` | `0..999` | Fractional seconds |
     * | `Z ZZ` | `+12:00` | Offset from UTC as `+-HH:mm`, `+-HHmm`, or `Z` |
     *
     * @static
     * @param {(Date | number)} value
     * @param {string} format
     * @returns {string}
     *
     * @memberOf DateUtils

     */
  static format (value: Date | number, format: string): string {

      let momDate: moment.Moment

      if (value instanceof Date) {
          momDate = moment(value)
        } else {
          const num = + value
          if (value < ONE_DAY) {
              return moment(value).utc().format(format)
            } else {
              momDate = moment(num)
            }
        }

      const formatedValue = momDate.format(format)
      if (format.indexOf('HH') >= 0 && this.isDSTRepeatedTime(momDate)) {
          return formatedValue + ' DST'
        }

      return formatedValue
    }

    /**
     *
     * Calculate the transition point of Timezone from the backwards
     * @private
     * @static
     * @param {Date} date Find the start time
     * @param {string} unit
     *  - month
     *  - day
     *  - hour
     * @param {number} times Find the maximum number of executions
     * @returns {Date}
     *
     * @memberOf DateUtils
    */
  private static calcJumpDate (date: Date, unit: string, times: number): Date {
      let mom = moment(date)
      let tz = mom.utcOffset()
      while (times > 0) {
          times--
          const d = new Date(mom.toDate())
          mom.add(-1, unit as any)
          if (mom.utcOffset() !== tz) {
              return d
            }
        }
    }
}
