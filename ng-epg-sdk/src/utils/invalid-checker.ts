import * as _ from 'underscore'

/**
 * Tools
 *
 * @export
 * @class TypeUtilsClass
 */
class InvalidCheckerClass {
  private urlReg = /^(http|https):\/\//

  public isURLInvalid (url: string) {
      return _.isEmpty(url) || !this.isURL(url)
    }

  private isURL (url = '') {
      return this.urlReg.test(url.toLowerCase())
    }
}

export const InvalidChecker = new InvalidCheckerClass()
