
const stringifyPrimitive = function (value: any) {

  switch (typeof value) {
      case 'boolean':
        return value ? 'true' : 'false'

      case 'number':
        return isFinite(value) ? value : ''

      case 'string':
        return value

      default:
        return ''
    }
}

export const encode = (obj: any, sep = '&', eq = '=', name?) => {
  if (obj === null) {
      obj = undefined
    }

  if (typeof obj === 'object') {
      return Object.keys(obj).map(function (k) {
          const ks = encodeURIComponent(stringifyPrimitive(k)) + eq
          if (Array.isArray(obj[k])) {
              return obj[k].map(function (v) {
                  return ks + encodeURIComponent(stringifyPrimitive(v))
                }).join(sep)
            } else {
              return ks + encodeURIComponent(stringifyPrimitive(obj[k]))
            }
        }).join(sep)
    }

  if (!name) {
      return ''
    }

  return encodeURIComponent(stringifyPrimitive(name)) + eq + encodeURIComponent(stringifyPrimitive(obj))
}
