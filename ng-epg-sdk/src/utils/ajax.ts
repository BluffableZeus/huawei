import { isSTB } from './is_stb'
import * as _ from 'underscore'
import { BehaviorSubject } from 'rxjs'

import { Logger } from './logger'
import { Config } from '../core/config'
import { HookService } from '../services/hook.service'
import { EventService } from '../services/event.service'
import { msaErrorCodeMap } from '../core/const'
import { xr, METHODS } from './xr'
import { http } from '../sdk/'
import { encode } from './encode'
import { sha256 } from '../sdk/lang/sha256'

const log = new Logger('ajax.ts')
const INF_REG = /.*\/([^/?#]*).*/

export const Ajax = {
  async: true,

  subject: new BehaviorSubject('AJAX_COMPLETE'),

  ajaxComplete: (done: Function) => {
      this.subject.subscribe({
          next: (name: string) => {
              if ('AJAX_COMPLETE' === name) {
                  done()
                  this.subject = new BehaviorSubject(name)
                }
            }
        })
    }
}

xr.configure({
  xmlHttpRequest: () => {
      const xhr = new XMLHttpRequest()
      const oldOpen = xhr.open
      xhr.open = (method, param) => {
          const ret = oldOpen.apply(xhr, [method, param, Ajax.async])
          Ajax.async = true
          return ret
        }
      return xhr
    }
})

export interface AjaxRequest {
  inf?: string
  method?: string
  url: string
  params?: any
  data?: { [key: string]: any }
  body?: { [key: string]: any }
  header?: {}
  headers?: {}
  dump?: Function
  load?: Function
  xmlHttpRequest?: Function
  promise?: Function
  withCredentials?: boolean
  abort?: Function
  options?: AjaxOptions
}

export interface AjaxResponse {
  status?: number
  body?: string
  data?: { result: { retCode: string } }
  xhr?: XMLHttpRequest
  header?: string
  dataSource?: string
  originCode?: string
  usedTime?: number
}

export interface AjaxSuccess {
  request: AjaxRequest
  response: AjaxResponse
}

export interface AjaxFail extends AjaxSuccess {
}

export interface AjaxNext {
  (request: AjaxRequest): Promise<AjaxResponse>
}

export interface AjaxOptions {
  method?: string
  params?: any
  headers?: {}
  withCredentials?: boolean
  abort?: Function
  [key: string]: any
}

export interface AjaxStatus {
  type: string
  request: AjaxRequest
  response?: AjaxResponse
}

let semaphore = 0

/**
 * If there is no Ajax request, execute done
 * If there is currently Ajax, after the end of Ajax, the implementation of done
 */
export const ajaxComplete = Ajax.ajaxComplete
export const AjaxEmitter = Ajax.subject
export const AjaxStatusEmitter = new BehaviorSubject<AjaxStatus>(null)

const semaphoreChange = (offset: number) => {
  const oldSemaphore = semaphore
  semaphore = semaphore + offset
  if (0 === oldSemaphore && 1 === semaphore) {
      Ajax.subject.next('AJAX_START')
    } else if (1 === oldSemaphore && 0 === semaphore) {
      Ajax.subject.next('AJAX_COMPLETE')
    }
}

const addTail = (url: string, params): string => {
  let tail = '&'
  if (url.indexOf('?') < 0) {
      tail = '?'
    }
  tail = tail + encode(params)
  if (!params.SID) {
      return url + tail
    }
  if (!params.DEVICE) {
      tail = tail + '&DEVICE=' + (isSTB ? 'EPG' : 'PC')
    }
  if (!params.DID) {
      tail = tail + '&DID=' + (isSTB ? sha256(Config.physicalDeviceID) : Config.physicalDeviceID)
    }
  return url + tail
}

/**
 * Ajax request method
 *
 * @export
 * @param {string} url
 * @param {*} req
 * @param {AjaxOptions} [options={}]
 * @returns
 */
export function ajax (url: string, req: any, options: AjaxOptions = {}) {
  options.isIgnoreError = _.isUndefined(options.isIgnoreError) ? false : options.isIgnoreError
  const id = _.uniqueId('')
  const method = options.method || METHODS.POST
  const openUrl = url && options.params ? addTail(url, options.params) : url
  const request: AjaxRequest = {
      method,
      url: openUrl,
      inf: _.last(INF_REG.exec(url)),
      xmlHttpRequest: () => {
          const xhr = new XMLHttpRequest()

          try {
              xhr.timeout = Config.get('ajax_timeout') || 15 * 1000
            } catch (error) {
              log.error('xhr.timeout, error:%s', error)
            }

          return xhr
        },
      load: (...args) => {
          if (log.isKpi()) {
              log.kpi('[VSP_HTTP]HybirdVideoKPI_EPG_ajax|before ajax() parse HTTP TASKID=%s, |%s|pass', id, Date.now())
            }
          try {
              return JSON.parse.apply(null, args)
            } catch (e) {
              log.error('[MSA_ERROR] Response json is invalid, [error code:%s]', msaErrorCodeMap.interface['38023'])
            }
        },
      header: options.headers,
      options
    }

  if (method === METHODS.GET) {
      request.params = req
    } else {
      _.extend(request, options)
      request.data = req
      request.body = req
    }

  const date = Date.now()

  log.kpi('[VSP_HTTP]HybirdVideoKPI_EPG_ajax|start ajax() HTTP TASKID=%s, request:%s|%s|pass', id, request.data || request.body, Date.now())

  semaphoreChange(1)
  AjaxStatusEmitter.next({
      type: 'start',
      request
    })

  const afterAjaxCb = (xrResp: AjaxResponse) => {
      semaphoreChange(-1)
      AjaxStatusEmitter.next({
          type: 'end',
          request,
          response: xrResp
        })
      xrResp.usedTime = Date.now() - date
    }

  const hookService: HookService = HookService['_instance']

  let token = log.fetchBegin(request.inf, 'kpi', request)

  return hookService.invoke(['ajax', `ajax:${request.inf}`], request, (_request) => {
      if (Config.get('enableSDKCache')) {
          return http.request(_request).then((resp) => {
              const response = _.extend({}, resp)
              response.data = resp.body && JSON.parse(resp.body)
              return response
            }, (resp) => {
              const response = _.extend({}, resp)
              response.data = resp.body
              return Promise.reject(response)
            })
        } else {
          return xr.send(_request)
        }
    }).then((xrResp: AjaxResponse) => {
      afterAjaxCb(xrResp)

      log.fetchEnd(request.inf, 'kpi', xrResp.xhr || xrResp.status, xrResp.data, token)

      if (log.isDebug()) {
          log.debug('[VSP_HTTP]end ajax() HTTP TASKID=%s DURATION(ms)=%s,inf:%s',
                id, Date.now() - date, request.inf, true)
        } else if (log.isKpi()) {
          log.kpi('[VSP_HTTP]HybirdVideoKPI_EPG_ajax|end ajax() HTTP TASKID=%s DURATION(ms)=%s,inf:%s, |%s|pass',
                id, xrResp.usedTime, request.inf, Date.now())
        }

      if (xrResp.usedTime > 10 * 1000) {
          log.error('[MSA_ERROR] Response use too long time: [%ss] [error code:%s]',
                xrResp.usedTime / 1000, msaErrorCodeMap.http['26752'])
        }

      return Promise.all([
          EventService.emitAsync(`${request.inf.toUpperCase()}_SUCCESS`, {
              request: request.body,
              response: xrResp.data
            }),
          EventService.emitAsync('AJAX_SUCCESS', {
              request: request,
              response: _.omit(xrResp, 'response', 'xhr')
            })
        ]).then(() => xrResp.data, () => xrResp.data)

    }, (xrResp: AjaxResponse) => {
      afterAjaxCb(xrResp)

      log.fetchEnd(request.inf, 'kpi', xrResp.xhr || xrResp.status, xrResp.data, token)

      if (log.isKpi()) {
          log.kpi('[VSP_HTTP]HybirdVideoKPI_EPG_ajax|end ajax() HTTP TASKID=%s DURATION(ms)=%s,url:%s|%s|fail',
                id, xrResp.usedTime, url, Date.now())
        } else {
          log.error('[VSP_HTTP]end ajax(%s), request:%s', id, request.body, true)
        }

      if (xrResp.data.result.retCode.indexOf('http.') >= 0 && msaErrorCodeMap.http[xrResp.status]) {
          log.error('[MSA_ERROR] Network connection failed: [error code:%s]', msaErrorCodeMap.http[xrResp.status])
        }

      return Promise.all([
          EventService.emitAsync(`${request.inf.toUpperCase()}_FAIL`, {
              request: request.data || request.body,
              response: xrResp.data
            }),
          EventService.emitAsync('AJAX_FAIL', {
              request: request,
              response: _.omit(xrResp, 'response', 'xhr')
            })
        ]).then(() => Promise.reject(xrResp.data), () => Promise.reject(xrResp.data))

    })
}
