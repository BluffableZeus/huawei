import * as _ from 'underscore'
import { Content, ChannelDetail, Channel } from '../vsp/'
import { ChannelService } from '../services'

const CONTENTTYPE_REG = /([^_]*)$/

export const CONTENTTYPE_TO_NAME_MAPPING = {
  'AUDIO_CHANNEL': 'channel',
  'CHANNEL': 'channel',
  'VIDEO_CHANNEL': 'channel',
  'VOD': 'VOD',
  'VIDEO_VOD': 'VOD',
  'AUDIO_VOD': 'VOD',
  'PROGRAM': 'playbill',
  'SUBJECT': 'subject',
  'GENRE': 'genre',
  'LOCK_SERIES': 'lockSeries'
}

/**
 * Content object tool class
 */
export const ContentUtils = {

    /**
     * Will be the specific type of content type, that is, do not distinguish between audio and video
     *
     *
     * @param {string} contentType
     * @returns
     */
  normalizeName: (contentType: string): 'channel' | 'vod' | 'genre' | 'lockSeries' | 'playbill' | 'subject' => {

      if (contentType && !CONTENTTYPE_TO_NAME_MAPPING[contentType]) {
          CONTENTTYPE_TO_NAME_MAPPING[contentType] = CONTENTTYPE_REG.exec(contentType)[1].toLocaleLowerCase()
        }
      return CONTENTTYPE_TO_NAME_MAPPING[contentType]
    },

    /**
     * Flatten the data in the Content object
     *
     * @param {Array<Content>} list
     * @returns
     */
  flatten: (list: Array<Content>) => {
      return _.map(list, (content: Content) => {
          const name = ContentUtils.normalizeName(ContentUtils.getContentType(content))
          return content[name]
        })
    },

    /**
     * Gets the ID in the content object
     *
     * @param {any} content
     * @returns
     */
  getID: (content) => {
      return content['genreID'] || content['ID'] || content['itemID']
    },

    /**
     * Get the content type
     *
     * @param {any} content
     * @returns
     */
  getContentType: (content) => {
      return content.contentType || content.type || content.lockType || content.bookmarkType
    },

  sortContentList: (contentList: Array<Content>, channelService: ChannelService): Array<Content> => {
      return _.sortBy(contentList, (content: Content) => {
          let channel: Channel
          if (channel = content.channel) {
              const channelDetai: ChannelDetail = channelService.getChannelByID(channel.ID)
              return channelDetai && +channelDetai.channelNO
            }
        })
    }
}
