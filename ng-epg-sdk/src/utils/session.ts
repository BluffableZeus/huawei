import * as _ from 'underscore'

/**
 *
 */
export class Session {

  private store = {}
    /**
     * Obtain value by key
     *
     * @param {String} key
     * @return {Object} value
     */
  public get (key) {
      return this.store[key]
    }

    /**
     * Save value to session
     *
     * @param {String} key
     * @param {Object} value
     * @param {Boolean} [persist=false] whether store value to local
     */
  public put (key: string, value: any): any {

      if (_.isNull(value) || _.isUndefined(value) || _.isNaN(value)) {
          return this.remove(key)
        }

      this.store[key] = value

      return value
    }

    /**
     * Clear values
     *
     * @param {Boolean} [persist=false] If true, will clear all values include data stored on local
     */
  public clear (): void {
      this.store = {}
    }
    /**
     * Remove value by key
     *
     * @param {String} key
     */
  public remove (key) {
      delete this.store[key]
    }

  public pop (key) {
      this.remove(key)
    }

  public getConfiguration (key: string) {
      const configurationResp = this.get('allParameterList')
      if (!configurationResp.configurations) {
          return null
        }

      const configs = _.chain(configurationResp.configurations).map(configuration => configuration.values).flatten().value()
      const config = _.find(configs, c => c.key === key)

      return config ? config.values : null
    }
}

export const session = new Session()
