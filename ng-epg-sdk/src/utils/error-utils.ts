import { msaErrorMap } from './../core/const'

/**
 * Tools
 *
 * @export
 * @class ErrorUtilsClass
 */
export class ErrorUtilsClass {
  public getMSAError (code = 'default') {
      let internalCode = code
      let msaError = msaErrorMap[internalCode]

      if (!msaError) {
          msaError = msaErrorMap['default']
          internalCode = 'default'
        }

      return this.parseMSAError(msaError, code)
    }

  private parseMSAError (error, code): MSAError {
      return {
          'internalCode': code,
          'externalCode': error.e,
          'message': error.m,
          'toString': () => {
              return `MSA.${code} externalCode: ${error.e} message: ${error.m}`
            }
        }
    }

  public reportMSAError () {

    }
}

export interface MSAError {
  'internalCode': string
  'externalCode': string
  'message': string
  'toString': Function
}
export const ErrorUtils = new ErrorUtilsClass()
