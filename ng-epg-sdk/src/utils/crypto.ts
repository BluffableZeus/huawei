import * as _ from 'underscore'
import { win } from './win'
const md5 = require('md5')

export function hwMd5 (str: string | number) {
  const encrypt = md5(str + '99991231')

  return _.map(_.compact(encrypt.split(/(.{2})/g)), function (c: string) {
      return c.replace(/^0/, '')
    }).join('').substr(0, 8)
}

export function securityRandomString (maxLength = 128): string {
  const randomIntArray = new Uint32Array(Math.ceil(maxLength / 7))
  const crypto = win['msCrypto'] || win['crypto']
  crypto.getRandomValues(randomIntArray)
  const randomStringArray = _.map(randomIntArray, i => {
      return i.toString(36)
    })
  return randomStringArray.join('').substring(0, maxLength)
}
