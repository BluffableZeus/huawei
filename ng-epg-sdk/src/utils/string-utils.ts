import { Config } from './../core/config'
import * as _ from 'underscore'

/**
 * Tool class
 *
 * @export
 * @class StringUtilsClass
 */
export class StringUtilsClass {

  private privacyKeyMap: { [key: string]: boolean }
  private specialCharacters: Array<string> = ['|', '[', '(', ')', '?', '*', '+']

    /**
     * @param inputString
     * @param keywords
     * @param className
     *
     * Adds a listener that
     *      =>
     * Adds a <span class="highlight">list</span>ener that
     */
  highlightKeywords (inputStrings: Array<string>, keywords: Array<string>, className = 'highlight') {
      keywords = _.compact(keywords)
      const keywordRegs = _.map(keywords, (keyword: string) => {
          if (keyword === '.') {
              return new RegExp(`(\\${keyword})`, 'i')
            } else if (this.hasSpecialCharacters(keywords)) {
              return new RegExp('[' + keyword + ']', 'i')
            } else {
              return new RegExp(`(${keyword})`, 'i')
            }
        })

      inputStrings = _.map(inputStrings, (inputString: string) => {
          return _.reduce<any, any>(keywordRegs, (str: string, reg: RegExp, index: number) => {
              return str.replace(reg, '##$1@@')
            }, inputString)
        })

      return _.map(inputStrings, (inputString: string) => {
          return inputString.replace(/##/g, `<span class="${className}">`).replace(/@@/g, '</span>')
        })
    }

  hasSpecialCharacters (keywords: Array<string>) {
      let hasSpecial = false
      keywords = (keywords.join('')).split('')
      let inputString = _.find(keywords, (keyword) => {
          return _.contains(this.specialCharacters, keyword)
        })
      if (inputString) {
          hasSpecial = true
        }
      return hasSpecial
    }

    /**
     * @param {Object} json
     * @param {boolean} isSanitize
     *
     * - true Do not print the configured privacy field
     * - false Do not filter
     */
  toString (json: Object, isSanitize = false) {
      if (_.isNull(json) || _.isUndefined(json)) {
          return ''
        }

      const valueList = []
      let retStr
      try {
          retStr = JSON.stringify(json, (key, value) => {

              if (_.isObject(value) && _.contains(valueList, value)) {
                  return '#CYCLE#'
                }

              valueList.push(value)

              if (isSanitize && this.isPrivacyKey(key)) {
                  return '******'
                } else if (_.isFunction(value)) {
                  return 'Function()'
                }

              return value
            })
        } catch (e) {
          retStr = '[STRINGIFY ERROR]' + e
        }

      return retStr.replace(/\\"/g, '\'')
    }

  private isPrivacyKey (key: string) {

      if (!this.privacyKeyMap) {
          const privacyKeys = Config.get('privacy_keys',
              ['password', 'pwd', 'profile', 'passwd', 'loginname', 'subscriber',
                'account', 'user', 'mail', 'phone', 'mobile', 'sessionid', 'token', 'responseText']) as Array<string>

          this.privacyKeyMap = _.chain(privacyKeys).map(_key => [_key.toUpperCase(), true]).object().value() as any
        }

      if (this.privacyKeyMap[key]) {
          return true
        } else if (this.privacyKeyMap[key.toUpperCase()]) {
          this.privacyKeyMap[key] = true
          return true
        }

      return false
    }

}

export const StringUtils = new StringUtilsClass()
