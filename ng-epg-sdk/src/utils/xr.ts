import { encode } from './encode'

export interface Methods {
  GET: 'GET'
  POST: 'POST'
  PUT: 'PUT'
  DELETE: 'DELETE'
  PATCH: 'PATCH'
  OPTIONS: 'OPTIONS'
  HEAD: 'HEAD'
}

export interface Events {
  READY_STATE_CHANGE: 'readystatechange'
  LOAD_START: 'loadstart'
  PROGRESS: 'progress'
  ABORT: 'abort'
  ERROR: 'error'
  LOAD: 'load'
  TIMEOUT: 'timeout'
  LOAD_END: 'loadend'
}

export interface Headers {
  [key: string]: string
}

export interface EventCallbacks {
  [key: string]: any
}

export type MethodType = 'GET' | 'POST' | 'PUT' | 'DELETE' | 'PATCH' | 'OPTIONS' | 'HEAD'

export interface Config {
  url?: string
  method: MethodType
  data?: any
  headers?: Headers
  dump?: (data: any) => string
  load?: (string: string) => any
  xmlHttpRequest?: () => XMLHttpRequest
  promise?: (fn: () => Promise<any>) => Promise<any>
  abort?: any
  params?: any
  withCredentials?: boolean
  raw?: boolean
  events?: EventCallbacks
}

export interface Response {
  status: number
  response: any
  data?: any
  xhr: XMLHttpRequest
}

export interface DynamicObject {
  [key: string]: any
}

export const METHODS: Methods = {
  GET: 'GET',
  POST: 'POST',
  PUT: 'PUT',
  DELETE: 'DELETE',
  PATCH: 'PATCH',
  OPTIONS: 'OPTIONS',
  HEAD: 'HEAD'
}

export const EVENTS: Events = {
  READY_STATE_CHANGE: 'readystatechange',
  LOAD_START: 'loadstart',
  PROGRESS: 'progress',
  ABORT: 'abort',
  ERROR: 'error',
  LOAD: 'load',
  TIMEOUT: 'timeout',
  LOAD_END: 'loadend'
}

export const XR_DEFAULTS = {
  method: METHODS.GET,
  data: undefined,
  headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
  dump: JSON.stringify,
  load: JSON.parse,
  xmlHttpRequest: (): XMLHttpRequest => new XMLHttpRequest(),
  promise: (fn: () => Promise<any>) => new Promise(fn),
  withCredentials: false
}

const res = (xhr: XMLHttpRequest, data?: any): Response => ({
  status: xhr.status,
  response: xhr.response,
  data,
  xhr
})

class XR {

  private config: Config = Object.assign({}, XR_DEFAULTS)

  public configure (opts: Partial<Config>): void {
      this.config = Object.assign({}, this.config, opts)
    }

  public send (args: Config): Promise<any> {
      return this.promise(args, (resolve: any, reject: any) => {
          const opts: Config = Object.assign({}, this.config, args)
          const xhr = opts.xmlHttpRequest()

          if (opts.abort) {
              args.abort(() => {
                  reject(res(xhr))
                  xhr.abort()
                })
            }

          if (opts.url === undefined) {
              throw new Error('No URL defined')
            }

          const openUrl = opts.params ? `${opts.url.split('?')[0]}?${encode(opts.params)}` : opts.url

          xhr.open(opts.method, openUrl, true)

            // setting after open for compatibility with IE versions <=10
          xhr.withCredentials = opts.withCredentials

          xhr.addEventListener(EVENTS.LOAD, () => {
              if (xhr.status >= 200 && xhr.status < 300) {
                  let data
                  if (xhr.responseText) {
                      data = opts.raw === true ? xhr.responseText : opts.load(xhr.responseText)
                    }
                  resolve(res(xhr, data))
                } else {
                  reject(res(xhr))
                }
            })

          xhr.addEventListener(EVENTS.ABORT, () => reject(res(xhr)))
          xhr.addEventListener(EVENTS.ERROR, () => reject(res(xhr)))
          xhr.addEventListener(EVENTS.TIMEOUT, () => reject(res(xhr)))

          for (const k in opts.headers) {
              if (!{}.hasOwnProperty.call(opts.headers, k)) {
                  continue
                }
              xhr.setRequestHeader(k, opts.headers[k])
            }

          if (opts.events) {
              for (const k in opts.events) {
                  if (!{}.hasOwnProperty.call(opts.events, k)) {
                      continue
                    }
                  xhr.addEventListener(k, opts.events[k].bind(null, xhr), false)
                }
            }

          const xhrData = (typeof opts.data === 'object' && !opts.raw) ? opts.dump(opts.data) : opts.data

          if (xhrData !== undefined) {
              xhr.send(xhrData)
            } else {
              xhr.send()
            }
        })
    }

  public get (url: string, params: any, args: Partial<Config>) {
      return this.send(Object.assign({ url, method: METHODS.GET, params }, args))
    }

  public put (url: string, data: any, args: Partial<Config>) {
      return this.send(Object.assign({ url, method: METHODS.PUT, data }, args))
    }

  public post (url: string, data: any, args: Partial<Config>) {
      return this.send(Object.assign({ url, method: METHODS.POST, data }, args))
    }

  public patch (url: string, data: any, args: Partial<Config>) {
      return this.send(Object.assign({ url, method: METHODS.PATCH, data }, args))
    }

  public del (url: string, args: Partial<Config>) {
      return this.send(Object.assign({ url, method: METHODS.DELETE }, args))
    }

  public options (url: string, args: Partial<Config>) {
      return this.send(Object.assign({ url, method: METHODS.OPTIONS }, args))
    }

  private promise (args: Config, fn: any) {
      if (args && args.promise) {
          return args.promise(fn)
        } else {
          return (this.config.promise || XR_DEFAULTS.promise)(fn)
        }
    }
}

export const xr = new XR()
