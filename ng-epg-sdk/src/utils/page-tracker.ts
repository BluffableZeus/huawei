import { win } from './win'
interface BrowseContentLog {
  subscriberID: string
  deviceID: string
  actionType: string
  contentCode: string
  businessType: number
  keyword: string
  genre: string
  castID: string
  source: number
  timestamp: string
  deviceType?: string
  entrance: string
  recmActionID: string
  number: number
  year: string
  productZone: string
  recmActionType: number
  productID: string
  extensionFields: string
}

interface BrowsePageLog {
  pageFrom: string
  pageDiv: string
  pageTo: string
  stayTime: string
  timestamp: string
  extensionFields: string
}

interface BrowsePageLogRequest {
  subscriberID: string
  deviceID: string
  epgTemplateID?: string
  pageFrom: string
  pageDiv?: string
  pageTo: string
  stayTime?: string
  timestamp?: string
  deviceType?: string
  extensionFields?: string
}

interface UserClickLog {
  EpgPageName: string
  EpgPagePath: string
  OperType: string
  OperObjectCode: string
  LinkInTime: string
  LinkOutTime: string
  extensionFields: string
}

interface UserClickLogRequest {
  subscriberID: string
  deviceID: string
  EpgPageName: string
  EpgPagePath: string
  OperType: string
  OperObjectCode: string
  LinkInTime: string
  LinkOutTime: string
  extensionFields: string
}

interface CommonEventLog {
  actionType: string
  label: string
  timestamp: string
}

interface CommonEventLogRequest {
  subscriberID: string
  deviceID: string
  actionType: string
  label?: string
  timestamp?: string
  deviceType?: string
}

interface Param {
  appid: string
  apppwd: string
  browseContentLog: string
  browsePageLog: string
  commonEventLog: string
  userClickLog: string
}

type LogType = 'browseContentLog' | 'commonEventLog' | 'browsePageLog' | 'userClickLog'

class PageTrackerClass {

    /**
     * Browse path acquisition server address
     */
  private actionUrl = ''

    /**
     * Subscriber ID
     */
  private subscriberID = ''

    /**
     * Terminal physical device ID
     */
  private deviceID = ''

    /**
     * EPG page template number
     */
  private epgTemplateID = ''

    /**
     * Terminal device type
     */
  private deviceType = ''

    /**
     * whether Upload switch
     */
  private needSend = true

    /**
     * EPG page application unique identifier
     */
  private appid = ''

    /**
     * Application of EPG page access authentication password
     */
  private apppwd = ''

    /**
     * Cumulative number
     */
  private sendCount = 5

    /**
     * Interval length
     */
  private sendTime = 10000

  private requestIntervalId

  private browseContentLog: Array<BrowseContentLog> = []
  private browsePageLog: Array<BrowsePageLogRequest> = []
  private userClickLogList = []
  private commonEventLog = []
  private sendParams
  private userLogCollectStatus

  constructor () {
      clearInterval(this.requestIntervalId)
      const _setInterval = win['__zone_symbol__setInterval'] || setInterval
      this.requestIntervalId = _setInterval(() => {
          if (this.checkSend(true)) {
              this.sendAjaxRequest(this.sendParams)
              this.browseContentLog.splice(0, this.browseContentLog.length)
              this.browsePageLog.splice(0, this.browsePageLog.length)
              this.commonEventLog.splice(0, this.commonEventLog.length)
            }
        }, this.sendTime)

    }

    /**
     * [__checkParmsContentInternal method for checking the validity of a parameter when uploading data]
     * @param  {[string]} logType      [Log type：browseContentLog,browsePageLog,commonEventLog]
     * @param  {[object]} parmscontent [content object]
     * @return {[boolean]}   [true:The parameters of the legal；false:Invalid parameter]
     */
  private checkParmsContent (logType: LogType, parmscontent: BrowseContentLog & BrowsePageLog & UserClickLog & CommonEventLog) {
        // All events must pass parameters
      let checkParms: CommonEventLog & BrowseContentLog & BrowsePageLog & UserClickLog = {
          'appid': this.appid,
          'apppwd': this.apppwd,
          'subscriberID': this.subscriberID,
          'deviceID': this.deviceID
        } as any
        // Different events will be sent to specific parameters

      switch (logType) {
          case 'browseContentLog':
            checkParms.actionType = parmscontent.actionType

            if (parmscontent.actionType === 'searchContent') {
                  checkParms.keyword = parmscontent.keyword
                }
            if (parmscontent.actionType === 'viewContentDetail') {
                  checkParms.source = parmscontent.source
                }
            break

          case 'commonEventLog':
            checkParms.actionType = parmscontent.actionType

                // When the keyword content through the keyword search field required
            if (parmscontent.actionType === 'searchContent') {
                  checkParms.keyword = parmscontent.keyword
                }
            break

          case 'browsePageLog':
            checkParms.pageFrom = parmscontent.pageFrom
            checkParms.pageTo = parmscontent.pageTo
            break

          case 'userClickLog':
            checkParms = {} as any
            checkParms.EpgPageName = parmscontent.EpgPageName
            checkParms.EpgPagePath = parmscontent.EpgPagePath
            break

          default:
            break
        }

      for (const key in checkParms) {
          if (checkParms.hasOwnProperty(key) && !this.checkParmsError(key, checkParms[key])) {
              return false
            }
        }
      return true
    }

    /**
     * [__checkParmsError judge null and undefined, print error log]
     * @param  {[type]} name [description]
     * @param  {[type]} val  [description]
     * @return {[type]}      [description]
     */
  private checkParmsError (name, val) {
      if (val === '') {
          return false
        }
      if (typeof (val) === 'undefined') {
          return false
        }
      return true
    }

  private sendAjaxRequest (params) {
      if (this.needSend && this.userLogCollectStatus) {
            // Do not upload page jump events while browsing content

          const ajaxRequest = new XMLHttpRequest()
          ajaxRequest.open('POST', this.actionUrl)
          ajaxRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded')
          ajaxRequest.send(JSON.stringify(params))
          ajaxRequest.addEventListener('readystatechange', () => {
              const finishState = 4
              const successStatus = 200
              if ((ajaxRequest.readyState === finishState) && (ajaxRequest.status === successStatus)) {

                } else {
                  this.browseContentLog = []
                  this.browsePageLog = []
                  this.commonEventLog = []
                }
            })

        }
    }

  private checkSend (isTimeout) {
      let returnVal = false
      if (isTimeout) {
          if ((this.browseContentLog.length + this.browsePageLog.length + this.commonEventLog.length) > 0) {
              this.sendParams = {
                  'appid': this.appid,
                  'apppwd': this.apppwd,
                  'browseContentLog': this.browseContentLog,
                  'browsePageLog': this.browsePageLog,
                  'commonEventLog': this.commonEventLog
                }
              returnVal = true
            }
        } else {
          if ((this.browseContentLog.length === this.sendCount || this.browsePageLog.length === this.sendCount || this.commonEventLog.length === this.sendCount)
                || (this.browseContentLog.length + this.browsePageLog.length + this.commonEventLog.length) > this.sendCount) {
              returnVal = true
            }
        }
      return returnVal
    }

    /**
     *  Gets the  needed parameters to upload the user settings
     * @param  {[type]} browseParamsMap [Default parameter list]
     * @param  {[type]} paramsArray     [Specific input parameters]
     * @return {[type]}                 [description]
     */
  private getSendParamsObj (browseParamsMap, paramsArray): any {
      const contentObj = {}
      for (let i = 0; i < paramsArray.length; i++) {
          if (paramsArray[i]) {
                // If the parameters are set , upload
              contentObj[browseParamsMap[i]] = paramsArray[i]
            }
        }
      return contentObj
    }

    /** [start description]Start push upload data, compatible with the old version
     * @param  {[type]} events [description]
     * @param  {[type]} parms0 [description]
     * @param  {[type]} parms1 [description]
     * @param  {[type]} parms2 [description]
     * @param  {[type]} parms3 [description]
     * @param  {[type]} parms4 [description]
     * @param  {[type]} parms5 [description]
     * @param  {[type]} parms6 [description]
     * @param  {[type]} parms7 [description]
     * @return {[type]}        [description]
     */
    // tslint:disable-next-line:cyclomatic-complexity
  public start (events, parms0, parms1, parms2, parms3, parms4, parms5, parms6, parms7) {
      if (events === 'browseContentLog') {
          const contentObj = {} as BrowseContentLog
          contentObj.subscriberID = this.subscriberID
          contentObj.deviceID = this.deviceID
          contentObj.deviceType = this.deviceType
          contentObj.actionType = parms0 || contentObj.actionType
          contentObj.contentCode = parms1 || contentObj.contentCode
          contentObj.businessType = parms2 || contentObj.businessType
          contentObj.keyword = parms3 || contentObj.keyword
          contentObj.genre = parms4 || contentObj.genre
          contentObj.castID = parms5 || contentObj.castID

          if ('viewContentDetail' === parms0 && parms6) {
              contentObj.source = parms6
            }

          contentObj.timestamp = parms7 || contentObj.timestamp

          this.browseContentLog.push(contentObj)

        } else if (events === 'browsePageLog') { // Browse content  page
          const pageObj = {} as BrowsePageLogRequest
          pageObj.subscriberID = this.subscriberID
          pageObj.deviceID = this.deviceID
          pageObj.epgTemplateID = this.epgTemplateID
          pageObj.deviceType = this.deviceType

          pageObj.pageFrom = parms0 || pageObj.pageFrom
          pageObj.pageDiv = parms1 || pageObj.pageDiv
          pageObj.pageTo = parms2 || pageObj.pageTo
          pageObj.stayTime = parms3 || pageObj.stayTime
          pageObj.timestamp = parms4 || pageObj.timestamp

          this.browsePageLog.push(pageObj)

        } else if (events === 'commonEventLog') { // Expand
          const eventObj = {} as CommonEventLogRequest
          eventObj.subscriberID = this.subscriberID
          eventObj.deviceID = this.deviceID
          eventObj.deviceType = this.deviceType

          eventObj.actionType = parms0 || eventObj.actionType
          eventObj.label = parms1 || eventObj.label
          eventObj.timestamp = parms2 || eventObj.timestamp

          this.commonEventLog.push(eventObj)

        }
      if (this.checkSend(false)) {
          const sendParams = {
              'appid': this.appid,
              'apppwd': this.apppwd,
              'browseContentLog': this.browseContentLog,
              'browsePageLog': this.browsePageLog,
              'commonEventLog': this.commonEventLog
            }
          this.sendAjaxRequest(sendParams)

          this.browseContentLog.splice(0, this.sendCount)
          this.browsePageLog.splice(0, this.sendCount)
          this.commonEventLog.splice(0, this.sendCount)
        }
    }

    /**
     * Start push to different types list and upload data
     * @param  {[type]} events      [Event type]
     * @param  {[type]} paramsArray [Input parameter array]
     * @return {[type]}             [description]
     */
  private __start (events, paramsArray) {
      switch (events) {
          case 'browseContentLog':
            const contentParamsMap = {
                  0: 'actionType',
                  1: 'contentCode',
                  2: 'businessType',
                  3: 'keyword',
                  4: 'genre',
                  5: 'castID',
                  6: 'source',
                  7: 'timestamp',
                  8: 'entrance',
                  9: 'recmActionID',
                  10: 'number',
                  11: 'year',
                  12: 'productZone',
                  13: 'recmActionType',
                  14: 'productID',
                  15: 'extensionFields'
                }
                // Set input parameters
            const contentObj: BrowseContentLog = this.getSendParamsObj(contentParamsMap, paramsArray)

            contentObj.subscriberID = this.subscriberID
            contentObj.deviceID = this.deviceID
            contentObj.deviceType = this.deviceType
            this.browseContentLog.push(contentObj)
            break
          case 'browsePageLog':
            const pageParamsMap = {
                  0: 'pageFrom',
                  1: 'pageDiv',
                  2: 'pageTo',
                  3: 'stayTime',
                  4: 'timestamp',
                  5: 'extensionFields'
                }
            const pageObj: BrowsePageLogRequest = this.getSendParamsObj(pageParamsMap, paramsArray)

            pageObj.subscriberID = this.subscriberID
            pageObj.deviceID = this.deviceID

            pageObj.deviceType = this.deviceType

            this.browsePageLog.push(pageObj)
            break
          case 'commonEventLog':
            const commonParamsMap = {
                  0: 'actionType',
                  1: 'label',
                  2: 'timestamp'
                }
            const commonObj: CommonEventLogRequest = this.getSendParamsObj(commonParamsMap, paramsArray) as any
            commonObj.subscriberID = this.subscriberID
            commonObj.deviceID = this.deviceID
            commonObj.deviceType = this.deviceType
            this.commonEventLog.push(commonObj)
            break
          case 'userClickLog':
            const userClickParamsMap = {
                  0: 'EpgPageName',
                  1: 'EpgPagePath',
                  2: 'OperType',
                  3: 'OperObjectCode',
                  4: 'LinkInTime',
                  5: 'LinkOutTime',
                  6: 'extensionFields'
                }
            const userClickLog: UserClickLogRequest = this.getSendParamsObj(userClickParamsMap, paramsArray)
            this.userClickLogList.push(userClickLog)
            break
          default:
            break
        }

      if (this.checkSend(false)) {
          const sendParams = {
              'appid': this.appid,
              'apppwd': this.apppwd,
              'browseContentLog': this.browseContentLog,
              'browsePageLog': this.browsePageLog,
              'commonEventLog': this.commonEventLog
            }
          this.sendAjaxRequest(sendParams)

          this.browseContentLog.splice(0, this.sendCount)
          this.browsePageLog.splice(0, this.sendCount)
          this.commonEventLog.splice(0, this.sendCount)
        }
    }

    /**
     * [pushBrowseContentList description]Upload content data includes a list, details and search
     * @param  {[type]} contentObj [description] Which actionType must pass
     * @return {[type]}            [description]
     */
    // tslint:disable-next-line:cyclomatic-complexity
  private pushBrowseContent (contentObj) {
      const actionType = contentObj.actionType
      const contentCode = contentObj.contentCode ? contentObj.contentCode : false
      const businessType = contentObj.businessType ? contentObj.businessType : false
      const keyword = contentObj.keyword ? contentObj.keyword : false
      const genre = contentObj.genre ? contentObj.genre : false
      const castID = contentObj.castID ? contentObj.castID : false
      const source = contentObj.source ? contentObj.source : false
      const timestamp = contentObj.timestamp ? contentObj.timestamp : false
        // Interface enhancement============================================================
      const entrance = contentObj.entrance || '',
            // Recommend users select the contents of the corresponding serial number,
            // The user to view the details, entrance to search content, the corresponding serial number search
          recmActionID = contentObj.recmActionID || '',
          number = contentObj.number || '',
            // Users browse the list of additional filtering conditions when the operation type is browseContentList
            // And through the field year, productZone, genre, castID filter, the corresponding field will pass
          year = contentObj.year || '',
          productZone = contentObj.productZone || '',
            // The type of operation, user access to the value recommended results include: 0: watch, 1; order, 2: collection, 3: remind
          recmActionType = contentObj.recmActionType || '',
            // When the operation type actionType is writeRecmBehavior users access to the recommended results behavior record
            // The type of user access to the recommended results is valid when ordering
          productID = contentObj.productID || '',
          extensionFields = contentObj.extensionFields || ''
        // =========================================================================
      if (this.checkParmsContent('browseContentLog', contentObj)) {
          const contentParamsArray = [actionType, contentCode,
              businessType, keyword, genre, castID, source, timestamp, entrance, recmActionID, number, year, productZone, recmActionType, productID, extensionFields
            ]
          this.__start('browseContentLog', contentParamsArray)
        }

    }

    /**
     * [pushBrowseContentList description]Browse content list
     * @param  {[type]} contentObj [description]
     * @return {[type]}            [description]
     */
  public pushBrowseContentList (contentObj) {
      contentObj.actionType = 'browseContentList'
      this.pushBrowseContent(contentObj)
    }

    /**
     * [pushBrowseContentDetail description]Browse details
     * @param  {[type]} contentObj [description]
     * @return {[type]}            [description]
     */
  public pushBrowseContentDetail (contentObj) {
      contentObj.actionType = 'viewContentDetail'
      this.pushBrowseContent(contentObj)
    }

    /**
     * [pushBrowseSearchContent description]Keyword search
     * @param  {[type]} contentObj [description]
     * @return {[type]}            [description]
     */
  public pushBrowseSearchContent (contentObj) {
      contentObj.actionType = 'searchContent'
      this.pushBrowseContent(contentObj)
    }

    /**
     * [pushBrowseSearchContent description]User access recommendation behavior record
     * @param  {[type]} contentObj [description]
     * @return {[type]}            [description]
     */
  public pushBrowseRecmBehavior (contentObj) {
      contentObj.actionType = 'writeRecmBehavior'
      this.pushBrowseContent(contentObj)
    }

    /**
     * [pushBrowsePage description]Page Jump event
     * @param  {[type]} pageObj [description]When pageFrom, pageTo must pass
     * @return {[type]}         [description]
     */
  public pushBrowsePage (pageObj) {
      const pageFrom = pageObj.pageFrom
      const pageDiv = pageObj.pageDiv ? pageObj.pageDiv : false
      const pageTo = pageObj.pageTo
      const stayTime = pageObj.stayTime ? pageObj.stayTime : false
      const timestamp = pageObj.timestamp ? pageObj.timestamp : ''
      const extensionFields = pageObj.extensionFields || ''
      if (this.checkParmsContent('browsePageLog', pageObj)) {
          const pageParamsArray = [pageFrom, pageDiv, pageTo, stayTime, timestamp, extensionFields]
          this.__start('browsePageLog', pageParamsArray)
        }
    }

    /**
     * [pushBrowsePage description]Page Jump event
     * @param  {[type]} pageObj [description] When pageFrom, pageTo must pass
     * @return {[type]}         [description]
     */
    // TODO provide ability only
  public userClickLog (pageObj) {
      const EpgPageName = pageObj.EpgPageName || false,
          EpgPagePath = pageObj.EpgPagePath || false,
          OperType = pageObj.OperType || false,
          OperObjectCode = pageObj.OperObjectCode || false,
          LinkInTime = pageObj.LinkInTime || false,
          LinkOutTime = pageObj.LinkOutTime || false,
          extensionFields = pageObj.extensionFields || false
      if (this.checkParmsContent('userClickLog', pageObj)) {
          const pageParamsArray = [EpgPageName, EpgPagePath, OperType, OperObjectCode, LinkInTime, LinkOutTime, extensionFields]
          this.__start('userClickLog', pageParamsArray)
        }
    }

    /**
     * [pushCommonEvent description]Extended general event
     * @param  {[type]} commonObj [description] Which actionType must pass
     * @return {[type]}           [description]
     */
  public pushCommonEvent (commonObj) {
      const actionType = commonObj.actionType,
          label = commonObj.label ? commonObj.label : false,
          timestamp = commonObj.timestamp ? commonObj.timestamp : false
      if (this.checkParmsContent('commonEventLog', commonObj)) {
          const commonParamsArray = [actionType, label, timestamp]
          this.__start('commonEventLog', commonParamsArray)
        }
    }

    /**
     * [setTrackerServerUrl set user browsing path collection server address]
     * @param {[type]} url [description]
     */
  public setTrackerServerUrl (url) {
        // The Url returned by the emulator authentication interface includes the address
      this.actionUrl = url
    }

    // Initial trajectory acquisition parameters
  public setTrackerParams (params) {
        // Set user browsing path collection server address
      this.setTrackerServerUrl(params.actionUrl)
        // Subscriber ID
      this.subscriberID = params.subscriberID || ''
        // Terminal physical device ID
      this.deviceID = params.deviceID || ''
        // EPG page template number
      this.epgTemplateID = params.epgTemplateID || ''
        // EPG page application unique identifier
      this.appid = params.appid || ''
        // EPG page application access authentication password
      this.apppwd = params.apppwd || ''
        // whether Log switch
      this.needSend = params.needSend || false
        // Terminal physical device type
      this.deviceType = params.deviceType || ''
        // Optional, default cumulative 5 send
      this.sendCount = params.sendCount || 5
        // Optional, default 10 seconds to send
      this.sendTime = params.sendTime || 10000
    }

  public setStatus (status) {
      this.userLogCollectStatus = (status === undefined) ? true : (status + '' === '1')
    }
}

export const pageTracker = new PageTrackerClass()
