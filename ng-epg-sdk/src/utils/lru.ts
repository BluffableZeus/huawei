
export interface LRUOptions {
    /**
     * maxElementsToStore
     */
  max: number

    /**
     * maxAgeInMilliseconds
     */
  maxAge?: number
}

export class LRU<T> {

    /**
     * The number of keys currently in the cache.
     */
  public length: number

  private cache: {}
  private head
  private tail
  private max: number
  private maxAge: number

  constructor (opts: LRUOptions) {
      this.cache = {}
      this.head = null
      this.tail = null
      this.length = 0
      this.max = opts.max || 1000
      this.maxAge = opts.maxAge || 0
    }

    /**
     * Clear the cache. This method does NOT emit the evict event.
     */
  clear () {
      this.cache = {}
      this.head = this.tail = null
      this.length = 0
    }

    /**
     * Remove the value from the cache.
     */
  remove (key: string) {
      if (!this.cache.hasOwnProperty(key)) {
          return
        }

      const element = this.cache[key]
      delete this.cache[key]
      this.unlink(key, element.prev, element.next)
      return element.value
    }

    /**
     * Query the value of the key without marking the key as most recently used.
     */
  peek (key: string): T {
      if (!this.cache.hasOwnProperty(key)) {
          return
        }

      let element = this.cache[key]

      if (!this.checkAge(key, element)) {
          return
        }
      return element.value
    }

    /**
     * Set the value of the key and mark the key as most recently used.
     */
  set (key: string, value: T): T {
      let element

      if (this.cache.hasOwnProperty(key)) {
          element = this.cache[key]
          element.value = value
          if (this.maxAge) {
              element.modified = Date.now()
            }

            // If it's already the head, there's nothing more to do:
          if (key === this.head) {
              return value
            }
          this.unlink(key, element.prev, element.next)
        } else {
          element = {
              value: value, modified: 0, next: null, prev: null
            }
          if (this.maxAge) {
              element.modified = Date.now()
            }

          this.cache[key] = element

            // Eviction is only possible if the key didn't already exist:
          if (this.length === this.max) {
              this.evict()
            }
        }

      this.length++
      element.next = null
      element.prev = this.head

      if (this.head) {
          this.cache[this.head].next = key
        }

      this.head = key

      if (!this.tail) {
          this.tail = key
        }
      return value
    }

    /**
     * Query the value of the key and mark the key as most recently used.
     */
  get (key: string): T {

      if (!this.cache.hasOwnProperty(key)) {
          return
        }

      const element = this.cache[key]

      if (!this.checkAge(key, element)) {
          return
        }

      if (this.head !== key) {
          if (key === this.tail) {
              this.tail = element.next
              this.cache[this.tail].prev = null
            } else {
                // Set prev.next -> element.next:
              this.cache[element.prev].next = element.next
            }

            // Set element.next.prev -> element.prev:
          this.cache[element.next].prev = element.prev

            // Element is the new head
          this.cache[this.head].next = key
          element.prev = this.head
          element.next = null
          this.head = key
        }

      return element.value
    }

    /**
     * Array of all the keys currently in the cache.
     */
  public keys (): Array<string> {
      return Object.keys(this.cache) || []
    }

  private checkAge (key, element) {
      if (this.maxAge && (Date.now() - element.modified) > this.maxAge) {
          this.remove(key)
          return false
        }
      return true
    }

  private evict () {
      if (!this.tail) {
          return
        }
      this.remove(this.tail)
    }

  private unlink (key, prev, next) {
      this.length--

      if (this.length === 0) {
          this.head = this.tail = null
        } else {
          if (this.head === key) {
              this.head = prev
              this.cache[this.head].next = null
            } else if (this.tail === key) {
              this.tail = next
              this.cache[this.tail].prev = null
            } else {
              this.cache[prev].next = next
              this.cache[next].prev = prev
            }
        }
    }
}
