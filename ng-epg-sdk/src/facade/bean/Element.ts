export interface Element {
    /**
     * The cell element is uniquely numbered
     */
  id: string
    /**
     * Whether the default data is forcibly used
     */
  forceDefaultData?: string
    /**
     * ID of the owning group
     */
  groupID?: string
    /**
     * Cell height
     */
  height: string
    /**
     * The coordinates of the upper left corner of the cell
     */
  left: string
    /**
    * Cell height
    */
  top: string
    /**
    * Cell width
    */
  width: string
    /**
   * Single item definition of content - related data
   */
  elementData: any
}
