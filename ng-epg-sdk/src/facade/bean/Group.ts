import { Element } from './Element'
import { Dialect } from './Dialect'

export interface Group {
    /**
     * List of page resource bits
     */
  elements: Element[]
    /**
     * Group number
     */
  id?: string
    /**
     * Group name
     */
  name: Dialect[]
    /**
     * data type
     */
  type?: string
    /**
     * The column number is valid when type is 1, 2, 3, 4, 5.
     */
  categoryCode?: string

  left: string

}
