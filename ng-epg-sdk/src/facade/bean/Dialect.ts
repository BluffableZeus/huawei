export interface Dialect {
    /**
	 * Language abbreviation, language type, see ISO-639 definition, for example: zh: english, en: english
	 */
  language: string
    /**
	 * Corresponding language information
	 */
  value: string
}
