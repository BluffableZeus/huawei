import { Dialect } from './Dialect'

export interface NavData {
     /**
      * Page unique number
      */
  id: string
     /**
      * Navigation name
      */
  name: Dialect[]
     /**
      * The navigation element normally displays the picture
      */
  focusImg?: string
     /**
      * The navigation element highlights the picture
      */
  img?: string
}
