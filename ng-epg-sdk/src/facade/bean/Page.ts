import { Group } from './Group'

export interface Page {
    /**
     * List of page resource bits
     */
  groups: Group[]
    /**
     * Page unique number
     */
  id: string
    /**
     * Page name
     */
  name: string

}
