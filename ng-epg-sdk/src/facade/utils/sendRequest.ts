import { Config } from '../../core/config'
import { ajax } from '../../utils'

/**
 *
 * Node Server requests the sending method
 *
 * @export
 * @param {string} url
 * @param {*} [req={}]
 * @param {*} [options={}]
 * @returns
 */
export function sendRequest (url: string, req: any = {}, options: any = {}) {

  let prefix: string
  if (Config.get('enableHttps', true)) {
      prefix = Config.facadeHttpsUrl
    } else {
      prefix = Config.facadeUrl
    }

  return ajax((prefix || '') + url, req, options)
}
