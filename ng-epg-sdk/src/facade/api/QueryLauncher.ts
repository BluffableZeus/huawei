import { sendRequest } from '../utils/sendRequest'
import { NamedParameter } from '../../vsp/api'
import { Result } from '../../vsp/api'
import { Page } from '../bean/Page'
import { NavData } from '../bean/NavData'
/**
 *  When the user exits the client (logout or shutdown)
 */
export function queryLauncher (req: QueryLauncherRequest): Promise<QueryLauncherResponse> {
  return sendRequest('/VSP/Node/QueryLauncher', req)
}

export interface QueryLauncherRequest {
    /**
	* The local desktop version number (corresponding to the update time that the Launcher was last downloaded from the platform),
	* because it may have no time when it be acquired at the first time,it can be empty.
	*/
  version?: string

    /**
    * Terminal model
    **/
  deviceModel: string

    /**
     * User Token
     */
  userToken: string

    /**
     * EPG Requests the HTTP port number of the PHS resource.
     */
  httpPort: string
    /**
     * Extended Information
     */
  extensionFields?: NamedParameter[]

}

export interface QueryLauncherResponse {
    /**
     * the result be returned
     */
  result: Result

  pages?: Array<Page>

  navs?: Array<NavData>

    /**
     * Desktop description file download link (that is, the template file access address http: // VSP address / VSP / desktop description directory,
     * Launcher stitching in the back of the description file), when the return code is 1,this parameter is required.
     * his parameter is required when successful match.
     */
  launcherLink?: string
    /**
     * The last time the current desktop is modified. This parameter is required when successful match
     */
  version?: string
    /**
     * Match the ID of the desktop after the subsequent heartbeat check the latest data on the desktop.
     * This parameter is required when successful match.
     */
  desktopID?: string
    /**
     * Heartbeat time interval in seconds.
     */
  interval?: number
    /**
     * There are two types of formats: 1. Return Absolute Address: http: // VSP IP: VSP Port / VSP / V3 / BatchGetResStrategyData.
     * 2. Return the relative address: / VSP / V3 / BatchGetResStrategyData, the desktop needs to be spliced IP / Port prefix, get the absolute address.
     */
  batchGetResStrategyDataURL?: number
    /**
     * Return to the desktop the URL format for the "Refresh Data Change Resource Bits ID List",
     * such as: http: // VSP IP: VSP Port / VSP / V3 / GetLatestResources
     */
  getLatestResourcesURL?: number
    /**
     * Extended Information
     */
  extensionFields?: NamedParameter[]
}
