import { Config } from './../../core'
import { STBPlayer } from './stb-player'
import * as _ from 'underscore'
import { ChannelDetail } from '../../vsp/api'

import { Logger } from '../../utils/logger'
const log = new Logger('mini-player')

/**
 * PIP play class
 *
 * @export
 * @class PipPlayer
 */
export class MiniPlayer {
  private player: STBPlayer
  private settings
  constructor (settings?: any, name?: any) {
      const convertSize = Config.get('convertSize', 1)
      const defaultConfig = {
          videoDisplayMode: 0,
          subtitleFlag: 0,
          width: 272 * convertSize,
          height: 160 * convertSize,
          left: 0,
          top: 0
        }
      if ('IPTV') {
          this.settings = _.extend({}, defaultConfig, settings, { name: name })
        }
      if (this.player) {
          this.player.stop()
        }
      this.player = new STBPlayer(this.settings)
    }

    /**
     * Play
     *
     * @param {ChannelDetail} chan
     *
     * @memberOf PipPlayer
     */
  public joinChannel (chan: ChannelDetail) {
      let mediaObj = {
          mediaURL: chan.channelNO + '',
          mediaCode: chan.ID + '',
          mediaType: 1,
          streamType: 2
        }
      const joinChannelOption = JSON.stringify(mediaObj)

      log.debug('PipPlayer joinChannel::%s ', joinChannelOption)
      this.player.setSingleMedia(joinChannelOption)
      this.player.playFromStart()
    }

  public joinChannelMosaic (chan: {
      meidaID: string,
      id: string,
      channelNO: string
    }) {
      let mediaObj = {
          mediaURL: Config.get('jspLogined') ? '' + chan.channelNO : 'mediaId://' + chan.meidaID,
          mediaCode: chan.id + '',
          mediaType: 1,
          streamType: 2,
          contentID: chan.id + ''
        }
      const joinChannelOption = JSON.stringify(mediaObj)

      log.debug('PipPlayer joinChannel::%s ', joinChannelOption)
      this.player.setSingleMedia(joinChannelOption)
      this.player.playFromStart()
    }

  public getVolume (): number {
      if (this.player) {
          return +this.player.getVolume()
        } else {
          return 0
        }
    }

  public play (video, auth, mediaType, timestamp, isVRContent = false, contentID?) {
      let mediaStrObj = {
          authData: auth,
          'mediaURL': auth.playURL,
          'mediaCode': contentID || video.ID || video.id || video.taskID,
            // 2：VOD 3：TVOD 10：PVR
          'mediaType': mediaType || 2,
          'allowTrickmode': 1,
          'contentID': contentID || video.ID
        }
      let mediaStr = JSON.stringify(mediaStrObj)

      if (!this.player) {
          this.player = new STBPlayer(this.settings)
        }

      this.player.setSingleMedia(mediaStr)

      if (isVRContent) {
          this.player.setVRFlag(1)
        } else {
          this.player.setVRFlag(0)
        }

      if (timestamp) {
          this.playByTime(timestamp)
        } else {
          this.player.playFromStart()
        }
    }

    /**
     * stop
     *
     *
     * @memberOf PipPlayer
     */
  public stop () {
      log.debug('PipPlayer stop: ')
      this.player.stop()
      this.player.releasePlayer()
    }

    /**
     * Bookmarks play
     *
     * @param {any} time
     *
     * @memberOf PipPlayer
     */
  public playByTime (time) {
      this.player.playByTime(1, time)
    }

  public getCurrentPlayTime () {
      return this.player.getCurrentPlayTime()
    }

    /**
     * current time
     *
     * @returns
     *
     * @memberOf PipPlayer
     */
  public getCurrentChannelTime () {
      return new Date()
    }

    /**
     * Mute
     *
     *
     * @memberOf PipPlayer
     */
  public mute () {
      this.player.setMuteFlag(1)
    }

    /**
     * Unmute
     *
     *
     * @memberOf PipPlayer
     */
  public unMute () {
      this.player.setMuteFlag(0)
    }

    /**
     * Whether it is muted
     *
     * @returns
     *
     * @memberOf PipPlayer
     */
  public isMute () {
      return this.player.getMuteFlag() === 1 || this.player.getVolume() === 0
    }

    /**
     * Instance ID
     *
     * @returns
     *
     * @memberOf PipPlayer
     */
  public getInstanceId () {
      return this.player.instanceId
    }

  public refreshVideoDisplay () {
      this.player.refreshVideoDisplay()
    }

  public setVideoDisplayArea (left, top, width, height) {
      this.player.setVideoDisplayArea(left, top, width, height)
    }

  public enableSQMMonitor () {
      this.player.enableSQMMonitor()
    }

  public disableSQMMonitor () {
      this.player.disableSQMMonitor()
    }

  public setVolume (volume: number) {
      if (this.player) {
          this.player.setVolume(volume)
        }
    }
    /**
    * set the MediaPlayer object corresponding to the video window zoom mode
    * @param videoZoomMode {Number} The value is of integer type
    */
  public setVideoZoomMode (videoZoomMode) {
      this.player.setVideoZoomMode(videoZoomMode)
    }

  public setCapability (capString) {
      this.player.setCapability(capString)
    }
}
