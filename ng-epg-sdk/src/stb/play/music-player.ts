import { STBPlayer } from './stb-player'

let player: STBPlayer

export class MusicPlayer {
  constructor (settings?: any) {
      player = new STBPlayer(settings)
    }

    /**
         *
         * play vod
         * @method play
         * @param video {JSON} video information, {id:"media id",playUrl:"play url"}
         * @param mediaType {Number} media type
         *      <ul>
         *      <li>2:vod</li>
         *      <li>3:tvod</li>
         *      <li>10:pvr</li>
         *      </ul>
         *  @param time {Number} time to start play, unit:second
         */
  public play (video, auth, mediaType, timestamp, contentID?) {
      const mediaStrObj = {
            authData: auth,
            'mediaURL': auth.playURL,
            'mediaCode': contentID || video.ID || video.id || video.taskID,
            'mediaType': mediaType || 2,
            'allowTrickmode': 1,
            'contentID': contentID || video.ID
          },
          mediaStr = JSON.stringify(mediaStrObj)
      player.setSingleMedia(mediaStr)
      if (timestamp) {
          this.playByTime(timestamp)
        } else {
          this.playFromStart()
        }
    }

    /**
     * Release the player
     *
     *
     * @memberOf VodPlayer
     */
  public releasePlayer () {
      player.releasePlayer()
    }

    /**
     * play
     *
     *
     * @memberOf VodPlayer
     */
  public playFromStart () {
      player.playFromStart()
    }

    /**
     * Bookmarks play
     *
     * @param {number} timestamp
     *
     * @memberOf VodPlayer
     */
  public playByTime (timestamp: number) {
      player.playByTime(1, timestamp + '')
    }

    /**
     * pause
     *
     *
     * @memberOf VodPlayer
     */
  public pause () {
      player.pause()
    }

    /**
     * Resume playing
     *
     *
     * @memberOf VodPlayer
     */
  public resume () {
      player.resume()
    }

    /**
     * stop
     *
     *
     * @memberOf VodPlayer
     */
  public stop () {
      player.stop()
    }

    /**
     * Stop and release the player
     *
     *
     * @memberOf VodPlayer
     */
  public stopAndRelease () {
      player.stop()
      player.releasePlayer()
    }

    /**
     * The current playing time
     *
     * @returns
     *
     * @memberOf VodPlayer
     */
  public getCurrentPlayTime () {
      return player.getCurrentPlayTime()
    }

    /**
     * Video duration
     *
     * @returns
     *
     * @memberOf VodPlayer
     */
  public getMediaDuration () {
      return player.getMediaDuration()
    }

    /**
     * Fast forward
     *
     *
     * @memberOf VodPlayer
     */
  public fastForward () {
      player.fastForward()
    }

    /**
     * Has been retired
     *
     *
     * @memberOf VodPlayer
     */
  public fastRewind () {
      player.fastRewind()
    }

    /**
     * Fast forward or rewind
     *
     * @param {any} fastSpeed
     *
     * @memberOf VodPlayer
     */
  public fastForwardOrRewind (fastSpeed) {
      player.fastForwardOrRewind(fastSpeed)
    }

    /**
     * The EPG calls this method to jump to the media starting point to start playing.
     *
     *
     * @memberOf VodPlayer
     */
  public gotoStart () {
      const currentTime = this.getCurrentPlayTime()
      if (currentTime && currentTime > 5) {
          this.playByTime(0)
        }
    }

    /**
     * The EPG calls this method to jump to the end of the media.
     *
     *
     * @memberOf VodPlayer
     */
  public gotoEnd () {
      player.gotoEnd()
    }

    /**
     * The EPG calls this method to get the current play mode of the local player instance corresponding to the MediaPlayer object.
     *
     * @returns
     *
     * @memberOf VodPlayer
     */
  public getPlaybackMode () {
      return player.getPlaybackMode()
    }

    /**
     * Mute
     *
     *
     * @memberOf VodPlayer
     */
  public mute () {
      player.setMuteFlag(1)
    }

    /**
     * Unmute
     *
     *
     * @memberOf VodPlayer
     */
  public unMute () {
      player.setMuteFlag(0)
    }

    /**
     *
     *
     * @returns
     *
     * @memberOf VodPlayer
     */
  public isMute () {
      return player.getMuteFlag() === 1 || player.getVolume() === 0
    }

    /**
     * Get volume
     *
     * @returns
     *
     * @memberOf VodPlayer
     */
  public getVolume () {
      return player.getVolume()
    }

    /**
     * Set the volume
     *
     * @param {any} val
     *
     * @memberOf VodPlayer
     */
  public setVolume (val) {
      player.setVolume(val)
    }

    /**
     * Get subtitles
     *
     * @returns
     *
     * @memberOf VodPlayer
     */
  public getSubtitles () {
      return player.getSubtitles()
    }

    /**
     * Get the current subtitle
     */
  public getCurrentSubtitle () {
      return player.getCurrentSubtitle()
    }

    /**
     * Set subtitles
     *
     * @param {any} subtitle
     *
     * @memberOf VodPlayer
     */
  public setCurrentSubtitle (subtitle) {
      player.setCurrentSubtitle(subtitle)
    }

    /**
     * Get all tracks
     *
     * @returns
     *
     * @memberOf VodPlayer
      */
  public getTracks () {
      return player.getTracks()
    }

    /**
     * Get the current track
     *
     * @returns
     *
     * @memberOf VodPlayer
     */
  public getCurrentTrack () {
      return player.getCurrentTrack()
    }
    /*
     * Switch to the next track
     */
  public switchAudioTrack () {
      player.switchAudioTrack()
    }

    /**
     * Set the track
     *
     * @param {any} track
     *
     * @memberOf VodPlayer
     */
  public setCurrentTrack (track) {
      player.setCurrentTrack(track)
    }

    /**
     * Set the default track
     *
     * @param {any} track
     *
     * @memberOf VodPlayer
     */
  public setCurrenttDefaultTrack (track) {
      player.setCurrenttDefaultTrack(track)
    }

    /**
     *
     *
     * @param {any} subtitleFlag
     *
     * @memberOf VodPlayer
     */
  public setSubtitleFlag (subtitleFlag) {
      player.setSubtitleFlag(subtitleFlag)
    }

    /**
     * Get subtitle logo
     *
     * @returns
     *
     * @memberOf VodPlayer
     */
  public getSubtitleFlag () {
      return player.getSubtitleFlag()
    }

    /**
     * Get the play instance ID
     *
     * @returns
     *
     * @memberOf VodPlayer
     */
  public getInstanceId () {
      return player.instanceId
    }

    /**
     * Set the VR rotation direction and angle
     *
     * @param {any} direction
     * @param {any} degree
     *
     * @memberOf VodPlayer
     */
  public setVRRotate (direction, degree, flag) {
      if (player.setVRRotate) {
          player.setVRRotate(direction, degree, flag)
        }
    }
    /**
     * monocular play
     * @method setVRFlag
     * @param vrFlag {int}: 0:not vr; 1: sphere vr ; 2:cylinder vr ; 3: cube vr;
     */
  public setVRFlag (vrFlag) {
      if (player.setVRFlag) {
          player.setVRFlag(vrFlag)
        }
    }

    /**
     * Turns on or off the output of the current player, including video, audio, and subtitles.
     * Call the interface after the player is still normal playback,
     * but not the video images, audio and subtitles output to the TV, the screen appears as a black screen.
     *
     * @param {any} outputFlag
     *
     * @memberOf VodPlayer
     */
  public setVideoOutput (outputFlag) {
      if (player.setVideoOutput) {
          player.setVideoOutput(outputFlag)
        }
    }
}
