import { Config } from './../../core'
import * as _ from 'underscore'
import { Logger } from '../../utils/logger'

declare const MediaPlayer

const log = new Logger('stb-player.ts')
const FORWARD_SPEED_ARRAY = [2, 4, 8, 16, 32]

const REWIND_SPEED_ARRAY = [-2, -4, -8, -16, -32]

export class STBPlayer {
  settings: any
  _mediaPlayer: any
  instanceId

  constructor (settings: any) {
      const convertSize = Config.get('convertSize', 1)
      const defaultSettings = {
          playListFlag: 0,
          videoDisplayMode: 2,
          width: 1280 * convertSize,
          height: 720 * convertSize,
          left: 0,
          top: 0,
          muteFlag: 0,
          subtitleFlag: 1,
          videoAlpha: 0,
          cycleFlag: 0,
          randomFlag: 0,
          autoDelFlag: 0,
          useNativeUIFlag: 0
        }
      this.settings = _.extend({}, defaultSettings, settings)
    }

    /**
     * Player instance
     *
     * @readonly
     *
     * @memberOf STBPlayer
     */
  get mediaPlayer () {

      if (!this._mediaPlayer) {
          this.initPlayer()
        }
      return this._mediaPlayer
    }

    /**
     * Initialize the player
     *
     *
     * @memberOf STBPlayer
      */
  public initPlayer () {
      log.debug('[MediaPlayer]STBPlayer instanceId(%s):initPlayer(), new MediaPlayer(%s)', this.instanceId, this.settings.name)
      this._mediaPlayer = new MediaPlayer(this.settings.name)
      this.instanceId = this._mediaPlayer.getNativePlayerInstanceID()
      this._mediaPlayer.initMediaPlayer(
            this.instanceId,
            this.settings.playListFlag,
            this.settings.videoDisplayMode,
            this.settings.height,
            this.settings.width,
            this.settings.left,
            this.settings.top,
            this.settings.muteFlag,
            this.settings.useNativeUIFlag,
            this.settings.subtitleFlag,
            this.settings.videoAlpha,
            this.settings.cycleFlag,
            this.settings.randomFlag,
            this.settings.autoDelFlag)
    }

    /**
     * Play channel
     *
     * @param {any} chanId
     *
     * @memberOf STBPlayer
     */
  public joinChannel (chanId) {
      log.debug('[MediaPlayer]STBPlayer instanceId(%s): joinChannel(), MediaPlayer.joinChannel(%s)', this.instanceId, chanId)
      this.mediaPlayer.joinChannel(chanId)
    }

    /**
     *  Release the player
     *
     *
     * @memberOf STBPlayer
     */
  public releasePlayer () {
      log.debug('[MediaPlayer]STBPlayer instanceId(%s): releasePlayer()', this.instanceId)
      if (this._mediaPlayer) {
          log.debug('[MediaPlayer]STBPlayer instanceId(%s): releasePlayer(), MediaPlayer.releaseMediaPlayer(%s)', this.instanceId, this.instanceId)
          this._mediaPlayer.releaseMediaPlayer(this.instanceId)
          this._mediaPlayer = null
        }

    }

    /**
     * Stops the channel being played. When the channel being played by the main player is stopped,
     * the screen becomes blank. When the channel being played by the PIP or mosaic player is stopped,
     * the video window of the player becomes blank and is not closed.
     * @method stop
     */
  public stop () {
      log.debug('[MediaPlayer]STBPlayer instanceId(%s): stop(), MediaPlayer.stop()', this.instanceId)
      this.mediaPlayer.stop()
    }

    /**
     * Pauses a media program.
     * @method pause
     */
  public pause () {
      log.debug('[MediaPlayer]STBPlayer instanceId(%s): pause(), MediaPlayer.pause()', this.instanceId)
      this.mediaPlayer.pause()
    }
    /**
     * Resumes the playback of a media program that is being paused, fast forwarded, or rewound.
     * @method resume
     */
  public resume () {
      log.debug('[MediaPlayer]STBPlayer instanceId(%s): resume(), MediaPlayer.resume()', this.instanceId)
      this.mediaPlayer.resume()
    }

    /**
     * Switches to the start point of a media program to play.
     * @mthod gotoStart
     */
  public gotoStart () {
      log.debug('[MediaPlayer]STBPlayer instanceId(%s): gotoStart(), MediaPlayer.gotoStart()', this.instanceId)
      this.mediaPlayer.gotoStart()
    }
    /**
     * Plays the end of a media program. When this method is invoked for an online or local TSTV program,
     *  the program is played as a live program.
     * When this method is invoked for a unicast program, the last 5 seconds of the program are played.
     * If the remaining of the unicast program lasts less than 5 seconds, the playback is not changed.
     *@method gotoEnd
     */
  public gotoEnd () {
      log.debug('[MediaPlayer]STBPlayer instanceId(%s): gotoEnd(), MediaPlayer.gotoEnd()', this.instanceId)
      this.mediaPlayer.gotoEnd()
    }
    /**
     * Fast forwards a media program. if the speed is maximun, then back to normal speed.
     * @method fastForward
     */
  public fastForward (isRoundbin?: boolean) {
      log.debug('[MediaPlayer]STBPlayer instanceId(%s): fastForward(%s)', this.instanceId, isRoundbin)
      let mode = this.getPlaybackMode(),
          index
      if (mode.Speed) {
          index = FORWARD_SPEED_ARRAY.indexOf(+mode.Speed.slice(0, -1))
          if (isRoundbin) {
              index = (++index + FORWARD_SPEED_ARRAY.length) % FORWARD_SPEED_ARRAY.length
            } else {
              index = index < (FORWARD_SPEED_ARRAY.length - 1) ? (index + 1) : (FORWARD_SPEED_ARRAY.length - 1)
            }
          log.debug('[MediaPlayer]STBPlayer instanceId(%s): fastForward(), MediaPlayer.fastForward(%s)', this.instanceId, FORWARD_SPEED_ARRAY[index])
          this.mediaPlayer.fastForward(FORWARD_SPEED_ARRAY[index])
        } else {
          log.debug('[MediaPlayer]STBPlayer instanceId(%s): fastForward(), MediaPlayer.fastForward(%s)', this.instanceId, FORWARD_SPEED_ARRAY[0])
          this.mediaPlayer.fastForward(FORWARD_SPEED_ARRAY[0])
        }
    }
    /**
     * Rewinds a media program. if the speed is maximun, then back to normal speed.
     * @method fastRewind
     */
  public fastRewind (isRoundbin?: boolean) {
      log.debug('[MediaPlayer]STBPlayer instanceId(%s): fastRewind(%s)', this.instanceId, isRoundbin)
      let mode = this.getPlaybackMode(),
          index

      if (mode.Speed) {
          index = REWIND_SPEED_ARRAY.indexOf(+mode.Speed.slice(0, -1))
          if (isRoundbin) {
              index = (++index + REWIND_SPEED_ARRAY.length) % REWIND_SPEED_ARRAY.length
            } else {
              index = index < (REWIND_SPEED_ARRAY.length - 1) ? (index + 1) : (REWIND_SPEED_ARRAY.length - 1)
            }
          log.debug('[MediaPlayer]STBPlayer instanceId(%s): fastRewind(), MediaPlayer.fastRewind(%s)', this.instanceId, REWIND_SPEED_ARRAY[index])
          this.mediaPlayer.fastRewind(REWIND_SPEED_ARRAY[index])
        } else {
          log.debug('[MediaPlayer]STBPlayer instanceId(%s): fastRewind(), MediaPlayer.fastRewind(%s)', this.instanceId, REWIND_SPEED_ARRAY[0])
          this.mediaPlayer.fastRewind(REWIND_SPEED_ARRAY[0])
        }
    }
    /**
     * Fast forwards a media program. if the speed is maximun, then back to normal speed.
     * OR
     * Rewinds a media program. if the speed is maximun, then back to normal speed.
     * @method fastSpeed
     */
  public fastForwardOrRewind (fastSpeed) {
      log.debug('[MediaPlayer]STBPlayer instanceId(%s): fastForwardOrRewind(%s)', this.instanceId, fastSpeed)
      if (fastSpeed > 0) {
          log.debug('[MediaPlayer]STBPlayer instanceId(%s): fastForwardOrRewind(), MediaPlayer.fastForward(%s)', this.instanceId, fastSpeed)
          this.mediaPlayer.fastForward(fastSpeed)
        } else {
          log.debug('[MediaPlayer]STBPlayer instanceId(%s): fastForwardOrRewind(), MediaPlayer.fastRewind(%s)', this.instanceId, fastSpeed)
          this.mediaPlayer.fastRewind(fastSpeed)
        }

    }
    /**
     *
     * Plays a media program that is not a channel program from the beginning of the program.<br/>
     * Plays a live, PIP, or mosaic channel from the current time.<br/><br/>
     * <em>Note: </em>If the EPG of an STB uses the playFromStart method instead of the stop method to switch to a live, PIP, or mosaic channel,
     * the STB switches to the channel based on the current channel switch mode.
     * @method playFromStart
     */
  public playFromStart () {
      log.debug('[MediaPlayer]STBPlayer instanceId(%s): playFromStart(), MediaPlayer.playFromStart()', this.instanceId)
      this.mediaPlayer.playFromStart()
    }
    /**
     * Plays a media program from a specific time point.
     * <br/><em>Note: </em>Plays a media program from a specific time point.
     * @method playByTime
     * @param type {Number} indicates a time format. The value is of integer type. This parameter is mandatory.
     * <ul>
     * <li>1: NPT<br/>
     * If the media program that is playing is a VOD program, Catch-up TV program, local file, or DLNA file, the NPT-sec time format is used.
     * A time in this format is accurate to second.</li>
     * <li>2: absolute time (clock time)<br/>
     * If the media program that is playing is a network or local TSTV, the UTC time format is used. For details, see section 3.1.1 Absolute Time or RFC2326.</li>
     * </ul>
     * @param timestamp {Number} indicates the time to start playback. The value is of string type. This parameter is mandatory.<br/>
     * If the value of type is 1, the value is the time counted from the start point of the program.<br/>
     * If the value of type is 2, the value is the UTC absolute time when the program starts playing.
     * @param speed {Number} indicates the playback speed. This parameter is optional.
     * @example
     * <pre>
     *To have the STB play a VOD program from the second minute:
     *  <em>playByTime(1,"120")</em>
     *To have the STB play a network TSTV program from 14:37:20.25 on November 8, 2011:
     *  <em>playByTime(1,"20111108T143720.25Z")</em>
     *</pre>
     */
  public playByTime (type, timestamp, speed?) {
      log.debug('[MediaPlayer]STBPlayer instanceId(%s): playByTime(), MediaPlayer.playByTime(%s, %s, %s)', this.instanceId, type, timestamp, speed)
      this.mediaPlayer.playByTime(type, timestamp, speed)
    }
    /**
     * Sets the sound status (mute or unmute) for a MediaPlayer object instance.
     * The setting does not affect the sound status of other STB player instances. The setting takes effect immediately.
     * @method setMuteFlag
     * @param  muteFlag {Number} mute flag
     * <ul>
     * <li>0:unmute sounds.</li>
     * <li>1:mute sounds.</li>
     * </ul><br/>
     * <em>Note: </em>The volumes and mute modes of local payers, including the main player, PIP player, and mosaic player, can be changed separately.
     * <br/>The mute modes of local players are initialized based on the default value 0 when an STB is powered on,
     * woken up from the true standby state, or woken from the false standby state.
     * <br/>When a MediaPlayer object is created, the mute mode of the object is initialized by using the default value 0.
     * <br/>If a MediaPlayer object is not released or is woken up from the false standby state, the STB does not change the value of this parameter.
     * <br/>If the sound of a player has been muted, the sound is unmuted when the setVolume is invoked.
     */
  public setMuteFlag (muteFlag) {
      log.debug('[MediaPlayer]STBPlayer instanceId(%s): setMuteFlag(%s), MediaPlayer.playByTime(%s, %s, %s)', this.instanceId, muteFlag)
      let currentMuteFlag = this.getMuteFlag()
      if (currentMuteFlag !== muteFlag) {
          if (muteFlag === 0) {
              let currentVolume = this.getVolume()
              this.setVolume(currentVolume)
            }
          log.debug('[MediaPlayer]STBPlayer instanceId(%s): setMuteFlag(), MediaPlayer.setMuteFlag(%s)', this.instanceId, muteFlag)
          this.mediaPlayer.setMuteFlag(muteFlag)
        }
    }
    /**
     * Obtains the sound status (mute or unmute) for a MediaPlayer object instance.
     * @method getMuteFlag
     * @return {Number} Sound status. The value is of integer type.
     * <ul>
     * <li>0: to unmute sound </li>
     * <li>1: to mute sound</li>
     * </ul>
     */
  public getMuteFlag () {
      log.debug('[MediaPlayer]STBPlayer instanceId(%s): getMuteFlag(), MediaPlayer.getMuteFlag()', this.instanceId)
      return this.mediaPlayer.getMuteFlag()
    }
    /**
     * Sets the volume of the local player instance corresponding to a MediaPlayer object.
     * @method setVolume
     * @param volume {Number} indicates the volume required. This parameter is mandatory.
     * The value is of integer type. The value ranges from 0 to 100, where 0 indicates no volume and 100 indicates the maximum volume.
     * <em>Note: </em>When a MediaPlayer object is created, the volume of the object is set to the default volume. The default volume is site specific.
     */
  public setVolume (volume) {
      log.debug('[MediaPlayer]STBPlayer instanceId(%s): setVolume(%s)', this.instanceId, volume)
      if ((0 <= volume && volume <= 100)) {
          log.debug('[MediaPlayer]STBPlayer instanceId(%s): setVolume(), MediaPlayer.setVolume(volume)', this.instanceId, volume)
          this.mediaPlayer.setVolume(volume)
        }
    }
    /**
     * Obtains the system volume.
     * @return {Number} The value ranges from 0 to 100, where 0 indicates no volume and 100 indicates the maximum volume.
     */
  public getVolume () {
      let volume = this.mediaPlayer.getVolume()
      log.debug('[MediaPlayer]STBPlayer instanceId(%s): getVolume(), MediaPlayer.getVolume() return %s', this.instanceId, volume)
      return volume
    }
    /**
     * Sets the parameters for playing a single media program to prepare for playback startup.
     * @method setSingleMedia
     * @param mediaStr {String} This parameter is encapsulated in JSON. The value is of string type and takes a maximum of 8192 bytes.
     * For the definition of this parameter
     * <br/><em>Note: </em>After the parameters are set, the playback starts only when the playback method is invoked.
     *
     */
  public setSingleMedia (mediaStr) {
      log.debug('[MediaPlayer]STBPlayer instanceId(%s): setSingleMedia(), MediaPlayer.setSingleMedia(%s)', this.instanceId, mediaStr)
      this.mediaPlayer.setSingleMedia(mediaStr)
    }

    /**
     * Sets the Macrovision content protection mode of the local player instance corresponding to a MediaPlayer object.
     * @method setMacrovisionFlag
     * @param macrovisionFlag {Number} The value is of integer type
     * <ul>
     * <li>0: to disable content protection</li>
     * <li>1: Level 1</li>
     * <li>2: Level 2</li>
     * <li>3: Level 3</li>
     * </ul>
     * <em>Note: </em>
     * <ul>
     * <li>The STB Macrovision content protection function is available only when this parameter is set to 1, 2, or 3.</li>
     * <li>If this parameter is not set, an STB uses the default value when the STB plays content. The default value is specified by
     * an interface. For details about the default value, see the IPTV Solution STB and EPG Interface Reference for Content Protection.</li>
     * <li>If there is only one player that is playing content, the video output depends on the content protection mode of the player.</li>
     * <li>If there are multiple players, the priority of the main player is higher than the PIP player. The priority of the PIP
     * player is higher than the priority of the mosaic player. The video output depends on the content protection mode of the player
     *  that has the highest priority. If multiple players have the same priority, the video output depends on the content protection
     * mode of the player that plays content last.</li>
     * </ul>
     */
  public setMacrovisionFlag (macrovisionFlag) {
      log.debug('[MediaPlayer]STBPlayer instanceId(%s): setMacrovisionFlag(), MediaPlayer.setMacrovisionFlag(%s)', this.instanceId, macrovisionFlag)
      this.mediaPlayer.setMacrovisionFlag(macrovisionFlag)
    }

    /**
     *
     * Sets the high-bandwidth digital content protection (HDCP) mode of the local player instance corresponding to a MediaPlayer object.
     * @method setHDCPFlag
     * @param hdcpFlag {Number} The value is of integer type
     * <ul>
     * <li>0: to disable HDCP</li>
     * <li>1: to enable HDCP</li>
     * </ul>
     *
     * <em>Note: </em>
     * <br/>If this parameter is not set, an STB uses the default value when the STB plays content. The default value is specified by an interface.
     * <br/>If there is only one player that is playing content, the video output depends on the content protection mode of the player.
     * <br/>If there are multiple players, the priority of the main player is higher than the PIP player. The priority of the PIP player is
     * higher than the priority of the mosaic player. The video output depends on the content protection mode of the player that has the highest priority.
     * If multiple players have the same priority, the video output depends on the content protection mode of the player that plays content last.
     * @example
     * To enable HDCP and set level 1 protection:<br/>setHDCPFlag(1)
     *
     */
  public setHDCPFlag (hdcpFlag) {
      log.debug('[MediaPlayer]STBPlayer instanceId(%s): setHDCPFlag(), MediaPlayer.setHDCPFlag(%s)', this.instanceId, hdcpFlag)
      this.mediaPlayer.setHDCPFlag(hdcpFlag)
    }

    /**
     * Sets the Copy Generation Management System-Analog (CGMS-A) content protection mode of the local player instance corresponding
     *  to a MediaPlayer object.
     * @method  setCGMSAFlag
     * @param cgmsaFlag {Number}
     * <ul>
     * <li> 0: CopyFreely Unlimited copies can be made of a piece of content.</li>
     *  <li>1: CopyNoMore No copy can be made of a piece of content because the content is a copy. Copies can only be made of the source content.</li>
     *  <li>2: CopyOnce Copies can only be made directly of a piece of content. The copies can be played but cannot be copied.</li>
     *  <li>3: CopyNever A piece of content is the source content and cannot be copied.</li>
     * </ul>
     *
     * <em>Note: </em>
     * <br/>If this parameter is not set, an STB uses the default value when the STB plays content. The default value is specified
     * by an interface. For details about the default value, see the IPTV STB and EPG Interface Reference for Content Protection.
     * <br/>If there is only one player that is playing content, the video output depends on the content protection mode of the player.
     * <br/>If there are multiple players, the priority of the main player is higher than the PIP player. The priority of the PIP player
     * is higher than the priority of the mosaic player. The video output depends on the content protection mode of the player that has
     * the highest priority. If multiple players have the same priority, the video output depends on the content protection mode of the
     * player that plays content last.
     * <br/>Content copying means that an analog interface of the STB outputs signal and a recording device records the signal.
     */
  public setCGMSAFlag (cgmsaFlag) {
      log.debug('[MediaPlayer]STBPlayer instanceId(%s): setCGMSAFlag(), MediaPlayer.setCGMSAFlag(%s)', this.instanceId, cgmsaFlag)
      this.mediaPlayer.setCGMSAFlag(cgmsaFlag)
    }
    /**
     * Detects the current media playback time of the local player instance corresponding to a MediaPlayer object.
     * @method getCurrentPlayTime
     * @return {Number}The value is of the string type.
     * <ul>
     * <li>If a VOD program, Catch-up TV program, or local file is played, the value is the difference between the beginning playback
     * time and the current playback time. The unit is second. The value is displayed in the npt-sec format, where npt indicates normal play time.</li>
     * <li>If an online or local TSTV program is played, the value is the current time in the absolute time (clock time) format. The
     * time is accurate to second. If an STB returns a Coordinated Universal Time (UTC), the EPG converts the time to a local time.</li>
     * <li>If a TV channel program, PIP program, or mosaic program is played, a string whose length is 0 is returned.</li>
     * <li>If the detection fails, a string whose length is 0 is returned.</li>
     * </ul>
     */
  public getCurrentPlayTime () {
      let currentPlayTime = this.mediaPlayer.getCurrentPlayTime()
      log.debug('[MediaPlayer]STBPlayer instanceId(%s): getCurrentPlayTime(), MediaPlayer.getCurrentPlayTime() return %s', this.instanceId, currentPlayTime)
      return currentPlayTime
    }
    /**
     * Detects the current playback mode of the local player instance corresponding to a MediaPlayer object.
     * @method getPlaybackMode
     * @return {String} The value is a string consisting the values of PlayMode and Speed. The value format is {"PlayMode":"PlayMode","Speed":"Speed"}.
     * If the detection fails, a string whose length is 0 is returned.
     */

  public getPlaybackMode () {
      let jsonStr = this.mediaPlayer.getPlaybackMode() + '',
          mode = jsonStr ? JSON.parse(jsonStr) : {
              PlayMode: 'Stop',
              Speed: '0x'
            }
      log.debug('[MediaPlayer]STBPlayer instanceId(%s): getPlaybackMode(), MediaPlayer.getPlaybackMode() return %s', this.instanceId, jsonStr)
      return mode
    }
    /**
     * Detects the duration of the content being played by the local player instance corresponding to a MediaPlayer object.
     * The content can be a Catch-up TV program, VOD program, online TSTV program, local TSTV program, local file, or DLNA file.
     * @method getMediaDuration
     * @return {Number} The value is of integer type. The unit is second.
     * <ul>
     * <li>If the content is a Catch-up TV program, VOD program, local file, or DLNA file, the total duration of the content is returned.</li>
     * <li>If the content is an online or local TSTV program, the duration of the recorded program is returned.</li>
     * <li>If the type of the content is not supported, the value 0 is returned.</li>
     * <li>If the detection fails, the value 0 is returned.</li>
     * </ul>
     * @example
     * If the current content is a VOD program and the total duration of the program is 2 hours, the value 7200 is returned.
     * <br/>If the current content is an online TSTV program, the total duration of the program is 2 hours, and only 1 hour of the program is recorded, the value 3600 is returned.
     */

  public getMediaDuration () {
      let duration = parseInt(this.mediaPlayer.getMediaDuration(), 10)
      log.debug('[MediaPlayer]STBPlayer instanceId(%s): getMediaDuration(), MediaPlayer.getMediaDuration() return %s', this.instanceId, duration)
      return duration
    }
    /**
     * Obtains the code of the media program that is playing in the MediaPlayer object player instance.
     * @method getMediaCode
     * @return {String} <br/>The string ID of the media. The value is of string type, and corresponds to the value of mediaCode in the mediaStr parameter.
     * <br/>If mediaCode is not contained in the mediaStr parameter, the return value is a string whose length is 0.
     * <br/>When the method fails, the return value is a string whose length is 0.
     *
     */

  public getMediaCode () {
      let mediaCode = this.mediaPlayer.getMediaCode()
      log.debug('[MediaPlayer]STBPlayer instanceId(%s): getMediaCode(), MediaPlayer.getMediaCode() return %s', this.instanceId, mediaCode)
      return mediaCode
    }

    /**
     * Obtains the current channel number of the local player instance corresponding to a MediaPlayer object.
     * Supported media types include TV channel program, PIP program, and mosaic program.
     * @method getChannelNum
     * @return {Number} If the current media type is not supported, the value -1 is returned.<br/>
     * If the detection fails, the value -1 is returned.
     */
  public getChannelNum () {
      let channelNum = this.mediaPlayer.getChannelNum()
      log.debug('[MediaPlayer]STBPlayer instanceId(%s): getChannelNum(), MediaPlayer.getChannelNum() return %s', this.instanceId, channelNum)
      return channelNum
    }
    /**
     * Sets the information about whether the player for a MediaPlayer object instance displays subtitles.
     * The setting takes effect immediately. If the default subtitle language is set, the player in the STB displays the corresponding subtitles.
     *  Otherwise, the subtitles matching the first PID are displayed.
     * @method setSubtitleFlag
     * @param subtitleFlag {Number} indicates whether the player displays subtitles. The value is of integer type. This parameter is mandatory.
     * <ul>
     * <li>0 : no</li>
     * <li>1: yes</li>
     * </ul>
     *
     * <em>Note: </em>
     * <ul>
     * <li>The subtitle display modes, which indicate whether subtitles are displayed, of local players are initialized based on the default
     * value 0 when an STB is powered on, woken up from the true standby state, or woken from the false standby state.</li>
     * <li>When a MediaPlayer object is created, the object is initialized by using the default value 0.</li>
     * <li>If a MediaPlayer object is not released or is woken up from the false standby state, the STB does not change the value of this parameter.</li>
     * </ul>
     */
  public setSubtitleFlag (subtitleFlag) {
      log.debug('[MediaPlayer]STBPlayer instanceId(%s): setSubtitleFlag(), MediaPlayer.setSubtitileFlag(%s)', this.instanceId, subtitleFlag)
      return this.mediaPlayer.setSubtitileFlag(subtitleFlag)
    }

    /**
     * Obtains the information about whether the player for a MediaPlayer object instance displays subtitles.
     * @method getSubtitleFlag
     * @return {Number} Whether the player displays subtitles. 0: no, 1: yes
     */
  public getSubtitleFlag () {
      let subtitleFlag = this.mediaPlayer.getSubtitileFlag()
      log.debug('[MediaPlayer]STBPlayer instanceId(%s): getSubtitleFlag(), MediaPlayer.getSubtitleFlag() return %s', this.instanceId, subtitleFlag)
      return subtitleFlag
    }
    /**
     * Obtain all subtitle information of the content that is playing.
     * @method getSubtitles
     * @return {Array|Subtitle}
     * @example
     * {"subtitle_list_count":2,"subtitle_list":[{"PID":21,"language_code":"chi","language_eng_name":"Chinese"},{"PID":22,"language_code":"eng","
     * language_eng_name":"English"}]}
     * <br/>If no subtitle information is found, the value {" subtitle_list_count":"0"} is returned.
     */
  public getSubtitles () {
      let subtitleString = this.mediaPlayer.getAllSubtitleInfo() + ''
      log.debug('[MediaPlayer]STBPlayer instanceId(%s): getSubtitles(), MediaPlayer.getAllSubtitleInfo() return %s', this.instanceId, subtitleString)
      let subtitles = this.invokeNativeMethod('getAllSubtitleInfo')
      return subtitles
    }

  private invokeNativeMethod (methodName: string, ...args) {
      const result = this.mediaPlayer[methodName].apply(this.mediaPlayer, args)
      try {
          return JSON.parse(result)
        } catch (e) {
          throw new Error('MedialPlayer.' + methodName + '() method result invalid, result: ' + result)
        }
    }

    /**
     * Obtains all audio track information of the content that is playing.
     * @return {Array}
     * @example
     * {"audio_track_list_count":2,"audio_track_list":[{"PID":33, "language_code":"chi","language_eng_name":"Chinese",},
     * {"PID":34,"language_code":"eng","language_eng_name":"English"}]}
     * <br/>If no audio track information is found, the value {"audio_track_list_count":"0"} is returned.
     */
  public getTracks () {
      let trackString, tracks
      trackString = this.mediaPlayer.getAllAudioTrackInfo() + ''
      log.debug('[MediaPlayer]STBPlayer instanceId(%s): getTracks(), MediaPlayer.getAllAudioTrackInfo() return %s', this.instanceId, trackString)
      tracks = JSON.parse(trackString)
      return tracks
    }
    /**
     * Obtains information about the current subtitle of the content that is playing.
     * @method getCurrentSubtitle
     * @return {Subtitle}
     * @example
     * {"PID":33,"language_code":"chi","language_eng_name":"Chinese"}
     * <br/>If no subtitle information is found, the value {"PID":-1} is returned.
     */
  public getCurrentSubtitle () {
      let subtitleString = this.mediaPlayer.getCurrentSubtitleInfo() + '',
          subtitleInfo = JSON.parse(subtitleString)
      log.debug('[MediaPlayer]STBPlayer instanceId(%s): getCurrentSubtitle(), MediaPlayer.getCurrentSubtitleInfo() return %s', this.instanceId, subtitleString)
      if (!subtitleInfo || subtitleInfo.PID === null || subtitleInfo.PID === -1) {
          return null
        } else {
          return subtitleInfo
        }
    }
    /**
     * Obtains information about the current audio track of the content that is playing.
     * @method getCurrentTrack
     * @return {JSON}
     * @example
     * {"PID":33,"language_code":"chi","language_eng_name":"Chinese"}
     * <br/>If no audio track information is found, the value {"PID":-1} is returned.
     */
  public getCurrentTrack () {
      let trackString = this.mediaPlayer.getCurrentAudioTrackInfo() + '',
          trackInfo = trackString && JSON.parse(trackString)
      log.debug('[MediaPlayer]STBPlayer instanceId(%s): getCurrentTrack(), MediaPlayer.getCurrentAudioTrackInfo() return %s', this.instanceId, trackString)
      if (!trackInfo || trackInfo.PID === null || trackInfo.PID === -1) {
          return null
        } else {
          return trackInfo
        }
    }

    /**
     * Switches to a specified subtitle.
     * @method setCurrentSubtitle
     * @param substitle {Subtitle} selected substitle
     * <em>Note: </em>If the subtitle does not exist, the STB does not perform any subtitle-related operation.
     */
  public setCurrentSubtitle (subtitle) {
      log.debug('[MediaPlayer]STBPlayer instanceId(%s): setCurrentSubtitle(), MediaPlayer.selectSubtitle(%s)', this.instanceId, subtitle.PID)
      this.mediaPlayer.selectSubtitle(subtitle.PID)
    }

    /**
     * Switches to a specified audio track.
     * @method setCurrentTrack
     * @param track {Track} selected track
     * <em>Note: </em>If the audio track information does not exist, the STB does not perform any audio track–related operation.
     */
  public setCurrenttDefaultTrack (track) {
      log.debug('[MediaPlayer]STBPlayer instanceId(%s): setCurrenttDefaultTrack(), MediaPlayer.selectDefaultAudio(%s)', this.instanceId, track.PID)
      this.mediaPlayer.selectDefaultAudio(track.PID)
    }
    /**
     * Set the default track
     */
  public setCurrentTrack (track) {
      log.debug('[MediaPlayer]STBPlayer instanceId(%s): setCurrentTrack(), MediaPlayer.selectAudio(%s)', this.instanceId, track.PID)
      this.mediaPlayer.selectAudio(track.PID)
    }
    /*
     * Switch to the next track
     */
  public switchAudioTrack () {
      log.debug('[MediaPlayer]STBPlayer instanceId(%s): switchAudioTrack(), MediaPlayer.switchAudioTrack()', this.instanceId)
      this.mediaPlayer.switchAudioTrack()
    }

    /**
     * Switches to the next subtitle. The current subtitle is switched to the next subtitle whose PID is next to the PID of the subtitle.
     * If the current subtitle is the last subtitle, no subtitle is displayed when the current subtitle is switched to the next subtitle
     * and the first audio track is displayed when the subtitle is switched again.
     * @method switchSubtitle
     */
  public switchSubtitle () {
      log.debug('[MediaPlayer]STBPlayer instanceId(%s): switchSubtitle(), MediaPlayer.switchSubtitle()', this.instanceId)
      this.mediaPlayer.switchSubtitle()
    }

    /**
     *Detects the audio channel type of the local player instance corresponding to a MediaPlayer object.
     *@method getCurrentAudioChannel
     *@return {Number}
     *<ul>
     *<li>Left</li>
     *<li>Right</li>
     *<li>Stereo</li>
     *<li>MONO</li>
     *</ul>
     */
  public getCurrentAudioChannel () {
      let currentAudioChannel = this.mediaPlayer.getCurrentAudioChannel()
      log.debug('[MediaPlayer]STBPlayer instanceId(%s): getCurrentAudioChannel(), MediaPlayer.getCurrentAudioChannel() return %s', this.instanceId, currentAudioChannel)
      return currentAudioChannel
    }

    /**
     * Switches to a specified audio channel. If an audio channel is specified in this method, the specified audio channel is used.
     * If no audio effect is specified, the audio channels are switched sequentially.
     * @method switchAudioChannel
     * @param type {String} indicates the specified audio channel.
     * <ul>
     * <li>Stereo:default value</li>
     * <li>MONO: mixes the sounds from both the left and right audio channels, and outputs the mixed sounds from both the left and right speakers.</li>
     * <li>Left: outputs sounds from the left audio channel.</li>
     * <li>Right: outputs sounds from the right audio channel.</li>
     * </ul>
     *
     * <em>Note: </em>
     * If an STB outputs digital audio, only the stereo and transparent transmission options are available. In this case, audio channel switches cannot be performed by this method.
     * <br/>Audio channel requirements are site specific. Some sites require only the stereo and mono modes.
     */
  public switchAudioChannel (type?: string) {
      if (_.isEmpty(type)) {
          log.debug('[MediaPlayer]STBPlayer instanceId(%s): switchAudioChannel(), MediaPlayer.switchAudioChannel()', this.instanceId)
          this.mediaPlayer.switchAudioChannel()
        } else {
          log.debug('[MediaPlayer]STBPlayer instanceId(%s): switchAudioChannel(), MediaPlayer.switchAudioChannel(%s)', this.instanceId, type)
          this.mediaPlayer.switchAudioChannel(type)
        }
    }

    /**
     * Get the player instance whether needs to display the sign of the Closed Caption subtitle
     *
     * @returns
     *
     * @memberOf STBPlayer
     */
  public getCCFlag () {
      let ccFlag = this.mediaPlayer.getCCFlag()
      log.debug('[MediaPlayer]STBPlayer instanceId(%s): getCCFlag(), MediaPlayer.getCCFlag() return %s', this.instanceId, ccFlag)
      return ccFlag
    }

    /**
     * Set the local player instance whether corresponding to the MediaPlayer object needs to display Closed Caption subtitles, then take effect immediately after setting
     *
     * @param {any} val
     *
     * @memberOf STBPlayer
     */
  public setCCFlag (val) {
      log.debug('[MediaPlayer]STBPlayer instanceId(%s): setCCFlag(), MediaPlayer.setCCFlag(%s)', this.instanceId, val)
      this.mediaPlayer.setCCFlag(val)
    }

    /**
     * Set the default track
     *
     * @param {any} track
     *
     * @memberOf STBPlayer
     */
  public selectDefaultAudio (track) {
      log.debug('[MediaPlayer]STBPlayer instanceId(%s): selectDefaultAudio(), MediaPlayer.selectDefaultAudio(%s)', this.instanceId, track.PID)
      this.mediaPlayer.selectDefaultAudio(track.PID)
    }

    /**
     * After the start of the play, EPG call setVideoDisplayMode and setVideoDisplayArea method to set the video display mode and video window size,
     * does not take effect immediately, need to EPG display call this method will take effect.
     *
     * @memberOf STBPlayer
     */
  public refreshVideoDisplay () {
      log.debug('[MediaPlayer]STBPlayer instanceId(%s): selectDefaultAudio(), MediaPlayer.refreshVideoDisplay()', this.instanceId)
      this.mediaPlayer.refreshVideoDisplay()
    }

    /**
     * Call this method to set the location, and size of the video window corresponding to the MediaPlayer object.
     * After the start of the call, call the interface to set the location and size of the video window, does not take effect in real time,
     * only wait until explicitly call refreshVideoDisplay method will take effect.
     * @param {any} left
     * @param {any} top
     * @param {any} width
     * @param {any} height
     *
     * @memberOf STBPlayer
     */
  public setVideoDisplayArea (left, top, width, height) {
      log.debug('[MediaPlayer]STBPlayer instanceId(%s): setVideoDisplayArea(), MediaPlayer.setVideoDisplayArea(%s, %s, %s, %s)', this.instanceId, left, top, width, height)
      this.mediaPlayer.setVideoDisplayArea(left, top, width, height)
    }

    /**
     * Call this method to set the MediaPlayer object corresponding to the video window display mode. After the start of the call,
     * call the interface to set the display mode, does not take effect in real time, only wait until the refresh call to refreshVideoDisplay method will take effect.
     * @param {any} videoDisplayMode
     *
     * @memberOf STBPlayer
     */
  public setVideoDisplayMode (videoDisplayMode) {
      log.debug('[MediaPlayer]STBPlayer instanceId(%s): setVideoDisplayMode(), MediaPlayer.setVideoDisplayMode(%s)', this.instanceId, videoDisplayMode)
      this.mediaPlayer.setVideoDisplayMode(videoDisplayMode)
    }

    /**
     * monocular play
     * @method setVRRotate
     * @param direction {float}: 0:up; 1: down; 2:left ; 3: right;
     * @param degree {float}: the degree relative to cureent visual angle;
     */
  public setVRRotate (direction, degree, flag) {
      log.debug('[MediaPlayer]STBPlayer instanceId(%s): setVRRotate(%s, %s)', this.instanceId, direction, degree)
      if (this.mediaPlayer.setVRRotate) {
          log.debug('[MediaPlayer]STBPlayer instanceId(%s): setVRRotate(), MediaPlayer.setVRRotate(%s, %s, %s)', this.instanceId, direction, degree, flag)
          this.mediaPlayer.setVRRotate(direction, degree, flag)
        }
    }
    /**
     * monocular play
     * @method setVRFlag
     * @param vrFlag {int}: 0:not vr; 1: sphere vr ; 2:cylinder vr ; 3: cube vr;
     */
  public setVRFlag (vrFlag) {
      log.debug('[MediaPlayer]STBPlayer instanceId(%s): setVRFlag(%s)', this.instanceId, vrFlag)
      if (this.mediaPlayer.setVRFlag) {
          log.debug('[MediaPlayer]STBPlayer instanceId(%s): setVRFlag(), MediaPlayer.setVRFlag(%s)', this.instanceId, vrFlag)
          this.mediaPlayer.setVRFlag(vrFlag)
        }
    }

    /**
     *
     * Turns on or off the output of the current player, including video, audio, and subtitles.
     * Call the interface after the player is still normal playback, but not the video images,
     * audio and subtitles output to the TV, the screen appears as a black screen.
     * @param {number} value
     * @returns
     *
     * @memberOf LiveTVPlayer
     */
  public setVideoOutput (outputFlag) {
      log.debug('[MediaPlayer]STBPlayer instanceId(%s): setVideoOutput(%s)', this.instanceId)
      if (this.mediaPlayer.setVideoOutput) {
          log.debug('[MediaPlayer]STBPlayer instanceId(%s): setVideoOutput(), MediaPlayer.setVideoOutput(%s)', this.instanceId, outputFlag)
          this.mediaPlayer.setVideoOutput(outputFlag)
        }
    }

    /**
     * Pre-play
     *
     *
     * @memberOf STBPlayer
     */
  public prepare () {
      log.debug('[MediaPlayer]STBPlayer instanceId(%s): prepare(), MediaPlayer.prepare()', this.instanceId)
      this.mediaPlayer.prepare()
    }

  public setTimeShiftLength (timeshiftLength) {
      log.debug('[MediaPlayer]STBPlayer instanceId(%s): setTimeShiftLength(), MediaPlayer.setTimeShiftLength()', this.instanceId)
      this.mediaPlayer.setTimeShiftLength(timeshiftLength)
    }

    /**
     * Preloaded from the specified time
     *
     *
     * @memberOf STBPlayer
     */
  public prepareByTime (type, timestamp) {
      log.debug('[MediaPlayer]STBPlayer instanceId(%s): prepareByTime(), MediaPlayer.prepareByTime()', this.instanceId)
      this.mediaPlayer.prepareByTime(type, timestamp)
    }

    /**
     * This method can be called to set the MediaPlayer object corresponding to the video window zoom mode.
     *  After starting the play, call the interface to set the display mode in real time.
     *
     * Zoom mode is as follows:
     * 	0：Original,According to the original size of the video display.
     *  1：Zoom1，Aspect ratio for the 4:3 video, on the 16:9 TV display, the left and right sides will be halved
     * (will cut off the upper and lower part of the video content).
     *  2：Zoom2，When the ratio of 4:3 to the video on the 16:9 TV display,
     * the way to enlarge the video to remove the left and right sides of the black (will cut off the upper and lower part of the video content).
     *  3：Zoom3，When the ratio of 1:2.35 to the video on the 16:9 TV show,
     * by the way to enlarge the video to remove the black side up and down (will cut off some of the video content).
     *  If this method is not called, the display mode defaults to 0.
     * @param {number} videoZoomMode
     *
     */
  public setVideoZoomMode (videoZoomMode) {
      log.debug('[MediaPlayer]STBPlayer instanceId(%s): setVideoZoomMode(%s)', this.instanceId)
      this.mediaPlayer.setVideoZoomMode(videoZoomMode)
    }

    /**
     * Call this method to obtain the MediaPlayer object corresponding to the video window zoom mode.
     * @returns
     */
  public getVideoZoomMode () {
      log.debug('[MediaPlayer]STBPlayer instanceId(%s): getVideoZoomMode(%s)', this.instanceId)
      let zoomMode = this.mediaPlayer.getVideoZoomMode()
      return zoomMode
    }

  public enableSQMMonitor () {
      if (this.mediaPlayer.enableSQMMonitor) {
          this.mediaPlayer.enableSQMMonitor()
        }
    }

  public disableSQMMonitor () {
      if (this.mediaPlayer.disableSQMMonitor) {
          this.mediaPlayer.disableSQMMonitor()
        }
    }

  public setCapability (capString) {
      this.mediaPlayer.setCapability(capString)
    }
}
