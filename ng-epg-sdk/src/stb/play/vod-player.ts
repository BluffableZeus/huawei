import * as _ from 'underscore'
import { STBPlayer } from './stb-player'

export class VodPlayer {
  static players: Array<VodPlayer> = []

  private stbPlayer: STBPlayer
  private mediaStr: string

  private playStatus: PlayStatus = {
      isMediaPlayed: false,
      currentPlayTime: '0'
    }

  public static getVolume (): number {
      const validPlayer = _.find(VodPlayer.players, player => {
          return !_.isUndefined(player.getVolume())
        })
      if (validPlayer) {
          let volume = validPlayer.getVolume()
          return _.isNumber(volume) ? volume : parseInt(volume, 10)
        } else {
          return 0
        }
    }

  public static setVolume (volume: number) {
      _.each(VodPlayer.players, player => {
          if (player) {
              player.setVolume(volume)
            }
        })
    }

  public static setMuteFlag (muteFlag: number) {
      _.each(VodPlayer.players, player => {
          if (player && player.stbPlayer) {
              player.stbPlayer.setMuteFlag(muteFlag)
            }
        })
    }

  constructor (private settings?: any) {
      this.releaseSTBPlayer()
      this.stbPlayer = new STBPlayer(settings)
      VodPlayer.players.push(this)
    }

    /**
         *
         * play vod
         * @method play
         * @param video {JSON} video information, {id:"media id",playUrl:"play url"}
         * @param mediaType {Number} media type
         *      <ul>
         *      <li>2:vod</li>
         *      <li>3:tvod</li>
         *      <li>10:pvr</li>
         *      </ul>
         *  @param time {Number} time to start play, unit:second
         */
  public play (video, auth, mediaType, timestamp, isVRContent = false, contentID?) {
      if (this.playStatus.isMediaPlayed && this.settings && this.settings.height === 1 && this.stbPlayer) {
          this.stbPlayer.releasePlayer()
          this.stbPlayer = new STBPlayer(this.settings)
        } else {
          this.renewSTBPlayer()
        }
      let mediaStrObj = {
          authData: auth,
          'mediaURL': auth.playURL,
          'mediaCode': contentID || video.ID || video.id || video.taskID,
            // 2：VOD 3：TVOD 10：PVR
          'mediaType': mediaType || 2,
          'allowTrickmode': 1,
          'contentID': contentID || video.ID
        }
      this.mediaStr = JSON.stringify(mediaStrObj)

      if (!this.stbPlayer) {
          this.stbPlayer = new STBPlayer(this.settings)
        }

      this.stbPlayer.setSingleMedia(this.mediaStr)

      if (isVRContent) {
          this.stbPlayer.setVRFlag(1)
        } else {
          this.stbPlayer.setVRFlag(0)
        }

      if (timestamp) {
          this.playByTime(timestamp)
        } else {
          this.playFromStart()
        }
    }

  public playAfterProtectContent (timestamp, isVRContent = false) {
      if (isVRContent) {
          this.stbPlayer.setVRFlag(1)
        } else {
          this.stbPlayer.setVRFlag(0)
        }

      if (timestamp) {
          this.playByTime(timestamp)
        } else {
          this.playFromStart()
        }
    }

  public setSingleMedia (video, auth, mediaType, contentID?) {
      if (this.playStatus.isMediaPlayed && this.settings && this.settings.height === 1 && this.stbPlayer) {
          this.stbPlayer.releasePlayer()
          this.stbPlayer = new STBPlayer(this.settings)
        } else {
          this.renewSTBPlayer()
        }
      let mediaStrObj = {
          authData: auth,
          'mediaURL': auth.playURL,
          'mediaCode': contentID || video.ID || video.id || video.taskID,
            // 2：VOD 3：TVOD 10：PVR
          'mediaType': mediaType || 2,
          'allowTrickmode': 1,
          'contentID': contentID || video.ID
        }
      this.mediaStr = JSON.stringify(mediaStrObj)

      if (!this.stbPlayer) {
          this.stbPlayer = new STBPlayer(this.settings)
        }

      this.stbPlayer.setSingleMedia(this.mediaStr)
    }

    /**
     * Pre-play
     *
     * @param {any} chan
     *
     * @memberOf VodPlayer
     */
  public prepare (video, auth, mediaType, timestamp, isVRContent = false, contentID?) {
      if (this.playStatus.isMediaPlayed && this.settings && this.settings.height === 1 && this.stbPlayer) {
          this.stbPlayer.releasePlayer()
          this.stbPlayer = new STBPlayer(this.settings)
        } else {
          this.renewSTBPlayer()
        }
      let mediaStrObj = {
          authData: auth,
          'mediaURL': auth.playURL,
          'mediaCode': contentID || video.ID || video.id || video.taskID,
            // 2：VOD 3：TVOD 10：PVR
          'mediaType': mediaType || 2,
          'allowTrickmode': 1,
          'contentID': contentID || video.ID
        }
      this.mediaStr = JSON.stringify(mediaStrObj)

      this.stbPlayer.setSingleMedia(this.mediaStr)

      if (isVRContent) {
          this.stbPlayer.setVRFlag(1)
        } else {
          this.stbPlayer.setVRFlag(0)
        }

      if (timestamp) {
          this.stbPlayer.prepareByTime(1, timestamp + '')
        } else {
          this.stbPlayer.prepareByTime(1, '0')
        }
    }

    /**
     * Called after parpare
     *
     *
     * @memberOf VodPlayer
     */
  public start (type, timestamp) {
      if (timestamp) {
          this.playByTime(timestamp)
        } else {
          this.playByTime(0)
        }

    }

    /**
     * Release the player
     *
     *
     * @memberOf VodPlayer
     */
  public releasePlayer () {
      this.playStatus.isMediaPlayed = false
      if (this.stbPlayer) {
          this.stbPlayer.releasePlayer()
          this.stbPlayer = null
        }
      VodPlayer.players.splice(VodPlayer.players.indexOf(this), 1)
    }

    /**
     * Play
     *
     *
     * @memberOf VodPlayer
     */
  public playFromStart () {
      if (this.renewSTBPlayer()) {
          this.stbPlayer.playFromStart()
          this.playStatus.isMediaPlayed = true
        }
    }

    /**
     * Bookmarks play
     *
     * @param {number} timestamp
     *
     * @memberOf VodPlayer
     */
  public playByTime (timestamp: number) {
      if (this.renewSTBPlayer()) {
          this.stbPlayer.playByTime(1, timestamp + '')
          this.playStatus.isMediaPlayed = true
        }
    }

    /**
     * pause
     *
     *
     * @memberOf VodPlayer
     */
  public pause () {
      if (this.isMediaPlayed()) {
          this.stbPlayer.pause()
        }
    }

    /**
     * Resume playing
     *
     *
     * @memberOf VodPlayer
     */
  public resume () {
      if (this.isMediaPlayed()) {
          this.stbPlayer.resume()
        } else {
          this.playByTime(parseInt(this.playStatus.currentPlayTime, 10))
        }
    }

    /**
     * stop
     *
     *
     * @memberOf VodPlayer
     */
  public stop () {
      if (this.playStatus.isMediaPlayed) {
          this.stbPlayer.stop()
        }
    }

    /**
     * Stop and release the player
     *
     *
     * @memberOf VodPlayer
     */
  public stopAndRelease () {
      this.stop()
      this.releasePlayer()
    }

    /**
     * The current playing time
     *
     * @returns
     *
     * @memberOf VodPlayer
     */
  public getCurrentPlayTime () {
      if (this.isMediaPlayed()) {
          this.playStatus.currentPlayTime = this.stbPlayer.getCurrentPlayTime()
        }
      return this.playStatus.currentPlayTime
    }

    /**
     * Video duration
     *
     * @returns
     *
     * @memberOf VodPlayer
     */
  public getMediaDuration () {
      if (this.isMediaPlayed()) {
          this.playStatus.mediaDuration = this.stbPlayer.getMediaDuration()
        }
      return this.playStatus.mediaDuration
    }

    /**
     * Fast forward
     *
     *
     * @memberOf VodPlayer
     */
  public fastForward (isRoundbin?: boolean) {
      if (this.isMediaPlayed()) {
          this.stbPlayer.fastForward(isRoundbin)
        }
    }

    /**
     * Rewind
     *
     *
     * @memberOf VodPlayer
     */
  public fastRewind (isRoundbin?: boolean) {
      if (this.isMediaPlayed()) {
          this.stbPlayer.fastRewind(isRoundbin)
        }
    }

    /**
     * Fast forward or rewind
     */
  public fastOrRewind (direction?: number, isRoundbin?: boolean) {
      if (direction > 0) {
          this.fastForward(isRoundbin)
        } else {
          this.fastRewind(isRoundbin)
        }
    }

    /**
     * Fast forward or rewind
     *
     * @param {any} fastSpeed
     *
     * @memberOf VodPlayer
     */
  public fastForwardOrRewind (fastSpeed) {
      if (this.isMediaPlayed()) {
          this.stbPlayer.fastForwardOrRewind(fastSpeed)
        }
    }

    /**
     * The EPG calls this method to jump to the media starting point to start playing.
     *
     *
     * @memberOf VodPlayer
     */
  public gotoStart () {
      let currentTime = this.getCurrentPlayTime()
      if (currentTime && parseInt(currentTime, 10) > 5) {
          this.playByTime(0)
        }
    }

    /**
     * The EPG calls this method to jump to the end of the media.
     *
     *
     * @memberOf VodPlayer
     */
  public gotoEnd () {
      if (this.isMediaPlayed()) {
          this.stbPlayer.gotoEnd()
        }
    }

    /**
     * The EPG calls this method to get the current play mode of the local player instance corresponding to the MediaPlayer object.
     *
     * @returns
     *
     * @memberOf VodPlayer
     */
  public getPlaybackMode () {
      if (this.isMediaPlayed()) {
          this.playStatus.playbackMode = this.stbPlayer.getPlaybackMode()
        }
      return this.playStatus.playbackMode
    }

    /**
     * Mute
     *
     *
     * @memberOf VodPlayer
     */
  public mute () {
      if (this.isMediaPlayed()) {
          this.stbPlayer.setMuteFlag(1)
        }
    }

    /**
     * Unmute
     *
     *
     * @memberOf VodPlayer
     */
  public unMute () {
      if (this.isMediaPlayed()) {
          this.stbPlayer.setMuteFlag(0)
        }
    }

    /**
     *
     *
     * @returns
     *
     * @memberOf VodPlayer
     */
  public isMute () {
      if (this.isMediaPlayed()) {
          return this.stbPlayer.getMuteFlag() === 1 || this.stbPlayer.getVolume() === 0
        }
      return true
    }

    /**
     * Get volume
     *
     * @returns
     *
     * @memberOf VodPlayer
     */
  public getVolume () {
      if (this.isMediaPlayed()) {
          this.playStatus.volume = this.stbPlayer.getVolume()
        }
      return this.playStatus.volume
    }

    /**
     * Set the volume
     *
     * @param {any} val
     *
     * @memberOf VodPlayer
     */
  public setVolume (val) {
      if (this.isMediaPlayed()) {
          this.stbPlayer.setVolume(val)
        }
    }

    /**
     * Get subtitles
     *
     * @returns
     *
     * @memberOf VodPlayer
     */
  public getSubtitles () {
      if (this.isMediaPlayed()) {
          this.playStatus.subtitles = this.stbPlayer.getSubtitles()
        }
      return this.playStatus.subtitles
    }

    /**
     * Get the current subtitle
     */
  public getCurrentSubtitle () {
      if (this.isMediaPlayed()) {
          this.playStatus.currentSubtitle = this.stbPlayer.getCurrentSubtitle()
        }
      return this.playStatus.currentSubtitle
    }

    /**
     * Set subtitles
     *
     * @param {any} subtitle
     *
     * @memberOf VodPlayer
     */
  public setCurrentSubtitle (subtitle) {
      if (this.isMediaPlayed()) {
          this.stbPlayer.setCurrentSubtitle(subtitle)
        }
    }

    /**
     * Get all tracks
     *
     * @returns
     *
     * @memberOf VodPlayer
      */
  public getTracks () {
      if (this.isMediaPlayed()) {
          this.playStatus.tracks = this.stbPlayer.getTracks()
        }
      return this.playStatus.tracks
    }

    /**
     * Get the current track
     *
     * @returns
     *
     * @memberOf VodPlayer
     */
  public getCurrentTrack () {
      if (this.isMediaPlayed()) {
          this.playStatus.currentTrack = this.stbPlayer.getCurrentTrack()
        }
      return this.playStatus.currentTrack
    }
    /*
	 * Switch to the next track
	 */
  public switchAudioTrack () {
      if (this.isMediaPlayed()) {
          this.stbPlayer.switchAudioTrack()
        }
    }

    /**
     * Set the track
     *
     * @param {any} track
     *
     * @memberOf VodPlayer
     */
  public setCurrentTrack (track) {
      if (this.isMediaPlayed()) {
          this.stbPlayer.setCurrentTrack(track)
        }
    }

    /**
     * Set the default track
     *
     * @param {any} track
     *
     * @memberOf VodPlayer
     */
  public setCurrenttDefaultTrack (track) {
      if (this.isMediaPlayed()) {
          this.stbPlayer.setCurrenttDefaultTrack(track)
        }
    }

    /**
     *
     *
     * @param {any} subtitleFlag
     *
     * @memberOf VodPlayer
     */
  public setSubtitleFlag (subtitleFlag) {
      if (this.isMediaPlayed()) {
          this.stbPlayer.setSubtitleFlag(subtitleFlag)
        }
    }

    /**
     * Get subtitle logo
     *
     * @returns
     *
     * @memberOf VodPlayer
     */
  public getSubtitleFlag () {
      if (this.isMediaPlayed()) {
          this.playStatus.subtitleFlag = this.stbPlayer.getSubtitleFlag()
        }
      return this.getSubtitleFlag
    }

    /**
     * Get the play instance ID
     *
     * @returns
     *
     * @memberOf VodPlayer
     */
  public getInstanceId () {
      if (this.stbPlayer) {
          return this.stbPlayer.instanceId
        } else {
          return null
        }
    }

    /**
     * Set the VR rotation direction and angle
     *
     * @param {any} direction
     * @param {any} degree
     *
     * @memberOf VodPlayer
     */
  public setVRRotate (direction, degree, flag) {
      if (this.renewSTBPlayer() && this.stbPlayer.setVRRotate) {
          this.stbPlayer.setVRRotate(direction, degree, flag)
        }
    }
    /**
     * monocular play
     * @method setVRFlag
     * @param vrFlag {int}: 0:not vr; 1: sphere vr ; 2:cylinder vr ; 3: cube vr;
     */
  public setVRFlag (vrFlag) {
      if (this.renewSTBPlayer() && this.stbPlayer.setVRFlag) {
          this.stbPlayer.setVRFlag(vrFlag)
        }
    }

    /**
     * Turns on or off the output of the current player, including video, audio, and subtitles.
     * Call the interface after the player is still normal playback,
     * but not the video images, audio and subtitles output to the TV, the screen appears as a black screen.
     *
     * @param {any} outputFlag
     *
     * @memberOf VodPlayer
     */
  public setVideoOutput (outputFlag) {
      if (this.renewSTBPlayer() && this.stbPlayer.setVideoOutput) {
          this.stbPlayer.setVideoOutput(outputFlag)
        }
    }

    /**
    * Set the channel.
    */
  public switchAudioChannel () {
      if (this.isMediaPlayed()) {
          this.stbPlayer.switchAudioChannel()
        }
    }

    /**
    * Get the channel value
    */
  public getCurrentAudioChannel () {
      if (this.isMediaPlayed()) {
          this.playStatus.currentAudioChannel = this.stbPlayer.getCurrentAudioChannel()
        }
      return this.playStatus.currentAudioChannel
    }

    /**
     * Sets the Macrovision content protection mode of the local player instance corresponding to a MediaPlayer object.
     * @param macrovisionFlag {Number} The value is of integer type
     */
  public setMacrovisionFlag (macrovisionFlag) {
      if (this.isMediaPlayed()) {
          this.stbPlayer.setMacrovisionFlag(macrovisionFlag)
        }
    }

    /**
     * Sets the high-bandwidth digital content protection (HDCP) mode of the local player instance corresponding to a MediaPlayer object.
     * @param hdcpFlag {Number} The value is of integer type
     */
  public setHDCPFlag (hdcpFlag) {
      if (this.isMediaPlayed()) {
          this.stbPlayer.setHDCPFlag(hdcpFlag)
        }
    }

    /**
     * Sets the Copy Generation Management System-Analog (CGMS-A) content protection mode of the local player instance corresponding
     * @param cgmsaFlag {Number} The value is of integer type
     */
  public setCGMSAFlag (cgmsaFlag) {
      if (this.isMediaPlayed()) {
          this.stbPlayer.setCGMSAFlag(cgmsaFlag)
        }
    }

  private isMediaPlayed () {
      return this.renewSTBPlayer() && this.playStatus.isMediaPlayed
    }

  private renewSTBPlayer (): boolean {
      if (VodPlayer.players.indexOf(this) !== VodPlayer.players.length - 1) {
          return false
        }
      this.releaseSTBPlayer()
      if (!this.stbPlayer) {
          this.stbPlayer = new STBPlayer(this.settings)
          this.stbPlayer.setSingleMedia(this.mediaStr)
          if (VodPlayer.players.indexOf(this) === -1) {
              VodPlayer.players.push(this)
            }
        }
      return true
    }

  private releaseSTBPlayer () {
      _.each(VodPlayer.players, player => {
          if (player !== this && player.stbPlayer) {
              player.stbPlayer.releasePlayer()
              player.stbPlayer = null
              player.playStatus.isMediaPlayed = false
            }
        })
    }

    /**
     * set the MediaPlayer object corresponding to the video window zoom mode
     * @param videoZoomMode {Number} The value is of integer type
     */
  public setVideoZoomMode (videoZoomMode) {
      if (this.isMediaPlayed()) {
          this.stbPlayer.setVideoZoomMode(videoZoomMode)
        }
    }

    /**
     * the video window zoom mode
     * @returns
     * @memberOf VodPlayer
     */
  public getVideoZoomMode () {
      if (this.isMediaPlayed()) {
          this.playStatus.zoomMode = this.stbPlayer.getVideoZoomMode()
        }
      return this.playStatus.zoomMode
    }

    /**
     * Set the display mode of the video window corresponding to the MediaPlayer object
     * @param {any} videoDisplayMode
     * @memberOf VodPlayer
     */
  public setVideoDisplayMode (videoDisplayMode) {
      if (this.isMediaPlayed()) {
          this.stbPlayer.setVideoZoomMode(videoDisplayMode)
        }
    }

    /**
     * Sets the location and size of the video window corresponding to the MediaPlayer object
     * @param {any} left
     * @param {any} top
     * @param {any} width
     * @param {any} height
     * @memberOf VodPlayer
     */
  public setVideoDisplayArea (left, top, width, height) {
      if (this.isMediaPlayed()) {
          this.stbPlayer.setVideoDisplayArea(left, top, width, height)
        }
    }

    /**
     * Video display mode and the size of the video window,
     * is not effective immediately, the need for EPG display call this method will take effect
     * @memberOf VodPlayer
     */
  public refreshVideoDisplay () {
      if (this.isMediaPlayed()) {
          this.stbPlayer.refreshVideoDisplay()
        }
    }
}

interface PlayStatus {
  isMediaPlayed: boolean
  currentPlayTime: string
  mediaDuration?: number
  playbackMode?: any
  volume?: number
  subtitles?: any
  currentSubtitle?: any
  tracks?: any
  currentTrack?: any
  subtitleFlag?: string
  currentAudioChannel?: any
  zoomMode?: any
}
