import { STBPlayer } from './stb-player'
import * as moment from 'moment'
import { Logger } from '../../utils/logger'
import { PlayChannelResponse } from '../../vsp/api'
import { Config } from '../../core/config'

let currentChan

const log = Logger.get('livetv-player.ts')

interface ChannelData {
  id: string
  meidaID: string
  channelNO: string
  authData?: PlayChannelResponse
}

interface LiveTVPlayerOptions {
  reuseForChannelId?: string
}

/**
 * Live play class
 *
 * @export
 * @class LiveTVPlayer
 */
export class LiveTVPlayer {

  static player: STBPlayer
  private _isSupportPltv = true
  private player: STBPlayer

  public static getVolume (): number {
      if (LiveTVPlayer.player) {
          return LiveTVPlayer.player.getVolume()
        } else {
          return 0
        }
    }

  public static setVolume (volume: number) {
      if (LiveTVPlayer.player) {
          LiveTVPlayer.player.setVolume(volume)
        }
    }

  public static setMuteFlag (muteFlag: number) {
      if (LiveTVPlayer.player) {
          LiveTVPlayer.player.setMuteFlag(muteFlag)
        }
    }

  constructor (settings?: any, options?: LiveTVPlayerOptions) {
      const reuseForChannelId = options && options.reuseForChannelId
      const { player } = LiveTVPlayer
      if (player && reuseForChannelId && player.mediaPlayer.isChannelIdPlay(reuseForChannelId)) {
          this.player = player
        } else {
          if (player) {
              player.stop()
              player.releasePlayer()
            }
          this.player = new STBPlayer(settings)
          LiveTVPlayer.player = this.player
        }
    }

    /**
     * Whether to support PLTV
     *
     * @returns
     *
     * @memberOf LiveTVPlayer
     */
  public isSupportPltv () {
      return this._isSupportPltv
    }

    /**
     * refresh support PLTV
     *
     * @memberOf LiveTVPlayer
     */
  public refreshPltv (isSupportPltv) {
      this._isSupportPltv = isSupportPltv
    }

    /**
     * Update time
     *
     * @param {any} chan
     *
     * @memberOf LiveTVPlayer
     */
  public refreshDate (chan) {
      currentChan = chan
    }

    /**
     * configure some parameter and play channel
     * @method joinChannel
     * @param  channel {Channel} channel to play
     */
  public joinChannel (chan: ChannelData, isVRContent: boolean, isCpltv?: boolean, timeshiftLength?, isSupportPltv?: boolean) {
      this._isSupportPltv = isSupportPltv
      this.setPlayerParams(chan)
      if (isCpltv && timeshiftLength) {
          this.player.setTimeShiftLength(timeshiftLength)
        }

      if (isVRContent) {
          this.player.setVRFlag(1)
        } else {
          this.player.setVRFlag(0)
        }

      this.player.playFromStart()
      LiveTVPlayer.player = this.player
    }

    /**
     * Pre-play
     *
     * @param {any} chan
     *
     * @memberOf LiveTVPlayer
     */
  public prepare (chan: ChannelData, isVRContent: boolean, isCpltv?: boolean, timeshiftLength?) {
      this.setPlayerParams(chan)

      if (isVRContent) {
          this.player.setVRFlag(1)
        } else {
          this.player.setVRFlag(0)
        }

      log.kpi('[MediaPlayer]live tv player begin prepare')
      this.player.prepare()
      log.kpi('[MediaPlayer]live tv player end prepare')
      if (isCpltv && timeshiftLength) {
          this.player.setTimeShiftLength(timeshiftLength)
        }
    }

    /**
     * Stop play
     *
     *
     * @memberOf LiveTVPlayer
     */
  public stop () {
      this.player.stop()
      this.player.releasePlayer()
      if (this.player === LiveTVPlayer.player) {
          LiveTVPlayer.player = null
        }
    }

    /**
     * Pause playback
     *
     *
     * @memberOf LiveTVPlayer
     */
  public pause () {
      if (this.isSupportPltv()) {
          this.player.pause()
        }
    }

    /**
     * Resume playing
     *
     *
     * @memberOf LiveTVPlayer
     */
  public resume () {
      this.player.resume()
    }

    /**
     * Fast forward
     *
     *
     * @memberOf LiveTVPlayer
     */
  public fastForward (isRoundbin?: boolean) {
      if (this.isSupportPltv()) {
          this.player.fastForward(isRoundbin)
        }
    }

    /**
     * Rewind
     *
     *
     * @memberOf LiveTVPlayer
     */
  public fastRewind (isRoundbin?: boolean) {
      if (this.isSupportPltv()) {
          this.player.fastRewind(isRoundbin)
        }
    }

    /**
     * Jump to the media starting point to start playing
     *
     *
     * @memberOf LiveTVPlayer
     */
  public gotoStart () {
      if (this.isSupportPltv()) {
          this.player.gotoStart()
        }
    }

    /**
     * Jump to the end of the media
     *
     *
     * @memberOf LiveTVPlayer
     */
  public gotoEnd () {
      if (this.isSupportPltv()) {
          this.player.gotoEnd()
        }
    }

    /**
     * Start play
     *
     *
     * @memberOf LiveTVPlayer
     */
  public playFromStart () {
      this.player.playFromStart()
      LiveTVPlayer.player = this.player
    }

    /**
     * Bookmarks play
     *
     * @param {any} time
     *
     * @memberOf LiveTVPlayer
     */
  public playByTime (time) {
      time = moment(time).utc().format('YYYYMMDDTHHmmss') + 'Z'
      this.player.playByTime(2, time)
      LiveTVPlayer.player = this.player
    }

    /**
     * The current playing time
     *
     * @returns
     *
     * @memberOf LiveTVPlayer
     */
  public getCurrentPlayTime () {
      let currentTime = this.player.getCurrentPlayTime() || moment().utc().format('YYYYMMDDTHHmmss') + 'Z'
      return moment.utc(currentTime, 'YYYYMMDDTHHmmssZ').toDate()
    }

    /**
     *
     *
     * @returns
     *
     * @memberOf LiveTVPlayer
     */
  public getCurrentChannelTime () {
      return new Date()
    }

    /**
     * Current play mode
     *
     * @returns
     *
     * @memberOf LiveTVPlayer
     */
  public getPlaybackMode () {
      return this.player.getPlaybackMode()
    }

    /**
     * Mute
     *
     *
     * @memberOf LiveTVPlayer
     */
  public mute () {
      this.player.setMuteFlag(1)
    }

    /**
     * Unmute
     *
     *
     * @memberOf LiveTVPlayer
     */
  public unMute () {
      this.player.setMuteFlag(0)
    }

    /**
     * To determine whether the mute
     *
     * @returns
     *
     * @memberOf LiveTVPlayer
     */
  public isMute () {
      return this.player.getMuteFlag() === 1 || this.player.getVolume() === 0
    }

    /**
     * Get volume
     *
     * @returns
     *
     * @memberOf LiveTVPlayer
     */
  public getVolume () {
      return this.player.getVolume()
    }

    /**
     * Set the volume
     *
     * @param {any} val
     *
     * @memberOf LiveTVPlayer
     */
  public setVolume (val) {
      this.player.setVolume(val)
    }

    /**
     * Subtitle information
     *
     * @returns
     *
     * @memberOf LiveTVPlayer
     */
  public getSubtitles () {
      return this.player.getSubtitles()
    }

    /**
     * Current subtitle information
     *
     * @returns
     *
     * @memberOf LiveTVPlayer
     */
  public getCurrentSubtitle () {
      return this.player.getCurrentSubtitle()
    }

    /**
     * Set subtitles
     *
     * @param {any} subtitle
     *
     * @memberOf LiveTVPlayer
     */
  public setCurrentSubtitle (subtitle) {
      this.player.setCurrentSubtitle(subtitle)
    }

    /**
     * Get the track
     *
     * @returns
     *
     * @memberOf LiveTVPlayer
     */
  public getTracks () {
      return this.player.getTracks()
    }

    /**
     * Current track
     *
     * @returns
     *
     * @memberOf LiveTVPlayer
     */
  public getCurrentTrack () {
      return this.player.getCurrentTrack()
    }

    /**
     *
     * Subtitle logo
     *
     * @returns
     *
     * @memberOf LiveTVPlayer
     */
  public getSubtitleFlag () {
      return this.player.getSubtitleFlag()
    }

    /**
     * Set the track
     *
     * @param {any} track
     *
     * @memberOf LiveTVPlayer
     */
  public setCurrentTrack (track) {
      this.player.setCurrentTrack(track)
    }

    /**
     * Set the subtitle logo
     *
     * @param {any} subtitleFlag
     *
     * @memberOf LiveTVPlayer
     */
  public setSubtitleFlag (subtitleFlag) {
      this.player.setSubtitleFlag(subtitleFlag)
    }

    /**
     * Get the play instance ID
     *
     * @returns
     *
     * @memberOf LiveTVPlayer
     */
  public getInstanceId () {
      return this.player.instanceId
    }

    /**
     * Get STB player instance that is used.
     *
     * @returns {STBPlayer}
     *      STB player instance that is used.
     *
     * @memberOf LiveTVPlayer
     */
  public getStbPlayer (): STBPlayer {
      return this.player
    }

    /**
     * Set the playback parameters
     *
     * @private
     *
     * @memberOf LiveTVPlayer
     */
  public setPlayerParams (chan: ChannelData) {
      currentChan = chan

      let mediaObj = {
          mediaURL: Config.get('jspLogined') ? '' + chan.channelNO : 'mediaId://' + chan.meidaID,
          mediaCode: chan.id + '',
          mediaType: 1,
          streamType: 2,
          contentID: chan.id + '',
          authData: chan.authData,
          channelNumber: chan.channelNO
        }

      const setSingleMediaOption = JSON.stringify(mediaObj)

      log.debug('[MediaPlayer]livetv player setSingleMedia: %s', setSingleMediaOption)

      this.player.setSingleMedia(setSingleMediaOption)
    }

    /**
     *
     * Turns on or off the output of the current player, including video, audio, and subtitles.
     * Call the interface after the player is still normal playback,
     * but not the video images, audio and subtitles output to the TV, the screen appears as a black screen.
     *
     * @param {number} value
     * @returns
     *
     * @memberOf LiveTVPlayer
     */
  public setVideoOutput (value: number) {
      return this.player.setVideoOutput(value)
    }

    /**
     * Set the VR rotation direction and angle
     *
     * @param {any} direction
     * @param {any} degree
     * @param {any} flag
     *
     * @memberOf LiveTVPlayer
     */
  public setVRRotate (direction, degree, flag) {
      if (this.player.setVRRotate) {
          this.player.setVRRotate(direction, degree, flag)
        }
    }

    /**
     * Set the VR flag
     *
     * @param {any} vrFlag
     *
     * @memberOf LiveTVPlayer
     */
  public setVRFlag (vrFlag) {
      if (this.player.setVRFlag) {
          this.player.setVRFlag(vrFlag)
        }
    }

    /**
     * Set the channel
     */
  public switchAudioChannel () {
      this.player.switchAudioChannel()
    }

    /**
     * Get the channel value
     */
  public getCurrentAudioChannel () {
      return this.player.getCurrentAudioChannel()
    }

    /**
    * Sets the Macrovision content protection mode of the local player instance corresponding to a MediaPlayer object.
    * @param macrovisionFlag {Number} The value is of integer type
    */
  public setMacrovisionFlag (macrovisionFlag) {
      this.player.setMacrovisionFlag(macrovisionFlag)
    }

    /**
     * Sets the high-bandwidth digital content protection (HDCP) mode of the local player instance corresponding to a MediaPlayer object.
     * @param hdcpFlag {Number} The value is of integer type
     */
  public setHDCPFlag (hdcpFlag) {
      this.player.setHDCPFlag(hdcpFlag)
    }

    /**
     * Sets the Copy Generation Management System-Analog (CGMS-A) content protection mode of the local player instance corresponding
     * @param cgmsaFlag {Number} The value is of integer type
     */
  public setCGMSAFlag (cgmsaFlag) {
      this.player.setCGMSAFlag(cgmsaFlag)
    }

    /**
     * set the MediaPlayer object corresponding to the video window zoom mode
     * @param videoZoomMode {Number} The value is of integer type
     */
  public setVideoZoomMode (videoZoomMode) {
      this.player.setVideoZoomMode(videoZoomMode)
    }

    /**
     * the video window zoom mode
     * @returns
     * @memberOf LiverPlayer
     */
  public getVideoZoomMode () {
      return this.player.getVideoZoomMode()
    }

    /**
     * Set the display mode of the video window corresponding to the MediaPlayer object
     * @param {any} videoDisplayMode
     * @memberOf LiverPlayer
     */
  public setVideoDisplayMode (videoDisplayMode) {
      this.player.setVideoDisplayMode(videoDisplayMode)
    }

    /**
     * Sets the location and size of the video window corresponding to the MediaPlayer object
     * @param {any} left
     * @param {any} top
     * @param {any} width
     * @param {any} height
     * @memberOf LiverPlayer
     */
  public setVideoDisplayArea (left, top, width, height) {
      this.player.setVideoDisplayArea(left, top, width, height)
    }

    /**
     * Video display mode and the size of the video window,
     * is not effective immediately, the need for EPG display call this method will take effect
     * @memberOf LiverPlayer
     */
  public refreshVideoDisplay () {
      this.player.refreshVideoDisplay()
    }
}
