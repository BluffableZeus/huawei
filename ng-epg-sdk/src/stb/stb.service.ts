import { getValueByName, setValueByName } from './utility-wrapper'
import * as _ from 'underscore'
import { isSTB } from '../utils/is_stb'
import { Config } from '../core/config'

export class STBService {

  private loadingAnimationNames: Array<string> = []
  private resetLoadingTimeoutId

    /**
	 * Determine whether the current STB environment
	 */
  public isSTB = isSTB

    /**
	 * @property {String} ntvuseraccount Access the network account, write STB Flash, the next use to take effect
	 */
  get ntvuseraccount (): string {
      return getValueByName('ntvuseraccount')
    }

  set ntvuseraccount (_ntvuseraccount: string) {
      setValueByName('ntvuseraccount', _ntvuseraccount)
    }

    /**
	 * @cfg access network password, write STB Flash, the next use to take effect
	 */
  get ntvuserpassword (): string {
      return getValueByName('ntvuserpassword')
    }

  set ntvuserpassword (_ntvuserpassword: string) {
      setValueByName('ntvuserpassword', _ntvuserpassword)
    }

    /**
	 * STB IP address, write STB Flash, restart effective.
	 */
  get stbIP (): string {
      return getValueByName('stbIP')
    }

  set stbIP (_stbIP: string) {
      setValueByName('stbIP', _stbIP)
    }

    /**
	 * STB operating system type, such as Linux, Android, can only read can not write.
	 */
  get OS_Type (): string {
      return getValueByName('OS_Type')
    }

    /**
	 * Gets the time zone STB terminal belongs.
	 */
  get AndroidTimeZone (): string {
      return getValueByName('AndroidTimeZone')
    }

    /**
	 * Used for Hybrid Video service MAC
	 */
  get MACAddress (): string {
      return getValueByName('MACAddress')
    }

  getVoicePiece (index: string): { audiodata: string, result: string, totalCount?: string } {
      return getValueByName('SpeechSearch.AudioData', { audioIndex: index })
    }

  formatDisk (deviceName: string, partitionName: string) {
      return setValueByName('HDDmangment.partition.format', {
          deviceName: deviceName,
          partition: partitionName,
          FilesystemType: 'EXT4'
        })
    }

  getFormatProgress (deviceName: string, partitionName: string) {
      return getValueByName('HDDmangment.format.progress', {
          device: deviceName,
          partition: partitionName
        })
    }

  getDiskInfo (deviceName: string) {
      return getValueByName('GetDiskInfo', { 'deviceName': deviceName })
    }

  getAllDiskNumberInfoByID (deviceID?: string) {
      return getValueByName('getAllDiskNumberInfo', { deviceID: deviceID })
    }

  setPVRPartition (device: string, partition: string) {
      return setValueByName('HDDmangment.setPVRPartition', { 'device': device, 'partition': partition })
    }

  get allDiskNumberInfo () {
      return getValueByName('getAllDiskNumberInfo')
    }

  sendCPVRParameter (pvrlistIncrementalRequestCount: number) {
      return setValueByName('pvrlistIncrementalRequestCount', pvrlistIncrementalRequestCount)
    }

  sendNotifyRemoteDevice (deviceID: string, masteSTBID: string) {
      let message = JSON.stringify({ command: 'SetMasterID', masterID: masteSTBID })
      return setValueByName('dlna.notifyRemoteDevice', { deviceID: deviceID, message: message })
    }

  setFriendlyName (friendlyName: string, deviceID: string) {
      return setValueByName('dlna.setFriendlyName', { friendlyName: friendlyName, deviceID: deviceID })
    }

  setRemoteCPVRDisk (deviceID: string, deviceName: string) {
      let message = JSON.stringify({ command: 'SetCPVRDisk', deviceName: deviceName })
      return setValueByName('dlna.notifyRemoteDevice', { deviceID: deviceID, message: message })
    }

    /**
     * Get the machine as a master set-top box
     * @returns {number} [1:Master,0:Slave,-1:fail]
     */
  isMasterSTB (): number {
      return getValueByName('MasterSTB')
    }

    /**
     * Gets the ID of the Master STB set by the current set-top box
     */
  get masterSTBID (): string {
      return getValueByName('masterSTBID')
    }

    /**
     * Gets the ID of the Master STB set by the current set-top box
     */
  set masterSTBID (masterSTBID: string) {
      setValueByName('masterSTBID', masterSTBID)
    }

    /**
     * Set the master set-top box
     */
  setMasterSTB (masterSTB: string) {
      return setValueByName('MasterSTB', masterSTB)
    }

  setPVRAbility (allowPVR: string) {
      return setValueByName('PVRAbility', { AllowPVR: allowPVR })
    }

    /**
     * Get all online set-top boxes
     */
  getDeviceList () {
      return getValueByName('dlna.getDeviceList')
    }

    /**
     * The secondary set-top box queries the list of task IDs on the specified DMS
     */
  queryDmsTaskList (deviceID: string, index: number, count: number): { count: number, taskIDList: string } {
      return getValueByName('dlna.queryDmsTaskList', {
          'deviceID': deviceID, 'index': index, 'count': count
        })
    }

    /**
     * Query the list of recording task IDs for the specified status from the main set-top box
     */
  getPVRTaskIDList (position: number, count: number, state: Array<string>): {
      Schedules_count: number, ScheduleIDList: string
    } {
      return getValueByName('PVR.ScheduleIDList.Get', {
          'position': position, 'count': count, 'state': state
        })
    }

    /**
     * Query the number of tasks for the specified status in the set-top box
     */
  getPVRTaskCount (state: Array<string>): number {
      return getValueByName('PVR.ScheduleList.Count', { 'state': state })
    }

    /**
     * set pip position and size
     */
  adjustPipRect (positionAndSize: Object) {
      return getValueByName('AdjustPipRect', positionAndSize)
    }

    /**
	 * Open the boot image
	 */
  showSplash () {
      setValueByName('ShowPic', '1')
    }
    /**
	 * Turn off the boot image
	 */
  hideSplash () {
      setValueByName('ShowPic', '2')
    }

    /**
	 * [Get the volume of the set-top box
	 * @return {number} [Set-top box volume]
	 */
  get stbVolume (): number {
      return getValueByName('STBVolume')
    }

    /**
	 * [Sets the volume of the set-top box]
	 * @param {number} _volume [volume]
	 */
  set stbVolume (_volume: number) {
      setValueByName('STBVolume', _volume)
    }

    /**
	 * [Mute Get the mute value]
	 * @return {number} [Mute flag: 1 mute, 0 unmuted]
	 */
  get stbMute (): number {
      return getValueByName('STBMuteFlag')
    }

    /**
	 * [set mute]
	 * @param {number} _mute [Mute flag: 1 mute, 0 unmuted]
	 */
  set stbMute (_mute: number) {
      setValueByName('STBMuteFlag', _mute)
    }

    /**
     * Friendly name
     */
  get friendlyName (): any {
      return getValueByName('friendlyName')
    }

  set friendlyName (_friendlyName: any) {
      setValueByName('dlna.setFriendlyName', { 'friendlyName': _friendlyName })
    }
    /**
     * Read the set-top box parameters
     */
  read (key) {
      let ret = getValueByName(key)
      ret = _.isUndefined(ret) ? ret : ret + ''
      return ret
    }
    /**
     * Write the set-top box parameters
     */
  write (key, value?) {
      let ret = setValueByName(key, value + '')
      return ret
    }

    /**
     * Start XMPP
     */
  xmppEnable () {
      setValueByName('XMPPService', '{"serviceEnable": "1" }')
    }

    /**
     * Close XMPP
     */
  xmppDisable () {
      setValueByName('XMPPService', '{"serviceEnable": "0" }')
    }

    /**
     * Whether to support XMPP
     *
     * @returns
     */
  isSupportXMPP () {
      return (this.read('XMPPCapability') === '1')
    }

    /**
     * Get the recording content
     *
     * @param {any} key
     * @returns
     */
  getRecord (key) {
      let value = this.read('Tools.Record.GetRecord,' + JSON.stringify({
          key: key
        }))
      return (/^undefined|null|^$/).test(value) ? null : value
    }

    /**
     * Last played channel
     *
     * @returns {string}
     */
  getlastChannelNO (): string {
      return getValueByName('lastChanKey, { "chanDomain": "IPTV" }')
    }

    /**
     * DHPublicKey
     *
     * @returns
     */
  getDHPublicKey () {
      return this.read('DHPublicKey')
    }

    /**
     * Set DHPublicKey
     *
     * @param {string} dhPublicKey
     * @returns
     */
  setDHPublicKey (dhPublicKey: string) {
      return setValueByName('DHSignatureKey', {
          platformPublicKey: dhPublicKey
        })
    }

    /**
     * AES encryption
     *
     * @param {string} encryptData
     * @returns
     */
  aesEncrypt (encryptData: string) {
      return getValueByName('safeSensitiveDataEncrypt', {
          sensitiveData: encryptData,
          encryptAlgorithm: 'AES_256_CBC'
        })
    }
    /**
     * AES encryption
     *
     * @param {string} decryptData
     * @returns
     */
  aesDecrypt (decryptData: string) {
      return getValueByName('safeSensitiveDataDecrypt', {
          sensitiveData: decryptData,
          encryptAlgorithm: 'AES_256_CBC'
        })
    }

    /**
     * get STB's domain
     */
  getSTBDomain () {
      return getValueByName('domain')
    }

    /**
     * notice STB refresh channellist
     */
  refreshChannellist () {
      if (!Config.get('jspLogined')) {
          setValueByName('hw_op_refreshchannellist', 'XXXX')
        }
    }

    /**
     * The EPG displays or hides the browser's loading animation through this interface.
     */
  showLoadingAnimation (enable: boolean, name?: string) {
      clearTimeout(this.resetLoadingTimeoutId)

      if (name) {

          switch (enable) {
              case true:
                this.loadingAnimationNames.push(name)
                this.loadingAnimationNames = _.uniq(this.loadingAnimationNames)
                break
              case false:
                this.loadingAnimationNames = _.without(this.loadingAnimationNames, name)
                break
            }

          if (this.loadingAnimationNames.length) {

              this.resetLoadingTimeoutId = setTimeout(() => {
                  this.showLoadingAnimation(false)
                }, 20 * 1000)

              return setValueByName('showLoadingAnimation', 1)
            } else {
              return setValueByName('showLoadingAnimation', 0)
            }

        } else {
          this.loadingAnimationNames = []
          setValueByName('showLoadingAnimation', enable ? 1 : 0)
        }
    }

  restart () {
      setValueByName('hw_op_restart', 'XXXX')
    }
}

export const stbService: STBService = new STBService()
