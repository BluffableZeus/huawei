import { STBService } from './../stb.service'
import { win } from '../../utils/win'

declare var Utility
win['Utility'] = { setValueByName: () => { }, getValueByName: () => { } }

describe('stbservice', () => {

  let stbService: STBService

  beforeEach(() => {
      stbService = new STBService()
      jasmine.clock().install()
    })

  afterEach(() => {
      jasmine.clock().uninstall()
    })

  it('showLoadingAnimation 1', () => {
      spyOn(Utility, 'setValueByName')
      stbService.showLoadingAnimation(true, 'live')
      expect(Utility.setValueByName).toHaveBeenCalledWith('showLoadingAnimation', '1')
    })

  it('showLoadingAnimation 2', () => {
      stbService.showLoadingAnimation(true, 'live')

      spyOn(Utility, 'setValueByName')
      stbService.showLoadingAnimation(false, 'live')
      expect(Utility.setValueByName).toHaveBeenCalledWith('showLoadingAnimation', '0')
    })

  it('showLoadingAnimation 3', () => {
      stbService.showLoadingAnimation(true, 'live')
      stbService.showLoadingAnimation(true, 'live')

      spyOn(Utility, 'setValueByName')
      stbService.showLoadingAnimation(false, 'live')
      expect(Utility.setValueByName).toHaveBeenCalledWith('showLoadingAnimation', '0')
    })

  it('showLoadingAnimation 4', () => {
      stbService.showLoadingAnimation(true, 'live')
      stbService.showLoadingAnimation(true, 'ajax')

      spyOn(Utility, 'setValueByName')
      stbService.showLoadingAnimation(false, 'live')
      expect(Utility.setValueByName).toHaveBeenCalledWith('showLoadingAnimation', '1')
    })

  it('showLoadingAnimation 5', () => {
      stbService.showLoadingAnimation(true, 'live')
      stbService.showLoadingAnimation(true, 'ajax')

      stbService.showLoadingAnimation(false, 'live')

      spyOn(Utility, 'setValueByName')
      stbService.showLoadingAnimation(false, 'ajax')
      expect(Utility.setValueByName).toHaveBeenCalledWith('showLoadingAnimation', '0')
    })

  it('showLoadingAnimation 5', () => {
      stbService.showLoadingAnimation(true, 'live')
      spyOn(Utility, 'setValueByName')
      jasmine.clock().tick(21 * 1000)
      expect(Utility.setValueByName).toHaveBeenCalledWith('showLoadingAnimation', '0')
    })

  it('showLoadingAnimation 6', () => {
      stbService.showLoadingAnimation(true, 'live')
      stbService.showLoadingAnimation(true, 'ajax')

      spyOn(Utility, 'setValueByName')
      stbService.showLoadingAnimation(false)
      expect(Utility.setValueByName).toHaveBeenCalledWith('showLoadingAnimation', '0')
    })
})
