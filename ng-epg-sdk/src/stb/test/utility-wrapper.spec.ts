import { setValueByName, getValueByName } from '../utility-wrapper'
import { win } from '../../utils/win'

declare var Utility

win['Utility'] = { setValueByName: () => { }, getValueByName: () => { } }

describe('Utility', () => {
  beforeEach(() => {

    })

  it('setValueByName test 1', () => {
      spyOn(Utility, 'setValueByName').and.returnValue('')
      setValueByName('key', 'value')
      expect(Utility.setValueByName).toHaveBeenCalledWith('key', 'value')
    })

  it('setValueByName test 2', () => {
      spyOn(Utility, 'setValueByName').and.returnValue('')
      setValueByName('key', 1)
      expect(Utility.setValueByName).toHaveBeenCalledWith('key', '1')
    })

  it('setValueByName test 3', () => {
      spyOn(Utility, 'setValueByName').and.returnValue('')
      setValueByName('key', { name: 'hwc' })
      expect(Utility.setValueByName).toHaveBeenCalledWith('key', '{"name":"hwc"}')
    })

  it('getValueByName test 1', () => {
      spyOn(Utility, 'getValueByName').and.returnValue('')
      getValueByName('key', 'value')
      expect(Utility.getValueByName).toHaveBeenCalledWith('key', 'value')
    })

  it('getValueByName test 2', () => {
      spyOn(Utility, 'getValueByName').and.returnValue('')
      getValueByName('key')
      expect(Utility.getValueByName).toHaveBeenCalledWith('key')
    })

  it('getValueByName test 3', () => {
      spyOn(Utility, 'getValueByName').and.returnValue('')
      getValueByName('key', { name: 'hwc' })
      expect(Utility.getValueByName).toHaveBeenCalledWith('key,{"name":"hwc"}')
    })

  it('result test 1', () => {
      spyOn(Utility, 'getValueByName').and.returnValue('')
      expect(getValueByName('key')).toEqual('')
    })

  it('result test 2', () => {
      spyOn(Utility, 'getValueByName').and.returnValue('{"name":"hwc"}')
      expect(getValueByName('key')).toEqual({ name: 'hwc' })
    })

  it('result test 3', () => {
      spyOn(Utility, 'getValueByName').and.returnValue('[1,2,3]')
      expect(getValueByName('key')).toEqual([1, 2, 3])
    })

  it('result test 4', () => {
      spyOn(Utility, 'getValueByName').and.returnValue('1,2,3')
      expect(getValueByName('key')).toEqual('1,2,3')
    })
})
