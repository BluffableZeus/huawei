import { Logger } from './../utils/logger'
import * as _ from 'underscore'

const log = new Logger('utility-wrapper.ts')

declare const Utility: {
  setValueByName (key, value);
  getValueByName (key, value?);
  getEvent ();
}

/**
 * Set the STB parameter
 * @param {String} key
 * @param {Object} value
 * @return {String} ret
 */
export function setValueByName (key: string, value: any) {

  if (null !== value && undefined !== value && !_.isString(value)) {
      value = JSON.stringify(value)
    }

  log.kpi('[Utility]begin setValueByName(), %s', { [key]: value })

  let ret = Utility.setValueByName(key, value)

  log.kpi('[Utility]end setValueByName(), return:%s', ret)

  return ret
}

/**
 * fetch value with key from stb
 *
 * sample
 *
 *       Utility.getValueByName('dvb_get_channel_list', {
 *            'type': 0,
 *            'position': 0,
 *            'count': 10
 *       });
 *
 *
 * @param {String} key
 * @param {JSON|String} value
 * @return {Object} ret
 */
export function getValueByName (key: string, value?: any) {

  let args: Array<any>
  if (_.isEmpty(value)) {
      args = [key]
    } else {
      if (_.isObject(value)) {
          args = [key + ',' + JSON.stringify(value)]
        } else {
          args = [key, value]
        }
    }

  log.kpi('[Utility]Begin getValueByName() args:%s', { [key]: args })

  let ret
  const normalArg = 1
  const specialArgs = 2
  switch (args.length) {
      case normalArg:
        ret = Utility.getValueByName(args[0])
        break
      case specialArgs:
        ret = Utility.getValueByName(args[0], args[1])
        break
      default:
        ret = Utility.getValueByName.apply(Utility, args)
        break
    }

  if (_.isString(ret)) {
      if (0 === ret.indexOf('{') || 0 === ret.indexOf('[')) {
          try {
              ret = JSON.parse(ret)
            } catch (e) {
              log.error('[Utility]getValueByName, parse result error, result:%s, error:%s', ret, e)
            }
        }
    }

  log.kpi('[Utility]End getValueByName, result: %s', { [key]: ret })
  return ret
}

export function getEvent () {
  const eventJsonStr = Utility.getEvent()
  if (eventJsonStr) {
      log.debug('[Utility][STB_EVENT]getEvent, event:%s', eventJsonStr)
      try {
          const ret = JSON.parse(eventJsonStr)
          ret.toString = () => {
              return eventJsonStr
            }
          return ret
        } catch (e) {
          log.debug('[Utility][STB_EVENT]getEvent, parse event error, event:%s, error:%s', eventJsonStr, e)
        }
    }

  return null
}
