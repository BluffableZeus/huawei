import * as _ from 'underscore'
import { QueryPlaybillListResponse, QueryPlaybillContextResponse, ChannelPlaybillContext, ChannelPlaybill } from './../vsp/api'

const MILLISECONDS_PER_HOUR = 60 * 60 * 1000
const DEFAULT_DAY = 7

export class PlaybillAutoFillService {

  private playbillLen = 7
  private recPlaybillLen = 7
  private channelIDsCache: { [channelID: string]: Array<any> } = {}

  setPlybillLimitDays (recPlaybillLen: number, playbillLen: number) {
      this.recPlaybillLen = recPlaybillLen || DEFAULT_DAY
      this.playbillLen = playbillLen || DEFAULT_DAY
    }

    /**
     * reset timeline playbills
     */
  setPlaybillListFillProgram (channelIDs: Array<string>, beginTime: number, endTime: number, resp: QueryPlaybillListResponse) {
      if (_.isEmpty(resp.channelPlaybills) || !resp) {
          _.extend(resp, { channelPlaybills: [] })
          _.map(channelIDs, (channelID) => {
              const playbills = this.setTimelinePlaybillLites(channelID, beginTime, endTime)
              resp.channelPlaybills.push(playbills as any)
            })
        } else if (resp.channelPlaybills.length < channelIDs.length) {
          _.extend(resp, { channelPlaybillsDB: [] })
          const returnChannelIDs = this.getChannelPlaybillChannelIDs(resp.channelPlaybills)
          _.map(channelIDs, (channelID, index) => {
              const returnIndex = _.indexOf(returnChannelIDs, channelID)
              if (returnIndex !== -1) {
                  (resp as any).channelPlaybillsDB[index] = resp.channelPlaybills[returnIndex]
                } else {
                  const playbills = this.setTimelinePlaybillLites(channelID, beginTime, endTime);
                  (resp as any).channelPlaybillsDB[index] = playbills
                }
            })
          resp.channelPlaybills = (resp as any).channelPlaybillsDB
        }
      return resp
    }

    /**
     * mock timeline playbills data
     */
  setTimelinePlaybillLites (channelID: string, beginTime: number, endTime: number) {
      let playbillLites = this.setTimelineLimitPlaybills(channelID)
      playbillLites = _.filter(playbillLites, (playbill) => {
          return +playbill.endTime >= beginTime && + playbill.startTime <= endTime
        })
      playbillLites = _.uniq(_.uniq(playbillLites, (playbill) => {
          return playbill.startTime
        }), (uniqPlaybill) => {
          return uniqPlaybill.ID
        })
      return { playbillLites }
    }

  setTimelineLimitPlaybills (channelID: string) {
      if (!_.isEmpty(this.channelIDsCache[channelID])) {
          return this.channelIDsCache[channelID]
        } else {
          this.channelIDsCache[channelID] = []
          const nowTime = Date.now()
          const beginTime = this.setFloorHour(nowTime - this.recPlaybillLen * 24 * MILLISECONDS_PER_HOUR)
          const endTime = this.setFloorHour(nowTime + this.playbillLen * 24 * MILLISECONDS_PER_HOUR)
          const playbillCount = (endTime - beginTime) / MILLISECONDS_PER_HOUR

          for (let i = 0; i < playbillCount; i++) {
              const playbillStartTime = beginTime + i * MILLISECONDS_PER_HOUR
              const playbillEndTime = playbillStartTime + MILLISECONDS_PER_HOUR
              const playbill = {
                  startTime: '' + playbillStartTime,
                  endTime: '' + playbillEndTime,
                  channelID: channelID,
                  CUTVStatus: '0',
                  ID: '@20189343@353' + (_.uniqueId() + _.random(10)),
                  PVRTaskType: '0',
                  hasRecordingPVR: '0',
                  isBlackout: '0',
                  isCUTV: '0',
                  isFillProgram: '3',
                  isLocked: '0',
                  name: '',
                  rating: { ID: '2', code: '2', name: 'PG20' }
                }
              this.channelIDsCache[channelID].push(playbill)
            }
          return this.channelIDsCache[channelID]
        }
    }

    /**
     * reset context playbills
     */
  setPlaybillContextFillProgram (channelIDs: Array<string>, currentTime: number, preNumber: number,
        nextNumber: number, resp: QueryPlaybillContextResponse, queryMore?: boolean) {
      if (_.isEmpty(resp.channelPlaybillContexts) || !resp) {
          _.extend(resp, { channelPlaybillContexts: [] })
          _.map(channelIDs, (channelID) => {
              const playbill = this.setContextPlaybillLites(channelID, currentTime, preNumber, nextNumber, queryMore)
              resp.channelPlaybillContexts.push(playbill as any)
            })
        } else if (resp.channelPlaybillContexts.length < channelIDs.length) {
          _.extend(resp, { channelPlaybillContextsDB: [] })
          const returnChannelIDs = this.getChannelPlaybillContextChannelIDs(resp.channelPlaybillContexts)
          _.map(channelIDs, (channelID, index) => {
              const returnIndex = _.indexOf(returnChannelIDs, channelID)
              if (returnIndex !== -1) {
                  (resp as any).channelPlaybillContextsDB[index] = resp.channelPlaybillContexts[returnIndex]
                } else {
                  (resp as any).channelPlaybillContextsDB[index] =
                        this.setContextPlaybillLites(channelID, currentTime, preNumber, nextNumber, queryMore)
                }
            })
          resp.channelPlaybillContexts = (resp as any).channelPlaybillContextsDB
        }
      return resp
    }

  getChannelPlaybillContextChannelIDs (content: Array<ChannelPlaybillContext>) {
      const channelIDs = _.pluck(_.pluck(content, 'currentPlaybillLite'), 'channelID')
      return channelIDs
    }

  getChannelPlaybillChannelIDs (content: Array<ChannelPlaybill>) {
      let channelIDs = []
      _.map(content, (channelPlaybill) => {
          let playbill = channelPlaybill && _.first(channelPlaybill.playbillLites)
          if (playbill) {
              channelIDs.push(playbill.channelID)
            }
        })
      return channelIDs
    }

    /**
     * mock context fillProgram
     */
  setContextPlaybillLites (channelID: string, currentTime: number, preNumber: number, nextNumber: number, queryMore?: boolean) {
      const timeObject = this.floorCurrentTime(currentTime)
      const currentPlaybillLite = {
          startTime: '' + timeObject.startTime,
          endTime: '' + timeObject.endTime,
          channelID: channelID,
          CUTVStatus: '0',
          ID: '@2017343@353' + (_.uniqueId() + _.random(10)),
          PVRTaskType: '0',
          hasRecordingPVR: '0',
          isBlackout: '0',
          isCUTV: '0',
          isFillProgram: '3',
          isLocked: '0',
          name: '',
          rating: { ID: '2', code: '2', name: 'PG20' }
        }
      let prePlaybillLites = []
      let nextPlaybillLites = []
      const moreLength = queryMore ? 30 : 0

      if (preNumber > 0) {
          for (let i = preNumber + moreLength; i > 0; i--) {
              const startTime = timeObject.startTime - i * MILLISECONDS_PER_HOUR
              const endTime = startTime + MILLISECONDS_PER_HOUR
              const playbillte = {
                  startTime: '' + startTime,
                  endTime: '' + endTime,
                  channelID: channelID,
                  CUTVStatus: '0',
                  ID: '@2017343@34' + (_.uniqueId() + _.random(15)),
                  PVRTaskType: '0',
                  hasRecordingPVR: '0',
                  isBlackout: '0',
                  isCUTV: '0',
                  isFillProgram: '3',
                  isLocked: '0',
                  name: '',
                  rating: { ID: '2', code: '2', name: 'PG20' }
                }
              prePlaybillLites.push(playbillte)
            }
          prePlaybillLites = _.uniq(prePlaybillLites, (playbill) => {
              return playbill.startTime
            })
        }

      if (nextNumber > 0) {
          for (let j = 1; j <= nextNumber + moreLength; j++) {
              const startTime = timeObject.startTime + j * MILLISECONDS_PER_HOUR
              const endTime = startTime + MILLISECONDS_PER_HOUR
              const playbillte = {
                  startTime: '' + startTime,
                  endTime: '' + endTime,
                  channelID: channelID,
                  CUTVStatus: '0',
                  ID: '@2017343@34@37' + (_.uniqueId() + _.random(20)),
                  PVRTaskType: '0',
                  hasRecordingPVR: '0',
                  isBlackout: '0',
                  isCUTV: '0',
                  isFillProgram: '3',
                  isLocked: '0',
                  name: '',
                  rating: { ID: '2', code: '2', name: 'PG20' }
                }
              nextPlaybillLites.push(playbillte)
            }
          nextPlaybillLites = _.uniq(nextPlaybillLites, (playbill) => {
              return playbill.startTime
            })
        }

      return { prePlaybillLites, currentPlaybillLite, nextPlaybillLites }
    }

  setFloorHour (currentTime: number) {
      const date = new Date(currentTime)
      date.setMilliseconds(0)
      date.setSeconds(0)
      date.setMinutes(0)
      return date.getTime()
    }

  floorCurrentTime (currentTime: number) {
      const date = this.setFloorHour(currentTime)
      return {
          startTime: date,
          endTime: date + MILLISECONDS_PER_HOUR
        }
    }
}
