import { Injectable } from '@angular/core'
import * as _ from 'underscore'

import {
    ChannelPlaybill, queryPlaybillContext, QueryPlaybillContextRequest, ChannelPlaybillContext,
    PlaybillLite, queryPlaybillList, QueryPlaybillListRequest as QueryPlaybillRequest,
    getPlaybillDetail, GetPlaybillDetailResponse, GetPlaybillDetailRequest
} from '../vsp/api'
import { PlaybillAutoFillService } from './playbill-auto-fill.service'

/**
 * playbill server service
 */
@Injectable()
export class PlaybillService {

  private _queryPlaybillList = queryPlaybillList
  private _queryPlaybillContext = queryPlaybillContext
  private _getPlaybillDetail = getPlaybillDetail
  private playbillAutoFillService: PlaybillAutoFillService

  constructor () {
      this.playbillAutoFillService = new PlaybillAutoFillService()
    }

    /**
     * fet playbills data
     */
  batchQueryPlaybillList (req: { list: { channelID: string, beginTime: number, endTime: number }[] }): Promise<{
      list: { channelID: string, list: PlaybillLite[] }[]
    }> {

      const channelIDs = _.uniq(_.pluck(req.list, 'channelID'))

      const [beginTime, endTime] = this.getMaxTimeSpan(req.list)
      const queryPlaybillRequest: QueryPlaybillRequest = {
          queryChannel: {
              channelIDs: channelIDs
            },
          queryPlaybill: {
              type: '0',
              startTime: '' + beginTime,
              endTime: '' + endTime,
              count: '100',
              offset: '0',
              isFillProgram: '1'
            },
          needChannel: '0'
        }

      return this._queryPlaybillList(queryPlaybillRequest).then((resp) => {
          const ret = { list: [] }
          ret.list = _.map(resp.channelPlaybills, (channelPlaybill: ChannelPlaybill) => {
              if (!_.isEmpty(channelPlaybill) && !_.isEmpty(channelPlaybill.playbillLites)) {
                  return { channelID: _.first(channelPlaybill.playbillLites).channelID, list: channelPlaybill.playbillLites }
                } else {
                  return {}
                }
            })
          return ret
        })
    }

    /**
     * get playbills data by context
     */
  batchQueryPlaybillContext (req: { list: { channelID: string, currentTime: number, preNumber: number, nextNumber: number }[] }): Promise<{
      list: { channelID: string, list: PlaybillLite[] }[]
    }> {

      const channelIDs = _.uniq(_.pluck(req.list, 'channelID'))
      const reqParam = _.first(req.list)
      const queryPlaybillConextRequest: QueryPlaybillContextRequest = {
          queryChannel: {
              channelIDs: channelIDs,
              channelFilter: { isCUTV: '1' }
            },
          queryPlaybillContext: {
              type: '0',
              date: '' + reqParam.currentTime,
              preNumber: '' + reqParam.preNumber,
              nextNumber: '' + reqParam.nextNumber,
              isFillProgram: '1'
            },
          needChannel: '0'
        }

      return this._queryPlaybillContext(queryPlaybillConextRequest, { params: { scene: 'EpgTVguide', 'SID': 'queryplaybillcontext2' } }).then((resp) => {
          resp = this.playbillAutoFillService.setPlaybillContextFillProgram(channelIDs, reqParam.currentTime,
                reqParam.preNumber, reqParam.nextNumber, resp, true)
          const ret = { list: [] }
          ret.list = _.map(resp.channelPlaybillContexts, (channelPlaybillContext: ChannelPlaybillContext) => {
              let prePlaybillList = channelPlaybillContext.prePlaybillLites ? channelPlaybillContext.prePlaybillLites : []
              let currentPlaybillLite = channelPlaybillContext.currentPlaybillLite ? channelPlaybillContext.currentPlaybillLite : []
              let nextPlaybillList = channelPlaybillContext.nextPlaybillLites ? channelPlaybillContext.nextPlaybillLites : []
              let allPlaybillLites = _.flatten([prePlaybillList, currentPlaybillLite, nextPlaybillList])
              let channelID
              if (!_.isEmpty(allPlaybillLites)) {
                  allPlaybillLites = _.sortBy(allPlaybillLites, (playbill: PlaybillLite) => {
                      return +playbill.startTime
                    })
                  channelID = _.first(allPlaybillLites).channelID
                }
              return { channelID: channelID, list: allPlaybillLites }
            })
          return ret
        })
    }

    /**
     * get playbilldetail
     */
  getPlaybillDetail (req: GetPlaybillDetailRequest): Promise<GetPlaybillDetailResponse> {
      return this._getPlaybillDetail(req)
    }

    /**
     * platform no support
     */
  private getMaxTimeSpan (requests: Array<{ channelID: string, beginTime: number, endTime: number }>): Array<number> {

      let minBeginTime: number = Number.MAX_VALUE
      let maxEndTime: number = Number.MIN_VALUE

      _.each(requests, (req) => {
          minBeginTime = Math.min(minBeginTime, req.beginTime)
          maxEndTime = Math.max(minBeginTime, req.endTime)
        })

      return [minBeginTime, maxEndTime]
    }
}
