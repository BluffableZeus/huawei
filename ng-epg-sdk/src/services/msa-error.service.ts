import { ErrorUtils } from './../utils/error-utils'
import { Logger } from './../utils/logger'
import { Injectable } from '@angular/core'
import { SQMErrorType, sqmService } from './../sdk/service/sqm.service'

const log = new Logger('msa-error.service')

/**
 * Manage the lock information of the current profile.
 * If you operate the lock of other profiles, request the interface directly
 */
@Injectable()
export class MSAErrorService {
    /**
     * Print MAS Error
     *
     * @param {string} code MSA internal error code
     *
     * @memberOf Logger
     */
  public printError (code: string, type = SQMErrorType.normal) {
      const err = ErrorUtils.getMSAError(code)

      log.error('[MSA_ERROR] %s', err.toString())

      sqmService.reportMSAError({
          errorType: type,
          errorCode: err.externalCode,
          msaCode: err.internalCode,
          errorMessage: err.message
        })
    }
}
