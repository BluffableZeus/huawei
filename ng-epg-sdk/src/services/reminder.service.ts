import { Reminder } from '../vsp/api'
import * as _ from 'underscore'
import { Injectable } from '@angular/core'
import {
    deleteReminder, DeleteReminderRequest, DeleteReminderResponse, queryReminder,
    QueryReminderResponse, CreateReminderRequest, createReminder, CreateReminderResponse
} from '../vsp'
import { VersionStore } from './version-store'
import { ContentUtils } from '../utils'
import { ContentIndexer } from './content-indexer'
import { HeartbeatService } from './heartbeat.service'
import { SessionService } from './session.service'
import { ChannelService } from './channel.service'
import { AjaxSuccess } from '../utils'
import { Logger } from '../utils/logger'
import { EventService } from './event.service'
const log = new Logger('reminder.service')

/**
 * Collection service class
 *
 * @export
 * @class ReminderService
 */
@Injectable()
export class ReminderService {

  private _store: VersionStore<QueryReminderResponse>
  private _queryReminder = queryReminder
  private _createReminder = createReminder
  private _deleteReminder = deleteReminder
  private reminderVersion: string
  private removeReminder = false

  private _indexer: ContentIndexer

  constructor (private _sessionService: SessionService, private _channelService: ChannelService) {
    }

  onNotification (eventName: string, data: any): any {
      switch (eventName) {
          case HeartbeatService.REMINDER_VERSION:
            return this.onReminderVersion(data as string)
          case 'AJAX_SUCCESS':
            return this.onAjaxSuccess(data)
        }
    }

  private initStore () {
      this._store = this._store || new VersionStore<QueryReminderResponse>('reminder', this._sessionService.getProfileID())
    }

  private onReminderVersion (version: string) {
      this.initStore()
      return this.refresh(version)
    }

  private onAjaxSuccess (data: AjaxSuccess) {
      switch (data.request.inf) {
          case 'Logout':
          case 'SwitchProfile':
            this._store = null
            break
        }
    }

  public saveAndIndex (resp: QueryReminderResponse, version: string): Promise<void> {
      resp.reminders = ContentUtils.sortContentList(resp.reminders, this._channelService)
      this._indexer = new ContentIndexer(resp.reminders)
      return this._store.save(version || null, resp)
    }

  addRemindersList (resp: QueryReminderResponse): Promise<void> {
      if (!resp) {
          return
        }
      return this.getStoreData().then((cachedResp) => {
          if (!cachedResp) {
              return
            }
          resp.reminders = _.uniq([...resp.reminders, ...cachedResp.reminders], (reminder) => {
              return reminder && reminder.playbill && reminder.playbill.ID
            })
          resp.reminders = ContentUtils.sortContentList(resp.reminders, this._channelService)
          this._indexer = new ContentIndexer(resp.reminders)
          return this._store.save(null, resp)
        })
    }

  refresh (version: string): Promise<any> {
      log.debug('begin refresh(), version:%s', version)
      this.reminderVersion = version
        // refresh data
      return this._store.getData(this.reminderVersion).then(cachedResp => {
          if (cachedResp) {
              this._indexer = new ContentIndexer(cachedResp.reminders)
            } else {
              return this._queryReminder({ queryType: '1', offset: '0', count: '50' }).then((resp: QueryReminderResponse) => {
                  this.saveAndIndex(resp, version)
                  log.debug('end refresh(), resp:%s', resp)
                })
            }

        })
    }

    /**
     * To determine whether the contents of the collection
     *
     * @param {string} id
     * @param {string} contentType
     * @returns {boolean}
     *
     */

  isRemindered (id: string, contentType: string): boolean {
      if (this._indexer) {
          return this._indexer.contains(id, contentType)
        }
      return false
    }

    /**
     * Add to Reminders
     *
     * @param {CreateReminderRequest} req
     * @returns {Promise<CreateReminderResponse>}
     *
     * @memberOf ReminderService
     */
  createReminder (req: CreateReminderRequest, playbillDetail): Promise<CreateReminderResponse> {
      log.debug('begin createReminder(), req:%s', req)
      return this.getStoreData().then((cachedResp) => {
          const oldCachedReminders = cachedResp.reminders
          return this._createReminder(req).then((resp) => {
              log.debug('end createReminder(), resp:%s', resp)
              cachedResp.reminders = oldCachedReminders.concat(_.map(req.reminders as any, (reminder: Reminder) => {
                  const ret = { contentType: reminder.contentType }
                  const name = ContentUtils.normalizeName(reminder.contentType)
                  ret[name] = playbillDetail
                  _.defaults(ret[name], {
                      reminder: {
                          contentID: playbillDetail.ID,
                          contentType: 'PROGRAM',
                          endTime: playbillDetail.endTime,
                          reminderTime: playbillDetail.startTime
                        },
                      channel: playbillDetail.channelDetail
                    })
                  return ret
                }))
              this.saveAndIndex(cachedResp, this._store.getVersion())
              EventService.emit('UPDATE_REMIND_STATUS', true)
              return resp
            }, (resp) => {
              log.debug('end createReminder(), resp:%s', resp)
              return Promise.reject(resp) as any
            })
        })
    }

  public getStoreData () {
      return this._store.getData().then((cachedResp) => {
            // filter out playbill endtime is overdue
          cachedResp.reminders = _.filter(cachedResp.reminders, (reminders) => {
              return +reminders.playbill.endTime > Date.now()
            })
            // sort by startTime
          cachedResp.reminders = _.sortBy(cachedResp.reminders, (item) => {
              return item && item.playbill && item.playbill.startTime
            })
          this.saveAndIndex(cachedResp, this._store.getVersion())
          return _.extend({ reminders: [] }, cachedResp)
        })
    }

    /**
     * Delete the collection
     *
     * @param {DeleteReminderRequest} req
     * @returns {Promise<DeleteReminderResponse>}
     *
     * @memberOf ReminderService
     */
  deleteReminder (req: DeleteReminderRequest, emitEvent = true): Promise<DeleteReminderResponse> {
      log.debug('begin deleteReminder(), req:%s', req)

      return this.getStoreData().then((cachedResp) => {
          const oldCachedReminders = cachedResp.reminders
          return this._deleteReminder(req).then((resp) => {
              log.debug('end deleteReminder(), resp:%s', resp)
              if (_.isEmpty(req.contentIDs) || _.isEmpty(req.contentTypes)) {
                    // delete all reminder
                  cachedResp.reminders = []
                } else {
                  if (req.deleteType === '1') {
                        // old reminders: no delete items
                      cachedResp.reminders = _.filter(oldCachedReminders, (reminderContent: any) => {
                          const name = ContentUtils.normalizeName(reminderContent.contentType)
                          const isFounded = !!_.find(req.contentIDs, (id: string, index: number) => {
                              return ContentUtils.normalizeName(ContentUtils.getContentType(reminderContent)) === ContentUtils.normalizeName(req.contentTypes[index])
                                    && ContentUtils.getID(reminderContent[name]) === id
                            })

                          return isFounded
                        })
                    } else {
                        // old reminders subtract delete
                      cachedResp.reminders = _.reject(oldCachedReminders, (reminderContent: any) => {
                          const name = ContentUtils.normalizeName(reminderContent.contentType)
                          const isFounded = !!_.find(req.contentIDs, (id: string, index: number) => {
                              return ContentUtils.normalizeName(ContentUtils.getContentType(reminderContent)) === ContentUtils.normalizeName(req.contentTypes[index])
                                    && ContentUtils.getID(reminderContent[name]) === id
                            })

                          return isFounded
                        })
                    }
                }
              this.saveAndIndex(cachedResp, this._store.getVersion())
              if (emitEvent) {
                  EventService.emit('UPDATE_REMIND_STATUS', false)
                }
              return resp
            }, (resp) => {
              log.debug('end deleteReminder(), resp:%s', resp)
              return Promise.reject(resp) as any
            })
        })
    }

  getRemoveReminder () {
      return this.removeReminder
    }

  setRemoveReminder (status: boolean) {
      this.removeReminder = status
    }

}
