import * as _ from 'underscore'
import { ContentUtils, CONTENTTYPE_TO_NAME_MAPPING } from '../utils'
import { Content, ChannelDetail, PlaybillDetail, Subject, Genre, LockSeries } from '../vsp/api'

/**
 * Used to construct the index of the Content list
 *
 * @export
 * @class ContentIndexer
 */
export class ContentIndexer {

    /**
       * A list of content organized by type
       *
       * @type {{
       *         all: Array<any>, channel?: Array<any>, VOD?: Array<any>, playbill?: Array<any>, subject?: Array<any>, genre?: Array<any>, lockSeries?: Array<any>
       *     }}
       * @memberOf ContentIndexer
       */
  group: {
      all: Array<any>,
      channel?: Array<any>,
      VOD?: Array<any>, playbill?:
        Array<any>, subject?: Array<any>,
      genre?: Array<any>,
      lockSeries?: Array<any>
    }

    /**
     *
     * Content ID index organized by type
     *
     * @type {{
     *         channel?: { [id: string]: any }, VOD?: { [id: string]: any }, playbill?: { [id: string]: any }, subject:
     * { [id: string]: any }, genre?: { [id: string]: any }, lockSeries?: { [id: string]: any }
     *     }}
     * @memberOf ContentIndexer
     */
  indexer: {
      channel?: { [id: string]: any },
      VOD?: { [id: string]: any },
      playbill?: { [id: string]: any },
      subject: { [id: string]: any },
      genre?: { [id: string]: any },
      lockSeries?: { [id: string]: any }
    }

  constructor (list: Array<Content>) {
      this.buildIndex(list)
    }

  private buildIndex (list: Array<Content>) {

      const flattenList = _.compact(ContentUtils.flatten(list))

      const group = _.groupBy(flattenList, (item: any) => {
          return ContentUtils.normalizeName(ContentUtils.getContentType(item))
        })

      group['all'] = flattenList

      _.each(CONTENTTYPE_TO_NAME_MAPPING as any, (name: string) => {
          group[name] = group[name] || []
        })

      const indexer = {}
      _.each(group, (items, name) => {
          indexer[name] = _.indexBy(items, 'genre' === name ? 'genreID' : 'ID')
        })

      this.group = group as any
      this.indexer = indexer as any
    }

    /**
     * According to the ID and type to determine whether the content exists
     *
     * @param {string} id
     * @param {string} contentType
     * @returns
     *
     * @memberOf ContentIndexer
     */
  contains (id: string, contentType: string) {
      const name = ContentUtils.normalizeName(contentType) || contentType
      return name && this.indexer && this.indexer[name] && !!this.indexer[name][id]
    }

    /**
     * Returns the content list by type
     *
     * @param {string} contentType
     * @returns
     *
     * @memberOf ContentIndexer
     */
  getList (contentType = 'all'): Array<ChannelDetail | PlaybillDetail | Subject | Genre | LockSeries> {
      const name = ContentUtils.normalizeName(contentType) || contentType
      let groupList = []
      if (name) {
          groupList = this.group[name]
        } else {
          groupList = this.group.all
        }
      groupList = _.uniq(groupList, (content) => {
          return content && content.ID
        })
      return groupList
    }

    /**
     * Get content
     *
     * @returns
     *
     * @memberOf ContentIndexer
     */
  getTotalCount () {
      return this.group.all.length
    }
}
