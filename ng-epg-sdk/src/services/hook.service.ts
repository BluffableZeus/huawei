import { Injectable } from '@angular/core'
import { Hook } from '../hooks'
import * as _ from 'underscore'

/**
 *
 * Used for functional dynamic injection
 *
 */
@Injectable()
export class HookService {

  private static _instance

  private _hookByName: { [hookName: string]: Array<Function> } = {}

  constructor () {
      HookService._instance = this
    }

    /**
     *
     * @param {string} hookName
     * @param {*} data
     * @param {(data?: any) => any} cb
     *
     * @memberOf HookService
     */
  invoke (hookName: string | string[], data: any, cb: (data?: any) => any) {

      let hookList: Function[]
      if ('string' === typeof hookName) {
          hookList = this._hookByName[hookName] || []
        } else {
          hookList = _.flatten(_.map(hookName, (name: string) => {
              return this._hookByName[name] || []
            }))
        }

      hookList = hookList.slice()
      hookList.push(cb)

      const exec = (req, i) => {
          return hookList[i](req, (_req) => {
              return exec(_req, i + 1)
            }, hookName)
        }

      return exec(data, 0)
    }

    /**
     * register Hook
     *
     * @param {string} hookName
     * @param {Hook} hook
     *
     * @memberOf HookService
     */
  registerHook (hookName: string, hook: Hook<any, any>) {
      this.on(hookName, hook.onHook.bind(hook))
    }

    /**
     *
     * @param {string} hookName
     * @param {(data: any, next: Function) => any} hookFunc
     *
     * @memberOf HookService
     */
  on (hookName: string, hookFunc: (data: any, next: Function, hookName: string) => any) {
      this._hookByName[hookName] = this._hookByName[hookName] || []
      this._hookByName[hookName].push(hookFunc)
    }
}
