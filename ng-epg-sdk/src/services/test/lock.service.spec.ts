import { ChannelService, HeartbeatService, SessionService, LockService } from '../'
import { QueryLockResponse } from '../../vsp/api'
import { Channel } from '../../vsp/api'

const channel: Channel = { ID: '1', name: '1', contentType: 'AUDIO_CHANNEL', channelNO: '1', currentPlaybill: undefined }

describe('lock service', () => {
  let lockService: LockService
  let queryLockResponse: QueryLockResponse

  beforeEach(() => {
      lockService = new LockService(new SessionService(), new ChannelService(new HeartbeatService(), new SessionService()))
      queryLockResponse = {} as QueryLockResponse
      lockService['initStore']()

      localStorage.clear()
    })

  it('OnNotification', () => {
      spyOn(lockService, 'refresh')
      lockService.onNotification('LOCK_VERSION', '1')

      expect(lockService.refresh).toHaveBeenCalled()
    })

  it('get lock list from indexer', (done) => {
      expect(lockService.isLocked('1', 'AUDIO_CHANNEL')).not.toBeTruthy()

      queryLockResponse.locks = [{ contentType: 'AUDIO_CHANNEL', channel: channel }] as any
      lockService['saveAndIndex']('1', queryLockResponse).then(() => {
          expect(lockService.getLockList('channel').length).toBe(1)
          expect(lockService.isLocked('1', 'channel')).toBeTruthy()
        }).then(done, fail)

    })

  it('refresh with cache', (done) => {
      spyOn(lockService['_store'], 'getData').and.returnValue(Promise.resolve(undefined))
      spyOn(lockService['_preStore'], 'getData').and.returnValue(Promise.resolve(undefined))
      spyOn<any>(lockService, '_queryLock').and.returnValue(Promise.resolve())

      spyOn<any>(lockService, 'saveAndIndex').and.returnValue(Promise.resolve())
      spyOn<any>(lockService, 'savePreLock').and.returnValue(Promise.resolve())

      lockService.refresh('1-1').then(() => {
          expect(lockService['_queryLock']).toHaveBeenCalled()
          expect(lockService['saveAndIndex']).not.toHaveBeenCalled()
          expect(lockService['savePreLock']).not.toHaveBeenCalled()
        }).then(done, fail)
    })

  it('refresh without cache', (done) => {
      const resp = { locks: [] }
      spyOn(lockService['_store'], 'getData').and.returnValue(Promise.resolve(resp))
      spyOn(lockService['_preStore'], 'getData').and.returnValue(Promise.resolve(resp))
      spyOn<any>(lockService, '_queryLock').and.returnValue(Promise.resolve())

      spyOn<any>(lockService, 'saveAndIndex').and.returnValue(Promise.resolve())
      spyOn<any>(lockService, 'savePreLock').and.returnValue(Promise.resolve())

      lockService.refresh('1-1').then(() => {
          expect(lockService['_queryLock']).not.toHaveBeenCalled()
          expect(lockService['saveAndIndex']).not.toHaveBeenCalled()
          expect(lockService['savePreLock']).not.toHaveBeenCalled()
        }).then(done, fail)
    })
})
