import { PlaybillLite } from '../../vsp/api'
import { QueryPlaybillListResponse } from '../../vsp/api'
import { ChannelPlaybill } from '../../vsp/api'
import { QueryPlaybillContextResponse } from '../../vsp/api'
import { ChannelPlaybillContext } from '../../vsp/api'
import { PlaybillService } from '../playbill.service'
import * as  _ from 'underscore'

describe('PlaybillService', () => {

  let playbillService: PlaybillService

  function setPlaybillLiteProp (id: string, start: string) {
      let playbillLite = {} as PlaybillLite
      playbillLite.channelID = id
      playbillLite.startTime = start
      return playbillLite
    }

  beforeEach(() => {
      playbillService = new PlaybillService(null)
    })

  it('batchQueryPlaybillList test', () => {
      let response = {} as QueryPlaybillListResponse
      response.channelPlaybills = [{ channelDetail: {}, playbillLites: [{ ID: '1', channelID: '11' }, { ID: '2', channelID: '11' }] } as ChannelPlaybill]

      spyOn(playbillService, '_queryPlaybillList').and.returnValue(Promise.resolve(response))

      playbillService.batchQueryPlaybillList({ list: [{ channelID: '11', beginTime: 1111, endTime: 2222 }] }).then((resp) => {
          expect(resp.list).toEqual([{ channelID: '11', list: [{ ID: '1', channelID: '11' }, { ID: '2', channelID: '11' }] }])
        })
    })

  it('batchQueryPlaybillContext test, allPlaybillLites is empty', () => {
      let response = {} as QueryPlaybillContextResponse
      response.channelPlaybillContexts = [{ channelDetail: {} } as ChannelPlaybillContext]

      spyOn(playbillService, '_queryPlaybillContext').and.returnValue(Promise.resolve(response))

      playbillService.batchQueryPlaybillContext({ list: [{ channelID: '11', currentTime: Date.now(), preNumber: 0, nextNumber: 2 }] }).then((resp) => {
          expect(resp.list[0].list.length).toBe(0)
        })
    })

  it('batchQueryPlaybillContext test, allPlaybillLites is not empty', () => {
      let channelPlaybillContext = {} as ChannelPlaybillContext
      channelPlaybillContext.prePlaybillLites = [setPlaybillLiteProp('11', '2')]
      channelPlaybillContext.currentPlaybillLite = setPlaybillLiteProp('11', '1')
      channelPlaybillContext.nextPlaybillLites = [setPlaybillLiteProp('11', '3')]
      let response = {} as QueryPlaybillContextResponse
      response.channelPlaybillContexts = [channelPlaybillContext]

      spyOn(playbillService, '_queryPlaybillContext').and.returnValue(Promise.resolve(response))

      playbillService.batchQueryPlaybillContext({ list: [{ channelID: '11', currentTime: Date.now(), preNumber: 1, nextNumber: 1 }] }).then((resp) => {
          expect(resp.list[0].channelID).toBe('11')
          _.each(resp.list[0].list, (playbillLite, index) => {
              expect(playbillLite.startTime).toBe(index + 1 + '')
            })
        })
    })
})
