import { CustomizeConfigService } from '../customize-config.service'
import { session } from '../../utils'
declare const spyOn
describe('customize-config.service', () => {

  let service: CustomizeConfigService

  beforeEach(() => {
      service = new CustomizeConfigService()
    })

  it('queryRatingSystemList succuss', () => {
      spyOn(session, 'get').and.returnValue({ ratings: [1, 2] })

      const list = service.queryRatingSystemList()
      expect(list).toEqual([1, 2] as any)
    })

})
