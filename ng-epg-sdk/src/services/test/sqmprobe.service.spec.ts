import { NgZone } from './../../@angular/core/index'
import { SQMProbeService } from '../sqmprobe.service'
import { AjaxSuccess } from '../../utils/ajax'

describe('SQMProbeService', () => {

  let sqmprobeService: SQMProbeService

  const event: AjaxSuccess = {
      request: {
          inf: 'QueryRecmVODList',
          url: 'http://127.0.0.1:8080/QueryRecmVODList'
        },
      response: {
          data: {
              result: { retCode: 'http.500' }
            },
          usedTime: 1000
        }
    }

  beforeEach(() => {
      sqmprobeService = new SQMProbeService(new NgZone())
    })

  it('sendToSQM test, no action', () => {
      event.request.inf = ''
      event.request.url = ''
      spyOn(sqmprobeService, '_ajax')
      sqmprobeService['sendToSQM'](event)

      expect(sqmprobeService['_ajax']).not.toHaveBeenCalled()
    })

  it('sendToSQM test, have action', () => {
      event.request.inf = 'QueryRecmVODList'
      event.request.url = 'http://127.0.0.1:8080/QueryRecmVODList'
      spyOn(sqmprobeService, '_ajax').and.returnValue({ catch: () => { } })
      sqmprobeService['sendToSQM'](event)

      expect(sqmprobeService['_ajax']).toHaveBeenCalled()
    })
})
