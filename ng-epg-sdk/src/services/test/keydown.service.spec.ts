import { KeydownService } from '../keydown.service'
import { Config } from '../../core'
import { NgZone } from '@angular/core'
import { Subject } from 'rxjs/Subject'

describe('keydown.service test', () => {

  let service: KeydownService
  let keydownSubject: Subject<any>
  let keyupSubject: Subject<any>

  beforeEach(() => {
      Config.set('longpress_milliseconds', 200)
      Config.set('shortpress_delay_milliseconds', 100)
      service = new KeydownService(new NgZone())
      keydownSubject = new Subject()
      keyupSubject = new Subject()
      service['keydownObservable'] = keydownSubject
      service['keyupObservable'] = keyupSubject

    })

  it('is long key press false', () => {
      service['isPressing'] = true
      service['keydownTime'] = Date.now() - 190
      expect(service.isLongPress()).toBeFalsy()
    })

  it('is long key press true', () => {
      service['isPressing'] = true
      service['keydownTime'] = Date.now() - 210
      expect(service.isLongPress()).toBeTruthy()
    })

  it('The first key KeyUp is a long press', (done) => {

      let count = 0
      const cb = service.createLongPressCallback(() => {
          count++
        })

      cb()

        // Long press
      setTimeout(function () {
          keyupSubject.next()
          expect(count).toEqual(2)
          done()
        }, 210)
    })

  it('Second press, short press time <interval time <long press time', (done) => {

      let count = 0
      const cb = service.createLongPressCallback(() => {
          count++
        })

      cb()
      keyupSubject.next()
      expect(count).toEqual(1)

      setTimeout(function () {
          cb()
          keyupSubject.next()
          expect(count).toEqual(2)
          done()
        }, 110)
    })

  xit('Second key, interval time <short press time', (done) => {

      let count = 0
      let value = 0

      const cb = service.createLongPressCallback((num) => {
          count++
          value = num
        })

      cb(1)
      keyupSubject.next()
      expect(count).toEqual(1)
      expect(value).toEqual(1)

        // Less than short press
      setTimeout(function () {
          cb(2)
          cb(3)
          cb(4)
          expect(count).toEqual(1)
          expect(value).toEqual(1)
        }, 90)

      setTimeout(function () {
          keyupSubject.next()
          expect(count).toEqual(1)
          expect(value).toEqual(1)
        }, 110)

      setTimeout(function () {
          expect(count).toEqual(2)
          expect(value).toEqual(4)
          done()
        }, 191)
    })

  it('Second press, long press time <interval time', (done) => {

      let count = 0
      const cb = service.createLongPressCallback((num) => {
          count = num
        })

      cb(1)
      expect(count).toEqual(1)
      keyupSubject.next()

      setTimeout(function () {
          cb(2)
          keyupSubject.next()
          expect(count).toEqual(2)
          done()
        }, 210)
    })

})
