import { EventEmitter2, Observer } from './../event.service'

describe('EventEmitter test', () => {

  let eventEmitter: EventEmitter2
  beforeEach(() => {
      eventEmitter = new EventEmitter2()
    })

  it('listNotificationInterests, receive onNotification', () => {

      const observer = {
          listNotificationInterests: () => ['A'],
          onNotification: () => { }
        }

      spyOn(observer, 'onNotification').and.returnValue(null)

      eventEmitter.registerObserver(observer)
      eventEmitter.emit('A', 'TEST')

      expect(observer.onNotification).toHaveBeenCalledWith('A', 'TEST')
    })

  it('listNotificationInterests, not reveive event', () => {

      const observer = {
          listNotificationInterests: () => ['A'],
          onNotification: () => { }
        }

      spyOn(observer, 'onNotification').and.returnValue(null)

      eventEmitter.registerObserver(observer)
      eventEmitter.emit('B', 'TEST')

      expect(observer.onNotification).not.toHaveBeenCalled()
    })

  it('listNotificationInterests, unregister', () => {

      const observer = {
          listNotificationInterests: () => ['A'],
          onNotification: () => { }
        }

      spyOn(observer, 'onNotification').and.returnValue(null)

      eventEmitter.registerObserver(observer)
      eventEmitter.unRegisterObserver(observer)

      eventEmitter.emit('A', 'TEST')
      expect(observer.onNotification).not.toHaveBeenCalled()
    })

  it('listNotificationInterests, receive all event', () => {

      const observer = {} as Observer
      observer.onNotification = () => { }

      spyOn(observer, 'onNotification').and.returnValue(null)

      eventEmitter.registerObserver(observer)

      eventEmitter.emit('A', 'TEST')
      expect(observer.onNotification).toHaveBeenCalledWith('A', 'TEST')
    })
})
