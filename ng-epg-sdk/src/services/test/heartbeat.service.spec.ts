import { PersonalDataVersion } from '../../vsp/api'
import { EventService } from '../event.service'
import { HeartbeatService } from '../heartbeat.service'

describe('heartbeat service', () => {
  const personalDataVersion: PersonalDataVersion = {
      channel: '1|1',
      bookmark: '1',
      favorite: '1',
      lock: '1',
      extensionFields: []
    }
  const lastVerionInfo = {
      CHANNEL_VERSION: '1|1',
      BOOKMARK_VERSION: '1',
      FAVORITE_VERSION: '1',
      LOCK_VERSION: '1',
      CHANNEL_STATIC_VERSION: '1',
      CHANNEL_DYNAMIC_VERSION: '1|1',
      PREFAVORITE_VERSION: '0',
      PRELOCK_VERSION: '0',
      REMINDER_VERSION: '0',
      PROFILE_VERSION: '0'
    }

  let heartbeatService: HeartbeatService

  beforeEach(() => {
      heartbeatService = new HeartbeatService()

      spyOn(Promise, 'all').and.returnValue(Promise.resolve({}))
      spyOn(EventService, 'emit')
    })

  it('processPersonalDataVersion changed', (done) => {
      heartbeatService['processPersonalDataVersion'](personalDataVersion).then(() => {
          expect(EventService.emit).toHaveBeenCalledWith('PERSONAL_DATA_VERSION_CHANGED')
          expect(heartbeatService['_lastVersionInfo']).toEqual(lastVerionInfo)
          done()
        })
    })

  it('processPersonalDataVersion not change', (done) => {
      heartbeatService['_lastVersionInfo'] = lastVerionInfo
      heartbeatService['processPersonalDataVersion'](personalDataVersion).then(() => {
          expect(EventService.emit).not.toHaveBeenCalledWith('PERSONAL_DATA_VERSION_CHANGED')
          done()
        })
    })
})
