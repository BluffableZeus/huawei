import { ContentIndexer } from '../content-indexer'

describe('content-indexer', () => {

  it('buildIndex', () => {
      const list = []
      list.push({
          contentType: 'VIDEO_CHANNEL',
          channel: { contentType: 'VIDEO_CHANNEL', ID: 0 }
        })

      list.push({
          contentType: 'VIDEO_CHANNEL',
          channel: { contentType: 'VIDEO_CHANNEL', ID: 1 }
        })

      list.push({
          contentType: 'AUDIO_CHANNEL',
          channel: { contentType: 'AUDIO_CHANNEL', ID: 2 }
        })

      list.push({
          contentType: 'VIDEO_VOD',
          VOD: { contentType: 'VIDEO_VOD', ID: 3 }
        })

      list.push({
          contentType: 'AUDIO_VOD',
          VOD: { contentType: 'AUDIO_VOD', ID: 4 }
        })

      list.push({
          contentType: 'SUBJECT',
          subject: { contentType: 'SUBJECT', ID: 5 }
        })

      const indexer = new ContentIndexer(list)
      expect(indexer.group.channel).toEqual([{ contentType: 'VIDEO_CHANNEL', ID: 0 }, { contentType: 'VIDEO_CHANNEL', ID: 1 }, { contentType: 'AUDIO_CHANNEL', ID: 2 }])
      expect(indexer.group.subject).toEqual([{ contentType: 'SUBJECT', ID: 5 }])

      expect(indexer.indexer.VOD[3]).toEqual({ contentType: 'VIDEO_VOD', ID: 3 })
      expect(indexer.group.genre).toEqual([])

      expect(indexer.group.all.length).toEqual(6)
    })

})
