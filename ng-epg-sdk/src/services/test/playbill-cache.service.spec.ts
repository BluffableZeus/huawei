import { PlaybillCacheService } from './../playbill-cache.service'
import * as _ from 'underscore'

describe('playbill-cache.service', () => {

  let service: PlaybillCacheService
  beforeEach(() => {
      service = new PlaybillCacheService()
    })

  it('sortByLastPlayedChannel', () => {
      const itemList = _.times(12, i => i)

      expect(service.getPageList(itemList, 8, 6)).toEqual([
            [6, 7, 8, 9, 10, 11],
            [0, 1, 2, 3, 4, 5]
        ])

      expect(service.getPageList(itemList, 8, 5)).toEqual([
            [5, 6, 7, 8, 9],
            [10, 11],
            [0, 1, 2, 3, 4]
        ])

      expect(service.getPageList(itemList, 8, 4)).toEqual([
            [8, 9, 10, 11],
            [4, 5, 6, 7],
            [0, 1, 2, 3]
        ])

      expect(service.getPageList(itemList, 8, 3)).toEqual([
            [6, 7, 8],
            [9, 10, 11],
            [3, 4, 5],
            [0, 1, 2]
        ])

      expect(service.getPageList(itemList, 8, 2)).toEqual([
            [8, 9],
            [10, 11],
            [6, 7],
            [4, 5],
            [2, 3],
            [0, 1]
        ])
    })
})
