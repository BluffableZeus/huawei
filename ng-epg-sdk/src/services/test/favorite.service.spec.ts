import { FavoriteService, SessionService, ChannelService, HeartbeatService } from '../'
import { VOD } from '../../vsp/api'
import { QueryFavoriteResponse, CreateFavoriteRequest, DeleteFavoriteRequest } from '../../vsp/api'

describe('favorite service', () => {

  let favoriteService: FavoriteService
  let localFavoriteResponse: QueryFavoriteResponse

  beforeEach(() => {
      favoriteService = new FavoriteService(new SessionService(), new ChannelService(new HeartbeatService(), new SessionService()))
      favoriteService['initStore']()
      localFavoriteResponse = {} as QueryFavoriteResponse

      localStorage.clear()
    })

  it('create favorite resolved', (done) => {
      localFavoriteResponse.favorites = []
      spyOn<any>(favoriteService, '_createFavorite').and.returnValue(Promise.resolve({}))

      favoriteService['saveAndIndex']('1', localFavoriteResponse).then(() => {

          let request = { favorites: [{ contentID: '1', contentType: 'VIDEO_VOD' }] } as CreateFavoriteRequest
          return favoriteService.createFavorite(request).then(() => {
              expect(favoriteService['_indexer'].group.VOD.length).toBe(1)
            })
        }).then(done, fail)

    })

  it('create favorite rejected', (done) => {
      spyOn(favoriteService, 'createFavorite').and.returnValue(Promise.reject({}))
      favoriteService['saveAndIndex']('1', localFavoriteResponse).then(() => {

          let request = { favorites: [{ contentID: '1', contentType: 'VIDEO_VOD' }] } as CreateFavoriteRequest
          return favoriteService.createFavorite(request).then(null, () => {
              expect(favoriteService['_indexer'].group.VOD.length).toBe(0)
            })

        }).then(done, fail)

    })

  const vod: VOD = { ID: '1', name: 'vod-1', contentType: 'VIDEO_VOD' } as any

  it('delete favorite resolved', (done) => {
      localFavoriteResponse.favorites = [{ contentType: 'VIDEO_VOD', VOD: vod }]
      let request = { contentTypes: ['VIDEO_VOD'], contentIDs: ['1'] } as DeleteFavoriteRequest
      spyOn<any>(favoriteService, '_deleteFavorite').and.returnValue(Promise.resolve({}))

      favoriteService['saveAndIndex']('1', localFavoriteResponse).then(() => {
          return favoriteService.deleteFavorite(request).then(() => {
              expect(favoriteService['_indexer'].group.VOD.length).toBe(0)
            })
        }).then(done, fail)
    })

  it('delete favorite rejected', (done) => {
      localFavoriteResponse.favorites = [{ contentType: 'VIDEO_VOD', VOD: vod }]

      spyOn(favoriteService, 'deleteFavorite').and.returnValue(Promise.reject({}))

      favoriteService['saveAndIndex']('1', localFavoriteResponse).then(() => {
          let request = { contentTypes: ['VIDEO_VOD'], contentIDs: ['1'] } as DeleteFavoriteRequest
          return favoriteService.deleteFavorite(request).then(null, () => {
              expect(favoriteService['_indexer'].group.VOD.length).toBe(1)
            })
        }).then(done, fail)

    })

  it('get favorite list from indexer', (done) => {
      expect(favoriteService.isFavorited('1', 'VIDEO_VOD')).not.toBeTruthy()

      localFavoriteResponse.favorites = [{ contentType: 'VIDEO_VOD', VOD: vod }]
      favoriteService['saveAndIndex']('1', localFavoriteResponse).then(() => {
          expect(favoriteService.getFavoriteList('VOD').length).toBe(1)
          expect(favoriteService.isFavorited('1', 'VOD')).toBeTruthy()
        }).then(done, fail)
    })

  it('onNotification test', () => {
      spyOn(favoriteService, 'refresh')
      favoriteService.onNotification('FAVORITE_VERSION', '1')

      expect(favoriteService.refresh).toHaveBeenCalled()
    })
})
