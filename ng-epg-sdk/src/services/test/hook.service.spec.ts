import { HookService } from '../hook.service'

describe('hook service test', () => {

  let service: HookService
  beforeEach(() => {
      jasmine.clock().install()
      service = new HookService()
    })

  afterEach(function () {
      jasmine.clock().uninstall()
    })

  it('passthrough', () => {
      service.on('test', (data, next) => {
          return next(data)
        })

      const result = service.invoke('test', 1, (data: number) => {
          return data + 1
        })

      expect(result).toEqual(2)
    })

  it('intecept', () => {
      service.on('test', (data, next) => {
          return 4
        })

      const result = service.invoke('test', 1, (data: number) => {
          return data + 1
        })

      expect(result).toEqual(4)
    })

  it('change parameter', () => {
      service.on('test', (data, next) => {
          return next(10)
        })

      const result = service.invoke('test', 1, (data: number) => {
          return data + 1
        })

      expect(result).toEqual(11)
    })

  xit('passthrough promise', () => {
      service.on('test', (data, next) => {
          return next(data)
        })

      let ret
      service.invoke('test', 1, (data: number) => {
          return new Promise((resolve) => {
              setTimeout(() => {
                  resolve(++data)
                }, 100)
            })
        }).then((resp) => {
          ret = resp
        })
      jasmine.clock().tick(101)
      expect(ret).toEqual(2)
    })

  xit('intecept promise', () => {
      service.on('test', (data, next) => {
          return new Promise((resolve) => {
              setTimeout(() => {
                  resolve(12)
                }, 100)
            })
        })

      let ret
      service.invoke('test', 1, (data: number) => {
          return new Promise((resolve) => {
              setTimeout(() => {
                  resolve(++data)
                }, 100)
            })
        }).then((resp) => {
          ret = resp
        })
      jasmine.clock().tick(101)
      expect(ret).toEqual(12)
    })

  xit('change parameter promise', () => {
      service.on('test', (data, next) => {
          return new Promise((resolve) => {
              setTimeout(() => {
                  next(12).then(resolve)
                }, 100)
            })
        })

      let ret
      service.invoke('test', 1, (data: number) => {
          return new Promise((resolve) => {
              setTimeout(() => {
                  resolve(++data)
                }, 100)
            })
        }).then((resp) => {
          ret = resp
        })
      jasmine.clock().tick(201)
      expect(ret).toEqual(13)
    })
})
