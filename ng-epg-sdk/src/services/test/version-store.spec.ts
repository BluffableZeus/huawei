import { VersionStore } from '../version-store'

describe('VersionStore', () => {

  let store: VersionStore<any>
  beforeEach((done) => {
      store = new VersionStore('test', '')
      store.clear().then(done)
    })

  it('save new version resp & getData', (done) => {

      store.save('1', 1).then(() => {
          return store.getData()
        }).then((resp) => {
          expect(resp).toEqual(1)
        }).then(done, fail)
    })

  it('save lower version resp & getData', (done) => {
      store.save('1', 1).then(() => {
          return store.save('0', 0)
        }).then(() => {
          return store.getData()
        }).then((resp) => {
          expect(resp).toEqual(0)
        }).then(done, fail)
    })

  it('save greater version resp & getData', (done) => {
      store.save('1', 1).then(() => {
          return store.save('2', 2)
        }).then(() => {
          return store.getData()
        }).then((resp) => {
          expect(resp).toEqual(2)
        }).then(done, fail)
    })

  it('change profileid', (done) => {
      store.save('1', 1).then(() => {
          store.setProfileID('profileID')
        }).then(() => {
          return store.getData()
        }).then((resp) => {
          expect(resp).toBeUndefined()
        }).then(done, fail)
    })

  it('getKey', () => {
      expect(store['getKey']()).toEqual('test_')
      store.setProfileID('testprofile')
      expect(store['getKey']()).toEqual('test_testprofile')
    })

  it('getResp', () => {
      let resp = store['getResp']('1', {
          version: '1', key: 'test_', resp: 1
        })
      expect(resp).toEqual(1)

      resp = store['getResp']('1', {
          version: '0', key: 'test_', resp: 1
        })
      expect(resp).toBeUndefined()

      resp = store['getResp']('1', {
          version: '1', key: 'test_p', resp: 1
        })
      expect(resp).toBeUndefined()
    })

})
