import { ChannelService, HeartbeatService } from '../'
import { QueryAllChannelResponse } from '../../vsp/api'
import { QueryAllChannelDynamicPropertiesResponse } from '../../vsp/api'
import { ContentRight } from '../../vsp/api'
import { SessionService } from '../session.service'

const constQueryStaticResp: QueryAllChannelResponse = {
  result: { retCode: '0', retMsg: '' },
  channelVersion: '1',
  channelDetails: [{
      ID: '1', channelNO: '11', name: 'channel-1', contentType: 'AUDIO_CHANNEL', SLSType: '0', isPPV: '0',
      rating: { ID: '1' }, scoreTimes: '0', averageScore: '0', deviceTypes: [], genres: [], customFields: [],
      isCUTVDependonLivetv: '0', isLocked: '0', physicalChannels: [], hasPIP: '0'
    }]
} as any

const constContentRight: ContentRight = { enable: '0', isSubscribed: '0' }

const constQueryDynamicResp: QueryAllChannelDynamicPropertiesResponse = {
  result: { retCode: '0', retMsg: '' },
  channelDynamaicProp: [{
      ID: '1', channelNO: '11', isLocked: '0',
      physicalChannelsDynamicProperties: [{ ID: '111', btvCR: constContentRight, pltvCR: constContentRight, cutvCR: constContentRight }]
    }],
  total: '10'
}

describe('ChannelService', () => {

  let channelService: ChannelService

  beforeEach((done) => {
      channelService = new ChannelService(new HeartbeatService(), new SessionService())

      spyOn<any>(channelService, 'getUserFilter').and.returnValue('')
      spyOn<any>(channelService, 'queryAllChannelDynamicProperties').and.returnValue(Promise.resolve(constQueryDynamicResp))
      spyOn<any>(channelService, 'queryAllChannel').and.returnValue(Promise.resolve({}))

      channelService['initStore']()
      Promise.all([channelService['staticStore'].clear(), channelService['dynamicStore'].clear()]).then(done, done)
    })

  it('refresh', () => {
      spyOn<any>(channelService, 'refreshStatic')
      spyOn<any>(channelService, 'refreshDynamic')

      channelService.refresh('1|1')

      expect(channelService.refreshStatic).toHaveBeenCalledWith('1')
      expect(channelService.refreshDynamic).toHaveBeenCalledWith('1')
    })

  it('refreshStatic with cache', (done) => {
      spyOn<any>(channelService, 'userFilterChanged').and.returnValue(Promise.resolve(false))
      spyOn<any>(channelService['staticStore'], 'getData').and.returnValue(Promise.resolve(constQueryStaticResp))
      spyOn<any>(channelService, 'buildIndexer').and.returnValue(Promise.resolve())

      channelService.refreshStatic('1').then(() => {
          expect(channelService['queryAllChannel']).not.toHaveBeenCalled()
          expect(channelService['buildIndexer']).toHaveBeenCalled()
        }).then(done, fail)
    })

  it('refreshStatic without cache', (done) => {
      spyOn<any>(channelService['staticStore'], 'getData').and.returnValue(Promise.resolve())
      spyOn<any>(channelService, 'buildIndexer').and.returnValue(Promise.resolve())

      channelService.refreshStatic('1').then(() => {
          expect(channelService['queryAllChannel']).toHaveBeenCalled()
          expect(channelService['buildIndexer']).toHaveBeenCalled()
        }).then(done, fail)
    })

  it('refreshDynamic with cache', (done) => {
      spyOn<any>(channelService['dynamicStore'], 'getData').and.returnValue(Promise.resolve(constQueryDynamicResp))
      spyOn<any>(channelService, 'buildIndexer').and.returnValue(Promise.resolve())

      channelService.refreshDynamic('1').then(() => {
          expect(channelService['queryAllChannelDynamicProperties']).not.toHaveBeenCalled()
          expect(channelService['buildIndexer']).toHaveBeenCalled()
        }).then(done, fail)
    })

  it('refreshDynamic without cache', (done) => {
      spyOn<any>(channelService['dynamicStore'], 'getData').and.returnValue(Promise.resolve())
      spyOn<any>(channelService, 'buildIndexer').and.returnValue(Promise.resolve())

      channelService.refreshDynamic('1').then(() => {
          expect(channelService['queryAllChannelDynamicProperties']).toHaveBeenCalled()
          expect(channelService['buildIndexer']).toHaveBeenCalled()
        }).then(done, fail)
    })

  it('buildIndex', (done) => {
      spyOn<any>(channelService['staticStore'], 'getData').and.returnValue(Promise.resolve(constQueryStaticResp))
      spyOn<any>(channelService['dynamicStore'], 'getData').and.returnValue(Promise.resolve(constQueryDynamicResp))

      expect(channelService.getChannelByID('1')).toBeUndefined()
      channelService['buildIndexer']().then(() => {
          expect(channelService.getChannelByID('1')).toBeDefined()
        }).then(done, fail)
    })
})
