import { AjaxResponse, AjaxRequest } from './../utils/ajax'
import * as _ from 'underscore'
import { Injectable } from '@angular/core'
import {
    queryAllChannel, QueryAllChannelResponse, OnLineHeartbeatResponse,
    queryAllChannelDynamicProperties, QueryAllChannelDynamicPropertiesResponse
} from '../vsp/api'
import { ChannelDynamicProperties, PhysicalChannelDynamicProperties } from '../vsp/api'
import { ChannelDetail } from '../vsp/api'
import { HeartbeatService } from './heartbeat.service'
import { Logger } from '../utils/logger'
import { VersionStore } from './version-store'
import { SessionService } from './session.service'
import { PromiseWrapper, PromiseCompleter } from '../shared'
import { Config } from './../core/config'
import { stbService } from '../stb'
import { idbStorge } from '../utils/idb-storge'

const log = new Logger('sdk-channel.service.ts')

/**
 *
 * Channel service class
 *
 * @export
 * @class ChannelService
 */
@Injectable()
export class ChannelService {

  private staticStore: VersionStore<QueryAllChannelResponse>

  private dynamicStore: VersionStore<QueryAllChannelDynamicPropertiesResponse>

  private queryAllChannel = queryAllChannel

  private queryAllChannelDynamicProperties = queryAllChannelDynamicProperties

  private staticChannelIDMapping: { [key: string]: ChannelDetail }

  private channelIDDynamicPropertiesMapping: { [key: string]: ChannelDynamicProperties }

  private mediaIDPhysicalChannelsDynamicPropertiesMapping: { [key: string]: PhysicalChannelDynamicProperties }

  private noToIDMapping: { [channelNO: string]: string } = {}

  private allChannelDetailList: Array<ChannelDetail>

  private allChannelPromise: PromiseCompleter<any> = PromiseWrapper.completer()

  constructor (private heartbeatService: HeartbeatService, private _sessionService: SessionService) {
    }

  onNotification (eventName: string, version: any) {
      switch (eventName) {
          case HeartbeatService.CHANNEL_STATIC_VERSION:
            this.initStore()
            return this.refreshStatic(version)
          case HeartbeatService.CHANNEL_DYNAMIC_VERSION:
            this.initStore()
            return this.refreshDynamic(version)
          case 'AJAX_SUCCESS':
            const data = version as { response: AjaxResponse, request: AjaxRequest }

            if (data && data.request && data.request.inf === 'SwitchProfile') {
                  this.staticStore = null
                  this.dynamicStore = null
                }
            break
          default:
            break
        }
    }

  clearChannelListStore () {
      this.staticStore.clear()
      this.dynamicStore.clear()
      idbStorge.removeItem('USERFILTER_' + this._sessionService.getProfileID())
    }

    /**
     *
     * Initialize the Store
     *
     * @private
     *
     * @memberOf ChannelService
     */
  private initStore () {
      this.staticStore = new VersionStore<QueryAllChannelResponse>('static_channel', this._sessionService.getProfileID())
      this.dynamicStore = new VersionStore<QueryAllChannelResponse>('dynamic_channel', this._sessionService.getProfileID())
    }

    /**
     *
     * Construct the index
     *
     * @private
     * @param {QueryAllChannelResponse} queryAllChannelResponse
     * @param {QueryAllChannelDynamicPropertiesResponse} queryAllChannelDynamicPropertiesResponse
     *
     * @memberOf ChannelService
     */
  private buildIndexer (): Promise<any> {
      const promise = Promise.all([this.staticStore.getData(), this.dynamicStore.getData()])

      return promise.then(([staticResp, dynamicResp]: [QueryAllChannelResponse, QueryAllChannelDynamicPropertiesResponse]) => {

          if (staticResp && dynamicResp) {
              log.debug('[buildIndexer] staticResp and dynamicResp are not empty')
                // Constructs the index of dynamic data
              const idToNOMapping = {}
              this.channelIDDynamicPropertiesMapping = {}
              this.mediaIDPhysicalChannelsDynamicPropertiesMapping = {}
              this.noToIDMapping = {}

              _.each(dynamicResp.channelDynamaicProp, (channelDynamicProperties: ChannelDynamicProperties) => {
                  idToNOMapping[channelDynamicProperties.ID] = channelDynamicProperties.channelNO
                  this.noToIDMapping[channelDynamicProperties.channelNO] = channelDynamicProperties.ID

                  this.channelIDDynamicPropertiesMapping[channelDynamicProperties.ID] = channelDynamicProperties
                  _.each(channelDynamicProperties.physicalChannelsDynamicProperties, (item: PhysicalChannelDynamicProperties) => {
                      this.mediaIDPhysicalChannelsDynamicPropertiesMapping[item.ID] = item
                    })
                })

                // Constructs the index of static data
              this.staticChannelIDMapping = {}
              this.allChannelDetailList = _.filter(staticResp.channelDetails, (channel: ChannelDetail) => {
                  const channelNO = idToNOMapping[channel.ID]
                  if (channelNO) {
                      channel.channelNO = channelNO
                      this.staticChannelIDMapping[channel.ID] = channel
                      return true
                    }
                })

                // Sort by channel number
              this.allChannelDetailList = _.sortBy(this.allChannelDetailList, (channelDetail: ChannelDetail) => {
                  return +channelDetail.channelNO
                })
              log.debug('[buildIndexer] allChannelDetailList length is: %s', this.allChannelDetailList.length)
              this.allChannelPromise.resolve(this.allChannelDetailList)
            } else {
              log.debug('[buildIndexer] staticResp or dynamicResp is empty')
              return Promise.reject(null)
            }
        })
    }

  allCPromise () {
      return this.allChannelPromise.promise
    }

    /**
     *
     *
     * @private
     * @returns {string}
     *
     * @memberOf ChannelService
     */
  private getUserFilter (): string {
      const onLineHeartbeatResponse: OnLineHeartbeatResponse = this.heartbeatService.getOnLineHeartbeatResponse()
      const userFilter = Config.userFilter ? Config.userFilter : null
      return onLineHeartbeatResponse ? onLineHeartbeatResponse.userFilter : userFilter
    }

  async userFilterChanged () {
      const currentUserFilter = this.getUserFilter()
      const userFilterStore = await idbStorge.getItem('USERFILTER_' + this._sessionService.getProfileID())
      return userFilterStore !== currentUserFilter
    }

    /**
     * According to the latest version number to refresh the local cache dynamic and static channel data
     * @param {String} version [static Version]|[dynamic Version]
     */
  refresh (version: string) {
      log.debug('begin refresh()')

      const arr = version.split('|')
      const staticVersion = arr[0]
      const dynamicVersion = arr[1]

      return Promise.all([this.refreshStatic(staticVersion), this.refreshDynamic(dynamicVersion)])
    }

    /**
     * Refresh static data
     *
     * @param {string} staticVersion
     * @returns
     *
     * @memberOf ChannelService
     */
  async refreshStatic (staticVersion: string): Promise<QueryAllChannelResponse> {
      const isChanged = await this.userFilterChanged()
      log.debug('begin refresh static cache, staticVersion is: %s, isChanged: %s', staticVersion, isChanged)

        // Refresh static data
      const promise = this.staticStore.getData(staticVersion).then((staticResp) => {
          if (staticResp && !isChanged) {
              log.debug('[refreshStatic] hit the cache, isChanged: %s', isChanged)
              return staticResp
            } else {
              log.debug('[refreshStatic] not hit the cache')
              const userFilter = this.getUserFilter()
              idbStorge.setItem('USERFILTER_' + this._sessionService.getProfileID(), userFilter)
              return this.queryAllChannel({
                  isReturnAllMedia: '1',
                  userFilter: userFilter
                }).then((_staticResp) => {
                  this.staticStore.save(_staticResp.channelVersion, _staticResp)
                  return _staticResp
                })
            }
        })

      return promise.then(() => {
          return this.buildIndexer()
        })
    }

    /**
     * Refresh static data
     *
     * @param {string} dynamicVersion
     * @returns
     *
     * @memberOf ChannelService
     */
  async refreshDynamic (dynamicVersion: string) {
      const isChanged = await this.userFilterChanged()
      log.debug('begin refreshDynamic(), dynamicVersion is: %s, isChanged: %s', dynamicVersion, isChanged)
      const promise = this.dynamicStore.getData(dynamicVersion).then((dynamicResp) => {
          if (dynamicResp && !isChanged) {
              log.debug('[refreshDynamic] hit the cache, isChanged: %s', isChanged)
              return dynamicResp
            } else {
              log.debug('[refreshDynamic] not hit the cache')
              stbService.refreshChannellist()
              return this.queryAllChannelDynamicProperties({
                  isReturnAllMedia: '1'
                }).then((_dynamicResp) => {
                  this.dynamicStore.save(dynamicVersion, _dynamicResp)
                  return _dynamicResp
                })
            }
        })

      return promise.then(() => {
          return this.buildIndexer()
        })
    }

  rejectMosaicChannelList (channelList: Array<ChannelDetail>) {
      log.debug('channelList length: %s', channelList && channelList.length)
      let orignalChannelList = _.clone(channelList)
      const rejectMosaic = _.reject(orignalChannelList, (channel: any) => {
          return channel && channel.isMosaicChannel === '1'
        })
      log.debug('rejectMosaic length: %s', rejectMosaic && rejectMosaic.length)
      return rejectMosaic
    }

    /**
     * Get all channels reject mosaic channel
     *
     * @returns {Array<ChannelDetail>}
     *
     * @memberOf ChannelService
     */
  getAllChannelList (): Array<ChannelDetail> {
      const channelList = this.rejectMosaicChannelList(this.allChannelDetailList)
      return channelList
    }

    /**
     * get iptv channelList reject mosaic channel
     */
  getChannelList (): Array<ChannelDetail> {
      const allChannelList = this.getAllChannelList()
      return this.getIPTVChannelList(allChannelList)
    }

    /**
     * get iptv channelList include mosaic channel
     */
  getChannelListIncludeMosaic () {
      const channelList = _.clone(this.allChannelDetailList)
      return this.getIPTVChannelList(channelList)
    }

  getIPTVChannelList (channelList: Array<ChannelDetail>): Array<ChannelDetail> {
      let iptvChannelList = []
      _.each(channelList, (channel) => {
          channel = this.filterIptvPhysicalChannel(channel)
          if (!_.isEmpty(channel.physicalChannels)) {
              iptvChannelList.push(channel)
            }
        })
      return iptvChannelList
    }

  filterIptvDynamicProperties (physicalChannelsDynamicProperties: Array<PhysicalChannelDynamicProperties>) {
      let iptvChannelDynamicProperties = []
      log.debug('channelNameSpace: %s', this._sessionService.channelNameSpace)
      if (!_.isEmpty(physicalChannelsDynamicProperties)) {
          iptvChannelDynamicProperties = physicalChannelsDynamicProperties.filter(physicalChannel => {
              const channelNamespaces = physicalChannel['channelNamespaces']
              log.debug('channelNamespaces: %s, physicalChannel: %s', channelNamespaces, physicalChannel)
              if (_.isEmpty(channelNamespaces)) {
                  return true
                } else {
                  return channelNamespaces.indexOf(this._sessionService.channelNameSpace) !== -1
                }
            })
        }
      log.debug('iptvChannelDynamicProperties: %s', iptvChannelDynamicProperties)
      return iptvChannelDynamicProperties || []
    }

  private filterIptvPhysicalChannel (channelDetail: ChannelDetail): ChannelDetail {
      let channel = channelDetail
      let iptvPhysicalChannels
      const physicalChannels = channel.physicalChannels
      const dimension = Config.get('dimension')
      if (!_.isEmpty(physicalChannels)) {
          iptvPhysicalChannels = physicalChannels.filter(physicalChannel => {
              const channelNamespaces = physicalChannel['channelNamespaces']
              if (dimension && physicalChannel.dimension && !_.contains(dimension, physicalChannel.dimension)) {
                  return false
                }
              if (_.isEmpty(channelNamespaces)) {
                  return true
                } else {
                  return channelNamespaces.indexOf(this._sessionService.channelNameSpace) !== -1
                }
            })
          channel.physicalChannels = iptvPhysicalChannels
        }
      return channel
    }

    /**
     * Get channel objects based on channel number
     *
     * @param {(string | Number)} channelNO
     * @returns
     *
     * @memberOf ChannelService
     */
  getChannelByNO (channelNO: string | Number) {
      const id: string = this.noToIDMapping['' + channelNO]
      return this.getChannelByID(id)
    }

    /**
     * Get channel objects based on channel ID
     *
     * @param {string} channelID
     * @returns
     *
     * @memberOf ChannelService
     */
  getChannelByID (channelID: string) {
      return this.staticChannelIDMapping && this.staticChannelIDMapping[channelID]
    }

    /**
     * Get the dynamic data of the channel
     *
     * @param {string} channelID
     * @returns {ChannelDynamicProperties}
     *
     * @memberOf ChannelService
     */
  getAllDynamicPropertiesByChannelID (channelID: string, isIptv = true): ChannelDynamicProperties {
      const prop = this.channelIDDynamicPropertiesMapping && this.channelIDDynamicPropertiesMapping[channelID]
      if (isIptv && prop) {
          const iptvPhysicalChannelsDynamicProperties = this.filterIptvDynamicProperties(prop.physicalChannelsDynamicProperties)
          prop.physicalChannelsDynamicProperties = iptvPhysicalChannelsDynamicProperties
        }
      return prop
    }

    /**
     * Obtain content permissions based on MediaID, it obtains not iptv dynamic properties
     *
     * @param {string} mediaID
     * @returns {PhysicalChannelsDynamicProperties}
     *
     * @memberOf ChannelService
     */
  getContentRightByMediaID (mediaID: string): PhysicalChannelDynamicProperties {
      return this.mediaIDPhysicalChannelsDynamicPropertiesMapping[mediaID]
    }

    /**
     * Obtain an associated listen to your content based on the channel ID
     *
     * @param {string} channelID
     * @returns {Array<PhysicalChannelsDynamicProperties>}
     *
     * @memberOf ChannelService
     */
  getContentRightsByChannelID (channelID: string, isIptv = true): Array<PhysicalChannelDynamicProperties> {
      const prop = this.channelIDDynamicPropertiesMapping && this.channelIDDynamicPropertiesMapping[channelID]
      if (prop) {
          if (isIptv) {
              const iptvPhysicalChannelsDynamicProperties = this.filterIptvDynamicProperties(prop.physicalChannelsDynamicProperties)
              return iptvPhysicalChannelsDynamicProperties
            }
          return prop.physicalChannelsDynamicProperties
        }
      return []
    }
}
