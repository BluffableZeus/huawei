import { Injectable } from '@angular/core'

import { ChannelPlaybill, PlaybillDetail, PlaybillLite } from '../vsp/api'
import { session } from '../utils'
import { SessionService } from './session.service'

@Injectable()
export class LiveTvPlaybillService {

  constructor (private sessionService: SessionService) {
    }

    /**
     * To determine whether to show the program lock
     */
  playbillIsLocked (channelRatingID: string, playbill: PlaybillDetail | PlaybillLite, ratingID: string): boolean {
      if (ratingID && playbill && playbill.rating && playbill.rating.ID && channelRatingID) {
          return (+ratingID - +playbill.rating.ID) < 0 || (+ratingID - +channelRatingID) < 0
        }
      return true
    }

    /**
     * [Get the list of unlocked programs]
     */
  getUnLockedPlaybills (): string[] {
      const profileID = this.sessionService.getProfileID()
      return session.get(profileID + '_UnLockedPlaybills')
    }

    /**
     * [Save the unlocked program]
     */
  setUnLockedPlaybills (playbillID: string) {
      const profileID = this.sessionService.getProfileID()
      let _unLockedPlaybills: String[] = this.getUnLockedPlaybills() || []
      _unLockedPlaybills.push(playbillID)
      session.put(profileID + '_UnLockedPlaybills', _unLockedPlaybills)
    }
    /**
      * [Clear the unlock program]
      */
  emptyUnLockedPlaybills () {
      const profileID = this.sessionService.getProfileID()
      session.put(profileID + '_UnLockedPlaybills', [])
    }
    /**
     * Get the current program
     */
  currentPlaybillLite (channelPlaybill: ChannelPlaybill, playTime?: number): PlaybillLite {
      if (channelPlaybill) {
          let time = new Date().getTime()
          if (playTime) {
              time = playTime
            }
          const playbillLite = channelPlaybill.playbillLites.find(billlite => {
              return parseInt(billlite.startTime, 10) <= time && parseInt(billlite.endTime, 10) > time
            })
          return playbillLite
        }
    }
}
