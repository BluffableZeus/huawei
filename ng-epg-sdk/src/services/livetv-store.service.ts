import { Logger } from '../utils/logger'
import * as _ from 'underscore'

import { Injectable } from '@angular/core'

import {
    ChannelDetail, QueryChannel, QueryPlaybill,
    queryPlaybillList, PhysicalChannelDynamicProperties,
    QueryPlaybillListRequest, QueryPlaybillListResponse
} from '../vsp/api'
import { session } from '../utils'
import { SessionService } from './session.service'
import { ChannelService } from './channel.service'
import * as localforage from 'localforage'

const log = new Logger('livetv-store.service.ts')

@Injectable()
export class LiveTvStoreService {
  private timeId: any
  private lastQueryPlaybillRequest: QueryPlaybillListRequest = null
  private lastQueryPlaybillResponsePromise: Promise<QueryPlaybillListResponse>

  constructor (
        private sessionService: SessionService,
        private channelService: ChannelService) {
    }

    /**
    * [According to the login profileID to get the last successful channel]
    */
  async getlastChannelNo (): Promise<string[]> {
      const profileID = this.sessionService.getProfileID()
      const lastChannelNos = await localforage.getItem<string[]>(profileID + '_Last_ChannelNos')
      return lastChannelNos
    }

    /**
     * [Store the last channel to play]
     */
  async setlastChannelNo (channelNo: string) {
      const profileID = this.sessionService.getProfileID()
      log.debug(`set lastChannelNo : ${channelNo}`)
      let channelNos = await this.getlastChannelNo()
      let _lastChannelNos: string[] = _.clone(channelNos) || []
      _lastChannelNos.unshift(channelNo)
      if (_lastChannelNos && _lastChannelNos.length > 2) {
          _lastChannelNos.length = 2
        }
      if (_.first(channelNos) !== channelNo) {
          localforage.setItem(profileID + '_Last_ChannelNos', _lastChannelNos)
        }
    }

    /**
     * Gets the currently playing channel
     */
  async getCurrentChannelNo (): Promise<string> {
      const profileID = this.sessionService.getProfileID()
      const channelNo = localforage.getItem<string>(profileID + '_Current_ChannelNo')
      return channelNo
    }

    /**
     * Save the currently playing channel
     */
  async setCurrentChannelNo (channelNo: string) {
      const profileID = this.sessionService.getProfileID()
      log.debug(`set currentChannelNo : ${channelNo}`)
      return localforage.setItem(profileID + '_Current_ChannelNo', channelNo)
    }

  upDataPlaybillLists (channelDetail: ChannelDetail) {
      clearTimeout(this.timeId)
      this.timeId = setTimeout(() => {
          this.upDataPlaybillLists(channelDetail)
        }, 59 * 60 * 1000)
      log.debug('refresh playbill timer start')
      return this.queryPlaybillList(channelDetail)
    }

  startUpDataPlaybillLists (channelDetail: ChannelDetail) {
      return this.upDataPlaybillLists(channelDetail)
    }

  closeUpDataPlaybillLists () {
      clearTimeout(this.timeId)
    }

    /**
    * [Get the program list]
    */
  queryPlaybillList (channelDetail: ChannelDetail): any {
      if (_.isEmpty(channelDetail)) {
          return
        }
      let physicalProperties: Array<PhysicalChannelDynamicProperties> = this.channelService.getContentRightsByChannelID(channelDetail.ID)
      let pltvCR = physicalProperties && physicalProperties[0] && physicalProperties[0].pltvCR
      log.debug('physicalProperties pltvCR : %s', pltvCR)
      let isPltv = pltvCR && pltvCR.enable === '1' && pltvCR.isContentValid === '1' && pltvCR.length !== '0'
      const currentTime = new Date().getTime()

      if (isPltv) {
          const queryChannel: QueryChannel = { channelIDs: [channelDetail.ID], contentType: 'CHANNEL' }
          const startTime = (currentTime - (+pltvCR.length) * 1000 * 2) + ''
          const endTime = currentTime + (60 * 60 * 1000) + ''
          const queryPlaybill: QueryPlaybill = {
              type: '0', count: '99', offset: '0',
              startTime: startTime, endTime: endTime, isFillProgram: '1'
            }
          const parms = { queryChannel: queryChannel, queryPlaybill: queryPlaybill, needChannel: '0' }
          log.debug('supportPltv send queryPlaybillList requestParms : %s', parms)
          return this._queryPlaybillList(parms)
        } else {
          const queryChannel: QueryChannel = { channelIDs: [channelDetail.ID], contentType: 'CHANNEL' }
          const startTime = currentTime + ''
          const endTime = (currentTime + (60 * 60 * 1000)) + ''
          const queryPlaybill: QueryPlaybill = {
              type: '1', count: '99', offset: '0',
              startTime: startTime, endTime: endTime, isFillProgram: '1'
            }
          const parms = { queryChannel: queryChannel, queryPlaybill: queryPlaybill, needChannel: '0' }
          log.debug('notSupportPltv send queryPlaybillList requestParms : %s', parms)
          return this._queryPlaybillList(parms)
        }
    }

    /**
     * [Get channel preview based on channelNo]
     */
  getChannelPreviewCount (channelNo: string) {
      const preview_Count = session.get(channelNo + '_Preview_Count')
      log.debug(`get channel_preview_Count : ${preview_Count}`)
      return preview_Count
    }

    /**
     * [Storage channel previews]
     */
  setChannelPreviewCount (channelNo: string, preview_Count: string) {
      session.put(channelNo + '_Preview_Count', preview_Count)
    }

    /**
     * cache
     */
  private _queryPlaybillList (req) {

      if (this.isSamePlaybillRequest(this.lastQueryPlaybillRequest, req)) {
          return this.lastQueryPlaybillResponsePromise
        } else {
          this.lastQueryPlaybillRequest = req
          this.lastQueryPlaybillResponsePromise = queryPlaybillList(req, { params: { 'SID': 'queryplaybilllist1' } }).then((resp) => {
              this.lastQueryPlaybillRequest = null
              this.lastQueryPlaybillResponsePromise = null
              return resp
            })

          return this.lastQueryPlaybillResponsePromise
        }

    }

  private isSamePlaybillRequest (req1: QueryPlaybillListRequest, req2: QueryPlaybillListRequest) {

      if (req1 && req2) {
          let req1StartTime = req1.queryPlaybill && req1.queryPlaybill.startTime
          let req1EndTime = req1.queryPlaybill && req1.queryPlaybill.endTime

          let req2StartTime = req2.queryPlaybill && req2.queryPlaybill.startTime
          let req2EndTime = req2.queryPlaybill && req2.queryPlaybill.endTime
          if (+req1EndTime - +req1StartTime === +req2EndTime - +req2StartTime && Math.abs(+req2StartTime - +req1StartTime) < 1000) {
              return this.stringifyPlaybillRequest(req1) === this.stringifyPlaybillRequest(req2)
            }
          return false
        }

      return false

    }

  private stringifyPlaybillRequest (req) {
      return JSON.stringify(req, (key, value) => {
          if (key === 'startTime' || key === 'endTime') {
              return
            }
          return value
        })
    }

}
