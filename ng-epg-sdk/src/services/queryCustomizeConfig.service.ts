import * as _ from 'underscore'
import { Injectable } from '@angular/core'

import { queryCustomizeConfig, CustomizeConfigQueryType, Configuration } from '../vsp'
import { session } from '../utils'

@Injectable()
export class QueryCustomizeConfigService {
  public queryCustomizeConfig (): Promise<any> {
      return queryCustomizeConfig({ queryType: '0,2,4,5,7' }).then(resp => {
          session.put('isFailQueryCustomizeConfig', false)

          const configurations = resp && resp.configurations
            // allParameterList is queryType: 0, 2,4,5,7
          session.put('allParameterList', resp)
          if (!configurations) {
              return
            }

            // put the configuration parameters of terminal parameters, queryType is 0
          const terminalConfigList = _.find(configurations, (configuration: Configuration) => {
              return configuration.cfgType === CustomizeConfigQueryType.TERMINAL_PARAMETER
            })

          session.put('terminalConfigList', terminalConfigList)
          this.setConfigutationSession('terminalConfigList', terminalConfigList, CustomizeConfigQueryType.TERMINAL_PARAMETER)

            // put the configuration parameters of VSP EPG parameters, queryType is 2
          const vspAndEpgConfigList = _.find(configurations, (configuration: Configuration) => {
              return configuration.cfgType === CustomizeConfigQueryType.VSP_EPG_PARAMETER
            })

          this.setConfigutationSession('vspAndEpgConfigList', vspAndEpgConfigList, CustomizeConfigQueryType.VSP_EPG_PARAMETER)
        }, () => {
          session.put('isFailQueryCustomizeConfig', true)
        })
    }

  setConfigutationSession (configurationType: string, configurationList: Configuration, type: string) {
      if (configurationList && configurationList.values && type === CustomizeConfigQueryType.TERMINAL_PARAMETER) {
          _.each(configurationList.values, (data) => {
              const values = _.first(data.values) as string
              session.put(configurationType + '_' + data.key, values)
            })
        } else if (configurationList && type === CustomizeConfigQueryType.VSP_EPG_PARAMETER) {
          const configurationKeys = Object.keys(configurationList)
          _.each(configurationKeys, (key) => {
              session.put(configurationType + '_' + key, configurationList[key])
            })
        }
    }
}
