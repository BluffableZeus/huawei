import { Logger } from './../utils/logger'
import { Injectable, NgZone } from '@angular/core'
import { Subject } from 'rxjs'
import 'rxjs/add/operator/take'

import { Config } from '../core'

const log = new Logger('keydown.service')

export interface OnKeyDownCallback {
  (event: any): any
}

/**
 *
 * Key processing service class
 *
 */
@Injectable()
export class KeydownService {

  public keydownObservable: Subject<any> = new Subject()
  public keyupObservable: Subject<any> = new Subject()

  private isPressing = false
  private keydownTime: number

  constructor (private ngZone: NgZone) {
    }

    /**
     * Whether or not the current key is long
     *
     * @returns
     *
     * @memberOf KeydownService
     */
  isLongPress (): boolean {
      return this.isPressing && ((Date.now() - this.keydownTime) > this.getlongPressDelay())
    }

    /**
     * create new function to handle long key press
     */
  createLongPressCallback (cb: Function, context?: any): Function {
      return this.longPress(cb, context)
    }

    /**
     * @deprecated use createLongPressCallback
     */
  private longPress (cb: Function, context?: any): Function {

      const shortPressDelay = this.getShortPressDelay()
      const longPressDelay = this.getlongPressDelay()

      let lastHandleTime = 0
      let shortKeyTimeoutId

      let handler: Function

      return (...args) => {

          handler = () => {
              this.ngZone.run(() => {
                  lastHandleTime = Date.now()
                  cb.apply(context, args)
                })
            }

            // If the key is not lifted, the subsequent keys are not processed
          if (this.isPressing) {
              return
            }

          this.isPressing = true

          clearTimeout(shortKeyTimeoutId)
          this.keydownTime = Date.now()
          let isShortPressInvoked = false
          log.debug('begin longPress add keyup listener.')
          this.keyupObservable.take(1).subscribe(() => {
              log.debug('end longPress, receive keyup.')
              this.isPressing = false
              const now = Date.now()
              const downAndUpDuration = now - this.keydownTime

                // If it is a long press, it is executed immediately
              if (downAndUpDuration >= longPressDelay) {
                  handler()
                } else {
                  if (!isShortPressInvoked) {
                      if (downAndUpDuration >= shortPressDelay) {
                            // If the duration is longer than the short press interval, then execute
                          handler()
                        } else {
                            // If the length of time does not exceed the short press interval, then make up the lack of time
                          shortKeyTimeoutId = setTimeout(() => {
                              handler()
                            }, shortPressDelay - downAndUpDuration)
                        }
                    }
                }
            })

            // If the key is pressed with the last processing interval> short press time, it is executed directly
          if ((this.keydownTime - lastHandleTime) > shortPressDelay) {
              isShortPressInvoked = true
              handler()
            }
        }
    }

  private getShortPressDelay () {
      return Config.get('shortpress_delay_milliseconds', 300)
    }

  private getlongPressDelay () {
      return Config.get('longpress_milliseconds', 600)
    }
}
