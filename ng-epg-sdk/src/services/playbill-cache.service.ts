import * as _ from 'underscore'
import * as moment from 'moment'

import { AjaxResponse } from './../utils/ajax'
import { OnNotification } from './../core/metadata/lifecycle_hooks'
import { Config } from './../core/config'
import { Logger } from './../utils/logger'
import { Map } from './../utils/map'
import {
    QueryPlaybillListRequest, queryPlaybillList, QueryPlaybillListResponse,
    ChannelPlaybillContext, QueryPlaybillVersionRequest
} from './../vsp/api'
import { BehaviorSubject, Subscription, Observable } from 'rxjs'
import 'rxjs/add/observable/of'
import 'rxjs/add/observable/timer'
import 'rxjs/add/observable/fromPromise'
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/concatMap'

import { PlaybillLite } from './../vsp/api'
import { ChannelDetail } from './../vsp/api'
import { PlaybillVersion } from '../vsp/api'
import { queryPlaybillVersion } from '../vsp/api'
import { EventService } from './event.service'
import { HeartbeatService } from './heartbeat.service'
import { OnLineHeartbeatResponse } from './../vsp/api'
import { PlaybillAutoFillService } from './playbill-auto-fill.service'

const LOAD_DYNAMIC_MILLISECONDS = 500
const PREPARE_SLEEP_MILLISECONDS = 500
const PREPARE_DAYS = 1
const ONE_SECOND = 1000
const EARLIEST_DAY = 8

const log = new Logger('playbill-cache.service')

export interface QueryPlaybillListByChannelsRequest {
  channelIDs: Array<string>
  beginTime: number
  endTime: number
    // delay to load personal data
  delay?: number
}

export interface QueryDataResponse {
  playbills: PlaybillLite[]
  channels: ChannelDetail[]
  focusedPlaybill: PlaybillLite
  timepoints: number[]
  gridBeginTime: number
  pageIndex: number
}

export interface PlaybillContextFromCacheRequest {
  channelID: string
  date: number
  preNumber: number
  nextNumber: number
}

interface StaticCache {
  [channelID: string]: {
      timestamp: any,
      [dayOfYear: string]: {
          version: string,
          map: Map<PlaybillLite>,
          timestamp: any
        }
    }
}

interface QueueItem {
  channelIDs: Array<string>
  dayOfYear: number
}

interface DynamicCache {
  [channelID: string]: {
      [playbillID: string]: boolean
    }
}

interface LocalPlaybillVersions {
  [dayOfYear: string]: Array<PlaybillVersion>
}

export class PlaybillCacheService implements OnNotification {

  private downloadPlaybillTimeoutId

  private staticCache: StaticCache = {}

  private dynamicCache: DynamicCache = {}

  private queue: Array<QueueItem> = []

  private dynamicQuerySubscription: Subscription = new Subscription()

  private localPlaybillVersions: LocalPlaybillVersions = {}

  private timeRange: number

  private dateRange: Array<number> = []

  private playbillAutoFillService: PlaybillAutoFillService

  constructor () {
      this.playbillAutoFillService = new PlaybillAutoFillService()
    }

  onNotification (eventName: string, data: any): any {
      const clearCacheInfs = [
          'CreateReminder',
          'DeleteReminder',
          'AddPVR',
          'AddPeriodicPVR',
          'CancelPVRByID',
          'CancelPVRByPlaybillID',
          'DeletePVRByCondition',
          'DeletePVRByID',
          'QueryPVR',
          'QueryPVRSpace',
          'QueryPVRSpaceByID',
          'UpdatePVR',
          'UpdatePeriodicPVR',
          'SwitchProfile'
        ]
      switch (eventName) {
          case 'AJAX_SUCCESS':
            const inf = data.request.inf

            if (_.contains(clearCacheInfs, inf)) {
                  log.debug('clear dynamic of all channel')
                  this.dynamicCache = {}
                } else if ('OnLineHeartbeat' === inf) {
                  const ajaxResponse: AjaxResponse = data.response
                  const onLineHeartbeatResponse = ajaxResponse.data as OnLineHeartbeatResponse
                  this.timeRange = +onLineHeartbeatResponse.nextCallInterval * ONE_SECOND * 2
                  if (!this.hasNotCache()) {
                      this.updateCachedPlaybills()
                    }
                } else if ('Logout' === inf) {
                  this.downloadPlaybillTimeoutId = true
                }
            break
          case HeartbeatService.REMINDER_VERSION_CHANGED:
            this.dynamicCache = {}
            break
        }
    }

  hasNotCache () {
      return Config.shouldClearCache && (!Config.reserveChannelCount || !Config.reserveCacheDays)
    }

  prepareCache (allChannelList: Array<ChannelDetail>, lastPlayedChannel: ChannelDetail) {
      log.debug('begin prepareCache()')

        // prepare last played channel first
      let channelList: Array<ChannelDetail> = _.first(this.getPageList<ChannelDetail>(allChannelList, lastPlayedChannel))
      if (_.isEmpty(channelList)) {
          log.debug('channelList is empty')
          return
        }
        // add playbill request to queue
      _.times(Config.get('preparePlaybillDays', PREPARE_DAYS), (i) => {
          const dayOfYear = this.getDayOfYear(moment().add('day', i).toDate().getTime())
          const channelIDs = _.pluck(channelList, 'ID')
          this.queue.push({ channelIDs, dayOfYear })
        })

        // start to cache one by one
      this.consumeQueue()

      log.debug('end prepareCache()')
    }

  queryPlaybillList ({ channelIDs, beginTime, endTime, delay }: QueryPlaybillListByChannelsRequest): Observable<PlaybillLite[][]> {
      delay = delay || LOAD_DYNAMIC_MILLISECONDS

      if (Config.shouldClearCache && (!Config.reserveChannelCount || !Config.reserveCacheDays)) {
          log.debug('should clear all cache')
          this.clearCache()
        }

      log.debug('begin queryPlaybillList(), channelIDs:%s, beginTime:%s, endTime:%s, delay:%s', channelIDs, beginTime, endTime, delay)

      this.dynamicQuerySubscription.unsubscribe()

      const dayOfYears = _.uniq([this.getDayOfYear(beginTime), this.getDayOfYear(endTime)])
      let { cachedFullPlaybillList, unCachedDynamicChannelIDs } = this.getCachedFullPlaybillList(channelIDs, beginTime, endTime)
      let retObservable: Observable<PlaybillLite[][]>

      if (_.isEmpty(cachedFullPlaybillList)) {
          log.debug('not hit cache')
          const cachedDays = this.getCachedDays()
            // add playbill prepare to queue
          dayOfYears.forEach((dayOfYear) => {
              this.queue.unshift({ channelIDs: channelIDs, dayOfYear: dayOfYear })
              const playbillVersionPromises = _.map([dayOfYear], day => this.queryPlaybillVersion(day))
              if (cachedDays && _.contains(cachedDays, dayOfYear)) {
                  this.clearLocalPlaybillVersions([dayOfYear])
                  Promise.all(playbillVersionPromises).then((newVersions: Array<LocalPlaybillVersions>) => {
                      if (!newVersions || !newVersions[0] || !this.localPlaybillVersions) {
                          return
                        }
                      const newVersionsOfDay = newVersions[0][dayOfYear]
                      const oldVersionsOfDay = this.localPlaybillVersions[dayOfYear]
                      log.debug('[queryPlaybillList] newVersionsOfDay: %s, changed oldVersionsOfDay: %s', newVersionsOfDay, oldVersionsOfDay)
                      if (oldVersionsOfDay && oldVersionsOfDay.length) {
                          const changedChannelIds = this.getVersionChangedChannelIds(newVersionsOfDay, oldVersionsOfDay, dayOfYear)
                          log.debug('[queryPlaybillList] dayOfYear: %s, changed channelIds: %s', dayOfYear, changedChannelIds)
                          if (changedChannelIds.length) {
                              EventService.emit('PLAYBILL_VERSION_CHANGED')
                            }
                        }

                    })
                }
            })

          if (!this.hasNotCache()) {
              this.consumeQueue()
            }

          retObservable = this.queryPlaybillListFromServer(channelIDs, beginTime, endTime)
        } else {
          if (_.isEmpty(unCachedDynamicChannelIDs)) {
              log.debug('hit full cache')
              retObservable = Observable.of(cachedFullPlaybillList)
            } else {
              log.debug('hit static cache, unCachedDynamicChannelIDs:%s', unCachedDynamicChannelIDs)

              if (delay > 0) {
                  log.debug('return static cache first, then return dynamic data after:%s milliseconds', delay)

                  retObservable = new BehaviorSubject(cachedFullPlaybillList) as any

                  this.dynamicQuerySubscription = Observable.timer(delay).concatMap(() => {
                        return this.queryDynamicPlaybillListFromServer(unCachedDynamicChannelIDs, beginTime, endTime)
                    })
                        .map((doublePlaybillList) => {
                            // remove unchanged playbill
                            return this.rejectEqualPlaybill(cachedFullPlaybillList, doublePlaybillList)
                        })
                        .map((doublePlaybillList) => {
                            // build same length as channelIDs's length Array
                            return this.toSameChannelCountPlaybillList(channelIDs, unCachedDynamicChannelIDs, doublePlaybillList)
                        }).subscribe((r) => {
                          (retObservable as BehaviorSubject<any>).next(r)
                        })

                } else {
                  log.debug('query dynamic data, then return full data')
                  retObservable = this.queryDynamicPlaybillListFromServer(unCachedDynamicChannelIDs, beginTime, endTime).map(() => {
                      return this.getCachedFullPlaybillList(channelIDs, beginTime, endTime).cachedFullPlaybillList
                    })
                }
            }
        }

      if (Config.shouldClearCache) {
          this.reduceStaticCache()
        }

      return retObservable
    }

  reduceStaticCache () {
      if (!this.staticCache) {
          return
        }

      const cacheList = _.sortBy(_.pairs(this.staticCache), (cacheItem) => {
          return cacheItem[1].timestamp
        })
      const lastCacheList = cacheList.slice(-Config.reserveChannelCount)
      this.staticCache = {}
      _.each(lastCacheList, (pair) => {
          const lastCache = _.chain(pair[1]).pairs()
                .reject(p => p[0] === 'timestamp')
                .sortBy(p => p[1].timestamp)
                .slice(-Config.reserveCacheDays)
                .object().value();
          (lastCache as any).timestamp = Date.now()
          this.staticCache[pair[0]] = lastCache as any
        })
    }

  getCachedFullPlaybillList (channelIDs: Array<string>, beginTime, endTime): {
      cachedFullPlaybillList: PlaybillLite[][],
      unCachedDynamicChannelIDs: Array<string>
    } {

      const cachedFullPlaybillList: PlaybillLite[][] = []
      const unCachedDynamicChannelIDs: Array<string> = []

      channelIDs.find((channelID: string) => {
          const cachedPlaybillList = this.getStaticCachedDayPlaybillListByTimeSpan(channelID, beginTime, endTime)
          const filteredList = this.filterByTimeSpan(cachedPlaybillList, beginTime, endTime)

          if (_.isEmpty(filteredList)) {
              log.debug('not cache, channelID:%s, beginTime:%s, endTime:%s', channelID, beginTime, endTime)
              return true
            } else {
              cachedFullPlaybillList.push(filteredList)

              if (!this.isDynamicCached(channelID, filteredList)) {
                  unCachedDynamicChannelIDs.push(channelID)
                }
            }
        })

      if (cachedFullPlaybillList.length === channelIDs.length) {
          return { cachedFullPlaybillList, unCachedDynamicChannelIDs }
        } else {
          return {
              cachedFullPlaybillList: [],
              unCachedDynamicChannelIDs: []
            }
        }
    }

  getDayOfYear (localTime: number) {
      return moment(localTime).dayOfYear()
    }

  getPageList<T> (itemList: Array<T>, focusItem: T, pageSize = 5): Array<Array<T>> {

      const pageList: Array<Array<T>> = []
      let sortedPageList = []

      let list = itemList
      while (list.length) {
          const onePageList = list.slice(0, pageSize)
          pageList.push(onePageList)
          list = list.slice(pageSize)
        }

      const index = pageList.findIndex((_list) => {
          return _.contains(_list, focusItem)
        })

      let offset = 0
      sortedPageList.push(pageList[index])
      while (true) {
          offset++
          if (pageList[index + offset] || pageList[index - offset]) {
              sortedPageList.push(pageList[index + offset])
              sortedPageList.push(pageList[index - offset])
              sortedPageList = _.compact(sortedPageList)
            } else {
              break
            }
        }

      return sortedPageList
    }

  private rejectEqualPlaybill (cachedFullPlaybillList: PlaybillLite[][], doublePlaybillList: PlaybillLite[][]) {
      return doublePlaybillList.map((playbillList, index) => {
          return _.reject(playbillList, (playbill) => {
              const cachedPlaybill = _.findWhere(cachedFullPlaybillList[index], { ID: playbill.ID })
              return !cachedPlaybill || this.isDynamicEqual(cachedPlaybill, playbill)
            })
        })
    }

  private isDynamicEqual (a: PlaybillLite, b: PlaybillLite) {
      if (a && b) {
          return a.isLocked === b.isLocked
                && a.reminderStatus === b.reminderStatus
                && a.hasRecordingPVR === b.hasRecordingPVR
        }
      return false
    }

  private toSameChannelCountPlaybillList (channelIDs: Array<string>, unCachedDynamicChannelIDs: Array<string>,
        doublePlaybillList: PlaybillLite[][]) {
      if (channelIDs.length === unCachedDynamicChannelIDs.length) {
          return doublePlaybillList
        }

      const retDoublePlaybillList = _.times(channelIDs.length, () => [])
      unCachedDynamicChannelIDs.forEach((channelID, index) => {
          retDoublePlaybillList[channelIDs.indexOf(channelID)] = doublePlaybillList[index]
        })

      return retDoublePlaybillList
    }

  private isDynamicCached (channelID: string, playbillList: Array<PlaybillLite>) {
      const dynamicCacheOfChannel = this.dynamicCache[channelID]
      if (dynamicCacheOfChannel) {
          const unCachedPlaybill = _.find(playbillList, (playbillLite) => {
              return dynamicCacheOfChannel && !dynamicCacheOfChannel[playbillLite.ID]
            })
          return !unCachedPlaybill
        }
      return false
    }

  private queryDynamicPlaybillListFromServer (channelIDs: Array<string>, beginTime, endTime): Observable<PlaybillLite[][]> {
      return this.queryPlaybillListFromServer(channelIDs, beginTime, endTime).map((doublePlaybillList) => {

          this.updateDynamicCache(doublePlaybillList)

          const dayOfYears = _.uniq([this.getDayOfYear(beginTime), this.getDayOfYear(endTime)])

          dayOfYears.forEach((dayOfYear) => {
              this.updateStaticCache(channelIDs, dayOfYear, doublePlaybillList)
            })

          return doublePlaybillList
        })
    }

  private queryPlaybillListFromServer (channelIDs: Array<string>, beginTime, endTime): Observable<PlaybillLite[][]> {
      const req = this.buildQueryPlaybillRequest(channelIDs, beginTime, endTime)
      const emptyRespCb = () => _.times(channelIDs.length, () => [])

      const promise = queryPlaybillList(req, { isShowLoading: false, params: { scene: 'EpgTVguide', 'SID': 'queryplaybilllist4' } }).then((resp) => {
          resp = this.playbillAutoFillService.setPlaybillListFillProgram(channelIDs, beginTime, endTime, resp)
          return resp.channelPlaybills.map(r => r.playbillLites)
        }, emptyRespCb)

      return Observable.fromPromise(promise)
    }

    /**
     * update dynamic cache flag
     */
  private updateDynamicCache (doublePlaybillList: PlaybillLite[][] = []) {
      log.debug('begin updateDynamicCacheFlag(), length:%s', doublePlaybillList.length)
      doublePlaybillList.forEach((playbillList, index) => {
          playbillList.forEach(playbillLite => {
              const channelID = playbillLite.channelID
              this.dynamicCache[channelID] = this.dynamicCache[channelID] || {}
              this.dynamicCache[channelID][playbillLite.ID] = true
            })
        })
      log.debug('end updateDynamicCacheFlag()')
    }

  private updateStaticCache (channelIDs: Array<string>, dayOfYear: number, doublePlaybillList: PlaybillLite[][]) {
      log.debug('begin updateStaticCache(), channelIDs:%s', channelIDs)
      doublePlaybillList.forEach((playbillList, index) => {
          const channelID = channelIDs[index]
          const map = this.getStaticCachedMap(channelID, dayOfYear)
          if (map) {
              playbillList.forEach(playbillLite => {
                  if (map.get(playbillLite.ID)) {
                      map.set(playbillLite.ID, playbillLite)
                    }
                })
            }
        })
      log.debug('end updateStaticCache()')
    }

  private buildQueryPlaybillRequest (channelIDs: Array<string>, beginTime: number, endTime: number): QueryPlaybillListRequest {
      return {
          queryChannel: {
              channelIDs: channelIDs
            },
          queryPlaybill: {
              type: '0',
              startTime: '' + beginTime,
              endTime: '' + endTime,
              count: '100',
              offset: '0',
              isFillProgram: '1'
            },
          needChannel: '0'
        }
    }

  private consumeQueue () {
      log.debug('begin consumeQueue()')

      if (this.downloadPlaybillTimeoutId) {
          return
        }

      this.downloadPlaybillTimeoutId = setTimeout(() => {

          log.debug('queue length:%s', this.queue.length)

          if (this.queue.length) {
              const queueItem = this.queue.shift()

              const unCachedChannelIDs = queueItem.channelIDs.filter((channelID) => {
                  return _.isEmpty(this.getStaticCachedPlaybillListByDayOfYear(channelID, queueItem.dayOfYear))
                })

              if (_.isEmpty(unCachedChannelIDs)) {
                  this.downloadPlaybillTimeoutId = null
                  this.consumeQueue()
                } else {
                  const times = this.getBdginAndEndTimeOfDay(queueItem.dayOfYear)
                  const req = this.buildQueryPlaybillRequest(unCachedChannelIDs, times.beginTime, times.endTime)

                  queryPlaybillList(req, { isShowLoading: false, params: { scene: 'EpgTVguide', 'SID': 'queryplaybilllist6' } }).then((resp: QueryPlaybillListResponse) => {
                      resp = this.playbillAutoFillService.setPlaybillListFillProgram(unCachedChannelIDs,
                            times.beginTime, times.endTime, resp)
                      this.downloadPlaybillTimeoutId = null
                      unCachedChannelIDs.forEach((channelID, index) => {
                          this.addPlaybillListToCache(channelID, queueItem.dayOfYear, resp.channelPlaybills[index].playbillLites)
                        })

                      this.consumeQueue()
                    }, () => {
                      this.downloadPlaybillTimeoutId = null
                      this.consumeQueue()
                    })
                }

            } else {
              this.downloadPlaybillTimeoutId = null
              log.debug('consume queue finished')
            }
        }, Config.get('prepare_sleep_milliseconds', PREPARE_SLEEP_MILLISECONDS))

      log.debug('end consumeQueue()')
    }

  private addPlaybillListToCache (channelID: string, dayOfYear: number, playbillList: Array<PlaybillLite>) {
      log.debug('begin addPlaybillListToCache(), channelID:%s, dayOfYear:%s, length:%s', channelID, dayOfYear, playbillList.length)

      const firstPlaybill = _.first(playbillList)
      if (firstPlaybill && firstPlaybill.channelID !== channelID) {
          return
        }

      const now = Date.now()

      this.staticCache[channelID] = this.staticCache[channelID] || { timestamp: now as any }
      this.staticCache[channelID][dayOfYear] = {
          version: '',
          map: new Map(playbillList),
          timestamp: now as any
        }

      this.updateDynamicCache([playbillList])
      log.debug('begin addPlaybillListToCache()')
    }

  private filterByTimeSpan (playbillList: Array<PlaybillLite>, beginTime: number, endTime: number) {
      if (_.isEmpty(playbillList)) {
          return
        }
      const beginIndex = _.findIndex(playbillList, (playbillLite: PlaybillLite) => {
          return beginTime <= +playbillLite.endTime
        })

      const endIndex = _.findLastIndex(playbillList, (playbillLite: PlaybillLite) => {
          return +playbillLite.startTime <= endTime
        })

      if (beginIndex >= 0 && endIndex >= beginIndex) {
          return playbillList.slice(beginIndex, endIndex + 1)
        }
    }

  private getStaticCachedDayPlaybillListByTimeSpan (channelID: string, beginTime: number, endTime: number): Array<PlaybillLite> {

      const beginDayOfYear = this.getDayOfYear(beginTime)
      const endDayOfYear = this.getDayOfYear(endTime)

      if (beginDayOfYear === endDayOfYear) {
          return this.getStaticCachedPlaybillListByDayOfYear(channelID, beginDayOfYear)
        } else {
          const beginDayList = this.getStaticCachedPlaybillListByDayOfYear(channelID, beginDayOfYear)
          let endDayList = this.getStaticCachedPlaybillListByDayOfYear(channelID, endDayOfYear)

          if (_.isEmpty(beginDayList) || _.isEmpty(endDayList)) {
              return []
            } else {
              const lastOfBeginList: PlaybillLite = _.last(beginDayList)
              const firstOfEndList: PlaybillLite = _.first(endDayList)
              if (firstOfEndList && firstOfEndList && lastOfBeginList.ID === firstOfEndList.ID) {
                  log.debug('remove duplicated playbill, channelID:%s, playbillID:%s', firstOfEndList.channelID, firstOfEndList.ID)
                  endDayList = endDayList.slice(1)
                }
              return [...beginDayList, ...endDayList]
            }
        }
    }

  private getStaticCachedPlaybillListByDayOfYear (channelID: string, dayOfYear: number): Array<PlaybillLite> {
      const map = this.getStaticCachedMap(channelID, dayOfYear)
      return map ? map.getValues() : []
    }

  private getStaticCachedMap (channelID: string, dayOfYear: number): Map<PlaybillLite> {
      return this.staticCache[channelID] && this.staticCache[channelID][dayOfYear] && this.staticCache[channelID][dayOfYear].map
    }

  getCachedPlaybillByID (playbillID: string): PlaybillLite {

      const channelIDs = Object.keys(this.staticCache)
      let playbill
      channelIDs.find((channelID) => {
          const channelCache = this.staticCache[channelID]
          if (channelCache) {
              const dayOfYears = Object.keys(channelCache)
              return !!dayOfYears.find((dayOfYear) => {
                  const map = this.getStaticCachedMap(channelID, +dayOfYear)
                  if (map) {
                      playbill = map.get(playbillID)
                    }

                  return !!playbill
                })
            }
        })

      return playbill
    }

  private updateCachedPlaybills () {
      const cachedDays = this.getCachedDays()
      const playbillVersionPromises = _.map(cachedDays, day => this.queryPlaybillVersion(day))

      this.clearLocalPlaybillVersions(cachedDays)

      Promise.all(playbillVersionPromises).then((newVersions: Array<LocalPlaybillVersions>) => {
          _.each(cachedDays, (day, i) => {
              const newVersionsOfDay = newVersions[i][day]
              const oldVersionsOfDay = this.localPlaybillVersions[day]
              if (oldVersionsOfDay && oldVersionsOfDay.length) {
                  const changedChannelIds = this.getVersionChangedChannelIds(newVersionsOfDay, oldVersionsOfDay, day)
                  if (changedChannelIds.length) {
                      log.debug('playbill versons changed, dayOfYear: %s, changed channelIds: %s', day, changedChannelIds)
                      EventService.emit('PLAYBILL_VERSION_CHANGED')
                      const pageList = this.getPageList(changedChannelIds, changedChannelIds[0])
                      _.each(pageList, channelIds => this.queue.push({
                          channelIDs: channelIds,
                          dayOfYear: day
                        }))
                    }
                } else {
                  this.localPlaybillVersions[day] = newVersionsOfDay
                }
            })

          if (this.queue.length) {
              this.consumeQueue()
            }
        })
    }

  private getCachedDays (): Array<number> {
      const allChannelIDs = Object.keys(this.staticCache)
      const firstCachedChannel = this.staticCache[_.first(allChannelIDs)]
      if (firstCachedChannel) {
          let days = _.map(Object.keys(firstCachedChannel), day => +day)
          _.each(allChannelIDs, (channelID) => {
              _.each(days, (day) => {
                  const outOfDate = !_.contains(this.dateRange, day) && day < _.last(this.dateRange)
                  if (this.staticCache[channelID] && this.staticCache[channelID][day] && outOfDate) {
                      delete this.staticCache[channelID][day]
                    }
                })
            })
          days = _.filter(days, (day) => {
              return day >= _.last(this.dateRange)
            })
          return days
        } else {
          const days = []
          _.times(Config.get('preparePlaybillDays', PREPARE_DAYS), i => {
              const dayOfYear = this.getDayOfYear(moment().add('day', i).toDate().getTime())
              days.push(dayOfYear)
            })
          return days
        }
    }

  private getBdginAndEndTimeOfDay (dayOfYear: number): { beginTime: number; endTime: number } {
      const beginTime = moment().hour(0).minute(0).second(0).dayOfYear(dayOfYear)
      const endTime = beginTime.clone().add('day', 1)
      return {
          beginTime: beginTime.toDate().getTime(),
          endTime: endTime.toDate().getTime()
        }
    }

  private queryPlaybillVersion (dayOfYear: number): Promise<LocalPlaybillVersions> {
      if (this.hasNotCache()) {
          log.debug('[queryPlaybillVersion] can not queryPlaybillVersion')
          return
        }
      const times = this.getBdginAndEndTimeOfDay(dayOfYear)

      let req: QueryPlaybillVersionRequest = {
          fromDate: times.beginTime + '',
          toDate: times.endTime + ''
        }

      const hasTimeRange = !(_.isEmpty(this.localPlaybillVersions) || !this.localPlaybillVersions[dayOfYear])
      if (hasTimeRange) {
          req.timeRange = (Date.now() - this.timeRange) + ''
        }

      return queryPlaybillVersion(req).then(resp => {
          const playbillVersions = resp.playbillVersion || []
          if (!this.localPlaybillVersions[dayOfYear]) {
              this.localPlaybillVersions[dayOfYear] = playbillVersions
            }
          return {
              [dayOfYear]: playbillVersions
            }
        }, () => ({ [dayOfYear]: [] }))
    }

  private getVersionChangedChannelIds (newVersions: Array<PlaybillVersion>,
        oldVersions: Array<PlaybillVersion>, dayOfYear: number): Array<string> {
      const channelIDs = []

      _.each(newVersions, newVersion => {
          const oldVersion = _.find(oldVersions, ov => ov.channelID === newVersion.channelID)
          if (oldVersion.version !== newVersion.version) {
              const cachedChannels = this.staticCache[oldVersion.channelID]
              if (cachedChannels && cachedChannels[dayOfYear]) {
                  channelIDs.push(oldVersion.channelID)
                  delete this.staticCache[oldVersion.channelID][dayOfYear]
                }
              oldVersion.version = newVersion.version
            }
        })

      return channelIDs
    }

  private clearLocalPlaybillVersions (cachedDays: Array<number>) {
      if (this.localPlaybillVersions) {
          const localKeys = Object.keys(this.localPlaybillVersions)
          _.each(localKeys, key => {
              if (cachedDays.indexOf(+key) === -1) {
                  delete this.localPlaybillVersions[key]
                }
            })
        }
    }

  public getPlaybillContextFromCache (req: PlaybillContextFromCacheRequest): ChannelPlaybillContext {
      _.defaults(req, { preNumber: 0, nextNumber: 0 })

      const millisecondsOfOneDay = 24 * 3600 * 1000
      const preDate = req.date - millisecondsOfOneDay
      const nextDate = req.date + millisecondsOfOneDay

        // only find from three day
      const playbillList = this.getStaticCachedDayPlaybillListByTimeSpan(req.channelID, preDate, nextDate)

      const index = _.findIndex(playbillList, (playbill: PlaybillLite) => {
          return +playbill.startTime <= req.date && req.date <= +playbill.endTime
        })

      if (index > 0) {
          return {
              prePlaybillLites: playbillList.slice(Math.max(index - req.preNumber, 0), index),
              currentPlaybillLite: playbillList[index],
              nextPlaybillLites: playbillList.slice(index + 1, Math.min(index + 1 + req.nextNumber, playbillList.length))
            }
        }
    }

    /**
     * set cache's date range
     */
  setCacheDateRange (day: number) {
      day = day + 1 || EARLIEST_DAY
      const currentDayOfYear = this.getDayOfYear(Date.now())
      _.times(day, (i) => {
          this.dateRange[i] = currentDayOfYear - i
        })
    }

    /**
     * query channel's future playbills in order to refresh series record status
     */
  querySingleChannelFuturePlaybills (channelID: string, startTime: number, endTime: number) {
      const req = this.buildQueryPlaybillRequest([channelID], startTime, endTime)
      return queryPlaybillList(req, { isShowLoading: false, params: { scene: 'EpgTVguide', 'SID': 'queryplaybilllist5' } })
    }

    /**
     * It is used to clear dynamicCache, staticCache and localPlaybillVersions when changed language
    */
  clearCache () {
      this.dynamicCache = {}
      this.staticCache = {}
      this.localPlaybillVersions = {}
    }

  clearDynamicCache () {
      this.dynamicCache = {}
    }
}
