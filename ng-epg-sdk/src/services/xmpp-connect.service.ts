import { Logger } from './../utils/logger'
import * as _ from 'underscore'
import { Injectable } from '@angular/core'
import { Config as SDKConfig } from '../core'
import { hwMd5 } from '../utils/crypto'
import { stbService } from '../stb/stb.service'

import { submitDeviceInfo, pushMsgByTag } from '../vsp/api'
import { session } from '../utils/session'

const log = new Logger('xmpp-connect.service.ts')

interface ImpConfig {
  impIp?: any
  impPort?: any
  imDomain?: any
  OTTIMPIP?: any
  OTTIMPPort?: any
  ImpAuthType?: any
  pushServerURL?: any
}
@Injectable()
export class XmppConnectApp {

  private ProfileID
  private ConfigSTBDomain

  constructor () { }

    /*
         * Configure the TVMS parameters
         */
    /*
 * Configure the TVMS parameters
 */
  configurate (resp, tvmsType, profileID, configSTBDomain) {
      this.ProfileID = profileID
      this.ConfigSTBDomain = configSTBDomain
      let disableTVMS = '0'
      log.debug('configurate() disableTVMS=0  tvmsType: %s', tvmsType)
      stbService.xmppDisable()
      log.debug('===isSupportXMPP===', stbService.isSupportXMPP())
      if (stbService.isSupportXMPP() && tvmsType !== disableTVMS) {

            // Determine whether to support the XMPP protocol
          log.debug('supportXMPP and tvmsType is not string 0')
            // If tvmsType is string "0",TVMS will not be supported
          this._setIMPInfo(resp)
        }
    }

    /*
     * get impconfig
     */

    /**
        stb with IMP server to establish long connection
     */
  private _getIMPConfig (resp) {
      log.debug('begin  _getIMPConfig() resp:%s', resp)
      if (_.isEmpty(resp.configurations)) {
          return {}
        }
      let configurations = _.find(resp.configurations, (val: any) => {
          return val.cfgType === '2'
        }) || []

        /**
         * After passing the authentication, the configuration information
         * of the IMP server is obtained by QueryCustomzieConfig (type 2) of the VSP.
         * The parameters include IPTVIMPIP (IMP server address in the IPTV field),
         * IPTVIMPPort (IMP service port in the IPTV field) and IMDomain ), And OTTIMPIP,
         *  OTTIMPPort, IMPAUTHTYPE.
         */
      let impCfg: ImpConfig = {}
      impCfg.impIp = configurations.IPTVIMPIP
      impCfg.impPort = configurations.IPTVIMPPort
      impCfg.imDomain = configurations.IMDomain
      impCfg.OTTIMPIP = configurations.OTTIMPIP
      impCfg.OTTIMPPort = configurations.OTTIMPPort
      impCfg.ImpAuthType = configurations.ImpAuthType
        // If method to determine the value assigned to the impCfg.pushServerURL
      if (impCfg.impIp && impCfg.impPort) {
          log.debug('impCfg.impIp && impCfg.impPort is exist and the template is IPTV')
          impCfg.pushServerURL = {
              pushServerURL: impCfg.impIp + ':' + impCfg.impPort
            }
        } else if (impCfg.OTTIMPIP && impCfg.OTTIMPPort) {
          log.debug('impCfg.OTTIMPIP && impCfg.OTTIMPPort is exist and the template is OTT')
          impCfg.pushServerURL = {
              pushServerURL: impCfg.OTTIMPIP + ':' + impCfg.OTTIMPPort
            }
        }
      log.debug('end  _getIMPConfig() ')
      return impCfg
    }

    /**
     * Report the device information
     * @param  {[type]} fullJid [Locally generated deviceToken]
     */
    /**
     * In the SubmitDeviceInfo interface, the third-party
     *  message push platform (APNS / GCM / MPNS) token is used for the deviceToken parameter,
     * or the full JID of the XMPP if the platform is an IMP platform (that is, Of the JID, the resource ID for the logical device ID).

     * For the pushStatus parameter, EPGGW needs to determine whether to accept the message parameters configured in the user profile.
     */
  private _submitDeviceInfo (fullJid) {
      log.debug('===========Begin SubmitDeviceInfo============ fullJid:%s', fullJid)
        // Whether to use GatewayApi
      return submitDeviceInfo({
          deviceType: 'HUAWEI',
          deviceToken: fullJid
        })
    }

    /**Support for xmpp after the operation
     * [set IMPInfo]
     * @type {[type]}
     */
  _setIMPInfo (resp) {
      let userToken = stbService.read('usertoken')
      log.debug('TVMS _setIMPInfo(),configurations[%s]', resp.configurations)
      let impCfg: ImpConfig = this._getIMPConfig(resp)
      if (_.isEmpty(impCfg)) {
          return
        }

        /**
         * NodeIdentifier @ Domain / ResourceIdentifier, "NodeIdentifier @ Domain" for RAW JID,
         * that  NodeIdentifier for the user's IMP account, the default profileID (IMP only allows numbers,
         * letters, underscores, decimal point, colon,%; other special characters need to escape the terminal :% Xx,
         *  that  xx is the hexadecimal value of the character)
         */
      if (impCfg.pushServerURL && impCfg.imDomain) {
          let xmppJid = this.getParametersxmppJid(impCfg)
          let fullJid = xmppJid + '/' + SDKConfig.deviceID
          let XMPPJID = {
              XMPPJID: fullJid,
              XMPPPasscode: ''
            }
          XMPPJID = this.isMd5Encryption(impCfg, XMPPJID)
          this.setIPTVAndOTTConfiguration(impCfg, XMPPJID, userToken)
          const pushMsg = () => {
              pushMsgByTag({
                  deviceToken: fullJid,
                  tag: 'startup'
                })
            }
          this._submitDeviceInfo(fullJid).then(pushMsg, pushMsg)
        }
    }

  getParametersxmppJid (impCfg) {
      let reg = /[\w\.\:\%]/
      let IDString = ''
      if (this.ProfileID) {
          for (let i = 0; i < this.ProfileID.length; i++) {
              if (reg.test(this.ProfileID[i])) {
                  IDString += this.ProfileID[i]
                } else {
                  let escapeString = '%' + this.ProfileID.charCodeAt(i).toString(16)
                  IDString += escapeString
                }
            }
        }
      let xmppJid = IDString + '@' + impCfg.imDomain
      return xmppJid
    }

  isMd5Encryption (impCfg, XMPPJID) {
        // MD5
      if (impCfg.ImpAuthType !== '1') {
            // if ImpAuthType is not   1. the method is MD5 ,and XMPPJID.XMPPPasscode is hwMd5(profilePassword)
          let encryptIdPassword = session.get('profilesIdPassword')
          let profilesIdPassword = []
          profilesIdPassword = (encryptIdPassword ? stbService.aesDecrypt(encryptIdPassword) : [])
          if (profilesIdPassword) {
              let index = _.indexOf(_.pluck(profilesIdPassword, 'ID'), this.ProfileID)
              if (index > -1) {
                  let profilePassword = profilesIdPassword[index].password
                  XMPPJID.XMPPPasscode = hwMd5(profilePassword)
                  log.debug('hwMd5 and impCfg.ImpAuthType is not string 1. impCfg.ImpAuthType :%s', impCfg.ImpAuthType)
                } else {
                  log.debug('profilesIdPassword array is not null, but current login profile  password is not store')
                }
            } else {
              log.debug('profilesIdPassword is null')
            }
        }
      return XMPPJID
    }

  setIPTVAndOTTConfiguration (impCfg, XMPPJID, userToken) {
        // IPTV mould or OTT mould impCfg.pushServerURL is difference.
      stbService.write('XMPPpushServerUri', JSON.stringify(impCfg.pushServerURL))
      log.debug('XMPPpushServerUri :%s', impCfg.pushServerURL)
        // if ImpAuthType is  undefined,the stb is only supporting MD5 method
      if (!impCfg.ImpAuthType) {
          stbService.write('XMPPJID', JSON.stringify(XMPPJID))
          log.debug(' impCfg.ImpAuthType is undefined success write XMPPJID ,the method is hwMD5')
        } else {
            // if ImpAuthType is  1 or 0 XMPPJID need write to stb
          stbService.write('impAuthType', impCfg.ImpAuthType)
          stbService.write('XMPPJID', JSON.stringify(XMPPJID))
          if (impCfg.ImpAuthType === '1') {
                // if ImpAuthType is '1' the method is token and need write in accesstoken
              stbService.write('accessToken', userToken)
              log.debug('success write accessToken')
            }
          log.debug('TVMS  end _setIMPInfo(),')
        }
      stbService.xmppEnable()
      log.debug('TVMS _setIMPInfo(), stbService.xmppEnable() ')
    }

}
