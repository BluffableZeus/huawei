import { isSTB } from './../utils/is_stb'
import * as _ from 'underscore'
import { Injectable, NgZone } from '@angular/core'

import { Config, OnNotification } from '../core'
import { ajax, AjaxSuccess, AjaxFail } from '../utils'
import { Logger } from '../utils/logger'
import { msaErrorCodeMap } from '../core/const'

const digitalReg = /\d+/
const sqmUrl = 'http://127.0.0.1:37080/epgkpi'
const log = Logger.get('sdk:sqmprobe.service.ts')

const INTERFACE_MAP = {
  QueryRecmVODList: 'Recommendation',
  QueryRecmContent: 'Recommendation',
  Search: 'Search',
  GetVODDetail: 'VODDetail',
  QueryVODListBySubject: 'VODList',
  QuerySubjectVODBySubjectID: 'VODList',
  QueryPlaybillList: 'TVGuide',
  QueryMyTVHomeData: 'HomePage',
  LoginRoute: 'LoginRoute',
  Login: 'Login',
  Logout: 'Login',
  Authenticate: 'Authenticate',
  SwitchProfile: 'Authenticate'
}

/**
 * SQM service class
 *
 * @export
 * @class SQMProbeService
 * @implements {OnNotification}
 */
@Injectable()
export class SQMProbeService implements OnNotification {

  private _ajax = ajax

  private _eventList = []

  private timeoutId

  constructor (private ngZone: NgZone) {

    }

  onNotification (eventName, event) {
      switch (eventName) {
          case 'AJAX_FAIL':
          case 'AJAX_SUCCESS':
            if (isSTB) {
                  this._eventList.push(event)
                  clearTimeout(this.timeoutId)
                  if (this._eventList.length > Config.get('event_list_max_num', 20)) {
                      this.updateEventList()
                    } else {
                      this.ngZone.runOutsideAngular(() => {
                          this.timeoutId = setTimeout(this.updateEventList.bind(this), Config.get('postpone_request_milliseconds', 5000))
                        })
                    }
                }
            break
        }
    }

  private sendToSQM (event: AjaxSuccess | AjaxFail) {
      let action = INTERFACE_MAP['' + event.request.inf]

      if (action && Config.isSupportSQMProbe) {
          const retCode = event.response.data.result.retCode
          let status = retCode.indexOf('http.') >= 0 ? +digitalReg.exec(retCode)[0] : 200
          const url = sqmUrl + '?action=' + action + '&code=' + status + '&result=' + (status === 200 ? '0' : '-1')
                + '&delay=' + event.response.usedTime
          this._ajax(url, null, {
              method: 'GET',
              isIgnoreError: true
            }).catch(() => {
              log.error('[MSA_ERROR] report sqm fail, [url:%s] [error code:%s]', url, msaErrorCodeMap.interface['38035'])
            })
        }
    }
  private updateEventList () {
      const list = this._eventList
      this._eventList = []
      _.each(list, (e) => {
          this.sendToSQM(e)
        })
    }
}
