import { Logger } from '../utils/logger'
import { idbStorge } from './../utils/idb-storge'

const log = new Logger('version-store')

interface VersionItem<T> {
  version: string
  resp: T
  key: string
}

/**
 * According to the version number, cache the content tool class
 *
 * @export
 * @class VersionStore
 * @template T
 */
export class VersionStore<T> {

  private data: VersionItem<T>

  constructor (private storeName: string, private profileID?: string) {
    }

  public setProfileID (profileID: string) {
      this.profileID = profileID
    }

  public save (version: string, resp: T): Promise<void> {
      if (!version && this.data) {
          this.data.resp = resp
        }

      if (this.data && version === this.data.version && this.data.resp === resp) {
          return Promise.resolve()
        }
      const key = this.getKey()
      this.data = { version, resp, key }

      log.debug('save key:%s, version:%s', this.getKey(), version)
      return idbStorge.setItem(this.getKey(), this.data)
    }

  public getData (newVersion = ''): Promise<T> {

      const resp = this.getResp(newVersion, this.data)
      if (resp) {
          return Promise.resolve(resp)
        } else {
          return idbStorge.getItem<VersionItem<T>>(this.getKey()).then((result) => {
              log.debug('getData from indexDB key:%s, version:%s', this.getKey(), result && result.version)
              this.data = result
              return this.getResp(newVersion, result)
            }, () => {
              return undefined
            })
        }
    }

  public clear (): Promise<void> {
      return idbStorge.removeItem(this.getKey())
    }

  public getVersion () {
      return this.data && this.data.version
    }

  private getKey (): string {
      return `${this.storeName}_${this.profileID || ''}`
    }

  private getResp (version: string, data: VersionItem<T>): T {
      if (data) {
          if (version <= data.version && data.key === this.getKey()) {
              return data.resp
            }
        }
    }
}
