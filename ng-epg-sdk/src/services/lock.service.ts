import * as _ from 'underscore'

import { Injectable } from '@angular/core'
import { ChannelService } from './channel.service'
import {
    Lock, createLock, DeleteLockRequest, DeleteLockResponse, deleteLock, LockItem,
    QueryLockResponse, CreateLockRequest, CreateLockResponse, queryLock, VOD, Channel, Subject, Playbill, Genre, LockSeries
} from '../vsp/api'
import { ContentUtils } from '../utils'
import { ContentIndexer } from './content-indexer'
import { VersionStore } from './version-store'
import { HeartbeatService } from './heartbeat.service'
import { AjaxSuccess } from '../utils'
import { Logger } from '../utils/logger'
import { SessionService } from './session.service'

const log = new Logger('lock.service')
const QUERY_COUNT = 50
/**
 * Manage the lock information of the current profile.
 * If you operate the lock of other profiles, request the interface directly
 */
@Injectable()
export class LockService {

  private _queryLock = queryLock

  private _createLock = createLock

  private _deleteLock = deleteLock

  private _store: VersionStore<QueryLockResponse>

  private _preStore: VersionStore<QueryLockResponse>

  private _indexer: ContentIndexer

  private preLockVersion: string

  private lockVersion: string

  private oldIndexer: ContentIndexer

  constructor (private _sessionService: SessionService, private _channelService: ChannelService) {
    }

  refreshIndexer () {
      this.oldIndexer = _.clone(this._indexer)
    }

    /**
     * Initialize store
     *
     * @private
     *
     * @memberOf LockService
     */
  private initStore () {
      this._store = this._store || new VersionStore<QueryLockResponse>('lock', this._sessionService.getProfileID())
      this._preStore = this._preStore || new VersionStore<QueryLockResponse>('preLock', this._sessionService.getProfileID())
    }

    /**
     * Lock version change processing
     *
     * @private
     * @param {string} version
     * @returns
     *
     * @memberOf LockService
     */
  private onLockVersion (version: string) {
      this.initStore()
      return this.refresh(version)
    }

    /**
     * Listen Ajax successfully
     *
     * @private
     * @param {AjaxSuccess} data
     *
     * @memberOf LockService
     */
  private onAjaxSuccess (data: AjaxSuccess) {
      switch (data.request.inf) {
          case 'Logout':
          case 'SwitchProfile':
            this.clearLockCache()
            break
          default:
            break
        }
    }

  clearLockCache () {
      this._store = null
      this._preStore = null
    }

    /**
     * Cache data, and update the index
     *
     * @param {string} version
     * @param {QueryLockResponse} resp
     *
     * @memberOf LockService
     */
  private saveAndIndex (version: string, resp: QueryLockResponse, canRefresh = true): Promise<void> {
      resp.locks = ContentUtils.sortContentList(resp.locks as any, this._channelService) as any
      this._indexer = new ContentIndexer(resp.locks as any)
      this.lockVersion = version
      if (canRefresh) {
          this.refreshIndexer()
        }
      return this._store.save(version, resp)
    }

  private savePreLock (version: string, resp: QueryLockResponse): Promise<void> {
      resp.locks = ContentUtils.sortContentList(resp.locks as any, this._channelService) as any
      this.preLockVersion = version
      return this._preStore.save(version, resp)
    }

    /**
     * Get cache data
     *
     * @private
     * @returns {QueryLockResponse}
     *
     * @memberOf LockService
     */
  private getStoreData (): Promise<QueryLockResponse> {
      return this._store.getData().then((cachedResp) => {
          return _.extend({ locks: [] }, cachedResp)
        })
    }

  public onNotification (eventName: string, data: any): any {
      switch (eventName) {
          case HeartbeatService.LOCK_VERSION:
            return this.onLockVersion(data as string)

          case 'AJAX_SUCCESS':
            return this.onAjaxSuccess(data)

          default:
        }
    }

    /**
     * Update data based on Lock version number
     *
     * @param {string} version
     * @returns {Promise<any>}
     *
     * @memberOf LockService
     */
  public refresh (version: string): Promise<any> {
      log.debug('begin refresh(), version:%s', version)
      const splits = version.split('-')
      this.preLockVersion = splits[0]
      this.lockVersion = splits[1]
        // Refresh the data
      const promise = Promise.all([this._store.getData(this.lockVersion), this._preStore.getData(this.preLockVersion)])
      return promise.then(([cachedResp, preLockCachedResp]) => {
          if (cachedResp && preLockCachedResp) {
              log.debug('[refresh] hit the cache')
              this._indexer = new ContentIndexer(cachedResp.locks as any)
            } else {
              log.debug('[refresh] not hit the cache')
              return this.iterateQueryLock(0, QUERY_COUNT)
            }
        })
    }

  iterateQueryLock (preOffset: number, preCount: number, preResp?: QueryLockResponse) {
      let offset: number
      if (preResp) {
          const total = +preResp.total
          offset = preOffset + QUERY_COUNT
          preCount = preCount + QUERY_COUNT
          if (+offset >= total) {
              return
            }
        } else {
          offset = preOffset
          preCount = preCount + QUERY_COUNT
        }
      return this._queryLock({ offset: '' + offset, count: '' + QUERY_COUNT }).then((resp: QueryLockResponse) => {
          if (!resp) {
              return
            }
          if (preResp) {
              resp.locks = preResp.locks ? preResp.locks.concat(resp.locks) : [].concat(resp.locks)
            }
          this.saveAndIndex(this.lockVersion, resp, false)
          this.savePreLock(this.preLockVersion, resp)
          return this.iterateQueryLock(offset, preCount, resp)
        })
    }

    /**
     * According to ID and content type, to determine whether the content lock
     *
     * @param {string} id
     * @param {string} contentType
     * @returns
     *
     * @memberOf LockService
     */
  isLocked (id: string, contentType: string) {
      if (this.oldIndexer) {
          return this.oldIndexer.contains(id, contentType)
        }
      return false
    }

    /**
     * According to the content type, the lock list is returned
     *
     * @param {string} [contentType]
     * @returns {(Array<VOD | Channel | Subject | Playbill | Genre | LockSeries>)}
     *
     * @memberOf LockService
     */
  getLockList (contentType?: string): Array<VOD | Channel | Subject | Playbill | Genre | LockSeries> {
      if (this.oldIndexer) {
          return this.oldIndexer.getList(contentType)
        }
      return []
    }

    /**
     * Add a lock
     *
     * @param {CreateLockRequest} req
     * @returns {Promise<CreateLockResponse>}
     *
     * @memberOf LockService
     */
  createLock (req: CreateLockRequest): Promise<CreateLockResponse> {
      log.debug('begin createLock(), req:%s', req)

      return this._createLock(req).then((resp) => {
          return this.getStoreData().then((cachedResp) => {
              log.debug('end createLock(), resp:%s', resp)
              const oldRespLocks = cachedResp.locks
              cachedResp.locks = oldRespLocks.concat(_.map(req.locks as any, (lock: Lock) => {
                  const name = ContentUtils.normalizeName(lock.lockType)
                  const ret = { lockType: lock.lockType }
                  ret[name] = {
                      ID: lock.itemID,
                      contentID: lock.itemID,
                      lockType: lock.lockType,
                      type: lock.lockType
                    }
                  return ret
                }) as any)

              if (resp.preVersion === this.lockVersion) {
                  this.savePreLock(resp.preVersion || this.preLockVersion, cachedResp)
                }
              this.saveAndIndex(resp.version || this.lockVersion, cachedResp)
              return resp
            })
        }, (resp: CreateLockResponse) => {
          log.debug('end createLock(), resp:%s', resp)
          return Promise.reject(resp)
        }) as any

    }

    /**
     * Remove the lock
     *
     * @param {DeleteLockRequest} req
     * @returns {Promise<DeleteLockResponse>}
     *
     * @memberOf LockService
     */
  deleteLock (req: DeleteLockRequest): Promise<DeleteLockResponse> {
      log.debug('begin deleteLock(), req:%s', req)

      return this._deleteLock(req).then((resp) => {
          return this.getStoreData().then((cachedResp) => {
              log.debug('end deleteLock(),resp:%s', resp)
              const oldRespLocks = cachedResp.locks

              if (_.isEmpty(req.itemIDs)) {
                  cachedResp.locks = _.reject(oldRespLocks as any, (lockContent: LockItem) => {
                      const isFounded = !!_.find(req.lockTypes as any, (lockType: string) => {
                          return ContentUtils.normalizeName(lockType) === ContentUtils.normalizeName(lockContent.lockType)
                        })

                      return isFounded
                    }) as any
                } else {
                  if (req.deleteType !== '1') {
                      cachedResp.locks = _.reject(oldRespLocks as any, (lockContent: LockItem) => {
                          const name = ContentUtils.normalizeName(lockContent.lockType)
                          const isFounded = !!_.find(req.itemIDs as any, (id: string, index: number) => {
                              return ContentUtils.getID(lockContent[name]) === id
                                    && ContentUtils.normalizeName(lockContent.lockType) === ContentUtils.normalizeName(req.lockTypes[index])
                            })

                          return isFounded
                        }) as any
                    } else {
                        // vod or channel
                      let difTypeLocks = _.reject(oldRespLocks as any, (lockContent: LockItem) => {
                          const isFounded = !!_.find(req.lockTypes as any, (lockType: string) => {
                              return ContentUtils.normalizeName(lockType) === ContentUtils.normalizeName(lockContent.lockType)
                            })

                          return isFounded
                        })
                        // deleteType = '1'
                      let noDeleteLocks = _.filter(oldRespLocks, (lockContent: LockItem) => {
                          const name = ContentUtils.normalizeName(lockContent.lockType)
                          const isFounded = !!_.find(req.itemIDs, (id: string, index: number) => {
                              return ContentUtils.normalizeName(ContentUtils.getContentType(lockContent)) === ContentUtils.normalizeName(req.lockTypes[index])
                                    && ContentUtils.getID(lockContent[name]) === id
                            })

                          return isFounded
                        })
                      cachedResp.locks = noDeleteLocks.concat(difTypeLocks)
                    }
                }

              if (resp.preVersion === this.lockVersion) {
                  this.savePreLock(resp.preVersion || this.preLockVersion, cachedResp)
                }
              this.saveAndIndex(resp.version || this.lockVersion, cachedResp)
              return resp
            })
        }, (resp: CreateLockResponse) => {
          log.debug('end deleteLock(),resp:%s', resp)
          return Promise.reject(resp)
        }) as any
    }
}
