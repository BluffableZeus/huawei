import * as _ from 'underscore'

import { onLineHeartbeat, PersonalDataVersion, OnLineHeartbeatResponse } from '../vsp/api'
import { EventService } from './event.service'
import { Injectable } from '@angular/core'
import { Logger } from '../utils/logger'

const log = new Logger('sdk:heartbeat.service.ts')
const MIN_HEART_TIME = 60

/**
 * Heartbeat class
 *
 * @export
 * @class HeartbeatService
 */
@Injectable()
export class HeartbeatService {

  static CHANNEL_VERSION = 'CHANNEL_VERSION'

  static CHANNEL_STATIC_VERSION = 'CHANNEL_STATIC_VERSION'

  static CHANNEL_DYNAMIC_VERSION = 'CHANNEL_DYNAMIC_VERSION'

  static BOOKMARK_VERSION = 'BOOKMARK_VERSION'

  static FAVORITE_VERSION = 'FAVORITE_VERSION'

  static LOCK_VERSION = 'LOCK_VERSION'

  static REMINDER_VERSION = 'REMINDER_VERSION'

  static CHANNEL_VERSION_CHANGED = 'CHANNEL_VERSION_CHANGED'

  static CHANNEL_STATIC_VERSION_CHANGED = 'CHANNEL_STATIC_VERSION_CHANGED'

  static CHANNEL_DYNAMIC_VERSION_CHANGED = 'CHANNEL_DYNAMIC_VERSION_CHANGED'

  static BOOKMARK_VERSION_CHANGED = 'BOOKMARK_VERSION_CHANGED'

  static FAVORITE_VERSION_CHANGED = 'FAVORITE_VERSION_CHANGED'

  static REMINDER_VERSION_CHANGED = 'REMINDER_VERSION_CHANGED'

  static LOCK_VERSION_CHANGED = 'LOCK_VERSION_CHANGED'

  static PERSONAL_DATA_VERSION_CHANGED = 'PERSONAL_DATA_VERSION_CHANGED'

  static USERFILTER_VERSION = 'USERFILTER_VERSION'

  static USERFILTER_VERSION_CHANGED = 'USERFILTER_VERSION_CHANGED'

  private _lastVersionInfo = {}

  private _onLineHeartbeat = onLineHeartbeat

  private _onLineHeartbeatResponse: OnLineHeartbeatResponse

  private _timeoutId

  constructor () {
    }

    /**
     * Start timing heartbeat
     *
     * @returns {Promise<OnLineHeartbeatResponse>}
     *
     * @memberOf HeartbeatService
     */
  public startHeartbeat (): Promise<OnLineHeartbeatResponse> {
      this.stopHeartbeat()
      return this._onLineHeartbeat({}).then(this.onHeartbeatSuccess.bind(this), this.onHeartbeatFail.bind(this))
    }

    /**
     * Stop heartbeat
     *
     *
     * @memberOf HeartbeatService
     */
  public stopHeartbeat (): void {
      clearTimeout(this._timeoutId)
    }

    /**
       *
       * Gets the return value of the last heartbeat
       *
       * @returns {OnLineHeartbeatResponse}
       *
       * @memberOf HeartbeatService
       */
  public getOnLineHeartbeatResponse (): OnLineHeartbeatResponse {
      return this._onLineHeartbeatResponse
    }

    /**
     * get version
     */
  public getVersion (name: string): string {
      const eventName = this.getEventNameByVersionName(name)
      return this._lastVersionInfo[eventName]
    }

    /**
     * set version
     */
  public setVersion (name: string, version: string): void {
      const eventName = this.getEventNameByVersionName(name)
      this._lastVersionInfo[eventName] = version
    }

    /**
     *  update cache version, if preversion is same with cache version
     */
  public updateVersion (name: string, preVersion: string, currVersion: string) {
      if (preVersion === this.getVersion(name)) {
          this.setVersion(name, currVersion)
        }
    }

    /**
     * reset last version info
     */
  public resetLastVersionInfo () {
      this._lastVersionInfo = {}
    }

    /**
     * Heartbeat successful callback
     *
     * @private
     * @param {OnLineHeartbeatResponse} resp
     * @returns
     *
     * @memberOf HeartbeatService
     */
  private onHeartbeatSuccess (resp: OnLineHeartbeatResponse) {
      log.debug('[OnLineHeartbeat] success: %s', resp)
      this._onLineHeartbeatResponse = resp
      this.delayStartHeartbeat(+resp.nextCallInterval)

      return this.processPersonalDataVersion(resp.personalDataVersions, resp.userFilter).then(() => resp, () => resp)
    }

    /**
     * Heartbeat failure callback
     *
     * @param {OnLineHeartbeatResponse} resp
     * @returns {Promise<OnLineHeartbeatResponse>}
     *
     * @memberOf HeartbeatService
     */
  private onHeartbeatFail (resp: OnLineHeartbeatResponse): Promise<OnLineHeartbeatResponse> {
      log.debug('[OnLineHeartbeat] fail: %s', resp)
      this.delayStartHeartbeat(this._onLineHeartbeatResponse ? +this._onLineHeartbeatResponse.nextCallInterval : 15 * 60)
      return Promise.resolve(resp)
    }

    /**
     * Delay to start the next heartbeat
     *
     * @private
     * @param {OnLineHeartbeatResponse} resp
     *
     * @memberOf HeartbeatService
     */
  private delayStartHeartbeat (nextCallInterval: number): void {
      nextCallInterval = Math.max(nextCallInterval, MIN_HEART_TIME)
      this.stopHeartbeat()
      this._timeoutId = setTimeout(this.startHeartbeat.bind(this), nextCallInterval * 1000)
    }

    /**
     * @event CHANNEL_VERSION
     * @event CHANNEL_STATIC_VERSION
     * @event CHANNEL_DYNAMIC_VERSION
     * @event BOOKMARK_VERSION
     * @event FAVORITE_VERSION
     * @event LOCK_VERSION
     * @event REMINDER_VERSION
     * @event PROFILE_VERSION
     */
  private processPersonalDataVersion (personalDataVersion: PersonalDataVersion, userFilter: string): Promise<any> {

      log.debug('begin processPersonalDataVersion(), %s', personalDataVersion)

      personalDataVersion = _.extend({
          channel: '0|0',
          bookmark: '0',
          preFavorite: '0',
          favorite: '0',
          preLock: '0',
          lock: '0',
          reminder: '0',
          profile: '0',
          userFilter: '0'
        }, _.extend(personalDataVersion, { userFilter: userFilter }))

      const versionInfo = {}

      _.each(personalDataVersion as any, (version: string, name: string) => {
          if (_.isString(version)) {
              versionInfo[this.getEventNameByVersionName(name)] = version
            }
        })

        // Processing channel static and dynamic version number
      const channelVersions = personalDataVersion.channel.split('|')
      versionInfo['CHANNEL_STATIC_VERSION'] = _.first(channelVersions)
      versionInfo['CHANNEL_DYNAMIC_VERSION'] = personalDataVersion.channel

      let favoriteVersion = ''
      let lockVersion = ''
      const promises = _.map(versionInfo as any, (version: string, eventName: string) => {
          if (eventName === 'PREFAVORITE_VERSION') {
              favoriteVersion = version
            }
          if (eventName === 'FAVORITE_VERSION') {
              version = favoriteVersion + '-' + version
            }

          if (eventName === 'PRELOCK_VERSION') {
              lockVersion = version
            }
          if (eventName === 'LOCK_VERSION') {
              version = lockVersion + '-' + version
            }
          return EventService.emitAsync(eventName, version)
        })

      return Promise.all(promises).then(() => {
          if (this._lastVersionInfo) {
              let isChanged = false
              _.each(versionInfo as any, (version: string, eventName: string) => {
                  if (this._lastVersionInfo[eventName] !== version) {
                      EventService.emit(eventName + '_CHANGED', version)
                      isChanged = true
                    }
                })

              if (isChanged) {
                  EventService.emit('PERSONAL_DATA_VERSION_CHANGED')
                }
            }
          this._lastVersionInfo = versionInfo
          log.debug('end processPersonalDataVersion()')
        })
    }

  private getEventNameByVersionName (versionName: string) {
      return versionName.toUpperCase() + '_VERSION'
    }
}
