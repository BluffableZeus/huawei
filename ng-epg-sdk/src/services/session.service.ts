import * as _ from 'underscore'
import { Injectable } from '@angular/core'
import { AuthenticateResponse, SwitchProfileRequest } from '../vsp/api'
import { AjaxSuccess, session } from '../utils'

/**
 * Used to cache information
 *
 * @export
 * @class SessionService
 * @implements {OnNotification}
 */
@Injectable()
export class SessionService {

  private _authenticateResponse: AuthenticateResponse

  private _switchProfileReqest: SwitchProfileRequest

  private _channelNameSpace: string

  onNotification (eventName: string, data: any) {
      switch (eventName) {
          case 'AJAX_SUCCESS':
            return this.onAjaxSuccess(data)
        }
    }

  private onAjaxSuccess (data: AjaxSuccess) {

      switch (data.request.inf) {
          case 'Authenticate':
            this._authenticateResponse = data.response.data as AuthenticateResponse
            break
          case 'SwitchProfile':
            this._switchProfileReqest = data.request.data as SwitchProfileRequest
            break

          case 'Logout':
            this._authenticateResponse = null
            this._switchProfileReqest = null
            break
        }
    }

    /**
     * Get the subscriber ID
     *
     * @returns {string}
     *
     * @memberOf SessionService
     */
  getSubscribeID (): string {
      return this._authenticateResponse && this._authenticateResponse.subscriberID
    }

    /**
     *  Gets the currently registered ProfileID
     *
     * @returns {string}
     *
     * @memberOf SessionService
     */
  getProfileID (): string {
      return (this._switchProfileReqest && this._switchProfileReqest.profileID)
            || (this._authenticateResponse && this._authenticateResponse.profileID)
    }

  getDSTTime (): string {
      return this._authenticateResponse && this._authenticateResponse.DSTTime
    }

  get channelNameSpace (): string {
      return this._channelNameSpace || 'IPTV'
    }
  set channelNameSpace (channelNameSpace: string) {
      this._channelNameSpace = channelNameSpace
    }
  public queryCustomizeConfig (): string {
      const terminalConfigList = session.get('terminalConfigList')
      if (terminalConfigList && terminalConfigList.values) {
          _.each(terminalConfigList.values, (data: any) => {
              const values = _.first(data.values) as string
              if ('iptv_channel_name_space' === data.key && values) {
                  return this.channelNameSpace = values
                }
            })
        }

      return this.channelNameSpace
    }
}
