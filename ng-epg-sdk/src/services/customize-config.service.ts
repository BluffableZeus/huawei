import { QueryCustomizeConfigResponse, RatingSystem } from '../vsp/api'
import { session } from '../utils'

/**
 *
 * Template custom parameter service
 *
 * @export
 * @class CustomizeConfigService
 */
export class CustomizeConfigService {

    /**
     * Query content level system
     *
     * @returns {Promise<Array<RatingSystem>>}
     *
     * @memberOf CustomizeConfigService
     */
  queryRatingSystemList (): Array<RatingSystem> {
      const allParameterList: QueryCustomizeConfigResponse = session.get('allParameterList')
      const ratings = allParameterList && allParameterList.ratings
      return ratings
    }

}
