import * as _ from 'underscore'
import { Injectable } from '@angular/core'
import { Favorite, Content, Subject, VOD, Channel, Playbill, Genre, LockSeries } from '../vsp/api'
import {
    deleteFavorite, DeleteFavoriteRequest, DeleteFavoriteResponse, queryFavorite,
    QueryFavoriteResponse, CreateFavoriteRequest, createFavorite, CreateFavoriteResponse
} from '../vsp/api'
import { VersionStore } from './version-store'
import { ContentUtils } from '../utils'
import { ContentIndexer } from './content-indexer'
import { HeartbeatService } from './heartbeat.service'
import { SessionService } from './session.service'
import { ChannelService } from './channel.service'
import { AjaxSuccess } from '../utils'
import { Logger } from '../utils/logger'

const log = new Logger('favorite.service')
const QUERY_COUNT = 50

/**
 * Collection service class
 *
 * @export
 * @class FavoriteService
 */
@Injectable()
export class FavoriteService {

  private _store: VersionStore<QueryFavoriteResponse>
  private _preStore: VersionStore<QueryFavoriteResponse>
  private _queryFavorite = queryFavorite
  private _createFavorite = createFavorite
  private _deleteFavorite = deleteFavorite
  private preFavoriteVersion: string
  private favoriteVersion: string
  private oldIndexer: ContentIndexer
  private _indexer: ContentIndexer

  constructor (private _sessionService: SessionService, private _channelService: ChannelService) {
    }

  onNotification (eventName: string, data: any): any {
      switch (eventName) {
          case HeartbeatService.FAVORITE_VERSION:
            return this.onFavoriteVersion(data as string)

          case 'AJAX_SUCCESS':
            return this.onAjaxSuccess(data)

        }
    }

  refreshIndexer () {
      this.oldIndexer = _.clone(this._indexer)
    }

  private initStore () {
      this._store = this._store || new VersionStore<QueryFavoriteResponse>('favorite', this._sessionService.getProfileID())
      this._preStore = this._preStore || new VersionStore<QueryFavoriteResponse>('preFavorite', this._sessionService.getProfileID())
    }

  private onFavoriteVersion (version: string) {
      this.initStore()
      return this.refresh(version)
    }

  private onAjaxSuccess (data: AjaxSuccess) {
      switch (data.request.inf) {
          case 'Logout':
          case 'SwitchProfile':
            this.clearFavoriteCache()
            break
        }
    }

  clearFavoriteCache () {
      this._store = null
      this._preStore = null
    }

  private saveAndIndex (version: string, resp: QueryFavoriteResponse, canRefresh = true): Promise<void> {
      resp.favorites = ContentUtils.sortContentList(resp.favorites, this._channelService)
      this._indexer = new ContentIndexer(resp.favorites)
      this.favoriteVersion = version
      if (canRefresh) {
          this.refreshIndexer()
        }
      return this._store.save(version, resp)
    }

  private savePreFavorite (version: string, resp: QueryFavoriteResponse): Promise<void> {
      resp.favorites = ContentUtils.sortContentList(resp.favorites, this._channelService)
      this.preFavoriteVersion = version
      return this._preStore.save(version, resp)
    }

    /**
     * According to the collection version number to refresh the collection data
     *
     * @param {string} version
     * @returns {Promise<any>}
     *
     * @memberOf FavoriteService
     */
  refresh (version: string): Promise<any> {
      log.debug('begin refresh(), version:%s', version)
      this.preFavoriteVersion = version.split('-')[0]
      this.favoriteVersion = version.split('-')[1]
        // refresh data

      return Promise.all([this._store.getData(this.favoriteVersion), this._preStore.getData(this.preFavoriteVersion)])
            .then(([cachedResp, preFavoriteCachedResp]) => {
              if (cachedResp && preFavoriteCachedResp) {
                  log.debug('[refresh] hit the cache')
                  this._indexer = new ContentIndexer(cachedResp.favorites)
                } else {
                  log.debug('[refresh] not hit the cache')
                  return this.iterateQueryFavorite(0, QUERY_COUNT)
                }
            })
    }

  iterateQueryFavorite (preOffset: number, preCount: number, preResp?: QueryFavoriteResponse) {
      let offset: number
      if (preResp) {
          const total = +preResp.total
          offset = preOffset + QUERY_COUNT
          preCount = preCount + QUERY_COUNT
          if (+offset >= total) {
              return
            }
        } else {
          offset = preOffset
          preCount = preCount + QUERY_COUNT
        }
      return this._queryFavorite({ offset: '' + offset, count: '' + QUERY_COUNT }).then((resp: QueryFavoriteResponse) => {
          if (!resp) {
              return
            }
          if (preResp) {
              resp.favorites = preResp.favorites ? preResp.favorites.concat(resp.favorites) : [].concat(resp.favorites)
            }
          this.saveAndIndex(this.favoriteVersion, resp, false)
          this.savePreFavorite(this.preFavoriteVersion, resp)
          return this.iterateQueryFavorite(offset, preCount, resp)
        })
    }

    /**
     * To determine whether the contents of the collection
     *
     * @param {string} id
     * @param {string} contentType
     * @returns {boolean}
     *
     */
  isFavorited (id: string, contentType: string): boolean {
      if (this.oldIndexer) {
          return this.oldIndexer.contains(id, contentType)
        }
      return false
    }

    /**
     * According to contentType return collection, do not distinguish between audio and video,
     * if not pass, then return to all collections
     *
     * @param {string} [contentType]
     *
     *  - VIDEO_VOD
     *  - AUDIO_VOD
     *  - VIDEO_CHANNEL
     *  - AUDIO_CHANNEL
     *  - PROGRAM
     *  - SUBJECT
     *
     * @returns {(Array<VOD | Channel | Subject | Playbill | Genre | LockSeries>)}
     *
     * @memberOf FavoriteService
     */
  getFavoriteList (contentType?: string): Array<VOD | Channel | Subject | Playbill | Genre | LockSeries> {
      if (this.oldIndexer) {
          return this.oldIndexer.getList(contentType)
        }
      return []
    }

    /**
     * Add to Favorites
     *
     * @param {CreateFavoriteRequest} req
     * @returns {Promise<CreateFavoriteResponse>}
     *
     * @memberOf FavoriteService
     */
  createFavorite (req: CreateFavoriteRequest): Promise<CreateFavoriteResponse> {
      log.debug('begin createFavorite(), req:%s', req)

      return this._createFavorite(req).then((resp) => {
          return this.getStoreData().then((cachedResp) => {
              log.debug('end createFavorite(), resp:%s', resp)
              const oldCachedFavorites = cachedResp.favorites
              cachedResp.favorites = oldCachedFavorites.concat(_.map(req.favorites as any, (favorite: Favorite) => {
                  const ret = { contentType: favorite.contentType }
                  const name = ContentUtils.normalizeName(favorite.contentType)
                  ret[name] = {
                      ID: favorite.contentID,
                      contentID: favorite.contentID,
                      contentType: favorite.contentType,
                      type: favorite.contentType
                    }
                  return ret
                }))
              if (resp.preVersion === this.favoriteVersion) {
                  this.savePreFavorite(resp.preVersion || this.preFavoriteVersion, cachedResp)
                }
              this.saveAndIndex(resp.version || this.favoriteVersion, cachedResp)
              return resp
            })
        }, (resp) => {
          log.debug('end createFavorite(), resp:%s', resp)
          return Promise.reject(resp) as any
        })
    }

  private getStoreData (): Promise<QueryFavoriteResponse> {
      return this._store.getData().then((cachedResp) => {
          return _.extend({ favorites: [] }, cachedResp)
        })
    }

    /**
     * Delete the collection
     *
     * @param {DeleteFavoriteRequest} req
     * @returns {Promise<DeleteFavoriteResponse>}
     *
     * @memberOf FavoriteService
     */
  deleteFavorite (req: DeleteFavoriteRequest): Promise<DeleteFavoriteResponse> {
      log.debug('begin deleteFavorite(), req:%s', req)

      return this._deleteFavorite(req).then((resp) => {
          return this.getStoreData().then((cachedResp) => {
              log.debug('end deleteFavorite(), resp:%s', resp)
              const oldCachedFavorites = cachedResp.favorites
              if (_.isEmpty(req.contentIDs)) {
                  cachedResp.favorites = _.reject(oldCachedFavorites as any, (favoriteContent: Content) => {
                      const isFounded = !!_.find(req.contentTypes as any, (contentType: string) => {
                          return ContentUtils.normalizeName(contentType) === ContentUtils.normalizeName(favoriteContent.contentType)
                        })

                      return isFounded
                    })
                } else {
                    // delete part
                  if (req.deleteType !== '1') {
                        // deleteType = '0'
                      cachedResp.favorites = _.reject(oldCachedFavorites, (favoriteContent: Content) => {
                          const name = ContentUtils.normalizeName(favoriteContent.contentType)
                          const isFounded = !!_.find(req.contentIDs, (id: string, index: number) => {
                              return ContentUtils.normalizeName(ContentUtils.getContentType(favoriteContent)) === ContentUtils.normalizeName(req.contentTypes[index])
                                    && ContentUtils.getID(favoriteContent[name]) === id
                            })

                          return isFounded
                        })
                    } else {
                        // vod or channel
                      let difTypeFavorites = _.reject(oldCachedFavorites as any, (favoriteContent: Content) => {
                          const isFounded = !!_.find(req.contentTypes as any, (contentType: string) => {
                              return ContentUtils.normalizeName(contentType) === ContentUtils.normalizeName(favoriteContent.contentType)
                            })

                          return isFounded
                        })
                        // deleteType = '1'
                      let noDeleteFavorites = _.filter(oldCachedFavorites, (favoriteContent: Content) => {
                          const name = ContentUtils.normalizeName(favoriteContent.contentType)
                          const isFounded = !!_.find(req.contentIDs, (id: string, index: number) => {
                              return ContentUtils.normalizeName(ContentUtils.getContentType(favoriteContent)) === ContentUtils.normalizeName(req.contentTypes[index])
                                    && ContentUtils.getID(favoriteContent[name]) === id
                            })

                          return isFounded
                        })
                      cachedResp.favorites = noDeleteFavorites.concat(difTypeFavorites)
                    }
                }
              if (resp.preVersion === this.favoriteVersion) {
                  this.savePreFavorite(resp.preVersion || this.preFavoriteVersion, cachedResp)
                }
              this.saveAndIndex(resp.version || this.favoriteVersion, cachedResp)
              return resp
            })
        }, (resp) => {
          log.debug('end deleteFavorite(), resp:%s', resp)
          return Promise.reject(resp) as any
        })
    }
}
