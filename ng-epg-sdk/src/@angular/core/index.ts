import { Subject } from 'rxjs'

export function Injectable () {
  return (obj: any): any => { }
}

export class NgZone {

  get onUnstable (): Subject<any> {
      return this.onStable
    }

  get onStable (): Subject<any> {
      const subject = new Subject()
      setTimeout(() => {
          subject.next({})
          subject.complete()
        })
      return subject
    }

  run (fn: () => any): any { return fn() }

  runGuarded (fn: () => any): any { return fn() }

  runOutsideAngular (fn: () => any): any { return fn() }
}
