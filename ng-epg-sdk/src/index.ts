import { KeydownDebounceHook } from './hooks/keydown/keydown-debounce.hook'
export * from './vsp/api'
export * from './utils'
export * from './core'
export * from './services'
export * from './hooks'

import {
    HookService, SQMProbeService, LockService, KeydownService, LiveTvPlaybillService, LiveTvStoreService,
    FavoriteService, CustomizeConfigService, SessionService, HeartbeatService, ChannelService, XmppConnectApp,
    PlaybillAutoFillService, MSAErrorService, ReminderService, QueryCustomizeConfigService, PlaybillService,
    PlaybillCacheService
} from './services'
import { AbortAjaxHook, LoginOccationHook, CheckResponseHook, AjaxCacheHook, ActiveAjaxHook } from './hooks'

export const SDK_PROVIDERS = [
  AjaxCacheHook,
  HookService,
  LockService,
  FavoriteService,
  CustomizeConfigService,
  SessionService,
  HeartbeatService,
  ChannelService,
  PlaybillAutoFillService,
  PlaybillCacheService,
  PlaybillService,
  AbortAjaxHook,
  LoginOccationHook,
  CheckResponseHook,
  KeydownDebounceHook,
  ActiveAjaxHook,
  SQMProbeService,
  KeydownService,
  LiveTvPlaybillService,
  LiveTvStoreService,
  MSAErrorService,
  XmppConnectApp,
  ReminderService,
  QueryCustomizeConfigService
]
