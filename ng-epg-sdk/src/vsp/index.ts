export * from './api'

import { Result, API_NAMES } from './api'

export interface Response {
  result: Result
}

export interface APIHandler {
  name: API_NAMES
  getUrl?: (url: string, req: any, options: any) => string
  then: Array<(resp: any, req: any) => any>
}

export const CustomizeConfigQueryType = {
    /**
     * Query the configuration parameters of the terminal
     */
  TERMINAL_PARAMETER: '0',

    /**
     * Query the configuration parameters of the template
     */
  TEMPLATE_PARAMETER: '1',

    /**
     * Query VSP EPG parameters
     */
  VSP_EPG_PARAMETER: '2',

    /**
     * Query third-party system integration parameters
     */
  THIRD_PARTY_PARAMETER: '3',

    /**
     * Query content level system
     */
  RATING_SYSTEM: '4',

    /**
     * Query the list of available languages
     */
  LANGUAGE_LIST: '5',

    /**
     * Query the ISO code list
     */
  ISO_CODE_LIST: '6',

    /**
     * Query currency type letter encoding
     */
  CURRENCY_CODE_LIST: '7'
}
