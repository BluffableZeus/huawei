import { Config } from '../../core/config'
import { ajax, session } from '../../utils'
import { environment } from '../../../../src/environments/environment'

/**
 * Add CSRF Header
 */
const appendCSRFHeader = (options) => {
  const csrfToken = localStorage.getItem('Authenticate.csrfToken')

  if (csrfToken) {
      options.headers = options.headers || {}
      options.headers.X_CSRFToken = csrfToken
    }
}

/**
 * Send the VSP's Ajax request
 *
 * @export
 * @template T
 * @param {string} url
 * @param {*} [req={}]
 * @param {*} [options={}]
 * @returns {Promise<T>}
 */
export function sendRequest<T> (url: string, req: any = {}, options: { url?: string } = {}): Promise<T> {

  url = options.url || url

  if (url.indexOf('http') >= 0) {
      return ajax(url, req, options)
    } else {
      appendCSRFHeader(options)

      let prefix: string
      if (Config.get('enableHttps', true)) {
          prefix = Config.vspHttpsUrl
        } else {
          prefix = Config.vspUrl
        }

        // Dirty hack
      prefix = environment.TV_PROXY_HTTPS

      return ajax((prefix || '') + url, req, options)
    }
}
