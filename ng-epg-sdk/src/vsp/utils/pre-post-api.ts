import { APIHandler } from './../index'
import {
    LoginRouteHandler,
    LoginHandler,
    AuthenticateHandler,
    QueryProfileHandler,
    GetMasterSTBIDHandler,
    OnLineHeartbeatHandler,
    QueryCustomizeConfigHandler,
    QueryAllChannelDynamicPropertiesHandler,
    QueryAllChannelHandler,
    QueryLauncherHandler,
    QueryVodSubjectAndVodListHandler,
    PlayChannelHandler,
    PlayVODHandler,
    PlayPVRHandler,
    QueryPlaybillListHandler,
    QueryPlaybillContextHandler
} from './api-handler'

const handlerList: Array<APIHandler> = [
  LoginRouteHandler,
  LoginHandler,
  AuthenticateHandler,
  QueryProfileHandler,
  GetMasterSTBIDHandler,
  OnLineHeartbeatHandler,
  QueryCustomizeConfigHandler,
  QueryAllChannelDynamicPropertiesHandler,
  QueryAllChannelHandler,
  QueryLauncherHandler,
  QueryVodSubjectAndVodListHandler,
  PlayChannelHandler,
  PlayVODHandler,
  PlayPVRHandler,
  QueryPlaybillListHandler,
  QueryPlaybillContextHandler
]

export const HANDLER_LIST = handlerList
