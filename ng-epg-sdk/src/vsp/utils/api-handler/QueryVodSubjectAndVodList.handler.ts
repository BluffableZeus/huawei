import { APIHandler } from '../../index'
import { QueryVodSubjectAndVodListResponse, QueryVodSubjectAndVodListRequest } from './../../index'

import { MSAError as MSA_ERROR } from '../../../core/const'
import { SQMErrorType, sqmService } from './../../../sdk/service/sqm.service'
import { ErrorUtils, Logger } from './../../../utils'

const log = new Logger('msa-checker.QueryVodSubjectAndVodList')

export const QueryVodSubjectAndVodListHandler: APIHandler = {
  name: 'QueryVodSubjectAndVodList',
  then: [(resp: QueryVodSubjectAndVodListResponse, req: QueryVodSubjectAndVodListRequest) => {
      if (!resp.VODTotal || resp.VODTotal === '0' || !resp.subjectTotal || resp.subjectTotal === '0') {
          let err = ErrorUtils.getMSAError(MSA_ERROR.VOD_LIST_EMPTY)
          log.error('[MSA_ERROR] [category id: %s], err: %s ', req.subjectID, err.toString())

          sqmService.reportMSAError({
              errorType: SQMErrorType.normal,
              errorCode: err.externalCode,
              msaCode: err.internalCode,
              errorMessage: err.message
            })
        }
      return resp
    }]
}
