import { APIHandler } from '../../index'
import { LoginRouteResponse } from './../../index'

import { MSAError as MSA_ERROR } from '../../../core/const'

import { Config } from '../../../core/config'
import { ErrorUtils, InvalidChecker, Logger } from './../../../utils'
import { SQMErrorType, sqmService } from './../../../sdk/service/sqm.service'

const log = new Logger('msa-checker.LoginRoute')

const reportMSAError = function (code) {
  const e = ErrorUtils.getMSAError(code)
  log.error('[MSA_ERROR] %s', e.toString())

  sqmService.reportMSAError({
      errorType: SQMErrorType.login,
      errorCode: e.externalCode,
      msaCode: e.internalCode,
      errorMessage: e.message
    })
}

export const LoginRouteHandler: APIHandler = {
  name: 'LoginRoute',
  getUrl: (_url: string, req: any, options: any) => {
      const url = (req.edsUrl || options.edsUrl) + _url
      delete req.edsUrl
      return url
    },
  then: [(resp: LoginRouteResponse) => {
      if (InvalidChecker.isURLInvalid(resp.vspURL)) {
          reportMSAError(MSA_ERROR.LOGIN_ROUTER_VSPURL_INVALID)
        }

      if (InvalidChecker.isURLInvalid(resp.vspHttpsURL)) {
          reportMSAError(MSA_ERROR.LOGIN_ROUTER_VSPHTTPSURL_INVALID)
        }

      Config.vspUrl = resp.vspURL
      Config.vspHttpsUrl = resp.vspHttpsURL

      Config.set('vspUrl', resp.vspURL)
      Config.set('vspHttpsUrl', resp.vspHttpsURL)

      return resp
    }]
}
