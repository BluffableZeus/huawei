import { APIHandler } from '../../index'
import { QueryLauncherResponse } from './../../index'

import { MSAError as MSA_ERROR } from '../../../core/const'
import { SQMErrorType, sqmService } from './../../../sdk/service/sqm.service'
import { ErrorUtils, Logger } from './../../../utils'
import * as _ from 'underscore'

const log = new Logger('msa-checker.QueryLauncher')

export const QueryLauncherHandler: APIHandler = {
  name: 'QueryLauncher',
  then: [(resp: QueryLauncherResponse) => {
        // LauncherLink of QueryLauncher is empty.
      if (_.isEmpty(resp.launcherLink)) {
          const err = ErrorUtils.getMSAError(MSA_ERROR.QUERY_LAUNCHER_LINK_INVALID)
          log.error('[MSA_ERROR] %s', err.toString())

          sqmService.reportMSAError({
              errorType: SQMErrorType.normal,
              errorCode: err.externalCode,
              msaCode: err.internalCode,
              errorMessage: err.message
            })
        }

      return resp
    }]
}
