import { APIHandler } from '../../index'
import { PlayChannelResponse } from './../../index'

import { MSAError as MSA_ERROR } from '../../../core/const'
import { SQMErrorType, sqmService } from './../../../sdk/service/sqm.service'
import { ErrorUtils, Logger } from './../../../utils'

const log = new Logger('msa-checker.PlayChannel')

export const PlayChannelHandler: APIHandler = {
  name: 'PlayChannel',
  then: [null, (resp: PlayChannelResponse) => {
        // No available production package..
      if (resp.result.retCode === '146021000') {
          const err = ErrorUtils.getMSAError(MSA_ERROR.PLAY_NO_AVAILABLE_PRODUCTION_PACKAGE)
          log.error('[MSA_ERROR] %s', err.toString())
          sqmService.reportMSAError({
              errorType: SQMErrorType.normal,
              errorCode: err.externalCode,
              msaCode: err.internalCode,
              errorMessage: err.message
            })
        }
      return Promise.reject(resp)
    }]
}
