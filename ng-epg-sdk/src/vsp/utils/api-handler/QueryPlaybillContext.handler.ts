import { APIHandler } from '../../index'
import { QueryPlaybillContextResponse } from './../../index'

import { Logger } from './../../../utils'

const log = new Logger('msa-checker.QueryPlaybillContext')

export const QueryPlaybillContextHandler: APIHandler = {
  name: 'QueryPlaybillContext',
  getUrl: (_url: string, req: any, options: any) => {
      let url = _url
      const scene = options && options.scene
      if (scene) {
          log.debug('[QueryPlaybillContextHandler], scene is tvguide')
          url = url + '?scene=' + scene
        }

      return url
    },
  then: [(resp: QueryPlaybillContextResponse) => {
      return resp
    }]
}
