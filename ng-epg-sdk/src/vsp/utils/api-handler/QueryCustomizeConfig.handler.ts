import { APIHandler } from '../../index'
import { QueryCustomizeConfigResponse, QueryCustomizeConfigRequest, NamedParameter } from './../../index'

import { MSAError as MSA_ERROR } from '../../../core/const'
import { SQMErrorType, sqmService } from './../../../sdk/service/sqm.service'
import { ErrorUtils, InvalidChecker, Logger } from './../../../utils'
import * as _ from 'underscore'

const log = new Logger('msa-checker.QueryCustomizeConfig')

const configErrorCheckMap = {
  'special_cluster_id': {
      code: MSA_ERROR.CONFIG_SPECIAL_CLUSTER_ID_INVALID,
      checker: (value) => {
          return _.isEmpty(value)
        }
    },
  'vod_subject_id': {
      code: MSA_ERROR.CONFIG_VOD_SUBJECT_ID_INVALID,
      checker: (value) => {
          return _.isEmpty(value)
        }
    },
  'vod_4k_subject_id': {
      code: MSA_ERROR.CONFIG_VOD_4K_SUBJECT_ID_INVALID,
      checker: (value) => {
          return _.isEmpty(value)
        }
    },
  'livetv_4k_subject_id': {
      code: MSA_ERROR.CONFIG_LIVETV_4K_SUBJECT_ID_INVALID,
      checker: (value) => {
          return _.isEmpty(value)
        }
    },
  'currency_symbol': {
      code: MSA_ERROR.CONFIG_CURRENCY_SYMBOL_INVALID,
      checker: (value) => {
          return _.isEmpty(value)
        }
    },
  'credit_limit_list': {
      code: MSA_ERROR.CONFIG_CREDIT_LIMIT_LIST_INVALID,
      checker: (value) => {
          let limits: Array<string> = value.split(',')

          return _.isEmpty(value) || _.find(limits, (lim) => {
              return _.isNaN(parseInt(lim, 10))
            })
        }
    },
  'ott_channel_name_space': {
      code: MSA_ERROR.CONFIG_OTT_CHANNEL_NAME_SPACE_INVALID,
      checker: (value) => {
          return _.isEmpty(value)
        }
    },
  'default_reccfg_single_or_series': {
      code: MSA_ERROR.CONFIG_DEFAULT_RECCFG_SINGLE_OR_SERIES_INVALID,
      checker: (value) => {
          return !_.contains(['Single', 'Series'], value)
        }
    },
  'default_reccfg_definition': {
      code: MSA_ERROR.CONFIG_DEFAULT_RECCFG_DEFINITION_INVALID,
      checker: (value) => {
          return !_.contains(['0', '1', '2'], value)
        }
    },
  'default_reccfg_pvr_type': {
      code: MSA_ERROR.CONFIG_DEFAULT_RECCFG_PVR_TYPE_INVALID,
      checker: (value) => {
          return !_.contains(['CPVR', 'NPVR', 'MIX'], value)
        }
    },
  'forgot_password_url': {
      code: MSA_ERROR.CONFIG_FORGOT_PASSWORD_URL_INVALID,
      checker: (value) => {
          return InvalidChecker.isURLInvalid(value)
        }
    },
  'sqm_url': {
      code: MSA_ERROR.CONFIG_SQM_URL_INVALID,
      checker: (value) => {
          return InvalidChecker.isURLInvalid(value)
        }
    },
  'sqm_username': {
      code: MSA_ERROR.CONFIG_SQM_USERNAME_INVALID,
      checker: (value) => {
          return _.isEmpty(value)
        }
    },
  'sqm_key': {
      code: MSA_ERROR.CONFIG_SQM_KEY_INVALID,
      checker: (value) => {
          return _.isEmpty(value)
        }
    }
}

const reportMSAError = function (code) {
  const err = ErrorUtils.getMSAError(code)
  log.error('[MSA_ERROR] %s', err.toString())

  sqmService.reportMSAError({
      errorType: SQMErrorType.normal,
      errorCode: err.externalCode,
      msaCode: err.internalCode,
      errorMessage: err.message
    })
}

export const QueryCustomizeConfigHandler: APIHandler = {
  name: 'QueryCustomizeConfig',
  then: [(resp: QueryCustomizeConfigResponse, req: QueryCustomizeConfigRequest) => {
      log.debug('[VSP_MUST_PARAMETER] /VSP/V3/QueryCustomizeConfig req: %s, resp: %s', req, resp)
        // QueryCustomizeConfig response ratings is empty.
      if (req.queryType === '4' && !resp.ratings) {
          reportMSAError(MSA_ERROR.CONFIG_RATING_INVALID)
        }

      if (req.queryType === '2') {
          const configs = resp.configurations && resp.configurations[0]

          if (!configs || !configs.IPTVIMPIP || !configs.IPTVIMPPort || !configs.IMDomain) {
              reportMSAError(MSA_ERROR.CONFIG_TVMS_INVAID)
            }
        }

      if (req.queryType === '0') {
          if (_.isEmpty(resp.configurations)) {
              let keys: Array<string> = req.key.split(',')
              _.each(keys, (key) => {
                  let errorChecker = configErrorCheckMap[key]

                  reportMSAError(errorChecker.code)
                })
            } else {
              const configuration = resp.configurations && resp.configurations[0]

              let configs

              if (req.key) {
                  let keys: Array<string> = req.key.split(',')
                  configs = _.filter(configuration.values, (param) => {
                      return _.contains(keys, param.key)
                    })
                } else {
                  configs = configuration.values
                }

              _.each(configs, (param: NamedParameter) => {
                  const value = _.first(param.values) as string

                  let errorChecker = configErrorCheckMap[param.key]

                  if (errorChecker && errorChecker.checker(value)) {
                      reportMSAError(errorChecker.code)
                    }
                })
            }
        }

      return resp
    }]
}
