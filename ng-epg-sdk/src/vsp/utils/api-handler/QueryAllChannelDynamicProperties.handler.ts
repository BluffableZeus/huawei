import { APIHandler } from '../../index'
import { QueryAllChannelDynamicPropertiesResponse } from './../../index'

import { MSAError as MSA_ERROR } from '../../../core/const'
import { SQMErrorType, sqmService } from './../../../sdk/service/sqm.service'
import { ErrorUtils, Logger } from './../../../utils'
import * as _ from 'underscore'

const log = new Logger('msa-checker.QueryAllChannelDynamicProperties')

export const QueryAllChannelDynamicPropertiesHandler: APIHandler = {
  name: 'QueryAllChannelDynamicProperties',
  then: [(resp: QueryAllChannelDynamicPropertiesResponse) => {
        // channelDynamaicProp is empty
      if (_.isEmpty(resp.channelDynamaicProp)) {
          const err = ErrorUtils.getMSAError(MSA_ERROR.CHANNEL_DYNAMAIC_PROP_INVALID)
          log.error('[MSA_ERROR] %s', err.toString())

          sqmService.reportMSAError({
              errorType: SQMErrorType.normal,
              errorCode: err.externalCode,
              msaCode: err.internalCode,
              errorMessage: err.message
            })
        }

      return resp
    }]
}
