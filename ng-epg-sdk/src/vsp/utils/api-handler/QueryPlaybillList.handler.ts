import { APIHandler } from '../../index'
import { QueryPlaybillListResponse } from './../../index'

import { Logger } from './../../../utils'

const log = new Logger('msa-checker.QueryPlaybillList')

export const QueryPlaybillListHandler: APIHandler = {
  name: 'QueryPlaybillList',
  getUrl: (_url: string, req: any, options: any) => {
      let url = _url
      const scene = options && options.scene
      if (scene) {
          log.debug('[QueryPlaybillListHandler], scene is tvguide')
          url = url + '?scene=' + scene
        }

      return url
    },
  then: [(resp: QueryPlaybillListResponse) => {
      return resp
    }]
}
