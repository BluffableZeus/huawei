import { APIHandler } from '../../index'
import { PlayVODResponse } from './../../index'

import { MSAError as MSA_ERROR } from '../../../core/const'
import { SQMErrorType, sqmService } from './../../../sdk/service/sqm.service'
import { ErrorUtils, Logger } from './../../../utils'
import * as _ from 'underscore'

const log = new Logger('msa-checker.PlayVOD')

const reportMSAError = function (code) {
  const err = ErrorUtils.getMSAError(code)
  log.error('[MSA_ERROR] %s', err.toString())

  sqmService.reportMSAError({
      errorType: SQMErrorType.normal,
      errorCode: err.externalCode,
      msaCode: err.internalCode,
      errorMessage: err.message
    })
}

export const PlayVODHandler: APIHandler = {
  name: 'PlayVOD',
  then: [(resp: PlayVODResponse) => {
      if (!resp.playURL) {
          reportMSAError(MSA_ERROR.PLAY_NO_AVALIABLE_PLAYURL)
        }
      if (resp.bookmark && _.isNaN(parseInt(resp.bookmark, 10))) {
          reportMSAError(MSA_ERROR.PLAY_BOOKMARK_INVALID)
        }
      return resp
    }, (resp: PlayVODResponse) => {
        // No available production package..
      if (resp.result.retCode === '146021000') {
          reportMSAError(MSA_ERROR.PLAY_NO_AVAILABLE_PRODUCTION_PACKAGE)
        }
      return Promise.reject(resp)
    }]
}
