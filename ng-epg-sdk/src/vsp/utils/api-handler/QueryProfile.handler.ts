import { APIHandler } from '../../index'
import { QueryProfileResponse } from './../../index'

import { MSAError as MSA_ERROR } from '../../../core/const'
import { SQMErrorType, sqmService } from './../../../sdk/service/sqm.service'
import { ErrorUtils, Logger } from './../../../utils'
import * as _ from 'underscore'

const log = new Logger('msa-checker.QueryProfile')

export const QueryProfileHandler: APIHandler = {
  name: 'QueryProfile',
  then: [(resp: QueryProfileResponse) => {
        // Profiles is empty.(QueryProfile)
      if (_.isEmpty(resp.profiles)) {
          const err = ErrorUtils.getMSAError(MSA_ERROR.QUERY_PROFILE_PROFILES_INVALID)
          log.error('[MSA_ERROR] %s', err.toString())

          sqmService.reportMSAError({
              errorType: SQMErrorType.normal,
              errorCode: err.externalCode,
              msaCode: err.internalCode,
              errorMessage: err.message
            })
        }

      return resp
    }]
}
