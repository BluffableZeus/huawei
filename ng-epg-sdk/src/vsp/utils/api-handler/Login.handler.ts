import { APIHandler } from '../../index'
import { LoginResponse } from './../../index'

import { MSAError as MSA_ERROR } from '../../../core/const'

import { Config } from '../../../core/config'
import { ErrorUtils, InvalidChecker, Logger } from './../../../utils'
import { SQMErrorType, sqmService } from './../../../sdk/service/sqm.service'

import * as _ from 'underscore'

const log = new Logger('msa-checker.Login')

const reportMSAError = function (code) {
  const e = ErrorUtils.getMSAError(code)
  log.error('[MSA_ERROR] %s', e.toString())

  sqmService.reportMSAError({
      errorType: SQMErrorType.login,
      errorCode: e.externalCode,
      msaCode: e.internalCode,
      errorMessage: e.message
    })
}

export const LoginHandler: APIHandler = {
  name: 'Login',
  then: [(resp: LoginResponse) => {
        // upgradeDomain and upgradeDomainBackups is empty(Login)
      if (_.isEmpty(resp.upgradeDomain) && _.isEmpty(resp.upgradeDomainBackup)) {
          reportMSAError(MSA_ERROR.LOGIN_UPGRADE_INVALID)
        }

        // NTPDomain and NTPDomainBackups is empty(Login).
      if (_.isEmpty(resp.NTPDomain) && _.isEmpty(resp.NTPDomainBackup)) {
          reportMSAError(MSA_ERROR.LOGIN_NTP_INVALID)
        }

        // TerminalParm is incomplete(Login)
      if (_.isEmpty(resp.terminalParm)) {
          reportMSAError(MSA_ERROR.LOGIN_TERMINALPARAM_INVALID)
        }

        // Parameters SQMURL is empty(Login)
      if (resp.parameters) {
            // Parameters SQMURL is empty(Login)
          if (InvalidChecker.isURLInvalid(resp.parameters.SQMURL)) {
              reportMSAError(MSA_ERROR.LOGIN_SQM_INVALID)
            }
            // MQMCURL is empty(Login).
          if (InvalidChecker.isURLInvalid(resp.parameters.MQMCURL)) {
              reportMSAError(MSA_ERROR.LOGIN_MQMCURL_INVALID)
            }
        }

        // vspURL is empty
      if (InvalidChecker.isURLInvalid(resp.vspURL)) {
          reportMSAError(MSA_ERROR.LOGIN_ROUTER_VSPURL_INVALID)
        }

        // vspHttpsUR is empty is empty
      if (InvalidChecker.isURLInvalid(resp.vspHttpsURL)) {
          reportMSAError(MSA_ERROR.LOGIN_ROUTER_VSPHTTPSURL_INVALID)
        }
      Config.vspUrl = resp.vspURL
      Config.vspHttpsUrl = resp.vspHttpsURL
      Config.encryptToken = (resp as any).encryptToken
      return resp
    }]
}
