import { APIHandler } from '../../index'
import { OnLineHeartbeatResponse } from './../../index'

import { MSAError as MSA_ERROR } from '../../../core/const'
import { SQMErrorType, sqmService } from './../../../sdk/service/sqm.service'
import { ErrorUtils, Logger } from './../../../utils'
import * as _ from 'underscore'

const log = new Logger('msa-checker.OnLineHeartbeat')

export const OnLineHeartbeatHandler: APIHandler = {
  name: 'OnLineHeartbeat',
  then: [(resp: OnLineHeartbeatResponse) => {
        // personalDataVersions.channel  is empty.
      if (_.isEmpty(resp.personalDataVersions) || _.isEmpty(resp.personalDataVersions.channel)) {
          const err = ErrorUtils.getMSAError(MSA_ERROR.PERSONAL_VERSIONS_INVALID)
          log.error('[MSA_ERROR] %s', err.toString())
          sqmService.reportMSAError({
              errorType: SQMErrorType.normal,
              errorCode: err.externalCode,
              msaCode: err.internalCode,
              errorMessage: err.message
            })
        }
      return resp
    }]
}
