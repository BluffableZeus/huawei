import { APIHandler } from '../../index'
import { AuthenticateResponse } from './../../index'

import { ErrorUtils, Logger, session } from './../../../utils'
import { MSAError as MSA_ERROR } from '../../../core/const'
import { SQMErrorType, sqmService } from './../../../sdk/service/sqm.service'
import { Config } from '../../../core/config'

import * as _ from 'underscore'

const log = new Logger('msa-checker.Authenticate')

const reportMSAError = function (code) {
  const e = ErrorUtils.getMSAError(code)
  log.error('[MSA_ERROR] %s', e.toString())

  sqmService.reportMSAError({
      errorType: SQMErrorType.login,
      errorCode: e.externalCode,
      msaCode: e.internalCode,
      errorMessage: e.message
    })
}

export const AuthenticateHandler: APIHandler = {
  name: 'Authenticate',
    // tslint:disable:cyclomatic-complexity
  then: [(resp: AuthenticateResponse) => {
        // userToken is empty(Authenticate)
      if (_.isEmpty(resp.userToken)) {
          reportMSAError(MSA_ERROR.AUTHENTICATE_TOKEN_INVALID)
        }

        // jSessionID is empty.
      if (_.isEmpty(resp.jSessionID)) {
          reportMSAError(MSA_ERROR.AUTHENTICATE_SESSIONID_INVALID)
        }

        // VUID is empty.
      if (_.isEmpty(resp.VUID)) {
          reportMSAError(MSA_ERROR.AUTHENTICATE_VUID_INVALID)
        }

        // profilID is empty or profileID is not in profiles.
      if (_.isEmpty(resp.profileID) || _.isEmpty(resp.profiles) || !_.find(resp.profiles, (profile) => {
          return profile.ID === resp.profileID
        })) {
          reportMSAError(MSA_ERROR.AUTHENTICATE_PROFILEID_INVALID)
        }

        // deviceID is empty.
      if (_.isEmpty(resp.deviceID)) {
          reportMSAError(MSA_ERROR.AUTHENTICATE_DEVICE_INVALID)
        }

      if (_.isEmpty(resp.CA)) {
            // ca info is empty.
          reportMSAError(MSA_ERROR.AUTHENTICATE_CA_INVALID)
        } else if (_.isEmpty(resp.CA.verimatrix)) {
            // CA.verimatrix is empty.
          reportMSAError(MSA_ERROR.AUTHENTICATE_CA_VM_INVALID)
        } else if (_.isEmpty(resp.CA.playReady)) {
            // CA.playReady is empty.
          reportMSAError(MSA_ERROR.AUTHENTICATE_CA_PLAYREADY_INVALID)
        }

        // platform is in loginOccasion status
      if ((+resp.loginOccasion) === 1) {
          Config.loginOccasion = true
          reportMSAError(MSA_ERROR.AUTHENTICATE_DISASTER_RECOVERY_STATE)
        }

        // loginOccasion params is missing
      if (!resp.areaCode || !resp.subnetID || !resp.bossID || !resp.templateName) {
          reportMSAError(MSA_ERROR.AUTHENTICATE_DISASTER_RECOVERY_INVALID)
        }

      Config.deviceID = resp.deviceID

      if (resp.profiles) {
          const hasEmptyLogoUrl = _.find(resp.profiles, profile => {
              return !profile.logoURL
            })
          if (hasEmptyLogoUrl) {
                // profile logoURL is empty
              reportMSAError(MSA_ERROR.AUTHENTICATE_LOGOURL_INVALID)
            }
          const masterProfile = _.find(resp.profiles, profile => {
              return profile.profileType === '0'
            })
            // has no master profile
          if (!masterProfile) {
              reportMSAError(MSA_ERROR.AUTHENTICATE_MISSING_MASTER_PROFILE)
            }
        }

      if (resp.csrfToken) {
          localStorage.setItem('Authenticate.csrfToken', resp.csrfToken)
        }
      return resp
    }]
}
