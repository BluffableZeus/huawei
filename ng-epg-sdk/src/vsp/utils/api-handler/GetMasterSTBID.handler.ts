import { APIHandler } from '../../index'
import { GetMasterSTBIDResponse } from './../../index'

import { MSAError as MSA_ERROR } from '../../../core/const'

import { ErrorUtils, Logger } from './../../../utils'
import { SQMErrorType, sqmService } from './../../../sdk/service/sqm.service'

const log = new Logger('msa-checker.GetMasterSTBID')

export const GetMasterSTBIDHandler: APIHandler = {
  name: 'GetMasterSTBID',
  then: [(resp: GetMasterSTBIDResponse) => {
        // masterSTBID is empty (GetMasterSTBID).
      if (!resp.masterSTBID) {
          const err = ErrorUtils.getMSAError(MSA_ERROR.MASTER_ID_INVALID)
          log.error('[MSA_ERROR] %s', err.toString())

          sqmService.reportMSAError({
              errorType: SQMErrorType.normal,
              errorCode: err.externalCode,
              msaCode: err.internalCode,
              errorMessage: err.message
            })
        }

      return resp
    }]
}
