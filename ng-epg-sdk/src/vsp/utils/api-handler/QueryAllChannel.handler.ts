import { APIHandler } from '../../index'
import { QueryAllChannelResponse } from './../../index'

import { MSAError as MSA_ERROR } from '../../../core/const'
import { SQMErrorType, sqmService } from './../../../sdk/service/sqm.service'
import { ErrorUtils, Logger } from './../../../utils'
import * as _ from 'underscore'

const log = new Logger('msa-checker.QueryAllChannel')

export const QueryAllChannelHandler: APIHandler = {
  name: 'QueryAllChannel',
  getUrl: (_url: string, req: any, options: any) => {
      let url = _url
      const userFilter = req.userFilter || options.userFilter
      if (userFilter) {
          url = url + '?userFilter=' + userFilter
          delete req.userFilter
        }

      return url
    },
  then: [(resp: QueryAllChannelResponse) => {
        // channel list is empty
      if (_.isEmpty(resp.channelDetails)) {
          const err = ErrorUtils.getMSAError(MSA_ERROR.QUERY_ALL_CHANNEL_LIST_EMPTY)
          log.error('[MSA_ERROR] %s', err.toString())

          sqmService.reportMSAError({
              errorType: SQMErrorType.normal,
              errorCode: err.externalCode,
              msaCode: err.internalCode,
              errorMessage: err.message
            })
        }

      return resp
    }]
}
