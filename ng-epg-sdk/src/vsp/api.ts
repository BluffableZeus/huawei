/**
 * this document is auto generated from docx
 */

// tslint:disable
import { Config } from '../core/config';
import { sendRequest } from './utils/sendRequest';
import { HANDLER_LIST } from './utils/pre-post-api';
import * as _ from 'underscore';

export type API_NAMES = 'LoginRoute' | 'Login' | 'Authenticate' | 'SwitchProfile' | 'OnLineHeartbeat' | 'Logout' | 'UpdateTimeZone' | 'SendSMS' | 'CreateSubscriber' | 'AddProfile' | 'ModifyProfile' | 'DeleteProfile' | 'QueryProfile' | 'GetBindCode' | 'BindProfile' | 'UnBindProfile' | 'BindSubscriber' | 'UnBindSubscriber' | 'QueryBindedSubscriber' | 'QueryDeviceList' | 'ReplaceDevice' | 'ModifyDeviceInfo' | 'CheckPassword' | 'ModifyPassword' | 'ResetPassword' | 'QrCodeAuthenticate' | 'QueryQrCodeAuthenticateStatus' | 'ReportQrCodeAuthenticateStatus' | 'QuitQrCodeAuthenticate' | 'QueryQrCodeAuthenticatedSubscriber' | 'SetQrCodeAuthenticatedSubscriber' | 'QueryProduct' | 'SubscribeProduct' | 'CancelSubscribe' | 'QueryMyContent' | 'QuerySubscription' | 'QueryChannelSubjectList' | 'QueryChannelListBySubject' | 'QueryAllChannel' | 'QueryAllChannelDynamicProperties' | 'QueryPlaybillList' | 'QueryPlaybillListByChannelContext' | 'QueryPlaybillContext' | 'QueryPlaybillContextByChannelContext' | 'GetPlaybillDetail' | 'QueryHotPlaybill' | 'PlayChannel' | 'DownloadCUTV' | 'ReportChannel' | 'PlayChannelHeartbeat' | 'QueryPlaybillVersion' | 'QueryMosaicChannelList' | 'BatchPlayChannel' | 'QueryVODSubjectList' | 'QuerySubjectDetail' | 'QueryVODListBySubject' | 'QuerySubjectVODBySubjectID' | 'QueryRecmVODList' | 'GetContentConfig' | 'GetVODDetail' | 'PlayVOD' | 'DownloadVOD' | 'ReportVOD' | 'PlayVODHeartbeat' | 'SetGlobalFilterCond' | 'SearchHotKey' | 'SearchContent' | 'SuggestKeyword' | 'QueryRecmContent' | 'QueryHotSearchContentsList' | 'AddFavoCatalog' | 'DeleteFavoCatalog' | 'UpdateFavoCatalog' | 'QueryFavoCatalog' | 'CreateFavorite' | 'DeleteFavorite' | 'QueryFavorite' | 'SortFavorite' | 'CreateLock' | 'DeleteLock' | 'QueryLock' | 'CreateBookmark' | 'DeleteBookmark' | 'QueryBookmark' | 'CreateContentScore' | 'SetContentLike' | 'CreatePlaylist' | 'UpdatePlaylist' | 'DeletePlaylist' | 'QueryPlaylist' | 'AddPlaylistContent' | 'SortPlaylistContent' | 'DeletePlaylistContent' | 'QueryPlaylistContent' | 'CreateReminder' | 'DeleteReminder' | 'QueryReminder' | 'QueryLauncher' | 'BatchGetResStrategyData' | 'GetLatestResources' | 'jumpURLjsp' | 'GetTopics' | 'SubmitDeviceInfo' | 'PushMsgByTag' | 'DownloadNPVR' | 'ReportPVR' | 'PlayNPVRHeartbeat' | 'AddPVR' | 'UpdatePVR' | 'AddPeriodicPVR' | 'UpdatePeriodicPVR' | 'QueryPVR' | 'SearchPVR' | 'QueryPVRByID' | 'CancelPVRByID' | 'CancelPVRByPlaybillID' | 'DeletePVRByID' | 'DeletePVRByCondition' | 'DeleteCPVRByDiskID' | 'QueryPVRSpace' | 'QueryPVRSpaceByID' | 'SolvePVRConflict' | 'SetMasterSTBID' | 'GetMasterSTBID' | 'UpdateCPVRStatus' | 'PlayPVR' | 'ApplyStream' | 'ReleaseStream' | 'GetStreamRecord' | 'PublishComment' | 'MaintainComment' | 'QueryComments' | 'ReportComment' | 'LikeComment' | 'Barrage' | 'GetBarrageList' | 'RegisterUserInfo' | 'QueryCustomizeConfig' | 'QueryTemplate' | 'QueryLocation' | 'GetRelatedContent' | 'AuthenticateSDKLicense' | 'QueryVodHomeData' | 'VodHome' | 'QueryHomeData' | 'QueryOTTLiveTVHomeData' | 'QueryMyTVHomeData' | 'QueryMoreRecommend' | 'QueryEpgHomeRecmPlaybill' | 'QueryEpgHomeVod' | 'QueryEpgHomeSubjects' | 'QueryVodSubjectAndVodList' | 'QueryPCVodHome' | 'QueryAllHomeData' | 'QueryRecmContent';

const handlerByName = _.indexBy(HANDLER_LIST, 'name');

type PlayRequest = PlayChannelRequest | PlayVODRequest | PlayPVRRequest;

function preparePlayRequest(req: PlayRequest): PlayRequest {
    // if (! ('URLFormat' in req)) {
    //     req.URLFormat = String(Config.playUrlFormat);
    // }
    if (! ('isHTTPS' in req)) {
        req.isHTTPS = Config.playHttps;
    }

    return req;
}

const _sendRequest = (name: string, url: string, req, options: any = {}) => {
    const handler: any = handlerByName[name];
    if (handler && handler.getUrl) {
        url = handler.getUrl(url, req, options);
    }

    const then = handler ? handler.then : [];
    const promise = sendRequest(url, req, options);

    let resolve, reject;
    if (then[0]) {
        resolve = (resp) => {
            return then[0](resp, req);
        }
    }

    if (then[1]) {
        reject = (resp) => {
            return then[1](resp, req);
        }
    }

    return promise.then(resolve, reject);
}

/**
* 客户端接入VSP时无法直接访问用户网关(VSP)服务器，需要调用此接口请求调度到可以提供服务的用户网关(VSP)服务器。
*/
export function loginRoute(req: LoginRouteRequest, options: any = {}): Promise<LoginRouteResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('LoginRoute', '/EDS/V3/LoginRoute', req, options).then((resp: any) => {
        return safeLoginRouteResponse(resp);
    });
}

/**
* 客户端通过EDS调度后，向用户网关(VSP)服务器发送登录请求的接口。
*/
export function login(req: LoginRequest, options: any = {}): Promise<LoginResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('Login', '/VSP/V3/Login', req, options).then((resp: any) => {
        return safeLoginResponse(resp);
    });
}

/**
* 客户端调用登录接口后，调用接口请求用户认证。
*
* 用户登录系统时，就必须经过认证请求，认证请求使用HTTPS安全协议，使用密码认证。
*/
export function authenticate(req: AuthenticateRequest, options: any = {}): Promise<AuthenticateResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('Authenticate', '/VSP/V3/Authenticate', req, options).then((resp: any) => {
        return safeAuthenticateResponse(resp);
    });
}

/**
* Profile认证接口。Authenticate成功后，如果有多个profile或者设备登录没有默认Profile则需要调用SwitchProfile接口做profile认证。
*/
export function switchProfile(req: SwitchProfileRequest, options: any = {}): Promise<SwitchProfileResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('SwitchProfile', '/VSP/V3/SwitchProfile', req, options).then((resp: any) => {
        return safeSwitchProfileResponse(resp);
    });
}

/**
* 客户端调用此接口向用户网关(VSP)服务器发送心跳消息。
*
* 客户端需要周期性（周期见此接口响应消息中的nextCallInterval参数）调用该接口向用户网关(VSP)服务器发送心跳请求，以维持与服务器的HTTP会话。
*
* 客户端本地需要缓存各种数据，例如频道列表、收藏列表、书签列表等，客户端调用该接口后可根据数据版本号判断数据是否更新，如果有更新则调用相关接口更新本地缓存的数据，具体请参见本接口的返回参数说明。
*/
export function onLineHeartbeat(req: OnLineHeartbeatRequest, options: any = {}): Promise<OnLineHeartbeatResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('OnLineHeartbeat', '/VSP/V3/OnLineHeartbeat', req, options).then((resp: any) => {
        return safeOnLineHeartbeatResponse(resp);
    });
}

/**
* 用户退出客户端时（注销或关机），客户端可调用此接口通知用户网关(VSP) Server。
*
* 此接口对客户端是可选的，主要目的是尽早释放VSP服务器资源。
*/
export function logout(req: LogoutRequest, options: any = {}): Promise<LogoutResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('Logout', '/VSP/V3/Logout', req, options).then((resp: any) => {
        return safeLogoutResponse(resp);
    });
}

/**
* 用户退出客户端时（注销或关机），客户端可调用此接口通知用户网关(VSP) Server。
*
* 此接口对客户端是可选的，主要目的是尽早释放VSP服务器资源。
*/
export function updateTimeZone(req: UpdateTimeZoneRequest, options: any = {}): Promise<UpdateTimeZoneResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('UpdateTimeZone', '/VSP/V3/UpdateTimeZone', req, options).then((resp: any) => {
        return safeUpdateTimeZoneResponse(resp);
    });
}

/**
* 给UI提供发送验证码接口，接口发送的目标手机号码（loginName）、发送验证码类型、语言、子网ID等参数，返回验证码发送结果。
*
* 原子接口
*/
export function sendSMS(req: SendSMSRequest, options: any = {}): Promise<SendSMSResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('SendSMS', '/VSP/V3/SendSMS', req, options).then((resp: any) => {
        return safeSendSMSResponse(resp);
    });
}

/**
* 用户注册。
*
* 原子接口
*/
export function createSubscriber(req: CreateSubscriberRequest, options: any = {}): Promise<CreateSubscriberResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('CreateSubscriber', '/VSP/V3/CreateSubscriber', req, options).then((resp: any) => {
        return safeCreateSubscriberResponse(resp);
    });
}

/**
* 需要支持多Profile时，客户端可以调用此接口创建子Profile。
*
* 用户开户时，系统默认为其创建一个主Profile。用户可以通过主Profile直接登录系统。登录后用户选择创建子Profile时，客户端调用此接口。
*
* 通过该接口创建Profile时可以通过ratingID参数设置该Profile的父母控制级别。
*/
export function addProfile(req: AddProfileRequest, options: any = {}): Promise<AddProfileResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('AddProfile', '/VSP/V3/AddProfile', req, options).then((resp: any) => {
        return safeAddProfileResponse(resp);
    });
}

/**
* 客户端调用此接口修改Profile的信息。
*
* 用户使用主Profile登录系统时，客户端可以调用该接口修改自己的Profile信息和其他子Profile信息，例如名称和密码。但是子Profile登录时只能修改自己的Profile信息。
*
* 原子接口
*/
export function modifyProfile(req: ModifyProfileRequest, options: any = {}): Promise<ModifyProfileResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('ModifyProfile', '/VSP/V3/ModifyProfile', req, options).then((resp: any) => {
        return safeModifyProfileResponse(resp);
    });
}

/**
* 客户端调用此接口删除子Profile信息接口。
*
* 主Profile可以在客户端界面上删除某个子Profile，并将此Profile关联的收藏、书签等一起删除。当用户需要删除某个子Profile时，客户端调用该接口。子Profile只能删除自己。
*
* 原子接口
*/
export function deleteProfile(req: DeleteProfileRequest, options: any = {}): Promise<DeleteProfileResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('DeleteProfile', '/VSP/V3/DeleteProfile', req, options).then((resp: any) => {
        return safeDeleteProfileResponse(resp);
    });
}

/**
* 查询所有Profile的接口。
*
* 原子接口
*/
export function queryProfile(req: QueryProfileRequest, options: any = {}): Promise<QueryProfileResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('QueryProfile', '/VSP/V3/QueryProfile', req, options).then((resp: any) => {
        return safeQueryProfileResponse(resp);
    });
}

/**
* 机顶盒主profile生成二维码。
*
* 原子接口
*/
export function getBindCode(req: GetBindCodeRequest, options: any = {}): Promise<GetBindCodeResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('GetBindCode', '/VSP/V3/GetBindCode', req, options).then((resp: any) => {
        return safeGetBindCodeResponse(resp);
    });
}

/**
* 绑定账号。
*
* 原子接口
*/
export function bindProfile(req: BindProfileRequest, options: any = {}): Promise<BindProfileResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('BindProfile', '/VSP/V3/BindProfile', req, options).then((resp: any) => {
        return safeBindProfileResponse(resp);
    });
}

/**
* 解绑帐号。
*
* 原子接口
*/
export function unBindProfile(req: UnBindProfileRequest, options: any = {}): Promise<UnBindProfileResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('UnBindProfile', '/VSP/V3/UnBindProfile', req, options).then((resp: any) => {
        return safeUnBindProfileResponse(resp);
    });
}

/**
* 绑定帐号。
*/
export function bindSubscriber(req: BindSubscriberRequest, options: any = {}): Promise<BindSubscriberResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('BindSubscriber', '/VSP/V3/BindSubscriber', req, options).then((resp: any) => {
        return safeBindSubscriberResponse(resp);
    });
}

/**
* 解绑帐号
*/
export function unBindSubscriber(req: UnBindSubscriberRequest, options: any = {}): Promise<UnBindSubscriberResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('UnBindSubscriber', '/VSP/V3/UnBindSubscriber', req, options).then((resp: any) => {
        return safeUnBindSubscriberResponse(resp);
    });
}

/**
* 查询已绑定帐号。
*/
export function queryBindedSubscriber(req: QueryBindedSubscriberRequest, options: any = {}): Promise<QueryBindedSubscriberResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('QueryBindedSubscriber', '/VSP/V3/QueryBindedSubscriber', req, options).then((resp: any) => {
        return safeQueryBindedSubscriberResponse(resp);
    });
}

/**
* 用户登录认证失败时，如果失败原因是用户绑定设备数量已达上限，客户端可以通过此接口获取用户绑定的设备列表，供用户选择其中的某一个设备解绑。
*/
export function queryDeviceList(req: QueryDeviceListRequest, options: any = {}): Promise<QueryDeviceListResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('QueryDeviceList', '/VSP/V3/QueryDeviceList', req, options).then((resp: any) => {
        return safeQueryDeviceListResponse(resp);
    });
}

/**
* 替换用户绑定的某一设备。用户换一台设备登录时，如果登录失败的原因是绑定的设备数量已达上限，可通过此接口将一台已绑定的设备替换为用户正在登录的设备。
*/
export function replaceDevice(req: ReplaceDeviceRequest, options: any = {}): Promise<ReplaceDeviceResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('ReplaceDevice', '/VSP/V3/ReplaceDevice', req, options).then((resp: any) => {
        return safeReplaceDeviceResponse(resp);
    });
}

/**
* VSP平台为终端提供的更新设备能力接口。
*/
export function modifyDeviceInfo(req: ModifyDeviceInfoRequest, options: any = {}): Promise<ModifyDeviceInfoResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('ModifyDeviceInfo', '/VSP/V3/ModifyDeviceInfo', req, options).then((resp: any) => {
        return safeModifyDeviceInfoResponse(resp);
    });
}

/**
* 客户端通过此接口校验用户密码。常用的用户密码的类型如表4-1所示。
*
* &lt;a id&#x3D;&quot;_table52725521&quot;&gt;&lt;/a&gt;常用用户密码类型
*/
export function checkPassword(req: CheckPasswordRequest, options: any = {}): Promise<CheckPasswordResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('CheckPassword', '/VSP/V3/CheckPassword', req, options).then((resp: any) => {
        return safeCheckPasswordResponse(resp);
    });
}

/**
* 修改密码接口。客户端可以通过此接口修改子Profile密码、订购密码、订户密码、主Profile密码、父母控制密码等。
*/
export function modifyPassword(req: ModifyPasswordRequest, options: any = {}): Promise<ModifyPasswordResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('ModifyPassword', '/VSP/V3/ModifyPassword', req, options).then((resp: any) => {
        return safeModifyPasswordResponse(resp);
    });
}

/**
* 重置密码接口。客户端可以通过此接口重置Profile密码、订购密码、订户登录密码、父母控制密码。
*/
export function resetPassword(req: ResetPasswordRequest, options: any = {}): Promise<ResetPasswordResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('ResetPassword', '/VSP/V3/ResetPassword', req, options).then((resp: any) => {
        return safeResetPasswordResponse(resp);
    });
}

/**
* 生成二维码并发起二维码认证。
*/
export function qrCodeAuthenticate(req: QrCodeAuthenticateRequest, options: any = {}): Promise<QrCodeAuthenticateResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('QrCodeAuthenticate', '/VSP/V3/QrCodeAuthenticate', req, options).then((resp: any) => {
        return safeQrCodeAuthenticateResponse(resp);
    });
}

/**
* 查询二维码认证状态。
*/
export function queryQrCodeAuthenticateStatus(req: QueryQrCodeAuthenticateStatusRequest, options: any = {}): Promise<QueryQrCodeAuthenticateStatusResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('QueryQrCodeAuthenticateStatus', '/VSP/V3/QueryQrCodeAuthenticateStatus', req, options).then((resp: any) => {
        return safeQueryQrCodeAuthenticateStatusResponse(resp);
    });
}

/**
* &lt;sup&gt;上报二维码认证状态。&lt;/sup&gt;
*/
export function reportQrCodeAuthenticateStatus(req: ReportQrCodeAuthenticateStatusRequest, options: any = {}): Promise<ReportQrCodeAuthenticateStatusResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('ReportQrCodeAuthenticateStatus', '/VSP/V3/ReportQrCodeAuthenticateStatus', req, options).then((resp: any) => {
        return safeReportQrCodeAuthenticateStatusResponse(resp);
    });
}

/**
* 清除当次二维码认证功能中写入会话中的各类缓存。
*/
export function quitQrCodeAuthenticate(req: QuitQrCodeAuthenticateRequest, options: any = {}): Promise<QuitQrCodeAuthenticateResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('QuitQrCodeAuthenticate', '/VSP/V3/QuitQrCodeAuthenticate', req, options).then((resp: any) => {
        return safeQuitQrCodeAuthenticateResponse(resp);
    });
}

/**
* 返回会话中缓存的二维码认证用户。
*/
export function queryQrCodeAuthenticatedSubscriber(req: QueryQrCodeAuthenticatedSubscriberRequest, options: any = {}): Promise<QueryQrCodeAuthenticatedSubscriberResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('QueryQrCodeAuthenticatedSubscriber', '/VSP/V3/QueryQrCodeAuthenticatedSubscriber', req, options).then((resp: any) => {
        return safeQueryQrCodeAuthenticatedSubscriberResponse(resp);
    });
}

/**
* 更新会话中缓存的二维码认证用户。
*/
export function setQrCodeAuthenticatedSubscriber(req: SetQrCodeAuthenticatedSubscriberRequest, options: any = {}): Promise<SetQrCodeAuthenticatedSubscriberResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('SetQrCodeAuthenticatedSubscriber', '/VSP/V3/SetQrCodeAuthenticatedSubscriber', req, options).then((resp: any) => {
        return safeSetQrCodeAuthenticatedSubscriberResponse(resp);
    });
}

/**
* 查询产品。
*
* 原子接口
*/
export function queryProduct(req: QueryProductRequest, options: any = {}): Promise<QueryProductResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('QueryProduct', '/VSP/V3/QueryProduct', req, options).then((resp: any) => {
        return safeQueryProductResponse(resp);
    });
}

/**
* VSP提供产品订购的接口，支持订购按次和包周期产品，如果订购成功，将返回生效的订购关系，否则返回失败原因。
*
* VSP对订购请求进行处理，包括：
*
* 1.检查订户是否合法，包括订户状态正常、是否支持在线订购等。
*
* 2.检查产品是否合法，包括产品已生效、是否支持在线订购等。
*
* 3.如果订购的是按次产品，检查按次产品对应的内容是否存在。
*
* 4.如果订购的是节目单Catch-up TV的按次产品且局点要求订购Catch-up TV节目单必须先订购直播，VSP检查用户是否订购了直播业务。
*
* 5.检查待订购的产品是否存在依赖关系，如果存在，检查依赖的产品是否订购。
*
* 6.检查用户的消费额度是否足够。
*
* 原子接口
*/
export function subscribeProduct(req: SubscribeProductRequest, options: any = {}): Promise<SubscribeProductResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('SubscribeProduct', '/VSP/V3/SubscribeProduct', req, options).then((resp: any) => {
        return safeSubscribeProductResponse(resp);
    });
}

/**
* 客户端调用该接口取消包时长产品的自动续订和取消PPV订购。
*/
export function cancelSubscribe(req: CancelSubscribeRequest, options: any = {}): Promise<CancelSubscribeResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('CancelSubscribe', '/VSP/V3/CancelSubscribe', req, options).then((resp: any) => {
        return safeCancelSubscribeResponse(resp);
    });
}

/**
* 查询用户已经订购的内容接口。客户端需要展示订户已订购内容列表时可以调用此接口。如果订户订购的产品是独享的且订购此产品的设备非当前登录设备，平台不返回此订购记录。
*/
export function queryMyContent(req: QueryMyContentRequest, options: any = {}): Promise<QueryMyContentResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('QueryMyContent', '/VSP/V3/QueryMyContent', req, options).then((resp: any) => {
        return safeQueryMyContentResponse(resp);
    });
}

/**
* 查询用户的订购记录，支持各种过滤选项。
*/
export function querySubscription(req: QuerySubscriptionRequest, options: any = {}): Promise<QuerySubscriptionResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('QuerySubscription', '/VSP/V3/QuerySubscription', req, options).then((resp: any) => {
        return safeQuerySubscriptionResponse(resp);
    });
}

/**
* 获取频道栏目列表。
*/
export function queryChannelSubjectList(req: QueryChannelSubjectListRequest, options: any = {}): Promise<QueryChannelSubjectListResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('QueryChannelSubjectList', '/VSP/V3/QueryChannelSubjectList', req, options).then((resp: any) => {
        return safeQueryChannelSubjectListResponse(resp);
    });
}

/**
* 获取特定栏目下的频道列表，如果不传栏目ID则获取所有的频道列表，支持各种排序和过滤功能。
*
* 为了提升频道切换速度，客户端可在登录成功后，即查询加载所有频道到本地，频道切换时直接从本地读取数据。
*/
export function queryChannelListBySubject(req: QueryChannelListBySubjectRequest, options: any = {}): Promise<QueryChannelListBySubjectResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('QueryChannelListBySubject', '/VSP/V3/QueryChannelListBySubject', req, options).then((resp: any) => {
        return safeQueryChannelListBySubjectResponse(resp);
    });
}

/**
* 查询频道列表中与用户无关的元数据。
*
* 为了提升频道切换速度，用户在初次登录后，默认就会在本地缓存全量的频道信息，后续开机、以及运行过程中除非是频道元数据变更，否则客户端不会调用当前接口刷新频道列表静态属性。
*/
export function queryAllChannel(req: QueryAllChannelRequest, options: any = {}): Promise<QueryAllChannelResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('QueryAllChannel', '/VSP/V3/QueryAllChannel', req, options).then((resp: any) => {
        return safeQueryAllChannelResponse(resp);
    });
}

/**
* 查询频道列表中与用户相关的个性化数据。
*
* 为了提升频道切换速度，用户在初次登录后，默认就会在本地缓存全量的频道信息，后续开机、以及运行过程中除非是频道元数据变更，否则客户端只会调用当前接口刷新频道列表动态属性。
*/
export function queryAllChannelDynamicProperties(req: QueryAllChannelDynamicPropertiesRequest, options: any = {}): Promise<QueryAllChannelDynamicPropertiesResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('QueryAllChannelDynamicProperties', '/VSP/V3/QueryAllChannelDynamicProperties', req, options).then((resp: any) => {
        return safeQueryAllChannelDynamicPropertiesResponse(resp);
    });
}

/**
* 支持根据栏目查询频道、支持查询所有频道、支持查询单个频道，支持查询多个频道，并支持针对查询到的频道获取节目单。
*
* 支持指定节目单时间区间查询节目单，返回结果支持返回频道基本信息及节目单信息。
*
* 支持对频道和节目单查询分别指定步长。
*
* 接口应用场景：
*
* 场景一：频道列表页面，展示30个频道的当前节目单。
*
* 场景二：EPG TVGUIDE。取10个频道的5小时的节目单。
*
* 场景三：Phone端、进入频道详情，取当前频道24小时的节目单。
*/
export function queryPlaybillList(req: QueryPlaybillListRequest, options: any = {}): Promise<QueryPlaybillListResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('QueryPlaybillList', '/VSP/V3/QueryPlaybillList', req, options).then((resp: any) => {
        return safeQueryPlaybillListResponse(resp);
    });
}

/**
* 支持根据栏目查询频道、支持查询所有频道、支持查询单个频道，支持查询多个频道，并支持针对查询到的频道获取节目单。
*
* 支持指定节目单时间区间查询节目单，返回结果支持返回频道基本信息及节目单信息。
*
* 支持对频道和节目单查询分别指定步长。
*
* 接口应用场景：
*
* 场景一：频道列表页面，展示30个频道的当前节目单。
*
* 场景二：EPG TVGUIDE。取10个频道的5小时的节目单。
*
* 场景三：Phone端、进入频道详情，取当前频道24小时的节目单。
*/
export function queryPlaybillListByChannelContext(req: QueryPlaybillListByChannelContextRequest, options: any = {}): Promise<QueryPlaybillListByChannelContextResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('QueryPlaybillListByChannelContext', '/VSP/V3/QueryPlaybillListByChannelContext', req, options).then((resp: any) => {
        return safeQueryPlaybillListByChannelContextResponse(resp);
    });
}

/**
* 支持根据栏目查询频道、支持查询所有频道、支持查询单个频道，支持查询多个频道，并支持针对查询到的频道获取节目单。
*
* 支持指定节目单时间区间查询节目单，返回结果支持返回频道基本信息及节目单信息。
*
* 支持对频道查询指定步长。
*
* 接口应用场景：
*
* 场景一：频道列表页面，展示30个频道的当前节目单。
*
* 场景二：EPG TVGUIDE。取10个频道的5小时的节目单。
*
* 场景三：Phone端、进入频道详情，取当前频道24小时的节目单。
*/
export function queryPlaybillContext(req: QueryPlaybillContextRequest, options: any = {}): Promise<QueryPlaybillContextResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('QueryPlaybillContext', '/VSP/V3/QueryPlaybillContext', req, options).then((resp: any) => {
        return safeQueryPlaybillContextResponse(resp);
    });
}

/**
* 支持根据栏目查询频道、支持查询所有频道、支持查询单个频道，支持查询多个频道，并支持针对查询到的频道获取节目单。
*
* 支持指定节目单时间区间查询节目单，返回结果支持返回频道基本信息及节目单信息。
*
* 支持对频道查询指定步长。
*
* 接口应用场景：
*
* 场景一：频道列表页面，展示30个频道的当前节目单。
*
* 场景二：EPG TVGUIDE。取10个频道的5小时的节目单。
*
* 场景三、Phone端、进入频道详情，取当前频道24小时的节目单。
*/
export function queryPlaybillContextByChannelContext(req: QueryPlaybillContextByChannelContextRequest, options: any = {}): Promise<QueryPlaybillContextByChannelContextResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('QueryPlaybillContextByChannelContext', '/VSP/V3/QueryPlaybillContextByChannelContext', req, options).then((resp: any) => {
        return safeQueryPlaybillContextByChannelContextResponse(resp);
    });
}

/**
* 进入节目单详情页面时，需要调用该接口查询节目单详情。
*/
export function getPlaybillDetail(req: GetPlaybillDetailRequest, options: any = {}): Promise<GetPlaybillDetailResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('GetPlaybillDetail', '/VSP/V3/GetPlaybillDetail', req, options).then((resp: any) => {
        return safeGetPlaybillDetailResponse(resp);
    });
}

/**
* 客户端调用此接口获取播放最热的录播节目列表。
*/
export function queryHotPlaybill(req: QueryHotPlaybillRequest, options: any = {}): Promise<QueryHotPlaybillResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('QueryHotPlaybill', '/VSP/V3/QueryHotPlaybill', req, options).then((resp: any) => {
        return safeQueryHotPlaybillResponse(resp);
    });
}

/**
* VSP提供直播播放鉴权的接口，支持对频道和节目单的直播、回看、时移等业务进行鉴权，如果鉴权失败，可以指定是否返回内容可订购的产品，如果鉴权成功，可以指定是否返回频道的播放URL。
*
* VSP对内容指定业务进行鉴权时，会进行如下检查：
*
*
*
*  - 如果终端要求VSP检查锁和父母字，VSP会检查频道和节目单是否加锁或被父母字控制。
*
* 1.VSP优先检查内容是否被父母字控制，如果父母字控制了，则不再继续检查内容是否加锁。
*
* 2.如果频道和节目单关联的栏目被加锁，那么频道和节目单也被锁定。
*
* 3.对于节目单，如果节目单关联的频道被加锁，那么节目单也被锁定。
*
* 4.如果用户创建了Series锁，当节目单的SeriesID等于加锁的SeriesID，那么节目单也被锁定。
*
* 5.如果用户创建了流派锁，当节目单的Genre包含加锁的GenreID，那么节目单也被锁定。
*
* 6.对于来自同一个LifetimeID的多个节目单，如果其中一个节目单被加锁了，另一个节目单也被锁定。
*
*
*
*  - 如果终端携带了解锁密码，VSP检查解锁密码是否正确，如果不正确，返回错误。
*
*  - VSP检查频道/节目单指定的业务是否被订购。
*
*  - 如果业务已订购，VSP检查频道支持的CP设备类型是否包含当前设备的CP设备类型，如果不包含，返回错误。
*
*  - 如果业务未订购，且接口指定了要返回内容定价的产品，VSP返回频道/节目单指定业务定价的产品。
*
*  - 如果系统支持免费产品直接鉴权通过，VSP检查频道定价的产品是否包含免费产品，即价格是0的按次产品，如果有，则内容鉴权通过。
*
*  - 如果业务已订购，VSP获取播放地址，如果是播放直播，检查家庭内正在播放此直播的设备数是否超限，直播并发播放数来自频道媒资，如果是OTT设备播放Catch-up TV，检查家庭内正在播放Catch-up TV的设备数是否超出CP要求的上限。
*/
export function playChannel(req: PlayChannelRequest, options: any = {}): Promise<PlayChannelResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('PlayChannel', '/VSP/V3/PlayChannel', preparePlayRequest(req), options).then((resp: any) => {
        return safePlayChannelResponse(resp);
    });
}

/**
* VSP提供内容下载鉴权的接口，支持对回看节目单的下载业务进行鉴权，如果鉴权成功，返回回看节目单的下载地址。
*/
export function downloadCUTV(req: DownloadCUTVRequest, options: any = {}): Promise<DownloadCUTVResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('DownloadCUTV', '/VSP/V3/DownloadCUTV', req, options).then((resp: any) => {
        return safeDownloadCUTVResponse(resp);
    });
}

/**
* 终端进入播放或退出播放时，调用此接口上报播放行为，VSP对进入播放和退出播放的处理如下：
*
*
*
*  - VSP接收到进入播放请求后，缓存用户当前的播放记录，如果系统支持生成开始话单，则生成提供给Report的播放开始话单。
*
*  - VSP接收到退出播放请求，生成提供给Report的播放结束话单，并清除缓存记录。
*
*  - VSP接收到切换频道请求后，生成切换前频道的播放结束话单，并缓存切换后频道的播放记录。
*
* 原子接口
*/
export function reportChannel(req: ReportChannelRequest, options: any = {}): Promise<ReportChannelResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('ReportChannel', '/VSP/V3/ReportChannel', req, options).then((resp: any) => {
        return safeReportChannelResponse(resp);
    });
}

/**
* 客户端调用该接口将频道播放的心跳消息上报给VSP。
*
* 对于有播放控制的局点，客户端播放媒体流时，需要定时调用此接口发送心跳消息给VSP。
*
* 发送心跳消息主要适用于以下两个目的：
*
*
*
*  - 当终端异常退出时，VSP可以及时释放用户会话，供其他用户播放。
*
*  - 如果播放会话数已经超限，用户可以中断家庭内其他用户的播放行为，VSP通过心跳响应通知要中断的设备停止播放。
*
* 原子接口
*/
export function playChannelHeartbeat(req: PlayChannelHeartbeatRequest, options: any = {}): Promise<PlayChannelHeartbeatResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('PlayChannelHeartbeat', '/VSP/V3/PlayChannelHeartbeat', req, options).then((resp: any) => {
        return safePlayChannelHeartbeatResponse(resp);
    });
}

/**
* 模型：每个频道每天的节目单一个版本号。
*
* 提供全量的节目单版本，和变化的节目单版本的查询的能力。
*
* 原子接口
*/
export function queryPlaybillVersion(req: QueryPlaybillVersionRequest, options: any = {}): Promise<QueryPlaybillVersionResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('QueryPlaybillVersion', '/VSP/V3/QueryPlaybillVersion', req, options).then((resp: any) => {
        return safeQueryPlaybillVersionResponse(resp);
    });
}

/**
* 支持根据指定马赛克频道ID获取马赛克信息，不指定的话查询该用户所属命名空间的所有马赛克频道。
*/
export function queryMosaicChannelList(req: QueryMosaicChannelListRequest, options: any = {}): Promise<QueryMosaicChannelListResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('QueryMosaicChannelList', '/VSP/V3/QueryMosaicChannelList', req, options).then((resp: any) => {
        return safeQueryMosaicChannelListResponse(resp);
    });
}

/**
* VSP提供多个直播频道的播放鉴权的接口，对于每个频道，功能和已有的PlayChannel接口一样，由EPGGW做编排。
*
* 有以下几点需要注意：
*
*
*
*  - 为提高响应时间，接口中默认不返回播放URL和可订购产品列表
*
*  - 如果有特殊场景需要返回URL和可订购产品的话再添加相应字段。
*/
export function batchPlayChannel(req: BatchPlayChannelRequest, options: any = {}): Promise<BatchPlayChannelResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('BatchPlayChannel', '/VSP/V3/BatchPlayChannel', req, options).then((resp: any) => {
        return safeBatchPlayChannelResponse(resp);
    });
}

/**
* 获取VOD栏目列表调用此接口。
*/
export function queryVODSubjectList(req: QueryVODSubjectListRequest, options: any = {}): Promise<QueryVODSubjectListResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('QueryVODSubjectList', '/VSP/V3/QueryVODSubjectList', req, options).then((resp: any) => {
        return safeQueryVODSubjectListResponse(resp);
    });
}

/**
* 该接口用于获取栏目详情。
*/
export function querySubjectDetail(req: QuerySubjectDetailRequest, options: any = {}): Promise<QuerySubjectDetailResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('QuerySubjectDetail', '/VSP/V3/QuerySubjectDetail', req, options).then((resp: any) => {
        return safeQuerySubjectDetailResponse(resp);
    });
}

/**
* 根据栏目ID获取VOD列表，栏目ID不传则获取所有VOD，本接口支持各种排序和过滤功能。
*/
export function queryVODListBySubject(req: QueryVODListBySubjectRequest, options: any = {}): Promise<QueryVODListBySubjectResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('QueryVODListBySubject', '/VSP/V3/QueryVODListBySubject', req, options).then((resp: any) => {
        return safeQueryVODListBySubjectResponse(resp);
    });
}

/**
* 根据栏目ID获取到子栏目及子栏目中的一定数量VOD内容，支持分页，支持排序。
*/
export function querySubjectVODBySubjectID(req: QuerySubjectVODBySubjectIDRequest, options: any = {}): Promise<QuerySubjectVODBySubjectIDResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('QuerySubjectVODBySubjectID', '/VSP/V3/QuerySubjectVODBySubjectID', req, options).then((resp: any) => {
        return safeQuerySubjectVODBySubjectIDResponse(resp);
    });
}

/**
* 客户端调用此接口获取推荐VOD列表。
*/
export function queryRecmVODList(req: QueryRecmVODListRequest, options: any = {}): Promise<QueryRecmVODListResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('QueryRecmVODList', '/VSP/V3/QueryRecmVODList', req, options).then((resp: any) => {
        return safeQueryRecmVODListResponse(resp);
    });
}

/**
* 客户端调用本接口获取内容配置相关的数据。
*/
export function getContentConfig(req: GetContentConfigRequest, options: any = {}): Promise<GetContentConfigResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('GetContentConfig', '/VSP/V3/GetContentConfig', req, options).then((resp: any) => {
        return safeGetContentConfigResponse(resp);
    });
}

/**
* 进入VOD详情页面时，需要调用该接口。
*/
export function getVODDetail(req: GetVODDetailRequest, options: any = {}): Promise<GetVODDetailResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('GetVODDetail', '/VSP/V3/GetVODDetail', req, options).then((resp: any) => {
        return safeGetVODDetailResponse(resp);
    });
}

/**
* VSP提供点播播放鉴权的接口，支持对VOD进行鉴权，如果鉴权失败，可以指定是否返回内容可订购的产品，如果鉴权成功，返回VOD的播放URL。
*
* VSP对内容指定业务进行鉴权时，会进行如下检查：
*
*
*
*  - 如果终端要求VSP检查锁和父母字，VSP会检查VOD是否加锁或被父母字控制。
*
* 1.VSP优先检查内容是否被父母字控制，如果父母字控制了，则不再继续检查内容是否加锁。
*
* 2.如果VOD关联的栏目被加锁，那么VOD也被锁定。
*
*
*
*  - 如果终端携带了解锁密码，VSP检查解锁密码是否正确，如果不正确，返回错误。
*
*  - VSP检查VOD是否被订购。
*
*  - 如果VOD已订购，VSP检查VOD支持的CP设备类型是否包含当前设备的CP设备类型，如果不包含，返回错误。
*
*  - 如果VOD未订购，且接口指定了要返回内容定价的产品，VSP返回VOD定价的产品。
*
*  - 如果系统支持免费产品直接鉴权通过，检查VOD定价的产品是否包含免费产品，即价格是0的按次产品，如果有则内容鉴权通过。
*
*  - 如果VOD已订购，VSP获取播放地址时，检查家庭内正在播放此VOD的设备数是否超限、检查子网下IPTV或OTT领域的播放会话数是否超过子网播放会话数上限、如果是OTT设备播放VOD，检查家庭内正在播放VOD的设备总数是否超出CP要求的上限。
*
* 原子接口
*/
export function playVOD(req: PlayVODRequest, options: any = {}): Promise<PlayVODResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('PlayVOD', '/VSP/V3/PlayVOD', preparePlayRequest(req), options).then((resp: any) => {
        return safePlayVODResponse(resp);
    });
}

/**
* VSP提供内容下载鉴权的接口，支持对VOD的下载业务进行鉴权，如果鉴权成功，返回VOD的下载地址。
*/
export function downloadVOD(req: DownloadVODRequest, options: any = {}): Promise<DownloadVODResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('DownloadVOD', '/VSP/V3/DownloadVOD', req, options).then((resp: any) => {
        return safeDownloadVODResponse(resp);
    });
}

/**
* 终端进入播放或退出播放时，调用此接口上报播放行为，VSP对进入播放和退出播放的处理如下：
*
*
*
*  - VSP接收到进入播放请求后，缓存用户当前的播放记录，如果系统支持生成开始话单，VSP生成提供给Report的播放开始话单。
*
*  - VSP接收到退出播放请求，VSP生成提供给Report的播放结束话单，并清除缓存记录。
*/
export function reportVOD(req: ReportVODRequest, options: any = {}): Promise<ReportVODResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('ReportVOD', '/VSP/V3/ReportVOD', req, options).then((resp: any) => {
        return safeReportVODResponse(resp);
    });
}

/**
* 客户端调用该接口将VOD播放的心跳消息上报给VSP。
*
* 对于有播放控制的局点，客户端播放媒体流时，需要定时调用此接口发送心跳消息给VSP。
*
* 发送心跳消息主要适用于以下两个目的：
*
*
*
*  - 当终端异常退出时，VSP可以及时释放用户会话，供其他用户播放。
*
*  - 如果播放会话数已经超限，用户可以中断家庭内其他用户的播放行为，VSP通过心跳响应通知要中断的设备停止播放。
*/
export function playVODHeartbeat(req: PlayVODHeartbeatRequest, options: any = {}): Promise<PlayVODHeartbeatResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('PlayVODHeartbeat', '/VSP/V3/PlayVODHeartbeat', req, options).then((resp: any) => {
        return safePlayVODHeartbeatResponse(resp);
    });
}

/**
* VSP提供设置全局搜索条件的接口给终端，终端通过该接口设置过滤条件后，VSP将该过滤条件保存到用户的会话中，后续终端调用VSP的内容查询类接口时，VSP会判断用户是否设置了全局过滤条件，如果设置了，则根据全局过滤条件进行内容过滤。
*
* 原子接口
*/
export function setGlobalFilterCond(req: SetGlobalFilterCondRequest, options: any = {}): Promise<SetGlobalFilterCondResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('SetGlobalFilterCond', '/VSP/V3/SetGlobalFilterCond', req, options).then((resp: any) => {
        return safeSetGlobalFilterCondResponse(resp);
    });
}

/**
* VSP提供返回系统搜索热词的接口，默认按热度排序返回。
*/
export function searchHotKey(req: SearchHotKeyRequest, options: any = {}): Promise<SearchHotKeyResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('SearchHotKey', '/VSP/V3/SearchHotKey', req, options).then((resp: any) => {
        return safeSearchHotKeyResponse(resp);
    });
}

/**
* VSP提供内容搜索的接口，支持搜索的内容类型包括VOD、频道、VAS和节目单，支持搜索的内容属性包括内容名称、流派、关键字、演职员信息等，过滤条件包括国家、订购方式等。
*/
export function searchContent(req: SearchContentRequest, options: any = {}): Promise<SearchContentResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('SearchContent', '/VSP/V3/SearchContent', req, options).then((resp: any) => {
        return safeSearchContentResponse(resp);
    });
}

/**
* 用户进行搜索时，系统可以将相关的热门搜索关键字，热门内容名称等罗列出来，为用户提供搜索关键词建议。联想方式包括关键词补全、纠错联想，拼音/拼音首字母缩写联想等。
*/
export function suggestKeyword(req: SuggestKeywordRequest, options: any = {}): Promise<SuggestKeywordResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('SuggestKeyword', '/VSP/V3/SuggestKeyword', req, options).then((resp: any) => {
        return safeSuggestKeywordResponse(resp);
    });
}

/**
* VSP提供查询推荐内容的接口，VSP接收到终端请求后，会调用视频大数据智能推荐平台获取内容个性推荐接口，返回推荐内容列表。
*
* UI可按推荐位选择调用VSP推荐接口，传入对应的推荐类型等业务参数。视频大数据推荐系统实时返回推荐内容列表展示给终端用户。
*/
export function queryRecmContent(req: QueryRecmContentRequest, options: any = {}): Promise<QueryRecmContentResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('QueryRecmContent', '/VSP/V3/QueryRecmContent', req, options).then((resp: any) => {
        return safeQueryRecmContentResponse(resp);
    });
}

/**
* VSP提供返回热搜内容排行的接口。
*/
export function queryHotSearchContentsList(req: QueryHotSearchContentsListRequest, options: any = {}): Promise<QueryHotSearchContentsListResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('QueryHotSearchContentsList', '/VSP/V3/QueryHotSearchContentsList', req, options).then((resp: any) => {
        return safeQueryHotSearchContentsListResponse(resp);
    });
}

/**
* VSP提供新增用户的收藏夹接口，收藏夹有上限，如果用户已创建的收藏夹已经超过上限，VSP将返回错误。
*
* VSP会给每个Profile创建一个默认收藏夹，如果用户收藏内容的时候未指定收藏夹，则收藏内容会默认保存到默认收藏夹里，默认收藏夹的编号是-1。
*/
export function addFavoCatalog(req: AddFavoCatalogRequest, options: any = {}): Promise<AddFavoCatalogResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('AddFavoCatalog', '/VSP/V3/AddFavoCatalog', req, options).then((resp: any) => {
        return safeAddFavoCatalogResponse(resp);
    });
}

/**
* VSP提供删除用户的收藏夹接口，删除收藏夹时，收藏夹下的收藏内容也会同时删除。
*/
export function deleteFavoCatalog(req: DeleteFavoCatalogRequest, options: any = {}): Promise<DeleteFavoCatalogResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('DeleteFavoCatalog', '/VSP/V3/DeleteFavoCatalog', req, options).then((resp: any) => {
        return safeDeleteFavoCatalogResponse(resp);
    });
}

/**
* VSP提供更新用户的收藏夹接口。
*/
export function updateFavoCatalog(req: UpdateFavoCatalogRequest, options: any = {}): Promise<UpdateFavoCatalogResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('UpdateFavoCatalog', '/VSP/V3/UpdateFavoCatalog', req, options).then((resp: any) => {
        return safeUpdateFavoCatalogResponse(resp);
    });
}

/**
* VSP提供查询用户的收藏夹接口，收藏夹默认按照创建时间升序排列。
*/
export function queryFavoCatalog(req: QueryFavoCatalogRequest, options: any = {}): Promise<QueryFavoCatalogResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('QueryFavoCatalog', '/VSP/V3/QueryFavoCatalog', req, options).then((resp: any) => {
        return safeQueryFavoCatalogResponse(resp);
    });
}

/**
* VSP提供新增内容收藏的接口，支持收藏的内容类型包括VOD、频道、VAS。
*
* 内容收藏记录都是Profile管理的，不支持Profile之间共享，主Profile和子Profile都可以使用收藏功能。
*
* 内容收藏由上限控制，如果已收藏的内容超过上限，终端可以指定autoCover参数确定是否自动覆盖最老的收藏。
*
* 目前收藏上限支持总数控制和分内容类型控制。
*/
export function createFavorite(req: CreateFavoriteRequest, options: any = {}): Promise<CreateFavoriteResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('CreateFavorite', '/VSP/V3/CreateFavorite', req, options).then((resp: any) => {
        return safeCreateFavoriteResponse(resp);
    });
}

/**
* VSP提供删除内容收藏的接口，支持清空用户的所有收藏、删除指定内容类型的所有收藏或者删除指定内容的收藏。
*/
export function deleteFavorite(req: DeleteFavoriteRequest, options: any = {}): Promise<DeleteFavoriteResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('DeleteFavorite', '/VSP/V3/DeleteFavorite', req, options).then((resp: any) => {
        return safeDeleteFavoriteResponse(resp);
    });
}

/**
* VSP提供查询内容收藏的接口，支持查询用户的所有收藏、查询指定内容类型的所有收藏。
*/
export function queryFavorite(req: QueryFavoriteRequest, options: any = {}): Promise<QueryFavoriteResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('QueryFavorite', '/VSP/V3/QueryFavorite', req, options).then((resp: any) => {
        return safeQueryFavoriteResponse(resp);
    });
}

/**
* VSP提供收藏排序功能，在收藏管理页面，如果用户能够对收藏内容列表的顺序进行自定义的调整，客户端可以调用该接口将用户排过序的收藏内容列表保存到VSP平台。
*/
export function sortFavorite(req: SortFavoriteRequest, options: any = {}): Promise<SortFavoriteResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('SortFavorite', '/VSP/V3/SortFavorite', req, options).then((resp: any) => {
        return safeSortFavoriteResponse(resp);
    });
}

/**
* VSP提供新增内容童锁的接口，支持加锁的对象包括VOD、频道、VAS、节目单、内容流派和节目单SeriesID。
*
* 内容童锁记录都是订户的主Profile管理的，主Profile可以添加对所有Profile生效的共享锁，也可以添加对指定Profile生效的独享锁。同时某些局点要求主Profile可以给自己添加独享锁。
*/
export function createLock(req: CreateLockRequest, options: any = {}): Promise<CreateLockResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('CreateLock', '/VSP/V3/CreateLock', req, options).then((resp: any) => {
        return safeCreateLockResponse(resp);
    });
}

/**
* VSP提供删除内容童锁的接口，支持清空用户的所有童锁、清空指定成员Profile的独享锁、删除指定童锁类型的所有记录或者删除指定内容/对象的加锁记录。
*/
export function deleteLock(req: DeleteLockRequest, options: any = {}): Promise<DeleteLockResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('DeleteLock', '/VSP/V3/DeleteLock', req, options).then((resp: any) => {
        return safeDeleteLockResponse(resp);
    });
}

/**
* VSP提供查询内容童锁的接口，支持查询用户的所有童锁、查询指定成员Profile的共享锁和独享锁、查询指定童锁类型的记录。
*/
export function queryLock(req: QueryLockRequest, options: any = {}): Promise<QueryLockResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('QueryLock', '/VSP/V3/QueryLock', req, options).then((resp: any) => {
        return safeQueryLockResponse(resp);
    });
}

/**
* VSP提供新增内容书签的接口，支持加锁的对象包括VOD、回看节目单。
*
* 内容书签记录都是User Profile管理，不支持User Profile之间共享。
*
* 内容书签有上限控制，如果书签超过上限，系统自动覆盖最老的书签。
*
* 目前书签上限支持总数控制和分书签类型控制。
*/
export function createBookmark(req: CreateBookmarkRequest, options: any = {}): Promise<CreateBookmarkResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('CreateBookmark', '/VSP/V3/CreateBookmark', req, options).then((resp: any) => {
        return safeCreateBookmarkResponse(resp);
    });
}

/**
* VSP提供删除内容书签的接口，支持清空用户的所有书签、删除指定书签类型的所有书签或者删除指定内容的书签。
*/
export function deleteBookmark(req: DeleteBookmarkRequest, options: any = {}): Promise<DeleteBookmarkResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('DeleteBookmark', '/VSP/V3/DeleteBookmark', req, options).then((resp: any) => {
        return safeDeleteBookmarkResponse(resp);
    });
}

/**
* VSP提供查询内容书签的接口，支持查询用户的所有书签和按书签类型查询书签。
*
* 如果接口指定了ratingID的过滤条件，VSP只返回内容观看级别小于等于该ratingID的书签。
*/
export function queryBookmark(req: QueryBookmarkRequest, options: any = {}): Promise<QueryBookmarkResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('QueryBookmark', '/VSP/V3/QueryBookmark', req, options).then((resp: any) => {
        return safeQueryBookmarkResponse(resp);
    });
}

/**
* VSP提供新增内容评分的接口，目前支持评分的对象只包括VOD。
*
* 个人评分记录都是Profile管理的，不支持Profile之间共享，主Profile和子Profile都可以使用此功能。
*
* 个人评分有上限控制，如果评分记录超过上限，VSP自动覆盖最老的记录。
*
* 如果用户对某个内容评分过，VSP会覆盖旧的个人评分值。
*/
export function createContentScore(req: CreateContentScoreRequest, options: any = {}): Promise<CreateContentScoreResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('CreateContentScore', '/VSP/V3/CreateContentScore', req, options).then((resp: any) => {
        return safeCreateContentScoreResponse(resp);
    });
}

/**
* VSP提供内容的接口，目前支持评分的对象只包括VOD。
*
* 个人点赞记录都是Profile管理的，不支持Profile之间共享，主Profile和子Profile都可以使用此功能。
*
* 个人点赞有上限控制，如果点赞记录超过上限，VSP自动覆盖最老的记录，但此时内容上的赞总数不会减少。
*
* 如果用户赞过某个内容，则只能取消点赞。相反，如果用户没有赞过某个内容，则只能点赞。
*/
export function setContentLike(req: SetContentLikeRequest, options: any = {}): Promise<SetContentLikeResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('SetContentLike', '/VSP/V3/SetContentLike', req, options).then((resp: any) => {
        return safeSetContentLikeResponse(resp);
    });
}

/**
* VSP提供新增播放列表的接口，播放列表是User Profile管理的，不支持User Profile之间共享。
*
* 播放列表有上限控制，如果新增的播放列表超过上限，VSP返回错误。
*/
export function createPlaylist(req: CreatePlaylistRequest, options: any = {}): Promise<CreatePlaylistResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('CreatePlaylist', '/VSP/V3/CreatePlaylist', req, options).then((resp: any) => {
        return safeCreatePlaylistResponse(resp);
    });
}

/**
* VSP提供更新播放列表的接口，目前可以更新的只是播放列表名称。
*/
export function updatePlaylist(req: UpdatePlaylistRequest, options: any = {}): Promise<UpdatePlaylistResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('UpdatePlaylist', '/VSP/V3/UpdatePlaylist', req, options).then((resp: any) => {
        return safeUpdatePlaylistResponse(resp);
    });
}

/**
* VSP提供删除播放列表的接口，删除播放列表时，播放列表里的内容会一并删除掉。
*/
export function deletePlaylist(req: DeletePlaylistRequest, options: any = {}): Promise<DeletePlaylistResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('DeletePlaylist', '/VSP/V3/DeletePlaylist', req, options).then((resp: any) => {
        return safeDeletePlaylistResponse(resp);
    });
}

/**
* VSP提供查询用户的播放列表接口。
*/
export function queryPlaylist(req: QueryPlaylistRequest, options: any = {}): Promise<QueryPlaylistResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('QueryPlaylist', '/VSP/V3/QueryPlaylist', req, options).then((resp: any) => {
        return safeQueryPlaylistResponse(resp);
    });
}

/**
* VSP提供添加内容到播放列表里的接口，每个播放列表的内容有上限控制，如果新添加的内容超过上限，VSP根据传参autoCover，确定是自动删除最早添加的内容还是返回错误。
*/
export function addPlaylistContent(req: AddPlaylistContentRequest, options: any = {}): Promise<AddPlaylistContentResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('AddPlaylistContent', '/VSP/V3/AddPlaylistContent', req, options).then((resp: any) => {
        return safeAddPlaylistContentResponse(resp);
    });
}

/**
* VSP提供对播放列表里的内容进行排序接口，终端需要将某个播放列表里的内容进行全量排序后，通知VSP平台。
*/
export function sortPlaylistContent(req: SortPlaylistContentRequest, options: any = {}): Promise<SortPlaylistContentResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('SortPlaylistContent', '/VSP/V3/SortPlaylistContent', req, options).then((resp: any) => {
        return safeSortPlaylistContentResponse(resp);
    });
}

/**
* VSP提供删除播放列表里的内容接口，支持按内容类型、内容ID删除，或者清空播放列表里的所有内容。
*/
export function deletePlaylistContent(req: DeletePlaylistContentRequest, options: any = {}): Promise<DeletePlaylistContentResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('DeletePlaylistContent', '/VSP/V3/DeletePlaylistContent', req, options).then((resp: any) => {
        return safeDeletePlaylistContentResponse(resp);
    });
}

/**
* VSP提供查询播放列表里的内容接口。
*/
export function queryPlaylistContent(req: QueryPlaylistContentRequest, options: any = {}): Promise<QueryPlaylistContentResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('QueryPlaylistContent', '/VSP/V3/QueryPlaylistContent', req, options).then((resp: any) => {
        return safeQueryPlaylistContentResponse(resp);
    });
}

/**
* VSP提供新增节目提醒的接口，支持提醒的内容类型包括VOD和未来节目单，其中未来节目单的开始时间必须大于【当前时间+提醒提前时长】，提醒提前时长可以用户自己设置，还可以使用系统默认参数。
*
* 节目提醒记录都是User Profile管理的，不支持User Profile之间共享。
*/
export function createReminder(req: CreateReminderRequest, options: any = {}): Promise<CreateReminderResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('CreateReminder', '/VSP/V3/CreateReminder', req, options).then((resp: any) => {
        return safeCreateReminderResponse(resp);
    });
}

/**
* VSP提供删除节目提醒的接口，支持清空用户的所有提醒、删除指定内容类型的所有提醒或者删除指定内容的提醒。
*/
export function deleteReminder(req: DeleteReminderRequest, options: any = {}): Promise<DeleteReminderResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('DeleteReminder', '/VSP/V3/DeleteReminder', req, options).then((resp: any) => {
        return safeDeleteReminderResponse(resp);
    });
}

/**
* VSP提供查询节目提醒的接口，支持根据内容类型（节目单，VOD)查询用户的所有提醒记录，节目单默认按提醒时间升序排列，VOD按上映日期升序排列。EPGGW需要根据查询出的VOD预约记录和内容呈现中查询的内容的上映日期进行排序。
*/
export function queryReminder(req: QueryReminderRequest, options: any = {}): Promise<QueryReminderResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('QueryReminder', '/VSP/V3/QueryReminder', req, options).then((resp: any) => {
        return safeQueryReminderResponse(resp);
    });
}

/**
* 此接口提供给Launcher使用，Launcher通过访问该接口获取桌面布局配置文件。
*/
export function queryLauncher(req: QueryLauncherRequest, options: any = {}): Promise<QueryLauncherResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('QueryLauncher', '/VSP/V3/QueryLauncher', req, options).then((resp: any) => {
        return safeQueryLauncherResponse(resp);
    });
}

/**
* Launcher根据需要获取资源位增量数据，向PHS发起资源位详情获取信息。
*/
export function batchGetResStrategyData(req: BatchGetResStrategyDataRequest, options: any = {}): Promise<BatchGetResStrategyDataResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('BatchGetResStrategyData', '/VSP/V3/BatchGetResStrategyData', req, options).then((resp: any) => {
        return safeBatchGetResStrategyDataResponse(resp);
    });
}

/**
* 此接口提供给Launcher使用，Launcher发送请求检查配置数据有没有最新的，用于定期更新数据（定期调用的频度为30分钟/15分钟，或者回桌面时及时进行调用）。
*/
export function getLatestResources(req: GetLatestResourcesRequest, options: any = {}): Promise<GetLatestResourcesResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('GetLatestResources', '/VSP/V3/GetLatestResources', req, options).then((resp: any) => {
        return safeGetLatestResourcesResponse(resp);
    });
}

/**
* 提供一个中转接口，屏蔽平台获取播放URL、跳转URL的差异，由智能桌面直接获取地址。
*/
export function jumpURLjsp(req: jumpURLjspRequest, options: any = {}): Promise<jumpURLjspResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('jumpURLjsp', '/VSP/MediaService/jumpURL.jsp', req, options).then((resp: any) => {
        return safejumpURLjspResponse(resp);
    });
}

/**
* 此接口提供给终端使用，终端发送请求检查专题数据有无更新(平台判断终端请求的时间戳与系统中该类型终端所有专题时间戳最大值是否相等)，平台通过该接口定期返回全量生效的专题数据（调用的频度由平台返回给终端，专题信息的更新场景很少，默认为30分钟）。
*/
export function getTopics(req: GetTopicsRequest, options: any = {}): Promise<GetTopicsResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('GetTopics', '/VSP/V3/GetTopics', req, options).then((resp: any) => {
        return safeGetTopicsResponse(resp);
    });
}

/**
* 终端上报设备信息给VSP，VSP负责通知给Push Server。
*
* 原子接口
*/
export function submitDeviceInfo(req: SubmitDeviceInfoRequest, options: any = {}): Promise<SubmitDeviceInfoResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('SubmitDeviceInfo', '/VSP/V3/SubmitDeviceInfo', req, options).then((resp: any) => {
        return safeSubmitDeviceInfoResponse(resp);
    });
}

/**
* VSP给终端提供主动触发按TAG推送消息的接口。
*
* 原子接口
*/
export function pushMsgByTag(req: PushMsgByTagRequest, options: any = {}): Promise<PushMsgByTagResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('PushMsgByTag', '/VSP/V3/PushMsgByTag', req, options).then((resp: any) => {
        return safePushMsgByTagResponse(resp);
    });
}

/**
* 1.VSP接收到下载NPVR任务的请求，根据录制文件ID查询任务信息，如果任务不存在，返回错误；
*
* 2.VSP从NPVR任务上可以获取到频道ID、媒资ID、录制开始和结束时间、节目单ID（可选，仅节目单录制支持）和播放URL（可选，仅CloudPVR支持）；
*
* 3.VSP进行频道的鉴权，逻辑同直播频道播放；
*
* 4.如果鉴权失败，返回可订购产品，逻辑同直播频道播放；
*
* 5.如果鉴权成功，VSP进行生成NPVR下载URL。
*/
export function downloadNPVR(req: DownloadNPVRRequest, options: any = {}): Promise<DownloadNPVRResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('DownloadNPVR', '/VSP/V3/DownloadNPVR', req, options).then((resp: any) => {
        return safeDownloadNPVRResponse(resp);
    });
}

/**
* 终端进入播放或退出播放时，调用此接口上报播放行为，VSP对进入播放和退出播放的处理如下：
*
*
*
*  - VSP接收到进入播放请求后，缓存用户当前的播放记录，如果系统支持生成开始话单，则生成提供给Report的播放开始话单。
*
*  - VSP接收到退出播放请求，生成提供给Report的播放结束话单，并清除缓存记录。
*/
export function reportPVR(req: ReportPVRRequest, options: any = {}): Promise<ReportPVRResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('ReportPVR', '/VSP/V3/ReportPVR', req, options).then((resp: any) => {
        return safeReportPVRResponse(resp);
    });
}

/**
* 客户端调用该接口将NPVR播放的心跳消息上报给VSP。
*
* 对于有播放控制的局点，客户端播放媒体流时，需要定时调用此接口发送心跳消息给VSP。
*
* 发送心跳消息主要适用于以下两个目的：
*
*
*
*  - 当终端异常退出时，VSP可以及时释放用户会话，供其他用户播放。
*
*  - 如果播放会话数已经超限，用户可以中断家庭内其他用户的播放行为，VSP通过心跳响应通知要中断的设备停止播放。
*
*  - 目前NPVR仅支持场景一。
*/
export function playNPVRHeartbeat(req: PlayNPVRHeartbeatRequest, options: any = {}): Promise<PlayNPVRHeartbeatResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('PlayNPVRHeartbeat', '/VSP/V3/PlayNPVRHeartbeat', req, options).then((resp: any) => {
        return safePlayNPVRHeartbeatResponse(resp);
    });
}

/**
* 接口提供新增时间段和节目单录制任务的功能。平台接收到新增录制任务新增后，会进行如下检查：
*
* **NPVR权限检查：**
*
* 如果传参包括媒资ID，媒资必须支持NPVR；
*
* 如果传参不包含媒资ID，频道下必须存在支持NPVR的媒资；
*
* 如果是节目单录制，节目单必须支持NPVR，如果节目单的前后padding对应的节目单不支持NPVR，将前后padding的时长置为0；
*
* 如果是时间段录制，时间段内的节目单必须都支持NPVR。
*
* **CPVR权限检查：**
*
* 媒资必须支持CPVR；
*
* 如果是节目单录制，节目单必须支持CPVR，如果节目单的前后padding对应的节目单不支持CPVR，将前后padding的时长置为0；
*
* 如果是时间段录制，时间段内的节目单必须都支持CPVR。
*
* **PVR空间检查：**
*
* 如果创建节目单或者时间段录制任务，如果任务已经开始，用户总空间必须大于等于[录制中、录制成功、录制部分成功]的文件加上新增任务占用的空间，否则用户总空间必须大于等于[未录制、录制中、录制成功、录制部分成功]的文件加上新增任务占用的空间。
*
* 对于即时录制，如果空间不足，平台返回错误，任务不会创建。
*
* 对于预约录制，如果空间不足，平台返回错误，但是任务仍会创建。
*
* 对于CPVR录制只有在传入磁盘信息的情况下才会做空间检测。
*
* 如果创建系列或者周期录制任务，平台会检查每个子任务的空间是否足够，如果空间不足，当第一个子任务空间不足且已启动，所有子任务不保存，否则子任务继续保存。
*
* 平台的NPVR空间支持2种计算方式，按容量计算和按时长计算：
*
* 如果按容量计算，每个录制文件的容量都要计算在内；
*
* 如果按时长计算，任务下的多个录制文件只算一份时长，以文件最大时长为准。
*
* **对于录制中的空间计算，结束时间以文件的录制结束时间为准，而不是当前时间。**
*
* **Cloud PVR和CPVR的并发检查：**
*
*
*
*  - 平台获取所有和待新增的录制任务时间有交叉的录制任务。
*
*  - 如果任务数超出了并发数上限，首先尝试撮合任务的前后padding时间，如果撮合失败，返回错误。
*
*  - 若发生CPVR并发冲突，需要返回冲突列表。
*
* 原子接口
*/
export function addPVR(req: AddPVRRequest, options: any = {}): Promise<AddPVRResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('AddPVR', '/VSP/V3/AddPVR', req, options).then((resp: any) => {
        return safeAddPVRResponse(resp);
    });
}

/**
* 接口提供修改录制任务的功能，每种录制任务可以修改的属性包括：
*
* 原子接口
*/
export function updatePVR(req: UpdatePVRRequest, options: any = {}): Promise<UpdatePVRResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('UpdatePVR', '/VSP/V3/UpdatePVR', req, options).then((resp: any) => {
        return safeUpdatePVRResponse(resp);
    });
}

/**
* 接口提供新增系列和周期录制任务的功能。平台接收到新增录制任务新增后，会进行如下检查：
*
* **PVR权限检查：**
*
*
*
*  - 如果传参包括媒资ID，媒资必须支持NPVR或CPVR；
*
*  - 若是新增NPVR系列周期任务，若传参不包含媒资ID，频道下必须存在支持NPVR的媒资。
*
* **系列录制重复检查：**
*
* 平台检查用户是否存在频道ID、SeriesID一样的系列录制任务，如果存在，平台对重复的录制任务执行修改操作。
*
* **生成系列或周期子任务：**
*
* 系列录制是根据直播节目单生成SeriesID匹配的N天内的子任务，如果子任务是重播节目，则不重复生成。
*
* 周期录制是生成从生效日期开始到N天内满足录制策略里的星期条件的子任务。
*
* N一般和直播节目单展示天数配置一致，比如7天。
*
* 对于生成的子任务，平台会检查录制权限，逻辑同单录制任务。
*
* 如果检查通过，平台会生成录制子任务和录制文件，如果录制文件已经开始了，平台会通知DM启动录制。
*
* 原子接口
*/
export function addPeriodicPVR(req: AddPeriodicPVRRequest, options: any = {}): Promise<AddPeriodicPVRResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('AddPeriodicPVR', '/VSP/V3/AddPeriodicPVR', req, options).then((resp: any) => {
        return safeAddPeriodicPVRResponse(resp);
    });
}

/**
* 接口提供修改录制任务的功能，每种录制任务可以修改的属性包括：
*
* 当进行修改录制任务时，平台会保证已经生成的子任务的录制ID不变。
*
* 原子接口
*/
export function updatePeriodicPVR(req: UpdatePeriodicPVRRequest, options: any = {}): Promise<UpdatePeriodicPVRResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('UpdatePeriodicPVR', '/VSP/V3/UpdatePeriodicPVR', req, options).then((resp: any) => {
        return safeUpdatePeriodicPVRResponse(resp);
    });
}

/**
* 平台提供查询录制任务的接口，支持传入父任务、单任务和录制文件的过滤条件。
*
* **父任务过滤原则：**
*
* 如果查询条件包括父任务过滤条件，平台根据[是否过期]条件过滤父任务，过期表示父任务的结束时间&lt;&#x3D;当前时间，返回的父任务不需要包含子任务和子任务ID。
*
* **单任务过滤原则：**
*
* 如果查询条件包含单任务过滤条件，平台根据录制策略类型和[是否包含子任务]条件，查询出单任务后根据录制状态过滤，对于录制状态的处理原则是：
*
* 未录制：录制任务的所有录制文件都是未录制的；
*
* 录制中：录制任务的任何一个录制文件是录制中的；
*
* 录制成功：录制任务的所有录制文件都结束了，且至少有一个录制文件的状态是成功；
*
* 录制部分成功：录制任务的所有录制文件都结束了，且录制文件的状态都不是成功，且至少有一个录制文件的状态是部分成功；
*
* 录制失败：录制任务的所有录制文件都结束了，且录制文件的状态都是失败；
*
* 其他：只要不是上述任何一种状态，都归类为其他。
*
* 其中，录制文件的未录制、录制中和录制结束都是根据时间计算出来的，规则如下：
*
* 未录制：录制文件的（开始时间-beginOffset）&gt; 当前时间；
*
* 录制中：录制文件的（开始时间-beginOffset）&lt;&#x3D;当前时间 且（结束时间+endOffset）&gt;当前时间；
*
* 录制结束：录制文件的（结束时间+endOffset）&lt;&#x3D;当前时间。
*
* **展示方式：**
*
* 如果单任务查询后的结果包含子任务，平台根据子任务的展示方式控制返回：
*
* 如果子任务单独返回，平台将普通任务和子任务平级返回。
*
* 如果子任务打包成父任务返回，平台返回普通任务和子任务归属的父任务，如果有多个子任务归属一个父任务，只返回一个父任务，父任务下包含其中一个子任务和所有子任务ID。
*
* 如果查询条件即有父任务也有单任务的，可能出现子任务打包后的父任务，和父任务过滤条件查询出的父任务重叠，此处只返回打包后的父任务即可。
*
* **排序和分页：**
*
* 平台对录制任务进行排序和分页，排序目前仅支持录制时间排序，其中单任务每种状态获取的时间是：
*
* 未录制：取录制任务的开始时间，包括padding时间；
*
* 录制中：取录制任务的开始时间，包括padding时间；
*
* 其他：任取一个文件的时间；
*
* 录制成功：任取一个成功的文件的时间；
*
* 录制部分成功：任取一个部分成功的文件的时间；
*
* 录制失败：取录制任务的开始时间，包括padding时间。
*
* 注意，如果查询的子任务是要打包成父任务返回，平台先对父任务下的子任务列表排序后，再将第一个子任务的时间作为父任务的时间，和其他任务排序，如果出现单录制任务时间一样，就按名称字典序排序。
*
* 对于没有子任务的父任务，如果是按开始时间正序排序，这些父任务排在最后，否则这些父任务排在最前面，如果有多个这样的父任务，按照名称字典序排序。
*
* 原子接口
*/
export function queryPVR(req: QueryPVRRequest, options: any = {}): Promise<QueryPVRResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('QueryPVR', '/VSP/V3/QueryPVR', req, options).then((resp: any) => {
        return safeQueryPVRResponse(resp);
    });
}

/**
* 平台提供搜索录制任务的接口，目前仅支持按录制名称搜索，且匹配方式为全模糊，比如搜索 abc, 匹配结果可以为：abc , abcde, aabcd, aaabc。
*
* 如果搜索出了子任务，子任务暂不需要包装成父任务返回。
*
* 原子接口
*/
export function searchPVR(req: SearchPVRRequest, options: any = {}): Promise<SearchPVRResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('SearchPVR', '/VSP/V3/SearchPVR', req, options).then((resp: any) => {
        return safeSearchPVRResponse(resp);
    });
}

/**
* 平台提供查询根据录制任务ID查询录制任务的接口。
*
* 原子接口
*/
export function queryPVRByID(req: QueryPVRByIDRequest, options: any = {}): Promise<QueryPVRByIDResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('QueryPVRByID', '/VSP/V3/QueryPVRByID', req, options).then((resp: any) => {
        return safeQueryPVRByIDResponse(resp);
    });
}

/**
* 取消录制任务是指，对未来的录制任务做删除，对正在进行中的录制任务做停止。改接口只对单个录制任务（节目单、时间段录制和周期系列子录制任务）生效。对已经结束的任务不做任何操作。
*
* 可以批量指定PVR的ID列表批量取消PVR，支持可以按类型取消。
*
* 原子接口
*/
export function cancelPVRByID(req: CancelPVRByIDRequest, options: any = {}): Promise<CancelPVRByIDResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('CancelPVRByID', '/VSP/V3/CancelPVRByID', req, options).then((resp: any) => {
        return safeCancelPVRByIDResponse(resp);
    });
}

/**
* 取消录制任务是指，对未来的录制任务做删除，对正在进行中的录制任务做停止。改接口只对单个录制任务（节目单、时间段录制和周期系列子录制任务）生效。对已经结束的任务不做任何操作。
*
* 可以批量指定节目单ID取消这个节目单下的所有PVR，支持按存储位置取消。
*
* 原子接口
*/
export function cancelPVRByPlaybillID(req: CancelPVRByPlaybillIDRequest, options: any = {}): Promise<CancelPVRByPlaybillIDResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('CancelPVRByPlaybillID', '/VSP/V3/CancelPVRByPlaybillID', req, options).then((resp: any) => {
        return safeCancelPVRByPlaybillIDResponse(resp);
    });
}

/**
* 接口提供删除录制任务的功能。删除支持按条件删除，条件的处理原则如下：
*
*
*
*  - 只有singlePVRIDs、periodicPVRIDs，如果删除的是节目单或者时间段的录制任务，删除录制任务的时候，录制文件也会一并删除，如果删除的系列和周期任务，将删除这个父任务所有未开始的子任务，删除未开始的子任务后，若父任务还存在正在录制中或录制结束的任务，则依然保留这个父任务，若父任务下没有任何子任务，则父任务也会被删除。
*
*  - 既有planIds又有storageType，表示删除指定录制任务下指定介质的录制文件，该场景仅在混合录制任务存在。
*
* 原子接口
*/
export function deletePVRByID(req: DeletePVRByIDRequest, options: any = {}): Promise<DeletePVRByIDResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('DeletePVRByID', '/VSP/V3/DeletePVRByID', req, options).then((resp: any) => {
        return safeDeletePVRByIDResponse(resp);
    });
}

/**
* 接口提供根据查询条件删除节目单、时间段、周期系列任务的功能。
*
* 原子接口
*/
export function deletePVRByCondition(req: DeletePVRByConditionRequest, options: any = {}): Promise<DeletePVRByConditionResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('DeletePVRByCondition', '/VSP/V3/DeletePVRByCondition', req, options).then((resp: any) => {
        return safeDeletePVRByConditionResponse(resp);
    });
}

/**
* 接口提供根据磁盘ID删除CPVR录制任务的功能。
*
* 原子接口
*/
export function deleteCPVRByDiskID(req: DeleteCPVRByDiskIDRequest, options: any = {}): Promise<DeleteCPVRByDiskIDResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('DeleteCPVRByDiskID', '/VSP/V3/DeleteCPVRByDiskID', req, options).then((resp: any) => {
        return safeDeleteCPVRByDiskIDResponse(resp);
    });
}

/**
* 接口提供查询订户的NPVR和CPVR空间的功能
*
* **NPVR空间查询：**
*
* 支持查询订户的NPVR总空间、已用空间和预约空间。
*
* 已用空间是计算[录制中、录制完成、录制成功、录制部分成功]的文件占用的空间。
*
* 预约空间是计算未录制的文件占用的空间。
*
* 平台的NPVR空间支持2种计算方式，按容量计算和按时长计算：
*
* 如果按容量计算，每个录制文件的容量都要计算在内；
*
* 如果按时长计算，任务下的多个录制文件只算一份时长，以文件最大时长为准；
*
* **CPVR空间查询：**
*
* 接口提供查询订户的指定STB预约空间的功能。
*
* 预约空间是计算未录制的文件占用的空间。
*
* 原子接口
*/
export function queryPVRSpace(req: QueryPVRSpaceRequest, options: any = {}): Promise<QueryPVRSpaceResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('QueryPVRSpace', '/VSP/V3/QueryPVRSpace', req, options).then((resp: any) => {
        return safeQueryPVRSpaceResponse(resp);
    });
}

/**
* 接口提供根据任务ID查询任务占用CPVR或NPVR空间的信息。
*
* 原子接口
*/
export function queryPVRSpaceByID(req: QueryPVRSpaceByIDRequest, options: any = {}): Promise<QueryPVRSpaceByIDResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('QueryPVRSpaceByID', '/VSP/V3/QueryPVRSpaceByID', req, options).then((resp: any) => {
        return safeQueryPVRSpaceByIDResponse(resp);
    });
}

/**
* 接口提供解决STB IO冲突的能力，当客户端在新增、修改单录制计划和父录制计划时，可能因为STB IO上限导致存在有冲突并发的录制计划。平台将按照录制计划冲突时间段，将冲突按分组返回。比如新增单录制计划A，已有单录制计划B、C、D：
*
* 根据A的录制时间，划分的时间段，以及每个时间段包括的录制计划分别是：
*
* T1：A、B、C
*
* T2：A、B
*
* T3：A
*
* T4：A、D
*
* 假设STB IO上限是2，那么平台只返回1个冲突分组，即T1：A、B、C
*
* 假设STB IO上限是1，那么平台只返回3个冲突分组，即{T1：A、B、C}、{ T2：A、B }、{ T4：A、D}。
*
* 所谓冲突解决是客户端根据平台返回的冲突分组，提示用户保留和去保留哪些录制计划，当用户勾选完毕某一个冲突分组后，Client调用此接口让平台检查是否冲突已经解决，如果待检查的冲突分组已经解决，平台会自动检查下一个分组是否也解决了，如果是的话，继续下个冲突检查，依次类推，直到整个冲突分组都检查完毕，如果在检查中途发现某个分组没有解决，就返回未解决的分组索引，Client会提示用户从这一个分组继续解决。
*
* 特殊的，当Client创建或者修改父录制计划时，如果所有的冲突录制计划都是父计划下的子计划，那么平台返回的冲突分组的录制计划是父计划，且只有一个冲突分组，如果用户去保留某个父计划，那么此父计划下有冲突的子计划都将不保留。
*
* 原子接口
*/
export function solvePVRConflict(req: SolvePVRConflictRequest, options: any = {}): Promise<SolvePVRConflictResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('SolvePVRConflict', '/VSP/V3/SolvePVRConflict', req, options).then((resp: any) => {
        return safeSolvePVRConflictResponse(resp);
    });
}

/**
* 设置master盒子，平台需要保存master盒子的ID，并将所有未开始的CPVR的目标设备ID改成用户设置的master盒子ID。
*
* 原子接口
*/
export function setMasterSTBID(req: SetMasterSTBIDRequest, options: any = {}): Promise<SetMasterSTBIDResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('SetMasterSTBID', '/VSP/V3/SetMasterSTBID', req, options).then((resp: any) => {
        return safeSetMasterSTBIDResponse(resp);
    });
}

/**
* 获取MasterID
*
* 原子接口
*/
export function getMasterSTBID(req: GetMasterSTBIDRequest, options: any = {}): Promise<GetMasterSTBIDResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('GetMasterSTBID', '/VSP/V3/GetMasterSTBID', req, options).then((resp: any) => {
        return safeGetMasterSTBIDResponse(resp);
    });
}

/**
* 接口提供更新STB录制结果的功能，当STB在录制完成后，会上报结果给STB EPG，STB EPG通过OpenAPI GW调用此接口更新录制文件的录制结果。
*
* 在上报录制结果时，需要注意：
*
*
*
*  - 更新后的状态必须是[录制成功、部分成功、录制失败]中的任何一种。
*
*  - 更新前的状态必须是[未录制、录制中、录制完成]中的任何一种。
*
*  - 如果录制成功或者部分成功，必须传入录制时长、文件大小、录制开始时间、录制结束时间、前后padding（可选）。
*
* 原子接口
*/
export function updateCPVRStatus(req: UpdateCPVRStatusRequest, options: any = {}): Promise<UpdateCPVRStatusResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('UpdateCPVRStatus', '/VSP/V3/UpdateCPVRStatus', req, options).then((resp: any) => {
        return safeUpdateCPVRStatusResponse(resp);
    });
}

/**
* 1.EPGGW接收到播放PVR任务的请求，根据录制文件ID查询任务信息，如果任务不存在，返回错误；
*
* 2.EPGGW从PVR任务上可以获取到频道ID、媒资ID、录制开始和结束时间、节目单ID（可选，仅节目单录制支持）和播放URL（可选，仅CloudPVR支持）；
*
* 3.EPGGW进行频道的鉴权，逻辑同直播频道播放；
*
* 4.如果鉴权失败，返回可订购产品，逻辑同直播频道播放；
*
* 5.如果是NPVR且鉴权成功，EPGGW进行生成NPVR播放URL。
*
* 原子接口
*/
export function playPVR(req: PlayPVRRequest, options: any = {}): Promise<PlayPVRResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('PlayPVR', '/VSP/V3/PlayPVR', preparePlayRequest(req), options).then((resp: any) => {
        return safePlayPVRResponse(resp);
    });
}

/**
* 完成带宽的申请。
*/
export function applyStream(req: ApplyStreamRequest, options: any = {}): Promise<ApplyStreamResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('ApplyStream', '/VSP/V3/ApplyStream', req, options).then((resp: any) => {
        return safeApplyStreamResponse(resp);
    });
}

/**
* 完成带宽释放。
*/
export function releaseStream(req: ReleaseStreamRequest, options: any = {}): Promise<ReleaseStreamResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('ReleaseStream', '/VSP/V3/ReleaseStream', req, options).then((resp: any) => {
        return safeReleaseStreamResponse(resp);
    });
}

/**
* 查询带宽申请记录。
*/
export function getStreamRecord(req: GetStreamRecordRequest, options: any = {}): Promise<GetStreamRecordResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('GetStreamRecord', '/VSP/V3/GetStreamRecord', req, options).then((resp: any) => {
        return safeGetStreamRecordResponse(resp);
    });
}

/**
* 该接口用于客户端发表内容评论。
*/
export function publishComment(req: PublishCommentRequest, options: any = {}): Promise<PublishCommentResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('PublishComment', '/VSP/Comment/PublishComment', req, options).then((resp: any) => {
        return safePublishCommentResponse(resp);
    });
}

/**
* 通过该接口，终端用户删除自己之前发表的评论。
*/
export function maintainComment(req: MaintainCommentRequest, options: any = {}): Promise<MaintainCommentResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('MaintainComment', '/VSP/Comment/MaintainComment', req, options).then((resp: any) => {
        return safeMaintainCommentResponse(resp);
    });
}

/**
* 通过该接口，终端用户查询指定条件的评论列表。
*/
export function queryComments(req: QueryCommentsRequest, options: any = {}): Promise<QueryCommentsResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('QueryComments', '/VSP/Comment/QueryComments', req, options).then((resp: any) => {
        return safeQueryCommentsResponse(resp);
    });
}

/**
* 该接口用于客户端对已有的评论进行举报。
*/
export function reportComment(req: ReportCommentRequest, options: any = {}): Promise<ReportCommentResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('ReportComment', '/VSP/Comment/ReportComment', req, options).then((resp: any) => {
        return safeReportCommentResponse(resp);
    });
}

/**
* 该接口用于客户端对已有的评论点赞。
*/
export function likeComment(req: LikeCommentRequest, options: any = {}): Promise<LikeCommentResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('LikeComment', '/VSP/Comment/LikeComment', req, options).then((resp: any) => {
        return safeLikeCommentResponse(resp);
    });
}

/**
* 该接口用于客户端发表弹幕消息。
*/
export function barrage(req: BarrageRequest, options: any = {}): Promise<BarrageResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('Barrage', '/VSP/Comment/Barrage', req, options).then((resp: any) => {
        return safeBarrageResponse(resp);
    });
}

/**
* 该接口用于客户端查询当前内容在指定时间范围内的弹幕消息列表。
*/
export function getBarrageList(req: GetBarrageListRequest, options: any = {}): Promise<GetBarrageListResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('GetBarrageList', '/VSP/Comment/GetBarrageList', req, options).then((resp: any) => {
        return safeGetBarrageListResponse(resp);
    });
}

/**
* 通过该接口向评论系统维护账号，并设置自己的昵称
*/
export function registerUserInfo(req: RegisterUserInfoRequest, options: any = {}): Promise<RegisterUserInfoResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('RegisterUserInfo', '/VSP/Comment/RegisterUserInfo', req, options).then((resp: any) => {
        return safeRegisterUserInfoResponse(resp);
    });
}

/**
* 客户端需要从VSP获取配置参数时调用此接口。
*/
export function queryCustomizeConfig(req: QueryCustomizeConfigRequest, options: any = {}): Promise<QueryCustomizeConfigResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('QueryCustomizeConfig', '/VSP/V3/QueryCustomizeConfig', req, options).then((resp: any) => {
        return safeQueryCustomizeConfigResponse(resp);
    });
}

/**
* VSP提供查询可用模板的接口，支持查询当前用户可用的所有模板信息。
*/
export function queryTemplate(req: QueryTemplateRequest, options: any = {}): Promise<QueryTemplateResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('QueryTemplate', '/VSP/V3/QueryTemplate', req, options).then((resp: any) => {
        return safeQueryTemplateResponse(resp);
    });
}

/**
* VSP提供地理位置查询接口，通过该接口获取终端所在的地理位置信息。
*/
export function queryLocation(req: QueryLocationRequest, options: any = {}): Promise<QueryLocationResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('QueryLocation', '/VSP/V3/QueryLocation', req, options).then((resp: any) => {
        return safeQueryLocationResponse(resp);
    });
}

/**
* 针对国内固移融合场景，返回固移融合关联内容列表。
*/
export function getRelatedContent(req: GetRelatedContentRequest, options: any = {}): Promise<GetRelatedContentResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('GetRelatedContent', '/VSP/V3/GetRelatedContent', req, options).then((resp: any) => {
        return safeGetRelatedContentResponse(resp);
    });
}

/**
* 终端向HVS平台获取端云协同SDK的License权限。
*/
export function authenticateSDKLicense(req: AuthenticateSDKLicenseRequest, options: any = {}): Promise<AuthenticateSDKLicenseResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('AuthenticateSDKLicense', '/VSP/V3/AuthenticateSDKLicense', req, options).then((resp: any) => {
        return safeAuthenticateSDKLicenseResponse(resp);
    });
}

/**
* 获取Phone或Pad VodHome页面的所有数据；
*
* 子接口
*
* 根据传参rootSubjectID调用QueryVODSubjectList接口（subjectID&#x3D;rootSubjectID,count&#x3D;50,offset&#x3D;0需翻页）获取到栏目列表（subjects.Subject[]）；
*
* 如果传参subjectIDList为空，返回查询到的列表到subjectList；
*
* 如果传参subjectIDList不为空，比较传参的subjectIDList和查询到的subjects.Subject[]比较后以客户端的顺序优先返回到subjectList。
*
* 根据传参subjectID，如果subjectID为非子栏目：调用QuerySubjectVODBySubjectID接口（subjectID&#x3D;subjectID,subjectCount&#x3D;50需翻页， VODCount&#x3D;firstVODCount,offset&#x3D;0）获取子栏目及栏目中VOD列表（subjectVODLists.SubjectVODList[]）。
*
*
*
*  - 如果传参subjectID为空，默认使用排序后的第一个栏目ID；
*
*  - 返回的第一个栏目的内容为banner栏目返回对应条数（bannerCount）到bannerVODs；
*
*  - 去除banner栏目和内容返回到subjectDetailList，第一个栏目的VOD内容条数为firstVODCount，其他栏目的VOD内容条数为VODCount。
*
* 根据传参subjectID，如果subjectID为子栏目：调用QueryVODListBySubject接口（subjectID&#x3D;$subjectID,count&#x3D;$,offset&#x3D;0,sortType&#x3D;STARTTIME:DESC）获取栏目下的VOD列表。
*
* 根据传参genreID调用GetContentConfig接口（types&#x3D;2）获取流派名称列表（genres.Genre[].genreName）。
*/
export function queryVodHomeData(req: QueryVodHomeDataRequest, options: any = {}): Promise<QueryVodHomeDataResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('QueryVodHomeData', '/VSP/V3/QueryVodHomeData', req, options).then((resp: any) => {
        return safeQueryVodHomeDataResponse(resp);
    });
}

/**
* 获取Phone/Pad VodHome页面的所有数据。
*
*
*
*  - 根据客户端本地的栏目顺序获取到栏目列表，如果本地没有顺序，将按服务器的顺序返回。
*
*  - 根据客户端请求的VOD根目录获取根目录下的推荐VOD列表。
*
*  - 根据栏目列表显示前N个栏目的VOD列表。
*
* 子接口
*
* 获取请求参数：
*
* 本地栏目顺序数组（subjectID[]）
*
* 根目录栏目ID（subjectID）
*
* 显示栏目数量（subjectCount）
*
* 显示栏目下的VOD数量（VODCount）
*
*
*
*  - 调用QueryVodSubjectList获取根目录栏目ID（subjectID）下一级子栏目列表，根据传入的本地栏目顺序数组（subjectID[]）参数返回对应顺序的栏目数组；如果为空，直接返回，返回数据为Subject数组。
*
*  - 调用获取VOD排行榜接口（QueryRecmVODList）接口获取根目录栏目ID（subjectID）目录下的周排行前栏目下的VOD数量（VODCount）个，返回数据为VOD数组。
*
*  - 调用QueryVodListBySubject获取前栏目数量（subjectCount）个下的栏目及下VOD列表，返回数据为subjectVODList数组。
*/
export function vodHome(req: VodHomeRequest, options: any = {}): Promise<VodHomeResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('VodHome', '/VSP/V3/VodHome', req, options).then((resp: any) => {
        return safeVodHomeResponse(resp);
    });
}

/**
* 通过VSP的编排能力，分别调用如下接口：
*
*
*
*  - 调用 “获取最热搜索词汇”SearchHotKey接口，获取一个最热门的搜索词汇。
*
*  - 调用“获取栏目下VOD列表”QueryVODListBySubject接口，获取指定栏目下的内容列表。
*
*  - 调用“动态推荐”接口QueryRecmContent，获取用户偏好/混合推荐的VOD列表，用户热门推荐的VOD列表，热门推荐的Catch-up TV列表，编排接口添加入参queryDynamicRecmContents. recmScenarios.entrance为“VOD_Top_Picks_For_You”。
*
*  - 对于everyone is watching，调用QueryRecmVODList获取周排行的VOD，排序方式为按VOD点击率降序排序。
*
*  - 对于HotTV show，调用QueryHotPlaybill获取周排行的Catch-up TV。
*
*  - 对于配置的栏目（通常为多个），调用“获取栏目详情”QuerySubjectDetail和“获取栏目下VOD列表”QueryVODListBySubject接口，获取指定栏目详情以及栏目下的VOD列表。
*
* 最后将这几个接口返回的数据进行组合封装。
*/
export function queryHomeData(req: QueryHomeDataRequest, options: any = {}): Promise<QueryHomeDataResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('QueryHomeData', '/VSP/V3/QueryHomeData', req, options).then((resp: any) => {
        return safeQueryHomeDataResponse(resp);
    });
}

/**
* 调用一次LiveTV首页编排接口，将返回的what&#x27;s on频道推荐，即将播放的节目单推荐，热播栏目的节目单数据填充到页面中。What&#x27;s on需要当前热播频道的频道基本信息和直播的节目单名称，海报等信息；Happynext需要当前即将热播频道的频道基本信息和直播的节目单名称，海报等信息，热播栏目需要返回各个栏目列表以及各个栏目下的频道列表，以及各个频道的当前直播的节目单信息。
*
*
*
*  - 编排接口通过QueryRecmContent获取what&#x27;on的直播推荐和即将热播的推荐，要获取的数目根据入参。直播推荐的entrace填写字符串“Program_Whats_On”，即将热播的推荐的entrance为：“Program_Happy_Next”。
*
* 如果channelSubjectID为空，通过QueryChannelSubjectList获取根栏目下非空的（有频道的）热门栏目的列表，subjectCount根据入参。
*
*
*
*  - &lt;a id&#x3D;&quot;li52942405&quot;&gt;&lt;/a&gt;然后根据上一步的栏目列表通过QueryPlaybillList获取栏目下的频道和节目单列表，频道的count根据入参，节目单入参：type为1，count为1，beginTime和endTime都填写为当前时间，isFillProgram为1。
*
*  - 如果channelSubjectID非空，根据channelSubjectID列表中的栏目ID，通过QueryPlaybillList获取各个栏目的节目单列表，参数要求同[2](#li52942405)的描述。
*
* 最后返回所有推荐数据。
*/
export function queryOTTLiveTVHomeData(req: QueryOTTLiveTVHomeDataRequest, options: any = {}): Promise<QueryOTTLiveTVHomeDataResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('QueryOTTLiveTVHomeData', '/VSP/V3/QueryOTTLiveTVHomeData', req, options).then((resp: any) => {
        return safeQueryOTTLiveTVHomeDataResponse(resp);
    });
}

/**
* 调用一次liveTV编排接口，查询出上图需要平台返回的总数。
*
*
*
*  - 同时调用QueryFavorite，查询当前收藏的VOD总数。内容类型为VOD，count为1。返回的total为收藏的vod总数。
*
*  - 同时调用QuerySubscription查询订购的产品总数，queryType传1。count为1，返回的total为购买的产品总数。
*
*  - 同时调用QueryReminder查询提醒总数。内容类型为节目单，基线queryType不传（临时KPN传1），count为1，返回的total为提醒总数。
*
*  - 同时调用QueryFavorite，查询当前收藏的频道总数。内容类型为频道(AUDIO_CHANNEL，VIDEO_CHANNEL)，count为1。返回的 total为收藏的频道总数。
*
*  - 同时调用QueryProfile，查询所有的profile总数。Type为1所有，count为1。返回的countTotal为profile的总数。
*
*  - 同时调用QueryBookmark，查询最近两部观看的VOD。bookmarkTypes为VOD，sortType为UPDATE_TIME:DESC，count&#x3D;2。返回的countTotal为书签总数，取返回的bookmarks中的vod数组。
*
* 接口将上述查询结果一起返回。
*/
export function queryMyTVHomeData(req: QueryMyTVHomeDataRequest, options: any = {}): Promise<QueryMyTVHomeDataResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('QueryMyTVHomeData', '/VSP/V3/QueryMyTVHomeData', req, options).then((resp: any) => {
        return safeQueryMyTVHomeDataResponse(resp);
    });
}

/**
* 调用一次编排接口，将返回所有的更多推荐的内容。
*
*
*
*  - 编排接口调用QueryBookmark查询最近观看的两个VOD书签内容（最多两部）。
*
*  - 编排接口调用QueryFavorite查询最近收藏的两部VOD内容（最多两部）。
*
*  - 调用QueryRecmContent获取指定数量用户偏好推荐的VOD列表。
*
*  - 根据1获取的书签内容调用QueryRecmContent获取内容相似推荐影片，每一个书签获取指定数量推荐影片。
*
*  - 根据2获取的收藏内容调用QueryRecmContent获取内容协同推荐影片，每一个收藏获取指定数量部推荐影片。
*
*  - 最后返回5组推荐数据，如果书签和收藏不足两部，就根据实际有多少数据返回多少数据。
*
* 特别说明一点，上面的指定数量，可以通过recommendCount参数指定。
*/
export function queryMoreRecommend(req: QueryMoreRecommendRequest, options: any = {}): Promise<QueryMoreRecommendResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('QueryMoreRecommend', '/VSP/V3/QueryMoreRecommend', req, options).then((resp: any) => {
        return safeQueryMoreRecommendResponse(resp);
    });
}

/**
* 通过VSP的编排能力QueryEpgHomeRecmPlaybill，同时调用如下的接口：
*
*
*
*  - 编排接口通过QueryRecmContent获取what’on的直播推荐，内容类型为节目单，recmWhatsOnCount根据入参。entrace填写字符串“Program_Whats_On”。
*
*  - 同时通过QueryRecmContent获取Happy Next的即将热播的推荐，内容类型为节目单，recmHappyNextCount根据入参。entrace填写字符串“Program_Happy_Nex”。
*
*  - 同时调用QueryHotPlaybill接口查询Catch-up TV的榜单，取周排行。
*
* 接口返回上述查询信息。
*/
export function queryEpgHomeRecmPlaybill(req: QueryEpgHomeRecmPlaybillRequest, options: any = {}): Promise<QueryEpgHomeRecmPlaybillResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('QueryEpgHomeRecmPlaybill', '/VSP/V3/QueryEpgHomeRecmPlaybill', req, options).then((resp: any) => {
        return safeQueryEpgHomeRecmPlaybillResponse(resp);
    });
}

/**
* 通过VSP的编排能力QueryEpgHomeVod，同时调用如下的接口：
*
*
*
*  - 通过QueryRecmVODList获取协同推荐everyone is watching的推荐的VOD，取周排行，按VOD点击率降序排序。
*
*  - 同时通过QueryRecmContent获取偏好的VOD（top picks for you）。推荐入参的内容类型和业务类型均为VOD。同时编排接口添加入参queryDynamicRecmContents. recmScenarios.entrance为“VOD_Top_Picks_For_You”。
*
*  - 获取传入参数vodSubjectIDList列表调用QueryVodListBySubject获取对应VOD栏目下的count个VOD内容。
*
* 接口返回上述查询信息。
*/
export function queryEpgHomeVod(req: QueryEpgHomeVodRequest, options: any = {}): Promise<QueryEpgHomeVodResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('QueryEpgHomeVod', '/VSP/V3/QueryEpgHomeVod', req, options).then((resp: any) => {
        return safeQueryEpgHomeVodResponse(resp);
    });
}

/**
* 通过VSP的编排能力QueryEpgHomeSubjects，同时调用如下的接口：
*
*
*
*  - 通过QueryVODSubjectList获取VOD的栏目列表，入参栏目类型为VOD，VOD的栏目ID和获取的子栏目的数目根据入参填写。
*
*  - 同时通过QueryChannelSubjectList获取频道热门栏目的列表，频道栏目ID和获取的子栏目的数目根据入参填写。
*
* 接口返回上述查询信息。
*/
export function queryEpgHomeSubjects(req: QueryEpgHomeSubjectsRequest, options: any = {}): Promise<QueryEpgHomeSubjectsResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('QueryEpgHomeSubjects', '/VSP/V3/QueryEpgHomeSubjects', req, options).then((resp: any) => {
        return safeQueryEpgHomeSubjectsResponse(resp);
    });
}

/**
* NA
*/
export function queryVodSubjectAndVodList(req: QueryVodSubjectAndVodListRequest, options: any = {}): Promise<QueryVodSubjectAndVodListResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('QueryVodSubjectAndVodList', '/VSP/V3/QueryVodSubjectAndVodList', req, options).then((resp: any) => {
        return safeQueryVodSubjectAndVodListResponse(resp);
    });
}

/**
* subjectID:VOD根栏目ID
*
* bannerSubjectID：banner栏目ID
*
* bannerVODCount：banner VOD显示数
*
* recmSubjectIDList：推荐栏目ID列表
*
* recmVODCount：推荐VOD显示数
*
* castIDList：推荐演员ID列表
*
* castVODCount：演职员VOD显示数
*
*
*
*  - 根据VOD根栏目ID（subjectID）调用获取VOD栏目列表（QueryVODSubjectList）接口获取到VOD根目录的下一级栏目列表；封装返回值为subjectList。
*
*  - 根据banner栏目ID（bannerSubjectID）和banner VOD显示数（bannerVODCount）调用获取栏目下VOD列表（QueryVODListBySubject）接口获取banner目录下的一定数量VOD列表；封装返回值为bannerVODs。
*
*  - 根据推荐栏目ID列表（recmSubjectIDList）和推荐VOD显示数（recmVODCount）调用获取栏目下VOD列表（QueryVODListBySubject）接口获取banner目录下的一定数量VOD列表；封装返回值为recmSubjectDetailList。
*
*  - 根据推荐演员ID列表（castIDList）和演职员VOD显示数（castVODCount）调用搜索内容（SearchContent）接口获取VOD列表；封装返回值为castContentsList。
*/
export function queryPCVodHome(req: QueryPCVodHomeRequest, options: any = {}): Promise<QueryPCVodHomeResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('QueryPCVodHome', '/VSP/V3/QueryPCVodHome', req, options).then((resp: any) => {
        return safeQueryPCVodHomeResponse(resp);
    });
};

export function queryVODListStcPropsBySubject (req, options: any = {} ): Promise<any> {
    options.method = options.method || 'POST';
    return _sendRequest('QueryVODListStcPropsBySubject', '/VSP/V3/QueryVODListStcPropsBySubject', req, options)
    .then((resp: any) => {
        return resp;
    });
}

export function queryPCVodHomeCategories(req?: QueryPCVodHomeRequest, options: any = {}): Promise<QueryPCVodHomeResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('QueryPCVodHome', '/VSP/V3/QueryPCVodHome', req || {subjectID: 'all'}, options).then((resp: any) => {
        return safeQueryPCVodHomeResponse(resp);
    });
};

/**
* 根据PHM动态绑定的数据类型，组合成批量的req[]，发送给平台，平台查询完成后，一次性返回所有的数据；
*
* 目前动态桌面支持的动态绑定数据类型如下：
*
* 1：获取VOD栏目列表
*
* 调用的VSP接口：QueryVODSubjectList,QuerySubjectDetail
*
* 2：获取频道栏目列表
*
* 调用的VSP接口：QueryChannelSubjectList,QuerySubjectDetail
*
* 3：获取栏目下VOD列表
*
* 调用的VSP接口：QueryVODListBySubject，QuerySubjectDetail
*
* 4：获取栏目下频道列表
*
* 调用的VSP接口：QueryChannelListBySubject,QuerySubjectDetail
*
* 5：获取用户协同推荐VOD列表
*
* 调用的VSP接口：QueryRecmContent
*
* 6：查询VOD排行榜
*
* 调用的VSP接口：QueryRecmVODList,QuerySubjectDetail
*
* 7：获取用户偏好推荐VOD列表
*
* 调用的VSP接口：QueryRecmContent
*
* 8：获取用户偏好推荐TVOD列表
*
* 调用的VSP接口：QueryHotPlaybill
*
* 9：获取正在热播直播节目单推荐
*
* 调用的VSP接口：QueryRecmContent
*
* 10：获取即将播放节目单推荐
*
* 调用的VSP接口：QueryRecmContent
*
* 11：获取用户播放VOD历史
*
* 调用的VSP接口：QueryBookmark
*
* 12：获取用户VOD收藏
*
* 调用的VSP接口：QueryFavorite
*
* 13：获取用户channel收藏
*
* 调用的VSP接口：QueryFavorite
*
* 14：查询VOD强档推荐
*
* 调用的VSP接口：QueryRecmVODList,QuerySubjectDetail
*
* 平台根据HomeParam []，依次执行请求获取响应和，组合返回给客户端。HomeParam []与HomeData []一一对应。
*/
export function queryAllHomeData(req: QueryAllHomeDataRequest, options: any = {}): Promise<QueryAllHomeDataResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('QueryAllHomeData', '/VSP/V3/QueryAllHomeData', req, options).then((resp: any) => {
        return safeQueryAllHomeDataResponse(resp);
    });
}



export interface AggregationAuthorizeObject {
    /**
     * 内容ID。
     */
    contentID: string;
    /**
     * 节目单ID。
     *
     * 如果播放直播业务，终端未上报节目单ID，VSP查询当前时间的直播节目单，如果存在直播节目单，VSP进行节目单的授权检查。
     *
     * 播放回看业务时必填。
     */
    playbillID?: string;
    /**
     * 媒资ID。
     */
    mediaID: string;
    /**
     * 鉴权的业务类型：
     *
     * 取值包括：
     *
     * BTV: 直播
     *
     * PLTV: 网络时移
     *
     * CUTV: 回看
     *
     * PREVIEW: 直播预览
     *
     * IR: 即时重放
     */
    businessType: string;
    /**
     * 扩展信息。
     */
    extensionFields: NamedParameter[];
}

function safeAggregationAuthorizeObject(v: AggregationAuthorizeObject): AggregationAuthorizeObject {
    if (v) {
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeAggregationAuthorizeObjects(vs: AggregationAuthorizeObject[]): AggregationAuthorizeObject[] {
    if (vs) {
        return vs.map((v: AggregationAuthorizeObject) => {
            return safeAggregationAuthorizeObject(v);
        });
    } else {
        return [];
    }
}

export interface AuthenticateBasic {

    /**
    * 用户登录终端时输入的用户唯一标识，VSP平台根据此字段可以获取用户归属的订户，目前VSP平台支持以下几种取值：
    *
    * 订户ID
    *
    * 逻辑设备ID
    *
    * Profile的loginName，loginName属性说明请参见2.83 Profile。
    */
    userID?: string;

    /**
    * 终端输入的userID对应的用户标识类型，取值包括：
    *
    * 0：订户ID，此时只校验订户密码(去掉主Profile密码的校验)。
    *
    * 1：Profile的loginName，此时只校验User Profile密码。
    *
    * 2：逻辑设备ID，此时只校验逻辑设备密码
    *
    * 3：Guest用户，Guest用户认证可以不携带userID，但是如果终端上报了，VSP会以终端上报的为准。
    */
    userType: string;

    /**
    * 认证方式，取值包括：
    *
    * 0：基于密码认证，密码类型可以是订户密码、设备密码、Profile密码以及第三方认证系统分配的密码。
    *
    * 1：免密登录用户名/密码认证（免密登录初次用户名/密码登录）。
    *
    * 2：免密登录认证（免密登录再次登录认证）。
    *
    * 注意
    *
    * authType为2的时候，authToken为必填参数。
    *
    * authType为1或2的时候，userID为loginName必传，userType为1。
    */
    authType?: string;

    /**
    * 终端保存的免密登录流程所需的认证token。
    *
    * 当authType值为2时，此字段必填。
    */
    authToken?: string;

    /**
    * 由终端传入密码。
    */
    clientPasswd?: string;

    /**
    * 终端所属时区。
    *
    * 支持夏令时的终端，格式类似Asia/Shanghai。
    *
    * 仅支持多时区无夏令的终端，格式可以类似Asia/Shanghai或者GMT+03:30。
    */
    timeZone?: string;

    /**
    * 默认频道查询使用的命名空间，如果不指定，VSP将按照终端型号绑定的频道命名空间处理。
    */
    channelNamespace?: string;

    /**
    * OTT设备的网络接入方式，取值包括：
    *
    * 1：WLAN
    *
    * 2：Cellular
    *
    * 默认值为1。
    */
    connectType?: string;

    /**
    * 用户所属领域，多值使用英文逗号分隔，取值范围：
    *
    * 0：IPTV
    *
    * 1：MTV
    *
    * 2：WebTV
    *
    * 默认值为0。
    *
    * 说明
    *
    * 该参数仅国内局点支持。
    */
    bizdomain?: string;

    /**
    * 终端和用户网关(VSP)支持数据防篡改时，上报preSharedKey的编号。
    *
    * 说明
    *
    * 数据防篡改的配置请参见VSP的VSC配置项antiTamperEnable，取值范围如下：
    *
    * No
    *
    * Yes
    *
    * 默认值为No。
    *
    * VSP平台的系统参数配置请参考《 产品文档》，可以从Support官方网站获取对应版本的产品文档资料。
    */
    preSharedKeyID?: string;

    /**
    * 终端生成的随机字符串，用于生成敏感数据加密密钥。
    *
    * 终端和用户网关(VSP) Server之间传输敏感数据时，不能明文传输，需要采用此加密密钥加密后传输，VSP保存该加密密钥，后续对敏感数据进行加密和解密时使用。
    */
    cnonce?: string;

    /**
    * 表示接入用户页面轨迹采集SDK的应用UI类型，对华为UI，默认数值如：HW_STB_VSPUI、HW_PC、HW_iPhone、HW_iPad、HW_AndroidPhone、HW_AndroidPad；对第三方UI，页面上操作员填写名称，如Youpeng_VSPUI。具体数值由多屏UI接入用户页面轨迹采集SDK时在网管进行统一分配。
    *
    * 对应[PageTracker](#_ZH-CN_TOPIC_0055055677)中的type。
    */
    pageTrackerUIType?: string;

    /**
    * 当前终端页面显示内容的语种属性。
    *
    * 如果语种是订户共享的且语种还未设置，VSP将终端上报的locale作为订户语种。
    *
    * 采用统一为ISO639-1缩写，如en。
    */
    lang?: string;

    /**
    * 终端支持图片格式
    *
    * 0：返回当前gif、png、jpg格式图片
    *
    * 1：只返回Webp格式图片
    *
    * 2：两种格式文件均返回
    *
    * 默认为0。
    */
    isSupportWebpImgFormat?: string;

    /**
    * 海报类型的枚举数组，具体取值如下：
    *
    * 0：缩略图
    *
    * 1 ：海报
    *
    * 2 ：剧照
    *
    * 3 ：图标
    *
    * 4 ：标题图
    *
    * 5 ：广告图
    *
    * 6 ：草图
    *
    * 7 ：背景图
    *
    * 9 ：频道图片
    *
    * 10： 频道黑白图片
    *
    * 12 ：频道名字图片
    */
    needPosterTypes?: string[];

    /**
    * 是否将该profile设置为默认profile
    *
    * 0：否
    *
    * 1：是
    *
    * 默认值为0。
    *
    * 如果接口请求中不传入该参数，则表示VSP平台保持当前设备上的默认profile不变化。
    */
    asDefaultProfile?: string;

    /**
    * 扩展信息。
    *
    * 福建移动通过扩展字段clientID,带上客户端标识可用于免绑定场景；
    */
    extensionFields?: NamedParameter[];
}

function safeAuthenticateBasic(v: AuthenticateBasic): AuthenticateBasic {
    if (v) {
        v.needPosterTypes = v.needPosterTypes || [];
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeAuthenticateBasics(vs: AuthenticateBasic[]): AuthenticateBasic[] {
    if (vs) {
        return vs.map((v: AuthenticateBasic) => {
            return safeAuthenticateBasic(v);
        });
    } else {
        return [];
    }
}

export interface AuthenticateDevice {

    /**
    * 终端MAC地址，此属性不仅标识物理地址也用于CA设备。
    *
    * 对于STB终端，取值必须填写STB设备的MAC地址
    *
    * 对于支持播放功能的OTT设备，取值必须填写为CA客户端插件的caDeviceId（如VMX插件的caDeviceId）。
    *
    * 说明
    *
    * 对于登录自助服务Portal的场景，终端以不绑定设备的方式登录，不需要填写此参数。
    */
    physicalDeviceID?: string;

    /**
    * 终端的唯一标识。
    */
    terminalID?: string;

    /**
    * STB的设备外部编号。
    */
    STBSN?: string;

    /**
    * 终端型号。对于需要绑定设备登录的场景，此参数必填。对于自助服务Portal方式登录，不需填写。
    *
    * VSP根据终端型号从配置的终端型号中查找确定终端所属领域。用户可以在VMPortal中的System-&gt;DeviceModel菜单查看对应的系统配置。
    */
    deviceModel?: string;

    /**
    * 设备厂商型号，该参数由终端上报。比如iPhone对应的deviceModel是OTT，terminalVendor是iPhone 4S。
    *
    * 当使用SilverLight注册时填写Silverlight+版本号，如Silverlight4.1。
    */
    terminalVendor?: string;

    /**
    * 终端操作系统版本号，该参数由终端上报。
    */
    OSVersion?: string;

    /**
    * STB版本，该参数由终端上报。
    *
    * 用户网关(VSP)服务器根据该字段找到用户匹配的VSP模板，只有STB终端需要，其他终端不需要携带该参数。
    */
    STBVersion?: string;

    /**
    * 终端应用的软件版本号，该参数由终端上报。
    */
    softwareVersion?: string;

    /**
    * CA设备信息。
    *
    * 如果不携带该参数，默认为Verimatrix CA设备。在AuthenticateDevice对象中，本字段对应的CADeviceInfo对象中的参数有两点特殊说明如下：
    *
    * caDeviceId默认为设备的mac地址（mac参数）。
    *
    * caDeviceType由VSP根据终端型号（deviceModel参数）识别后确定。
    */
    CADeviceInfos?: CADeviceInfo[];

    /**
    * 终端的唯一标识，免密登录场景下必传。该参数是终端自带的参数。
    */
    authTerminalID?: string;

    /**
    * 设备向Verimatrix或MultiDRM注册后返回的ID，针对Verimatrix该字段仅在Verimatrix3.8及以后的版本中支持。
    */
    VUID?: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeAuthenticateDevice(v: AuthenticateDevice): AuthenticateDevice {
    if (v) {
        v.CADeviceInfos = safeCADeviceInfos(v.CADeviceInfos);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeAuthenticateDevices(vs: AuthenticateDevice[]): AuthenticateDevice[] {
    if (vs) {
        return vs.map((v: AuthenticateDevice) => {
            return safeAuthenticateDevice(v);
        });
    } else {
        return [];
    }
}

export interface AuthenticateTolerant {

    /**
    * 区域的外部编号。
    *
    * 首次认证时，不需要携带该参数。
    *
    * 用户认证成功后需要将认证响应中的areaCode写入终端，并在后续认证请求中传给VSP服务器，以便Envision Video Platform容灾后（如数据库故障，VSP服务器宕机），仍然能够为用户提供服务。
    */
    areaCode?: string;

    /**
    * 模板名称。
    *
    * 首次认证时，不需要携带该参数。
    *
    * 用户认证成功后需要将认证响应中的templateName写入终端，并在后续认证请求中传给用户网关(VSP)服务器，以便Envision Video Platform容灾后（如数据库故障，VSP服务器宕机），仍然能够为用户提供服务。
    *
    * 如果终端不需要VSP平台提供模板管理功能，忽略此参数。
    */
    templateName?: string;

    /**
    * 用户组ID。
    *
    * 首次认证时，不需要携带该参数。
    *
    * 用户认证成功后需要将认证响应中的usergroup写入终端，并在后续认证请求中传给用户网关(VSP)服务器，以便Envision Video Platform容灾后（如数据库故障，VSP服务器宕机），仍然能够为用户提供服务。
    */
    userGroup?: string;

    /**
    * 订户归属的子网运营商ID。
    *
    * 首次认证时，不需要携带该参数。
    *
    * 用户认证成功后需要将认证响应中的subnetId写入终端，并在后续认证请求中传给用户网关(VSP)服务器，以便Envision Video Platform容灾后（如数据库故障，VSP服务器宕机），仍然能够为用户提供服务。
    */
    subnetID?: string;

    /**
    * 订户归属的BOSS编号。
    *
    * 首次认证时，不需要携带该参数。
    *
    * 用户认证成功后需要将认证响应中的bossID写入终端，并在后续认证请求中传给用户网关(VSP)服务器，以便Envision Video Platform容灾后（如数据库故障，VSP服务器宕机），仍然能够为用户提供服务。
    */
    bossID?: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeAuthenticateTolerant(v: AuthenticateTolerant): AuthenticateTolerant {
    if (v) {
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeAuthenticateTolerants(vs: AuthenticateTolerant[]): AuthenticateTolerant[] {
    if (vs) {
        return vs.map((v: AuthenticateTolerant) => {
            return safeAuthenticateTolerant(v);
        });
    } else {
        return [];
    }
}

export interface AuthorizeResult {

    /**
    * 如果鉴权成功，返回订购的产品ID。
    */
    productID?: string;

    /**
    * 如果PlayVOD和PlayChannel接口入参isReturnProduct指定要返回内容定价的产品，返回产品列表，默认按照产品价格升序排列。
    */
    pricedProducts?: Product[];

    /**
    * 返回用户已订购但是未生效的内容定价的产品。
    */
    unenforcedProducts?: Subscription[];

    /**
    * 如果鉴权的内容/媒资依赖其他业务且被依赖的业务没有订购，返回被依赖的业务的可订购产品列表。
    *
    * 该参数当前不支持。预留。
    */
    reliantProducts?: Product[];

    /**
    * 如果内容已订购，但是内容定价的产品限制条件检查失败，返回产品的限制条件。
    */
    productRestriction?: ProductRestriction;

    /**
    * 如果接口要求检查锁和父母字，返回内容是否被加锁，取值包括：
    *
    * -1：未知
    *
    * 0：未加锁
    *
    * 1：已加锁
    *
    * 如果终端不做父母字和锁检查，返回-1。
    */
    isLocked: string;

    /**
    * 如果接口要求检查锁和父母字，返回内容是否被父母字控制，取值包括：
    *
    * -1：未知
    *
    * 0：未控制
    *
    * 1：已控制
    *
    * 如果终端不做父母字和锁检查，返回-1。
    */
    isParentControl: string;

    /**
    * 如果内容被加锁或者被父母字控制，返回受限的内容ID。
    */
    contentID?: string;

    /**
    * 如果内容被加锁或者被父母字控制，返回受限的内容类型，取值包括：
    *
    * VOD：点播
    *
    * CHANNEL：频道
    *
    * PROGRAM：节目单
    *
    * SUBJECT：栏目
    */
    contentType?: string;

    /**
    * 客户端集成的是PlayReady，下发获取CA License的触发器。
    */
    triggers?: GetLicenseTrigger[];
}

function safeAuthorizeResult(v: AuthorizeResult): AuthorizeResult {
    if (v) {
        v.pricedProducts = safeProducts(v.pricedProducts);
        v.unenforcedProducts = safeSubscriptions(v.unenforcedProducts);
        v.reliantProducts = safeProducts(v.reliantProducts);
        v.productRestriction = safeProductRestriction(v.productRestriction);
        v.triggers = safeGetLicenseTriggers(v.triggers);
    }
    return v;
}

function safeAuthorizeResults(vs: AuthorizeResult[]): AuthorizeResult[] {
    if (vs) {
        return vs.map((v: AuthorizeResult) => {
            return safeAuthorizeResult(v);
        });
    } else {
        return [];
    }
}

export interface AggregationAuthorizeResult {
    /**
     * 频道ID。
     */
    channelID: string;
    /**
     * 频道媒资ID。
     */
    mediaID: string;
    /**
     * 是否鉴权成功。
     *
     * 取值包括：
     *
     * 0: 鉴权失败。直播鉴权接口中返回失败后该值为0。
     *
     * 1: 鉴权成功。直播鉴权接口中返回成功后该值为1。
     */
    isAuthorizedSuccess: string;
    /**
     * 该内容是否已订购并且有效。
     *
     * isAuthorizedSuccess为1时必填。
     *
     * 取值包括：
     *
     * 0: 其他情况
     *
     * 1: 该内容已订购并且有效
     */
    isOrdered?: string;
    /**
     * isOrdered=1，当前用户是否可以使用此业务。
     *
     * isAuthorizedSuccess为0时必填。
     *
     * 取值包括：
     *
     * 0: 不能使用
     *
     * 1: 可以使用
     */
    isValid?: string;
    /**
     * 如果接口要求检查锁和父母字，返回内容是否被加锁。
     *
     * 如果终端不做父母字和锁检查，返回-1。
     *
     * isAuthorizedSuccess为1时必填。
     *
     * 取值包括：
     *
     * -1: 未知
     *
     * 0: 未加锁
     *
     * 1: 已加锁
     */
    isLocked?: string;
    /**
     * 如果接口要求检查锁和父母字，返回内容是否被父母字控制。
     *
     * 如果终端不做父母字和锁检查，返回-1。
     *
     * isAuthorizedSuccess为1时必填。
     *
     * 取值包括：
     *
     * -1: 未知
     *
     * 0: 未控制
     *
     * 1: 已控制
     */
    isParentControl?: string;
    /**
     * 扩展信息。
     */
    extensionFields?: NamedParameter[];
}

function safeAggregationAuthorizeResult(v: AggregationAuthorizeResult): AggregationAuthorizeResult {
    if (v) {
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeAggregationAuthorizeResults(vs: AggregationAuthorizeResult[]): AggregationAuthorizeResult[] {
    if (vs) {
        return vs.map((v: AggregationAuthorizeResult) => {
            return safeAggregationAuthorizeResult(v);
        });
    } else {
        return [];
    }
}

export interface BarrageFilter {

    /**
    * UTC时间，格式yyyymmddHHMMSS，用户发表的弹幕的起始时间
    */
    barrageBeginTime?: string;

    /**
    * UTC时间，格式yyyymmddHHMMSS，用户发表的弹幕的截止时间。
    *
    * 与上一个参数一起，可以用于过滤一周内/一月内的弹幕
    */
    barrageEndTime?: string;

    /**
    * 评论的内容的编号
    */
    contentCode?: string;

    /**
    * 评论的内容类型
    */
    contentType?: string;

    /**
    * UTC时间，格式yyyymmddHHMMSS，基于内容发表时，内容播放的起始时间。
    */
    beginTimestamp?: string;

    /**
    * UTC时间，格式yyyymmddHHMMSS，基于内容发表弹幕时，内容播放的截止时间。
    *
    * 与上个参数一起，可查询内容播放一段时间范围内的弹幕
    */
    endTimestamp?: string;

    /**
    * TIME_ASC: 按照发表时间升序排列
    */
    sortType?: string;
}

function safeBarrageFilter(v: BarrageFilter): BarrageFilter {
    if (v) {
    }
    return v;
}

function safeBarrageFilters(vs: BarrageFilter[]): BarrageFilter[] {
    if (vs) {
        return vs.map((v: BarrageFilter) => {
            return safeBarrageFilter(v);
        });
    } else {
        return [];
    }
}

export interface BarrageObject {

    /**
    * 弹幕ID
    */
    barrageID: string;

    /**
    * 弹幕内容
    */
    barrage: string;

    /**
    * 评论的内容的编号
    */
    contentCode: string;

    /**
    * 评论的内容的类型
    */
    contentType: string;

    /**
    * UTC时间，格式yyyymmddHHMMSS，基于内容发表弹幕时，内容播放的时间。
    *
    * 如果是点播，实际只会是yyyymmdd填写为0，只有后面的HHMMSS有效
    */
    timestamp: string;

    /**
    * UTC时间，格式yyyymmddHMMSS，用户发表弹幕的时间。
    */
    barrageTime: string;

    /**
    * 作者的订户ID
    */
    userID: string;

    /**
    * 者账号序号
    */
    profileSN?: string;

    /**
    * 扩展信息。
    */
    extendProperties?: NamedParameter[];
}

function safeBarrageObject(v: BarrageObject): BarrageObject {
    if (v) {
        v.extendProperties = safeNamedParameters(v.extendProperties);
    }
    return v;
}

function safeBarrageObjects(vs: BarrageObject[]): BarrageObject[] {
    if (vs) {
        return vs.map((v: BarrageObject) => {
            return safeBarrageObject(v);
        });
    } else {
        return [];
    }
}

export interface Bookmark {

    /**
    * 书签类型，取值包括：
    *
    * VOD：点播书签
    *
    * PROGRAM：回看节目单书签
    *
    * NPVR：NPVR书签
    *
    * CPVR：CPVR书签
    */
    bookmarkType: string;

    /**
    * 书签内容ID。
    *
    * 如果是VOD书签，取值为VOD内容ID。
    *
    * 如果是PROGRAM书签，取值为节目单ID。
    *
    * 如果是NPVR或者CPVR书签，取值是录制计划ID。
    */
    itemID: string;

    /**
    * 书签时间点，也就是中途退出播放的断点距离节目开始的秒数。
    */
    rangeTime: string;

    /**
    * 如果是VOD书签且itemID是连续剧，此属性表示子集ID。
    */
    subContentID?: string;

    /**
    * subContentID对应的内容类型，取值包括：
    *
    * VOD：点播
    *
    * 说明
    *
    * 如果终端请求包含subContentID，subContentType也必须同时携带。
    *
    * 平台后续会增加一种特殊的VAS书签，此VAS会关联多个VOD、频道和节目单，当用户播放VAS下的VOD、频道或者节目单时中途退出，平台会保存此中途退出的内容，所以subContentType会为此书签预留，目前只存在VOD。
    */
    subContentType?: string;

    /**
    * 书签更新时间，取值为距离1970年1月1号的毫秒数。
    *
    * 此参数不需要终端设置，当终端查询书签时，VSP会返回。
    */
    updateTime?: string;

    /**
    * 返回subContentID的集号。
    *
    * 此参数不需要终端设置，当终端查询连续剧书签时，VSP会返回。
    */
    sitcomNO?: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeBookmark(v: Bookmark): Bookmark {
    if (v) {
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeBookmarks(vs: Bookmark[]): Bookmark[] {
    if (vs) {
        return vs.map((v: Bookmark) => {
            return safeBookmark(v);
        });
    } else {
        return [];
    }
}

export interface BookmarkItem {

    /**
    * 书签类型，取值包括：
    *
    * VOD：点播书签
    *
    * PROGRAM：回看节目单书签
    *
    * NPVR：nPVR书签
    *
    * CPVR：CPVR书签
    */
    bookmarkType: string;

    /**
    * VOD内容描述，bookmarkType等于VOD时返回。
    */
    VOD?: VOD;

    /**
    * program内容描述，
    *
    * bookmarkType等于PROGRAM时返回。
    */
    playbill?: Playbill;

    /**
    * NPVR书签，则返回Bookmark对象，具体参数参见对象的定义。
    */
    npvrBookmark?: Bookmark;

    /**
    * CPVR书签，则返回Bookmark对象，具体参数参见对象的定义。
    */
    cpvrBookmark?: Bookmark;
}

function safeBookmarkItem(v: BookmarkItem): BookmarkItem {
    if (v) {
        v.VOD = safeVOD(v.VOD);
        v.playbill = safePlaybill(v.playbill);
        v.npvrBookmark = safeBookmark(v.npvrBookmark);
        v.cpvrBookmark = safeBookmark(v.cpvrBookmark);
    }
    return v;
}

function safeBookmarkItems(vs: BookmarkItem[]): BookmarkItem[] {
    if (vs) {
        return vs.map((v: BookmarkItem) => {
            return safeBookmarkItem(v);
        });
    } else {
        return [];
    }
}

export interface BookmarkFilter {

    /**
    * 书签的内容观看级别。
    */
    ratingID?: string;
}

function safeBookmarkFilter(v: BookmarkFilter): BookmarkFilter {
    if (v) {
    }
    return v;
}

function safeBookmarkFilters(vs: BookmarkFilter[]): BookmarkFilter[] {
    if (vs) {
        return vs.map((v: BookmarkFilter) => {
            return safeBookmarkFilter(v);
        });
    } else {
        return [];
    }
}

export interface BrotherSeasonVOD {

    /**
    * 季播剧。
    */
    VOD: VOD;

    /**
    * 该季播剧是连续剧的第几季。
    */
    sitcomNO: string;
}

function safeBrotherSeasonVOD(v: BrotherSeasonVOD): BrotherSeasonVOD {
    if (v) {
        v.VOD = safeVOD(v.VOD);
    }
    return v;
}

function safeBrotherSeasonVODs(vs: BrotherSeasonVOD[]): BrotherSeasonVOD[] {
    if (vs) {
        return vs.map((v: BrotherSeasonVOD) => {
            return safeBrotherSeasonVOD(v);
        });
    } else {
        return [];
    }
}

export interface CADeviceInfo {

    /**
    * CA设备类型。
    *
    * 取值范围：
    *
    * 1：DTV 硬卡STB
    *
    * 2：DTV 软卡STB
    *
    * 3：IPTV STB
    *
    * 4：OTT STB
    *
    * 5：HLS OTT client
    *
    * 6：Playready OTT client
    *
    * 7：Widevine OTT client
    */
    CADeviceType: string;

    /**
    * CA设备编号。
    *
    * 对于非高安STB，取值是STB的MAC地址。
    *
    * 对于高安STB，取值是STB的ChipSeriaNumber。
    */
    CADeviceID?: string;

    /**
    * 设备ID签名。
    *
    * 该参数不为空，即表示终端和VSP平台进行双向认证，该参数用于传输ChipsetID签名。
    */
    CADeviceIDSignature?: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeCADeviceInfo(v: CADeviceInfo): CADeviceInfo {
    if (v) {
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeCADeviceInfos(vs: CADeviceInfo[]): CADeviceInfo[] {
    if (vs) {
        return vs.map((v: CADeviceInfo) => {
            return safeCADeviceInfo(v);
        });
    } else {
        return [];
    }
}

export interface CAInfo {

    /**
    * Verimatrix的加密信息。
    */
    verimatrix?: Verimatrix;

    /**
    * PlayReady的加密信息。
    */
    playReady?: Playready;

    /**
    * Nagra的加密信息
    */
    nagra?: Nagra;
}

function safeCAInfo(v: CAInfo): CAInfo {
    if (v) {
        v.verimatrix = safeVerimatrix(v.verimatrix);
        v.playReady = safePlayready(v.playReady);
        v.nagra = safeNagra(v.nagra);
    }
    return v;
}

function safeCAInfos(vs: CAInfo[]): CAInfo[] {
    if (vs) {
        return vs.map((v: CAInfo) => {
            return safeCAInfo(v);
        });
    } else {
        return [];
    }
}

export interface CancelSubscribe {

    /**
    * 产品ID。
    */
    productID: string;

    /**
    * 取消类型。
    *
    * 取值范围：
    *
    * 0：退续订（退订，下周期生效）
    *
    * 1：立即退订
    *
    * 2：退续订（退订，月末生效）
    *
    * 说明
    *
    * 客户端需根据局点产品退订的类型选择cancelType的具体取值。
    */
    cancelType: string;

    /**
    * 如果退订按次产品，需要传入待退订的定价对象。
    *
    * 说明
    *
    * 产品数据模型请参见16.3 内容模型。
    */
    priceObject?: PriceObject;

    /**
    * 订购的开始时间，退订PPV产品时必须传入，如果支持分时段退订包月产品也需要携带。
    *
    * 取值为距离1970年1月1号的毫秒数。
    *
    * 说明
    *
    * 分时段包月产品，即多个时段的包月产品。
    */
    startTime?: string;

    /**
    * 退订非共享产品绑定的逻辑设备ID，来自Product的deviceId属性，如果不携带，VSP默认退订当前设备的非共享产品。
    *
    * 说明
    *
    * 对于共享产品该参数将不生效。
    *
    * 共享产品，即用户订购了一个产品，在多个终端上均可以进行观看产品包含的内容。
    *
    * 非共享产品，即用户在某个终端上订购了该内容，只能在本终端观看，不能跨终端观看该内容。
    */
    deviceID?: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeCancelSubscribe(v: CancelSubscribe): CancelSubscribe {
    if (v) {
        v.priceObject = safePriceObject(v.priceObject);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeCancelSubscribes(vs: CancelSubscribe[]): CancelSubscribe[] {
    if (vs) {
        return vs.map((v: CancelSubscribe) => {
            return safeCancelSubscribe(v);
        });
    } else {
        return [];
    }
}

export interface Cast {

    /**
    * 演职员编号。
    */
    castID: string;

    /**
    * 演职员名称。
    */
    castName: string;

    /**
    * 第三方系统分配的Code。
    */
    castCode?: string;

    /**
    * 演职员图片。
    */
    picture?: Picture;

    /**
    * 扩展字段。
    */
    extensionFields?: NamedParameter[];
}

function safeCast(v: Cast): Cast {
    if (v) {
        v.picture = safePicture(v.picture);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeCasts(vs: Cast[]): Cast[] {
    if (vs) {
        return vs.map((v: Cast) => {
            return safeCast(v);
        });
    } else {
        return [];
    }
}

export interface CastRole {

    /**
    * 角色类型，取值包括：
    *
    * 0：演员
    *
    * 1：导演
    *
    * 2：词曲作者
    *
    * 3：演唱者
    *
    * 4：出品人
    *
    * 5：编剧
    *
    * 6：解说员
    *
    * 7：主办人
    *
    * 8：化妆师
    *
    * 9：音响师
    *
    * 100：其他
    *
    * 说明
    *
    * 对于MV类的视频VOD，Cast只支持singer，而且singer对应的roleType为0。
    *
    * 对于音频VOD的Cast也只支持singer，singer对应的roleType为3，终端在使用该参数时需要注意。
    *
    * roleType为7，8和9目前仅节目单支持。
    */
    roleType: string;

    /**
    * 艺术家信息。
    */
    casts: Cast[];
}

function safeCastRole(v: CastRole): CastRole {
    if (v) {
        v.casts = safeCasts(v.casts);
    }
    return v;
}

function safeCastRoles(vs: CastRole[]): CastRole[] {
    if (vs) {
        return vs.map((v: CastRole) => {
            return safeCastRole(v);
        });
    } else {
        return [];
    }
}

export interface Channel {

    /**
    * 频道的内容ID。
    */
    ID: string;

    /**
    * 第三方系统分配的内容Code。
    */
    code?: string;

    /**
    * 关联内容唯一标识
    */
    rMediaCode?: string;

    /**
    * 频道的内容名称。
    */
    name: string;

    /**
    * 频道类型，取值包括：
    *
    * AUDIO_CHANNEL
    *
    * VIDEO_CHANNEL
    */
    contentType: string;

    /**
    * 返回SP操作员设置的系统频道号。
    *
    * 如果是根据频道ID获取频道信息，而且一个逻辑频道有多个频道号的话，channelNO随机返回一个。
    */
    channelNO: string;

    /**
    * 频道Logo。
    */
    logo?: ChannelLogo;

    /**
    * 频道海报路径。
    *
    * 参考“2.65 Picture”
    */
    picture?: Picture;

    /**
    * 如果频道被收藏，返回收藏记录。
    */
    favorite?: Favorite;

    /**
    * 物理频道属性集合。
    */
    channelSet?: PhysicalChannelSet;

    /**
    * 当前的节目单，取频道下当前时间的节目单；如果当前没有节目单则返回填充节目单。
    */
    currentPlaybill?: PlaybillLite;

    /**
    * 推荐理由。
    *
    * 说明
    *
    * 该字段在对接推荐系统时返回，如推荐系统未返回则为空。
    */
    recmExplain?: string;

    /**
    * 内容地理版权信息，两位的国家码(遵从ISO 3166-1)。
    */
    locationCopyrights?: string[];

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeChannel(v: Channel): Channel {
    if (v) {
        v.logo = safeChannelLogo(v.logo);
        v.picture = safePicture(v.picture);
        v.favorite = safeFavorite(v.favorite);
        v.channelSet = safePhysicalChannelSet(v.channelSet);
        v.currentPlaybill = safePlaybillLite(v.currentPlaybill);
        v.locationCopyrights = v.locationCopyrights || [];
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeChannels(vs: Channel[]): Channel[] {
    if (vs) {
        return vs.map((v: Channel) => {
            return safeChannel(v);
        });
    } else {
        return [];
    }
}

export interface ChannelDetail {

    /**
    * 内容ID。
    */
    ID: string;

    /**
    * 第三方系统分配的内容Code。
    */
    code?: string;

    /**
    * 关联内容唯一标识
    */
    rMediaCode?: string;

    /**
    * 返回SP操作员设置的系统频道号。
    *
    * 如果是根据频道ID获取频道信息，而且一个逻辑频道有多个频道号的话，ChannelNO随机返回一个。
    */
    channelNO?: string;

    /**
    * 内容名称。
    */
    name: string;

    /**
    * 内容简介。
    */
    introduce?: string;

    /**
    * 内容类型。
    *
    * AUDIO_CHANNEL
    *
    * VIDEO_CHANNEL
    */
    contentType: string;

    /**
    * 频道是否支持节目单级的授权。
    *
    * 取值范围：
    *
    * 0：不支持
    *
    * 1：支持
    */
    isPPV: string;

    /**
    * 观看级别。
    */
    rating: Rating;

    /**
    * 海报图片路径。
    *
    * 参见“[Picture](#_ZH-CN_TOPIC_0055055568)”类型。
    */
    picture?: Picture;

    /**
    * 频道Logo。
    *
    * 请参见“[ChannelLogo](#_ZH-CN_TOPIC_0055055495)”类型。
    */
    logo?: ChannelLogo;

    /**
    * 内容价格。
    *
    * 单位为最小货币单位。
    */
    price?: string;

    /**
    * 频道支持的音轨语种，基线版本为ISO 639-1双字母缩写。
    *
    * 简码对应的名称是平台根据当前的用户语种，查询ISO标准编码表获取。当前支持的语种包括：
    *
    * English（默认，如果用户语种不在这几种内，返回该语种对应的语种）
    *
    * Chinese
    *
    * Arabic
    *
    * Hungarian
    *
    * German
    */
    audioLanguages?: ISOCode[];

    /**
    * 频道支持的字幕语种，基线版本为ISO 639-1双字母缩写。
    *
    * 简码对应的名称是平台根据当前的用户语种，查询ISO标准编码表获取。当前支持的语种包括：
    *
    * English（默认，如果用户语种不在这几种内，返回该语种对应的语种）
    *
    * Chinese
    *
    * Arabic
    *
    * Hungarian
    *
    * German
    */
    subtitleLanguages?: ISOCode[];

    /**
    * 内容关联的CP设备类型。
    */
    deviceTypes?: DeviceType[];

    /**
    * 频道的流派信息。
    */
    genres?: Genre[];

    /**
    * 频道元数据的自定义扩展属性，其中扩展属性的Key由局点CMS定制。
    */
    customFields?: NamedParameter[];

    /**
    * Catch-up TV是否依赖于直播特性，取值包括：
    *
    * 0：不依赖
    *
    * 1：依赖
    *
    * 默认值为0。
    *
    * 说明
    *
    * 如果依赖，用户必须已经订购了该频道的直播特性才可以订购Catch-up TV（包括订购频道内容/媒资的Catch-up TV特性、订购节目单的Catch-up TV特性，以及订购包含了Catch-up TV特性、节目单的套餐包）。
    *
    * 如果不依赖，则无需订购该频道的直播特性，可以直接订购Catch-up TV。
    */
    isCUTVDependonLivetv: string;

    /**
    * 频道/VAS关联的栏目ID。
    */
    subjectIDs?: string[];

    /**
    * 如果频道被收藏，返回收藏记录。
    */
    favorite?: Favorite;

    /**
    * 标识内容是否有童锁。
    *
    * 取值范围：
    *
    * 1：有童锁
    *
    * 0：无童锁
    */
    isLocked?: string;

    /**
    * 是否包含PIP小流频道。
    *
    * 0：否
    *
    * 1：是
    *
    * IPTV频道和OTT频道都支持，其中IPTV/OTT分别处理如下：
    *
    * IPTV频道的PIP使用PhysicalChannel.isSupportPIP中isSupportPIP&#x3D;1的物理媒资；
    *
    * OTT频道的PIP使用PhysicalChannel.isSupportPIP中isSupportPIP&#x3D;1的物理媒资，取其最低码率的码流播放。
    */
    hasPIP: string;

    /**
    * 物理频道信息。
    */
    physicalChannels: PhysicalChannel[];

    /**
    * IPTV领域的PIP小流物理频道信息，OTT领域的PIP小流是多码率的物理频道中码率最小的码流，不在这个字段中体现。
    */
    pipPhysicalChannel?: PhysicalChannel;

    /**
    * 内容地理版权信息，两位的国家码(遵从ISO 3166-1)
    */
    locationCopyrights?: string[];

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeChannelDetail(v: ChannelDetail): ChannelDetail {
    if (v) {
        v.rating = safeRating(v.rating);
        v.picture = safePicture(v.picture);
        v.logo = safeChannelLogo(v.logo);
        v.audioLanguages = safeISOCodes(v.audioLanguages);
        v.subtitleLanguages = safeISOCodes(v.subtitleLanguages);
        v.deviceTypes = safeDeviceTypes(v.deviceTypes);
        v.genres = safeGenres(v.genres);
        v.customFields = safeNamedParameters(v.customFields);
        v.subjectIDs = v.subjectIDs || [];
        v.favorite = safeFavorite(v.favorite);
        v.physicalChannels = safePhysicalChannels(v.physicalChannels);
        v.pipPhysicalChannel = safePhysicalChannel(v.pipPhysicalChannel);
        v.locationCopyrights = v.locationCopyrights || [];
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeChannelDetails(vs: ChannelDetail[]): ChannelDetail[] {
    if (vs) {
        return vs.map((v: ChannelDetail) => {
            return safeChannelDetail(v);
        });
    } else {
        return [];
    }
}

export interface ChannelDynamicProperties {

    /**
    * 内容ID。
    */
    ID: string;

    /**
    * 频道号，返回SP操作员设置的系统频道号。
    *
    * 如果是根据频道ID获取频道信息，而且一个逻辑频道有多个频道号的话，channelNO随机返回一个。
    */
    channelNO: string;

    /**
    * 如果频道被收藏，返回收藏记录。
    */
    favorite?: Favorite;

    /**
    * 标识内容是否有童锁。
    *
    * 取值范围：
    *
    * 1：有童锁
    *
    * 0：无童锁
    */
    isLocked: string;

    /**
    * 物理频道动态属性信息。
    */
    physicalChannelsDynamicProperties: PhysicalChannelDynamicProperties[];
}

function safeChannelDynamicProperties(v: ChannelDynamicProperties): ChannelDynamicProperties {
    if (v) {
        v.favorite = safeFavorite(v.favorite);
        v.physicalChannelsDynamicProperties = safePhysicalChannelDynamicPropertiess(v.physicalChannelsDynamicProperties);
    }
    return v;
}

function safeChannelDynamicPropertiess(vs: ChannelDynamicProperties[]): ChannelDynamicProperties[] {
    if (vs) {
        return vs.map((v: ChannelDynamicProperties) => {
            return safeChannelDynamicProperties(v);
        });
    } else {
        return [];
    }
}

export interface ChannelEncrypt {

    /**
    * 频道是否加密。
    *
    * 取值范围：
    *
    * 0：未加密
    *
    * 1：加密
    */
    encrypt: string;

    /**
    * 内容是否支持HDCP。
    *
    * 取值范围：
    *
    * 0：不支持
    *
    * 1：支持
    */
    hdcpEnable: string;

    /**
    * 频道的Macrovision属性。
    *
    * 取值范围：
    *
    * 0：Macrovision off
    *
    * 1：AGC
    *
    * 2：AGC + 2-stripe
    *
    * 3：AGC + 4-stripe
    *
    * 默认值为0。
    */
    macrovision: string;

    /**
    * 内容CGMS-A属性，取值包括：
    *
    * 0：Copy Freely
    *
    * 1：Copy No More
    *
    * 2：Copy Once
    *
    * 3：Copy Never
    */
    CGMSA: string;
}

function safeChannelEncrypt(v: ChannelEncrypt): ChannelEncrypt {
    if (v) {
    }
    return v;
}

function safeChannelEncrypts(vs: ChannelEncrypt[]): ChannelEncrypt[] {
    if (vs) {
        return vs.map((v: ChannelEncrypt) => {
            return safeChannelEncrypt(v);
        });
    } else {
        return [];
    }
}

export interface ChannelFilter {

    /**
    * 流派ID。
    */
    genres?: string[];

    /**
    * 订购方式，取值如下：
    *
    * -1：所有
    *
    * 0：已订购且被定价为包月产品或免费包周期产品
    *
    * 1：已订购且被定价为按次产品或免费按次产品
    *
    * 2：未订购
    *
    * 3：未订购且被定价为按次产品
    *
    * 4：未订购且被定价为包月产品
    *
    * 默认取值为-1
    *
    * 说明
    *
    * 针对订购信息的过滤，会造成接口性能的大幅下降，所以请在用户低频访问、并且业务场景必须要求根据订购关系过滤的UI中使用。
    */
    subscriptionTypes?: string[];

    /**
    * 频道是否收藏。
    *
    * 0：未收藏
    *
    * 1：已收藏
    *
    * 如果取值为空，表示不针对内容是否收藏进行过滤
    */
    isFavorite?: string;

    /**
    * 频道是否支持CUTV，取值范围：
    *
    * 0：不支持
    *
    * 1：支持
    *
    * 如果取值为空，表示不针对内容是否支持CUTV进行过滤
    */
    isCUTV?: string;

    /**
    * 频道是否支持PVR，取值范围：
    *
    * 1：支持NPVR
    *
    * 2：支持CPVR
    *
    * 3：支持NPVR或者CPVR
    *
    * 如果取值为空，表示不针对内容是否支持PVR进行过滤
    */
    isPVR?: string;
}

function safeChannelFilter(v: ChannelFilter): ChannelFilter {
    if (v) {
        v.genres = v.genres || [];
        v.subscriptionTypes = v.subscriptionTypes || [];
    }
    return v;
}

function safeChannelFilters(vs: ChannelFilter[]): ChannelFilter[] {
    if (vs) {
        return vs.map((v: ChannelFilter) => {
            return safeChannelFilter(v);
        });
    } else {
        return [];
    }
}

export interface ChannelLogo {

    /**
    * 台标URL。
    *
    * 如：http://10.12.2.25:8080/VSP/jsp/image/a.jpg。
    *
    * VSP IP地址支持IPv4或IPv6，平台根据终端接入的网络地址类型返回对应的HTTP地址。
    */
    url: string;

    /**
    * 台标显示，隐藏时长，格式为“显示时长隐藏时长”。
    *
    * 单位：秒。
    */
    display: string;

    /**
    * 台标位置，格式为“X,Y”，单位为像素。
    */
    location: string;

    /**
    * 台标大小，格式为“高度,宽度”，单位为像素。
    */
    size: string;
}

function safeChannelLogo(v: ChannelLogo): ChannelLogo {
    if (v) {
    }
    return v;
}

function safeChannelLogos(vs: ChannelLogo[]): ChannelLogo[] {
    if (vs) {
        return vs.map((v: ChannelLogo) => {
            return safeChannelLogo(v);
        });
    } else {
        return [];
    }
}

export interface ChannelPlaybill {

    /**
    * 频道信息，根据终端请求参数确定是否填充。
    */
    channelDetail?: ChannelDetail;

    /**
    * 频道对应的节目单总数。
    */
    playbillCount?: string;

    /**
    * 频道对应的节目单列表。
    */
    playbillLites?: PlaybillLite[];
}

function safeChannelPlaybill(v: ChannelPlaybill): ChannelPlaybill {
    if (v) {
        v.channelDetail = safeChannelDetail(v.channelDetail);
        v.playbillLites = safePlaybillLites(v.playbillLites);
    }
    return v;
}

function safeChannelPlaybills(vs: ChannelPlaybill[]): ChannelPlaybill[] {
    if (vs) {
        return vs.map((v: ChannelPlaybill) => {
            return safeChannelPlaybill(v);
        });
    } else {
        return [];
    }
}

export interface ChannelPlaybillContext {

    /**
    * 频道信息，根据终端请求参数确定是否填充。
    */
    channelDetail?: ChannelDetail;

    /**
    * 返回的节目单列表，按节目单开始时间升序排序。
    *
    * 如没有节目单且终端请求不填充，返回空prePlaybillLites标签。
    */
    prePlaybillLites?: PlaybillLite[];

    /**
    * 当前节目，节目属性请参见“[PlaybillLite](#_ZH-CN_TOPIC_0055055502)”。
    *
    * 如果当前没有节目单且填充节目单配置值有效，则平台返回填充节目单；填充节目时如果节目单被过滤，则构造类型为2的填充节目单。
    *
    * 如果填充条件无效，且此时无节目单，此时返回null
    */
    currentPlaybillLite?: PlaybillLite;

    /**
    * 返回的节目单列表，按节目单开始时间升序排序。
    *
    * 如没有节目单且不填充，返回空nextPlaybillLites标签。
    */
    nextPlaybillLites?: PlaybillLite[];
}

function safeChannelPlaybillContext(v: ChannelPlaybillContext): ChannelPlaybillContext {
    if (v) {
        v.channelDetail = safeChannelDetail(v.channelDetail);
        v.prePlaybillLites = safePlaybillLites(v.prePlaybillLites);
        v.currentPlaybillLite = safePlaybillLite(v.currentPlaybillLite);
        v.nextPlaybillLites = safePlaybillLites(v.nextPlaybillLites);
    }
    return v;
}

function safeChannelPlaybillContexts(vs: ChannelPlaybillContext[]): ChannelPlaybillContext[] {
    if (vs) {
        return vs.map((v: ChannelPlaybillContext) => {
            return safeChannelPlaybillContext(v);
        });
    } else {
        return [];
    }
}

export interface Chapter {

    /**
    * 章节编号。
    */
    ID: string;

    /**
    * 章节标题。
    */
    title: string;

    /**
    * 章节相对于VOD节目开始时间的秒数。
    */
    offTime: string;

    /**
    * 章节海报的绝对路径，为HTTP URL，例如，http://ip:port/VSP/jsp/image/12.jpg。
    */
    posters?: string[];
}

function safeChapter(v: Chapter): Chapter {
    if (v) {
        v.posters = v.posters || [];
    }
    return v;
}

function safeChapters(vs: Chapter[]): Chapter[] {
    if (vs) {
        return vs.map((v: Chapter) => {
            return safeChapter(v);
        });
    } else {
        return [];
    }
}

export interface CheckLock {

    /**
    * 检查类型，取值包括：
    *
    * 0：检查父母字和加锁
    *
    * 1：检查解锁密码
    */
    checkType: string;

    /**
    * 如果checkType&#x3D;1，此字段必填，表示用户输入的解锁密码，所传递的密码以明文方式传递。
    */
    password?: string;

    /**
    * 如果checkType&#x3D;1，此字段必填，表示解密密码的密码类型，取值包括：
    *
    * 0：Profile密码
    *
    * 1：订购密码
    *
    * 2：订户密码
    *
    * 3：Admin Profile密码
    *
    * 4：父母控制密码
    */
    type?: string;
}

function safeCheckLock(v: CheckLock): CheckLock {
    if (v) {
    }
    return v;
}

function safeCheckLocks(vs: CheckLock[]): CheckLock[] {
    if (vs) {
        return vs.map((v: CheckLock) => {
            return safeCheckLock(v);
        });
    } else {
        return [];
    }
}

export interface CommentFilter {

    /**
    * 作者的编号ID（查询自己发布的评论/查询指定作者的评论）
    */
    authorID?: string;

    /**
    * 子账号编号ID。
    */
    profileSN?: string;

    /**
    * 评论的内容的编号
    */
    contentCode?: string;

    /**
    * 评论的内容类型
    */
    contentType?: string;

    /**
    * UTC时间，格式yyyymmddHHMMSS，用户评论的起始时间。
    */
    commentBeginTime?: string;

    /**
    * UTC时间，格式yyyymmddHHMMSS，用户评论的截止时间。
    *
    * 与上一个参数一起，可以用于过滤一周内/一月内的评论。
    */
    commentEndTime?: string;

    /**
    * 父评论ID，用于展开查询评论下的子评论列表。
    */
    commentID?: string;
}

function safeCommentFilter(v: CommentFilter): CommentFilter {
    if (v) {
    }
    return v;
}

function safeCommentFilters(vs: CommentFilter[]): CommentFilter[] {
    if (vs) {
        return vs.map((v: CommentFilter) => {
            return safeCommentFilter(v);
        });
    } else {
        return [];
    }
}

export interface CommentObject {

    /**
    * 评论ID
    */
    commentID: string;

    /**
    * 评论内容
    */
    comment: string;

    /**
    * 评论的内容的编号
    */
    contentCode: string;

    /**
    * 评论的内容的类型
    */
    contentType: string;

    /**
    * 父评论的编号
    */
    parentCommentID?: string;

    /**
    * UTC时间，格式yyyymmddHHMMSS，用户评论的时间
    */
    commentTime?: string;

    /**
    * 子评论列表
    */
    subComments?: CommentObject[];

    /**
    * 子评论的数量
    */
    subCommentCount?: string;

    /**
    * 作者的订户ID
    */
    userID: string;

    /**
    * 作者账号序号
    */
    profileSN?: string;

    /**
    * 作者昵称
    */
    nickName?: string;

    /**
    * 作者头像的URL或编号
    */
    headPic?: string;

    /**
    * 当前评论是否被查询人点赞
    */
    liked?: Boolean;

    /**
    * 当前评论被点赞的总次数
    */
    likedCount?: string;

    /**
    * 扩展信息。
    */
    extendProperties?: NamedParameter[];
}

function safeCommentObject(v: CommentObject): CommentObject {
    if (v) {
        v.subComments = safeCommentObjects(v.subComments);
        v.extendProperties = safeNamedParameters(v.extendProperties);
    }
    return v;
}

function safeCommentObjects(vs: CommentObject[]): CommentObject[] {
    if (vs) {
        return vs.map((v: CommentObject) => {
            return safeCommentObject(v);
        });
    } else {
        return [];
    }
}

export interface Configuration {

    /**
    * 参数配置类型。
    *
    * 取值范围：
    *
    * 0：查询终端的配置参数
    *
    * 1：查询EPG模板的配置参数
    *
    * 2：查询VSP EPG参数
    *
    * 3：查询第三方系统集成参数
    */
    cfgType: string;

    /**
    * 配置参数列表。
    */
    values?: NamedParameter[];

    /**
    * STB日志服务器地址。
    */
    STBLogServerURL?: string;

    /**
    * STB日志上载周期。
    *
    * 单位：秒。
    */
    STBLogUploadInterval?: string;

    /**
    * 播放心跳周期，单位为秒。
    */
    playHeartBitInterval?: string;

    /**
    * Netrix设备信息上报URL。
    */
    netrixPushServerURL?: string;

    /**
    * Skip播放跳转时长跨度，单位是s。
    */
    skippingTimeBlock?: string;

    /**
    * VOD收藏上限。
    */
    VODFavLimit?: string;

    /**
    * 频道收藏上限。
    */
    channelFavLimit?: string;

    /**
    * VAS收藏上限。
    */
    VASFavLimit?: string;

    /**
    * 用户进行下载播放时的最小缓冲时长，单位为秒，默认30s。
    */
    downloadedBufferLength?: string;

    /**
    * 社交平台的服务地址。
    */
    SocialURL?: string;

    /**
    * 用户收藏上限。
    *
    * 每个子用户的收藏个数上限单独控制。
    */
    favouriteLimit?: string;

    /**
    * 书签上限。
    *
    * 每个子用户的书签个数上限单独控制。
    */
    bookmarkLimit?: string;

    /**
    * 订户的锁上限。
    */
    lockLimit?: string;

    /**
    * 用户Profile个数上限。
    */
    profileLimit?: string;

    /**
    * 对接的第三方业务质量管理系统地址。
    *
    * IP地址支持V4或V6。
    */
    SQMURL?: string;

    /**
    * 密码最小长度。
    */
    UserPwdMinLength?: string;

    /**
    * 是否一定要包含大写字母A-Z。
    *
    * 0：不一定包含
    *
    * 1：必须包含
    *
    * 默认值为1。
    */
    UserPwdUpperCaseLetters?: string;

    /**
    * 是否一定要包含小写字母a-z。
    *
    * 0：不一定包含
    *
    * 1：必须包含
    *
    * 默认值为1。
    */
    UserPwdLowerCaseLetters?: string;

    /**
    * 是否一定要包含数字。
    *
    * 0：不一定包含
    *
    * 1：必须包含
    *
    * 默认值为1。
    */
    UserPwdNumbers?: string;

    /**
    * 特殊字符。
    *
    * 如果为空，则表示不需要一定包含特殊字符。
    *
    * 如果不为空，则表示一定要包含此字段值中的特殊字符中的一个或多个。
    */
    UserPwdOthersLetters?: string;

    /**
    * 是否支持空格作为密码字符。
    *
    * 0：不支持
    *
    * 1：支持
    *
    * 默认值为1。
    */
    UserPwdSupportSpace?: string;

    /**
    * 节目单录制提前启动时长，单位秒，默认0，取值范围是0~3600。
    */
    BeginOffset?: string;

    /**
    * 节目单录制延后录制时长，单位秒，默认0，取值范围是0~3600。
    */
    EndOffset?: string;

    /**
    * NPVR业务的空间运营模式，取值包括：
    *
    * 1：按照网络容量计算
    *
    * 2：按照录制时长计算
    *
    * 默认值为2。
    */
    NPVRSpaceStrategy?: string;

    /**
    * OTT领域的IM服务器BOSH服务IP V6 URL。
    */
    OTTIMPURLforIPv?: string;

    /**
    * OTT领域的IM服务器BOSH服务IP V4 URL。
    */
    OTTIMPURL?: string;

    /**
    * OTT领域的IM服务端口。
    */
    OTTIMPPort?: string;

    /**
    * OTT领域的IM服务器IP V6地址。
    */
    OTTIMPIPforIPv?: string;

    /**
    * UserID最小长度。
    */
    UserIDMinLength?: string;

    /**
    * UserID最大长度。
    */
    UserIDMaxLength?: string;

    /**
    * 是否支持大写字母A-Z。
    *
    * 0：不支持
    *
    * 1：支持
    *
    * 默认值为1。
    */
    UserIDUpperCaseLetters?: string;

    /**
    * 是否一定支持小写字母a-z。
    *
    * 0：不支持
    *
    * 1：支持
    *
    * 默认值为1。
    */
    UserIDLowerCaseLetters?: string;

    /**
    * 是否一定支持数字。
    *
    * 0：不支持
    *
    * 1：支持
    *
    * 默认值为1。
    */
    UserIDNumbers?: string;

    /**
    * 特殊字符。
    *
    * 如果为空，则表示不支持包含特殊字符。
    *
    * 如果不为空，则表示支持包含此字段值中的特殊字符中的一个或多个。
    */
    UserIDOthersLetters?: string;

    /**
    * 是否支持空格。
    *
    * 0：不支持
    *
    * 1：支持
    *
    * 默认值为1。
    */
    UserIDSupportSpace?: string;

    /**
    * ProfileName最小长度。
    */
    ProfileNameMinLength?: string;

    /**
    * ProfileName最大长度。
    */
    ProfileNameMaxLength?: string;

    /**
    * 是否支持大写字母A-Z。
    *
    * 0：不支持
    *
    * 1：支持
    *
    * 默认值为1。
    */
    ProfileNameUpperCaseLetters?: string;

    /**
    * 是否支持小写字母a-z。
    *
    * 0：不支持
    *
    * 1：支持
    *
    * 默认值为1。
    */
    ProfileNameLowerCaseLetters?: string;

    /**
    * 是否支持数字。
    *
    * 0：不支持
    *
    * 1：支持
    *
    * 默认值为1。
    */
    ProfileNameNumbers?: string;

    /**
    * 特殊字符。
    *
    * 如果为空，则表示不支持包含特殊字符。
    *
    * 如果不为空，则表示支持包含此字段值中的特殊字符中的一个或多个。
    */
    ProfileNameOthersLetters?: string;

    /**
    * 是否支持空格。
    *
    * 0：不支持
    *
    * 1：支持
    *
    * 默认值为1。
    */
    ProfileNameSupportSpace?: string;

    /**
    * DeviceName最小长度。
    */
    DeviceNameMinLength?: string;

    /**
    * DeviceName最大长度。
    */
    DeviceNameMaxLength?: string;

    /**
    * 是否支持大写字母A-Z。
    *
    * 0：不支持
    *
    * 1：支持
    *
    * 默认值为1。
    */
    DeviceNameUpperCaseLetters?: string;

    /**
    * 是否支持小写字母a-z。
    *
    * 0：不支持
    *
    * 1：支持
    *
    * 默认值为1。
    */
    DeviceNameLowerCaseLetters?: string;

    /**
    * 是否支持数字。
    *
    * 0：不支持
    *
    * 1：支持
    *
    * 默认值为1。
    */
    DeviceNameNumbers?: string;

    /**
    * 特殊字符。
    *
    * 如果为空，则表示不支持包含特殊字符。
    *
    * 如果不为空，则表示支持包含此字段值中的特殊字符中的一个或多个。
    */
    DeviceNameOthersLetters?: string;

    /**
    * 是否支持空格。
    *
    * 0：不支持
    *
    * 1：支持
    *
    * 默认值为1。
    */
    DeviceNameSupportSpace?: string;

    /**
    * SearchKey最小长度。
    */
    SearchKeyMinLength?: string;

    /**
    * SearchKey最大长度。
    */
    SearchKeyMaxLength?: string;

    /**
    * 是否支持大写字母A-Z。
    *
    * 0：不支持
    *
    * 1：支持
    *
    * 默认值为1。
    */
    SearchKeyUpperCaseLetters?: string;

    /**
    * 是否支持小写字母a-z。
    *
    * 0：不支持
    *
    * 1：支持
    *
    * 默认值为1。
    */
    SearchKeyLowerCaseLetters?: string;

    /**
    * 是否支持数字。
    *
    * 0：不支持
    *
    * 1：支持
    *
    * 默认值为1。
    */
    SearchKeyNumbers?: string;

    /**
    * 特殊字符。
    *
    * 如果为空，则表示不支持包含特殊字符。
    *
    * 如果不为空，则表示支持包含此字段值中的特殊字符中的一个或多个。
    */
    SearchKeyOthersLetters?: string;

    /**
    * 是否支持空格。
    *
    * 0：不支持
    *
    * 1：支持
    *
    * 默认值为1。
    */
    SearchKeySupportSpace?: string;

    /**
    * PlaylistName最小长度。
    */
    PlaylistNameMinLength?: string;

    /**
    * PlaylistName最大长度。
    */
    PlaylistNameMaxLength?: string;

    /**
    * 是否支持大写字母A-Z。
    *
    * 0：不支持
    *
    * 1：支持
    *
    * 默认值为1。
    */
    PlaylistNameUpperCaseLetters?: string;

    /**
    * 是否支持小写字母a-z。
    *
    * 0：不支持
    *
    * 1：支持
    *
    * 默认值为1。
    */
    PlaylistNameLowerCaseLetters?: string;

    /**
    * 是否支持数字。
    *
    * 0：不支持
    *
    * 1：支持
    *
    * 默认值为1。
    */
    PlaylistNameNumbers?: string;

    /**
    * 特殊字符。
    *
    * 如果为空，则表示不支持包含特殊字符。
    *
    * 如果不为空，则表示支持包含此字段值中的特殊字符中的一个或多个。
    */
    PlaylistNameOthersLetters?: string;

    /**
    * 是否支持空格。
    *
    * 0：不支持
    *
    * 1：支持
    *
    * 默认值为1。
    */
    PlaylistNameSupportSpace?: string;

    /**
    * OTT领域的IM服务器IP V4地址。
    */
    OTTIMPIP?: string;

    /**
    * IPTV领域的IM服务器BOSH服务IP V6 URL。
    */
    IPTVIMPURLforIPv?: string;

    /**
    * IPTV领域的IM服务器BOSH服务IP V4 URL。
    */
    IPTVIMPURL?: string;

    /**
    * IPTV领域的IM服务端口。
    */
    IPTVIMPPort?: string;

    /**
    * IPTV领域的IM服务器IP V6地址。
    */
    IPTVIMPIPforIPv?: string;

    /**
    * IPTV领域的IM服务器IP V4地址。
    */
    IPTVIMPIP?: string;

    /**
    * IM服务的域名。
    */
    IMDomain?: string;

    /**
    * IMP对客户端登录的鉴权方式。
    *
    * 0：采用摘要校验
    *
    * 1：采用会话TOKEN校验
    *
    * 默认值为0。
    */
    ImpAuthType?: string;

    /**
    * 节目单展示的最晚时间间隔，单位天，默认值7，表示终端展示最晚7天后的节目单
    */
    PlaybillLen?: string;

    /**
    * 节目单展示的最早时间间隔，单位天，默认值7，表示终端展示最早7天前的节目单
    */
    RecPlaybillLen?: string;

    /**
    * 第三方业务质量管理系统地址。
    *
    * IP地址支持V4或V6。
    */
    MQMCURL?: string;

    /**
    * 机顶盒支持的CPVR并发录制数
    */
    ConcurrentCpvrTaskLimit?: string;
}

function safeConfiguration(v: Configuration): Configuration {
    if (v) {
        v.values = safeNamedParameters(v.values);
    }
    return v;
}

function safeConfigurations(vs: Configuration[]): Configuration[] {
    if (vs) {
        return vs.map((v: Configuration) => {
            return safeConfiguration(v);
        });
    } else {
        return [];
    }
}

export interface ConflictPVRGroup {

    /**
    * 冲突时间段的开始时间，取值为距离1970年1月1号的毫秒数。
    *
    * 注：多屏目前用不到这个字段，但需要SolvePVRConflict接口中传给平台
    */
    beginTime: string;

    /**
    * 冲突时间段的结束时间，取值为距离1970年1月1号的毫秒数。
    *
    * 注：多屏目前用不到这个字段，但需要SolvePVRConflict接口中传给平台
    */
    endTime: string;

    /**
    * 冲突分组内的任务列表。作为SolvePVRConflict接口入参时，除了待添加的任务(id为-1的任务)需要传完整的PVR对象，其他PVR只需要传ID即可
    */
    PVRList: PVRTaskBasic[];

    /**
    * 冲突的任务是否保留，和PVRList一一对应，取值包括：
    *
    * 0：不保留
    *
    * 1：保留
    *
    * 不传默认值为1
    */
    isReserves?: string[];

    /**
    * 冲突类型，仅对单个录制任务的冲突分组生效，支持返回多个，取值包括：
    *
    * IO：IO冲突
    */
    conflictType?: string[];
}

function safeConflictPVRGroup(v: ConflictPVRGroup): ConflictPVRGroup {
    if (v) {
        v.PVRList = safePVRTaskBasics(v.PVRList);
        v.isReserves = v.isReserves || [];
        v.conflictType = v.conflictType || [];
    }
    return v;
}

function safeConflictPVRGroups(vs: ConflictPVRGroup[]): ConflictPVRGroup[] {
    if (vs) {
        return vs.map((v: ConflictPVRGroup) => {
            return safeConflictPVRGroup(v);
        });
    } else {
        return [];
    }
}

export interface Content {

    /**
    * 内容类型，取值包括：
    *
    * VIDEO_VOD：视频VOD
    *
    * AUDIO_VOD：音频VOD
    *
    * VIDEO_CHANNEL：视频频道
    *
    * AUDIO_CHANNEL：音频频道
    *
    * PROGRAM：节目单
    *
    * SUBJECT：栏目
    */
    contentType: string;

    /**
    * VOD内容描述，contentType等于VIDEO_VOD和AUDIO_VOD时返回。
    */
    VOD?: VOD;

    /**
    * channel内容描述。contentType等于
    *
    * VIDEO_CHANNEL和AUDIO_CHANNEL时返回。
    */
    channel?: Channel;

    /**
    * 栏目内容描述，contentType等于SUBJECT时返回。
    */
    subject?: Subject;

    /**
    * program内容描述，contentType等于PROGRAM时返回。
    */
    playbill?: Playbill;

    /**
    * 内容搜索趋势信息，取值如下：
    *
    * 0：与上次持平；
    *
    * 1：上升；
    *
    * 2：下降
    */
    tendency?: string;
}

function safeContent(v: Content): Content {
    if (v) {
        v.VOD = safeVOD(v.VOD);
        v.channel = safeChannel(v.channel);
        v.subject = safeSubject(v.subject);
        v.playbill = safePlaybill(v.playbill);
    }
    return v;
}

function safeContents(vs: Content[]): Content[] {
    if (vs) {
        return vs.map((v: Content) => {
            return safeContent(v);
        });
    } else {
        return [];
    }
}

export interface ContentLikes {

    /**
    * 内容ID。
    */
    contentID: string;

    /**
    * 内容类型，取值包括：
    *
    * VOD：点播
    *
    * 说明
    *
    * 平台目前仅支持VOD评分，此属性为了后续增加节目单或频道评分预留。
    */
    contentType: string;

    /**
    * 点赞总数。
    */
    likes: string;
}

function safeContentLikes(v: ContentLikes): ContentLikes {
    if (v) {
    }
    return v;
}

function safeContentLikess(vs: ContentLikes[]): ContentLikes[] {
    if (vs) {
        return vs.map((v: ContentLikes) => {
            return safeContentLikes(v);
        });
    } else {
        return [];
    }
}

export interface ContentLikeSetting {

    /**
    * 点赞操作。
    *
    * 0：取消点赞
    *
    * 1：点赞
    */
    action: string;

    /**
    * 内容ID。
    */
    contentID: string;

    /**
    * 内容类型，取值包括：
    *
    * VOD：点播
    *
    * 说明
    *
    * 平台目前仅支持VOD评分，此属性为了后续增加节目单或频道评分预留。
    */
    contentType: string;
}

function safeContentLikeSetting(v: ContentLikeSetting): ContentLikeSetting {
    if (v) {
    }
    return v;
}

function safeContentLikeSettings(vs: ContentLikeSetting[]): ContentLikeSetting[] {
    if (vs) {
        return vs.map((v: ContentLikeSetting) => {
            return safeContentLikeSetting(v);
        });
    } else {
        return [];
    }
}

export interface ContentRight {

    /**
    * 业务是否支持，取值包括：
    *
    * 0：不支持
    *
    * 1：支持
    */
    enable: string;

    /**
    * 如果enable&#x3D;1，此字段必填，当前用户是否可以使用此业务，取值包括：
    *
    * 0：不能使用
    *
    * 1：可以使用
    */
    isContentValid?: string;

    /**
    * 用户是否购买了业务定价的产品。
    *
    * 取值范围：
    *
    * 0：未订购
    *
    * 1：已订购
    */
    isSubscribed: string;

    /**
    * 如果isSubscribed&#x3D;1，当前用户是否可以使用此业务，取值包括：
    *
    * 0：不能使用
    *
    * 1：可以使用
    */
    isValid?: string;

    /**
    * PLTV/CPLTV/NPVRRecord/CUTV/IR业务的录制时长，单位为秒，其中NPVRRecord取值0表示不限制。
    */
    length?: string;

    /**
    * 表示业务实际使用的mediaId，比如对DVB频道进行NPVR录制，录制的是某个IPTV/OTT逻辑频道。仅PLTV/NPVR/CUTV支持
    */
    bizMediaId?: string;

    /**
    * 是否支持快进，默认值0。
    *
    * 0：不支持
    *
    * 1：支持
    *
    * 仅CUTV支持
    */
    isSupportFF?: string;

    /**
    * 是否支持快退，默认值0。
    *
    * 0：不支持
    *
    * 1：支持
    *
    * 仅CUTV支持
    */
    isSupportFB?: string;

    /**
    * 若不支持快进快退（当isSupportFF&#x3D;0和isSupportFB&#x3D;0时），则表示从片头开始多长时间内支持快进快退，单位是秒。
    *
    * 仅CUTV支持
    */
    timelengthOfFFFB?: string;

    /**
    * 若不支持快进快退（当isSupportFF&#x3D;0和isSupportFB&#x3D;0时），是否支持书签以前内容快进快退，默认值0。
    *
    * 0：不支持
    *
    * 1：支持
    *
    * 仅CUTV支持
    */
    isSupportFFFBbyBookmark?: string;

    /**
    * CUTV内容播放结束后，再播放多久，单位是秒。
    *
    * 仅CUTV支持
    */
    timelengthAfterPlay?: string;
}

function safeContentRight(v: ContentRight): ContentRight {
    if (v) {
    }
    return v;
}

function safeContentRights(vs: ContentRight[]): ContentRight[] {
    if (vs) {
        return vs.map((v: ContentRight) => {
            return safeContentRight(v);
        });
    } else {
        return [];
    }
}

export interface ContentScore {

    /**
    * 内容ID。
    */
    contentID: string;

    /**
    * 内容类型，取值包括：
    *
    * VOD：点播
    *
    * 说明
    *
    * 1.平台目前仅支持VOD评分，此属性为了后续增加节目单或频道评分预留。
    */
    contentType: string;

    /**
    * 分值，取值范围：1-10。
    */
    score: string;
}

function safeContentScore(v: ContentScore): ContentScore {
    if (v) {
    }
    return v;
}

function safeContentScores(vs: ContentScore[]): ContentScore[] {
    if (vs) {
        return vs.map((v: ContentScore) => {
            return safeContentScore(v);
        });
    } else {
        return [];
    }
}

export interface CustomizedProperty {

    /**
    * 只有周期系列父任务有这个属性，并只在QueryPVR接口中返回，代表这个周期系列父任务在查询的过滤条件下，开始时间最大的子任务的开始时间，取值为距离1970年1月1号的毫秒数。若父任务下没有任何子集则不返回此参数。
    *
    * 使用场景：在未开始录制管理界面中，结合PVRSpace.notEnoughStartTime字段，多屏客户端可以判断出这个周期系列父任务下是否存在空间不够的子集
    */
    childPVRMaxStartTime?: string;
}

function safeCustomizedProperty(v: CustomizedProperty): CustomizedProperty {
    if (v) {
    }
    return v;
}

function safeCustomizedPropertys(vs: CustomizedProperty[]): CustomizedProperty[] {
    if (vs) {
        return vs.map((v: CustomizedProperty) => {
            return safeCustomizedProperty(v);
        });
    } else {
        return [];
    }
}

export interface Device {

    /**
    * 逻辑设备ID。
    */
    ID: string;

    /**
    * 设备名称。
    */
    name?: string;

    /**
    * 设备类型的编号，0~3是系统预置的枚举值，&gt;&#x3D;5是管理员自定义的设备类型。取值范围：
    *
    * 0：STB
    *
    * 1：PC Client
    *
    * 2：OTT（包括PC Plugin、iOS和Andriod device）
    *
    * 3：Mobile（注意这里的Mobile是MTV解决方案的设备）
    *
    * &gt;&#x3D;5: 自定义的设备类型
    *
    * 说明
    *
    * PC Client需要在PC本地安装APP。
    *
    * PC Plugin不需要在PC本地安装APP，通过IE浏览。
    */
    deviceType: string;

    /**
    * 终端对应的设备型号，只有设备登录过且上报了终端型号，该属性才有值。
    */
    deviceModel?: string;

    /**
    * 设备状态，取值包括:
    *
    * 1：激活
    *
    * 0：暂停
    *
    * 默认值为1。
    */
    status?: string;

    /**
    * 设备在线标识。
    *
    * 取值范围：
    *
    * 0：不在线
    *
    * 1：在线
    *
    * 2：待机
    *
    * 3：休眠
    *
    * 默认值为0
    */
    onlineState?: string;

    /**
    * 终端设备物理地址。
    *
    * 对于STB设备，此参数为MAC地址。
    *
    * 对于OTT设备，此参数为客户端生成的UUID独立设备编码。
    */
    physicalDeviceID?: string;

    /**
    * 终端上次下线时间。
    *
    * 取值为距离1970年1月1号的毫秒数。
    *
    * 如果设备从未登录过则不会返回。
    */
    lastOfflineTime?: string;

    /**
    * CA设备信息。
    */
    CADeviceInfos?: CADeviceInfo[];

    /**
    * 设备对应的设备型号关联的频道命名空间。
    */
    channelNamespace?: string;

    /**
    * 对于XMPP方式，为从Push Server获取的设备token；
    *
    * 对于代理模式，为APNS/GCM/MPNS分配的token。
    *
    * 如果设备不在线，则不返回此属性。
    */
    deviceToken?: string;

    /**
    * 设备厂商型号，比如iPhone对应的deviceModel是iPhone，terminalVendor是iPhone 4S。
    *
    * 当使用SilverLight注册时填写Silverlight+版本号，如Silverlight4.1。
    */
    terminalVendor?: string;

    /**
    * 视频编码格式，取值包括：
    *
    * H.263
    *
    * H.264
    *
    * H.265
    *
    * 如果终端支持多种编码格式，使用英文逗号分隔。
    */
    videoCodec?: string;

    /**
    * 高清标清标识。取值范围：
    *
    * 0：SD
    *
    * 1：HD
    *
    * 2：4K
    *
    * 如果终端支持多种definition，使用英文逗号分隔。
    */
    definition?: string;

    /**
    * 内容帧率，单位是fps，取值范围(0,256]，默认值是30。
    */
    fps?: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];

    /**
    * 透传的DSV自定义扩展字段。
    */
    customFields?: NamedParameter[];
}

function safeDevice(v: Device): Device {
    if (v) {
        v.CADeviceInfos = safeCADeviceInfos(v.CADeviceInfos);
        v.extensionFields = safeNamedParameters(v.extensionFields);
        v.customFields = safeNamedParameters(v.customFields);
    }
    return v;
}

function safeDevices(vs: Device[]): Device[] {
    if (vs) {
        return vs.map((v: Device) => {
            return safeDevice(v);
        });
    } else {
        return [];
    }
}

export interface DeviceType {

    /**
    * 设备类型。
    */
    deviceType: string;

    /**
    * 设备类型名称。
    */
    deviceTypeName: string;

    /**
    * 设备类型所属CP/SP，取值包括：
    *
    * 0：CP分组
    *
    * 1：SP分组
    */
    type: string;
}

function safeDeviceType(v: DeviceType): DeviceType {
    if (v) {
    }
    return v;
}

function safeDeviceTypes(vs: DeviceType[]): DeviceType[] {
    if (vs) {
        return vs.map((v: DeviceType) => {
            return safeDeviceType(v);
        });
    } else {
        return [];
    }
}

export interface DiskInfo {

    /**
    * STB本地硬盘或者外接硬盘的剩余空间。
    *
    * 单位是MB。
    */
    availableSpace: string;
}

function safeDiskInfo(v: DiskInfo): DiskInfo {
    if (v) {
    }
    return v;
}

function safeDiskInfos(vs: DiskInfo[]): DiskInfo[] {
    if (vs) {
        return vs.map((v: DiskInfo) => {
            return safeDiskInfo(v);
        });
    } else {
        return [];
    }
}

export interface Episode {

    /**
    * 子集VOD信息。
    */
    VOD: VOD;

    /**
    * 表示该子集是连续剧的第几集。
    */
    sitcomNO: string;
}

function safeEpisode(v: Episode): Episode {
    if (v) {
        v.VOD = safeVOD(v.VOD);
    }
    return v;
}

function safeEpisodes(vs: Episode[]): Episode[] {
    if (vs) {
        return vs.map((v: Episode) => {
            return safeEpisode(v);
        });
    } else {
        return [];
    }
}

export interface FavoCatalog {

    /**
    * 收藏夹ID。
    *
    * 说明
    *
    * 新增操作，终端可以不携带该参数，由VSP平台生成。
    */
    catalogID?: string;

    /**
    * 收藏夹名称。
    */
    catalogName: string;

    /**
    * 局点定制的扩展字段。
    */
    extensionFields?: NamedParameter[];
}

function safeFavoCatalog(v: FavoCatalog): FavoCatalog {
    if (v) {
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeFavoCatalogs(vs: FavoCatalog[]): FavoCatalog[] {
    if (vs) {
        return vs.map((v: FavoCatalog) => {
            return safeFavoCatalog(v);
        });
    } else {
        return [];
    }
}

export interface Favorite {

    /**
    * 内容ID。
    */
    contentID: string;

    /**
    * 内容类型，取值包括：
    *
    * VOD：点播
    *
    * CHANNEL：频道
    */
    contentType: string;

    /**
    * 收藏夹ID，如果不携带，VSP使用-1:默认收藏夹。
    */
    catalogID?: string;

    /**
    * 推荐页面入口，用于推荐数据采集功能，如果收藏操作不是从推荐位入口进入的，则该字段默认为空。
    */
    entrance?: string;

    /**
    * 用户收藏的内容对应的推荐请求流水号，用于推荐数据采集功能，如果收藏操作不是从推荐位入口进入的，则该字默认为空。
    */
    recmActionID?: string;

    /**
    * 收藏时间，取值为距离1970年1月1号的毫秒数。
    *
    * 此参数不需要终端设置，当终端查询收藏时，会返回。
    */
    collectTime?: string;

    /**
    * 扩展字段。
    */
    extensionFields?: NamedParameter[];
}

function safeFavorite(v: Favorite): Favorite {
    if (v) {
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeFavorites(vs: Favorite[]): Favorite[] {
    if (vs) {
        return vs.map((v: Favorite) => {
            return safeFavorite(v);
        });
    } else {
        return [];
    }
}

export interface Genre {

    /**
    * 流派编号。
    */
    genreID: string;

    /**
    * 流派类型，取值包括：
    *
    * 1：音乐类，供音频内容使用
    *
    * 2：影视类，供视频内容使用
    *
    * 6：信息类，供VAS内容使用
    *
    * 7：频道节目类，供频道和节目单使用
    */
    genreType: string;

    /**
    * 流派名称。
    */
    genreName: string;

    /**
    * 局点定制的扩展字段。
    */
    extensionFields?: NamedParameter[];
}

function safeGenre(v: Genre): Genre {
    if (v) {
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeGenres(vs: Genre[]): Genre[] {
    if (vs) {
        return vs.map((v: Genre) => {
            return safeGenre(v);
        });
    } else {
        return [];
    }
}

export interface GetLicenseTrigger {

    /**
    * License服务器地址。
    *
    * 如果地址是IP形式，IP地址支持V4或V6，平台根据终端接入的网络地址类型返回对应的服务器地址。
    */
    licenseURL: string;

    /**
    * 存放用户和内容保护信息。
    */
    customData: string;
}

function safeGetLicenseTrigger(v: GetLicenseTrigger): GetLicenseTrigger {
    if (v) {
    }
    return v;
}

function safeGetLicenseTriggers(vs: GetLicenseTrigger[]): GetLicenseTrigger[] {
    if (vs) {
        return vs.map((v: GetLicenseTrigger) => {
            return safeGetLicenseTrigger(v);
        });
    } else {
        return [];
    }
}

export interface GlobalFilter {

    /**
    * 频道命名空间。
    */
    channelNamespace?: string;

    /**
    * 媒资清晰度，取值包括：
    *
    * 0：SD
    *
    * 1：HD
    *
    * 2：4K
    *
    * 如果取值为空，表示definitions不进行过滤。
    */
    definitions?: string[];

    /**
    * 内容帧率，表示获取媒质帧率小于等于此帧率的内容。
    */
    fps?: string;

    /**
    * 视频编码格式，取值包括：
    *
    * H.263
    *
    * H.264
    *
    * H.265
    *
    * 如果取值为空，表示videoCodecs不进行过滤。
    */
    videoCodecs?: string[];

    /**
    * 视频类型。
    *
    * 取值范围：
    *
    * 2：2D
    *
    * 3：3D
    *
    * 4： 2D VR
    *
    * 5： 3D VR
    */
    dimension?: string[];
}

function safeGlobalFilter(v: GlobalFilter): GlobalFilter {
    if (v) {
        v.definitions = v.definitions || [];
        v.videoCodecs = v.videoCodecs || [];
        v.dimension = v.dimension || [];
    }
    return v;
}

function safeGlobalFilters(vs: GlobalFilter[]): GlobalFilter[] {
    if (vs) {
        return vs.map((v: GlobalFilter) => {
            return safeGlobalFilter(v);
        });
    } else {
        return [];
    }
}

export interface ISOCode {

    /**
    * ISO编码。
    *
    * 对ISO 639-1，为小写的双字母英文缩写；
    *
    * 对于ISO 3166-1 alpha-2，为大写的双字母英文缩写。
    *
    * 对于ISO 639-2或ISO 639-3，为小写的三字母英文缩写。
    */
    shortName: string;

    /**
    * 编码对应的语种或国家名称。
    */
    fullName: string;
}

function safeISOCode(v: ISOCode): ISOCode {
    if (v) {
    }
    return v;
}

function safeISOCodes(vs: ISOCode[]): ISOCode[] {
    if (vs) {
        return vs.map((v: ISOCode) => {
            return safeISOCode(v);
        });
    } else {
        return [];
    }
}

export interface Language {

    /**
    * 语种ID，如果存在多个，使用英文逗号分隔。
    *
    * 从1开始依次编号，如果支持两个语种，则返回“1,2”。
    */
    ID: string;

    /**
    * 语种缩略语，如果存在多个使用英文逗号分隔，比如语种编号为1，2，语种缩写分别为en，zh。
    */
    name: string;

    /**
    * 语种全称。
    */
    fullName?: string;
}

function safeLanguage(v: Language): Language {
    if (v) {
    }
    return v;
}

function safeLanguages(vs: Language[]): Language[] {
    if (vs) {
        return vs.map((v: Language) => {
            return safeLanguage(v);
        });
    } else {
        return [];
    }
}

export interface Layout {

    /**
    * 是否需要返回父任务，取值包括：
    *
    * 0：父任务的子任务作为独立的任务返回
    *
    * 1：所有子任务都需要打包成父任务，并在父任务内返回
    *
    * 默认取值1
    */
    childPlanLayout?: string;
}

function safeLayout(v: Layout): Layout {
    if (v) {
    }
    return v;
}

function safeLayouts(vs: Layout[]): Layout[] {
    if (vs) {
        return vs.map((v: Layout) => {
            return safeLayout(v);
        });
    } else {
        return [];
    }
}

export interface Lock {

    /**
    * 子ProfileID，表示主Profile对指定的子Profile添加独享锁。
    */
    profileID?: string;

    /**
    * 加锁的对象类型，取值包括：
    *
    * CHANNEL：频道
    *
    * SUBJECT：栏目
    *
    * VOD：点播
    *
    * PROGRAM：节目单
    *
    * SERIES：节目单连续剧
    *
    * GENRE：流派
    */
    lockType: string;

    /**
    * 加锁内容的标识。
    *
    * 如果VOD加锁，就是VOD内容ID。
    *
    * 如果对频道加锁，就是频道内容ID。
    *
    * 如果对节目单加锁，就是节目单内容ID。
    *
    * 如果对栏目加锁，就是栏目ID。
    *
    * 如果对连续剧节目单加锁，就是节目单的SeriesID。
    *
    * 如果对流派加锁，就是流派ID。
    *
    * 如果对VAS加锁，就是VAS内容ID。
    */
    itemID: string;

    /**
    * 扩展字段。
    */
    extensionFields?: NamedParameter[];
}

function safeLock(v: Lock): Lock {
    if (v) {
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeLocks(vs: Lock[]): Lock[] {
    if (vs) {
        return vs.map((v: Lock) => {
            return safeLock(v);
        });
    } else {
        return [];
    }
}

export interface LockItem {

    /**
    * 加锁的对象类型，取值包括：
    *
    * CHANNEL：频道
    *
    * SUBJECT：栏目
    *
    * VOD：点播
    *
    * PROGRAM：节目单
    *
    * SERIES：节目单连续剧
    *
    * GENRE：流派
    */
    lockType: string;

    /**
    * VOD内容描述，lockType等于VOD时返回。
    */
    VOD?: VOD;

    /**
    * channel内容描述。lockType等于CHANNEL时返回。
    */
    channel?: Channel;

    /**
    * 节目单内容描述，lockType等于SUBJECT时返回。
    */
    subject?: Subject;

    /**
    * program内容描述，lockType等于
    *
    * PROGRAM时返回。
    */
    playbill?: Playbill;

    /**
    * 内容流派信息，lockType等于GENRE时返回。
    */
    genre?: Genre;

    /**
    * 加锁的SeriesID信息。
    *
    * lockType等于SERIES时返回。
    */
    lockSeries?: LockSeries;
}

function safeLockItem(v: LockItem): LockItem {
    if (v) {
        v.VOD = safeVOD(v.VOD);
        v.channel = safeChannel(v.channel);
        v.subject = safeSubject(v.subject);
        v.playbill = safePlaybill(v.playbill);
        v.genre = safeGenre(v.genre);
        v.lockSeries = safeLockSeries(v.lockSeries);
    }
    return v;
}

function safeLockItems(vs: LockItem[]): LockItem[] {
    if (vs) {
        return vs.map((v: LockItem) => {
            return safeLockItem(v);
        });
    } else {
        return [];
    }
}

export interface LockSeries {

    /**
    * 加锁的连续剧节目单的SeriesID。
    */
    ID: string;

    /**
    * 连续剧节目单的名称。
    */
    name: string;
}

function safeLockSeries(v: LockSeries): LockSeries {
    if (v) {
    }
    return v;
}

function safeLockSeriess(vs: LockSeries[]): LockSeries[] {
    if (vs) {
        return vs.map((v: LockSeries) => {
            return safeLockSeries(v);
        });
    } else {
        return [];
    }
}

export interface MosaicChannel {

    /**
    * 马赛克频道的ID。
    */
    id: string;

    /**
    * 马赛克频道的名称。
    */
    name: string;

    /**
    * 马赛克频道的描述信息。
    */
    description?: string;

    /**
    * 马赛克频道在马赛克频道列表中的序号。
    */
    mosaicChannelIndex: string;

    /**
    * 马赛克频道背景流对应的逻辑频道ID。
    */
    bgChannelID: string;

    /**
    * 马赛克频道背景流对应的物理频道ID。
    */
    bgPhysicalChannelID: string;

    /**
    * 马赛克频道背景流对应的频道混排号。
    */
    bgChannelNo: string;

    /**
    * 马赛克频道背景流的高度，单位：像素。
    */
    bgHeight?: string;

    /**
    * 马赛克频道背景流的宽度，单位：像素。
    */
    bgWidth?: string;

    /**
    * 马赛克频道中子频道信息更新版本号，每更新一次，版本号加一。这个值溢出后再从0开始。
    */
    pipChannelsVer?: string;

    /**
     *马赛克频道布局更新版本号，每更新一次，版本号加一。这个值溢出后再从0开始。这个场景一般在门户上修改马赛克频道布局时会发生。
     */
    layoutVer?: string;

    /**
    * 实际背景流分辨率下的马赛克页面中每个子频道焦点信息。
    */
    pipChannels: MosaicConfig[];

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeMosaicChannel(v: MosaicChannel): MosaicChannel {
    if (v) {
        v.pipChannels = safeMosaicConfigs(v.pipChannels);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeMosaicChannels(vs: MosaicChannel[]): MosaicChannel[] {
    if (vs) {
        return vs.map((v: MosaicChannel) => {
            return safeMosaicChannel(v);
        });
    } else {
        return [];
    }
}

export interface MosaicConfig {

    /**
    * 实际背景流分辨率下的焦点位置（距背景流顶部的距离）。单位：像素。
    */
    top: string;

    /**
    * 实际背景流分辨率下的焦点位置（距背景流左边的距离）。单位：像素。
    */
    left: string;

    /**
    * 实际背景流分辨率下的焦点高度。单位：像素。
    */
    height: string;

    /**
    * 实际背景流分辨率下的焦点宽度。单位：像素。
    */
    width: string;

    /**
    * 焦点对应频道的音频PID。
    */
    audioPID: string;

    /**
    * 焦点对应频道号。
    */
    channelNO: string;

    /**
    * 子频道的频道ID。
    */
    channelID: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeMosaicConfig(v: MosaicConfig): MosaicConfig {
    if (v) {
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeMosaicConfigs(vs: MosaicConfig[]): MosaicConfig[] {
    if (vs) {
        return vs.map((v: MosaicConfig) => {
            return safeMosaicConfig(v);
        });
    } else {
        return [];
    }
}
export interface MyContent {

    /**
    * 内容信息。
    */
    content?: Content;

    /**
    * 内容生效时间点，取值为距离1970年1月1号的毫秒数。
    */
    startTime: string;

    /**
    * 内容失效时间点，取值为距离1970年1月1号的毫秒数。
    */
    endTime: string;

    /**
    * 订购开始时间，取值为距离1970年1月1号的毫秒数。
    */
    serStartTime: string;

    /**
    * 订购结束时间，取值为距离1970年1月1号的毫秒数。
    */
    serEndTime: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeMyContent(v: MyContent): MyContent {
    if (v) {
        v.content = safeContent(v.content);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeMyContents(vs: MyContent[]): MyContent[] {
    if (vs) {
        return vs.map((v: MyContent) => {
            return safeMyContent(v);
        });
    } else {
        return [];
    }
}

export interface MultiRights_playready {

    /**
    * client请求获取license的地址。
    */
    LA_URL: string;

    /**
    * URL of the PlayReady portal. This parameter is not used currently.
    */
    LUI_URL?: string;
}

function safeMultiRights_playready(v: MultiRights_playready): MultiRights_playready {
    if (v) {
    }
    return v;
}

function safeMultiRights_playreadys(vs: MultiRights_playready[]): MultiRights_playready[] {
    if (vs) {
        return vs.map((v: MultiRights_playready) => {
            return safeMultiRights_playready(v);
        });
    } else {
        return [];
    }
}

export interface Multirights_widevine {

    /**
    * client请求获取license的地址。
    */
    LA_URL: string;

    /**
    * PlayReady的Portal对应的URL，该参数暂时 未使用。
    */
    LUI_URL?: string;
}

function safeMultirights_widevine(v: Multirights_widevine): Multirights_widevine {
    if (v) {
    }
    return v;
}

function safeMultirights_widevines(vs: Multirights_widevine[]): Multirights_widevine[] {
    if (vs) {
        return vs.map((v: Multirights_widevine) => {
            return safeMultirights_widevine(v);
        });
    } else {
        return [];
    }
}

export interface Nagra {

    /**
    * Nagra提供的部署在云端的PVS服务器接入URL
    */
    pvsUrl: string;

    /**
    * Nagra提供的客户端获取License的接入URL
    */
    laUrl: string;
}

function safeNagra(v: Nagra): Nagra {
    if (v) {
    }
    return v;
}

function safeNagras(vs: Nagra[]): Nagra[] {
    if (vs) {
        return vs.map((v: Nagra) => {
            return safeNagra(v);
        });
    } else {
        return [];
    }
}

export interface NamedParameter {

    /**
    * 参数名称。
    */
    key: string;

    /**
    * 参数值。
    */
    values: string[];
}

function safeNamedParameter(v: NamedParameter): NamedParameter {
    if (v) {
        v.values = v.values || [];
    }
    return v;
}

function safeNamedParameters(vs: NamedParameter[]): NamedParameter[] {
    if (vs) {
        return vs.map((v: NamedParameter) => {
            return safeNamedParameter(v);
        });
    } else {
        return [];
    }
}

export interface PageTracker {

    /**
    * 表示接入应用UI。
    *
    * 对华为UI，默认数值如：HW_STB_EPGUI、HW_PC、HW_iPhone、HW_iPad、HW_AndroidPhone、HW_AndroidPad；
    *
    * 对第三方UI，由管理员在网管上分配。
    */
    type: string;

    /**
    * 表示接入应用唯一标识，格式是由6位字母和数字组成。
    */
    appID: string;

    /**
    * 表示接入应用鉴权码，格式为：识别码|验证码。识别码是终端每次登录认证成功之后系统会生成的唯一标识字符串。验证码：Base64(HMAC(sha256, 识别码,AppPWD))。
    */
    appPassword: string;

    /**
    * 是否支持实时采集用户页面浏览轨迹，取值包括：
    *
    * 0：不支持
    *
    * 1：支持
    *
    * 默认值为0。
    */
    isSupportedUserLogCollect: string;

    /**
    * 用户浏览轨迹采集服务端URL，格式形如&quot;IP:Port/VSPLogServer/LogTracker&quot;。
    */
    pageTrackerServerURL: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safePageTracker(v: PageTracker): PageTracker {
    if (v) {
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safePageTrackers(vs: PageTracker[]): PageTracker[] {
    if (vs) {
        return vs.map((v: PageTracker) => {
            return safePageTracker(v);
        });
    } else {
        return [];
    }
}

export interface Parameters {

    /**
    * 每个Profile支持的收藏上限。
    *
    * 说明
    *
    * VSP平台支持按收藏总数控制，也支持按内容类型控制每种内容的收藏上限，此参数表示收藏总数上限。
    */
    favouriteLimit: string;

    /**
    * 每个Profile支持的书签上限。
    *
    * 说明
    *
    * VSP平台支持按书签总数控制，也支持按书签类型控制每种内容的书签上限，此参数表示书签总数上限。
    */
    bookmarkLimit: string;

    /**
    * 每个订户支持的童锁上限。
    */
    lockLimit: string;

    /**
    * 用户Profile个数上限。
    */
    profileLimit: string;

    /**
    * 收藏夹分类个数。
    */
    favoCatalogLimit?: string;

    /**
    * Mashup平台地址。
    */
    mashupAddress?: string;

    /**
    * TVMS发送心跳消息的地址。
    *
    * 例如：http://ip:port/gateway/queryxml.do
    *
    * IP地址支持V4或V6.
    */
    TVMSHeartbitURL?: string;

    /**
    * TVMS发送VOD消息的地址。
    *
    * 例如：http://ip:port/gateway/queryxml.do
    *
    * IP地址支持V4或V6.
    */
    TVMSVodHeartbitURL?: string;

    /**
    * TVMS的心跳周期。
    *
    * 单位：秒。
    */
    TVMSHeartbitInterval: string;

    /**
    * TVMS的消息冲突时的展示延迟时长。
    *
    * 单位：秒。
    */
    TVMSDelayLength: string;

    /**
    * 是否支持公众广告。
    *
    * 取值范围：
    *
    * 1：支持
    *
    * 0：不支持
    */
    isSupportPublicAD: string;

    /**
    * 广告平台URL。
    *
    * IP地址支持V4或V6。
    */
    ADPlatformURL?: string;

    /**
    * 获取广告策略URL。
    *
    * IP地址支持V4或V6。
    */
    ADPublicStrategyURL?: string;

    /**
    * 广告播放完成通知地址。
    *
    * IP地址支持V4或V6。
    */
    ADPlayOverNotifyURL?: string;

    /**
    * 查看广告内容详细信息赠送多少积分。
    */
    giftLoyaltyByBrowseAD: string;

    /**
    * 通过短信接收广告信息赠送多少积分。
    */
    giftLoyaltyByReceiveADWithSMS: string;

    /**
    * 通过邮件接收广告信息赠送多少积分。
    */
    giftLoyaltyByReceiveADWithEmail: string;

    /**
    * 频道启动本地时移的延时。
    *
    * 单位：秒。
    */
    PLTVDelay: string;

    /**
    * Envision Video Platform是否支持维护DVB。
    *
    * 取值范围：
    *
    * 0：不支持
    *
    * 1：支持
    */
    DVBEnable: string;

    /**
    * IPTV终端对接的第三方业务质量管理系统地址。
    *
    * IP地址支持V4或V6。
    */
    SQMURL?: string;

    /**
    * OTT终端对接的第三方业务质量管理系统地址。
    *
    * IP地址支持V4或V6。
    */
    MQMCURL?: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeParameters(v: Parameters): Parameters {
    if (v) {
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeParameterss(vs: Parameters[]): Parameters[] {
    if (vs) {
        return vs.map((v: Parameters) => {
            return safeParameters(v);
        });
    } else {
        return [];
    }
}

export interface PaymentInfo {

    /**
    * 订购模式，取值包括：
    *
    * CASH：现金订购
    *
    * POINTS：积分订购 [字段预留]
    *
    * PREPAY：预付费订购
    *
    * OSES [字段预留]
    *
    * MOBILE [字段预留]
    *
    * STREAMYX_BILL [字段预留]
    *
    * VOUCHER：优惠劵订购 [字段预留]
    *
    * 默认值为CASH。
    */
    servicePayType?: string;

    /**
    * 促销码编号，当用户使用促销码订购时才有效。
    */
    promoCodeID?: string;

    /**
    * 如果通过手机支付，传入支付的手机号。[字段预留]
    */
    mobilePhone?: string;

    /**
    * 如果通过手机支付，输入支付验证码。[字段预留]
    */
    verifyCode?: string;

    /**
    * 产品试算价格。
    */
    discountFee?: string;

    /**
    * 账户的信用额度是否可以超额，取值包括：
    *
    * 0：不可以
    *
    * 1：可以
    *
    * 说明
    *
    * 如果取值是1，如果账户消费限额不足，则强制扣减消费限额（可以将消费限额扣减为负值）。
    *
    * 默认值是0。
    */
    canExceedQuota?: string;

    /**
    * 本参数暂时预留，功能待实现。
    *
    * 支付方式的子分类的编号，取值包括：
    *
    * CreditOrDebitCard：信用卡或储蓄卡
    *
    * Maybank2u
    *
    * CIMBClicks
    *
    * Paypal
    *
    * RHB
    *
    * iTalk
    *
    * Digibill
    */
    subServicePayType?: string;

    /**
    * 该支付交易的唯一流水号，由终端生成并传递到平台。
    */
    merchantTranID?: string;

    /**
    * 最终实际扣费的价格，产品的试算价格减去Voucher面额。
    *
    * 说明
    *
    * Voucher信息可以通过VMPortal进行查看。
    */
    finalFee?: string;

    /**
    * 第三方VoucherID.
    *
    * [字段预留]
    */
    externalVoucherID?: string;

    /**
    * 第三方Voucher面额，价格最小单位，目前仅TM使用。
    *
    * [字段预留]
    */
    externalVoucherPrice?: string;

    /**
    * 当支付方式是Streamyx Bill，表示Streamyx 的UserId。
    *
    * [字段预留]
    */
    streamyxUserID?: string;

    /**
    * 用户输入的证件类型，取值包括：
    *
    * New NRIC：新身份证号
    *
    * Old NRIC：旧身份证号
    *
    * Passport：护照
    *
    * [字段预留]
    */
    IDType?: string;

    /**
    * 用户证件信息。
    *
    * [字段预留]
    */
    IDNO?: string;

    /**
    * 消费率编号。
    *
    * [字段预留]
    */
    GSTCode?: string;

    /**
    * 消费税率。
    *
    * [字段预留]
    */
    GSTRatio?: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safePaymentInfo(v: PaymentInfo): PaymentInfo {
    if (v) {
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safePaymentInfos(vs: PaymentInfo[]): PaymentInfo[] {
    if (vs) {
        return vs.map((v: PaymentInfo) => {
            return safePaymentInfo(v);
        });
    } else {
        return [];
    }
}

export interface PeriodPVR {

    /**
    * 录制任务编号，当新增录制任务时，终端不需要携带，由平台生成。
    */
    ID?: string;

    /**
    * 任务的名称。周期录制添加时必须设置
    */
    name?: string;

    /**
    * 录制的逻辑频道ID。
    */
    channelID?: string;

    /**
    * 录制文件存储的物理介质，取值包括：
    *
    * CPVR：录制在STB侧
    *
    * NPVR：录制在网络侧
    *
    * MIXPVR：混合录制
    */
    storageType?: string;

    /**
    * 如果是Remote CPVR操作，指定录制STB的逻辑设备ID。
    */
    destDeviceID?: string;

    /**
    * CPVR的媒资ID。
    *
    * storageType为CPVR或MIXPVR时，添加时必须传入。
    */
    cpvrMediaID?: string;

    /**
    * NPVR的媒资ID。
    *
    * storageType为NPVR或MIXPVR时，添加时可以指定录制某个NPVR媒资，若不传，则代表录制NPVR的所有媒资。
    */
    npvrMediaID?: string;

    /**
    * 录制策略类型，取值包括：
    *
    * Series：系列录制父任务
    *
    * Period：周期录制父任务
    */
    policyType?: string;

    /**
    * 每周的哪几天，取值包括：
    *
    * 1：星期一
    *
    * 2：星期二
    *
    * 3：星期三
    *
    * 4：星期四
    *
    * 5：星期五
    *
    * 6：星期六
    *
    * 7：星期天
    */
    days?: string[];

    /**
    * 每天录制开始时间，格式HHmmss，必须是本地时间。
    */
    beginDayTime?: string;

    /**
    * 每天录制结束时间，格式HHmmss，必须是本地时间。
    */
    endDayTime?: string;

    /**
    * 表示录制任务的生效时间，取值为：
    *
    * 格式为YYYYMMDD，必须是本地时间，添加接口必传。
    */
    effectiveDate?: string;

    /**
    * 表示录制任务的失效时间，取值为：
    *
    * 格式为YYYYMMDD，必须是本地时间如果不传代表永久生效
    *
    * ，添加接口必传。
    *
    * 【约定】若添加永不失效的周期录制任务，或将周期录制修改成永不失效的周期录制任务，则将失效时间传入20991231
    */
    expireDate?: string;

    /**
    * 根据查询条件过滤排序后的子任务列表不为空，则返回经过排序后的第一个满足条件的子任务
    *
    * 这里实际返回的是TimeBasedPVR对象
    */
    childPVR?: PVRTaskBasic;

    /**
    * 根据查询条件过滤条件的子任务ID列表
    */
    childPVRIDs?: string[];

    /**
    * 定制属性
    */
    customizedProperty?: CustomizedProperty;

    /**
    * 周期系列父任务是否过期，取值包括：
    *
    * 0：未过期
    *
    * 1：已过期
    */
    isOverdue?: string;

    /**
    * 局点定制的扩展属性。
    */
    extensionFields?: NamedParameter[];
}

function safePeriodPVR(v: PeriodPVR): PeriodPVR {
    if (v) {
        v.days = v.days || [];
        v.childPVR = safePVRTaskBasic(v.childPVR);
        v.childPVRIDs = v.childPVRIDs || [];
        v.customizedProperty = safeCustomizedProperty(v.customizedProperty);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safePeriodPVRs(vs: PeriodPVR[]): PeriodPVR[] {
    if (vs) {
        return vs.map((v: PeriodPVR) => {
            return safePeriodPVR(v);
        });
    } else {
        return [];
    }
}

export interface PeriodicPVRFilter {

    /**
    * 周期系列父任务是否过期，取值包括：
    *
    * -1：ALL
    *
    * 0：未过期
    *
    * 1：已过期
    *
    * 默认值为-1。
    */
    isOverdue?: string;

    /**
    * 录制文件存储的物理介质，取值包括：
    *
    * CPVR：录制在STB侧
    *
    * NPVR：录制在网络侧
    *
    * MIXPVR：混合录制(预留)
    *
    * 不传代表查询所有存储位置的任务。
    */
    storageType?: string;
}

function safePeriodicPVRFilter(v: PeriodicPVRFilter): PeriodicPVRFilter {
    if (v) {
    }
    return v;
}

function safePeriodicPVRFilters(vs: PeriodicPVRFilter[]): PeriodicPVRFilter[] {
    if (vs) {
        return vs.map((v: PeriodicPVRFilter) => {
            return safePeriodicPVRFilter(v);
        });
    } else {
        return [];
    }
}

export interface PersonalDataVersion {

    /**
    * 频道版本号。
    *
    * 频道版本号格式如下：“channelversion|subscribeversion”。
    *
    * 其中， “channelversion”为频道元数据版本号，当CP操作员修改频道元数据时会修改；“subscribeversion”为用户订购关系版本号，当用户订购关系变化时会修改。
    */
    channel?: string;

    /**
    * 用户书签版本号，目前使用14位的时间表示，格式类似yyyyMMddHHmmss。
    */
    bookmark?: string;

    /**
    * 用户收藏版本号，目前使用14位的时间表示，格式类似yyyyMMddHHmmss。
    */
    favorite?: string;

    /**
    * 用户收藏前一次的版本号，目前使用14位的时间表示，格式类似yyyyMMddHHmmss。
    *
    * 用于频道缓存场景的业务逻辑处理。
    */
    preFavorite?: string;

    /**
    * 用户童锁版本号，目前使用14位的时间表示，格式类似yyyyMMddHHmmss。
    */
    lock?: string;

    /**
    * 用户童锁前一次版本号，目前使用14位的时间表示，格式类似yyyyMMddHHmmss。
    *
    * 用于频道缓存场景的业务逻辑处理。
    */
    preLock?: string;

    /**
    * 用户提醒版本号，目前使用14位的时间表示，格式类似yyyyMMddHHmmss。
    */
    reminder?: string;

    /**
    * profile版本号，目前使用Long整数表示。
    */
    profile?: string;

    /**
    * 机顶盒PVR录制文件版本号。
    */
    stbFileVersion?: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safePersonalDataVersion(v: PersonalDataVersion): PersonalDataVersion {
    if (v) {
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safePersonalDataVersions(vs: PersonalDataVersion[]): PersonalDataVersion[] {
    if (vs) {
        return vs.map((v: PersonalDataVersion) => {
            return safePersonalDataVersion(v);
        });
    } else {
        return [];
    }
}

export interface PhysicalChannel {

    /**
    * 媒体ID。
    */
    ID: string;

    /**
    * 媒资名称。
    */
    mediaName?: string;

    /**
    * 第三方系统分配的媒资Code。
    */
    code?: string;

    /**
    * 视频编码格式，取值包括：
    *
    * H.263
    *
    * H.264
    *
    * H.265
    */
    videoCodec?: string;

    /**
    * 频道带宽。
    *
    * 单位：kbps。
    */
    bitrate: string;

    /**
    * 内容帧率，取值范围(0,256]。
    */
    fps?: string;

    /**
    * 高清标清标识。
    *
    * 取值范围：
    *
    * 0：SD
    *
    * 1：HD
    *
    * 2：4K
    */
    definition: string;

    /**
    * 视频类型。取值范围：
    *
    * 2：2D
    *
    * 3：3D
    *
    * 4： 2D VR
    *
    * 5： 3D VR
    *
    * 默认值为2。
    */
    dimension: string;

    /**
    * 3D格式，该字段仅对3D内容（即dimension参数值为3）有效。取值范围：
    *
    * 1：side-by-side
    *
    * 2：top-and-bottom
    *
    * 默认值为1。
    */
    formatOf: string;

    /**
    * 频道加密信息。
    */
    channelEncrypt: ChannelEncrypt;

    /**
    * BTV业务的内容权限。
    */
    btvCR?: ContentRight;

    /**
    * 网络时移业务的内容权限。
    */
    pltvCR?: ContentRight;

    /**
    * 本地时移业务的内容权限
    */
    cpltvCR?: ContentRight;

    /**
    * CUTV业务的内容权限。
    */
    cutvCR?: ContentRight;

    /**
    * 网络录制业务的录制权限。
    */
    npvrRecordCR?: ContentRight;

    /**
    * 本地录制业务的录制权限。
    */
    cpvrRecordCR?: ContentRight;

    /**
    * 即时重播业务的内容权限
    */
    irCR?: ContentRight;

    /**
    * 媒体文件的内容格式。
    *
    * 取值范围：
    *
    * 1：TS
    *
    * 2：HLS
    *
    * 3：HSS
    *
    * 4：DASH
    *
    * 5：MPEG DASH over Flute
    *
    * 7：Flute
    *
    * 8：DASH-M-ABR
    *
    * 默认值为1。
    */
    fileFormat: string;

    /**
    * 是否支持PIP小流。
    *
    * 针对IPTV频道，表示当前物理媒资为PIP流；
    *
    * 针对OTT频道，表示取当前物理媒资的最低码率作为PIP流。
    */
    isSupportPIP: string;

    /**
    * 预览时长。
    *
    * 如果不支持取值为0。
    *
    * 单位：秒。
    */
    previewLength: string;

    /**
    * 预览次数。
    *
    * 如果不支持取值为0。
    */
    previewCount: string;

    /**
    * 频道是否支持FCC和RET。
    *
    * 取值范围：
    *
    * 0：不支持FCC和RET
    *
    * 1：支持FCC和RET
    *
    * 2：仅支持FCC
    *
    * 3：仅支持RET
    */
    fccEnable: string;

    /**
    * 频道是否支持FEC属性，取值包括：
    *
    * 0：不支持
    *
    * 1：支持
    */
    fecEnable: string;

    /**
    * 频道对应的峰值码率，只有频道（Capped VBR）才有峰值码率。
    */
    maxBitrate?: string;

    /**
    * 对于OTT多码率媒资，返回多个码率信息。
    *
    * 码率单位为kbps，同时多个码率会按照码率从大到小排序，例如一个媒资有10M、9.5M、9M、8M四种码率，则multiBitrates[]数字的取值顺序为：10000、9500、9000、8000。
    */
    multiBitrates?: string[];

    /**
    * 不允许访问的网络类型，取值包括：
    *
    * 1：wlan网络
    *
    * 2：cellular网络
    *
    * 如果没有值，表示wlan和cellular网络都可以访问。
    */
    blockedNetworks?: string[];

    /**
    * 频道媒资的扩展属性，其中扩展属性的Key由局点CMS定制。
    */
    customFields?: NamedParameter[];
}

function safePhysicalChannel(v: PhysicalChannel): PhysicalChannel {
    if (v) {
        v.channelEncrypt = safeChannelEncrypt(v.channelEncrypt);
        v.btvCR = safeContentRight(v.btvCR);
        v.pltvCR = safeContentRight(v.pltvCR);
        v.cpltvCR = safeContentRight(v.cpltvCR);
        v.cutvCR = safeContentRight(v.cutvCR);
        v.npvrRecordCR = safeContentRight(v.npvrRecordCR);
        v.cpvrRecordCR = safeContentRight(v.cpvrRecordCR);
        v.irCR = safeContentRight(v.irCR);
        v.multiBitrates = v.multiBitrates || [];
        v.blockedNetworks = v.blockedNetworks || [];
        v.customFields = safeNamedParameters(v.customFields);
    }
    return v;
}

function safePhysicalChannels(vs: PhysicalChannel[]): PhysicalChannel[] {
    if (vs) {
        return vs.map((v: PhysicalChannel) => {
            return safePhysicalChannel(v);
        });
    } else {
        return [];
    }
}

export interface PhysicalChannelDynamicProperties {

    /**
    * 媒体ID。
    */
    ID: string;

    /**
    * BTV业务的内容权限。
    */
    btvCR?: ContentRight;

    /**
    * 网络时移业务的内容权限。
    */
    pltvCR?: ContentRight;

    /**
    * 本地网络时移的内容权限
    */
    cpltvCR?: ContentRight;

    /**
    * CUTV业务的内容权限。
    */
    cutvCR?: ContentRight;

    /**
    * 网络录制业务的录制权限。
    */
    npvrRecordCR?: ContentRight;

    /**
    * 本地录制业务的录制权限。
    */
    cpvrRecordCR?: ContentRight;

    /**
    * 即时重播业务的内容权限
    */
    irCR?: ContentRight;

    /**
    * 物理频道归属的命名所有命名空间
    */
    channelNamespaces?: string[];
}

function safePhysicalChannelDynamicProperties(v: PhysicalChannelDynamicProperties): PhysicalChannelDynamicProperties {
    if (v) {
        v.btvCR = safeContentRight(v.btvCR);
        v.pltvCR = safeContentRight(v.pltvCR);
        v.cpltvCR = safeContentRight(v.cpltvCR);
        v.cutvCR = safeContentRight(v.cutvCR);
        v.npvrRecordCR = safeContentRight(v.npvrRecordCR);
        v.cpvrRecordCR = safeContentRight(v.cpvrRecordCR);
        v.irCR = safeContentRight(v.irCR);
        v.channelNamespaces = v.channelNamespaces || [];
    }
    return v;
}

function safePhysicalChannelDynamicPropertiess(vs: PhysicalChannelDynamicProperties[]): PhysicalChannelDynamicProperties[] {
    if (vs) {
        return vs.map((v: PhysicalChannelDynamicProperties) => {
            return safePhysicalChannelDynamicProperties(v);
        });
    } else {
        return [];
    }
}

export interface PhysicalChannelSet {

    /**
    * 是否支持标清。
    *
    * 0：不支持
    *
    * 1：支持
    *
    * 默认值为0。
    */
    isSupportDefinitionSD: string;

    /**
    * 是否支持高清。
    *
    * 0：不支持
    *
    * 1：支持
    *
    * 默认值为0。
    */
    isSupportDefinitionHD: string;

    /**
    * 是否支持4K。
    *
    * 0：不支持
    *
    * 1：支持
    *
    * 默认值为0。
    */
    isSupportDefinition: string;
}

function safePhysicalChannelSet(v: PhysicalChannelSet): PhysicalChannelSet {
    if (v) {
    }
    return v;
}

function safePhysicalChannelSets(vs: PhysicalChannelSet[]): PhysicalChannelSet[] {
    if (vs) {
        return vs.map((v: PhysicalChannelSet) => {
            return safePhysicalChannelSet(v);
        });
    } else {
        return [];
    }
}

export interface Picture {

    /**
    * 缩略图绝对路径。（为HTTP URL，例如，http://ip:port/VSP/jsp/image/12.jpg）。
    */
    deflates?: string[];

    /**
    * 海报绝对路径。
    */
    posters?: string[];

    /**
    * 剧照绝对路径。
    */
    stills?: string[];

    /**
    * 图标绝对路径。
    */
    icons?: string[];

    /**
    * 标题图绝对路径。
    */
    titles?: string[];

    /**
    * 广告图绝对路径。
    */
    ads?: string[];

    /**
    * 草图路径绝对路径。
    */
    drafts?: string[];

    /**
    * 背景图绝对路径。
    */
    backgrounds?: string[];

    /**
    * 频道图片绝对路径。
    */
    channelPics?: string[];

    /**
    * 频道黑白图片绝对路径。
    */
    channelBlackWhites?: string[];

    /**
    * 频道名字图片绝对路径。
    */
    channelNamePics?: string[];

    /**
    * secondary图片绝对路径。
    */
    secondarys?: string[];

    /**
    * logoBrightBg图片绝对路径。
    */
    logoBrightBgs?: string[];

    /**
    * logoDarkBg图片绝对路径。
    */
    logoDarkBgs?: string[];

    /**
    * mainWide图片绝对路径。
    */
    mainWides?: string[];

    /**
    * 拖动预览图片绝对路径。
    */
    previews?: string[];

    /**
    * secondaryWide图片绝对路径。
    */
    secondaryWides?: string[];

    /**
    * instantRestartLogo图片绝对路径。
    */
    instantRestartLogos?: string[];

    /**
    * channelFallbackWide图片绝对路径。
    */
    channelFallbackWides?: string[];

    /**
    * 其他图片的绝对路径。
    */
    others?: string[];

    /**
    * HCS备用地址。
    */
    hcsSlaveAddrs?: string[];

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safePicture(v: Picture): Picture {
    if (v) {
        v.deflates = v.deflates || [];
        v.posters = v.posters || [];
        v.stills = v.stills || [];
        v.icons = v.icons || [];
        v.titles = v.titles || [];
        v.ads = v.ads || [];
        v.drafts = v.drafts || [];
        v.backgrounds = v.backgrounds || [];
        v.channelPics = v.channelPics || [];
        v.channelBlackWhites = v.channelBlackWhites || [];
        v.channelNamePics = v.channelNamePics || [];
        v.secondarys = v.secondarys || [];
        v.logoBrightBgs = v.logoBrightBgs || [];
        v.logoDarkBgs = v.logoDarkBgs || [];
        v.mainWides = v.mainWides || [];
        v.previews = v.previews || [];
        v.secondaryWides = v.secondaryWides || [];
        v.instantRestartLogos = v.instantRestartLogos || [];
        v.channelFallbackWides = v.channelFallbackWides || [];
        v.others = v.others || [];
        v.hcsSlaveAddrs = v.hcsSlaveAddrs || [];
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safePictures(vs: Picture[]): Picture[] {
    if (vs) {
        return vs.map((v: Picture) => {
            return safePicture(v);
        });
    } else {
        return [];
    }
}

export interface Playbill {

    /**
    * 节目的内部唯一标识。
    */
    ID: string;

    /**
    * 第三方系统分配的内容Code。
    */
    code?: string;

    /**
    * 节目所属的频道。
    */
    channel: Channel;

    /**
    * 内容名称。
    *
    * 填充节目单时，填充为空字符。
    *
    * 终端侧根据需要展示自定义的字段。
    */
    name: string;

    /**
    * 内容简介。
    */
    introduce?: string;

    /**
    * 节目开始时间。
    *
    * 取值为距离1970年1月1号的毫秒数。
    */
    startTime?: string;

    /**
    * 节目结束时间点。
    *
    * 取值为距离1970年1月1号的毫秒数。
    */
    endTime?: string;

    /**
    * 观看级别。
    *
    * 填充节目单时，填充为最低级别（有业务需要时，可向平台要求配置）。
    */
    rating: Rating;

    /**
    * 节目海报路径。
    */
    picture?: Picture;

    /**
    * 是否支持录播。
    *
    * 取值范围：
    *
    * 0：不支持
    *
    * 1：支持
    */
    isCUTV: string;

    /**
    * 流派信息。
    */
    genres?: Genre[];

    /**
    * 录制状态，取值包括：
    *
    * 0：录制失败
    *
    * 1：录制成功且有效
    *
    * 对于支持录制的直播节目单，平台会返回录制成功，不支持录制的直播节目单，返回录制失败。
    */
    CUTVStatus: string;

    /**
    * 连续剧节目单属性。
    */
    playbillSeries?: PlaybillSeries;

    /**
    * 标识内容是否被加锁。
    *
    * 取值范围：
    *
    * 1：已加锁
    *
    * 0：未加锁
    */
    isLocked: string;

    /**
    * 书签观看的断点时间。
    *
    * 单位：秒。
    *
    * 说明
    *
    * 仅录制成功的CUTV节目单添加书签后才有该参数。
    */
    bookmark?: string;

    /**
    * 如果节目单添加提醒，返回提醒记录。
    */
    reminder?: Reminder;

    /**
    * 节目观看次数。
    *
    * 说明
    *
    * 当前获取录播排行总榜单的数据。
    */
    playTimes?: string;

    /**
    * 当前节目单是否为填充节目单。
    *
    * 0：普通节目单。
    *
    * 1：填充节目单
    *
    * 2：填充节目单，表示平台有普通的节目单，但是由于被过滤不能返回给终端，方便呈现，节目单里面的取值都按照填充节目单的数值进行赋值。
    *
    * 默认为普通节目单。
    */
    isFillProgram?: string;

    /**
    * 是否支持网络录制。
    *
    * 取值范围：
    *
    * 0：不支持
    *
    * 1：支持
    */
    isNPVR: string;

    /**
    * 是否支持本地录制
    *
    * 取值范围：
    *
    * 0：不支持
    *
    * 1：支持
    *
    * 默认值为1。
    */
    isCPVR?: string;

    /**
    * 节目单是否关联了录制中的PVR录制计划，取值包括：
    *
    * 0：不关联
    *
    * 1：关联节目单录制计划
    *
    * 2：关联系列录制子计划
    */
    hasRecordingPVR: string;

    /**
    * 是否支持InstantRestart。
    *
    * 取值范围：
    *
    * 0：不支持
    *
    * 1：支持
    */
    isInstantRestart: string;

    /**
    * 是否blackout，取值包括：
    *
    * 0：不支持
    *
    * 1：支持
    *
    * 默认值为1。
    */
    isBlackout?: string;
}

function safePlaybill(v: Playbill): Playbill {
    if (v) {
        v.channel = safeChannel(v.channel);
        v.rating = safeRating(v.rating);
        v.picture = safePicture(v.picture);
        v.genres = safeGenres(v.genres);
        v.playbillSeries = safePlaybillSeries(v.playbillSeries);
        v.reminder = safeReminder(v.reminder);
    }
    return v;
}

function safePlaybills(vs: Playbill[]): Playbill[] {
    if (vs) {
        return vs.map((v: Playbill) => {
            return safePlaybill(v);
        });
    } else {
        return [];
    }
}

export interface PlaybillBasedPVR {

    /**
    * 录制任务编号，当新增录制任务时，终端不需要携带，由平台生成。
    */
    ID?: string;

    /**
    * 任务的名称。新增录制任务时，可以不传此参数，由平台自动生成。
    *
    * 约束：
    *
    * 非连续剧：title是节目单名称。
    *
    * 连续剧：title是连续剧名称，subtitle是子集名称。
    *
    * 对于非连续剧的节目单录制任务，就是节目单名称title，否则就是title+’ ’+ subtitle。
    *
    * 对于系列录制任务，就是连续剧名称title。
    */
    name?: string;

    /**
    * 录制的逻辑频道ID。
    */
    channelID?: string;

    /**
    * 录制文件存储的物理介质，取值包括：
    *
    * CPVR：录制在STB侧
    *
    * NPVR：录制在网络侧
    *
    * MIXPVR：混合录制
    */
    storageType?: string;

    /**
    * 如果是Remote CPVR操作，指定录制STB的逻辑设备ID。
    */
    destDeviceID?: string;

    /**
    * CPVR的媒资ID。
    *
    * storageType为CPVR或MIXPVR时，添加时必须传入。
    */
    cpvrMediaID?: string;

    /**
    * NPVR的媒资ID。
    *
    * storageType为NPVR或MIXPVR时，添加时可以指定录制某个NPVR媒资，若不传，则代表录制NPVR的所有媒资。
    */
    npvrMediaID?: string;

    /**
    * 录制策略类型，取值包括：
    *
    * PlaybillBased：按节目单录制（包括系列任务的子任务）
    *
    * TimeBased：按时间段录制（包括周期任务的子任务）
    */
    policyType?: string;

    /**
    * 节目单ID。
    */
    playbillID?: string;

    /**
    * 节目单录制提前录制的时长
    *
    * 单位：秒
    */
    beginOffset?: string;

    /**
    * 节目单录制延后录制的时长
    *
    * 单位：秒
    */
    endOffset?: string;

    /**
    * 录制开始时间，取值为距离1970年1月1号的毫秒数。
    */
    startTime?: string;

    /**
    * 录制结束时间，取值为距离1970年1月1号的毫秒数。
    */
    endTime?: string;

    /**
    * 如果是基于节目单的PVR，返回节目单信息。
    */
    playbill?: Playbill;

    /**
    * 录制状态，取值包括：
    *
    * WAIT_RECORD：待录制
    *
    * RECORDING：录制中
    *
    * SUCCESS：录制成功
    *
    * PART_SUCCESS：录制部分成功
    *
    * FAIL：录制失败
    *
    * OTHER：其他
    */
    status?: string;

    /**
    * 如果录制失败，返回失败原因。
    */
    failReason?: string;

    /**
    * 返回录制时间。
    *
    * 单位为秒。
    */
    duration?: string;

    /**
    * 查询PVR任务时，返回高清标清标识，取值范围：
    *
    * 0：SD
    *
    * 1：HD
    *
    * 2：4K
    */
    definition?: string[];

    /**
    * 如果录制成功或者部分成功，任务是否允许播放，取值包括：
    *
    * 0：禁止播放
    *
    * 1：允许播放
    */
    canPlay?: string;

    /**
    * 任务的级别。
    */
    rating?: Rating;

    /**
    * 如果是系列任务产生的子任务，返回子任务对应的父任务ID。
    */
    parentPlanID?: string;

    /**
    * 文件列表
    */
    files?: RecordFile[];

    /**
    * 局点定制的扩展属性。
    */
    extensionFields?: NamedParameter[];
}

function safePlaybillBasedPVR(v: PlaybillBasedPVR): PlaybillBasedPVR {
    if (v) {
        v.playbill = safePlaybill(v.playbill);
        v.definition = v.definition || [];
        v.rating = safeRating(v.rating);
        v.files = safeRecordFiles(v.files);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safePlaybillBasedPVRs(vs: PlaybillBasedPVR[]): PlaybillBasedPVR[] {
    if (vs) {
        return vs.map((v: PlaybillBasedPVR) => {
            return safePlaybillBasedPVR(v);
        });
    } else {
        return [];
    }
}

export interface PlaybillDetail {

    /**
    * 节目的内部唯一标识。
    */
    ID: string;

    /**
    * 第三方系统分配的内容Code。
    */
    code?: string;

    /**
    * 节目单所属的频道。
    */
    channelDetail: ChannelDetail;

    /**
    * 内容名称。
    *
    * 填充节目单时，填充为空字符。
    *
    * 终端侧根据需要展示自定义的字段。
    */
    name: string;

    /**
    * 内容简介。
    */
    introduce?: string;

    /**
    * 节目单开始时间。
    *
    * 取值为距离1970年1月1号的毫秒数。
    */
    startTime?: string;

    /**
    * 节目结束时间点。
    *
    * 取值为距离1970年1月1号的毫秒数。
    */
    endTime?: string;

    /**
    * 观看级别。
    *
    * 填充节目单时，填充为最低级别（有业务需要时，可向平台要求配置）。
    */
    rating: Rating;

    /**
    * 节目海报路径。
    */
    picture?: Picture;

    /**
    * 是否支持录播。
    *
    * 取值范围：
    *
    * 0：不支持
    *
    * 1：支持
    */
    isCUTV: string;

    /**
    * 内容的演职员信息。
    */
    casts?: CastRole[];

    /**
    * 流派信息。
    */
    genres?: Genre[];

    /**
    * 出品国家。
    *
    * 说明：出品国家名称是平台根据当前的用户语种，查询ISO标准编码表（ISO3166-1）获取。当前支持的语种包括：
    *
    * English（默认，如果用户语种不在这几种内，返回该语种对应的国家名称）
    *
    * Chinese
    *
    * Arabic
    *
    * Hungarian
    *
    * German
    */
    countries?: ISOCode[];

    /**
    * 出品日期，格式为yyyy-MM-dd。
    */
    produceDate?: string;

    /**
    * 搜索关键字。
    */
    keyword?: string;

    /**
    * 节目类型，取值包括：
    *
    * movie：电影
    *
    * episode：连续剧子集
    *
    * program：普通节目
    */
    programType?: string;

    /**
    * 已播放次数。
    */
    visitTimes?: string;

    /**
    * 内容类别，类似Violence、Sexual等信息。
    */
    advisories?: string[];

    /**
    * 节目单的扩展属性，其中扩展属性的Key由局点CMS定制。
    */
    customFields?: NamedParameter[];

    /**
    * 录制状态，取值包括：
    *
    * 0：录制失败
    *
    * 1：录制成功且有效
    *
    * 说明
    *
    * 不支持录制的直播节目单，返回录制失败。
    *
    * 对于支持录制的直播节目单，平台根据节目单结束时间，未完成录制的返回录制失败，已完成录制的实际判断节目单录制状态。
    */
    CUTVStatus: string;

    /**
    * 录制成功的频道媒资ID，由于节目单关联的逻辑频道可以对应多个物理媒资，对于出现部分媒资录制成功、部分媒资录制失败的情况，此属性只返回录制成功的媒资ID。
    *
    * 说明
    *
    * 1\. 不支持录制的频道，返回null。
    *
    * 2\. 节目单是未完成的，也返回null。
    *
    * 3\. 完成录制的根据实际情况返回。
    */
    recordedMediaIDs?: string[];

    /**
    * 连续剧节目单属性。
    */
    playbillSeries?: PlaybillSeries;

    /**
    * 标识内容是否被加锁。
    *
    * 取值范围：
    *
    * 1：已加锁
    *
    * 0：未加锁
    */
    isLocked: string;

    /**
    * 书签观看的断点时间。
    *
    * 单位：秒。
    *
    * 说明
    *
    * 仅录制成功的CUTV节目单添加书签后才有该参数。
    */
    bookmark?: string;

    /**
    * 如果节目单被收藏，返回收藏记录。
    */
    favorite?: Favorite;

    /**
    * 如果节目单添加提醒，返回提醒记录。
    */
    reminder?: Reminder;

    /**
    * 当前节目单是否为填充节目单。
    *
    * 0：普通节目单。
    *
    * 1：填充节目单
    *
    * 2：填充节目单，表示平台有普通的节目单，但是由于被过滤不能返回给终端，方便呈现，节目单里面的取值都按照填充节目单的数值进行赋值。
    *
    * 默认为普通节目单。
    */
    isFillProgram?: string;

    /**
    * 是否支持网络录制。
    *
    * 取值范围：
    *
    * 0：不支持
    *
    * 1：支持
    */
    isNPVR: string;

    /**
    * 是否支持本地录制
    *
    * 取值范围：
    *
    * 0：不支持
    *
    * 1：支持
    *
    * 默认值为1。
    */
    isCPVR?: string;

    /**
    * 节目单是否关联了录制中的PVR录制计划，取值包括：
    *
    * 0：不关联
    *
    * 1：关联节目单录制计划
    *
    * 2：关联系列录制子计划
    */
    hasRecordingPVR: string;

    /**
    * 是否支持InstantRestart。
    *
    * 取值范围：
    *
    * 0：不支持
    *
    * 1：支持
    */
    isInstantRestart: string;

    /**
    * 是否blackout，取值包括：
    *
    * 0：不支持
    *
    * 1：支持
    *
    * 默认值为1。
    */
    isBlackout?: string;
}

function safePlaybillDetail(v: PlaybillDetail): PlaybillDetail {
    if (v) {
        v.channelDetail = safeChannelDetail(v.channelDetail);
        v.rating = safeRating(v.rating);
        v.picture = safePicture(v.picture);
        v.casts = safeCastRoles(v.casts);
        v.genres = safeGenres(v.genres);
        v.countries = safeISOCodes(v.countries);
        v.advisories = v.advisories || [];
        v.customFields = safeNamedParameters(v.customFields);
        v.recordedMediaIDs = v.recordedMediaIDs || [];
        v.playbillSeries = safePlaybillSeries(v.playbillSeries);
        v.favorite = safeFavorite(v.favorite);
        v.reminder = safeReminder(v.reminder);
    }
    return v;
}

function safePlaybillDetails(vs: PlaybillDetail[]): PlaybillDetail[] {
    if (vs) {
        return vs.map((v: PlaybillDetail) => {
            return safePlaybillDetail(v);
        });
    } else {
        return [];
    }
}

export interface PlaybillExcluder {

    /**
    * 出品国家。
    */
    countries?: string[];

    /**
    * 节目单ID。
    */
    playbillIDs?: string[];
}

function safePlaybillExcluder(v: PlaybillExcluder): PlaybillExcluder {
    if (v) {
        v.countries = v.countries || [];
        v.playbillIDs = v.playbillIDs || [];
    }
    return v;
}

function safePlaybillExcluders(vs: PlaybillExcluder[]): PlaybillExcluder[] {
    if (vs) {
        return vs.map((v: PlaybillExcluder) => {
            return safePlaybillExcluder(v);
        });
    } else {
        return [];
    }
}

export interface PlaybillFilter {

    /**
    * 对内容名称首字母进行过滤，取值对应用户语系的字母和0-9的10个数字。
    */
    initials?: string[];

    /**
    * 根据节目单的lifetimeID查询节目单。
    */
    lifetimeIDs?: string[];

    /**
    * 出品国家。
    */
    countries?: string[];

    /**
    * 流派ID。
    */
    genres?: string[];
}

function safePlaybillFilter(v: PlaybillFilter): PlaybillFilter {
    if (v) {
        v.initials = v.initials || [];
        v.lifetimeIDs = v.lifetimeIDs || [];
        v.countries = v.countries || [];
        v.genres = v.genres || [];
    }
    return v;
}

function safePlaybillFilters(vs: PlaybillFilter[]): PlaybillFilter[] {
    if (vs) {
        return vs.map((v: PlaybillFilter) => {
            return safePlaybillFilter(v);
        });
    } else {
        return [];
    }
}

export interface PlaybillLite {

    /**
    * 节目的内部唯一标识。
    */
    ID: string;

    /**
    * 节目所属的频道ID。
    */
    channelID: string;

    /**
    * 内容名称。
    *
    * 填充节目单时，填充为&quot;&quot;（终端侧根据需要展示自定义的字段）
    */
    name: string;

    /**
    * 节目单开始时间。
    *
    * 取值为距离1970年1月1号的毫秒数。
    */
    startTime?: string;

    /**
    * 节目结束时间点。取值为距离1970年1月1号的毫秒数。
    */
    endTime?: string;

    /**
    * 观看级别。
    *
    * 填充节目单时，填充为最低级别（有业务需要时，可向平台要求配置）
    */
    rating: Rating;

    /**
    * 节目海报路径。
    *
    * 参考“2.65 Picture”。
    */
    picture?: Picture;

    /**
    * 是否支持录播。
    *
    * 取值范围：
    *
    * 0：不支持
    *
    * 1：支持
    */
    isCUTV: string;

    /**
    * 录制状态，取值包括：
    *
    * 0：录制失败
    *
    * 1：录制成功且有效
    *
    * 对于支持录制的直播节目单，平台会返回录制成功，不支持录制的直播节目单，返回录制失败。
    */
    CUTVStatus: string;

    /**
    * 连续剧节目单属性。
    */
    playbillSeries?: PlaybillSeries;

    /**
    * 标识内容是否被加锁。
    *
    * 取值范围：
    *
    * 1：已加锁
    *
    * 0：未加锁
    */
    isLocked: string;

    /**
    * 提醒状态。
    *
    * 0：未添加提醒
    *
    * 1：已添加提醒
    *
    * 默认值为0.
    */
    reminderStatus?: string;

    /**
    * 推荐理由。
    *
    * 说明
    *
    * 该字段针对8.4 推荐内容时返回。可选参数。如推荐系统未返回则为空。
    */
    recmExplain?: string;

    /**
    * 节目观看次数。
    *
    * 如系统无法获取节目观看次数，默认为-1，表示次数未知。
    *
    * 说明
    *
    * 该字段针对8.4 推荐内容且推荐类型&#x3D;正在热播直播节目推荐时返回。
    */
    playTimes?: string;

    /**
    * 0：普通节目单
    *
    * 1：填充节目单
    *
    * 2：填充节目单，表示平台有普通的节目单，但是由于被过滤不能返回给终端，方便呈现，节目单里面的取值都按照填充节目单的数值进行赋值。
    *
    * 默认为普通节目单。
    */
    isFillProgram?: string;

    /**
    * 是否支持网络录制。
    *
    * 取值范围：
    *
    * 0：不支持
    *
    * 1：支持
    */
    isNPVR?: string;

    /**
    * 是否支持本地录制
    *
    * 取值范围：
    *
    * 0：不支持
    *
    * 1：支持
    *
    * 默认值为1。
    */
    isCPVR?: string;

    /**
    * 节目单是否关联了录制中的PVR录制计划，取值包括：
    *
    * 0：不关联
    *
    * 1：关联节目单录制计划
    *
    * 2：关联系列录制子计划
    */
    hasRecordingPVR?: string;

    /**
    * 是否支持InstantRestart。
    *
    * 取值范围：
    *
    * 0：不支持
    *
    * 1：支持
    */
    isInstantRestart: string;

    /**
    * 是否blackout，取值包括：
    *
    * 0：不支持
    *
    * 1：支持
    *
    * 默认值为1。
    */
    isBlackout?: string;
}

function safePlaybillLite(v: PlaybillLite): PlaybillLite {
    if (v) {
        v.rating = safeRating(v.rating);
        v.picture = safePicture(v.picture);
        v.playbillSeries = safePlaybillSeries(v.playbillSeries);
    }
    return v;
}

function safePlaybillLites(vs: PlaybillLite[]): PlaybillLite[] {
    if (vs) {
        return vs.map((v: PlaybillLite) => {
            return safePlaybillLite(v);
        });
    } else {
        return [];
    }
}

export interface PlaybillSeries {

    /**
    * 节目子集名称，当节目是连续剧时才有效。
    */
    sitcomName?: string;

    /**
    * 节目子集编号，当连续剧有父子集关系时才有效。
    */
    sitcomNO?: string;

    /**
    * 节目的季号，当节目是季播剧才有效。
    */
    seasonNO?: string;

    /**
    * 连续剧的系列标识。
    */
    seriesID?: string;

    /**
    * 节目单全局ID，如果同一个内容在几个不同的频道播放，这个ID是相同的。
    */
    lifetimeID?: string;

    /**
    * 季播剧标识。
    */
    seasonID?: string;
}

function safePlaybillSeries(v: PlaybillSeries): PlaybillSeries {
    if (v) {
    }
    return v;
}

function safePlaybillSeriess(vs: PlaybillSeries[]): PlaybillSeries[] {
    if (vs) {
        return vs.map((v: PlaybillSeries) => {
            return safePlaybillSeries(v);
        });
    } else {
        return [];
    }
}

export interface PlaybillVersion {

    /**
    * 频道ID
    */
    channelID: string;

    /**
    * 统计日期，格式为yyyyMMdd
    *
    * 注：为UTC时间
    */
    date: string;

    /**
    * 频道对应日期的节目版本号
    */
    version: string;
}

function safePlaybillVersion(v: PlaybillVersion): PlaybillVersion {
    if (v) {
    }
    return v;
}

function safePlaybillVersions(vs: PlaybillVersion[]): PlaybillVersion[] {
    if (vs) {
        return vs.map((v: PlaybillVersion) => {
            return safePlaybillVersion(v);
        });
    } else {
        return [];
    }
}

export interface Playlist {

    /**
    * 播放列表ID。
    *
    * 说明
    *
    * 新增播放列表时不需要终端携带该参数，由平台生成。
    */
    playlistID?: string;

    /**
    * 播放列表名称。
    */
    name: string;

    /**
    * 局点定制的扩展字段。
    */
    extensionFields?: NamedParameter[];
}

function safePlaylist(v: Playlist): Playlist {
    if (v) {
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safePlaylists(vs: Playlist[]): Playlist[] {
    if (vs) {
        return vs.map((v: Playlist) => {
            return safePlaylist(v);
        });
    } else {
        return [];
    }
}

export interface PlaylistContent {

    /**
    * 播放列表ID。
    */
    playlistID: string;

    /**
    * 内容ID。
    */
    contentID: string;

    /**
    * contentID对应的内容类型，取值包括：
    *
    * VOD：点播
    *
    * CHANNEL：频道
    *
    * PROGRAM：节目单
    */
    contentType: string;

    /**
    * 此VOD如果为连续剧子集，则此处记录添加内容时的连续剧父集ID
    */
    seriesID?: string;

    /**
    * 局点定制的扩展字段。
    */
    extensionFields?: NamedParameter[];
}

function safePlaylistContent(v: PlaylistContent): PlaylistContent {
    if (v) {
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safePlaylistContents(vs: PlaylistContent[]): PlaylistContent[] {
    if (vs) {
        return vs.map((v: PlaylistContent) => {
            return safePlaylistContent(v);
        });
    } else {
        return [];
    }
}

export interface Playready {

    /**
    * client请求获取license的地址。
    */
    LA_URL: string;

    /**
    * PlayReady的portal对应的URL，该参数暂时未使用。
    */
    LUI_URL?: string;
}

function safePlayready(v: Playready): Playready {
    if (v) {
    }
    return v;
}

function safePlayreadys(vs: Playready[]): Playready[] {
    if (vs) {
        return vs.map((v: Playready) => {
            return safePlayready(v);
        });
    } else {
        return [];
    }
}

export interface PlaySession {

    /**
    * 播放会话主键。
    */
    sessionKey: string;

    /**
    * 播放会话的ProfileID。
    */
    profileID: string;

    /**
    * 播放会话的逻辑设备ID。
    */
    deviceID: string;
}

function safePlaySession(v: PlaySession): PlaySession {
    if (v) {
    }
    return v;
}

function safePlaySessions(vs: PlaySession[]): PlaySession[] {
    if (vs) {
        return vs.map((v: PlaySession) => {
            return safePlaySession(v);
        });
    } else {
        return [];
    }
}

export interface PriceObject {

    /**
    * 定价对象ID。
    */
    ID: string;

    /**
    * 定价对象类型。
    *
    * 取值范围：
    *
    * 1：内容&amp;媒资特性[预留]
    *
    * 2：内容
    *
    * 3：栏目
    *
    * 4：媒资[预留]
    *
    * 5：全局特性[预留]
    */
    type: string;

    /**
    * type&#x3D;2时，表示内容对应的内容类型，具体取值包括：
    *
    * VOD：点播
    *
    * CHANNEL：频道
    *
    * PROGRAM：节目单
    */
    contentType: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safePriceObject(v: PriceObject): PriceObject {
    if (v) {
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safePriceObjects(vs: PriceObject[]): PriceObject[] {
    if (vs) {
        return vs.map((v: PriceObject) => {
            return safePriceObject(v);
        });
    } else {
        return [];
    }
}

export interface PriceObjectDetail {

    /**
    * 定价对象ID。
    */
    ID: string;

    /**
    * 定价对象类型。
    *
    * 取值范围：
    *
    * 1：内容&amp;媒资特性[预留]
    *
    * 2：内容
    *
    * 3：栏目[预留]
    *
    * 4：媒资[预留]
    *
    * 5：全局特性[预留]
    */
    type: string;

    /**
    * type&#x3D;2时，表示内容对应的内容类型，具体取值包括：
    *
    * VOD：点播
    *
    * CHANNEL：频道
    *
    * PROGRAM：节目单
    */
    contentType?: string;

    /**
    * 如果contentType&#x3D; VOD，返回VOD对象。
    */
    VOD?: VOD;

    /**
    * 如果contentType&#x3D;CHANNEL，返回Channel对象。
    */
    channel?: Channel;

    /**
    * 如果contentType&#x3D; PROGRAM，返回节目单对象。
    */
    playbill?: Playbill;
}

function safePriceObjectDetail(v: PriceObjectDetail): PriceObjectDetail {
    if (v) {
        v.VOD = safeVOD(v.VOD);
        v.channel = safeChannel(v.channel);
        v.playbill = safePlaybill(v.playbill);
    }
    return v;
}

function safePriceObjectDetails(vs: PriceObjectDetail[]): PriceObjectDetail[] {
    if (vs) {
        return vs.map((v: PriceObjectDetail) => {
            return safePriceObjectDetail(v);
        });
    } else {
        return [];
    }
}

export interface ProduceZone {

    /**
    * 出品国家或地区。
    *
    * 如果支持指定具体国家，取值为ISO3166-1双字母缩写。
    *
    * 如果指定地区，取值为从1开始编号的数字。
    */
    ID: string;

    /**
    * 出品国家（或地区）名称。
    *
    * 当前支持的语种包括：
    *
    * English（默认，如果用户语种不在这几种内，返回该语种对应的语种）
    *
    * Chinese
    *
    * Arabic
    *
    * Hungarian
    *
    * German
    *
    * 说明
    *
    * 如果系统配置的是ISO3166-1双字母缩写国家码，则根据当前的用户语种，查询ISO标准编码表获取国家名称。
    */
    name: string;
}

function safeProduceZone(v: ProduceZone): ProduceZone {
    if (v) {
    }
    return v;
}

function safeProduceZones(vs: ProduceZone[]): ProduceZone[] {
    if (vs) {
        return vs.map((v: ProduceZone) => {
            return safeProduceZone(v);
        });
    } else {
        return [];
    }
}

export interface Product {

    /**
    * 产品ID。
    */
    ID: string;

    /**
    * 产品名称。
    */
    name: string;

    /**
    * 产品简介。
    */
    introduce: string;

    /**
    * 产品价格，单位为最小单位。
    */
    price: string;

    /**
    * 可订购开始时间，取值为距离1970年1月1号的毫秒数。
    */
    startTime?: string;

    /**
    * 可订购结束时间，时间格式为距离1970年1月1号的毫秒数。
    */
    endTime?: string;

    /**
    * 产品类型，取值范围：
    *
    * 0：包周期
    *
    * 1：按次
    */
    productType: string;

    /**
    * 包周期产品的计量方式，取值包括：
    *
    * 0：包月
    *
    * 10：包多天
    *
    * 13：包多月
    *
    * 18 ：包天
    *
    * 19 ：包周
    *
    * 20 ：包多周
    */
    chargeMode?: string;

    /**
    * 包周期产品的周期长度。
    *
    * 例如，当chargeMode取值为13时表示包2月，periodLength取值就是2；如果是包月、包天或者包周，取值固定是1。
    */
    periodLength?: string;

    /**
    * 是否支持自动续订。
    *
    * 取值范围：
    *
    * 0：否
    *
    * 1：是
    */
    isAutoExtend: string;

    /**
    * 如果是包周期产品，表示是否为基础包，取值包括：
    *
    * 0：非基础包
    *
    * 1：基础包
    *
    * 终端可以单独订购基础包，但是订购非基础包前，必须先订购基础包。
    */
    isMain?: string;

    /**
    * 是否已经订购，取值范围：
    *
    * 0：未订购
    *
    * 1：已订购
    */
    isSubscribed: string;

    /**
    * 是否支持在线订购，取值包括：
    *
    * 0：不支持
    *
    * 1：支持
    */
    isOnlinePurchase: string;

    /**
    * 按次产品租用期，单位：小时。
    */
    rentPeriod?: string;

    /**
    * 如果是播放和下载鉴权接口返回的可订购按次产品，返回此按次产品适用的定价对象。
    */
    priceObject?: PriceObject;

    /**
    * 产品支持的限制条件参见“2.82 ProductRestriction”属性。
    */
    productRestriction?: ProductRestriction;

    /**
    * 产品的海报路径。
    */
    picture?: Picture;

    /**
    * 产品支持的营销策略。
    */
    marketInfo?: any;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeProduct(v: Product): Product {
    if (v) {
        v.priceObject = safePriceObject(v.priceObject);
        v.productRestriction = safeProductRestriction(v.productRestriction);
        v.picture = safePicture(v.picture);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeProducts(vs: Product[]): Product[] {
    if (vs) {
        return vs.map((v: Product) => {
            return safeProduct(v);
        });
    } else {
        return [];
    }
}

export interface ProductRestriction {

    /**
    * 网络接入方式，规则取值包括：
    *
    * 1：WLAN
    *
    * 2：Cellular
    */
    net?: Rule;

    /**
    * 区域外部编号。
    */
    areaCode?: Rule;

    /**
    * 设备型号。
    */
    deviceModel?: Rule;

    /**
    * 用户分组ID。
    */
    userGroupID?: Rule;

    /**
    * 允许接入的SP设备类型。
    */
    deviceType?: Rule;

    /**
    * 产品Definition限制条件，取值规则为：
    *
    * 0：2D+HD
    *
    * 1：2D+SD
    *
    * 2：3D
    *
    * 3：4K
    *
    * 其中，HD+3D为“0,2”， SD+3D为“1,2”。
    */
    definition?: Rule;
}

function safeProductRestriction(v: ProductRestriction): ProductRestriction {
    if (v) {
        v.net = safeRule(v.net);
        v.areaCode = safeRule(v.areaCode);
        v.deviceModel = safeRule(v.deviceModel);
        v.userGroupID = safeRule(v.userGroupID);
        v.deviceType = safeRule(v.deviceType);
        v.definition = safeRule(v.definition);
    }
    return v;
}

function safeProductRestrictions(vs: ProductRestriction[]): ProductRestriction[] {
    if (vs) {
        return vs.map((v: ProductRestriction) => {
            return safeProductRestriction(v);
        });
    } else {
        return [];
    }
}

export interface Profile {

    /**
    * Profile对象的编号。
    *
    * [ModifyProfile](#_ZH-CN_TOPIC_0055055603)接口中，此字段为必填。
    */
    ID?: string;

    /**
    * Profile名称。
    */
    name?: string;

    /**
    * 成员类型。在创建profile的时候必选，在修改profile的时候可选。
    *
    * 取值范围：
    *
    * 0：家长
    *
    * 1：成员
    */
    profileType?: string;

    /**
    * 用户角色，指用户在家庭中的角色。
    *
    * 取值范围：
    *
    * 1：Dad
    *
    * 2：Mom
    *
    * 3：Son
    *
    * 4：Daughter
    *
    * 5：Grandpa
    *
    * 6：Grandma
    *
    * 7：Maid
    *
    * 8：Driver
    *
    * 9：Other
    */
    familyRole?: string;

    /**
    * Profile简介。
    */
    introduce?: string;

    /**
    * 对于[AddProfile](#_ZH-CN_TOPIC_0055055646)接口，携带密码。
    */
    password?: string;

    /**
    * 登录帐号列表
    *
    * 注：仅支持查询接口中有值。
    */
    loginAccounts?: string[];

    /**
    * 消费额度，-1表示不限制，但是受订户信用额度的限制。
    *
    * 说明
    *
    * 这里的信用额度表示帐户的信用额度，针对每个Profile每月消费额度的限制。
    */
    quota?: string;

    /**
    * 头像地址，只需要携带文件名，如0.png。
    *
    * 说明
    *
    * VSP模板或者多屏本地内置几个图片给用户选择，服务器只是保存用户选择的本地的文件名，平台不支持终端从本地上传头像到服务器。
    */
    logoURL?: string;

    /**
    * 观看级别ID（rating），一个Profile只有一个级别。
    *
    * 当Profile的父母控制级别高于或等于内容的父母控制级别时，可正常观看内容。
    *
    * 当Profile的父母控制级别低于内容的父母控制级别时，则必须输入父母控制密码才能观看。
    *
    * 说明
    *
    * 若为游客，则取系统配置的游客Profile的ratingID。
    */
    ratingID?: string;

    /**
    * 观看级别名称。
    *
    * 说明
    *
    * 仅在查询接口4.2.4 查询Profile（JS）中有效返回。
    */
    ratingName?: string;

    /**
    * 受限的栏目ID列表，此列表中的栏目，该Profile被限制访问。
    */
    subjectIDs?: string[];

    /**
    * 受限的频道ID列表，此列表中的频道，该Profile被限制播放。
    */
    channelIDs?: string[];

    /**
    * 受限的VAS ID列表，此列表中的VAS，该Profile被限制访问。
    */
    VASIDs?: string[];

    /**
    * 是否展示TVMS消息。
    *
    * 取值范围：
    *
    * 0：不展示
    *
    * 1：展示
    *
    * 默认值为0。
    */
    isShowMessage?: string;

    /**
    * VSP模板名称，局点如果提供多种风格的VSP模板，Profile可以根据喜好切换到对应的模板。
    */
    templateName?: string;

    /**
    * 用户语种缩写
    *
    * 采用统一为ISO639-1缩写，如en。
    */
    lang?: string;

    /**
    * 用户联系方式（手机号码）。
    */
    mobilePhone?: string;

    /**
    * 是否接收短信通知。只有IPTV系统支持短信发送时才有意义。
    *
    * 取值范围：
    *
    * 0：不接收
    *
    * 1：接收
    *
    * 默认值为0。
    */
    isReceiveSMS?: string;

    /**
    * 订购产品时是否需要输入订购密码。
    *
    * 取值范围：
    *
    * 0：不需要
    *
    * 1：需要
    *
    * 默认值为1。
    */
    isNeedSubscribePIN?: string;

    /**
    * 邮箱地址。
    */
    email?: string;

    /**
    * 切换频道时客户端是否展示Info Bar。
    *
    * 取值范围：
    *
    * 0：不展示
    *
    * 1：展示
    *
    * 默认值为0。
    */
    isDisplayInfoBar?: string;

    /**
    * 默认频道列表类型。
    *
    * 取值范围：
    *
    * 1：系统频道列表
    *
    * 2：自定义频道列表
    *
    * 默认值为1。
    */
    channelListType?: string;

    /**
    * 是否支持短信提醒。只有IPTV系统支持短信发送时才有意义。
    *
    * 取值范围：
    *
    * 0：不支持
    *
    * 1：支持
    *
    * 默认值为0。
    */
    isSendSMSForReminder?: string;

    /**
    * Reminder间隔。
    *
    * 单位：分钟。
    *
    * 默认值为5。
    */
    reminderInterval?: string;

    /**
    * 提前发送Reminder的时间。
    *
    * 单位：分钟。
    *
    * 默认值为5。
    */
    leadTimeForSendReminder?: string;

    /**
    * 和deviceID配合使用。是否设置为deviceID对应的设备的默认Profile。在修改profile和查询profile的时候该参数有效。
    *
    * 0：否
    *
    * 1：是
    *
    * 默认值为0。
    *
    * 说明
    *
    * 在修改profile的时候，该属性为空 或 该属性为1且deviceID为空时，不处理设备的默认Profile。
    */
    isDefaultProfile?: string;

    /**
    * Profile绑定的设备ID。
    *
    * 说明
    *
    * 当isDefaultProfile&#x3D;1时，该属性生效。表示设置该Profile是设备的Default Profile
    */
    deviceID?: string;

    /**
    * 出生年月，格式为yyyyMMdd。
    */
    birthday?: string;

    /**
    * 用户国籍，类型定义参考ISO 3166-2，比如CN表示中国，UK表示英国。
    */
    nationality?: string;

    /**
    * 广告接收方式。
    *
    * 取值范围：
    *
    * 0：通过短信接收
    *
    * 1：通过Email接收
    *
    * 如果有多个值，使用英文逗号分隔，如果不携带，表示用户只在客户端上浏览广告。
    */
    receiveADType?: string;

    /**
    * 登录时是否需要输入Profile密码。
    *
    * 取值范围：
    *
    * 0：不输入Profile密码
    *
    * 1：输入Profile密码
    *
    * 默认值为1。
    */
    profilePINEnable?: string;

    /**
    * 是否允许多个终端同时登录。
    *
    * 取值范围：
    *
    * 0：允许
    *
    * 1：不允许
    *
    * 默认值为0。
    */
    multiscreenEnable?: string;

    /**
    * 是否支持订购，取值范围：
    *
    * 0：不支持。
    *
    * 1：支持。
    *
    * 默认值为1
    */
    purchaseEnable?: string;

    /**
    * 全局唯一的用户注册名，取值由终端用户定义。
    *
    * 如果终端用户未定义，平台默认和Profile ID取值保持一致。
    */
    loginName?: string;

    /**
    * Profile是否在线，取值范围：
    *
    * 0：不在线
    *
    * 1：在线
    *
    * 说明
    *
    * 目前只有查询Profile接口4.2.4 查询Profile（JS）的请求参数type为4时才支持返回该参数，即只有按ProfileId查询时才需要返回此参数。后续如果有其他查询类型也需要返回，再放开限制。
    */
    isOnline?: string;

    /**
    * Profile对应的订户ID。
    *
    * 在addProfile、modifyProfile接口入参中，此字段可空；在4.2.4 查询Profile（JS）接口返回的查询结果中，此字段不可空。
    */
    subscriberID?: string;

    /**
    * 用户地址。
    */
    location?: string;

    /**
    * 个性化签名。
    */
    sign?: string;

    /**
    * Profile的创建时间，取值为距离1970年1月1号的毫秒数。仅查询接口4.2.4 查询Profile（JS）支持。
    */
    createTime?: string;

    /**
    * 是否过滤掉高级别的内容，取值包括：
    *
    * 0：不过滤
    *
    * 1：过滤
    *
    * 默认值为0。
    */
    isFilterLevel?: string;

    /**
    * 是否反馈用户初始偏好，取值范围：
    *
    * 0：未反馈
    *
    * 1：反馈
    *
    * 默认值为0。
    */
    hasCollectUserPreference?: string;

    /**
    * 是否接收普通消息，取值包括：0：不接收 1：接收
    *
    * 默认值为1。
    */
    pushStatus?: string;

    /**
    * 透传的扩展字段。
    */
    customFields?: NamedParameter[];

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeProfile(v: Profile): Profile {
    if (v) {
        v.loginAccounts = v.loginAccounts || [];
        v.subjectIDs = v.subjectIDs || [];
        v.channelIDs = v.channelIDs || [];
        v.VASIDs = v.VASIDs || [];
        v.customFields = safeNamedParameters(v.customFields);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeProfiles(vs: Profile[]): Profile[] {
    if (vs) {
        return vs.map((v: Profile) => {
            return safeProfile(v);
        });
    } else {
        return [];
    }
}

export interface PromoCode {

    /**
    * 促销码。
    */
    promoCodeID: string;

    /**
    * 获取时间，取值为距离1970年1月1号的毫秒数。
    */
    actionTime?: string;

    /**
    * 获取渠道，取值包括：
    *
    * 1：订购事件
    *
    * 2：分享事件
    */
    actionType: string;

    /**
    * 使用状态，取值包括：
    *
    * 0：用户未使用
    *
    * 1：用户已使用
    */
    isUsed: string;

    /**
    * 促销码关联的优惠劵信息。
    */
    voucher: Voucher;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safePromoCode(v: PromoCode): PromoCode {
    if (v) {
        v.voucher = safeVoucher(v.voucher);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safePromoCodes(vs: PromoCode[]): PromoCode[] {
    if (vs) {
        return vs.map((v: PromoCode) => {
            return safePromoCode(v);
        });
    } else {
        return [];
    }
}

export interface PVRCondition {

    /**
    * 录制策略类型，取值包括：
    *
    * PlaybillBased：按节目单录制（包括系列子任务）
    *
    * TimeBased：按时间段录制（包括周期子任务）
    *
    * Series：系列录制父任务
    *
    * Period：周期录制父任务
    *
    * 不传代表查询所有录制任务
    */
    policyType?: string[];

    /**
    * 当policyType为PlaybillBased和TimeBased，代表查询单个录制（包含周期系列子任务）的过滤条件：
    *
    * 传空对象代表使用SinglePVRFilter的默认取值查询单个任务
    *
    * 不传代表不做任何过滤
    */
    singlePVRFilter?: SinglePVRFilter;

    /**
    * 当policyType为Series和Period时，代表查询周期系列父任务的过滤条件：
    *
    * 传空对象代表使用PeriodicPVRFilter的默认取值查询周期系列父任务
    *
    * 不传代表不做任何过滤
    */
    periodicPVRFilter?: PeriodicPVRFilter;

    /**
    * 周期系列父任务的返回策略，不传代表使用Layout的默认取值返回父任务。
    */
    layout?: Layout;
}

function safePVRCondition(v: PVRCondition): PVRCondition {
    if (v) {
        v.policyType = v.policyType || [];
        v.singlePVRFilter = safeSinglePVRFilter(v.singlePVRFilter);
        v.periodicPVRFilter = safePeriodicPVRFilter(v.periodicPVRFilter);
        v.layout = safeLayout(v.layout);
    }
    return v;
}

function safePVRConditions(vs: PVRCondition[]): PVRCondition[] {
    if (vs) {
        return vs.map((v: PVRCondition) => {
            return safePVRCondition(v);
        });
    } else {
        return [];
    }
}

export interface PVRSpace {

    /**
    * NPVR业务的空间运营模式，取值包括：
    *
    * 1：按照网络容量计算
    *
    * 2：按照录制时长计算
    *
    * 如果空间按容量计算，单位是MB。否则是秒。
    */
    spaceMode?: string;

    /**
    * 如果查询成功，返回总空间。
    */
    totalSize?: string;

    /**
    * 如果查询成功，返回已用空间。
    */
    usedSize?: string;

    /**
    * 如果查询成功，返回预约空间。
    */
    bookingSize?: string;

    /**
    * 取值为距离1970年1月1号的毫秒数：
    *
    * 若usedSize + bookingSize &gt; totalSize，此字段是离当前时间最近的一个空间不足的PVR任务的开始时间，代表从这个时间开始之后的所有录制任务预计空间都将不够。
    *
    * 若空间够，则返回0
    */
    notEnoughStartTime?: string;
}

function safePVRSpace(v: PVRSpace): PVRSpace {
    if (v) {
    }
    return v;
}

function safePVRSpaces(vs: PVRSpace[]): PVRSpace[] {
    if (vs) {
        return vs.map((v: PVRSpace) => {
            return safePVRSpace(v);
        });
    } else {
        return [];
    }
}

export interface PVRTaskBasic {

    /**
    * ID
    */
    ID?: string;

    /**
    * name
    */
    name?: string;

    /**
    * channelID
    */
    channelID?: string;

    /**
    * storageType
    */
    storageType?: string;

    /**
    * destDeviceID
    */
    destDeviceID?: string;

    /**
    * cpvrMediaID
    */
    cpvrMediaID?: string;

    /**
    * npvrMediaID
    */
    npvrMediaID?: string;

    /**
    * policyType
    */
    policyType?: string;

    /**
    * status
    */
    status?: string;

    /**
    * failReason
    */
    failReason?: string;

    /**
    * duration
    */
    duration?: string;

    /**
    * definition
    */
    definition?: string[];

    /**
    * canPlay
    */
    canPlay?: string;

    /**
    * rating
    */
    rating?: Rating;

    /**
    * parentPlanID
    */
    parentPlanID?: string;

    /**
    * files
    */
    files?: RecordFile[];

    /**
    * startTime
    */
    startTime?: string;

    /**
    * endTime
    */
    endTime?: string;

    /**
    * playbillID
    */
    playbillID?: string;

    /**
    * beginOffset
    */
    beginOffset?: string;

    /**
    * endOffset
    */
    endOffset?: string;

    /**
    * playbill
    */
    playbill?: Playbill;

    /**
    * childPVR
    */
    childPVR?: PVRTaskBasic;

    /**
    * childPVRIDs
    */
    childPVRIDs?: string[];

    /**
    * customizedProperty
    */
    customizedProperty?: CustomizedProperty;

    /**
    * isOverdue
    */
    isOverdue?: string;

    /**
    * seriesID
    */
    seriesID?: string;

    /**
    * days
    */
    days?: string[];

    /**
    * beginDayTime
    */
    beginDayTime?: string;

    /**
    * endDayTime
    */
    endDayTime?: string;

    /**
    * effectiveDate
    */
    effectiveDate?: string;

    /**
    * expireDate
    */
    expireDate?: string;

    /**
    * extensionFields
    */
    extensionFields?: NamedParameter[];
}

function safePVRTaskBasic(v: PVRTaskBasic): PVRTaskBasic {
    if (v) {
        v.definition = v.definition || [];
        v.rating = safeRating(v.rating);
        v.files = safeRecordFiles(v.files);
        v.playbill = safePlaybill(v.playbill);
        v.childPVR = safePVRTaskBasic(v.childPVR);
        v.childPVRIDs = v.childPVRIDs || [];
        v.customizedProperty = safeCustomizedProperty(v.customizedProperty);
        v.days = v.days || [];
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safePVRTaskBasics(vs: PVRTaskBasic[]): PVRTaskBasic[] {
    if (vs) {
        return vs.map((v: PVRTaskBasic) => {
            return safePVRTaskBasic(v);
        });
    } else {
        return [];
    }
}

export interface QueryChannel {

    /**
    * 栏目ID。
    *
    * 取值仅支持：
    *
    * -1
    *
    * 叶子栏目
    *
    * 其中栏目ID不传、或传-1时，表示查询所有频道，不是按照栏目递归查询频道；当栏目ID为叶子栏目时，表示获取此叶子栏目下的频道列表。
    *
    * 说明
    *
    * 如果传入了channelIDs或者channelNOs，则忽略该参数
    */
    subjectID?: string;

    /**
    * 频道ID列表，channelID和channelNOs二选一，如果这两个参数有入参取值，则channelID、channelNOs、count之外的其他查询条件全部忽略。
    *
    * 个数不超过count的限制。
    */
    channelIDs?: string[];

    /**
    * 频道号列表，channelID和channelNOs二选一，如果这两个字段取值，则channelID、channelNOs、count之外的其他查询条件全部忽略。
    *
    * 个数不超过count的限制。
    */
    channelNOs?: string[];

    /**
    * 一次查询的频道总条数。
    *
    * 不能设置为-1，调用者一定要指定获取数据的总条数，最大条数默认不超过30，最大条数可配置，超过最大条数返回错误。
    *
    * 说明
    *
    * 针对根据栏目ID查询频道列表的场景有效。
    */
    count?: string;

    /**
    * 查询的起始位置。默认值为0，表示从第一个频道开始。
    *
    * 说明
    *
    * 针对根据栏目ID查询频道列表的场景有效。
    */
    offset?: string;

    /**
    * 内容类型。
    *
    * 取值范围：
    *
    * CHANNEL
    *
    * AUDIO_CHANNEL
    *
    * VIDEO_CHANNEL
    *
    * 该参数不传，表示所有频道都进行查询。
    */
    contentType?: string;

    /**
    * 频道归属的命名空间
    */
    channelNamespace?: string;

    /**
    * 是否返回所有物理频道。
    *
    * 0：否（默认）
    *
    * 1：是
    */
    isReturnAllMedia?: string;

    /**
    * 搜索条件。
    */
    channelFilter?: ChannelFilter;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeQueryChannel(v: QueryChannel): QueryChannel {
    if (v) {
        v.channelIDs = v.channelIDs || [];
        v.channelNOs = v.channelNOs || [];
        v.channelFilter = safeChannelFilter(v.channelFilter);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeQueryChannels(vs: QueryChannel[]): QueryChannel[] {
    if (vs) {
        return vs.map((v: QueryChannel) => {
            return safeQueryChannel(v);
        });
    } else {
        return [];
    }
}

export interface QueryChannelContext {

    /**
    * 频道号。
    */
    channelNO: string;

    /**
    * preNumber大于等于0，表示获取指定channelID以前preNumber个频道。
    *
    * 默认不超过15个。
    */
    preNumber: string;

    /**
    * nextNumber大于等于0，表示获取指定channelID之后nextNumber个频道。
    *
    * 默认不超过15个。
    */
    nextNumber: string;

    /**
    * 内容类型（不传，表示所有频道都进行查询）。
    *
    * 取值范围：
    *
    * CHANNEL
    *
    * AUDIO_CHANNEL
    *
    * VIDEO_CHANNEL
    */
    contentType?: string;

    /**
    * 频道查询使用的命名空间，如果不指定，VSP将按照当前用户会话保存的名空间返回。
    */
    channelNamespace?: string;

    /**
    * 是否返回所有物理频道。
    *
    * 0：否（默认）
    *
    * 1：是
    */
    isReturnAllMedia?: string;

    /**
    * 搜索条件。
    */
    channelFilter?: ChannelFilter;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeQueryChannelContext(v: QueryChannelContext): QueryChannelContext {
    if (v) {
        v.channelFilter = safeChannelFilter(v.channelFilter);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeQueryChannelContexts(vs: QueryChannelContext[]): QueryChannelContext[] {
    if (vs) {
        return vs.map((v: QueryChannelContext) => {
            return safeQueryChannelContext(v);
        });
    } else {
        return [];
    }
}

export interface QueryDynamicRecmContent {

    /**
    * 推荐位参数。
    *
    * 说明
    *
    * 如一个页面涉及多个独立推荐位，该接口支持批量查询，分别指定每个推荐位的推荐参数。
    *
    * 推荐批量查询接口，接口响应时延/性能会随着推荐位的个数增加而降低。所以应尽量在1个接口传递较少的批量个数。
    *
    * 为保证接口性能，V6基线版本默认在1次推荐接口请求中批量查询推荐位数量最大为2(后台参数可配置)，如在1次推荐接口请求中批量查询推荐位数量超过2，则接口返回批量查询数量超过系统限制错误码。
    */
    recmScenarios: RecmScenario[];

    /**
    * 终端接入的网络类型。
    *
    * 如果为空，则表示不处理此字段
    *
    * 取值包括：
    *
    * 1: WLAN
    *
    * 2: Cellular
    */
    network?: string;

    /**
    * 推荐内容范围，该字段主要面向国内牌照方CP自行定制UI场景，在本UI推荐结果只推荐该牌照方CP编号对应的内容列表。取值VSP平台CP内部ID。
    *
    * 该字段主要应用于国内解决方案。针对海外解决方案该字段不需使用，默认为空，则不需按CP条件过滤只返回属于该CP 的内容。
    *
    * 默认为空。
    */
    CPs?: string[];

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeQueryDynamicRecmContent(v: QueryDynamicRecmContent): QueryDynamicRecmContent {
    if (v) {
        v.recmScenarios = safeRecmScenarios(v.recmScenarios);
        v.CPs = v.CPs || [];
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeQueryDynamicRecmContents(vs: QueryDynamicRecmContent[]): QueryDynamicRecmContent[] {
    if (vs) {
        return vs.map((v: QueryDynamicRecmContent) => {
            return safeQueryDynamicRecmContent(v);
        });
    } else {
        return [];
    }
}

export interface QueryPlaybill {

    /**
    * 节目类型。
    *
    * 取值范围：
    *
    * 0：直播节目，包括过期的、当前的和未来的直播节目
    *
    * 1：直播节目，包括当前的和未来的节目，不包括过期的节目
    *
    * 2：直播节目，包括历史的成功录制的Catch-up TV节目、当前的直播节目和未来的直播节目
    *
    * 3：Catch-up TV直播节目，只包含已录制成功的节目
    *
    * 4：Catch-up TV直播节目，包括历史的Catch-up TV节目（含录制成功或者录制失败）、当前的Catch-up TV直播节目和未来的Catch-up TV直播节目
    */
    type: string;

    /**
    * 获取节目单的开始时间，取值为距离1970年1月1号的毫秒数。
    *
    * 根据终端要求，此值不填充，则平台自动填充为当前时间。
    */
    startTime?: string;

    /**
    * 获取节目单的结束时间，取值为距离1970年1月1号的毫秒数。
    *
    * 根据与终端协商，此值不填充，则平台默认为基于startTime延后24小时。
    */
    endTime?: string;

    /**
    * 节目单是否全包含在startTime和 endTime区间内。
    *
    * 0：否，允许节目跨startTime或endTime。
    *
    * 1：是，只返回完全在startTime和 endTime区间内的节目，不返回跨startTime或endTime的节目。
    */
    mustIncluded?: string;

    /**
    * 一次查询的节目的总条数，不能设置为-1，调用者一定要指定获取数据的总条数，最大条数默认不超过100，最大条数可配置，超过最大条数返回错误。
    */
    count: string;

    /**
    * 查询的起始位置。默认值为0，表示从第一个节目开始查询。
    */
    offset: string;

    /**
    * 是否自动填充节目。
    *
    * 取值范围：
    *
    * 0：不填充
    *
    * 1：按照整点填充
    *
    * 默认值为0。
    *
    * 当type取值为0、1，且不支持playbillFilters和playbillExcluders时才有效。
    *
    * 例如：isFillProgram&#x3D;1，startTime&#x3D;20120305080000
    *
    * 如果7:40点到9点没有节目，则VSP构造两个节目（id规则为&quot;@@&quot;+channelID+&quot;@@&quot;+starttime(14位的年月日时分秒)），起止时间分别为7:40~8:00和8:00~900的两个节目。节目名称为&quot;&quot;(即长度为0的字符串)。
    *
    * 说明
    *
    * 1\. 平台侧有配置项，如果配置为不允许返回高级别的内容，则节目单可能被过滤，此时如果终端要求填充，则填充类型为2。
    *
    * 2\. 如果平台填充节目单，则填充节目单也参与offset、count计算。
    *
    * 3\. 只有在平台保存的节目单的范围内的查询才会返回填充节目单。比如平台保存前7天到后7天，不能通过查询返回前7天前，或者后7天后的填充节目单。
    */
    isFillProgram?: string;

    /**
    * 节目单搜索条件。
    */
    playbillFilter?: PlaybillFilter;

    /**
    * 节目单排他条件。
    *
    * 说明
    *
    * 如果playbillFilter和playbillExcluder中包含相同的条件，则以playbillFilter中包含的为准，忽略playbillExcluder中的条件。
    */
    playbillExcluder?: PlaybillExcluder;

    /**
    * 节目单排序方式。以下列方式指定排序方式：
    *
    * PLAYTIMES:ASC：按节目单点击率升序排序，PLAYTIMES:DESC：按节目单点击率降序排序
    *
    * NAME:ASC：按节目单名称升序排序，NAME:DESC：按节目单名称降序排序
    *
    * STARTTIME:ASC：按节目单开始时间升序排序，STARTTIME:DESC：按节目单开始时间降序排序，如果节目单开始时间相同，则按照节目单ID排列
    *
    * 如果不指定，按平台默认实现处理，即按照STARTTIME:ASC处理。
    */
    sortType?: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeQueryPlaybill(v: QueryPlaybill): QueryPlaybill {
    if (v) {
        v.playbillFilter = safePlaybillFilter(v.playbillFilter);
        v.playbillExcluder = safePlaybillExcluder(v.playbillExcluder);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeQueryPlaybills(vs: QueryPlaybill[]): QueryPlaybill[] {
    if (vs) {
        return vs.map((v: QueryPlaybill) => {
            return safeQueryPlaybill(v);
        });
    } else {
        return [];
    }
}

export interface QueryPlaybillContext {

    /**
    * 节目查询时间，取值为距离1970年1月1号的毫秒数。
    */
    date: string;

    /**
    * 节目类型。
    *
    * 取值范围：
    *
    * 0：直播节目，包括过期的、当前的和未来的直播节目
    *
    * 1：直播节目，包括当前的和未来的界面，不包括过期的节目
    *
    * 2：直播节目，包括历史的成功录制的Catch-up TV节目、当前的直播节目和未来的直播节目
    *
    * 3：Catch-up TV直播节目，只包含已录制成功的节目
    *
    * 4：Catch-up TV直播节目，包括历史的Catch-up TV节目（含录制成功或者录制失败）、当前的Catch-up TV直播节目和未来的Catch-up TV直播节目
    */
    type: string;

    /**
    * preNumber大于等于0，表示获取指定时间点以前preNumber个节目单；
    *
    * 取值不大于20（平台侧有配置项控制，基线默认为20）。
    */
    preNumber: string;

    /**
    * nextNumber大于等于0，表示获取指定时间点之后nextNumber个节目单；
    *
    * 取值不大于20（平台侧有配置项控制，基线默认为20）。
    */
    nextNumber: string;

    /**
    * 是否自动填充节目。
    *
    * 取值范围：
    *
    * 0：不填充
    *
    * 1：按照整点填充
    *
    * 默认值为0。
    *
    * 当type取值为0、1时才允许填充节目单。
    *
    * 例如：isFillProgram&#x3D;1，如果7:40点到9点没有节目，则VSP构造两个节目（id规则为&quot;@@&quot;+channelID+&quot;@@&quot;+starttime(14位的年月日时分秒)），起止时间分别为7:40~8:00和8点到9点的两个节目。节目名称为&quot;&quot;(即长度为0的字符串)。
    *
    * 说明
    *
    * 如果平台填充节目单，则填充节目单也参与offset/count计算。
    */
    isFillProgram?: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeQueryPlaybillContext(v: QueryPlaybillContext): QueryPlaybillContext {
    if (v) {
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeQueryPlaybillContexts(vs: QueryPlaybillContext[]): QueryPlaybillContext[] {
    if (vs) {
        return vs.map((v: QueryPlaybillContext) => {
            return safeQueryPlaybillContext(v);
        });
    } else {
        return [];
    }
}

export interface RatingSystem {

    /**
    * 体系名称。
    */
    systemName: string;

    /**
    * 体系下的级别。
    */
    ratings: Rating[];
}

function safeRatingSystem(v: RatingSystem): RatingSystem {
    if (v) {
        v.ratings = safeRatings(v.ratings);
    }
    return v;
}

function safeRatingSystems(vs: RatingSystem[]): RatingSystem[] {
    if (vs) {
        return vs.map((v: RatingSystem) => {
            return safeRatingSystem(v);
        });
    } else {
        return [];
    }
}

export interface Rating {

    /**
    * 级别ID，一般按照Age定义。
    */
    ID: string;

    /**
    * 级别外键。
    */
    code?: string;

    /**
    * 级别名称。
    */
    name?: string;
}

function safeRating(v: Rating): Rating {
    if (v) {
    }
    return v;
}

function safeRatings(vs: Rating[]): Rating[] {
    if (vs) {
        return vs.map((v: Rating) => {
            return safeRating(v);
        });
    } else {
        return [];
    }
}

export interface RecmVODFilter {

    /**
    * 流派ID。
    */
    genreIDs?: string[];

    /**
    * VOD支持的语种，为语言的ISO 639-1双字节缩写，比如en、zh。
    */
    languages?: string[];

    /**
    * 返回该内容级别以下的推荐内容。
    */
    ratingID?: string;
}

function safeRecmVODFilter(v: RecmVODFilter): RecmVODFilter {
    if (v) {
        v.genreIDs = v.genreIDs || [];
        v.languages = v.languages || [];
    }
    return v;
}

function safeRecmVODFilters(vs: RecmVODFilter[]): RecmVODFilter[] {
    if (vs) {
        return vs.map((v: RecmVODFilter) => {
            return safeRecmVODFilter(v);
        });
    } else {
        return [];
    }
}

export interface RecmScenario {

    /**
    * 推荐内容类型，取值包括：
    *
    * VOD：点播内容
    *
    * CHANNEL：频道内容
    *
    * PROGRAM：节目单内容
    *
    * 推荐业务类型为直播、回看时必填。如未传递，默认为VOD。
    *
    * 其中，推荐业务类型为直播时：
    *
    * 如推荐个性频道列表则推荐内容类型为CHANNEL；
    *
    * 如推荐正在热播节目列表、即将播放节目列表则推荐内容类型为PROGRAM；
    */
    contentType?: string;

    /**
    * 推荐业务类型，取值包括：
    *
    * VOD：点播
    *
    * BTV：直播
    *
    * CUTV：回看
    *
    * 推荐业务类型为直播、回看时必填。如未传递，默认为点播。
    */
    businessType?: string;

    /**
    * 推荐位/场景名称，具体名称由业务运营人员在视频大数据推荐系统管理页面配置。
    *
    * V6基线版本默认预制的推荐位/场景名称如下，如已预制的推荐位/场景名称不满足要求，业务运营人员可自行在视频大数据推荐系统管理页面新增：
    *
    * VOD_Top_Picks_For_You
    *
    * VOD_More_Like
    *
    * VOD_Because_You_Like
    *
    * VOD_Like_Many_of_its_Kind
    *
    * VOD_Viewers_also_Watched
    *
    * Channel_Preferred_LiveTV
    *
    * Program_Whats_On
    *
    * Program_Happy_Next
    *
    * Program_Similar_Recommendations
    *
    * Preferred_CatchupTV
    *
    * CatchupTV_Similar_Recommendations
    *
    * 说明
    *
    * 推荐位/场景名称entrance、推荐类型recmType 2个字段必须传递1个字段，如2个都传递推荐系统默认按照推荐位/场景名称entrance匹配后台配置的推荐算法策略。
    *
    * V6基线版本默认需传递推荐位/场景名称entrance字段。
    */
    entrance?: string;

    /**
    * 推荐类型，取值包括：
    *
    * 0：内容协同推荐
    *
    * 1：内容相似推荐
    *
    * 2：用户偏好推荐
    *
    * 3：用户协同推荐
    *
    * 4：用户偏好收集
    *
    * 5：正在热播节目
    *
    * 说明
    *
    * 针对用户偏好收集场景，该字段需传递4：用户偏好收集，其他UI展示推荐位场景，如已传递推荐位/场景名称entrance字段，则该字段可选不用传递。
    */
    recmType?: string;

    /**
    * 用户当前操作的内容ID。
    *
    * 推荐类型为2、3、4和5时不填，0和1时必填。
    */
    contentID?: string;

    /**
    * 用户偏好数据收集，当recType&#x3D;4（用户偏好收集）时，此参数有效。
    *
    * 用户偏好收集当前版本只支持VOD点播内容。
    */
    userPreference?: UserPreference;

    /**
    * 一次查询的总条数，最大不超过30条。
    */
    count: string;

    /**
    * 查询的起始位置。默认值为0，0表示从推荐结果的第1个开始获取。
    */
    offset: string;

    /**
     * C30新增。栏目ID，用于banner推荐场景。
     * 最大长度：128
     */
    subjectID?: string;

    /**
     * C30新增。推荐策略个数，默认：20。
     */
    rownum?: string;

    /**
     * C30新增。每个策略中推荐结果个数，默认：10。
     */
    colnum?: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeRecmScenario(v: RecmScenario): RecmScenario {
    if (v) {
        v.userPreference = safeUserPreference(v.userPreference);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeRecmScenarios(vs: RecmScenario[]): RecmScenario[] {
    if (vs) {
        return vs.map((v: RecmScenario) => {
            return safeRecmScenario(v);
        });
    } else {
        return [];
    }
}

export interface RecmContents {

    /**
    * 推荐记录总数。
    *
    * 若实际条数少于请求推荐的条目数，则返回实际条目数。
    */
    total: string;

    /**
    * 推荐内容类型，取值包括：
    *
    * VOD：点播内容
    *
    * CHANNEL：频道内容
    *
    * PROGRAM：节目单内容
    */
    recmContentType: string;

    /**
    * 点播内容描述，当推荐内容类型为VOD时有效。
    */
    recmVODs?: VOD[];

    /**
    * 频道内容描述，当推荐内容类型为CHANNEL时有效。
    */
    recmChannels?: Channel[];

    /**
    * 节目单内容描述，当推荐内容类型为PROGRAM 时有效。
    *
    * 推荐结果返回热点直播节目推荐列表时（即针对推荐位名称entrance&#x3D; Program_Whats_On或推荐类型recmType&#x3D;5：正在热播节目），推荐的直播节目单列表按照节目播放次数从高到低排序。
    */
    recmPrograms?: ChannelPlaybill[];

    /**
     * C30新增。策略名称。在个性化页面生成推荐场景下必填，其他推荐场景不填。
     * 最大长度：128
     */
    strategyName?: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeRecmContents(v: RecmContents): RecmContents {
    if (v) {
        v.recmVODs = safeVODs(v.recmVODs);
        v.recmChannels = safeChannels(v.recmChannels);
        v.recmPrograms = safeChannelPlaybills(v.recmPrograms);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeRecmContentss(vs: RecmContents[]): RecmContents[] {
    if (vs) {
        return vs.map((v: RecmContents) => {
            return safeRecmContents(v);
        });
    } else {
        return [];
    }
}

export interface RecordFile {

    /**
    * 文件ID。
    */
    fileID: string;

    /**
    * 录制文件的媒资ID。
    */
    mediaID: string;

    /**
    * 媒资的码率，取值包括：
    *
    * 0：SD
    *
    * 1：HD
    *
    * 2：4K
    */
    definition: string;

    /**
    * 媒资码率，单位是kpbs。
    */
    bitrate: string;

    /**
    * 录制文件存储的物理介质，取值包括：
    *
    * CPVR：录制在STB侧
    *
    * NPVR：录制在网络侧
    */
    storageType: string;

    /**
    * 如果是STB录制文件，表示录制任务存储的硬盘ID。该信息由在STB录制结果里上报。
    */
    diskID?: string;

    /**
    * 录制状态，取值包括：
    *
    * WAIT_RECORD：待录制
    *
    * RECORDING：录制中
    *
    * RECORDED：完成录制
    *
    * SUCCESS：录制成功
    *
    * PART_SUCCESS：录制部分成功
    *
    * FAIL：录制失败
    *
    * 备注：
    *
    * 待录制、录制中和完成录制是根据录制文件的录制时间计算出来的，录制时间要算上padding时间。
    *
    * 录制成功、部分成功和录制失败是根据STB和DM返回的结果更新。
    */
    status: string;

    /**
    * 如果录制失败，返回失败原因。
    */
    failReason?: string;

    /**
    * 如果录制成功或者部分成功,返回实际录制时间，否则返回0
    *
    * 单位为秒。
    */
    duration: string;

    /**
    * 如果录制成功或者部分成功,返回录制文件大小，否则返回0。
    *
    * 单位为MB。
    */
    fileSize: string;

    /**
    * 录制开始时间，取值为距离1970年1月1号的毫秒数。
    *
    * 如果文件状态是录制成功或者部分成功，使用STB和DM上报的真实的录制开始时间和节目单时间，得到校准后的开始时间，注意这个时间不含padding时间。
    *
    * 否则，如果是节目单录制，该时间是不含padding的时间。
    */
    beginTime: string;

    /**
    * 录制结束时间，取值为距离1970年1月1号的毫秒数。
    *
    * 如果文件状态是录制成功或者部分成功，返回STB和DM上报的真实的录制结束时间和节目单时间，得到校准后的开始时间，注意这个时间不含padding时间。
    *
    * 否则，如果是节目单录制，该时间是不含padding后的时间。
    */
    endTime: string;

    /**
    * 如果是节目单录制，表示节目单录制的提前偏移量。
    *
    * 如果文件状态是录制成功或者部分成功，平台会根据STB和DM上报的真实录制时间和节目单时间，重新计算padding时长。
    *
    * 单位：秒，默认值为0。
    */
    beginOffset: string;

    /**
    * 如果是节目单录制，表示节目单录制的延后偏移量。
    *
    * 如果文件状态是录制成功或者部分成功，平台会根据STB和DM上报的真实录制时间和节目单时间，重新计算padding时长。
    *
    * 单位：秒，默认值为0。
    */
    endOffset: string;

    /**
    * 文件是否允许播放，取值包括：
    *
    * 0：禁止播放
    *
    * 1：允许播放
    *
    * ，默认值是1。
    *
    * 对于已经完成的录制任务，默认为允许播放。如果CP操作员修改频道权限，将支持录制的改成不支持后，平台将更新任务为禁止播放。
    */
    canPlay: string;

    /**
    * 任务的级别。
    */
    rating: Rating;

    /**
    * 局点定制的扩展属性。
    */
    extensionFields?: NamedParameter[];
}

function safeRecordFile(v: RecordFile): RecordFile {
    if (v) {
        v.rating = safeRating(v.rating);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeRecordFiles(vs: RecordFile[]): RecordFile[] {
    if (vs) {
        return vs.map((v: RecordFile) => {
            return safeRecordFile(v);
        });
    } else {
        return [];
    }
}

export interface Reminder {

    /**
    * 内容ID
    */
    contentID: string;

    /**
    * 内容类型，取值包括：
    *
    * PROGRAM
    *
    * VOD
    */
    contentType: string;

    /**
    * 提醒时间，取值为距离1970年1月1号的毫秒数。
    *
    * 仅对节目单有效。如果新增提醒，如果不设置，系统会根据用户设置的提前偏移量确定提醒时间，其中用户设置的提前偏移量参见Profile对象的leadTimeForSendReminder属性。
    *
    * 如果查询提醒，参数取值非空。
    */
    reminderTime?: string;

    /**
    * 局点定制的扩展属性。
    */
    extensionFields?: NamedParameter[];
}

function safeReminder(v: Reminder): Reminder {
    if (v) {
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeReminders(vs: Reminder[]): Reminder[] {
    if (vs) {
        return vs.map((v: Reminder) => {
            return safeReminder(v);
        });
    } else {
        return [];
    }
}

export interface ResStrategyData {

    /**
    * 资源位展示类型。
    *
    * 0：视频（直接播放频道或者片花，支持用户点击后跳转）
    *
    * 1：图片（指资源位展示图片）
    *
    * 2：Widget（指资源位展示Widget小程序）
    *
    * 3：WebView（指资源位需要打开页面，呈现第三方页面的入口）
    *
    * 4：文本（资源位展示文字，支持用户点击后跳转）
    */
    resItemType: string;

    /**
    * 当数据类型是0、1、2、3时的相关静态展示URL。
    */
    contentURL?: string;

    /**
    * 当资源展示位要展示4-文字时，文字描述。
    */
    textContent?: string;

    /**
    * 用户点击资源位后，Launcher需要进行的动作。
    *
    * 0：播放视频
    *
    * 1：跳转到第三方网址
    *
    * 2：打开网页
    *
    * 3：打开应用APP
    */
    actionType: string;

    /**
    * actionType为0时，为播放视频的URL
    *
    * actionType为1或2时为，跳转链接的URL
    *
    * actionType为3时，为APK的包名
    *
    * 遵循本JSON的URL格式规范。
    */
    actionURL?: string;

    /**
    * 启动参数，触发动作类型为3时有效。
    *
    * 启动参数以键值对的形式拼装，多个参数之间以&amp;分隔，如参数1＝值1&amp;参数2＝值2&amp;参数3＝值3。
    */
    appParam?: string;

    /**
    * 此策略数据有效起始时间，取值为距离1970年1月1号的毫秒数。
    */
    validStartTime?: string;

    /**
    * 此策略数据有效截止时间，取值为距离1970年1月1号的毫秒数。
    */
    validEndTime?: string;

    /**
    * 此策略匹配数据的最后更新时间，建议下发数据后，Launcher据此排列同一个资源位上数据的展示顺序。
    */
    updateTime: string;

    /**
    * 描述信息，策略的摘要字段。
    */
    introduce?: string;

    /**
    * 返回缓存中资源位配置数据对象的扩展信息，该信息有PHM门户进行配置，通过VSP平台透传给终端
    */
    extensionInfo?: NamedParameter[];
}

function safeResStrategyData(v: ResStrategyData): ResStrategyData {
    if (v) {
        v.extensionInfo = safeNamedParameters(v.extensionInfo);
    }
    return v;
}

function safeResStrategyDatas(vs: ResStrategyData[]): ResStrategyData[] {
    if (vs) {
        return vs.map((v: ResStrategyData) => {
            return safeResStrategyData(v);
        });
    } else {
        return [];
    }
}

export interface Result {

    /**
    * 返回码。
    */
    retCode: string;

    /**
    * 描述信息。
    */
    retMsg: string;
}

function safeResult(v: Result): Result {
    if (v) {
    }
    return v;
}

function safeResults(vs: Result[]): Result[] {
    if (vs) {
        return vs.map((v: Result) => {
            return safeResult(v);
        });
    } else {
        return [];
    }
}

export interface Rule {

    /**
    * 规则类型，取值包括：
    *
    * 0：包含
    *
    * 1：排他
    */
    type: string;

    /**
    * 规则取值。
    */
    values: string[];
}

function safeRule(v: Rule): Rule {
    if (v) {
        v.values = v.values || [];
    }
    return v;
}

function safeRules(vs: Rule[]): Rule[] {
    if (vs) {
        return vs.map((v: Rule) => {
            return safeRule(v);
        });
    } else {
        return [];
    }
}

export interface SearchExcluder {

    /**
    * 流派名称。
    */
    genreName?: string;

    /**
    * 出品国家。
    */
    country?: string;

    /**
    * 父母字级别ID。
    */
    ratingID?: string;

    /**
    * 栏目ID。
    */
    subjectID?: string[];

    /**
    * Indicates whether to search for episodes of TV series.
    * The options are as follows:
    * 1: Yes
    * 0: No
    */
   subSitcomSearchFlag?: number;
}

function safeSearchExcluder(v: SearchExcluder): SearchExcluder {
    if (v) {
        v.subjectID = v.subjectID || [];
    }
    return v;
}

function safeSearchExcluders(vs: SearchExcluder[]): SearchExcluder[] {
    if (vs) {
        return vs.map((v: SearchExcluder) => {
            return safeSearchExcluder(v);
        });
    } else {
        return [];
    }
}

export interface SearchFilter {

    /**
    * 如果搜索VOD内容，指定搜索的VOD类型范围。
    *
    * 0：非电视剧
    *
    * 1：普通连续剧
    *
    * 2：季播剧父集
    *
    * 3：季播剧
    *
    * 如果为空，表示不进行此维度的过滤。
    */
    VODTypes?: string[];

    /**
    * 流派名称。
    */
    genreName?: string;

    /**
    * 出品国家的缩写或者地区编号。
    */
    produceZoneID?: string;

    /**
    * 对内容名称首字母进行过滤，取值为对应用户语系的字母和0-9的10个数字。
    */
    initial?: string;

    /**
    * 搜索节目单时(含直播节目单和回看节目单)指定节目单搜索范围，包括：
    *
    * 0：所有频道
    *
    * 1：已订购频道
    *
    * 2：未订购频道
    *
    * 3：指定频道ID
    *
    * 默认值是0，该条件不支持多值，对于已订购频道的判断逻辑和搜索已订购频道的实现一致，即只要订购了直播特性，频道就算已订购，同时已订购的判断不需要检查产品的限制条件是否满足。
    */
    channelScope?: string;

    /**
    * 如果ChannelScope&#x3D;3，指定频道ID。
    */
    channelIDs?: string[];
}

function safeSearchFilter(v: SearchFilter): SearchFilter {
    if (v) {
        v.VODTypes = v.VODTypes || [];
        v.channelIDs = v.channelIDs || [];
    }
    return v;
}

function safeSearchFilters(vs: SearchFilter[]): SearchFilter[] {
    if (vs) {
        return vs.map((v: SearchFilter) => {
            return safeSearchFilter(v);
        });
    } else {
        return [];
    }
}

export interface SeriesPVR {

    /**
    * 录制任务编号，当新增录制任务时，终端不需要携带，由平台生成。
    */
    ID?: string;

    /**
    * 任务的名称。新增录制任务时，可以不传此参数，由平台自动生成。
    *
    * 约束：
    *
    * 非连续剧：title是节目单名称。
    *
    * 连续剧：title是连续剧名称，subtitle是子集名称。
    *
    * 对于非连续剧的节目单录制任务，就是节目单名称title，否则就是title+’ ’+ subtitle。
    *
    * 对于系列录制任务，就是连续剧名称title。
    */
    name?: string;

    /**
    * 录制的逻辑频道ID。
    */
    channelID?: string;

    /**
    * 录制文件存储的物理介质，取值包括：
    *
    * CPVR：录制在STB侧
    *
    * NPVR：录制在网络侧
    *
    * MIXPVR：混合录制
    */
    storageType?: string;

    /**
    * 如果是Remote CPVR操作，指定录制STB的逻辑设备ID。
    */
    destDeviceID?: string;

    /**
    * CPVR的媒资ID。
    *
    * storageType为CPVR或MIXPVR时，添加时必须传入。
    */
    cpvrMediaID?: string;

    /**
    * NPVR的媒资ID。
    *
    * storageType为NPVR或MIXPVR时，添加时可以指定录制某个NPVR媒资，若不传，则代表录制NPVR的所有媒资。
    */
    npvrMediaID?: string;

    /**
    * 录制策略类型，取值包括：
    *
    * Series：系列录制父任务
    *
    * Period：周期录制父任务
    */
    policyType?: string;

    /**
    * 指定节目单ID。
    */
    playbillID?: string;

    /**
    * 系列录制任务ID。
    */
    seriesID?: string;

    /**
    * 节目单录制可以提前录制的时长。
    */
    beginOffset?: string;

    /**
    * 节目单录制可以延后录制的时长。
    */
    endOffset?: string;

    /**
    * 根据查询条件过滤排序后的子任务列表不为空，则返回经过排序后的第一个满足条件的子任务。
    *
    * 这里实际返回的是PlaybillBasedPVR对象。
    */
    childPVR?: PVRTaskBasic;

    /**
    * 根据查询条件过滤条件的子任务ID列表。
    */
    childPVRIDs?: string[];

    /**
    * 定制属性。
    */
    customizedProperty?: CustomizedProperty;

    /**
    * 周期系列父任务是否过期，取值包括：
    *
    * 0：未过期
    *
    * 1：已过期
    */
    isOverdue?: string;

    /**
    * 局点定制的扩展属性。
    */
    extensionFields?: NamedParameter[];
}

function safeSeriesPVR(v: SeriesPVR): SeriesPVR {
    if (v) {
        v.childPVR = safePVRTaskBasic(v.childPVR);
        v.childPVRIDs = v.childPVRIDs || [];
        v.customizedProperty = safeCustomizedProperty(v.customizedProperty);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeSeriesPVRs(vs: SeriesPVR[]): SeriesPVR[] {
    if (vs) {
        return vs.map((v: SeriesPVR) => {
            return safeSeriesPVR(v);
        });
    } else {
        return [];
    }
}

export interface SinglePVRFilter {

    /**
    * 单个任务的查询范围，取值包括：
    *
    * OnlyChild：只查周期系列子任务
    *
    * NotIncludeChild：不查周期系列子任务
    *
    * ALL：查询包括周期系列子任务在内的所有单个任务
    *
    * 默认取值为ALL。
    */
    scope?: string;

    /**
    * 周期系列父任务的ID，在scope传OnlyChild或ALL时生效，不传代表查询所有周期系列子任务。
    */
    periodicPVRID?: string;

    /**
    * 录制文件存储的物理介质，取值包括：
    *
    * CPVR：录制在STB侧
    *
    * NPVR：录制在网络侧
    *
    * 不传代表查询所有存储位置的任务
    */
    storageType?: string;

    /**
    * 录制任务状态，取值包括：
    *
    * WAIT_RECORD：待录制
    *
    * RECORDING：录制中
    *
    * OTHER：其他
    *
    * SUCCESS：录制成功
    *
    * PART_SUCCESS：录制部分成功
    *
    * FAIL：录制失败
    *
    * 支持传多个值，不传代表查询所有状态的任务
    */
    status?: string[];

    /**
    * nPVR/cPVR任务是否根据终端能力进行过滤，取值包括：
    *
    * 0：不过滤，即平台返回的PVR任务可能存在当前设备不能播放的内容。
    *
    * 1：过滤，即平台返回的PVR任务都是当前设备能播放的内容。
    *
    * 不传默认值取0。
    */
    isFilterByDevice?: string;

    /**
    * CPVR录制文件的硬盘ID。
    *
    * 此查询条件对周期系列父任务不生效。
    */
    diskID?: string[];
}

function safeSinglePVRFilter(v: SinglePVRFilter): SinglePVRFilter {
    if (v) {
        v.status = v.status || [];
        v.diskID = v.diskID || [];
    }
    return v;
}

function safeSinglePVRFilters(vs: SinglePVRFilter[]): SinglePVRFilter[] {
    if (vs) {
        return vs.map((v: SinglePVRFilter) => {
            return safeSinglePVRFilter(v);
        });
    } else {
        return [];
    }
}

export interface Sitcom {

    /**
    * 父集VOD的ID。
    */
    VODID: string;

    /**
    * 该VOD在父集中的子集号。
    */
    sitcomNO: string;
}

function safeSitcom(v: Sitcom): Sitcom {
    if (v) {
    }
    return v;
}

function safeSitcoms(vs: Sitcom[]): Sitcom[] {
    if (vs) {
        return vs.map((v: Sitcom) => {
            return safeSitcom(v);
        });
    } else {
        return [];
    }
}

export interface StrategyData {

    /**
    * 资源位ID。
    */
    resourceID: string;

    /**
    * 资源位策略匹配数据。
    */
    resStrategyDatas: ResStrategyData[];

    /**
    * 返回缓存中资源位对象的扩展信息，该信息有PHM门户进行配置，通过VSP平台透传给终端。
    */
    extensionInfo?: NamedParameter[];
}

function safeStrategyData(v: StrategyData): StrategyData {
    if (v) {
        v.resStrategyDatas = safeResStrategyDatas(v.resStrategyDatas);
        v.extensionInfo = safeNamedParameters(v.extensionInfo);
    }
    return v;
}

function safeStrategyDatas(vs: StrategyData[]): StrategyData[] {
    if (vs) {
        return vs.map((v: StrategyData) => {
            return safeStrategyData(v);
        });
    } else {
        return [];
    }
}

export interface StreamRecord {

    /**
    * 唯一索引。
    */
    taskId: string;

    /**
    * 逻辑设备ID。
    */
    deviceId: string;

    /**
    * 业务类型，取值包括：
    *
    * 0：EPG浏览
    *
    * 1：VOD
    *
    * 2：BTV(直播观看)
    *
    * 5：Cacth-up TV
    *
    * 6：CPVR(CPVR内容观看及下载)
    *
    * 8：NPVR
    *
    * 13：IR(Instant Restart)
    *
    * 17：Download
    *
    * 18：UserMosaic
    */
    businessType: string;

    /**
    * 如果是CPVR/NPVR业务，表示录制任务编号。
    */
    pvrId?: string;

    /**
    * 内容ID。
    */
    contentId?: string;

    /**
    * 如果业务类型是BTV、CPVR、VOD和IR，表示播放/录制/下载的媒资ID。
    */
    mediaId?: string;

    /**
    * 占用的WAN带宽，单位为kb/s。
    */
    wanBitrate: string;
}

function safeStreamRecord(v: StreamRecord): StreamRecord {
    if (v) {
    }
    return v;
}

function safeStreamRecords(vs: StreamRecord[]): StreamRecord[] {
    if (vs) {
        return vs.map((v: StreamRecord) => {
            return safeStreamRecord(v);
        });
    } else {
        return [];
    }
}

export interface StreamResponse {

    /**
    * 返回码。
    */
    result: Result;

    /**
    * 如果带宽申请成功，返回VSP分配的任务ID。
    */
    taskId?: string;

    /**
    * 如果带宽申请成功，返回占用的WAN带宽，单位为kb/s。
    */
    usedBitrate?: string;

    /**
    * 如果带宽申请成功且申请BTV/IR带宽，返回终端可播放/录制/下载的媒资ID。
    */
    mediaId?: string;

    /**
    * 如果带宽申请成功，返回频道是否可以做FCC，取值包括：
    *
    * 0：不支持FCC
    *
    * 1：支持FCC
    *
    * 备注：IR和CPVR不支持FCC，频道是否支持FCC依赖媒资属性和用户剩余带宽是否满足FCC带宽要求。
    */
    isFCC?: string;

    /**
    * 如果家庭带宽不足，返回低优先级的记录。
    */
    lowPriorityRecords?: StreamRecord[];
}

function safeStreamResponse(v: StreamResponse): StreamResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.lowPriorityRecords = safeStreamRecords(v.lowPriorityRecords);
    }
    return v;
}

function safeStreamResponses(vs: StreamResponse[]): StreamResponse[] {
    if (vs) {
        return vs.map((v: StreamResponse) => {
            return safeStreamResponse(v);
        });
    } else {
        return [];
    }
}

export interface Subject {

    /**
    * 栏目ID。
    */
    ID: string;

    /**
    * 栏目名称。
    */
    name: string;

    /**
    * 栏目下可以挂载的内容类型。
    *
    * 取值范围：
    *
    * VOD：点播
    *
    * AUDIO_VOD：音频点播
    *
    * VIDEO_VOD：视频点播
    *
    * CHANNEL：频道
    *
    * AUDIO_CHANNEL：音频频道
    *
    * VIDEO_CHANNEL：视频频道
    *
    * MIX：混合，任何内容都可以挂载
    *
    * PROGRAM：节目单
    */
    contentType: string;

    /**
    * 栏目简介。
    */
    introduce?: string;

    /**
    * 栏目海报信息。图片信息参考“2.65 Picture”。
    */
    picture?: Picture;

    /**
    * 是否有子栏目。
    *
    * 取值范围：
    *
    * -1：该栏目是叶子栏目
    *
    * 0：有子栏目，但是子栏目不可用
    *
    * 1：有可用的子栏目
    */
    hasChildren: string;

    /**
    * 是否已经订购。
    *
    * 取值范围：
    *
    * 0：未订购
    *
    * 1：已订购
    */
    isSubscribed: string;

    /**
    * 父栏目ID。
    *
    * 如果id为-1，这个参数无效。
    */
    parentSubjectID: string;

    /**
    * 标识栏目是否被加锁。
    *
    * 取值范围：
    *
    * 1：已加锁
    *
    * 0：未加锁
    */
    isLocked: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeSubject(v: Subject): Subject {
    if (v) {
        v.picture = safePicture(v.picture);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeSubjects(vs: Subject[]): Subject[] {
    if (vs) {
        return vs.map((v: Subject) => {
            return safeSubject(v);
        });
    } else {
        return [];
    }
}

export interface SubjectVODList {

    /**
    * 栏目信息。
    */
    subject: Subject;

    /**
    * 栏目下VOD信息列表。
    */
    VODs?: VOD[];
}

function safeSubjectVODList(v: SubjectVODList): SubjectVODList {
    if (v) {
        v.subject = safeSubject(v.subject);
        v.VODs = safeVODs(v.VODs);
    }
    return v;
}

function safeSubjectVODLists(vs: SubjectVODList[]): SubjectVODList[] {
    if (vs) {
        return vs.map((v: SubjectVODList) => {
            return safeSubjectVODList(v);
        });
    } else {
        return [];
    }
}

export interface Subscribe {

    /**
    * 订购类型，取值包括：
    *
    * 0：订购
    *
    * 1：预约订购(预留枚举值，不使用)
    *
    * 默认值为0。
    */
    subscribeType?: string;

    /**
    * 待订购的产品ID。
    */
    productID: string;

    /**
    * 如果订购按次产品或者包周期限次产品，需要传入待订购的定价对象，定价对象可以是内容、媒资、内容或媒资的业务特性，目前只支持内容。
    *
    * 说明
    *
    * 包周期限次产品可以选订多个定价对象，本次暂不提供包周期限次功能。
    */
    priceObjects?: PriceObject[];

    /**
    * 支付信息。
    */
    payment: PaymentInfo;

    /**
    * 订购包周期产品，是否支持自动续订，取值包括：
    *
    * 0：不续订
    *
    * 1：续订
    */
    isAutoExtend: string;

    /**
    * 如果待订购的产品是独享产品，产品的订购关系将绑定到此逻辑设备上，如果不指定，默认绑定到当前登录的逻辑设备上。
    */
    deviceID?: string;

    /**
    * 内容依赖的媒资ID，目前只有节目单存在依赖的直播媒资ID。
    *
    * [字段预留]
    */
    reliantMediaID?: string;

    /**
    * 订购关系的生效时间，取值为距离1970年1月1号的毫秒数，如果不指定，VSP根据产品策略自动计算生效时间。
    */
    effectiveTime?: string;

    /**
    * Qtel CGW返回的事件ID。
    *
    * [字段预留]
    */
    eventID?: string;

    /**
    * Turkcell局点，终端用户在线订购产品时，签署的Remote Sales Contract 和Pre-disclosure文件版本号，第一个数组元素是Remote Sales Contract文件版本号，第二个数据元素是Pre-disclosure文件版本号。[字段预留]
    */
    approvalVersions?: string[];

    /**
    * 终端在进行包月和按次订购时，需要按照下面样例的定义格式将内容ID、内容类型、栏目ID传递给VSP平台，。
    *
    * 数据结构中key的命名方式定义为：contentId contentType categoryId。
    *
    * contentType取值有AUDIO_VOD，VIDEO_VOD，VIDEO_CHANNEL，AUDIO_CHANNEL，WEB_CHANNEL，PROGRAM，VAS。
    *
    * 说明
    *
    * 传参样例：
    *
    * &quot;toReportInfo&quot;:[
    * {
    * &quot;key&quot;:&quot;contentId&quot;,
    * &quot;value&quot;:&quot;1&quot;
    * },
    * {
    * &quot;key&quot;:&quot;contentType&quot;,
    * &quot;value&quot;:&quot;VIDEO_VOD&quot;
    * },
    * {
    * &quot;key&quot;:&quot;categoryId&quot;,
    * &quot;value&quot;:&quot;110&quot;
    * }
    * ]
    */
    toReportInfo?: NamedParameter[];

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeSubscribe(v: Subscribe): Subscribe {
    if (v) {
        v.priceObjects = safePriceObjects(v.priceObjects);
        v.payment = safePaymentInfo(v.payment);
        v.approvalVersions = v.approvalVersions || [];
        v.toReportInfo = safeNamedParameters(v.toReportInfo);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeSubscribes(vs: Subscribe[]): Subscribe[] {
    if (vs) {
        return vs.map((v: Subscribe) => {
            return safeSubscribe(v);
        });
    } else {
        return [];
    }
}

export interface Subscription {

    /**
    * 订购关系主键。
    */
    productOrderKey: string;

    /**
    * 产品ID。
    */
    productID: string;

    /**
    * 产品名称。
    */
    productName: string;

    /**
    * 产品简介。
    */
    productDesc: string;

    /**
    * 支付方式。
    *
    * 订购模式，取值包括：
    *
    * CASH：现金订购
    *
    * POINTS：积分订购 [字段预留]
    *
    * PREPAY：预付费订购
    *
    * OSES [字段预留]
    *
    * MOBILE [字段预留]
    *
    * STREAMYX_BILL [字段预留]
    *
    * VOUCHER：优惠劵订购 [字段预留]
    *
    * 默认值为CASH。
    */
    servicePayType?: string;

    /**
    * 本参数暂时预留，功能待实现。
    *
    * 支付方式的子分类，取值范围
    *
    * CreditOrDebitCard：信用卡或储蓄卡
    *
    * Maybank2u
    *
    * CIMBClicks
    *
    * Paypal
    *
    * RHB
    *
    * iTalk
    *
    * Digibill
    */
    subServicePayType?: string;

    /**
    * 购买产品实际花费的金额/积分数。
    *
    * 当servicePayType取值为CASH，表示消费的金额，使用货币最小单位。
    *
    * 当servicePaytype取值为POINTS，表示消费的积分数。
    */
    price: string;

    /**
    * 订购关系生效时间，取值为距离1970年1月1号的毫秒数。
    */
    startTime?: string;

    /**
    * 订购关系失效时间，取值为距离1970年1月1号的毫秒数。
    */
    endTime?: string;

    /**
    * 产品类型，取值范围：
    *
    * 1：按次
    *
    * 0：包周期
    */
    productType: string;

    /**
    * 产品订购的时间，取值为距离1970年1月1号的毫秒数。
    */
    orderTime?: string;

    /**
    * 是否支持自动续订，默认值1。
    *
    * 取值范围：
    *
    * 0：否
    *
    * 1：是
    */
    isAutoExtend?: string;

    /**
    * 如果订购的是按次产品，表示实际订购的定价对象，定价对象可以是内容、媒资、内容或媒资的特性，目前只支持内容。
    */
    priceObjectDetail?: PriceObjectDetail;

    /**
    * 产品绑定的逻辑设备ID，只有非共享产品才有效。
    */
    deviceID?: string;

    /**
    * 订购该产品的Profile ID。
    */
    profileID?: string;

    /**
    * 是否为基础包，取值包括：
    *
    * 0：非基础包
    *
    * 1：基础包
    *
    * 说明：依据订购的具体产品属性而定
    */
    isMainPackage?: string;

    /**
    * 订购状态，取值包括：
    *
    * 0：订购状态
    *
    * 1：退订状态
    *
    * 说明：依据终端用户的选择而定
    */
    orderState?: string;

    /**
    * 产品支持的限制条件参见2.67-ProductRestriction属性。
    */
    restrictions?: ProductRestriction[];

    /**
    * 客户端集成的是PlayReady且用户已订购成功，下发获取CA License的触发器。
    *
    * [字段预留]
    */
    triggers?: GetLicenseTrigger[];

    /**
    * 如果是套餐产品，该属性表示用户可以选订该套餐包含的定价对象个数，如果平台未返回该属性，表示个数不限制，即订购了套餐产品后，套餐中包含的所有定价对象都可以使用。
    *
    * 如果用户已订购了此产品，该属性返回订购时产品的可选内容数量，如果操作员在订购后修改了产品的可选内容数据，该属性值不受影响。
    *
    * [字段预留]
    */
    totalChooseNum?: string;

    /**
    * 套餐产品的剩余可选内容数量，如果套餐产品未订购，取值totalChooseNum。
    *
    * [字段预留]
    */
    residualChooseNum?: string;

    /**
    * 对于已订购的包周期续订套餐产品，表示订购关系本周期的结束时间。取值为距离1970年1月1号的毫秒数。
    *
    * [字段预留]
    */
    cycleEndTime?: string;

    /**
    * 如果是套餐产品且totalChooseNum有值，该属性表示自选内容的租用期，单位为小时。
    *
    * [字段预留]
    */
    contentRentPeriod?: string;

    /**
    * 包周期产品的计量方式，取值包括：
    *
    * 0：包月
    *
    * 10：包多天
    *
    * 13 ：包多月
    *
    * 18 ：包天
    *
    * 19 ：包周
    *
    * 20 ：包多周
    */
    chargeMode?: string;

    /**
    * 包周期产品的周期长度。
    *
    * 例如，chargeMode取值为13表示包2月时，periodLength取值就是2。
    *
    * 如果是包月、包天或者包周，取值固定是1。
    */
    periodLength?: string;

    /**
    * 产品的海报路径。
    */
    picture?: Picture;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeSubscription(v: Subscription): Subscription {
    if (v) {
        v.priceObjectDetail = safePriceObjectDetail(v.priceObjectDetail);
        v.restrictions = safeProductRestrictions(v.restrictions);
        v.triggers = safeGetLicenseTriggers(v.triggers);
        v.picture = safePicture(v.picture);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeSubscriptions(vs: Subscription[]): Subscription[] {
    if (vs) {
        return vs.map((v: Subscription) => {
            return safeSubscription(v);
        });
    } else {
        return [];
    }
}

export interface Template {

    /**
    * 模板ID。
    */
    templateId: string;

    /**
    * VSC模板目录名称。
    */
    templateName: string;

    /**
    * 模板压缩包名。
    */
    fileName?: string;

    /**
    * 模板别名多语列表。
    */
    templateAlias?: string;

    /**
    * 模板描述多语列表。
    */
    templateDesc?: string;
}

function safeTemplate(v: Template): Template {
    if (v) {
    }
    return v;
}

function safeTemplates(vs: Template[]): Template[] {
    if (vs) {
        return vs.map((v: Template) => {
            return safeTemplate(v);
        });
    } else {
        return [];
    }
}

export interface Topic {

    /**
    * 专题ID
    */
    id: string;

    /**
    * 关联栏目ID。通过VMPortal创建的栏目信息，独立给专题使用，关联栏目为叶子栏目
    */
    relationSubjectId: string;

    /**
    * 模板样式ID，终端提供的模板样式包中该值已经固定，比如：PhonePad002
    */
    topicStyleId: string;

    /**
    * 各专题对应的参数集合列表
    */
    params?: NamedParameter[];

    /**
    * 透传自定义扩展字段
    */
    customFields?: NamedParameter[];
}

function safeTopic(v: Topic): Topic {
    if (v) {
        v.params = safeNamedParameters(v.params);
        v.customFields = safeNamedParameters(v.customFields);
    }
    return v;
}

function safeTopics(vs: Topic[]): Topic[] {
    if (vs) {
        return vs.map((v: Topic) => {
            return safeTopic(v);
        });
    } else {
        return [];
    }
}

export interface TimeBasedPVR {

    /**
    * 录制任务编号，当新增录制任务时，终端不需要携带，由平台生成。
    */
    ID?: string;

    /**
    * 任务的名称。时间段录制添加时必须设置
    */
    name?: string;

    /**
    * 录制的逻辑频道ID。
    */
    channelID?: string;

    /**
    * 录制文件存储的物理介质，取值包括：
    *
    * CPVR：录制在STB侧
    *
    * NPVR：录制在网络侧
    *
    * MIXPVR：混合录制
    */
    storageType?: string;

    /**
    * 如果是Remote CPVR操作，指定录制STB的逻辑设备ID。
    */
    destDeviceID?: string;

    /**
    * CPVR的媒资ID。
    *
    * storageType为CPVR或MIXPVR时，添加时必须传入。
    */
    cpvrMediaID?: string;

    /**
    * NPVR的媒资ID。
    *
    * storageType为NPVR或MIXPVR时，添加时可以指定录制某个NPVR媒资，若不传，则代表录制NPVR的所有媒资。
    */
    npvrMediaID?: string;

    /**
    * 录制策略类型，取值包括：
    *
    * PlaybillBased：按节目单录制（包括系列任务的子任务）
    *
    * TimeBased：按时间段录制（包括周期任务的子任务）
    */
    policyType?: string;

    /**
    * 录制开始时间，取值为距离1970年1月1号的毫秒数。
    */
    startTime?: string;

    /**
    * 录制结束时间，取值为距离1970年1月1号的毫秒数。
    */
    endTime?: string;

    /**
    * 录制状态，取值包括：
    *
    * WAIT_RECORD：待录制
    *
    * RECORDING：录制中
    *
    * SUCCESS：录制成功
    *
    * PART_SUCCESS：录制部分成功
    *
    * FAIL：录制失败
    *
    * OTHER：其他
    */
    status?: string;

    /**
    * 如果录制失败，返回失败原因。
    */
    failReason?: string;

    /**
    * 返回录制时间。
    *
    * 单位为秒。
    */
    duration?: string;

    /**
    * 查询PVR任务时，返回高清标清标识，取值范围：
    *
    * 0：SD
    *
    * 1：HD
    *
    * 2：4K
    */
    definition?: string[];

    /**
    * 如果录制成功或者部分成功，任务是否允许播放，取值包括：
    *
    * 0：禁止播放
    *
    * 1：允许播放
    */
    canPlay?: string;

    /**
    * 任务的级别。
    */
    rating?: Rating;

    /**
    * 如果是周期任务产生的子任务，返回子任务对应的父任务ID。
    */
    parentPlanID?: string;

    /**
    * 文件列表
    */
    files?: RecordFile[];

    /**
    * 局点定制的扩展属性。
    */
    extensionFields?: NamedParameter[];
}

function safeTimeBasedPVR(v: TimeBasedPVR): TimeBasedPVR {
    if (v) {
        v.definition = v.definition || [];
        v.rating = safeRating(v.rating);
        v.files = safeRecordFiles(v.files);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeTimeBasedPVRs(vs: TimeBasedPVR[]): TimeBasedPVR[] {
    if (vs) {
        return vs.map((v: TimeBasedPVR) => {
            return safeTimeBasedPVR(v);
        });
    } else {
        return [];
    }
}

export interface UserLoginHistoryInfo {

    /**
    * 最后一次认证成功信息。
    */
    latestSuccessItem?: UserLoginHistoryItem;

    /**
    * 本次登录时，如果检测到有该用户在另外一个设备上登录，则填充其登录时间（格式YYYY-MM-DD）和登录客户端的IP，其余信息无须填充。
    *
    * 采用数组主要是因为，可能同时有多个登录信息存在。
    */
    curValidLoginItem?: UserLoginHistoryInfo[];
}

function safeUserLoginHistoryInfo(v: UserLoginHistoryInfo): UserLoginHistoryInfo {
    if (v) {
        v.latestSuccessItem = safeUserLoginHistoryItem(v.latestSuccessItem);
        v.curValidLoginItem = safeUserLoginHistoryInfos(v.curValidLoginItem);
    }
    return v;
}

function safeUserLoginHistoryInfos(vs: UserLoginHistoryInfo[]): UserLoginHistoryInfo[] {
    if (vs) {
        return vs.map((v: UserLoginHistoryInfo) => {
            return safeUserLoginHistoryInfo(v);
        });
    } else {
        return [];
    }
}

export interface UserLoginHistoryItem {

    /**
    * 订户键值。
    */
    subscriberSN: string;

    /**
    * Profile键值。
    */
    profileSN?: string;

    /**
    * 登录日期，取值为距离1970年1月1号的毫秒数。
    */
    logindate: string;

    /**
    * 终端IP。
    */
    clientIP?: string;
}

function safeUserLoginHistoryItem(v: UserLoginHistoryItem): UserLoginHistoryItem {
    if (v) {
    }
    return v;
}

function safeUserLoginHistoryItems(vs: UserLoginHistoryItem[]): UserLoginHistoryItem[] {
    if (vs) {
        return vs.map((v: UserLoginHistoryItem) => {
            return safeUserLoginHistoryItem(v);
        });
    } else {
        return [];
    }
}

export interface UserPreference {

    /**
    * 用户偏爱的内容ID。
    */
    preferContentIDs?: string[];

    /**
    * 用户不知道是否喜欢的内容ID。
    */
    unknownContentIDs?: string[];

    /**
    * 用户不喜欢的内容ID。
    */
    dislikeContentIDs?: string[];
}

function safeUserPreference(v: UserPreference): UserPreference {
    if (v) {
        v.preferContentIDs = v.preferContentIDs || [];
        v.unknownContentIDs = v.unknownContentIDs || [];
        v.dislikeContentIDs = v.dislikeContentIDs || [];
    }
    return v;
}

function safeUserPreferences(vs: UserPreference[]): UserPreference[] {
    if (vs) {
        return vs.map((v: UserPreference) => {
            return safeUserPreference(v);
        });
    } else {
        return [];
    }
}

export interface VAS {

    /**
    * VAS的内容ID。
    */
    ID: string;

    /**
    * VAS的内容名称。
    */
    name: string;

    /**
    * VAS的海报路径。
    */
    picture?: Picture;

    /**
    * 如果VAS被收藏，返回收藏记录。
    */
    favorite?: Favorite;

    /**
    * 内容地理版权信息，两位的国家码(遵从ISO 3166-1)
    */
    locationCopyrights?: string[];
}

function safeVAS(v: VAS): VAS {
    if (v) {
        v.picture = safePicture(v.picture);
        v.favorite = safeFavorite(v.favorite);
        v.locationCopyrights = v.locationCopyrights || [];
    }
    return v;
}

function safeVASs(vs: VAS[]): VAS[] {
    if (vs) {
        return vs.map((v: VAS) => {
            return safeVAS(v);
        });
    } else {
        return [];
    }
}

export interface VASDetail {

    /**
    * VAS 编号。
    */
    ID: string;

    /**
    * 第三方系统分配的内容Code。
    */
    code?: string;

    /**
    * 返回SP操作员设置的系统频道号。
    *
    * 只有QueryScheduledVAS接口才返回该属性。
    */
    channelNO?: string;

    /**
    * 内容名称。
    */
    name: string;

    /**
    * 内容简介。
    */
    introduce?: string;

    /**
    * VAS地址，通过该URL可以跳转到第三方增值业务平台。
    */
    URL: string;

    /**
    * 观看级别。
    */
    rating: Rating;

    /**
    * 是否已订购。
    *
    * 取值范围：
    *
    * 0：未订购
    *
    * 1：已订购
    */
    isSubscribed: string;

    /**
    * 海报图片路径。
    */
    picture?: Picture;

    /**
    * VAS生效时间。
    *
    * 取值为距离1970年1月1号的毫秒数。
    */
    startTime: string;

    /**
    * VAS失效时间。
    *
    * 取值为距离1970年1月1号的毫秒数。
    */
    endTime: string;

    /**
    * VAS关联的栏目ID。
    */
    subjectIDs?: string[];

    /**
    * VAS的流派信息。
    */
    genres?: Genre[];

    /**
    * 内容对应的CP设备类型。
    */
    deviceType?: DeviceType[];

    /**
    * 如果VAS被收藏，返回收藏记录。
    */
    favorite?: Favorite;

    /**
    * 标识内容是否被加锁。
    *
    * 取值范围：
    *
    * 1：已加锁
    *
    * 0：未加锁
    */
    isLocked: string;

    /**
    * 内容地理版权信息，两位的国家码(遵从ISO 3166-1)
    */
    locationCopyrights?: string[];

    /**
    * VAS的自定义扩展属性，其中扩展属性的Key由局点CMS定制。
    */
    customFields?: NamedParameter[];
}

function safeVASDetail(v: VASDetail): VASDetail {
    if (v) {
        v.rating = safeRating(v.rating);
        v.picture = safePicture(v.picture);
        v.subjectIDs = v.subjectIDs || [];
        v.genres = safeGenres(v.genres);
        v.deviceType = safeDeviceTypes(v.deviceType);
        v.favorite = safeFavorite(v.favorite);
        v.locationCopyrights = v.locationCopyrights || [];
        v.customFields = safeNamedParameters(v.customFields);
    }
    return v;
}

function safeVASDetails(vs: VASDetail[]): VASDetail[] {
    if (vs) {
        return vs.map((v: VASDetail) => {
            return safeVASDetail(v);
        });
    } else {
        return [];
    }
}

export interface Verimatrix {

    /**
    * 申请VeriMatrix系统License时提供的公司信息，用于机顶盒与VeriMatrix系统的通信认证。
    */
    company: string;

    /**
    * VCI服务器的IP地址/域名，IPTV终端访问VMX CA的VCI地址，该服务器用于管理Client设备和服务器之间的安全通信接口。
    *
    * IP地址支持V4或V6，平台根据终端接入的网络地址类型返回对应的服务器地址。
    */
    serverAddr: string;

    /**
    * VCI服务使用的业务端口，IPTV终端访问VMX CA的VCI端口。
    */
    serverPort: string;

    /**
    * VKS服务器的IP地址/域名，IPTV终端访问VMX CA的VKS地址，该服务器用于生成并管理频道密钥。
    *
    * IP地址支持V4或V6，平台根据终端接入的网络地址类型返回对应的服务器地址。
    */
    VKSAddr: string;

    /**
    * CSM的IP地址，OTT终端访问VMX CA的地址，VMX3.1以上版本才支持返回。
    *
    * 该服务器用于设备的认证和授权，密钥的生成，管理和下发。
    *
    * IP地址支持V4或V6，平台根据终端接入的网络地址类型返回对应的服务器地址。
    */
    CSMIP?: string;

    /**
    * CSM的端口，OTT终端访问VMX CA的端口，VMX3.1以上版本才支持返回。
    */
    CSMPort?: string;

    /**
    * Verimatrix提供的Widevine服务器。
    */
    multirights_widevine?: Multirights_widevine;

    /**
    * Verimatrix提供的Playready服务器。
    */
    multiRights_playready?: MultiRights_playready;
}

function safeVerimatrix(v: Verimatrix): Verimatrix {
    if (v) {
        v.multirights_widevine = safeMultirights_widevine(v.multirights_widevine);
        v.multiRights_playready = safeMultiRights_playready(v.multiRights_playready);
    }
    return v;
}

function safeVerimatrixs(vs: Verimatrix[]): Verimatrix[] {
    if (vs) {
        return vs.map((v: Verimatrix) => {
            return safeVerimatrix(v);
        });
    } else {
        return [];
    }
}

export interface VOD {

    /**
    * 点播内容ID。
    */
    ID: string;

    /**
    * 第三方系统分配的内容Code。
    */
    code?: string;

    /**
    * 关联内容唯一标识
    */
    rMediaCode?: string;

    /**
    * 点播内容名称，根据Profile的当前语种返回对应语种的名称。
    */
    name: string;

    /**
    * 点播内容类型。
    *
    * 取值范围：
    *
    * AUDIO_VOD：音频点播
    *
    * VIDEO _VOD：视频点播
    */
    contentType: string;

    /**
    * 内容简介。
    */
    introduce?: string;

    /**
    * 海报路径。
    */
    picture?: Picture;

    /**
    * 内容的演职员信息。
    *
    * CastRole类型请参见“2.15 CastRole”。
    */
    castRoles?: CastRole[];

    /**
    * VOD类型。
    *
    * 取值范围：
    *
    * 0：非电视剧
    *
    * 1：普通连续剧
    *
    * 2：季播剧父集
    *
    * 3：季播剧
    */
    VODType: string;

    /**
    * 如果VODType是连续剧/季播剧的类型，字段必填，取值包括：
    *
    * 0：普通连续剧
    *
    * 1：Movie Series
    */
    seriesType?: string;

    /**
    * 子集总集数。
    *
    * 当VODType为1（连续剧）返回连续剧下面的子集总集数。
    *
    * 当VODType为2（季播剧父集）返回连续剧下面的季播剧总集数。
    *
    * 当VODType为3（季播剧）时，返回季播剧下面的子集数。
    */
    episodeCount?: string;

    /**
    * 当VODType&#x3D;2季播剧父集时，返回连续剧所有季里面的子集总数。
    */
    episodeTotalCount?: string;

    /**
    * 父集列表，当VOD是子集或者季播剧时有效。
    */
    series?: Sitcom[];

    /**
    * 内容价格。
    *
    * 最小货币单位。
    */
    price?: string;

    /**
    * VOD类别信息。
    */
    genres?: Genre[];

    /**
    * VOD媒体文件列表。
    */
    mediaFiles?: VODMediaFile[];

    /**
    * 内容的剩余观看时长，即用户鉴权通过的订购关系失效时间距离当前时间的间隔。
    *
    * 如果用户未订购该内容返回0。
    *
    * 单位：秒。
    */
    rentPeriod: string;

    /**
    * 观看级别。
    */
    rating: Rating;

    /**
    * VOD上线时间，取值为距离1970年1月1号的毫秒数。
    */
    startTime: string;

    /**
    * VOD下线时间。
    *
    * 取值为距离1970年1月1号的毫秒数。
    */
    endTime: string;

    /**
    * 如果VOD被收藏，返回收藏记录。
    */
    favorite?: Favorite;

    /**
    * 标识内容是否被加锁。
    *
    * 取值范围：
    *
    * 1：已加锁
    *
    * 0：未加锁
    */
    isLocked: string;

    /**
    * 书签。
    */
    bookmark?: Bookmark;

    /**
    * 是否已订购。
    *
    * 取值范围：
    *
    * 0：未订购
    *
    * 1：订购
    */
    isSubscribed: string;

    /**
    * VOD出品日期，格式为yyyyMMdd。
    */
    produceDate?: string;

    /**
    * VOD被评价次数。
    */
    scoreTimes: string;

    /**
    * VOD被评价的均值。
    *
    * 保留到小数点后一位。
    */
    averageScore: string;

    /**
    * VOD支持的音轨语种，为语言的ISO 639-1双字节缩写。
    */
    audioLanguages?: ISOCode[];

    /**
    * 出品国家信息。
    */
    produceZone?: ProduceZone;

    /**
    * 已播放次数。
    */
    visitTimes?: string;

    /**
    * 字幕的语言，为ISO 639-1双字母缩写。
    */
    subtitleLanguages?: ISOCode[];

    /**
    * 内容类别，类似Violence、Sexual等信息。
    */
    advisories?: string[];

    /**
    * VOD的扩展属性，其中扩展属性的Key由局点CMS定制。
    */
    customFields?: NamedParameter[];

    /**
    * 发行公司名。
    */
    companyName?: string;

    /**
    * 推荐理由。
    *
    * 说明
    *
    * 该字段针对8.4 推荐内容时返回。如推荐系统未返回则为空。
    */
    recmExplain?: string;

    /**
    * 用户对该VOD的评分值，如果用户已评分，取值范围是1-10，否则取值是0。
    */
    userScore: string;

    /**
    * 用户对该内容的点赞总数。
    */
    likes?: string;

    /**
    * 当前登录的用户是否点赞
    *
    * 0:当前未点赞
    *
    * 1:当前用户已点赞
    */
    isLike?: string;

    /**
    * 内容归属的内容提供商 ID
    */
    cpId?: string;

    /**
    * 内容地理版权信息，两位的国家码(遵从ISO 3166-1)
    */
    locationCopyrights?: string[];

    /**
    * 预计上映日期，用于没有正片的VOD
    *
    * [V6R1C20未实现]
    */
    airDate?: string;
}

function safeVOD(v: VOD): VOD {
    if (v) {
        v.picture = safePicture(v.picture);
        v.castRoles = safeCastRoles(v.castRoles);
        v.series = safeSitcoms(v.series);
        v.genres = safeGenres(v.genres);
        v.mediaFiles = safeVODMediaFiles(v.mediaFiles);
        v.rating = safeRating(v.rating);
        v.favorite = safeFavorite(v.favorite);
        v.bookmark = safeBookmark(v.bookmark);
        v.audioLanguages = safeISOCodes(v.audioLanguages);
        v.produceZone = safeProduceZone(v.produceZone);
        v.subtitleLanguages = safeISOCodes(v.subtitleLanguages);
        v.advisories = v.advisories || [];
        v.customFields = safeNamedParameters(v.customFields);
        v.locationCopyrights = v.locationCopyrights || [];
    }
    return v;
}

function safeVODs(vs: VOD[]): VOD[] {
    if (vs) {
        return vs.map((v: VOD) => {
            return safeVOD(v);
        });
    } else {
        return [];
    }
}

export interface VODDetail {

    /**
    * 点播内容ID。
    */
    ID: string;

    /**
    * 第三方系统分配的内容Code。
    */
    code?: string;

    /**
    * 关联内容唯一标识
    */
    rMediaCode?: string;

    /**
    * 点播内容名称，根据Profile的当前语种返回对应语种的名称。
    */
    name: string;

    /**
    * 点播内容类型。
    *
    * 取值范围：
    *
    * AUDIO_VOD：音频点播
    *
    * VIDEO _VOD：视频点播
    */
    contentType: string;

    /**
    * 内容简介。
    */
    introduce?: string;

    /**
    * 海报路径。
    */
    picture?: Picture;

    /**
    * 内容的演职员信息。
    *
    * CastRole类型请参见“2.15 CastRole”
    */
    castRoles?: CastRole[];

    /**
    * VOD类型。
    *
    * 取值范围：
    *
    * 0：非电视剧
    *
    * 1：普通连续剧
    *
    * 2：季播剧父集
    *
    * 3：季播剧
    */
    VODType: string;

    /**
    * 如果VODType是连续剧/季播剧的类型，字段必填，取值包括：
    *
    * 0：普通连续剧
    *
    * 1：Movie Series
    */
    seriesType?: string;

    /**
    * 如果是连续剧（包括：普通连续剧/季播剧父集/季播剧）的话，需要返回子集列表。
    */
    episodes?: Episode[];

    /**
    * 子集总集数。
    *
    * 当VODType为1（连续剧）返回连续剧下面的子集总集数。
    *
    * 当VODType为2（季播剧父集）返回连续剧下面的季播剧总集数。
    *
    * 当VODType为3（季播剧）时，返回季播剧下面的子集数。
    */
    episodeCount?: string;

    /**
    * 当VODType&#x3D;2季播剧父集时，返回连续剧所有季里面的子集总数。
    */
    episodeTotalCount?: string;

    /**
    * 如果VOD是季播剧，返回和VOD归属父集下的所有季播剧，包含当前季。
    *
    * 比如老友记分为第一季、第二季、等等，如果该VOD是第一季，此参数返回的就是第一季、第二季等。
    *
    * 说明
    *
    * 如果季播剧属于多个父集，返回其归属第一个父集的所有季播剧。
    */
    brotherSeasonVODs?: BrotherSeasonVOD[];

    /**
    * 父集列表，当VOD是子集或者季播剧时有效。
    *
    * Sitcom类型请参见“2.107 Sitcom”。
    */
    series?: Sitcom[];

    /**
    * 内容价格。
    *
    * 最小货币单位。
    */
    price?: string;

    /**
    * VOD类别信息。
    */
    genres?: Genre[];

    /**
    * VOD媒体文件列表。
    *
    * VODMediaFile请参见“2.128 VODMediaFile”。
    */
    mediaFiles?: VODMediaFile[];

    /**
    * 片花媒体文件列表，正片有片花该值才有效。
    *
    * VODMediaFile请参见“2.128 VODMediaFile”。
    */
    clipfiles?: VODMediaFile[];

    /**
    * 背景音乐媒体文件。
    *
    * VODMediaFile请参见“2.128 VODMediaFile”。
    */
    bgMusicFile?: VODMediaFile;

    /**
    * 多章节信息。
    *
    * Chapter类型请参见“2.24 Chapter”。
    */
    chapters?: Chapter[];

    /**
    * 内容的剩余观看时长，即用户鉴权通过的订购关系失效时间距离当前时间的间隔。
    *
    * 如果用户未订购该内容返回0。
    *
    * 单位：秒。
    */
    rentPeriod: string;

    /**
    * 观看级别。
    */
    rating: Rating;

    /**
    * VOD上线时间，取值为距离1970年1月1号的毫秒数。
    */
    startTime: string;

    /**
    * VOD下线时间，取值为距离1970年1月1号的毫秒数。
    */
    endTime: string;

    /**
    * 如果VOD被收藏，返回收藏记录。
    */
    favorite?: Favorite;

    /**
    * 标识内容是否被加锁。
    *
    * 取值范围：
    *
    * 1：已加锁
    *
    * 0：未加锁
    */
    isLocked: string;

    /**
    * 书签。
    */
    bookmark?: Bookmark;

    /**
    * 如果点播添加提醒，返回提醒记录。[V6R1C20未实现]
    */
    reminder?: Reminder;

    /**
    * 是否已订购。
    *
    * 取值范围：
    *
    * 0：未订购
    *
    * 1：订购
    */
    isSubscribed: string;

    /**
    * VOD鉴权通过的产品类型，取值范围：
    *
    * 0：包周期订购
    *
    * 1：按次订购
    *
    * 2：未订购
    */
    subscriptionType: string;

    /**
    * VOD出品日期，格式为yyyyMMdd。
    */
    produceDate?: string;

    /**
    * VOD被评价次数。
    */
    scoreTimes: string;

    /**
    * VOD被评价的均值。
    *
    * 保留到小数点后一位
    */
    averageScore: string;

    /**
    * 积分价格。
    *
    * 此参数仅在支持积分消费时才有意义，仅供页面展示用，实际订购时需要消费的积分从订购的产品信息中获取。
    */
    loyaltyCount?: string;

    /**
    * VOD支持的音轨语种，为语言的ISO 639-1双字节缩写。
    */
    audioLanguages?: ISOCode[];

    /**
    * 出品国家信息。
    */
    produceZone?: ProduceZone;

    /**
    * 观看Credit VOD需要赠送的积分数。
    */
    creditVODSendLoyalty?: string;

    /**
    * 已播放次数。
    */
    visitTimes?: string;

    /**
    * 字幕的语言，为ISO 639-1双字母缩写。
    */
    subtitleLanguages?: ISOCode[];

    /**
    * 该VOD的获奖情况。
    */
    awards?: string;

    /**
    * 内容对应的设备分组。
    *
    * DeviceType类型请参见“2.37 DeviceType”。
    */
    deviceTypes?: DeviceType[];

    /**
    * 关键字。
    */
    keyword?: string;

    /**
    * 内容类别，类似Violence、Sexual等信息。
    */
    advisories?: string[];

    /**
    * VOD是否存在当前终端能播放的正片，取值包括：
    *
    * 0：不存在
    *
    * 1：存在
    *
    * 说明
    *
    * 1.如果playable&#x3D;1,mediafiles表示当前终端可播放的正片媒资。
    *
    * 2.如果playable&#x3D;0且查询VOD详情接口的filterType&#x3D;1，将返回VOD所有正片媒资。
    */
    playable: string;

    /**
    * 关联的叶子栏目ID。
    */
    subjectIDs?: string[];

    /**
    * VOD的扩展属性，其中扩展属性的Key由局点CMS定制。
    */
    customFields?: NamedParameter[];

    /**
    * 发行公司名。
    */
    companyName?: string;

    /**
    * 用户对该VOD的评分值，如果用户已评分，取值范围是1-10，否则取值是0。
    */
    userScore: string;

    /**
    * 内容地理版权信息，两位的国家码(遵从ISO 3166-1)
    */
    locationCopyrights?: string[];

    /**
    * 内容归属的内容提供商 ID
    */
    cpId?: string;

    /**
    * 用户对该内容的点赞总数。
    */
    likes?: string;

    /**
    * 当前登录的用户是否点赞
    *
    * 0:当前未点赞
    *
    * 1:当前用户已点赞
    */
    isLike?: string;

    /**
    * 预计上映日期，用于没有正片的VOD
    */
    airDate?: string;
}

function safeVODDetail(v: VODDetail): VODDetail {
    if (v) {
        v.picture = safePicture(v.picture);
        v.castRoles = safeCastRoles(v.castRoles);
        v.episodes = safeEpisodes(v.episodes);
        v.brotherSeasonVODs = safeBrotherSeasonVODs(v.brotherSeasonVODs);
        v.series = safeSitcoms(v.series);
        v.genres = safeGenres(v.genres);
        v.mediaFiles = safeVODMediaFiles(v.mediaFiles);
        v.clipfiles = safeVODMediaFiles(v.clipfiles);
        v.bgMusicFile = safeVODMediaFile(v.bgMusicFile);
        v.chapters = safeChapters(v.chapters);
        v.rating = safeRating(v.rating);
        v.favorite = safeFavorite(v.favorite);
        v.bookmark = safeBookmark(v.bookmark);
        v.reminder = safeReminder(v.reminder);
        v.audioLanguages = safeISOCodes(v.audioLanguages);
        v.produceZone = safeProduceZone(v.produceZone);
        v.subtitleLanguages = safeISOCodes(v.subtitleLanguages);
        v.deviceTypes = safeDeviceTypes(v.deviceTypes);
        v.advisories = v.advisories || [];
        v.subjectIDs = v.subjectIDs || [];
        v.customFields = safeNamedParameters(v.customFields);
        v.locationCopyrights = v.locationCopyrights || [];
    }
    return v;
}

function safeVODDetails(vs: VODDetail[]): VODDetail[] {
    if (vs) {
        return vs.map((v: VODDetail) => {
            return safeVODDetail(v);
        });
    } else {
        return [];
    }
}

export interface VODExcluder {

    /**
    * 出产国家或者地区。
    */
    produceZoneIDs?: string[];

    /**
    * 订购方式，取值如下：
    *
    * -1：所有
    *
    * 0：包周期订购
    *
    * 1：按次订购
    *
    * 默认值是-1。
    */
    subscriptionTypes?: string[];

    /**
    * 发布年份。
    *
    * 例如：produceYear传入了2011,2010，即查询出品日期是2011和2010的VOD。
    */
    produceYears?: string[];

    /**
    * 流派ID。
    */
    genreIDs?: string[];

    /**
    * VOD父集类型。
    *
    * 1：普通连续剧父集
    *
    * 2：季播剧父集
    *
    * 如果不传，表示不做过滤。
    */
    supersetType?: string;
}

function safeVODExcluder(v: VODExcluder): VODExcluder {
    if (v) {
        v.produceZoneIDs = v.produceZoneIDs || [];
        v.subscriptionTypes = v.subscriptionTypes || [];
        v.produceYears = v.produceYears || [];
        v.genreIDs = v.genreIDs || [];
    }
    return v;
}

function safeVODExcluders(vs: VODExcluder[]): VODExcluder[] {
    if (vs) {
        return vs.map((v: VODExcluder) => {
            return safeVODExcluder(v);
        });
    } else {
        return [];
    }
}

export interface VODFilter {

    /**
    * 出产国家或者地区。
    */
    produceZoneIDs?: string[];

    /**
    * 订购方式，取值如下：
    *
    * -1：所有
    *
    * 0：已订购且被定价为包周期产品
    *
    * 1：已订购且按定价为按次产品
    *
    * 2：未订购
    *
    * 3：未订购且被定价为按次产品
    *
    * 4：未订购且被定价为包周期产品
    *
    * 默认值是-1。
    */
    subscriptionTypes?: string[];

    /**
    * 发布年份。
    *
    * 例如：produceYear传入了2011,2010，即查询出品日期是2011和2010的VOD。
    */
    produceYears?: string[];

    /**
    * 流派ID。
    */
    genreIDs?: string[];

    /**
    * 对内容名称首字母进行过滤，取值对应用户语系的字母和0-9的10个数字。
    */
    initials?: string[];

    /**
    * VOD支持的语种，为语言的ISO 639-1双字节缩写。比如en、zh
    */
    languages?: string[];

    /**
    * 点播内容归属的CP ID,多个cpId以&quot;,&quot;分割
    */
    cpId?: string;

    /**
    * 是否过滤含正片的。
    *
    * 1：查询所有
    *
    * 2：查询含有正片的VOD
    *
    * 3：查询不含正片的VOD
    *
    * 如果不传，表示查询所有。
    */
    hasCommon?: string;
}

function safeVODFilter(v: VODFilter): VODFilter {
    if (v) {
        v.produceZoneIDs = v.produceZoneIDs || [];
        v.subscriptionTypes = v.subscriptionTypes || [];
        v.produceYears = v.produceYears || [];
        v.genreIDs = v.genreIDs || [];
        v.initials = v.initials || [];
        v.languages = v.languages || [];
    }
    return v;
}

function safeVODFilters(vs: VODFilter[]): VODFilter[] {
    if (vs) {
        return vs.map((v: VODFilter) => {
            return safeVODFilter(v);
        });
    } else {
        return [];
    }
}

export interface VODMediaFile {

    /**
    * 媒体文件的ID。
    *
    * 客户端获取媒体文件的播放地址的时候需要用到该参数。
    */
    ID: string;

    /**
    * 媒体播放时长。
    *
    * 单位：秒。
    */
    elapseTime?: string;

    /**
    * 媒体文件码率。
    *
    * 单位：kbit/s。
    */
    bitrate?: string;

    /**
    * 媒体是否支持下载。
    *
    * 取值范围：
    *
    * 0：不支持
    *
    * 1：支持
    *
    * 默认值为0。
    */
    isDownload: string;

    /**
    * 高清标清标识，取值范围：
    *
    * 0：SD
    *
    * 1：HD
    *
    * 2：4K
    */
    definition: string;

    /**
    * 点播媒体文件是否支持HDCP。
    *
    * 取值范围：
    *
    * 0：不支持
    *
    * 1：支持
    *
    * 默认值为0。
    */
    HDCPEnable: string;

    /**
    * 点播内容的Macrovision属性。
    *
    * 取值范围：
    *
    * 0：Macrovision off
    *
    * 1：AGC
    *
    * 2：AGC + 2-stripe
    *
    * 3：AGC + 4-stripe
    *
    * 默认值为0。
    */
    macrovision: string;

    /**
    * 视频类型。
    *
    * 取值范围：
    *
    * 2：2D
    *
    * 3：3D
    *
    * 4： 2D VR
    *
    * 5： 3D VR
    *
    * 默认值为2。
    */
    dimension: string;

    /**
    * 3D格式，该字段仅对3D内容（即dimension参数值为3）有效。
    *
    * 取值范围：
    *
    * 1：side-by-side
    *
    * 2：top-and-bottom
    *
    * 默认值为1。
    */
    formatOf: string;

    /**
    * 媒体文件支持的终端类型。
    *
    * TV屏的内容取值范围：
    *
    * 1：STB
    *
    * 2：PC
    *
    * 3：LG-TV
    *
    * Web屏内容取值范围：
    *
    * 1：Browser
    *
    * 2：iPhone/iPad
    *
    * 3：Android
    */
    supportTerminals?: string[];

    /**
    * 媒体文件的内容格式。
    *
    * 取值范围：
    *
    * 1：TS
    *
    * 2：HLS
    *
    * 3：HSS
    *
    * 4：DASH
    *
    * 默认值为1。
    */
    fileFormat: string;

    /**
    * 是否CA加密。
    *
    * 取值范围：
    *
    * 0：未加密
    *
    * 1：加密
    */
    encrypt: string;

    /**
    * 内容CGMS-A属性。
    *
    * 取值范围：
    *
    * 0：Copy Freely
    *
    * 1：Copy No More
    *
    * 2：Copy Once
    *
    * 3：CopyNever
    */
    CGMSA: string;

    /**
    * 是否支持模拟端口输出。
    *
    * 0：不支持
    *
    * 1：支持
    */
    analogOutputEnable: string;

    /**
    * 视频编码格式，取值包括：
    *
    * H.263
    *
    * H.264
    *
    * H.265
    */
    videoCodec?: string;

    /**
    * VOD媒资的扩展属性，其中扩展属性的Key由局点CMS定制。
    */
    customFields?: NamedParameter[];

    /**
    * 标识该媒资的声场技术，常见的声场技术有Dolby ProLogic，Dolby Digital，Stereo，Mono。
    */
    audioType?: string;

    /**
    * 第三方系统分配的媒资Code。
    */
    code?: string;

    /**
    * 对于OTT多码率媒资，返回多个码率信息。
    *
    * 码率单位为kbps，同时多个码率会按照码率从大到小排序，例如一个媒资有10M、9.5M、9M、8M四种码率，则multiBitrates[]数字的取值顺序为：10000、9500、9000、8000。
    */
    multiBitrates?: string[];

    /**
    * 内容帧率，取值范围(0,256]。
    */
    fps?: string;

    /**
    * 点播对应的峰值码率，只有VBR的点播才有峰值码率。
    */
    maxBitrate?: string;

    /**
    * 海报路径。
    *
    * 说明
    *
    * 目前只有片花有海报，正片不支持。
    */
    picture?: Picture;

    /**
    * 可否预览，取值包括：
    *
    * 0：否
    *
    * 1：是
    */
    preview: string;

    /**
    * 如果支持预览，返回预览开始时间，相对于节目开始的时间，格式为HHmmss。
    */
    previewStartTime?: string;

    /**
    * 如果支持预览，返回预览结束时间，相对于节目开始的时间，格式为HHmmss。
    */
    previewEndTime?: string;

    /**
     * 片头时长。
     * 单位：秒。
     */
    headDuration?: string;

    /**
     * 片尾时长。
     * 单位：秒。
     */
    tailDuration?: string;

    /**
    * 局点定制的扩展字段。
    */
    extensionFields?: NamedParameter[];
}

function safeVODMediaFile(v: VODMediaFile): VODMediaFile {
    if (v) {
        v.supportTerminals = v.supportTerminals || [];
        v.customFields = safeNamedParameters(v.customFields);
        v.multiBitrates = v.multiBitrates || [];
        v.picture = safePicture(v.picture);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeVODMediaFiles(vs: VODMediaFile[]): VODMediaFile[] {
    if (vs) {
        return vs.map((v: VODMediaFile) => {
            return safeVODMediaFile(v);
        });
    } else {
        return [];
    }
}

export interface Voucher {

    /**
    * 促销码对应的优惠劵编号。
    */
    voucherID: string;

    /**
    * 优惠劵生效时间，取值为距离1970年1月1号的毫秒数。
    */
    applyTime: string;

    /**
    * 优惠劵失效时间，取值为距离1970年1月1号的毫秒数。
    */
    expireTime: string;

    /**
    * 优惠券类型，取值包括：
    *
    * 1：折扣型
    *
    * 2：抵扣类
    *
    * 3：全额抵扣秒杀型
    */
    voucherType: string;

    /**
    * 折扣/抵扣信息。
    *
    * 如果voucherType为1，表示折扣后的利率，取值为0~100，0表示免费。
    *
    * 如果voucherType为2，表示扣减金额。
    *
    * 如果voucherType为3，表示秒杀金额。
    */
    discount: string;

    /**
    * 包周期产品的折扣周期，即使用Voucher订购包周期，该折扣或抵扣的周期。
    *
    * 例如，折扣周期为3个周期，若是Voucher续订方式订购了一个包月产品，则该产品只能享受3个月的折扣；折扣周期为2个周期，若是Voucher方式订购了一个包周产品，则该产品只能享受2周的折扣。
    *
    * 说明
    *
    * 对于按次产品的Voucher订购该属性无意义。
    */
    cycleCount?: string;

    /**
    * 取值说明：
    *
    * ALL_SVOD：所有包周期产品
    *
    * ALL_TVOD：所有按次产品
    *
    * ALL_PROD：所有产品
    *
    * SERVICE：业务（保留）
    *
    * CONTENT:：内容
    *
    * DEFAULT_FEE：默认资费包
    *
    * NODE：节点
    *
    * PROD：指定产品
    */
    applyType: string;

    /**
    * 优惠券可以应用的内容范围，对应定价对象外键键值。
    */
    applyScopes?: string[];

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeVoucher(v: Voucher): Voucher {
    if (v) {
        v.applyScopes = v.applyScopes || [];
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeVouchers(vs: Voucher[]): Voucher[] {
    if (vs) {
        return vs.map((v: Voucher) => {
            return safeVoucher(v);
        });
    } else {
        return [];
    }
}

export interface LicenseItem {

    /**
    * License类型，取值包括：
    *
    * IPTVSTB：IPTV STB License数量（IPTV+DVB）
    *
    * HYBRIDIPTVSTB：Hybrid IPTVSTB License数量（IPTV+DVB）
    *
    * HYBRIDOTTSTB：Hybrid OTTSTB License数量
    *
    * OTTSTB：OTTSTB License数量
    *
    * OTTDEVICE：OTT Device License数量（非STB）
    *
    * PLAYREADY：Playready License数量
    *
    * SUBSCRIBER：订户数量
    *
    * IPTVSTBXMPP：IPTV STB XMPP License数量
    *
    * OTTSTBXMPP：OTT STB XMPP License数量
    *
    * HYBRIDIPTVSTBXMPP：Hybrid IPTV STB XMPP License数量
    *
    * HYBRIDOTTSTBXMPP：Hybrid OTT STB XMPP License数量
    *
    * OTTDEVICEXMPP：OTT Device XMPP License数量
    *
    * OTTDEVICEPLAYERSQM：OTT Device Player&amp;SQM License数量
    */
    licenseType: string;

    /**
    * License总量
    */
    count?: string;

    /**
    * License使用量
    */
    useCount?: string;

    /**
    * 是否超过告警阀值，取值包括：
    *
    * 0：否
    *
    * 1：是
    */
    isAlarm?: string;
}

function safeLicenseItem(v: LicenseItem): LicenseItem {
    if (v) {
    }
    return v;
}

function safeLicenseItems(vs: LicenseItem[]): LicenseItem[] {
    if (vs) {
        return vs.map((v: LicenseItem) => {
            return safeLicenseItem(v);
        });
    } else {
        return [];
    }
}

export interface LoginRouteRequest {

    /**
    * 订户ID，用于基于订户ID的VSP调度。
    */
    subscriberID?: string;

    /**
    * 本轮调度禁用的VSP的 IP或者域名。
    *
    * 如果VSP是集群部署，返回的是集群的VIP或域名。
    *
    * 该字段的取值就是返回参数里的vspURL的VSPIP或者Domain。当终端发起LoginRoute请求后，如果EDS返回的VSP不可用，终端需要重新发送LoginRoute请求，同时请求带上上次不可用的VSP地址，以免EDS再次返回这个不可用的VSP地址。
    */
    disableVSPAddr?: string;
}

function safeLoginRouteRequest(v: LoginRouteRequest): LoginRouteRequest {
    if (v) {
    }
    return v;
}

function safeLoginRouteRequests(vs: LoginRouteRequest[]): LoginRouteRequest[] {
    if (vs) {
        return vs.map((v: LoginRouteRequest) => {
            return safeLoginRouteRequest(v);
        });
    } else {
        return [];
    }
}

export interface LoginRouteResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 如果调度成功，返回VSP服务器的HTTP访问地址，格式为http://&lt;VSPIP|Domain&gt;:&lt;VSPHttpPort&gt;。
    *
    * 如果地址是IP形式，IP地址支持V4或V6，VSP平台(EDS)根据终端接入的网络地址类型返回对应的VSP地址。
    */
    vspURL: string;

    /**
    * 如果调度成功，返回VSP服务器的HTTPS地址，当用户网关(VSP)服务器支持HTTPS协议才返回，格式为https://&lt;VSPIP|Domain &gt;:&lt;VSPHttpsPort&gt;。
    *
    * 如果地址是IP形式，IP地址支持V4或V6，VSP平台(EDS)根据终端接入的网络地址类型返回对应的用户网关(VSP)地址。
    */
    vspHttpsURL?: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeLoginRouteResponse(v: LoginRouteResponse): LoginRouteResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeLoginRouteResponses(vs: LoginRouteResponse[]): LoginRouteResponse[] {
    if (vs) {
        return vs.map((v: LoginRouteResponse) => {
            return safeLoginRouteResponse(v);
        });
    } else {
        return [];
    }
}

export interface LoginRequest {

    /**
    * 订户ID。
    */
    subscriberID?: string;

    /**
    * 终端型号。
    */
    deviceModel?: string;
}

function safeLoginRequest(v: LoginRequest): LoginRequest {
    if (v) {
    }
    return v;
}

function safeLoginRequests(vs: LoginRequest[]): LoginRequest[] {
    if (vs) {
        return vs.map((v: LoginRequest) => {
            return safeLoginRequest(v);
        });
    } else {
        return [];
    }
}

export interface LoginResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 用户网关(VSP)服务器访问地址，格式为http://&lt;VSPip&gt;:&lt;VSPhttpport&gt;。
    *
    * 如果地址是IP形式，IP地址支持V4或V6，平台根据终端接入的网络地址类型返回对应的用户网关(VSP)地址。
    */
    vspURL: string;

    /**
    * 用户网关(VSP)服务器的HTTPS地址，当用户网关(VSP)服务器支持HTTPS协议才返回，格式为https://&lt;VSPip&gt;:&lt;VSPhttpsport&gt;。
    *
    * 这里的IP和Port是用户网关(VSP)服务器的IP和端口。
    *
    * 如果地址是IP形式，IP地址支持V4或V6，平台根据终端接入的网络地址类型返回对应的用户网关(VSP)地址。
    *
    * 客户端如与用户网关(VSP) Server采取HTTPS交互，可以通过此参数获取HTTPS端口。
    *
    * 用户网关(VSP)服务器是否支持HTTPS协议可以在IPTV平台侧配置。
    */
    vspHttpsURL?: string;

    /**
    * 根证书下发地址，当用户网关(VSP)服务器支持根证书下发时才返回，格式类似http://ip或者domain:port/getrootceraddrVSP/CA/iptv_ca.der。
    *
    * 这里的IP和Port是用户网关(VSP)服务器的IP和端口。
    *
    * 如果地址是IP形式，IP地址支持V4或V6，平台根据终端接入的网络地址类型返回对应的用户网关(VSP)地址。
    *
    * 客户端如需更新根证书，可以通过此参数获取根证书下载地址。如客户端不支持HTTPS，忽略此参数。
    */
    rootCerAddr?: string;

    /**
    * 升级服务器的域名。
    *
    * 格式为：http://ip或域名:port/服务名。
    *
    * IP地址支持V4或V6，平台根据终端接入的网络地址类型返回对应的服务器地址。
    *
    * 如果终端升级需要经过EDS，服务名为EDS，否则为UPGRADE。
    */
    upgradeDomain?: string;

    /**
    * 备份升级服务器的域名。
    *
    * 格式为：http://ip或域名:port/服务名。
    *
    * IP地址支持V4或V6，平台根据终端接入的网络地址类型返回对应的服务器地址。
    *
    * 如果终端升级需要经过EDS，服务名为EDS，否则为UPGRADE。
    */
    upgradeDomainBackup?: string;

    /**
    * 终端网管的Domain。
    *
    * 格式为：http://ip或域名:port/。
    *
    * port默认为80。
    *
    * IP地址支持V4或V6，平台根据终端接入的网络地址类型返回对应的服务器地址。
    *
    * 说明
    *
    * 当域名为空时，表示不对接终端网管。
    */
    mgmtDomain?: string;

    /**
    * 备份管理服务器的Domain。
    *
    * 格式为：http://ip或域名:port/。
    *
    * port默认为80。
    *
    * IP地址支持V4或V6，平台根据终端接入的网络地址类型返回对应的服务器地址。
    */
    mgmtDomainBackup?: string;

    /**
    * NTP服务器的Domain。
    *
    * 格式为：http://ip或域名:port/。
    *
    * port默认为80。
    *
    * IP地址支持V4或V6，平台根据终端接入的网络地址类型返回对应的服务器地址。
    */
    NTPDomain?: string;

    /**
    * 备份NTP服务器的Domain。
    *
    * 格式为：http://ip或域名:port/。
    *
    * port默认为80。
    *
    * IP地址支持V4或V6，平台根据终端接入的网络地址类型返回对应的服务器地址。
    */
    NTPDomainBackup?: string;

    /**
    * 和用户相关的系统参数，请参见“2.57 Parameters”类型。
    */
    parameters?: Parameters;

    /**
    * 终端系统参数，请参见“2.28 Configuration”类型
    *
    * 说明
    *
    * terminalParam是终端团队根据自身的业务需要定义的参数，平台只是包括参数名和参数值，不感知具体含义。
    *
    * 终端参数可以通过DV配置。
    */
    terminalParm?: Configuration;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeLoginResponse(v: LoginResponse): LoginResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.parameters = safeParameters(v.parameters);
        v.terminalParm = safeConfiguration(v.terminalParm);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeLoginResponses(vs: LoginResponse[]): LoginResponse[] {
    if (vs) {
        return vs.map((v: LoginResponse) => {
            return safeLoginResponse(v);
        });
    } else {
        return [];
    }
}

export interface AuthenticateRequest {

    /**
    * 基本认证参数。
    */
    authenticateBasic: AuthenticateBasic;

    /**
    * 设备相关认证参数。
    */
    authenticateDevice?: AuthenticateDevice;

    /**
    * 用户容灾相关认证参数。
    */
    authenticateTolerant?: AuthenticateTolerant;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeAuthenticateRequest(v: AuthenticateRequest): AuthenticateRequest {
    if (v) {
        v.authenticateBasic = safeAuthenticateBasic(v.authenticateBasic);
        v.authenticateDevice = safeAuthenticateDevice(v.authenticateDevice);
        v.authenticateTolerant = safeAuthenticateTolerant(v.authenticateTolerant);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeAuthenticateRequests(vs: AuthenticateRequest[]): AuthenticateRequest[] {
    if (vs) {
        return vs.map((v: AuthenticateRequest) => {
            return safeAuthenticateRequest(v);
        });
    } else {
        return [];
    }
}

export interface AuthenticateResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 当前登录用户的令牌，由VSP平台在认证成功后自动生成。
    */
    userToken?: string;

    /**
    * 用户使用的物理设备对应的逻辑设备ID，当用户创建CPVR任务会用到该参数。
    */
    deviceID?: string;

    /**
    * 返回当前设备的设备名称，只有认证请求携带了设备物理信息，才支持返回。
    */
    deviceName?: string;

    /**
    * 用户的设备列表。
    *
    * 在返回设备绑定达到上限时返回。
    */
    devices?: Device[];

    /**
    * Envision Video Platform可以支持对接多家CA厂商，每个CA厂商需要下发给终端的参数不完全相同。
    *
    * 认证成功后返回。
    *
    * 请参见“[CAInfo](#_ZH-CN_TOPIC_0055055526)”类型。
    */
    CA?: CAInfo;

    /**
    * 订户ID。
    *
    * 当登录成功或设备绑定达到上限时返回。
    */
    subscriberID?: string;

    /**
    * 如果请求参数userType为1或者支持Profile自动登录，返回用户对应的profileID。
    *
    * 说明
    *
    * Profile自动登录包括如下场景：当前登录设备带默认Profile；游客登录；订户只有一个Profile
    */
    profileID?: string;

    /**
    * 如果请求参数userType为1或者支持Profile自动登录，返回用户对应的profileSn。
    *
    * 说明
    *
    * Profile自动登录包括如下场景：当前登录设备带默认Profile；游客登录；订户只有一个Profile
    */
    profileSN?: string;

    /**
    * 订户所有的User Profile信息。
    */
    profiles?: Profile[];

    /**
    * 如果userId是订户或者设备Id，表示订户是否是首次登录IPTV系统，如果userId是Profile的loginName，表示Profile是否是首次登录IPTV系统。
    *
    * 取值范围：
    *
    * 0：非首次登录
    *
    * 1：首次登录
    */
    isFirstLogin: string;

    /**
    * 用户登录状态标识。
    *
    * 认证成功后返回。
    *
    * 取值范围：
    *
    * 0：正常登录
    *
    * 1：放通登录
    *
    * 默认值为0。
    *
    * 如果系统配置为支持放通，用户开机认证时，如果数据库或VSP故障，用户网关(VSP)服务器也会让用户认证通过，此时用户的登录状态即放通登录。
    */
    loginOccasion?: string;

    /**
    * 终端所属时区。
    *
    * 格式为：GMT+/-N。
    *
    * 有些地方的时区并不是整数，N以“hh:ss”时间形式表示。如：“GMT+03:30”。
    *
    * 说明
    *
    * 终端和平台支持UTC时区和本地时区，参考认证接口的UTCEnable字段。
    *
    * 如果用本地时区的话，终端可以上报用户指定的时区，如果用户不指定的话，平台会根据用户归属的区域查询此区域的本地时区，区域是开户时指定的。
    */
    timeZone?: string;

    /**
    * 终端所在时区的夏令时时段。
    *
    * 如果时区不支持夏令时可不携带该参数，否则返回“夏令时UTC开始时间，夏令时UTC结束时间”。
    *
    * 取值为距离1970年1月1号的毫秒数。
    *
    * 例如：“20070401080000,20071028065959”。
    */
    DSTTime?: string;

    /**
    * 用户区域的外部编号，认证成功后返回。
    *
    * 用户认证成功后需要写入终端。
    */
    areaCode?: string;

    /**
    * 如果终端上报templatename，返回终端上报的取值，否则：
    *
    * 如果是Profile认证的，平台首先查询Profile设置的模板名，如果Profile未设置模板则返回运营商给订户分配的默认模板。
    *
    * 如果是订户/设备认证的，平台首先查询Admin Profile设置的模板名，如果Admin Profile未设置模板则返回运营商给订户分配的默认模板。
    *
    * 如果是容灾用户，平台默认返回default。
    */
    templateName?: string;

    /**
    * 用户模板最近更新的时间，取值为距离1970年1月1号的毫秒数。
    */
    templateTimeStamp?: string;

    /**
    * 用户组ID。
    *
    * 用户认证成功后需要将认证响应中的usergroup写入终端，并在后续认证请求中传给用户网关(VSP)服务器，以便Envision Video Platform容灾后，仍然能够为用户提供服务。
    */
    userGroup?: string;

    /**
    * 订户归属的子网运营商ID，终端首次开机登录成功后终端需要保存到本地，当下次开机时携带给VSP，以便Envision Video Platform容灾后，仍然能够为用户提供服务。
    */
    subnetID?: string;

    /**
    * 订户归属的BOSS编号。
    */
    bossID?: string;

    /**
    * 用户带宽，认证成功后返回。
    *
    * 单位：kbit/s。
    */
    bandWidth?: string;

    /**
    * 用户在观看单播时，终端根据TransportProtocol属性表示的协议来接收媒体流，对于不支持该属性的终端按照出厂设置处理。认证成功后返回。
    *
    * 取值范围：
    *
    * -1：使用出厂设置
    *
    * 0：TCP
    *
    * 1：UDP
    *
    * 2：RTP over TCP
    *
    * 3：RTP over UDP
    *
    * 默认值为-1。
    */
    transportProtocol?: string;

    /**
    * 认证成功后，返回订户开通的业务类型。
    *
    * 基线版本，取值包括：
    *
    * IPTV
    *
    * SAT-only
    *
    * Hybrid-IP
    *
    * OTT
    *
    * 一个设备支持多种业务类型（使用英文逗号分隔），为兼容OTE的Hybrid-IP，Hybrid-IP特指”IPTV,DVB”。
    */
    provisioningType?: string[];

    /**
    * 订户同一个时刻在其他地方登录的IP地址，支持IPv4和IPv6格式。
    */
    loginIP?: string[];

    /**
    * 用户地理位置，为两位的国家码，遵从ISO 3166-1。仅在认证成功时返回。
    */
    location?: string;

    /**
    * 区域RRS地址，格式为http://rrsip:rrsport。
    */
    RRSAddr?: string;

    /**
    * 采集用户页面浏览轨迹参数。
    */
    pageTrackers?: PageTracker[];

    /**
    * 防盗链图片地址。
    */
    pictureURL?: string[];

    /**
    * 支持防篡改检查的接口，key为接口名，比如Play、Authorization等。
    *
    * value是需要做为数据一致性校验的属性名，入参和出参使用分号分隔，多个入参/出参之间使用英文逗号分隔。如果接口不需要校验入参或出参，使用空串代替。
    */
    antiTamperURI?: NamedParameter[];

    /**
    * 为了防止跨站攻击，服务器侧生成临时Token。
    */
    csrfToken?: string;

    /**
    * 是否是TriplePlay用户。
    *
    * 取值范围：
    *
    * 0：非TriplePlay用户
    *
    * 1：TriplePlay用户
    *
    * 默认值为0。
    *
    * 是否需要携带该参数取决于局点需求。
    */
    isTriplePlay?: string;

    /**
    * 连续登录失败多少次锁定账号，如果局点不需要此功能，该字段将返回0。
    */
    lockedNum?: string;

    /**
    * 等待解锁的时间，单位：秒。
    */
    waitUnlockTime?: string;

    /**
    * 还剩余尝试登录次数。
    */
    remainLockedNum?: string;

    /**
    * 密码重置时间，取值为距离1970年1月1号的毫秒数。
    */
    pwdResetTime?: string;

    /**
    * 用户是否订购了iPad遥控器业务。
    *
    * 取值范围：
    *
    * 0：未订购
    *
    * 1：已订购
    *
    * 默认值为0。
    */
    STBRCUSubscribed?: string;

    /**
    * 表示用户是否同意平台采集自己的行为数据，取值包括：
    *
    * 0：opt in
    *
    * 1：opt out
    *
    * 默认值为0。
    */
    opt?: string;

    /**
    * 是否反馈用户初始偏好，取值范围：
    *
    * 0：未反馈
    *
    * 1：反馈
    *
    * 默认值为0。
    */
    hasCollectUserPreference?: string;

    /**
    * 本次认证以前的认证日志信息以及当前其它地方有效的登录信息（如果有的话）。
    */
    userLoginHistoryInfo?: UserLoginHistoryInfo;

    /**
    * 设备向Verimatrix或MultiDRM注册后返回的ID，针对Verimatrix该字段仅在Verimatrix3.8及以后的版本中支持。
    *
    * 说明
    *
    * Verimatrix保护类型MulitRights的一种保护方式，该种保护方式下，向CA绑定设备时，会返回VUID，播放器请求播放时，携带该字符串到Verimatrix。
    */
    VUID?: string;

    /**
    * 设备所处网络。当前取值为如下字符串：
    *
    * “MultiRights” （对接Verimatrix MultiRights ）
    *
    * “MultiDRM ”（对接华为自研MultiDRM，当前不支持空字符串(即null)）
    */
    network?: string;

    /**
    * 如果认证成功，返回会话ID。
    */
    jSessionID?: string;

    /**
    * 免密登录流程所需的认证token。
    *
    * 当请求消息中的authenticateBasic中的authType值为1且用户登录认证成功时，返回此字段。
    */
    authToken?: string;

    /**
    * 用户过滤条件，包括订户子网、订户语种、订户区域、订户分组、订户领域、频道命名空间、频道版本号汇总后的哈希值。
    *
    * 认证成功的情况下一定返回该字段。
    */
    userFilter?: string;

    /**
    * 每次用户登录认证时随机生成的ID
    */
    sqmSessionId?: string;

    /**
    * Base64(HMAC-SHA256(sqmSessionId + deviceid + subscriberid, key))
    */
    sqmToken?: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeAuthenticateResponse(v: AuthenticateResponse): AuthenticateResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.devices = safeDevices(v.devices);
        v.CA = safeCAInfo(v.CA);
        v.profiles = safeProfiles(v.profiles);
        v.provisioningType = v.provisioningType || [];
        v.loginIP = v.loginIP || [];
        v.pageTrackers = safePageTrackers(v.pageTrackers);
        v.pictureURL = v.pictureURL || [];
        v.antiTamperURI = safeNamedParameters(v.antiTamperURI);
        v.userLoginHistoryInfo = safeUserLoginHistoryInfo(v.userLoginHistoryInfo);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeAuthenticateResponses(vs: AuthenticateResponse[]): AuthenticateResponse[] {
    if (vs) {
        return vs.map((v: AuthenticateResponse) => {
            return safeAuthenticateResponse(v);
        });
    } else {
        return [];
    }
}

export interface SwitchProfileRequest {

    /**
    * 待认证的Profile ID。
    */
    profileID: string;

    /**
    * Profile密码，此处是明文。
    *
    * 如果当前登录设备绑定了默认Profile且默认Profile就是待切换的ProfileID，以及主Profile切换到子Profile，终端可以不上报密码。
    */
    password?: string;

    /**
    * Profile是否强制登录。
    *
    * 取值范围：
    *
    * 0：不强制
    *
    * 1：强制
    *
    * 默认为1。如果一个Profile已经在一个终端A上登录，该Profile尝试又在另一终端B上登录，当forceLogin&#x3D;0，系统会提示返回“登录失败，Profile已登录”的错误码，当foreLogin&#x3D;1，系统自动断开最早的会话。
    */
    forceLogin?: string;

    /**
    * 是否将该profile设置为默认profile。
    *
    * 0：否
    *
    * 1：是
    *
    * 如果不传该字段，则表示保持当前设备上的默认profile不变化。
    */
    asDefaultProfile?: string;

    /**
    * 当前客户端页面显示内容的语种属性。
    *
    * 采用统一为ISO639-1缩写，如en。
    */
    lang?: string;

    /**
    * 当前设备对应的逻辑设备ID。
    *
    * 如果asDefaultProfile有值，则需要上报该字段。
    */
    deviceID?: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeSwitchProfileRequest(v: SwitchProfileRequest): SwitchProfileRequest {
    if (v) {
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeSwitchProfileRequests(vs: SwitchProfileRequest[]): SwitchProfileRequest[] {
    if (vs) {
        return vs.map((v: SwitchProfileRequest) => {
            return safeSwitchProfileRequest(v);
        });
    } else {
        return [];
    }
}

export interface SwitchProfileResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 默认profile处理（设置/取消设置）成功后，返回当前profileID。
    *
    * 如果没有设置请求或设置失败，则不返回。
    */
    defaultProfileID?: string;

    /**
    * 当前登录用户的令牌，由VSP在User Profile认证成功后自动生成。
    */
    userToken?: string;

    /**
    * Profile是否是首次登录Envision Video Platform。
    *
    * 取值范围：
    *
    * 0：非首次登录
    *
    * 1：首次登录
    *
    * 默认值为0。
    */
    isFirstLogin: string;

    /**
    * 是否反馈用户初始偏好，取值范围：
    *
    * 0：未反馈
    *
    * 1：反馈
    *
    * 默认值为0。
    */
    hasCollectUserPreference?: string;

    /**
    * 本次认证以前的认证日志信息。
    *
    * 说明
    *
    * 安全要求，在认证的时候，需要提示用户最近登录的IP/登录结果（成功/失败），终端可以根据的业务需要向用户提示。类似的还有提示用户当前是不是在另外一个设备上有登录。
    */
    userLoginHistoryInfo?: UserLoginHistoryInfo;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeSwitchProfileResponse(v: SwitchProfileResponse): SwitchProfileResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.userLoginHistoryInfo = safeUserLoginHistoryInfo(v.userLoginHistoryInfo);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeSwitchProfileResponses(vs: SwitchProfileResponse[]): SwitchProfileResponse[] {
    if (vs) {
        return vs.map((v: SwitchProfileResponse) => {
            return safeSwitchProfileResponse(v);
        });
    } else {
        return [];
    }
}

export interface OnLineHeartbeatRequest {
}

function safeOnLineHeartbeatRequest(v: OnLineHeartbeatRequest): OnLineHeartbeatRequest {
    if (v) {
    }
    return v;
}

function safeOnLineHeartbeatRequests(vs: OnLineHeartbeatRequest[]): OnLineHeartbeatRequest[] {
    if (vs) {
        return vs.map((v: OnLineHeartbeatRequest) => {
            return safeOnLineHeartbeatRequest(v);
        });
    } else {
        return [];
    }
}

export interface OnLineHeartbeatResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 用户合法标志。取值范围：
    *
    * true：合法
    *
    * false：非法
    */
    userValid: string;

    /**
    * 心跳周期，单位：秒。
    *
    * 客户端请务必要按照该间隔调用心跳接口。如果随意高频调用，将会严重影响平台性能。
    *
    * 正常情况下，平台一般将该值设为900，即15分钟，仅供参考。
    *
    * 如果平台连续两个心跳时长仍未收到终端的心跳消息，即认为终端已经离线，将会释放会话。
    */
    nextCallInterval: string;

    /**
    * 个性化数据的版本号。
    */
    personalDataVersions?: PersonalDataVersion;

    /**
    * 用户区域的外部编号。
    */
    areaCode?: string;

    /**
    * 用户组ID。
    */
    userGroup?: string;

    /**
    * 订户ID。
    */
    subscriberID?: string;

    /**
    * 用户设置的in/out数据是否采集属性，取值包括：
    *
    * 0：opt in
    *
    * 1：opt out
    *
    * 默认值为0。
    *
    * 虽然QuerySubscriber接口也会返回opt属性，但是此接口资源消耗较高，而终端每次数据采集都要根据opt判断，为了保证用户的opt属性能够准实时被终端感知，VSP在心跳接口里将用户更新后的opt返回给终端。
    */
    opt?: string;

    /**
    * DigitalMedia验证终端合法性的密钥。
    */
    validInfo?: string;

    /**
    * DigitalMedia验证终端合法性的随机数。
    */
    validKey?: string;

    /**
    * 用户模板最近更新的时间，取值为距离1970年1月1号的毫秒数。
    */
    templateTimeStamp?: string;

    /**
    * 是否支持实时采集用户页面浏览轨迹，取值包括：
    *
    * 0：不支持
    *
    * 1：支持
    *
    * 默认值为0。
    */
    isSupportedUserLogCollect?: string;

    /**
    * 用户过滤条件，包括订户子网、订户语种、订户区域、订户分组、订户领域、频道命名空间、频道版本号汇总后的哈希值。
    */
    userFilter?: string;

    /**
    * 当前登录用户的令牌，由VSP平台在认证成功后自动生成，过期自动刷新。
    */
    userToken?: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeOnLineHeartbeatResponse(v: OnLineHeartbeatResponse): OnLineHeartbeatResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.personalDataVersions = safePersonalDataVersion(v.personalDataVersions);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeOnLineHeartbeatResponses(vs: OnLineHeartbeatResponse[]): OnLineHeartbeatResponse[] {
    if (vs) {
        return vs.map((v: OnLineHeartbeatResponse) => {
            return safeOnLineHeartbeatResponse(v);
        });
    } else {
        return [];
    }
}

export interface LogoutRequest {

    /**
    * 退出类型。取值范围：
    *
    * 0：用户注销
    *
    * 1：用户退出，类似PC的关机
    *
    * 默认值为0。
    */
    type?: string;

    /**
    * 设备状态，取值包括：
    *
    * 0：离线
    *
    * 1：在线
    *
    * 2：待机
    *
    * 3：休眠
    *
    * 默认值为0。
    */
    deviceStatus?: string;

    /**
    * 当Type&#x3D;1且deviceStatus&#x3D;0时，通知指定的物理设备已下线。
    *
    * 对于STB设备，此参数为MAC地址。
    *
    * 对于OTT设备，此参数为VMX插件生成的clientId。
    */
    physicalDeviceId?: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeLogoutRequest(v: LogoutRequest): LogoutRequest {
    if (v) {
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeLogoutRequests(vs: LogoutRequest[]): LogoutRequest[] {
    if (vs) {
        return vs.map((v: LogoutRequest) => {
            return safeLogoutRequest(v);
        });
    } else {
        return [];
    }
}

export interface LogoutResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeLogoutResponse(v: LogoutResponse): LogoutResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeLogoutResponses(vs: LogoutResponse[]): LogoutResponse[] {
    if (vs) {
        return vs.map((v: LogoutResponse) => {
            return safeLogoutResponse(v);
        });
    } else {
        return [];
    }
}

export interface UpdateTimeZoneRequest {

    /**
    * 终端所在的新时区，格式可以是Asia/Shanghai 携带地域信息可以支持夏令时，如果是GMT+/GMT-的格式则不支持夏令时，因同一时区不同国家夏令时可不同。
    */
    newTimezone: string;
}

function safeUpdateTimeZoneRequest(v: UpdateTimeZoneRequest): UpdateTimeZoneRequest {
    if (v) {
    }
    return v;
}

function safeUpdateTimeZoneRequests(vs: UpdateTimeZoneRequest[]): UpdateTimeZoneRequest[] {
    if (vs) {
        return vs.map((v: UpdateTimeZoneRequest) => {
            return safeUpdateTimeZoneRequest(v);
        });
    } else {
        return [];
    }
}

export interface UpdateTimeZoneResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 终端所属时区。
    *
    * 格式为：GMT+/-N。
    *
    * 有些地方的时区并不是整数，N以“hh:ss”时间形式表示。如：“GMT+03:30”。
    *
    * 说明
    *
    * 终端和平台支持UTC时区和本地时区，参考认证接口的UTCEnable字段。
    *
    * 如果用本地时区的话，终端可以上报用户指定的时区，如果用户不指定的话，平台会根据用户归属的区域查询此区域的本地时区，区域是开户时指定的。
    */
    timeZone?: string;

    /**
    * 终端所在时区的夏令时时段。
    *
    * 如果时区不支持夏令时可不携带该参数，否则返回“夏令时UTC开始时间，夏令时UTC结束时间”。
    *
    * 取值为距离1970年1月1号的毫秒数。
    *
    * 例如：“20070401080000,20071028065959”。
    */
    DSTTime?: string;
}

function safeUpdateTimeZoneResponse(v: UpdateTimeZoneResponse): UpdateTimeZoneResponse {
    if (v) {
        v.result = safeResult(v.result);
    }
    return v;
}

function safeUpdateTimeZoneResponses(vs: UpdateTimeZoneResponse[]): UpdateTimeZoneResponse[] {
    if (vs) {
        return vs.map((v: UpdateTimeZoneResponse) => {
            return safeUpdateTimeZoneResponse(v);
        });
    } else {
        return [];
    }
}

export interface SendSMSRequest {

    /**
    * 注册手机号
    */
    loginName: string;

    /**
    * 1.短信验证码（预留email注册等）默认1
    */
    msgType?: string;

    /**
    * 当前终端页面显示内容的语种属性。
    *
    * 如果语种是订户共享的且语种还未设置，VSP将终端上报的locale作为订户语种。
    *
    * 采用统一为ISO639-1缩写，如en。
    */
    lang?: string;

    /**
    * 子网ID
    */
    subnetId?: string;

    /**
    * 扩展信息
    */
    extensionFields?: NamedParameter[];
}

function safeSendSMSRequest(v: SendSMSRequest): SendSMSRequest {
    if (v) {
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeSendSMSRequests(vs: SendSMSRequest[]): SendSMSRequest[] {
    if (vs) {
        return vs.map((v: SendSMSRequest) => {
            return safeSendSMSRequest(v);
        });
    } else {
        return [];
    }
}

export interface SendSMSResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeSendSMSResponse(v: SendSMSResponse): SendSMSResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeSendSMSResponses(vs: SendSMSResponse[]): SendSMSResponse[] {
    if (vs) {
        return vs.map((v: SendSMSResponse) => {
            return safeSendSMSResponse(v);
        });
    } else {
        return [];
    }
}

export interface CreateSubscriberRequest {

    /**
    * 注册手机号。
    */
    loginName: string;

    /**
    * 账号密码。
    */
    password?: string;

    /**
    * 注册方式。
    *
    * 1：手机注册(预留其他注册方式)，默认1
    */
    regType?: string;

    /**
    * 子网ID
    */
    subnetId?: string;

    /**
    * 用户是否同意平台采集自己的行为数据。
    *
    * 0：不同意
    *
    * 1：同意
    */
    opt?: string;

    /**
    * 验证码。
    */
    verifyCode: string;
}

function safeCreateSubscriberRequest(v: CreateSubscriberRequest): CreateSubscriberRequest {
    if (v) {
    }
    return v;
}

function safeCreateSubscriberRequests(vs: CreateSubscriberRequest[]): CreateSubscriberRequest[] {
    if (vs) {
        return vs.map((v: CreateSubscriberRequest) => {
            return safeCreateSubscriberRequest(v);
        });
    } else {
        return [];
    }
}

export interface CreateSubscriberResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeCreateSubscriberResponse(v: CreateSubscriberResponse): CreateSubscriberResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeCreateSubscriberResponses(vs: CreateSubscriberResponse[]): CreateSubscriberResponse[] {
    if (vs) {
        return vs.map((v: CreateSubscriberResponse) => {
            return safeCreateSubscriberResponse(v);
        });
    } else {
        return [];
    }
}

export interface AddProfileRequest {

    /**
    * Profile信息。
    */
    profile: Profile;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeAddProfileRequest(v: AddProfileRequest): AddProfileRequest {
    if (v) {
        v.profile = safeProfile(v.profile);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeAddProfileRequests(vs: AddProfileRequest[]): AddProfileRequest[] {
    if (vs) {
        return vs.map((v: AddProfileRequest) => {
            return safeAddProfileRequest(v);
        });
    } else {
        return [];
    }
}

export interface AddProfileResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 创建的子Profile编号。
    */
    profileID?: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeAddProfileResponse(v: AddProfileResponse): AddProfileResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeAddProfileResponses(vs: AddProfileResponse[]): AddProfileResponse[] {
    if (vs) {
        return vs.map((v: AddProfileResponse) => {
            return safeAddProfileResponse(v);
        });
    } else {
        return [];
    }
}

export interface ModifyProfileRequest {

    /**
    * Profile信息。
    *
    * 说明
    *
    * Profile的属性，如果调用者不传值，即不携带标签，VSP认为不修改该属性，比如用户不想修改logourl，只要调用者发起的请求不携带&lt;logourl&gt;，VSP不修改该参数，如果调用者发起的请求是&lt;logourl /&gt;，VSP认为是将属性置空。
    */
    profile: Profile;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeModifyProfileRequest(v: ModifyProfileRequest): ModifyProfileRequest {
    if (v) {
        v.profile = safeProfile(v.profile);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeModifyProfileRequests(vs: ModifyProfileRequest[]): ModifyProfileRequest[] {
    if (vs) {
        return vs.map((v: ModifyProfileRequest) => {
            return safeModifyProfileRequest(v);
        });
    } else {
        return [];
    }
}

export interface ModifyProfileResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 返回结果为成功时，该字段不返回，返回结果为失败，且出现错误码85983471的时候，表示loginName的修改周期
    */
    updatePeriod?: string;

    /**
    * 返回结果为成功时，该字段不返回，返回结果为失败，且出现错误码85983471的时候，表示loginName的修改次数
    */
    updateTimes?: string;

    /**
    * 返回结果为成功时，该字段不返回，返回结果为失败，且出现错误码85983471的时候，表示距下次能修改loginName，用户还需要等待天数。
    */
    remainDays?: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeModifyProfileResponse(v: ModifyProfileResponse): ModifyProfileResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeModifyProfileResponses(vs: ModifyProfileResponse[]): ModifyProfileResponse[] {
    if (vs) {
        return vs.map((v: ModifyProfileResponse) => {
            return safeModifyProfileResponse(v);
        });
    } else {
        return [];
    }
}

export interface DeleteProfileRequest {

    /**
    * Profile编号列表。
    */
    profileIDs: string[];

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeDeleteProfileRequest(v: DeleteProfileRequest): DeleteProfileRequest {
    if (v) {
        v.profileIDs = v.profileIDs || [];
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeDeleteProfileRequests(vs: DeleteProfileRequest[]): DeleteProfileRequest[] {
    if (vs) {
        return vs.map((v: DeleteProfileRequest) => {
            return safeDeleteProfileRequest(v);
        });
    } else {
        return [];
    }
}

export interface DeleteProfileResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeDeleteProfileResponse(v: DeleteProfileResponse): DeleteProfileResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeDeleteProfileResponses(vs: DeleteProfileResponse[]): DeleteProfileResponse[] {
    if (vs) {
        return vs.map((v: DeleteProfileResponse) => {
            return safeDeleteProfileResponse(v);
        });
    } else {
        return [];
    }
}

export interface QueryProfileRequest {

    /**
    * 查询类型。
    *
    * 取值范围：
    *
    * 0：查询当前Profile
    *
    * 1：查询所有Profile信息
    *
    * 2：查询主Profile信息
    *
    * 4：根据profile的ID精确查询系统所有User Profile
    *
    * type取值为3/4，不返回password、mobilePhone、email、birthday这4个字段。
    */
    type: string;

    /**
    * 查询条件。
    *
    * 当type&#x3D;4时表示Profile ID，目前仅支持在数组中传入一个ProfileID
    */
    condition?: string[];

    /**
    * 一次查询的总条数，不能设置为-1，调用者一定要指定获取数据的总条数，最大条数默认不超过50，最大条数可配置，超过最大条数返回错误。
    */
    count: string;

    /**
    * 查询的起始位置，从0开始（即0表示第一个）。
    */
    offset: string;

    /**
    * OTT设备是否返回Profile密码，取值包括：
    *
    * 0：返回（默认值）
    *
    * 1：不返回
    *
    * 说明
    *
    * 目前Guest登录的用户都会返回Profile密码。
    *
    * Envision Video Platform根据终端上报的此参数返回；
    *
    * OTT设备如果要返回Profile密码必须满足2个条件：VSP平台支持返回OTT设备的Profile密码且isReturnPassword取值为0。
    */
    isReturnPassword?: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeQueryProfileRequest(v: QueryProfileRequest): QueryProfileRequest {
    if (v) {
        v.condition = v.condition || [];
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeQueryProfileRequests(vs: QueryProfileRequest[]): QueryProfileRequest[] {
    if (vs) {
        return vs.map((v: QueryProfileRequest) => {
            return safeQueryProfileRequest(v);
        });
    } else {
        return [];
    }
}

export interface QueryProfileResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * Profile的详细信息，请参见“2.83 Profile”类型。
    *
    * 说明
    *
    * 如果根据condition查询不到任何profile，VSP只返回&lt; profiles&gt;&lt;/ profiles &gt;。
    *
    * 如果type&#x3D;3或4且condition携带多值，当VSP查询不到对应profileID或者loginName的Profile，VSP将不返回任何Profile对象标签，即不会出现&lt; profile&gt;&lt;/ profile&gt;。
    */
    profiles: Profile[];

    /**
    * 当前登录用户对应的订户ID。
    *
    * 说明
    *
    * 该参数后续不推荐使用，建议使用Profile对象返回的subscriberId。
    */
    subscriberID: string;

    /**
    * 当前登录用户的令牌，由VSP在用户认证成功后自动生成。
    */
    userToken?: string;

    /**
    * Profile总个数。
    */
    total: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeQueryProfileResponse(v: QueryProfileResponse): QueryProfileResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.profiles = safeProfiles(v.profiles);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeQueryProfileResponses(vs: QueryProfileResponse[]): QueryProfileResponse[] {
    if (vs) {
        return vs.map((v: QueryProfileResponse) => {
            return safeQueryProfileResponse(v);
        });
    } else {
        return [];
    }
}

export interface GetBindCodeRequest {
}

function safeGetBindCodeRequest(v: GetBindCodeRequest): GetBindCodeRequest {
    if (v) {
    }
    return v;
}

function safeGetBindCodeRequests(vs: GetBindCodeRequest[]): GetBindCodeRequest[] {
    if (vs) {
        return vs.map((v: GetBindCodeRequest) => {
            return safeGetBindCodeRequest(v);
        });
    } else {
        return [];
    }
}

export interface GetBindCodeResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 二维码信息。
    *
    * {
    *
    * &quot;loginName&quot;: &quot;**** &quot;,
    *
    * &quot;subscriberId&quot;: &quot;**** &quot;,
    *
    * &quot;deviceId&quot;: &quot;**** &quot;,
    *
    * &quot;bindCode&quot;: &quot;**** &quot;,
    *
    * &quot;profileId&quot;: &quot;**** &quot;
    *
    * }
    */
    QRCode?: string;

    /**
    * 失效时间。
    */
    validTime?: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeGetBindCodeResponse(v: GetBindCodeResponse): GetBindCodeResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeGetBindCodeResponses(vs: GetBindCodeResponse[]): GetBindCodeResponse[] {
    if (vs) {
        return vs.map((v: GetBindCodeResponse) => {
            return safeGetBindCodeResponse(v);
        });
    } else {
        return [];
    }
}

export interface BindProfileRequest {

    /**
    * 绑订户帐号。
    */
    subscriberId: string;

    /**
    * 绑定订户profileId。
    */
    profileId: string;

    /**
    * 绑定订户的设备ID。
    */
    deviceId: string;

    /**
    * 验证码。
    */
    bindCode: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeBindProfileRequest(v: BindProfileRequest): BindProfileRequest {
    if (v) {
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeBindProfileRequests(vs: BindProfileRequest[]): BindProfileRequest[] {
    if (vs) {
        return vs.map((v: BindProfileRequest) => {
            return safeBindProfileRequest(v);
        });
    } else {
        return [];
    }
}

export interface BindProfileResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeBindProfileResponse(v: BindProfileResponse): BindProfileResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeBindProfileResponses(vs: BindProfileResponse[]): BindProfileResponse[] {
    if (vs) {
        return vs.map((v: BindProfileResponse) => {
            return safeBindProfileResponse(v);
        });
    } else {
        return [];
    }
}

export interface UnBindProfileRequest {

    /**
    * 解绑的profile账号。
    */
    profileId: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeUnBindProfileRequest(v: UnBindProfileRequest): UnBindProfileRequest {
    if (v) {
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeUnBindProfileRequests(vs: UnBindProfileRequest[]): UnBindProfileRequest[] {
    if (vs) {
        return vs.map((v: UnBindProfileRequest) => {
            return safeUnBindProfileRequest(v);
        });
    } else {
        return [];
    }
}

export interface UnBindProfileResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeUnBindProfileResponse(v: UnBindProfileResponse): UnBindProfileResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeUnBindProfileResponses(vs: UnBindProfileResponse[]): UnBindProfileResponse[] {
    if (vs) {
        return vs.map((v: UnBindProfileResponse) => {
            return safeUnBindProfileResponse(v);
        });
    } else {
        return [];
    }
}

export interface BindSubscriberRequest {

    /**
    * 待绑定的目标订户ID
    */
    targetSubscriberId: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeBindSubscriberRequest(v: BindSubscriberRequest): BindSubscriberRequest {
    if (v) {
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeBindSubscriberRequests(vs: BindSubscriberRequest[]): BindSubscriberRequest[] {
    if (vs) {
        return vs.map((v: BindSubscriberRequest) => {
            return safeBindSubscriberRequest(v);
        });
    } else {
        return [];
    }
}

export interface BindSubscriberResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeBindSubscriberResponse(v: BindSubscriberResponse): BindSubscriberResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeBindSubscriberResponses(vs: BindSubscriberResponse[]): BindSubscriberResponse[] {
    if (vs) {
        return vs.map((v: BindSubscriberResponse) => {
            return safeBindSubscriberResponse(v);
        });
    } else {
        return [];
    }
}

export interface UnBindSubscriberRequest {

    /**
    * 待解绑的目标订户ID
    */
    targetSubscriberId: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeUnBindSubscriberRequest(v: UnBindSubscriberRequest): UnBindSubscriberRequest {
    if (v) {
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeUnBindSubscriberRequests(vs: UnBindSubscriberRequest[]): UnBindSubscriberRequest[] {
    if (vs) {
        return vs.map((v: UnBindSubscriberRequest) => {
            return safeUnBindSubscriberRequest(v);
        });
    } else {
        return [];
    }
}

export interface UnBindSubscriberResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeUnBindSubscriberResponse(v: UnBindSubscriberResponse): UnBindSubscriberResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeUnBindSubscriberResponses(vs: UnBindSubscriberResponse[]): UnBindSubscriberResponse[] {
    if (vs) {
        return vs.map((v: UnBindSubscriberResponse) => {
            return safeUnBindSubscriberResponse(v);
        });
    } else {
        return [];
    }
}

export interface QueryBindedSubscriberRequest {
}

function safeQueryBindedSubscriberRequest(v: QueryBindedSubscriberRequest): QueryBindedSubscriberRequest {
    if (v) {
    }
    return v;
}

function safeQueryBindedSubscriberRequests(vs: QueryBindedSubscriberRequest[]): QueryBindedSubscriberRequest[] {
    if (vs) {
        return vs.map((v: QueryBindedSubscriberRequest) => {
            return safeQueryBindedSubscriberRequest(v);
        });
    } else {
        return [];
    }
}

export interface QueryBindedSubscriberResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 已绑定的订户列表
    */
    bindedSubscribers?: string[];

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeQueryBindedSubscriberResponse(v: QueryBindedSubscriberResponse): QueryBindedSubscriberResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.bindedSubscribers = v.bindedSubscribers || [];
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeQueryBindedSubscriberResponses(vs: QueryBindedSubscriberResponse[]): QueryBindedSubscriberResponse[] {
    if (vs) {
        return vs.map((v: QueryBindedSubscriberResponse) => {
            return safeQueryBindedSubscriberResponse(v);
        });
    } else {
        return [];
    }
}

export interface QueryDeviceListRequest {

    /**
    * 订户ID。
    *
    * 说明
    *
    * 1\. VSP平台支持通过subscriberID或profileID查询设备列表，如果接口请求中同时传入subscriberID和profileID参数，平台以subscriberID为准，查询对应的设备列表。
    *
    * 2\. subscriberID和profileID不允许同时为空。
    */
    subscriberID?: string;

    /**
    * ProfileID。首先获取Profile所属的Subscriber，再获取用户绑定的设备。
    */
    profileID?: string;

    /**
    * 设备类型分组，多值使用分号分隔。
    *
    * 取值范围：
    *
    * 0：STB
    *
    * 2：OTT设备（包括PC Plugin、iOS和Andriod device）
    *
    * 3：Mobile(注意这里的Mobile是MTV解决方案的设备)
    *
    * 4：all
    *
    * 如果未携带这个参数则默认取登录终端的设备类型分组。
    */
    deviceType?: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeQueryDeviceListRequest(v: QueryDeviceListRequest): QueryDeviceListRequest {
    if (v) {
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeQueryDeviceListRequests(vs: QueryDeviceListRequest[]): QueryDeviceListRequest[] {
    if (vs) {
        return vs.map((v: QueryDeviceListRequest) => {
            return safeQueryDeviceListRequest(v);
        });
    } else {
        return [];
    }
}

export interface QueryDeviceListResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 用户的设备列表。请参见“2.36 Device”类型。如果用户没有绑定设备，则不返回该参数。
    */
    devices?: Device[];

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeQueryDeviceListResponse(v: QueryDeviceListResponse): QueryDeviceListResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.devices = safeDevices(v.devices);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeQueryDeviceListResponses(vs: QueryDeviceListResponse[]): QueryDeviceListResponse[] {
    if (vs) {
        return vs.map((v: QueryDeviceListResponse) => {
            return safeQueryDeviceListResponse(v);
        });
    } else {
        return [];
    }
}

export interface ReplaceDeviceRequest {

    /**
    * 订户ID。
    */
    subscriberID: string;

    /**
    * 旧设备的逻辑设备ID，来自[Device](#_ZH-CN_TOPIC_0055055589)对象的deviceId属性。
    */
    orgDeviceID: string;

    /**
    * 目标设备的物理地址，取值包括：
    *
    * 对于STB终端，取值为设备的MAC地址。
    *
    * 对于OTT设备，取值为CA客户端插件的caDeviceId，如VMX插件生成的clientId。
    */
    destDeviceID?: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeReplaceDeviceRequest(v: ReplaceDeviceRequest): ReplaceDeviceRequest {
    if (v) {
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeReplaceDeviceRequests(vs: ReplaceDeviceRequest[]): ReplaceDeviceRequest[] {
    if (vs) {
        return vs.map((v: ReplaceDeviceRequest) => {
            return safeReplaceDeviceRequest(v);
        });
    } else {
        return [];
    }
}

export interface ReplaceDeviceResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeReplaceDeviceResponse(v: ReplaceDeviceResponse): ReplaceDeviceResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeReplaceDeviceResponses(vs: ReplaceDeviceResponse[]): ReplaceDeviceResponse[] {
    if (vs) {
        return vs.map((v: ReplaceDeviceResponse) => {
            return safeReplaceDeviceResponse(v);
        });
    } else {
        return [];
    }
}

export interface ModifyDeviceInfoRequest {

    /**
    * 设备信息。
    */
    device: Device;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeModifyDeviceInfoRequest(v: ModifyDeviceInfoRequest): ModifyDeviceInfoRequest {
    if (v) {
        v.device = safeDevice(v.device);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeModifyDeviceInfoRequests(vs: ModifyDeviceInfoRequest[]): ModifyDeviceInfoRequest[] {
    if (vs) {
        return vs.map((v: ModifyDeviceInfoRequest) => {
            return safeModifyDeviceInfoRequest(v);
        });
    } else {
        return [];
    }
}

export interface ModifyDeviceInfoResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeModifyDeviceInfoResponse(v: ModifyDeviceInfoResponse): ModifyDeviceInfoResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeModifyDeviceInfoResponses(vs: ModifyDeviceInfoResponse[]): ModifyDeviceInfoResponse[] {
    if (vs) {
        return vs.map((v: ModifyDeviceInfoResponse) => {
            return safeModifyDeviceInfoResponse(v);
        });
    } else {
        return [];
    }
}

export interface CheckPasswordRequest {

    /**
    * 用户密码，此处是明文。
    */
    password: string;

    /**
    * 密码类型。
    *
    * 取值范围：
    *
    * 0：Profile密码
    *
    * 1：订购密码
    *
    * 2：订户登录密码
    *
    * 3：主Profile密码
    *
    * 4：父母控制密码
    *
    * 如果不携带此参数，默认值为3。
    *
    * 说明
    *
    * 用户网关（VSP） Server只负责根据type参数的值校验对应的密码，如果密码正确则校验通过。具体的调用场景由客户端决定。
    */
    type?: string;

    /**
    * 客户端请求密码校验的请求的唯一标识
    *
    * 当客户端需要执行需校验密码后才能继续的事务时，需在校验密码时携带此字段到，服务端生成事务并返回事务token，客户端执行后续的操作需携带此correlator和对应返回的事务token才可以被服务端认证通过
    *
    * 事务token有效期15分钟
    */
    correlator?: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeCheckPasswordRequest(v: CheckPasswordRequest): CheckPasswordRequest {
    if (v) {
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeCheckPasswordRequests(vs: CheckPasswordRequest[]): CheckPasswordRequest[] {
    if (vs) {
        return vs.map((v: CheckPasswordRequest) => {
            return safeCheckPasswordRequest(v);
        });
    } else {
        return [];
    }
}

export interface CheckPasswordResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 当客户端需要执行需校验密码后才能继续的事务时，需在校验密码时携带此字段到，服务端生成事务并返回事务token，客户端执行后续的操作需携带correlator和此事务token才可以被服务端认证通过
    */
    token?: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeCheckPasswordResponse(v: CheckPasswordResponse): CheckPasswordResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeCheckPasswordResponses(vs: CheckPasswordResponse[]): CheckPasswordResponse[] {
    if (vs) {
        return vs.map((v: CheckPasswordResponse) => {
            return safeCheckPasswordResponse(v);
        });
    } else {
        return [];
    }
}

export interface ModifyPasswordRequest {

    /**
    * 旧密码，必须是明文。
    */
    oldPwd: string;

    /**
    * 新密码，必须是明文。
    */
    newPwd: string;

    /**
    * 确认密码，必须和newPwd保持一致，必须是明文。
    */
    confirmPwd: string;

    /**
    * 新密码，以明文形式传递。
    *
    * 说明
    *
    * 仅在**type**为2的时候需要传入该参数。
    */
    encryptPwd?: string;

    /**
    * 密码类型。
    *
    * 取值范围：
    *
    * 0：子Profile密码
    *
    * 1：订购密码
    *
    * 2：订户登录密码
    *
    * 3：主Profile密码
    *
    * 4：父母控制密码
    *
    * 6：逻辑设备密码
    *
    * 如不携带此参数，默认值为0。
    */
    type?: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeModifyPasswordRequest(v: ModifyPasswordRequest): ModifyPasswordRequest {
    if (v) {
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeModifyPasswordRequests(vs: ModifyPasswordRequest[]): ModifyPasswordRequest[] {
    if (vs) {
        return vs.map((v: ModifyPasswordRequest) => {
            return safeModifyPasswordRequest(v);
        });
    } else {
        return [];
    }
}

export interface ModifyPasswordResponse {

    /**
    * 操作返回结果。
    */
    result: Result;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeModifyPasswordResponse(v: ModifyPasswordResponse): ModifyPasswordResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeModifyPasswordResponses(vs: ModifyPasswordResponse[]): ModifyPasswordResponse[] {
    if (vs) {
        return vs.map((v: ModifyPasswordResponse) => {
            return safeModifyPasswordResponse(v);
        });
    } else {
        return [];
    }
}

export interface ResetPasswordRequest {

    /**
    * 待重置密码的Profile编号。
    */
    profileID: string;

    /**
    * 密码类型。
    *
    * 取值范围：
    *
    * 0：子Profile密码
    *
    * 1：订购密码
    *
    * 2：订户登录密码
    *
    * 4：父母控制密码
    *
    * 如不携带此参数，默认值为0。
    *
    * 说明
    *
    * 如果在线重置订户登录密码，必须携带encryptPwd。
    */
    type?: string;

    /**
    * 重置后的密码，必须是明文。
    *
    * 如果未携带则使用VSP平台配置的默认密码。
    *
    * 说明
    *
    * Profile密码和主profile密码的重置不支持指定重置后的密码，如果要修改其他值建议使用Profile接口。
    */
    resetPwd?: string;

    /**
    * 新密码，以明文方式传递。
    *
    * 说明
    *
    * 仅在**type**为2的时候需要传入该参数。
    */
    encryptPwd?: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeResetPasswordRequest(v: ResetPasswordRequest): ResetPasswordRequest {
    if (v) {
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeResetPasswordRequests(vs: ResetPasswordRequest[]): ResetPasswordRequest[] {
    if (vs) {
        return vs.map((v: ResetPasswordRequest) => {
            return safeResetPasswordRequest(v);
        });
    } else {
        return [];
    }
}

export interface ResetPasswordResponse {

    /**
    * 操作返回结果。
    */
    result: Result;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeResetPasswordResponse(v: ResetPasswordResponse): ResetPasswordResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeResetPasswordResponses(vs: ResetPasswordResponse[]): ResetPasswordResponse[] {
    if (vs) {
        return vs.map((v: ResetPasswordResponse) => {
            return safeResetPasswordResponse(v);
        });
    } else {
        return [];
    }
}

export interface QrCodeAuthenticateRequest {
}

function safeQrCodeAuthenticateRequest(v: QrCodeAuthenticateRequest): QrCodeAuthenticateRequest {
    if (v) {
    }
    return v;
}

function safeQrCodeAuthenticateRequests(vs: QrCodeAuthenticateRequest[]): QrCodeAuthenticateRequest[] {
    if (vs) {
        return vs.map((v: QrCodeAuthenticateRequest) => {
            return safeQrCodeAuthenticateRequest(v);
        });
    } else {
        return [];
    }
}

export interface QrCodeAuthenticateResponse {

    /**
    * 返回结果
    */
    result: Result;

    /**
    * 二维码信息
    *
    * {
    *
    * &quot;loginName&quot;: &quot;**** &quot;,
    *
    * &quot;subscriberId&quot;: &quot;**** &quot;,
    *
    * &quot;deviceId&quot;: &quot;**** &quot;,
    *
    * &quot;bindCode&quot;: &quot;**** &quot;
    *
    * }
    */
    QRCode?: string;

    /**
    * 失效时间
    */
    validTime?: string;

    /**
    * 扩展信息
    */
    extensionFields?: NamedParameter[];
}

function safeQrCodeAuthenticateResponse(v: QrCodeAuthenticateResponse): QrCodeAuthenticateResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeQrCodeAuthenticateResponses(vs: QrCodeAuthenticateResponse[]): QrCodeAuthenticateResponse[] {
    if (vs) {
        return vs.map((v: QrCodeAuthenticateResponse) => {
            return safeQrCodeAuthenticateResponse(v);
        });
    } else {
        return [];
    }
}

export interface QueryQrCodeAuthenticateStatusRequest {
}

function safeQueryQrCodeAuthenticateStatusRequest(v: QueryQrCodeAuthenticateStatusRequest): QueryQrCodeAuthenticateStatusRequest {
    if (v) {
    }
    return v;
}

function safeQueryQrCodeAuthenticateStatusRequests(vs: QueryQrCodeAuthenticateStatusRequest[]): QueryQrCodeAuthenticateStatusRequest[] {
    if (vs) {
        return vs.map((v: QueryQrCodeAuthenticateStatusRequest) => {
            return safeQueryQrCodeAuthenticateStatusRequest(v);
        });
    } else {
        return [];
    }
}

export interface QueryQrCodeAuthenticateStatusResponse {

    /**
    * 查询结果。
    */
    result: Result;

    /**
    * 二维码认证状态。
    *
    * WAITING
    *
    * SCANNED
    *
    * SUCCESSED
    *
    * FAILED
    */
    status?: string;

    /**
    * 扫码者的订户Id。
    */
    scannedSubscriberId?: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeQueryQrCodeAuthenticateStatusResponse(v: QueryQrCodeAuthenticateStatusResponse): QueryQrCodeAuthenticateStatusResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeQueryQrCodeAuthenticateStatusResponses(vs: QueryQrCodeAuthenticateStatusResponse[]): QueryQrCodeAuthenticateStatusResponse[] {
    if (vs) {
        return vs.map((v: QueryQrCodeAuthenticateStatusResponse) => {
            return safeQueryQrCodeAuthenticateStatusResponse(v);
        });
    } else {
        return [];
    }
}

export interface ReportQrCodeAuthenticateStatusRequest {

    /**
    * 二维码信息。
    *
    * {
    *
    * &quot;loginName&quot;: &quot;**** &quot;,
    *
    * &quot;subscriberId&quot;: &quot;**** &quot;,
    *
    * &quot;deviceId&quot;: &quot;**** &quot;,
    *
    * &quot;bindCode&quot;: &quot;**** &quot;
    *
    * }
    */
    QRCode?: string;

    /**
    * 当前二维码认证状态。
    *
    * SCANNED
    *
    * SUCCESSED
    *
    * FAILED
    */
    currentStatus: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeReportQrCodeAuthenticateStatusRequest(v: ReportQrCodeAuthenticateStatusRequest): ReportQrCodeAuthenticateStatusRequest {
    if (v) {
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeReportQrCodeAuthenticateStatusRequests(vs: ReportQrCodeAuthenticateStatusRequest[]): ReportQrCodeAuthenticateStatusRequest[] {
    if (vs) {
        return vs.map((v: ReportQrCodeAuthenticateStatusRequest) => {
            return safeReportQrCodeAuthenticateStatusRequest(v);
        });
    } else {
        return [];
    }
}

export interface ReportQrCodeAuthenticateStatusResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeReportQrCodeAuthenticateStatusResponse(v: ReportQrCodeAuthenticateStatusResponse): ReportQrCodeAuthenticateStatusResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeReportQrCodeAuthenticateStatusResponses(vs: ReportQrCodeAuthenticateStatusResponse[]): ReportQrCodeAuthenticateStatusResponse[] {
    if (vs) {
        return vs.map((v: ReportQrCodeAuthenticateStatusResponse) => {
            return safeReportQrCodeAuthenticateStatusResponse(v);
        });
    } else {
        return [];
    }
}

export interface QuitQrCodeAuthenticateRequest {
}

function safeQuitQrCodeAuthenticateRequest(v: QuitQrCodeAuthenticateRequest): QuitQrCodeAuthenticateRequest {
    if (v) {
    }
    return v;
}

function safeQuitQrCodeAuthenticateRequests(vs: QuitQrCodeAuthenticateRequest[]): QuitQrCodeAuthenticateRequest[] {
    if (vs) {
        return vs.map((v: QuitQrCodeAuthenticateRequest) => {
            return safeQuitQrCodeAuthenticateRequest(v);
        });
    } else {
        return [];
    }
}

export interface QuitQrCodeAuthenticateResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeQuitQrCodeAuthenticateResponse(v: QuitQrCodeAuthenticateResponse): QuitQrCodeAuthenticateResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeQuitQrCodeAuthenticateResponses(vs: QuitQrCodeAuthenticateResponse[]): QuitQrCodeAuthenticateResponse[] {
    if (vs) {
        return vs.map((v: QuitQrCodeAuthenticateResponse) => {
            return safeQuitQrCodeAuthenticateResponse(v);
        });
    } else {
        return [];
    }
}

export interface QueryQrCodeAuthenticatedSubscriberRequest {
}

function safeQueryQrCodeAuthenticatedSubscriberRequest(v: QueryQrCodeAuthenticatedSubscriberRequest): QueryQrCodeAuthenticatedSubscriberRequest {
    if (v) {
    }
    return v;
}

function safeQueryQrCodeAuthenticatedSubscriberRequests(vs: QueryQrCodeAuthenticatedSubscriberRequest[]): QueryQrCodeAuthenticatedSubscriberRequest[] {
    if (vs) {
        return vs.map((v: QueryQrCodeAuthenticatedSubscriberRequest) => {
            return safeQueryQrCodeAuthenticatedSubscriberRequest(v);
        });
    } else {
        return [];
    }
}

export interface QueryQrCodeAuthenticatedSubscriberResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 扫码认证成功的订户Id。
    */
    qrCodeAuthenticatedSubscriberId?: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeQueryQrCodeAuthenticatedSubscriberResponse(v: QueryQrCodeAuthenticatedSubscriberResponse): QueryQrCodeAuthenticatedSubscriberResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeQueryQrCodeAuthenticatedSubscriberResponses(vs: QueryQrCodeAuthenticatedSubscriberResponse[]): QueryQrCodeAuthenticatedSubscriberResponse[] {
    if (vs) {
        return vs.map((v: QueryQrCodeAuthenticatedSubscriberResponse) => {
            return safeQueryQrCodeAuthenticatedSubscriberResponse(v);
        });
    } else {
        return [];
    }
}

export interface SetQrCodeAuthenticatedSubscriberRequest {

    /**
    * 扫码认证成功的订户Id。
    */
    qrCodeAuthenticatedSubscriberId: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeSetQrCodeAuthenticatedSubscriberRequest(v: SetQrCodeAuthenticatedSubscriberRequest): SetQrCodeAuthenticatedSubscriberRequest {
    if (v) {
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeSetQrCodeAuthenticatedSubscriberRequests(vs: SetQrCodeAuthenticatedSubscriberRequest[]): SetQrCodeAuthenticatedSubscriberRequest[] {
    if (vs) {
        return vs.map((v: SetQrCodeAuthenticatedSubscriberRequest) => {
            return safeSetQrCodeAuthenticatedSubscriberRequest(v);
        });
    } else {
        return [];
    }
}

export interface SetQrCodeAuthenticatedSubscriberResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeSetQrCodeAuthenticatedSubscriberResponse(v: SetQrCodeAuthenticatedSubscriberResponse): SetQrCodeAuthenticatedSubscriberResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeSetQrCodeAuthenticatedSubscriberResponses(vs: SetQrCodeAuthenticatedSubscriberResponse[]): SetQrCodeAuthenticatedSubscriberResponse[] {
    if (vs) {
        return vs.map((v: SetQrCodeAuthenticatedSubscriberResponse) => {
            return safeSetQrCodeAuthenticatedSubscriberResponse(v);
        });
    } else {
        return [];
    }
}

export interface QueryProductRequest {

    /**
    * 查询类型，取值包括：
    *
    * ALL：查询所有产品
    *
    * UNORDERED：查询所有未订购产品
    *
    * BY_IDLIST：根据ID列表查询
    *
    * 默认值为ALL。
    */
    queryType: string;

    /**
    * 产品ID。
    *
    * 当queryType&#x3D;BY_IDLIST时，为必填，同时后续入参均不再生效。
    */
    productIds?: string[];

    /**
    * 产品类型，取值包括：
    *
    * ORDER_BY_CYCLE：包周期
    *
    * PPV：按次
    *
    * ALL：全部
    *
    * 默认值为ALL。
    */
    productType?: string;

    /**
    * 一次查询的总条数，取值范围为-1或大于0的值。
    *
    * 如果不传该参数，默认为-1。表示获取全部记录。
    */
    count?: string;

    /**
    * 查询的起始位置。
    *
    * 从0开始（即0表示第一个）。如不传默认取0。
    */
    offset?: string;

    /**
    * 是否查询基础包，取值包括：
    *
    * NO：非基础包
    *
    * YES：基础包
    *
    * ALL：所有
    *
    * 默认值为ALL。
    */
    isMain?: string;

    /**
    * 是否返回不支持在线订购的产品，取值包括：
    *
    * NO：不返回
    *
    * YES：返回
    *
    * 默认取值为NO.
    */
    isEnableOnlinePurchase?: string;
}

function safeQueryProductRequest(v: QueryProductRequest): QueryProductRequest {
    if (v) {
        v.productIds = v.productIds || [];
    }
    return v;
}

function safeQueryProductRequests(vs: QueryProductRequest[]): QueryProductRequest[] {
    if (vs) {
        return vs.map((v: QueryProductRequest) => {
            return safeQueryProductRequest(v);
        });
    } else {
        return [];
    }
}

export interface QueryProductResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 产品总个数，查询成功返回。
    */
    countTotal?: string;

    /**
    * 产品列表。
    */
    productList?: Product[];
}

function safeQueryProductResponse(v: QueryProductResponse): QueryProductResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.productList = safeProducts(v.productList);
    }
    return v;
}

function safeQueryProductResponses(vs: QueryProductResponse[]): QueryProductResponse[] {
    if (vs) {
        return vs.map((v: QueryProductResponse) => {
            return safeQueryProductResponse(v);
        });
    } else {
        return [];
    }
}

export interface SubscribeProductRequest {

    /**
    * 产品订购请求。
    */
    subscribe: Subscribe;

    /**
    * 订购的验证类型。
    *
    * 1：主profile密码订购
    */
    type: string;

    /**
    * 验证码。
    */
    correlator: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeSubscribeProductRequest(v: SubscribeProductRequest): SubscribeProductRequest {
    if (v) {
        v.subscribe = safeSubscribe(v.subscribe);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeSubscribeProductRequests(vs: SubscribeProductRequest[]): SubscribeProductRequest[] {
    if (vs) {
        return vs.map((v: SubscribeProductRequest) => {
            return safeSubscribeProductRequest(v);
        });
    } else {
        return [];
    }
}

export interface SubscribeProductResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 透传第三方系统的错误码，key包括：
    *
    * OriginCode：BOSS返回的错误码
    *
    * OriginMsg1：BOSS返回的错误提示(语种1)
    *
    * OriginMsg2：BOSS返回的错误提示(语种2)
    */
    originInfo?: NamedParameter[];

    /**
    * 如果订购成功或者重复订购，返回新生成或者重复的订购关系。
    */
    subscription?: Subscription;

    /**
    * 客户端集成的是PlayReady且本次已订购成功，VSP平台下发获取CA License的触发器。[字段预留]
    */
    triggers?: GetLicenseTrigger[];

    /**
    * 如果订购成功，返回获得促销码。[字段预留]
    */
    promoCodes?: PromoCode[];

    /**
    * 当前信用额度周期内剩余的信用额度。
    *
    * 说明：
    *
    * 如果User Profile的信用帐期额度不足、订户的信用帐期额度不足、订户的周期信用金额额度不足或订户的周期信用次数额度不足，返回账户的信用额度信息。
    */
    remanentCredit?: string;

    /**
    * 当前信用额度周期的剩余时间，单位：分钟。
    *
    * 说明：
    *
    * 如果User Profile的信用帐期额度不足、订户的信用帐期额度不足、订户的周期信用金额额度不足或订户的周期信用次数额度不足，返回账户的信用额度信息。
    */
    remanentCreditDuration?: string;

    /**
    * 单个信用额度周期的总信用额度。
    *
    * 说明：
    *
    * 如果User Profile的信用帐期额度不足、订户的信用帐期额度不足、订户的周期信用金额额度不足或订户的周期信用次数额度不足，返回账户的信用额度信息。
    */
    totalCredit?: string;

    /**
    * 信用额度周期，表示信用额度持续可用时间。
    *
    * 单位：分钟。
    *
    * 说明：
    *
    * 如果User Profile的信用帐期额度不足、订户的信用帐期额度不足、订户的周期信用金额额度不足或订户的周期信用次数额度不足，返回账户的信用额度信息。
    */
    totalCreditDuration?: string;

    /**
    * 如果产品依赖检查失败，返回依赖的产品信息。
    */
    reliantProducts?: Product[];

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeSubscribeProductResponse(v: SubscribeProductResponse): SubscribeProductResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.originInfo = safeNamedParameters(v.originInfo);
        v.subscription = safeSubscription(v.subscription);
        v.triggers = safeGetLicenseTriggers(v.triggers);
        v.promoCodes = safePromoCodes(v.promoCodes);
        v.reliantProducts = safeProducts(v.reliantProducts);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeSubscribeProductResponses(vs: SubscribeProductResponse[]): SubscribeProductResponse[] {
    if (vs) {
        return vs.map((v: SubscribeProductResponse) => {
            return safeSubscribeProductResponse(v);
        });
    } else {
        return [];
    }
}

export interface CancelSubscribeRequest {

    /**
    * 产品退订请求。
    */
    cancelSubscribe: CancelSubscribe;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeCancelSubscribeRequest(v: CancelSubscribeRequest): CancelSubscribeRequest {
    if (v) {
        v.cancelSubscribe = safeCancelSubscribe(v.cancelSubscribe);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeCancelSubscribeRequests(vs: CancelSubscribeRequest[]): CancelSubscribeRequest[] {
    if (vs) {
        return vs.map((v: CancelSubscribeRequest) => {
            return safeCancelSubscribeRequest(v);
        });
    } else {
        return [];
    }
}

export interface CancelSubscribeResponse {

    /**
    * 操作结果。
    */
    result?: Result;

    /**
    * 第三方系统返回的信息，key包括：
    *
    * OriginCode：BOSS返回的错误码
    *
    * OriginMsg1：BOSS返回的错误提示(语种1)
    *
    * OriginMsg2：BOSS返回的错误提示(语种2)
    */
    originInfo?: NamedParameter[];

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeCancelSubscribeResponse(v: CancelSubscribeResponse): CancelSubscribeResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.originInfo = safeNamedParameters(v.originInfo);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeCancelSubscribeResponses(vs: CancelSubscribeResponse[]): CancelSubscribeResponse[] {
    if (vs) {
        return vs.map((v: CancelSubscribeResponse) => {
            return safeCancelSubscribeResponse(v);
        });
    } else {
        return [];
    }
}

export interface QueryMyContentRequest {

    /**
    * 内容类型。如果有多个，使用英文逗号分隔。
    *
    * 取值范围：
    *
    * VAS
    *
    * CHANNEL
    *
    * AUDIO_CHANNEL
    *
    * VIDEO_CHANNEL
    *
    * WEB_CHANNEL[字段预留]
    *
    * FILECAST_CHANNEL[字段预留]
    *
    * SUBJECT[字段预留]
    *
    * VOD
    *
    * AUDIO_VOD
    *
    * VIDEO_VOD
    *
    * PROGRAM
    */
    contentType: string;

    /**
    * 一次查询的总条数，不能设置为-1，调用者一定要指定获取数据的总条数，最大条数默认不超过50，最大条数可配置，超过最大条数返回错误。
    */
    count: string;

    /**
    * 查询的起始位置，从0开始（即0表示第一个）。
    */
    offset: string;

    /**
    * 查询的排序方式。如果有多个，使用英文逗号分隔。
    *
    * 取值范围：
    *
    * NAME:ASC：按名称升序排序
    *
    * NAME:DESC：按名称降序排序
    *
    * STARTTIME:ASC：按订购开始时间升序排序
    *
    * STARTTIME:DESC：按订购开始时间降序排序
    *
    * CONTENTTYPE:ASC：按内容类型升序排序
    *
    * CONTENTTYPE:DESC：按内容类型降序排序
    *
    * CHANNELNO:ASC：按频道号升序排序
    *
    * ENDTIME:ASC：按订购失效时间升序排序
    *
    * ENDTIME:DESC：按订购失效时间降序排列
    *
    * 如果不传此参数，则不排序。
    */
    sortType?: string;

    /**
    * 产品类型。取值如下：
    *
    * 0：包周期，比如包天、包周、包月等
    *
    * 1：按次
    *
    * 如果不携带该参数，则查询所有已订购产品下的内容，不区分产品类型。
    */
    productType?: string;

    /**
    * 栏目类型。
    *
    * 当contentType为SUBJECT时该参数有效，不需要区分栏目类型时，该值可不传，查询多个指定类型时，使用英文逗号分隔。
    */
    subjectType?: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeQueryMyContentRequest(v: QueryMyContentRequest): QueryMyContentRequest {
    if (v) {
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeQueryMyContentRequests(vs: QueryMyContentRequest[]): QueryMyContentRequest[] {
    if (vs) {
        return vs.map((v: QueryMyContentRequest) => {
            return safeQueryMyContentRequest(v);
        });
    } else {
        return [];
    }
}

export interface QueryMyContentResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 内容总个数。
    */
    total: string;

    /**
    * 内容列表。
    *
    * 如果没有，不返回。
    */
    contents?: MyContent[];

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeQueryMyContentResponse(v: QueryMyContentResponse): QueryMyContentResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.contents = safeMyContents(v.contents);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeQueryMyContentResponses(vs: QueryMyContentResponse[]): QueryMyContentResponse[] {
    if (vs) {
        return vs.map((v: QueryMyContentResponse) => {
            return safeQueryMyContentResponse(v);
        });
    } else {
        return [];
    }
}

export interface QuerySubscriptionRequest {

    /**
    * Profile编号，表示获取此Profile的订购关系。
    *
    * 如果不填则获取当前登录Profile订购的产品，如果当前登录用户是主Profile，则查询订户下所有订购的产品。
    *
    * 如果传入是的是主profileID，则返回主Profile订购的产品。
    *
    * 如果传入的是子profileID，则返回子Profile订购的产品。
    *
    * 如果传-1，则表示查询订户下所有订购的产品。
    */
    profileID?: string;

    /**
    * 开始日期，取值为距离1970年1月1号的毫秒数。
    *
    * fromDate与toDate必须同时存在或同时不存在，不可只传一个。
    *
    * 说明：如果不携带fromDate和toDate，则表示查询已订购和取消订购但是还可以使用的产品，否则表示查询用户所有的订购历史（最多支持半年）。
    */
    fromDate?: string;

    /**
    * 结束日期，取值为距离1970年1月1号的毫秒数。
    *
    * fromDate与toDate必须同时存在或同时不存在，不可只传一个。
    */
    toDate?: string;

    /**
    * 一次查询的总条数，不能设置为-1，调用者一定要指定获取数据的总条数，最大条数默认不超过50，最大条数可配置，超过最大条数返回错误。
    */
    count: string;

    /**
    * 查询的起始位置。
    *
    * 从0开始（即0表示第一个）。如不传默认取0。
    */
    offset: string;

    /**
    * 产品类型。
    *
    * 取值范围：
    *
    * 1：按次
    *
    * 0：包周期，比如包天、包周、包月等
    *
    * 如果不携带此参数，则查询所有产品。
    */
    productType?: string;

    /**
    * 产品绑定的逻辑设备ID。
    *
    * 如果指定该参数，则只返回该设备上的非终端共享的订购关系。
    *
    * 如果不携带此参数，则查询所有产品。
    */
    deviceID?: string;

    /**
    * 是否查询基础包，取值包括：
    *
    * 0：非基础包
    *
    * 1：基础包
    *
    * -1：所有
    *
    * 如果不携带此参数，则查询所有产品。
    */
    isMain?: string;

    /**
    * 订购关系键值，订购关系的唯一主键，如果终端携带此参数，上述参数VSP将不关注。
    */
    subscriptionKey?: string;

    /**
    * 订购关系排序方式，取值包括：
    *
    * ORDERTIME:ASC：按订购时间升序排列
    *
    * ORDERTIME:DESC：按订购时间降序排列
    *
    * ENDTIME:ASC：按订购关系失效时间升序排列
    *
    * ENDTIME:DESC：按订购关系失效时间降序排列
    *
    * CONTENTNAME:ASC：按定价对象名称升序排列
    *
    * CONTENTNAME:DESC：按定价对象名称降序排列
    *
    * 默认值为ORDERTIME:DESC。
    */
    sortType?: string;

    /**
    * 查询订购记录类型：
    *
    * 0：查询当前已生效的记录
    *
    * 1：查询已生效和未来生效的记录
    *
    * 默认为0。
    */
    queryType?: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeQuerySubscriptionRequest(v: QuerySubscriptionRequest): QuerySubscriptionRequest {
    if (v) {
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeQuerySubscriptionRequests(vs: QuerySubscriptionRequest[]): QuerySubscriptionRequest[] {
    if (vs) {
        return vs.map((v: QuerySubscriptionRequest) => {
            return safeQuerySubscriptionRequest(v);
        });
    } else {
        return [];
    }
}

export interface QuerySubscriptionResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 产品总个数，查询成功返回。
    */
    total?: string;

    /**
    * 已订购产品列表，查询成功后返回。如果查询失败或没有已订购产品，不返回。
    */
    products?: Subscription[];

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeQuerySubscriptionResponse(v: QuerySubscriptionResponse): QuerySubscriptionResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.products = safeSubscriptions(v.products);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeQuerySubscriptionResponses(vs: QuerySubscriptionResponse[]): QuerySubscriptionResponse[] {
    if (vs) {
        return vs.map((v: QuerySubscriptionResponse) => {
            return safeQuerySubscriptionResponse(v);
        });
    } else {
        return [];
    }
}

export interface QueryChannelSubjectListRequest {

    /**
    * 父栏目ID，查询此栏目下的子栏目列表。如果为空，标示查询 “-1”根栏目下的栏目列表。
    */
    subjectID?: string;

    /**
    * 栏目类型。
    *
    * 具体取值包括：
    *
    * CHANNEL
    *
    * AUDIO_CHANNEL
    *
    * VIDEO_CHANNEL
    *
    * 说明
    *
    * 为空时，表示获取所有类型的频道栏目。
    */
    contentTypes?: string[];

    /**
    * 一次查询的总条数，不能设置为-1，调用者一定要指定获取数据的总条数，最大条数默认不超过50，超过最大条数返回错误。
    */
    count: string;

    /**
    * 查询的起始位置。从0开始（即0表示第一个）。
    */
    offset: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeQueryChannelSubjectListRequest(v: QueryChannelSubjectListRequest): QueryChannelSubjectListRequest {
    if (v) {
        v.contentTypes = v.contentTypes || [];
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeQueryChannelSubjectListRequests(vs: QueryChannelSubjectListRequest[]): QueryChannelSubjectListRequest[] {
    if (vs) {
        return vs.map((v: QueryChannelSubjectListRequest) => {
            return safeQueryChannelSubjectListRequest(v);
        });
    } else {
        return [];
    }
}

export interface QueryChannelSubjectListResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 子栏目总个数。
    */
    total: string;

    /**
    * 频道栏目列表，频道栏目属性请参见“2.111 Subject”类型。
    *
    * 如果查询结果为空，不返回该参数。
    */
    subjects?: Subject[];

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeQueryChannelSubjectListResponse(v: QueryChannelSubjectListResponse): QueryChannelSubjectListResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.subjects = safeSubjects(v.subjects);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeQueryChannelSubjectListResponses(vs: QueryChannelSubjectListResponse[]): QueryChannelSubjectListResponse[] {
    if (vs) {
        return vs.map((v: QueryChannelSubjectListResponse) => {
            return safeQueryChannelSubjectListResponse(v);
        });
    } else {
        return [];
    }
}

export interface QueryChannelListBySubjectRequest {

    /**
    * 栏目ID，取值仅支持：
    *
    * -1
    *
    * 叶子栏目
    *
    * 其中栏目ID不传、或传-1时，表示查询所有频道；当栏目ID为叶子栏目是，表示获取此叶子栏目下的频道列表。
    */
    subjectID?: string;

    /**
    * 一次查询的总条数，不能设置为-1，终端一定要指定获取数据的总条数，最大条数默认不超过1000，最大条数可配置，超过最大条数返回错误。
    */
    count: string;

    /**
    * 查询的起始位置。默认值为0，表示从第一个频道开始。
    */
    offset: string;

    /**
    * 内容类型。
    *
    * 取值范围：
    *
    * CHANNEL
    *
    * AUDIO_CHANNEL
    *
    * VIDEO_CHANNEL
    *
    * 说明
    *
    * 为空时，表示获取所有类型频道。
    */
    contentTypes?: string[];

    /**
    * 频道查询使用的命名空间，如果不指定，VSP将按照当前用户会话保存的命名空间返回。
    */
    channelNamespace?: string;

    /**
    * 是否返回所有物理频道。
    *
    * 0：否（默认）
    *
    * 1：是
    */
    isReturnAllMedia?: string;

    /**
    * 搜索条件。
    */
    filter?: ChannelFilter;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeQueryChannelListBySubjectRequest(v: QueryChannelListBySubjectRequest): QueryChannelListBySubjectRequest {
    if (v) {
        v.contentTypes = v.contentTypes || [];
        v.filter = safeChannelFilter(v.filter);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeQueryChannelListBySubjectRequests(vs: QueryChannelListBySubjectRequest[]): QueryChannelListBySubjectRequest[] {
    if (vs) {
        return vs.map((v: QueryChannelListBySubjectRequest) => {
            return safeQueryChannelListBySubjectRequest(v);
        });
    } else {
        return [];
    }
}

export interface QueryChannelListBySubjectResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 频道总个数。
    */
    total: string;

    /**
    * 频道列表信息。
    *
    * 如果没有获取到频道，不返回该参数。
    */
    channelDetails?: ChannelDetail[];

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeQueryChannelListBySubjectResponse(v: QueryChannelListBySubjectResponse): QueryChannelListBySubjectResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.channelDetails = safeChannelDetails(v.channelDetails);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeQueryChannelListBySubjectResponses(vs: QueryChannelListBySubjectResponse[]): QueryChannelListBySubjectResponse[] {
    if (vs) {
        return vs.map((v: QueryChannelListBySubjectResponse) => {
            return safeQueryChannelListBySubjectResponse(v);
        });
    } else {
        return [];
    }
}

export interface QueryAllChannelRequest {

    /**
    * 频道查询使用的命名空间，如果不指定，VSP将按照当前用户会话保存的名空间返回。
    */
    channelNamespace?: string;

    /**
    * 是否返回所有物理频道。
    *
    * 0：否（默认）
    *
    * 1：是
    */
    isReturnAllMedia?: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];

    /**
    * 通过GET方式传递，取值为心跳消息中的userFilter。
    */
    userFilter: string;
}

function safeQueryAllChannelRequest(v: QueryAllChannelRequest): QueryAllChannelRequest {
    if (v) {
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeQueryAllChannelRequests(vs: QueryAllChannelRequest[]): QueryAllChannelRequest[] {
    if (vs) {
        return vs.map((v: QueryAllChannelRequest) => {
            return safeQueryAllChannelRequest(v);
        });
    } else {
        return [];
    }
}

export interface QueryAllChannelResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 频道总个数。
    */
    total: string;

    /**
    * 频道列表信息。
    *
    * 此接口中返回的ChannelDetail对象不会包含ChannelDynamicProperties中的属性，如ID、channelNO等。
    *
    * 如果没有获取到频道，不返回该参数。
    */
    channelDetails?: ChannelDetail[];

    /**
    * 频道版本号。
    */
    channelVersion?: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeQueryAllChannelResponse(v: QueryAllChannelResponse): QueryAllChannelResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.channelDetails = safeChannelDetails(v.channelDetails);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeQueryAllChannelResponses(vs: QueryAllChannelResponse[]): QueryAllChannelResponse[] {
    if (vs) {
        return vs.map((v: QueryAllChannelResponse) => {
            return safeQueryAllChannelResponse(v);
        });
    } else {
        return [];
    }
}

export interface QueryAllChannelDynamicPropertiesRequest {

    /**
    * 频道查询使用的命名空间，如果不指定，VSP将按照当前用户会话保存的名空间返回。
    */
    channelNamespace?: string;

    /**
    * 是否返回所有物理频道。
    *
    * 0：否（默认）
    *
    * 1：是
    */
    isReturnAllMedia?: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeQueryAllChannelDynamicPropertiesRequest(v: QueryAllChannelDynamicPropertiesRequest): QueryAllChannelDynamicPropertiesRequest {
    if (v) {
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeQueryAllChannelDynamicPropertiesRequests(vs: QueryAllChannelDynamicPropertiesRequest[]): QueryAllChannelDynamicPropertiesRequest[] {
    if (vs) {
        return vs.map((v: QueryAllChannelDynamicPropertiesRequest) => {
            return safeQueryAllChannelDynamicPropertiesRequest(v);
        });
    } else {
        return [];
    }
}

export interface QueryAllChannelDynamicPropertiesResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 频道总个数。
    */
    total: string;

    /**
    * 频道列表信息。
    *
    * 如果没有获取到频道，不返回该参数。
    */
    channelDynamaicProp?: ChannelDynamicProperties[];

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeQueryAllChannelDynamicPropertiesResponse(v: QueryAllChannelDynamicPropertiesResponse): QueryAllChannelDynamicPropertiesResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.channelDynamaicProp = safeChannelDynamicPropertiess(v.channelDynamaicProp);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeQueryAllChannelDynamicPropertiesResponses(vs: QueryAllChannelDynamicPropertiesResponse[]): QueryAllChannelDynamicPropertiesResponse[] {
    if (vs) {
        return vs.map((v: QueryAllChannelDynamicPropertiesResponse) => {
            return safeQueryAllChannelDynamicPropertiesResponse(v);
        });
    } else {
        return [];
    }
}

export interface QueryPlaybillListRequest {

    /**
    * 频道查询条件。
    */
    queryChannel: QueryChannel;

    /**
    * 节目单查询条件。
    *
    * 该条件为空时，不返回频道关联的节目单。
    */
    queryPlaybill?: QueryPlaybill;

    /**
    * 在返回对象中是否需要返回频道信息（即ChannelPlaybill中要不要封装频道信息）。
    *
    * 1：需要，
    *
    * 0：不需要
    *
    * 默认为1。
    */
    needChannel?: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeQueryPlaybillListRequest(v: QueryPlaybillListRequest): QueryPlaybillListRequest {
    if (v) {
        v.queryChannel = safeQueryChannel(v.queryChannel);
        v.queryPlaybill = safeQueryPlaybill(v.queryPlaybill);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeQueryPlaybillListRequests(vs: QueryPlaybillListRequest[]): QueryPlaybillListRequest[] {
    if (vs) {
        return vs.map((v: QueryPlaybillListRequest) => {
            return safeQueryPlaybillListRequest(v);
        });
    } else {
        return [];
    }
}

export interface QueryPlaybillListResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 频道总个数。
    *
    * 说明
    *
    * 在查询频道的条件下，不考虑offset/count时符合条件的频道数，方便终端分页。
    */
    total: string;

    /**
    * 包含多个频道及频道节目单信息，ChannelPlaybill属性请参见“2.22 ChannelPlaybill”类型。
    *
    * 如果没有，不返回该参数。
    */
    channelPlaybills?: ChannelPlaybill[];

    /**
    * 表示节目版本号，取值为JSON格式，格式如下：
    *
    * {&quot;channelId&quot;: &quot;&lt;频道ID&gt;&quot;, &quot;date&quot;: &quot;&lt;日期&gt;&quot;, &quot;version&quot;: &quot;&lt;版本号&gt;&quot;},{&quot;channelId&quot;:&quot;&lt;频道ID&gt;&quot;,&quot;date&quot;:&quot;&lt;日期&gt;&quot;,&quot;version&quot;: &quot;&lt;版本号&gt;&quot;},…
    *
    * channelId是频道ID
    *
    * date是统计日期，格式为yyyyMMdd
    *
    * version是频道对应日期的节目版本号；
    *
    * 由于节目版本号统计到日期一级，所以date和version都是UTC时间。
    *
    * 注：返回的节目单为空时，本字段不返回。
    */
    playbillVersion?: PlaybillVersion[];

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeQueryPlaybillListResponse(v: QueryPlaybillListResponse): QueryPlaybillListResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.channelPlaybills = safeChannelPlaybills(v.channelPlaybills);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeQueryPlaybillListResponses(vs: QueryPlaybillListResponse[]): QueryPlaybillListResponse[] {
    if (vs) {
        return vs.map((v: QueryPlaybillListResponse) => {
            return safeQueryPlaybillListResponse(v);
        });
    } else {
        return [];
    }
}

export interface QueryPlaybillListByChannelContextRequest {

    /**
    * 频道上下文。
    */
    queryChannelContext: QueryChannelContext;

    /**
    * 节目单查询条件。
    *
    * 该条件为空时，不返回频道关联的节目单。
    */
    queryPlaybill?: QueryPlaybill;

    /**
    * 在返回对象中是否需要返回频道信息（即ChannelPlaybill中要不要封装频道信息）。
    *
    * 1：需要
    *
    * 0：不需要
    *
    * 默认值为1。
    */
    needChannel?: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeQueryPlaybillListByChannelContextRequest(v: QueryPlaybillListByChannelContextRequest): QueryPlaybillListByChannelContextRequest {
    if (v) {
        v.queryChannelContext = safeQueryChannelContext(v.queryChannelContext);
        v.queryPlaybill = safeQueryPlaybill(v.queryPlaybill);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeQueryPlaybillListByChannelContextRequests(vs: QueryPlaybillListByChannelContextRequest[]): QueryPlaybillListByChannelContextRequest[] {
    if (vs) {
        return vs.map((v: QueryPlaybillListByChannelContextRequest) => {
            return safeQueryPlaybillListByChannelContextRequest(v);
        });
    } else {
        return [];
    }
}

export interface QueryPlaybillListByChannelContextResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 包含多个频道及频道节目单信息，ChannelPlaybill属性请参见“2.22 ChannelPlaybill”类型。
    *
    * 如果没有，不返回该参数。
    */
    channelPlaybills?: ChannelPlaybill[];

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeQueryPlaybillListByChannelContextResponse(v: QueryPlaybillListByChannelContextResponse): QueryPlaybillListByChannelContextResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.channelPlaybills = safeChannelPlaybills(v.channelPlaybills);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeQueryPlaybillListByChannelContextResponses(vs: QueryPlaybillListByChannelContextResponse[]): QueryPlaybillListByChannelContextResponse[] {
    if (vs) {
        return vs.map((v: QueryPlaybillListByChannelContextResponse) => {
            return safeQueryPlaybillListByChannelContextResponse(v);
        });
    } else {
        return [];
    }
}

export interface QueryPlaybillContextRequest {

    /**
    * 频道查询条件。
    */
    queryChannel: QueryChannel;

    /**
    * 节目单上下文查询条件。
    */
    queryPlaybillContext: QueryPlaybillContext;

    /**
    * 在返回对象中是否需要返回频道信息（即ChannelPlaybill中要不要封装频道信息）。
    *
    * 1：需要
    *
    * 0：不需要
    *
    * 默认值为1。
    */
    needChannel?: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeQueryPlaybillContextRequest(v: QueryPlaybillContextRequest): QueryPlaybillContextRequest {
    if (v) {
        v.queryChannel = safeQueryChannel(v.queryChannel);
        v.queryPlaybillContext = safeQueryPlaybillContext(v.queryPlaybillContext);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeQueryPlaybillContextRequests(vs: QueryPlaybillContextRequest[]): QueryPlaybillContextRequest[] {
    if (vs) {
        return vs.map((v: QueryPlaybillContextRequest) => {
            return safeQueryPlaybillContextRequest(v);
        });
    } else {
        return [];
    }
}

export interface QueryPlaybillContextResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 频道总个数。字段含义：在查询频道的条件下，不考虑offset/count时符合条件的频道数，方便终端分页。
    */
    total: string;

    /**
    * 频道及频道对应的节目单上下文信息，ChannelPlaybillContext属性请参见“2.23 ChannelPlaybillContext”类型。
    *
    * 此处返回QueryPlaybillContext指定的查询时间点的频道节目单上下文。
    *
    * 如果没有查询到频道，不返回该参数。
    */
    channelPlaybillContexts?: ChannelPlaybillContext[];

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeQueryPlaybillContextResponse(v: QueryPlaybillContextResponse): QueryPlaybillContextResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.channelPlaybillContexts = safeChannelPlaybillContexts(v.channelPlaybillContexts);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeQueryPlaybillContextResponses(vs: QueryPlaybillContextResponse[]): QueryPlaybillContextResponse[] {
    if (vs) {
        return vs.map((v: QueryPlaybillContextResponse) => {
            return safeQueryPlaybillContextResponse(v);
        });
    } else {
        return [];
    }
}

export interface QueryPlaybillContextByChannelContextRequest {

    /**
    * 频道上下文。
    */
    queryChannelContext: QueryChannelContext;

    /**
    * 节目单上下文查询条件。
    */
    queryPlaybillContext: QueryPlaybillContext;

    /**
    * 在返回对象中是否需要返回频道信息（即ChannelPlaybill中要不要封装频道信息）。
    *
    * 1：需要
    *
    * 0：不需要
    *
    * 默认为1。
    */
    needChannel?: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeQueryPlaybillContextByChannelContextRequest(v: QueryPlaybillContextByChannelContextRequest): QueryPlaybillContextByChannelContextRequest {
    if (v) {
        v.queryChannelContext = safeQueryChannelContext(v.queryChannelContext);
        v.queryPlaybillContext = safeQueryPlaybillContext(v.queryPlaybillContext);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeQueryPlaybillContextByChannelContextRequests(vs: QueryPlaybillContextByChannelContextRequest[]): QueryPlaybillContextByChannelContextRequest[] {
    if (vs) {
        return vs.map((v: QueryPlaybillContextByChannelContextRequest) => {
            return safeQueryPlaybillContextByChannelContextRequest(v);
        });
    } else {
        return [];
    }
}

export interface QueryPlaybillContextByChannelContextResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 频道及频道对应的节目单上下文信息，ChannelPlaybillContext属性请参见“2.23 ChannelPlaybillContext”类型。
    *
    * 此处返回QueryPlaybillContext指定的查询时间点的频道节目单上下文。
    *
    * 如果没有查询到频道，不返回该参数。
    */
    channelPlaybillContexts?: ChannelPlaybillContext[];

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeQueryPlaybillContextByChannelContextResponse(v: QueryPlaybillContextByChannelContextResponse): QueryPlaybillContextByChannelContextResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.channelPlaybillContexts = safeChannelPlaybillContexts(v.channelPlaybillContexts);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeQueryPlaybillContextByChannelContextResponses(vs: QueryPlaybillContextByChannelContextResponse[]): QueryPlaybillContextByChannelContextResponse[] {
    if (vs) {
        return vs.map((v: QueryPlaybillContextByChannelContextResponse) => {
            return safeQueryPlaybillContextByChannelContextResponse(v);
        });
    } else {
        return [];
    }
}

export interface GetPlaybillDetailRequest {

    /**
    * 节目ID。
    */
    playbillID: string;

    /**
    * 内容编号的类型，取值包括：
    *
    * 0：内容内键，取值由VSP生成
    *
    * 1：内容Code，取值由第三方系统生成
    *
    * 默认值为0。
    */
    IDType?: string;

    /**
    * 节目单内容的过滤类型，取值包括：
    *
    * 0：根据用户属性和终端能力进行过滤
    *
    * 1：不做任何条件过滤
    *
    * 默认值为0.
    *
    * 说明
    *
    * 若接口要获取推荐内容，该参数也决定VSPCore是否对获取到的推荐内容做过滤。
    *
    * 大数据内容推荐平台在返回推荐内容时会根据用户的属性过滤。所以，建议在filterType为0时，才一并获取推荐内容。
    */
    filterType?: string;

    /**
    * 该参数非空时，根据该参数获取对应的推荐内容
    */
    queryDynamicRecmContent?: QueryDynamicRecmContent;

    /**
    * 频道归属的命名空间
    */
    channelNamespace?: string;

    /**
    * 是否返回所有物理频道。
    *
    * 0：否（默认）
    *
    * 1：是
    */
    isReturnAllMedia?: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeGetPlaybillDetailRequest(v: GetPlaybillDetailRequest): GetPlaybillDetailRequest {
    if (v) {
        v.queryDynamicRecmContent = safeQueryDynamicRecmContent(v.queryDynamicRecmContent);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeGetPlaybillDetailRequests(vs: GetPlaybillDetailRequest[]): GetPlaybillDetailRequest[] {
    if (vs) {
        return vs.map((v: GetPlaybillDetailRequest) => {
            return safeGetPlaybillDetailRequest(v);
        });
    } else {
        return [];
    }
}

export interface GetPlaybillDetailResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 节目单详情。
    *
    * PlaybillDetail参数请参见“2.68 PlaybillDetail”。
    */
    playbillDetail?: PlaybillDetail;

    /**
    * 推荐请求流水号。
    *
    * 字段由推荐系统产生返回给VSP透传，用于唯一标记某个内容推荐请求操作，用于后续与用户推荐结果反馈（如点击/观看推荐本次推荐结果的某个内容）关联。
    */
    recmActionID?: string;

    /**
    * 推荐结果列表。
    *
    * 说明
    *
    * 由于推荐接口支持1次批量查询多个推荐位场景列表，因此推荐结果数组数量、数组内数据顺序必须和推荐请求参数RecmScenario指定的推荐场景参数相同并且一一对应。如果某个推荐请求对应的推荐结果内容没有数据则推荐结果数组中对应对象返回空。
    */
    recmContents?: RecmContents[];

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeGetPlaybillDetailResponse(v: GetPlaybillDetailResponse): GetPlaybillDetailResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.playbillDetail = safePlaybillDetail(v.playbillDetail);
        v.recmContents = safeRecmContentss(v.recmContents);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeGetPlaybillDetailResponses(vs: GetPlaybillDetailResponse[]): GetPlaybillDetailResponse[] {
    if (vs) {
        return vs.map((v: GetPlaybillDetailResponse) => {
            return safeGetPlaybillDetailResponse(v);
        });
    } else {
        return [];
    }
}

export interface QueryHotPlaybillRequest {

    /**
    * 一次查询的总条数，不能设置为-1，终端一定要指定获取数据的总条数，最大条数默认不超过50，最大条数可配置，超过最大条数返回错误。
    */
    count: string;

    /**
    * 查询的起始位置。从0开始（即0表示第一个）。
    */
    offset: string;

    /**
    * 榜单类型。
    *
    * 取值范围：
    *
    * 0：总排行
    *
    * 1：月排行
    *
    * 2：周排行
    *
    * 3：日排行
    *
    * 如果不指定，默认值为0。
    */
    boardType?: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeQueryHotPlaybillRequest(v: QueryHotPlaybillRequest): QueryHotPlaybillRequest {
    if (v) {
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeQueryHotPlaybillRequests(vs: QueryHotPlaybillRequest[]): QueryHotPlaybillRequest[] {
    if (vs) {
        return vs.map((v: QueryHotPlaybillRequest) => {
            return safeQueryHotPlaybillRequest(v);
        });
    } else {
        return [];
    }
}

export interface QueryHotPlaybillResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 节目的总个数。
    */
    total: string;

    /**
    * 包含多个节目信息。
    *
    * 节目属性请参见“2.66 Playbill”类型。
    *
    * 如果查询结果为空，不返回该参数。
    */
    playbills?: Playbill[];

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeQueryHotPlaybillResponse(v: QueryHotPlaybillResponse): QueryHotPlaybillResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.playbills = safePlaybills(v.playbills);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeQueryHotPlaybillResponses(vs: QueryHotPlaybillResponse[]): QueryHotPlaybillResponse[] {
    if (vs) {
        return vs.map((v: QueryHotPlaybillResponse) => {
            return safeQueryHotPlaybillResponse(v);
        });
    } else {
        return [];
    }
}

export interface PlayChannelRequest {

    /**
    * 频道ID。
    */
    channelID: string;

    /**
    * 节目单ID。
    *
    * 如果播放直播业务，终端未上报节目单ID，平台查询当前时间的直播节目单，如果存在直播节目单，平台进行节目单的授权检查。
    */
    playbillID?: string;

    /**
    * 播放的媒资ID。
    */
    mediaID: string;

    /**
    * 鉴权的业务类型，取值包括：
    *
    * BTV：直播
    *
    * PLTV：网络时移
    *
    * CUTV：回看
    *
    * PREVIEW：直播预览
    *
    * IR：即时重播
    */
    businessType: string;

    /**
    * 检查频道或节目单是否被加锁和父母字控制，或者如果加锁或父母字控制，需要进行密码校验。
    *
    * 如果终端不传，表示平台不需要做加锁和父母字检查，以及解锁功能。
    *
    * 说明
    *
    * 如果是直播业务，当存在直播节目单，平台进行直播节目单的锁和父母字检查，否则进行频道的锁和父母字检查。
    *
    * 如果非直播业务，如果终端传入了节目单，平台进行节目单的锁和父母字检查，否则进行频道的锁和父母字检查。
    */
    checkLock?: CheckLock;

    /**
    * 如果内容已订购，是否返回播放URL，取值包括：
    *
    * 0：不返回
    *
    * 1：返回
    *
    * 默认值为1。
    *
    * 说明
    *
    * 终端在某些场景仅需要做鉴权，而不需要返回播放URL，比如直播节目单切换时。
    */
    isReturnURL?: string;

    /**
    * 如果需要返回播放URL，指定返回的URL格式，取值包括：
    *
    * 0：默认格式
    *
    * 1：转换成HLS协议
    *
    * 默认值是0。
    */
    URLFormat?: string;

    /**
    * Indicates whether to return the HTTPS URL.
    *
    * 0: no
    *
    * 1: yes
    *
    * Default: 0
    */
   isHTTPS?: number;

    /**
    * 如果内容未订购是否返回内容定价的产品，取值包括：
    *
    * 0：不返回
    *
    * 1：返回
    *
    * 默认值是1。
    */
    isReturnProduct?: string;

    /**
    * 如果返回产品，指定产品的排序方式：
    *
    * PRICE:ASC：按产品价格升序排序
    *
    * PRICE:DESC：按产品价格降序排序
    *
    * 默认值是PRICE:ASC。
    */
    sortType?: string;

    /**
    * 如果因为播放会话数超限导致播放失败，用户可以选择中止其他会话的播放行为。
    */
    playSessionKey?: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safePlayChannelRequest(v: PlayChannelRequest): PlayChannelRequest {
    if (v) {
        v.checkLock = safeCheckLock(v.checkLock);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safePlayChannelRequests(vs: PlayChannelRequest[]): PlayChannelRequest[] {
    if (vs) {
        return vs.map((v: PlayChannelRequest) => {
            return safePlayChannelRequest(v);
        });
    } else {
        return [];
    }
}

export interface PlayChannelResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 鉴权结果。
    */
    authorizeResult: AuthorizeResult;

    /**
    * 如果鉴权成功，返回内容播放地址。
    */
    playURL?: string;

    /**
    * 由于直播和网络时移业务，用户可以自由切换，所以为了提升用户体验，当终端在请求直播或网络时移的播放地址时，平台可能返回网络时移或直播的播放地址，原则如下：
    *
    * 如果businessType是BTV且频道支持网络时移且用户可以使用网络时移业务，返回网络时移的播放地址。
    *
    * 如果businessType是PLTV且用户可以使用直播业务，返回直播的播放地址。
    */
    attachedPlayURL?: string;

    /**
    * 如果鉴权成功且回看节目单创建过书签，返回书签断点时间。
    *
    * 单位是秒。
    */
    bookmark?: string;

    /**
    * 如果内容并发播放数超限，返回正在播放此内容的播放会话。
    */
    playSessions?: PlaySession[];

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safePlayChannelResponse(v: PlayChannelResponse): PlayChannelResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.authorizeResult = safeAuthorizeResult(v.authorizeResult);
        v.playSessions = safePlaySessions(v.playSessions);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safePlayChannelResponses(vs: PlayChannelResponse[]): PlayChannelResponse[] {
    if (vs) {
        return vs.map((v: PlayChannelResponse) => {
            return safePlayChannelResponse(v);
        });
    } else {
        return [];
    }
}

export interface DownloadCUTVRequest {

    /**
    * 节目单ID。
    */
    playbillID: string;

    /**
    * 待下载的频道媒资ID。
    */
    mediaID: string;

    /**
    * 检查节目单是否被加锁和父母字控制，或者如果加锁或父母字控制，需要进行密码校验。
    *
    * 如果终端不传，表示平台不需要做加锁和父母字检查，以及解锁功能。
    */
    checkLock?: CheckLock;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeDownloadCUTVRequest(v: DownloadCUTVRequest): DownloadCUTVRequest {
    if (v) {
        v.checkLock = safeCheckLock(v.checkLock);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeDownloadCUTVRequests(vs: DownloadCUTVRequest[]): DownloadCUTVRequest[] {
    if (vs) {
        return vs.map((v: DownloadCUTVRequest) => {
            return safeDownloadCUTVRequest(v);
        });
    } else {
        return [];
    }
}

export interface DownloadCUTVResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 鉴权结果。
    */
    authorizeResult: AuthorizeResult;

    /**
    * 如果鉴权成功，返回内容下载地址。
    */
    downloadURL?: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeDownloadCUTVResponse(v: DownloadCUTVResponse): DownloadCUTVResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.authorizeResult = safeAuthorizeResult(v.authorizeResult);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeDownloadCUTVResponses(vs: DownloadCUTVResponse[]): DownloadCUTVResponse[] {
    if (vs) {
        return vs.map((v: DownloadCUTVResponse) => {
            return safeDownloadCUTVResponse(v);
        });
    } else {
        return [];
    }
}

export interface ReportChannelRequest {

    /**
    * 播放行为，取值包括：
    *
    * 0：开始播放
    *
    * 1：退出播放
    *
    * 2：切换频道
    *
    * 3：直播和时移切换
    */
    action: string;

    /**
    * 播放会话ID，在入参action为1退出播放时候传入。
    */
    playSessionKey?: string;

    /**
    * 在多画面播放的时候，每个播放窗口需要不同的实例ID。
    */
    playerInstanceId?: string;

    /**
    * 播放/切换前的频道ID。
    */
    channelID: string;

    /**
    * 如果action&#x3D;2，表示切换后的频道ID。
    */
    nextChannelID?: string;

    /**
    * 如果播放回看节目单，表示节目单ID。
    */
    playbillID?: string;

    /**
    * 播放/切换前的媒资ID。
    */
    mediaID: string;

    /**
    * 如果action&#x3D;2，表示切换后的媒资ID。
    */
    nextMediaID?: string;

    /**
    * 鉴权的业务类型，取值包括：
    *
    * BTV：直播
    *
    * PLTV：网络时移
    *
    * CUTV：回看
    */
    businessType: string;

    /**
    * 如果播放回看，是否播放本地已下载的内容，取值包括：
    *
    * 0：否
    *
    * 1：是
    *
    * 默认值为0。
    */
    isDownload?: string;

    /**
    * 鉴权通过的产品ID。
    */
    productID: string;

    /**
    * 如果从指定的栏目进入频道，携带栏目ID，用于Reporter按栏目统计用户行为。
    */
    subjectID: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeReportChannelRequest(v: ReportChannelRequest): ReportChannelRequest {
    if (v) {
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeReportChannelRequests(vs: ReportChannelRequest[]): ReportChannelRequest[] {
    if (vs) {
        return vs.map((v: ReportChannelRequest) => {
            return safeReportChannelRequest(v);
        });
    } else {
        return [];
    }
}

export interface ReportChannelResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 播放会话ID，在入参action为0,2,3的时候返回。
    */
    playSessionKey?: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeReportChannelResponse(v: ReportChannelResponse): ReportChannelResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeReportChannelResponses(vs: ReportChannelResponse[]): ReportChannelResponse[] {
    if (vs) {
        return vs.map((v: ReportChannelResponse) => {
            return safeReportChannelResponse(v);
        });
    } else {
        return [];
    }
}

export interface PlayChannelHeartbeatRequest {

    /**
    * 频道ID。
    */
    channelID: string;

    /**
    * 如果播放回看节目单，表示节目单ID。
    */
    playbillID?: string;

    /**
    * 播放的媒资ID。
    */
    mediaID: string;

    /**
    * 播放会话主键
    */
    playSessionKey?: string;

    /**
    * 播放实例ID，用于一个终端同时支持多个播放会话的场景。
    */
    playerInstanceId?: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safePlayChannelHeartbeatRequest(v: PlayChannelHeartbeatRequest): PlayChannelHeartbeatRequest {
    if (v) {
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safePlayChannelHeartbeatRequests(vs: PlayChannelHeartbeatRequest[]): PlayChannelHeartbeatRequest[] {
    if (vs) {
        return vs.map((v: PlayChannelHeartbeatRequest) => {
            return safePlayChannelHeartbeatRequest(v);
        });
    } else {
        return [];
    }
}

export interface PlayChannelHeartbeatResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 播放是否合法，取值包括：
    *
    * 0：非法
    *
    * 1：合法
    */
    isValid: string;

    /**
    * 如果播放会话是被其他设备中断，返回发起中断请求的ProfileID。
    */
    interrupter?: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safePlayChannelHeartbeatResponse(v: PlayChannelHeartbeatResponse): PlayChannelHeartbeatResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safePlayChannelHeartbeatResponses(vs: PlayChannelHeartbeatResponse[]): PlayChannelHeartbeatResponse[] {
    if (vs) {
        return vs.map((v: PlayChannelHeartbeatResponse) => {
            return safePlayChannelHeartbeatResponse(v);
        });
    } else {
        return [];
    }
}

export interface QueryPlaybillVersionRequest {

    /**
    * 节目单开始的天（0点）。取值为距离1970年1月1号的毫秒数。
    */
    fromDate: string;

    /**
    * 节目单结束的天（0点）。取值为距离1970年1月1号的毫秒数。
    */
    toDate: string;

    /**
    * 取值为距离1970年1月1号的毫秒数。
    *
    * 如果传了该参数，表示查询该时间点到当前时间范围内修改的fromDate到toDate范围的节目单版本号。
    *
    * 如果没有传该参数，表示查询fromDate到toDate的所有版本号。
    */
    timeRange?: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeQueryPlaybillVersionRequest(v: QueryPlaybillVersionRequest): QueryPlaybillVersionRequest {
    if (v) {
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeQueryPlaybillVersionRequests(vs: QueryPlaybillVersionRequest[]): QueryPlaybillVersionRequest[] {
    if (vs) {
        return vs.map((v: QueryPlaybillVersionRequest) => {
            return safeQueryPlaybillVersionRequest(v);
        });
    } else {
        return [];
    }
}

export interface QueryPlaybillVersionResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 返回的节目单版本号。
    */
    playbillVersion?: PlaybillVersion[];

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeQueryPlaybillVersionResponse(v: QueryPlaybillVersionResponse): QueryPlaybillVersionResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.playbillVersion = safePlaybillVersions(v.playbillVersion);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeQueryPlaybillVersionResponses(vs: QueryPlaybillVersionResponse[]): QueryPlaybillVersionResponse[] {
    if (vs) {
        return vs.map((v: QueryPlaybillVersionResponse) => {
            return safeQueryPlaybillVersionResponse(v);
        });
    } else {
        return [];
    }
}

export interface QueryMosaicChannelListRequest {

    /**
    * Mosaic频道编号,如果指定则表示只查找此编号对应的马赛克频道，如果不指定则根据用户登录的命名空间查询命名空间下的所有的马赛克频道
    */
    mosaicChannelID?: string;

    /**
    * 一次查询的总条数，不能设置为-1，调用者一定要指定获取数据的总条数，最大条数默认不超过50，超过最大条数返回错误。
    */
    count: string;

    /**
    * 查询的起始位置。从0开始（即0表示第一个）。
    */
    offset: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeQueryMosaicChannelListRequest(v: QueryMosaicChannelListRequest): QueryMosaicChannelListRequest {
    if (v) {
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeQueryMosaicChannelListRequests(vs: QueryMosaicChannelListRequest[]): QueryMosaicChannelListRequest[] {
    if (vs) {
        return vs.map((v: QueryMosaicChannelListRequest) => {
            return safeQueryMosaicChannelListRequest(v);
        });
    } else {
        return [];
    }
}

export interface QueryMosaicChannelListResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 马赛克频道总个数。
    */
    total: string;

    /**
    * 马赛克频道列表，马赛克频道属性请参见“MosaicChannel”类型。
    *
    * 如果查询结果为空，不返回该参数。
    */
    mosaicChannels?: MosaicChannel[];

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeQueryMosaicChannelListResponse(v: QueryMosaicChannelListResponse): QueryMosaicChannelListResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.mosaicChannels = safeMosaicChannels(v.mosaicChannels);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeQueryMosaicChannelListResponses(vs: QueryMosaicChannelListResponse[]): QueryMosaicChannelListResponse[] {
    if (vs) {
        return vs.map((v: QueryMosaicChannelListResponse) => {
            return safeQueryMosaicChannelListResponse(v);
        });
    } else {
        return [];
    }
}

export interface BatchPlayChannelRequest {
    /**
     * 频道鉴权内容列表
     */
    aggregationAuthorizeObjects: AggregationAuthorizeObject[];
    /**
    * 检查频道或节目单是否被加锁和父母字控制，或者如果加锁或父母字控制，需要进行密码校验。
    *
    * 如果终端不传，表示VSP不需要做加锁和父母字检查，以及解锁功能。
    *
    * 说明
    *
    * 如果是直播业务，当存在直播节目单，VSP进行直播节目单的锁和父母字检查，否则进行频道的锁和父母字检查。
    *
    * 如果非直播业务，如果终端传入了节目单，VSP进行节目单的锁和父母字检查，否则进行频道的锁和父母字检查。
    */
    checkLock?: CheckLock;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeBatchPlayChannelRequest(v: BatchPlayChannelRequest): BatchPlayChannelRequest {
    if (v) {
        v.aggregationAuthorizeObjects = safeAggregationAuthorizeObjects(v.aggregationAuthorizeObjects);
        v.checkLock = safeCheckLock(v.checkLock);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeBatchPlayChannelRequests(vs: BatchPlayChannelRequest[]): BatchPlayChannelRequest[] {
    if (vs) {
        return vs.map((v: BatchPlayChannelRequest) => {
            return safeBatchPlayChannelRequest(v);
        });
    } else {
        return [];
    }
}

export interface BatchPlayChannelResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 鉴权结果。
    */
    aggregationAuthorizeResults: AggregationAuthorizeResult[];

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeBatchPlayChannelResponse(v: BatchPlayChannelResponse): BatchPlayChannelResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.aggregationAuthorizeResults = safeAggregationAuthorizeResults(v.aggregationAuthorizeResults);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeBatchPlayChannelResponses(vs: BatchPlayChannelResponse[]): BatchPlayChannelResponse[] {
    if (vs) {
        return vs.map((v: BatchPlayChannelResponse) => {
            return safeBatchPlayChannelResponse(v);
        });
    } else {
        return [];
    }
}

export interface QueryVODSubjectListRequest {

    /**
    * 父栏目ID，查询此栏目下的子栏目列表。如果为空，标示查询 “-1”根栏目下的栏目列表。
    */
    subjectID?: string;

    /**
    * 栏目类型，具体取值包括：
    *
    * VOD：点播
    *
    * AUDIO_VOD：音频点播
    *
    * VIDEO_VOD：视频点播
    *
    * 默认值是VOD。
    *
    * 说明
    *
    * VOD类型的栏目下可以包含音频点播和视频点播，而AUDIO_VOD类型的栏目下只能包含音频点播、VIDEO_VOD类型的栏目下只能包含视频点播。
    */
    contentTypes?: string[];

    /**
    * 一次查询的总条数，不能设置为-1，调用者一定要指定获取数据的总条数，最大条数默认不超过50，最大条数可配置，超过最大条数返回错误。
    */
    count: string;

    /**
    * 查询的起始位置。从0开始（即0表示第一个）。
    */
    offset: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeQueryVODSubjectListRequest(v: QueryVODSubjectListRequest): QueryVODSubjectListRequest {
    if (v) {
        v.contentTypes = v.contentTypes || [];
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeQueryVODSubjectListRequests(vs: QueryVODSubjectListRequest[]): QueryVODSubjectListRequest[] {
    if (vs) {
        return vs.map((v: QueryVODSubjectListRequest) => {
            return safeQueryVODSubjectListRequest(v);
        });
    } else {
        return [];
    }
}

export interface QueryVODSubjectListResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 子栏目总个数。
    */
    total: string;

    /**
    * VOD栏目列表，VOD栏目属性请参见 “2.111 Subject”类型。
    *
    * 如果查询结果为空，不返回该参数。
    */
    subjects?: Subject[];

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeQueryVODSubjectListResponse(v: QueryVODSubjectListResponse): QueryVODSubjectListResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.subjects = safeSubjects(v.subjects);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeQueryVODSubjectListResponses(vs: QueryVODSubjectListResponse[]): QueryVODSubjectListResponse[] {
    if (vs) {
        return vs.map((v: QueryVODSubjectListResponse) => {
            return safeQueryVODSubjectListResponse(v);
        });
    } else {
        return [];
    }
}

export interface QuerySubjectDetailRequest {

    /**
    * 栏目ID列表。
    */
    subjectIDs: string[];

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeQuerySubjectDetailRequest(v: QuerySubjectDetailRequest): QuerySubjectDetailRequest {
    if (v) {
        v.subjectIDs = v.subjectIDs || [];
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeQuerySubjectDetailRequests(vs: QuerySubjectDetailRequest[]): QuerySubjectDetailRequest[] {
    if (vs) {
        return vs.map((v: QuerySubjectDetailRequest) => {
            return safeQuerySubjectDetailRequest(v);
        });
    } else {
        return [];
    }
}

export interface QuerySubjectDetailResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 栏目列表，VOD栏目属性请参见 “2.111 Subject”。
    *
    * 如果查询结果为空，不返回该参数。
    */
    subjects?: Subject[];

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeQuerySubjectDetailResponse(v: QuerySubjectDetailResponse): QuerySubjectDetailResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.subjects = safeSubjects(v.subjects);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeQuerySubjectDetailResponses(vs: QuerySubjectDetailResponse[]): QuerySubjectDetailResponse[] {
    if (vs) {
        return vs.map((v: QuerySubjectDetailResponse) => {
            return safeQuerySubjectDetailResponse(v);
        });
    } else {
        return [];
    }
}

export interface QueryVODListBySubjectRequest {

    /**
    * 栏目ID（可以是父栏目或者叶子栏目），查询此栏目下的VOD列表。如不传该参数，表示查询根栏目下所有叶子栏目(包括父栏目下的叶子栏目)下绑定的普通VOD和父集VOD。
    */
    subjectID?: string;

    /**
    * 一次查询的总条数，不能设置为-1，调用者一定要指定获取数据的总条数，最大条数默认不超过50，最大条数可配置，超过最大条数返回错误。
    */
    count: string;

    /**
    * 查询的起始位置。从0开始（即0表示第一个）。
    */
    offset: string;

    /**
    * VOD排序方式。以下列方式指定排序方式：
    *
    * STARTTIME:ASC：按VOD上线时间升序排序
    *
    * STARTTIME:DESC：按VOD上线时间降序排序
    *
    * AVGSCORE:ASC：按VOD评分均值升序排序
    *
    * AVGSCORE:DESC：按VOD评分均值降序排序
    *
    * PLAYTIMES:ASC：按VOD点击率升序排序
    *
    * PLAYTIMES:DESC：按VOD点击率降序排序
    *
    * CNTARRANGE：按栏目下内容编排顺序排序，仅对subjectid表示的栏目是叶子栏目时才有效
    *
    * VODNAME:ASC：按VOD名称字典升序排序
    *
    * VODNAME:DESC：按VOD名称字典降序排序
    *
    * 说明
    *
    * 当subjectID为叶子栏目且sortType未携带，sortType默认取CNTARRANGE:ASC。
    *
    * 如果subjectID非叶子栏目且sortType未携带，sortType默认取STARTTIME:DESC。
    *
    * 平台不支持对父栏目按照内容自定义的顺序排序，自定义顺序只针对叶子栏目。
    */
    sortType?: string;

    /**
    * 搜索条件。
    *
    * VODFilter参考“2.127 VODFilter”
    *
    * 说明
    *
    * 如果VODFilters和VODExcluders参数中包含相同的过滤条件，则以VODFilters中包含的为准，忽略VODExcluders中的同名过滤条件。
    */
    VODFilter?: VODFilter;

    /**
    * 排他条件。
    *
    * VODExcluder参考“2.126 VODExcluder”
    */
    VODExcluder?: VODExcluder;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeQueryVODListBySubjectRequest(v: QueryVODListBySubjectRequest): QueryVODListBySubjectRequest {
    if (v) {
        v.VODFilter = safeVODFilter(v.VODFilter);
        v.VODExcluder = safeVODExcluder(v.VODExcluder);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeQueryVODListBySubjectRequests(vs: QueryVODListBySubjectRequest[]): QueryVODListBySubjectRequest[] {
    if (vs) {
        return vs.map((v: QueryVODListBySubjectRequest) => {
            return safeQueryVODListBySubjectRequest(v);
        });
    } else {
        return [];
    }
}

export interface QueryVODListBySubjectResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * VOD总个数。
    */
    total: string;

    /**
    * VOD列表信息，VOD属性请参见“2.124 VOD”类型。
    *
    * 如果查询结果为空，不返回该参数。
    */
    VODs?: VOD[];

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeQueryVODListBySubjectResponse(v: QueryVODListBySubjectResponse): QueryVODListBySubjectResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.VODs = safeVODs(v.VODs);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeQueryVODListBySubjectResponses(vs: QueryVODListBySubjectResponse[]): QueryVODListBySubjectResponse[] {
    if (vs) {
        return vs.map((v: QueryVODListBySubjectResponse) => {
            return safeQueryVODListBySubjectResponse(v);
        });
    } else {
        return [];
    }
}

export interface QuerySubjectVODBySubjectIDRequest {

    /**
    * 栏目ID(只支持父栏目，不能是叶子栏目)，查询此栏目下的子栏目及子栏目下VOD列表。如不传该参数，表示查询根栏目下所有子栏目及子栏目下的VOD列表。
    */
    subjectID?: string;

    /**
    * 子栏目的排序方式。以下列方式指定排序方式：
    *
    * ID:ASC：按子栏目ID升序排序
    *
    * ID:DESC：按子栏目ID降序排序
    *
    * NAME:ASC：按子栏目名称升序排序
    *
    * NAME:DESC：按子栏目名称降序排序
    *
    * 如果不指定，平台先按照栏目自定义序号降序排序，再按照栏目ID降序排序。
    */
    subjectSortType?: string;

    /**
    * 一次查询的子栏目的总条数，不能设置为-1，调用者一定要指定获取数据的总条数，最大条数默认不超过50，最大条数可配置，超过最大条数返回错误。
    */
    subjectCount: string;

    /**
    * 子栏目下VOD的排序方式。以下列方式指定排序方式：
    *
    * STARTTIME:ASC：按VOD上线时间升序排序
    *
    * STARTTIME:DESC：按VOD上线时间降序排序
    *
    * AVGSCORE:ASC：按VOD评分均值升序排序
    *
    * AVGSCORE:DESC：按VOD评分均值降序排序
    *
    * PLAYTIMES:ASC：按VOD点击率升序排序
    *
    * PLAYTIMES:DESC：按VOD点击率降序排序
    *
    * CNTARRANGE：按栏目下内容编排顺序排序，仅对subjectid表示的栏目是叶子栏目时才有效
    *
    * VODNAME:ASC：按VOD名称字典升序排序
    *
    * VODNAME:DESC：按VOD名称字典降序排序
    *
    * 说明
    *
    * 当subjectID为叶子栏目且sortType未携带，sortType默认取CNTARRANGE:ASC。
    *
    * 如果subjectID非叶子栏目且sortType未携带，sortType默认取STARTTIME:DESC。
    */
    VODSortType?: string;

    /**
    * 一次查询的子栏目中VOD的总条数，不能设置为-1，终端一定要指定获取数据的总条数，最大条数默认不超过50，最大条数可配置，超过最大条数返回错误。
    */
    VODCount: string;

    /**
    * 查询的子栏目的起始位置。从0开始（即0表示第一个）。
    */
    offset: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeQuerySubjectVODBySubjectIDRequest(v: QuerySubjectVODBySubjectIDRequest): QuerySubjectVODBySubjectIDRequest {
    if (v) {
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeQuerySubjectVODBySubjectIDRequests(vs: QuerySubjectVODBySubjectIDRequest[]): QuerySubjectVODBySubjectIDRequest[] {
    if (vs) {
        return vs.map((v: QuerySubjectVODBySubjectIDRequest) => {
            return safeQuerySubjectVODBySubjectIDRequest(v);
        });
    } else {
        return [];
    }
}

export interface QuerySubjectVODBySubjectIDResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 子栏目总个数。
    */
    total: string;

    /**
    * 子栏目及栏目下VOD列表信息，如果查询结果为空，不返回该参数。
    */
    subjectVODLists?: SubjectVODList[];

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeQuerySubjectVODBySubjectIDResponse(v: QuerySubjectVODBySubjectIDResponse): QuerySubjectVODBySubjectIDResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.subjectVODLists = safeSubjectVODLists(v.subjectVODLists);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeQuerySubjectVODBySubjectIDResponses(vs: QuerySubjectVODBySubjectIDResponse[]): QuerySubjectVODBySubjectIDResponse[] {
    if (vs) {
        return vs.map((v: QuerySubjectVODBySubjectIDResponse) => {
            return safeQuerySubjectVODBySubjectIDResponse(v);
        });
    } else {
        return [];
    }
}

export interface QueryRecmVODListRequest {

    /**
    * 一次查询的VOD的总条数，不能设置为-1，调用者一定要指定获取数据的总条数，最大条数默认不超过50，最大条数可配置，超过最大条数返回错误。
    */
    count: string;

    /**
    * 查询的子栏目的起始位置。从0开始（即0表示第一个）。
    */
    offset: string;

    /**
    * 推荐类型，取值范围：
    *
    * 1：今日更新影片
    *
    * 2：最新上线
    *
    * 3：即将下线
    *
    * 4：热点排行
    *
    * 5：周排行
    *
    * 6：强档推荐
    */
    action: string;

    /**
    * VOD排序方式。只在“action”为“4”和“5”时有效。
    *
    * PLAYTIMES:DESC：按VOD点击率降序排序
    *
    * AVERAGESCORE:DESC：按评价均值降序排序
    *
    * 如果不传该参数，默认按照VOD上线时间降序排列。
    */
    sortType?: string;

    /**
    * 栏目ID，如果携带subjectID则查询属于该栏目的所有推荐影片。该栏目可以是父栏目。如不携带subjectID，则查询所有的推荐影片。
    */
    subjectID?: string;

    /**
    * 推荐VOD的展示位置。
    *
    * “position”不为空时，则首先查询对应位置配置的推荐内容列表，然后根据“action”查询对应的内容列表，并组合起来返回给客户端。
    *
    * 如果“position”为空时，根据“action”查询对应的内容列表，并返回给客户端。
    *
    * 说明
    *
    * 该参数只在“action”为2,3,4,6时有效，和展示内容的维护功能对应。
    */
    position?: string;

    /**
    * VOD类型。取值范围：
    *
    * 0：非电视剧
    *
    * 1：普通连续剧
    *
    * 2：季播剧父集
    *
    * 3：季播剧
    *
    * 说明
    *
    * 该参数为空表示查所有。
    */
    VODTypes?: string[];

    /**
    * 推荐过滤条件。
    *
    * RecmVODFilter参考“2.95 RecmVODFilter”。
    */
    recmVODFilter?: RecmVODFilter;
}

function safeQueryRecmVODListRequest(v: QueryRecmVODListRequest): QueryRecmVODListRequest {
    if (v) {
        v.VODTypes = v.VODTypes || [];
        v.recmVODFilter = safeRecmVODFilter(v.recmVODFilter);
    }
    return v;
}

function safeQueryRecmVODListRequests(vs: QueryRecmVODListRequest[]): QueryRecmVODListRequest[] {
    if (vs) {
        return vs.map((v: QueryRecmVODListRequest) => {
            return safeQueryRecmVODListRequest(v);
        });
    } else {
        return [];
    }
}

export interface QueryRecmVODListResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 推荐VOD的总个数。
    */
    total: string;

    /**
    * VOD列表信息，如果查询结果为空，不返回该参数。
    */
    VODs?: VOD[];

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeQueryRecmVODListResponse(v: QueryRecmVODListResponse): QueryRecmVODListResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.VODs = safeVODs(v.VODs);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeQueryRecmVODListResponses(vs: QueryRecmVODListResponse[]): QueryRecmVODListResponse[] {
    if (vs) {
        return vs.map((v: QueryRecmVODListResponse) => {
            return safeQueryRecmVODListResponse(v);
        });
    } else {
        return [];
    }
}

export interface GetContentConfigRequest {

    /**
    * 配置参数类型，指定需要哪些配置，为空表示返回所有的。
    *
    * 1：ProduceZone
    *
    * 2：Genre
    */
    types?: string[];

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeGetContentConfigRequest(v: GetContentConfigRequest): GetContentConfigRequest {
    if (v) {
        v.types = v.types || [];
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeGetContentConfigRequests(vs: GetContentConfigRequest[]): GetContentConfigRequest[] {
    if (vs) {
        return vs.map((v: GetContentConfigRequest) => {
            return safeGetContentConfigRequest(v);
        });
    } else {
        return [];
    }
}

export interface GetContentConfigResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 内容的出品地信息。
    */
    produceZones?: ProduceZone[];

    /**
    * 内容的流派信息。
    */
    genres?: Genre[];

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeGetContentConfigResponse(v: GetContentConfigResponse): GetContentConfigResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.produceZones = safeProduceZones(v.produceZones);
        v.genres = safeGenres(v.genres);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeGetContentConfigResponses(vs: GetContentConfigResponse[]): GetContentConfigResponse[] {
    if (vs) {
        return vs.map((v: GetContentConfigResponse) => {
            return safeGetContentConfigResponse(v);
        });
    } else {
        return [];
    }
}

export interface GetVODDetailRequest {

    /**
    * VOD 编号。
    */
    VODID: string;

    /**
    * 内容编号的类型，取值包括：
    *
    * 0：内容内键，取值由VSP生成。
    *
    * 1：内容Code，取值由第三方系统生成。
    *
    * 默认值为0。
    */
    IDType?: string;

    /**
    * 内容的过滤类型，取值包括：
    *
    * 0：根据用户属性和终端能力进行过滤
    *
    * 1：不做任何条件过滤
    *
    * 默认值为0。
    *
    * 说明
    *
    * 若接口要获取推荐内容，该参数也决定VSP是否对获取到的推荐内容做过滤。如果filterType&#x3D;0，VSP会对推荐系统返回的推荐内容，根据用户的属性过滤。否则不过滤，如果对内容不做过滤，有可能播放失败。所以，建议终端在filterType为0时，才获取推荐内容。
    */
    filterType?: string;

    /**
    * 该参数非空时，根据该参数获取对应的推荐内容。
    */
    queryDynamicRecmContent?: QueryDynamicRecmContent;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeGetVODDetailRequest(v: GetVODDetailRequest): GetVODDetailRequest {
    if (v) {
        v.queryDynamicRecmContent = safeQueryDynamicRecmContent(v.queryDynamicRecmContent);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeGetVODDetailRequests(vs: GetVODDetailRequest[]): GetVODDetailRequest[] {
    if (vs) {
        return vs.map((v: GetVODDetailRequest) => {
            return safeGetVODDetailRequest(v);
        });
    } else {
        return [];
    }
}

export interface GetVODDetailResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * VODDetail详情信息，参见 2.125 VODDetail类型。
    *
    * 如果查询结果为空，不返回该参数。
    */
    VODDetail?: VODDetail;

    /**
    * 推荐请求流水号。
    *
    * 字段由推荐系统产生返回给VSP透传，用于唯一标记某个内容推荐请求操作，用于后续与用户推荐结果反馈（如点击/观看推荐本次推荐结果的某个内容）关联。
    */
    recmActionID?: string;

    /**
    * 推荐结果列表。
    *
    * 说明
    *
    * 由于推荐系统支持1次批量查询多个推荐位场景列表，因此推荐结果数组数量、数组内数据顺序必须和推荐请求参数RecmScenario指定的推荐场景参数相同并且一一对应。如果某个推荐请求对应的推荐结果内容没有数据则推荐结果数组中对应对象返回空。
    */
    recmContents?: RecmContents[];

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeGetVODDetailResponse(v: GetVODDetailResponse): GetVODDetailResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.VODDetail = safeVODDetail(v.VODDetail);
        v.recmContents = safeRecmContentss(v.recmContents);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeGetVODDetailResponses(vs: GetVODDetailResponse[]): GetVODDetailResponse[] {
    if (vs) {
        return vs.map((v: GetVODDetailResponse) => {
            return safeGetVODDetailResponse(v);
        });
    } else {
        return [];
    }
}

export interface PlayVODRequest {

    /**
    * 播放的VOD的ID，如果播放子集的话，传子集的VOD ID。
    *
    * 如果传入的VODID是连续剧父集ID，mediaID只能是父集片花的媒资ID。
    */
    VODID: string;

    /**
    * 播放的VOD媒资ID，可以是正片媒资ID或者是片花媒资ID。
    */
    mediaID: string;

    /**
    * 如果播放的是子集，传入子集对应的连续剧父集ID，用于连续剧书签的返回。
    */
    seriesID?: string;

    /**
    * 检查VOD是否被加锁和父母字控制，或者如果加锁或父母字控制，需要进行密码校验。如果终端不传，表示平台不需要做加锁和父母字检查，以及解锁功能。
    */
    checkLock?: CheckLock;

    /**
    * 指定返回的URL格式，取值包括：
    *
    * 0：默认格式
    *
    * 1：转换成HLS协议
    *
    * 默认值是0。
    */
    URLFormat?: string;

    /**
    * Indicates whether to return the HTTPS URL.
    *
    * 0: no
    *
    * 1: yes
    *
    * Default: 0
    */
   isHTTPS?: number;

    /**
    * 如果内容未订购是否返回内容定价的产品，取值包括：
    *
    * 0：不返回
    *
    * 1：返回
    *
    * 默认值为1。
    */
    isReturnProduct?: string;

    /**
    * 如果返回产品，指定产品的排序方式：
    *
    * PRICE:ASC：按产品价格升序排序
    *
    * PRICE:DESC：按产品价格降序排序
    *
    * 默认值是PRICE:ASC。
    */
    sortType?: string;

    /**
    * 如果因为播放会话数超限导致播放失败，用户可以选择中止其他会话的播放行为。
    */
    playSessionKey?: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safePlayVODRequest(v: PlayVODRequest): PlayVODRequest {
    if (v) {
        v.checkLock = safeCheckLock(v.checkLock);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safePlayVODRequests(vs: PlayVODRequest[]): PlayVODRequest[] {
    if (vs) {
        return vs.map((v: PlayVODRequest) => {
            return safePlayVODRequest(v);
        });
    } else {
        return [];
    }
}

export interface PlayVODResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 鉴权结果。
    */
    authorizeResult: AuthorizeResult;

    /**
    * 如果鉴权成功，返回内容播放地址。
    */
    playURL?: string;

    /**
    * 如果鉴权成功且VOD创建过书签，返回书签断点时间。
    *
    * 单位是秒。
    */
    bookmark?: string;

    /**
    * 如果内容并发播放数超限，返回正在播放此内容的播放会话。
    */
    playSessions?: PlaySession[];

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safePlayVODResponse(v: PlayVODResponse): PlayVODResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.authorizeResult = safeAuthorizeResult(v.authorizeResult);
        v.playSessions = safePlaySessions(v.playSessions);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safePlayVODResponses(vs: PlayVODResponse[]): PlayVODResponse[] {
    if (vs) {
        return vs.map((v: PlayVODResponse) => {
            return safePlayVODResponse(v);
        });
    } else {
        return [];
    }
}

export interface DownloadVODRequest {

    /**
    * VOD的ID，可是普通VOD、连续剧父集或VOD子集。
    */
    VODID: string;

    /**
    * 如果VODID是连续剧父集，表示待下载的子集ID，且必须携带。
    */
    episodeIDs?: string[];

    /**
    * 待下载的VOD媒资ID。
    *
    * 说明
    *
    * 如果VODID是普通VOD或者VOD子集，只支持传入一个媒资ID。否则，必须传入待下载的VOD子集的媒资ID，episodeIDs和mediaIDs是一对一关系。
    */
    mediaIDs: string[];

    /**
    * 检查VOD是否被加锁和父母字控制，或者如果加锁或父母字控制，需要进行密码校验。
    *
    * 如果终端不传，表示平台不需要做加锁和父母字检查，以及解锁功能。
    */
    checkLock?: CheckLock;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeDownloadVODRequest(v: DownloadVODRequest): DownloadVODRequest {
    if (v) {
        v.episodeIDs = v.episodeIDs || [];
        v.mediaIDs = v.mediaIDs || [];
        v.checkLock = safeCheckLock(v.checkLock);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeDownloadVODRequests(vs: DownloadVODRequest[]): DownloadVODRequest[] {
    if (vs) {
        return vs.map((v: DownloadVODRequest) => {
            return safeDownloadVODRequest(v);
        });
    } else {
        return [];
    }
}

export interface DownloadVODResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 鉴权结果。
    */
    authorizeResult: AuthorizeResult;

    /**
    * 如果鉴权成功，返回VOD下载地址。
    *
    * downloadURLs和mediaIDs是一一对应的关系。
    */
    downloadURLs?: string[];

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeDownloadVODResponse(v: DownloadVODResponse): DownloadVODResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.authorizeResult = safeAuthorizeResult(v.authorizeResult);
        v.downloadURLs = v.downloadURLs || [];
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeDownloadVODResponses(vs: DownloadVODResponse[]): DownloadVODResponse[] {
    if (vs) {
        return vs.map((v: DownloadVODResponse) => {
            return safeDownloadVODResponse(v);
        });
    } else {
        return [];
    }
}

export interface ReportVODRequest {

    /**
    * 播放行为，取值包括：
    *
    * 0：开始播放
    *
    * 1：退出播放
    */
    action: string;

    /**
    * VOD的ID。
    */
    VODID: string;

    /**
    * 播放的媒资ID。
    */
    mediaID: string;

    /**
    * 是否播放本地已下载的VOD，取值包括：
    *
    * 0：否
    *
    * 1：是
    *
    * 默认值是0。
    */
    isDownload?: string;

    /**
    * 如果从指定的栏目进入VOD，携带栏目ID，用于Reporter按栏目统计用户行为。
    */
    subjectID: string;

    /**
    * 鉴权通过的产品ID。
    */
    productID: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeReportVODRequest(v: ReportVODRequest): ReportVODRequest {
    if (v) {
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeReportVODRequests(vs: ReportVODRequest[]): ReportVODRequest[] {
    if (vs) {
        return vs.map((v: ReportVODRequest) => {
            return safeReportVODRequest(v);
        });
    } else {
        return [];
    }
}

export interface ReportVODResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeReportVODResponse(v: ReportVODResponse): ReportVODResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeReportVODResponses(vs: ReportVODResponse[]): ReportVODResponse[] {
    if (vs) {
        return vs.map((v: ReportVODResponse) => {
            return safeReportVODResponse(v);
        });
    } else {
        return [];
    }
}

export interface PlayVODHeartbeatRequest {

    /**
    * VOD的ID。
    */
    VODID: string;

    /**
    * 播放的媒资ID。
    */
    mediaID: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safePlayVODHeartbeatRequest(v: PlayVODHeartbeatRequest): PlayVODHeartbeatRequest {
    if (v) {
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safePlayVODHeartbeatRequests(vs: PlayVODHeartbeatRequest[]): PlayVODHeartbeatRequest[] {
    if (vs) {
        return vs.map((v: PlayVODHeartbeatRequest) => {
            return safePlayVODHeartbeatRequest(v);
        });
    } else {
        return [];
    }
}

export interface PlayVODHeartbeatResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 播放是否合法，取值包括：
    *
    * 0：非法
    *
    * 1：合法
    */
    isValid: string;

    /**
    * 如果播放会话是被其他设备中断，返回发起中断请求的ProfileID。
    */
    interrupter?: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safePlayVODHeartbeatResponse(v: PlayVODHeartbeatResponse): PlayVODHeartbeatResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safePlayVODHeartbeatResponses(vs: PlayVODHeartbeatResponse[]): PlayVODHeartbeatResponse[] {
    if (vs) {
        return vs.map((v: PlayVODHeartbeatResponse) => {
            return safePlayVODHeartbeatResponse(v);
        });
    } else {
        return [];
    }
}

export interface SetGlobalFilterCondRequest {

    /**
    * 全局过滤条件。
    */
    globalFilter: GlobalFilter;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeSetGlobalFilterCondRequest(v: SetGlobalFilterCondRequest): SetGlobalFilterCondRequest {
    if (v) {
        v.globalFilter = safeGlobalFilter(v.globalFilter);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeSetGlobalFilterCondRequests(vs: SetGlobalFilterCondRequest[]): SetGlobalFilterCondRequest[] {
    if (vs) {
        return vs.map((v: SetGlobalFilterCondRequest) => {
            return safeSetGlobalFilterCondRequest(v);
        });
    } else {
        return [];
    }
}

export interface SetGlobalFilterCondResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeSetGlobalFilterCondResponse(v: SetGlobalFilterCondResponse): SetGlobalFilterCondResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeSetGlobalFilterCondResponses(vs: SetGlobalFilterCondResponse[]): SetGlobalFilterCondResponse[] {
    if (vs) {
        return vs.map((v: SetGlobalFilterCondResponse) => {
            return safeSetGlobalFilterCondResponse(v);
        });
    } else {
        return [];
    }
}

export interface SearchHotKeyRequest {

    /**
    * 一次查询的总条数。
    */
    count: string;

    /**
    * 查询的起始位，0表示第一个。
    */
    offset: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeSearchHotKeyRequest(v: SearchHotKeyRequest): SearchHotKeyRequest {
    if (v) {
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeSearchHotKeyRequests(vs: SearchHotKeyRequest[]): SearchHotKeyRequest[] {
    if (vs) {
        return vs.map((v: SearchHotKeyRequest) => {
            return safeSearchHotKeyRequest(v);
        });
    } else {
        return [];
    }
}

export interface SearchHotKeyResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 支持查询的搜索热词总数。
    */
    total: string;

    /**
    * 搜索热词关键字信息。有结果时返回。每个热词长度小于等于128。
    */
    hotKeys?: string[];

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeSearchHotKeyResponse(v: SearchHotKeyResponse): SearchHotKeyResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.hotKeys = v.hotKeys || [];
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeSearchHotKeyResponses(vs: SearchHotKeyResponse[]): SearchHotKeyResponse[] {
    if (vs) {
        return vs.map((v: SearchHotKeyResponse) => {
            return safeSearchHotKeyResponse(v);
        });
    } else {
        return [];
    }
}

export interface SearchContentRequest {

    /**
    * 搜索关键字。如果搜索关键字中包括特殊字符，VSP会将特殊字符替换成空格符，特殊字符包括：
    *
    * ASCII码0~31和127，以及如下字符: “~,!,^,(,),[,],\\,{,},:,\,@,#,$,%,&amp;,*,-,&#x3D;,_,+,|,;,\&#x27;,英文逗号,.,/,&lt;,&gt;,?”。
    */
    searchKey: string;

    /**
    * 搜索的内容类型，取值范围：
    *
    * VOD：点播内容，包括音频和视频内容
    *
    * AUDIO_VOD：点播内容，仅限于音频内容
    *
    * VIDEO_VOD：点播内容，仅限于视频内容
    *
    * CHANNEL：直播频道
    *
    * AUDIO_CHANNEL：直播频道，仅限于音频
    *
    * VIDEO_CHANNEL：直播频道，仅限于视频
    *
    * FILECAST_CHANNEL：[字段预留]
    *
    * PROGRAM：正在播放和未开始的直播节目单
    *
    * TVOD：录制成功的过期节目单
    *
    * VAS：增值业务
    */
    contentTypes: string[];

    /**
    * 搜索范围，取值范围：
    *
    * CONTENT_NAME：内容名称
    *
    * GENRE_NAME：流派名称，仅VOD、节目单和VAS支持
    *
    * TAGS：点播标签
    *
    * KEYWORD：关键字，仅VOD、频道和节目单支持
    *
    * ACTOR：演员名称
    *
    * DIRECTOR：导演名称
    *
    * WRITTER：作者名称
    *
    * SINGER：演唱者名称
    *
    * PRODUCER：制片人名称
    *
    * ADAPTOR：编剧名称
    *
    * OTHER_CAST：其他演职员名称
    *
    * CAST_ID：按照演职员ID精确搜索相关的VOD，节目单内容，不与以上字段组合使用，searchKey必须为准确演职员ID。
    *
    * ALL：所有以上字段。
    */
    searchScopes: string[];

    /**
    * 栏目ID，在指定栏目下搜索内容，栏目可以是父栏目。
    */
    subjectID?: string;

    /**
    * 过滤条件。
    */
    filter?: SearchFilter;

    /**
    * 排他条件。
    */
    excluder?: SearchExcluder;

    /**
    * 一次查询的总条数。
    */
    count: string;

    /**
    * 查询的起始位置。
    */
    offset: string;

    /**
    * 排序方式，取值范围：
    *
    * RELEVANCE：关键词与内容相关性降序
    *
    * CONTENT_NAME:ASC：内容名称升序
    *
    * CONTENT_NAME:DESC：内容名称降序
    *
    * CONTENT_TYPE:ASC：内容类型升序
    *
    * CONTENT_TYPE:DESC：内容类型降序
    *
    * START_TIME:ASC：内容生效时间/节目单开始时间升序
    *
    * START_TIME:DESC：内容生效时间/节目单开始时间降序
    *
    * END_TIME:ASC：内容失效时间/节目单结束时间升序
    *
    * END_TIME:DESC：内容失效时间/节目单结束时间降序
    *
    * PLAY_TIMES:ASC：播放次数升序
    *
    * PLAY_TIMES:DESC：播放次数降序
    *
    * AVG_SCORE:ASC：评分均值升序
    *
    * AVG_SCORE:DESC：评分均值降序
    *
    * GENRE_NAME:ASC：流派名称升序
    *
    * GENRE_NAME:DESC：流派名称降序
    *
    * PERSONAL：个性化排序
    *
    * 如果传入多个排序方式，将按照传入顺序依次排序。默认按相关性排序。
    *
    * 约束：个性化排序方式不支持与其它排序方式叠加，不支持修改升降序。
    */
    sortType: string[];

    /**
    * 是否使用搜索关键词纠错功能，取值包括：
    *
    * 1：使用关键词纠错
    *
    * 0：使用原词搜索
    *
    * 默认值为1。
    */
    correction?: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeSearchContentRequest(v: SearchContentRequest): SearchContentRequest {
    if (v) {
        v.contentTypes = v.contentTypes || [];
        v.searchScopes = v.searchScopes || [];
        v.filter = safeSearchFilter(v.filter);
        v.excluder = safeSearchExcluder(v.excluder);
        v.sortType = v.sortType || [];
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeSearchContentRequests(vs: SearchContentRequest[]): SearchContentRequest[] {
    if (vs) {
        return vs.map((v: SearchContentRequest) => {
            return safeSearchContentRequest(v);
        });
    } else {
        return [];
    }
}

export interface SearchContentResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 是否对关键词进行了纠错：
    *
    * 1：纠错，使用纠错词汇搜索；
    *
    * 0：未纠错，原词搜索。
    *
    * 默认值为0。
    */
    correction?: string;

    /**
    * 纠错后关键词，当correction取值1时必填。
    */
    correctKey?: string;

    /**
    * 搜索记录总数。总数计算不准确时不返回该参数。
    */
    total?: string;

    /**
    * 如果搜索成功，返回搜索内容。
    *
    * 搜索请求参数contentTypes取值范围为：
    *
    * VOD：点播内容，包括音频和视频内容
    *
    * AUDIO_VOD：点播内容，仅限于音频内容
    *
    * VIDEO_VOD：点播内容，仅限于视频内容
    *
    * 对应返回Content中vod字段。
    *
    * 搜索请求参数contentTypes取值范围为：
    *
    * CHANNEL：直播频道
    *
    * AUDIO_CHANNEL：直播频道，仅限于音频
    *
    * VIDEO_CHANNEL：直播频道，仅限于视频
    *
    * FILECAST_CHANNEL：[字段预留]
    *
    * 对应返回Content中channel字段。
    *
    * 搜索请求参数contentTypes取值范围为：
    *
    * PROGRAM：正在播放和未开始的直播节目单
    *
    * TVOD：录制成功的过期节目单
    *
    * 对应返回Content中的playbill字段。
    *
    * 搜索请求参数contentTypes取值范围为VAS对应返回Content中的vas字段。
    */
    contents?: Content[];

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeSearchContentResponse(v: SearchContentResponse): SearchContentResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.contents = safeContents(v.contents);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeSearchContentResponses(vs: SearchContentResponse[]): SearchContentResponse[] {
    if (vs) {
        return vs.map((v: SearchContentResponse) => {
            return safeSearchContentResponse(v);
        });
    } else {
        return [];
    }
}

export interface SuggestKeywordRequest {

    /**
    * 一次查询的总条数。建议不超过50。
    */
    count: string;

    /**
    * 搜索关键字。
    */
    key: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeSuggestKeywordRequest(v: SuggestKeywordRequest): SuggestKeywordRequest {
    if (v) {
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeSuggestKeywordRequests(vs: SuggestKeywordRequest[]): SuggestKeywordRequest[] {
    if (vs) {
        return vs.map((v: SuggestKeywordRequest) => {
            return safeSuggestKeywordRequest(v);
        });
    } else {
        return [];
    }
}

export interface SuggestKeywordResponse {

    /**
    * 返回结果。
    */
    result?: Result;

    /**
    * 建议的搜索关键词列表，有建议词汇时返回。
    *
    * 数组中的每条数据的长度小于等于128
    */
    suggests?: string[];

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeSuggestKeywordResponse(v: SuggestKeywordResponse): SuggestKeywordResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.suggests = v.suggests || [];
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeSuggestKeywordResponses(vs: SuggestKeywordResponse[]): SuggestKeywordResponse[] {
    if (vs) {
        return vs.map((v: SuggestKeywordResponse) => {
            return safeSuggestKeywordResponse(v);
        });
    } else {
        return [];
    }
}

export interface QueryRecmContentRequest {

    /**
    * 推荐场景参数。
    */
    queryDynamicRecmContent: QueryDynamicRecmContent;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeQueryRecmContentRequest(v: QueryRecmContentRequest): QueryRecmContentRequest {
    if (v) {
        v.queryDynamicRecmContent = safeQueryDynamicRecmContent(v.queryDynamicRecmContent);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeQueryRecmContentRequests(vs: QueryRecmContentRequest[]): QueryRecmContentRequest[] {
    if (vs) {
        return vs.map((v: QueryRecmContentRequest) => {
            return safeQueryRecmContentRequest(v);
        });
    } else {
        return [];
    }
}

export interface QueryRecmContentResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 推荐请求流水号。
    *
    * 字段由推荐系统产生返回给VSP透传，用于唯一标记某个内容推荐请求操作，用于后续与用户推荐结果反馈关联。
    */
    recmActionID?: string;

    /**
    * 推荐结果列表。
    *
    * 说明
    *
    * 由于推荐接口支持1次批量查询多个推荐位列表，因此推荐结果数组数量、数组内数据顺序必须和推荐请求参数RecmScenario指定的推荐位参数相同并且一一对应。如果某个推荐请求对应的推荐结果内容没有数据则推荐结果数组中对应对象返回空。
    */
    recmContents?: RecmContents[];

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeQueryRecmContentResponse(v: QueryRecmContentResponse): QueryRecmContentResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.recmContents = safeRecmContentss(v.recmContents);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeQueryRecmContentResponses(vs: QueryRecmContentResponse[]): QueryRecmContentResponse[] {
    if (vs) {
        return vs.map((v: QueryRecmContentResponse) => {
            return safeQueryRecmContentResponse(v);
        });
    } else {
        return [];
    }
}

export interface QueryHotSearchContentsListRequest {

    /**
    * 一次查询的总条数，不能设置为-1，调用者一定要指定获取数据的总条数，最大条数默认不超过50，最大条数可配置，超过最大条数返回错误。
    */
    count: string;

    /**
    * 查询的起始位，0表示第一个。
    */
    offset: string;

    /**
    * 要查询的热门搜索内容排行榜类型：
    *
    * ALL：总排行榜，用来呈现热搜内容名称
    *
    * Series：VOD电视剧
    *
    * Movies：VOD电影
    *
    * Program：节目单
    *
    * 后续支持扩展
    */
    boardType: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeQueryHotSearchContentsListRequest(v: QueryHotSearchContentsListRequest): QueryHotSearchContentsListRequest {
    if (v) {
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeQueryHotSearchContentsListRequests(vs: QueryHotSearchContentsListRequest[]): QueryHotSearchContentsListRequest[] {
    if (vs) {
        return vs.map((v: QueryHotSearchContentsListRequest) => {
            return safeQueryHotSearchContentsListRequest(v);
        });
    } else {
        return [];
    }
}

export interface QueryHotSearchContentsListResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 热门搜索内容总个数，空的时候为0。
    */
    total: string;

    /**
    * 热门搜索内容列表。如果查询结果为空，不返回该参数。
    */
    hotContent?: Content[];

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeQueryHotSearchContentsListResponse(v: QueryHotSearchContentsListResponse): QueryHotSearchContentsListResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.hotContent = safeContents(v.hotContent);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeQueryHotSearchContentsListResponses(vs: QueryHotSearchContentsListResponse[]): QueryHotSearchContentsListResponse[] {
    if (vs) {
        return vs.map((v: QueryHotSearchContentsListResponse) => {
            return safeQueryHotSearchContentsListResponse(v);
        });
    } else {
        return [];
    }
}

export interface AddFavoCatalogRequest {

    /**
    * 待新增的收藏夹记录。
    */
    catalog: FavoCatalog;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeAddFavoCatalogRequest(v: AddFavoCatalogRequest): AddFavoCatalogRequest {
    if (v) {
        v.catalog = safeFavoCatalog(v.catalog);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeAddFavoCatalogRequests(vs: AddFavoCatalogRequest[]): AddFavoCatalogRequest[] {
    if (vs) {
        return vs.map((v: AddFavoCatalogRequest) => {
            return safeAddFavoCatalogRequest(v);
        });
    } else {
        return [];
    }
}

export interface AddFavoCatalogResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeAddFavoCatalogResponse(v: AddFavoCatalogResponse): AddFavoCatalogResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeAddFavoCatalogResponses(vs: AddFavoCatalogResponse[]): AddFavoCatalogResponse[] {
    if (vs) {
        return vs.map((v: AddFavoCatalogResponse) => {
            return safeAddFavoCatalogResponse(v);
        });
    } else {
        return [];
    }
}

export interface DeleteFavoCatalogRequest {

    /**
    * 收藏夹ID。如果没有值，表示清空所有收藏夹分类。
    *
    * 说明
    *
    * 默认收藏夹不能被删除。
    */
    catalogID?: string;

    /**
    * 删除类型。
    *
    * 0 : 按收藏夹ID删除，根据传入的收藏夹ID删除指定的收藏夹
    *
    * 1 : 按收藏夹ID反选删除，保留传入的收藏夹ID，其他的收藏夹数据全部删除
    *
    * 此字段默认为0。
    */
    deleteType?: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeDeleteFavoCatalogRequest(v: DeleteFavoCatalogRequest): DeleteFavoCatalogRequest {
    if (v) {
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeDeleteFavoCatalogRequests(vs: DeleteFavoCatalogRequest[]): DeleteFavoCatalogRequest[] {
    if (vs) {
        return vs.map((v: DeleteFavoCatalogRequest) => {
            return safeDeleteFavoCatalogRequest(v);
        });
    } else {
        return [];
    }
}

export interface DeleteFavoCatalogResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeDeleteFavoCatalogResponse(v: DeleteFavoCatalogResponse): DeleteFavoCatalogResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeDeleteFavoCatalogResponses(vs: DeleteFavoCatalogResponse[]): DeleteFavoCatalogResponse[] {
    if (vs) {
        return vs.map((v: DeleteFavoCatalogResponse) => {
            return safeDeleteFavoCatalogResponse(v);
        });
    } else {
        return [];
    }
}

export interface UpdateFavoCatalogRequest {

    /**
    * 待更新的收藏夹记录，目前只能更新收藏夹名称。
    */
    catalog: FavoCatalog;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeUpdateFavoCatalogRequest(v: UpdateFavoCatalogRequest): UpdateFavoCatalogRequest {
    if (v) {
        v.catalog = safeFavoCatalog(v.catalog);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeUpdateFavoCatalogRequests(vs: UpdateFavoCatalogRequest[]): UpdateFavoCatalogRequest[] {
    if (vs) {
        return vs.map((v: UpdateFavoCatalogRequest) => {
            return safeUpdateFavoCatalogRequest(v);
        });
    } else {
        return [];
    }
}

export interface UpdateFavoCatalogResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeUpdateFavoCatalogResponse(v: UpdateFavoCatalogResponse): UpdateFavoCatalogResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeUpdateFavoCatalogResponses(vs: UpdateFavoCatalogResponse[]): UpdateFavoCatalogResponse[] {
    if (vs) {
        return vs.map((v: UpdateFavoCatalogResponse) => {
            return safeUpdateFavoCatalogResponse(v);
        });
    } else {
        return [];
    }
}

export interface QueryFavoCatalogRequest {

    /**
    * 一次查询的总条数，最大条数默认不超过50，最大条数可配置，超过最大条数返回错误。
    */
    count: string;

    /**
    * 查询的起始位置，从0开始（即0表示第一个）。
    */
    offset: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeQueryFavoCatalogRequest(v: QueryFavoCatalogRequest): QueryFavoCatalogRequest {
    if (v) {
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeQueryFavoCatalogRequests(vs: QueryFavoCatalogRequest[]): QueryFavoCatalogRequest[] {
    if (vs) {
        return vs.map((v: QueryFavoCatalogRequest) => {
            return safeQueryFavoCatalogRequest(v);
        });
    } else {
        return [];
    }
}

export interface QueryFavoCatalogResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 总记录数。
    */
    total?: string;

    /**
    * 如果查询成功，返回用户的收藏夹。
    */
    catalogs?: FavoCatalog[];

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeQueryFavoCatalogResponse(v: QueryFavoCatalogResponse): QueryFavoCatalogResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.catalogs = safeFavoCatalogs(v.catalogs);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeQueryFavoCatalogResponses(vs: QueryFavoCatalogResponse[]): QueryFavoCatalogResponse[] {
    if (vs) {
        return vs.map((v: QueryFavoCatalogResponse) => {
            return safeQueryFavoCatalogResponse(v);
        });
    } else {
        return [];
    }
}

export interface CreateFavoriteRequest {

    /**
    * 用户待创建的收藏内容。
    */
    favorites: Favorite[];

    /**
    * 当添加收藏到达上限，是否支持自动覆盖最老的收藏，取值包括：
    *
    * 0：不支持
    *
    * 1：支持
    *
    * 默认值为1。
    */
    autoCover?: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeCreateFavoriteRequest(v: CreateFavoriteRequest): CreateFavoriteRequest {
    if (v) {
        v.favorites = safeFavorites(v.favorites);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeCreateFavoriteRequests(vs: CreateFavoriteRequest[]): CreateFavoriteRequest[] {
    if (vs) {
        return vs.map((v: CreateFavoriteRequest) => {
            return safeCreateFavoriteRequest(v);
        });
    } else {
        return [];
    }
}

export interface CreateFavoriteResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 用户收藏的最新版本号，目前使用14位的时间表示，格式类似yyyyMMddHHmmss。
    *
    * 用于频道缓存场景的业务逻辑处理。
    */
    version?: string;

    /**
    * 用户收藏的前一次的版本号，目前使用14位的时间表示，格式类似yyyyMMddHHmmss。
    *
    * 用于频道缓存场景的业务逻辑处理。
    */
    preVersion?: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeCreateFavoriteResponse(v: CreateFavoriteResponse): CreateFavoriteResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeCreateFavoriteResponses(vs: CreateFavoriteResponse[]): CreateFavoriteResponse[] {
    if (vs) {
        return vs.map((v: CreateFavoriteResponse) => {
            return safeCreateFavoriteResponse(v);
        });
    } else {
        return [];
    }
}

export interface DeleteFavoriteRequest {

    /**
    * 删除指定收藏夹下的内容。
    *
    * 如果没有传值，表示删除所有收藏夹下的收藏内容。
    *
    * 如果取值是-1，表示删除默认收藏夹下的收藏内容。
    */
    catalogID?: string;

    /**
    * 删除指定内容类型的收藏。
    *
    * 枚举值参考Favorite对象的contentType。
    */
    contentTypes?: string[];

    /**
    * 删除指定内容ID的收藏。
    */
    contentIDs?: string[];

    /**
    * 删除类型。
    *
    * 0 : 按收藏内容ID删除，根据传入的收藏内容ID删除指定的收藏（在同一个contentType下）
    *
    * 1 : 按收藏内容ID反选删除，保留传入的收藏内容ID，此收藏夹的其余收藏数据全部删除（在同一个contentType下）
    *
    * 此字段默认为0。
    */
    deleteType?: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeDeleteFavoriteRequest(v: DeleteFavoriteRequest): DeleteFavoriteRequest {
    if (v) {
        v.contentTypes = v.contentTypes || [];
        v.contentIDs = v.contentIDs || [];
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeDeleteFavoriteRequests(vs: DeleteFavoriteRequest[]): DeleteFavoriteRequest[] {
    if (vs) {
        return vs.map((v: DeleteFavoriteRequest) => {
            return safeDeleteFavoriteRequest(v);
        });
    } else {
        return [];
    }
}

export interface DeleteFavoriteResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 用户收藏的最新版本号，目前使用14位的时间表示，格式类似yyyyMMddHHmmss。
    *
    * 用于频道缓存场景的业务逻辑处理。
    */
    version?: string;

    /**
    * 用户收藏的前一次的版本号，目前使用14位的时间表示，格式类似yyyyMMddHHmmss。
    *
    * 用于频道缓存场景的业务逻辑处理。
    */
    preVersion?: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeDeleteFavoriteResponse(v: DeleteFavoriteResponse): DeleteFavoriteResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeDeleteFavoriteResponses(vs: DeleteFavoriteResponse[]): DeleteFavoriteResponse[] {
    if (vs) {
        return vs.map((v: DeleteFavoriteResponse) => {
            return safeDeleteFavoriteResponse(v);
        });
    } else {
        return [];
    }
}

export interface QueryFavoriteRequest {

    /**
    * 查询指定收藏夹下的内容。
    *
    * 如果没有传值，表示查询所有收藏夹下的收藏内容。
    *
    * 如果取值是-1，表示查询默认收藏夹下的收藏内容。
    */
    catalogID?: string;

    /**
    * 查询指定内容类型的收藏。
    *
    * 如果不指定，将查询指定收藏夹或者所有收藏夹下的内容。
    *
    * 枚举值参考Favorite对象的contentType。
    */
    contentTypes?: string[];

    /**
    * 一次查询的总条数，最大条数默认不超过50，最大条数可配置，超过最大条数返回错误。
    */
    count: string;

    /**
    * 查询的起始位置，从0开始（即0表示第一个）。
    */
    offset: string;

    /**
    * 收藏排序方式，取值包括：
    *
    * FAVO_TIME:ASC：按收藏时间升序排列
    *
    * FAVO_TIME:DESC：按收藏时间降序排列
    *
    * SORT_NO:ASC：按照收藏序号升序排列
    *
    * SORT_NO:DESC：按照收藏序号降序排列
    *
    * CHAN_NO:ASC：按照频道号升序排列
    *
    * CHAN_NO:DESC：按照频道号降序排列
    *
    * 默认值为FAVO_TIME:ASC。
    */
    sortType?: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeQueryFavoriteRequest(v: QueryFavoriteRequest): QueryFavoriteRequest {
    if (v) {
        v.contentTypes = v.contentTypes || [];
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeQueryFavoriteRequests(vs: QueryFavoriteRequest[]): QueryFavoriteRequest[] {
    if (vs) {
        return vs.map((v: QueryFavoriteRequest) => {
            return safeQueryFavoriteRequest(v);
        });
    } else {
        return [];
    }
}

export interface QueryFavoriteResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 如果查询成功，返回条件匹配的总记录数。
    */
    total?: string;

    /**
    * 如果查询成功，返回收藏版本号。
    */
    version?: string;

    /**
    * 如果查询成功，返回个性化收藏信息。
    */
    favorites?: Content[];

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeQueryFavoriteResponse(v: QueryFavoriteResponse): QueryFavoriteResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.favorites = safeContents(v.favorites);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeQueryFavoriteResponses(vs: QueryFavoriteResponse[]): QueryFavoriteResponse[] {
    if (vs) {
        return vs.map((v: QueryFavoriteResponse) => {
            return safeQueryFavoriteResponse(v);
        });
    } else {
        return [];
    }
}

export interface SortFavoriteRequest {

    /**
    * 用户排好序的收藏内容。
    */
    favorites: Favorite[];

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeSortFavoriteRequest(v: SortFavoriteRequest): SortFavoriteRequest {
    if (v) {
        v.favorites = safeFavorites(v.favorites);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeSortFavoriteRequests(vs: SortFavoriteRequest[]): SortFavoriteRequest[] {
    if (vs) {
        return vs.map((v: SortFavoriteRequest) => {
            return safeSortFavoriteRequest(v);
        });
    } else {
        return [];
    }
}

export interface SortFavoriteResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeSortFavoriteResponse(v: SortFavoriteResponse): SortFavoriteResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeSortFavoriteResponses(vs: SortFavoriteResponse[]): SortFavoriteResponse[] {
    if (vs) {
        return vs.map((v: SortFavoriteResponse) => {
            return safeSortFavoriteResponse(v);
        });
    } else {
        return [];
    }
}

export interface CreateLockRequest {

    /**
    * 用户待创建的童锁。
    */
    locks: Lock[];

    /**
    * 锁共享方式，取值包括：
    *
    * 0：独享锁
    *
    * 1：共享锁
    *
    * 说明
    *
    * 如果Lock指定了子Profile，该参数不生效，因为固定是独享锁。
    *
    * 如果Lock未指定子Profile，该参数表示锁对所有Profile生效(也就是共享锁)还是只对主Profile生效(也就是独享锁)。
    *
    * 默认值是1。
    */
    isShare?: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeCreateLockRequest(v: CreateLockRequest): CreateLockRequest {
    if (v) {
        v.locks = safeLocks(v.locks);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeCreateLockRequests(vs: CreateLockRequest[]): CreateLockRequest[] {
    if (vs) {
        return vs.map((v: CreateLockRequest) => {
            return safeCreateLockRequest(v);
        });
    } else {
        return [];
    }
}

export interface CreateLockResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 用户童锁的最新版本号，目前使用14位的时间表示，格式类似yyyyMMddHHmmss。
    *
    * 用于频道缓存场景的业务逻辑处理。
    */
    version?: string;

    /**
    * 用户童锁的前一次的版本号，目前使用14位的时间表示，格式类似yyyyMMddHHmmss。
    *
    * 用于频道缓存场景的业务逻辑处理。
    */
    preVersion?: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeCreateLockResponse(v: CreateLockResponse): CreateLockResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeCreateLockResponses(vs: CreateLockResponse[]): CreateLockResponse[] {
    if (vs) {
        return vs.map((v: CreateLockResponse) => {
            return safeCreateLockResponse(v);
        });
    } else {
        return [];
    }
}

export interface DeleteLockRequest {

    /**
    * 删除指定Profile的锁，如果profileID是子Profile，则删除子Profile的独享锁，如果是主profileID，则删除共享锁和主Profile的独享锁。
    *
    * 如果没有值，表示删除共享锁、主Profile和子Profile的独享锁中满足要求的记录。
    */
    profileID?: string;

    /**
    * 加锁的对象类型，枚举值参考Lock对象的lockType属性。
    */
    lockTypes?: string[];

    /**
    * 加锁内容的标识。
    *
    * 如果对VOD加锁，就是VOD内容ID。
    *
    * 如果对频道加锁，就是频道内容ID。
    *
    * 如果对节目单加锁，就是节目单内容ID。
    *
    * 如果对栏目加锁，就是栏目ID。
    *
    * 如果对连续剧节目单加锁，就是节目单的SeriesID。
    *
    * 如果对流派加锁，就是流派ID。
    *
    * 如果对VAS加锁，就是VAS内容ID。
    *
    * 说明
    *
    * 由于不同加锁类型的itemID可能冲突(主要是SeriesID和流派ID，和内容ID不唯一)，如果终端请求包含itemIDS，必须同时携带lockTypes，而且itemIDS和lockType元素是1对1的关系。
    */
    itemIDs?: string[];

    /**
    * 删除类型。
    *
    * 0 : 按加锁内容ID删除，根据传入的加锁内容ID删除指定的锁信息
    *
    * 1 : 按加锁内容ID反选删除，保留传入的加锁内容ID，其他的锁数据全部删除
    *
    * 此字段默认为0。
    */
    deleteType?: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeDeleteLockRequest(v: DeleteLockRequest): DeleteLockRequest {
    if (v) {
        v.lockTypes = v.lockTypes || [];
        v.itemIDs = v.itemIDs || [];
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeDeleteLockRequests(vs: DeleteLockRequest[]): DeleteLockRequest[] {
    if (vs) {
        return vs.map((v: DeleteLockRequest) => {
            return safeDeleteLockRequest(v);
        });
    } else {
        return [];
    }
}

export interface DeleteLockResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 用户童锁的最新版本号，目前使用14位的时间表示，格式类似yyyyMMddHHmmss。
    *
    * 用于频道缓存场景的业务逻辑处理。
    */
    version?: string;

    /**
    * 用户童锁的前一次的版本号，目前使用14位的时间表示，格式类似yyyyMMddHHmmss。
    *
    * 用于频道缓存场景的业务逻辑处理。
    */
    preVersion?: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeDeleteLockResponse(v: DeleteLockResponse): DeleteLockResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeDeleteLockResponses(vs: DeleteLockResponse[]): DeleteLockResponse[] {
    if (vs) {
        return vs.map((v: DeleteLockResponse) => {
            return safeDeleteLockResponse(v);
        });
    } else {
        return [];
    }
}

export interface QueryLockRequest {

    /**
    * 查找指定账户的锁。
    */
    profileID?: string;

    /**
    * 共享方式，取值包括：
    *
    * -1：所有
    *
    * 0：独享锁
    *
    * 1：共享锁
    *
    * 默认值是-1。
    */
    isShare?: string;

    /**
    * 加锁的对象类型，枚举值参考Lock对象的lockType属性。
    */
    lockTypes?: string[];

    /**
    * 一次查询的总条数，最大条数默认不超过50，最大条数可配置，超过最大条数返回错误。
    */
    count: string;

    /**
    * 查询的起始位置，从0开始（即0表示第一个）。
    */
    offset: string;

    /**
    * 收藏排序方式，取值包括：
    *
    * CREATE_TIME:ASC：按创建时间升序排列
    *
    * CREATE_TIME:DESC：按创建时间降序排列
    *
    * CHAN_NO:ASC：按照频道号升序排列
    *
    * CHAN_NO:DESC：按照频道号降序排列
    *
    * 默认值是CREATE_TIME:ASC。
    *
    * 说明
    *
    * 按频道号排序只对频道和VAS有效。
    */
    sortType?: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeQueryLockRequest(v: QueryLockRequest): QueryLockRequest {
    if (v) {
        v.lockTypes = v.lockTypes || [];
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeQueryLockRequests(vs: QueryLockRequest[]): QueryLockRequest[] {
    if (vs) {
        return vs.map((v: QueryLockRequest) => {
            return safeQueryLockRequest(v);
        });
    } else {
        return [];
    }
}

export interface QueryLockResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 如果查询成功，返回条件匹配的总记录数。
    *
    * 如果查询失败，该参数不返回。
    */
    total?: string;

    /**
    * 如果查询成功，返回童锁版本号。
    *
    * 如果查询失败，该参数不返回。
    */
    version?: string;

    /**
    * 如果查询成功，返回个性化加锁信息。
    *
    * 如果查询失败，该参数不返回。
    */
    locks?: LockItem[];

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeQueryLockResponse(v: QueryLockResponse): QueryLockResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.locks = safeLockItems(v.locks);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeQueryLockResponses(vs: QueryLockResponse[]): QueryLockResponse[] {
    if (vs) {
        return vs.map((v: QueryLockResponse) => {
            return safeQueryLockResponse(v);
        });
    } else {
        return [];
    }
}

export interface CreateBookmarkRequest {

    /**
    * 用户待创建的内容书签。
    */
    bookmarks: Bookmark[];

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeCreateBookmarkRequest(v: CreateBookmarkRequest): CreateBookmarkRequest {
    if (v) {
        v.bookmarks = safeBookmarks(v.bookmarks);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeCreateBookmarkRequests(vs: CreateBookmarkRequest[]): CreateBookmarkRequest[] {
    if (vs) {
        return vs.map((v: CreateBookmarkRequest) => {
            return safeCreateBookmarkRequest(v);
        });
    } else {
        return [];
    }
}

export interface CreateBookmarkResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeCreateBookmarkResponse(v: CreateBookmarkResponse): CreateBookmarkResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeCreateBookmarkResponses(vs: CreateBookmarkResponse[]): CreateBookmarkResponse[] {
    if (vs) {
        return vs.map((v: CreateBookmarkResponse) => {
            return safeCreateBookmarkResponse(v);
        });
    } else {
        return [];
    }
}

export interface DeleteBookmarkRequest {

    /**
    * 书签类型，枚举值参考Bookmark对象的bookmarkType属性。
    */
    bookmarkTypes?: string[];

    /**
    * 书签内容ID。
    *
    * 如果是VOD书签，取值为VOD内容ID。
    *
    * 如果是Catch-up TV书签，取值为节目单ID。
    *
    * 如果是NPVR书签，取值是录制计划ID。
    *
    * 如果是CPVR书签，取值是录制计划ID。
    *
    * 说明
    *
    * 由于不同书签类型的itemID可能冲突(主要是后续提供的PVR任务ID和内容ID不唯一)，如果终端请求包含itemIDS，必须同时携带bookmarkTypes，而且itemIDS和bookmarkTypes元素是1对1的关系。
    */
    itemIDs?: string[];

    /**
    * 删除类型。
    *
    * 0 : 按书签内容ID删除，根据传入的书签内容ID删除指定的书签数据
    *
    * 1 : 按书签内容ID反选删除，保留传入的书签内容ID，其他的书签数据全部删除
    *
    * 此字段默认为0。
    */
    deleteType?: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeDeleteBookmarkRequest(v: DeleteBookmarkRequest): DeleteBookmarkRequest {
    if (v) {
        v.bookmarkTypes = v.bookmarkTypes || [];
        v.itemIDs = v.itemIDs || [];
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeDeleteBookmarkRequests(vs: DeleteBookmarkRequest[]): DeleteBookmarkRequest[] {
    if (vs) {
        return vs.map((v: DeleteBookmarkRequest) => {
            return safeDeleteBookmarkRequest(v);
        });
    } else {
        return [];
    }
}

export interface DeleteBookmarkResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeDeleteBookmarkResponse(v: DeleteBookmarkResponse): DeleteBookmarkResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeDeleteBookmarkResponses(vs: DeleteBookmarkResponse[]): DeleteBookmarkResponse[] {
    if (vs) {
        return vs.map((v: DeleteBookmarkResponse) => {
            return safeDeleteBookmarkResponse(v);
        });
    } else {
        return [];
    }
}

export interface QueryBookmarkRequest {

    /**
    * 书签类型，枚举值参考Bookmark对象的bookmarkType属性。
    */
    bookmarkTypes?: string[];

    /**
    * 过滤条件。
    */
    filter?: BookmarkFilter;

    /**
    * 一次查询的总条数，最大条数默认不超过50，最大条数可配置，超过最大条数返回错误。
    */
    count: string;

    /**
    * 查询的起始位置，从0开始（即0表示第一个）。
    */
    offset: string;

    /**
    * 排序方式。取值范围：
    *
    * UPDATE_TIME:ASC：按更新时间升序排列
    *
    * UPDATE_TIME:DESC：按更新时间降序排列
    *
    * 默认值为UPDATE_TIME:DESC。
    */
    sortType?: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeQueryBookmarkRequest(v: QueryBookmarkRequest): QueryBookmarkRequest {
    if (v) {
        v.bookmarkTypes = v.bookmarkTypes || [];
        v.filter = safeBookmarkFilter(v.filter);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeQueryBookmarkRequests(vs: QueryBookmarkRequest[]): QueryBookmarkRequest[] {
    if (vs) {
        return vs.map((v: QueryBookmarkRequest) => {
            return safeQueryBookmarkRequest(v);
        });
    } else {
        return [];
    }
}

export interface QueryBookmarkResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 如果查询成功，返回条件匹配的总记录数。
    *
    * 如果查询失败，该参数不返回。
    */
    total?: string;

    /**
    * 如果查询成功，返回书签版本号。
    *
    * 如果查询失败，该参数不返回。
    */
    version?: string;

    /**
    * 如果查询成功，返回个性化书签信息。
    *
    * 如果查询失败，该参数不返回。
    */
    bookmarks?: BookmarkItem[];

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeQueryBookmarkResponse(v: QueryBookmarkResponse): QueryBookmarkResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.bookmarks = safeBookmarkItems(v.bookmarks);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeQueryBookmarkResponses(vs: QueryBookmarkResponse[]): QueryBookmarkResponse[] {
    if (vs) {
        return vs.map((v: QueryBookmarkResponse) => {
            return safeQueryBookmarkResponse(v);
        });
    } else {
        return [];
    }
}

export interface CreateContentScoreRequest {

    /**
    * 用户待创建的评分。
    */
    scores: ContentScore[];

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeCreateContentScoreRequest(v: CreateContentScoreRequest): CreateContentScoreRequest {
    if (v) {
        v.scores = safeContentScores(v.scores);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeCreateContentScoreRequests(vs: CreateContentScoreRequest[]): CreateContentScoreRequest[] {
    if (vs) {
        return vs.map((v: CreateContentScoreRequest) => {
            return safeCreateContentScoreRequest(v);
        });
    } else {
        return [];
    }
}

export interface CreateContentScoreResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 如果评分添加成功，返回内容最新的评分均值，支持小数，返回小数点后一位小数。
    */
    newScores?: string[];

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeCreateContentScoreResponse(v: CreateContentScoreResponse): CreateContentScoreResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.newScores = v.newScores || [];
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeCreateContentScoreResponses(vs: CreateContentScoreResponse[]): CreateContentScoreResponse[] {
    if (vs) {
        return vs.map((v: CreateContentScoreResponse) => {
            return safeCreateContentScoreResponse(v);
        });
    } else {
        return [];
    }
}

export interface SetContentLikeRequest {

    /**
    * 用户设置赞或取消赞。
    */
    settings: ContentLikeSetting[];

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeSetContentLikeRequest(v: SetContentLikeRequest): SetContentLikeRequest {
    if (v) {
        v.settings = safeContentLikeSettings(v.settings);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeSetContentLikeRequests(vs: SetContentLikeRequest[]): SetContentLikeRequest[] {
    if (vs) {
        return vs.map((v: SetContentLikeRequest) => {
            return safeSetContentLikeRequest(v);
        });
    } else {
        return [];
    }
}

export interface SetContentLikeResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 如果赞或取消赞设置成功，返回内容最新的总的赞数量
    */
    contentLikes?: ContentLikes[];

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeSetContentLikeResponse(v: SetContentLikeResponse): SetContentLikeResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.contentLikes = safeContentLikess(v.contentLikes);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeSetContentLikeResponses(vs: SetContentLikeResponse[]): SetContentLikeResponse[] {
    if (vs) {
        return vs.map((v: SetContentLikeResponse) => {
            return safeSetContentLikeResponse(v);
        });
    } else {
        return [];
    }
}

export interface CreatePlaylistRequest {

    /**
    * 待新增的播放列表。
    */
    playlists: Playlist[];

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeCreatePlaylistRequest(v: CreatePlaylistRequest): CreatePlaylistRequest {
    if (v) {
        v.playlists = safePlaylists(v.playlists);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeCreatePlaylistRequests(vs: CreatePlaylistRequest[]): CreatePlaylistRequest[] {
    if (vs) {
        return vs.map((v: CreatePlaylistRequest) => {
            return safeCreatePlaylistRequest(v);
        });
    } else {
        return [];
    }
}

export interface CreatePlaylistResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 如果创建成功，返回播放列表的ID。
    */
    playlistIDs?: string[];

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeCreatePlaylistResponse(v: CreatePlaylistResponse): CreatePlaylistResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.playlistIDs = v.playlistIDs || [];
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeCreatePlaylistResponses(vs: CreatePlaylistResponse[]): CreatePlaylistResponse[] {
    if (vs) {
        return vs.map((v: CreatePlaylistResponse) => {
            return safeCreatePlaylistResponse(v);
        });
    } else {
        return [];
    }
}

export interface UpdatePlaylistRequest {

    /**
    * 待更新的播放列表。
    */
    playlists: Playlist[];

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeUpdatePlaylistRequest(v: UpdatePlaylistRequest): UpdatePlaylistRequest {
    if (v) {
        v.playlists = safePlaylists(v.playlists);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeUpdatePlaylistRequests(vs: UpdatePlaylistRequest[]): UpdatePlaylistRequest[] {
    if (vs) {
        return vs.map((v: UpdatePlaylistRequest) => {
            return safeUpdatePlaylistRequest(v);
        });
    } else {
        return [];
    }
}

export interface UpdatePlaylistResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeUpdatePlaylistResponse(v: UpdatePlaylistResponse): UpdatePlaylistResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeUpdatePlaylistResponses(vs: UpdatePlaylistResponse[]): UpdatePlaylistResponse[] {
    if (vs) {
        return vs.map((v: UpdatePlaylistResponse) => {
            return safeUpdatePlaylistResponse(v);
        });
    } else {
        return [];
    }
}

export interface DeletePlaylistRequest {

    /**
    * 待删除的播放列表ID，如果不携带，表示删除所有播放列表。
    */
    playlistIDs?: string[];

    /**
    * 删除类型。
    *
    * 0 : 按播放列表ID删除，根据传入的播放列表ID删除播放列表
    *
    * 1 : 按播放列表ID反选删除，保留传入的播放列表ID，其他的播放列表数据全部删除
    *
    * 此字段默认为0。
    */
    deleteType?: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeDeletePlaylistRequest(v: DeletePlaylistRequest): DeletePlaylistRequest {
    if (v) {
        v.playlistIDs = v.playlistIDs || [];
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeDeletePlaylistRequests(vs: DeletePlaylistRequest[]): DeletePlaylistRequest[] {
    if (vs) {
        return vs.map((v: DeletePlaylistRequest) => {
            return safeDeletePlaylistRequest(v);
        });
    } else {
        return [];
    }
}

export interface DeletePlaylistResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeDeletePlaylistResponse(v: DeletePlaylistResponse): DeletePlaylistResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeDeletePlaylistResponses(vs: DeletePlaylistResponse[]): DeletePlaylistResponse[] {
    if (vs) {
        return vs.map((v: DeletePlaylistResponse) => {
            return safeDeletePlaylistResponse(v);
        });
    } else {
        return [];
    }
}

export interface QueryPlaylistRequest {

    /**
    * 一次查询的总条数，最大条数默认不超过50，最大条数可配置，超过最大条数返回错误。
    */
    count: string;

    /**
    * 查询的起始位置，从0开始（即0表示第一个）。
    */
    offset: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeQueryPlaylistRequest(v: QueryPlaylistRequest): QueryPlaylistRequest {
    if (v) {
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeQueryPlaylistRequests(vs: QueryPlaylistRequest[]): QueryPlaylistRequest[] {
    if (vs) {
        return vs.map((v: QueryPlaylistRequest) => {
            return safeQueryPlaylistRequest(v);
        });
    } else {
        return [];
    }
}

export interface QueryPlaylistResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 总记录数。
    */
    total?: string;

    /**
    * 如果查询成功，返回所有的播放列表，VSP默认按播放列表的更新时间升序排列。
    */
    playlists?: Playlist[];

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeQueryPlaylistResponse(v: QueryPlaylistResponse): QueryPlaylistResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.playlists = safePlaylists(v.playlists);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeQueryPlaylistResponses(vs: QueryPlaylistResponse[]): QueryPlaylistResponse[] {
    if (vs) {
        return vs.map((v: QueryPlaylistResponse) => {
            return safeQueryPlaylistResponse(v);
        });
    } else {
        return [];
    }
}

export interface AddPlaylistContentRequest {

    /**
    * 待添加的播放列表内容，支持在多个播放列表里增加内容。
    */
    contents: PlaylistContent[];

    /**
    * 当添加的内容到达上限，是否支持自动覆盖最早添加的收藏，取值包括：
    *
    * 0：不支持
    *
    * 1：支持
    *
    * 默认值为1。
    */
    autoCover?: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeAddPlaylistContentRequest(v: AddPlaylistContentRequest): AddPlaylistContentRequest {
    if (v) {
        v.contents = safePlaylistContents(v.contents);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeAddPlaylistContentRequests(vs: AddPlaylistContentRequest[]): AddPlaylistContentRequest[] {
    if (vs) {
        return vs.map((v: AddPlaylistContentRequest) => {
            return safeAddPlaylistContentRequest(v);
        });
    } else {
        return [];
    }
}

export interface AddPlaylistContentResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeAddPlaylistContentResponse(v: AddPlaylistContentResponse): AddPlaylistContentResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeAddPlaylistContentResponses(vs: AddPlaylistContentResponse[]): AddPlaylistContentResponse[] {
    if (vs) {
        return vs.map((v: AddPlaylistContentResponse) => {
            return safeAddPlaylistContentResponse(v);
        });
    } else {
        return [];
    }
}

export interface SortPlaylistContentRequest {

    /**
    * 已排序好的播放列表内容，只支持对一个播放列表里的内容进行全量排序。
    */
    contents: PlaylistContent[];

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeSortPlaylistContentRequest(v: SortPlaylistContentRequest): SortPlaylistContentRequest {
    if (v) {
        v.contents = safePlaylistContents(v.contents);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeSortPlaylistContentRequests(vs: SortPlaylistContentRequest[]): SortPlaylistContentRequest[] {
    if (vs) {
        return vs.map((v: SortPlaylistContentRequest) => {
            return safeSortPlaylistContentRequest(v);
        });
    } else {
        return [];
    }
}

export interface SortPlaylistContentResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeSortPlaylistContentResponse(v: SortPlaylistContentResponse): SortPlaylistContentResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeSortPlaylistContentResponses(vs: SortPlaylistContentResponse[]): SortPlaylistContentResponse[] {
    if (vs) {
        return vs.map((v: SortPlaylistContentResponse) => {
            return safeSortPlaylistContentResponse(v);
        });
    } else {
        return [];
    }
}

export interface DeletePlaylistContentRequest {

    /**
    * 播放列表ID。
    */
    playlistID: string;

    /**
    * 删除指定内容类型的记录，枚举值参考PlaylistContent对象的contentType。
    */
    contentTypes?: string[];

    /**
    * 删除指定内容ID的记录。
    *
    * 说明
    *
    * 该参数必须和contenTypes同时出现，而且每个数组元素是1对1的关系。
    */
    contentIDs?: string[];

    /**
    * 删除类型。
    *
    * 0 : 按内容ID删除，根据传入的内容ID删除播放列表中的内容
    *
    * 1 : 按内容ID反选删除，播放列表中保留传入的内容ID，播放列表中的其他的内容数据全部删除
    *
    * 此字段默认为0。
    */
    deleteType?: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeDeletePlaylistContentRequest(v: DeletePlaylistContentRequest): DeletePlaylistContentRequest {
    if (v) {
        v.contentTypes = v.contentTypes || [];
        v.contentIDs = v.contentIDs || [];
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeDeletePlaylistContentRequests(vs: DeletePlaylistContentRequest[]): DeletePlaylistContentRequest[] {
    if (vs) {
        return vs.map((v: DeletePlaylistContentRequest) => {
            return safeDeletePlaylistContentRequest(v);
        });
    } else {
        return [];
    }
}

export interface DeletePlaylistContentResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeDeletePlaylistContentResponse(v: DeletePlaylistContentResponse): DeletePlaylistContentResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeDeletePlaylistContentResponses(vs: DeletePlaylistContentResponse[]): DeletePlaylistContentResponse[] {
    if (vs) {
        return vs.map((v: DeletePlaylistContentResponse) => {
            return safeDeletePlaylistContentResponse(v);
        });
    } else {
        return [];
    }
}

export interface QueryPlaylistContentRequest {

    /**
    * 播放列表ID。
    */
    playlistID: string;

    /**
    * 一次查询的总条数，最大条数默认不超过50，最大条数可配置，超过最大条数返回错误。
    */
    count: string;

    /**
    * 查询的起始位置，从0开始（即0表示第一个）。
    */
    offset: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeQueryPlaylistContentRequest(v: QueryPlaylistContentRequest): QueryPlaylistContentRequest {
    if (v) {
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeQueryPlaylistContentRequests(vs: QueryPlaylistContentRequest[]): QueryPlaylistContentRequest[] {
    if (vs) {
        return vs.map((v: QueryPlaylistContentRequest) => {
            return safeQueryPlaylistContentRequest(v);
        });
    } else {
        return [];
    }
}

export interface QueryPlaylistContentResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 总记录数。
    */
    total?: string;

    /**
    * 如果查询成功，返回个性化播放内容信息。
    */
    contents?: Content[];

    /**
    * 此VOD如果为连续剧子集，则此处记录添加内容时的连续剧父集ID
    *
    * 为保证对应关系，此数组与contents字段的数组长度保持一致。如果contents中某个内容添加时未录入相应的父集ID，则seriesIDs数组中对应位置的元素以空字符串填充
    */
    seriesIDs?: string[];

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeQueryPlaylistContentResponse(v: QueryPlaylistContentResponse): QueryPlaylistContentResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.contents = safeContents(v.contents);
        v.seriesIDs = v.seriesIDs || [];
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeQueryPlaylistContentResponses(vs: QueryPlaylistContentResponse[]): QueryPlaylistContentResponse[] {
    if (vs) {
        return vs.map((v: QueryPlaylistContentResponse) => {
            return safeQueryPlaylistContentResponse(v);
        });
    } else {
        return [];
    }
}

export interface CreateReminderRequest {

    /**
    * 用户待创建的内容提醒。
    */
    reminders: Reminder[];

    /**
    * 个人频道号设置的维度：
    *
    * 0：设置到UserProfile上。
    *
    * 1：设置到用户设备上。
    *
    * 默认值为0。
    *
    * 注：C10版本中该字段将忽略，后台直接使用子网运营商的配置进行处理。
    */
    setType?: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeCreateReminderRequest(v: CreateReminderRequest): CreateReminderRequest {
    if (v) {
        v.reminders = safeReminders(v.reminders);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeCreateReminderRequests(vs: CreateReminderRequest[]): CreateReminderRequest[] {
    if (vs) {
        return vs.map((v: CreateReminderRequest) => {
            return safeCreateReminderRequest(v);
        });
    } else {
        return [];
    }
}

export interface CreateReminderResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeCreateReminderResponse(v: CreateReminderResponse): CreateReminderResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeCreateReminderResponses(vs: CreateReminderResponse[]): CreateReminderResponse[] {
    if (vs) {
        return vs.map((v: CreateReminderResponse) => {
            return safeCreateReminderResponse(v);
        });
    } else {
        return [];
    }
}

export interface DeleteReminderRequest {

    /**
    * 个人频道号设置的维度：
    *
    * 0：设置到UserProfile上。
    *
    * 1：设置到用户设备上。
    *
    * 默认值为0。
    *
    * 注：C10版本中该字段将忽略，后台直接使用子网运营商的配置进行处理。
    */
    setType?: string;

    /**
    * 删除指定内容类型的提醒，如果不指定，将删除所有提醒。
    *
    * 枚举值参考Reminder对象的contentType。
    */
    contentTypes?: string[];

    /**
    * 删除指定内容ID的提醒。
    *
    * 备注：
    *
    * 1.该参数必须和contenTypes同时出现，而且每个数组元素是1对1的关系。
    *
    * 2.如果不指定，将删除指定内容类型所有的提醒。
    */
    contentIDs?: string[];

    /**
    * 删除类型。
    *
    * 0 : 按提醒内容ID删除，根据传入的提醒内容ID删除提醒数据
    *
    * 1 : 按提醒内容ID反选删除，提醒数据列表中保留传入的提醒内容ID，内容的提醒数据全部删除
    *
    * 此字段默认为0。
    */
    deleteType?: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeDeleteReminderRequest(v: DeleteReminderRequest): DeleteReminderRequest {
    if (v) {
        v.contentTypes = v.contentTypes || [];
        v.contentIDs = v.contentIDs || [];
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeDeleteReminderRequests(vs: DeleteReminderRequest[]): DeleteReminderRequest[] {
    if (vs) {
        return vs.map((v: DeleteReminderRequest) => {
            return safeDeleteReminderRequest(v);
        });
    } else {
        return [];
    }
}

export interface DeleteReminderResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeDeleteReminderResponse(v: DeleteReminderResponse): DeleteReminderResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeDeleteReminderResponses(vs: DeleteReminderResponse[]): DeleteReminderResponse[] {
    if (vs) {
        return vs.map((v: DeleteReminderResponse) => {
            return safeDeleteReminderResponse(v);
        });
    } else {
        return [];
    }
}

export interface QueryReminderRequest {

    /**
    * 个人频道号设置的维度：
    *
    * 0：查询UserProfile上的提醒。
    *
    * 1：查询用户设备上的提醒。
    *
    * 默认值为0。
    *
    * 注：C10版本中该字段将忽略，后台直接使用子网运营商的配置进行处理。
    */
    queryType?: string;

    /**
    * 内容类型,取值包括：
    *
    * PROGRAM
    *
    * VOD
    *
    * 如果该字段传值为空，默认只查节目单
    *
    * [V6R1C20未实现]
    */
    contentType?: string;

    /**
    * 一次查询的总条数，最大条数默认不超过50，最大条数可配置，超过最大条数返回错误。
    */
    count: string;

    /**
    * 查询的起始位置。从0开始
    */
    offset: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeQueryReminderRequest(v: QueryReminderRequest): QueryReminderRequest {
    if (v) {
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeQueryReminderRequests(vs: QueryReminderRequest[]): QueryReminderRequest[] {
    if (vs) {
        return vs.map((v: QueryReminderRequest) => {
            return safeQueryReminderRequest(v);
        });
    } else {
        return [];
    }
}

export interface QueryReminderResponse {

    /**
    * 返回结果
    */
    result: Result;

    /**
    * 总记录数。
    */
    total?: string;

    /**
    * 如果查询成功，返回提醒版本号。
    */
    version?: string;

    /**
    * 如果查询成功，返回个性化节目提醒信息。
    */
    reminders?: Content[];

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeQueryReminderResponse(v: QueryReminderResponse): QueryReminderResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.reminders = safeContents(v.reminders);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeQueryReminderResponses(vs: QueryReminderResponse[]): QueryReminderResponse[] {
    if (vs) {
        return vs.map((v: QueryReminderResponse) => {
            return safeQueryReminderResponse(v);
        });
    } else {
        return [];
    }
}

export interface QueryLauncherRequest {

    /**
    * 本地桌面版本号(对应于Launcher上次从PHS平台下载的桌面的更新时间)，由于首次获取时，未必有值，因此可空。
    */
    version?: string;

    /**
    * 桌面类型。
    *
    * 0：TV EPG
    *
    * 1：Android Launcher
    *
    * 2：Pad
    *
    * 3：Phone
    *
    * 默认值：0
    *
    * 说明：
    *
    * 兼容性考虑保留该入参，后续版本逐步废弃。
    *
    * 不推荐使用deviceModel入参。推荐使用desktopType入参。
    *
    * deviceModel和desktopType必须至少有一个传值。若两个都传，则以desktopType为准。
    */
    deviceModel?: string;

    /**
    * 终端请求的桌面类型。
    *
    * 0：TV EPG
    *
    * 1：Android Launcher
    *
    * 2：Pad
    *
    * 3：Phone
    *
    * 说明：
    *
    * deviceModel和desktopType必须至少有一个传值。若两个都传，则以desktopType为准。
    */
    desktopType?: string;

    /**
    * 对接华为平台终端，该字段表示用户Token。
    *
    * 对接第三方平台的终端时，该字段表示厂商ID
    *
    * 取值分别为：
    *
    * 0：HW
    *
    * 1：ZTE
    *
    * 2：商城
    */
    userToken: string;

    /**
    * 扩展信息。
    *
    * 对接第三方平台的终端时，根据局点运营需要在扩展信息中加入如下属性areaId、spId、groupId、subnetId、packageId，deviceModel。含义分别如下：
    *
    * subnetId：用户归属的子网运营商ID信息；
    *
    * deviceModel：第三方平台终端的型号信息；
    *
    * groupId：第三方平台终端的用户分组信息；
    *
    * areaId：第三方平台终端所属的区域信息；
    *
    * spId：第三方平台所属的sp信息；
    *
    * packageId：第三方平台所属的产品包信息；
    */
    extensionFields?: NamedParameter[];
}

function safeQueryLauncherRequest(v: QueryLauncherRequest): QueryLauncherRequest {
    if (v) {
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeQueryLauncherRequests(vs: QueryLauncherRequest[]): QueryLauncherRequest[] {
    if (vs) {
        return vs.map((v: QueryLauncherRequest) => {
            return safeQueryLauncherRequest(v);
        });
    } else {
        return [];
    }
}

export interface QueryLauncherResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 桌面描述文件下载链接相对地址（终端拼装请求桌面模板描述文件的访问绝对路径地址:http://VSP address/VSP/桌面描述文件返回相对目录，Launcher在后面拼接描述文件，描述文件名默认为Launcher.json）。
    *
    * 成功匹配到时，该参数必填。
    */
    launcherLink?: string;

    /**
    * 桌面描述文件下载链接相对地址（终端拼装请求桌面模板描述文件的访问绝对路径地址:https://VSP address/VSP/桌面描述文件返回相对目录，Launcher在后面拼接描述文件，描述文件名默认为Launcher.json）。
    *
    * 成功匹配到时，该参数必填。
    */
    launcherLinkHttps?: string;

    /**
    * 当前桌面的最后修改时间。
    *
    * 成功匹配到时，该参数必填。
    */
    version?: string;

    /**
    * 匹配后桌面的ID，用于后续终端检查该桌面的最新数据。
    *
    * 成功匹配到时，该参数必填。
    */
    desktopID?: string;

    /**
    * 终端定期到平台获取资源为数据的时间间隔，单位秒。
    */
    interval?: string;

    /**
    * Launcher向PHS批量获取TV屏的资源位数据接口地址。
    *
    * PHS返回相对地址：/VSP/V3/BatchGetResStrategyData，桌面需要拼接IP/Port前缀，得到绝对地址。
    */
    batchGetResStrategyDataURL?: string;

    /**
    * 该字段表示的是接口“刷新数据变化的资源位ID列表”的访问URL。
    *
    * Launcher通过该接口向PHS查询哪些资源位的数据发生了变化。
    *
    * 格式如：/VSP/V3/GetLatestResources，Launcher需要拼接IP/Port前缀，得到绝对地址。
    */
    getLatestResourcesURL?: string;

    /**
    * 平台匹配到桌面时，该桌面资源位策略数据的版本号。
    */
    currentResourcesVersion?: string;

    /**
    * 提供给终端的获取专题数据接口。
    *
    * PHS返回相对地址，如果对应大颗粒接口，格式为：/VSP/V3/GetTopics，客户端需要拼接IP/Port前缀，得到绝对地址。
    */
    getTopicUrl?: string;

    /**
    * 返回缓存中智能桌面对象的扩展信息，该信息有PHM门户进行配置，通过VSP平台透传给终端
    */
    extensionInfo?: NamedParameter[];

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeQueryLauncherResponse(v: QueryLauncherResponse): QueryLauncherResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.extensionInfo = safeNamedParameters(v.extensionInfo);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeQueryLauncherResponses(vs: QueryLauncherResponse[]): QueryLauncherResponse[] {
    if (vs) {
        return vs.map((v: QueryLauncherResponse) => {
            return safeQueryLauncherResponse(v);
        });
    } else {
        return [];
    }
}

export interface BatchGetResStrategyDataRequest {

    /**
    * 资源位ID。
    */
    resourceIDs: string[];

    /**
    * 用户Token。
    *
    * 对接华为平台终端，该字段表示用户Token。
    *
    * 对接第三方平台的终端时，该字段表示厂商ID
    *
    * 取值分别为：
    *
    * 0：HW
    *
    * 1：ZTE
    *
    * 2：商城
    */
    userToken: string;

    /**
    * 扩展信息。
    *
    * 对接第三方平台的终端时，根据局点运营需要在扩展信息中加入如下属性areaId、spId、groupId、subnetId、packageId，deviceModel。含义分别如下：
    *
    * subnetId：用户归属的子网运营商ID信息；
    *
    * deviceModel：第三方平台终端的型号信息；
    *
    * groupId：第三方平台终端的用户分组信息；
    *
    * areaId：第三方平台终端所属的区域信息；
    *
    * spId：第三方平台所属的sp信息；
    *
    * packageId：第三方平台所属的产品包信息；
    */
    extensionFields?: NamedParameter[];
}

function safeBatchGetResStrategyDataRequest(v: BatchGetResStrategyDataRequest): BatchGetResStrategyDataRequest {
    if (v) {
        v.resourceIDs = v.resourceIDs || [];
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeBatchGetResStrategyDataRequests(vs: BatchGetResStrategyDataRequest[]): BatchGetResStrategyDataRequest[] {
    if (vs) {
        return vs.map((v: BatchGetResStrategyDataRequest) => {
            return safeBatchGetResStrategyDataRequest(v);
        });
    } else {
        return [];
    }
}

export interface BatchGetResStrategyDataResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 各资源位数据。
    */
    resourceDatas?: StrategyData[];

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeBatchGetResStrategyDataResponse(v: BatchGetResStrategyDataResponse): BatchGetResStrategyDataResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.resourceDatas = safeStrategyDatas(v.resourceDatas);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeBatchGetResStrategyDataResponses(vs: BatchGetResStrategyDataResponse[]): BatchGetResStrategyDataResponse[] {
    if (vs) {
        return vs.map((v: BatchGetResStrategyDataResponse) => {
            return safeBatchGetResStrategyDataResponse(v);
        });
    } else {
        return [];
    }
}

export interface GetLatestResourcesRequest {

    /**
    * Launcher获取资源位数据的最新一次请求的时间。
    *
    * 首次请求时，此字段来自于获取桌面描述文件时平台返回的currentResourcesVersion字段。
    */
    version: string;

    /**
    * 桌面标识。
    */
    desktopID: string;

    /**
    * 用户Token。
    *
    * 对接华为平台终端，该字段表示用户Token。
    *
    * 对接第三方平台的终端时，该字段表示厂商ID
    *
    * 取值分别为：
    *
    * 0：HW
    *
    * 1：ZTE
    *
    * 2：商城
    */
    userToken: string;

    /**
    * 扩展信息。
    *
    * 对接第三方平台的终端时，根据局点运营需要在扩展信息中加入如下属性areaId、spId、groupId、subnetId、packageId，deviceModel。含义分别如下：
    *
    * subnetId：用户归属的子网运营商ID信息；
    *
    * deviceModel：第三方平台终端的型号信息；
    *
    * groupId：第三方平台终端的用户分组信息；
    *
    * areaId：第三方平台终端所属的区域信息；
    *
    * spId：第三方平台所属的sp信息；
    *
    * packageId：第三方平台所属的产品包信息；
    */
    extensionFields?: NamedParameter[];
}

function safeGetLatestResourcesRequest(v: GetLatestResourcesRequest): GetLatestResourcesRequest {
    if (v) {
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeGetLatestResourcesRequests(vs: GetLatestResourcesRequest[]): GetLatestResourcesRequest[] {
    if (vs) {
        return vs.map((v: GetLatestResourcesRequest) => {
            return safeGetLatestResourcesRequest(v);
        });
    } else {
        return [];
    }
}

export interface GetLatestResourcesResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 该桌面上最新配置的策略匹配数据的最后更新时间，如果平台侧检测资源位未发生变化，则此字段不返回，终端下次请求时继续沿用以前的资源位版本号。
    */
    version?: string;

    /**
    * 哪些资源位的数据发生变化。
    */
    resourceIDs?: string[];

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeGetLatestResourcesResponse(v: GetLatestResourcesResponse): GetLatestResourcesResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.resourceIDs = v.resourceIDs || [];
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeGetLatestResourcesResponses(vs: GetLatestResourcesResponse[]): GetLatestResourcesResponse[] {
    if (vs) {
        return vs.map((v: GetLatestResourcesResponse) => {
            return safeGetLatestResourcesResponse(v);
        });
    } else {
        return [];
    }
}

export interface jumpURLjspRequest {

    /**
    * 内容CODE（当Action&#x3D; SmallScreen或者FullScreen时必填）。
    */
    contentCode?: string;

    /**
    * 内容类型（当Action&#x3D; SmallScreen或者FullScreen时必填）。
    */
    contentType?: string;

    /**
    * 第三方中转URL（当Action&#x3D;Link时必填）。
    */
    linkURL?: string;

    /**
    * SmallScreen：表示返回播放地址；
    *
    * FullScreen：表示需要进行HTTP 302重定向，由模板启动播放器；
    *
    * View：表示进入详情；
    *
    * Link：表示向第三方系统进行跳转
    *
    * 具体根据PHM与PHS间接口文档里面的约定。
    */
    action: string;

    /**
    * 用户Token。
    */
    userToken: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safejumpURLjspRequest(v: jumpURLjspRequest): jumpURLjspRequest {
    if (v) {
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safejumpURLjspRequests(vs: jumpURLjspRequest[]): jumpURLjspRequest[] {
    if (vs) {
        return vs.map((v: jumpURLjspRequest) => {
            return safejumpURLjspRequest(v);
        });
    } else {
        return [];
    }
}

export interface jumpURLjspResponse {

    /**
    * 播放URL，没有找到内容时，返回0长度的字符串&quot;&quot;。
    *
    * 格式如：{&quot;playUrl&quot;:&quot;igmp:x.x.x.x:8099&quot;}
    *
    * 或者格式是{&quot;playUrl&quot;:&quot;rtsp://ip/888888/20151120/24390018.ts?k1&#x3D;v1&amp;k2&#x3D;v2&amp;k3&#x3D;v3…&quot;}。
    */
    playURL?: string;

    /**
    * 跳转URL，类似于Launcher隐藏，调用终端浏览器进入浏览器二级页面（VOD详情、公告等）。
    *
    * 格式是{&quot;redirectURL&quot;:&quot;http://ip:port/xxxxx/index.php?k1&#x3D;v1&amp;k2&#x3D;v2&amp;k3&#x3D;v3…&quot;}
    *
    * 说明
    *
    * redirectURL与playURL只能二选一的出现，如果同时返回则终端会出问题。
    */
    redirectURL?: string;
}

function safejumpURLjspResponse(v: jumpURLjspResponse): jumpURLjspResponse {
    if (v) {
    }
    return v;
}

function safejumpURLjspResponses(vs: jumpURLjspResponse[]): jumpURLjspResponse[] {
    if (vs) {
        return vs.map((v: jumpURLjspResponse) => {
            return safejumpURLjspResponse(v);
        });
    } else {
        return [];
    }
}

export interface GetTopicsRequest {

    /**
    * 终端获取专题数据信息的最新一次请求的时间，时间的毫秒数格式。
    */
    version: string;

    /**
    * 终端请求的桌面类型。
    *
    * 0：TV EPG
    *
    * 1：Android Launcher
    *
    * 2：Pad
    *
    * 3：Phone
    *
    * 约束：
    *
    * 不支持在Android Launcher终端发布专题。
    */
    desktopType: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeGetTopicsRequest(v: GetTopicsRequest): GetTopicsRequest {
    if (v) {
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeGetTopicsRequests(vs: GetTopicsRequest[]): GetTopicsRequest[] {
    if (vs) {
        return vs.map((v: GetTopicsRequest) => {
            return safeGetTopicsRequest(v);
        });
    } else {
        return [];
    }
}

export interface GetTopicsResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 终端下次专题请求的时间间隔周期，默认为30分钟；
    */
    nextIntervalPeriod: string;

    /**
    * 该桌面上指定终端型号最新变更的专题数据的最后更新时间，如果平台侧检测专题信息未发生变化，则返回入参version字段，终端下次请求时继续沿用以前的专题版本号，时间的毫秒数格式。
    */
    version?: string;

    /**
    * 返回指定终端类型下全量生效的专题信息列表；
    */
    topics?: Topic[];

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeGetTopicsResponse(v: GetTopicsResponse): GetTopicsResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.topics = safeTopics(v.topics);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeGetTopicsResponses(vs: GetTopicsResponse[]): GetTopicsResponse[] {
    if (vs) {
        return vs.map((v: GetTopicsResponse) => {
            return safeGetTopicsResponse(v);
        });
    } else {
        return [];
    }
}

export interface SubmitDeviceInfoRequest {

    /**
    * 设备类型，取值包括：
    *
    * IOS：iOS系统设备
    *
    * ANDROID：android系统设备
    *
    * WP：Windows Phone系统设备（后续扩展用）
    *
    * HUAWEI：华为自建推送通道
    *
    * Push server根据该字段，建立不同的消息通道。比如IOS，则建立APNS消息通道。
    */
    deviceType: string;

    /**
    * 如果是使用第三方消息推送平台，则填写第三方消息推送平台（APNS/GCM/FCM/MPNS）的token
    *
    * 如果是IMP平台，则填写XMPP的JID
    */
    deviceToken: string;

    /**
    * 设备应用ID，对iOS设备有效。
    */
    terminalAppID?: string;

    /**
    * 从PUSH Server获取的XMPP token的过期时间，取值为距离1970年1月1号的毫秒数。
    */
    tokenExpireTime?: string;

    /**
    * 从第三方消息服务系统获取的备用Token，对于代理模式，deviceToken表示第三方系统分配的主用Token,两者用途不同。
    */
    relayDeviceToken?: string;

    /**
    * 备用设备类型参数，和relayDeviceToken成对出现。
    */
    relayDeviceType?: string;
}

function safeSubmitDeviceInfoRequest(v: SubmitDeviceInfoRequest): SubmitDeviceInfoRequest {
    if (v) {
    }
    return v;
}

function safeSubmitDeviceInfoRequests(vs: SubmitDeviceInfoRequest[]): SubmitDeviceInfoRequest[] {
    if (vs) {
        return vs.map((v: SubmitDeviceInfoRequest) => {
            return safeSubmitDeviceInfoRequest(v);
        });
    } else {
        return [];
    }
}

export interface SubmitDeviceInfoResponse {

    /**
    * 返回结果。
    */
    result: Result;
}

function safeSubmitDeviceInfoResponse(v: SubmitDeviceInfoResponse): SubmitDeviceInfoResponse {
    if (v) {
        v.result = safeResult(v.result);
    }
    return v;
}

function safeSubmitDeviceInfoResponses(vs: SubmitDeviceInfoResponse[]): SubmitDeviceInfoResponse[] {
    if (vs) {
        return vs.map((v: SubmitDeviceInfoResponse) => {
            return safeSubmitDeviceInfoResponse(v);
        });
    } else {
        return [];
    }
}

export interface PushMsgByTagRequest {

    /**
    * 表示当前设备的Token
    *
    * 如果是使用第三方消息推送平台，则填写第三方消息推送平台（APNS/GCM/FCM/MPNS）的token
    *
    * 如果是IMP平台，则填写XMPP的JID。
    *
    * JID格式为：NodeIdentifier@Domain，其中NodeIdentifier为用户的IMP账号，默认为profileID，Domain为IMP服务域名，从VSP配置的IMDomain参数获取。
    */
    deviceToken: string;

    /**
    * 平台预定义好的消息标签，只支持开机消息，tag取值约定为“startup”。
    */
    tag: string;
}

function safePushMsgByTagRequest(v: PushMsgByTagRequest): PushMsgByTagRequest {
    if (v) {
    }
    return v;
}

function safePushMsgByTagRequests(vs: PushMsgByTagRequest[]): PushMsgByTagRequest[] {
    if (vs) {
        return vs.map((v: PushMsgByTagRequest) => {
            return safePushMsgByTagRequest(v);
        });
    } else {
        return [];
    }
}

export interface PushMsgByTagResponse {

    /**
    * 返回结果。
    */
    result: Result;
}

function safePushMsgByTagResponse(v: PushMsgByTagResponse): PushMsgByTagResponse {
    if (v) {
        v.result = safeResult(v.result);
    }
    return v;
}

function safePushMsgByTagResponses(vs: PushMsgByTagResponse[]): PushMsgByTagResponse[] {
    if (vs) {
        return vs.map((v: PushMsgByTagResponse) => {
            return safePushMsgByTagResponse(v);
        });
    } else {
        return [];
    }
}

export interface DownloadNPVRRequest {

    /**
    * 录制计划ID。
    */
    planID: string;

    /**
    * 如果录制计划下有多个文件，选择一个文件播放。
    */
    fileID: string;

    /**
    * 检查PVR是否被加锁和父母字控制，如果加锁或父母字控制，需要进行密码校验。
    */
    checkLock?: CheckLock;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeDownloadNPVRRequest(v: DownloadNPVRRequest): DownloadNPVRRequest {
    if (v) {
        v.checkLock = safeCheckLock(v.checkLock);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeDownloadNPVRRequests(vs: DownloadNPVRRequest[]): DownloadNPVRRequest[] {
    if (vs) {
        return vs.map((v: DownloadNPVRRequest) => {
            return safeDownloadNPVRRequest(v);
        });
    } else {
        return [];
    }
}

export interface DownloadNPVRResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 鉴权结果。
    */
    authorizeResult: AuthorizeResult;

    /**
    * 如果鉴权成功，返回下载地址。
    */
    downloadURL?: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeDownloadNPVRResponse(v: DownloadNPVRResponse): DownloadNPVRResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.authorizeResult = safeAuthorizeResult(v.authorizeResult);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeDownloadNPVRResponses(vs: DownloadNPVRResponse[]): DownloadNPVRResponse[] {
    if (vs) {
        return vs.map((v: DownloadNPVRResponse) => {
            return safeDownloadNPVRResponse(v);
        });
    } else {
        return [];
    }
}

export interface ReportPVRRequest {

    /**
    * 录制计划ID。
    */
    planID: string;

    /**
    * 如果录制计划下有多个文件，选择一个文件播放。
    */
    fileID: string;

    /**
    * 终端行为，取值包括：
    *
    * 0：开始播放
    *
    * 1：退出播放
    */
    action: string;

    /**
    * 如果播放NPVR，是否播放本地已下载的NPVR，取值包括：
    *
    * 0：否
    *
    * 1：是
    *
    * 默认值是0。
    */
    isDownload?: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeReportPVRRequest(v: ReportPVRRequest): ReportPVRRequest {
    if (v) {
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeReportPVRRequests(vs: ReportPVRRequest[]): ReportPVRRequest[] {
    if (vs) {
        return vs.map((v: ReportPVRRequest) => {
            return safeReportPVRRequest(v);
        });
    } else {
        return [];
    }
}

export interface ReportPVRResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeReportPVRResponse(v: ReportPVRResponse): ReportPVRResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeReportPVRResponses(vs: ReportPVRResponse[]): ReportPVRResponse[] {
    if (vs) {
        return vs.map((v: ReportPVRResponse) => {
            return safeReportPVRResponse(v);
        });
    } else {
        return [];
    }
}

export interface PlayNPVRHeartbeatRequest {

    /**
    * 录制计划ID。
    */
    planID: string;

    /**
    * 如果录制计划下有多个文件，选择一个文件播放。
    */
    fileID: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safePlayNPVRHeartbeatRequest(v: PlayNPVRHeartbeatRequest): PlayNPVRHeartbeatRequest {
    if (v) {
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safePlayNPVRHeartbeatRequests(vs: PlayNPVRHeartbeatRequest[]): PlayNPVRHeartbeatRequest[] {
    if (vs) {
        return vs.map((v: PlayNPVRHeartbeatRequest) => {
            return safePlayNPVRHeartbeatRequest(v);
        });
    } else {
        return [];
    }
}

export interface PlayNPVRHeartbeatResponse {

    /**
    * 返回码。
    */
    result: Result;

    /**
    * 播放是否合法，取值包括
    *
    * 0：非法
    *
    * 1：合法
    */
    isValid: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safePlayNPVRHeartbeatResponse(v: PlayNPVRHeartbeatResponse): PlayNPVRHeartbeatResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safePlayNPVRHeartbeatResponses(vs: PlayNPVRHeartbeatResponse[]): PlayNPVRHeartbeatResponse[] {
    if (vs) {
        return vs.map((v: PlayNPVRHeartbeatResponse) => {
            return safePlayNPVRHeartbeatResponse(v);
        });
    } else {
        return [];
    }
}

export interface AddPVRRequest {

    /**
    * 待添加PVR录制任务，支持传入TimeBasedPVR或 PlaybillBasedPVR数据类型。
    */
    PVR: PVRTaskBasic;

    /**
    * 添加CPVR或混合录制任务时，如果需要平台检测空间，则需要传diskInfo。
    */
    diskInfo?: DiskInfo;
}

function safeAddPVRRequest(v: AddPVRRequest): AddPVRRequest {
    if (v) {
        v.PVR = safePVRTaskBasic(v.PVR);
        v.diskInfo = safeDiskInfo(v.diskInfo);
    }
    return v;
}

function safeAddPVRRequests(vs: AddPVRRequest[]): AddPVRRequest[] {
    if (vs) {
        return vs.map((v: AddPVRRequest) => {
            return safeAddPVRRequest(v);
        });
    } else {
        return [];
    }
}

export interface AddPVRResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 创建的时间段或节目单录制的ID。
    *
    * 添加时若检测到重复的节目单录制，则返回这个以存在的录制任务ID。
    */
    PVRID?: string;

    /**
    * NPVR空间是否足够，取值：
    *
    * 0：空间不够
    *
    * 1：空间足够
    */
    isNPVRSpaceEnough?: string;

    /**
    * CPVR空间是否足够，取值：
    *
    * 0：空间不够
    *
    * 1：空间足够
    */
    isCPVRSpaceEnough?: string;

    /**
    * CPVR冲突分组列表，在有CPVR冲突时返回。
    */
    conflictGroups?: ConflictPVRGroup[];

    /**
    * 如果新增CPVR任务成功，返回Master STB的设备信息。
    */
    masterSTB?: Device;
}

function safeAddPVRResponse(v: AddPVRResponse): AddPVRResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.conflictGroups = safeConflictPVRGroups(v.conflictGroups);
        v.masterSTB = safeDevice(v.masterSTB);
    }
    return v;
}

function safeAddPVRResponses(vs: AddPVRResponse[]): AddPVRResponse[] {
    if (vs) {
        return vs.map((v: AddPVRResponse) => {
            return safeAddPVRResponse(v);
        });
    } else {
        return [];
    }
}

export interface UpdatePVRRequest {

    /**
    * 待更新的PVR录制任务，传入TimeBasedPVR或 PlaybillBasedPVR
    */
    PVR: PVRTaskBasic;

    /**
    * 更新CPVR或混合录制任务时，如果需要平台检测空间，则需要传diskInfo
    */
    diskInfo?: DiskInfo;
}

function safeUpdatePVRRequest(v: UpdatePVRRequest): UpdatePVRRequest {
    if (v) {
        v.PVR = safePVRTaskBasic(v.PVR);
        v.diskInfo = safeDiskInfo(v.diskInfo);
    }
    return v;
}

function safeUpdatePVRRequests(vs: UpdatePVRRequest[]): UpdatePVRRequest[] {
    if (vs) {
        return vs.map((v: UpdatePVRRequest) => {
            return safeUpdatePVRRequest(v);
        });
    } else {
        return [];
    }
}

export interface UpdatePVRResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * NPVR空间是否足够，取值：
    *
    * 0：空间不够
    *
    * 1：空间足够
    */
    isNPVRSpaceEnough?: string;

    /**
    * CPVR空间是否足够，取值：
    *
    * 0：空间不够
    *
    * 1：空间足够
    */
    isCPVRSpaceEnough?: string;

    /**
    * CPVR冲突分组列表，在有CPVR冲突时返回。
    */
    conflictGroups?: ConflictPVRGroup[];

    /**
    * 如果修改CPVR任务成功（包含NPVR转CPVR、CPVR转NPVR），返回Master STB的设备信息。
    */
    masterSTB?: Device;
}

function safeUpdatePVRResponse(v: UpdatePVRResponse): UpdatePVRResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.conflictGroups = safeConflictPVRGroups(v.conflictGroups);
        v.masterSTB = safeDevice(v.masterSTB);
    }
    return v;
}

function safeUpdatePVRResponses(vs: UpdatePVRResponse[]): UpdatePVRResponse[] {
    if (vs) {
        return vs.map((v: UpdatePVRResponse) => {
            return safeUpdatePVRResponse(v);
        });
    } else {
        return [];
    }
}

export interface AddPeriodicPVRRequest {

    /**
    * 待添加的周期系列录制任务，传入PeriodPVR 或 SeriesPVR。
    */
    PVR: PVRTaskBasic;

    /**
    * 节目单录制任务ID，节目单录制转系列任务场景必传。
    */
    playbillBasedPVRID?: string;

    /**
    * 添加CPVR或混合周期系列父任务时，如果需要平台检测空间，则需要传diskInfo。
    */
    diskInfo?: DiskInfo;

    /**
    * 需要剔除掉，不进行录制的子集的关键信息：
    *
    * 若添加的是周期父任务，则传的是希望剔除掉的子任务的日期，格式是yyyyMMdd的本地时区。
    *
    * 若添加的是系列父任务，则传入的是希望剔除掉的子任务的节目单ID。
    */
    excludePVRKeys?: string[];

    /**
    * 冲突检测类型：
    *
    * 0：若存在冲突不能入库，返回冲突列表。
    *
    * 1：即使存在冲突也要强制入库，自动取消所有与之冲突的任务 。
    *
    * 默认值是0。
    */
    conflictCheckType?: string;
}

function safeAddPeriodicPVRRequest(v: AddPeriodicPVRRequest): AddPeriodicPVRRequest {
    if (v) {
        v.PVR = safePVRTaskBasic(v.PVR);
        v.diskInfo = safeDiskInfo(v.diskInfo);
        v.excludePVRKeys = v.excludePVRKeys || [];
    }
    return v;
}

function safeAddPeriodicPVRRequests(vs: AddPeriodicPVRRequest[]): AddPeriodicPVRRequest[] {
    if (vs) {
        return vs.map((v: AddPeriodicPVRRequest) => {
            return safeAddPeriodicPVRRequest(v);
        });
    } else {
        return [];
    }
}

export interface AddPeriodicPVRResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 创建的周期系列录制父任务ID。
    */
    periodicPVRID?: string;

    /**
    * 若是系列录制任务，则返回新生成的子任务所在的节目单ID列表。
    */
    childPVRPlaybillIDs?: string[];

    /**
    * NPVR空间是否足够，取值：
    *
    * 0：空间不够
    *
    * 1：空间足够
    */
    isNPVRSpaceEnough?: string;

    /**
    * CPVR空间是否足够，取值：
    *
    * 0：空间不够
    *
    * 1：空间足够
    */
    isCPVRSpaceEnough?: string;

    /**
    * CPVR冲突分组列表，在有CPVR冲突时返回。
    */
    conflictGroups?: ConflictPVRGroup[];

    /**
    * 如果新增CPVR任务成功，返回Master STB的设备信息。
    */
    masterSTB?: Device;
}

function safeAddPeriodicPVRResponse(v: AddPeriodicPVRResponse): AddPeriodicPVRResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.childPVRPlaybillIDs = v.childPVRPlaybillIDs || [];
        v.conflictGroups = safeConflictPVRGroups(v.conflictGroups);
        v.masterSTB = safeDevice(v.masterSTB);
    }
    return v;
}

function safeAddPeriodicPVRResponses(vs: AddPeriodicPVRResponse[]): AddPeriodicPVRResponse[] {
    if (vs) {
        return vs.map((v: AddPeriodicPVRResponse) => {
            return safeAddPeriodicPVRResponse(v);
        });
    } else {
        return [];
    }
}

export interface UpdatePeriodicPVRRequest {

    /**
    * 待更新的周期系列录制任务，传入PeriodPVR 或 SeriesPVR。
    */
    PVR: PVRTaskBasic;

    /**
    * 修改CPVR或混合周期系列父任务时，如果需要平台检测空间，则需要传diskInfo。
    */
    diskInfo?: DiskInfo;

    /**
    * 需要剔除掉，不进行录制的子集的关键信息：
    *
    * 若更新的是周期父任务，则传的是希望剔除掉的子任务的日期，格式是yyyyMMdd的本地时区。
    *
    * 若更新的是系列父任务，则传入的是希望剔除掉的子任务的节目单ID。
    */
    excludePVRKeys?: string[];

    /**
    * 冲突检测类型：
    *
    * 0：若存在冲突不能入库，返回冲突列表。
    *
    * 1：即使存在冲突也要强制入库，自动取消所有与之冲突的任务 。
    *
    * 默认值是0。
    */
    conflictCheckType?: string;
}

function safeUpdatePeriodicPVRRequest(v: UpdatePeriodicPVRRequest): UpdatePeriodicPVRRequest {
    if (v) {
        v.PVR = safePVRTaskBasic(v.PVR);
        v.diskInfo = safeDiskInfo(v.diskInfo);
        v.excludePVRKeys = v.excludePVRKeys || [];
    }
    return v;
}

function safeUpdatePeriodicPVRRequests(vs: UpdatePeriodicPVRRequest[]): UpdatePeriodicPVRRequest[] {
    if (vs) {
        return vs.map((v: UpdatePeriodicPVRRequest) => {
            return safeUpdatePeriodicPVRRequest(v);
        });
    } else {
        return [];
    }
}

export interface UpdatePeriodicPVRResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * NPVR空间是否足够，取值：
    *
    * 0：空间不够
    *
    * 1：空间足够
    */
    isNPVRSpaceEnough?: string;

    /**
    * CPVR空间是否足够，取值：
    *
    * 0：空间不够
    *
    * 1：空间足够
    */
    isCPVRSpaceEnough?: string;

    /**
    * CPVR冲突分组列表，在有CPVR冲突时返回。
    */
    conflictGroups?: ConflictPVRGroup[];

    /**
    * 如果修改CPVR任务成功（包含NPVR转CPVR、CPVR转NPVR），返回Master STB的设备信息。
    */
    masterSTB?: Device;
}

function safeUpdatePeriodicPVRResponse(v: UpdatePeriodicPVRResponse): UpdatePeriodicPVRResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.conflictGroups = safeConflictPVRGroups(v.conflictGroups);
        v.masterSTB = safeDevice(v.masterSTB);
    }
    return v;
}

function safeUpdatePeriodicPVRResponses(vs: UpdatePeriodicPVRResponse[]): UpdatePeriodicPVRResponse[] {
    if (vs) {
        return vs.map((v: UpdatePeriodicPVRResponse) => {
            return safeUpdatePeriodicPVRResponse(v);
        });
    } else {
        return [];
    }
}

export interface QueryPVRRequest {

    /**
    * 录制任务的查询条件。
    */
    PVRCondition?: PVRCondition;

    /**
    * 录制任务的排序字段，取值包括：
    *
    * STARTTIME:ASC：按录制任务开始时间升序排序
    *
    * STARTTIME:DESC：按录制任务开始时间降序排序
    *
    * 默认值是STARTTIME:ASC。
    */
    sortType?: string;

    /**
    * 一次查询的总条数，不能设置为-1，终端一定要指定获取数据的总条数，最大条数默认不超过50，最大条数可配置，超过最大条数返回错误。
    */
    count: string;

    /**
    * 查询的起始位置。值为0时，表示从第一个节目开始查询。
    */
    offset: string;
}

function safeQueryPVRRequest(v: QueryPVRRequest): QueryPVRRequest {
    if (v) {
        v.PVRCondition = safePVRCondition(v.PVRCondition);
    }
    return v;
}

function safeQueryPVRRequests(vs: QueryPVRRequest[]): QueryPVRRequest[] {
    if (vs) {
        return vs.map((v: QueryPVRRequest) => {
            return safeQueryPVRRequest(v);
        });
    } else {
        return [];
    }
}

export interface QueryPVRResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 如果查询成功，返回条件匹配的任务总数。
    */
    total?: string;

    /**
    * PVR列表，只支持返回TimeBasedPVR，PlaybillBasedPVR。
    */
    PVRList?: PVRTaskBasic[];

    /**
    * 如果查询成功且传参singlePVRFilter有值，返回满足条件的所有单计划ID。
    *
    * 该计划ID在删除场景下会使用。
    */
    singlePVRIDs?: string[];

    /**
    * 如果查询成功且传参periodicPVRFilter有值，返回满足条件的所有父计划ID。
    *
    * 该计划ID在删除场景下会使用。
    */
    periodicPVRIDs?: string[];
}

function safeQueryPVRResponse(v: QueryPVRResponse): QueryPVRResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.PVRList = safePVRTaskBasics(v.PVRList);
        v.singlePVRIDs = v.singlePVRIDs || [];
        v.periodicPVRIDs = v.periodicPVRIDs || [];
    }
    return v;
}

function safeQueryPVRResponses(vs: QueryPVRResponse[]): QueryPVRResponse[] {
    if (vs) {
        return vs.map((v: QueryPVRResponse) => {
            return safeQueryPVRResponse(v);
        });
    } else {
        return [];
    }
}

export interface SearchPVRRequest {

    /**
    * 搜索关键字。
    */
    searchKey: string;

    /**
    * 单个任务过滤条件，传空对象代表用SinglePVRFilter的默认值做过滤，不传代表不做任何过滤
    */
    singlePVRfilter?: SinglePVRFilter;

    /**
    * 录制任务的排序字段，取值包括：
    *
    * STARTTIME:ASC：按录制任务开始时间升序排序
    *
    * STARTTIME:DESC：按录制任务开始时间降序排序
    *
    * 默认值是STARTTIME:ASC。
    */
    sortType?: string;

    /**
    * 一次查询的总条数，不能设置为-1，终端一定要指定获取数据的总条数，最大条数默认不超过50，最大条数可配置，超过最大条数返回错误。
    */
    count: string;

    /**
    * 查询的起始位置。默认值为0，表示从第一个节目开始查询。
    */
    offset: string;
}

function safeSearchPVRRequest(v: SearchPVRRequest): SearchPVRRequest {
    if (v) {
        v.singlePVRfilter = safeSinglePVRFilter(v.singlePVRfilter);
    }
    return v;
}

function safeSearchPVRRequests(vs: SearchPVRRequest[]): SearchPVRRequest[] {
    if (vs) {
        return vs.map((v: SearchPVRRequest) => {
            return safeSearchPVRRequest(v);
        });
    } else {
        return [];
    }
}

export interface SearchPVRResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 如果查询成功，返回条件匹配的任务总数。
    */
    total?: string;

    /**
    * PVR列表，只支持返回TimeBasedPVR，PlaybillBasedPVR。
    */
    PVRList?: PVRTaskBasic[];
}

function safeSearchPVRResponse(v: SearchPVRResponse): SearchPVRResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.PVRList = safePVRTaskBasics(v.PVRList);
    }
    return v;
}

function safeSearchPVRResponses(vs: SearchPVRResponse[]): SearchPVRResponse[] {
    if (vs) {
        return vs.map((v: SearchPVRResponse) => {
            return safeSearchPVRResponse(v);
        });
    } else {
        return [];
    }
}

export interface QueryPVRByIDRequest {

    /**
    * PVR录制任务ID，可以是单个任务ID，也可以是周期系列父任务ID。
    */
    ID: string;
}

function safeQueryPVRByIDRequest(v: QueryPVRByIDRequest): QueryPVRByIDRequest {
    if (v) {
    }
    return v;
}

function safeQueryPVRByIDRequests(vs: QueryPVRByIDRequest[]): QueryPVRByIDRequest[] {
    if (vs) {
        return vs.map((v: QueryPVRByIDRequest) => {
            return safeQueryPVRByIDRequest(v);
        });
    } else {
        return [];
    }
}

export interface QueryPVRByIDResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * PVR信息，若查询不到则不返回该字段。
    */
    PVR?: PVRTaskBasic;
}

function safeQueryPVRByIDResponse(v: QueryPVRByIDResponse): QueryPVRByIDResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.PVR = safePVRTaskBasic(v.PVR);
    }
    return v;
}

function safeQueryPVRByIDResponses(vs: QueryPVRByIDResponse[]): QueryPVRByIDResponse[] {
    if (vs) {
        return vs.map((v: QueryPVRByIDResponse) => {
            return safeQueryPVRByIDResponse(v);
        });
    } else {
        return [];
    }
}

export interface CancelPVRByIDRequest {

    /**
    * 单个录制任务ID列表。
    */
    singlePVRIDs?: string[];

    /**
    * 录制文件存储的物理介质，取值包括：
    *
    * CPVR：录制在STB侧
    *
    * NPVR：录制在网络侧
    *
    * 不传代表全部取消。
    */
    storageType?: string;
}

function safeCancelPVRByIDRequest(v: CancelPVRByIDRequest): CancelPVRByIDRequest {
    if (v) {
        v.singlePVRIDs = v.singlePVRIDs || [];
    }
    return v;
}

function safeCancelPVRByIDRequests(vs: CancelPVRByIDRequest[]): CancelPVRByIDRequest[] {
    if (vs) {
        return vs.map((v: CancelPVRByIDRequest) => {
            return safeCancelPVRByIDRequest(v);
        });
    } else {
        return [];
    }
}

export interface CancelPVRByIDResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 如果取消CPVR任务成功，返回Master STB的设备信息。
    */
    masterSTB?: Device;
}

function safeCancelPVRByIDResponse(v: CancelPVRByIDResponse): CancelPVRByIDResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.masterSTB = safeDevice(v.masterSTB);
    }
    return v;
}

function safeCancelPVRByIDResponses(vs: CancelPVRByIDResponse[]): CancelPVRByIDResponse[] {
    if (vs) {
        return vs.map((v: CancelPVRByIDResponse) => {
            return safeCancelPVRByIDResponse(v);
        });
    } else {
        return [];
    }
}

export interface CancelPVRByPlaybillIDRequest {

    /**
    * 节目单ID列表。
    */
    playbillIDs?: string[];

    /**
    * 录制文件存储的物理介质，取值包括：
    *
    * CPVR：录制在STB侧
    *
    * NPVR：录制在网络侧
    *
    * 不传代表全部取消。
    */
    storageType?: string;
}

function safeCancelPVRByPlaybillIDRequest(v: CancelPVRByPlaybillIDRequest): CancelPVRByPlaybillIDRequest {
    if (v) {
        v.playbillIDs = v.playbillIDs || [];
    }
    return v;
}

function safeCancelPVRByPlaybillIDRequests(vs: CancelPVRByPlaybillIDRequest[]): CancelPVRByPlaybillIDRequest[] {
    if (vs) {
        return vs.map((v: CancelPVRByPlaybillIDRequest) => {
            return safeCancelPVRByPlaybillIDRequest(v);
        });
    } else {
        return [];
    }
}

export interface CancelPVRByPlaybillIDResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 如果取消CPVR任务成功，返回Master STB的设备信息。
    */
    masterSTB?: Device;
}

function safeCancelPVRByPlaybillIDResponse(v: CancelPVRByPlaybillIDResponse): CancelPVRByPlaybillIDResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.masterSTB = safeDevice(v.masterSTB);
    }
    return v;
}

function safeCancelPVRByPlaybillIDResponses(vs: CancelPVRByPlaybillIDResponse[]): CancelPVRByPlaybillIDResponse[] {
    if (vs) {
        return vs.map((v: CancelPVRByPlaybillIDResponse) => {
            return safeCancelPVRByPlaybillIDResponse(v);
        });
    } else {
        return [];
    }
}

export interface DeletePVRByIDRequest {

    /**
    * 待删除的单个录制任务ID列表。
    */
    singlePVRIDs?: string[];

    /**
    * 待删除的周期系列父任务ID列表。
    */
    periodicPVRIDs?: string[];

    /**
    * 录制文件存储的物理介质，取值包括：
    *
    * CPVR：录制在STB侧
    *
    * NPVR：录制在网络侧
    *
    * 不传代表全部删除。
    */
    storageType?: string;
}

function safeDeletePVRByIDRequest(v: DeletePVRByIDRequest): DeletePVRByIDRequest {
    if (v) {
        v.singlePVRIDs = v.singlePVRIDs || [];
        v.periodicPVRIDs = v.periodicPVRIDs || [];
    }
    return v;
}

function safeDeletePVRByIDRequests(vs: DeletePVRByIDRequest[]): DeletePVRByIDRequest[] {
    if (vs) {
        return vs.map((v: DeletePVRByIDRequest) => {
            return safeDeletePVRByIDRequest(v);
        });
    } else {
        return [];
    }
}

export interface DeletePVRByIDResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 如果删除CPVR任务成功，返回Master STB的设备信息。
    */
    masterSTB?: Device;
}

function safeDeletePVRByIDResponse(v: DeletePVRByIDResponse): DeletePVRByIDResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.masterSTB = safeDevice(v.masterSTB);
    }
    return v;
}

function safeDeletePVRByIDResponses(vs: DeletePVRByIDResponse[]): DeletePVRByIDResponse[] {
    if (vs) {
        return vs.map((v: DeletePVRByIDResponse) => {
            return safeDeletePVRByIDResponse(v);
        });
    } else {
        return [];
    }
}

export interface DeletePVRByConditionRequest {

    /**
    * 待删除的PVR查询条件，用于全选删除的场景。
    */
    deleteConditions?: PVRCondition;

    /**
    * 待保留的单个录制任务ID列表，在deleteConditions入参存在时有意义。
    */
    reservedSinglePVRIDs?: string[];

    /**
    * 待保留的单个录制任务ID列表，在deleteConditions入参存在时有意义。
    */
    reservedPeriodicPVRIDs?: string[];

    /**
    * 录制文件存储的物理介质，取值包括：
    *
    * CPVR：录制在STB侧
    *
    * NPVR：录制在网络侧
    *
    * 不传代表全部删除
    */
    storageType?: string;
}

function safeDeletePVRByConditionRequest(v: DeletePVRByConditionRequest): DeletePVRByConditionRequest {
    if (v) {
        v.deleteConditions = safePVRCondition(v.deleteConditions);
        v.reservedSinglePVRIDs = v.reservedSinglePVRIDs || [];
        v.reservedPeriodicPVRIDs = v.reservedPeriodicPVRIDs || [];
    }
    return v;
}

function safeDeletePVRByConditionRequests(vs: DeletePVRByConditionRequest[]): DeletePVRByConditionRequest[] {
    if (vs) {
        return vs.map((v: DeletePVRByConditionRequest) => {
            return safeDeletePVRByConditionRequest(v);
        });
    } else {
        return [];
    }
}

export interface DeletePVRByConditionResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 如果删除CPVR任务成功，返回Master STB的设备信息。
    */
    masterSTB?: Device;
}

function safeDeletePVRByConditionResponse(v: DeletePVRByConditionResponse): DeletePVRByConditionResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.masterSTB = safeDevice(v.masterSTB);
    }
    return v;
}

function safeDeletePVRByConditionResponses(vs: DeletePVRByConditionResponse[]): DeletePVRByConditionResponse[] {
    if (vs) {
        return vs.map((v: DeletePVRByConditionResponse) => {
            return safeDeletePVRByConditionResponse(v);
        });
    } else {
        return [];
    }
}

export interface DeleteCPVRByDiskIDRequest {

    /**
    * 磁盘ID。
    */
    diskID: string;
}

function safeDeleteCPVRByDiskIDRequest(v: DeleteCPVRByDiskIDRequest): DeleteCPVRByDiskIDRequest {
    if (v) {
    }
    return v;
}

function safeDeleteCPVRByDiskIDRequests(vs: DeleteCPVRByDiskIDRequest[]): DeleteCPVRByDiskIDRequest[] {
    if (vs) {
        return vs.map((v: DeleteCPVRByDiskIDRequest) => {
            return safeDeleteCPVRByDiskIDRequest(v);
        });
    } else {
        return [];
    }
}

export interface DeleteCPVRByDiskIDResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 如果删除CPVR任务成功，返回Master STB的设备信息。
    */
    masterSTB?: Device;
}

function safeDeleteCPVRByDiskIDResponse(v: DeleteCPVRByDiskIDResponse): DeleteCPVRByDiskIDResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.masterSTB = safeDevice(v.masterSTB);
    }
    return v;
}

function safeDeleteCPVRByDiskIDResponses(vs: DeleteCPVRByDiskIDResponse[]): DeleteCPVRByDiskIDResponse[] {
    if (vs) {
        return vs.map((v: DeleteCPVRByDiskIDResponse) => {
            return safeDeleteCPVRByDiskIDResponse(v);
        });
    } else {
        return [];
    }
}

export interface QueryPVRSpaceRequest {

    /**
    * 查询类型，取值包括：
    *
    * CPVR：查询CPVR空间占用，
    *
    * NPVR：查询NPVR空间占用
    *
    * 不传代表查询NPVR和CPVR空间。
    */
    storageType?: string;

    /**
    * 当查询CPVR空间时，如果需要平台返回CPVR空间的notEnoughStartTime，需要传入此参数。
    */
    diskInfo?: DiskInfo;
}

function safeQueryPVRSpaceRequest(v: QueryPVRSpaceRequest): QueryPVRSpaceRequest {
    if (v) {
        v.diskInfo = safeDiskInfo(v.diskInfo);
    }
    return v;
}

function safeQueryPVRSpaceRequests(vs: QueryPVRSpaceRequest[]): QueryPVRSpaceRequest[] {
    if (vs) {
        return vs.map((v: QueryPVRSpaceRequest) => {
            return safeQueryPVRSpaceRequest(v);
        });
    } else {
        return [];
    }
}

export interface QueryPVRSpaceResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * CPVR空间占用情况，只返回bookingSize和notEnoughStartTime。
    */
    CPVRStorage?: PVRSpace;

    /**
    * NPVR空间占用情况。
    */
    NPVRStorage?: PVRSpace;
}

function safeQueryPVRSpaceResponse(v: QueryPVRSpaceResponse): QueryPVRSpaceResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.CPVRStorage = safePVRSpace(v.CPVRStorage);
        v.NPVRStorage = safePVRSpace(v.NPVRStorage);
    }
    return v;
}

function safeQueryPVRSpaceResponses(vs: QueryPVRSpaceResponse[]): QueryPVRSpaceResponse[] {
    if (vs) {
        return vs.map((v: QueryPVRSpaceResponse) => {
            return safeQueryPVRSpaceResponse(v);
        });
    } else {
        return [];
    }
}

export interface QueryPVRSpaceByIDRequest {

    /**
    * 待查询的单个录制任务占用的空间大小。
    */
    singlePVRIDs?: string[];

    /**
    * 查询类型，取值包括：
    *
    * CPVR：查询CPVR空间占用，
    *
    * NPVR：查询NPVR空间占用
    *
    * 不传代表查询NPVR和CPVR空间。
    */
    storageType?: string;
}

function safeQueryPVRSpaceByIDRequest(v: QueryPVRSpaceByIDRequest): QueryPVRSpaceByIDRequest {
    if (v) {
        v.singlePVRIDs = v.singlePVRIDs || [];
    }
    return v;
}

function safeQueryPVRSpaceByIDRequests(vs: QueryPVRSpaceByIDRequest[]): QueryPVRSpaceByIDRequest[] {
    if (vs) {
        return vs.map((v: QueryPVRSpaceByIDRequest) => {
            return safeQueryPVRSpaceByIDRequest(v);
        });
    } else {
        return [];
    }
}

export interface QueryPVRSpaceByIDResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * singlePVRIDs的任务占用的CPVR总空间数，单位为MB。
    */
    CPVRStorage?: PVRSpace;

    /**
    * singlePVRIDs的任务占用的NPVR总空间数，目前只支持按时间的空间查询，单位为秒。
    */
    NPVRStorage?: PVRSpace;
}

function safeQueryPVRSpaceByIDResponse(v: QueryPVRSpaceByIDResponse): QueryPVRSpaceByIDResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.CPVRStorage = safePVRSpace(v.CPVRStorage);
        v.NPVRStorage = safePVRSpace(v.NPVRStorage);
    }
    return v;
}

function safeQueryPVRSpaceByIDResponses(vs: QueryPVRSpaceByIDResponse[]): QueryPVRSpaceByIDResponse[] {
    if (vs) {
        return vs.map((v: QueryPVRSpaceByIDResponse) => {
            return safeQueryPVRSpaceByIDResponse(v);
        });
    } else {
        return [];
    }
}

export interface SolvePVRConflictRequest {

    /**
    * 用户已经解决的冲突分组列表。
    */
    conflictGroups: ConflictPVRGroup[];

    /**
    * 从第几个分组开始检查，取值是大于等于0的整数，0表示第一个分组。
    */
    groupIndex: string;
}

function safeSolvePVRConflictRequest(v: SolvePVRConflictRequest): SolvePVRConflictRequest {
    if (v) {
        v.conflictGroups = safeConflictPVRGroups(v.conflictGroups);
    }
    return v;
}

function safeSolvePVRConflictRequests(vs: SolvePVRConflictRequest[]): SolvePVRConflictRequest[] {
    if (vs) {
        return vs.map((v: SolvePVRConflictRequest) => {
            return safeSolvePVRConflictRequest(v);
        });
    } else {
        return [];
    }
}

export interface SolvePVRConflictResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 如果仍然PVR冲突，返回冲突的任务分组列表。
    */
    conflictGroups?: ConflictPVRGroup[];

    /**
    * 如果冲突没有解决，返回从第几个分组继续解决。若冲突解决返回-1。
    */
    nextGroupIndex?: string;
}

function safeSolvePVRConflictResponse(v: SolvePVRConflictResponse): SolvePVRConflictResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.conflictGroups = safeConflictPVRGroups(v.conflictGroups);
    }
    return v;
}

function safeSolvePVRConflictResponses(vs: SolvePVRConflictResponse[]): SolvePVRConflictResponse[] {
    if (vs) {
        return vs.map((v: SolvePVRConflictResponse) => {
            return safeSolvePVRConflictResponse(v);
        });
    } else {
        return [];
    }
}

export interface SetMasterSTBIDRequest {

    /**
    * Master机顶盒的物理设备ID。
    */
    masterSTBID: string;
}

function safeSetMasterSTBIDRequest(v: SetMasterSTBIDRequest): SetMasterSTBIDRequest {
    if (v) {
    }
    return v;
}

function safeSetMasterSTBIDRequests(vs: SetMasterSTBIDRequest[]): SetMasterSTBIDRequest[] {
    if (vs) {
        return vs.map((v: SetMasterSTBIDRequest) => {
            return safeSetMasterSTBIDRequest(v);
        });
    } else {
        return [];
    }
}

export interface SetMasterSTBIDResponse {

    /**
    * 返回结果。
    */
    result: Result;
}

function safeSetMasterSTBIDResponse(v: SetMasterSTBIDResponse): SetMasterSTBIDResponse {
    if (v) {
        v.result = safeResult(v.result);
    }
    return v;
}

function safeSetMasterSTBIDResponses(vs: SetMasterSTBIDResponse[]): SetMasterSTBIDResponse[] {
    if (vs) {
        return vs.map((v: SetMasterSTBIDResponse) => {
            return safeSetMasterSTBIDResponse(v);
        });
    } else {
        return [];
    }
}

export interface GetMasterSTBIDRequest {
}

function safeGetMasterSTBIDRequest(v: GetMasterSTBIDRequest): GetMasterSTBIDRequest {
    if (v) {
    }
    return v;
}

function safeGetMasterSTBIDRequests(vs: GetMasterSTBIDRequest[]): GetMasterSTBIDRequest[] {
    if (vs) {
        return vs.map((v: GetMasterSTBIDRequest) => {
            return safeGetMasterSTBIDRequest(v);
        });
    } else {
        return [];
    }
}

export interface GetMasterSTBIDResponse {

    /**
    * 返回结果
    */
    result: Result;

    /**
    * Master机顶盒的ID。
    */
    masterSTBID: string;
}

function safeGetMasterSTBIDResponse(v: GetMasterSTBIDResponse): GetMasterSTBIDResponse {
    if (v) {
        v.result = safeResult(v.result);
    }
    return v;
}

function safeGetMasterSTBIDResponses(vs: GetMasterSTBIDResponse[]): GetMasterSTBIDResponse[] {
    if (vs) {
        return vs.map((v: GetMasterSTBIDResponse) => {
            return safeGetMasterSTBIDResponse(v);
        });
    } else {
        return [];
    }
}

export interface UpdateCPVRStatusRequest {

    /**
    * CPVR录制文件ID（对应机顶盒接口和事件中的schedule_id字段）。
    */
    fileID: string;

    /**
    * PVR任务ID。
    */
    PVRID: string;

    /**
    * CPVR的录制状态，支持上报以下状态：
    *
    * SUCCESS：录制成功
    *
    * PART_SUCCESS：录制部分成功
    *
    * FAIL：录制失败
    */
    status: string;

    /**
    * 录制文件所在的磁盘ID。
    */
    diskID?: string;

    /**
    * 最终录制任务的时长，若上报状态为SUCCESS、PART_SUCCESS，需要上报实际录制时长。单位为秒。
    */
    duration?: string;

    /**
    * 最终录制任务的文件大小，若上报状态为SUCCESS、PART_SUCCESS，需要上报实际录制文件大小。单位为MB。
    */
    fileSize?: string;

    /**
    * 最终录制任务的录制开始时间，取值为距离1970年1月1号的毫秒数。
    *
    * 若上报状态为SUCCESS、PART_SUCCESS，需要上报。
    *
    * 对于基于节目单的录制，这是包含了前padding的录制开始时间。
    */
    recordBeginTime?: string;

    /**
    * 最终录制任务的录制结束时间，取值为距离1970年1月1号的毫秒数。
    *
    * 若上报状态为SUCCESS、PART_SUCCESS，需要上报。
    *
    * 对于基于节目单的录制，这是包含了后padding的录制结束时间。
    */
    recordEndTime?: string;
}

function safeUpdateCPVRStatusRequest(v: UpdateCPVRStatusRequest): UpdateCPVRStatusRequest {
    if (v) {
    }
    return v;
}

function safeUpdateCPVRStatusRequests(vs: UpdateCPVRStatusRequest[]): UpdateCPVRStatusRequest[] {
    if (vs) {
        return vs.map((v: UpdateCPVRStatusRequest) => {
            return safeUpdateCPVRStatusRequest(v);
        });
    } else {
        return [];
    }
}

export interface UpdateCPVRStatusResponse {

    /**
    * 返回结果。
    */
    result: Result;
}

function safeUpdateCPVRStatusResponse(v: UpdateCPVRStatusResponse): UpdateCPVRStatusResponse {
    if (v) {
        v.result = safeResult(v.result);
    }
    return v;
}

function safeUpdateCPVRStatusResponses(vs: UpdateCPVRStatusResponse[]): UpdateCPVRStatusResponse[] {
    if (vs) {
        return vs.map((v: UpdateCPVRStatusResponse) => {
            return safeUpdateCPVRStatusResponse(v);
        });
    } else {
        return [];
    }
}

export interface PlayPVRRequest {

    /**
    * 录制计划ID。
    */
    planID: string;

    /**
    * 如果录制计划下有多个文件，选择一个文件播放。
    */
    fileID: string;

    /**
    * 检查PVR是否被加锁和父母字控制，如果加锁或父母字控制，需要进行密码校验。
    */
    checkLock?: CheckLock;

    /**
    * 指定返回的URL格式，取值包括：
    *
    * 0：默认格式
    *
    * 1：转换成HLS协议
    *
    * 默认值是0。
    */
    URLFormat?: string;

    /**
    * Indicates whether to return the HTTPS URL.
    *
    * 0: no
    *
    * 1: yes
    *
    * Default: 0
    */
   isHTTPS?: number;

    /**
    * 如果内容未订购是否返回内容定价的产品，取值包括：
    *
    * 0：不返回
    *
    * 1：返回
    *
    * 默认值是1。
    */
    isReturnProduct?: string;

    /**
    * 如果返回产品，指定产品的排序方式：
    *
    * PRICE:ASC：按产品价格升序排序
    *
    * PRICE:DESC：按产品价格降序排序
    *
    * 默认值是PRICE:ASC。
    */
    sortType?: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safePlayPVRRequest(v: PlayPVRRequest): PlayPVRRequest {
    if (v) {
        v.checkLock = safeCheckLock(v.checkLock);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safePlayPVRRequests(vs: PlayPVRRequest[]): PlayPVRRequest[] {
    if (vs) {
        return vs.map((v: PlayPVRRequest) => {
            return safePlayPVRRequest(v);
        });
    } else {
        return [];
    }
}

export interface PlayPVRResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 鉴权结果。
    */
    authorizeResult: AuthorizeResult;

    /**
    * 如果鉴权成功，返回内容播放地址。
    */
    playURL?: string;

    /**
    * 如果鉴权成功且NPVR创建过书签，返回书签断点时间。
    *
    * 单位是秒。
    */
    bookmark?: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safePlayPVRResponse(v: PlayPVRResponse): PlayPVRResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.authorizeResult = safeAuthorizeResult(v.authorizeResult);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safePlayPVRResponses(vs: PlayPVRResponse[]): PlayPVRResponse[] {
    if (vs) {
        return vs.map((v: PlayPVRResponse) => {
            return safePlayPVRResponse(v);
        });
    } else {
        return [];
    }
}

export interface ApplyStreamRequest {

    /**
    * 带宽类型，取值包括：
    *
    * 0：EPG浏览
    *
    * 1：VOD
    *
    * 2：BTV（直播观看）
    *
    * 5：CUTV
    *
    * 6：CPVR（预留）
    *
    * 8：NPVR
    *
    * 13：IR（IR切换）
    *
    * 17：Download（预留）
    *
    * 18：UserMosaic（预留）
    */
    streamType: string;

    /**
    * 针对每个StreamType取值，contentID取值说明如下
    *
    * EPG浏览：
    *
    * contentID留空
    *
    * VOD：
    *
    * 点播内容ID。
    *
    * BTV：
    *
    * 频道ID，如果是频道快速切换进入，表示切换后的目的频道ID。
    *
    * CUTV：
    *
    * 节目单ID。
    *
    * NPVR：
    *
    * 录制任务的频道ID。
    *
    * IR：
    *
    * 频道ID。
    */
    contentID?: string;

    /**
    * 针对每个StreamType取值，分别进行mediaIds取值说明如下
    *
    * EPG浏览：
    *
    * mediaIds留空
    *
    * VOD：
    *
    * 申请VOD带宽场景下，待申请的媒资ID。
    *
    * 备注:
    *
    * 1.mediaIds和maxBitrate必须有一个存在
    *
    * 2.如果mediaIds和maxBitrate同时上报，且mediaId存在对应的媒资信息，VSP优先使用mediaId的带宽，否则使用终端上报的maxBitrate/minBitrate
    *
    * BTV：
    *
    * 频道媒资ID，如果是频道快速切换进入，表示切换后的目的媒资ID。
    *
    * CUTV：
    *
    * 频道媒资ID。
    *
    * NPVR：
    *
    * 录制任务的媒资ID。
    *
    * IR：
    *
    * 频道媒资ID。
    */
    mediaIds: string[];

    /**
    * 表示要释放的上一个带宽任务，平台会自动释放上一个任务申请的带宽，如传入的任务ID在平台不存在，平台不会返回错误，仍然会正常进行本次带宽申请。
    */
    preTaskId?: string;

    /**
    * 切换模式，取值包括：
    *
    * 0：从直播切换到IR
    *
    * 1：从IR切换到直播
    *
    * 当streamType&#x3D;13，即IR时，此字段为必填，否则此字段不处理。
    */
    switchMode?: string;

    /**
    * 待申请的媒资最大带宽，单位为kb/s。针对OTT媒资场景下生效。
    */
    maxBitrate?: string;

    /**
    * 待申请的媒资最小带宽，单位为kb/s。针对OTT媒资场景下生效。
    */
    minBitrate?: string;

    /**
    * 申请CUTV/IR带宽场景下必填
    *
    * 节目单ID。
    */
    programID?: string;

    /**
    * 申请PVR带宽场景下必填
    *
    * 录制任务编号。
    */
    pvrID?: string;
}

function safeApplyStreamRequest(v: ApplyStreamRequest): ApplyStreamRequest {
    if (v) {
        v.mediaIds = v.mediaIds || [];
    }
    return v;
}

function safeApplyStreamRequests(vs: ApplyStreamRequest[]): ApplyStreamRequest[] {
    if (vs) {
        return vs.map((v: ApplyStreamRequest) => {
            return safeApplyStreamRequest(v);
        });
    } else {
        return [];
    }
}

export interface ApplyStreamResponse {

    /**
    * 返回码。
    */
    result: Result;

    /**
    * 如果带宽申请成功，返回VSP分配的任务ID。
    */
    taskId?: string;

    /**
    * 如果带宽申请成功，返回占用的WAN带宽，单位为kb/s。
    */
    usedBitrate?: string;

    /**
    * 如果带宽申请成功且申请BTV/IR带宽，返回终端可播放/录制/下载的媒资ID。
    *
    * 备注：
    */
    mediaId?: string;

    /**
    * 如果带宽申请成功，返回频道是否可以做FCC，取值包括：
    *
    * 0：不支持FCC
    *
    * 1：支持FCC
    *
    * 备注：IR和CPVR不支持FCC，频道是否支持FCC依赖媒资属性和用户剩余带宽是否满足FCC带宽要求。
    */
    isFCC?: string;

    /**
    * 如果家庭带宽不足，返回低优先级的记录。
    */
    lowPriorityRecords?: StreamRecord[];
}

function safeApplyStreamResponse(v: ApplyStreamResponse): ApplyStreamResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.lowPriorityRecords = safeStreamRecords(v.lowPriorityRecords);
    }
    return v;
}

function safeApplyStreamResponses(vs: ApplyStreamResponse[]): ApplyStreamResponse[] {
    if (vs) {
        return vs.map((v: ApplyStreamResponse) => {
            return safeApplyStreamResponse(v);
        });
    } else {
        return [];
    }
}

export interface ReleaseStreamRequest {

    /**
    * 平台分配的任务ID，如果不传，表示释放当前设备申请的所有带宽。
    */
    taskID?: string;

    /**
    * 释放类型，取值包括：
    *
    * 0：全部释放
    *
    * 1：仅释放FCC多余带宽
    */
    releaseMode: string;
}

function safeReleaseStreamRequest(v: ReleaseStreamRequest): ReleaseStreamRequest {
    if (v) {
    }
    return v;
}

function safeReleaseStreamRequests(vs: ReleaseStreamRequest[]): ReleaseStreamRequest[] {
    if (vs) {
        return vs.map((v: ReleaseStreamRequest) => {
            return safeReleaseStreamRequest(v);
        });
    } else {
        return [];
    }
}

export interface ReleaseStreamResponse {

    /**
    * 返回码。
    */
    retCode: string;

    /**
    * 描述信息。
    */
    retMsg: string;
}

function safeReleaseStreamResponse(v: ReleaseStreamResponse): ReleaseStreamResponse {
    if (v) {
    }
    return v;
}

function safeReleaseStreamResponses(vs: ReleaseStreamResponse[]): ReleaseStreamResponse[] {
    if (vs) {
        return vs.map((v: ReleaseStreamResponse) => {
            return safeReleaseStreamResponse(v);
        });
    } else {
        return [];
    }
}

export interface GetStreamRecordRequest {
}

function safeGetStreamRecordRequest(v: GetStreamRecordRequest): GetStreamRecordRequest {
    if (v) {
    }
    return v;
}

function safeGetStreamRecordRequests(vs: GetStreamRecordRequest[]): GetStreamRecordRequest[] {
    if (vs) {
        return vs.map((v: GetStreamRecordRequest) => {
            return safeGetStreamRecordRequest(v);
        });
    } else {
        return [];
    }
}

export interface GetStreamRecordResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 家庭总带宽，单位为kb/s。
    */
    totalWanBitrate?: string;

    /**
    * 家庭已用带宽，单位为kb/s。
    */
    usedWanBitrate?: string;

    /**
    * 当前设备总带宽，单位为kb/s。
    */
    totalDevBitrate?: string;

    /**
    * 当前设备已用带宽，单位为kb/s。
    */
    usedDevBitrate?: string;

    /**
    * 家庭内所有设备申请的带宽记录。
    *
    * 更多详细信息，请参见对象定义Stream[Record](file:///E:/PIA/Hybrid%20Video%20Solution/Docs/%E6%8E%A5%E5%8F%A3%E6%96%87%E6%A1%A3/02%20MEM/Hybrid%20Video%20Solution%20MEM%E4%B8%8E%E7%AC%AC%E4%B8%89%E6%96%B9%E5%AE%A2%E6%88%B7%E7%AB%AF%E7%9A%84XML%E6%8E%A5%E5%8F%A3%E8%A7%84%E8%8C%83.doc#_ZH-CN_TOPIC_0019197763)。
    */
    records?: StreamRecord[];

    /**
    * FCC带宽申请倍数。
    */
    fccRatio?: string;

    /**
    * 页面预留带宽，单位为kb/s。
    */
    epgBrowsingBitrate?: string;
}

function safeGetStreamRecordResponse(v: GetStreamRecordResponse): GetStreamRecordResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.records = safeStreamRecords(v.records);
    }
    return v;
}

function safeGetStreamRecordResponses(vs: GetStreamRecordResponse[]): GetStreamRecordResponse[] {
    if (vs) {
        return vs.map((v: GetStreamRecordResponse) => {
            return safeGetStreamRecordResponse(v);
        });
    } else {
        return [];
    }
}

export interface PublishCommentRequest {

    /**
    * 订户编号
    */
    userID: string;

    /**
    * 用户ProfileSN，如果客户端不基于Profile发表评论，则可以统一都传-1
    */
    profileSN?: string;

    /**
    * 内容编号
    */
    contentCode: string;

    /**
    * 内容类型
    */
    contentType: string;

    /**
    * 用户发表的评论内容
    */
    comment: string;

    /**
    * 发表评论的时间
    */
    commentTime?: string;

    /**
    * 回帖的父评论ID，如果是新发帖，则描述为“-1”。
    */
    parentCommentID?: string;
}

function safePublishCommentRequest(v: PublishCommentRequest): PublishCommentRequest {
    if (v) {
    }
    return v;
}

function safePublishCommentRequests(vs: PublishCommentRequest[]): PublishCommentRequest[] {
    if (vs) {
        return vs.map((v: PublishCommentRequest) => {
            return safePublishCommentRequest(v);
        });
    } else {
        return [];
    }
}

export interface PublishCommentResponse {

    /**
    * 返回码。
    */
    result: Result;

    /**
    * 新发表的评论的ID
    */
    commentID?: string;
}

function safePublishCommentResponse(v: PublishCommentResponse): PublishCommentResponse {
    if (v) {
        v.result = safeResult(v.result);
    }
    return v;
}

function safePublishCommentResponses(vs: PublishCommentResponse[]): PublishCommentResponse[] {
    if (vs) {
        return vs.map((v: PublishCommentResponse) => {
            return safePublishCommentResponse(v);
        });
    } else {
        return [];
    }
}

export interface MaintainCommentRequest {

    /**
    * 操作方式（当前仅支持DELETE）：
    *
    * DELETE：删除评论
    *
    * UPDATE：修改评论内容
    */
    action: string;

    /**
    * 订户编号
    */
    userID: string;

    /**
    * 用户ProfileSN，如果客户端不基于Profile发表评论，则可以统一都传-1
    */
    profileSN?: string;

    /**
    * 评论的ID
    */
    commentID: string;

    /**
    * 评论内容
    */
    comment?: string;
}

function safeMaintainCommentRequest(v: MaintainCommentRequest): MaintainCommentRequest {
    if (v) {
    }
    return v;
}

function safeMaintainCommentRequests(vs: MaintainCommentRequest[]): MaintainCommentRequest[] {
    if (vs) {
        return vs.map((v: MaintainCommentRequest) => {
            return safeMaintainCommentRequest(v);
        });
    } else {
        return [];
    }
}

export interface MaintainCommentResponse {

    /**
    * 返回码。
    */
    result: Result;
}

function safeMaintainCommentResponse(v: MaintainCommentResponse): MaintainCommentResponse {
    if (v) {
        v.result = safeResult(v.result);
    }
    return v;
}

function safeMaintainCommentResponses(vs: MaintainCommentResponse[]): MaintainCommentResponse[] {
    if (vs) {
        return vs.map((v: MaintainCommentResponse) => {
            return safeMaintainCommentResponse(v);
        });
    } else {
        return [];
    }
}

export interface QueryCommentsRequest {

    /**
    * 评论查询条件
    */
    filter: CommentFilter;

    /**
    * 查询的偏移量
    */
    offset: string;

    /**
    * 本次查询的返回数量
    */
    length: string;
}

function safeQueryCommentsRequest(v: QueryCommentsRequest): QueryCommentsRequest {
    if (v) {
        v.filter = safeCommentFilter(v.filter);
    }
    return v;
}

function safeQueryCommentsRequests(vs: QueryCommentsRequest[]): QueryCommentsRequest[] {
    if (vs) {
        return vs.map((v: QueryCommentsRequest) => {
            return safeQueryCommentsRequest(v);
        });
    } else {
        return [];
    }
}

export interface QueryCommentsResponse {

    /**
    * 返回码。
    */
    result: Result;

    /**
    * 符合查询条件filter的评论总数
    */
    total: string;

    /**
    * 查询到的评论列表
    */
    comments?: CommentObject[];
}

function safeQueryCommentsResponse(v: QueryCommentsResponse): QueryCommentsResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.comments = safeCommentObjects(v.comments);
    }
    return v;
}

function safeQueryCommentsResponses(vs: QueryCommentsResponse[]): QueryCommentsResponse[] {
    if (vs) {
        return vs.map((v: QueryCommentsResponse) => {
            return safeQueryCommentsResponse(v);
        });
    } else {
        return [];
    }
}

export interface ReportCommentRequest {

    /**
    * 订户编号
    */
    userID: string;

    /**
    * 用户ProfileSN，如果客户端不基于Profile发表评论，则可以统一都传-1
    */
    profileSN?: string;

    /**
    * 被举报的评论的ID
    */
    commentID: string;

    /**
    * 举报的原因
    */
    text?: string;
}

function safeReportCommentRequest(v: ReportCommentRequest): ReportCommentRequest {
    if (v) {
    }
    return v;
}

function safeReportCommentRequests(vs: ReportCommentRequest[]): ReportCommentRequest[] {
    if (vs) {
        return vs.map((v: ReportCommentRequest) => {
            return safeReportCommentRequest(v);
        });
    } else {
        return [];
    }
}

export interface ReportCommentResponse {

    /**
    * 返回码。
    */
    result: Result;
}

function safeReportCommentResponse(v: ReportCommentResponse): ReportCommentResponse {
    if (v) {
        v.result = safeResult(v.result);
    }
    return v;
}

function safeReportCommentResponses(vs: ReportCommentResponse[]): ReportCommentResponse[] {
    if (vs) {
        return vs.map((v: ReportCommentResponse) => {
            return safeReportCommentResponse(v);
        });
    } else {
        return [];
    }
}

export interface LikeCommentRequest {

    /**
    * 订户编号
    */
    userID: string;

    /**
    * 用户ProfileSN，如果客户端不基于Profile发表评论，则可以统一都传-1
    */
    profileSN?: string;

    /**
    * 被点赞的评论的ID
    */
    commentID: string;
}

function safeLikeCommentRequest(v: LikeCommentRequest): LikeCommentRequest {
    if (v) {
    }
    return v;
}

function safeLikeCommentRequests(vs: LikeCommentRequest[]): LikeCommentRequest[] {
    if (vs) {
        return vs.map((v: LikeCommentRequest) => {
            return safeLikeCommentRequest(v);
        });
    } else {
        return [];
    }
}

export interface LikeCommentResponse {

    /**
    * 返回码。
    */
    result: Result;

    /**
    * 是否已经点赞
    */
    liked: Boolean;

    /**
    * 评论被点赞的总数
    */
    likedCount: string;
}

function safeLikeCommentResponse(v: LikeCommentResponse): LikeCommentResponse {
    if (v) {
        v.result = safeResult(v.result);
    }
    return v;
}

function safeLikeCommentResponses(vs: LikeCommentResponse[]): LikeCommentResponse[] {
    if (vs) {
        return vs.map((v: LikeCommentResponse) => {
            return safeLikeCommentResponse(v);
        });
    } else {
        return [];
    }
}

export interface BarrageRequest {

    /**
    * 订户编号
    */
    userID: string;

    /**
    * 用户ProfileSN，如果客户端不基于Profile发表弹幕消息，则可以统一都传-1
    */
    profileSN?: string;

    /**
    * 弹幕内容
    */
    barrage: string;

    /**
    * 发表弹幕的内容的编号（点播/直播）
    */
    contentCode: string;

    /**
    * 发表弹幕的内容类型
    */
    contentType: string;

    /**
    * UTC时间，格式yyyymmddHHMMSS，基于内容发表弹幕时，内容播放的时间。
    *
    * 如果是点播，实际只会是yyyymmdd填写为0，只有后面的HHMMSS有效
    */
    timestamp: string;

    /**
    * UTC时间，格式yyyymmddHMMSS，用户发表弹幕的时间。
    */
    barrageTime: string;

    /**
    * 弹幕的字体颜色(rgb格式表示)
    */
    fontColor?: string;

    /**
    * 扩展信息。
    */
    extendProperties?: NamedParameter[];
}

function safeBarrageRequest(v: BarrageRequest): BarrageRequest {
    if (v) {
        v.extendProperties = safeNamedParameters(v.extendProperties);
    }
    return v;
}

function safeBarrageRequests(vs: BarrageRequest[]): BarrageRequest[] {
    if (vs) {
        return vs.map((v: BarrageRequest) => {
            return safeBarrageRequest(v);
        });
    } else {
        return [];
    }
}

export interface BarrageResponse {

    /**
    * 返回码。
    */
    result: Result;

    /**
    * 新发表的弹幕的ID
    */
    barrageID?: string;
}

function safeBarrageResponse(v: BarrageResponse): BarrageResponse {
    if (v) {
        v.result = safeResult(v.result);
    }
    return v;
}

function safeBarrageResponses(vs: BarrageResponse[]): BarrageResponse[] {
    if (vs) {
        return vs.map((v: BarrageResponse) => {
            return safeBarrageResponse(v);
        });
    } else {
        return [];
    }
}

export interface GetBarrageListRequest {

    /**
    * 订户编号
    */
    userID?: string;

    /**
    * 用户ProfileSN，如果客户端不基于Profile发表评论，则可以统一都传-1
    */
    profileSN?: string;

    /**
    * 弹幕查询条件
    */
    filter: BarrageFilter;

    /**
    * 查询的偏移量
    */
    offset: string;

    /**
    * 本次查询的返回数量
    */
    length: string;
}

function safeGetBarrageListRequest(v: GetBarrageListRequest): GetBarrageListRequest {
    if (v) {
        v.filter = safeBarrageFilter(v.filter);
    }
    return v;
}

function safeGetBarrageListRequests(vs: GetBarrageListRequest[]): GetBarrageListRequest[] {
    if (vs) {
        return vs.map((v: GetBarrageListRequest) => {
            return safeGetBarrageListRequest(v);
        });
    } else {
        return [];
    }
}

export interface GetBarrageListResponse {

    /**
    * 返回码。
    */
    result: Result;

    /**
    * 符合条件的总数
    */
    total?: string;

    /**
    * 弹幕列表
    */
    barrageObjects?: BarrageObject[];

    /**
    * 服务侧返回的弹幕的最大时间
    *
    * 平台根据实际情况返回弹幕的数据，可能会返回超出请求条件的弹幕数据，此字段是平台返回的当前返回弹幕数据的之间线。APP可据此再查询数据。
    */
    lastBarrageTime: string;
}

function safeGetBarrageListResponse(v: GetBarrageListResponse): GetBarrageListResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.barrageObjects = safeBarrageObjects(v.barrageObjects);
    }
    return v;
}

function safeGetBarrageListResponses(vs: GetBarrageListResponse[]): GetBarrageListResponse[] {
    if (vs) {
        return vs.map((v: GetBarrageListResponse) => {
            return safeGetBarrageListResponse(v);
        });
    } else {
        return [];
    }
}

export interface RegisterUserInfoRequest {

    /**
    * 订户编号
    */
    userID: string;

    /**
    * 用户ProfileSN，如果客户端不基于Profile发表评论，则可以统一都传-1
    */
    profileSN?: string;

    /**
    * 用户的昵称
    */
    nickName?: string;

    /**
    * 头像URL或者编号。
    */
    headPic?: string;
}

function safeRegisterUserInfoRequest(v: RegisterUserInfoRequest): RegisterUserInfoRequest {
    if (v) {
    }
    return v;
}

function safeRegisterUserInfoRequests(vs: RegisterUserInfoRequest[]): RegisterUserInfoRequest[] {
    if (vs) {
        return vs.map((v: RegisterUserInfoRequest) => {
            return safeRegisterUserInfoRequest(v);
        });
    } else {
        return [];
    }
}

export interface RegisterUserInfoResponse {

    /**
    * 返回码。
    */
    result: Result;
}

function safeRegisterUserInfoResponse(v: RegisterUserInfoResponse): RegisterUserInfoResponse {
    if (v) {
        v.result = safeResult(v.result);
    }
    return v;
}

function safeRegisterUserInfoResponses(vs: RegisterUserInfoResponse[]): RegisterUserInfoResponse[] {
    if (vs) {
        return vs.map((v: RegisterUserInfoResponse) => {
            return safeRegisterUserInfoResponse(v);
        });
    } else {
        return [];
    }
}

export interface QueryCustomizeConfigRequest {

    /**
    * 参数名称，如果携带多个参数名称则以英文逗号分隔；如果不指定，则查询终端或模板所有的参数信息。当queryType为3时，该参数必选，表示集成部件名。
    */
    key?: string;

    /**
    * 查询类型，支持查询多个类型，以英文逗号分隔，取值范围：
    *
    * 0：查询终端的配置参数
    *
    * 1：查询模板的配置参数
    *
    * 2：查询VSP EPG参数
    *
    * 3：查询第三方系统集成参数[字段预留]
    *
    * 4：查询内容级别体系
    *
    * 5：查询可用语种列表
    *
    * 6：查询ISO编码列表
    *
    * 7：查询货币类型字母编码
    *
    * 说明
    *
    * 当queryType为多个的时候，忽略key值(包括queryType为3的这种场景)。
    *
    * 终端在查询多个配置信息时请审视需要的配置参数，如果某种类型的参数在业务中不需要，请从请求参数中过滤掉，以保证接口性能要求。
    */
    queryType: string;

    /**
    * 当queryType取值为0和1时，可以指定查询类型，取值包括：
    *
    * 0：精确查询
    *
    * 1：模糊查询
    *
    * 默认值为0。
    */
    isFuzzyQuery?: string;

    /**
    * 当queryType &#x3D;0时，表示终端型号。如果终端未携带，默认取用户会话里的终端型号。
    */
    deviceModel?: string;

    /**
    * 当queryType&#x3D;6时，全称语种，该字段为必填项，平台当前支持如下语种包括（客户端传的值需要与以下选项全匹配，区分大小写），终端可以定制其他语种：
    *
    * English
    *
    * Chinese
    *
    * Arabic
    *
    * Hungarian
    *
    * German
    *
    * 默认值为English。
    */
    language?: string;

    /**
    * 当queryType&#x3D;6时，ISO标准，该字段为必填项，平台当前支持的选项包括，终端可以定制其他ISO标准：
    *
    * ISO 3166-1 alpha-2
    *
    * ISO 639-1
    *
    * ISO 639-2
    *
    * ISO 639-3
    *
    * ISO 639-3目前仅支持英文和德文。
    */
    standard?: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeQueryCustomizeConfigRequest(v: QueryCustomizeConfigRequest): QueryCustomizeConfigRequest {
    if (v) {
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeQueryCustomizeConfigRequests(vs: QueryCustomizeConfigRequest[]): QueryCustomizeConfigRequest[] {
    if (vs) {
        return vs.map((v: QueryCustomizeConfigRequest) => {
            return safeQueryCustomizeConfigRequest(v);
        });
    } else {
        return [];
    }
}

export interface QueryCustomizeConfigResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 配置参数列表。
    */
    configurations?: Configuration[];

    /**
    * 内容级别体系。
    */
    ratings?: RatingSystem[];

    /**
    * 可用语种列表。
    */
    languages?: Language[];

    /**
    * ISO编码列表。
    */
    ISOCodes?: ISOCode[];

    /**
    * 对应的货币类型字母编码，参考ISO 4217。
    *
    * 取值范围：
    *
    * SGD：新加坡
    *
    * THB：泰国
    *
    * PHP：菲律宾
    *
    * USD：美国
    *
    * CNY：中国
    */
    currencyAlphCode?: string;

    /**
    * 货币最小单位与展示单位之间的转换率。
    *
    * 默认值为100。
    */
    currencyRate?: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeQueryCustomizeConfigResponse(v: QueryCustomizeConfigResponse): QueryCustomizeConfigResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.configurations = safeConfigurations(v.configurations);
        v.ratings = safeRatingSystems(v.ratings);
        v.languages = safeLanguages(v.languages);
        v.ISOCodes = safeISOCodes(v.ISOCodes);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeQueryCustomizeConfigResponses(vs: QueryCustomizeConfigResponse[]): QueryCustomizeConfigResponse[] {
    if (vs) {
        return vs.map((v: QueryCustomizeConfigResponse) => {
            return safeQueryCustomizeConfigResponse(v);
        });
    } else {
        return [];
    }
}

export interface QueryTemplateRequest {

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeQueryTemplateRequest(v: QueryTemplateRequest): QueryTemplateRequest {
    if (v) {
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeQueryTemplateRequests(vs: QueryTemplateRequest[]): QueryTemplateRequest[] {
    if (vs) {
        return vs.map((v: QueryTemplateRequest) => {
            return safeQueryTemplateRequest(v);
        });
    } else {
        return [];
    }
}

export interface QueryTemplateResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 如果查询成功，返回可用的模板总记录数。
    */
    total?: string;

    /**
    * 如果查询成功，返回模板列表。其中数组的第一条记录为当前用户正在使用的模板信息。
    */
    templates?: Template[];

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeQueryTemplateResponse(v: QueryTemplateResponse): QueryTemplateResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.templates = safeTemplates(v.templates);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeQueryTemplateResponses(vs: QueryTemplateResponse[]): QueryTemplateResponse[] {
    if (vs) {
        return vs.map((v: QueryTemplateResponse) => {
            return safeQueryTemplateResponse(v);
        });
    } else {
        return [];
    }
}

export interface QueryLocationRequest {

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeQueryLocationRequest(v: QueryLocationRequest): QueryLocationRequest {
    if (v) {
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeQueryLocationRequests(vs: QueryLocationRequest[]): QueryLocationRequest[] {
    if (vs) {
        return vs.map((v: QueryLocationRequest) => {
            return safeQueryLocationRequest(v);
        });
    } else {
        return [];
    }
}

export interface QueryLocationResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 用户地理位置，两位的国家码(遵从ISO 3166-1)
    */
    location?: string;

    /**
    * 用户IPv4或者IPv6地址，查询成功后返回。
    */
    ip?: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeQueryLocationResponse(v: QueryLocationResponse): QueryLocationResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeQueryLocationResponses(vs: QueryLocationResponse[]): QueryLocationResponse[] {
    if (vs) {
        return vs.map((v: QueryLocationResponse) => {
            return safeQueryLocationResponse(v);
        });
    } else {
        return [];
    }
}

export interface GetRelatedContentRequest {

    /**
    * 内容ID
    */
    contentID?: string;

    /**
    * 内容类型，取值范围：
    *
    * VOD：点播内容，包括音频和视频内容
    *
    * CHANNEL：直播频道
    */
    contentType: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeGetRelatedContentRequest(v: GetRelatedContentRequest): GetRelatedContentRequest {
    if (v) {
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeGetRelatedContentRequests(vs: GetRelatedContentRequest[]): GetRelatedContentRequest[] {
    if (vs) {
        return vs.map((v: GetRelatedContentRequest) => {
            return safeGetRelatedContentRequest(v);
        });
    } else {
        return [];
    }
}

export interface GetRelatedContentResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 固移融合关联内容列表
    */
    contents?: Content[];

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeGetRelatedContentResponse(v: GetRelatedContentResponse): GetRelatedContentResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.contents = safeContents(v.contents);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeGetRelatedContentResponses(vs: GetRelatedContentResponse[]): GetRelatedContentResponse[] {
    if (vs) {
        return vs.map((v: GetRelatedContentResponse) => {
            return safeGetRelatedContentResponse(v);
        });
    } else {
        return [];
    }
}

export interface AuthenticateSDKLicenseRequest {

    /**
    * 待认证的License。只使用licenseType。不能为空，因为服务端不知道SDK对应的License类型。
    */
    licenseItem: LicenseItem[];

    /**
    * 扩展信息
    */
    extensionFields?: NamedParameter[];
}

function safeAuthenticateSDKLicenseRequest(v: AuthenticateSDKLicenseRequest): AuthenticateSDKLicenseRequest {
    if (v) {
        v.licenseItem = safeLicenseItems(v.licenseItem);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeAuthenticateSDKLicenseRequests(vs: AuthenticateSDKLicenseRequest[]): AuthenticateSDKLicenseRequest[] {
    if (vs) {
        return vs.map((v: AuthenticateSDKLicenseRequest) => {
            return safeAuthenticateSDKLicenseRequest(v);
        });
    } else {
        return [];
    }
}

export interface AuthenticateSDKLicenseResponse {

    /**
    * 返回结果
    */
    result: Result;

    /**
    * 认证的License
    */
    licenseItem?: LicenseItem[];

    /**
    * 扩展信息
    */
    extensionFields?: NamedParameter[];
}

function safeAuthenticateSDKLicenseResponse(v: AuthenticateSDKLicenseResponse): AuthenticateSDKLicenseResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.licenseItem = safeLicenseItems(v.licenseItem);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeAuthenticateSDKLicenseResponses(vs: AuthenticateSDKLicenseResponse[]): AuthenticateSDKLicenseResponse[] {
    if (vs) {
        return vs.map((v: AuthenticateSDKLicenseResponse) => {
            return safeAuthenticateSDKLicenseResponse(v);
        });
    } else {
        return [];
    }
}

export interface CastContents {

    /**
    * 推荐记录总数。
    *
    * 若实际条数少于请求推荐的条目数，则返回实际条目数。
    */
    total: string;

    /**
    * 演职员信息。
    */
    cast: Cast;

    /**
    * 点播内容描述，当推荐内容类型为VOD时有效。
    */
    recmVODs?: VOD[];

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeCastContents(v: CastContents): CastContents {
    if (v) {
        v.cast = safeCast(v.cast);
        v.recmVODs = safeVODs(v.recmVODs);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeCastContentss(vs: CastContents[]): CastContents[] {
    if (vs) {
        return vs.map((v: CastContents) => {
            return safeCastContents(v);
        });
    } else {
        return [];
    }
}

export interface ContentRec {

    /**
    * 推荐条件的影片ID，比如收藏或者书签的影片ID。
    */
    contentID?: string;

    /**
    * 推荐条件的影片名称，比如收藏或者书签的影片名称。
    */
    contentName?: string;

    /**
    * 推荐出来的影片列表。
    */
    recList: VOD[];

    /**
    * 推荐流水号。在调用推荐失败，VSP会补齐recList，但是这种情况下没有推荐流水号。
    */
    recmActionID?: string;
}

function safeContentRec(v: ContentRec): ContentRec {
    if (v) {
        v.recList = safeVODs(v.recList);
    }
    return v;
}

function safeContentRecs(vs: ContentRec[]): ContentRec[] {
    if (vs) {
        return vs.map((v: ContentRec) => {
            return safeContentRec(v);
        });
    } else {
        return [];
    }
}

export interface HomeData {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 当type&#x3D;13时，返回该值。
    */
    channelList?: Channel[];

    /**
    * 当type&#x3D;3、5、6、7、11、12、14返回该值。
    */
    vodList?: VOD[];

    /**
    * 当type&#x3D;8，返回该值。
    */
    playbillList?: Playbill[];

    /**
    * 当type&#x3D;9、10时，返回该值。
    */
    playbillLiteList?: PlaybillLite[];

    /**
    * 当type&#x3D;1、2时，返回该值。
    */
    subjectList?: Subject[];

    /**
    * 当type&#x3D;4时，返回该值。
    */
    channelDetailList?: ChannelDetail[];

    /**
    * 当type&#x3D;1、2、3、4、6、14时，返回入参subjectID对应的栏目详情。
    */
    subject?: Subject;

    /**
    * 当type&#x3D;5、7、9、10，返回该值。
    */
    recmActionID?: string;
}

function safeHomeData(v: HomeData): HomeData {
    if (v) {
        v.result = safeResult(v.result);
        v.channelList = safeChannels(v.channelList);
        v.vodList = safeVODs(v.vodList);
        v.playbillList = safePlaybills(v.playbillList);
        v.playbillLiteList = safePlaybillLites(v.playbillLiteList);
        v.subjectList = safeSubjects(v.subjectList);
        v.channelDetailList = safeChannelDetails(v.channelDetailList);
        v.subject = safeSubject(v.subject);
    }
    return v;
}

function safeHomeDatas(vs: HomeData[]): HomeData[] {
    if (vs) {
        return vs.map((v: HomeData) => {
            return safeHomeData(v);
        });
    } else {
        return [];
    }
}

export interface HomeParam {

    /**
    * 动态数据类型枚举
    */
    type: string;

    /**
    * List中需要的内容个数
    */
    count: string;

    /**
    * 扩展信息。
    *
    * 当入参type&#x3D;1、2、3、4、6、14时，该扩展下需要有key为“subjectID”，值为type对应的栏目ID；
    *
    * 否则为空。
    */
    extensionFields?: NamedParameter[];
}

function safeHomeParam(v: HomeParam): HomeParam {
    if (v) {
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeHomeParams(vs: HomeParam[]): HomeParam[] {
    if (vs) {
        return vs.map((v: HomeParam) => {
            return safeHomeParam(v);
        });
    } else {
        return [];
    }
}

export interface LiveTVHomeSubject {

    /**
    * 栏目ID。对于Hot genres的栏目为平台配置的栏目，对于推荐的栏目：what’s on填写-1，Happy Next填写-2。
    */
    ID: string;

    /**
    * 栏目名称。对于Hot genres的栏目为平台配置的栏目，对于推荐的栏目。直播推荐填写What’s On，即将播放的节目单推荐填写Happy Next。
    */
    name: string;

    /**
    * 栏目类型。
    *
    * 取值范围：
    *
    * VOD：点播
    *
    * AUDIO_VOD：音频点播
    *
    * VIDEO_VOD：视频点播
    *
    * CREDIT_VOD：广告内容
    *
    * CHANNEL：频道
    *
    * AUDIO_CHANNEL：音频频道
    *
    * VIDEO_CHANNEL：视频频道
    *
    * WEB_CHANNEL：页面频道
    *
    * MIX：混合
    *
    * VAS：增值业务
    *
    * PROGRAM：节目单
    */
    type: string;

    /**
    * 推荐请求流水号。
    *
    * 字段由推荐系统产生返回给VSP透传，用于唯一标记某个内容推荐请求操作，用于后续与用户推荐结果反馈（如点击/观看推荐本次推荐结果的某个内容）关联。
    *
    * 对于直播节目单推荐和即将播放的节目单的推荐有效。
    */
    recmActionID: string;

    /**
    * 栏目的节目单列表。
    */
    channelPlaybillList: ChannelPlaybill[];
}

function safeLiveTVHomeSubject(v: LiveTVHomeSubject): LiveTVHomeSubject {
    if (v) {
        v.channelPlaybillList = safeChannelPlaybills(v.channelPlaybillList);
    }
    return v;
}

function safeLiveTVHomeSubjects(vs: LiveTVHomeSubject[]): LiveTVHomeSubject[] {
    if (vs) {
        return vs.map((v: LiveTVHomeSubject) => {
            return safeLiveTVHomeSubject(v);
        });
    } else {
        return [];
    }
}

export interface RecmPlaybill {

    /**
    * 推荐请求流水号。
    */
    recmActionID?: string;

    /**
    * 推荐直播或未来节目单。
    */
    channelPlaybill?: ChannelPlaybill[];
}

function safeRecmPlaybill(v: RecmPlaybill): RecmPlaybill {
    if (v) {
        v.channelPlaybill = safeChannelPlaybills(v.channelPlaybill);
    }
    return v;
}

function safeRecmPlaybills(vs: RecmPlaybill[]): RecmPlaybill[] {
    if (vs) {
        return vs.map((v: RecmPlaybill) => {
            return safeRecmPlaybill(v);
        });
    } else {
        return [];
    }
}

export interface RecSubject {

    /**
    * 该栏目详情，参考平台的Subject的定义。
    */
    subject?: Subject;

    /**
    * 专题栏目下的VOD列表。
    */
    VODList?: VOD[];
}

function safeRecSubject(v: RecSubject): RecSubject {
    if (v) {
        v.subject = safeSubject(v.subject);
        v.VODList = safeVODs(v.VODList);
    }
    return v;
}

function safeRecSubjects(vs: RecSubject[]): RecSubject[] {
    if (vs) {
        return vs.map((v: RecSubject) => {
            return safeRecSubject(v);
        });
    } else {
        return [];
    }
}

export interface SubjectGroup {

    /**
    * 父栏目ID。
    */
    subjectID?: string;

    /**
    * 该父栏目的下的子栏目列表。
    */
    subjectList?: Subject[];
}

function safeSubjectGroup(v: SubjectGroup): SubjectGroup {
    if (v) {
        v.subjectList = safeSubjects(v.subjectList);
    }
    return v;
}

function safeSubjectGroups(vs: SubjectGroup[]): SubjectGroup[] {
    if (vs) {
        return vs.map((v: SubjectGroup) => {
            return safeSubjectGroup(v);
        });
    } else {
        return [];
    }
}

export interface SubjectIDList {

    /**
    * 栏目ID。
    */
    subjectID: string;

    /**
    * 该栏目下请求的内容数目。
    */
    count: string;
}

function safeSubjectIDList(v: SubjectIDList): SubjectIDList {
    if (v) {
    }
    return v;
}

function safeSubjectIDLists(vs: SubjectIDList[]): SubjectIDList[] {
    if (vs) {
        return vs.map((v: SubjectIDList) => {
            return safeSubjectIDList(v);
        });
    } else {
        return [];
    }
}

export interface SubjectVodsList {

    /**
    * VOD栏目ID。
    */
    subjectID?: string;

    /**
    * VOD内容列表。
    */
    vodList?: VOD[];
}

function safeSubjectVodsList(v: SubjectVodsList): SubjectVodsList {
    if (v) {
        v.vodList = safeVODs(v.vodList);
    }
    return v;
}

function safeSubjectVodsLists(vs: SubjectVodsList[]): SubjectVodsList[] {
    if (vs) {
        return vs.map((v: SubjectVodsList) => {
            return safeSubjectVodsList(v);
        });
    } else {
        return [];
    }
}

export interface QueryVodHomeDataRequest {

    /**
    * VOD根栏目ID，或者4K根栏目ID。
    *
    * 平台配置跟栏目ID，认证或getcustomconfig接口中获取。
    */
    rootSubjectID: string;

    /**
    * 查询当前栏目下的子栏目和内容。
    */
    subjectID?: string;

    /**
    * 本地保存的栏目ID次序。
    *
    * 有值时，需要服务端对一级栏目排序；
    *
    * 为空时，服务端不用排序，不返回subjectIDList。
    */
    subjectIDList?: string[];

    /**
    * Banner栏目展示个数。
    *
    * 默认值为3。
    */
    bannerCount?: string;

    /**
    * 推荐流派ID，多选用逗号分隔。
    */
    genreID?: string;

    /**
    * 一次查询的子栏目的总条数，不能设置为-1，调用者一定要指定获取数据的总条数，最大条数默认不超过50，最大条数可配置，超过最大条数返回错误。
    *
    * 默认值为4。
    */
    firstVODCount?: string;

    /**
    * 一次查询的子栏目中VOD的总条数，不能设置为-1，调用者一定要指定获取数据的总条数，最大条数默认不超过50，最大条数可配置，超过最大条数返回错误。
    *
    * 默认值为4。
    */
    VODCount?: string;

    /**
    * 如果此栏目为非叶子栏目，一次取栏目列表数量，不包括banner。
    */
    count: string;

    /**
    * 如果此栏目为叶子栏目，一次取VOD列表数量。
    */
    childrenVODCount: string;
}

function safeQueryVodHomeDataRequest(v: QueryVodHomeDataRequest): QueryVodHomeDataRequest {
    if (v) {
        v.subjectIDList = v.subjectIDList || [];
    }
    return v;
}

function safeQueryVodHomeDataRequests(vs: QueryVodHomeDataRequest[]): QueryVodHomeDataRequest[] {
    if (vs) {
        return vs.map((v: QueryVodHomeDataRequest) => {
            return safeQueryVodHomeDataRequest(v);
        });
    } else {
        return [];
    }
}

export interface QueryVodHomeDataResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 子栏目总数或vod总数
    *
    * 栏目为叶子栏目的时候返回vod总数
    *
    * 栏目为非叶子栏目的时候返回子栏目总数
    */
    countTotal: string;

    /**
    * VOD栏目列表，如果查询结果为空，不返回该参数。
    */
    subjectList: Subject[];

    /**
    * 只有在请求参数有bannerSubjectID的情况下才返回。
    */
    bannerVODs?: VOD[];

    /**
    * 子栏目及栏目下VOD列表信息，如果查询结果为空，不返回该参数。
    */
    subjectDetailList?: SubjectVODList[];

    /**
    * 如果栏目为叶子栏目，返回VOD列表值。
    */
    childrenVODs?: VOD[];

    /**
    * 推荐热门流派名称。
    */
    genreName?: string[];
}

function safeQueryVodHomeDataResponse(v: QueryVodHomeDataResponse): QueryVodHomeDataResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.subjectList = safeSubjects(v.subjectList);
        v.bannerVODs = safeVODs(v.bannerVODs);
        v.subjectDetailList = safeSubjectVODLists(v.subjectDetailList);
        v.childrenVODs = safeVODs(v.childrenVODs);
        v.genreName = v.genreName || [];
    }
    return v;
}

function safeQueryVodHomeDataResponses(vs: QueryVodHomeDataResponse[]): QueryVodHomeDataResponse[] {
    if (vs) {
        return vs.map((v: QueryVodHomeDataResponse) => {
            return safeQueryVodHomeDataResponse(v);
        });
    } else {
        return [];
    }
}

export interface VodHomeRequest {

    /**
    * 本地保存的栏目ID次序。
    */
    subjectIDList: string[];

    /**
    * VOD根栏目ID。
    *
    * 平台配置根栏目ID，认证或getcustomconfig接口中获取。
    */
    subjectID?: string;

    /**
    * PAD专用，PAD的banner区域的栏目ID，认证时从终端参数获取。
    */
    bannerSubjectID?: string;

    /**
    * PAD专用，PAD的banner区域展示的图片数量。
    */
    bannerCount?: string;

    /**
    * 一次查询的子栏目的总条数，不能设置为-1，调用者一定要指定获取数据的总条数，最大条数默认不超过50，最大条数可配置，超过最大条数返回错误。
    */
    subjectCount: string;

    /**
    * 一次查询的子栏目中VOD的总条数，不能设置为-1，调用者一定要指定获取数据的总条数，最大条数默认不超过50，最大条数可配置，超过最大条数返回错误。
    */
    VODCount: string;
}

function safeVodHomeRequest(v: VodHomeRequest): VodHomeRequest {
    if (v) {
        v.subjectIDList = v.subjectIDList || [];
    }
    return v;
}

function safeVodHomeRequests(vs: VodHomeRequest[]): VodHomeRequest[] {
    if (vs) {
        return vs.map((v: VodHomeRequest) => {
            return safeVodHomeRequest(v);
        });
    } else {
        return [];
    }
}

export interface VodHomeResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 子栏目总数。
    */
    countTotal?: string;

    /**
    * VOD栏目列表，VOD栏目属性请参见“Subject”类型。
    *
    * 如果查询结果为空，不返回该参数。
    */
    subjectList?: Subject[];

    /**
    * 只有在请求参数有bannerSubjectID的情况下才返回。
    */
    bannerVODs?: VOD[];

    /**
    * 推荐Banner VOD列表信息。
    *
    * VOD列表信息，如果查询结果为空，不返回该参数。
    */
    rankVODs?: VOD[];

    /**
    * 子栏目及栏目下VOD列表信息，如果查询结果为空，不返回该参数。
    */
    subjectDetailList?: SubjectVODList[];
}

function safeVodHomeResponse(v: VodHomeResponse): VodHomeResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.subjectList = safeSubjects(v.subjectList);
        v.bannerVODs = safeVODs(v.bannerVODs);
        v.rankVODs = safeVODs(v.rankVODs);
        v.subjectDetailList = safeSubjectVODLists(v.subjectDetailList);
    }
    return v;
}

function safeVodHomeResponses(vs: VodHomeResponse[]): VodHomeResponse[] {
    if (vs) {
        return vs.map((v: VodHomeResponse) => {
            return safeVodHomeResponse(v);
        });
    } else {
        return [];
    }
}

export interface QueryHomeDataRequest {

    /**
    * 栏目ID。
    *
    * 如果客户端设置此参数，则返回指定栏目ID下的VOD列表。
    */
    bannerSubjectID?: string;

    /**
    * 需要和参数“bannerSubjectID”同时设置，用于限制返回的VOD数量。
    */
    bannerDataCount?: string;

    /**
    * 限制动态推荐返回的内容数量，包括：
    *
    * 用户偏好推荐返回的VOD数量
    *
    * 用户热门推荐返回的VOD数量
    */
    recmDataCount: string;

    /**
    * 限制用户偏好推荐返回的Catch-up TV数量。
    */
    hotTVCount: string;

    /**
    * 专题栏目的栏目ID，如果客户端设置此参数，则返回指定栏目ID栏目详情，以栏目下的VOD列表。可以指定多个栏目，以逗号分隔。
    */
    subjectID?: string;

    /**
    * 限制返回某个栏目下的VOD数量，该栏目为参数“subjectID”指定的栏目。
    */
    subjectDataCount?: string;
}

function safeQueryHomeDataRequest(v: QueryHomeDataRequest): QueryHomeDataRequest {
    if (v) {
    }
    return v;
}

function safeQueryHomeDataRequests(vs: QueryHomeDataRequest[]): QueryHomeDataRequest[] {
    if (vs) {
        return vs.map((v: QueryHomeDataRequest) => {
            return safeQueryHomeDataRequest(v);
        });
    } else {
        return [];
    }
}

export interface QueryHomeDataResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 返回热门的搜索关键字。
    */
    hotKeyword?: string;

    /**
    * 返回由参数“bannerCategoryID”指定的栏目下VOD列表。
    */
    banner?: VOD[];

    /**
    * 返回用户偏好推荐的流水号。
    */
    topRecmActionID?: string;

    /**
    * 返回用户偏好推荐返回的VOD列表。
    *
    * 对应接口为VSP/V3/QueryRecmContent。
    *
    * 对应参数为：
    *
    * recmType：2
    *
    * contentType： VOD
    *
    * businessType：1
    */
    topPicksForYou?: VOD[];

    /**
    * 返回用户热门推荐返回的VOD列表。
    *
    * 对应接口为VSP/V3/QueryRecmVODList/。
    *
    * action传5。
    */
    everyoneIsWatching?: VOD[];

    /**
    * 返回用户热门推荐返回的Catch-up TV列表。
    *
    * 对应接口为VSP/V3/QueryHotPlaybill。
    */
    hotTVShow?: Playbill[];

    /**
    * 返回由参数“subjectID”指定的栏目信息以及栏目下的VOD列表。
    */
    recSubject?: RecSubject[];
}

function safeQueryHomeDataResponse(v: QueryHomeDataResponse): QueryHomeDataResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.banner = safeVODs(v.banner);
        v.topPicksForYou = safeVODs(v.topPicksForYou);
        v.everyoneIsWatching = safeVODs(v.everyoneIsWatching);
        v.hotTVShow = safePlaybills(v.hotTVShow);
        v.recSubject = safeRecSubjects(v.recSubject);
    }
    return v;
}

function safeQueryHomeDataResponses(vs: QueryHomeDataResponse[]): QueryHomeDataResponse[] {
    if (vs) {
        return vs.map((v: QueryHomeDataResponse) => {
            return safeQueryHomeDataResponse(v);
        });
    } else {
        return [];
    }
}

export interface QueryOTTLiveTVHomeDataRequest {

    /**
    * 当前时间，取值为距离1970年1月1号的毫秒数。
    */
    date: string;

    /**
    * 各个栏目返回的频道的最大值。
    */
    count: string;

    /**
    * What&#x27;s on栏目需要返回的频道数目，如不填写默认和count取值相同。
    */
    whatsOnCount?: string;

    /**
    * Happy Next栏目需要返回的频道数目，如不填写默认和count取值相同。
    */
    happyNextCount?: string;

    /**
    * 热门栏目返回的最大栏目数。如果传入的channelSubjectID栏目的数目大于subjectCount，则取subjectCount个栏目返回。
    */
    subjectCount: string;

    /**
    * 要查询的channel栏目的ID列表，用逗号分隔栏目ID。如果不传则表示获取根栏目下的所有一级栏目。
    */
    channelSubjectID?: string;
}

function safeQueryOTTLiveTVHomeDataRequest(v: QueryOTTLiveTVHomeDataRequest): QueryOTTLiveTVHomeDataRequest {
    if (v) {
    }
    return v;
}

function safeQueryOTTLiveTVHomeDataRequests(vs: QueryOTTLiveTVHomeDataRequest[]): QueryOTTLiveTVHomeDataRequest[] {
    if (vs) {
        return vs.map((v: QueryOTTLiveTVHomeDataRequest) => {
            return safeQueryOTTLiveTVHomeDataRequest(v);
        });
    } else {
        return [];
    }
}

export interface QueryOTTLiveTVHomeDataResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 栏目。
    */
    liveTVHomeSubjectList: LiveTVHomeSubject[];
}

function safeQueryOTTLiveTVHomeDataResponse(v: QueryOTTLiveTVHomeDataResponse): QueryOTTLiveTVHomeDataResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.liveTVHomeSubjectList = safeLiveTVHomeSubjects(v.liveTVHomeSubjectList);
    }
    return v;
}

function safeQueryOTTLiveTVHomeDataResponses(vs: QueryOTTLiveTVHomeDataResponse[]): QueryOTTLiveTVHomeDataResponse[] {
    if (vs) {
        return vs.map((v: QueryOTTLiveTVHomeDataResponse) => {
            return safeQueryOTTLiveTVHomeDataResponse(v);
        });
    } else {
        return [];
    }
}

export interface QueryMyTVHomeDataRequest {
}

function safeQueryMyTVHomeDataRequest(v: QueryMyTVHomeDataRequest): QueryMyTVHomeDataRequest {
    if (v) {
    }
    return v;
}

function safeQueryMyTVHomeDataRequests(vs: QueryMyTVHomeDataRequest[]): QueryMyTVHomeDataRequest[] {
    if (vs) {
        return vs.map((v: QueryMyTVHomeDataRequest) => {
            return safeQueryMyTVHomeDataRequest(v);
        });
    } else {
        return [];
    }
}

export interface QueryMyTVHomeDataResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 收藏频道的数目。
    */
    favoriteChannelCount?: string;

    /**
    * 提醒的总数
    */
    reminderCount?: string;

    /**
    * 购买产品数。
    */
    purchasedCount?: string;

    /**
    * 观看的VOD数。
    */
    vodHistoryCount?: string;

    /**
    * 收藏VOD数。
    */
    favoriteVodCount?: string;

    /**
    * Profile总数。
    */
    profileCount?: string;

    /**
    * 历史观看的VOD列表。
    */
    vodHistoryList?: VOD[];
}

function safeQueryMyTVHomeDataResponse(v: QueryMyTVHomeDataResponse): QueryMyTVHomeDataResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.vodHistoryList = safeVODs(v.vodHistoryList);
    }
    return v;
}

function safeQueryMyTVHomeDataResponses(vs: QueryMyTVHomeDataResponse[]): QueryMyTVHomeDataResponse[] {
    if (vs) {
        return vs.map((v: QueryMyTVHomeDataResponse) => {
            return safeQueryMyTVHomeDataResponse(v);
        });
    } else {
        return [];
    }
}

export interface QueryMoreRecommendRequest {

    /**
    * 最多获取推荐影片的数量，不传默认为12。
    */
    recommendCount?: string;
}

function safeQueryMoreRecommendRequest(v: QueryMoreRecommendRequest): QueryMoreRecommendRequest {
    if (v) {
    }
    return v;
}

function safeQueryMoreRecommendRequests(vs: QueryMoreRecommendRequest[]): QueryMoreRecommendRequest[] {
    if (vs) {
        return vs.map((v: QueryMoreRecommendRequest) => {
            return safeQueryMoreRecommendRequest(v);
        });
    } else {
        return [];
    }
}

export interface QueryMoreRecommendResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 根据用户偏好获取的推荐列表。
    *
    * 返回用户偏好推荐返回的VOD列表。
    *
    * 对应接口为：VSP/V3/QueryRecmContent。
    *
    * 对应参数为：
    *
    * entrance：VOD_Top_Picks_For_You
    *
    * contentType：VOD
    *
    * businessType：VOD
    */
    topPicksForYou?: ContentRec[];

    /**
    * 根据书签获取的推荐列表。
    *
    * 返回用户more lice返回的VOD列表。
    *
    * 对应接口为：VSP/V3/QueryRecmContent。
    *
    * 对应参数为：
    *
    * entrance： VOD_More_Like
    *
    * contentType： VOD
    *
    * businessType：VOD
    */
    moreLike?: ContentRec[];

    /**
    * 根据收藏获取的推荐列表。
    *
    * 返回用户more like返回的VOD列表。
    *
    * 对应接口为：VSP/V3/QueryRecmContent。
    *
    * 对应参数为：
    *
    * entrance： VOD_Because_You_Like
    *
    * contentType： VOD
    *
    * businessType：VOD
    */
    youLike?: ContentRec[];
}

function safeQueryMoreRecommendResponse(v: QueryMoreRecommendResponse): QueryMoreRecommendResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.topPicksForYou = safeContentRecs(v.topPicksForYou);
        v.moreLike = safeContentRecs(v.moreLike);
        v.youLike = safeContentRecs(v.youLike);
    }
    return v;
}

function safeQueryMoreRecommendResponses(vs: QueryMoreRecommendResponse[]): QueryMoreRecommendResponse[] {
    if (vs) {
        return vs.map((v: QueryMoreRecommendResponse) => {
            return safeQueryMoreRecommendResponse(v);
        });
    } else {
        return [];
    }
}

export interface QueryEpgHomeRecmPlaybillRequest {

    /**
    * 热播节目单需返回的推荐数目。
    */
    recmWhatsOnCount: string;

    /**
    * 即将热播的节目单需返回的推荐数目。
    */
    recmHappyNextCount: string;

    /**
    * 推荐返回的Catch-up TV数量。
    */
    hotTVCount: string;
}

function safeQueryEpgHomeRecmPlaybillRequest(v: QueryEpgHomeRecmPlaybillRequest): QueryEpgHomeRecmPlaybillRequest {
    if (v) {
    }
    return v;
}

function safeQueryEpgHomeRecmPlaybillRequests(vs: QueryEpgHomeRecmPlaybillRequest[]): QueryEpgHomeRecmPlaybillRequest[] {
    if (vs) {
        return vs.map((v: QueryEpgHomeRecmPlaybillRequest) => {
            return safeQueryEpgHomeRecmPlaybillRequest(v);
        });
    } else {
        return [];
    }
}

export interface QueryEpgHomeRecmPlaybillResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 返回用户热门推荐返回的Catch-up TV列表。
    */
    hotTVShow?: Playbill[];

    /**
    * 推荐的直播节目单。
    */
    recmWhatsOn?: RecmPlaybill;

    /**
    * 推荐的未来节目单。
    */
    recmHappyNext?: RecmPlaybill;
}

function safeQueryEpgHomeRecmPlaybillResponse(v: QueryEpgHomeRecmPlaybillResponse): QueryEpgHomeRecmPlaybillResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.hotTVShow = safePlaybills(v.hotTVShow);
        v.recmWhatsOn = safeRecmPlaybill(v.recmWhatsOn);
        v.recmHappyNext = safeRecmPlaybill(v.recmHappyNext);
    }
    return v;
}

function safeQueryEpgHomeRecmPlaybillResponses(vs: QueryEpgHomeRecmPlaybillResponse[]): QueryEpgHomeRecmPlaybillResponse[] {
    if (vs) {
        return vs.map((v: QueryEpgHomeRecmPlaybillResponse) => {
            return safeQueryEpgHomeRecmPlaybillResponse(v);
        });
    } else {
        return [];
    }
}

export interface QueryEpgHomeVodRequest {

    /**
    * 协同推荐返回数量。数量大于等于0
    */
    everyoneIsWatchingCount: string;

    /**
    * 用户偏好推荐的数量。数量大于等于0
    */
    topPicksForYouCount: string;

    /**
    * VOD栏目请求。要同时获取这些栏目下的VOD列表
    */
    subjectIDList?: SubjectIDList[];

    /**
    * 该参数对于QueryVodListBySubject接口有效。
    *
    * 表示VOD的排序方式。以下列方式指定排序方式：
    *
    * STARTIME:ASC：按VOD上线时间升序排序，STARTIME:DESC：按VOD上线时间降序排序
    *
    * AVGSCORE:ASC：按VOD评分均值升序排序，AVGSCORE:DESC：按VOD评分均值降序排序
    *
    * PLAYTIMES:ASC：按VOD点击率升序排序，PLAYTIMES:DESC：按VOD点击率降序排序
    *
    * CNTARRANGE：按栏目下内容编排顺序排序，仅对subjectid表示的栏目是叶子栏目时才有效
    *
    * VODNAME:ASC：按VOD名称字典升序排序，VODNAME:DESC：按VOD名称字典降序排序
    *
    * 备注：
    *
    * 当subjectID为叶子栏目且sortType未携带，sortType默认取CNTARRANGE:ASC。
    *
    * 如果subjectID非叶子栏目且sortType未携带，sortType默认取STARTIME:DESC。
    */
    VODSortType?: string;
}

function safeQueryEpgHomeVodRequest(v: QueryEpgHomeVodRequest): QueryEpgHomeVodRequest {
    if (v) {
        v.subjectIDList = safeSubjectIDLists(v.subjectIDList);
    }
    return v;
}

function safeQueryEpgHomeVodRequests(vs: QueryEpgHomeVodRequest[]): QueryEpgHomeVodRequest[] {
    if (vs) {
        return vs.map((v: QueryEpgHomeVodRequest) => {
            return safeQueryEpgHomeVodRequest(v);
        });
    } else {
        return [];
    }
}

export interface QueryEpgHomeVodResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 返回用户偏好推荐的流水号。
    */
    topRecmActionID?: string;

    /**
    * 返回用户偏好推荐返回的VOD列表。
    */
    topPicksForYou?: VOD[];

    /**
    * 返回用户热门推荐返回的VOD列表。
    */
    everyoneIsWatching?: VOD[];

    /**
    * VOD栏目内容列表。如果请求参数subjectIDList的为空，则不返回该字段。
    */
    subjectVodLists?: SubjectVodsList[];
}

function safeQueryEpgHomeVodResponse(v: QueryEpgHomeVodResponse): QueryEpgHomeVodResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.topPicksForYou = safeVODs(v.topPicksForYou);
        v.everyoneIsWatching = safeVODs(v.everyoneIsWatching);
        v.subjectVodLists = safeSubjectVodsLists(v.subjectVodLists);
    }
    return v;
}

function safeQueryEpgHomeVodResponses(vs: QueryEpgHomeVodResponse[]): QueryEpgHomeVodResponse[] {
    if (vs) {
        return vs.map((v: QueryEpgHomeVodResponse) => {
            return safeQueryEpgHomeVodResponse(v);
        });
    } else {
        return [];
    }
}

export interface QueryEpgHomeSubjectsRequest {

    /**
    * 获取该VOD栏目列表下的VOD子栏目。
    */
    vodSubject?: SubjectIDList[];

    /**
    * 获取该频道栏目列表下的子栏目。
    */
    channelSubject?: SubjectIDList[];
}

function safeQueryEpgHomeSubjectsRequest(v: QueryEpgHomeSubjectsRequest): QueryEpgHomeSubjectsRequest {
    if (v) {
        v.vodSubject = safeSubjectIDLists(v.vodSubject);
        v.channelSubject = safeSubjectIDLists(v.channelSubject);
    }
    return v;
}

function safeQueryEpgHomeSubjectsRequests(vs: QueryEpgHomeSubjectsRequest[]): QueryEpgHomeSubjectsRequest[] {
    if (vs) {
        return vs.map((v: QueryEpgHomeSubjectsRequest) => {
            return safeQueryEpgHomeSubjectsRequest(v);
        });
    } else {
        return [];
    }
}

export interface QueryEpgHomeSubjectsResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * VOD子栏目。
    */
    vodSubjectList?: SubjectGroup[];

    /**
    * 频道栏目。
    */
    channelSubjectList?: SubjectGroup[];
}

function safeQueryEpgHomeSubjectsResponse(v: QueryEpgHomeSubjectsResponse): QueryEpgHomeSubjectsResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.vodSubjectList = safeSubjectGroups(v.vodSubjectList);
        v.channelSubjectList = safeSubjectGroups(v.channelSubjectList);
    }
    return v;
}

function safeQueryEpgHomeSubjectsResponses(vs: QueryEpgHomeSubjectsResponse[]): QueryEpgHomeSubjectsResponse[] {
    if (vs) {
        return vs.map((v: QueryEpgHomeSubjectsResponse) => {
            return safeQueryEpgHomeSubjectsResponse(v);
        });
    } else {
        return [];
    }
}

export interface QueryVodSubjectAndVodListRequest {

    /**
    * 父栏目ID，查询此栏目下的子栏目列表。如果为空，标示查询 “-1”根栏目下的栏目列表。
    */
    subjectID?: string;

    /**
    * 栏目类型,具体取值包括：
    *
    * VOD
    *
    * AUDIO_VOD
    *
    * VIDEO_VOD
    *
    * MIXED
    */
    types: string[];

    /**
    * 一次查询的总条数，不能设置为-1，调用者一定要指定获取数据的总条数，最大条数默认不超过50，最大条数可配置，超过最大条数返回错误。
    */
    subjectCount: string;

    /**
    * 一次查询的总条数，不能设置为-1，调用者一定要指定获取数据的总条数，最大条数默认不超过50，最大条数可配置，超过最大条数返回错误。
    */
    VODCount: string;

    /**
    * VOD排序方式。以下列方式指定排序方式：
    *
    * STARTTIME:ASC：按VOD上线时间升序排序，STARTTIME:DESC：按VOD上线时间降序排序
    *
    * AVGSCORE:ASC：按VOD评分均值升序排序，AVGSCORE:DESC：按VOD评分均值降序排序
    *
    * PLAYTIMES:ASC：按VOD点击率升序排序，PLAYTIMES:DESC：按VOD点击率降序排序
    *
    * CNTARRANGE：按栏目下内容编排顺序排序，仅对subjectid表示的栏目是叶子栏目时才有效
    *
    * VODNAME:ASC：按VOD名称字典升序排序，VODNAME:DESC：按VOD名称字典降序排序
    *
    * 说明
    *
    * 当subjectID为叶子栏目且sortType未携带，sortType默认取CNTARRANGE:ASC。
    *
    * 如果subjectID非叶子栏目且sortType未携带，sortType默认取STARTTIME:DESC。
    *
    * 平台不支持对父栏目按照内容自定义的顺序排序，自定义顺序只针对叶子栏目。
    */
    sortType?: string;

    /**
    * 搜索条件。
    *
    * 说明：如果VODFilters和VODExcluders参数中包含相同的过滤条件，则以VODFilters中包含的为准，忽略VODExcluders中的同名过滤条件。
    */
    VODFilters?: VODFilter[];

    /**
    * 排他条件。
    */
    VODExcluders?: VODExcluder[];
}

function safeQueryVodSubjectAndVodListRequest(v: QueryVodSubjectAndVodListRequest): QueryVodSubjectAndVodListRequest {
    if (v) {
        v.types = v.types || [];
        v.VODFilters = safeVODFilters(v.VODFilters);
        v.VODExcluders = safeVODExcluders(v.VODExcluders);
    }
    return v;
}

function safeQueryVodSubjectAndVodListRequests(vs: QueryVodSubjectAndVodListRequest[]): QueryVodSubjectAndVodListRequest[] {
    if (vs) {
        return vs.map((v: QueryVodSubjectAndVodListRequest) => {
            return safeQueryVodSubjectAndVodListRequest(v);
        });
    } else {
        return [];
    }
}

export interface QueryVodSubjectAndVodListResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 子栏目总个数。
    */
    subjectTotal: string;

    /**
    * VOD栏目列表，VOD栏目属性请参见“Subject”类型。
    *
    * 如果查询结果为空，不返回该参数。
    */
    subjects?: Subject[];

    /**
    * VOD总个数。
    */
    VODTotal: string;

    /**
    * VOD列表信息，VOD属性请参见“Subject”类型。
    *
    * 如果查询结果为空，不返回该参数。
    */
    VODs?: VOD[];

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeQueryVodSubjectAndVodListResponse(v: QueryVodSubjectAndVodListResponse): QueryVodSubjectAndVodListResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.subjects = safeSubjects(v.subjects);
        v.VODs = safeVODs(v.VODs);
        v.extensionFields = safeNamedParameters(v.extensionFields);
    }
    return v;
}

function safeQueryVodSubjectAndVodListResponses(vs: QueryVodSubjectAndVodListResponse[]): QueryVodSubjectAndVodListResponse[] {
    if (vs) {
        return vs.map((v: QueryVodSubjectAndVodListResponse) => {
            return safeQueryVodSubjectAndVodListResponse(v);
        });
    } else {
        return [];
    }
}

export interface QueryPCVodHomeRequest {

    /**
    * VOD根栏目ID。
    *
    * 平台配置跟栏目ID，认证或getcustomconfig接口中获取。
    */
    subjectID: string;

    /**
    * Banner栏目ID。
    */
    bannerSubjectID?: string;

    /**
    * banner区域展示VOD的数量。
    */
    bannerVODCount?: string;

    /**
    * 推荐栏目ID列表。
    */
    recmSubjectIDList?: string[];

    /**
    * 查询推荐栏目VOD的数量。
    */
    recmVODCount?: string;

    /**
    * 推荐演职员ID列表，从终端配置参数中获取。
    */
    castIDList?: string[];

    /**
    * 演职员VOD显示数。
    */
    castVODCount?: string;
}

function safeQueryPCVodHomeRequest(v: QueryPCVodHomeRequest): QueryPCVodHomeRequest {
    if (v) {
        v.recmSubjectIDList = v.recmSubjectIDList || [];
        v.castIDList = v.castIDList || [];
    }
    return v;
}

function safeQueryPCVodHomeRequests(vs: QueryPCVodHomeRequest[]): QueryPCVodHomeRequest[] {
    if (vs) {
        return vs.map((v: QueryPCVodHomeRequest) => {
            return safeQueryPCVodHomeRequest(v);
        });
    } else {
        return [];
    }
}

export interface QueryPCVodHomeResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * VOD栏目列表，VOD栏目属性请参见“Subject”类型。
    *
    * 如果查询结果为空，不返回该参数。
    */
    subjectList: Subject[];

    /**
    * 只有在请求参数有bannerSubjectID的情况下才返回。
    */
    bannerVODs?: VOD[];

    /**
    * TOP THIS WEEK,VOD列表信息，如果查询结果为空，不返回该参数。
    */
    rankVODs?: VOD[];

    /**
    * 子栏目及栏目下VOD列表信息，如果查询结果为空，不返回该参数。
    */
    recmSubjectDetailList?: SubjectVODList[];

    /**
    * 推荐演职员和所属的VOD列表。
    */
    castContentsList?: CastContents[];
}

function safeQueryPCVodHomeResponse(v: QueryPCVodHomeResponse): QueryPCVodHomeResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.subjectList = safeSubjects(v.subjectList);
        v.bannerVODs = safeVODs(v.bannerVODs);
        v.rankVODs = safeVODs(v.rankVODs);
        v.recmSubjectDetailList = safeSubjectVODLists(v.recmSubjectDetailList);
        v.castContentsList = safeCastContentss(v.castContentsList);
    }
    return v;
}

function safeQueryPCVodHomeResponses(vs: QueryPCVodHomeResponse[]): QueryPCVodHomeResponse[] {
    if (vs) {
        return vs.map((v: QueryPCVodHomeResponse) => {
            return safeQueryPCVodHomeResponse(v);
        });
    } else {
        return [];
    }
}

export interface QueryAllHomeDataRequest {

    /**
    * 请求对象数组
    */
    request: HomeParam[];
}

function safeQueryAllHomeDataRequest(v: QueryAllHomeDataRequest): QueryAllHomeDataRequest {
    if (v) {
        v.request = safeHomeParams(v.request);
    }
    return v;
}

function safeQueryAllHomeDataRequests(vs: QueryAllHomeDataRequest[]): QueryAllHomeDataRequest[] {
    if (vs) {
        return vs.map((v: QueryAllHomeDataRequest) => {
            return safeQueryAllHomeDataRequest(v);
        });
    } else {
        return [];
    }
}

export interface QueryAllHomeDataResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 返回对象数组
    */
    response: HomeData[];
}

function safeQueryAllHomeDataResponse(v: QueryAllHomeDataResponse): QueryAllHomeDataResponse {
    if (v) {
        v.result = safeResult(v.result);
        v.response = safeHomeDatas(v.response);
    }
    return v;
}

function safeQueryAllHomeDataResponses(vs: QueryAllHomeDataResponse[]): QueryAllHomeDataResponse[] {
    if (vs) {
        return vs.map((v: QueryAllHomeDataResponse) => {
            return safeQueryAllHomeDataResponse(v);
        });
    } else {
        return [];
    }
}

/**
* 平台提供语音识别语义的接口，支持返回语音转文本和语义
*
*
*
*  - 识别直播换台的指令；
*
*  - 识别内容搜索的指令；
*
*  - 识别播放控制的指令；
*
* 提供分片传输录音文件的功能，保证识别响应时延最低。
*/
export function voiceControl(req: VoiceControlRequest, options: any = {}): Promise<VoiceControlResponse> {
    options.method = options.method || 'POST';
    return _sendRequest('VoiceControl', '/VSP/V3/VoiceControl', req, options).then((resp: any) => {
        return safeVoiceControlResponse(resp);
    });
}



export interface CastFromASR {

    /**
    * 角色类型，取值包括：
    *
    * 0：演员
    *
    * 1：导演
    *
    * 2：词曲作者
    *
    * 3：演唱者
    *
    * 4：出品人
    *
    * 5：编剧
    *
    * 6：解说员
    *
    * 7：主办人
    *
    * 8：化妆师
    *
    * 9：音响师
    *
    * 100：其他
    */
    castType: string;

    /**
    * 演职员名称
    */
    castName: string[];
}

function safeCastFromASR(v: CastFromASR): CastFromASR {
    if (v) {
        v.castName = v.castName || [];
    }
    return v;
}

function safeCastFromASRs(vs: CastFromASR[]): CastFromASR[] {
    if (vs) {
        return vs.map((v: CastFromASR) => {
            return safeCastFromASR(v);
        });
    } else {
        return [];
    }
}

export interface IntentObject {

    /**
    * 控制命令，枚举值
    */
    action: string;

    /**
    * 时
    */
    hour?: string;

    /**
    * 分
    */
    minute?: string;

    /**
    * 秒
    */
    second?: string;

    /**
    *
    */
    number?: string;

    /**
    *
    */
    category?: string;

    /**
    *
    */
    value?: string;

    /**
    *
    */
    name?: string;

    /**
    * 内容的演职员信息。
    *
    * 详细参见“CastFromASR”对象
    */
    castFromASR?: CastFromASR[];

    /**
    * 关键字。 对标签，关键字触发搜索
    */
    keyword?: string;

    /**
    * 流派名称
    */
    genreName?: string;

    /**
    * 出品国家/地区。
    */
    produceArea?: string;
}

function safeIntentObject(v: IntentObject): IntentObject {
    if (v) {
        v.castFromASR = safeCastFromASRs(v.castFromASR);
    }
    return v;
}

function safeIntentObjects(vs: IntentObject[]): IntentObject[] {
    if (vs) {
        return vs.map((v: IntentObject) => {
            return safeIntentObject(v);
        });
    } else {
        return [];
    }
}

export interface VoiceControlRequest {

    /**
    * 语音压缩格式
    *
    * pcm
    *
    * speex
    *
    * amr
    *
    * opus
    */
    format: string;

    /**
    * 8000
    *
    * 16000
    */
    rate: string;

    /**
    * 1：单声道
    *
    * 2：双声道
    */
    channel: string;

    /**
    * 音频的语种方言详情。
    *
    * 说明：遵循ISO标准编码表（ISO639-1，ISO639-3部分方言639-1不具备时用639-3标准）。当前支持的语言包括：
    *
    * en（英语）
    *
    * yue（中文-粤语）
    *
    * cmn（中文-普通话）
    */
    language: string;

    /**
    * 音频文件分段录制并传输时进行赋值，要保证唯一性，时间建议使用录音起始时间点
    *
    * 格式：deviceID+YYYYMMDDHHMMSS
    */
    recordID?: string;

    /**
    * 每个分片的编号，递增传送
    *
    * 从1开始
    */
    index?: string;

    /**
    * true:是最后一个分片
    *
    * false:不是最后一个分片
    */
    isLast?: string;

    /**
    * 音频文件Base64后的字符串
    */
    audioData: string;

    /**
    * 扩展信息。
    */
    extensionFields?: NamedParameter[];
}

function safeVoiceControlRequest(v: VoiceControlRequest): VoiceControlRequest {
    if (v) {
        v.extensionFields = v.extensionFields || [];
    }
    return v;
}

function safeVoiceControlRequests(vs: VoiceControlRequest[]): VoiceControlRequest[] {
    if (vs) {
        return vs.map((v: VoiceControlRequest) => {
            return safeVoiceControlRequest(v);
        });
    } else {
        return [];
    }
}

export interface VoiceControlResponse {

    /**
    * 返回结果。
    */
    result: Result;

    /**
    * 语音识别的结果文本，完整句子。
    *
    * 可选字段返回值场景说明：
    *
    * 1.  携带了recordID和isLast&#x3D;true时，返回语义对象
    * 2.  未携带recordID，会返回语义对象
    */
    audioText?: string;

    /**
    * 领域类型，枚举值：
    *
    * videoControl :视频播放控制语义
    *
    * contentControl：视频内容控制语义
    *
    * channelControl：频道切换语义
    */
    domain?: string;

    /**
    * 意图对象，不同领域有不同的意图对象，参见元数据类型中的意图定义
    */
    intentObject?: IntentObject;
}

function safeVoiceControlResponse(v: VoiceControlResponse): VoiceControlResponse {
    if (v) {
        v.intentObject = safeIntentObject(v.intentObject);
    }
    return v;
}

function safeVoiceControlResponses(vs: VoiceControlResponse[]): VoiceControlResponse[] {
    if (vs) {
        return vs.map((v: VoiceControlResponse) => {
            return safeVoiceControlResponse(v);
        });
    } else {
        return [];
    }
}
