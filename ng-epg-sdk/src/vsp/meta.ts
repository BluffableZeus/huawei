// tslint:disable 
 export default {
  "infs": [
    {
      "suffix": "LoginRoute",
      "request": "LoginRouteRequest",
      "response": "LoginRouteResponse"
    },
    {
      "suffix": "Login",
      "request": "LoginRequest",
      "response": "LoginResponse"
    },
    {
      "suffix": "Authenticate",
      "request": "AuthenticateRequest",
      "response": "AuthenticateResponse"
    },
    {
      "suffix": "SwitchProfile",
      "request": "SwitchProfileRequest",
      "response": "SwitchProfileResponse"
    },
    {
      "suffix": "OnLineHeartbeat",
      "request": "OnLineHeartbeatRequest",
      "response": "OnLineHeartbeatResponse"
    },
    {
      "suffix": "Logout",
      "request": "LogoutRequest",
      "response": "LogoutResponse"
    },
    {
      "suffix": "UpdateTimeZone",
      "request": "UpdateTimeZoneRequest",
      "response": "UpdateTimeZoneResponse"
    },
    {
      "suffix": "SendSMS",
      "request": "SendSMSRequest",
      "response": "SendSMSResponse"
    },
    {
      "suffix": "CreateSubscriber",
      "request": "CreateSubscriberRequest",
      "response": "CreateSubscriberResponse"
    },
    {
      "suffix": "AddProfile",
      "request": "AddProfileRequest",
      "response": "AddProfileResponse"
    },
    {
      "suffix": "ModifyProfile",
      "request": "ModifyProfileRequest",
      "response": "ModifyProfileResponse"
    },
    {
      "suffix": "DeleteProfile",
      "request": "DeleteProfileRequest",
      "response": "DeleteProfileResponse"
    },
    {
      "suffix": "QueryProfile",
      "request": "QueryProfileRequest",
      "response": "QueryProfileResponse"
    },
    {
      "suffix": "GetBindCode",
      "request": "GetBindCodeRequest",
      "response": "GetBindCodeResponse"
    },
    {
      "suffix": "BindProfile",
      "request": "BindProfileRequest",
      "response": "BindProfileResponse"
    },
    {
      "suffix": "UnBindProfile",
      "request": "UnBindProfileRequest",
      "response": "UnBindProfileResponse"
    },
    {
      "suffix": "BindSubscriber",
      "request": "BindSubscriberRequest",
      "response": "BindSubscriberResponse"
    },
    {
      "suffix": "UnBindSubscriber",
      "request": "UnBindSubscriberRequest",
      "response": "UnBindSubscriberResponse"
    },
    {
      "suffix": "QueryBindedSubscriber",
      "request": "QueryBindedSubscriberRequest",
      "response": "QueryBindedSubscriberResponse"
    },
    {
      "suffix": "QueryDeviceList",
      "request": "QueryDeviceListRequest",
      "response": "QueryDeviceListResponse"
    },
    {
      "suffix": "ReplaceDevice",
      "request": "ReplaceDeviceRequest",
      "response": "ReplaceDeviceResponse"
    },
    {
      "suffix": "ModifyDeviceInfo",
      "request": "ModifyDeviceInfoRequest",
      "response": "ModifyDeviceInfoResponse"
    },
    {
      "suffix": "CheckPassword",
      "request": "CheckPasswordRequest",
      "response": "CheckPasswordResponse"
    },
    {
      "suffix": "ModifyPassword",
      "request": "ModifyPasswordRequest",
      "response": "ModifyPasswordResponse"
    },
    {
      "suffix": "ResetPassword",
      "request": "ResetPasswordRequest",
      "response": "ResetPasswordResponse"
    },
    {
      "suffix": "QrCodeAuthenticate",
      "request": "QrCodeAuthenticateRequest",
      "response": "QrCodeAuthenticateResponse"
    },
    {
      "suffix": "QueryQrCodeAuthenticateStatus",
      "request": "QueryQrCodeAuthenticateStatusRequest",
      "response": "QueryQrCodeAuthenticateStatusResponse"
    },
    {
      "suffix": "ReportQrCodeAuthenticateStatus",
      "request": "ReportQrCodeAuthenticateStatusRequest",
      "response": "ReportQrCodeAuthenticateStatusResponse"
    },
    {
      "suffix": "QuitQrCodeAuthenticate",
      "request": "QuitQrCodeAuthenticateRequest",
      "response": "QuitQrCodeAuthenticateResponse"
    },
    {
      "suffix": "QueryQrCodeAuthenticatedSubscriber",
      "request": "QueryQrCodeAuthenticatedSubscriberRequest",
      "response": "QueryQrCodeAuthenticatedSubscriberResponse"
    },
    {
      "suffix": "SetQrCodeAuthenticatedSubscriber",
      "request": "SetQrCodeAuthenticatedSubscriberRequest",
      "response": "SetQrCodeAuthenticatedSubscriberResponse"
    },
    {
      "suffix": "QueryProduct",
      "request": "QueryProductRequest",
      "response": "QueryProductResponse"
    },
    {
      "suffix": "SubscribeProduct",
      "request": "SubscribeProductRequest",
      "response": "SubscribeProductResponse"
    },
    {
      "suffix": "CancelSubscribe",
      "request": "CancelSubscribeRequest",
      "response": "CancelSubscribeResponse"
    },
    {
      "suffix": "QueryMyContent",
      "request": "QueryMyContentRequest",
      "response": "QueryMyContentResponse"
    },
    {
      "suffix": "QuerySubscription",
      "request": "QuerySubscriptionRequest",
      "response": "QuerySubscriptionResponse"
    },
    {
      "suffix": "QueryChannelSubjectList",
      "request": "QueryChannelSubjectListRequest",
      "response": "QueryChannelSubjectListResponse"
    },
    {
      "suffix": "QueryChannelListBySubject",
      "request": "QueryChannelListBySubjectRequest",
      "response": "QueryChannelListBySubjectResponse"
    },
    {
      "suffix": "QueryAllChannel",
      "request": "QueryAllChannelRequest",
      "response": "QueryAllChannelResponse"
    },
    {
      "suffix": "QueryAllChannelDynamicProperties",
      "request": "QueryAllChannelDynamicPropertiesRequest",
      "response": "QueryAllChannelDynamicPropertiesResponse"
    },
    {
      "suffix": "QueryPlaybillList",
      "request": "QueryPlaybillListRequest",
      "response": "QueryPlaybillListResponse"
    },
    {
      "suffix": "QueryPlaybillListByChannelContext",
      "request": "QueryPlaybillListByChannelContextRequest",
      "response": "QueryPlaybillListByChannelContextResponse"
    },
    {
      "suffix": "QueryPlaybillContext",
      "request": "QueryPlaybillContextRequest",
      "response": "QueryPlaybillContextResponse"
    },
    {
      "suffix": "QueryPlaybillContextByChannelContext",
      "request": "QueryPlaybillContextByChannelContextRequest",
      "response": "QueryPlaybillContextByChannelContextResponse"
    },
    {
      "suffix": "GetPlaybillDetail",
      "request": "GetPlaybillDetailRequest",
      "response": "GetPlaybillDetailResponse"
    },
    {
      "suffix": "QueryHotPlaybill",
      "request": "QueryHotPlaybillRequest",
      "response": "QueryHotPlaybillResponse"
    },
    {
      "suffix": "PlayChannel",
      "request": "PlayChannelRequest",
      "response": "PlayChannelResponse"
    },
    {
      "suffix": "DownloadCUTV",
      "request": "DownloadCUTVRequest",
      "response": "DownloadCUTVResponse"
    },
    {
      "suffix": "ReportChannel",
      "request": "ReportChannelRequest",
      "response": "ReportChannelResponse"
    },
    {
      "suffix": "PlayChannelHeartbeat",
      "request": "PlayChannelHeartbeatRequest",
      "response": "PlayChannelHeartbeatResponse"
    },
    {
      "suffix": "QueryPlaybillVersion",
      "request": "QueryPlaybillVersionRequest",
      "response": "QueryPlaybillVersionResponse"
    },
    {
      "suffix": "QueryMosaicChannelList",
      "request": "QueryMosaicChannelListRequest",
      "response": "QueryMosaicChannelListResponse"
    },
    {
      "suffix": "BatchPlayChannel",
      "request": "BatchPlayChannelRequest",
      "response": "BatchPlayChannelResponse"
    },
    {
      "suffix": "QueryVODSubjectList",
      "request": "QueryVODSubjectListRequest",
      "response": "QueryVODSubjectListResponse"
    },
    {
      "suffix": "QuerySubjectDetail",
      "request": "QuerySubjectDetailRequest",
      "response": "QuerySubjectDetailResponse"
    },
    {
      "suffix": "QueryVODListBySubject",
      "request": "QueryVODListBySubjectRequest",
      "response": "QueryVODListBySubjectResponse"
    },
    {
      "suffix": "QuerySubjectVODBySubjectID",
      "request": "QuerySubjectVODBySubjectIDRequest",
      "response": "QuerySubjectVODBySubjectIDResponse"
    },
    {
      "suffix": "QueryRecmVODList",
      "request": "QueryRecmVODListRequest",
      "response": "QueryRecmVODListResponse"
    },
    {
      "suffix": "GetContentConfig",
      "request": "GetContentConfigRequest",
      "response": "GetContentConfigResponse"
    },
    {
      "suffix": "GetVODDetail",
      "request": "GetVODDetailRequest",
      "response": "GetVODDetailResponse"
    },
    {
      "suffix": "PlayVOD",
      "request": "PlayVODRequest",
      "response": "PlayVODResponse"
    },
    {
      "suffix": "DownloadVOD",
      "request": "DownloadVODRequest",
      "response": "DownloadVODResponse"
    },
    {
      "suffix": "ReportVOD",
      "request": "ReportVODRequest",
      "response": "ReportVODResponse"
    },
    {
      "suffix": "PlayVODHeartbeat",
      "request": "PlayVODHeartbeatRequest",
      "response": "PlayVODHeartbeatResponse"
    },
    {
      "suffix": "SetGlobalFilterCond",
      "request": "SetGlobalFilterCondRequest",
      "response": "SetGlobalFilterCondResponse"
    },
    {
      "suffix": "SearchHotKey",
      "request": "SearchHotKeyRequest",
      "response": "SearchHotKeyResponse"
    },
    {
      "suffix": "SearchContent",
      "request": "SearchContentRequest",
      "response": "SearchContentResponse"
    },
    {
      "suffix": "SuggestKeyword",
      "request": "SuggestKeywordRequest",
      "response": "SuggestKeywordResponse"
    },
    {
      "suffix": "QueryRecmContent",
      "request": "QueryRecmContentRequest",
      "response": "QueryRecmContentResponse"
    },
    {
      "suffix": "QueryHotSearchContentsList",
      "request": "QueryHotSearchContentsListRequest",
      "response": "QueryHotSearchContentsListResponse"
    },
    {
      "suffix": "AddFavoCatalog",
      "request": "AddFavoCatalogRequest",
      "response": "AddFavoCatalogResponse"
    },
    {
      "suffix": "DeleteFavoCatalog",
      "request": "DeleteFavoCatalogRequest",
      "response": "DeleteFavoCatalogResponse"
    },
    {
      "suffix": "UpdateFavoCatalog",
      "request": "UpdateFavoCatalogRequest",
      "response": "UpdateFavoCatalogResponse"
    },
    {
      "suffix": "QueryFavoCatalog",
      "request": "QueryFavoCatalogRequest",
      "response": "QueryFavoCatalogResponse"
    },
    {
      "suffix": "CreateFavorite",
      "request": "CreateFavoriteRequest",
      "response": "CreateFavoriteResponse"
    },
    {
      "suffix": "DeleteFavorite",
      "request": "DeleteFavoriteRequest",
      "response": "DeleteFavoriteResponse"
    },
    {
      "suffix": "QueryFavorite",
      "request": "QueryFavoriteRequest",
      "response": "QueryFavoriteResponse"
    },
    {
      "suffix": "SortFavorite",
      "request": "SortFavoriteRequest",
      "response": "SortFavoriteResponse"
    },
    {
      "suffix": "CreateLock",
      "request": "CreateLockRequest",
      "response": "CreateLockResponse"
    },
    {
      "suffix": "DeleteLock",
      "request": "DeleteLockRequest",
      "response": "DeleteLockResponse"
    },
    {
      "suffix": "QueryLock",
      "request": "QueryLockRequest",
      "response": "QueryLockResponse"
    },
    {
      "suffix": "CreateBookmark",
      "request": "CreateBookmarkRequest",
      "response": "CreateBookmarkResponse"
    },
    {
      "suffix": "DeleteBookmark",
      "request": "DeleteBookmarkRequest",
      "response": "DeleteBookmarkResponse"
    },
    {
      "suffix": "QueryBookmark",
      "request": "QueryBookmarkRequest",
      "response": "QueryBookmarkResponse"
    },
    {
      "suffix": "CreateContentScore",
      "request": "CreateContentScoreRequest",
      "response": "CreateContentScoreResponse"
    },
    {
      "suffix": "SetContentLike",
      "request": "SetContentLikeRequest",
      "response": "SetContentLikeResponse"
    },
    {
      "suffix": "CreatePlaylist",
      "request": "CreatePlaylistRequest",
      "response": "CreatePlaylistResponse"
    },
    {
      "suffix": "UpdatePlaylist",
      "request": "UpdatePlaylistRequest",
      "response": "UpdatePlaylistResponse"
    },
    {
      "suffix": "DeletePlaylist",
      "request": "DeletePlaylistRequest",
      "response": "DeletePlaylistResponse"
    },
    {
      "suffix": "QueryPlaylist",
      "request": "QueryPlaylistRequest",
      "response": "QueryPlaylistResponse"
    },
    {
      "suffix": "AddPlaylistContent",
      "request": "AddPlaylistContentRequest",
      "response": "AddPlaylistContentResponse"
    },
    {
      "suffix": "SortPlaylistContent",
      "request": "SortPlaylistContentRequest",
      "response": "SortPlaylistContentResponse"
    },
    {
      "suffix": "DeletePlaylistContent",
      "request": "DeletePlaylistContentRequest",
      "response": "DeletePlaylistContentResponse"
    },
    {
      "suffix": "QueryPlaylistContent",
      "request": "QueryPlaylistContentRequest",
      "response": "QueryPlaylistContentResponse"
    },
    {
      "suffix": "CreateReminder",
      "request": "CreateReminderRequest",
      "response": "CreateReminderResponse"
    },
    {
      "suffix": "DeleteReminder",
      "request": "DeleteReminderRequest",
      "response": "DeleteReminderResponse"
    },
    {
      "suffix": "QueryReminder",
      "request": "QueryReminderRequest",
      "response": "QueryReminderResponse"
    },
    {
      "suffix": "QueryLauncher",
      "request": "QueryLauncherRequest",
      "response": "QueryLauncherResponse"
    },
    {
      "suffix": "BatchGetResStrategyData",
      "request": "BatchGetResStrategyDataRequest",
      "response": "BatchGetResStrategyDataResponse"
    },
    {
      "suffix": "GetLatestResources",
      "request": "GetLatestResourcesRequest",
      "response": "GetLatestResourcesResponse"
    },
    {
      "suffix": "jumpURLjsp",
      "request": "jumpURLjspRequest",
      "response": "jumpURLjspResponse"
    },
    {
      "suffix": "GetTopics",
      "request": "GetTopicsRequest",
      "response": "GetTopicsResponse"
    },
    {
      "suffix": "SubmitDeviceInfo",
      "request": "SubmitDeviceInfoRequest",
      "response": "SubmitDeviceInfoResponse"
    },
    {
      "suffix": "PushMsgByTag",
      "request": "PushMsgByTagRequest",
      "response": "PushMsgByTagResponse"
    },
    {
      "suffix": "DownloadNPVR",
      "request": "DownloadNPVRRequest",
      "response": "DownloadNPVRResponse"
    },
    {
      "suffix": "ReportPVR",
      "request": "ReportPVRRequest",
      "response": "ReportPVRResponse"
    },
    {
      "suffix": "PlayNPVRHeartbeat",
      "request": "PlayNPVRHeartbeatRequest",
      "response": "PlayNPVRHeartbeatResponse"
    },
    {
      "suffix": "AddPVR",
      "request": "AddPVRRequest",
      "response": "AddPVRResponse"
    },
    {
      "suffix": "UpdatePVR",
      "request": "UpdatePVRRequest",
      "response": "UpdatePVRResponse"
    },
    {
      "suffix": "AddPeriodicPVR",
      "request": "AddPeriodicPVRRequest",
      "response": "AddPeriodicPVRResponse"
    },
    {
      "suffix": "UpdatePeriodicPVR",
      "request": "UpdatePeriodicPVRRequest",
      "response": "UpdatePeriodicPVRResponse"
    },
    {
      "suffix": "QueryPVR",
      "request": "QueryPVRRequest",
      "response": "QueryPVRResponse"
    },
    {
      "suffix": "SearchPVR",
      "request": "SearchPVRRequest",
      "response": "SearchPVRResponse"
    },
    {
      "suffix": "QueryPVRByID",
      "request": "QueryPVRByIDRequest",
      "response": "QueryPVRByIDResponse"
    },
    {
      "suffix": "CancelPVRByID",
      "request": "CancelPVRByIDRequest",
      "response": "CancelPVRByIDResponse"
    },
    {
      "suffix": "CancelPVRByPlaybillID",
      "request": "CancelPVRByPlaybillIDRequest",
      "response": "CancelPVRByPlaybillIDResponse"
    },
    {
      "suffix": "DeletePVRByID",
      "request": "DeletePVRByIDRequest",
      "response": "DeletePVRByIDResponse"
    },
    {
      "suffix": "DeletePVRByCondition",
      "request": "DeletePVRByConditionRequest",
      "response": "DeletePVRByConditionResponse"
    },
    {
      "suffix": "DeleteCPVRByDiskID",
      "request": "DeleteCPVRByDiskIDRequest",
      "response": "DeleteCPVRByDiskIDResponse"
    },
    {
      "suffix": "QueryPVRSpace",
      "request": "QueryPVRSpaceRequest",
      "response": "QueryPVRSpaceResponse"
    },
    {
      "suffix": "QueryPVRSpaceByID",
      "request": "QueryPVRSpaceByIDRequest",
      "response": "QueryPVRSpaceByIDResponse"
    },
    {
      "suffix": "SolvePVRConflict",
      "request": "SolvePVRConflictRequest",
      "response": "SolvePVRConflictResponse"
    },
    {
      "suffix": "SetMasterSTBID",
      "request": "SetMasterSTBIDRequest",
      "response": "SetMasterSTBIDResponse"
    },
    {
      "suffix": "GetMasterSTBID",
      "request": "GetMasterSTBIDRequest",
      "response": "GetMasterSTBIDResponse"
    },
    {
      "suffix": "UpdateCPVRStatus",
      "request": "UpdateCPVRStatusRequest",
      "response": "UpdateCPVRStatusResponse"
    },
    {
      "suffix": "PlayPVR",
      "request": "PlayPVRRequest",
      "response": "PlayPVRResponse"
    },
    {
      "suffix": "ApplyStream",
      "request": "ApplyStreamRequest",
      "response": "ApplyStreamResponse"
    },
    {
      "suffix": "ReleaseStream",
      "request": "ReleaseStreamRequest",
      "response": "ReleaseStreamResponse"
    },
    {
      "suffix": "GetStreamRecord",
      "request": "GetStreamRecordRequest",
      "response": "GetStreamRecordResponse"
    },
    {
      "suffix": "PublishComment",
      "request": "PublishCommentRequest",
      "response": "PublishCommentResponse"
    },
    {
      "suffix": "MaintainComment",
      "request": "MaintainCommentRequest",
      "response": "MaintainCommentResponse"
    },
    {
      "suffix": "QueryComments",
      "request": "QueryCommentsRequest",
      "response": "QueryCommentsResponse"
    },
    {
      "suffix": "ReportComment",
      "request": "ReportCommentRequest",
      "response": "ReportCommentResponse"
    },
    {
      "suffix": "LikeComment",
      "request": "LikeCommentRequest",
      "response": "LikeCommentResponse"
    },
    {
      "suffix": "Barrage",
      "request": "BarrageRequest",
      "response": "BarrageResponse"
    },
    {
      "suffix": "GetBarrageList",
      "request": "GetBarrageListRequest",
      "response": "GetBarrageListResponse"
    },
    {
      "suffix": "RegisterUserInfo",
      "request": "RegisterUserInfoRequest",
      "response": "RegisterUserInfoResponse"
    },
    {
      "suffix": "QueryCustomizeConfig",
      "request": "QueryCustomizeConfigRequest",
      "response": "QueryCustomizeConfigResponse"
    },
    {
      "suffix": "QueryTemplate",
      "request": "QueryTemplateRequest",
      "response": "QueryTemplateResponse"
    },
    {
      "suffix": "QueryLocation",
      "request": "QueryLocationRequest",
      "response": "QueryLocationResponse"
    },
    {
      "suffix": "GetRelatedContent",
      "request": "GetRelatedContentRequest",
      "response": "GetRelatedContentResponse"
    },
    {
      "suffix": "AuthenticateSDKLicense",
      "request": "AuthenticateSDKLicenseRequest",
      "response": "AuthenticateSDKLicenseResponse"
    },
    {
      "suffix": "QueryVodHomeData",
      "request": "QueryVodHomeDataRequest",
      "response": "QueryVodHomeDataResponse"
    },
    {
      "suffix": "VodHome",
      "request": "VodHomeRequest",
      "response": "VodHomeResponse"
    },
    {
      "suffix": "QueryHomeData",
      "request": "QueryHomeDataRequest",
      "response": "QueryHomeDataResponse"
    },
    {
      "suffix": "QueryOTTLiveTVHomeData",
      "request": "QueryOTTLiveTVHomeDataRequest",
      "response": "QueryOTTLiveTVHomeDataResponse"
    },
    {
      "suffix": "QueryMyTVHomeData",
      "request": "QueryMyTVHomeDataRequest",
      "response": "QueryMyTVHomeDataResponse"
    },
    {
      "suffix": "QueryMoreRecommend",
      "request": "QueryMoreRecommendRequest",
      "response": "QueryMoreRecommendResponse"
    },
    {
      "suffix": "QueryEpgHomeRecmPlaybill",
      "request": "QueryEpgHomeRecmPlaybillRequest",
      "response": "QueryEpgHomeRecmPlaybillResponse"
    },
    {
      "suffix": "QueryEpgHomeVod",
      "request": "QueryEpgHomeVodRequest",
      "response": "QueryEpgHomeVodResponse"
    },
    {
      "suffix": "QueryEpgHomeSubjects",
      "request": "QueryEpgHomeSubjectsRequest",
      "response": "QueryEpgHomeSubjectsResponse"
    },
    {
      "suffix": "QueryVodSubjectAndVodList",
      "request": "QueryVodSubjectAndVodListRequest",
      "response": "QueryVodSubjectAndVodListResponse"
    },
    {
      "suffix": "QueryPCVodHome",
      "request": "QueryPCVodHomeRequest",
      "response": "QueryPCVodHomeResponse"
    },
    {
      "suffix": "QueryAllHomeData",
      "request": "QueryAllHomeDataRequest",
      "response": "QueryAllHomeDataResponse"
    },
    {
      "suffix": "VoiceControl",
      "request": "VoiceControlRequest",
      "response": "VoiceControlResponse"
    }
  ],
  "beans": [
    {
      "name": "AuthenticateBasic",
      "fieldList": [
        {
          "name": "userID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "userType",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "authType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "authToken",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "clientPasswd",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "timeZone",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "channelNamespace",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "connectType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "bizdomain",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "preSharedKeyID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "cnonce",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "pageTrackerUIType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "lang",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "isSupportWebpImgFormat",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "needPosterTypes",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "asDefaultProfile",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "AuthenticateDevice",
      "fieldList": [
        {
          "name": "physicalDeviceID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "terminalID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "STBSN",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "deviceModel",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "terminalVendor",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "OSVersion",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "STBVersion",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "softwareVersion",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "CADeviceInfos",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "authTerminalID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "VUID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "AuthenticateTolerant",
      "fieldList": [
        {
          "name": "areaCode",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "templateName",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "userGroup",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "subnetID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "bossID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "AuthorizeResult",
      "fieldList": [
        {
          "name": "productID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "pricedProducts",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "unenforcedProducts",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "reliantProducts",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "productRestriction",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "isLocked",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "isParentControl",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "contentID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "contentType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "triggers",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "BarrageFilter",
      "fieldList": [
        {
          "name": "barrageBeginTime",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "barrageEndTime",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "contentCode",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "contentType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "beginTimestamp",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "endTimestamp",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "sortType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "BarrageObject",
      "fieldList": [
        {
          "name": "barrageID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "barrage",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "contentCode",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "contentType",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "timestamp",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "barrageTime",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "userID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "profileSN",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extendProperties",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "Bookmark",
      "fieldList": [
        {
          "name": "bookmarkType",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "itemID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "rangeTime",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "subContentID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "subContentType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "updateTime",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "sitcomNO",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "BookmarkItem",
      "fieldList": [
        {
          "name": "bookmarkType",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "VOD",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "playbill",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "npvrBookmark",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "cpvrBookmark",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "BookmarkFilter",
      "fieldList": [
        {
          "name": "ratingID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "BrotherSeasonVOD",
      "fieldList": [
        {
          "name": "VOD",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "sitcomNO",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "CADeviceInfo",
      "fieldList": [
        {
          "name": "CADeviceType",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "CADeviceID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "CADeviceIDSignature",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "CAInfo",
      "fieldList": [
        {
          "name": "verimatrix",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "playReady",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "nagra",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "CancelSubscribe",
      "fieldList": [
        {
          "name": "productID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "cancelType",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "priceObject",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "startTime",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "deviceID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "Cast",
      "fieldList": [
        {
          "name": "castID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "castName",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "castCode",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "picture",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "CastRole",
      "fieldList": [
        {
          "name": "roleType",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "casts",
          "required": true,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "Channel",
      "fieldList": [
        {
          "name": "ID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "code",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "rMediaCode",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "name",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "contentType",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "channelNO",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "logo",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "picture",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "favorite",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "channelSet",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "currentPlaybill",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "recmExplain",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "locationCopyrights",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "ChannelDetail",
      "fieldList": [
        {
          "name": "ID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "code",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "rMediaCode",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "channelNO",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "name",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "introduce",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "contentType",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "isPPV",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "rating",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "picture",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "logo",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "price",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "audioLanguages",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "subtitleLanguages",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "deviceTypes",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "genres",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "customFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "isCUTVDependonLivetv",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "subjectIDs",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "favorite",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "isLocked",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "hasPIP",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "physicalChannels",
          "required": true,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "pipPhysicalChannel",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "locationCopyrights",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "ChannelDynamicProperties",
      "fieldList": [
        {
          "name": "ID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "channelNO",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "favorite",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "isLocked",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "physicalChannelsDynamicProperties",
          "required": true,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "ChannelEncrypt",
      "fieldList": [
        {
          "name": "encrypt",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "hdcpEnable",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "macrovision",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "CGMSA",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "ChannelFilter",
      "fieldList": [
        {
          "name": "genres",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "subscriptionTypes",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "isFavorite",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "isCUTV",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "isPVR",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "ChannelLogo",
      "fieldList": [
        {
          "name": "url",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "display",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "location",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "size",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "ChannelPlaybill",
      "fieldList": [
        {
          "name": "channelDetail",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "playbillCount",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "playbillLites",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "ChannelPlaybillContext",
      "fieldList": [
        {
          "name": "channelDetail",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "prePlaybillLites",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "currentPlaybillLite",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "nextPlaybillLites",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "Chapter",
      "fieldList": [
        {
          "name": "ID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "title",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "offTime",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "posters",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "CheckLock",
      "fieldList": [
        {
          "name": "checkType",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "password",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "type",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "CommentFilter",
      "fieldList": [
        {
          "name": "authorID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "profileSN",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "contentCode",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "contentType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "commentBeginTime",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "commentEndTime",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "commentID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "CommentObject",
      "fieldList": [
        {
          "name": "commentID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "comment",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "contentCode",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "contentType",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "parentCommentID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "commentTime",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "subComments",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "subCommentCount",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "userID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "profileSN",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "nickName",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "headPic",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "liked",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "likedCount",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "extendProperties",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "Configuration",
      "fieldList": [
        {
          "name": "cfgType",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "values",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "STBLogServerURL",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "STBLogUploadInterval",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "playHeartBitInterval",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "netrixPushServerURL",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "skippingTimeBlock",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "VODFavLimit",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "channelFavLimit",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "VASFavLimit",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "downloadedBufferLength",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "SocialURL",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "favouriteLimit",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "bookmarkLimit",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "lockLimit",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "profileLimit",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "SQMURL",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "UserPwdMinLength",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "UserPwdUpperCaseLetters",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "UserPwdLowerCaseLetters",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "UserPwdNumbers",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "UserPwdOthersLetters",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "UserPwdSupportSpace",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "BeginOffset",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "EndOffset",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "NPVRSpaceStrategy",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "OTTIMPURLforIPv",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "OTTIMPURL",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "OTTIMPPort",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "OTTIMPIPforIPv",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "UserIDMinLength",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "UserIDMaxLength",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "UserIDUpperCaseLetters",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "UserIDLowerCaseLetters",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "UserIDNumbers",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "UserIDOthersLetters",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "UserIDSupportSpace",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "ProfileNameMinLength",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "ProfileNameMaxLength",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "ProfileNameUpperCaseLetters",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "ProfileNameLowerCaseLetters",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "ProfileNameNumbers",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "ProfileNameOthersLetters",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "ProfileNameSupportSpace",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "DeviceNameMinLength",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "DeviceNameMaxLength",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "DeviceNameUpperCaseLetters",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "DeviceNameLowerCaseLetters",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "DeviceNameNumbers",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "DeviceNameOthersLetters",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "DeviceNameSupportSpace",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "SearchKeyMinLength",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "SearchKeyMaxLength",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "SearchKeyUpperCaseLetters",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "SearchKeyLowerCaseLetters",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "SearchKeyNumbers",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "SearchKeyOthersLetters",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "SearchKeySupportSpace",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "PlaylistNameMinLength",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "PlaylistNameMaxLength",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "PlaylistNameUpperCaseLetters",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "PlaylistNameLowerCaseLetters",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "PlaylistNameNumbers",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "PlaylistNameOthersLetters",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "PlaylistNameSupportSpace",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "OTTIMPIP",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "IPTVIMPURLforIPv",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "IPTVIMPURL",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "IPTVIMPPort",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "IPTVIMPIPforIPv",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "IPTVIMPIP",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "IMDomain",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "ImpAuthType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "PlaybillLen",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "RecPlaybillLen",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "MQMCURL",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "ConcurrentCpvrTaskLimit",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "ConflictPVRGroup",
      "fieldList": [
        {
          "name": "beginTime",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "endTime",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "PVRList",
          "required": true,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "isReserves",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "conflictType",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "Content",
      "fieldList": [
        {
          "name": "contentType",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "VOD",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "channel",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "subject",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "playbill",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "tendency",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "ContentLikes",
      "fieldList": [
        {
          "name": "contentID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "contentType",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "likes",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "ContentLikeSetting",
      "fieldList": [
        {
          "name": "action",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "contentID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "contentType",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "ContentRight",
      "fieldList": [
        {
          "name": "enable",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "isContentValid",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "isSubscribed",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "isValid",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "length",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "bizMediaId",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "isSupportFF",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "isSupportFB",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "timelengthOfFFFB",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "isSupportFFFBbyBookmark",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "timelengthAfterPlay",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "ContentScore",
      "fieldList": [
        {
          "name": "contentID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "contentType",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "score",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "CustomizedProperty",
      "fieldList": [
        {
          "name": "childPVRMaxStartTime",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "Device",
      "fieldList": [
        {
          "name": "ID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "name",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "deviceType",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "deviceModel",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "status",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "onlineState",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "physicalDeviceID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "lastOfflineTime",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "CADeviceInfos",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "channelNamespace",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "deviceToken",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "terminalVendor",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "videoCodec",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "definition",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "fps",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "customFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "DeviceType",
      "fieldList": [
        {
          "name": "deviceType",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "deviceTypeName",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "type",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "DiskInfo",
      "fieldList": [
        {
          "name": "availableSpace",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "Episode",
      "fieldList": [
        {
          "name": "VOD",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "sitcomNO",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "FavoCatalog",
      "fieldList": [
        {
          "name": "catalogID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "catalogName",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "Favorite",
      "fieldList": [
        {
          "name": "contentID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "contentType",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "catalogID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "entrance",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "recmActionID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "collectTime",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "Genre",
      "fieldList": [
        {
          "name": "genreID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "genreType",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "genreName",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "GetLicenseTrigger",
      "fieldList": [
        {
          "name": "licenseURL",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "customData",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "GlobalFilter",
      "fieldList": [
        {
          "name": "channelNamespace",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "definitions",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "fps",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "videoCodecs",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "dimension",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "ISOCode",
      "fieldList": [
        {
          "name": "shortName",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "fullName",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "Language",
      "fieldList": [
        {
          "name": "ID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "name",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "fullName",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "Layout",
      "fieldList": [
        {
          "name": "childPlanLayout",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "Lock",
      "fieldList": [
        {
          "name": "profileID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "lockType",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "itemID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "LockItem",
      "fieldList": [
        {
          "name": "lockType",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "VOD",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "channel",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "subject",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "playbill",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "genre",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "lockSeries",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "LockSeries",
      "fieldList": [
        {
          "name": "ID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "name",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "MyContent",
      "fieldList": [
        {
          "name": "content",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "startTime",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "endTime",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "serStartTime",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "serEndTime",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "MultiRights_playready",
      "fieldList": [
        {
          "name": "LA_URL",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "LUI_URL",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "Multirights_widevine",
      "fieldList": [
        {
          "name": "LA_URL",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "LUI_URL",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "Nagra",
      "fieldList": [
        {
          "name": "pvsUrl",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "laUrl",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "NamedParameter",
      "fieldList": [
        {
          "name": "key",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "values",
          "required": true,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "PageTracker",
      "fieldList": [
        {
          "name": "type",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "appID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "appPassword",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "isSupportedUserLogCollect",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "pageTrackerServerURL",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "Parameters",
      "fieldList": [
        {
          "name": "favouriteLimit",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "bookmarkLimit",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "lockLimit",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "profileLimit",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "favoCatalogLimit",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "mashupAddress",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "TVMSHeartbitURL",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "TVMSVodHeartbitURL",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "TVMSHeartbitInterval",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "TVMSDelayLength",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "isSupportPublicAD",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "ADPlatformURL",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "ADPublicStrategyURL",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "ADPlayOverNotifyURL",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "giftLoyaltyByBrowseAD",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "giftLoyaltyByReceiveADWithSMS",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "giftLoyaltyByReceiveADWithEmail",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "PLTVDelay",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "DVBEnable",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "SQMURL",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "MQMCURL",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "PaymentInfo",
      "fieldList": [
        {
          "name": "servicePayType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "promoCodeID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "mobilePhone",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "verifyCode",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "discountFee",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "canExceedQuota",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "subServicePayType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "merchantTranID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "finalFee",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "externalVoucherID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "externalVoucherPrice",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "streamyxUserID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "IDType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "IDNO",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "GSTCode",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "GSTRatio",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "PeriodPVR",
      "fieldList": [
        {
          "name": "ID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "name",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "channelID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "storageType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "destDeviceID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "cpvrMediaID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "npvrMediaID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "policyType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "days",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "beginDayTime",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "endDayTime",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "effectiveDate",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "expireDate",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "childPVR",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "childPVRIDs",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "customizedProperty",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "isOverdue",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "PeriodicPVRFilter",
      "fieldList": [
        {
          "name": "isOverdue",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "storageType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "PersonalDataVersion",
      "fieldList": [
        {
          "name": "channel",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "bookmark",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "favorite",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "preFavorite",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "lock",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "preLock",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "reminder",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "profile",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "stbFileVersion",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "PhysicalChannel",
      "fieldList": [
        {
          "name": "ID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "mediaName",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "code",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "videoCodec",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "bitrate",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "fps",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "definition",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "dimension",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "formatOf",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "channelEncrypt",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "btvCR",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "pltvCR",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "cpltvCR",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "cutvCR",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "npvrRecordCR",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "cpvrRecordCR",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "irCR",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "fileFormat",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "isSupportPIP",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "previewLength",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "previewCount",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "fccEnable",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "fecEnable",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "maxBitrate",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "multiBitrates",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "blockedNetworks",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "customFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "PhysicalChannelDynamicProperties",
      "fieldList": [
        {
          "name": "ID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "btvCR",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "pltvCR",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "cpltvCR",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "cutvCR",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "npvrRecordCR",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "cpvrRecordCR",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "irCR",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "channelNamespaces",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "PhysicalChannelSet",
      "fieldList": [
        {
          "name": "isSupportDefinitionSD",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "isSupportDefinitionHD",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "isSupportDefinition",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "Picture",
      "fieldList": [
        {
          "name": "deflates",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "posters",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "stills",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "icons",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "titles",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "ads",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "drafts",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "backgrounds",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "channelPics",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "channelBlackWhites",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "channelNamePics",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "secondarys",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "logoBrightBgs",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "logoDarkBgs",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "mainWides",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "previews",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "secondaryWides",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "instantRestartLogos",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "channelFallbackWides",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "others",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "hcsSlaveAddrs",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "Playbill",
      "fieldList": [
        {
          "name": "ID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "code",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "channel",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "name",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "introduce",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "startTime",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "endTime",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "rating",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "picture",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "isCUTV",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "genres",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "CUTVStatus",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "playbillSeries",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "isLocked",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "bookmark",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "reminder",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "playTimes",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "isFillProgram",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "isNPVR",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "isCPVR",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "hasRecordingPVR",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "isInstantRestart",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "isBlackout",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "PlaybillBasedPVR",
      "fieldList": [
        {
          "name": "ID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "name",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "channelID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "storageType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "destDeviceID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "cpvrMediaID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "npvrMediaID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "policyType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "playbillID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "beginOffset",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "endOffset",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "startTime",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "endTime",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "playbill",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "status",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "failReason",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "duration",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "definition",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "canPlay",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "rating",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "parentPlanID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "files",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "PlaybillDetail",
      "fieldList": [
        {
          "name": "ID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "code",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "channelDetail",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "name",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "introduce",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "startTime",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "endTime",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "rating",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "picture",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "isCUTV",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "casts",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "genres",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "countries",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "produceDate",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "keyword",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "programType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "visitTimes",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "advisories",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "customFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "CUTVStatus",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "recordedMediaIDs",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "playbillSeries",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "isLocked",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "bookmark",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "favorite",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "reminder",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "isFillProgram",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "isNPVR",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "isCPVR",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "hasRecordingPVR",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "isInstantRestart",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "isBlackout",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "PlaybillExcluder",
      "fieldList": [
        {
          "name": "countries",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "playbillIDs",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "PlaybillFilter",
      "fieldList": [
        {
          "name": "initials",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "lifetimeIDs",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "countries",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "genres",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "PlaybillLite",
      "fieldList": [
        {
          "name": "ID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "channelID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "name",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "startTime",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "endTime",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "rating",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "picture",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "isCUTV",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "CUTVStatus",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "playbillSeries",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "isLocked",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "reminderStatus",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "recmExplain",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "playTimes",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "isFillProgram",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "isNPVR",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "isCPVR",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "hasRecordingPVR",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "isInstantRestart",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "isBlackout",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "PlaybillSeries",
      "fieldList": [
        {
          "name": "sitcomName",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "sitcomNO",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "seasonNO",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "seriesID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "lifetimeID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "seasonID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "PlaybillVersion",
      "fieldList": [
        {
          "name": "channelID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "date",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "version",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "Playlist",
      "fieldList": [
        {
          "name": "playlistID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "name",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "PlaylistContent",
      "fieldList": [
        {
          "name": "playlistID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "contentID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "contentType",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "seriesID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "Playready",
      "fieldList": [
        {
          "name": "LA_URL",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "LUI_URL",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "PlaySession",
      "fieldList": [
        {
          "name": "sessionKey",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "profileID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "deviceID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "PriceObject",
      "fieldList": [
        {
          "name": "ID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "type",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "contentType",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "PriceObjectDetail",
      "fieldList": [
        {
          "name": "ID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "type",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "contentType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "VOD",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "channel",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "playbill",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "ProduceZone",
      "fieldList": [
        {
          "name": "ID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "name",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "Product",
      "fieldList": [
        {
          "name": "ID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "name",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "introduce",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "price",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "startTime",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "endTime",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "productType",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "chargeMode",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "periodLength",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "isAutoExtend",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "isMain",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "isSubscribed",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "isOnlinePurchase",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "rentPeriod",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "priceObject",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "productRestriction",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "picture",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "marketInfo",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "ProductRestriction",
      "fieldList": [
        {
          "name": "net",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "areaCode",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "deviceModel",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "userGroupID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "deviceType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "definition",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "Profile",
      "fieldList": [
        {
          "name": "ID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "name",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "profileType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "familyRole",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "introduce",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "password",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "loginAccounts",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "quota",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "logoURL",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "ratingID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "ratingName",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "subjectIDs",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "channelIDs",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "VASIDs",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "isShowMessage",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "templateName",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "lang",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "mobilePhone",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "isReceiveSMS",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "isNeedSubscribePIN",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "email",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "isDisplayInfoBar",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "channelListType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "isSendSMSForReminder",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "reminderInterval",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "leadTimeForSendReminder",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "isDefaultProfile",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "deviceID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "birthday",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "nationality",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "receiveADType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "profilePINEnable",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "multiscreenEnable",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "purchaseEnable",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "loginName",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "isOnline",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "subscriberID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "location",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "sign",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "createTime",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "isFilterLevel",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "hasCollectUserPreference",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "pushStatus",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "customFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "PromoCode",
      "fieldList": [
        {
          "name": "promoCodeID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "actionTime",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "actionType",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "isUsed",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "voucher",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "PVRCondition",
      "fieldList": [
        {
          "name": "policyType",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "singlePVRFilter",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "periodicPVRFilter",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "layout",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "PVRSpace",
      "fieldList": [
        {
          "name": "spaceMode",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "totalSize",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "usedSize",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "bookingSize",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "notEnoughStartTime",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "PVRTaskBasic",
      "fieldList": [
        {
          "name": "ID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "name",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "channelID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "storageType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "destDeviceID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "cpvrMediaID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "npvrMediaID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "policyType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "status",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "failReason",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "duration",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "definition",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "canPlay",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "rating",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "parentPlanID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "files",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "startTime",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "endTime",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "playbillID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "beginOffset",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "endOffset",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "playbill",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "childPVR",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "childPVRIDs",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "customizedProperty",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "isOverdue",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "seriesID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "days",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "beginDayTime",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "endDayTime",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "effectiveDate",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "expireDate",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryChannel",
      "fieldList": [
        {
          "name": "subjectID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "channelIDs",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "channelNOs",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "count",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "offset",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "contentType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "channelNamespace",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "isReturnAllMedia",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "channelFilter",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryChannelContext",
      "fieldList": [
        {
          "name": "channelNO",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "preNumber",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "nextNumber",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "contentType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "channelNamespace",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "isReturnAllMedia",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "channelFilter",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryDynamicRecmContent",
      "fieldList": []
    },
    {
      "name": "QueryPlaybill",
      "fieldList": [
        {
          "name": "type",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "startTime",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "endTime",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "mustIncluded",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "count",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "offset",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "isFillProgram",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "playbillFilter",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "playbillExcluder",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "sortType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryPlaybillContext",
      "fieldList": [
        {
          "name": "date",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "type",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "preNumber",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "nextNumber",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "isFillProgram",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "RatingSystem",
      "fieldList": [
        {
          "name": "systemName",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "ratings",
          "required": true,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "Rating",
      "fieldList": [
        {
          "name": "ID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "code",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "name",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "RecmVODFilter",
      "fieldList": [
        {
          "name": "genreIDs",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "languages",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "ratingID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "RecmScenario",
      "fieldList": [
        {
          "name": "contentType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "businessType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "entrance",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "recmType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "contentID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "userPreference",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "count",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "offset",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "RecmContents",
      "fieldList": [
        {
          "name": "total",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "recmContentType",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "recmVODs",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "recmChannels",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "recmPrograms",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "RecordFile",
      "fieldList": [
        {
          "name": "fileID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "mediaID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "definition",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "bitrate",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "storageType",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "diskID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "status",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "failReason",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "duration",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "fileSize",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "beginTime",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "endTime",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "beginOffset",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "endOffset",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "canPlay",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "rating",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "Reminder",
      "fieldList": [
        {
          "name": "contentID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "contentType",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "reminderTime",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "ResStrategyData",
      "fieldList": [
        {
          "name": "resItemType",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "contentURL",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "textContent",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "actionType",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "actionURL",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "appParam",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "validStartTime",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "validEndTime",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "updateTime",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "introduce",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionInfo",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "Result",
      "fieldList": [
        {
          "name": "retCode",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "retMsg",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "Rule",
      "fieldList": [
        {
          "name": "type",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "values",
          "required": true,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "SearchExcluder",
      "fieldList": [
        {
          "name": "genreName",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "country",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "ratingID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "subjectID",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "SearchFilter",
      "fieldList": [
        {
          "name": "VODTypes",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "genreName",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "produceZoneID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "initial",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "channelScope",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "channelIDs",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "SeriesPVR",
      "fieldList": [
        {
          "name": "ID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "name",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "channelID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "storageType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "destDeviceID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "cpvrMediaID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "npvrMediaID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "policyType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "playbillID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "seriesID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "beginOffset",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "endOffset",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "childPVR",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "childPVRIDs",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "customizedProperty",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "isOverdue",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "SinglePVRFilter",
      "fieldList": [
        {
          "name": "scope",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "periodicPVRID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "storageType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "status",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "isFilterByDevice",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "diskID",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "Sitcom",
      "fieldList": [
        {
          "name": "VODID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "sitcomNO",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "StrategyData",
      "fieldList": [
        {
          "name": "resourceID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "resStrategyDatas",
          "required": true,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionInfo",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "StreamRecord",
      "fieldList": [
        {
          "name": "taskId",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "deviceId",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "businessType",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "pvrId",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "contentId",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "mediaId",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "wanBitrate",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "StreamResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "taskId",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "usedBitrate",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "mediaId",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "isFCC",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "lowPriorityRecords",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "Subject",
      "fieldList": [
        {
          "name": "ID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "name",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "contentType",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "introduce",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "picture",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "hasChildren",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "isSubscribed",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "parentSubjectID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "isLocked",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "SubjectVODList",
      "fieldList": [
        {
          "name": "subject",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "VODs",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "Subscribe",
      "fieldList": [
        {
          "name": "subscribeType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "productID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "priceObjects",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "payment",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "isAutoExtend",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "deviceID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "reliantMediaID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "effectiveTime",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "eventID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "approvalVersions",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "toReportInfo",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "Subscription",
      "fieldList": [
        {
          "name": "productOrderKey",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "productID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "productName",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "productDesc",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "servicePayType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "subServicePayType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "price",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "startTime",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "endTime",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "productType",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "orderTime",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "isAutoExtend",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "priceObjectDetail",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "deviceID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "profileID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "isMainPackage",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "orderState",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "restrictions",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "triggers",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "totalChooseNum",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "residualChooseNum",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "cycleEndTime",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "contentRentPeriod",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "chargeMode",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "periodLength",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "picture",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "Template",
      "fieldList": [
        {
          "name": "templateId",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "templateName",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "fileName",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "templateAlias",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "templateDesc",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "Topic",
      "fieldList": [
        {
          "name": "id",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "relationSubjectId",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "topicStyleId",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "params",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "customFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "TimeBasedPVR",
      "fieldList": [
        {
          "name": "ID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "name",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "channelID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "storageType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "destDeviceID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "cpvrMediaID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "npvrMediaID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "policyType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "startTime",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "endTime",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "status",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "failReason",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "duration",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "definition",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "canPlay",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "rating",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "parentPlanID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "files",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "UserLoginHistoryInfo",
      "fieldList": [
        {
          "name": "latestSuccessItem",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "curValidLoginItem",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "UserLoginHistoryItem",
      "fieldList": [
        {
          "name": "subscriberSN",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "profileSN",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "logindate",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "clientIP",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "UserPreference",
      "fieldList": [
        {
          "name": "preferContentIDs",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "unknownContentIDs",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "dislikeContentIDs",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "VAS",
      "fieldList": [
        {
          "name": "ID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "name",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "picture",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "favorite",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "locationCopyrights",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "VASDetail",
      "fieldList": [
        {
          "name": "ID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "code",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "channelNO",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "name",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "introduce",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "URL",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "rating",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "isSubscribed",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "picture",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "startTime",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "endTime",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "subjectIDs",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "genres",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "deviceType",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "favorite",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "isLocked",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "locationCopyrights",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "customFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "Verimatrix",
      "fieldList": [
        {
          "name": "company",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "serverAddr",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "serverPort",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "VKSAddr",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "CSMIP",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "CSMPort",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "multirights_widevine",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "multiRights_playready",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "VOD",
      "fieldList": [
        {
          "name": "ID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "code",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "rMediaCode",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "name",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "contentType",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "introduce",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "picture",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "castRoles",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "VODType",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "seriesType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "episodeCount",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "episodeTotalCount",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "series",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "price",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "genres",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "mediaFiles",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "rentPeriod",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "rating",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "startTime",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "endTime",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "favorite",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "isLocked",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "bookmark",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "isSubscribed",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "produceDate",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "scoreTimes",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "averageScore",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "audioLanguages",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "produceZone",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "visitTimes",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "subtitleLanguages",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "advisories",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "customFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "companyName",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "recmExplain",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "userScore",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "likes",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "isLike",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "cpId",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "locationCopyrights",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "airDate",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "VODDetail",
      "fieldList": [
        {
          "name": "ID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "code",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "rMediaCode",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "name",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "contentType",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "introduce",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "picture",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "castRoles",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "VODType",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "seriesType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "episodes",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "episodeCount",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "episodeTotalCount",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "brotherSeasonVODs",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "series",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "price",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "genres",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "mediaFiles",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "clipfiles",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "bgMusicFile",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "chapters",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "rentPeriod",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "rating",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "startTime",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "endTime",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "favorite",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "isLocked",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "bookmark",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "reminder",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "isSubscribed",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "subscriptionType",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "produceDate",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "scoreTimes",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "averageScore",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "loyaltyCount",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "audioLanguages",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "produceZone",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "creditVODSendLoyalty",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "visitTimes",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "subtitleLanguages",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "awards",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "deviceTypes",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "keyword",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "advisories",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "playable",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "subjectIDs",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "customFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "companyName",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "userScore",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "locationCopyrights",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "cpId",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "likes",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "isLike",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "airDate",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "VODExcluder",
      "fieldList": [
        {
          "name": "produceZoneIDs",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "subscriptionTypes",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "produceYears",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "genreIDs",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "supersetType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "VODFilter",
      "fieldList": [
        {
          "name": "produceZoneIDs",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "subscriptionTypes",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "produceYears",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "genreIDs",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "initials",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "languages",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "cpId",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "hasCommon",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "VODMediaFile",
      "fieldList": [
        {
          "name": "ID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "elapseTime",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "bitrate",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "isDownload",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "definition",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "HDCPEnable",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "macrovision",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "dimension",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "formatOf",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "supportTerminals",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "fileFormat",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "encrypt",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "CGMSA",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "analogOutputEnable",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "videoCodec",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "customFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "audioType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "code",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "multiBitrates",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "fps",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "maxBitrate",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "picture",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "preview",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "previewStartTime",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "previewEndTime",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "Voucher",
      "fieldList": [
        {
          "name": "voucherID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "applyTime",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "expireTime",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "voucherType",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "discount",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "cycleCount",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "applyType",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "applyScopes",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "LicenseItem",
      "fieldList": [
        {
          "name": "licenseType",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "count",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "useCount",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "isAlarm",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "LoginRouteRequest",
      "fieldList": [
        {
          "name": "subscriberID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "disableVSPAddr",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "LoginRouteResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "vspURL",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "vspHttpsURL",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "LoginRequest",
      "fieldList": [
        {
          "name": "subscriberID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "deviceModel",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "LoginResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "vspURL",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "vspHttpsURL",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "rootCerAddr",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "upgradeDomain",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "upgradeDomainBackup",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "mgmtDomain",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "mgmtDomainBackup",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "NTPDomain",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "NTPDomainBackup",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "parameters",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "terminalParm",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "AuthenticateRequest",
      "fieldList": [
        {
          "name": "authenticateBasic",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "authenticateDevice",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "authenticateTolerant",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "AuthenticateResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "userToken",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "deviceID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "deviceName",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "devices",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "CA",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "subscriberID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "profileID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "profileSN",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "profiles",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "isFirstLogin",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "loginOccasion",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "timeZone",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "DSTTime",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "areaCode",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "templateName",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "templateTimeStamp",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "userGroup",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "subnetID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "bossID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "bandWidth",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "transportProtocol",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "provisioningType",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "loginIP",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "location",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "RRSAddr",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "pageTrackers",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "pictureURL",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "antiTamperURI",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "csrfToken",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "isTriplePlay",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "lockedNum",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "waitUnlockTime",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "remainLockedNum",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "pwdResetTime",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "STBRCUSubscribed",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "opt",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "hasCollectUserPreference",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "userLoginHistoryInfo",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "VUID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "network",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "jSessionID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "authToken",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "userFilter",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "sqmSessionId",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "sqmToken",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "SwitchProfileRequest",
      "fieldList": [
        {
          "name": "profileID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "password",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "forceLogin",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "asDefaultProfile",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "lang",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "deviceID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "SwitchProfileResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "defaultProfileID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "userToken",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "isFirstLogin",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "hasCollectUserPreference",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "userLoginHistoryInfo",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "OnLineHeartbeatRequest",
      "fieldList": []
    },
    {
      "name": "OnLineHeartbeatResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "userValid",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "nextCallInterval",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "personalDataVersions",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "areaCode",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "userGroup",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "subscriberID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "opt",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "validInfo",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "validKey",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "templateTimeStamp",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "isSupportedUserLogCollect",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "userFilter",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "userToken",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "LogoutRequest",
      "fieldList": [
        {
          "name": "type",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "deviceStatus",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "physicalDeviceId",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "LogoutResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "UpdateTimeZoneRequest",
      "fieldList": [
        {
          "name": "newTimezone",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "UpdateTimeZoneResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "timeZone",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "DSTTime",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "SendSMSRequest",
      "fieldList": [
        {
          "name": "loginName",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "msgType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "lang",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "subnetId",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "SendSMSResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "CreateSubscriberRequest",
      "fieldList": [
        {
          "name": "loginName",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "password",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "regType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "subnetId",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "opt",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "verifyCode",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "CreateSubscriberResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "AddProfileRequest",
      "fieldList": [
        {
          "name": "profile",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "AddProfileResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "profileID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "ModifyProfileRequest",
      "fieldList": [
        {
          "name": "profile",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "ModifyProfileResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "updatePeriod",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "updateTimes",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "remainDays",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "DeleteProfileRequest",
      "fieldList": [
        {
          "name": "profileIDs",
          "required": true,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "DeleteProfileResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryProfileRequest",
      "fieldList": [
        {
          "name": "type",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "condition",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "count",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "offset",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "isReturnPassword",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryProfileResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "profiles",
          "required": true,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "subscriberID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "userToken",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "total",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "GetBindCodeRequest",
      "fieldList": []
    },
    {
      "name": "GetBindCodeResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "QRCode",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "validTime",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "BindProfileRequest",
      "fieldList": [
        {
          "name": "subscriberId",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "profileId",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "deviceId",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "bindCode",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "BindProfileResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "UnBindProfileRequest",
      "fieldList": [
        {
          "name": "profileId",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "UnBindProfileResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "BindSubscriberRequest",
      "fieldList": [
        {
          "name": "targetSubscriberId",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "BindSubscriberResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "UnBindSubscriberRequest",
      "fieldList": [
        {
          "name": "targetSubscriberId",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "UnBindSubscriberResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryBindedSubscriberRequest",
      "fieldList": []
    },
    {
      "name": "QueryBindedSubscriberResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "bindedSubscribers",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryDeviceListRequest",
      "fieldList": [
        {
          "name": "subscriberID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "profileID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "deviceType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryDeviceListResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "devices",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "ReplaceDeviceRequest",
      "fieldList": [
        {
          "name": "subscriberID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "orgDeviceID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "destDeviceID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "ReplaceDeviceResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "ModifyDeviceInfoRequest",
      "fieldList": [
        {
          "name": "device",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "ModifyDeviceInfoResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "CheckPasswordRequest",
      "fieldList": [
        {
          "name": "password",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "type",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "correlator",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "CheckPasswordResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "token",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "ModifyPasswordRequest",
      "fieldList": [
        {
          "name": "oldPwd",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "newPwd",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "confirmPwd",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "encryptPwd",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "type",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "ModifyPasswordResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "ResetPasswordRequest",
      "fieldList": [
        {
          "name": "profileID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "type",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "resetPwd",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "encryptPwd",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "ResetPasswordResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QrCodeAuthenticateRequest",
      "fieldList": []
    },
    {
      "name": "QrCodeAuthenticateResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "QRCode",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "validTime",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryQrCodeAuthenticateStatusRequest",
      "fieldList": []
    },
    {
      "name": "QueryQrCodeAuthenticateStatusResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "status",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "scannedSubscriberId",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "ReportQrCodeAuthenticateStatusRequest",
      "fieldList": [
        {
          "name": "QRCode",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "currentStatus",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "ReportQrCodeAuthenticateStatusResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QuitQrCodeAuthenticateRequest",
      "fieldList": []
    },
    {
      "name": "QuitQrCodeAuthenticateResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryQrCodeAuthenticatedSubscriberRequest",
      "fieldList": []
    },
    {
      "name": "QueryQrCodeAuthenticatedSubscriberResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "qrCodeAuthenticatedSubscriberId",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "SetQrCodeAuthenticatedSubscriberRequest",
      "fieldList": [
        {
          "name": "qrCodeAuthenticatedSubscriberId",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "SetQrCodeAuthenticatedSubscriberResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryProductRequest",
      "fieldList": [
        {
          "name": "queryType",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "productIds",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "productType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "count",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "offset",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "isMain",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "isEnableOnlinePurchase",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryProductResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "countTotal",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "productList",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "SubscribeProductRequest",
      "fieldList": [
        {
          "name": "subscribe",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "type",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "correlator",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "SubscribeProductResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "originInfo",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "subscription",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "triggers",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "promoCodes",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "remanentCredit",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "remanentCreditDuration",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "totalCredit",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "totalCreditDuration",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "reliantProducts",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "CancelSubscribeRequest",
      "fieldList": [
        {
          "name": "cancelSubscribe",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "CancelSubscribeResponse",
      "fieldList": [
        {
          "name": "result",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "originInfo",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryMyContentRequest",
      "fieldList": [
        {
          "name": "contentType",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "count",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "offset",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "sortType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "productType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "subjectType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryMyContentResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "total",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "contents",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QuerySubscriptionRequest",
      "fieldList": [
        {
          "name": "profileID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "fromDate",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "toDate",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "count",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "offset",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "productType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "deviceID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "isMain",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "subscriptionKey",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "sortType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "queryType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QuerySubscriptionResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "total",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "products",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryChannelSubjectListRequest",
      "fieldList": [
        {
          "name": "subjectID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "contentTypes",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "count",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "offset",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryChannelSubjectListResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "total",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "subjects",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryChannelListBySubjectRequest",
      "fieldList": [
        {
          "name": "subjectID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "count",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "offset",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "contentTypes",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "channelNamespace",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "isReturnAllMedia",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "filter",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryChannelListBySubjectResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "total",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "channelDetails",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryAllChannelRequest",
      "fieldList": [
        {
          "name": "channelNamespace",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "isReturnAllMedia",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "userFilter",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryAllChannelResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "total",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "channelDetails",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "channelVersion",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryAllChannelDynamicPropertiesRequest",
      "fieldList": [
        {
          "name": "channelNamespace",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "isReturnAllMedia",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryAllChannelDynamicPropertiesResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "total",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "channelDynamaicProp",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryPlaybillListRequest",
      "fieldList": [
        {
          "name": "queryChannel",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "queryPlaybill",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "needChannel",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryPlaybillListResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "total",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "channelPlaybills",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "playbillVersion",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryPlaybillListByChannelContextRequest",
      "fieldList": [
        {
          "name": "queryChannelContext",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "queryPlaybill",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "needChannel",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryPlaybillListByChannelContextResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "channelPlaybills",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryPlaybillContextRequest",
      "fieldList": [
        {
          "name": "queryChannel",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "queryPlaybillContext",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "needChannel",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryPlaybillContextResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "total",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "channelPlaybillContexts",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryPlaybillContextByChannelContextRequest",
      "fieldList": [
        {
          "name": "queryChannelContext",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "queryPlaybillContext",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "needChannel",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryPlaybillContextByChannelContextResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "channelPlaybillContexts",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "GetPlaybillDetailRequest",
      "fieldList": [
        {
          "name": "playbillID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "IDType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "filterType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "queryDynamicRecmContent",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "channelNamespace",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "isReturnAllMedia",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "GetPlaybillDetailResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "playbillDetail",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "recmActionID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "recmContents",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryHotPlaybillRequest",
      "fieldList": [
        {
          "name": "count",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "offset",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "boardType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryHotPlaybillResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "total",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "playbills",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "PlayChannelRequest",
      "fieldList": [
        {
          "name": "channelID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "playbillID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "mediaID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "businessType",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "checkLock",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "isReturnURL",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "URLFormat",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "isReturnProduct",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "sortType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "playSessionKey",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "PlayChannelResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "authorizeResult",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "playURL",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "attachedPlayURL",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "bookmark",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "playSessions",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "DownloadCUTVRequest",
      "fieldList": [
        {
          "name": "playbillID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "mediaID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "checkLock",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "DownloadCUTVResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "authorizeResult",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "downloadURL",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "ReportChannelRequest",
      "fieldList": [
        {
          "name": "action",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "playSessionKey",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "playerInstanceId",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "channelID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "nextChannelID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "playbillID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "mediaID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "nextMediaID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "businessType",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "isDownload",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "productID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "subjectID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "ReportChannelResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "playSessionKey",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "PlayChannelHeartbeatRequest",
      "fieldList": [
        {
          "name": "channelID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "playbillID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "mediaID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "playSessionKey",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "playerInstanceId",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "PlayChannelHeartbeatResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "isValid",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "interrupter",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryPlaybillVersionRequest",
      "fieldList": [
        {
          "name": "fromDate",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "toDate",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "timeRange",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryPlaybillVersionResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "playbillVersion",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryVODSubjectListRequest",
      "fieldList": [
        {
          "name": "subjectID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "contentTypes",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "count",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "offset",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryVODSubjectListResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "total",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "subjects",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QuerySubjectDetailRequest",
      "fieldList": [
        {
          "name": "subjectIDs",
          "required": true,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QuerySubjectDetailResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "subjects",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryVODListBySubjectRequest",
      "fieldList": [
        {
          "name": "subjectID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "count",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "offset",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "sortType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "VODFilter",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "VODExcluder",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryVODListBySubjectResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "total",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "VODs",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QuerySubjectVODBySubjectIDRequest",
      "fieldList": [
        {
          "name": "subjectID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "subjectSortType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "subjectCount",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "VODSortType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "VODCount",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "offset",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QuerySubjectVODBySubjectIDResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "total",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "subjectVODLists",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryRecmVODListRequest",
      "fieldList": [
        {
          "name": "count",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "offset",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "action",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "sortType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "subjectID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "position",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "VODTypes",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "recmVODFilter",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryRecmVODListResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "total",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "VODs",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "GetContentConfigRequest",
      "fieldList": [
        {
          "name": "types",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "GetContentConfigResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "produceZones",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "genres",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "GetVODDetailRequest",
      "fieldList": [
        {
          "name": "VODID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "IDType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "filterType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "queryDynamicRecmContent",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "GetVODDetailResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "VODDetail",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "recmActionID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "recmContents",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "PlayVODRequest",
      "fieldList": [
        {
          "name": "VODID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "mediaID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "seriesID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "checkLock",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "URLFormat",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "isReturnProduct",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "sortType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "playSessionKey",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "PlayVODResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "authorizeResult",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "playURL",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "bookmark",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "playSessions",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "DownloadVODRequest",
      "fieldList": [
        {
          "name": "VODID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "episodeIDs",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "mediaIDs",
          "required": true,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "checkLock",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "DownloadVODResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "authorizeResult",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "downloadURLs",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "ReportVODRequest",
      "fieldList": [
        {
          "name": "action",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "VODID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "mediaID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "isDownload",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "subjectID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "productID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "ReportVODResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "PlayVODHeartbeatRequest",
      "fieldList": [
        {
          "name": "VODID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "mediaID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "PlayVODHeartbeatResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "isValid",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "interrupter",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "SetGlobalFilterCondRequest",
      "fieldList": [
        {
          "name": "globalFilter",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "SetGlobalFilterCondResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "SearchHotKeyRequest",
      "fieldList": [
        {
          "name": "count",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "offset",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "SearchHotKeyResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "total",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "hotKeys",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "SearchContentRequest",
      "fieldList": [
        {
          "name": "searchKey",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "contentTypes",
          "required": true,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "searchScopes",
          "required": true,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "subjectID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "filter",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "excluder",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "count",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "offset",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "sortType",
          "required": true,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "correction",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "SearchContentResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "correction",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "correctKey",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "total",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "contents",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "SuggestKeywordRequest",
      "fieldList": [
        {
          "name": "count",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "key",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "SuggestKeywordResponse",
      "fieldList": [
        {
          "name": "result",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "suggests",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryRecmContentRequest",
      "fieldList": [
        {
          "name": "queryDynamicRecmContent",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryRecmContentResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "recmActionID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "recmContents",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryHotSearchContentsListRequest",
      "fieldList": [
        {
          "name": "count",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "offset",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "boardType",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryHotSearchContentsListResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "total",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "hotContent",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "AddFavoCatalogRequest",
      "fieldList": [
        {
          "name": "catalog",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "AddFavoCatalogResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "DeleteFavoCatalogRequest",
      "fieldList": [
        {
          "name": "catalogID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "deleteType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "DeleteFavoCatalogResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "UpdateFavoCatalogRequest",
      "fieldList": [
        {
          "name": "catalog",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "UpdateFavoCatalogResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryFavoCatalogRequest",
      "fieldList": [
        {
          "name": "count",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "offset",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryFavoCatalogResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "total",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "catalogs",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "CreateFavoriteRequest",
      "fieldList": [
        {
          "name": "favorites",
          "required": true,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "autoCover",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "CreateFavoriteResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "version",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "preVersion",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "DeleteFavoriteRequest",
      "fieldList": [
        {
          "name": "catalogID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "contentTypes",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "contentIDs",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "deleteType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "DeleteFavoriteResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "version",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "preVersion",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryFavoriteRequest",
      "fieldList": [
        {
          "name": "catalogID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "contentTypes",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "count",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "offset",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "sortType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryFavoriteResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "total",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "version",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "favorites",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "SortFavoriteRequest",
      "fieldList": [
        {
          "name": "favorites",
          "required": true,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "SortFavoriteResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "CreateLockRequest",
      "fieldList": [
        {
          "name": "locks",
          "required": true,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "isShare",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "CreateLockResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "version",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "preVersion",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "DeleteLockRequest",
      "fieldList": [
        {
          "name": "profileID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "lockTypes",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "itemIDs",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "deleteType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "DeleteLockResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "version",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "preVersion",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryLockRequest",
      "fieldList": [
        {
          "name": "profileID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "isShare",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "lockTypes",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "count",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "offset",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "sortType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryLockResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "total",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "version",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "locks",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "CreateBookmarkRequest",
      "fieldList": [
        {
          "name": "bookmarks",
          "required": true,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "CreateBookmarkResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "DeleteBookmarkRequest",
      "fieldList": [
        {
          "name": "bookmarkTypes",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "itemIDs",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "deleteType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "DeleteBookmarkResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryBookmarkRequest",
      "fieldList": [
        {
          "name": "bookmarkTypes",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "filter",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "count",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "offset",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "sortType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryBookmarkResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "total",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "version",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "bookmarks",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "CreateContentScoreRequest",
      "fieldList": [
        {
          "name": "scores",
          "required": true,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "CreateContentScoreResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "newScores",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "SetContentLikeRequest",
      "fieldList": [
        {
          "name": "settings",
          "required": true,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "SetContentLikeResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "contentLikes",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "CreatePlaylistRequest",
      "fieldList": [
        {
          "name": "playlists",
          "required": true,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "CreatePlaylistResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "playlistIDs",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "UpdatePlaylistRequest",
      "fieldList": [
        {
          "name": "playlists",
          "required": true,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "UpdatePlaylistResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "DeletePlaylistRequest",
      "fieldList": [
        {
          "name": "playlistIDs",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "deleteType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "DeletePlaylistResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryPlaylistRequest",
      "fieldList": [
        {
          "name": "count",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "offset",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryPlaylistResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "total",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "playlists",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "AddPlaylistContentRequest",
      "fieldList": [
        {
          "name": "contents",
          "required": true,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "autoCover",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "AddPlaylistContentResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "SortPlaylistContentRequest",
      "fieldList": [
        {
          "name": "contents",
          "required": true,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "SortPlaylistContentResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "DeletePlaylistContentRequest",
      "fieldList": [
        {
          "name": "playlistID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "contentTypes",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "contentIDs",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "deleteType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "DeletePlaylistContentResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryPlaylistContentRequest",
      "fieldList": [
        {
          "name": "playlistID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "count",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "offset",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryPlaylistContentResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "total",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "contents",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "seriesIDs",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "CreateReminderRequest",
      "fieldList": [
        {
          "name": "reminders",
          "required": true,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "setType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "CreateReminderResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "DeleteReminderRequest",
      "fieldList": [
        {
          "name": "setType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "contentTypes",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "contentIDs",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "deleteType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "DeleteReminderResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryReminderRequest",
      "fieldList": [
        {
          "name": "queryType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "contentType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "count",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "offset",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryReminderResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "total",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "version",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "reminders",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryLauncherRequest",
      "fieldList": [
        {
          "name": "version",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "deviceModel",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "desktopType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "userToken",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryLauncherResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "launcherLink",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "launcherLinkHttps",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "version",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "desktopID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "interval",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "batchGetResStrategyDataURL",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "getLatestResourcesURL",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "currentResourcesVersion",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "getTopicUrl",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionInfo",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "BatchGetResStrategyDataRequest",
      "fieldList": [
        {
          "name": "resourceIDs",
          "required": true,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "userToken",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "BatchGetResStrategyDataResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "resourceDatas",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "GetLatestResourcesRequest",
      "fieldList": [
        {
          "name": "version",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "desktopID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "userToken",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "GetLatestResourcesResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "version",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "resourceIDs",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "jumpURLjspRequest",
      "fieldList": [
        {
          "name": "contentCode",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "contentType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "linkURL",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "action",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "userToken",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "jumpURLjspResponse",
      "fieldList": [
        {
          "name": "playURL",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "redirectURL",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "GetTopicsRequest",
      "fieldList": [
        {
          "name": "version",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "desktopType",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "GetTopicsResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "nextIntervalPeriod",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "version",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "topics",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "SubmitDeviceInfoRequest",
      "fieldList": [
        {
          "name": "deviceType",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "deviceToken",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "terminalAppID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "tokenExpireTime",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "relayDeviceToken",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "relayDeviceType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "SubmitDeviceInfoResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "PushMsgByTagRequest",
      "fieldList": [
        {
          "name": "deviceToken",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "tag",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "PushMsgByTagResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "DownloadNPVRRequest",
      "fieldList": [
        {
          "name": "planID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "fileID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "checkLock",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "DownloadNPVRResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "authorizeResult",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "downloadURL",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "ReportPVRRequest",
      "fieldList": [
        {
          "name": "planID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "fileID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "action",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "isDownload",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "ReportPVRResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "PlayNPVRHeartbeatRequest",
      "fieldList": [
        {
          "name": "planID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "fileID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "PlayNPVRHeartbeatResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "isValid",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "AddPVRRequest",
      "fieldList": [
        {
          "name": "PVR",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "diskInfo",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "AddPVRResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "PVRID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "isNPVRSpaceEnough",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "isCPVRSpaceEnough",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "conflictGroups",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "masterSTB",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "UpdatePVRRequest",
      "fieldList": [
        {
          "name": "PVR",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "diskInfo",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "UpdatePVRResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "isNPVRSpaceEnough",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "isCPVRSpaceEnough",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "conflictGroups",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "masterSTB",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "AddPeriodicPVRRequest",
      "fieldList": [
        {
          "name": "PVR",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "playbillBasedPVRID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "diskInfo",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "excludePVRKeys",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "conflictCheckType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "AddPeriodicPVRResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "periodicPVRID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "childPVRPlaybillIDs",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "isNPVRSpaceEnough",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "isCPVRSpaceEnough",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "conflictGroups",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "masterSTB",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "UpdatePeriodicPVRRequest",
      "fieldList": [
        {
          "name": "PVR",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "diskInfo",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "excludePVRKeys",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "conflictCheckType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "UpdatePeriodicPVRResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "isNPVRSpaceEnough",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "isCPVRSpaceEnough",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "conflictGroups",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "masterSTB",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryPVRRequest",
      "fieldList": [
        {
          "name": "PVRCondition",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "sortType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "count",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "offset",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryPVRResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "total",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "PVRList",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "singlePVRIDs",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "periodicPVRIDs",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "SearchPVRRequest",
      "fieldList": [
        {
          "name": "searchKey",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "singlePVRfilter",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "sortType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "count",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "offset",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "SearchPVRResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "total",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "PVRList",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryPVRByIDRequest",
      "fieldList": [
        {
          "name": "ID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryPVRByIDResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "PVR",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "CancelPVRByIDRequest",
      "fieldList": [
        {
          "name": "singlePVRIDs",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "storageType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "CancelPVRByIDResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "masterSTB",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "CancelPVRByPlaybillIDRequest",
      "fieldList": [
        {
          "name": "playbillIDs",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "storageType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "CancelPVRByPlaybillIDResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "masterSTB",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "DeletePVRByIDRequest",
      "fieldList": [
        {
          "name": "singlePVRIDs",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "periodicPVRIDs",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "storageType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "DeletePVRByIDResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "masterSTB",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "DeletePVRByConditionRequest",
      "fieldList": [
        {
          "name": "deleteConditions",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "reservedSinglePVRIDs",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "reservedPeriodicPVRIDs",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "storageType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "DeletePVRByConditionResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "masterSTB",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "DeleteCPVRByDiskIDRequest",
      "fieldList": [
        {
          "name": "diskID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "DeleteCPVRByDiskIDResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "masterSTB",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryPVRSpaceRequest",
      "fieldList": [
        {
          "name": "storageType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "diskInfo",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryPVRSpaceResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "CPVRStorage",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "NPVRStorage",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryPVRSpaceByIDRequest",
      "fieldList": [
        {
          "name": "singlePVRIDs",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "storageType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryPVRSpaceByIDResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "CPVRStorage",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "NPVRStorage",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "SolvePVRConflictRequest",
      "fieldList": [
        {
          "name": "conflictGroups",
          "required": true,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "groupIndex",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "SolvePVRConflictResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "conflictGroups",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "nextGroupIndex",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "SetMasterSTBIDRequest",
      "fieldList": [
        {
          "name": "masterSTBID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "SetMasterSTBIDResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "GetMasterSTBIDRequest",
      "fieldList": []
    },
    {
      "name": "GetMasterSTBIDResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "masterSTBID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "UpdateCPVRStatusRequest",
      "fieldList": [
        {
          "name": "fileID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "PVRID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "status",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "diskID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "duration",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "fileSize",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "recordBeginTime",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "recordEndTime",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "UpdateCPVRStatusResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "PlayPVRRequest",
      "fieldList": [
        {
          "name": "planID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "fileID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "checkLock",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "URLFormat",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "isReturnProduct",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "sortType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "PlayPVRResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "authorizeResult",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "playURL",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "bookmark",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "ApplyStreamRequest",
      "fieldList": [
        {
          "name": "streamType",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "contentID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "mediaIds",
          "required": true,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "preTaskId",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "switchMode",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "maxBitrate",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "minBitrate",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "programID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "pvrID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "ApplyStreamResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "taskId",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "usedBitrate",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "mediaId",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "isFCC",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "lowPriorityRecords",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "ReleaseStreamRequest",
      "fieldList": [
        {
          "name": "taskID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "releaseMode",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "ReleaseStreamResponse",
      "fieldList": [
        {
          "name": "retCode",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "retMsg",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "GetStreamRecordRequest",
      "fieldList": []
    },
    {
      "name": "GetStreamRecordResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "totalWanBitrate",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "usedWanBitrate",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "totalDevBitrate",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "usedDevBitrate",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "records",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "fccRatio",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "epgBrowsingBitrate",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "PublishCommentRequest",
      "fieldList": [
        {
          "name": "userID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "profileSN",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "contentCode",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "contentType",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "comment",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "commentTime",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "parentCommentID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "PublishCommentResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "commentID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "MaintainCommentRequest",
      "fieldList": [
        {
          "name": "action",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "userID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "profileSN",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "commentID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "comment",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "MaintainCommentResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryCommentsRequest",
      "fieldList": [
        {
          "name": "filter",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "offset",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "length",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryCommentsResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "total",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "comments",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "ReportCommentRequest",
      "fieldList": [
        {
          "name": "userID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "profileSN",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "commentID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "text",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "ReportCommentResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "LikeCommentRequest",
      "fieldList": [
        {
          "name": "userID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "profileSN",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "commentID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "LikeCommentResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "liked",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "likedCount",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "BarrageRequest",
      "fieldList": [
        {
          "name": "userID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "profileSN",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "barrage",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "contentCode",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "contentType",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "timestamp",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "barrageTime",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "fontColor",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extendProperties",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "BarrageResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "barrageID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "GetBarrageListRequest",
      "fieldList": [
        {
          "name": "userID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "profileSN",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "filter",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "offset",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "length",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "GetBarrageListResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "total",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "barrageObjects",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "lastBarrageTime",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "RegisterUserInfoRequest",
      "fieldList": [
        {
          "name": "userID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "profileSN",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "nickName",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "headPic",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "RegisterUserInfoResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryCustomizeConfigRequest",
      "fieldList": [
        {
          "name": "key",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "queryType",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "isFuzzyQuery",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "deviceModel",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "language",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "standard",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryCustomizeConfigResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "configurations",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "ratings",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "languages",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "ISOCodes",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "currencyAlphCode",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "currencyRate",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryTemplateRequest",
      "fieldList": [
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryTemplateResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "total",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "templates",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryLocationRequest",
      "fieldList": [
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryLocationResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "location",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "ip",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "GetRelatedContentRequest",
      "fieldList": [
        {
          "name": "contentID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "contentType",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "GetRelatedContentResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "contents",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "AuthenticateSDKLicenseRequest",
      "fieldList": [
        {
          "name": "licenseItem",
          "required": true,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "AuthenticateSDKLicenseResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "licenseItem",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "CastContents",
      "fieldList": [
        {
          "name": "total",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "cast",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "recmVODs",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "ContentRec",
      "fieldList": [
        {
          "name": "contentID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "contentName",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "recList",
          "required": true,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "recmActionID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "HomeData",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "channelList",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "vodList",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "playbillList",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "playbillLiteList",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "subjectList",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "channelDetailList",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "subject",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "recmActionID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "HomeParam",
      "fieldList": [
        {
          "name": "type",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "count",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "LiveTVHomeSubject",
      "fieldList": [
        {
          "name": "ID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "name",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "type",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "recmActionID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "channelPlaybillList",
          "required": true,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "RecmPlaybill",
      "fieldList": [
        {
          "name": "recmActionID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "channelPlaybill",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "RecSubject",
      "fieldList": [
        {
          "name": "subject",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "VODList",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "SubjectGroup",
      "fieldList": [
        {
          "name": "subjectID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "subjectList",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "SubjectIDList",
      "fieldList": [
        {
          "name": "subjectID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "count",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "SubjectVodsList",
      "fieldList": [
        {
          "name": "subjectID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "vodList",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryVodHomeDataRequest",
      "fieldList": [
        {
          "name": "rootSubjectID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "subjectID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "subjectIDList",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "bannerCount",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "genreID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "firstVODCount",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "VODCount",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "count",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "childrenVODCount",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryVodHomeDataResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "countTotal",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "subjectList",
          "required": true,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "bannerVODs",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "subjectDetailList",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "childrenVODs",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "genreName",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "VodHomeRequest",
      "fieldList": [
        {
          "name": "subjectIDList",
          "required": true,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "subjectID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "bannerSubjectID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "bannerCount",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "subjectCount",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "VODCount",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "VodHomeResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "countTotal",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "subjectList",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "bannerVODs",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "rankVODs",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "subjectDetailList",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryHomeDataRequest",
      "fieldList": [
        {
          "name": "bannerSubjectID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "bannerDataCount",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "recmDataCount",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "hotTVCount",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "subjectID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "subjectDataCount",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryHomeDataResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "hotKeyword",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "banner",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "topRecmActionID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "topPicksForYou",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "everyoneIsWatching",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "hotTVShow",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "recSubject",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryOTTLiveTVHomeDataRequest",
      "fieldList": [
        {
          "name": "date",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "count",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "whatsOnCount",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "happyNextCount",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "subjectCount",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "channelSubjectID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryOTTLiveTVHomeDataResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "liveTVHomeSubjectList",
          "required": true,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryMyTVHomeDataRequest",
      "fieldList": []
    },
    {
      "name": "QueryMyTVHomeDataResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "favoriteChannelCount",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "reminderCount",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "purchasedCount",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "vodHistoryCount",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "favoriteVodCount",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "profileCount",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "vodHistoryList",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryMoreRecommendRequest",
      "fieldList": [
        {
          "name": "recommendCount",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryMoreRecommendResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "topPicksForYou",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "moreLike",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "youLike",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryEpgHomeRecmPlaybillRequest",
      "fieldList": [
        {
          "name": "recmWhatsOnCount",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "recmHappyNextCount",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "hotTVCount",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryEpgHomeRecmPlaybillResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "hotTVShow",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "recmWhatsOn",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "recmHappyNext",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryEpgHomeVodRequest",
      "fieldList": [
        {
          "name": "everyoneIsWatchingCount",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "topPicksForYouCount",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "subjectIDList",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "VODSortType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryEpgHomeVodResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "topRecmActionID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "topPicksForYou",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "everyoneIsWatching",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "subjectVodLists",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryEpgHomeSubjectsRequest",
      "fieldList": [
        {
          "name": "vodSubject",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "channelSubject",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryEpgHomeSubjectsResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "vodSubjectList",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "channelSubjectList",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryVodSubjectAndVodListRequest",
      "fieldList": [
        {
          "name": "subjectID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "types",
          "required": true,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "subjectCount",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "VODCount",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "sortType",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "VODFilters",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "VODExcluders",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryVodSubjectAndVodListResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "subjectTotal",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "subjects",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "VODTotal",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "VODs",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryPCVodHomeRequest",
      "fieldList": [
        {
          "name": "subjectID",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "bannerSubjectID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "bannerVODCount",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "recmSubjectIDList",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "recmVODCount",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "castIDList",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "castVODCount",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryPCVodHomeResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "subjectList",
          "required": true,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "bannerVODs",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "rankVODs",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "recmSubjectDetailList",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "castContentsList",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryAllHomeDataRequest",
      "fieldList": [
        {
          "name": "request",
          "required": true,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "QueryAllHomeDataResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "response",
          "required": true,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "CastFromASR",
      "fieldList": [
        {
          "name": "castType",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "castName",
          "required": true,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "IntentObject",
      "fieldList": [
        {
          "name": "action",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "hour",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "minute",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "second",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "number",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "category",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "value",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "name",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "castFromASR",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "keyword",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "genreName",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "produceArea",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "VoiceControlRequest",
      "fieldList": [
        {
          "name": "format",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "rate",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "channel",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "language",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "recordID",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "index",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            },
            {
              "name": "actualType",
              "infos": [
                "number"
              ]
            }
          ]
        },
        {
          "name": "isLast",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "audioData",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "extensionFields",
          "required": false,
          "isArray": true,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "VoiceControlResponse",
      "fieldList": [
        {
          "name": "result",
          "required": true,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "audioText",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "domain",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        },
        {
          "name": "intentObject",
          "required": false,
          "isArray": false,
          "restrictions": [
            {
              "name": "stringLimit",
              "infos": [
                "<=",
                "128"
              ]
            }
          ]
        }
      ]
    }
  ]
}
