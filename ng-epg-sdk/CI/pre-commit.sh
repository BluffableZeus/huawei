#!/bin/sh
#
# An example hook script to verify what is about to be committed.
# Called by "git commit" with no arguments.  The hook should
# exit with non-zero status after issuing an appropriate message if
# it wants to stop the commit.
#
# To enable this hook, rename this file to "pre-commit".

#checked file config
includeFile="^src/.*\.ts$"
#files having tslint error but ignored
#example excludeFile="pvr\.service\|common\.service"
excludeFile="^$"
#max allowed errors
maxError=0

if git rev-parse --verify HEAD >/dev/null 2>&1
then
	against=HEAD
else
	# Initial commit: diff against an empty tree object
	against=4b825dc642cb6eb9a060e54bf8d69288fbee4904
fi

fileList=`git diff --cached --name-only $against | grep $includeFile | grep -v $excludeFile`

if [ ! $fileList ];
then
	exit 0
else
	echo "do tslint..."
	sh ./node_modules/.bin/tslint $fileList > .git/tslintError.log
	cat -n .git/tslintError.log
	tslintErrorNum=`wc -l < .git/tslintError.log`
	if [ "$tslintErrorNum" -gt "$maxError" ]
	then
		echo "【TSLINT ERROR】error occured while pre-commit check, tslint error count: $tslintErrorNum. See details in <.git/tslintError.log>"
		exit 1
	fi
fi

exit 0
