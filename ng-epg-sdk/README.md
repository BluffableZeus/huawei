### 如何定制VSP接口和元数据类型
如果出现当前需求所需要的接口与SDK中定义的不一致的情况（增加接口，接口定义不一致），目前推荐的做法是在epg（页面工程）自己重新定义接口，具体做法如下：
#### 接口字段变更比如Login接口：
```typescript
import { LoginRequest, LoginResponse, login } from 'ng-epg-sdk/vsp';

export interface CLoginRequest extends LoginRequest {
    newRequestFiled: string;
}

export interface CLoginResponse extends LoginResponse {
    newResponseFiled: string;
}

export function cLogin(request: CLoginRequest, response: CLoginResponse){
    return login(<LoginRequest>request, <LoginResponse>response);
}
```
#### 新增接口
```typescript
import { sendRequest } from 'ng-epg-sdk/vsp/uitls';

export interface NewInterfaceRequest {
    newRequestFiled: string;
    ......
}

export interface NewInterfaceRequest {
    newResponseFiled: string;
    ......
}

export function newInterface(request: NewInterfaceRequest, response: NewInterfaceRequest){
    return sendRequest('/VSP/V3/NewInterface', request);
}
```
