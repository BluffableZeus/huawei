var gulp = require('gulp');
var replace = require('gulp-replace');
var rename = require('gulp-rename');
var clean = require('gulp-clean');
var _ = require('underscore');
var Stream = require('stream');
var zip = require('gulp-zip');
var uglify = require('gulp-uglify');
var pump = require('pump');
var ts = require('gulp-typescript');
var merge = require('merge2');
var mergeStream = require('merge-stream');
var buildDistPath = 'build';
gulp.task('clean-release', function () {
    return gulp.src('release/', {
            read: false
        })
        .pipe(clean({
            force: true
        }));
});

gulp.task('compile', function () {
    var tsResult = gulp.src(['typings/browser/ambient/**/*.d.ts',
            'src/**/*.ts',
            '!**/*.spec.ts'
        ])
        .pipe(ts({
            declaration: true,
            noExternalResolve: false,
            "target": "es6",
            "module": "commonjs",
            "emitDecoratorMetadata": true,
            "experimentalDecorators": true,
            "sourceMap": true,
            "suppressImplicitAnyIndexErrors": true
        }));

    return merge([
        tsResult.dts.pipe(gulp.dest('release/')),
        tsResult.js.pipe(gulp.dest('release/'))
    ]);
});

gulp.task('compress', ['compile'], function (cb) {
    pump([
        gulp.src('release/**/*.js'),
        uglify(),
        gulp.dest('release')
    ], cb);
});

gulp.task('copy-assets', ['compress'], function () {
    gulp.src([
            'src/**/*',
            '!src/**/*.ts'
        ])
        .pipe(gulp.dest('release/'));
});

gulp.task('repareTslintReporter', function () {
    gulp.src('build/reporters/tslint.checkstyle.xml')
        .pipe(replace('</checkstyle><?xml version="1.0" encoding="utf-8"?><checkstyle version="4.3">', ''))
        .pipe(replace(/^$/, '<?xml version="1.0" encoding="utf-8"?><checkstyle version="4.3"></checkstyle>'))
        .pipe(clean({
            force: true
        }))
        .pipe(gulp.dest('build/reporters/'));
});

gulp.task('compress-sdk', function (cb) {
    pump([
            gulp.src('build/sdk/*.js'),
            uglify(),
            gulp.dest('build/sdk/dist')
        ],
        cb
    );
});

function countTestCase() {

    var stream = new Stream.Transform({
        objectMode: true
    });

    stream._transform = function (file, unused, callback) {
        var fileStr = new Buffer(file.contents).toString();
        var matchs;
        var reg = /[\s"](\w{1,3}\d+)\s/ig;
        var ids = [];
        while (matchs = reg.exec(fileStr)) {
            ids.push(matchs[0].trim());
        }
        var outputList = _.map(_.countBy(ids), function (count, id) {
            return id + '\t' + count;
        });

        outputList.sort();

        var outputStr = outputList.join('\n');
        console.log(outputStr);
        file.contents = new Buffer(outputStr);
        callback(null, file);
    };

    return stream;
}

gulp.task('zip', function () {
    return mergeStream([
            gulp.src([`${buildDistPath}/sdk/doc/**/*`], {})
            .pipe(zip('doc.zip', {
                compress: true
            })), gulp.src([`${buildDistPath}/sdk/dist/*.js`], {})
        ])
        .pipe(zip('ng-epg-sdk.zip', {
            compress: true
        }))
        .pipe(clean({
            force: true
        }))
        .pipe(gulp.dest('build'));
});

gulp.task('testCaseCountReporter', function () {
    gulp.src('build/reporters/unit-test/junit.xml')
        .pipe(countTestCase())
        .pipe(rename('testCaseCount.txt'))
        .pipe(gulp.dest('build/reporters/unit-test'));
});
