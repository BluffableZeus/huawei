import { Doc2Api } from './docx2api'

// const vspParser = new Doc2Api(__dirname + '/Hybrid Video Solution VSP and Multi-screen Application Interface Reference(V3.0,Java).docx', {
//     metaTypeSectionName: 'Metadata Types',
//     sectionIndex: {
//         InterfaceFunction: 'Interface Function',
//         InterfaceRestriction: 'Interface Restriction',
//         InterfacePrototype: 'Interface Prototype',
//         InputParameters: 'Input Parameters',
//         OutputParameters: 'Output Parameters'
//     }
// });

const vspParser = new Doc2Api(__dirname + '/Envision Video Platform VSP与多屏客户端的接口规范(V3.0).docx', {
  metaTypeSectionName: '元数据类型',
  sectionIndex: {
      InterfaceFunction: '接口功能',
      InterfaceRestriction: '接口约束',
      InterfacePrototype: '接口原型',
      InputParameters: '请求参数',
      OutputParameters: '返回参数'
    }
})

const gwParser = new Doc2Api(__dirname + '/Envision Video Platform VSP与多屏客户端的编排接口规范.docx', {
  metaTypeSectionName: '元数据说明',
  sectionIndex: {
      InterfaceFunction: '接口功能',
      InterfaceRestriction: '接口约束',
      InterfacePrototype: '接口原型',
      InputParameters: '请求参数',
      OutputParameters: '返回参数'
    }
})

Doc2Api.generateTypeScript('src/vsp', ['api', 'meta'], [vspParser, gwParser]).catch((e) => {
  console.error(e)
})
