export const apiTemplate = `/**
 * this document is auto generated from docx
 */

// tslint:disable
import { sendRequest } from './utils/sendRequest';
import { HANDLER_LIST } from './utils/pre-post-api';
import * as _ from 'underscore';

export type API_NAMES = {{{apiNames}}};

const handlerByName = _.indexBy(HANDLER_LIST,'name');

const _sendRequest = (name: string, url: string, req, options:any={}) => {
  const handler: any = handlerByName[name];
  if (handler && handler.getUrl) {
    url = handler.getUrl(url, req, options);
  }

  const then = handler ? handler.then : [];
  const promise = sendRequest(url, req, options);

  let resolve, reject;
  if (then[0]) {
    resolve = (resp) => {
      return then[0](resp, req);
    }
  }

  if (then[1]) {
    reject = (resp) => {
      return then[1](resp, req);
    }
  }

  return promise.then(resolve, reject);
}

{{#each apiTypeList}}
/**
{{description}}
*/
export function {{methodName}}(req: {{request.name}}, options:any = {}): Promise<{{#if response.name}}{{response.name}}{{else}}{result:Result}{{/if}}> {
    options.method = options.method || '{{method}}';
    return _sendRequest('{{name}}', '{{url}}', req, options).then((resp:any)=>{
        return safe{{response.name}}(resp);
    });
}

{{/each}}

{{#each metaTypeList}}

export interface {{name}} {
{{#each fieldInfoList}}

/**
{{description}}
*/
  {{name}}{{#if isOptional}}?{{/if}}:{{type}}{{#if isArray}}[]{{/if}};
{{/each}}
}

function safe{{name}}(v: {{name}}): {{name}} {
    if (v){
        {{#each fieldInfoList}}
        {{#if isArray}}
            {{#if (isMetaType type)}}
        v.{{name}} = safe{{type}}s(v.{{name}});
            {{else}}
        v.{{name}} = v.{{name}} || [];
            {{/if}}
        {{else}}
            {{#if (isMetaType type)}}
        v.{{name}} = safe{{type}}(v.{{name}});
            {{/if}}
        {{/if}}
        {{/each}}
    }
    return v;
}

function safe{{name}}s(vs: {{name}}[]): {{name}}[] {
    if (vs){
        return vs.map((v:{{name}}) => {
            return safe{{name}}(v);
        });
    } else {
        return [];
    }
}
{{/each}}
`
