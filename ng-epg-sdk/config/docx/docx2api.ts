import { FieldIndex, FieldInfo } from './docx2api'
// tslint:disable
import { MetaType, ApiType } from './docx2api';
import * as cheerio from 'cheerio';
import * as fs from 'fs';
import * as Handlebars from 'handlebars';
import * as _ from 'underscore';
import { apiTemplate } from './apiTemplate';

const toMarkdown = require('to-markdown');
const mammoth = require("mammoth");
const numberReg = /(Int(eger)?|Long|Float)\b/ig;

const fieldMapping = {
    name: ['参数名称', 'Parameter'],
    isOptional: ['必选', 'Mandatory'],
    type: ['类型', 'Type'],
    description: ['参数含义', 'Description']
}

export interface FieldInfo {
    name: string;
    isOptional: boolean;
    type: string;
    description: string;
    isArray: boolean;
    restrictionList: Array<{ name: string, infos: Array<string> }>;
}

export interface FieldIndex {
    name: number;
    isOptional: number;
    type: number;
    description: number;
}

export interface MetaType {
    name: string;
    fieldInfoList: Array<FieldInfo>;
}

export interface ApiType {
    name: string;
    methodName: string;
    url: string;
    request: MetaType;
    response: MetaType;
    description: string;
    method: string;
}

export interface SectionIndex {
    InterfaceFunction: string,
    InterfaceRestriction: string,
    InterfacePrototype: string,
    InputParameters: string,
    OutputParameters: string
}

const tsTemplate = apiTemplate;

export class Doc2Api {

    private $: CheerioStatic;

    constructor(private path: string, private options: {
        metaTypeSectionName: string,
        sectionIndex: SectionIndex
    }) {
    }

    static async generateTypeScript(outputDir: string, targets: Array<'meta' | 'api'>, docParserList: Array<Doc2Api>) {
        let allMetaTypeList = [];
        let allApiTypeList = [];
        let promises = docParserList.map(async (docParser) => {
            return docParser.parse();
        });

        await Promise.all(promises).then((list: Array<any>) => {
            allMetaTypeList = _.chain(list).pluck('metaTypeList').flatten().value();
            allApiTypeList = _.chain(list).pluck('apiTypeList').flatten().value();
        });

        const apiNames = `'${_.pluck(allApiTypeList, 'name').join("' | '")}'`;
        const data = {
            metaTypeList: _.uniq(allMetaTypeList, (metaType: MetaType) => {
                return metaType.name;
            }),
            apiTypeList: _.uniq(allApiTypeList, (api: ApiType) => {
                return api.url;
            }),
            apiNames: apiNames
        };

        //注册HandleBar Helper，帮助判断是否为元数据类型
        var metaNames = _.pluck(allMetaTypeList, 'name');
        Handlebars.registerHelper('isMetaType', (name: string) => {
            return (metaNames.indexOf(name) !== -1);
        })

        targets.forEach((target: string) => {
            let outputStr = '';
            let outputPath = `${outputDir}/${target}.ts`;
            switch (target) {
                case 'meta':
                    const outData = {
                        infs: _.map(data.apiTypeList, (apiType: ApiType) => {
                            return {
                                suffix: apiType.name,
                                request: apiType.request.name,
                                response: apiType.response ? apiType.response.name : ''
                            }
                        }),
                        beans: _.map(data.metaTypeList, (metaType: MetaType) => {
                            return {
                                name: metaType.name,
                                fieldList: _.map(metaType.fieldInfoList, (fieldInfo: FieldInfo) => {
                                    return {
                                        name: fieldInfo.name,
                                        required: !fieldInfo.isOptional,
                                        isArray: fieldInfo.isArray,
                                        restrictions: fieldInfo.restrictionList
                                    }
                                })
                            }
                        })
                    };
                    outputStr = `// tslint:disable \r\n export default ${JSON.stringify(outData, null, 2)}\r\n`;
                    break;
                case 'api':
                    outputStr = Handlebars.compile(tsTemplate)(data);
                    break;
            }

            console.log(`generate output file:${outputPath}`);
            fs.writeFileSync(outputPath, outputStr);
        });
    }


    async parse(): Promise<{ metaTypeList: Array<MetaType>, apiTypeList: Array<ApiType> }> {
        console.log(`===================parse ${this.path}`);
        const html = await this.buildHtml();
        const $ = this.$ = cheerio.load(html);

        const elementList = $.root().children().toArray();


        // 处理原数据对象
        const metaTypeList = this.handleMetaType(elementList);

        // 处理接口定义
        const apiTypeList = this.handleInterface(elementList);

        const metaTypeListOfApi = _.chain(apiTypeList).map((apiType) => {
            return [apiType.request, apiType.response];
        }).flatten().compact().value();

        return { metaTypeList: [...metaTypeList, ...metaTypeListOfApi], apiTypeList };
    }

    private buildHtml(): Promise<string> {
        return new Promise((resolve) => {
            mammoth.convertToHtml({ path: this.path }).then(function (result) {
                resolve(result.value);
            }).done();
        })
    }

    /**
         * 解析文档中的元数据类型
     */
    private handleMetaType(elementList: Array<CheerioElement>): Array<MetaType> {
        const $ = this.$;

        let isMetadataTypes = false;
        let h2List = [];
        let tableList = [];

        elementList.forEach((element: CheerioElement) => {
            const txt = $(element).text();
            if (element.tagName === 'h1') {
                if (this.options.metaTypeSectionName === this.getText(element)) {
                    isMetadataTypes = true;
                } else {
                    isMetadataTypes = false;
                }
            }

            if (isMetadataTypes) {
                switch (element.tagName) {
                    case 'h2':
                        const metaType = /[\w-_]+/.exec(this.getText(element))[0];

                        console.log(`parse metadata:${metaType}`);
                        h2List.push(metaType);
                        break;
                    case 'table':
                        tableList.push(this.parseFieldInfo(element));
                        break;
                }
            }
        });
        return h2List.map((metaName: string, index: number) => {
            return {
                name: metaName,
                fieldInfoList: tableList[index]
            }
        })
    }

    private handleInterface(elementList: Array<CheerioElement>): Array<ApiType> {
        const sectionIndex = this.options.sectionIndex;
        const sectionValues = _.values(sectionIndex);

        const valueIdIndex = _.chain(sectionIndex).keys().map((id: string) => {
            return [sectionIndex[id], id];
        }).value();

        const apiTypeList: Array<ApiType> = [];
        const $ = this.$;

        let apiType: ApiType;
        let sectionTxt;
        let children: Array<CheerioElement>;
        let table: CheerioElement;
        elementList.forEach((element: CheerioElement) => {
            const txt = this.getText(element);

            if (!txt) {
                return;
            }

            if (sectionTxt) {
                const isSectionEnd = /^h/.test(element.tagName) || _.contains(sectionValues, txt);
                if (isSectionEnd) {
                    switch (sectionTxt) {
                        // case sectionIndex.Scenario:
                        //     apiType = <ApiType>{};
                        //     apiTypeList.push(apiType);
                        //     break;

                        case sectionIndex.InterfaceFunction:
                            apiType = <ApiType>{};
                            apiTypeList.push(apiType);

                            const descriptionHtml = _.map(children, (el) => {
                                return `<p>${$(el).html()}</p>`;
                            }).join('');
                            apiType.description = this.toMarkdownDescription(descriptionHtml);
                            break;

                        case sectionIndex.InterfacePrototype:

                            const reg = /^http.*$/img;
                            const prototypeContent = toMarkdown($(table).html());

                            const method = /^https?\s+(get|post|put|delete|option)\b/im.exec(prototypeContent)[1];

                            const httpUrl = /^https?:\/\/.*$/m.exec(prototypeContent)[0].replace(/[*\s]/g, '');
                            const relativeUrl = /\/\/.*?(\/[\/\w.]*)/.exec(httpUrl)[1];

                            apiType.url = relativeUrl;
                            apiType.name = relativeUrl.split('/').slice(-1)[0].replace(/\W/g, '');
                            apiType.methodName = apiType.name.replace(/\w{1}/, (r) => r.toLowerCase());
                            apiType.method = method.toUpperCase() || 'POST';
                            console.log(`parse interface:${relativeUrl} method:${method}`);
                            break;

                        case sectionIndex.InputParameters:
                            apiType.request = { name: `${apiType.name}Request`, fieldInfoList: [] };
                            if (table) {
                                apiType.request.fieldInfoList = this.parseFieldInfo(table);
                            }
                            break;

                        case sectionIndex.OutputParameters:
                            apiType.response = { name: `${apiType.name}Response`, fieldInfoList: [] };
                            if (table) {
                                apiType.response.fieldInfoList = this.parseFieldInfo(table);
                            }
                            break;
                    }

                    sectionTxt = null;
                }
            }

            if (_.contains(sectionValues, txt)) {
                sectionTxt = txt;
                children = [];
                table = null;
            } else if (sectionTxt) {
                if ('table' === element.tagName) {
                    table = element;
                } else {
                    children.push(element);
                }
            }
        });
        return apiTypeList;
    }

    private matchSection($: CheerioStatic, element: CheerioElement, sections: Array<string>, tagName?: string) {
        if (tagName && element.tagName !== tagName) {
            return false;
        }

        const txt = $(element).text();
        return !!_.find(sections, (section: string) => {
            return txt.indexOf(section) >= 0;
        });
    }

    private parseFieldInfo(table: CheerioElement): Array<FieldInfo> {

        const trList = this.$('tr', table).toArray();
        const headerTr = trList.shift();
        const fieldIndex = this.getFieldIndex(headerTr);

        const fieldInfoList = trList.map((tr: CheerioElement) => {
            const tdList = this.$('td', tr).toArray();
            const fieldInfo = <FieldInfo>{};

            fieldInfo.name = this.getText(tdList[fieldIndex.name]);
            fieldInfo.name = _.first(/[a-zA-Z_-]+/.exec(fieldInfo.name));

            if (!fieldInfo.name) {
                return;
            }


            fieldInfo.isOptional = /o/i.test(this.getText(tdList[fieldIndex.isOptional]));

            const typeValue = this.getText(tdList[fieldIndex.type]);
            fieldInfo.restrictionList = this.parseRestrictionList(typeValue);
            fieldInfo.isArray = /\[\s*\]/.test(typeValue);
            fieldInfo.type = this.parseType(typeValue);
            fieldInfo.description = `* ${fieldInfo.name}`;
            if (fieldIndex.description) {
                fieldInfo.description = this.toMarkdownDescription(this.$(tdList[fieldIndex.description]).html() || '');
            }

            return fieldInfo;
        });

        return _.compact(fieldInfoList);
    };

    parseRestrictionList(typeValue: string) {
        const list = [];
        const matchs = /\(([><=]{0,2})(\d+)\)/.exec('String(<=128)');
        if (matchs) {
            list.push({
                name: 'stringLimit',
                infos: [matchs[1], matchs[2]]
            });
        }

        const isNumber = numberReg.test(typeValue);
        if (isNumber) {
            list.push({
                name: 'actualType',
                infos: ['number']
            });
        }
        return list;
    }

    private getFieldIndex(headerTr: CheerioElement): FieldIndex {
        const thList = this.$('th', headerTr).toArray();
        const headerNames = _.map(thList, (th: CheerioElement) => {
            return this.getText(th);
        })
        const retFieldIndex = <FieldIndex>{};
        const keys = _.chain(fieldMapping).keys().each((key: string) => {
            _.find(headerNames, (headName: string, index) => {
                const found = _.find(fieldMapping[key], (str: string) => {
                    return headName.indexOf(str) >= 0;
                });
                if (found) {
                    retFieldIndex[key] = index;
                }
                return found;
            });
        }).value();
        return retFieldIndex;

    }

    private parseType(typeStr: string): string {
        try {
            let type = /[a-zA-Z_-]+/.exec(typeStr)[0];
            type = type.replace(numberReg, 'string');
            type = type.replace('String', 'string');
            return type;
        } catch (e) {
            debugger;
        }
    }

    private getText(el: CheerioElement): string {
        return this.$(el).text().trim();
    }

    private toMarkdownDescription(descriptionHtml: string) {
        return toMarkdown(descriptionHtml).replace(/\*   /g, '\r\n - ').replace(/^/mg, '* ').replace(/\*\//g, '*');
    }
}
