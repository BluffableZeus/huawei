import * as Lint from '../../node_modules/tslint/lib/lint'
import * as ts from 'typescript'
export declare class Rule extends Lint.Rules.AbstractRule {
  static metadata: Lint.IRuleMetadata
  static FAILURE_STRING_SUFFIX: string
  isEnabled (): boolean
  apply (sourceFile: ts.SourceFile): Lint.RuleFailure[]
}
