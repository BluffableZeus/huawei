"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Lint = require('../../node_modules/tslint/lib/lint');
var ts = require('typescript');
var Rule = (function (_super) {
    __extends(Rule, _super);
    function Rule() {
        _super.apply(this, arguments);
    }
    Rule.prototype.isEnabled = function () {
        if (_super.prototype.isEnabled.call(this)) {
            var option = this.getOptions().ruleArguments[0];
            if (typeof option === 'number' && option > 0) {
                return true;
            }
        }
        return false;
    };
    Rule.prototype.apply = function (sourceFile) {
        return this.applyWithWalker(new TestWalker(sourceFile, this.getOptions()));
    };
    Rule.metadata = {
        ruleName: 'min-notes-rate',
        description: 'Notes rate of public service file cannot be too low.',
        rationale: 'rational',
        optionsDescription: 'Not configurable.',
        options: {
            type: 'number',
            minimum: '1'
        },
        optionExamples: ['true, 50'],
        type: 'maintainability'
    };
    Rule.FAILURE_STRING_SUFFIX = '% notes rate is less than the threshold of ';
    return Rule;
}(Lint.Rules.AbstractRule));
exports.Rule = Rule;
var SOURCEFILE_PATH = 'src/app/shared/services/';
var EXCLUDE_PATH = SOURCEFILE_PATH + 'test/';
var TestWalker = (function (_super) {
    __extends(TestWalker, _super);
    function TestWalker() {
        _super.apply(this, arguments);
    }
    TestWalker.prototype.visitSourceFile = function (node) {
        var _this = this;
        var path = node.path;
        var minNotesRate = this.getOptions()[0];
        if (path.indexOf(SOURCEFILE_PATH) > -1 && path.indexOf(EXCLUDE_PATH) < 0) {
            _super.prototype.visitSourceFile.call(this, node);
            var totalLineCnt = node.getLineStarts().length;
            var commentCnt_1 = 0;
            var blankLineCnt_1 = 0;
            var consecutiveNewLine_1 = 1;
            Lint.scanAllTokens(ts.createScanner(ts.ScriptTarget.ES5, false, ts.LanguageVariant.Standard, node.text), function (scanner) {
                var startPos = scanner.getStartPos();
                if (_this.tokensToSkipStartEndMap[startPos] != null) {
                    // tokens to skip are places where the scanner gets confused about what the token is, without the proper context
                    // (specifically, regex, identifiers, and templates). So skip those tokens.
                    scanner.setTextPos(_this.tokensToSkipStartEndMap[startPos]);
                    consecutiveNewLine_1 = 0;
                    return;
                }
                if (scanner.getToken() === ts.SyntaxKind.SingleLineCommentTrivia) {
                    // 单行注释
                    var width = scanner.getTokenText().length - 2;
                    if (width > 0) {
                        ++commentCnt_1;
                    }
                }
                else if (scanner.getToken() === ts.SyntaxKind.MultiLineCommentTrivia) {
                    // 多行注释
                    var startLine = ts.getLineAndCharacterOfPosition(node, scanner.getTokenPos()).line;
                    var endLine = ts.getLineAndCharacterOfPosition(node, scanner.getTextPos()).line;
                    commentCnt_1 += endLine - startLine + 1;
                }
                // 空行，当连续出现2个NewLineTrivia时，存在空行
                if (scanner.getToken() === ts.SyntaxKind.NewLineTrivia) {
                    ++consecutiveNewLine_1;
                    if (consecutiveNewLine_1 >= 2) {
                        blankLineCnt_1++;
                    }
                }
                else {
                    consecutiveNewLine_1 = 0;
                }
            });
            // 如果有eof token，算入最后一行空行
            var eofTokenFullText = node.endOfFileToken.getFullText();
            if (eofTokenFullText.length && eofTokenFullText.charAt(eofTokenFullText.length - 1) === '\n') {
                blankLineCnt_1++;
            }
            var notesRate = 100 * commentCnt_1 / (totalLineCnt - blankLineCnt_1);
            if (notesRate < minNotesRate) {
                var notesRateStr = notesRate.toFixed(2) + Rule.FAILURE_STRING_SUFFIX + minNotesRate + '%';
                var notesRateStatistics = this.createFailure(0, 0, notesRateStr);
                this.addFailure(notesRateStatistics);
            }
        }
    };
    return TestWalker;
}(Lint.SkippableTokenAwareRuleWalker));
