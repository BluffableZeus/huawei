import * as Lint from '../../node_modules/tslint/lib/lint'
import * as ts from 'typescript'
export declare class Rule extends Lint.Rules.AbstractRule {
  public static DEFAULT_THRESHOLD: number
  public static MINIMUM_THRESHOLD: number
  static metadata: Lint.IRuleMetadata
  public static ANONYMOUS_FAILURE_STRING: string
  public static NAMED_FAILURE_STRING: string

  apply (sourceFile: ts.SourceFile): Lint.RuleFailure[]
  isEnabled (): boolean
}
