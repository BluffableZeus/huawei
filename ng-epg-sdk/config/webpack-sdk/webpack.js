/**
 * @author: @AngularClass
 */

const webpack = require('webpack');
const helpers = require('./helpers');

const NormalModuleReplacementPlugin = require('webpack/lib/NormalModuleReplacementPlugin');
const ContextReplacementPlugin = require('webpack/lib/ContextReplacementPlugin');
const CommonsChunkPlugin = require('webpack/lib/optimize/CommonsChunkPlugin');
const CheckerPlugin = require('awesome-typescript-loader').CheckerPlugin;
const LoaderOptionsPlugin = require('webpack/lib/LoaderOptionsPlugin');


/**
 * Webpack configuration
 *
 * See: http://webpack.github.io/docs/configuration.html#cli
 */
module.exports = function () {

    function getConfig(config) {
        return {

            /**
             * Cache generated modules and chunks to improve performance for multiple incremental builds.
             * This is enabled by default in watch mode.
             * You can pass false to disable it.
             *
             * See: http://webpack.github.io/docs/configuration.html#cache
             */
            //cache: false,
            devtool: 'source-map',
            /**
             * The entry point for the bundle
             * Our Angular.js app
             *
             * See: http://webpack.github.io/docs/configuration.html#entry
             */
            entry: config.entry,

            /**
             * Options affecting the resolving of modules.
             *
             * See: http://webpack.github.io/docs/configuration.html#resolve
             */
            resolve: {

                /**
                 * An array of extensions that should be used to resolve modules.
                 *
                 * See: http://webpack.github.io/docs/configuration.html#resolve-extensions
                 */
                extensions: ['.ts', '.js', '.json'],

                /**
                 * An array of directory names to be resolved to the current directory
                 */
                modules: [helpers.root('node_modules')],

            },

            output: {

                /**
                 * The output directory as absolute path (required).
                 *
                 * See: http://webpack.github.io/docs/configuration.html#output-path
                 */
                path: helpers.root('build/sdk'),

                /**
                 * Specifies the name of each output file on disk.
                 * IMPORTANT: You must not specify an absolute path here!
                 *
                 * See: http://webpack.github.io/docs/configuration.html#output-filename
                 */
                filename: '[name].js',

                /**
                 * The filename of the SourceMaps for the JavaScript files.
                 * They are inside the output.path directory.
                 *
                 * See: http://webpack.github.io/docs/configuration.html#output-sourcemapfilename
                 */
                sourceMapFilename: '[file].umd.map',

                /** The filename of non-entry chunks as relative path
                 * inside the output.path directory.
                 *
                 * See: http://webpack.github.io/docs/configuration.html#output-chunkfilename
                 */
                chunkFilename: '[id].chunk.js',

                library: config.library,
                libraryTarget: 'umd',
            },

            /**
             * Options affecting the normal modules.
             *
             * See: http://webpack.github.io/docs/configuration.html#module
             */
            module: {

                rules: [

                    /**
                     * Typescript loader support for .ts
                     *
                     * Component Template/Style integration using `angular2-template-loader`
                     * Angular 2 lazy loading (async routes) via `ng-router-loader`
                     *
                     * `ng-router-loader` expects vanilla JavaScript code, not TypeScript code. This is why the
                     * order of the loader matter.
                     *
                     * See: https://github.com/s-panferov/awesome-typescript-loader
                     * See: https://github.com/TheLarkInn/angular2-template-loader
                     * See: https://github.com/shlomiassaf/ng-router-loader
                     */
                    {
                        exclude: [/\.(spec|e2e)\.ts$/],
                        test: /\.ts$/,
                        use: [{
                            loader: 'awesome-typescript-loader',
                            options: {
                                configFileName: './config/webpack-sdk/tsconfig-sdk.json',
                                useCache: false
                            }
                        }]
                    }
                ],

            },

            /**
             * Add additional plugins to the compiler.
             *
             * See: http://webpack.github.io/docs/configuration.html#plugins
             */
            plugins: [
                /**
                 * Plugin: ForkCheckerPlugin
                 * Description: Do type checking in a separate process, so webpack don't need to wait.
                 *
                 * See: https://github.com/s-panferov/awesome-typescript-loader#forkchecker-boolean-defaultfalse
                 */
                new CheckerPlugin(),
                /**
                 * Plugin: CommonsChunkPlugin
                 * Description: Shares common code between the pages.
                 * It identifies common modules and put them into a commons chunk.
                 *
                 * See: https://webpack.github.io/docs/list-of-plugins.html#commonschunkplugin
                 * See: https://github.com/webpack/docs/wiki/optimization#multi-page-app
                 */
                new CommonsChunkPlugin({
                    name: 'polyfills',
                    chunks: ['polyfills']
                }),
                /**
                 * Plugin LoaderOptionsPlugin (experimental)
                 *
                 * See: https://gist.github.com/sokra/27b24881210b56bbaff7
                 */
                new LoaderOptionsPlugin({}),
            ],

            /**
             * Include polyfills or mocks for various node stuff
             * Description: Node configuration
             *
             * See: https://webpack.github.io/docs/configuration.html#node
             */
            node: {
                global: true,
                crypto: 'empty',
                process: true,
                module: false,
                clearImmediate: false,
                setImmediate: false
            }
        };
    }
    const configs = [{
            entry: {
                'msa-http': './src/sdk/index.ts'
            },
            library: 'MsaHttp'
        },
        {
            entry: {
                'msa-log': './src/utils/logger.ts'
            },
            library: 'MsaLog'
        }
    ];
    return configs.map((config) => {
        return getConfig(config)
    });

}
