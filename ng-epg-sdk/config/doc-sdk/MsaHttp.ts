export interface RequestOptionsArgs {
    /**
     * 请求所要访问的URL。<br/>
     * 比如：
     * ```
     * https://VSP IP:VSP Port/VSP/V3/Login
     * ```
     */
  url: string
    /**
     * 请求参数。
     */
  body?: { [key: string]: any }
    /**
     * 设置HTTP请求头。<br/>
     * 符合XMLHttpRequest标准的键值对。
     */
  header?: { [key: string]: any }
    /**
     * 符合XMLHttpRequest标准的HTTP方法。<br/>
     * 默认为POST，有效值：GET, POST。
     */
  method?: 'GET' | 'POST'
    /**
     * 是否使用类似cookies,authorization headers(头部授权)或者TLS客户端证书这一类资格证书来创建一个跨站点访问控制（cross-site Access-Control）请求。<br/>
     * 布尔类型，默认为false。
     */
  withCredentials?: boolean

    /**
     * 请求超时时间。<br/>
     * 数值类型，单位秒，默认值15秒。
     */
  timeout?: number

    /**
     * 接口调用异常时，如果错误码在retryErrorCodes内，进行重试的次数。<br/>
     * 数值类型，默认值为0，表示不重试。最大值不大于3。
     */
  retryTimes?: number

    /**
     * 需要重试的场景，对应的网络异常错误码。<br/>
     * 为空则不会重试。
     */
  retryExceptionCode?: Array<number>

    /**
     * 表示该请求将在这段时间内，随机的一个时间点发送。<br/>
     * 不传则直接发送。<br/>
     * 数值类型，单位秒。
     */
  scatterTimeRange?: number
}

export interface ResponseOptionsArgs {
    /**
     * 服务器返回的状态码（HTTP Status）。
     */
  status?: number
    /**
     * 服务器返回的消息体（HTTP Response Body）。<br/>
     */
  body?: string
    /**
     * 服务器返回的响应头（HTTP Response Header）。<br/>
     * 以key:value键值对格式返回。
     */
  header?: { [key: string]: any }
    /**
     * 数据是否来自缓存，以及缓存的状态。<br/>
     * 取值如下：<br/>
     * FROM_SERVER：来自服务器的实时响应。<br/>
     * FROM_VALID_CACHE：来自未过期缓存的数据。<br/>
     * FROM_EXPIRED_CACHE：来自已过期缓存的数据。
     */
  dataSource?: 'FROM_SERVER' | 'FROM_VALID_CACHE' | 'FROM_EXPIRED_CACHE'
    /**
     * 如果服务器请求失败，但使用了缓存策略，则该字段表示服务器实际返回的状态码。
     */
  originCode?: number
}

export interface GlobalPolicy {
    /**
     * 全局缓存清理条件，表示调用这些接口时，需要清空所有缓存。<br/>
     * 已知需要进行全局清理的接口包括：'/VSP/V3/ModifyProfile','/VSP/V3/SwitchProfile','/VSP/V3/Authenticate'。
     */
  CLEANING_CONDITION?: Array<string>
    /**
     * 针对GET消息，在URL中?后面携带的参数中，部分参数是与接口业务无关的。<br/>
     * 例如页面场景ID，或者时间戳。<br/>
     * 在生成缓存key时，需要过滤掉此类参数。<br/>
     * 此参数用于配置需要过滤的参数名称。
     */
  FILTER_PARAMETERS?: Array<string>
}

export interface GlobalCachePolicy {
    /**
     * 全局缓存策略。<br/>
     * 比如：
     * ```
     * {
     *     "Global": {
     *         "CLEANING_CONDITION": [ '/VSP/V3/ModifyProfile','/VSP/V3/SwitchProfile','/VSP/V3/Authenticate'],
     *         "FILTER_PARAMETERS": ["scenario"]
     *     }
     * }
     * ```
     */
  Global?: GlobalPolicy
}

export interface DefaultPolicy {
    /**
     * 接口调用成功的关键字。
     */
  SUCCESS_KEY?: Array<string>

    /**
     * 默认缓存池大小。
     */
  CACHE_NUMBER?: number

    /**
     * 默认缓存有效期。
     */
  MAX_AGE?: number

    /**
     * 令牌桶中最大令牌个数。<br/>
     * 每个令牌表示一次API调用的机会。<br/>
     * 数值类型，最小值为1。
     */
  TOKEN_NUMBER?: number

    /**
     * 令牌桶中令牌增长速度，表示多少秒增加一个令牌。
     */
  TOKEN_RATE?: number
}

export interface DefaultCachePolicy {
    /**
     * 默认缓存策略。<br/>
     * 比如：
     * ```
     * {
     *     "Default": {
     *          "CACHE_NUMBER": 10,
     *          "MAX_AGE": 3600,
     *          "TOKEN_NUMBER": 10,
     *          "TOKEN_RATE": 3,
     *          "SUCCESS_KEY": ["\"retCode\":\"000000000\""]
     *     }
     * }
     * ```
     */
  Default?: DefaultPolicy
}

export interface InfPolicy {
    /**
     * 是否使用SDK缓存能力。<br/>
     * 默认为false，不使用SDK缓存。<br/>
     * 只有配置为true的接口才使用SDK缓存。<br/>
     * 打开之后，提供有效期内的缓存能力，以及在接口异常时提供过期数据的应急缓存数据。
     */
  CACHE_SWITCH?: boolean

    /**
     * 缓存清理条件。<br/>
     * 说明哪些接口被调用时，需要清理当前接口的缓存池。<br/>
     * 清理缓存仅限于当前Profile中的缓存；不同Profile缓存相互不可见。
     */
  CLEANING_CONDITION?: Array<string>

    /**
     * 接口调用成功的关键字，用于判断接口调用是否成功。<br/>
     * SDK在接口响应消息中进行搜索，命中则表示此次接口调用是成功的。
     */
  SUCCESS_KEY?: Array<string>

    /**
     * 允许缓存的最大数量。<br/>
     * 数值类型。
     */
  CACHE_NUMBER?: number

    /**
     * 缓存有效期，表示每条缓存记录多久失效。<br/>
     * 缓存有效期必须大于0。<br/>
     * 数值类型，单位秒。
     */
  MAX_AGE?: number

    /**
     * 令牌桶中最大令牌个数。<br/>
     * 每个令牌表示一次API调用的机会。<br/>
     * 最小值为1。
     */
  TOKEN_NUMBER?: number

    /**
     * 令牌桶中令牌增长速度，表示多少秒增加一个令牌。
     */
  TOKEN_RATE?: number
}

export interface InfCachePolicy {
    /**
     * 接口缓存策略。<br/>
     * 接口不包括服务器地址和端口。<br/>
     * 比如：
     * ```
     * {
     *    "VSP/V3/QueryFavorite":{
     *        "TOKEN_NUMBER": 20,
     *        "TOKEN_RATE": 1
     *    }
     * }
     * ```
     */
  [inf: string]: InfPolicy
}

export type FullCachePolicy = InfCachePolicy & GlobalCachePolicy & DefaultCachePolicy

export class Http {

    /**
     *
     * 该接口用于发送HTTP/HTTPS消息的异步方法。<br/>
     * 需要传入消息的url、body和header等内容。<br/>
     * SDK会根据url取值，自动选择对应的缓存策略。
     * @param {RequestOptionsArgs} request 需要发送的请求信息。
     * @returns {Promise<ResponseOptionsArgs>} Promise对象，Resolved状态：返回请求结果；Rejected状态：表示请求失败。
     */
  public request (request: RequestOptionsArgs): Promise<ResponseOptionsArgs> {
      return Promise.resolve({})
    }

    /**
     * 设置缓存规则。<br/>
     * 这里设置的规则，是增量的，即不会覆盖SDK内部已有的不重名规则。<br/>
     * 只有接口URL相同，才覆盖SDK内部规则。<br/>
     * 规则由全局规则、默认规则、接口规则三部分组成。<br/>
     * 文件格式需要符合FullCachePolicy定义，如果不符合，则规则不生效。<br/>
     * 比如：
     * ```
     * {
     *     "Global": {
     *         "CLEANING_CONDITION": [
     *             "/VSP/V3/ModifyProfile"
     *         ],
     *         "FILTER_PARAMETERS": [
     *             "scenario"
     *         ]
     *     },
     *     "Default": {
     *         "CACHE_NUMBER": 10,
     *         "MAX_AGE": 3600,
     *         "TOKEN_NUMBER": 10,
     *         "TOKEN_RATE": 3,
     *         "SUCCESS_KEY": [
     *             "\"retCode\":\"000000000\""
     *         ]
     *     },
     *     "/VSP/V3/GetContentConfig": {
     *         "CACHE_SWITCH": true,
     *         "CACHE_NUMBER": 2,
     *         "MAX_AGE": 86400
     *     }
     * }
     * ```
     * @param {FullCachePolicy} policy 缓存规则。
     * @returns {void} 无返回值。
     */
  public setCachePolicy (policy: FullCachePolicy): void {
      return
    }

    /**
     * 清理接口缓存。<br/>
     * @param {string} url 接口名称，不包含服务器地址和端口，比如："/VSP/V3/QueryFavorite"。
     * @returns {Promise<boolean>} Promise对象，Resolved状态：true表示清理成功，false表示无需清理；Rejected状态：表示清理失败。
     */
  public clearCache (url: string): Promise<boolean> {
      return Promise.resolve(true)
    }

    /**
     * 用于设置请求超时时间。<br/>
     * 不设置，默认为15秒。<br/>
     * 数值类型，单位秒。
     * @param {number} second 超时时间。
     * @returns {boolean}  设置成功返回true，设置失败返回false。
     */
  public setTimeout (second: number): boolean {
      return true
    }

}
