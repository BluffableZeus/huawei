export class Logger {
    /**
     * 设置日志级别，默认为ERROR级别。<br/>
     * 只支持'DEBUG'、'INFO'、'KPI'、'ERROR'级别。
     * @static
     * @param {string} level 日志级别。
     * @returns {void} 无返回值。
     */
  public static setLevel (level: 'DEBUG' | 'INFO' | 'KPI' | 'ERROR'): void {

    }
    /**
     * 构造函数。
     * @param {string} name 日志名称。
     * @returns {Logger} Logger实例。
     */
  constructor (name: string) {
    }
    /**
     * 输出DEBUG级别日志。
     * @param {string} msg 一个JavaScript字符串,其中包含零个或多个替代字符串（%s）。
     * @param {...Array<any>} args JavaScript对象，用来依次替换msg中的替代字符串。
     * @returns {void} 无返回值。
     */
  public debug (msg: string, ...args: Array<any>): void {

    }
    /**
     * 当前日志级别是否为DEBUG级别。
     * @returns {boolean} 是或否。
     */
  public isDebug (): boolean {
      return true
    }
    /**
     * 输出INFO级别日志。
     * @param {string} msg 一个JavaScript字符串,其中包含零个或多个替代字符串。
     * @param {...Array<any>} args JavaScript对象，用来依次替换msg中的替代字符串。
     * @returns {void} 无返回值。
     */
  public info (msg: string, ...args: Array<any>): void {

    }
    /**
     * 当前日志级别是否为INFO级别。
     * @returns {boolean} 是或否。
     */
  public isInfo (): boolean {
      return true
    }
    /**
     * 输出KPI级别日志。
     * @param {string} msg 一个JavaScript字符串,其中包含零个或多个替代字符串。
     * @param {...Array<any>} args JavaScript对象，用来依次替换msg中的替代字符串。
     * @returns {void} 无返回值。
     */
  public kpi (msg: string, ...args: Array<any>): void {

    }

    /**
     * 当前日志级别是否为KPI级别。
     * @returns {boolean} 是或否。
     */
  public isKpi (): boolean {
      return true
    }
    /**
     * 输出ERROR级别日志。
     * @param {string} msg 一个JavaScript字符串,其中包含零个或多个替代字符串。
     * @param {...Array<any>} args JavaScript对象，用来依次替换msg中的替代字符串。
     * @returns {void} 无返回值。
     */
  public error (msg: string, ...args: Array<any>): void {

    }
    /**
     * 当前日志级别是否为ERROR级别。
     * @returns {boolean} 是或否。
     */
  public isError (): boolean {
      return true
    }

}
