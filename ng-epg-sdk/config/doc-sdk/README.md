## 使用说明
### MsaHttp.Http
如果需要使用SDK缓存和流控能力，需要实现以下接口。

#### setCachePolicy
业务在初始化阶段需要调用setCachePolicy，将缓存规则下发给SDK使用。
<p>
<font color=red>如果从未设置过缓存规则，SDK缓存和流控能力将无法实现。</font>
</p>
规则由全局规则、默认规则、接口规则三部分组成。

#### request
发送HTTP/HTTPS消息的异步方法，调用服务器接口时使用。

#### clearCache
清理缓存接口。当不需要使用缓存时，可在调用该接口后，调用服务器接口获取数据。

#### setTimeout
请求超时时间设置，默认15s。

### MsaLog.Logger
如果需要使用SDK日志打印能力，可以实现以下接口。
#### setLevel
设置日志级别，默认为ERROR级别。

日志级别从低到高按下述定义，低级别日志包括高级别日志。
```
debug < info < kpi < error
```

可以通过该接口改变日志输出级别，便于问题定位。

### debug/info/kpi/error
不同级别的日志输出方法，可根据需要使用。
