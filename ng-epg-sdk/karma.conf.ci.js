module.exports = function(config) {
    var testWebpackConfig = require('./webpack.config.js');

    var configuration = {


        // base path that will be used to resolve all patterns (e.g. files, exclude)
        basePath: '',

        frameworks: ['jasmine'],

        exclude: [],

        files: [{
            pattern: './spec-bundle.js',
            watched: false
        }],

        preprocessors: { './spec-bundle.js': ['coverage', 'webpack', 'sourcemap'] },

        webpack: testWebpackConfig,

        coverageReporter: {
            type: 'in-memory'
        },

        remapCoverageReporter: {
            'text-summary': null,
            cobertura: './build/coverage/coverage.xml',
            json: './build/coverage/coverage.json',
            html: './build/coverage/html'
        },

        webpackMiddleware: { stats: 'errors-only' },

        reporters: ['mocha', 'coverage', 'remap-coverage'],

        port: 9876,

        colors: true,

        logLevel: config.LOG_ERROR,

        autoWatch: false,

        browsers: [
            'Chrome'
        ],

        singleRun: true
    };

    config.set(configuration);
};
