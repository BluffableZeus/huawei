var path = require('path');
var ROOT = path.resolve(__dirname);
var TsConfigPathsPlugin = require('awesome-typescript-loader').TsConfigPathsPlugin;

function root(args) {
    args = Array.prototype.slice.call(arguments, 0);
    return path.join.apply(path, [ROOT].concat(args));
}

module.exports = {
    devtool: 'inline-source-map',
    resolve: {
        extensions: ['', '.ts', '.js'],
        root: root('src')
    },
    module: {

        preLoaders: [{
                test: /\.js$/,
                loader: 'source-map-loader',
                exclude: [
                    root('node_modules/rxjs'),
                ]
            }

        ],
        loaders: [{
                test: /\.ts$/,
                loader: 'awesome-typescript-loader',
                query: {
                    sourceMap: false,
                    inlineSourceMap: true,
                    compilerOptions: {
                        removeComments: true,
                        typeRoots: ["./node_modules/@types"]
                    }
                },
                exclude: [/\.e2e\.ts$/]
            },

            { test: /\.json$/, loader: 'json-loader', exclude: [root('src/index.html')] }

        ],
        postLoaders: [{
            test: /\.(js|ts)$/,
            loader: 'istanbul-instrumenter-loader',
            include: root('src'),
            exclude: [
                /\.(e2e|spec)\.ts$/,
                /node_modules/
            ]
        }]
    },
    plugins: [],
    node: {
        global: 'window',
        process: false,
        crypto: 'empty',
        module: false,
        clearImmediate: false,
        setImmediate: false
    }
};