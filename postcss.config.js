const postcssPresetEnv = require('postcss-preset-env')
const autoprefixer = require('autoprefixer')
const cssnano = require('cssnano')
const purgecss = require('@fullhuman/postcss-purgecss')

module.exports = {
  plugins: [
    postcssPresetEnv,
    purgecss({ content: ['./src/**/*.html', './ng-epg-ui/src/**/*.html'], whitelistPatterns: [/dynamic-class/] }),
    autoprefixer,
    cssnano
  ]
};
